<%-- 
    Document   : PromisReportPDF
    Created on : Mar 31, 2012, 10:40:29 AM
    Author     : shreyansh.shah
--%>


<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FinancialYearService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    FinancialYearService financialYearService = (FinancialYearService) AppContext.getSpringBean("financialYearService");
                    int userid = 0;

                    HttpSession hs = request.getSession();

                    if (hs != null) {
                        if (hs.getAttribute("userId") != null) {
                            userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        }
                    } else {
                        userid = Integer.parseInt(request.getParameter("userId").toString());
                    }

                    boolean isPDF = false;
                    if (request.getParameter("isPDF") != null) {
                        isPDF = true;
                    }

                    String Fyear = request.getParameter("fyear");
                    String PType = request.getParameter("ptype");
                    String DeptId = request.getParameter("deptid");
                    String Officeid = request.getParameter("officeid");
                    String PEID = request.getParameter("PEId");
                    String PageNo = request.getParameter("pageNo");
                    String Size = request.getParameter("size");
                    String districtId = request.getParameter("districtId");
                    String quater = request.getParameter("quater");
                    String projectId = request.getParameter("projectId");

                    //  System.out.println("isPDF value :"+isPDF);
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Promise Indicator</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //$('#titleDiv').show();
                $('#print_div').printElement(options);
                //$('#titleDiv').hide();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            
                        <div id="print_div">
                            <table width="100%">
                                <tr>
                                    <td><div class="tableHead_1 t_space" id="getSubHeading">
                    1. Percentage of Invitation for Tender/Proposal (IFT) Published in Newspaper
                </div></td>
                                </tr>
                                <tr>
                                    <td>

                
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                        <tr id="subtableheading">

                        </tr>

                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="1000"/>
                    </div>
                
                <div>&nbsp;</div>
                                    </td></tr>
                </table>
                        </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

    <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        function changeTab(tabNo){
            loadTenderTableone();
        }

    </script>




    <script type="text/javascript">

        /**
         *  In PROMIS Indicator columns are changing based on indicator. Here we have created function which will display
         *  column heading based on indicator selected by user from listbox.
         *  @param peid
         *  @return list of columns for selected indicator id
         *  Note : For details of columns heading for each indicator you can refer report format given by Dohatec
         */
        function getTableHeading()
        {
            var tableheadings = '';
            var PromiseIndicatorId = 0;
            PromiseIndicatorId = '<%=PEID%>';
           if (PromiseIndicatorId == 1) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No. of Tenders/Proposals Published in Newspaper</th>'+
                    '<th width="10%" class="t-align-center">Percentage of Tenders/Proposals Published in Newspaper</th>'
            } else if (PromiseIndicatorId == 2) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No. of Tenders/Proposals Published in CPTU Website</th>'+
                    '<th width="10%" class="t-align-center">Percentage of Tenders/Proposals Published in CPTU Website</th>'
            } else if (PromiseIndicatorId == 3) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal Following Rules</th>'+
                    '<th width="15%" class="t-align-center">Tenders/Proposals Following GoB Procurement Rules</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Following GoB Procurement Rules %</th>'
            } else if (PromiseIndicatorId == 4) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal Following Rules</th>'+
                    '<th width="15%" class="t-align-center">Tenders/Proposals Following Development Partner Rules</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Following  Development Partner Rules %</th>'
            } else if (PromiseIndicatorId == 5) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Tenders/Proposals Submission in Multiple Location</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Submission in Multiple Location %</th>'
            } else if (PromiseIndicatorId == 6) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="10%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Days (Submission-Advertisement)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Submission-Advertisement)</th>'
            } else if (PromiseIndicatorId == 7) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Having Sufficient Tender/Proposal Submission Time</th>'+
                    '<th width="20%" class="t-align-center">Having Sufficient Tender/Proposal Submission Time(%)</th>'
            } else if (PromiseIndicatorId == 8) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No. of Tenderers/Consultants Purchased Tender/Proposal Documents</th>'+
                    '<th width="20%" class="t-align-center">Avg No. of Tenderers/Consultants Purchased Tender/Proposal Documents</th>'
            } else if (PromiseIndicatorId == 9) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No of Tenderers/Consultants Submitted Tenders/Proposals</th>'+
                    '<th width="20%" class="t-align-center">Avg No of Tenderers/Consultants Submitted Tenders/Proposals</th>'
            } else if (PromiseIndicatorId == 10) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Tender/Proposal Documents Sold</th>'+
                    '<th width="15%" class="t-align-center">Tenderers/Consultants Submitted Tenders/Proposals</th>'+
                    '<th width="20%" class="t-align-center">Ratio of No. of Tender/Proposal Document Sold and No. of Participants</th>'
            } else if (PromiseIndicatorId == 11) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TOC Included at Least One Member from PEC/TEC</th>'+
                    '<th width="20%" class="t-align-center">TOC Included at Least One Member from PEC/TEC(%)</th>'
            } else if (PromiseIndicatorId == 12) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Formed by Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TEC Formed by Approving Authority (%)</th>'
            } else if (PromiseIndicatorId == 13) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Included Two External Members</th>'+
                    '<th width="20%" class="t-align-center">TEC Included 2 External Members (%)</th>'
            } else if (PromiseIndicatorId == 14) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Hierarchy Node Name</th>'+
                    '<th width="20%" class="t-align-center">Total Evaluation Completed Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days (Evaluation-Opening)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Evaluation-Opening)</th>'
            } else if (PromiseIndicatorId == 15) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Approved Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Approval Completed within Timeline</th>'+
                    '<th width="20%" class="t-align-center">Approval Completed within Timeline (%)</th>'
            } else if (PromiseIndicatorId == 16) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="9%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="9%" class="t-align-center">Total Evaluation Completed Tender/Proposal </th>'+
                    '<th width="9%" class="t-align-center">Total Participants</th>'+
                    '<th width="9%" class="t-align-center">Total Responsive</th>'+
                    '<th width="9%" class="t-align-center">Average Participants</th>'+
                    '<th width="9%" class="t-align-center">Average Responsive</th>'
            } else if (PromiseIndicatorId == 17) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Recommended Re-Tendering</th>'+
                    '<th width="20%" class="t-align-center">TEC Recommended Re-Tendering (%)</th>'
            } else if (PromiseIndicatorId == 18) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Cancelled Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Cancelled Tender/Proposal(%)</th>'
            } else if (PromiseIndicatorId == 19) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="20%" class="t-align-center">Total Submitted Tender/Proposal for Approval</th>'+
                    '<th width="15%" class="t-align-center">Total Days (Evaluation to Approval)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Approval-Evaluation)</th>'
            } else if (PromiseIndicatorId == 20) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Approved by Financial Delegated Authority</th>'+
                    '<th width="20%" class="t-align-center">Approved by Financial Delegated Authority (%)</th>'
            } else if (PromiseIndicatorId == 21) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Report Submitted to the Contract Approving Authority</th>'+
                    '<th width="15%" class="t-align-center">TEC Submitted Report Directly to the Contract Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TEC Submitted Report Directly to the Contract Approving Authority(%)</th>'
            } else if (PromiseIndicatorId == 22) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Contract Award Decision Made within Timeline</th>'+
                    '<th width="20%" class="t-align-center">Contract Award Decision Made within Timeline(%)</th>'
            } else if (PromiseIndicatorId == 23) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Report Submitted to the Contract Approving Authority</th>'+
                    '<th width="15%" class="t-align-center">TER Reviewed by Person/Commettee Besides Contract Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TER Reviewed by Person/Commettee Besides Contract Approving Authority(%)</th>'
            } else if (PromiseIndicatorId == 24) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Higher Tier Approval</th>'+
                    '<th width="20%" class="t-align-center">Total Higher Tier Approval (%)</th>'
            } else if (PromiseIndicatorId == 25) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Approval to LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Approval to LOA)</th>'
            } else if (PromiseIndicatorId == 26) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Opening to LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Opening to LOA)</th>'
            } else if (PromiseIndicatorId == 27) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Invitation-LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Invitation-LOA)</th>'
            } else if (PromiseIndicatorId == 28) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="20%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Awarded Tender/Proposal </th>'+
                    '<th width="10%" class="t-align-center">Publicly Award Disclosed</th>'+
                    '<th width="10%" class="t-align-center">Publicly Award Disclosed(%)</th>'
            } else if (PromiseIndicatorId == 29) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="35%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Awarded Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Completed Tender/Proposal within Initial Validity Period</th>'+
                    '<th width="20%" class="t-align-center">Complete Tender/Proposal within Initial Validity Period (%)</th>'
            } else if (PromiseIndicatorId == 30) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Delivered Tender/Proposal within Original Schedule</th>'+
                    '<th width="20%" class="t-align-center">Delivered Tender/Proposal within Original Schedule(%)</th>'
            } else if (PromiseIndicatorId == 31) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Contracts Having Liquidated Damage</th>'+
                    '<th width="20%" class="t-align-center">Contracts Having Liquidated Damage(%)</th>'
            } else if (PromiseIndicatorId == 32) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Contract Signed</th>'+
                    '<th width="15%" class="t-align-center">Contract Completed</th>'+
                    '<th width="20%" class="t-align-center">Contract Completed(%)</th>'
            } else if (PromiseIndicatorId == 33) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="25%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="25%" class="t-align-center">Average no of Days</th>'
            } else if (PromiseIndicatorId == 34) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="10%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="10%" class="t-align-center">Total Cases</th>'+
                    '<th width="10%" class="t-align-center">Tenders/Proposals of Late Payment</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals of Late Payment(%)</th>'
            } else if (PromiseIndicatorId == 35) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Interests Paid for Delayed Payment</th>'+
                    '<th width="20%" class="t-align-center">Interests Paid for Delayed Payment(%)</th>'
            } else if (PromiseIndicatorId == 36) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Approved Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Tender/Proposal Procedure Complaints</th>'+
                    '<th width="20%" class="t-align-center">Tender/Proposal Procedure Complaints(%)</th>'
            } else if (PromiseIndicatorId == 37) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="10%" class="t-align-center">Total Complaints</th>'+
                    '<th width="10%" class="t-align-center">Total Resolution</th>'+
                    '<th width="15%" class="t-align-center">Resolution of Compliants with Modification of Award</th>'+
                    '<th width="15%" class="t-align-center">Percentage of Resolution of Compliants with Modification of Award(%)</th>'
            } else if (PromiseIndicatorId == 38) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Complaints</th>'+
                    '<th width="15%" class="t-align-center">Total Resolved Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Resolved Tender/Proposal(%)</th>'
            } else if (PromiseIndicatorId == 39) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="10%" class="t-align-center">Total Complaints</th>'+
                    '<th width="10%" class="t-align-center">Total Resolution</th>'+
                    '<th width="15%" class="t-align-center">Contracts having Review Panel\'s Decisions Upheld</th>'+
                    '<th width="15%" class="t-align-center">Contracts having Review Panel\'s Decisions Upheld(%)</th>'
            } else if (PromiseIndicatorId == 40) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="10%" class="t-align-center">Advertised Tender/Proposal(s)</th>'+
                    '<th width="10%" class="t-align-center">Contract Signed</th>'+
                    '<th width="15%" class="t-align-center">Contract Variation</th>'+
                    '<th width="15%" class="t-align-center">Contract Variation (%)</th>'
            } else if (PromiseIndicatorId == 41) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Contract</th>'+
                    '<th width="15%" class="t-align-center">Unresolved Dispute</th>'+
                    '<th width="20%" class="t-align-center">Dispute Unresolved Dispute(%)</th>'
            } else if (PromiseIndicatorId == 42) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal with Fraud and Corruption Cases</th>'+
                    '<th width="20%" class="t-align-center">Percentage of Tender/Proposal with Fraud and Corruption Cases(%)</th>'
            } else if (PromiseIndicatorId == 43) {
                tableheadings = '<th width="10%" class="t-align-center">Total No. of Procuring Entities</th>'+
                    '<th width="40%" class="t-align-center">Total Trained Procurement Staff</th>'+
                    '<th width="50%" class="t-align-center">Average No. of Trained Procurement Staff</th>'
            } else if (PromiseIndicatorId == 44) {
                tableheadings = '<th width="10%" class="t-align-center">Total No. of Procuring Entities</th>'+
                    '<th width="40%" class="t-align-center">Total No. of Procurement Entities which has Atleast One Trained/Certified Staff</th>'+
                    '<th width="50%" class="t-align-center">Percentage Of Procurement Entities which has Atleast One Trained/Certified Staff</th>'
            } else if (PromiseIndicatorId == 45) {
                tableheadings = '<th width="10%" class="t-align-center">Agency ID</th>'+
                    '<th width="40%" class="t-align-center">Agency Name</th>'+
                    '<th width="50%" class="t-align-center">Trained Staff</th>'
            }
            return tableheadings;
        }
        /**
         *  In PROMIS Indicator heading change according to selected indicator id.
         *  Here we have cteated function which will return heading of selected promis id
         *  @param peid
         *  @return Heading of promis report
         *  To check heading of promis report you can refer report format given by Dohatec
         */
        function getMainHeading()
        {
            var peid;
            var Heading = "";
            peid = '<%=PEID%>'
            //  alert(peid);
            switch (parseInt(peid)) {
                case 1:
                    Heading = "1. Advertisement of Tender/Proposal Opportunities in Newspaper";
                    break;
                case 2:
                    Heading = "2. Advertisement of Tender/Proposal Opportunities in CPTU's Website";
                    break;
                case 3:
                    Heading = "3. Tenders/Proposals Following GoB Procurement Rules";
                    break;
                case 4:
                    Heading = "4. Tenders/Proposals Following Development Partner Rules";
                    break;
                case 5:
                    Heading = "5. Multiple Locations Submission Tenders/Proposals";
                    break;
                case 6:
                    Heading = "6. Tender/Proposal Preparation Time in Open Tendering Method";
                    break;
                case 7:
                    Heading = "7. Tender/Proposal Time Compliance";
                    break;
                case 8:
                    Heading = "8. Sale of Tender/Proposal Documents";
                    break;
                case 9:
                    Heading = "9. Bidder/Consultant Participation";
                    break;
                case 10:
                    Heading = "10. Bidder/Consultant Participation Index";
                    break;
                case 11:
                    Heading = "11. Tender/Proposal Opening Committee Formation";
                    break;
                case 12:
                    Heading = "12. Tender/Proposal Evaluation Committee Formation";
                    break;
                case 13:
                    Heading = "13. External Member in TEC";
                    break;
                case 14:
                    Heading = "14. Tender/Proposal Evaluation Time";
                    break;
                case 15:
                    Heading = "15. Compliance of Tender/Proposal Evaluation Time";
                    break;
                case 16:
                    Heading = "16. Tender/Proposal Acceptance";
                    break;
                case 17:
                    Heading = "17. Re-Tendering";
                    break;
                case 18:
                    Heading = "18. Tender/Proposal Cancellation";
                    break;
                case 19:
                    Heading = "19. Tender/Proposal Evaluation Approval Time";
                    break;
                case 20:
                    Heading = "20. Compliance of Financial Delegation";
                    break;
                case 21:
                    Heading = "21. Submission of Evaluation Report to Appropriate Authority";
                    break;
                case 22:
                    Heading = "22. TER Approval Compliance";
                    break;
                case 23:
                    Heading = "23. Additional Review of TER";
                    break;
                case 24:
                    Heading = "24. Higher Tier Approval";
                    break;
                case 25:
                    Heading = "25. Time for Issuance of LOA to Bidder/Consultant";
                    break;
                case 26:
                    Heading = "26. Tender/Proposal Processing Lead Time";
                    break;
                case 27:
                    Heading = "27. Total Tender/Proposal Processing Time";
                    break;
                case 28:
                    Heading = "28. Publication of Award Information";
                    break;
                case 29:
                    Heading = "29. Efficiency in Contract Award";
                    break;
                case 30:
                    Heading = "30. Delivery Time";
                    break;
                case 31:
                    Heading = "31. Liquidated Damage";
                    break;
                case 32:
                    Heading = "32. Completion Rate";
                    break;
                case 33:
                    Heading = "33. Payment Release Compliance";
                    break;
                case 34:
                    Heading = "34. Late Payment";
                    break;
                case 35:
                    Heading = "35. Interests Paid for Delayed Payment";
                    break;
                case 36:
                    Heading = "36. Tender/Proposal Procedure Complaints";
                    break;
                case 37:
                    Heading = "37. Resolution of Complaints With Award Modification";
                    break;
                case 38:
                    Heading = "38. Resolution of Complaints";
                    break;
                case 39:
                    Heading = "39. Independent Review Panel";
                    break;
                case 40:
                    Heading = "40. Contract Amendments/Variations";
                    break;
                case 41:
                    Heading = "41. Unresolved Disputes";
                    break;
                case 42:
                    Heading = "42. Fraud and Corruption";
                    break;
                case 43:
                    Heading = "43. Procurement Training";
                    break;
                case 44:
                    Heading = "44. Percentage of Procurement Entities with Trained Persons";
                    break;
                case 45:
                    Heading = "45. Number of Procurement Persons by Organization";
                    break;
                default:

                    Heading = "1. Advertisement of Tender/Proposal Opportunities in Newspaper";

            }
            return Heading;
        }
        /**
         * In PROMIS Indicator subheadings are changing based on indicator. Here we have created function which will display
         *  subheading heading based on indicator selected by user from listbox.
         *  @param peid
         *  @return SubHeading for selected indicator id
         *  Note : For details of  subheading for each indicator you can refer report format given by Dohatec
         */
        function getSubTitle() {
            var SubHeading = "";
            var peid;
            peid = '<%=PEID%>';
            switch (parseInt(peid)) {
                case 1:
                    SubHeading = "1. Percentage of Invitation for Tender/Proposal (IFT) Published in Newspaper";
                    break;
                case 2:
                    SubHeading = "2. Percentage of Invitation for Tender/Proposal (above threshold) Advertised in CPTU’s Website";
                    break;
                case 3:
                    SubHeading = "3. Percentage of Tenders/Proposals Following GoB Procurement Rules";
                    break;
                case 4:
                    SubHeading = "4. Percentage of Tenders/Proposals Following Development Partner Rules";
                    break;
                case 5:
                    SubHeading = "5. Percentage of Tenders/Proposals Allowed to Submit in Multiple Locations";
                    break;
                case 6:
                    SubHeading = "6. Average Number of Days Between Publishing of Advertisement and Tender/Proposal Submission Deadline";
                    break;
                case 7:
                    SubHeading = "7. Percentage of Tenders/Proposals having Sufficient Tender/Proposal Submission Time";
                    break;
                case 8:
                    SubHeading = "8. Average Number of Tenderers/Consultants Purchased Tender/Proposal Documents";
                    break;
                case 9:
                    SubHeading = "9. Average Number of Tenderers/Consultants Submitted Tenders/Proposals";
                    break;
                case 10:
                    SubHeading = "10. Ratio of Number of Tender/Proposal Document Sold and Number of Tender/Proposal Submission";
                    break;
                case 11:
                    SubHeading = "11. Percentage of Cases TOC Included At Least One Member From PEC/TEC";
                    break;
                case 12:
                    SubHeading = "12. Percentage of Cases TEC Formed by Approving Authority";
                    break;
                case 13:
                    SubHeading = "13. Percentage of Cases TEC Included Two External Members Outside The Procuring Entity";
                    break;
                case 14:
                    SubHeading = "14. Average Number of Days Between Tender/Proposal Opening and Completion of Evaluation";
                    break;
                case 15:
                    SubHeading = "15. Percentage of Cases Tender/Proposal Evaluation has been Completed within Timeline";
                    break;
                case 16:
                    SubHeading = "16. Average Number of Responsive Tenders/Proposals";
                    break;
                case 17:
                    SubHeading = "17. Percentage of Cases TEC Recommended Re-Tendering";
                    break;
                case 18:
                    SubHeading = "18. Percentage of Cases where Tender/Proposal Process Cancelled";
                    break;
                case 19:
                    SubHeading = "19. Average Number of Days Taken Between Submission of Tender/Proposal Evaluation and Approval Contract";
                    break;
                case 20:
                    SubHeading = "20. Average Number of Tenders/Proposals Approved by Proper Financial Delegated Authority";
                    break;
                case 21:
                    SubHeading = "21. Percentage of Cases TEC Submitted Report Directly to The Contract Approving Authority";
                    break;
                case 22:
                    SubHeading = "22. Percentage of Cases Contract Award Decision Made within Timeline by Contract Approving Authority";
                    break;
                case 23:
                    SubHeading = "23. Percentage of Cases TER Reviewed by Person/Committee Other Than The Contract Approving Authority";
                    break;
                case 24:
                    SubHeading = "24. Percentage of Tenders/Proposals Approved by Higher Tier than the Contract Approving Authority";
                    break;
                case 25:
                    SubHeading = "25. Average Number of Days Between Final Approval and Letter of Acceptance (LOA)";
                    break;
                case 26:
                    SubHeading = "26. Average Number of Days Between Tender/Proposal Opening and Letter of Acceptance (LOA)";
                    break;
                case 27:
                    SubHeading = "27. Average Number of Days Between Invitation for Tender/Proposal (IFT) and Letter of Acceptance (LOA)";
                    break;
                case 28:
                    SubHeading = "28. Percentage of Contract Award Published in CPTU’s Website";
                    break;
                case 29:
                    SubHeading = "29. Percentage of Contract Awarded within Initial Tender/Proposal Validity Period";
                    break;
                case 30:
                    SubHeading = "30. Percent of Contracts Completed/Delivered within the Original Schedule as Mentioned in Contract";
                    break;
                case 31:
                    SubHeading = "31. Percentage of Contracts Having Liquidated Damage Imposed for Delayed Delivery/Completion";
                    break;
                case 32:
                    SubHeading = "32. Percentage of Contracts Fully Completed and Accepted";
                    break;
                case 33:
                    SubHeading = "33. Average Number of Days Taken to Release Payments";
                    break;
                case 34:
                    SubHeading = "34. Percentage of Cases (considering each installment as a case) with Delayed Payment";
                    break;
                case 35:
                    SubHeading = "35. Percentage of Contracts where Interest for Delayed Payments was Made";
                    break;
                case 36:
                    SubHeading = "36. Percentage of Tender/Proposal Procedure Complaints";
                    break;
                case 37:
                    SubHeading = "37. Percentage of Complaints Resulting in Modification of Awards";
                    break;
                case 38:
                    SubHeading = "38. Percentage of Cases Complaints have been Resolved";
                    break;
                case 39:
                    SubHeading = "39. Percentage of Cases Review Panel’s Decisions Upheld";
                    break;
                case 40:
                    SubHeading = "40. Percentage of Contract Amendments/Variations";
                    break;
                case 41:
                    SubHeading = "41. Percentage of Contracts With Unresolved Disputes";
                    break;
                case 42:
                    SubHeading = "42. Percentage of Tenders/Proposals of Fraud and Corruption";
                    break;
                case 43:
                    SubHeading = "43. Average no of Trained Procurement Staff in Each Procuring Entity";
                    break;
                case 44:
                    SubHeading = "44. Percentage of Procuring Entity Which Has At Least One Trained/Certified Procurement Staff";
                    break;
                case 45:
                    SubHeading = "45. Total Number of Procurement Persons in The Organization With Procurement Training";
                    break;

            }
            return SubHeading;
        }
        /**
         * This function loads all data based on filter selected by user
         */
        function loadTenderTable()
        {
            //alert("Indicatror id :"+);
            var Heading = getMainHeading();
            var deptid = 0;
            $("#Mainheading").html(Heading);
            var SubHeading = getSubTitle();
            $("#getSubHeading").html(SubHeading);
            var subtableheading = getTableHeading();
            $("#subtableheading").html(subtableheading);
            //alert($("#cmbOffice").val());
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                funName : "PromiseReport",
                fyear :'<%=Fyear%>',
                ptype: '<%=PType%>',
                deptid: '<%=DeptId%>',
                officeid:'<%=Officeid%>',
                PEId : '<%=PEID%>',
                pageNo:'<%=PageNo%>',
                size:'<%=Size%>',
                isPDF:'<%=isPDF%>',
                districtId:'<%=districtId%>',
                quater:'<%=quater%>',
                projectId:'<%=projectId%>'
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                //                if($('#noRecordFound').attr('value') == "noRecordFound"){
                //                    $('#pagination').hide();
                //                }else{
                //                    $('#pagination').show();
                //                }
                //                chkdisble($("#pageNo").val());
                //                if($("#totalPages").val() == 1){
                //                    $('#btnNext').attr("disabled", "true");
                //                    $('#btnLast').attr("disabled", "true");
                //                }else{
                //                    $('#btnNext').removeAttr("disabled");
                //                    $('#btnLast').removeAttr("disabled");
                //                }
                //                $("#pageTot").html($("#totalPages").val());
                //                $("#pageNoTot").html($("#pageNo").val());
                //                $('#resultDiv').show();

                //                var counter = $('#cntTenBrief').val();
                //                for(var i=0;i<counter;i++){
                //                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                //                    var temp1 = $('#tenderBrief_'+i).html();
                //                    if(temp.length > 250){
                //                        temp = temp1.substr(0, 250);
                //                        $('#tenderBrief_'+i).html(temp+'...');
                //                    }
                //                    //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                //                }
            });
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
                loadTenderTable();
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        //  alert('herer');
        loadTenderTable();
    </script>
    <script type="text/javascript">
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='- Collapse'){
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Expand';
            }else{
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Collapse';
            }
        }

        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>

