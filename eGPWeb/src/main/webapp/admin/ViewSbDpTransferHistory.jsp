<%-- 
    Document   : ViewSbDpTransferHistory
    Created on : Apr 18, 2011, 11:26:04 AM
    Author     : Ketan Prajapati
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblPartnerAdminTransfer"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="java.util.Collections"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:useBean id="scBankCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean"/>
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    scBankCreationSrBean.setLogUserId(logUserId);
                    scBankPartnerAdminSrBean.setLogUserId(logUserId);
                    byte usrTypeId = 0;
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    String usrId = request.getParameter("userId");
                    boolean bole_fromWhere = false;
                    if (usrId.equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        bole_fromWhere = true;
                    }
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo();
                    }

                    String partnerType = request.getParameter("partnerType");

                    String type = "";
                    String title = "";
                    String title1 = "";
                    String userType = "";
                    String title2 = "Transfer History";

                    String from = request.getParameter("from");

                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner User";
                        title1 = "Development Partner";
                        //title2 = "Development Partner";
                        userType = "dev";
                    } else {
                        type = "ScheduleBank";
                        title = "Schedule Financial Institute User";
                        title1 = "Scheduled Financial Institute";
                        //title2 = "Scheduled Bank Branch";
                        userType = "sb";
                    }
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="userId";
                    int auditId=Integer.parseInt(usrId); 
                    String auditAction="View Transfer History of Development Partner User";
                    String moduleName=EgpModule.Manage_Users.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if (bole_fromWhere) {%>
        <title>View Profile</title>
        <%} else {%>
        <title><%=title%> - <%=title2%> </title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    </head>


    <body>
        <%
                    TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                    List<TblPartnerAdminTransfer> list = transferEmployeeServiceImpl.viewTransferHistory(usrId);
                    boolean flag = false;
                    int j = 1;
        %>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <%if (!bole_fromWhere) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->
                                    <td>
                                        <div class="pageHead_1">
                                            <%if (bole_fromWhere) {%>
                                            View Profile
                                            <%} else {%>
                                            <%=title%> - <%=title2%>
                                            <%}%>
                                        <span class="c-alignment-right"><a href="ViewSbDevPartUser.jsp?userId=<%=usrId%>&partnerType=<%=type%>&userTypeId=<%=usrTypeId%>" class="action-button-goback">Go Back</a></span>
                                        </div>
                                        <div class="t_space">
                                        </div>
                                        <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                            <%if (!"Development".equalsIgnoreCase(type)) {%>

                                            <tr>
                                                <td class="ff" width="15%">
                                                    Role :
                                                </td>
                                                <td>
                                                    <%/*if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BankChecker")){*/%>
                                                    <!--                                                            <input type="radio" name="userRole" id="bankCheckerRole" value="BankChecker" checked="cheked"/> Bank Checker &nbsp;-->
                                                    <%--<label>Bank Checker</label>--%>
                                                    <%/*}else*/ if (partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BranchChecker")) {%>
                                                    <!--                                                            <input type="radio" name="userRole" id="branchCheckerRole" value="BranchChecker" checked="cheked"/> Branch Checker &nbsp;-->
                                                    <label> Branch Checker</label>
                                                    <%} else {%>
                                                    <!--                                                            <input type="radio" name="userRole" id="branchMakerRole" value="maker" checked="cheked"/> Branch Maker-->
                                                    <label> Branch Maker</label>

                                                    <%}%>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <%
                                                        scBankCreationSrBean.getBankForAdmin((partnerAdminMasterDtBean.getSbankDevelopId()));
                                                        if (scBankCreationSrBean.getsBankDevelopId() != 0) {
                                            %>
                                            <tr>
                                                <td width="15%" class="ff"><%=title1%> : </td>
                                                <td width="85%">
                                                    <label id="bankName"><%=scBankCreationSrBean.getSbDevelopName()%></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%" class="ff"><%=title1%> Branch: </td>
                                                <td width="85%">
                                                    <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                </td>
                                            </tr>
                                            <%} else {%>
                                            <tr>
                                                <td width="15%" class="ff"><%=title1%> : </td>
                                                <td width="85%">
                                                    <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td class="ff">e-mail ID : </td>
                                                <td>
                                                    <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                </td>
                                            </tr>
                                            
                                            <%if (!"Development".equalsIgnoreCase(type)) {
                                            %>
                                            <tr>
                                                <td class="ff">National ID : </td>
                                                <td>
                                                    <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td class="ff">Designation :</td>
                                                <td>
                                                    <label id="txtDesignation">${partnerAdminMasterDtBean.designation}</label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="tableList_1 t_space" cellspacing="0" width="100%">
                                            <tr>
                                                <th class="t-align-center" style="width:4%">Sl. No.</th>
                                                <th class="t-align-center">Full Name</th>
                                                <th class="t-align-center">Last Working Date</th>
                                                <th class="t-align-center">Replacement Date and Time</th>
                                                <th class="t-align-center">Replaced By</th>
                                                <th class="t-align-center">Comments</th>
                                            </tr>
                                            <%

                                                        for (int i = 0; i < list.size(); i++) {
                                                            String strFullName = "-";
                                                            String strReplacedBy = "-";
                                                            String strComments = "-";

                                                            if (!"".equalsIgnoreCase(list.get(i).getReplacedBy())) {
                                                                strReplacedBy = list.get(i).getReplacedBy();
                                                                flag = true;

                                                                if (!"".equalsIgnoreCase(list.get(i).getFullName())) {
                                                                    strFullName = list.get(i).getFullName();
                                                                }

                                                                if (!"".equalsIgnoreCase(list.get(i).getComments())) {
                                                                    strComments = list.get(i).getComments();
                                                                }
                                            %>
                                            <tr
                                                <%if (j % 2 == 0) {%>
                                                class="bgColor-Green"
                                                <%} else {%>
                                                class="bgColor-white"
                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    <%j++;%>
                                                </td>
                                                <td class="t-align-center"><%=strFullName%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getTranseredDt())%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getTranseredDt())%></td>
                                                <td class="t-align-center"><%=strReplacedBy%></td>
                                                <td class="t-align-center"><%=strComments%></td>
                                            </tr>
                                            <%}
                                                        }
                                                        if (!flag) {
                                            %>
                                            <td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td>
                                            <%}%>


                                        </table>
                                        <!--Page Content End-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%if (bole_fromWhere) {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMyAcc");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%} else {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%}%>
</html>
