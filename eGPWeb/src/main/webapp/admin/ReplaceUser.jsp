<%--
    Document   : SchedBankCreateuser
    Created on : Oct 23, 2010, 7:04:46 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String title1 = "";
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
                title1 = "Development Partner";
            } else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
                title1 = "Financial Institution";
            }
            byte usrTypeId = 0;
            if (!request.getParameter("userTypeId").equals("")) {
                usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
            }
            String usertypeid = request.getParameter("userTypeId");
            String usrId = request.getParameter("userId");
            boolean bole_fromWhere = false;
            if (usrId.equalsIgnoreCase(session.getAttribute("userId").toString())) {
                bole_fromWhere = true;
            }
            if (usrId != null && !usrId.equals("")) {
                partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                partnerAdminMasterDtBean.populateInfo();
            }
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - <%=title1%> User Role Creation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(function() {
                $('#Retrive').click(function() {


                    var email = document.getElementById("txtMail").value;
                    $('span.#mailMsg').hide();
                    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if(reg.test(email) == false) {
                        $('span.#mailMsg').html("Please enter valid Email Id");
                        $('span.#mailMsg').css("color","red");
                        $('span.#mailMsg').show();
                    }else{
                        if($.trim($('#txtMail').val()) == $('#hdEmailId').val()){
                            jAlert("New e-mail ID should be different from Existing User e-mail ID.","<%=type%> user Replace", function(RetVal) {});
                        }else{
                            $.post("<%=request.getContextPath()%>/UserTransferServlet", {emailId:$('#txtMail').val(),userTypeId:'<%=usertypeid%>',action:"retrive"},  function(j){
                                if(j.toString() != ''){
                                    $('#newUserInfo').html(j.toString());
                                }else{
                                    $('#txtFullName').val('');
                                    $('#txtMob').val('');
                                    $('#txtNtnlId').val('');
                                    $('#txtMail').val('');
                                    $('span.#mailMsg').html("No user Registered with this Email Id");
                                    $('span.#mailMsg').css("color","red");
                                    $('span.#mailMsg').show();
                                }
                            });
                        }
                    }
                });


               
            });
        </script>
        <script type="text/javascript">
           
            function actionSubmit(){
                var bVal = true;
              
                var spaceTest = "";
                spaceTest = /^\s+|\s+$/g;
                var spaceTest2 =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                var spaceTest1 =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                var fulname = document.getElementById("txtFullName").value;
                var comments = document.getElementById("comments").value;
                if($.trim($('#txtMail').val()) == $('#hdEmailId').val()){
                    jAlert("New e-mail ID should be different from Existing User e-mail ID.","<%=type%> user Replace", function(RetVal) {});
                    bVal = false;
                }
//                if(fulname.length == 0 && comments.length == 0){
//                    $('#spnFullName').html('Please enter Full Name');
//                    $('#spnFullName').show();
//                    $('#passMsg').html('Please enter Comments');
//                    $('#passMsg').show();
//                    bVal = false;
//                }
                if(fulname.length == 0)
                {
                    $('#spnFullName').html('Please enter Full Name');
                    $('#spnFullName').show();
                    bVal = false;
                }
                if(comments.length == 0){
                    
                    $('#passMsg').html('Please enter Comments');
                    $('#passMsg').show();
                    bVal = false;
                }
                if($('#comments').val().length>2000){
                    $('#passMsg').html('Allows maximum 2000 characters only');
                    $('#passMsg').show();
                    bVal = false;
                }
               
                if(spaceTest.test($('#comments').val())){
                    $('#passMsg').html('Only Space is not allowed');
                    $('#passMsg').show();
                    bVal = false;

                }
                if($("#txtNtnlId").val() != "") {
                    if(!spaceTest1.test($("#txtNtnlId").val())){
                        $('#NtnlIdMsg').css("color","red");
                        $('#NtnlIdMsg').html('Please enter digits only (o to 9)');
                        $('#NtnlIdMsg').show();
                        bVal = false;
                    }else{
                        if($("#txtNtnlId").val().length<11){
                            $('#NtnlIdMsg').css("color","red");
                            $('#NtnlIdMsg').html('CID No Should comprise of minimum 11 digits');
                            $('#NtnlIdMsg').show();
                            bVal = false;
                        }else if($("#txtNtnlId").val().length>11){
                            $('#NtnlIdMsg').css("color","red");
                            $('#NtnlIdMsg').html('CID No Should comprise of maximum 11 digits');
                            $('#NtnlIdMsg').show();
                            bVal = false;
                        }else{
                            $('#NtnlIdMsg').html('');
                        }
                    }
                }
                if($("#txtNtnlId").val() == "") {
                    $('#NtnlIdMsg').css("color","red");
                    $('#NtnlIdMsg').html('Please Enter CID No.');
                    $('#NtnlIdMsg').show();
                    bVal = false;
                }
                if($('#txtMob').val()!=""){
                    if(!spaceTest2.test($('#txtMob').val())){
                        $('#MobileNoMsg').html('Please enter digits (0-9) only');
                        $('#MobileNoMsg').show();
                       bVal = false;
                    }
                    else{
                        $('#MobileNoMsg').html('');
                        $('#MobileNoMsg').hide();
                       

                        if($('#txtMob').val().length<8){
                            $('#MobileNoMsg').html('Minimum 8 digits are required');
                            $('#MobileNoMsg').show();
                            bVal = false;
                           
                        }else if($('#txtMob').val().length > 8){
                            $('#MobileNoMsg').html('Allows maximum 8 digits only');
                            $('#MobileNoMsg').show();
                            bVal = false;
                           
                        }else{
//                            $('#MobileNoMsg').html('');
//                            $('#MobileNoMsg').hide();
//                            $('#NtnlIdMsg').hide();
//                            $('#passMsg').hide();
//                            $('#spnFullName').hide();

                            /*jConfirm('Are you sure you want to replace this user?', '<%=type%> user', function (ans) {
                                if (ans)
                                    document.frmSchedBankUser.submit();

                            });*/
                        }
                    }
                }
                if($('#txtMob').val()==""){
                    $('#MobileNoMsg').html('Please Enter Mobile No.');
                    $('#MobileNoMsg').show();
                    bVal = false;
                }
                else {
//                    $('#NtnlIdMsg').hide();
//                    $('#passMsg').hide();
//                    $('#spnFullName').hide();

                    /*jConfirm('Are you sure you want to replace this user?', '<%=type%> user', function (ans) {
                        if (ans)
                            document.frmSchedBankUser.submit();
                
                    });*/
                    
                }
                if(bVal){
                    jConfirm('Are you sure you want to replace this user?', '<%=type%> user', function (ans) {
                        if (ans)
                            document.frmSchedBankUser.submit();

                    });
                }else{
                    return false;
                }
            }
        </script>


    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>

                        <td class="contentArea_1">
                            <div class="pageHead_1"> Transfer User : </div>
                            <div class="t_space"></div>
                            <div class="tableHead_1">Existing <%=title1%> User :</div>
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=title%> User Creation Failed.</div>
                            <%} else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=title%> User Created Successfully</div>
                            <%}%>
                            <%
                                        int strM = -1;
                                        String msgValidate = "";
                                        if (request.getParameter("strM") != null) {
                                            try {
                                                strM = Integer.parseInt(request.getParameter("strM"));
                                            } catch (Exception ex) {
                                                strM = 4;
                                            }
                                            switch (strM) {
                                                case (1):
                                                    msg = "Please Enter Valid e-mail ID";
                                                    break;
                                                case (2):
                                                    msg = "e-mail ID already exists, Please enter another e-mail ID";
                                                    break;
                                                case (3):
                                                    msg = "CID already exists";
                                                    break;
                                                case (4):
                                                    msg = "Pelase Enter Valid MobileNo";
                                                    break;
                                                case (5):
                                                    msg = "MobileNo already exists";
                                                    break;
                                            }
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=msg%></div>
                            <%
                                            msgValidate = null;
                                        }
                            %>
                            <!-- Result Display End-->

                            <%
                                        scBankCreationSrBean.getBankForAdmin((partnerAdminMasterDtBean.getSbankDevelopId()));
                                        String logUserId = "0";
                                        if (session.getAttribute("userId") != null) {
                                            logUserId = session.getAttribute("userId").toString();
                                        }
                                        scBankCreationSrBean.setLogUserId(logUserId);
                            %>

                            <form method="POST" name ="frmSchedBankUser" id="frmSchedBankUser" action="<%=request.getContextPath()%>/UserTransferServlet">
                                <input type="hidden" name="desig" id="" value="${partnerAdminMasterDtBean.designation}" />
                                <input type="hidden" name="isadmin" id="" value="${partnerAdminMasterDtBean.isAdmin}" />
                                <input type="hidden" name="makerchecker" id="" value="${partnerAdminMasterDtBean.isMakerChecker}" />
                                <input type="hidden" name="partnetid" id="" value="${partnerAdminMasterDtBean.partnerId}" />
                                <input type="hidden" name="sbdevid" id="" value="${partnerAdminMasterDtBean.sbankDevelopId}" />
                                <input type="hidden" name="userid" id="" value="<%=usertypeid%>" />
                                <input type="hidden" name="fulname" id="" value="${partnerAdminMasterDtBean.fullName}" />
                                <input type="hidden" name="emailid" id="hdEmailId" value="${partnerAdminMasterDtBean.emailId}" />
                                <input type="hidden" name="funName" id="" value="changeuser" />

                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td><b>e-mail ID : </b> ${partnerAdminMasterDtBean.emailId}</td>
                                        <td><b>Full Name : </b>${partnerAdminMasterDtBean.fullName}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><b>CID No. : </b> ${partnerAdminMasterDtBean.nationalId} </td>
                                        <td><b>Mobile No. : </b><%if (!partnerAdminMasterDtBean.getMobileNo().equalsIgnoreCase("")) {%>${partnerAdminMasterDtBean.mobileNo}<%}%></td>
                                    </tr>

                                    <tr>
                                        <td width="30%"><b><%=title1%> : </b><%=scBankCreationSrBean.getSbDevelopName()%></td>
                                        <td width="30%"><b>Designation : </b>${partnerAdminMasterDtBean.designation}</td>
                                    </tr>
                                    <tr>
                                        <td  width="62%"><b><%=title1%>/Regional/Country Office : </b>${partnerAdminMasterDtBean.sbDevelopName} </td>
                                    </tr>
                                    <tr>

                                    </tr>

                                </table>
                                <div class="tableHead_1">New <%=title1%> User :</div>
                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff" width="30%">e-mail ID : </td>

                                        <td width="70%"><input name="mailId" id="txtMail" type="text" class="formTxtBox_1" onfocus=" $('span.#mailMsg').hide();" onchange="" style="width:200px;" />

                                            <label class="formBtn_1">
                                                <input type="button" name="Retrive" value="Retrieve" id="Retrive" /></label>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td><td><span id="mailMsg" class="reqF_1" style="display: none;"></span></td></tr>

                                    </tr>
                                    <tbody id="newUserInfo">
                                        <tr>
                                            <td class="ff">Full Name : <span>*</span></td>
                                            <td><input name="fullName" id="txtFullName" type="text" onfocus="$('span.#spnFullName').hide();" class="formTxtBox_1" style="width:200px;" />
                                                <span class="reqF_1" id="spnFullName" style="display: none;"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">CID No. : <span>*</span></td>
                                            <td><input name="ntnlId" id="txtNtnlId" type="text" class="formTxtBox_1" onfocus="$('span.#NtnlIdMsg').hide();" style="width:200px;" maxlength="26" />
                                                <span id="NtnlIdMsg" class="reqF_1" style="display: none;">&nbsp;</span></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Mobile No : <span>*</span></td>
                                            <td><input name="mobileNo" id="txtMob" type="text" class="formTxtBox_1" style="width:200px;"  onfocus="$('span.#MobileNoMsg').hide();" maxlength="16" /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 01234567)</span>
                                                <span id="MobileNoMsg" class="reqF_1" style="display: none;"></span></td>
                                        </tr>
                                    </tbody>
                                    <tr>
                                        <td class="ff" width="30%">Comments : <span>*</span></td>
                                        <td width="70%"> <textarea cols=""  rows="5" id="comments" name="comments" class="formTxtBox_1" onfocus="$('span.#passMsg').hide();" style="width:50%;"></textarea>
                                            <span class="reqF_1" id="passMsg" style="display: none;"></span>
                                        </td>

                                    </tr>
                                    <%--  <tr>
                                          <td class="ff">fax No : </td>
                                          <td><input name="faxNo" type="text" class="formTxtBox_1" id="txtFax" style="width:200px;" /></td>
                                      </tr>--%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="button" name="btnSubmit" id="btnSubmit" value="Replace" onclick="return actionSubmit();"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Dashboard Content Part End-->
                </table>
                <script>
                    var obj = document.getElementById('lblGovUsersCreation');
                    if(obj != null){
                        if(obj.innerHTML == 'Create Government Users'){
                            obj.setAttribute('class', 'selected');
                        }
                    }

                </script>
                <script>
                    var headSel_Obj = document.getElementById("headTabMngUser");
                    if(headSel_Obj != null){
                        headSel_Obj.setAttribute("class", "selected");
                    }
                </script>
                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>


    </body>
</html>
