<%--
    Document   : ConfigureCalendar
    Created on : Oct 30, 2010, 7:55:54 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHolidayMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--jsp:useBean id="configureCalendarSrBean" class="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean" /-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <%@page buffer="15kb"%>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Holiday Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />


        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr valign="top">
                        <%
                                    List holiday = new ArrayList();
                                    int i = 1;
                                    boolean check = false;
                                    String msg = "not";
                                    ConfigureCalendarSrBean ccs = new ConfigureCalendarSrBean();
                                    ccs.setLogUserId(session.getAttribute("userId").toString());
                                    if ("Submit".equals(request.getParameter("button"))) {
                                        if (request.getParameter("textfield_" + i) != null) {
                                            while (request.getParameter("textfield_" + i) != null) {

                                                TblHolidayMaster holidayMaster = new TblHolidayMaster();
                                                holiday.add(request.getParameter("textfield_" + i));
                                                String dat = request.getParameter("textfield_" + i);
                                                if (!"".equals(dat)) {
                                                    String year = dat.substring(dat.length() - 4, dat.length());
                                                    holidayMaster.setYear(year);
                                                    holidayMaster.setIsWeekend("");
                                                    holidayMaster.setDescription(request.getParameter("textdescription_" + i));
                                                    holidayMaster.setHolidayDate(DateUtils.formatStdString(dat));

                                                    ccs.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                    ccs.addHolidayMasterData(holidayMaster);
                                                }
                                                i++;
                                            }
                                            msg = "Holidays added successfully";
                                            response.sendRedirect("HolidayConfigurationView.jsp?msg=" + msg);
                                        } else {
                                            check = true;
                                        }

                                    }
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }


                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <form name="frmConfigureCalendar" id="frmConfigureCalendar" action="ConfigureCalendar.jsp" method="post" >
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr valign="top">
                                        <td class="contentArea_1">
                                            <div class="pageHead_1">Configure Holidays</div>
                                            <div>&nbsp;</div>
                                            <div><span id="remmsg" ></span></div><br/>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <tr>
                                                    <td class="ff">Holidays : </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="action-button-add">Add Holiday</a>
                                                        <a href="javascript:void(0);" class="action-button-delete">Remove Holiday</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id ="tableListDetail">

                                                            <tr id="tr_0">
                                                                <th>Select</th>
                                                                <th>Date</th>
                                                                <th>Description</th>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><label class="formBtn_1">
                                                            <input type="submit" name="button" id="button" value="Submit" onclick="return check();"/>
                                                        </label>
                                                    </td>
                                                </tr>

                                            </table>
                                            <div>&nbsp;</div>
                                        </td>
                                    </tr>

                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>

                </table>
                <%
                            if (check) {
                %>
                <script language="JavaScript" type="text/javascript">
                    jAlert("Add at least 1 holiday"," Alert ", "Alert");
                </script>
                <%                                       }%>

                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var obj = document.getElementById('lblHolidyConfig');
            if(obj != null){
                if(obj.innerHTML == 'Configure'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabConfig");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <script>

            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            $(document).ready(function(){
                $(".action-button-add").each(function(){

                    $(this).bind("click", function(){
                        $('#remmsg').removeClass('responseMsg successMsg');
                        $('#remmsg').html(' ');
                        var last_id = $("#tableListDetail tr:last-child").attr("id");
                        var counter = last_id.split("_")[1] * 1;
                        var txtf = "'textfield_"+(counter+1)+"','textfield_" + (counter+1) + "'";
                        var imgf= "'textfield_"+(counter+1)+"','imgfield_" + (counter+1) + "'";
                        var html = "<tr id='tr_" + (counter+1) + "'><td class='t-align-center'>"+
                            "<input type='checkbox' name='chkHoliday' value='checkbox' id='chkHoliday_"+(counter+1)+"' /></td>" +
                            "<td class='t-align-center'><input name='textfield_" + (counter+1) + "' " +
                            "type='text' class='formTxtBox_1' id='textfield_" + (counter+1) + "' " +
                            "style='width:100px;' readonly='readonly' value='' onblur='return DateBlank(this);'/> " +
                            '<a href="javascript:void(0);"  title="Calender" onclick="GetCal('+txtf+');" >' +
                            "<img src='../resources/images/Dashboard/calendarIcn.png' alt='add Date' id='imgfield_" + (counter+1) + "' " +
                            'style="vertical-align:middle;" border="0" onclick="GetCal('+imgf+');" /></a>'+
                            "<span id='spDate_"+ (counter+1)+ "'style='color: red;'>&nbsp;</span></td>"+
                            "<td class='t-align-center'><input name='textdescription_" + (counter+1) + "' " +
                            "type='text' class='formTxtBox_1' id='textdescription_" + (counter+1) + "' " +
                            "style='width:300px;' value=''/><span id='spDescription_"+ (counter+1)+ "'style='color: red;'>&nbsp;</span></td>"+
                            "<input type='hidden' name='totrow' value='0' id='totrow'/></tr>";
                        $("#tableListDetail").append(html);
                        document.getElementById("totrow").value=(counter+1);
                    });
                });

                $(".action-button-delete").each(function(){
                    $('#remmsg').show();
                    $(this).bind("click", function(){
                        var removeStart = 0;
                        var c = 0;
                        var chkrem = false;
                        $("input[name='chkHoliday']").each(function(){
                            if($(this).attr("checked")){
                                if(c==0){
                                    removeStart = ($(this).attr("id").split("_")[1]*1);
                                    //alert("removeStar==>" + removeStart);
                                    c=1;
                                }
                                var counter = $(this).attr("id").split("_")[1];
                                $("#tr_"+counter).remove();
                                $('#remmsg').addClass('responseMsg successMsg');
                                $('#remmsg').html('Holiday(s) removed successfully');

                                chkrem = true;
                            }
                        });
                        if(!chkrem){
                            alert("Please select date to be removed");
                        }else{
                            var last_id = ($("#tableListDetail tr:last-child").attr("id").split("_")[1]*1);
                            counter = (removeStart*1);
                            for(var i=removeStart+1;i<=last_id;i++){
                                //alert("in for loop...i:" + i);
                                if($("#tr_"+i).attr("id") != undefined){
                                    $("#tr_"+i).attr("id","tr_"+counter);
                                    $("#chkHoliday_"+i).attr("id","chkHoliday_"+counter);
                                    $("#textfield_"+i).attr("name","textfield_"+counter);
                                    $("#textfield_"+i).attr("id","textfield_"+counter);
                                    counter++;
                                }
                            }
                        }
                    });
                });
            });

            function check()
            {
                var flag=true;
                if(document.getElementById("totrow")!=null)
                {
                    var counter=eval(document.getElementById("totrow").value) + 1;
                    //alert(counter);
                    for(var k=1;k<=counter;k++)
                    {
                        if(document.getElementById("textfield_"+k)!= null)
                        {
                            if(document.getElementById("textfield_"+k).value=="")
                            {
                                document.getElementById("spDate_"+k).innerHTML="<br/>Please Select Date";
                                flag=false;
                            }
                        }
                        if(document.getElementById("textdescription_"+k)!= null)
                        {
                            if(trim(document.getElementById("textdescription_"+k).value)=="")
                            {
                                document.getElementById("spDescription_"+k).innerHTML="<br/>Please Enter Description";
                                flag=false;
                            }
                            else if ((document.getElementById("textdescription_"+k).value).length > 150){
                                document.getElementById("spDescription_"+k).innerHTML="<br/>Only 150 characters are allowed";
                                flag=false;
                            }
                        }
                    }
                    if (flag==false){
                        return false;
                    }else{
                        // Start OF--Checking for Unique Rows
                        var chk=true;
                        var i=0;
                        var j=0;
                        for(i=1; i<=counter; i++) //Loop For Newly Added Rows
                        {
                            if($.trim(document.getElementById("textfield_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                            {
                                for(j=1; j<=counter && j!=i; j++) // Loop for Total Count but Not same as (i) value
                                {
                                    chk=true;
                                    //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                                    //IF Row is same Give alert message.
                                    if($.trim(document.getElementById("textfield_"+i).value) != $.trim(document.getElementById("textfield_"+j).value))
                                    {
                                        chk=false;
                                    }
                                    if(chk==true) //If Row is same then give alert message
                                    {
                                        alert("Duplicates Found.");
                                        return false;
                                    }
                                }

                            }
                        }
                    }
                    // End OF--Checking for Unique Rows


                }
            }
            function DateBlank(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;
                if(obj.value!=null)
                {
                    if(obj.value != '')
                    {
                        document.getElementById("spDate_"+i).innerHTML="";
                        return true;
                    }
                }
            }

        </script>
    </body>
</html>
