<%-- 
    Document   : ViewConfigPaThreshold
    Created on : Apr 20, 2017, 4:00:15 PM
    Author     : feroz
--%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPaThreshold"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ConfigPaThresholdService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Threshold Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    ConfigPaThresholdService configPaThresholdService = (ConfigPaThresholdService) AppContext.getSpringBean("ConfigPaThresholdService");
                    List<TblConfigPaThreshold> paThresholds = configPaThresholdService.getAllTblConfigPaThreshold();
         %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Threshold Configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2');">Save as PDF</a></span>
                        </div>
                        <%
                            if(paThresholds.isEmpty())
                            {
                                %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                            }
                            else
                            {
                        %>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="">
                            <tbody id="tbodyData">&nbsp;
                                <%
                                String message = "";
                                if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                    message = request.getParameter("msg");
                                    if ("editsucc".equalsIgnoreCase(message)) {
                                %>
                            <div class="responseMsg successMsg t_space"><span>Threshold updated successfully</span></div><br/>
                            <%}
                            else if ("failed".equalsIgnoreCase(message)){
                            %>
                                <div class="responseMsg errorMsg t_space"><span>Error occurred while updating threshold!</span></div><br/>
                            <%                                    }
                            }
                            %>
                            <tr>
                                <th class="t-align-center" style="width: 4%">Sl. No.</th>
                                <th class="t-align-center">Area</th>
                                <th class="t-align-center">Procurement Category</th>
                                <th class="t-align-center">Max Value (In Nu.)</th>
                                <th class="t-align-center">Min Value (In Nu.)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                              int cnt = 0;
                              for(TblConfigPaThreshold paThreshold : paThresholds){
                                  cnt++;
                                  if(cnt%2==0){%>
                                        <tr>
                                <%}else{%>
                                        <tr style='background-color:#E4FAD0;'>
                                <%}%>
                                        <td class="t-align-center">
                                            <%out.print(cnt);%>
                                        </td>
                                        <td class="t-align-center">
                                            <%
                                             if(paThreshold.getArea().contains("SubDistrict"))
                                             {
                                                 out.print("Dungkhag");
                                             }
                                             else if(paThreshold.getArea().contains("District"))
                                             {
                                                 out.print("Dzongkhag");
                                             }
                                             else if(paThreshold.getArea().contains("Division"))
                                             {
                                                 out.print("Department");
                                             }
                                             else if(paThreshold.getArea().contains("Organization"))
                                             {
                                                 out.print("Division");
                                             }
                                             else if(paThreshold.getArea().contains("Autonomus"))
                                             {
                                                 out.print("Autonomous");
                                             }
                                             else
                                             {
                                                 out.print(paThreshold.getArea());
                                             }
                                            %>
                                        </td>
                                        <td class="t-align-center">
                                            <%
                                             if(paThreshold.getProcurementNatureId()==1)
                                             {
                                                 out.print("Goods");
                                             }
                                             else if(paThreshold.getProcurementNatureId()==2)
                                             {
                                                 out.print("Works");
                                             }
                                             else
                                             {
                                                 out.print("Services");
                                             }
                                            %>
                                        </td>
                                        <td class="t-align-center">
                                            <%out.print(paThreshold.getMaxValue());%>
                                        </td>
                                        <td class="t-align-center">
                                            <%out.print(paThreshold.getMinValue());%>
                                        </td>
                                        <td class="t-align-center">
                                            <a href="EditConfigPaThreshold.jsp?ConfigPaThresholdId=<%=paThreshold.getConfigPathresholdId()%>">Edit</a>
                                        </td>
                                        </tr>
                            <%
                                } %>
                            </tbody>
                        </table>
                                <%}%>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="ThresholdConfiguration_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        var obj = document.getElementById('lblConfigPaThreshold');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
