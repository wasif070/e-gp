<%-- 
    Document   : PreparePCC
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare PCC</div>
                            </div>

                            <form action="UploadSTDDoc.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td colspan="2">Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses</td>
                                    </tr>
                                    <tr>
                                        <td><b>PCC Clause</b></td>
                                        <td>Amendments of, and Supplements to, Clauses in the General Conditions of Contract</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            1.1 GCC In the Conditions of Contract, which include Particular Conditions and these General Conditions, the following words and expressions shall have the meaning hereby assigned to them. Boldface type is used to identify the defined terms:
                                            <br></br>
                                            <br></br>(a)	Act means The Public Procurement Act, 2006 (Act 24 of 2006).
                                            <br></br>(b)	Approving Authority means the authority which, in accordance with the Delegation of Financial Powers, approves the award of contract
                                            <br></br> (c)	Bill of Quantities (BOQ) means the priced and completed Bill of   Quantities forming part of the Contract defined in GCC Clause 21
                                            <br></br>(d)	Contractor means the Person under contract with the Procuring Entity for the execution of Works under the Rules and the Act as stated in the PCC
                                            <br></br>(e)	Completion Date is the actual date of completion of the Works and Physical services certified by the Project Manager, in accordance with GCC Clause 31 & 32
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                           <textarea cols="80" id="editor1" name="editor1" rows="10">&lt;html&gt;&lt;head&gt;&lt;title&gt;CKEditor Sample&lt;/title&gt;&lt;/head&gt;&lt;body&gt;&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'editor1',
                                                {
                                                    fullPage : true
                                                });

                                                //]]>
                                            </script>
                                        </td>
                                    </tr>
                                </table>
                                <div class="t_space" align="center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="button" value="Next" /></label>
                                </div>
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>