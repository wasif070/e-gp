<%-- 
    Document   : ViewMandatoryDocs
    Created on : Apr 25, 2011, 6:45:50 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Required Documents for a form</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    </head>
    <body>
        <%
                    TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
                    List<Object[]> mandDocList = templateForm.getSTDMandDocs(request.getParameter("tId"), request.getParameter("sId"), request.getParameter("fId"));
        %>
        <div class="contentArea_1">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <div class="pageHead_1">View Required Documents for a form<span style="float: right;"><a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("tId")%>">Go Back to Dashboard</a></span></div>
            <table class="tableList_1" cellspacing="0" width="100%" id="members">
                <tbody id="tbodyData">
                    <tr>
                        <th class="t-align-center" width="4%">Sl. <br/> No.</th>
                        <th class="t-align-center" width="66%">Name of Document</th>
                        <th class="t-align-center" width="10%">Action</th>
                    </tr>
                    <%
                                int man_cnt = 1;
                                for (Object[] data : mandDocList) {
                    %>
                    <tr>
                        <td class="t-align-center"><%=man_cnt%></td>
                        <td><%=data[1]%></td>
                        <td class="t-align-center">
                            <a href="EditMandatoryDocs.jsp?mId=<%=data[0]%>&pg=2&tId=<%=request.getParameter("tId")%>&fId=<%=request.getParameter("fId")%>&sId=<%=request.getParameter("sId")%>">Edit</a>&nbsp;|&nbsp;
                            <a href="<%=request.getContextPath()%>/CreateSTDForm?action=delSTDMandDocs&mId=<%=data[0]%>">Remove</a>
                        </td>
                    </tr>
                    <%man_cnt++;
                                }%>
                </tbody>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>