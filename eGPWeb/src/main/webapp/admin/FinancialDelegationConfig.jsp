<%-- 
    Document   : FinancialDelegationConfig
    Created on : Jun 14, 2015, 11:02:51 AM
    Author     : Istiak (Dohatec)
--%>

<jsp:useBean id="oFinancialDelegationSrBean" class="com.cptu.egp.eps.web.servicebean.FinancialDelegationSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "No-store, No-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "No-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Financial Delegation Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>


        <%
            boolean isEdit = false;
            String cType = "multiple";

            String budgetType = oFinancialDelegationSrBean.getBudgetType();
            String tenderType = oFinancialDelegationSrBean.getTenderType();
            String procMethod = oFinancialDelegationSrBean.getProcurementMethod();
            String procType = oFinancialDelegationSrBean.getProcurementType();
            String procNature = oFinancialDelegationSrBean.getProcurementNature();
            String isBoD = oFinancialDelegationSrBean.getIsBoD();
            String isCorporation = oFinancialDelegationSrBean.getIsCorporation();
            String OfficeLevel = oFinancialDelegationSrBean.getOfficeLevel();
            String appAuthority = oFinancialDelegationSrBean.getApprovingAuthority();

            if (request.getParameter("isEdit") != null && request.getParameter("isEdit").equalsIgnoreCase("y")) {
                isEdit = true;
                if (request.getParameter("cType") != null && request.getParameter("cType").equalsIgnoreCase("single")) {
                    cType = "single";
                }
            }
        %>

        <%if (isEdit) {%>
        <script type="text/javascript">
            $(document).ready(function () {
                LoadTimeline();
            });

            function LoadTimeline() {
                $.post("<%=request.getContextPath()%>/FinancialDelegationServlet", {action: "ViewTimeline", flag: "configuration", configureType: "<%=cType%>", id: "<%=request.getParameter("id")%>"}, function (j) {
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    DigitToWord();
                });
            }

            function DigitToWord() {
                $('.MinBDT').each(function () {
                    var i = ($(this)[0].id.substr($(this)[0].id.indexOf("_") + 1));
                    document.getElementById("MinBDTword_" + i).innerHTML = WORD(document.getElementById("MinBDT_" + i).value);
                });
                $('.MaxBDT').each(function () {
                    var i = ($(this)[0].id.substr($(this)[0].id.indexOf("_") + 1));
                    document.getElementById("MaxBDTword_" + i).innerHTML = WORD(document.getElementById("MaxBDT_" + i).value);
                });
                $('.MinProjectBDT').each(function () {
                    var i = ($(this)[0].id.substr($(this)[0].id.indexOf("_") + 1));
                    document.getElementById("MinProjectBDTword_" + i).innerHTML = WORD(document.getElementById("MinProjectBDT_" + i).value);
                });
                $('.MaxProjectBDT').each(function () {
                    var i = ($(this)[0].id.substr($(this)[0].id.indexOf("_") + 1));
                    document.getElementById("MaxProjectBDTword_" + i).innerHTML = WORD(document.getElementById("MaxProjectBDT_" + i).value);
                });
            }
            function MinValueWord(obj) {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                document.getElementById("MinBDTword_" + i).innerHTML = WORD(document.getElementById("MinBDT_" + i).value);
            }
            function MaxValueWord(obj) {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                document.getElementById("MaxBDTword_" + i).innerHTML = WORD(document.getElementById("MaxBDT_" + i).value);
            }
            function ProjectMinValueWord(obj) {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                document.getElementById("MinProjectBDTword_" + i).innerHTML = WORD(document.getElementById("MinProjectBDT_" + i).value);
            }
            function ProjectMaxValueWord(obj) {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                document.getElementById("MaxProjectBDTword_" + i).innerHTML = WORD(document.getElementById("MaxProjectBDT_" + i).value);
            }
            function DeleteRule() {

                var checkedCount = $('input[type="checkbox"][name="RulesNo"]:checked').length;

                if (checkedCount > 0) {
                    var id = "";
                    $('input[type="checkbox"][name="RulesNo"]:checked').each(function () {
                        if ($(this).val() != 'n')
                            id = $(this).val() + "," + id;
                    });
                    id = id.substr(0, (id.length - 1));

                    if (confirm("Are You Sure You want to Delete?")) {
                        if (id.length > 0) {
                            $.post("<%=request.getContextPath()%>/FinancialDelegationServlet", {action: "DeleteRows", rowNo: id, flag: "configuration"}, function (j) {
                                if (j == "false") {
                                    alert("Rule delete failed!!");
                                } else {
                                    $('input[type="checkbox"][name="RulesNo"]:checked').each(function () {
                                        $(this).closest('tr').remove();
                                    });
                                    alert("Rule delete successfully.");
                                }
                            });
                        } else if (id.length == 0 && checkedCount > 0) {
                            $('input[type="checkbox"][name="RulesNo"]:checked').each(function () {
                                $(this).closest('tr').remove();
                            });

                            alert("Rule delete successfully.");
                        }
                    }
                } else {
                    alert("Please Select checkbox first!!!");
                }
            }

            function NewOrEdit1(id) {
                var edit = false;

                if ($('#BudgetType_' + id).val() != $('#BTOld_' + id).val())
                    edit = true;
                else if ($('#TenderType_' + id).val() != $('#TTOld_' + id).val())
                    edit = true;
                else if ($('#ProcurementMethod_' + id).val() != $('#PMOld_' + id).val())
                    edit = true;
                else if ($('#ProcurementNature_' + id).val() != $('#PNOld_' + id).val())
                    edit = true;
                else if ($('#TenderEmergency_' + id).val() != $('#TEOld_' + id).val())
                    edit = true;
                else if ($('#MinBDT_' + id).val() != $('#MinOld_' + id).val())
                    edit = true;
                else if ($('#MaxBDT_' + id).val() != $('#MaxOld_' + id).val())
                    edit = true;
                else if ($('#ApprovingAuthority_' + id).val() != $('#AAOld_' + id).val())
                    edit = true;
                else if ($('#MinProjectBDT_' + id).val() != $('#MinProjectOld_' + id).val())
                    edit = true;
                else if ($('#MaxProjectBDT_' + id).val() != $('#MaxProjectOld_' + id).val())
                    edit = true;
                else if ($('#isBodApplicable_' + id).val() != $('#isBoDOld_' + id).val())
                    edit = true;
                else if ($('#isCorporation_' + id).val() != $('#isCorporationOld_' + id).val())
                    edit = true;
                else if ($('#OfficeLevel_' + id).val() != $('#OfficeLevelOld__' + id).val())
                    edit = true;

                if (edit) {
                    if ($('#NewOldEdit_' + id).val() == "Old")
                        $('#NewOldEdit_' + id).val("Edit");
                }
            }

            function NewOrEdit(id) {
                var edit = false;

                if ($('#BudgetType_' + id).val() != $('#BTOld_' + id).val())
                    edit = true;
                else if ($('#TenderType_' + id).val() != $('#TTOld_' + id).val())
                    edit = true;
                else if ($('#ProcurementMethod_' + id).val() != $('#PMOld_' + id).val())
                    edit = true;
                else if ($('#ProcurementNature_' + id).val() != $('#PNOld_' + id).val())
                    edit = true;
                else if ($('#TenderEmergency_' + id).val() != $('#TEOld_' + id).val())
                    edit = true;
                else if ($('#MinBDT_' + id).val() != $('#MinOld_' + id).val())
                    edit = true;
                else if ($('#MaxBDT_' + id).val() != $('#MaxOld_' + id).val())
                    edit = true;
                else if ($('#ApprovingAuthority_' + id).val() != $('#AAOld_' + id).val())
                    edit = true;
                else if ($('#MinProjectBDT_' + id).val() != $('#MinProjectOld_' + id).val())
                    edit = true;
                else if ($('#MaxProjectBDT_' + id).val() != $('#MaxProjectOld_' + id).val())
                    edit = true;
                else if ($('#isBodApplicable_' + id).val() != $('#isBoDOld_' + id).val())
                    edit = true;
                else if ($('#isCorporation_' + id).val() != $('#isCorporationOld_' + id).val())
                    edit = true;
                else if ($('#OfficeLevel_' + id).val() != $('#OfficeLevelOld__' + id).val())
                    edit = true;

                if (edit) {
                    if ($('#NewOldEdit_' + id).val() == "Old")
                        $('#NewOldEdit_' + id).val("Edit");
                }
            }
        </script>
        <%} else {%>
        <script type="text/javascript">
            $(document).ready(function () {
                AddRule();
            });

            function DeleteRule() {

                var checkedCount = $('input[type="checkbox"][name="RulesNo"]:checked').length;

                if (checkedCount > 0) {
                    if (confirm("Are You Sure You want to Delete?")) {
                        $('input[type="checkbox"][name="RulesNo"]:checked').each(function () {
                            $(this).closest('tr').remove();
                        });
                    }
                } else {
                    alert("Please Select checkbox first!!!");
                }
            }
        </script>
        <%}%>

        <script type="text/javascript">

            var regex = new RegExp("[0-9]+(\.[0-9][0-9]?)?");

            function AddRule() {

                var lastId = Math.abs($('input[type="checkbox"][name="RulesNo"]:last').val());
                var newId = 0;
                if (isNaN(lastId))
                    newId = 1;
                else
                    newId = lastId + 1;

                $('#tbodyData').append("<tr>" +
                        "<td class='t-align-center' width='10%'><input id='chk_" + newId + "' name='RulesNo' type='checkbox' value='n' />" +
                        "<input type='hidden' name='FinancialDeligationId' id='FinancialDeligationId_" + newId + " value='" + newId + "'>" +
                        "<input type='hidden' name='NewOldEdit' id='NewOldEdit_" + newId + "' value='New'></td>" +
                        "<td class='t-align-center' width='10%'>" + BudgetTypeList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + TenderTypeList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + ProcMethodList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + ProcNatureList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + TenderEmergencyList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'><input type='text' class='formTxtBox_1 MinBDT' name='MinBDT' id='MinBDT_" + newId + "' value='' onblur='return MinValueWord(this);' ></td>" +
                        "<td class='t-align-center'><span id='MinBDTword_" + newId + "'>&nbsp;</span></td>" +
                        "<td class='t-align-center' width='10%'><input type='text' class='formTxtBox_1 MaxBDT' name='MaxBDT' id='MaxBDT_" + newId + "' value='' onblur='return MaxValueWord(this);'></td>" +
                        "<td class='t-align-center'><span id='MaxBDTword_" + newId + "'>&nbsp;</span></td>" +
                        "<td class='t-align-center' width='10%'><input type='text' class='formTxtBox_1 MinProjectBDT' name='MinProjectBDT' id='MinProjectBDT_" + newId + "' value='' onblur='return ProjectMinValueWord(this);'></td>" +
                        "<td class='t-align-center'><span id='MinProjectBDTword_" + newId + "'>&nbsp;</span></td>" +
                        "<td class='t-align-center' width='10%'><input type='text' class='formTxtBox_1 MaxProjectBDT' name='MaxProjectBDT' id='MaxProjectBDT_" + newId + "' value='' onblur='return ProjectMaxValueWord(this);'></td>" +
                        "<td class='t-align-center'><span id='MaxProjectBDTword_" + newId + "'>&nbsp;</span></td>" +
                        "<td class='t-align-center' width='10%'>" + isBodList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + isCorporationList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + OfficeLevelList(newId) + "</td>" +
                        "<td class='t-align-center' width='10%'>" + ApprovalAuthorityList(newId) + "</td>" +
                        "</tr>");
            }

            function BudgetTypeList(id) {
                return "<select name='BudgetType' class='formTxtBox_1 BudgetType' id='BudgetType_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#budgetType').val() +
                        "</select>";
            }

            function TenderTypeList(id) {
                return "<select name='TenderType' class='formTxtBox_1 TenderType' id='TenderType_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#tenderType').val() +
                        "</select>";
            }

            function ProcMethodList(id) {
                return "<select name='ProcurementMethod' class='formTxtBox_1 ProcurementMethod' id='ProcurementMethod_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#procMethod').val() +
                        "</select>";
            }


            function ProcNatureList(id) {
                return "<select name='ProcurementNature' class='formTxtBox_1 ProcurementNature' id='ProcurementNature_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#procNature').val() +
                        "</select>";
            }

            function TenderEmergencyList(id) {
                return "<select name='TenderEmergency' class='formTxtBox_1 TenderEmergency' id='TenderEmergency_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        "<option value='Normal'>Normal</option>" +
                        "<option value='National Disaster'>National Disaster</option>" +
                        "<option value='Urgent(Catastrophe)'>Urgent(Catastrophe)</option>" +
                        "</select>";
            }

            function ApprovalAuthorityList(id) {
                return "<select name='ApprovingAuthority' class='formTxtBox_1 ApprovingAuthority' id='ApprovingAuthority_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#appAuthority').val() +
                        "</select>";
            }

            function isBodList(id) {
                return "<select name='isBoD' class='formTxtBox_1 isBoD' id='isBoD_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#isBoD').val() +
                        "</select>";
            }

            function isCorporationList(id) {
                return "<select name='isCorporation' class='formTxtBox_1 isCorporation' id='isCorporation_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#isCorporation').val() +
                        "</select>";
            }

            function OfficeLevelList(id) {
                return "<select name='OfficeLevel' class='formTxtBox_1 OfficeLevel' id='OfficeLevel_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        $('#OfficeLevel').val() +
                        "</select>";
            }

            function Validate() {
                var submit = true;

                if (submit) {
                    $('.BudgetType').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Budget Type');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.TenderType').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Tender Type');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.ProcurementMethod').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Procurement Method');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.ProcurementNature').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Procurement Category');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.TenderEmergency').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Tender Emergency');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.ApprovingAuthority').each(function () {
                        if ($(this).val() == '') {
                            alert('Please Select Approving Authority');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('.MinBDT').each(function () {
                        if ($(this).val() == '') {
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        } else if (!regex.test($(this).val())) {
                            alert('Please enter decimal value only.');
                            submit = false;
                            return false;
                        }
                    });
                }
                if (submit) {
                    $('.MaxBDT').each(function () {
                        if ($(this).val() == '') {
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        } else if (!regex.test($(this).val())) {
                            alert('Please enter decimal value only.');
                            submit = false;
                            return false;
                        }
                    });
                }
                if (submit) {
                    $('.MinProjectBDT').each(function () {
                        if ($(this).val() == '') {
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        } else if (!regex.test($(this).val())) {
                            alert('Please enter decimal value only.');
                            submit = false;
                            return false;
                        }
                    });
                }
                if (submit) {
                    $('.MaxProjectBDT').each(function () {
                        if ($(this).val() == '') {
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        } else if (!regex.test($(this).val())) {
                            alert('Please enter decimal value only.');
                            submit = false;
                            return false;
                        }
                    });
                }

                if (submit) {
                    $('input:hidden[name=NewOldEdit]').each(function () {
                        if ($(this).val() == 'Old') {
                            submit = false;
                        } else if ($(this).val() == 'New' || $(this).val() == 'Edit') {
                            submit = true;
                            return false;
                        }
                    });

                    if (!submit)
                        alert('You did not change or add any rule.');
                }

                return submit;
            }
        </script>

    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                        <td class="contentArea">
                            <div class="pageHead_1">Financial Delegation Configuration</div>
                            <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                            <div align="right" class="t_space b_space">
                            <%if ("multiple".equalsIgnoreCase(cType)) {%>
                            <a href="javascript:void(0);" class="action-button-add" id="addRule" onclick="AddRule()">Add Rule</a>
                            <a href="javascript:void(0);" class="action-button-delete" id="removeRule" onclick="DeleteRule()">Remove Rule</a>
                            <%}%>
                        </div>

                        <br/>
                        <div >&nbsp;
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("y")) {%>
                            <div class='responseMsg successMsg'>Data save successfully</div>
                            <%}
                                if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("n")) {%>
                            <div class='responseMsg errorMsg'>Data save failed</div>
                            <%}%>
                        </div>
                        <br/>

                        <form action="<%=request.getContextPath()%>/FinancialDelegationServlet?action=AddUpdate" method="post">

                            <table class="tableList_1" cellspacing="0" width="100%" id="resultTable">
                                <tbody id="tbodyData">
                                    <tr>
                                        <th class="t-align-center" width="5%">Select</th>
                                        <th class="t-align-center" width="10%">Budget Type (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Tender <br/>Type (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Procurement <br/>Method (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Procurement <br/>Category (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Type of <br/>Emergency (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Minimum Value <br/>(In Nu.) (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Minimum Value<br/>(In Nu. <br/>Words) </th>
                                        <th class="t-align-center" width="10%">Maximum Value <br/>(In Nu.) (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="7%">Maximum Value<br/>(In Nu. <br/>Words)</th>
                                        <th class="t-align-center" width="6%">Minimum Project<br/>Value<br/>(In Nu.)(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="7%">Minimum Project<br/>Value<br/>(In Nu.<br/>Words)</th>
                                        <th class="t-align-center" width="6%">Maximum Project<br/>Value<br/>(In Nu.)(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="7%">Maximum Project<br/>Value<br/>(In Nu.<br/>Words)</th>
                                        <th class="t-align-center" width="5%">Bod<br/>Applicable(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="5%">Corporation<br/>Applicable(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="5%">Office<br/>Level(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="10%">Approving <br/>Authority (<span class="mandatory" style="color: red;">*</span>)</th>
                                    </tr>
                                </tbody>
                            </table><div>&nbsp;</div>

                            <div align="center">
                                <span class="formBtn_1" >
                                    <input name="btnSubmit" align="center" id="btnSubmit" value="Submit" type="submit" onclick="return Validate();">
                                </span>
                            </div>
                            <div>&nbsp;</div>
                        </form>

                        <input type="hidden" id="budgetType" value="<%=budgetType%>">
                        <input type="hidden" id="tenderType" value="<%=tenderType%>">
                        <input type="hidden" id="procMethod" value="<%=procMethod%>">
                        <input type="hidden" id="procNature" value="<%=procNature%>">
                        <input type="hidden" id="isBoD" value="<%=isBoD%>">
                        <input type="hidden" id="isCorporation" value="<%=isCorporation%>">
                        <input type="hidden" id="OfficeLevel" value="<%=OfficeLevel%>">
                        <input type="hidden" id="appAuthority" value="<%=appAuthority%>">

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var obj = document.getElementById('finDelEdit');
            if (obj != null) {
                if (obj.innerHTML == 'Edit') {
                    obj.setAttribute('class', 'selected');
                }
            }

            var headSel_Obj = document.getElementById("headTabConfig");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
