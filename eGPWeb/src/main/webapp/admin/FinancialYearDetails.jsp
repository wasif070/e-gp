<%--
    Document   : FinancialYearDetails
    Created on : Nov 1, 2010, 7:05:42 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.FinancialYearSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="java.util.Iterator"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Financial Year</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 20) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 20) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/FinancialYearServlet", {funName : "viewfinancialyeardetails",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 20) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
    </head>
    <body onload="loadTable();">
        <%
                    String mesg = request.getParameter("msg");
                    int id = 0;
        %>

        <div class="dashboard_div">
             <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea_1">

                        <td class="contentArea" valign="top">
  <div class="pageHead_1">View Financial Year
  <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('3');">Save as PDF</a></span>
  </div>

    <%if (mesg != null) {%>
         <div class="responseMsg successMsg t_space"><%=mesg%></div>
    <%}%>
    <div class="t-align-right t_space b_space">

       <a href="FinancialYear.jsp" class="action-button-configure">Configure Financial Year</a>

    </div>
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,3">
        <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th width="26%" class="t-align-center">Financial Year</th>
                                <th width="15%" class="t-align-center">Is Default financial year?</th>
                                <th width="15%" class="t-align-center">Action</th>
        </tr>
                            </table>
                            <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                            <form name="frmConfigureCalendarView" id="frmConfigureCalendarView" action="HolidayConfigurationView.jsp" method="post" >
          <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="button" name="button" id="btnGoto" id="button" value="Go To Page" />
                                </label></td>
                            <td align="right" class="prevNext-container"><ul>
                                    <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                    <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                </ul></td>
                            </tr></form>
    </table>
                            <input type="hidden" id="pageNo" value="1"/>
                            <input type="hidden" id="size" value="20"/>

<form id="formstyle" action="" method="post" name="formstyle">

   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
   <%
     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
     String appenddate = dateFormat1.format(new Date());
   %>
   <input type="hidden" name="fileName" id="fileName" value="FinancialYear_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="FinancialYear" />
</form>
  <div>&nbsp;</div>
  <%--<div align="center"> <%=msg%></div>--%>
   </td>
  </tr>
            </table>

            <!--Dashboard Content Part Start-->

            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
        <script>
            var Interval;
            clearInterval(Interval);
            Interval = setInterval(function(){
                var PageNo = document.getElementById('pageNo').value;
                var Size = document.getElementById('size').value;
                CorrectSerialNumber(PageNo-1,Size); 
            }, 100);

            var obj = document.getElementById('lblFinancialYearView');
            if(obj != null){
                if(obj.innerHTML == 'View'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
