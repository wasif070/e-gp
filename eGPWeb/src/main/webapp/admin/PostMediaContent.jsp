<%-- 
    Document   : PostMediaContent
    Created on : Apr 2, 2011, 11:36:02 AM
    Author     : Ketan Prajapati
--%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            String strMediaType = request.getParameter("contentType");
            String strPageTitle = "";
            if (strMediaType.equals("Press")) {
                strPageTitle = "Post New Press Release";
            } else if (strMediaType.equals("Complaint")) {
                strPageTitle = "Post New Complaint Resolution";
            } else {
                strPageTitle = "Post New Other Information";
            }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=strPageTitle%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
            $(document).ready(function(){
                $(function(){
                    /* $('#txt_MediaSubject').keyup(function(){
                        $("#errSubject").remove();
                        if($.trim($("#txt_MediaSubject").val()).length == 0) {
                            $("#txt_MediaSubject").parent().append("<div class='err' id='errSubject' style='color:red;'>Please enter Subject</div>");
                        }
                        else  if($.trim($("#txt_MediaSubject").val()).length > 100) {
                            $("#txt_MediaSubject").parent().append("<div class='err' id='errSubject' style='color:red;'>Maximum 100 characters are allowed</div>");
                        }
                    });*/
                    $('#txt_MediaSubject').blur(function(){
                        $("#errSubject").remove();
                        if($.trim($("#txt_MediaSubject").val()).length == 0) {
                            $("#txt_MediaSubject").parent().append("<div class='err' id='errSubject' style='color:red;'>Please enter Subject</div>");
                            $("#txt_MediaSubject").val("");
                        }
                        else if($.trim($("#txt_MediaSubject").val()).length > 100) {
                            $("#txt_MediaSubject").parent().append("<div class='err' id='errSubject' style='color:red;'>Maximum 100 characters are allowed</div>");
                        }
                    });
                    //$('#txtarea_MediaDetail').ckeditor();
                    // CKEDITOR.instances["txtarea_MediaDetail"].on("instanceReady", function() {
                    CKEDITOR.instances.txtarea_MediaDetail.on('blur', function() {
                        $("#errDetail").remove();
                        if(isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtarea_MediaDetail.getData().replace(/<[^>]*>|\s/g, '')))){
                            $("#txtarea_MediaDetail").parent().append("<div class='err' id='errDetail' style='color:red;'>Please enter Detail</div>");
                            CKEDITOR.instances.txtarea_MediaDetail.setData("");
                        }
                        else if($.trim(CKEDITOR.instances.txtarea_MediaDetail.getData().replace(/<[^>]*>|\s/g, '')).length > 5000) {
                            $("#txtarea_MediaDetail").parent().append("<div class='err' id='errDetail' style='color:red;'>Maximum 5000 characters are allowed</div>");
                        }
                    });
                    // });

                });
            });
            
            function goBack()
            {
                var vlink;
                var vUserTypeId = "<%=strUserTypeId%>";
                if(vUserTypeId == "1")
                {
                    vLink = "<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType="+"<%=strMediaType%>";
                    window.location=vLink;
                    return true;
                }
                else
                {
                    vLink = "<%=request.getContextPath()%>/tenderer/MediaContList.jsp?contentType="+"<%=strMediaType%>";
                    window.location=vLink;
                    return true;
                }
            }
        </script>

        <script type="text/javascript">
            function validate()
            {
                $(".err").remove();
                var valid = true;
                if($.trim($("#txt_MediaSubject").val()).length == 0) {
                    $("#txt_MediaSubject").parent().append("<div class='err'id='errSubject' style='color:red;'>Please enter Subject</div>");
                    $("#txt_MediaSubject").val("");
                    valid = false;
                }
                if($("#txt_MediaSubject").val().length > 100){
                    $("#txt_MediaSubject").parent().append("<div class='err' id='errSubject' style='color:red;'>Maximum 100 characters are allowed</div>");
                    valid = false;
                }
                if(isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtarea_MediaDetail.getData().replace(/<[^>]*>|\s/g, '')))){
                    $("#txtarea_MediaDetail").parent().append("<div class='err' id='errDetail' style='color:red;'>Please enter Detail</div>");
                    valid = false;
                }
                else if($.trim(CKEDITOR.instances.txtarea_MediaDetail.getData().replace(/<[^>]*>|\s/g, '')).length > 5000){
                    $("#txtarea_MediaDetail").parent().append("<div class='err' id='errDetail' style='color:red;'>Maximum 5000 characters are allowed</div>");
                    valid = false;
                }
                else
                {
                        
                }
                if(!valid){
                    return false;
                }
                else
                {
                    var vMediaType = $("#hdn_MediaType").val();
                    var vConfirmMsg;
                    var vConfirmTitle;
                    if(vMediaType == 'Press')
                    {
                        vConfirmMsg = 'Please confirm the content of Press Release';
                        vConfirmTitle = 'New Press Release';
                    }
                    else if(vMediaType == 'Complaint')
                    {
                        vConfirmMsg = 'Please confirm the content of Complaint Resolution';
                        vConfirmTitle = 'New Complaint Resolution';
                    }
                    else if(vMediaType == 'Other')
                    {
                        vConfirmMsg = 'Please confirm the content of Other Information';
                        vConfirmTitle = 'New Other Information';
                    }
                    else
                    {

                    }
                    jConfirm(vConfirmMsg, vConfirmTitle, function(RetVal) {
                        if (RetVal) {
                            document.forms["frmPostMediaContent"].submit();
                        }
                    });
                }
            }
        </script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">

                                <%if (strUserTypeId.equals("1")) {%>
                                <td>
                                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                                </td>
                                <%}%>
                                  <td class="contentArea">
                <%if (strMediaType.equals("Press")) {%>
                <div class ="pageHead_1">New Press Release
                    <%} else if (strMediaType.equals("Complaint")) {%>
                    <div class ="pageHead_1">New Complaint Resolution
                        <%} else {%>
                        <div class ="pageHead_1">New Other Information
                            <%}%>
                            <span class="c-alignment-right"><a href="#" onclick="return goBack()"   title="Go Back" class="action-button-goback">Go Back</a></span></div>
                        <form  id = "frmPostMediaContent" name = "frmPostMediaContent" method = "post" action ="<%=request.getContextPath()%>/GetPressRelease">
                            <table width="100%" cellspacing="8" class="formStyle_1 ">
                                <tr>
                                    <td style="font-style: italic" colspan="2" class="ff">Fields marked with (<span>*</span>) are mandatory</td>
                                </tr>
                                <tr>
                                    <td width="9%" class="ff">Subject : <span>*</span></td>
                                            <td width="91%"><input type="text" name="txt_MediaSubject" id="txt_MediaSubject" class="formTxtBox_1"  style="width:50%;" maxlength="101" /></td>
                                </tr>
                                <tr>
                                    <td width="9%" class="ff">Detail : <span>*</span></td>
                                    <td>
                                        <textarea cols="100" rows="5" name="txtarea_MediaDetail" id="txtarea_MediaDetail"  class="formTxtBox_1"></textarea>
                                        <script type="text/javascript">
                                            CKEDITOR.replace( 'txtarea_MediaDetail',
                                            {
                                                fullPage : false
                                            });
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">&nbsp;</td>
                                    <td><label class="formBtn_1"><input type="button" name="button3" id="button3" value="Submit" onclick=" validate();" /></label></td>
                                </tr>
                            </table>


                            <input type="hidden" value="postMediaContent" name="hdn_action" id="action" />
                            <input type="hidden" value="<%=strMediaType%>" name="hdn_MediaType" id="hdn_MediaType" />
                        </form>
                    </div>
                </div>
                                  </td>
                            </tr>
                </table>
                    <!--Dashboard Content Part End-->

                    <!--Dashboard Footer Start-->
                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    <!--Dashboard Footer End-->
                </div>
                </body>
            <script type="text/javascript" >
                                    var vContentType="<%=strMediaType%>";
                                    var headSel_ObjLeft;
                                    if(vContentType == "Press")
                                    {
                                        headSel_ObjLeft = document.getElementById("lblPressCreate");
                                    }
                                    else if(vContentType == "Complaint")
                                    {
                                        headSel_ObjLeft = document.getElementById("lblComplaintCreate");
                                    }
                                    else
                                    {
                                        headSel_ObjLeft = document.getElementById("lblOtherInfoCreate");
                                    }
                                    headSel_ObjLeft.setAttribute("class", "selected");
                                    var headSel_ObjTop = document.getElementById("headTabContent");
                                    headSel_ObjTop.setAttribute("class", "selected");
                                </script>
</html>
