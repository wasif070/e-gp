<%-- 
    Document   : ViewProjectDetail
    Created on : Nov 15, 2010, 7:03:50 PM
    Author     : parag
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="java.util.List,java.util.Date,com.cptu.egp.eps.dao.storedprocedure.CommonAppData,com.cptu.egp.eps.web.utility.SelectItem" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn,java.text.SimpleDateFormat"  %>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%
            String isPDF = "abc";
            if (request.getParameter("isPDF") != null) {
                isPDF = request.getParameter("isPDF");
            }

            Logger LOGGER = Logger.getLogger("ViewProjectDetail.jsp");
            String userID="0";
            
            if (!(isPDF.equalsIgnoreCase("true"))) {
                if(session.getAttribute("userId")!=null){
                userID=session.getAttribute("userId").toString();
                }
                projSrUser.setLogUserId(userID);
            }
            else
            {
                userID = request.getParameter("userid");
            }
            String str = request.getContextPath();
            int projectId = 1;
            String mode="";

            if(request.getParameter("projectId") != null ){
                 projectId = Integer.parseInt(request.getParameter("projectId"));                
            }
            if(request.getParameter("mode") != null){
                   mode = request.getParameter("mode");
            }

            List<SelectItem> programList = projSrUser.getProgramList();
            List<SelectItem> developmentPartner = projSrUser.getDevelopmentPartner();
            SPProjectDetailReturn pProjectDetailReturns = null;
            List<SPProjectDetailReturn> developmentPar = null;
            projSrUser.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            pProjectDetailReturns = projSrUser.getpProjectDetailReturns(projectId,Integer.parseInt(userID)).get(0);
            
            
            developmentPar = projSrUser.getDevelopmentPar(projectId,Integer.parseInt(userID));
            // projectId = pProjectDetailReturns.getProjectId();
                
                
            

            List<SPProjectRolesReturn> projectRolesReturns = null;

            if (request.getParameter("hidProjectId") != null) {
                projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());
                
            }

            try {
                projectRolesReturns = projSrUser.getRolesReturns(projectId);
            } catch (Exception e) {
                LOGGER.error(" projectRolesReturns ::: " + e);
            }
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Project Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <div class="mainDiv">
            <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%}%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                            String reqURL = request.getRequestURL().toString();
                            String reqQuery = "mode=" + mode + "&projectId=" + Integer.toString(projectId) +"&userid="+userID;
                            String folderName = pdfConstant.PROJECT;
                            String id = Integer.toString(projectId);
                             if (!(isPDF.equalsIgnoreCase("true"))) {
                                pdfCmd.genrateCmd(reqURL, reqQuery, folderName, id);
                                }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                            <span style="float: right; text-align: right;" id="blockprint">
                                <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=id%>">Save As PDF </a>&nbsp;
                                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                <a class="action-button-goback" href="ProjectDetail.jsp" title="go back">Go Back</a>
                            </span>
                            <%}%>
                            <div  id="print_area">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="viewprotable">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1 t-align-left">View Project
                                            
                                        </div>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <form method="POST" id="frmGovUser" action="">

                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff" width="25%" nowrap>Programme : </td>
                                                    <td width="78%">

                                                        <% if(!(pProjectDetailReturns.getProgName()).equalsIgnoreCase("")){
                                                        out.print(pProjectDetailReturns.getProgName());}else{out.print("-");}
                                                        %></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" nowrap>Project Code :  </td>
                                                    <td><%out.print(pProjectDetailReturns.getProjectCode());%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" nowrap>Project Name :  </td>
                                                    <td><% out.print(pProjectDetailReturns.getProjectName());%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" nowrap>Project Cost (In Nu.) : </td>
                                                    <td><%out.print(String.format("%.2f",pProjectDetailReturns.getProjectCost()));%>
                                                    </td>
                                                </tr>
                                                <% SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
                                                            SimpleDateFormat sd1 = new SimpleDateFormat("dd/MM/yyyy");
                                                %>
                                                <tr>
                                                    <td class="ff" nowrap> Project Start Date :   </td>
                                                    <%--<td><%out.print(sd1.format(sd.parse(pProjectDetailReturns.getProjectStartDate())));--%>
                                                       <td><%out.print((pProjectDetailReturns.getProjectStartDate()));%></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" nowrap>Project End Date :   </td>
                                                    <%--<td><%out.print(sd1.format(sd.parse(pProjectDetailReturns.getProjectEndDate())));%>--%>
                                                        <td><%out.print((pProjectDetailReturns.getProjectEndDate()));%></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" nowrap>Source of Fund :  </td>
                                                    <td><%out.print(pProjectDetailReturns.getSourceOfFund());%>
                                                </tr>
                                                <%if(developmentPar.size()>0){%>
                                                <tr>
                                                    <td class="ff" nowrap>Development Partner : </td>
                                                    <td><%String devePar = "";
                                                                for (int i = 0; i < (developmentPartner.size()); i++) {
                                                                    for (int j = 0; j < (developmentPar.size()); j++) {
                                                                        if (developmentPar.get(j).getsBankDevelopId() == Integer.parseInt(developmentPartner.get(i).getObjectId().toString())) {
                                                                         devePar += developmentPar.get(j).getSbDevelopName()+",   ";

                                                                        }
                                                                    }
                                                                }
                                                                    int lenString  = devePar.length();
                                                                   // devePar = devePar.substring(0, lenString-1);
                                                                out.print(devePar.substring(0, lenString-4));
                                                        %>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                                <div class="tableHead_1 t_space">Project Users</div>
                                    <%--<div align="right" class=""></div>--%>
                                            <table width="100%" cellspacing="0" class="tableList_1" >
                                                <tr>
                                                    <th class="t-align-left" width="6%" >Sl. No. </th>
                                                    <th class="t-align-left" >Name of Officer - Designation</th>
                                                    
                                                    <th class="t-align-left" width="18%">Procurement Role </th>
                                                    <th class="t-align-left" >Office </th>
<!--                                                    <th class="t-align-center" >View Financial Power </th>-->
                                                </tr>
                                                <%if (projectRolesReturns != null) {
                                                    if(projectRolesReturns.size() > 0){
                                                         for (int i = 0; i < projectRolesReturns.size(); i++) {%>
                                                <tr>
                                                    <td class="t-align-center" width="5%" ><%=(i + 1)%></td>
                                                    <td class="t-align-left"><%=projectRolesReturns.get(i).getEmpName()%></td>
                                                    
                                                    <td class="t-align-left">
                                                        <%
                                                            if(projectRolesReturns.get(i).getRolesName().contains("PE"))
                                                            {
                                                                out.print("PA");
                                                            }
                                                            else
                                                            {
                                                                out.print(projectRolesReturns.get(i).getRolesName());
                                                            }
                                                            
                                                        %>
                                                    </td>
                                                    <td class="t-align-center"><%=projectRolesReturns.get(i).getOfficeName()%></td>
                                                    <%--<td class="t-align-center"><a href="ProjectRoles.jsp?Edit=Edit&userId=<%=projectRolesReturns.get(i).getUserId()%>&hidProjectId=<%=projectId%>" > Edit </a></td>                                                    --%>
                                                    
<!--                                                    <td class="t-align-center">
                                                        <%if(projectRolesReturns.get(i).getRolesName().trim().equals("AU")){out.print("-");}
                                                        else{%>
                                                        <a href="#" onclick="window.open('ViewProjectUserFP.jsp?userId=<%=projectRolesReturns.get(i).getUserId()%>&projectId=<%=projectId%>', '', 'resizable=yes,scrollbars=1','')" title="View">View</a>
                                                        <%}%>
                                                    </td>-->
                                                </tr>
                                                <%}
                                                  }else{
                                                        out.print("<tr> <td class='t-align-center' colspan='5' > No Records Found. </td></tr>");
                                               }                         
                                             }
                                              else{
                                                        out.print("<tr> <td class='t-align-center' colspan='5' > No Records Found. </td></tr>");
                                               }
                                            %>
                                            </table>
                                            <%if("approve".equalsIgnoreCase(mode)){ %>
                                            <table border="0" cellspacing="0" cellpadding="10" width="100%" class="formStyle_1 t_space">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-center">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmission" id="btnSubmission" value="Approve" onclick="forApprove();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <%}%>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            </div>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <%@include file="/resources/common/Bottom.jsp" %>
                <%}%>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<form id="form1" method="post">
    <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
</form>
<script>
function forApprove(){
    var flag=false;
      document.getElementById("form1").action="<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&action=approve";
      document.getElementById("form1").submit();
}
</script>
<script type="text/javascript">
    $(document).ready(function() {
                $("#print").click(function() {                    
                    printElem({ leaveOpen: true, printMode: 'popup' });                    
                });
            });
            function printElem(options){
                //document.getElementById('blockprint').style.visibility = 'hidden';
                $('#print_area').printElement(options);
                
            }
        </script>      

