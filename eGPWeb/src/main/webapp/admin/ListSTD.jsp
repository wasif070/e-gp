<%--
    Document   : ListSTD
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Standard Bidding Document (SBD)</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
         <script src="../resources/js/jQuery/jquery.generatepdf.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    
    <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }
            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", true)
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0 && $('#pageNo').val()!=totalPages)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo < totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        }); 

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnSearch').click(function() {
                count=0;
                if(document.getElementById('SBDName') !=null && document.getElementById('SBDName').value !=''){
                    count = 1;
                }

                if(count == 0){
                    $('#valMsg').html('Please enter at least One search criteria');
                }else{
                    $('#valMsg').html('');
                    $('#pageNo').val('1');
                    loadTable();
                }
            });
            $('#btnReset').click(function() {
               $("#pageNo").val("1");
                $("#SBDName").val('');
                $('#valMsg').html('');
                loadTable();
            });
        });
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/StdGrid", {
                    action: "getData",
                    stat: $("#stat").val(),
                    SBDName: $("#SBDName").val(),
                    yrmplateName: "string", 
                    noOfSections: "byte",
                    pageNo: $("#pageNo").val(),
                    size: $("#size").val()
                },  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                //sortTable();
                //sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                chkdisble($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
            });
        }
    </script>      

    
    <body onload="loadTable();">
        <%
            StringBuffer colHeader = new StringBuffer();
            colHeader.append("'Sl. No.','SBD Name','Procurement Type','No. Of Section','Action'");
        %>
        <script type="text/javascript" >
            function changeTab(ind){
                if(eval(ind) == 1){
                    document.getElementById("linkAccepted").className = "sMenu";
                    document.getElementById("linkCanceled").className = "";
                    document.getElementById("linkPending").className = "";
                    document.getElementById("stat").value = "A";
                    //loadGrid();
                    loadTable();
                }else if(eval(ind) == 2){
                    document.getElementById("linkAccepted").className = "";
                    document.getElementById("linkCanceled").className = "sMenu";
                    document.getElementById("linkPending").className = "";
                    document.getElementById("stat").value = "C";
                    //loadGrid();
                    loadTable();
                }else if(eval(ind) == 3){
                    document.getElementById("linkAccepted").className = "";
                    document.getElementById("linkCanceled").className = "";
                    document.getElementById("linkPending").className = "sMenu";
                    document.getElementById("stat").value = "P";
                    //loadGrid();
                    loadTable();
                }
            }
            /*function loadGrid(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    datatype: 'xml',
                    url : '<%=request.getContextPath()%>/StdGrid?action=getData&stat='+document.getElementById("stat").value+"&yrmplateName=string&noOfSections=byte",
                    height: 233,
                    colNames:[<%= colHeader %>],
                    colModel:[
                        {name:'Sl. No.',index:'Sl. No.', width:10,sortable: false, search: false, align:'center'},
                        {name:'templateName',index:'templateName', width:60, searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'procType',index:'procType', width:30, sortable: true, search: false},
                        {name:'noOfSections',index:'noOfSections', width:10, search: false, hidden: true},
                        {name:'Edit',index:'Edit', width:50,sortable: false, search: false, align:'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: " ",
                    searchGrid: {sopt:['cn','bw','eq','ne','lt','gt','ew']},
                    gridComplete: function(){
                        //$("#list tr:nth-child(odd)").addClass("bgColor-lightGreen");
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                        //$('#' + "#list" + ' tr:nth-child(even)').addClass("evenTableRow");
                        //$('#' + "#list" + ' tr:nth-child(odd)').addClass("oddTableRow");
                    }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            $(document).ready(function(){
                loadGrid();
            });*/
            function confirmDelete(){
                if(window.confirm("Do you really want to delete the SBD??")){
                    //obj.href = href="../StdGrid?action=delStd&templateId=" + tenderId;
                    return true;
                }
                else{
                    return false;
                }
            }
        </script>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <!--Page Content Start-->
            <div class="contentArea">
                <div class="t_space">
                    <div class="pageHead_1">
                        Standard Bidding Documents (SBD)
                        <span style="float: right; text-align: right;">
                            <a class="action-button-add" href="CreateTemplate.jsp">Create Standard Bidding Document</a>
                            <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('3');">Save as PDF</a>
                        </span>
                    </div>
                </div>
                <input type="hidden" id="stat" name="stat" value="P" />
            
                <% if(request.getParameter("msg")!=null && "delSuc".equalsIgnoreCase(request.getParameter("msg"))){ %>
                    <div class="responseMsg successMsg">SBD deleted successfully</div>
                <% } %>

                <div class="formBg_1">                 
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                        <tr>
                            <td class="ff">SBD Name : </td>
                            <td><input type="text" class="formTxtBox_1" id="SBDName" style="width:202px;" /></td>
                        </tr>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                <label class="formBtn_1"><input type="button" tabindex="6" name="search" id="btnSearch" value="Search"/>
                                </label>&nbsp;&nbsp;
                                <label class="formBtn_1"><input type="button" tabindex="7" name="reset" id="btnReset" value="Reset"/>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                <div class="reqF_1" id="valMsg"></div>
                            </td>
                        </tr>
                    </table>
                </div>  

            
            
                <table width="100%" cellspacing="0" class="t_space">
                    <tr valign="top">
                        <td>
                            <ul class="tabPanel_2">
                                <li><a href="javascript:void(0);" id="linkPending" class="sMenu" onclick="changeTab(3);">Pending</a></li>

                            <%
                               if(userTypeId == 19 || userTypeId == 20)
                               {

                                    if(objUserRightsMap!=null && objUserRightsMap.containsValue("Accepted"))
                                    { %> <li> <% }
                                    else
                                    { %> <li style="display: none;"> <% }

                                    %>
                                        <a href="javascript:void(0);" id="linkAccepted" onclick="changeTab(1);">Accepted</a></li>
                                    <%
                                    if(objUserRightsMap!=null && objUserRightsMap.containsValue("Cancelled"))
                                    { %> <li> <% }
                                    else
                                    { %> <li style="display: none;"> <% }
                                      %>
                                        <a href="javascript:void(0);" id="linkCanceled" onclick="changeTab(2);">Cancelled</a></li>
                                    <%
                               }
                               else
                               {
                                %>
                                <li><a href="javascript:void(0);" id="linkAccepted" onclick="changeTab(1);">Accepted</a></li>
                                <li><a href="javascript:void(0);" id="linkCanceled" onclick="changeTab(2);">Cancelled</a></li>
                               <%
                               }
                           %>
                            </ul>
    <!--                        <div class="tabPanelArea_1">
                                <div id="jQGrid" align="center" style="width: 98.3%;">
                                    <div><table id="list"></table></div>
                                    <div id="page"></div>
                                </div>
                            </div>-->
                            <!--Bottom controls-->
                        </td>
                    </tr>
                </table>
                            
                <div class="tableHead_1 t_space">
                    Search Result
                </div>
            
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,4">
                        <tr>
                            <th width="5%" class="t-align-center">
                                Sl. <br/>
                                No.
                            </th>
                            <th width="40%" class="t-align-center">
                                SBD Name                             
                            </th>
                            <th width="20%" class="t-align-center">
                                Procurement Type
                            </th>
                            <th width="35%" class="t-align-center">
                                Action
                            </th>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                            <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                            &nbsp;
                            <label class="formBtn_1">
                                <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                            </label></td>
                            <td  class="prevNext-container">
                                <ul>
                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="10"/>
                    </div>
                </div>
            </div>
            <!--Page Content End-->
            <!--Middle Content Table End-->
            <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="STDListing_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="debarment" />
            </form>
            <!--For Generate PDF  Ends-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
         
    </body>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
<%
    if(colHeader != null){
        colHeader = null;
    }
%>