<%-- 
    Document   : AppReviseConfigaration
    Created on : May 10, 2016, 10:50:10 AM
    Author     : aprojit
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppreviseConfiguration"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="appReviseSrBean" class="com.cptu.egp.eps.web.servicebean.AppReviseSrBean"/>

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>APP Revise Date Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>        
    </head>
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body>            
    <script type="text/javascript">
        $(document).ready(function(){
            $("#frmAppReviseConfig").validate({
             rules:{
                 financialyear:{required:true},
                 startDate:{required:true},
                 endDate:{required:true},
                 maxReviseCount:{required:true,digits:true, maxlength: 3}
             },
             messages:{
                 financialyear:{required:"<div class='reqF_1'>Please Select Financial Year</div>"},
                 startDate:{required:"<div class='reqF_1'>Please Enter Start Date</div>"},
                 endDate:{required:"<div class='reqF_1'>Please Enter End Date</div>"},
                 maxReviseCount:{required:"<div class='reqF_1'>Please Enter Maximum Revise Count Number</div>", digits:"<div class='reqF_1'>Please Enter Integer Number</div>", maxlength:"<div class='reqF_1'>Maximum 3 numbers are allowed</div>"}
             },
            errorPlacement: function(error, element) {                                
                        if (element.attr("name") == "startDate"){
                            error.insertAfter("#astartDate");
                        }                        
                        else if (element.attr("name") == "endDate"){
                            error.insertAfter("#aendDate");
                        }
                        else{
                            error.insertAfter(element);
                        }
                    } 
            });              
        });
        function GetCalWithouTime(txtname,controlname)
                {
                    new Calendar({
                        inputField: txtname,
                        trigger: controlname,
                        showTime: false,
                        dateFormat:"%d/%m/%Y",
                        onSelect: function() {
                            var date = Calendar.intToDate(this.selection.get());
                            LEFT_CAL.args.min = date;
                            LEFT_CAL.redraw();
                            this.hide();
                            document.getElementById(txtname).focus();
                        }
                    });

                    var LEFT_CAL = Calendar.setup({
                        weekNumbers: false
                    })
                }
    </script>
        <%
        java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
        appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");
        appReviseSrBean.setLogUserId(session.getAttribute("userId").toString());
        TblAppreviseConfiguration tblAppreviseConfigurationEdit = new TblAppreviseConfiguration();
        %>
        
        <%
        String mesg = request.getParameter("msg");
        String financialYear = "";
        Date startDate = null;
        Date endDate= null;
        String maxReviseCount = "";
        String userid = session.getAttribute("userId").toString();
        String msg="";
        String show = "";
        
        String configid = "";
        boolean isEdit = false;
        List forEdit = null;
        if(request.getParameter("appConfgId")!=null){
                configid =request.getParameter("appConfgId");
                isEdit = true;
                 forEdit = appReviseSrBean.getAppConfigurationDetails(configid);
                 if(forEdit!=null){
                    if(forEdit.size()>0){
                        tblAppreviseConfigurationEdit = (TblAppreviseConfiguration)forEdit.get(0);
                    }
                 }

            }
        
        if ("Submit".equals(request.getParameter("submit")) || "Update".equals(request.getParameter("update"))) {
            financialYear = request.getParameter("financialyear");            
            startDate = DateUtils.formatStdString(request.getParameter("startDate"));
            endDate = DateUtils.formatStdString(request.getParameter("endDate"));
            maxReviseCount =  request.getParameter("maxReviseCount"); 
            appReviseSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
           if("Update".equals(request.getParameter("update"))){
               String finanYearUpdate = request.getParameter("hdnFinYear");
                msg = appReviseSrBean.updateAppReviseconfiguration(finanYearUpdate, startDate, endDate, Integer.parseInt(maxReviseCount), Integer.parseInt(userid));
                System.out.println("Testing---------------------->>>>>>>>>>>>  "+msg);
                if (msg.equals("Value Updated")) {
                       show = "APP Revise Date Configuration Successfully Updated";
                       response.sendRedirect("AppReviseConfigurationView.jsp?msg=" + show);
                    }
           }else{
                if (appReviseSrBean.isexistConfiguration(financialYear)){
                    System.out.println("Testing---------------------->>>>>>>>>>>>  Data Already Exist");
                    show = "true";
                    response.sendRedirect("AppReviseConfiguration.jsp?msg=" + show);
                }
                else{
                    msg = appReviseSrBean.addAppReviseconfiguration(financialYear, startDate, endDate, Integer.parseInt(maxReviseCount), Integer.parseInt(userid));
                    System.out.println("Testing---------------------->>>>>>>>>>>>  "+msg);
                    if (msg.equals("Value Added")) {
                       show = "APP Revise Date Configuration Successfully Added";
                       response.sendRedirect("AppReviseConfigurationView.jsp?msg=" + show);
                    }
                }  
           }
        }
        
        %>
          <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">APP Revise Date Configuration</div>
                        
                        <div>&nbsp;</div>
                        <%if(mesg != null){%>
                        <div id="existMessage" style="color:#1c8400;"><h2>App Revise Date of this Financial Year Already Configured</h2></div>
                        <%}%>
                        <div>&nbsp;</div>
                        <div style="font-style: italic;" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

                        <form id="frmAppReviseConfig" action="AppReviseConfiguration.jsp" method="post">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                        <td class="ff" width="23%">Financial Year : <span class="mandatory">*</span></td>
                                           <td>
                                               <select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;"<%if(isEdit){out.print("disabled");}%> onchange='document.getElementById("existMessage").innerHTML="";' >
                                                    <%String defaultValue = appListDtBean.get(0).getFieldName2();
                                                                if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                                    for (CommonAppData commonApp : appListDtBean) {
                                                    %>
                                                    <option <%
                                                        if(isEdit){
                                                            if(commonApp.getFieldName2().trim().equals(tblAppreviseConfigurationEdit.getFinancialYear().trim())){
                                                                     out.print("selected");
                                                                     defaultValue = commonApp.getFieldName2(); 
                                                            }
                                                        } else{
                                                        if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                                                                        out.print("selected");
                                                                        defaultValue = commonApp.getFieldName2();
                                                                    }
                                                          }
                                                    %> value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                                    <%
                                                                    }
                                                                }
                                                    %>
                                                </select>
                                                <input type="hidden" name="hdnFinYear" id="hdnFinYear" value="<%=defaultValue%>"/>
                                           </td>
                                 </tr>
                                 <tr>
                                        <td class="ff" width="23%">Start Date : <span class="mandatory">*</span></td>
                                           <td>
                                              <%if(isEdit){%>
                                              <input name="startDate" type="text" class="formTxtBox_1" id="txtstartDate" style="width:185px;" readonly="true"  onfocus="GetCalWithouTime('txtstartDate','txtstartDate');" value="<%=DateUtils.convertStrToDatenew(tblAppreviseConfigurationEdit.getStartDate())%>"/> &nbsp;
                                              <%}else {%>
                                              <input name="startDate" type="text" class="formTxtBox_1" id="txtstartDate" style="width:185px;" readonly="true"  onfocus="GetCalWithouTime('txtstartDate','txtstartDate');"/> &nbsp;
                                              <%}%>
                                              <a  href="javascript:void(0);" title="Calender" id="astartDate"> <img id="txtstartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtstartDate','txtstartDateimg');"/> </a>
                                          </td>
                                 </tr>
                                 <tr>
                                        <td class="ff" width="23%">End Date : <span class="mandatory">*</span></td>
                                           <td>
                                              <%if(isEdit){%>
                                              <input name="endDate" type="text" class="formTxtBox_1" id="txtendDate" style="width:185px;" readonly="true"  onfocus="GetCalWithouTime('txtendDate','txtendDate');" value="<%=DateUtils.convertStrToDatenew(tblAppreviseConfigurationEdit.getEndDate())%>"/> &nbsp;
                                              <%}else {%>
                                              <input name="endDate" type="text" class="formTxtBox_1" id="txtendDate" style="width:185px;" readonly="true"  onfocus="GetCalWithouTime('txtendDate','txtendDate');"/> &nbsp;
                                              <%}%>
                                              <a  href="javascript:void(0);" title="Calender" id="aendDate"> <img id="txtendDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtendDate','txtendDateimg');"/> </a>
                                           </td>
                                 </tr>
                                 <tr>
                                        <td class="ff" width="23%">Maximum Revise Count : <span class="mandatory">*</span></td>
                                           <td>
                                              <%if(isEdit){%>
                                              <input name="maxReviseCount" type="text" class="formTxtBox_1" id="txtmaxReviseCount" style="width:185px;" value="<%=tblAppreviseConfigurationEdit.getMaxReviseCount()%>"/>
                                              <%}else {%>
                                              <input name="maxReviseCount" type="text" class="formTxtBox_1" id="txtmaxReviseCount" style="width:185px;"/>
                                              <%}%>
                                           </td>
                                 </tr>
                                 <tr>
                                     <td class="ff" width="23%">&nbsp; </td>
                                           <td>
                                              <label class="formBtn_1">
                                                <%if(isEdit){%>
                                                <input name="update" type="submit" id="btnsubmit" value="Update"/>
                                                <%}else {%>
                                                <input name="submit" type="submit" id="btnsubmit" value="Submit"/>
                                                <%}%>
                                              </label>
                                           </td>
                                 </tr>
                            </table>
                        </form>
                    </td>   
                </tr>
            </table>
                                 
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>
