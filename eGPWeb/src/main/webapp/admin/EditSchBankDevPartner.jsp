<%-- 
    Document   : EditSchBankDevPartner
    Created on : Oct 29, 2010, 8:03:11 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="scBankCreationDtBean" class="com.cptu.egp.eps.web.databean.ScBankCreationDtBean" scope="request"/>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<%
            String logUserId = "0";
            if(session.getAttribute("userId")!=null){
                    logUserId = session.getAttribute("userId").toString();
                }
            scBankCreationSrBean.setLogUserId(logUserId);
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String strOffice = request.getParameter("officeId");
            int officeId = 0;
            if (strOffice != null && !strOffice.equalsIgnoreCase("")) {
                officeId = Integer.parseInt(strOffice);
            }
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
            }
            else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
            }
            scBankCreationSrBean.setAuditTrail(null);
            if (officeId != 0) {
                scBankCreationDtBean.setOfficeId(officeId);
                scBankCreationSrBean.setAuditTrail(null);
                scBankCreationDtBean.populateInfo();
            }
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit <%=title%> Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod(
                "regex",
                 function(value, element, regexp) {
                 var check = false;
                 return this.optional(element) || regexp.test(value);
                 },
                 "Please check your input."
                 );
                $('#btnUpdate').attr("disabled", false);
                $("#frmBankSchedule").validate({
                    rules: {
                        //bankName:{required: true},
                        officeAddress:{required:true,maxlength:300},
                        city:{//requiredWithoutSpace: true,
                            spacevalidate:true,alphaCity: true,maxlength:100},
                        thanaUpzilla:{requiredWithoutSpace: true,spacevalidate:true,alphaCity: true,maxlength:100},
                        postCode:{number:true,minlength:4},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        webSite: {url:true,maxlength:50}
                    },
                    messages: {
                        //bankName:{ required: ""
                        //},

                        officeAddress:{ required: "<div class='reqF_1'>Please enter Head Office Address</div>",
                            maxlength:"<div class='reqF_1'Allows maximum 300 characters only</div>"},

                        city:{//requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
                            alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"
                        },
                        thanaUpzilla:{
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Gewog</div>",
                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        postCode:{//required:"<div class='reqF_1'>Please enter Post code.</div>",
                            number: "<div class='reqF_1'>Please enter numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 4 Characters are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 Characters are required.</div>"
                        },

                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Phone no.</br>Do not repeat Area Code.</div>",
                            },
                        faxNo: {regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Fax no.</br>Do not repeat Area Code.</div>",
                            },
                        webSite: {url: "<div class='reqF_1'>Please enter website in www.xyz.gov.bt format</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 50 characters only</div>"}
                    },

                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                            error.insertAfter(element);
                    }
                });
            });
            
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            var bvalidationPostCode = true;
//function valPost(){
//    if($('#txtPostCode').val()!=''){
//                    bvalidationPostCode = true;
//        if($('#partnerType').val()=='Development'){
//            if(alphanumeric($('#txtPostCode').val()))
//            {
//                if(!ChkMaxLength($('#txtPostCode').val()))
//                {
//                    $('#postCodeMsg').html("<br/>Postcode should comprise of 10 digits only");
//                    bvalidationPostCode=false;
//                }
//                else
//                {
//                    $('#postCodeMsg').html("");
//                }
//            }
//            else
//            {
//                        bvalidationPostCode = false;
//                $('#postCodeMsg').html("<br/>Please enter Only Alphanumeric Value");
//            }
//        }else{
//            if(numeric($('#txtPostCode').val()))
//            {
//                if(!ChkMaxLength($('#txtPostCode').val()))
//                {
//                    $('#postCodeMsg').html("<br/>Postcode should comprise of 4 digits only");
//                    bvalidationPostCode=false;
//                }
//                else
//                {
//                    $('#postCodeMsg').html("");
//                }
//            }
//            else
//            {
//                        bvalidationPostCode = false;
//                $('#postCodeMsg').html("<br/>Please enter Numbers Only");
//            }
//        }
//    }else{
//                        bvalidationPostCode = false;
//        $('#postCodeMsg').html('Please enter Postcode');
//                    }
//}
            var bValidation;
            $(function() {
                $('#btnUpdate').click(function() {
                    bValidation = true;
                    if($('#txtBankName').val()==''){
                        $('#branchValidation').html('Please enter <%=title%> Name');
                        bValidation = false;
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function alphanumeric(value)
                {
                    return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
                }
                function numeric(value)
                {
                    return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
                }
                function ChkMaxLength(value)
                {
                    var length = 4;
                    if($('#partnerType').val()=='Development'){
                        length = 10;
                    }
                    var ValueChk=value;
                    if(ValueChk.length>length)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                function chkreg(value)
                {
                    return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

                }
                function regForSpace(value)
                {
                    return /^\s+|\s+$/g.test(value)

                }
                var bvalidationName;
            $(function() {
                $('#txtBankName').change(function() {
                    if($('#txtBankName').val() != $("#oldName").val()) {
                        bvalidationName = true;
                        if($('#txtBankName').val() == ''){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Please enter <%=title%> Name');
                            bvalidationName = false;
                        }else if(regForSpace($('#txtBankName').val())){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Only Space is not allowed');
                            bvalidationName = false;
                        }else if(!chkreg($('#txtBankName').val())){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                            bvalidationName = false;
                        }else if($('#txtBankName').val().length > 100){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Allows maximum 100 characters only');
                            bvalidationName = false;
                        }else{
                            $('#branchValidation').html('');
                            //var alNumRegex = /^([a-zA-Z][a-zA-Z ]*[a-zA-Z])$/;
                                //var alNumRegex = /^[\sa-zA-Z-.\-\(\)&/]+$/;
                            $('#officeMsg').css("color","red");
                            $('#officeMsg').html("Checking for unique <%=title%>...");
                            $.post("<%=request.getContextPath()%>/ScBankServlet", {title: "<%=title%>",type: "<%=type%>",officeName:$('#txtBankName').val(),isBranch: 'No',funName:'verifyOffice'},
                            function(j){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('#officeMsg').css("color","green");
                                }
                                else{
                                    $('#officeMsg').css("color","red");
                                }
                                $('#officeMsg').html(j);
                            });
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                    });
                    if($('#cmbCountry option:selected').text() == "Bangladesh" && $('#partnerType').val() == "ScheduleBank")
                        $('#upjilla').css("display", "table-row");
                    else
                        $('#upjilla').css("display", "none");
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmBankSchedule').submit(function() {
                    if((bvalidationPostCode == true) && bValidation == true &&(bvalidationName== undefined || bvalidationName == true)){
                        if(($('#officeMsg').html().length > 0)) {
                            if(($('#officeMsg').html()=="OK")) {
                                return true;
                            }
                            else{
                                return false;
                            }
                        }
                        else
                        {
                             return true;
                        }
                    }else {
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>

                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Edit <%=title%> Details</div>
                            <!--Page Content Start-->
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=title%> Updation Failed.</div>
                            <%}
                                                                    else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg">Office Updated Successfully</div>
                            <%}%>
                            <!-- Result Display End-->
                            <form name="frmBankSchedule" action="<%=request.getContextPath()%>/ScBankServlet" method="POST" id ="frmBankSchedule">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Name of <%=title%> : <span>*</span></td>
                                        <td>
                                            <input type="text" class="formTxtBox_1" style="width: 400px;" id="txtBankName" name="bankName" value="${scBankCreationDtBean.bankName}" maxlength="101"/>
                                            <div id="officeMsg" style="font-weight: bold;"></div>
                                            <div id="branchValidation" class="reqF_1"></div>
                                            <input type="hidden" id="oldName" name="oldName" value="${scBankCreationDtBean.bankName}"/>
                                            <input type="hidden" id="partnerType" name="partnerType" value="<%=type%>"/>
                                            <input type="hidden" name="isBranch" value="No"/>
                                            <input type="hidden" name="swiftCode" value="${scBankCreationDtBean.swiftCode}"/>
                                            <input type="hidden" name="createdBy" value="${scBankCreationDtBean.createdBy}"/>
                                            <input type="hidden" name="createdDate" value="${scBankCreationDtBean.createdDate}"/>
                                            <input type="hidden" name="officeId" value="${scBankCreationDtBean.officeId}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200"> Address : <span>*</span></td>
                                        <td align="left"><textarea cols="20" rows="6" class="formTxtBox_1"  id="txtaAddress" name="officeAddress" style="width: 400px"  onkeypress="return imposeMaxLength(this, 301, event);">${scBankCreationDtBean.officeAddress}</textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country : <span>*</span></td>
                                        <td>

                                             <%
                                             String country = scBankCreationDtBean.getCountry();
                                                if(!type.equals("ScheduleBank")){%>
                                            <select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 208px">
                                                <%
                                                            
                                                            for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase(country)) {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>" selected="selected"><%=countryItem.getObjectValue()%></option>
                                                <%
                                                                                                                }
                                                                                                                else {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>"><%=countryItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                            <%}else{
                                                for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase(country)) {
                                            %>
                                            <label   id="cmbCountry" name="country" value="<%=countryItem.getObjectId()%>" ><%=countryItem.getObjectValue()%></label>
                                            <input type="hidden"   name="country" value="<%=countryItem.getObjectId()%>" />
                                            <%}}}%>
                                            <input type="hidden" name="action" value="update"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                <%
                                                            String state = scBankCreationDtBean.getState();

                                                            for (SelectItem stateItem : scBankCreationSrBean.getStateList(scBankCreationDtBean.getCountryId())) {
                                                                if (stateItem.getObjectValue().equalsIgnoreCase(state)) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                                                                                }
                                                                                                                else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City / Town : </td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" value="${scBankCreationDtBean.city}" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>
                                    <%          String maxLen = "maxlength='11'";
                                                if (type.equals("ScheduleBank")) {
                                                    maxLen = "maxlength='5'";
                                                    String display = country.equals("Bangladesh") ? "table-row" : "none";
                                    %>
<!--                                    <tr id="upjilla" style="display: <%=display%>">
                                        <td class="ff" width="200">Thana / UpaZilla : <span>*</span></td>
                                        <td><input id="txtUpJilla" class="formTxtBox_1" name="thanaUpzilla" style="width: 200px" type="text" maxlength="101"
                                                   value="${scBankCreationDtBean.thanaUpzilla}"/>
                                        </td>
                                    </tr>-->
                                    <%}%>
                                    <tr>
                                        <td class="ff" width="200">Post Code : </td>
                                        <td><input id="txtPostCode" name="postCode" class="formTxtBox_1" style="width: 200px;" type="text" maxlength="19"
                                                   value="${scBankCreationDtBean.postCode}" onblur="return valPost();"/>
                                        <div id="postCodeMsg" style="color: red;"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNo" style="width: 200px;" name="phoneNo" type="text" maxlength="245"
                                                   value="${scBankCreationDtBean.phoneNo}"/>&nbsp;
                                             <%if (type.equals("ScheduleBank")) {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336962)</span>
                                            <%} else {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336962)</span>
                                        <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. : </td>
                                        <td><input class="formTxtBox_1" id="txtFaxNo" style="width: 200px;" name="faxNo" type="text" maxlength="245"
                                                   value="${scBankCreationDtBean.faxNo}"/>&nbsp;
                                            <%if (type.equals("ScheduleBank")) {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336961)</span>
                                            <%} else {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336961)</span>
                                        <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website : </td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="webSite" type="text" value="${scBankCreationDtBean.webSite}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left" >
                                            <label class="formBtn_1"><input id="btnUpdate" name="update" value="Update" type="submit"  onclick="return valPost();" disabled /></label></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>