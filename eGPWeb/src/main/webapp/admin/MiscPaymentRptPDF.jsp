<%-- 
    Document   : MiscPaymentRptPDF
    Created on : Mar 22, 2012, 11:01:30 AM
    Author     : shreyansh.shah
--%>


<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.service.serviceimpl.CommonSearchService,com.cptu.egp.eps.web.utility.AppContext,java.util.List"%>
<html>
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Miscellaneous Payment Report</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />

</head>
<body>
<%
                String paymentDate = "";
                String emailId = "";
                String payeeName = "";
                String payeeNameSearch = "";
                String[] dtArr = null;
                String sortEmail = "";
                String Amount = "";
                String amountSearch = "";
                String sortEmailId = "";


                if (request.getParameter("emailId") != null && !"".equalsIgnoreCase(request.getParameter("emailId"))) {
                    emailId = request.getParameter("emailId");
                }
                if (request.getParameter("sortEmailId") != null && !"".equalsIgnoreCase(request.getParameter("sortEmailId"))) {
                    sortEmailId = request.getParameter("sortEmailId");
                }
                if (request.getParameter("paymentDate") != null && !"".equalsIgnoreCase(request.getParameter("paymentDate"))) {
                    paymentDate = request.getParameter("paymentDate");
                    dtArr = paymentDate.split("/");
                    paymentDate = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                   // paymentDate = request.getParameter("paymentDate");
                }
                if (request.getParameter("payeeName") != null && !"".equalsIgnoreCase(request.getParameter("payeeName"))) {
                    payeeName = request.getParameter("payeeName");
                }
                if (request.getParameter("payeeNameSearch") != null && !"".equalsIgnoreCase(request.getParameter("payeeNameSearch"))) {
                    payeeNameSearch = request.getParameter("payeeNameSearch");
                }
                if (request.getParameter("Amount") != null && !"".equalsIgnoreCase(request.getParameter("Amount"))) {
                    Amount = request.getParameter("amount");
                }
                if (request.getParameter("amountSearch") != null && !"".equalsIgnoreCase(request.getParameter("amountSearch"))) {
                    amountSearch = request.getParameter("amountSearch");
                }


                String strPageNo = request.getParameter("strPageNo");
                int pageNo = Integer.parseInt(strPageNo);

                String strOffset = request.getParameter("strOffset");
                //String strOffset = "1";
                int recordOffset = Integer.parseInt(strOffset);
                //int recordOffset = 1;

                int srNo = 0;
                srNo = (pageNo - 1) * (recordOffset);
               
                 String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
%>
 <div class="dashboard_div">
  <div class="pageHead_1">Miscellaneous Payment Report</div>
<div id="resultDiv" style="display: block;">
<table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
      <tr>
                <th width="4%" class="t-align-center">Sl. No.</th>
        <th width="14%" class="t-align-center">Description of Payment</th>
        <th width="10%" class="t-align-center">E-Mail ID</th>
        <th width="15%" class="t-align-center">Payee Name</th>
        <th width="20%" class="t-align-center">Payee Address</th>
        <th width="10%" class="t-align-center">Date of Payment</th>
        <th width="10%" class="t-align-center">Amount (In Nu.)</th>
        <th width="12%" class="t-align-center">Remarks</th>

    </tr>
     <%
       CommonSearchDataMoreService searchService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
       List<SPCommonSearchDataMore> spSearchData = searchService.getCommonSearchData("getMiscPaymentReport", strPageNo, strOffset, emailId, sortEmailId, paymentDate, payeeName, payeeNameSearch, Amount, amountSearch);
      for(int i = 0; i < spSearchData.size(); i++){ %>
      <tr>
                                <td class="t-align-center"><%=i+1%></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName6() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName7() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName4() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName5() %></td>
                                <td class="t-align-center"><%= spSearchData.get(i).getFieldName3() %></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName8()%></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName9()%></td>
     </tr>
<% }
%>
</table>
</div>
  <p>&nbsp;</p>
    <div>&nbsp;</div>

</div>
</body>
</html>

