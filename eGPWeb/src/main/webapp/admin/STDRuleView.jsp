<%-- 
    Document   : STDRuleView
    Created on : Nov 25, 2010, 12:30:38 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigStd"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Standard Bidding Document Selection Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
           <script type="text/javascript">
                    $(document).ready(function() {
                        sortTable();
                    });
                </script>
         <script type="text/javascript">
            function conform()
            {
                if (confirm("Do you want to delete this business rule?"))
                    return true;
                else
                    return false;
            }
        </script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();


        %>

    </head>
    <body>

        <%
                    String mesg = request.getParameter("msg");
                    String delMsg = "";
                    if ("delete".equals(request.getParameter("action"))) {
                        String action = "Remove STD Section Rules";
                        try{
                            int configId = Integer.parseInt(request.getParameter("id"));
                            TblConfigStd configStd = new TblConfigStd();
                            configStd.setConfigStdId(configId);
                            configStd.setOperator(">");
                            configStd.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                            configStd.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));
                            configStd.setTblProcurementTypes(new TblProcurementTypes(Byte.parseByte("1")));
                            configStd.setTblTemplateMaster(new TblTemplateMaster(Byte.parseByte("1")));
                            configStd.setTenderValue(new BigDecimal(Byte.parseByte("1")));
                            configPreTenderRuleSrBean.delConfigStd(configStd);
                            delMsg = "SBD Business Rule Deleted Successfully";
                        }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                       }finally{
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                            action=null;
                       }
                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Standard Bidding Document Selection Rule
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('8');">Save as PDF</a></span>
                        </div>
                        <div>&nbsp;</div>
                        <%if ("delete".equals(request.getParameter("action"))) {%>
                        <div class="responseMsg successMsg"><%=delMsg%></div>
                        <%}%>
                        <%if (mesg != null) {%>
                        <div class="responseMsg successMsg"><%=mesg%></div>
                        <%}%>

                        <form action="STDRuleView.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" name="resultTable" id="resultTable"  cols="@8">
                                <tr>
                                    <th width="100" nowrap>Tender Type<br /></th>
                                    <th nowrap>Procurement<br /> Category<br /></th>
                                    <th nowrap>Procurement<br /> Method<br /></th>
                                    <th nowrap>Procurement<br /> Type<br /></th>
                                    <th nowrap>Operator<br /></th>
                                    <th nowrap>Value<br/>(in Nu.)<br /></th>
                                    <th nowrap>Value<br/>(in Words (Nu.))</th>
                                    <th nowrap>SBD Name<br /></th>
                                    <th nowrap>Action</th>
                                </tr>

                                <%
                                            List l = configPreTenderRuleSrBean.getAllConfigStd();

                                            String msg = "";
                                            if (!l.isEmpty() || l != null) {
                                                Iterator it = l.iterator();
                                                int i = 0;
                                                TblConfigStd configStd;
                                                while (it.hasNext()) {
                                                    configStd = (TblConfigStd) it.next();
                                                    i++;

                                %>

                                <%if (i % 2 == 0) {%>
                                <tr style='background-color:#E4FAD0;'>
                                    <%} else {%>
                                    <tr>
                                        <%}%>
                                        <%
                                                                String tenderType = configPreTenderRuleSrBean.getTenderType(configStd.getTenderTypeId());
                                        %>
                                        <td class="t-align-center"><%=tenderType%></td>
                                        <td class="t-align-center"><%=configStd.getTblProcurementNature().getProcurementNature()%></td>
                                        <td class="t-align-center"><%if(configStd.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(configStd.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(configStd.getTblProcurementMethod().getProcurementMethod());}%></td>
                                        <td class="t-align-center"><%if(configStd.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(configStd.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");} %></td>
                                        <td class="t-align-center"><%=configStd.getOperator()%></td>
                                        <td class="t-align-center"><%=configStd.getTenderValue()%></td>
                                        <td class="t-align-center"><span  id="inlakhs_<%=i%>"></span></td>
                                        <td class="t-align-center"><%=configStd.getTblTemplateMaster().getTemplateName()%></td>
                                        <td class="t-align-center"><a href="STDRuleDetails.jsp?action=edit&id=<%=configStd.getConfigStdId()%>">Edit</a>&nbsp;|&nbsp;<a href="STDRuleView.jsp?action=delete&id=<%=configStd.getConfigStdId()%>" onclick="return conform();">Delete</a></td>
                                    </tr></tr>
                                <script type="text/javascript">
                                    $("#inlakhs_"+<%=i%>).html(CurrencyConverter(<%=configStd.getTenderValue()%>));
                                </script>
                                <%
                                                }
                                            } else {

                                                msg = "No Record Found";
                                            }

                                %>

                            </table>
                            <%--<div align="center"> <%=msg%></div>--%>
                        </form>
                    </td>
                </tr>
            </table>
            <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="STDRule_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="STDRule" />
            </form>
            <div>&nbsp;</div>

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var obj = document.getElementById('lblSTDRuleView');
            if(obj != null){
                if(obj.innerHTML == 'View'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabConfig");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>

