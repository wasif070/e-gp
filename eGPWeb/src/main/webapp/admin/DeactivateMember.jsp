<%-- 
    Document   : DeactivateMember
    Created on : May 2, 2017, 11:03:46 AM
    Author     : feroz
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Deactivate Debarment Committee Member</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <%
                    BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
                    TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeThis = new TblBhutanDebarmentCommittee();

                    if ("Deactivate".equals(request.getParameter("button"))) 
                    {
                        String doneFlag = "";
                        String action = "";
                        DateFormat dF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        String uName = request.getParameter("uName");
                        String uDesig = request.getParameter("uDesig");
                        String uRole = request.getParameter("uRole");
                        String uMail = request.getParameter("uMail");
                        String uMob = request.getParameter("uMob");
                        String uCid = request.getParameter("uCid");
                        String uAddress = request.getParameter("uAddress");
                        String fromDate = request.getParameter("fromDate");
                        String toDate = request.getParameter("toDate");
                        String createdDate = request.getParameter("createdDate");
                        String remarks = request.getParameter("remarks");
                        int createdBy = Integer.parseInt(request.getParameter("createdBy"));
                        try{
                                tblBhutanDebarmentCommitteeThis.setId(Integer.parseInt(request.getParameter("memberId")));
                                tblBhutanDebarmentCommitteeThis.setMemberName(uName);
                                tblBhutanDebarmentCommitteeThis.setDesignation(uDesig);
                                tblBhutanDebarmentCommitteeThis.setRole(uRole);
                                tblBhutanDebarmentCommitteeThis.setEmailId(uMail);
                                tblBhutanDebarmentCommitteeThis.setMobileNo(uMob);
                                tblBhutanDebarmentCommitteeThis.setNationalId(uCid);
                                tblBhutanDebarmentCommitteeThis.setContactAddress(uAddress);
                                tblBhutanDebarmentCommitteeThis.setFromDate(DateUtils.convertDateToStr(fromDate));
                                tblBhutanDebarmentCommitteeThis.setToDate(DateUtils.convertDateToStr(toDate));
                                tblBhutanDebarmentCommitteeThis.setCreateDate(DateUtils.convertDateToStr(createdDate));
                                tblBhutanDebarmentCommitteeThis.setIsActive(false);
                                tblBhutanDebarmentCommitteeThis.setCreatedBy(createdBy);
                                tblBhutanDebarmentCommitteeThis.setRemarks(remarks);
                                doneFlag = bhutanDebarmentCommitteeService.updateTblBhutanDebarmentCommittee(tblBhutanDebarmentCommitteeThis);
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        if(uRole.equals("Chairman"))
                        {
                            if(doneFlag.equals("editSucc"))
                            {
                                response.sendRedirect("DebarComChair.jsp?msg=dctS");
                            }
                            else
                            {
                                response.sendRedirect("DebarComChair.jsp?msg=dctF");
                            }

                        }else
                        {
                            if(doneFlag.equals("editSucc"))
                            {
                                response.sendRedirect("InactiveMembers.jsp?msg=dctS");
                            }
                            else
                            {
                                response.sendRedirect("InactiveMembers.jsp?msg=dctS");
                            }
                        }
                        
                    }
         %>
        
    </head>
    <body>
        <%
            if(request.getParameter("memberId")!=null)
            {
                List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommitteeEdit = bhutanDebarmentCommitteeService.findTblBhutanDebarmentCommittee(Integer.parseInt(request.getParameter("memberId")));
            
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"><div class="pageHead_1">Deactivate Debarment Committee Member</div>
                        <form action="DeactivateMember.jsp" method="post">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td class="ff">Full Name :</td>
                                <td><input type="hidden" id="uName" name="uName" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getMemberName()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getMemberName());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Designation :</td>
                                <td><input type="hidden" id="uDesig" name="uDesig" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getDesignation()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getDesignation());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Role :</td>
                                <td><input type="hidden" id="uRole" name="uRole" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getRole()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getRole());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Email Id :</td>
                                <td><input type="hidden" id="uMail" name="uMail" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getEmailId()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getEmailId());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Mobile No. :</td>
                                <td><input type="hidden" id="uMob" name="uMob" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getMobileNo()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getMobileNo());%></td>
                            </tr>
                            <tr>
                                <td class="ff">CID No. :</td>
                                <td><input type="hidden" id="uCid" name="uCid" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getNationalId()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getNationalId());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Contact Address :</td>
                                <td><input type="hidden" id="uAddress" name="uAddress" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getContactAddress()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getContactAddress());%></td>
                            </tr>
                            <tr>
                                <td class="ff">From Date :</td>
                                <td>
                                    <input name="fromDate" type="hidden" class="formTxtBox_1" id="fromDate" style="width:100px;" readonly="true"  onfocus="GetCal('fromDate', 'fromDate');" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getFromDate()%>"/>
                                    <%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getFromDate());%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">To Date :</td>
                                <td>
                                    <input name="toDate" type="hidden" class="formTxtBox_1" id="toDate" style="width:100px;" readonly="true"  onfocus="GetCal('toDate', 'toDate');" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getToDate()%>"/>
                                    <%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getToDate());%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><input type="hidden" id="remarks" name="remarks" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getRemarks()%>"><%out.print(tblBhutanDebarmentCommitteeEdit.get(0).getRemarks());%></td>
                            </tr>
                            <input type="hidden" name="memberId" id="memberId" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getId()%>">
                            <input type="hidden" name="createdDate" id="createdDate" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getCreateDate()%>">
                            <input type="hidden" name="createdBy" id="createdBy" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getCreatedBy()%>">
                                            
                            </tbody>
                        </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:9%;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Deactivate" onclick=""/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <%}%>
    </body>
    <script>
        var obj = document.getElementById('lblConfigPaThreshold');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function GetCal(txtname, controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                onSelect: function () {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                    
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })

        }
    </script>
</html>