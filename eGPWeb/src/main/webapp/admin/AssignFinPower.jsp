<%-- 
    Document   : AssignFinPower
    Created on : Jan 12, 2011, 7:24:07 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialPowerByRole"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Assign Financial Power</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        
        <%
                    StringBuilder finYear = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procRole = new StringBuilder();
                    StringBuilder budgetType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();

                    TblFinancialYear finYear2 = new TblFinancialYear();                    
                    Iterator fi2 = configPreTenderRuleSrBean.getFinancialYear().iterator();
                    
                    Map m = new Hashtable();
                    StringBuffer sb2 = new StringBuffer();
                    while(fi2.hasNext()){
                            TblFinancialYear ty =(TblFinancialYear)fi2.next();
                            String fst = ty.getFinancialYear();
                            String st[]=fst.split("-");
                            if(!m.containsKey(st[0])){
                            m.put(st[0], fst);
                            sb2.append(st[0]+",");
                            }                            
                        }
                    if(sb2.length()>0)
                    sb2.deleteCharAt(sb2.length()-1);
                    
                    String[] strevn = sb2.toString().split(",");
                    List l = Arrays.asList(strevn);
                    Collections.sort(l);

                   
                    long size = configPreTenderRuleSrBean.getFinancialYearCount();
                    int s = 0;
                    while (s<l.size()) {
                        Iterator fit2 = configPreTenderRuleSrBean.getFinancialYear().iterator();
                        while (fit2.hasNext()) {
                            finYear2 = (TblFinancialYear) fit2.next();
                            String yearfirst = (String) l.get(s);
                            String[] yrstr = finYear2.getFinancialYear().split("-");
                            if(yearfirst.equals(yrstr[0]))
                            finYear.append("<option value='" + finYear2.getFinancialYearId() + "'>" + finYear2.getFinancialYear() + "</option>");
                        }
                        s++;
                    }

                    TblBudgetType budgetType2 = new TblBudgetType();
                    Iterator bit2 = configPreTenderRuleSrBean.getBudgetType().iterator();
                    while (bit2.hasNext()) {
                        budgetType2 = (TblBudgetType) bit2.next();
                        budgetType.append("<option value='" + budgetType2.getBudgetTypeId() + "'>" + budgetType2.getBudgetType() + "</option>");
                    }

                    TblProcurementRole tblProcurementRole2 = new TblProcurementRole();
                    Iterator prit2 = configPreTenderRuleSrBean.getProcureRole().iterator();
                    while (prit2.hasNext()) {
                        tblProcurementRole2 = (TblProcurementRole) prit2.next();
                        procRole.append("<option value='" + tblProcurementRole2.getProcurementRoleId() + "'>" + tblProcurementRole2.getProcurementRole() + "</option>");
                    }

                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

        %>


        <script type="text/javascript">

            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='financialYear"+(count+1)+"' class='formTxtBox_1' id='financialYear_"+(count+1)+"'><%=finYear%></select></td>"+
                        "<td class='t-align-center'><select name='budgetType"+(count+1)+"' class='formTxtBox_1' id='budgetType_"+(count+1)+"'><%=budgetType%></select></td>"+
                        "<td class='t-align-center'><select name='procRole"+(count+1)+"' class='formTxtBox_1' id='procRole_"+(count+1)+"'><%=procRole%></select></td>"+
                        "<td class='t-align-center'><select name='procNature"+(count+1)+"' class='formTxtBox_1' id='procNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(count+1)+"' class='formTxtBox_1' id='procMethod_"+(count+1)+"'><%=procMethod%></select></td>"+
                        "<td  class='t-align-center'><select name='operator"+(count+1)+"' class='formTxtBox_1' id='operator_"+(count+1)+"'><option><</option><option>></option><option><=</option><option>>=</option><option>=</option></td>"+
                        "<td class='t-align-center'><input name='amount"+(count+1)+"' type='text' class='formTxtBox_1' id='amount_"+(count+1)+"' /></td>"+
                        "</tr>";

                    totCnt++;
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;

                        }
                    }

                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>

    </head>
    <body>

        <%!        ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRule"));
                       
                        msg = configPreTenderRuleSrBean.delAllFinancialPowerByRole();
                        if (msg.equals("Deleted")) {
                            for (int i = 1; i <= row; i++) {

                                if (request.getParameter("financialYear" + i) != null) {
                                    int finalYear = Integer.parseInt(request.getParameter("financialYear" + i));
                                    byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                    byte prole = Byte.parseByte(request.getParameter("procRole" + i));
                                    byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                    byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                    String operator = request.getParameter("operator"+i);
                                    float amount = Float.parseFloat(request.getParameter("amount"+i));

                                    TblFinancialPowerByRole tblFinancialPowerByRole = new TblFinancialPowerByRole();
                                    tblFinancialPowerByRole.setTblFinancialYear(new TblFinancialYear(finalYear));
                                    tblFinancialPowerByRole.setTblBudgetType(new TblBudgetType(btype));
                                    tblFinancialPowerByRole.setTblProcurementRole(new TblProcurementRole(prole));
                                    tblFinancialPowerByRole.setTblProcurementNature(new TblProcurementNature(pnature));
                                    tblFinancialPowerByRole.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    tblFinancialPowerByRole.setOperator(operator);
                                    tblFinancialPowerByRole.setFpAmount(new BigDecimal(amount));

                                    msg = configPreTenderRuleSrBean.addTblFinancialPowerByRole(tblFinancialPowerByRole);
                                    
                                }
                            }
                       }
                       if (msg.equals("Values Added")) {
                            msg = "Assign Financial Power Configured Successfully";
                            response.sendRedirect("AssignFinPowerView.jsp?msg=" + msg);
                        }

                    }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                 <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 250px; display: block;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            </tr>
                        </table>
                    </td>
                    <td class="contentArea">


                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Assign Financial Power</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; visibility: collapse; float: left;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <div>&nbsp;</div>
                        <div class="overFlowContainer t-align-right">
                            <form action="AssignFinPower.jsp" method="post">

                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                    <tr  >
                                        <th >Select</th>
                                        <th >Financial Year<br />(<span class="mandatory">*</span>)</th>
                                        <th >Budget Type<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Role<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Category<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Method<br />(<span class="mandatory">*</span>)</th>
                                        <th >Operator<br />(<span class="mandatory">*</span>)</th>
                                        <th >Amount<br />(<span class="mandatory">*</span>)</th>
                                     </tr>
                                    <input type="hidden" name="delrow" value="0" id="delrow"/>
                                    <input type="hidden" name="TotRule" id="TotRule" value="1"/>
                                    <input type="hidden" name="introw" value="" id="introw"/>
                                    <tr id="trrule_1">
                                        <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="1" /></td>

                                        <td class="t-align-center"><select name="financialYear1" class="formTxtBox_1" id="financialYear_1">
                                                <%=finYear%></select>
                                        </td>
                                        <td class="t-align-center"><select name="budgetType1" class="formTxtBox_1" id="budgetType_1">
                                                <%=budgetType%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procRole1" class="formTxtBox_1" id="procRole_1">
                                                <%=procRole%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procNature1" class="formTxtBox_1" id="procNature_1">
                                                <%=procNature%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procMethod1" class="formTxtBox_1" id="procMethod_1">
                                                <%=procMethod%></select>
                                        </td>
                                        <td class="t-align-center"><select name="operator1" class="formTxtBox_1" id="operator_1">
                                                <option><</option>
                                                <option>></option>
                                                <option><=</option>
                                                <option>>=</option>
                                                <option>=</option>
                                            </select>
                                        </td>
                                        <td class="t-align-center"><input type="text" name="amount1" class="formTxtBox_1" id="amount_1"/></td>
                                        
                                    </tr>
                                </table>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" >
                                    <tr>
                                        <td align="right" colspan="9" class="t-align-center">
                                            <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                        </td>
                                    </tr>
                                </table>
                            </form>

                        </div>

                        <!--Dashboard Content Part Start-->

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>

    </body>
</html>

