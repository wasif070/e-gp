<%-- 
    Document   : questionDetails
    Created on : Jun 6, 2017, 10:29:20 AM
    Author     : feroz
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Question Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <%
            if ("Edit".equals(request.getParameter("button"))) 
            {
                int questionId = Integer.parseInt(request.getParameter("questionId"));
                String link = "editQuestion.jsp?questionId="+questionId;
                response.sendRedirect(link);
                                              
            }
            else if ("Ok".equals(request.getParameter("button"))) 
            {
                response.sendRedirect("moduleForViewQuestion.jsp");
                                              
            }
        %>
        
    </head>
    <body>
        <%
            QuizQuestionService quizQuestionService = (QuizQuestionService) AppContext.getSpringBean("QuizQuestionService");
            QuizAnswerService quizAnswerService = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                    
            if(request.getParameter("questionId")!=null)
            {
                    List<TblQuizQuestion> tblQuizQuestionEdit = quizQuestionService.findTblQuizQuestionByQid(Integer.parseInt(request.getParameter("questionId")));
                    List<TblQuizAnswer> tblQuizAnswerEdit = quizAnswerService.findTblQuizAnswer(Integer.parseInt(request.getParameter("questionId")));
                    
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.getAllTblQuestionModule();
                    
                    
                    

         %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"><div class="pageHead_1">Question Details</div>
                        <form id="setQ" name="setQ" action="questionDetails.jsp" method="post">
                            <div style="float: left">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td class="ff">Question : </td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="hidden" id="quizQuestion" name="quizQuestion" maxlength="450" value="<%=tblQuizQuestionEdit.get(0).getQuestion()%>"><%out.print(tblQuizQuestionEdit.get(0).getQuestion());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Option A : </td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="hidden" id="optA" name="optA" maxlength="150" value="<%=tblQuizAnswerEdit.get(0).getAnswer()%>"><%out.print(tblQuizAnswerEdit.get(0).getAnswer());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Option B : </td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="hidden" id="optB" name="optB" maxlength="150" value="<%=tblQuizAnswerEdit.get(1).getAnswer()%>"><%out.print(tblQuizAnswerEdit.get(1).getAnswer());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Option C : </td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="hidden" id="optC" name="optC" maxlength="150" value="<%=tblQuizAnswerEdit.get(2).getAnswer()%>"><%out.print(tblQuizAnswerEdit.get(2).getAnswer());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Option D : </td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="hidden" id="optD" name="optD" maxlength="150" value="<%=tblQuizAnswerEdit.get(3).getAnswer()%>"><%out.print(tblQuizAnswerEdit.get(3).getAnswer());%></td>
                            </tr>
                            <tr>
                                <td class="ff">Correct Answer : </td>
                                <td>
                                        <%if(tblQuizAnswerEdit.get(0).isIsCorrect()){out.print("A");}%>
                                        <%if(tblQuizAnswerEdit.get(1).isIsCorrect()){out.print("B");}%>
                                        <%if(tblQuizAnswerEdit.get(2).isIsCorrect()){out.print("C");}%>
                                        <%if(tblQuizAnswerEdit.get(3).isIsCorrect()){out.print("D");}%>
                                    
                                </td>
                            </tr>
                            </tbody>
                        </table>
                            </div>
                            <input type="hidden" name="questionId" id="questionId" value="<%=tblQuizQuestionEdit.get(0).getQuestionId()%>">
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="float: left; padding-left: 8%"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Edit" onclick=""/></span>&nbsp;
                                    <span class="formBtn_1"><input type="submit" name="button" id="button" value="Ok" onclick=""/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
                                    <%}%>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>