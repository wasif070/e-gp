<%-- 
    Document   : Logout
    Created on : Nov 1, 2010, 2:17:27 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.cptu.egp.eps.web.utility.LogoutUtils"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
    HttpSession hs = request.getSession();

    hs.removeAttribute("userId");
    Object objUserType = hs.getAttribute("userTypeId");
    if (objUserType != null && objUserType.equals("1")) {
        hs.removeAttribute("userType");
    }
    hs.removeAttribute("userTypeId");
    hs.removeAttribute("userName");
    LogoutUtils logoutUtils = new LogoutUtils();
    logoutUtils.setLogUserId("0");
    Object objSessionid = hs.getAttribute("sessionId");
    if (objSessionid != null) {
        logoutUtils.updateSessionMaster(Integer.parseInt(objSessionid.toString()));
        hs.removeAttribute("sessionId");
    }
    hs.invalidate();
    response.sendRedirect(request.getContextPath() + "/Index.jsp");
%>
