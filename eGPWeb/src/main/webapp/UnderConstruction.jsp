<%-- 
    Document   : UnderConstruction
    Created on : 20-Jan-2011, 2:45:06 PM
    Author     : yagnesh
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Invalid Page</title>
        <link href="<%= request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->

            <table width="100%" cellpadding="0" cellspacing="10" class="t_space">
                <tr>

                    <td height="400" align="center" valign="middle" style="font-size:16px; color:#6e6e6e; font-weight:bold;">
                        <img src="<%= request.getContextPath()%>/resources/images/Dashboard/Button_Warning.png" width="128" height="128" style="margin-bottom:10px;" /> <br />
                        <legend class="heading-text">Page not found</legend><br />
                        <div class="reason">Sorry, this page could not be found. For query, please contact GPPMD Office. </div></td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>