<%--

--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Terms and Conditions of e-GP System user agreement</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            String lang = null, contenttermscond = null;

            if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
                lang = request.getParameter("lang");
            }else{
                lang = "en_US";
            }

            MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
            List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"termsncond");

            if(!langContentList.isEmpty())
            {
                for(TblMultiLangContent tblMultiLangContent:langContentList)
                {
                    if(tblMultiLangContent.getSubTitle().equals("content_termsncond")){
                        if("bn_IN".equals(lang)){
                           contenttermscond = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                        }else{
                           contenttermscond = new String(tblMultiLangContent.getValue());
                        }
                    }
                }
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start -->
                 <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea-Blogin">
                    <div class="pageHead_1">
                        Terms and Conditions</div></td>  </tr>
                     <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                             
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                             <div style=" padding: 5px; border-top: 1px #ededed solid; border-left: 1px #ededed solid; width:100%; height: 400px; text-align: justify;overflow: scroll">
                                                <b style="color: green;">Terms and Conditions for e-GP System User Agreement</b>
                                                <br/><br/>Electronic Government Procurement (e-GP) system (i.e. https://www.egp.gov.bt) of the Royal Government of Bhutan is developed, owned and operated by Government Procurement and Property Management Division (GPPMD), Ministry of Finance, Royal Government of Bhutan (RGOB) for carrying out the procurement activities of the Procuring Agencies of the RGOB.
                                                <br/><br/>GPPMD  of the RGoB also runs a training server (http://egptraining.egp.gov.bt/) to allow the users to try and learn by themselves all the functionalities of e-GP system through an online mock-up of real transactional e-GP system. Users may try all activities, which is available in real transaction system. None of the activities done in training servers will be taken as real transactions.
                                                <br/><br/>For carrying out the real procurement transactions, users must use the e-GP system at https://www.egp.gov.bt/). 
                                                <br/><br/>User account will be created only when the following terms and conditions of e-GP system user agreement is read and accepted.
                                                <br/><br/><b>TERMS AND CONDITIONS OF E-GP SYSTEM USER AGREEMENT</b>
                                                <br/><br/>For accessing and using this e-GP user services, you shall be deemed to have accepted to be legally bound by these terms and conditions of use and comply with all of the terms and conditions given below, and the guidelines as stipulated in Electronic Government Procurement guidelines:
                                                <br/><br/>Bidder/Supplier/Consultant registration request, Email verification, and Credential documents verification
                                                <br/><br/><b>Email Verification</b>
                                                <br/><br/>Your email will be used as the username for accessing e-GP system. Upon submission of your basic user identity information opened by clicking on the New User Registration button from the home page of e-GP system, you will receive an email in the email address provided by you, an email from e-GP system (https://www.egp.gov.bt ) with a link to click, your unique security key, and other instruction related to your credential documents verification, and payment process. When you click the link provided in your email, an email verification page with a form will open. You need to enter the email, password and the received security key, and press the Submit button. If you correctly enter the information, this process will complete the email verification process successfully.
                                                <br/><br/>Your account will be successfully created, and you will be displayed another form for entering your specific information, uploading digitally scanned mandatory credential documents (scanned documents of Tax Payment No., valid Trade License, CDB No., Company Registration Certificate, CID No. of contact person, must be easily readable).
                                                <br/><br/><b>Credential documents verification (In case of Hard Copy documents) </b>
                                                <br/><br/>Bidders/ Suppliers/Consultants, may visit e-GP Users Registration Desk  in the GPPMD, Ministry of Finance (MoF), RGoB, Deobum Lam, Box 116, Thimphu, Bhutan with the original credential documents used during online registration process or send the documents via registered post or courier service for the post-verification for authenticity. Bidders/ Suppliers/Consultants, must also include envelope return address written or typed, and with required postage stamp. The verification process may take from one day to two weeks.
                                                <br/><br/>After verification of the original credential documents, Bidders/ Suppliers/Consultants get the confirmation email notification of registration and will instantly get full access to secured personal dashboard for user specific functions of the e-GP system as the e-GP system User.
                                                <br/><br/>Procuring agencies, development partners, payment network partners (banks and others), and media will be registered through official communication with GPPMD, MoF, RGoB.
                                                <br/><br/><b>Maintaining confidentiality</b>
                                                <br/><br/>Users are responsible for maintaining the confidentiality of their password and are fully responsible for all activities that occur using your account (e-mail ID and password). e-GP system does not store user passwords, but it will store only the generated irreversible hash value of the password as e-signature. User must notify e-GP Admin of GPPMD via ( Admin email address) of any unauthorized use of your password or any other suspected security breaches. Users must ensure that they appropriately log-out every time from their unattended computers or from the computers you are using in public places. GPPMD is not liable for any loss or damage arising from such compromise of your user account and password.
                                                <br/><br/>The e-GP system allows modifying, updating their user details including password except the login e-mail ID. 
                                                <br/><br/><b>Internet Browser and Users' Computer compatibility</b>
                                                <br/><br/>To access the e-GP system securely, users should use appropriate web browsers and their associated security settings. However because of the rapid development of new browsers and new security measures coming up frequently, users need to update or install new components and configuration settings as and when these come into effect. Current version of e-GP system can be best viewed at all popular latest browsers.
                                                <br/><br/><b>Applicable Time</b>
                                                <br/><br/>The e-GP system shall use the Government Data Centre  server time as the reference time for all time-bound activities of procurement processes. The Government Data Centre is located  at Thimphu TechPark, Babesa.
                                                <br/><br/><b>Proprietary Rights</b>
                                                <br/><br/>This e-GP system is developed and maintained by the GPPMD, Ministry of Finance of the RGoB.
                                                <br/><br/>The materials located on this e-GP web system including the information and software programs (source code) are copyrighted to GPPMD, MoF, RGoB.
                                                <br/><br/><b>Registration Fees</b>
                                                <br/><br/>Bidders/Suppliers/Consultants will be charged a minimal fee of Nu. 500 (Ngultrum Five Hundred) for the user registration, and annually it should be renewed. Nu. 250 (Ngultrum Tow Hundred Fifty) will be charged each year for renewal of their account. For international Bidders/Consultants and Consultants, registration fee is USD $100 (US Dollars One Hundred Only) and annual renewal fee is USD $30 (US Dollars Thirty Only). Users must make sure the required amount is deposited to GPPMD designated account prior to expiry of membership.
                                                <br/><br/>Users may be charged and/or waived specified amount of money for different categories of use including registration, subscription and periodic renewal, additional storage space, transactions, facilities to use specific features/modules of the e-GP system and different services from the operation, maintenance and management entity. GPPMD shall have the rights to set reasonable charges or waiver to promote the use of the e-GP system and sustainability of the system in long run.
                                                <br/><br/><b>Tender Submission</b>
                                                <br/><br/>The Bidders/Suppliers/Consultants are responsible to plan their time sufficient to complete the documents upload, third party transactions like bid security preparation and submission through banks, verify completeness of tender, and final submission of Bidders? documents for the specific tenders. Before final submission, the Bidders/Suppliers/Consultants may upload documents, fill-in required online forms, modify and verify the documents, and complete other activities, part by part. But attempt to submit incomplete tender will not be allowed by the e-GP system.
                                                <br/><br/><b>Payment process</b>
                                                <br/><br/>Until the e-Payment infrastructure is available in Bhutan, the e-GP system will use the service of Financial Institutions. Financial Institutions and other payment service providers will get secured access to the e-GP system with their own dedicated and secured dashboard, from where, they can carry out the financial transactions related to public procurement such as collecting fees and charges, providing guarantees, tracking the guarantees, making payment transactions, and other service fees, etc.
                                                <br/><br/>Bidders/Suppliers/Consultants should pay to bank the required amount of money for the specific purpose of transaction with e-GP system. Financial Institute will collect the charges and fees from bidders crediting the account opened by GPPMD for specific service/transaction in e-GP system, and bank will immediately update the payment information in the e-GP system through the provided bank user access.
                                                <br/><br/>When bank guarantees and securities (bid security, performance security, etc.) are issued by the bank, the same should be immediately updated in the e-GP system.
                                                <br/><br/>When procuring agencies or GPPMD instructs the bank for releasing the guarantees or securities, and deposit in specific procuring agencies or GPPMD accounts, the bank will carry out the transactions, and update the transaction information in the e-GP system.
                                                <br/><br/>The GPPMD shall not be responsible for the transactions made by banks using bank rules with the e-GP system users.
                                                <br/><br/>In case of international Bidders/Suppliers/consultants, payments should be made to the designated bank account opened by GPPMD through bank wire transfer or any other method clearly mentioning the purpose of payment.
                                                <br/><br/>International Bidders/Suppliers/Consultants must communicate with the banks of e-GP online payment network for updating their payment details in e-GP system. Any charges incurred for payment transfer, communication or any currency conversion should be borne by the bidders/applicants/consultants themselves.
                                                
                                                <br/><br/><b>Virus and Integrity of documents</b>
                                                <br/><br/>If the electronic records entered online and files containing the tender/ application/ proposal are corrupt, contain a virus, or are unreadable for any reason, the tender will not be considered.  It is strictly the responsibility of the bidder/applicant/Consultant (national or international) to ensure the integrity, completeness and authenticity of the Tender, and also should comply with the applicable laws of the Kingdom of Bhutan.
                                                <br/><br/><b>External Web References</b>
                                                <br/><br/>GPPMD does not take any responsibility of its availability and authenticity of the external third party web references, links referred in the e-GP system, as GPPMD does not have any control over those websites.
                                                <br/><br/><b>Operation, Maintenance and Management</b>
                                                <br/><br/>The GPPMD reserves the right to outsource operation, maintenance and management services of e-GP data center, e-GP system and other related services to any third party. The users of e-GP system are to be obliging such any agreement with any outsourced firm/company.
                                                <br/><br/><b>Governing Law</b>
                                                <br/><br/>This terms and conditions of use agreement of e-GP system shall all be governed by the laws of the Kingdom Bhutan applicable to agreements made and to be performed in Bhutan.
                                                <br/><br/>Royal Government of Bhutan and GPPMD reserve the right to initiate any legal action against those users violating any of the above mentioned terms & conditions of e-GP system user agreement.
                                                <br/><br/>Changes in e-GP system and terms and conditions of use
                                                <br/><br/>GPPMD shall have the right to modify clauses of the terms and conditions without prior notice.
                                                <br/><br/>GPPMD reserves the right to modify, add, delete and/or change the functions, user interface, contents, and other items in e-GP system at any time without any prior notice. User is responsible to use the updated e-GP system functions and terms and conditions of use.
                                                <br/><br/>Issued on 29 April 2016<br/>&nbsp;
                                            </div>
                        <%-- <%=contenttermscond --%>
                            <!--Page Content End-->
                        </td>
                          <% if (session.getAttribute("userId") == null) { %>
                                    <td width="266">
                                        <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                                    </td>
                         <%}%>
                    </tr>
                </table>
                <!--Middle Content Table End -->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
