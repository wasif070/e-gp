<%--
    Document   : OnlinePmtForNUR
    Created on : Feb 16, 2012, 11:37:56 AM
    Author     : nishit
--%>

<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="resources/js/jQuery/print/jquery.txt"></script>
        <title>New User Registration - Pay Online Registration Fee</title>
        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });

                function printElem(options){
                $('#printTitle').show();
                $('#hideWarning').hide();
                if (document.getElementById("print_area")!=null){
                    $('#print_area').printElement(options);
                }
                $('#printTitle').hide();
                $('#hideWarning').show();
            }
        </script>
    </head>
    <%
                UserRegisterService userRegisterServiceNavigate = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                if (session.getAttribute("payment") == null) {
                    Object[] paymentNavigateObj =
                            userRegisterServiceNavigate.getLoginDetails(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                    if (paymentNavigateObj != null) {
                        List<SPCommonSearchDataMore> listData = csdms.geteGPDataMore("getNUROnlineTranDetail", request.getSession().getAttribute("userId").toString());
                        if (listData != null && !listData.isEmpty()) {
                            String serviceCharge = String.valueOf(Double.parseDouble(listData.get(0).getFieldName7()) * 100 / Double.parseDouble(listData.get(0).getFieldName5()));
                            OnlinePaymentDtBean onpdb = new OnlinePaymentDtBean("",listData.get(0).getFieldName5(), "nur", "0",serviceCharge);
                            onpdb.setOnlineTraId(listData.get(0).getFieldName2());
                            onpdb.setResponse(listData.get(0).getFieldName1()+"PAYMENT_DATETIME :"+ listData.get(0).getFieldName3());
                            onpdb.setShowRespMsg(false);
                            session.setAttribute("payment", onpdb);
                        } else {
                            OnlinePaymentDtBean onpdb = new OnlinePaymentDtBean(String.valueOf(paymentNavigateObj[0]),
                                    XMLReader.getMessage("localTenderRegFee"), "nur", "0",
                                    XMLReader.getMessage("ServiceCharge"));
                            session.setAttribute("payment", onpdb);
                            onpdb = null;
                            paymentNavigateObj = null;
                        }

                    }
                }
    %>
    <body>
        <div class="fixDiv">
            <jsp:include page="resources/common/Top.jsp"/>
            <!--Middle Content Table Start-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                    <td width="266">
                        <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                    </td>
                    <td class="contentArea-Blogin">
                        <div class="pageHead_1">New User Registration - Pay Online
                            Registration Fee</div>

                        <div class="stepWiz_1 t_space" style="width: 100%; height: 18px;">
                            <div style="width:80%; float: left;">
                                <ul>
                                    <li class="sMenu">
                                        Online Payment
                                    </li> >
                                    <li>
                                        <a href="<%=request.getContextPath()%>/LoginSrBean?funName=cmpltVeriPrcs">Complete Registration Process</a>
                                    </li>
                                </ul>
                            </div></div>
                        <div class="contentArea_1">

                            <%
                                        OnlinePaymentDtBean opdb = (OnlinePaymentDtBean) session.getAttribute("payment");
                                        if (opdb != null) {
                                            System.out.println("card number : "+opdb.getCardNumber());
                                            if (opdb.getResponse() != null && ((opdb.getResponse().indexOf("000") > 0
                                                    && opdb.getResponse().indexOf("OK") > 0)
                                                    || opdb.getResponse().indexOf("<txn_status>ACCEPTED</txn_status>") > 0
                                                    )) {
                            %>
                            <jsp:include page="resources/common/OnlinePaymentSuc.jsp" ></jsp:include>
                            <%} else {%>
                            <jsp:include page="resources/common/OnlinePayment.jsp" ></jsp:include>
                            <%}
                                        }%>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
