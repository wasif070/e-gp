<%--
    Document   : UserStatus
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">        
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP: Successful Registration</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />        
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <%
                    if ("3".equals(request.getParameter("returnValue"))) {
                        response.sendRedirect("Index.jsp?returnValue=3");
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <%
                            String msg = "";
                            boolean isGreen = false;
                            boolean isNotice = false;
                            if ("r".equals(request.getParameter("status"))) {
                                msg = "Your Profile has been Rejected.";
                            } else if ("p".equals(request.getParameter("status"))) {
                                msg = "Your profile Approval is pending";
                                isNotice = true;
                            } else if ("b".equals(request.getParameter("status"))) {
                                msg = "Your Profile has been BlackListed.";
                            } else if ("sus".equals(request.getParameter("status"))) {
                                msg = "Your Profile has been Suspended by Company Admin.";
                            } else if ("succ".equals(request.getParameter("status"))) {
                                Date date = new Date();
                                if (!"".equals(XMLReader.getMessage("DocSubmitLimit").trim())) {
                                    date.setDate(date.getDate() + Integer.parseInt(XMLReader.getMessage("DocSubmitLimit")));
                                } else {
                                    date.setDate(date.getDate() + 14);
                                }
                                msg = "Thank you for Registering on Electronic Government Procurement (e-GP) System, Royal Government of Bhutan.<br/>Your Registration details will be verified by the GPPMD Office. On approval or modification of your profile, you will be notified by <br/> e-mail / SMS Alert.";
                                isGreen = true;
                            }
                            else if ("dis".equals(request.getParameter("status"))) {
                                msg = "Your Profile has been Deactivated by the System.";
                            }
                %>
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0" >
                    <tr valign="top">
                        
                        <td class="contentArea-Blogin">
                            <div class="t_space"><div class="responseMsg <%if (isGreen) {%>success<%} else if (isNotice) {%>notice<%} else {%>error<%}%>Msg"><%=msg%></div></div>
                            &nbsp;&nbsp;&nbsp;
                            <% if ("succ".equals(request.getParameter("status"))) {%>
                            <table class="contactUs_border"  width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="contactUs_header">
				Address of Government Procurement and Property Management Division (GPPMD) Office</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="contactUs_border"  width="100%" cellspacing="10" cellpadding="0" border="0">

                                            <tr>
                                                <td class="t-align-left">
                                                    &nbsp;Government Procurement and Property Management Devision (GPPMD)<br/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="t-align-left" >
                                                    &nbsp;Ministry of Finance,<br/>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="t-align-left" >
                                                    &nbsp;Royal Government of Bhutan<br/>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="t-align-left" >
                                                    &nbsp;Box 116<br/>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="t-align-left" >
                                                    &nbsp;Thimphu, Bhutan<br/>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="t-align-left" >
                                                    &nbsp;Bhutan
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>                            
                            <br/>
                            <%}%>
                            <center><a href="<%= request.getContextPath()%>/Index.jsp"  class="anchorLink">Go Back To Home Page</a></center>
                        </td>
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
