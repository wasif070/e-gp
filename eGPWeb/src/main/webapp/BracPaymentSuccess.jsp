<%-- 
    Document   : BracPaymentSuccess
    Created on : Jun 19, 2012, 6:01:54 PM
    Author     : shreyansh.shah
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.io.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

            String stripAddress = request.getHeader("X-FORWARDED-FOR");

            OnlinePaymentDtBean opdb = (OnlinePaymentDtBean)session.getAttribute("payment");
            if (stripAddress == null) {
                stripAddress = request.getRemoteAddr();
            }

            if (request.getParameter("trans_id") != null) {

            if (request.getParameter("trans_id") != null) {
                String requestFrom ="";
                String strTrnsID = request.getParameter("trans_id");

                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> listData = csdms.geteGPDataMore("getRegVASPaymentData", request.getParameter("trans_id"));
                if (listData != null && !listData.isEmpty() && listData.get(0).getFieldName11() != null && (listData.get(0).getFieldName11().equalsIgnoreCase("fromvas"))) {
                     OnlinePaymentDtBean onlinePaymentDtBean = new OnlinePaymentDtBean(listData.get(0).getFieldName1(), listData.get(0).getFieldName7(), "fromvas", "0", listData.get(0).getFieldName8());
                     onlinePaymentDtBean.setOnlineTraId(strTrnsID);
                     onlinePaymentDtBean.setOnlineId(Integer.parseInt(listData.get(0).getFieldName12()));
                     onlinePaymentDtBean.setPaymentGateway(listData.get(0).getFieldName13());
                     request.setAttribute("vasPayment", onlinePaymentDtBean);
                     requestFrom ="&reqFrom=regVasFeePayment";
                    RequestDispatcher dispatcher = request.getRequestDispatcher(request.getContextPath()+"/OnlinePaymentServlet?action=update"+requestFrom+"&barkResp="+request.getParameter("encryptedReceiptPay"));
                    if (dispatcher != null){
                        dispatcher.forward(request, response);
                    }
                }else{
                       response.sendRedirect(request.getContextPath()+"/OnlinePaymentServlet?action=update"+"&barkResp="+request.getParameter("encryptedReceiptPay"));
                }
            }
    }
%>
