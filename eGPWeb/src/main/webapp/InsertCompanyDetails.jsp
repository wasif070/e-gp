<%--
    Document   : InsertCompanyDetails
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserPrefService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.TempBiddingPermission"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.model.table.TblTempBiddingPermission,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserPrefrence,com.cptu.egp.eps.model.table.TblEmailPrefMaster,com.cptu.egp.eps.model.table.TblSmsPrefMaster" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
<jsp:useBean id="tempBiddingPermission" class="com.cptu.egp.eps.web.databean.TempBiddingPermission"/>
<jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head></head>
    <body>
        <%
            tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
            //System.out.print("**************post11111111***********");
            //System.out.print(request.getParameter("hdnsaveNEdit"));
            TblTempCompanyMaster dtBean = tendererMasterSrBean.getCompanyDetails(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
            //List<TblTempBiddingPermission> dtBeanBiddingPermission = tendererMasterSrBean.getBiddingPermission(Integer.parseInt(session.getAttribute("userId").toString()));
            List<TempBiddingPermission> tempBiddingPermissionList = new ArrayList<TempBiddingPermission>();
            
            if ("Save".equals(request.getParameter("hdnsaveNEdit"))) {
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("regOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setRegOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("regOffUpjilla"))) {
                    tempCompanyMasterDtBean.setRegOffUpjilla("");
                }
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("corpOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setCorpOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("corpOffUpjilla"))) {
                    tempCompanyMasterDtBean.setCorpOffUpjilla("");
                }
                //if(tempCompanyMasterDtBean.getCompanyRegNumber()!=null||tempCompanyMasterDtBean.getTradeLicenseNumber()!=null){
                //UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                //if(userRegisterService.companyRegNoCheck(tempCompanyMasterDtBean.getCompanyRegNumber(), 0).isEmpty()){
                //Removed Unique company RegNo Check
                //System.out.print("**************post***********");

                //TempBiddingPermission bidpermission = new TempBiddingPermission();
                if (request.getParameter("ProcurementType1") != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setProcurementCategory("Goods");
                    bidpermission.setWorkType("");
                    bidpermission.setWorkCategroy("");
                    tempBiddingPermissionList.add(bidpermission);
                }
                for (int i = 0; i <= 3; i++) {
                    if (request.getParameter("large" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Large");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }

                    if (request.getParameter("medium" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Medium");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }

                    if (request.getParameter("small" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Small");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                }

                //registered
                if (request.getParameter("Registered") != null) {
                    boolean isLarge = false, isMedium = false, isSmall = false;
                    for (int i = 0; i < tempBiddingPermissionList.size(); i++) {
                        if (!"Goods".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString()) && !"Services".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString())) {
                            if ("Large".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isLarge = true;
                            }
                            if ("Medium".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isMedium = true;
                            }
                            if ("Small".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isSmall = true;
                            }
                        }
                    }

                    if (!isLarge) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Large");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);

                    }
                    if (!isMedium) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Medium");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                    if (!isSmall) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Small");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                }

                for (int i = 0; i <= 1; i++) {
                    if (request.getParameter("servicetype" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkType(request.getParameter("servicetype" + i));
                        bidpermission.setProcurementCategory("Services");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                }

                //sms / email alert
                UserPrefService userPrefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
                TblUserPrefrence tblUserPreference = new TblUserPrefrence();

                if (request.getParameter("Alert") != null) {
                    String[] alertValue = request.getParameterValues("Alert");
                    if (alertValue[0].equalsIgnoreCase("EmailAlert")) {
                        List<Object[]> emailList = userPrefService.findEmail("2");
                        Iterator itr = emailList.iterator();
                        String email = "";
                        while (itr.hasNext()) {
                            TblEmailPrefMaster emailprefmaster = (TblEmailPrefMaster) itr.next();
                            email += emailprefmaster.getEpid() + ",";
                        }
                        if (email != null && !"".equals(email)) {
                            email = email.substring(0, email.length() - 1);
                        }
                        tblUserPreference.setEmailAlert(email);
                    }
                    if (alertValue[0].equalsIgnoreCase("SMSAlert")) {
                        List<Object[]> smsList = userPrefService.findSms("2");
                        String sms = "";
                        Iterator itr = smsList.iterator();
                        while (itr.hasNext()) {
                            TblSmsPrefMaster smsprefmaster = (TblSmsPrefMaster) itr.next();
                            sms += smsprefmaster.getSpid() + ",";
                        }
                        if (sms != null && !"".equals(sms)) {
                            sms = sms.substring(0, sms.length() - 1);
                        }
                        tblUserPreference.setSmsAlert(sms);
                    }
                    if (alertValue[0].equalsIgnoreCase("BothAlert")) {
                        List<Object[]> emailList = userPrefService.findEmail("2");
                        List<Object[]> smsList = userPrefService.findSms("2");
                        Iterator itr = emailList.iterator();
                        String email = "";
                        String sms = "";
                        while (itr.hasNext()) {
                            TblEmailPrefMaster emailprefmaster = (TblEmailPrefMaster) itr.next();
                            email += emailprefmaster.getEpid() + ",";
                        }
                        if (email != null && !"".equals(email)) {
                            email = email.substring(0, email.length() - 1);
                        }
                        itr = smsList.iterator();
                        while (itr.hasNext()) {
                            TblSmsPrefMaster smsprefmaster = (TblSmsPrefMaster) itr.next();
                            sms += smsprefmaster.getSpid() + ",";
                        }
                        if (sms != null && !"".equals(sms)) {
                            sms = sms.substring(0, sms.length() - 1);
                        }
                        tblUserPreference.setEmailAlert(email);
                        tblUserPreference.setSmsAlert(sms);
                    }
                }

                if (true) {
                    //response.sendRedirect("CompanyDetails.jsp");
                    if (tendererMasterSrBean.registerCompanyDetail(tempCompanyMasterDtBean, tempBiddingPermissionList, tblUserPreference, Integer.parseInt(session.getAttribute("userId").toString()))) {//Integer.parseInt(session.getAttribute("userId").toString())
                        response.sendRedirect("PersonalDetails.jsp");
                    } else {
                        response.sendRedirect("CompanyDetails.jsp?msg=error");
                    }

                } else {
                    response.sendRedirect("CompanyDetails.jsp?cpr=y");
                }
                //}else{
                //   response.sendRedirect("CompanyDetails.jsp");
                //}
            } else if ("Update".equals(request.getParameter("hdnsaveNEdit"))) {
                tempCompanyMasterDtBean.setCompanyId(dtBean.getCompanyId());
                tempCompanyMasterDtBean.setUserId(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
                           
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("regOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setRegOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("regOffUpjilla"))) {
                    tempCompanyMasterDtBean.setRegOffUpjilla("");
                }
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("corpOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setCorpOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("corpOffUpjilla"))) {
                    tempCompanyMasterDtBean.setCorpOffUpjilla("");
                }
                
                out.print(request.getParameter("userPrefId")); 
                //List<TempBiddingPermission> tempBiddingPermissionList = new ArrayList<TempBiddingPermission>();
                if (request.getParameter("ProcurementType1") != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setProcurementCategory("Goods");
                    bidpermission.setWorkType("");
                    bidpermission.setWorkCategroy("");
                    tempBiddingPermissionList.add(bidpermission);
                }
                for (int i = 0; i <= 3; i++) {
                    if (request.getParameter("large" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Large");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }

                    if (request.getParameter("medium" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Medium");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }

                    if (request.getParameter("small" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W" + (i + 1));
                        bidpermission.setWorkType("Small");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                }

                //registered
                if (request.getParameter("Registered") != null) {
                    boolean isLarge = false, isMedium = false, isSmall = false;
                    for (int i = 0; i < tempBiddingPermissionList.size(); i++) {
                        if (!"Goods".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString()) && !"Services".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString())) {
                            if ("Large".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isLarge = true;
                            }
                            if ("Medium".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isMedium = true;
                            }
                            if ("Small".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                    && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                                isSmall = true;
                            }
                        }

                    }

                    if (!isLarge) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Large");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);

                    }
                    if (!isMedium) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Medium");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                    if (!isSmall) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();
                        bidpermission.setWorkCategroy("W2");
                        bidpermission.setWorkType("Small");
                        bidpermission.setProcurementCategory("Works");
                        tempBiddingPermissionList.add(bidpermission);
                    }
                }

                for (int i = 0; i <= 1; i++) {
                    if (request.getParameter("servicetype" + i) != null) {
                        TempBiddingPermission bidpermission = new TempBiddingPermission();

                        bidpermission.setWorkType(request.getParameter("servicetype" + i));
                        bidpermission.setProcurementCategory("Services");
                        tempBiddingPermissionList.add(bidpermission);
                    }

                }

                //sms / email alert
                UserPrefService userPrefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
                TblUserPrefrence tblUserPreference = new TblUserPrefrence();

                if (request.getParameter("Alert") != null) {
                    
                    String[] values = request.getParameterValues("Alert");
                    if (values[0].equalsIgnoreCase("EmailAlert")) {
                        List<Object[]> emailList = userPrefService.findEmail("2");
                        Iterator itr = emailList.iterator();
                        String email = "";
                        while (itr.hasNext()) {
                            TblEmailPrefMaster emailprefmaster = (TblEmailPrefMaster) itr.next();
                            email += emailprefmaster.getEpid() + ",";
                        }
                        if (email != null && !"".equals(email)) {
                            email = email.substring(0, email.length() - 1);
                        }
                        tblUserPreference.setEmailAlert(email);
                    } else if (values[0].equalsIgnoreCase("SMSAlert")) {
                        List<Object[]> smsList = userPrefService.findSms("2");
                        String sms = "";
                        Iterator itr = smsList.iterator();
                        while (itr.hasNext()) {
                            TblSmsPrefMaster smsprefmaster = (TblSmsPrefMaster) itr.next();
                            sms += smsprefmaster.getSpid() + ",";
                        }
                        if (sms != null && !"".equals(sms)) {
                            sms = sms.substring(0, sms.length() - 1);
                        }
                        tblUserPreference.setSmsAlert(sms);
                    } else if (values[0].equalsIgnoreCase("BothAlert")) {
                        List<Object[]> emailList = userPrefService.findEmail("2");
                        List<Object[]> smsList = userPrefService.findSms("2");
                        Iterator itr = emailList.iterator();
                        String email = "";
                        String sms = "";
                        while (itr.hasNext()) {
                            TblEmailPrefMaster emailprefmaster = (TblEmailPrefMaster) itr.next();
                            email += emailprefmaster.getEpid() + ",";
                        }
                        if (email != null && !"".equals(email)) {
                            email = email.substring(0, email.length() - 1);
                        }
                        itr = smsList.iterator();
                        while (itr.hasNext()) {
                            TblSmsPrefMaster smsprefmaster = (TblSmsPrefMaster) itr.next();
                            sms += smsprefmaster.getSpid() + ",";
                        }
                        if (sms != null && !"".equals(sms)) {
                            sms = sms.substring(0, sms.length() - 1);
                        }
                        tblUserPreference.setEmailAlert(email);
                        tblUserPreference.setSmsAlert(sms);
                    }
                    tblUserPreference.setUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                    tblUserPreference.setUserPrefId(Integer.parseInt(request.getParameter("userPrefId"))); 
                }
                 

                String qstring = null;
                if (tendererMasterSrBean.updateCompanyDetails(tempCompanyMasterDtBean, tempBiddingPermissionList, tblUserPreference)) {
                    qstring = "?msg=s";
                } else {
                    qstring = "?msg=fail";
                }
                response.sendRedirect("CompanyDetails.jsp" + qstring);
            }
            if (dtBean != null) {
                dtBean = null;
            }
            if (tempCompanyMasterDtBean != null) {
                tempCompanyMasterDtBean = null;
            }
            if (tendererMasterSrBean != null) {
                tendererMasterSrBean = null;
            }
        %>
    </body>
</html>

