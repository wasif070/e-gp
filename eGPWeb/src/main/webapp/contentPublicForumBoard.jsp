<%--
    Document   : AdminDashboard
    Created on : Oct 25, 2010, 10:51:28 AM
    Author     : parag
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Public Forums</title>
       <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
       <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="resources/common/AfterLoginTop.jsp"  %>
            <!--Middle Content Table Start-->
            

            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <%@include file="LeftPanelForForum.jsp"  %>
                
    <!--Dashboard Content Part End-->
                   <td class="contentArea_1" style="vertical-align: top; height: 400px;" align="center" valign="middle">
                            <h1 style="margin-top: 20%;">Welcome  to e-GP System</h1>
                        </td>
          </tr>
            
            </table>

        </div>
  <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
    </body>

</html>
