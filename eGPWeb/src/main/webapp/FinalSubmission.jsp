<%--
    Document   : FinalSubmission
    Created on : Dec 1, 2010, 3:13:00 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTempTendererMaster"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean"%>
<jsp:useBean id="tempCompanyDocumentsSrBean" class="com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean"/>
<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
<%@page import="com.cptu.egp.eps.web.databean.JvcaCompListDtBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyDocuments"%>
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">        
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <title>e-GP Bidder Registration : Final Submission</title>
    </head>
    <body>
        <%
            TblTempTendererMaster dtBean = tendererMasterSrBean.getTendererDetails(Integer.parseInt(session.getAttribute("userId").toString()));
            tempCompanyDocumentsSrBean.setLogUserId(session.getAttribute("userId").toString());            
            tempCompanyDocumentsSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                    if ("Click here for Final Submission".equalsIgnoreCase(request.getParameter("next"))) {}else{
                %>
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <%
                    }
                %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="content-table-all">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            <%--<div class="txt_1" style="width: 70px; height: 25px; padding-right: 10px; float: right; line-height: 28px; font-weight:bold;"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" ><img src="<%=request.getContextPath()%>/resources/images/Dashboard/helpIcon.png" style="margin-right: 5px; float: left;color: #333;font-size:35px; " />
                            <span style="font-size:15px;">Help</span></a></div>--%>
                            <div class="pageHead_1">
                                Bidder Registration - Final Submission
                                <span class="c-alignment-right"><a href="SupportingDocuments.jsp" class="action-button-goback">Go Back</a></span>                                
                            </div>
                           
                            <%
                                    int tendererId = Integer.parseInt(tempCompanyDocumentsSrBean.checkJVCA(session.getAttribute("userId").toString()));
                                    java.util.List<TblTempCompanyDocuments> documentsList = new java.util.ArrayList<TblTempCompanyDocuments>();
                                    documentsList = tempCompanyDocumentsSrBean.getTblTempCompanyDocumentses(tendererId);
                                    
                                    boolean FileExists = true;
                                            String FileNotExistsString = "";
                                            for(TblTempCompanyDocuments documents :documentsList){
                                                if(!briefcaseDoc.isRegisDocumentExistsOrNot(documents.getDocumentName(), session.getAttribute("userId").toString()))
                                                    {
                                                FileNotExistsString = FileNotExistsString + " " + documents.getDocumentName() + ",";
                                                if(FileExists){FileExists = false;}
                                                }
                                            }
                                if(FileExists){
                                tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
                                String msg=null;
                                if ("Click here for Final Submission".equalsIgnoreCase(request.getParameter("next"))) {
                                    UserRegisterService urs = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                    String fUserId = request.getSession().getAttribute("userId").toString();
                                    boolean isPayMade = true;
                                    boolean isPayVerified = false;
                                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                                    if(!"media".equalsIgnoreCase(commonService.getRegType(fUserId))){
                                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                         List<SPCommonSearchDataMore> payChkList = commonSearchDataMoreService.geteGPDataMore("isRegFeePaidByTenderer",fUserId);
                                         if(payChkList!=null && (!payChkList.isEmpty())){
                                            isPayVerified = payChkList.get(0).getFieldName1().equalsIgnoreCase("yes");
                                         }else{
                                             isPayMade = false;
                                         }
                                    }
                                    if(urs.isJVCA(request.getSession().getAttribute("userId").toString()).equalsIgnoreCase("yes")){
                                         java.util.List<JvcaCompListDtBean> compListDtBeans = tendererMasterSrBean.getCompanyJointVentures(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                         int leadCnt = 0;
                                            int otherCnt = 0;
                                            for (JvcaCompListDtBean jvcaCompListDtBean : compListDtBeans) {
                                                if (jvcaCompListDtBean.getJvcaRole().equals("lead")) {
                                                    leadCnt++;
                                                } else {
                                                    otherCnt++;
                                                }
                                            }
                                            if (!(leadCnt == 1 && otherCnt >= 1)) {
                                            %>      
                                                <br/><div class="responseMsg errorMsg">JVCA details must contain lead and secondary partner</div><br/>
                                            <%
                                            }else{                                               
                                                if(isPayMade || true){ //alwayes ture to eliminate payment process
                                                    if(isPayVerified || true){//true is added to not consider payverify
                                                        com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk cmc = tempCompanyDocumentsSrBean.addUser(Integer.parseInt((fUserId)), "FinalSubmission", "","");
                                                        if(cmc.getFlag()){
                                                             //response.sendRedirect("UserStatus.jsp?status=succ");
                                                            response.sendRedirect("LoginSrBean?funName=userstatus");
                                                        }else{
                                                            out.print("<br/><div class='responseMsg errorMsg'>"+cmc.getMsg()+"</div><br/>");
                                                        }
                                                    }else{
                                                        out.print("<br/><div class='responseMsg errorMsg'>Your registration fee is yet not verified by the Bank. You can submit your profile only if your registration fee is verified</div><br/>");
                                                    }
                                               }else{
                                                   out.print("<br/><div class='responseMsg errorMsg'>You need to pay registration fee first to submit your profile</div><br/>");
                                               }
                                            }                                        
                                    }else{
                                        if(isPayMade || true){//alwayes ture to eliminate payment process
                                            if(isPayVerified || true){//true is added to not consider payverify
                                                com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk cmc = tempCompanyDocumentsSrBean.addUser(Integer.parseInt((fUserId)), "FinalSubmission", dtBean.getTitle(), dtBean.getFirstName());
                                                if(cmc.getFlag()){
                                                     //response.sendRedirect("UserStatus.jsp?status=succ");
                                                    response.sendRedirect("LoginSrBean?funName=userstatus");
                                                }else{
                                                    out.print("<br/><div class='responseMsg errorMsg'>"+cmc.getMsg()+"</div><br/>");
                                                }
                                            }else{
                                                out.print("<br/><div class='responseMsg errorMsg'>Your registration fee is yet not verified by the Bank. You can submit your profile only if your registration fee is verified</div><br/>");
                                            }
                                       }else{
                                           out.print("<br/><div class='responseMsg errorMsg'>You need to pay registration fee first to submit your profile</div><br/>");
                                       }
                                   }
                                }
                            %>
                            <form  id="frmUploadDoc" method="post" action="FinalSubmission.jsp" >
                                <div class="atxt_1">
                                    
                                    <div style="padding-top:20px; text-align:justify;">Dear <%out.print(dtBean.getTitle());%> <%out.print(dtBean.getFirstName());%>,<br/><br/>

                                    Please note that once you complete final submission of your profile, it will be sent to e-GP Content Admin for verification. Once the final submission of a profile is completed, modification in profile won't be allowed.

<!--                                    <br/><br/>You need to complete final submission of your profile within < %=XMLReader.getMessage("FinalSubComplete")%> from the date of e-mail ID verification.-->
                                    </div>
                                    <div class="t-align-center t_space" style="margin-top:20px;">
                                        <label class="formBtn_1"> <input type="submit" name="next" id="btnNext" value="Click here for Final Submission" /></label>
                                    </div>
                                </div>
                            </form>
                            <%}
                              else{
                                      FileNotExistsString = FileNotExistsString.substring(0, FileNotExistsString.length()-1);
                             %>
                             <div class="t-align-center t_space" style="margin-top:20px;">
                                    <span style="color: red; font-size: large"><b>Problem occurred during file upload.</b></span> </br>
                                    <b>Try to remove and again upload these files: </b> </br>
                                    <b><%=FileNotExistsString%></b>
                             </div>
                             <%
                              }
                            %>
                            <!--Page Content End-->
                        </td>
                             <td width="266" class="td-background">
                             <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
