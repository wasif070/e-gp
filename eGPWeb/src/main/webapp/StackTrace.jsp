<%--
    Document   : StackTrace
    Created on : Dec 30, 2010, 4:55:39 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP</title>
        <link href="<%= request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <fieldset class="sessionTimeOut" style="height:150px;">
                                <legend class="heading-text"><img src="<%= request.getContextPath() %>/resources/images/Warning.png" width="16" height="16" /> &nbsp;Dear User
                                </legend>
                                <div class="reason">You have encountered a problem. Please report an issue to Government Procurement and Property Management Division (GPPMD).</div>

                                <div class="t_space" style="line-height:18px;">
                                    Thanks for your cooperation.
                                    <br />
                                    Government Procurement and Property Management Division (GPPMD)<br/>
                                    Department of National Properties<br/>
                                    Ministry of Finance<br/>
                                </div>
                            </fieldset>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>