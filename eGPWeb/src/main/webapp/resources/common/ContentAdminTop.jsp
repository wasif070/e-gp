<%-- 
    Document   : ContentAdminTop
    Created on : Nov 5, 2010, 11:40 PM
    Author     : taher
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Date,java.text.DateFormat"%>
<form id="frmAfterLoginTop" name="frmAfterLoginTop" method="POST">
    <input type="hidden" name="hdnUserType" id="hdnUserType">
</form>
<script>
    function redirect(type) {
        document.getElementById("hdnUserType").value = type;
        document.forms[0].action = "../admin/AdminDashboard.jsp";
        document.forms[0].submit();
    }
</script>
<%!    DateFormat formatter; //Formats the date displayed
    public String lastdate = "1"; //String to hold date displayed
    Date currentDate; //Used to get date to display
%>
<%
            formatter = DateFormat.getDateTimeInstance(DateFormat.FULL,
                    DateFormat.MEDIUM);
            currentDate = new Date();
            lastdate = formatter.format(currentDate);
%>
<!--Dashboard Header Start-->

<link REL="SHORTCUT ICON" HREF="<%=request.getContextPath()%>/resources/favicon2.ico">
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<div class="topHeader">
    <table width="100%" cellspacing="0">
        <tr valign="top">
            <td><div id="ddtopmenubar" class="topMenu_1">
                    <ul>
                        <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
                        <li><a href="CompanyVerification.jsp"><img src="../resources/images/Dashboard/configIcn.png" />Company Verification</a></li>
                        <li><a href="ListSTD.jsp" rel="ddsubmenu1"><img src="../resources/images/Dashboard/tenderIcn.png" />Content Verification</a></li>
                        <li><a href="#" rel="contentsubmenu"><img src="../resources/images/Dashboard/docLibIcn.png" />Reports</a></li><!--APP Serach(Rishita)-->
                        <li><a href="ManageEmployee.jsp" rel="ddsubmenu2"><img src="../resources/images/Dashboard/committeeIcn.png" />My Profile</a></li>
                        <li><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank"><img src="../resources/images/Dashboard/helpIcn.png" />Help</a></li>
                    </ul>
                </div>
                <script type="text/javascript">
                    ddlevelsmenu.setup("ddtopmenubar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
                </script>
                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                <ul id="ddsubmenu1" class="ddsubmenustyle">
                    <li><a href="#">Forum content</a></li>
                    <li><a href="#">Ask Proc. content</a></li>
                </ul>                
                <!--Multilevel Menu 1 (rel="ddsubmenu2")-->                
                <table width="100%" cellspacing="6" class="loginInfoBar">
                    <tr>
                        <td align="left" id="lblTime" style="font-weight: bold"></td>
                        <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
                        <%
                        StringBuilder userName = new StringBuilder();
                        if(session.getAttribute("userName")==null){
                                    if (session.getAttribute("userId") != null) {
                                        int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                        int userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                        userName.append(commonService.getUserName(userId, userTypeId));
                                        session.setAttribute("userName", userName.toString());
                                    }
                                    else {
                                        response.sendRedirect(request.getContextPath() + "/Index.jsp");
                                    }
                            }
                        else{
                            userName.append(session.getAttribute("userName"));
                            }
                           String logoutPage = request.getContextPath() + "/Logout.jsp";
                        %>
                        <td align="right"><img alt="" src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> <%=userName%>   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href=<%=logoutPage%> title="Logout">Logout</a>
                            <input type="hidden" id="curDate" value="<%=lastdate%>"/></td>
                    </tr>
                </table></td>
            <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    Start();
</script>
<!--Dashboard Header End-->
