<%--
    Document   : Top
    Created on : Oct 21, 2010, 6:05:10 PM
    Author     : yanki
--%>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent" %>
<%@page import="java.lang.String" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    AppMessage appMessage = new AppMessage();
    if (application.getRealPath("/") != null) {
        appMessage.setAppPath(application.getRealPath("/").toString());
    }

    if (request.getRequestURL().toString().contains("JSInstruction.jsp")) {
    } else {
%>
<noscript>
<meta http-equiv="refresh" content="0; URL=<%= request.getContextPath()%>/JSInstruction.jsp">
</noscript>
<%
    }
%>
<% String contextPath = request.getContextPath();%>
<script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
<script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<!--Header Table-->
<link REL="SHORTCUT ICON" HREF="<%=request.getContextPath()%>/resources/favicon2.ico">
<%!
    DateFormat formatter; //Formats the date displayed
    public String lastdate = "1"; //String to hold date displayed
    Date currentDate; //Used to get date to display
%>
<%
    formatter = DateFormat.getDateTimeInstance(DateFormat.FULL,
            DateFormat.MEDIUM);
    currentDate = new Date();
    // System.out.println(" currentDate  "+currentDate );
    lastdate = formatter.format(currentDate);
    //System.out.println(" lastdate "+lastdate);
    //out.println(request.getHeader("referer"));
%>
<script type="text/javascript">
    var $j = jQuery.noConflict();
    var langType = 'en_US';
    $j(function () {
        $j("#lang-dialog-message").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                Ok: function () {
                    $j(this).dialog("close");
                }
            }
        });

        $j("#lang-dialog-message").bind("dialogclose", function (event, ui) {
            updateLanguage(langType);
        });

        $j("#tender-dialog-message").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                Ok: function () {
                    $j(this).dialog("close");
                }
            }
        });

        $j("#tender-dialog-message").bind("dialogclose", function (event, ui) {

            location.href = window.location.protocol + '//' + window.location.host + '/resources/common/SearchTenderOffline.jsp';
        });
        $j("#awarded-dialog-message").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                Ok: function () {
                    $j(this).dialog("close");
                }
            }
        });

        $j("#awarded-dialog-message").bind("dialogclose", function (event, ui) {

            location.href = window.location.protocol + '//' + window.location.host + '/resources/common/SearchAwardedContractOffline.jsp';
        });
    });

    function changelanguage(language)
    {
        langType = language;
        if (language == "bn_IN")
        {
            $j("#lang-dialog-message").dialog("open");
        } else
        {
            updateLanguage(langType);
        }
    }

    function popupMsg()
    {
        var flag = alert("Dear User, \n\nThis is applicable for manual tender. To view details of tender notice \ninformation, " +
                "please  click on 'View' link. You can see same or more \noff-line tender notice/information from GPPMD web site \n(www.pppd.gov.bt). " +
                "If you need  to buy tender document, please \ncontact with concerned PA Office for off-line tendering.");
        return flag;
    }

    function awardedMessage()
    {
        // langType = language;
        // if(language=="bn_IN")
        // {
        $j("#awarded-dialog-message").dialog("open");
        // }
        // else
        // {
        //     updateLanguage(langType);
        //  }
    }
    function tenderMessage()
    {
        // langType = language;
        // if(language=="bn_IN")
        // {
        $j("#tender-dialog-message").dialog("open");
        // }
        // else
        // {
        //     updateLanguage(langType);
        //  }
    }

    function updateLanguage(langType)
    {
        // alert(document.URL);
        if (document.URL.indexOf('?lang=') > 0)
        {
            location.href = document.URL.substring(0, document.URL.indexOf('?lang=')) + '?lang=en_US&langForMenu=' + langType
        } else if (document.URL.indexOf("Index.jsp") == -1)
        {
            //location.href = document.URL+'Index.jsp?lang='+langType;
            location.href = window.location.protocol + '//' + window.location.host + '/Index.jsp?lang=en_US&langForMenu=' + langType;
        } else// if(document.URL.indexOf("Index.jsp") == -1)
        {
            location.href = document.URL + '?lang=en_US&langForMenu=' + langType;
        }
        //alert(location.href);

    <%--
          String repURL = "";
          String queryStr = "";

                if(request.getQueryString()== null){ %>
                 location.href = "<%=request.getRequestURL() %>?lang="+langType;
             < %} else if(request.getQueryString()!=null && request.getParameter("lang")!=null){ %>
                 <%
                    if("en_US".equalsIgnoreCase(request.getParameter("lang").toString())){
                        queryStr = request.getQueryString().toString();
                        repURL = queryStr.replace("lang=en_US", "lang=bn_IN");
                 %>
                        location.href = "<%=request.getRequestURL()+"?"+repURL%>";
                 <%
                    }else if("bn_IN".equalsIgnoreCase(request.getParameter("lang").toString())){
                        queryStr = request.getQueryString().toString();
                        repURL = queryStr.replace("lang=bn_IN", "lang=en_US");

                 %>
                        location.href = "<%=request.getRequestURL()+"?"+repURL%>";
                 <%
                    }
                 %>

            <% }else{ %>
                 location.href = "<%=request.getRequestURL()+"?"+request.getQueryString()%>&lang="+langType;
            <% } --%>
    }


    function navigateSearch()
    {
        var url = ""
        var kword = $.trim(document.getElementById('txtKeyword').value);
        if (kword == '' || kword == 'Type your Keyword') {
            //$j("#msg").html('Please Enter Search Keyword').fadeIn(2000);
            //document.getElementById('txtKeyword').value = "";
        } else
        {
            var url = "<%=request.getContextPath()%>";
            var crntTab = $j('#crntTab').val();

            if (crntTab == "tenderTab")
            {
                url += '/resources/common/TenderListing.jsp?h=t';
            } else if (crntTab == "appTab")
                url += '/resources/common/AppDetails.jsp';
            else if (crntTab == "noaTab")
                url += '/resources/common/ContractListing.jsp';

            var srchForm = document.getElementById("stdSrchFrm");
            srchForm.action = url;
            srchForm.submit();
        }
    }

    function setKeywordValue(id) {

        if ($.trim($(id).val()) == "") {
            $(id).val("Type your Keyword");
        }
    }   

    $j(document).ready(function () {
        $j('#btnKeyword').click(function () {
            navigateSearch();
        });
    });
</script>
<script type="text/javascript">
    function onChangeOfCombo(cmbObj) {
        var selectedTab = cmbObj.value;
        chngTab(selectedTab);
    }

    function chngTab(tabNo)
    {
        var cmbObj = document.getElementById("cmbSearchTab");
        var url = "<%=request.getContextPath()%>";
        if (tabNo == 1)
        {
            //$j("#tenderTab").addClass("sMenu");
            //$j("#appTab").removeClass("sMenu");
            //$j("#noaTab").removeClass("sMenu");
            cmbObj.options[0].selected = true;
            cmbObj.options[1].selected = false;
            cmbObj.options[2].selected = false;

            $j("#crntTab").val("tenderTab");
            url += "/resources/common/AdvTenderListing.jsp?h=t";
            $j("#advSrch").attr('href', url);
        } else if (tabNo == 2)
        {
            //$j("#tenderTab").removeClass("sMenu");
            //$j("#appTab").addClass("sMenu");
            //$j("#noaTab").removeClass("sMenu");
            cmbObj.options[0].selected = false;
            cmbObj.options[1].selected = true;
            cmbObj.options[2].selected = false;

            $j("#crntTab").val("appTab");
            url += "/resources/common/AdvAppListing.jsp";
            $j("#advSrch").attr('href', url);
        } else if (tabNo == 3)
        {
            //$j("#tenderTab").removeClass("sMenu");
            //$j("#appTab").removeClass("sMenu");
            //$j("#noaTab").addClass("sMenu");
            cmbObj.options[0].selected = false;
            cmbObj.options[1].selected = false;
            cmbObj.options[2].selected = true;

            $j("#crntTab").val("noaTab");
            url += "/resources/common/AdvContractListing.jsp";
            $j("#advSrch").attr('href', url);
        }
    }
</script>
<%
    String lang = "en_US", home = null, aboutegp = null, contactus = null, myDashboard = null, langForMenu = "en_US";
    String rssfeed = null;
    String toptitle = null, Gotostr = null, tenders = null, annualprocplan = null, strreports = null, viewallnotifi = null, strsearch = null, stradvsearch = null, awardedcontracts = null, debarredtenderers = null, debarmentrules = null, debarmentcomittee = null, debarmentchariman = null, debarmentdecision = null, debarmentlist = null;

    String offlineTenders = null, offlineAwrdCntrct = null;

    if (request.getParameter("lang") != null && request.getParameter("lang") != "") {
        lang = request.getParameter("lang");
    } else {
        lang = "en_US";
    }

    if (request.getParameter("langForMenu") != null && request.getParameter("langForMenu") != "") {
        langForMenu = request.getParameter("langForMenu");
    } else {
        langForMenu = "en_US";
    }

    MultiLingualService multiLingualService = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
    List<TblMultiLangContent> langContentList = multiLingualService.findContent(langForMenu, "top");
    List<TblMultiLangContent> langContentList2 = multiLingualService.findContent(lang, "top");

    if (!langContentList2.isEmpty()) {
        for (TblMultiLangContent tblMultiLangContent : langContentList2) {
            if (tblMultiLangContent.getSubTitle().equals("lbl_search")) {
                if ("bn_IN".equals(lang)) {
                    strsearch = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    strsearch = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_advancesearch")) {
                if ("bn_IN".equals(lang)) {
                    stradvsearch = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    stradvsearch = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_dashboard")) {
                if ("bn_IN".equals(lang)) {
                    myDashboard = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    myDashboard = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentRule")) {
                if ("bn_IN".equals(lang)) {
                    debarmentrules = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarmentrules = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentComittee")) {
                if ("bn_IN".equals(lang)) {
                    debarmentcomittee = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarmentcomittee = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentChairman")) {
                if ("bn_IN".equals(lang)) {
                    debarmentchariman = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarmentchariman = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentDecision")) {
                if ("bn_IN".equals(lang)) {
                    debarmentdecision = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarmentdecision = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentList")) {
                if ("bn_IN".equals(lang)) {
                    debarmentlist = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarmentlist = new String(tblMultiLangContent.getValue());
                }
            }
        }
    }

    if (!langContentList.isEmpty()) {
        for (TblMultiLangContent tblMultiLangContent : langContentList) {
            if (tblMultiLangContent.getSubTitle().equals("lbl_home")) {
                if ("bn_IN".equals(langForMenu)) {
                    home = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    home = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_aboutegp")) {
                if ("bn_IN".equals(langForMenu)) {
                    aboutegp = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    aboutegp = new String(tblMultiLangContent.getValue());
                }
            }

            if (tblMultiLangContent.getSubTitle().equals("lbl_contactus")) {
                if ("bn_IN".equals(langForMenu)) {
                    contactus = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    contactus = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_rssfeed")) {
                if ("bn_IN".equals(langForMenu)) {
                    rssfeed = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    rssfeed = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_toptitle")) {
                if ("bn_IN".equals(langForMenu)) {
                    toptitle = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    toptitle = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_goto")) {
                if ("bn_IN".equals(langForMenu)) {
                    Gotostr = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    Gotostr = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_tenders")) {
                if ("bn_IN".equals(langForMenu)) {
                    tenders = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    tenders = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_annuprocplan")) {
                if ("bn_IN".equals(langForMenu)) {
                    annualprocplan = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    annualprocplan = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_reports")) {
                if ("bn_IN".equals(langForMenu)) {
                    strreports = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    strreports = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_viewallnotification")) {
                if ("bn_IN".equals(langForMenu)) {
                    viewallnotifi = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    viewallnotifi = new String(tblMultiLangContent.getValue());
                }
            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_search")) {
//                if ("bn_IN".equals(lang)) {
//                    strsearch = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    strsearch = new String(tblMultiLangContent.getValue());
//                }
//            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_advancesearch")) {
//                if ("bn_IN".equals(lang)) {
//                    stradvsearch = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    stradvsearch = new String(tblMultiLangContent.getValue());
//                }
//            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_awardedcontract")) {
                if ("bn_IN".equals(langForMenu)) {
                    awardedcontracts = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    awardedcontracts = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarredtender")) {
                if ("bn_IN".equals(langForMenu)) {
                    debarredtenderers = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarredtenderers = new String(tblMultiLangContent.getValue());
                }
            }

//            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentRule")) {
//                if ("bn_IN".equals(lang)) {
//                    debarmentrules = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    debarmentrules = new String(tblMultiLangContent.getValue());
//                }
//            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentComittee")) {
//                if ("bn_IN".equals(lang)) {
//                    debarmentcomittee = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    debarmentcomittee = new String(tblMultiLangContent.getValue());
//                }
//            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentChairman")) {
//                if ("bn_IN".equals(lang)) {
//                    debarmentchariman = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    debarmentchariman = new String(tblMultiLangContent.getValue());
//                }
//            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentDecision")) {
//                if ("bn_IN".equals(lang)) {
//                    debarmentdecision = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    debarmentdecision = new String(tblMultiLangContent.getValue());
//                }
//            }
//            if (tblMultiLangContent.getSubTitle().equals("lbl_debarmentList")) {
//                if ("bn_IN".equals(lang)) {
//                    debarmentlist = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                } else {
//                    debarmentlist = new String(tblMultiLangContent.getValue());
//                }
//            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_offlinetender")) {
                if ("bn_IN".equals(langForMenu)) {
                    offlineTenders = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    offlineTenders = new String(tblMultiLangContent.getValue());
                }
            }

            if (tblMultiLangContent.getSubTitle().equals("lbl_offlineawardedcontract")) {
                if ("bn_IN".equals(langForMenu)) {
                    offlineAwrdCntrct = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    offlineAwrdCntrct = new String(tblMultiLangContent.getValue());
                }
            }

        }
    }

    boolean isBeforeLogin = true;
    Object objUserName = "";
    HttpSession sn = request.getSession();

    String keyword = "";
    if (request.getParameter("keyword") != null && !"".equals(request.getParameter("keyword"))) {
        keyword = request.getParameter("keyword");
    }

    if (sn.getAttribute("userTypeId") != null) {
        int userTypeId = 0;
        if (sn.getAttribute("userTypeId") != null) {
            userTypeId = Integer.parseInt(sn.getAttribute("userTypeId").toString());
        }
        objUserName = sn.getAttribute("userName");
        if (objUserName == null) {
            objUserName = "";
        }

        if (userTypeId == 1) {
            objUserName = "e-GP Admin";
        }
        isBeforeLogin = false;
    }
%>
<%--<table width="100%" border="0" cellspacing="0" cellpadding="0" class="topTitle">
    <tr>
        <td align="center">--%>
<div style="margin-left: 15px;margin-right: 15px;">
    <% if (request.getParameter("lang") == null || request.getParameter("langForMenu") == null) {
    %>
    <a href="<%=request.getContextPath()%>/Index.jsp?lang=en_US" >
        <img src="<%=request.getContextPath()%>/resources/images/header.jpg"  style ="width:100%;height: auto;" alt="EGP" style="margin-top: 15px;" />
        <div id="innerHover" title="hover text" style="font-size: 11px;"><div id="popupdiv">
                The dark green power button symbolizes procuring at the click of the button ensuring Value for Money (VfM) in Government Procurement.<br/><br/>

                The red burning flame overarching the whole logo signifies the elimination of any fraudulent and corrupt practices in procurement. <br/><br/>

                The letters 'e-GP' on a white background symbolizes integrity and transparency in government procurement. The open book at the bottom of the logo signifies transition from manual to electronic (paperless) procurement system.


            </div></div>
    </a>
    <%
    } else {
    %>
    <a href="<%=request.getContextPath()%>/Index.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu")%>" >
        <img src="<%=request.getContextPath()%>/resources/images/header.jpg"  style ="width:100%;height: auto;" alt="EGP" style="margin-top: 15px;" />
    </a>

    <%
        }
    %>
    <!--<%//=toptitle%>-->
</div>
<%--</td>
</tr>
</table>--%>
<!--Logo Panel Table-->
<div id="language-placeholder">
    <div id="language-placeholder-content">
        Language
        <select name="namelanguage" id="idlanguage" onchange="changelanguage(this.value);" style="width:70px;">
            <option value="en_US"<% if ("en_US".equals(langForMenu)) {%>selected<% }%>> English</option>
            <option value="bn_IN"<% if ("bn_IN".equals(langForMenu)) {%>selected<% }%>>Dzongkha</option>
        </select>
    </div>
</div>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="t_space">
    <tr valign="top">

        <td class="contentArea_2 t-align-left">

            <table width="100%" border="0" cellspacing="0" cellpadding="0" s>
                <tr>
                    <td class="tabPanelArea_top" valign="middle" style="vertical-align: middle;">
                        <table class="newNavbar" width="100%" border="0"  cellpadding="0" style="table-layout: fixed; border-collapse: collapse!important;">
                            <tr>
                                <td class="">
                                    <% if (request.getParameter("lang") != null) {%>
                                    <a href="<%=request.getContextPath()%>/Index.jsp?lang=en_US&langForMenu=<%=langForMenu%>"><%=home%></a>
                                    <% }

                                        
                                            else {%>
                                    <a href="<%=request.getContextPath()%>/Index.jsp"><%=home%></a>
                                    <% }%>   
                                </td>
                                <td>
                                    <% if (request.getParameter (
                                                 
                                            "lang") != null) {%>
                                    <a href="<%=request.getContextPath()%>/AboutEGP.jsp?lang=en_US&langForMenu=<%=langForMenu%>"><%=aboutegp%></a>
                                    <% }

                                        
                                            else {%>
                                    <a href="<%=request.getContextPath()%>/AboutEGP.jsp"><%=aboutegp%></a>
                                    <% }%>

                                </td>
                                <td>

                                    <% if (request.getParameter (
                                                 
                                            "lang") != null) {%>
                                    <a href="<%=contextPath%>/resources/common/TenderListing.jsp?lang=en_US&langForMenu=<%=langForMenu%>&h=t"><%= tenders%></a></td>
                                    <% }

                                        
                                            else {%>
                            <a href="<%=contextPath%>/resources/common/TenderListing.jsp?h=t"><%= tenders%></a></td>
                            <% }%>

                    </td>
                    <td>
                        <% if (request.getParameter (
                                     
                                "lang") != null) {%>
                        <a href="<%=contextPath%>/resources/common/AppListing.jsp?lang=en_US&langForMenu=<%=langForMenu%>"><%= annualprocplan%></a>
                        <% }

                            
                                else{ %>
                        <a href="<%=contextPath%>/resources/common/AppListing.jsp"><%= annualprocplan%></a>
                        <% }%>


                    </td>
                    <td>
                        <% if (request.getParameter (
                                     
                                "lang") != null) {%>
                        <a href="<%=contextPath%>/resources/common/ContractListing.jsp?lang=en_US&langForMenu=<%=langForMenu%>"><%=awardedcontracts%></a>
                        <% }

                            
                                else{ %>
                        <a href="<%=contextPath%>/resources/common/ContractListing.jsp"><%=awardedcontracts%></a>
                        <% }%>
                    </td>
                    <td onmouseover="showmenu('tutorials')" onmouseout="hidemenu('tutorials')"><a href="#"><%=debarredtenderers%></a>
                        <table class="submenu" id="tutorials">
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=contextPath%>/resources/common/Chairmanship.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;&nbsp;<%=debarmentchariman%></a></td></tr>
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=contextPath%>/resources/common/DebarmentCommitteeMember.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;<%=debarmentcomittee%></a></td></tr>                          
                            <!--<tr><td style="padding-left: 0px; text-align: left;"><a href="<%=contextPath%>/resources/common/DebarmentDecisions.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;<%=debarmentdecision%></a></td></tr>-->
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=contextPath%>/resources/common/DebarmentListing.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;<%=debarmentlist%></a></td></tr>
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=contextPath%>/resources/common/DebarmentRules.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp; <%=debarmentrules%> </a></td></tr>


                            <%--<tr><td style="padding-left: 5px; text-align: left;"><a href="<%=contextPath%>/resources/common/DebarmentList.jsp"><%=debarmentlist%></a></td></tr> --%>
                        </table>                             

                    </td>
                    <td onmouseover="showmenu('tutorialsRev')" onmouseout="hidemenu('tutorialsRev')">
                        <% if (request.getParameter (
                                     
                                "lang") != null) {%>
                        <!--a href="<%=request.getContextPath()%>/contactUs.jsp?lang=<%=request.getParameter("lang")%>"><%=contactus%></a-->
                        <a href="#"><%=contactus%></a>
                        <% }

                            
                                else {%>
                        <!--a href="<%=request.getContextPath()%>/contactUs.jsp"><%=contactus%></a-->
                        <a href="#"><%=contactus%></a>
                        <% }%>


                        <table class="submenu" id="tutorialsRev">
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=request.getContextPath()%>/Grievance.jsp?submenu=FlowChart&lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;  Complaint Process Flow Chart</a></td></tr>
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=request.getContextPath()%>/Grievance.jsp?submenu=Decision&lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;  Decision</a></td></tr>
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=request.getContextPath()%>/Grievance.jsp?submenu=IRBMembers&lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp; IRB  Members </a></td></tr>
                            <tr><td style="padding-left: 0px; text-align: left;"><a href="<%=request.getContextPath()%>/Grievance.jsp?submenu=IRBRules&lang=en_US&langForMenu=<%=request.getParameter("langForMenu") == null ? "en_US" : request.getParameter("langForMenu")%>">&nbsp;&nbsp;  Rules and Procedure of IRB</a></td></tr>

                        </table>   


                    </td>                              

                </tr>                      


            </table>
        </td>
    </tr>

</table>
<div style="margin-top:0px;">
    <%-- <jsp:include page="MarqueeDisplay.jsp" >
        <jsp:param name="location" value="Home Page"/>
        <jsp:param name="userType" value="1"/>
        <jsp:param name="viewallnot" value="<%= viewallnotifi %>"/>
    </jsp:include> --%>
</div>
</td>
</tr>
<tr>
    <td class="contentArea_2">
        <div class="login_board">
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                <form id="stdSrchFrm" onsubmit="navigateSearch()" method="post">
                    <tr>
                        <td class="txtSearchBoxtd">
                            <input name="keyword" type="text" id="txtKeyword" class="txtSearchBox" style="width: 90%" value="<% if(!"".equals(keyword) 
                                    ){ out.print(keyword);
                                }

                                
                                

                                else{ %>Type your Keyword<% } %>" onclick="this.value = '';" onblur="setKeywordValue(this);"/>
                        </td>
                        <td  height="30" class="cmbSearchTabtd">
                            <select id="cmbSearchTab" name="cmbSearchTab" onchange="onChangeOfCombo(this);" class="txtSearchBox" style="padding: 1px;width: 90%">
                                <%--style="font-size: 18px;height: 32px;vertical-align: middle;"--%>
                                <option value="1">Tender</option>
                                <option value="2">Annual Procurement Plan</option>
                                <option value="3">Contract</option>
                            </select>
                        </td>
                        <td height="30" valign="middle" class="btnKeywordtd">
                            <label
                                <% if("bn_IN".equals(lang) 
                                    

                                    
                                    
                                    ){ %> class="btnSearch_bn" <% }else{ %> class="btnSearch" <% } %> style="width:90%" >
                                <input name="keyword" id="btnKeyword" type="button" value="<%= strsearch%>" />
                            </label>
                            <div class="msg" id="msg" style="color: red"></div>
                            <input type="hidden" id="curDate" value="<%=lastdate%>"/>
                            <input type="hidden" name="crntTab" id="crntTab" value="tenderTab"/>
                            <input type="hidden" id="fromHome" value="true"/>
                        </td>
                        <td width="17%" class="serchTxt" align="left" valign="middle">
                            <a id="advSrch" href="<%=contextPath%>/resources/common/AdvTenderListing.jsp?h=t" style="font-weight:bold; text-align: left;"><%=  stradvsearch%></a>
                        </td>
                        <td width="47%" align="right" valign="middle" height="30" class="homeMenu">
                            <%
                                String top_Url = request.getRequestURL().toString();
                                if (!isBeforeLogin

                                
                                    ) {
                                    if (false && (top_Url.contains("CompanyDetails.jsp") || top_Url.contains("PersonalDetails.jsp") || top_Url.contains("SupportingDocuments.jsp")
                                            || top_Url.contains("JvcaDetails.jsp") || top_Url.contains("IndividualConsultant.jsp") || top_Url.contains("FinalSubmission.jsp") || top_Url.contains("ChangePassword.jsp"))) {

                                    } else {
                                        int t_uId = 0;
                                        int t_uTypeId = 0;
                                        if (session.getAttribute("userId") != null) {
                                            t_uId = Integer.parseInt(session.getAttribute("userId").toString());
                                        }
                                        if (session.getAttribute("userTypeId") != null) {
                                            t_uTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                                        }
                                        boolean t_userApproved = false;
                                        boolean t_isFirstLogin = false;
                                        String t_status = "";
                                        TenderCommonService t_tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                        List<SPTenderCommonData> t_list = t_tenderCommonService.returndata("getStatusOfUser", t_uId + "", "");
                                        if (t_list != null) {
                                            if (t_list.size() > 0) {
                                                t_status = t_list.get(0).getFieldName1();
                                                if ("approved".equalsIgnoreCase(t_list.get(0).getFieldName1())) {
                                                    t_userApproved = true;
                                                }
                                                //if(t_uTypeId != 2 && t_uTypeId != 1){
                                                if (t_uTypeId != 1) {
                                                    if ("yes".equalsIgnoreCase(t_list.get(0).getFieldName2())) {
                                                        t_isFirstLogin = true;
                                                    }
                                                }
                                            }
                                            t_list = null;
                                        }
                                        if (t_userApproved && !(t_isFirstLogin)) {
                            %>
                            <img alt="" src="<%=contextPath%>/resources/images/Dashboard/userIcn.png" class="linkIcon_1" />Welcome, <%=objUserName%> &nbsp;|&nbsp;
                            <% if (t_uTypeId == 2) {
                                    if (request.getParameter("lang") != null) {%>
                            <a href="<%=request.getContextPath()%>/resources/common/InboxMessage.jsp?lang=en_US&langForMenu=<%=request.getParameter("langForMenu")%>"><%= myDashboard%></a>&nbsp; | &nbsp;
                            <% } else {%>
                            <a href="<%=request.getContextPath()%>/resources/common/InboxMessage.jsp"><%= myDashboard%></a>&nbsp; | &nbsp;
                            <% }
                            } else if (t_uTypeId == 8) {
                                if (request.getParameter("lang") != null) {%>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp?viewtype=pending&lang=en_US&langForMenu=<%=request.getParameter("langForMenu")%>">Dashboard</a>&nbsp; | &nbsp;
                            <%} else {%>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp?viewtype=pending">Dashboard</a>&nbsp; | &nbsp;
                            <% }
                            } else if (t_uTypeId == 3) {
                                CommonService commonService_media = (CommonService) AppContext.getSpringBean("CommonService");
                                String role = "";
                                role = commonService_media.getUserRoleByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                                    if (role.contains("HOPA")) {%>
                            <a href="<%=request.getContextPath()%>/resources/common/PaDashboard.jsp?viewtype=pending">Dashboard</a>&nbsp; | &nbsp;
                            <%} else if (request.getParameter("lang") != null) {%>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp?viewtype=pending&lang=en_US&langForMenu=<%=request.getParameter("langForMenu")%>">Dashboard</a>&nbsp; | &nbsp;
                            <%} else {%>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp?viewtype=pending">Dashboard</a>&nbsp; | &nbsp;
                            <% }
                                    } else if (t_uTypeId == 1 || t_uTypeId == 4 || t_uTypeId == 5 || t_uTypeId == 19) {
                                        if (request.getParameter("lang") != null && false) {%>
                            <a href="<%=request.getContextPath()%>/admin/AdminDashboard.jsp&lang=en_US&langForMenu=<%=request.getParameter("langForMenu")%>"><%= myDashboard%></a>&nbsp; | &nbsp;
                            <% } else {%>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp"><%= myDashboard%></a>&nbsp; | &nbsp;
                            <% }
                            } else if (t_uTypeId == 7) {
                            %>
                            <a href="<%=request.getContextPath()%>/resources/common/Dashboard.jsp"><%= myDashboard%></a>&nbsp; | &nbsp;
                            <%
                                }
                            } else
                            %>
                            <%--  <% if (request.getParameter("lang") != null) {%>
                            <a href="<%=request.getContextPath()%>/Index.jsp?lang=<%=request.getParameter("lang")%>"><%=home%></a>&nbsp; | &nbsp;
                            <% } else {%>
                            <a href="<%=request.getContextPath()%>/Index.jsp"><%=home%></a>&nbsp; | &nbsp;
                            <% }%> --%>
                            <% if ("incomplete".equalsIgnoreCase(t_status)) {%>
                            <a href="<%=request.getContextPath()%>/LoginSrBean?funName=cmpltVeriPrcs"><font color="red">Complete Registration Process</font></a>&nbsp; | &nbsp;
                                <% }%>
                                <%
                                        }
                                    }
                                    else
                                %>                                                                     


 <!-- <a href="<%=request.getContextPath()%>/SubscribeToRSS.jsp"><%=rssfeed%></a>-->

                      <!--  <a target="blank" href="<%//request.getContextPath()%>/RSS/RSS_eGP.xml" style="text-align: center;">
                        <img src="<%//contextPath%>/resources/images/rss.gif" width="36" height="14">&nbsp;&nbsp;<%//rssfeed%>
                        </a>-->
                            <%--                                        <a href=""><span style="color:black;">Language</span></a>&nbsp;
                                                                    <select name="namelanguage" id="idlanguage" onchange="changelanguage(this.value);" style="width:70px;">
                                                                        <option value="en_US"<% if ("en_US".equals(lang)) {%>selected<% }%>>English</option>
                                                                        <option value="bn_IN"<% if ("bn_IN".equals(lang)) {%>selected<% }%>>Bangla</option>
                                                                    </select>--%>
                            <%
                                if (!isBeforeLogin

                                
                                    ) {
                                    //if(top_Url.contains("CompanyDetails.jsp") || top_Url.contains("PersonalDetails.jsp") || top_Url.contains("SupportingDocuments.jsp")
                                    //        || top_Url.contains("JvcaDetails.jsp") || top_Url.contains("IndividualConsultant.jsp") || top_Url.contains("FinalSubmission.jsp")){

                    //}else{
                            %>
                            <img src="<%=contextPath%>/resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" />                            <a href="<%= request.getContextPath()%>/Logout.jsp" title="Logout">Logout</a>
                            <%
                                    //}
                                }
                            %>
                        </td>
                    </tr>
                </form>
            </table>

        </div>

    </td>
</tr>


</table>
<div id="lang-dialog-message" title="Instruction" style="display:none;">
    <p>
        Dear User,
    </p><br />
    <p>
        To view the content of this website in Dzongkha properly, you need to have 'Proper Dzongkha Font' installed in your computer. You can install Dzongkha font by:
    </p><br />
    <ul style="padding-left: 15px;">
        <li>
            1. http://www.dzongkha.gov.bt/
        </li>
        <li>
            2. Click on English
        </li>
        <li>
            3. Click on Download Menu
        </li>
        
        <li>
            4. Click on Unicode Tools
        </li>
        <li>
            5. Download Dzongkha Fonts <br/>
            &nbsp;&nbsp;&nbsp;a. Unzip the folder<br/>
            &nbsp;&nbsp;&nbsp;b. Select DDC Rinzin, DDC_Uchen, Jomolhari-aplha3c-0605331,<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wangdi_29 and Tsuyig Font_041028_)OT<br/>
            &nbsp;&nbsp;&nbsp;c. Copy those selected font and paste in C drive/ Windows/Fonts

        </li>
        <li>
            6. Download Dzongkha Locale<br/>
            &nbsp;&nbsp;&nbsp;a. Unzip the Folder<br/>
            &nbsp;&nbsp;&nbsp;b. Click on the set up file and install

        </li>
        <li>
            7. Download Dzongkha Keyboard for Windows<br/>
            &nbsp;&nbsp;&nbsp;a. Unzip the folder<br/>
            &nbsp;&nbsp;&nbsp;b. Click on the Set Up

        </li>
        <li>
            8. Refresh the page.
        </li>
    </ul>
</div>
<div id="tender-dialog-message" title="Instruction" style="display:none;">
    <p>
        Dear User,
    </p><br />
    <p>
        This tender is manual tender and you need to buy the tender document from Procuring Agency Office. To view the details tender notice click on "View" link.
        Same information you can get from GPPMD web site.
        <br />
        www.pppd.gov.bt
    </p>


</div>
<div id="awarded-dialog-message" title="Instruction" style="display:none;">
    <p>
        Dear User,
    </p><br />
    <p>
        This Contract Awarded is manual Contract and you need to buy the tender document from Procuring Agency Office. To view the details information click on "View" link.
        Same information you can get from GPPMD web site.
        <br />
        www.pppd.gov.bt
    </p>
    <br />

</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/DatenTime.js"></script>
<script type="text/javascript">
                                Start();
</script>
<script>
    function showmenu(elmnt)
    {
        document.getElementById(elmnt).style.visibility = "visible";
    }
    function hidemenu(elmnt)
    {
        document.getElementById(elmnt).style.visibility = "hidden";
    }
</script>
