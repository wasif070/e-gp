<%-- 
    Document   : EmpTransferHist
    Created on : May 5, 2012, 12:03:08 PM
    Author     : nishit
--%>
<%if(request.getAttribute("listEmpTras")!=null){%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.String"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeTrasfer"%>
<%@page import="java.util.List"%>
<jsp:useBean id="transGovSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<div class="ff t_space" id="empTransFerDiv">
    User Transfer History</div>
<table class="tableList_1 t_space" cellspacing="0" width="100%" id="empTransFerTbl">
                                            <tr>
                                                <th class="t-align-center">Sl. No.</th>
                                                <th class="t-align-center">Full Name</th>
                                                <th class="t-align-center">Last Working Date</th>
                                                <th class="t-align-center">Replacement Date and Time</th>
                                                <th class="t-align-center">Replaced By</th>
                                                <th class="t-align-center">Comments</th>
                                            </tr>
<%
boolean noRec = false;
Map<String,String> comUserIdandUserTypeIdMap = new HashMap<String,String>();

    comUserIdandUserTypeIdMap = (Map<String,String>)request.getAttribute("listEmpTras");

boolean flag = false;
int j = 1;
        
if(comUserIdandUserTypeIdMap!=null && !comUserIdandUserTypeIdMap.isEmpty()){
    Iterator it = comUserIdandUserTypeIdMap.entrySet().iterator();
    while(it.hasNext()){
            Map.Entry k = (Map.Entry)it.next();
        if(k.getValue().toString().equalsIgnoreCase("3")){
        List<TblEmployeeTrasfer> listComGovtEmpTrans = transGovSrUser.getUserHistory(Integer.parseInt(k.getKey().toString()));
%>
  
                                            <%
                                                        
                                                        for (int i = 0; i < listComGovtEmpTrans.size(); i++) {
                                                            String strGovtUserFullName = "-";
                                                            String strGovtUserReplacedBy = "-";
                                                            String strGovtUserComments = "-";

                                                            if (!"".equalsIgnoreCase(listComGovtEmpTrans.get(i).getReplacedBy())) {
                                                                strGovtUserReplacedBy = listComGovtEmpTrans.get(i).getReplacedBy();
                                                                flag = true;
                                                                if (!"".equalsIgnoreCase(listComGovtEmpTrans.get(i).getEmployeeName())) {
                                                                    strGovtUserFullName = listComGovtEmpTrans.get(i).getEmployeeName();
                                                                }

                                                                if (!"".equalsIgnoreCase(listComGovtEmpTrans.get(i).getRemarks())){
                                                                    strGovtUserComments = listComGovtEmpTrans.get(i).getRemarks();
                                                                }
                                                                
                                            %>
                                            <tr
                                                <%if (i % 2 == 0) {%>
                                                class="bgColor-Green"
                                                <%} else {%>
                                                class="bgColor-white"

                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    
                                                </td>
                                                <td class="t-align-center"><%=strGovtUserFullName%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(listComGovtEmpTrans.get(i).getTransferDt())%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(listComGovtEmpTrans.get(i).getTransferDt())%></td>
                                                <td class="t-align-center"><%=strGovtUserReplacedBy%></td>
                                                <td class="t-align-center"><%=strGovtUserComments%></td>
                                                
                                            </tr>
                                            <%j++;}
                                            strGovtUserComments = null;
                                            strGovtUserReplacedBy = null;
                                            strGovtUserFullName = null;
                                                        }
                                                        %>


<%
if(listComGovtEmpTrans!=null){
    listComGovtEmpTrans.clear();
    listComGovtEmpTrans = null;
}
        }else{
            TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
            List<TblUserActivationHistory> listComSchDevTransHis = transferEmployeeServiceImpl.viewAccountStatusHistory(k.getKey().toString());
%>
  
                                            <jsp:useBean id="comPartnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
                                            <%

                                                        for (int i = 0; i < listComSchDevTransHis.size(); i++) {
                                                            String strSchDevFullName = "-";
                                                            String stSchDevraction = "-";
                                                            String strSchDevComments = "-";
                                                            String strSchDevDt = "-";

                                                            if (!"".equalsIgnoreCase(listComSchDevTransHis.get(i).getAction())) {
                                                                if(listComSchDevTransHis.get(i).getAction().equalsIgnoreCase("deactive")){
                                                                    stSchDevraction = "Deactive";
                                                                }else if(listComSchDevTransHis.get(i).getAction().equalsIgnoreCase("approved")){
                                                                    stSchDevraction = "Active";
                                                                }
                                                                flag = true;

                                                               
                                                                    strSchDevFullName = comPartnerAdminMasterDtBean.getFullName();
                                                                

                                                                if (!"".equalsIgnoreCase(listComSchDevTransHis.get(i).getComments())) {
                                                                    strSchDevComments = listComSchDevTransHis.get(i).getComments();
                                                                }
                                                                /*if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                                    straction = list.get(i).getAction();
                                                                }
                                                               */
                                            %>
                                            <tr
                                                <%if (i % 2 == 0) {%>
                                                class="bgColor-white"
                                                <%} else {%>
                                                class="bgColor-Green"
                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    <%j++;%>
                                                </td>
                                                <td class="t-align-center"><%=strSchDevFullName%></td>
                                                <td class="t-align-center"><%=stSchDevraction%></td>
                                                <td class="t-align-center"><%=listComSchDevTransHis.get(i).getActionByName()%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(listComSchDevTransHis.get(i).getActionDt())%></td>
                                                 <td class="t-align-center"><%=strSchDevComments%></td>
                                                 
                                            </tr>
                                            <%}
                                               strSchDevFullName = null;
                                               stSchDevraction = null;
                                               strSchDevComments = null;
                                               strSchDevDt = null;             
                                                        }%>

                                   
<%
if(listComSchDevTransHis!=null){
    
    listComSchDevTransHis.clear();
    listComSchDevTransHis = null;
}/*nullfied condtion ends here*/
        }/*UserType Condtion Ends here*/
    }/*For loop of userId Values ends here*/
}/*UserType Id condition Ends here*/%>
<%
if (!flag) {
                                            %>
                                            <tr><td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td></tr>
                                            <%}%>

     </table>
<%if(!flag){%>
<script type="text/javascript">
    $('#empTransFerTbl').hide();
    $('#empTransFerDiv').hide();
</script>
<%}
if(comUserIdandUserTypeIdMap!=null){
    comUserIdandUserTypeIdMap.clear();
    comUserIdandUserTypeIdMap = null;
}%>
<%}%>