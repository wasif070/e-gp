<%-- 
    Document   : GovtUserFirstLogin
    Created on : Oct 23, 2010, 7:29:28 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Govt User First Login</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="/resources/common/Top.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">User Profile</div>
            <form method="POST" id="frmLogin" action="">
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td class="ff">e-mail ID : <span>*</span></td>
                        <td><input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  readonly="readonly" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td class="ff">Change Password : <span>*</span></td>
                        <td><input name="password" id="txtPass" type="text" class="formTxtBox_1" style="width:200px;" maxlength="25" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td class="ff">Confirm Password : <span>*</span></td>
                        <td><input name="confPassword" id="txtConfPass" type="text" class="formTxtBox_1" style="width:200px;" autocomplete="off"/></td>
                    </tr>
                    <tr>
                        <td class="ff">Hint Question : <span>*</span></td>
                        <td><select name="hintQstn" class="formTxtBox_1" id="cmbHintQstn" style="width:208px;">
                                <option>Select Question</option>
                                <option>1</option>
                                <option>2</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Create your Own Hint Question :</td>
                        <td><textarea name="createQstn" id="txtaCreateQstn" rows="3" cols="20" class="formTxtBox_1" style="width:200px;"></textarea></td>
                    </tr>
                    <tr>
                        <td class="ff">Hint Answer : <span>*</span></td>
                        <td><input name="hintAns" type="txtHintAns" class="formTxtBox_1" id="textfield7" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" />
                            </label>
                            
                        </td>
                    </tr>
                </table>
            </form>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
