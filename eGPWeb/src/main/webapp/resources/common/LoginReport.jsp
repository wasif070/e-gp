<%-- 
    Document   : LoginReport
    Created on : Feb 3, 2011, 4:58:22 PM
    Author     : Rajesh Singh,rishita
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login Report </title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>

        <script src="../js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.tablesorter.js"  type="text/javascript"></script>
        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script src="../js/jQuery/jquery.blockUI.js" type="text/javascript"></script>

        <script type="text/javascript">
                    $(document).ready(function() {
                                $("#print").click(function() {
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                });

                            });
                            function printElem(options){
                                $('#title').show();
                                $('#print_area').printElement(options);
                                $('#title').hide();
                            }
        </script>
        <script type="text/javascript">
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        //loadTenderTable();
                        loadTable(1);
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        //loadTenderTable();
                        loadTable(1);
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        //loadTenderTable();
                        loadTable(1);
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable(1);
                        //loadTenderTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTenderTable();
                            loadTable(1);
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>

        <script type="text/javascript">
            //function loadTenderTable()
            function loadTable(checkvalue)
            {
                if (checkvalue==1)
                {
                    //$.blockUI();
                    $.post("<%=request.getContextPath()%>/LoginReportServlet", {action: 'fetchData',emailID: $("#txtemailid").val(),searchDate: $("#txtsingledate").val(),dateFrom: $("#txtfromdate").val(),dateTo: $("#txttodate").val(),ipaddress:$("#txtipaddress").val(),TypeUser: $("#drpUserType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                //$.post("< %=request.getContextPath()%>/AppSearchServlet", {keyword: $("#keyword").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                   // $.unblockUI();
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    
                    sortTable();
                    //alert('123'+$('#resultTable').html());
                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    //alert($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    //alert($('#resultTable').html());
                });
                }else
                {
                //alert("rajesh")
                $.blockUI();
                $.post("<%=request.getContextPath()%>/LoginReportServlet", {action: 'fetchData',emailID: $("#txtemailid").val(),searchDate: $("#txtsingledate").val(),dateFrom: $("#txtfromdate").val(),dateTo: $("#txttodate").val(),ipaddress:$("#txtipaddress").val(),TypeUser: $("#drpUserType").val(),pageNo: '0',size: '0'},  function(j){
                //$.post("< %=request.getContextPath()%>/AppSearchServlet", {keyword: $("#keyword").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                $.unblockUI();
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    //alert($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                });

                }
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>
<script type="text/javascript">
function resetPage(){
    $('#drpUserType').val('');
    $('#txtfromdate').val('');
    $('#txtemailid').val('');
    $('#txtsingledate').val('');
    $('#txttodate').val('');
    $('#txtipaddress').val('');
    defaltGrid();
}
</script>
        
         
    </head>
    <%
                int userid = 0;
                int userType=0;
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = Integer.parseInt(hs.getAttribute("userId").toString());
                }
                if (hs.getAttribute("userTypeId") != null) {
                    userType= Integer.parseInt(hs.getAttribute("userTypeId").toString());
                }
                //out.println(userType);
    %>
    <script type="text/javascript">


        //Onclick Events
        function Validate()
        {
            var check=true;
            if(document.getElementById("txtemailid").value!=''){
                if(!email(document.getElementById("txtemailid").value)){
                    document.getElementById("semailid").innerHTML='<br/>Please enter proper e-mail ID.';
                    check= false;
                }
                else
                {
                    document.getElementById("semailid").innerHTML='';
                }
            }else
                {
                    document.getElementById("semailid").innerHTML='';
                }


            if(check==false){
                return false;
            }
            else
            {
                loadTable(1);
            }
        }

        //OnBlur Events
        function Emailid()
        {
            if(document.getElementById("txtemailid").value!=''){
                if(!email(document.getElementById("txtemailid").value)){
                    document.getElementById("semailid").innerHTML='<br/>Please enter proper e-mail ID.';
                }
                else
                {
                    document.getElementById("semailid").innerHTML='';
                }
            }
        }

        function email(value)
        {
            return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
        }

    </script>
    <script type="text/javascript">
        function defaltGrid(){
            loadTable(1);
        }
    </script>
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body onload="defaltGrid();hide();">
   
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Login Report</div>
            <div>&nbsp;</div>
            <table width="98%" cellspacing="0">
                <tr valign="top">
                    <td class="contentArea">
                        <div class="formBg_1 t_space">
                            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                            <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                                <tr>
                                    <td width="15%" class="ff">Type of User :</td>
                                    <td width="35%">

                                    <select name="drpUserType" class="formTxtBox_1" id="drpUserType" style="width:150px;" <%if(userType!=1){%>disabled="false" <%}%>>
                                            <option value="0">All</option>
                                            <option value="2">Bidder/Consultant</option>
                                            <option value="6">DP</option>
                                            <option value="7">Financial Institution User</option>
                                            <option value="8">Admin User</option>
                                            <option value="3">Government Users</option>
                                        </select>

                                    </td>
                                    <td width="15%"><span class="ff">Search for a Date : </span></td>
                                    <td width="35%">
                                        <input name="txtsingledate" type="text" class="formTxtBox_1" id="txtsingledate" style="width:100px;" readonly="readonly" onclick="GetCal('txtsingledate','txtsingledate');"/>
                                        &nbsp;<img id="imgtxtsingledate" name="imgtxtsingledate" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtsingledate','imgtxtsingledate');"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">From Date : </td>
                                    <td>
                                        <input name="txtfromdate" type="text" class="formTxtBox_1" id="txtfromdate" style="width:100px;" readonly="readonly" onclick="GetCal('txtfromdate','txtfromdate');"/>
                                        &nbsp;<img id="imgtxtfromdate" name="imgtxtfromdate" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtfromdate','imgtxtfromdate');"/>
                                    </td>
                                    <td><span class="ff">To Date : </span></td>
                                    <td><input name="txttodate" type="text" class="formTxtBox_1" id="txttodate" style="width:100px;" readonly="readonly" onclick="GetCal('txttodate','txttodate');"/>
                                        &nbsp;<img id="imgtxttodate" name="imgtxtfromdate" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txttodate','imgtxttodate');"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">e-mail ID :</td>
                                    <td><input type="text" class="formTxtBox_1" id="txtemailid" name="txtemailid" style="width:200px;" onblur="Emailid();"/>
                                        <span id="semailid" style="color: red;" />
                                    </td>
                                    <td class="ff">IP Address :</td>
                                    <td><input type="text" class="formTxtBox_1" id="txtipaddress" name="txtipaddress" style="width:100px;" />
                                        <span id="semailid" style="color: red;" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="t-align-center ff" ><span class="formBtn_1" >
                                            <input type="submit" name="btnsubmit" id="btnsubmit" value="Generate" onclick="return Validate();"/>

                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="formBtn_1" ><input type="submit" name="btnsubmit" id="btnsubmit" value="Reset" onclick="resetPage();"/></span></td>
                                    <td class="t-align-right">
                                        <%
                                                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                                                    String reqURL = "";
                                                    String reqQuery = "";
                                                    String folderName = pdfConstant.LOGINREPORT;
                                                    String genId = cdate + "_" + userid;
                                        %>
                                        <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('-1');">Save As PDF </a>
                                           &nbsp;
                                         <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="tabPanelArea_1">
                            <div id="resultDiv">
                                <div  id="print_area">
                                    <div id="title" class="pageHead_1" style="display: none">Login Report</div>
                                <table width="100%" cellspacing="0" id="resultTable" class="tableList_3" cols="@0,4">
                                    <tr>
                                        <th width="4%" class="t-align-center">Sl. No.</th>
                                        <th width="32%" class="t-align-center">e-mail ID</th>
                                        <th width="22%" class="t-align-center">Log In Date and Time</th>
                                        <th width="22%" class="t-align-center">Log Out Date and Time</th>
                                        <th width="20%" class="t-align-center">IP Address</th>
                                    </tr>
                                </table>
                                </div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="hidden" name="size" id="size" value="100" style="width:70px;"/>
                                        </td>
                                        <td align="center">
                                            <input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:50px;" />
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                            </label></td>
                                        <td  class="prevNext-container"><ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                          </ul></td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <input type="hidden" id="pageNo" value="1"/>

                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="LoginReport_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="LoginReport" />
                </form>
                <!--For Generate PDF  Ends-->
    <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
            <jsp:include page="Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
