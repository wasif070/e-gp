<%-- 
    Document   : OpeningComCommentsInclude
    Created on : Apr 18, 2012, 11:57:54 AM
    Author     : nishit
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean"%>
<jsp:useBean id="tendComSrBean" class="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"/>
<%
String strComType = "TOCMember";
String strCaption = "Opening";
if(request.getParameter("comType")!=null){
    if("2".equalsIgnoreCase(request.getParameter("comType"))){
       strComType = "TECMember"; 
       strCaption = "Evaluation";
    }
    if("3".equalsIgnoreCase(request.getParameter("comType"))){
       strComType = "TSCMember";
       strCaption = "Evaluation";        
    }
}
     List<CommitteMemDtBean> list = new ArrayList<CommitteMemDtBean>();
    /*if(request.getAttribute("comMemberDtBeans")!=null){
        list = (List<CommitteMemDtBean>)request.getAttribute("comMemberDtBeans");
    }else{*/
        list = tendComSrBean.committeUser(Integer.parseInt(request.getParameter("tenderId")), strComType);
    //}
if(list!=null && !list.isEmpty()){
    int icount=0;
%>
<table width="100%" cellspacing="0" class="tableList_1 t_space">
    <%
        for (CommitteMemDtBean omdb : list) {
          if(omdb.getRemarks()!=null && omdb.getRemarks().trim().length()>1){  
              
          icount++;
        if(icount==1){
    %>
    <div class="ff b_space">Comments for <%=strCaption%> by Committee Members</div>
    <tr>
        <th class="t-align-center" width="20%">Member Name</th>
        <th class="t-align-center" width="60%">Comments</th>
        <th class="t-align-center" width="20%">Date and Time</th>
    </tr>
    <%}%>
    <tr>
        <td class="t-align-left"><%=omdb.getEmpName()%></td>
        <td class="t-align-left"><%=URLDecoder.decode(omdb.getRemarks(),"UTF-8") %></td>
        <td class="t-align-left"><%=omdb.getAapDate()%></td>
    </tr>
    <%}}%>

</table>
<%}%>