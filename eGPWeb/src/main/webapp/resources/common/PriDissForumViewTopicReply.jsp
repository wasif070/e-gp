<%-- 
    Document   : PriDissForumPostTopic
    Created on : Mar 14, 2012, 2:18:05 PM
    Author     : dipal.shah
--%>

<%@page import="com.cptu.egp.eps.model.table.TblPriTopicReplyDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicReply"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.PrivateDissForumService"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.math.BigDecimal"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
        boolean flag = false;
        if(request.getParameter("flag")!=null)
        {
            flag = true;
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if(flag){%>View Topic<%}else{%>View Response<%}%></title>
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>        
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/form/CommonValidation.js"></script>
    </head>
     <%
        String topicId = "";
        List<TblPriTopicReplyDocument> listDocument=null;
        
        if(request.getParameter("topicId")!=null)
        {
            topicId = request.getParameter("topicId");
        }
        String topicReplyId = "";
        if(request.getParameter("topicReplyId")!=null)
        {
            topicReplyId = request.getParameter("topicReplyId");
        }
        String tenderId = "";
        if(request.getParameter("tenderId")!=null)
        {
            tenderId = request.getParameter("tenderId");
        }
        int userId=0;
        if(session.getAttribute("userId")!=null)
        {
            userId=Integer.parseInt(session.getAttribute("userId").toString());
        }

        PrivateDissForumService privateDissForumService = (PrivateDissForumService) AppContext.getSpringBean("PrivateDissForumService");
        List<TblPriTopicMaster> listTopic = privateDissForumService.getTopic(Integer.parseInt(topicId));
        List<TblPriTopicReply> getParticularReply = null;
        if(request.getParameter("topicReplyId") != null && !"".equals(request.getParameter("topicReplyId")))
        {
            getParticularReply = privateDissForumService.getParticularReply(Integer.parseInt(topicReplyId));
            if(getParticularReply!= null && getParticularReply.size() >0 && getParticularReply.get(0).getPostedBy() != userId &&  getParticularReply.get(0).getHasSeen() == 0)
            {
               TblPriTopicReply ob=getParticularReply.get(0);
               ob.setHasSeen(1);
               privateDissForumService.updateReadCountResponse(ob);
            }

            listDocument=privateDissForumService.getDocumentDetail(getParticularReply.get(0).getTopicReplyId(), "Response");

        }
        else
        {
            listDocument=privateDissForumService.getDocumentDetail(listTopic.get(0).getTopicId(), "Topic");
            
            
        }

         // Coad added by Dipal for Audit Trail Log.
       // ConsolodateService servicee = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
       // int conIdd = servicee.getContractId(Integer.parseInt(request.getParameter("tenderId")));

        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
//        String idType="contractId";
  //      int auditId=conIdd
        String idType="tenderId";
        int auditId=Integer.parseInt(tenderId);

        String auditAction="View Topic";
        String moduleName=EgpModule.Private_Disscussion_Forum.getName();
        String remarks="User Id: "+session.getAttribute("userId")+" has View Topic Id: "+topicId;
        if(request.getParameter("topicReplyId") != null)
        {
            auditAction="View Response";
            remarks="User Id: "+session.getAttribute("userId")+" has View Topic Response Id: "+topicId;
        }
        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            
    %>
    <body>
        <div class="dashboard_div">
            <%@include file="AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="pageHead_1"><%if(flag){%>View Topic<%}else{%>View Response<%}%>
                    <span class="c-alignment-right"><a href="PriDissForumTopicDetails.jsp?tenderId=<%=tenderId%>&topicId=<%=topicId%>&type=<%= request.getParameter("type")%>" class="action-button-goback">Go Back</a></span> </div>
                <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="#">   
                    <table width="97%" cellspacing="5" class="formStyle_1">
                       
                        <tr>
                            <td width="10%" class="ff"></td>
                            <td width="90%" class="ff"></td>
                        </tr>
                        <tr>
                            <td class="ff"> Topic :</td>
                            <td>
                                <%
                                    if(listTopic!=null && !listTopic.isEmpty())
                                    {
                                        out.print(listTopic.get(0).getTopic());
                                    }    
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Description : </td>
                            <td>
                               
                                <%
                                    if(flag)
                                    {
                                        if(listTopic!=null && !listTopic.isEmpty())
                                        {
                                            out.println(listTopic.get(0).getDescription());
                                        }
                                    }else{
                                        if(getParticularReply!=null && !getParticularReply.isEmpty())
                                        {
                                            out.println(getParticularReply.get(0).getDescription());
                                        }
                                    }
                                    
                                %>
                               
                            </td>
                        </tr>
                    </table>
                                <BR/>
                    <div class="tabPanelArea_1" style="width: 97%;">

                   <div class="t-align-left ff">
                        Uploaded Document:
                   </div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="28%">File Uploaded by</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%
                    int docCnt=0;
                    int uploadUserTypeId=0;
                    String uploadedBy="";
                    for (TblPriTopicReplyDocument objDoc : listDocument)
                    {
                                docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=objDoc.getDocName()%></td>
                        <td class="t-align-left"><%=objDoc.getDocDescription() %></td>
                        <td class="t-align-center">
                            <%= new BigDecimal(Double.parseDouble(objDoc.getDocSize()) / 1024).setScale(2,0)%>
                        </td>
                        <td class="t-align-left">
                        <%

                            uploadUserTypeId=commonService.getUserType(objDoc.getUploadedBy()).get(0).getTblUserTypeMaster().getUserTypeId();;
                            uploadedBy=commonService.getUserName(objDoc.getUploadedBy(), uploadUserTypeId);
                            if(uploadedBy != null && uploadedBy.length()>0 && uploadedBy.indexOf("%$") != -1)
                            {
                                uploadedBy = uploadedBy.substring(uploadedBy.indexOf("%$")+2,uploadedBy.length());
                            }
                            out.println(uploadedBy);
                        %>
                        </td>
                         <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/PriDissForumServlet?funName=downloadDocument&tenderId=<%=tenderId%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocName()%>&docSize=<%=objDoc.getDocSize()%>"><img src="../images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>

                    <%   if (objDoc != null) {
                             objDoc = null;
                         }
                     }
                     if (docCnt == 0)
                      {%>
                        <tr>
                            <td colspan="6" class="t-align-center">No records found.</td>
                        </tr>
                    <%}%>
                </table>
                 </div>
                <div>&nbsp;</div>
                </form>

                <!--Dashboard Content Part End-->

            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>