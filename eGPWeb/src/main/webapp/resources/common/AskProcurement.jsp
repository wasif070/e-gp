<%-- 
    Document   : AskProcurement
    Created on : Jan 11, 2011, 7:42:10 PM
    Author     : Naresh.Akurathi,rishita
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblAskProcurement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionCategory"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AskProcurementSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Ask Procurement Expert</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <!--        <script src="../../ckeditor/_samples/sample.js" type="text/javascript"></script>
                <link href="../../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->

        <script type="text/javascript">
            $(function() {
                $('#recaptcha_response_field').blur(function() {
                    if($('#recaptcha_response_field').val()!=""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {challenge:$('#recaptcha_challenge_field').val(),userInput:$('#recaptcha_response_field').val(),funName:'captchaValid'},
                        function(j){
                            if(j.toString()=="OK"){
                                $('span.#vericode').css("color","green");
                            }
                            else{
                                javascript:Recaptcha.reload();
                                $('span.#vericode').css("color","red");
                            }
                            $('span.#vericode').html(j);
                        });
                    }else{
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                    }
                });
            });
            function Validate()
            {
                var validatebool=true;
                //if($.trim(CKEDITOR.instances.question.getData()) == ""){
                if($.trim($("#txtuname").val()) == '') {
                    document.getElementById("valuname").innerHTML="Please enter User's Name";
                    valid = false;
                }else if($.trim($("#txtuname").val()) != '') {
                    document.getElementById("valuname").innerHTML="";
                }

                if(document.getElementById('question').value == ''){
                    document.getElementById("valQuestion").innerHTML="Please enter Query";
                    validatebool=false;
                }else if($.trim($("#question").val())=="" ){
                    document.getElementById("valQuestion").innerHTML="Only Space is not allowed";
                    validatebool=false;
                }else if($.trim($("#question").val()).search('>') >= 0 || $.trim($("#question").val()).search('<') >= 0 || $.trim($("#question").val()).search('&lt;') >= 0 || $.trim($("#question").val()).search('&gt;') >= 0){
                    document.getElementById("valQuestion").innerHTML="Special Characters are not allowed.";
                    validatebool=false;
                }else{
                    document.getElementById("valQuestion").innerHTML="";
                }
                if($('#recaptcha_response_field').val() != undefined){
                    if($('#recaptcha_response_field').val()==""){
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                        validatebool =  false;
                    }else{
                        if($('#vericode').html()!="OK"){
                            javascript:Recaptcha.reload();
                            validatebool =  false;
                        }
                    }
                }
                return validatebool;
            }
            function regForSpace(value){
                if(value.charAt(0) == ' '){
                    return true;
                }else{
                    return /^\s+|\s+$/g.test(value);
                }
            }
        </script>
    </head>
    <body>
        <%
                    int userTypeid = 0;
                    Map<String, String> errors = new HashMap<String, String>();
                    if (session.getAttribute("userTypeId") != null) {
                        userTypeid = Integer.parseInt(session.getAttribute(("userTypeId")).toString());
                    }
                    AskProcurementSrBean askProcurementSrBean = new AskProcurementSrBean();
                    String msg = "";
                    int userId = 0;
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                        askProcurementSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    askProcurementSrBean.setAuditTrail(null);

                    if ("Submit".equals(request.getParameter("submit"))) {
                        askProcurementSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        String question = request.getParameter("question");
                        if (question.indexOf(">") >= 0 || question.indexOf("<") >= 0 || question.indexOf("&lt;") >= 0 || question.indexOf("&gt;") >= 0) {
                            errors.put("question", "Special Characters are not allowed.");
                        }
                        int questionCatId = Integer.parseInt(request.getParameter("questionCategeory"));
                        if (errors.isEmpty()) {
                            TblAskProcurement askProcurement = new TblAskProcurement();
                            askProcurement.setTblQuestionCategory(new TblQuestionCategory(questionCatId));
                            askProcurement.setQuestion(question.trim());//DateUtils.convertStrToDate(new Date())
                            askProcurement.setPostedDate(new java.util.Date());
                            askProcurement.setPostedBy(userId);
                            askProcurement.setAnswer("");
                            askProcurement.setUserTypeId(userTypeid);
                            String postedByName = "";
                            //if(session.getAttribute("userName") != null || !session.getAttribute("userName").equals("")){
                            postedByName = request.getParameter("txtuname");
                            //}
                            askProcurement.setPostedByName(postedByName);
                            //askProcurement.setAnswerDate(new java.util.Date());
                            //askProcurement.setAnsweredBy(xx);

                            msg = askProcurementSrBean.addTblAskProcurement(askProcurement);
                            if (msg.equals("Values Added")) {
                                response.sendRedirect("QuestionListing.jsp?msg=create");
                            }
                        } else {
                            request.setAttribute("questions", errors);
                        }
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <jsp:include page="Top.jsp" ></jsp:include>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="contentArea">
                        <div class="pageHead_1">Ask Procurement Expert
                            <span class="c-alignment-right">
                                <a href="QuestionListing.jsp" class="action-button-goback">Go Back To Dashboard</a>
                            </span>
                        </div>

                        <form action="AskProcurement.jsp" method="post" id="frmAskProcurement" name="frmAskProcurement">
                            <table width="100%" cellspacing="10" cellpadding="0" border="0" class="formStyle_1 t_space">
                                <tr>
                                    <td width="16%" class="ff">User's Name:<span class="mendatory" >*</span></td>
                                    <td width="84%">
                                        <input type="text" name="txtuname" class="formTxtBox_1" id="txtuname" style="width:200px;" maxlength="100"  value="<%=session.getAttribute("userId") != null ? session.getAttribute("userName") : ""%>" <%=session.getAttribute("userId") != null ? " readonly " : ""%>/>
                                        <div class="reqF_1" id="valuname"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="16%" class="ff">Procurement Category : <span>*</span></td>
                                    <td width="84%">
                                        <select name="questionCategeory" class="formTxtBox_1" id="questionCategeory">
                                            <%

                                                        List<TblQuestionCategory> lst = askProcurementSrBean.getAllQusetions();
                                                        if (!lst.isEmpty()) {
                                                            Iterator it = lst.iterator();
                                                            while (it.hasNext()) {
                                                                TblQuestionCategory tblQuestionCategory = (TblQuestionCategory) it.next();

                                            %>
                                            <option value="<%=tblQuestionCategory.getQueCatId()%>"><%=tblQuestionCategory.getCatagory()%></option>
                                            <%
                                                            }
                                                        }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Post Query : <span>*</span></td>
                                    <td><textarea rows="5" id="question" name="question" class="formTxtBox_1" style="width:90%;"></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[

                                            //                                                CKEDITOR.replace( 'question',
                                            //                                                {
                                            //                                                    toolbar : "egpToolbar"
                                            //
                                            //                                                });
                                            //]]>
                                        </script>
                                        <div class="reqF_1" id="valQuestion"></div>
                                        <span class="reqF_1">${questions.question}</span>
                                    </td>
                                </tr>
                                <%if (session.getAttribute("userId") == null) {%>
                                <tr>
                                    <td class="ff">Verification Code : <span>*</span></td>
                                    <td>
                                        <%
                                            String pubKey = "6Lfw4r0SAAAAAO2WEc7snoFlSixnCQ2IGaO7k_iO";
                                            String privKey = "6Lfw4r0SAAAAAN_Pr3YGPA39kRIyNsuMCDJIpUsS";
                                            StringBuffer reqURL = request.getRequestURL();
                                            if (reqURL.toString().substring(reqURL.toString().indexOf("//") + 2, reqURL.toString().lastIndexOf("/")).equals("www.eprocure.gov.bd")) {
                                                pubKey = "6Lc0ssISAAAAAKh369J1vrGKmSfpEGhbr07sl1gS";
                                                privKey = "6Lc0ssISAAAAAJf6jPcfSBElxYSRk1LB46MBsWUk";
                                            }
                                            ReCaptcha c = ReCaptchaFactory.newReCaptcha(pubKey, privKey, false);
                                            out.print(c.createRecaptchaHtml(null, null));
                                            pubKey = null;
                                            privKey = null;
                                            reqURL = null;
                                        %>
                                        <span class="formNoteTxt">(If you cannot read the text, you may get new Verification Code by clicking <img alt="refresh"  src="resources/images/refresh.gif"/>, and if you want to hear the Verification Code, please click <img alt="sound"  src="resources/images/sound.gif"/> )</span>
                                        <span class="reqF_1" id="vericode"></span>
                                    </td>
                                </tr>
                                <%}%>
                            </table>

                            <div class="t-align-center">
                                <label class="formBtn_1"><input name="submit" type="submit" value="Submit" onclick="return Validate();"/></label>
                            </div>
                            <div class="t-align-left t_space">
                                <label class="mandatory">Important Note: Please note that posting of any issue which is not related to Procurement or use of abusive language can lead to debarment & deactivation of your account with or without penalty.</label>
                            </div>
                        </form>

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>