<%--
    Document   : ViewNegTendProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation</title>
<link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="dashboard_div">
    <%@include  file="AfterLoginTop.jsp" %>
    <div class="contentArea">
  <!--Dashboard Header Start-->
   
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
   <%
        int tenderId =0;
        if(request.getParameter("tenderId") != null)
            tenderId= Integer.parseInt(request.getParameter("tenderId"));

        pageContext.setAttribute("tenderId", tenderId);
   %>
  <div class="pageHead_1">
        Negotiation
        <span style="float:right;">
        <a href="<%=request.getContextPath()%>/tenderer/NegoTendererProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
       </span>
  </div>
  <%@include file="../../resources/common/TenderInfoBar.jsp" %>
  <%boolean negDebar=false; 
  if(userTypeId == 2){ %>
  <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../../tenderer/TendererTabPanel.jsp" %>
      <div class="tabPanelArea_1">
      <jsp:include page="../../tenderer/EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="4" />
        <jsp:param name="tenderId" value="<%=tenderId%>" />
      </jsp:include>
       
  <%
     negDebar=is_debared;
    }else{ %>
      <% pageContext.setAttribute("tab", "7");%>
        <%@include  file="../../officer/officerTabPanel.jsp"%>
        <%-- Start: Common Evaluation Table --%>
    <%@include file="/officer/EvalCommCommon.jsp" %>
    <%-- End: Common Evaluation Table --%>
        <div class="tabPanelArea_1">
        <%  pageContext.setAttribute("TSCtab", "7"); %>
        <%@include file="AfterLoginTSC.jsp" %>
  <%} %>
  <%if(!negDebar){%>
  <div class="tabPanelArea_1">
             <%
                int negId = 0;
                String companyName = "";
                String negMode = "";
                String startDate = "";
                String endDate = "";
                String details = "";
                if(request.getParameter("negId") != null)
                    negId= Integer.parseInt(request.getParameter("negId"));

                List<SPTenderCommonData> listBidderNegDetails = negotiationdtbean.getBidderNegDetailsById(tenderId, negId);

                if(!listBidderNegDetails.isEmpty()){
                    startDate = listBidderNegDetails.get(0).getFieldName2();
                    endDate = listBidderNegDetails.get(0).getFieldName3();
                    companyName = listBidderNegDetails.get(0).getFieldName4();
                    details = listBidderNegDetails.get(0).getFieldName5();
                    negMode = listBidderNegDetails.get(0).getFieldName7();
                }
             %>
            <table width="100%" cellspacing="0" class="tableList_1">
                <%if(userTypeId != 2){%>
                <tr>
                    <td class="t-align-left ff">Company Name :</td>
                    <td class="t-align-left"><%=companyName%></td>
                </tr>
                <%}%>
                <tr>
                    <td class="t-align-left ff">Negotiation Mode :</td>
                    <td><%=negMode%></td>
                </tr>
                <tr>
                    <td class="t-align-left ff" nowrap> Negotiation start date and time : </td>
                     <td><%=startDate%></td>
                </tr>
                <tr>
                     <td class="t-align-left ff" nowrap> Negotiation end date and time : </td>
                     <td><%=endDate%></td>
                </tr>
                <tr>
                    <td width="20%" class="t-align-left ff"> Invitation details :</td>
                    <td width="80%" class="t-align-left"><%=details%></td>
                </tr>               
            </table>
        </div><%}%>
   <div>&nbsp;</div>
      </div>
   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center">
         <%@include file="../../resources/common/Bottom.jsp" %>
   </div>
   <!--Dashboard Footer End-->
</div>
</div>
</body>
<script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
</html>