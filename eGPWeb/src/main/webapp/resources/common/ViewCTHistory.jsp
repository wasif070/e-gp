<%--
    Document   : CTHistory
    Created on : Sep 27, 2011, 4:00:34 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../../resources/js/jQuery/print/jquery.txt"></script>
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Termination History</title>
    </head>
    <%
                pageContext.setAttribute("TSCtab", "4");
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    lotId = request.getParameter("lotId");
                }
                String ContractTerminationId = "";
                if (request.getParameter("ContractTerminationId") != null) {
                    ContractTerminationId = request.getParameter("ContractTerminationId");
                }
                CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
                cmsCTReasonTypeBean.setLogUserId(userId);
                List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();

                CmsContractTerminationService cmsContractTerminationService =
                (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");
                TblCmsContractTermination  tblCmsContractTermination = cmsContractTerminationService.getCmsContractTermination(Integer.parseInt(ContractTerminationId));

                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId,tenderId);
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <div  id="print_area">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">View History
                                <span style="float: right; text-align: right;" class="noprint">
                                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/resources/common/CTHistory.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>" title="Delivery Schedule">Go Back</a>
                                </span>
                            </div>
                            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
                            <%@include file="../../resources/common/TenderInfoBar.jsp" %>
                            <%@include file="../../resources/common/ContractInfoBar.jsp" %>
                            <%
                                        pageContext.setAttribute("tab", "14");
                            %>
                                <div class="tabPanelArea_1 t_space">
                                 <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                   <%for(Object[] obj : list){%>
                                        <tr>
                                            <td width="20%">Lot No.</td>
                                            <td width="80%"><%=obj[0] %></td>
                                        </tr>
                                        <tr>
                                            <td>Lot Description</td>
                                            <td class="t-align-left"><%=obj[1]%></td>
                                        </tr>
                                        <%}%>
                                 </table>                                 
                                 <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                <tr id="tblRow_dateOfTermination">
                                    <td class="ff" width="16%">Date of Termination : </td>
                                    <td><%=DateUtils.gridDateToStrWithoutSec(tblCmsContractTermination.getDateOfTermination())%> </td>
                                </tr>
                                <tr id="tblRow_typeOfReason">
                                    <td class="ff">Type of Reason : </td>
                                    <td>
                                        <%
                                                    String[] reasonTypes = tblCmsContractTermination.getReasonType().split("\\^");
                                                    for (String reasonTypeId : reasonTypes) {
                                                        int reasonId = Integer.parseInt(reasonTypeId);
                                                        for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                                            if (cmsCTReasonType.getCtReasonTypeId() == reasonId) {
                                                                out.print(cmsCTReasonType.getCtReason() + "<br>");
                                                                break;
                                                            }
                                                        }//inner for
                                                    }// outer for
                                        %>
                                    </td>
                                </tr>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Status : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getStatus()%>
                                    </td>
                                </tr>                                
                                <%if(tblCmsContractTermination.getPaySetAmt()!=null && !"0.000".equalsIgnoreCase(tblCmsContractTermination.getPaySetAmt().toString())){%>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Payment Settlement Amount (In Nu.) :</td>
                                    <td>
                                        <%=tblCmsContractTermination.getPaySetAmt().setScale(3,0)%>
                                    </td>
                                </tr>
                                <%}%>
                                <%if(tblCmsContractTermination.getPaySetAmtRemarks()!=null && !"".equalsIgnoreCase(tblCmsContractTermination.getPaySetAmtRemarks())){%>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Remarks for Payment Settlement Amount :</td>
                                    <td>
                                        <%= tblCmsContractTermination.getPaySetAmtRemarks()%>
                                    </td>
                                </tr>
                                <%}%>
                                <%if(tblCmsContractTermination.getReleasePg()!=null && !"".equalsIgnoreCase(tblCmsContractTermination.getReleasePg())){%>
                                <tr id="tblRow_releasePerformanceGuarantee" >
                                    <td class="ff">Release Performance Security : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getReleasePg()%>
                                    </td>
                                </tr>
                                <%}%>
                                <tr id="tblRow_reason">
                                    <td  class="ff">Reason For Termination : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getReason()%>
                                    </td>
                                </tr>
                            </table>
                                </div>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End--></div>
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>

