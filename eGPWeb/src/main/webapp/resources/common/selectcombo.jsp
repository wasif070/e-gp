<%-- 
    Document   : selectcombo
    Created on : Feb 6, 2011, 4:31:30 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.model.table.TblListBoxDetail"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ComboSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            String id = "";
            if(request.getParameter("id")!=null){
                id = request.getParameter("id");
            }

            String rowcount = "";
            if(request.getParameter("row")!=null){
                rowcount = request.getParameter("row");
            }

            String formId = "";
            if(request.getParameter("formId")!=null){
                formId = request.getParameter("formId");
            }
        %>
        <%
            ComboSrBean cmbSrBean = new ComboSrBean();
            List<TblListBoxMaster> listcombo = cmbSrBean.getListBoxMaster(Integer.parseInt(formId));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Select Combo Box</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script language="javascript">
            function closewindow(){

                var checkedVal = $('input:radio[name=nameCombo]:checked').val();
                
//                var radioLength = document.frmselectcombo.nameCombo.length;
//                alert(radioLength);
//                var i=0;
//                var id;
//                var checkedVal = "";
//                for(var i=0;i<radioLength;i++){
//                    if(document.forms[0].nameCombo[i].checked){
//                        checkedVal = document.forms[0].nameCombo[i].value;
//                    }
//                }
                
                window.opener.document.getElementById("comboValue<%=id%>").value = checkedVal;

                <%
                    if(id.indexOf("_")!= -1){
                        
                    }
                    else
                    {
                %>
                        for(i=1; i <= <%=rowcount%>;i++)
                        {
                            var newId = parseInt(<%=id%>) + 1;
                            id = "comboValue"+ i +"_"+newId;
                            window.opener.document.getElementById(id).value = checkedVal;
                        }
                <%  }
                %>
                this.close();
            }
        </script>
    </head>
    <body>
        
        <div class="dashboard_div">
            <!--Dashboard Content Part Start-->
            <div>&nbsp;</div>
            <div class="contentArea_1">
                <form name="frmselectcombo" id="idfrmselectcombo" method="post" action="">
                <div class="pageHead_1">Select Combo</div>
                   <table width="300" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="10%" class="t-align-left"></th>
                        <th class="t-align-left" width="40%">Combo Name</th>
                        <th class="t-align-left" width="40%"></th>
                    </tr>
                    <% if(listcombo.size() > 0){
                            for(TblListBoxMaster tblListBoxMaster : listcombo){
                                List<TblListBoxDetail> listBoxDetail = cmbSrBean.getListBoxDetail(tblListBoxMaster.getListBoxId());
                     %>
                    <tr>
                        <td class="t-align-center"><input type="radio" name="nameCombo" id="idCombo<%=tblListBoxMaster.getListBoxId()%>" value="<%=tblListBoxMaster.getListBoxId()%>" /> </td>
                        <td class="t-align-left"><%=tblListBoxMaster.getListBoxName()%> </td>
                        <td class="t-align-left">
                            <select id="idcombodetail" name="namecombodetail<%=tblListBoxMaster.getListBoxId()%>" class="formTxtBox_1" style="width: 100%">
                                <option value="">--Select--</option>
                                <% for(TblListBoxDetail tblListBoxDetail:listBoxDetail){ %>
                                    <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                <% } %>
                            </select>
                        </td>
                    </tr>
                    <%     }
                       }else{ %>
                    <tr>
                        <td class="t-align-left" colspan="3">No Records found</td>
                    </tr>
                    <% } %>
                </table>
                <% if(listcombo.size() > 0){ %>
                <div class="t_space" align="center" style="width: 300px;">
                     <label class="formBtn_1">
                         <input type="button" name="btnsubmit" id="idbtnsubmit" value="Submit" onclick="closewindow();" />
                    </label>
                </div>
                <% } %>
                </form>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>