<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
   <body>
   <%
    
         int uid = 0;
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
       }


   // here i have added static uid if it has in session then
   // delete below line and enable above two lines.
     //int uid = 1;
     MessageProcessSrBean mps = new MessageProcessSrBean();
     mps.setLogUserId(""+uid);

     /**********/
     int archiveFolderId = mps.createDefaultArchiveFolder(uid);
    /***********/

     List folderData = mps.getFolderDataByUserId(uid);
     request.setAttribute("fdata", folderData);

  String contextPath = "";
  HttpSession sn = request.getSession();
            if ((sn.getAttribute("userId") == null || sn.getAttribute("userName") == null || sn.getAttribute("userTypeId") == null)) {
                response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp"); //Index.jsp
            }
            CommonService commonService_media = (CommonService) AppContext.getSpringBean("CommonService");
            contextPath = request.getContextPath();
     long totalrecievedmsgs = 0;
     long totalunreadmsgs = 0;
     totalrecievedmsgs =  mps.getTotalRecievedMsgs("TblMessageInBox", "msgToUserId ="+uid+" and msgStatus='Live' and folderId= 0 AND msgStatus<>'trash'");
     totalunreadmsgs = mps.getTotalUnreadMsgs("TblMessageInBox", "msgToUserId ="+uid+" and isMsgRead='No' and msgStatus='Live' and folderId= 0 and msgStatus<>'trash'");

   %>
<ul class="tabPanel_1 t_space" id="tabForApproved">
                 <%--   <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu">Pending</a></li>
                    <li><a href="javascript:void(0);" id="liveTab" onclick="changeTab(2);">Processing</a></li>
                    <li><a href="javascript:void(0);" id="processingTab" onclick="changeTab(3);">Approved</a></li>
--%>
<li><a id="pendingTab" <%if (request.getParameter("viewtype").equalsIgnoreCase("pending")){%>class="sMenu" <%}%> href="<%=contextPath%>/resources/common/PaDashboard.jsp?viewtype=pending">Pending task</a></li>
                    <li><a id="processingTab" <%if (request.getParameter("viewtype").equalsIgnoreCase("processed")){%>class="sMenu"<%}%> href="<%=contextPath%>/resources/common/PaDashboard.jsp?viewtype=processed">Processed task</a></li>
                    <li><a id="ApprovedTab" <%if (request.getParameter("viewtype").equalsIgnoreCase("defaultworkflow")){%>class="sMenu"<%}%> href="<%=contextPath%>/resources/common/PaDashboard.jsp?viewtype=defaultworkflow">Approved Workflow</a></li>
        </ul>
</body>