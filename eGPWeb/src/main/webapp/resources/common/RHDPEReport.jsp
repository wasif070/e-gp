<%-- 
    Document   : index
    Created on : Feb 16, 2012, 12:17:28 PM
    Author     : shreyansh.shah
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationPEDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.RHDIntegrationPEDetailsService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RHD PE Report</title>
    </head>
    <body>

        <!--Header Table-->

        <noscript>
            <meta http-equiv="refresh" content="">
        </noscript>


        <%
                    Boolean isexport = false;
                    if (request.getParameter("Submit") != null) {
                        response.setContentType("application/vnd.ms-excel");
                        response.setHeader("content-disposition", "attachment; filename=RHDPEReport.xls");
                        isexport = true;
                    }
        %>

        <form name="frmRHDPE" action="">
            <table border="1"    cellpadding="0" cellspacing="0" width="100%" style="margin-top:21px;" >
                <%
                            if (isexport == false) {
                %>
                <tr>
                    <td align="left" colspan="51"> <input type="Submit" value="Export To Excel" name="Submit">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
                <tr  align="center">
                    <td align="center" bgcolor="#CCFF99">PE Id</td>
                    <td align="center" bgcolor="#CCFF99">PE Name</td>
                    <td align="center" bgcolor="#CCFF99">Office Address</td>
                    <td align="center" bgcolor="#CCFF99">RHD Org Name</td>
                    <td align="center" bgcolor="#CCFF99">Dzongkhag / District Name</td>

                </tr>
                <%
                            List<String> requiredRHDIntegrationDetailsList = new ArrayList<String>();
                            List<RHDIntegrationPEDetails> RHDIntegrationDetailsList = new ArrayList<RHDIntegrationPEDetails>();
                            RHDIntegrationPEDetailsService RHDIntegrationDetailsService =
                                    (RHDIntegrationPEDetailsService) AppContext.getSpringBean("RHDIntegrationPEDetailsService");
                            RHDIntegrationDetailsList = RHDIntegrationDetailsService.getSharedPEDetailForRHD();
                            if (RHDIntegrationDetailsList != null && RHDIntegrationDetailsList.size() > 0) {
                                for (RHDIntegrationPEDetails rhdIntegrationDetails : RHDIntegrationDetailsList) {
                %>
                <tr>
                    <td align="center" > <%=rhdIntegrationDetails.getPEId()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPEOfficeName()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getOfficeAddr()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getRHDOrgName()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getDistrictName()%></td>

                </tr>
                <%
                            }//end FOR loop
                        }//end IF condition
                              else {%>
                <tr> <td colspan="5" align="center">No Records Found</td></tr>
                <% }



                %>
                <%
                            if (isexport == false) {
                %>
                <tr>
                    <td align="left" colspan="51"> <input type="Submit" value="Export To Excel" name="Submit">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
            </table>
        </form>
    </body>
</html>
