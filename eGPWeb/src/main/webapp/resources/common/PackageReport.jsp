<%--
    Document   : PackageReport
    Created on : Nov 9, 2010, 11:31:20 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
    int totalNoofDays = 0;
    StringBuilder userType = new StringBuilder();
    if (request.getParameter("hdnUserType") != null) {
        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
            userType.append(request.getParameter("hdnUserType"));
        } else {
            userType.append("org");
        }
    } else {
        userType.append("org");
    }
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            String idType = "appId";
            int auditId = Integer.parseInt(request.getParameter("appId"));
            String auditAction = "View Package Report";
            String moduleName = EgpModule.APP.getName();
            String remarks = "User id:" + session.getAttribute("userId") + " has view APP Package Report for Package id: " + request.getParameter("pkgId") + " and App id: " + request.getParameter("appId");
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>View APP Package Report : </title>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--
                <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
                <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
                <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
                <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
                <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>        
    </head>
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%--<jsp:include page="AfterLoginLeft.jsp" ></jsp:include>--%>
                        <td class="contentArea_1">
                            <jsp:useBean id="appViewPkgDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
                            <%    String strAppId = request.getParameter("appId");
                                String strPkgId = request.getParameter("pkgId");
                                String strPkgType = request.getParameter("pkgType");
                                int appId = 0;
                                int pkgId = 0;
                                if (strAppId != null && !strAppId.equals("")) {
                                    appId = Integer.parseInt(strAppId);
                                }
                                if (strPkgId != null && !strPkgId.equals("")) {
                                    pkgId = Integer.parseInt(strPkgId);
                                }

                                SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
                                //out.write("AppId: " + appId + "Package Id:" + pkgId);

                            %>
                            <div class="t_space" align="center">
                                <div id="print_area" style="text-align: left;">
                                    <div class="pageHead_1" align="left">APP Package Report<span style="float:right;" class="noprint">
                                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                            <a href="<%=request.getContextPath()%>/officer/APPDashboard.jsp?appID=<%=appId%>" class="action-button-goback">Go Back to Dashboard</a></span></div>
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="930px">
                                        <%
                                            AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                                            if (!((appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = " + appId + " and packageId = " + pkgId)) > 0)) {
                                                strPkgType = "AppPackage";
                                            } else {
                                                strPkgType = "Package";
                                            }
                                            if (appId != 0 && pkgId != 0) {

                                                appViewPkgDtBean.populateInfo(appId, pkgId, strPkgType, 0);
                                                if (appViewPkgDtBean.isDataExists()) {

                                                    List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                                    CommonAppData appData = new CommonAppData();
                                                    appListDtBean = appServlet.getAPPDetails("GetAppMinistry", String.valueOf(appId), "");
                                                    if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                        appData = appListDtBean.get(0);
                                                    }
                                                    TenderCommonService commonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> details = commonService.returndata("GetAppDetailById", appData.getFieldName6(), "0");
                                        %>
                                        <tr>
                                            <td style="width: 130px" class="ff" align="left">Hierarchy Node: </td>
                                            <td style="width: 287px" align="left" colspan="3"><%=details.get(0).getFieldName1()%></td>
                                        </tr>
                                        <%--<tr>
                                            <td class="ff" align="left">Division :</td>
                                            <td align="left" colspan="3"><%=details.get(0).getFieldName2()%></td>
                                        </tr>--%>
                                        <tr>
                                            <td class="ff" align="left">Organization :</td>
                                            <td align="left"><%=appData.getFieldName3()%></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" align="left">PA Office & Code :</td>
                                            <td align="left"><%=appData.getFieldName4()%></td>

                                        </tr>
                                        <tr>
                                            <td class="ff" align="left" style="width: 130px" >Budget Type :</td>
                                            <td align="left" style="width: 287px" >${appViewPkgDtBean.budgetType}</td>
                                            <td class="ff" align="left" style="width: 130px" >Project Name :</td>
                                            <td align="left" style="width: 287px" >${appViewPkgDtBean.projectName}</td>
                                        </tr>
                                    </table>
                                    <div>&nbsp;</div>
                                    <table width="100%" cellspacing="0" class="tableList_1">
                                        <tr>
                                            <th class="t-align-center">Package No.</th>
                                            <th class="t-align-center">Package Description</th>
                                            <th class="t-align-center">Procurement Method and<br /> Type</th>
                                            <th class="t-align-center">Contract Approving Authority</th>
                                            <th class="t-align-center">Source of Fund</th>
                                            <th class="t-align-center">Est. Cost <br />(In Nu.) </th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-center">${appViewPkgDtBean.packageNo}</td>
                                            <td class="t-align-center">
                                                <%
                                                    if (appViewPkgDtBean.getPkgLotDeail().size() > 0) {
                                                %>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                    <tr>
                                                        <th class="t-align-center"><% if ("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())) { %>Package No. <% } else { %>Lot No.<% } %> </th>
                                                        <th class="t-align-center"><% if ("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())) { %>Package Description <% } else { %>Lot Description<% } %></th>
                                                        <th class="t-align-center">Qty</th>
                                                        <th class="t-align-center">Unit</th>
                                                        <th class="t-align-center">Estimated Cost<br/>(In Nu.)</th>
                                                    </tr>
                                                    <%
                                                        for (int i = 0; i < appViewPkgDtBean.getPkgLotDeail().size(); i++) {
                                                    %>
                                                    <tr>
                                                        <td class="t-align-center"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotNo()%></td>
                                                        <td class="t-align-left"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotDesc()%></td>
                                                        <td class="t-align-center"><% if ("0.00".equalsIgnoreCase(appViewPkgDtBean.getPkgLotDeail().get(i).getQuantity().toString())) { %>-<%} else {
                                                                out.println(appViewPkgDtBean.getPkgLotDeail().get(i).getQuantity());
                                                            } %></td>
                                                        <td class="t-align-center"><% if (!"".equalsIgnoreCase(appViewPkgDtBean.getPkgLotDeail().get(i).getUnit())) {
                                                                out.println(appViewPkgDtBean.getPkgLotDeail().get(i).getUnit());
                                                            } else {
                                                                out.println("-");
                                                            }%></td>
                                                        <td class="t-align-center"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotEstCost().setScale(2, 0)%></td>
                                                    </tr>
                                                    <% } %>                                              
                                                </table>
                                                <%
                                                } else {
                                                %>
                                                <label class="ff">No Data Found</label>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <%
                                                String nature = appViewPkgDtBean.getProcurementnature();
                                                String isPQReqd = appViewPkgDtBean.getIsPQRequired();
                                                String methodName2 = appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId());
                                                String methodName = "";
                                                if(methodName2.equalsIgnoreCase("RFQ"))
                                                    methodName = "LEM";
                                                if(methodName2.equalsIgnoreCase("DPM"))
                                                    methodName = "DCM";
                                                else 
                                                    methodName = methodName2;
                                                String REOreq = appViewPkgDtBean.getReoiRfaRequired();

                                                String type = "";
                                                if (isPQReqd.equalsIgnoreCase("Yes")) {
                                                    type = "Tender/Proposal";
                                                }
                                                if (methodName.equalsIgnoreCase("RFQ") || methodName.equalsIgnoreCase("RFQU") || methodName.equalsIgnoreCase("RFQL")) {
                                                    type = "RFQ";
                                                } else {
                                                    type = "Tender/Proposal";
                                                }
                                            %>
                                            <td class="t-align-center"><%=methodName%> - <%if (appViewPkgDtBean.getProcurementType().equalsIgnoreCase("ICT")) {
        out.print("ICB");
    } else if (appViewPkgDtBean.getProcurementType().equalsIgnoreCase("NCT")) {
        out.print("NCB");
    } %></td>
                                            <!--Edit by aprojit -->
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getAaEmpId() != null) {
        if (appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()).equalsIgnoreCase("PE")) {
            out.print("PE");
        } else if (appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()).equalsIgnoreCase("HOPE")) {
            out.print("HOPA");
        } else {
            out.print(appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()));
        }
    }%></td>
                                            <td class="t-align-center">${appViewPkgDtBean.sourceOfFund}</td>
                                            <td class="t-align-center"><%=appViewPkgDtBean.getPkgEstCost().setScale(2, 0)%></td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="0" class="tableList_1">
                                        <tr>
                                            <th class="t-align-center">Key Date</th>
                                            <th class="t-align-center">Planned Date</th>
                                            <th class="t-align-center">Planned Days</th>
                                            <th class="t-align-center">Actual Date</th>
                                            <!--                                        <th class="t-align-center">Delay (in No. of Days)</th>-->
                                        </tr>
                                        <!-- TSTM Method-->
                                        <%
                                            if ("TSTM".equalsIgnoreCase(methodName)) {
                                        %>
                                        <tr>
                                            <th class="table-section-header" colspan="4">1st Stage</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Advertisement of IFB on e-GP website</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getAdvtDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.advtDays}  <% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                            } %> </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actAdvtDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of submission of Tenders</td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDays} <% if (appViewPkgDtBean.getSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Opening of Tenders</td>
                                            <td class="t-align-center">${appViewPkgDtBean.openDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.openDays} <% if (appViewPkgDtBean.getOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getOpenDays();
                                            }%></td>
                                            <td class="t-align-center"> </td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Evaluation Report</td>
                                            <td class="t-align-center">${appViewPkgDtBean.evalRptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.evalRptDays} <% if (appViewPkgDtBean.getEvalRptDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getEvalRptDays();
                                            } %></td>
                                            <td class="t-align-center"> </td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval of the Evaluation Report</td>
                                            <td class="t-align-center">${appViewPkgDtBean.appLstDt}</td>
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center"></td>
                                        </tr>
                                        <tr>
                                            <th class="table-section-header" colspan="4">2nd Stage</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of issue of Final Tender Document to qualified bidders</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getTenderAdvertDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderAdvertDays} <% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderAdvertDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Tender</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDays} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Opening of Tender</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDays} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderOpenDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Evaluation Report</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptDt} </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptdays} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                            }%></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderEvalRptDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval of the Evaluation Report</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptAppDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptAppDays} <% if (appViewPkgDtBean.getTenderEvalRptAppDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptAppDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderEvalRptDt}</td>
                                        </tr>
                                        <!--Code by Proshanto-->
                                        <tr>
                                            <td class="t-align-left">Expected Date of Letter of Intent to Award</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderLetterIntentDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tnderLetterIntentDays} <% if (appViewPkgDtBean.getTnderLetterIntentDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTnderLetterIntentDays();
                                            } %></td>
                                            <!--<td class="t-align-center">${appViewPkgDtBean.actTenderNoaIssueDt}</td>-->
                                            <td class="t-align-center">-</td>
                                        </tr>
                                        <!--End-->
                                        <tr>
                                            <td class="t-align-left">Expected Date of Issue of Letter of Acceptance (LOA)</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderNoaIssueDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderNoaIssueDays} <% if (appViewPkgDtBean.getTenderNoaIssueDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderNoaIssueDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderNoaIssueDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Signing of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDays} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays(); } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractSignDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Completion of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            <td class="t-align-center"> - </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractCompDt}</td>
                                        </tr>
                                        <!-- TSTM End-->
                                        <!-- REOI Start-->
                                        <%
                                        } else if ("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired()) || "RFP".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) {
                                        %>
                                        <%
                                            String strprocLabel = "";
                                            if ("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) {
                                                strprocLabel = "RFA";
                                            } else {
                                                strprocLabel = "RFP";
                                            }
                                        %>
                                        <tr  class="tableHead_1 t_space">
                                            <td class="t-align-left"><%=strprocLabel%> Dates:</td>
                                            <td class="t-align-center"> </td>
                                            <td class="t-align-center">   </td>
                                            <td class="t-align-center">     </td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Advertisement of <%=strprocLabel%> on e-GP Website</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getRfaAdvertDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getRfaAdvertDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.rfaAdvertDays} <% if (appViewPkgDtBean.getRfaAdvertDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaAdvertDays();
                                            } %> </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaAdvertDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Receipt of Application</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaReceiptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaReceiptDays} <% if (appViewPkgDtBean.getRfaReceiptDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaReceiptDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaReceiptDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Evaluation of Application</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaEvalDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaEvalDays} <% if (appViewPkgDtBean.getRfaEvalDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaEvalDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaEvalDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Interview of Selected Individuals </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaInterviewDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaInterviewDays} <% if (appViewPkgDtBean.getRfaInterviewDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaInterviewDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaInterviewDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Evaluation of Final selection list </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaFinalSelDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaFinalSelDays} <% if (appViewPkgDtBean.getRfaFinalSelDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaFinalSelDays();
                                            } %> </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaFinalSelDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Submission of Evaluation Report </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaEvalRptSubDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaEvalRptSubDays} <% if (appViewPkgDtBean.getRfaEvalRptSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaEvalRptSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaEvalRptSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Date of Approval of Consultants </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfaAppConsultantDt}</td>
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfaAppConsultantDt}</td>
                                        </tr>
                                        <!-- RFA End-->
                                        <!-- RFP Start-->
                                        <%
                                        } else if ("REOI".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) {
                                        %>
                                        <tr  class="tableHead_1 t_space">
                                            <td class="t-align-left">REOI Dates:</td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Advertisement of REOI on e-GP website</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getAdvtDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.advtDays}<% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                            } %> </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actAdvtDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Last Date of Receipt of EOI</td>
                                            <td class="t-align-center">${appViewPkgDtBean.reoiReceiptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.reoiReceiptDays} <% if (appViewPkgDtBean.getReoiReceiptDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getReoiReceiptDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actReoiReceiptDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Recommended Short-listed Firm </td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDays} <% if (appViewPkgDtBean.getSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getSubDays();
                                            }%></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval of Recommended Shortlisted Firm </td>
                                            <td class="t-align-center">${appViewPkgDtBean.appLstDt}</td>
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center">-</td>
                                        </tr>
                                        <tr  class="tableHead_1 t_space">
                                            <td class="t-align-left">RFP Dates:</td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Issue of RFP </td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getTenderAdvertDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.tenderAdvertDays}<% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                            } %> </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderAdvertDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Proposal</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDays} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Technical Proposal Opening</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDays} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderOpenDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Technical Proposal Evaluation</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpTechEvalDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpTechEvalDays} <% if (appViewPkgDtBean.getRfpTechEvalDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpTechEvalDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfpTechEvalDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Financial Proposal Opening </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpFinancialOpenDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpFinancialOpenDays} <% if (appViewPkgDtBean.getRfpFinancialOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpFinancialOpenDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfpFinancialOpenDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Combined Evaluation Report </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptdays} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.acttenderEvalRptAppDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval of Combined Evaluation Report </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptAppDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptdays} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.acttenderEvalRptAppDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Completion of Negotiation</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpNegCompDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpNegCompDays} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfpNegComDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval for Award of Contract </td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpContractAppDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.rfpContractAppDays} <% if (appViewPkgDtBean.getRfpContractAppDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpContractAppDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actRfpContractAppDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Signing of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDays} <% if (appViewPkgDtBean.getTenderContractSignDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractSignDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Completion of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractCompDt}</td>
                                        </tr>
                                        <!-- RFP End-->
                                        <%
                                        } else {
                                            if ("Yes".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) {
                                        %>
                                        <tr  class="tableHead_1 t_space">
                                            <td class="t-align-left">PQ Dates:</td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                            <td class="t-align-center"></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Advertisement of Invitation on e-GP website</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getAdvtDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.advtDays} <% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actAdvtDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Applications Submission</td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.subDays} <% if (appViewPkgDtBean.getSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date Of Submission Of Evaluation Report With Recommended List</td>
                                            <td class="t-align-center">${appViewPkgDtBean.evalRptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.evalRptDays} <% if (appViewPkgDtBean.getEvalRptDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getEvalRptDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actEvalRptDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval Of List</td>
                                            <!--Code by Proshanto Kumar Saha-->
                                            <td class="t-align-center">${appViewPkgDtBean.appLstDt}</td>
                                            <!--Code End-->
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center">${appViewPkgDtBean.actAppLstDt}</td>
                                        </tr>
                                        <%
                                            }
                                            if ((!"".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired()) && !"TSTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || (("".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) && (("OTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("OSTETM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("DPM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) && "".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) || ("LTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) && "".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) || ("RFQL".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQU".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQ".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())))))) {
                                        %>
                                        <tr  class="tableHead_1 t_space">
                                            <td class="t-align-left"><%=type%> Dates:</td>
                                            <td class="t-align-center"> </td>
                                            <td class="t-align-center">   </td>
                                            <td class="t-align-center">     </td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Advertisement of IFB on e-GP website</td>
                                            <td class="t-align-center"><% if (appViewPkgDtBean.getTenderAdvertDt() != null) {
                                                out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                            } %></td>
                                            <td class="t-align-center"> ${appViewPkgDtBean.tenderAdvertDays} <% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderAdvertDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Tenders</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderSubDays} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderSubDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Opening of Tenders</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderOpenDays} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderOpenDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Submission of Evaluation Report</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderEvalRptdays} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                            }  %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderEvalRptDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Approval for Award of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractAppDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractAppDays} <% if (appViewPkgDtBean.getTenderContractAppDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractAppDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractAppDt}</td>
                                        </tr>
                                        <!--Code by Proshanto Kumar Saha-->
                                        <tr>
                                            <td class="t-align-left">Expected Date of Letter of Intent to Award </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderLetterIntentDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tnderLetterIntentDays} <% if (appViewPkgDtBean.getTnderLetterIntentDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTnderLetterIntentDays();
                                            } %></td>
                                            <!--<td class="t-align-center">${appViewPkgDtBean.actTenderNoaIssueDt}</td>-->
                                            <td class="t-align-center">-</td>
                                        </tr>
                                        <!--End-->

                                        <tr>
                                            <td class="t-align-left">Expected Date of Issuance of the Letter of Acceptance (LOA) </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderNoaIssueDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderNoaIssueDays} <% if (appViewPkgDtBean.getTenderNoaIssueDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderNoaIssueDays();
                                            } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderNoaIssueDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Signing of Contract </td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDt}</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractSignDays} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays(); } %></td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractSignDt}</td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left">Expected Date of Completion of Contract</td>
                                            <td class="t-align-center">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            <td class="t-align-center"> - </td>
                                            <td class="t-align-center">${appViewPkgDtBean.actTenderContractCompDt}</td>
                                        </tr>
                                        <%
                                                }
                                            }
                                        } else {
                                        %>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <div class="responseMsg errorMsg">Package Details NOT Available</div></td>
                                        </tr>
                                        <%
                                            }
                                        } else {
                                        %>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <div class="responseMsg errorMsg">Provide Valid App-Id, Package-Id * Packgae Type</div></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td>Total Time to Contract Signing</td>
                                            <td class="t-align-center">-</td>
                                            <td class="t-align-center"><%=totalNoofDays%></td>
                                            <td class="t-align-center">-</td>
                                        </tr>
                                    </table>
                                    <div>&nbsp;</div>
                                </div>
                                <!-- <label class="formBtn_1">
                                     <input name="print" id="print" type="button" value="Print" class="button" />
                                 </label> -->
                                <script type="text/javascript">
                                    $(document).ready(function () {

                                        $("#print").click(function () {
                                            //alert('sa');
                                            printElem({overrideElementCSS: false, leaveOpen: true, printMode: 'popup'});
                                        });

                                    });
                                    function printElem(options) {
                                        //alert(options);

                                        $('#print_area').printElement(options);
                                        //$('#trLast').hide();
                                    }

                                </script>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabApp");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
