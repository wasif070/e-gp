<%-- 
    Document   : Dashboard
    Created on : Apr 20, 2016, 12:17:15 PM
    Author     : Nishith
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblActivityMaster"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.web.servlet.PendingProcessedServlet" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <!-- <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
       <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />-->
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <title>Dashboard</title>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <!--aprojit-Start -->
    <script type="text/javascript"> 
        function myalert(){
            var msg = "<%=request.getParameter("msg")%>";
            if(msg != "null"){
                if(msg == "hitsuccess"){
                    jAlert("Hint Question and Answer Changed Sucessfully","Sucessfull", function(RetVal) {
                                   });   
                }
            }
        }
       </script>
    <!--aprojit-End --> 
    <body onload="myalert();">  
        <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="AfterLoginTop.jsp" %>
                <div align="center" style="margin-bottom: 200px;">
                    <div style="width:50%; padding:20px;border: 1px solid #FF9326" >                        
                        <h1> WELCOME </h1><br/>
                        
                        <p style="line-height:20px; font-size: 14px;"> Electronic Government Procurement (e-GP) System<br/>
                        Government Procurement and Property Management Division (GPPMD)<br/>
                            Department of National Properties<br/>
                            Ministry of Finance<br/>
                            Royal Government of Bhutan<br/>

                       </p>
                    
                    </div>
                   
                </div>
                <div style="margin-top: 350px;">
                     <jsp:include page="Bottom.jsp" ></jsp:include>
                </div>
            </div>
            <% 
            if (userTypeId == 1){ %>
             <script>
                        var headSel_Obj = document.getElementById("headTabDashboard");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                        }
                    </script>
           <%  } %>
    </body>
</html>
