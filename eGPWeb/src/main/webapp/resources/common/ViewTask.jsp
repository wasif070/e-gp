<%-- 
    Document   : ViewTask
    Created on : Nov 25, 2010, 11:39:01 AM
    Author     : test
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblToDoList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AddTaskSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
 
        <title>View Task</title>
    </head>
    <body>
        <%
            String taskid = request.getParameter("taskid");
            String datefrom=request.getParameter("DateFrom");
            String dateto=request.getParameter("DateTo");
            String status=request.getParameter("select");
            AddTaskSrBean taskSrBean=new AddTaskSrBean();
            String logUserId="0";
            if(session.getAttribute("userId")!=null){
                logUserId=session.getAttribute("userId").toString();
                taskSrBean.setLogUserId(logUserId);
            }
            TblToDoList tbltodoList = null;
             List<TblToDoList> todoList = null;
             todoList = taskSrBean.getTasksBytaskId(Integer.parseInt(taskid));
               if(todoList.size()>0){
            tbltodoList = todoList.get(0);
            request.setAttribute("tbltodoList", tbltodoList);

             if("Complete".equals(request.getParameter("complete"))){
                  tbltodoList.setIsComplete("Yes");
                    taskSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    taskSrBean.updateTask(tbltodoList);
                    response.sendRedirect("ViewTaskDetails.jsp?DateFrom="+datefrom+"&DateTo="+dateto+"&select=Pending&reply=completed");
                    
                 }
           

            %>

            <div class="topHeader">
             <%@include file="AfterLoginTop.jsp" %>
          </div>
          <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav"><ul class="lftLinkBtn_1"></ul>
       <!-- Left content of the page -->
      <jsp:include page="MsgBoxLeft.jsp" ></jsp:include>
	</td>
        
        <td class="contentArea"><div class="pageHead_1"><label id="headermsg">View Task</label>&nbsp;&nbsp;
               
            </div>


          
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
            <tr>
                <th width="20%" class="t-align-left">Task Brief</th>
                <td width="80%" class="t-align-left"><% out.write(tbltodoList.getTaskBrief()); %></td>
            </tr>
            <tr>
                 <th width="20%" class="t-align-left">Task Details</th>
                 <td width="80%" class="t-align-left"><% out.write(tbltodoList.getTaskDetails()); %></td>
            </tr>
            <tr>
                <th width="20%" class="t-align-left">Priority</th>
                <td width="80%" class="t-align-left"><% out.write(tbltodoList.getPriority()); %></td>
            </tr>
            <tr>
              <th width="20%" class="t-align-left">Start Date</th>
              <td width="80%" class="t-align-left"><%
                if(tbltodoList.getStartDate() != null){
                out.write(DateUtils.customDateFormate(tbltodoList.getStartDate())) ;
                }%></td>
            </tr>
            <tr>
              <th width="20%" class="t-align-left">End Date</th>
              <td width="80%" class="t-align-left"><%
                  if(tbltodoList.getEndDate()!= null){
                  out.write(DateUtils.customDateFormate(tbltodoList.getEndDate()));
                  }%></td>
            </tr>
            <% if(status.equals("Pending")){ %>
            <form action="ViewTask.jsp" method="post">
             <tr>
                <td colspan="2">
                    <label class="formBtn_1"><input type="submit" value="Complete" id="complete" name="complete" /></label>
                    <input  type="hidden" value="<%=taskid%>" name="taskid" id="taskid" />
                    <input  type="hidden" value="<%=datefrom %>" name="DateFrom" id="DateFrom" />
                    <input  type="hidden" value="<%=dateto %>" name="DateTo" id="DateTo" />
                    <input  type="hidden" value="<%=status%>" name="select" id="select" />
                    </td>
            </tr>
            </form>
            <% } } %>
          </table>
         </td>
          </tr>
  </table>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a hr;ef="#">Privacy Policy</a></td>
    </tr>
  </table>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>