<%--
    Document   : MarqueeDisplay.jsp
    Created on : Nov 18, 2010, 2:20:07 PM
    Author     : Kinjal Shah
               : For display Marquee in page
--%>

<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Marquee Display Page</title>
<!--        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />-->

    </head>
    <body>


        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="79%">
                    <%
                    String expiryDt="";
                    if(session.getAttribute("userId")!=null && session.getAttribute("userId")!=""){
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        List<TblLoginMaster> tblLoginMasters = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(session.getAttribute("userId").toString()));
                        TblLoginMaster tblLoginMaster = null;
                        Date expiryDate=null;
                        Calendar calExpiry= Calendar.getInstance() ,calCurrent= Calendar.getInstance();
                        if(tblLoginMasters!=null && (!tblLoginMasters.isEmpty())){
                            tblLoginMaster = tblLoginMasters.get(0);
                            if(tblLoginMaster.getTblUserTypeMaster().getUserTypeId()==2 && tblLoginMaster.getStatus().equalsIgnoreCase("approved")){
                            expiryDate = tblLoginMaster.getValidUpTo();
                            expiryDt=DateUtils.customDateFormate(expiryDate);
                            calExpiry.setTime(expiryDate);
                            calCurrent.add(Calendar.DATE, +30);
                                if(calExpiry.equals(calCurrent) || calExpiry.before(calCurrent)){%>
                                    <blink> <strong style="height:15px; line-height:15px;  color:#ff0000; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong> </blink>
                                <%}else{%>
                                    <strong style="height:15px; line-height:15px; color:#78A951; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong>
                <%}}}}%>
                    <marquee behavior=scroll scrollamount="5" style="height:25px; line-height:25px; color:#ff0000; font-size: 14px; font-weight: bold;">
                    <%
                    String lid = "";
                    String utypeid = "";

                    if (!request.getParameter("location").equals("") || request.getParameter("userType").equals("")) {
                        lid = request.getParameter("location");
                        utypeid = request.getParameter("userType");
                    }

                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueedisplay", lid, utypeid);
                    if (!list.isEmpty()) {

                    %>
                    <%=URLDecoder.decode(list.get(0).getFieldName1(),"UTF-8")%>
                    <%
                    }else {%>

                    <%}%>
                </marquee>
                </td>
                <td width="1%"></td>
                <td width="20%" align="right" >
                    <div class="ReadMore">
                        <%

                    String userName = "";
                    if (session.getAttribute("userName") != null) {
                        userName = session.getAttribute("userName").toString();
                    }

                    String strViewAllNoti = "";

                    if(request.getParameter("viewallnot")!=null && !"".equalsIgnoreCase(request.getParameter("viewallnot"))){
                        strViewAllNoti = request.getParameter("viewallnot");
                    }
        %>
                        <%if(userName==""){%>
                        <strong><a href="<%=request.getContextPath()%>/ViewMarquee.jsp"><%=strViewAllNoti%></a>&nbsp;</strong>
                    <%}else{%>
                    <strong><a href="<%=request.getContextPath()%>/resources/common/ViewMarqueeList.jsp"><%=strViewAllNoti%></a></strong>
                    <%}%>
                    </div>
                </td>
            </tr>
        </table>




</body>
</html>
