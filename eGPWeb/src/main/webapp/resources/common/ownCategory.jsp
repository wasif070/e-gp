<%--
    Document   : ownCategory
    Created on : Nov 15, 2010, 2:16:52 PM
    Author     : aprojit
--%>
<!--Code start By Proshanto Saha-->
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<!--Code end By Proshanto Saha-->
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            StringBuilder mode = new StringBuilder();
            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
            
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
            if (isLoggedIn) {
                mode.append("WatchList");
            } else {
                mode.append("Search");
            }
            StringBuilder userType = new StringBuilder();
            if (request.getParameter("hdnUserType") != null) {
                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                } else {
                    userType.append("org");
                }
            } else {
                userType.append("org");
            }
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advanced Search for Tenders</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../css/border-radius.css" />
        <script type="text/javascript" src="../js/jscal2.js"></script>
        <script type="text/javascript" src="../js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
                    function doBlink() {
            var blink = document.all.tags("BLINK")
            for (var i=0; i<blink.length; i++)
            blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : ""
        }

        function startBlink() {
            if (document.all)
            setInterval("doBlink()",1000)
        }
        </script>

    </head>
    <body onload="loadTable(); startBlink();hide();<%if (session.getAttribute("userId") == null) {%> chngTab(1); <% }%>">
        <div class="mainDiv">
            <%
                        if (isLoggedIn) {
            %>
            <div class="dashboard_div">
                <%@include file="AfterLoginTop.jsp" %> <%} else {%>
                <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include> <%}%>
                    <!--Middle Content Table Start-->
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea">
                                <!--Page Content Start-->

                                <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                                <!-- Start by Proshanto kumar saha-->
                                <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
                                <!--End by Proshanto kumar saha -->
                                <div class="t_space">
                                    <%
                                                Object objUsrTypeId = session.getAttribute("userTypeId");
                                                StringBuilder title = new StringBuilder("Advanced Search for Tenders");
                                                if (objUsrTypeId != null) {
                                                    title.delete(0, title.length());
                                                    title.append("Own Category Tenders");
                                                }
                                    %>
                                    <div class="pageHead_1"><%=title.toString()%>
                                    <%if(objUsrTypeId!=null && objUsrTypeId!=""){%>
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                                    <%}%>
                                    </div>
                                    <div class="formBg_1 t_space">
                                        <form id="alltenderFrm" action="" method="post">


                                            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tblSearchBox">
                                                <tr>
                                                    <td class="ff">Hierarchy Node:</td>
                                                      <td colspan="3"><input type="hidden" name="viewType" id="viewType" value="Live"/>
                                                    <%if (objUserId != null){if (Integer.parseInt(strUserTypeId) == 5) {
                                                    Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(objUserId.toString()));
                                                    out.print(objData[1].toString());
                                                    out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                    out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                    out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + Integer.parseInt(strUserTypeId) + "\"/>");
                                                    }else{%>
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>

                                             <% }} else {%>
                                                  
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td> <%}%>

                                                </tr>
                                                <tr>
                                                    <!--<td class="ff">Procuring Entity :</td>-->
                                                    <!--Change Procuring Entity to Procuring Agency by Proshanto Kumar Saha-->
                                                    <td class="ff">Procuring Agency :</td>
                                                    <td colspan="3">
                                                        <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:208px;">
                                                            <option value="">-- Select Office --</option>
                                                        <%
                                                            List<Object[]> cmbLists = null;
                                                            if(objUserId!=null){
                                                            if (Integer.parseInt(strUserTypeId) == 5 ) {
                                                                cmbLists = mISService.getOfficeList(objUserId.toString(), strUserTypeId);
                                                                for (Object[] cmbList : cmbLists) {
                                                                    String selected = "";
                                                                    if (request.getParameter("offId") != null) {
                                                                        if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                            selected = "selected";
                                                                        }
                                                                    }
                                                                    out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                                    selected = null;
                                                                }
                                                                if (cmbLists.isEmpty()) {
                                                                    out.print("<option value='0'> No Office Found.</option>");
                                                                }
                                                            }}
                                                        %>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="22%" class="ff">Procurement Category : </td>
                                                    <td width="38%">
                                                        <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                                            <option value="" selected="selected">-- Select Category --</option>
                                                            <option value="1">Goods</option>
                                                            <option value="2">Works</option>
                                                            <option value="3">Service</option>
                                                        </select>
                                                    </td>
                                                    <td width="15%" class="ff"></td>
                                                    <td width="25%">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procurement Type :</td>
                                                    <td>
                                                        <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                                            <option value="">-- Select Type --</option>
                                                            <option value="NCT">NCB</option>
                                                            <option value="ICT">ICB</option>
                                                        </select>
                                                    </td>
                                                    <td class="ff">Procurement Method :</td>
                                                    <td><select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                                            <option value="0" selected="selected">- Select Procurement Method -</option>
                                                            <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                                                <c:choose>
                                                                    <c:when test = "${procMethod.objectValue=='RFQ'}">
                                                                        <option value="${procMethod.objectId}">LEM</option>
                                                                    </c:when>
                                                                    <c:when test = "${procMethod.objectValue=='DPM'}">
                                                                        <option value="${procMethod.objectId}">DCM</option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Tender ID :</td>
                                                    <td>
                                                        <input name="tenderId" id="tenderId" onkeypress="checkKey(event);" type="text" class="formTxtBox_1" id="textfield5" style="width:194px;" />
                                                        <span class="reqF_1" id="msgTenderId"></span>
                                                    </td>
                                                    <td class="ff">Reference No :</td>
                                                    <td><input name="refNo" id="refNo" type="text" onkeypress="checkKey(event);" class="formTxtBox_1" style="width:194px;" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">From Publishing Date :</td>
                                                    <td><input name="pubDtFrm" id="pubDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('pubDtFrm','pubDtFrmImg');"/>&nbsp;
                                                        <a  href="javascript:void(0);" title="Calender"><img id="pubDtFrmImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtFrm','pubDtFrmImg');"/></a>
                                                        <span id="dtErrPub" class="reqF_1"><br /></span>
                                                    </td>
                                                    <td class="ff">To Publishing Date :</td>
                                                    <td><input name="pubDtTo" id="pubDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                        <a  href="javascript:void(0);" title="Calender"><img id="pubDtToImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtTo','pubDtToImg');"/></a></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">From Closing Date :</td>
                                                    <td><input name="closeDtFrm" id="closeDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                        <a  href="javascript:void(0);" title="Calender"><img id="closeDtFrmImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtFrm','closeDtFrmImg');"/></a>
                                                        <span id="dtErrClose" class="reqF_1"><br /></span>
                                                    </td>
                                                    <td class="ff">To Closing Date :</td>
                                                    <td><input name="closeDtTo" id="closeDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                        <a  href="javascript:void(0);" title="Calender"><img id="closeDtToImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtTo','closeDtToImg');"/></a></td>
                                                </tr>

                                                <!--Code by Proshanto Kumar Saha-->
<!--                                                <tr>
                                                    <td class="ff"> Dzongkhag / District : </td>
                                                    <td><select name="state" class="formTxtBox_1" id="cmbState">
                                                    <option value="0" selected="selected">-- Select Dzongkhag / District --</option>
                                                    <%String cntName = "Bhutan";
                                                           // if (ifEdit) {
                                                               // cntName = dtBean.getCountry();
                                                            //} else {
                                                               // cntName = "Bangladesh";
                                                            //}

                                                            for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                                   <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                                                            <%}%>
                                                       </select></td>
                                                 </tr>-->
                                                            <input type="hidden" value="0" name="state" class="formTxtBox_1" id="cmbState">

                                                  <tr id="trRthana">
                                                     <!--Gewogs comment by Proshanto--> 
<!--                                                    <td class="ff">Gewog : </td>-->
                                                    <!--<td class="ff">Gewogs : </td>-->
                                                    <td><input type="hidden" name="upJilla" type="text" onkeypress="checkKey(event);"  class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;" /><div id="upErr" style="color: red;"></div></td>
                                                  </tr>
                                                <!--Code End by Proshanto Kumar Saha-->
                                                
                                                <tr style="display: none">
                                                    <td class="ff">Category :</td>
                                                    <td colspan="3">
                                                        <input name="cpvCat" id="txtaCPV" type="text" class="formTxtBox_1" style="width:202px;" />
                                                        <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()">Select Category</a>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td colspan="4" class="t-align-center">
                                                        
                                                        <label class="formBtn_1 t_space"><input type="button" name="search" id="btnSearch" value="Search" /></label>&nbsp;&nbsp;
                                                        <label class="formBtn_1 t_space"><input type="reset" name="btnReset" id="btnReset" value="Reset" /></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="t-align-center" colspan="4">
                                                        <div id="valAll" class="reqF_1"></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                    <input type="hidden" id="CurrentTime" value="" >
                                    <div id="resultDiv" style="display: none">
                                        <div class="tableHead_1 t_space">Tender Search Results</div>
                                        <ul class="tabPanel_1 t_space">
                                            <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Live</a></li>
                                            <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);">Archive</a></li>
                                            <li><a href="javascript:void(0);" id="linkCancel" onclick="test(4);">Cancelled</a></li>
                                            <li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">All</a></li>
                                        </ul>
                                        <table id="resultTable" width="100%" cellspacing="0" class="tableList_3" cols="@0,6">
                                            <div id="progressBar" class="tableHead_1 t_space" align="center" style="visibility:hidden"  >
                                                <img id="theImg" alt="ddd" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                                            </div>
                                            <tr>
                                                <th width="4%" class="t-align-center">Sl.<br/>No.</th>
                                                <th width="15%" class="t-align-center">Tender <br/> ID,<br />
                                                    Reference No</th>
                                                <th width="22%" class="t-align-center">Procurement Category,
                                                    <br/>Title</th>
                                                <th width="15%" class="t-align-center">Hierarchy Node</th>
                                                <th width="10%" class="t-align-center">Type, <br />
                                                    Method</th>
                                                <th width="19%" class="">Publishing Date & Time| <br />
                                                    Closing Date & Time</th>
                                                    <%
                                                                if (objUsrTypeId != null && objUsrTypeId.toString().equals("2")) {
                                                    %>
                                                <th width=22%" class="t-align-left"><div align="center">Dashboard</div></th>
                                                <%}%>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                            <tr>
                                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                 <%if(objUsrTypeId!=null && objUsrTypeId!=""){%>
                                                 Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/>
                                                 <%}else{%>
                                                 <input type="hidden" name="size" id="size" value="10"/>
                                                 <%}%>
                                                </td>
                                                <td align="center"><input name="textfield3" type="text" onkeypress="checkKeyGoTo(event);" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                    &nbsp;
                                                    <label class="formBtn_1">
                                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                    </label></td>
                                                <td class="prevNext-container"><ul>
                                                        <li>&laquo; <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                        <li>&#8249; <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                    </ul></td>
                                            </tr>
                                        </table>
                                        <div align="center">
                                            <input type="hidden" id="pageNo" value="1"/>
                                            <!--<input type="hidden" name="size" id="size" value="10"/>-->
                                        </div>
                                       <form id="formstyle" action="" method="post" name="formstyle">

                                           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                                           <%
                                             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                             String appenddate = dateFormat1.format(new Date());
                                           %>
                                           <input type="hidden" name="fileName" id="fileName" value="AllTenders_<%=appenddate%>" />
                                            <input type="hidden" name="id" id="id" value="AllTenders" />
                                        </form>
                                        <div>&nbsp;</div>
                                    </div>
                                </div>

                                <!--Page Content End-->
                            </td>
                            <%
                                    if (objUName == null) {
                            %>
                            <td width="266">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                            <% }%>
                        </tr>
                    </table>

                    <!--Middle Content Table End-->
                    <jsp:include page="Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
            function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
            function loadCPVTree()
            {
                window.open('CPVTree.jsp','CPVTree','menubar=0,scrollbars=1,width=700px');
            }
        </script>
        <script type="text/javascript">

            function validateSearch(){
                if(document.getElementById('tenderId').value!=""){
                    if(isNaN(document.getElementById('tenderId').value)){
                        document.getElementById("msgTenderId").innerHTML = "Tender ID should be numeric";
                        return false;
                    }
                }
                return true;
            }

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            function loadOffice() {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }

        </script>
        <script type="text/javascript">
            var count;
            function valSearch(){
                count = 0;
                if(document.getElementById('txtdepartment') !=null && document.getElementById('txtdepartment').value !=''){
                    count = 1;
                }else if(document.getElementById('cmbOffice') !=null && document.getElementById('cmbOffice').value !=''){
                    count = 1;
                }else if(document.getElementById('procNature') !=null && document.getElementById('procNature').value !=''){
                    count = 1;
                }else if(document.getElementById('cmbType') !=null && document.getElementById('cmbType').value !=''){
                    count = 1;
                }else if(document.getElementById('tenderId') !=null && document.getElementById('tenderId').value !=''){
                    count = 1;
                }else if(document.getElementById('pubDtFrm') !=null && document.getElementById('pubDtFrm').value !=''){
                    count = 1;
                }else if(document.getElementById('closeDtFrm') !=null && document.getElementById('closeDtFrm').value !=''){
                    count = 1;
                }else if(document.getElementById('txtaCPV') !=null && document.getElementById('txtaCPV').value !=''){
                    count = 1;
                }else if(document.getElementById('cmbProcMethod') !=null && document.getElementById('cmbProcMethod').value !='0'){
                    count = 1;
                }else if(document.getElementById('refNo') !=null && document.getElementById('refNo').value !=''){
                    count = 1;
                }else if(document.getElementById('pubDtTo') !=null && document.getElementById('pubDtTo').value !=''){
                    count = 1;
                }else if(document.getElementById('closeDtTo') !=null && document.getElementById('closeDtTo').value !=''){
                    count = 1;
                }
                //Code Start by Proshanto Kumar Saha
                else if(document.getElementById('cmbState') !=null && document.getElementById('cmbState').value !=''){
                    count = 1;
                }
                else if(document.getElementById('txtThana') !=null && document.getElementById('txtThana').value !=''){
                    count = 1;
                }
                //Code End by Proshanto Kumar Saha

            }
            $(function() {
                $('#btnSearch').click(function() {
                    if(!validateSearch()){
                        return false;
                    }
                    valSearch();
                    if(count != '0'){
                        $("#valAll").html('');
                    var flag = CompareDate();
                    if(flag){
                        $("#mode").val("Search");
                        $("#pageNo").val("1");
                        var deptId=$('#txtdepartmentid').val();
                        //alert($("#refNo").val());
                        $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "ownCategory",viewType:$("#viewType").val(),departmentId: deptId,office: $("#cmbOffice").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),closeDtFrm: $("#closeDtFrm").val(),closeDtTo: $("#closeDtTo").val(),cpvCategory: $("#txtaCPV").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),h:"<%=request.getParameter("h")%>",district:$("#cmbState").val(),thana:$("#txtThana").val(),from:'<%=request.getParameter("filter")%>',userid:'<%=session.getAttribute("userId")%>'},  function(j){
                            $('#resultTable').find("tr:gt(0)").remove();
                            $('#resultTable tr:last').after(j);
                             sortTable();
                                if($('#noRecordFound').attr("value") == "noRecordFound"){
                                $('#pagination').hide();
                            }else{
                                $('#pagination').show();
                            }
                            chkdisble($("#pageNo").val());
                            if($("#totalPages").val() == 1){
                                $('#btnNext').attr("disabled", "true");
                                $('#btnLast').attr("disabled", "true");
                            }else{
                                $('#btnNext').removeAttr("disabled");
                                $('#btnLast').removeAttr("disabled");
                            }
                            $("#pageTot").html($("#totalPages").val());
                            $("#pageNoTot").html($("#pageNo").val());
                            $('#resultDiv').show();

                            var counter = $('#cntTenBrief').val();
                            //alert(counter);
                            for(var i=0;i<counter;i++){
                                try
                                {
                                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                                    var temp1 = $('#tenderBrief_'+i).html();
                                    if(temp.length > 250){
                                        temp = temp1.substr(0, 250);
                                        $('#tenderBrief_'+i).html(temp+'...');
                                    }
                                    //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                                }
                                catch(e){}
                            }

                        });
                    }
                    }else{
                        $("#valAll").html('Please enter at least One search criteria');
                    }
                });
            });
        </script>
        <!-- reset -->
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#mode").val("Search");
                    $("#pageNo").val("1");
                    document.getElementById('dtErrPub').innerHTML='';
                    document.getElementById('dtErrClose').innerHTML='';
                    document.getElementById('txtdepartmentid').value = '';
                    var deptId=document.getElementById('txtdepartmentid').value;
                    $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "ownCategory",departmentId: deptId,viewType:$("#viewType").val(),office: '',procNature: '',procType:'',procMethod: '0',tenderId: '0',refNo: '',pubDtFrm: '',pubDtTo: '',closeDtFrm: '',closeDtTo: '',cpvCategory: '',pageNo: $("#pageNo").val(),size: $("#size").val(),h:"<%=request.getParameter("h")%>",district:'0',thana:'',from:'<%=request.getParameter("filter")%>',userid:'<%=session.getAttribute("userId")%>'},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                         sortTable();
                        if($('#noRecordFound').attr("value") == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();

                        var counter = $('#cntTenBrief').val();
                        for(var i=0;i<counter;i++)
                        {
                            try
                            {
                                var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                                var temp1 = $('#tenderBrief_'+i).html();
                                if(temp.length > 250){
                                    temp = temp1.substr(0, 250);
                                    $('#tenderBrief_'+i).html(temp+'...');
                                }
                            }
                            catch(e){}
                        }
                    });
                });
            });
        </script>


        <script type="text/javascript">
            function loadTable()
            {
                $("#progressBar").show();
                $("#progressBar").css("visibility","visible");
                var deptId=$('#txtdepartmentid').val();
//                alert(document.getElementById('txtdepartmentid').value);
//                alert(deptId);
//                alert($('#txtdepartmentid').val());
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "ownCategory",departmentId: deptId,viewType:$("#viewType").val(),office: $("#cmbOffice").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),closeDtFrm: $("#closeDtFrm").val(),closeDtTo: $("#closeDtTo").val(),cpvCategory: $("#txtaCPV").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),h:"<%=request.getParameter("h")%>",district:$("#cmbState").val(),thana:$("#txtThana").val(),from:'<%=request.getParameter("filter")%>',userid:'<%=session.getAttribute("userId")%>'},  function(j){
                     $("#progressBar").hide();
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++)
                    {
                        try
                        {
                            var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                            var temp1 = $('#tenderBrief_'+i).html();
                            if(temp.length > 250){
                                temp = temp1.substr(0, 250);
                                $('#tenderBrief_'+i).html(temp+'...');
                            }
                        }
                        catch(e){}
                        //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                    }

                });
                //TimelineShow();
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                        //chkdisble($('#pageNo').val());

                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var deptId=document.getElementById('txtdepartmentid').value
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                            //chkdisble($('#pageNo').val());
                            /*if(parseInt($('#pageNo').val(), 10) != 1)
                                $('#btnFirst').removeAttr("disabled");
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnFirst').attr("disabled", "true")

                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnLast').attr("disabled", "true");
                            else
                                $('#btnLast').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnNext').attr("disabled", "true")
                            else
                                $('#btnNext').removeAttr("disabled");*/
                        }
                    }
                });
            });
        </script>
<script type ="text/javascript">
            function CompareDate(){
                var pubDtfrm = document.getElementById('pubDtFrm').value;
                var pubDtTo = document.getElementById('pubDtTo').value;
                var closeDtfrm = document.getElementById('closeDtFrm').value;
                var closeDtTo = document.getElementById('closeDtTo').value;
                if(pubDtfrm!=null && pubDtTo!=null){
                    if(pubDtfrm!='' && pubDtTo!=''){
                        var mdy = pubDtfrm.split('/')  //Date and month split
                        var mdyhr= mdy[2].split(' ');  //Year and time split

                        var mdy1 = pubDtTo.split('/')  //Date and month split
                        var mdyhr1= mdy1[2].split(' ');  //Year and time split

                        if(mdyhr[1] == undefined){
                            var dt_pubDateFrm = new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                        }

                        if(mdyhr1[1] == undefined){
                            var dt_pubDateTo =new Date(mdyhr1[0], mdy1[1]-1, mdy1[0]);
                        }

                        if(Date.parse(dt_pubDateFrm) < Date.parse(dt_pubDateTo)){
                            document.getElementById('dtErrPub').innerHTML='';
                            return true;
                        }else{
                            document.getElementById('dtErrPub').innerHTML='<br/>Publishing Date From should be less then Publising Date To';
                            return false;
                        }
                    }
                }

                if(closeDtfrm!=null && closeDtTo!=null){
                    if(closeDtfrm!='' && closeDtTo!=''){
                        var mdy2 = closeDtfrm.split('/')  //Date and month split
                        var mdyhr2= mdy2[2].split(' ');  //Year and time split

                        var mdy3 = closeDtTo.split('/')  //Date and month split
                        var mdyhr3= mdy3[2].split(' ');  //Year and time split

                        if(mdyhr2[1] == undefined){
                            var dt_closeDtfrm = new Date(mdyhr2[0], mdy2[1]-1, mdy2[0]);
                        }

                        if(mdyhr3[1] == undefined){
                            var dt_closeDtTo =new Date(mdyhr3[0], mdy3[1]-1, mdy3[0]);
                        }

                        if(Date.parse(dt_closeDtfrm) < Date.parse(dt_closeDtTo)){
                            document.getElementById('dtErrClose').innerHTML='';
                            return true;
                        }else{
                            document.getElementById('dtErrClose').innerHTML='<br/>Closing Date From should be less then Closing Date To';
                            return false;
                        }
                    }
                }
                return true;
            }
        </script>
    <script>
        function test(type){
            if(type==1){
                document.getElementById("linkPending").className = "sMenu";
                document.getElementById("linkApproved").className = "";
                document.getElementById("linkRejected").className = "";
                document.getElementById("linkCancel").className = "";
                document.getElementById("viewType").value = "Live";
                //fillGridOnEvent('pending');
                $("#pageNo").val("1");
                //clearInterval(IntervalVar);
                loadTable();
            }else if(type==2){
                document.getElementById("linkPending").className = "";
                document.getElementById("linkApproved").className = "sMenu";
                document.getElementById("linkRejected").className = "";
                document.getElementById("linkCancel").className = "";
                document.getElementById("viewType").value = "Archive";
                $("#pageNo").val("1");
                //clearInterval(IntervalVar);
                loadTable();
            }else if(type==4){
                document.getElementById("linkPending").className = "";
                document.getElementById("linkApproved").className = "";
                document.getElementById("linkRejected").className = "";
                document.getElementById("linkCancel").className = "sMenu";
                document.getElementById("viewType").value = "Cancel";
                $("#pageNo").val("1");
                //clearInterval(IntervalVar);
                loadTable();
            }else{
                document.getElementById("linkPending").className = "";
                document.getElementById("linkApproved").className = "";
                document.getElementById("linkRejected").className = "sMenu";
                document.getElementById("linkCancel").className = "";
                document.getElementById("viewType").value = "AllTenders";
                //fillGridOnEvent('rejected');
                $("#pageNo").val("1");
                //clearInterval(IntervalVar);
                loadTable();
            }
        }

        function checkKey(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $('#pageNo').val('1')
                $('#btnSearch').click();
                //loadTable();
            }
        }
        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                //$('#btnGoto').click(function() {
                        var pageNo=parseInt($('#dispPage').val(),10);
                        var totalPages=parseInt($('#totalPages').val(),10);
                        if(pageNo > 0)
                        {
                            if(pageNo <= totalPages) {
                                $('#pageNo').val(Number(pageNo));
                                //loadTable();
                                $('#btnGoto').click();
                                $('#dispPage').val(Number(pageNo));
                            }
                        }
                    });
                //});
            }
        }
        function SetCurrentDate(CurrentTime)
        {
            document.getElementById("CurrentTime").value = CurrentTime;
        }
        function UpdateCurrentTimeByOneSec()
        {
            var Current = document.getElementById("CurrentTime").value ;
            AddOneSecond = parseInt(Current) + parseInt(1000);
            document.getElementById("CurrentTime").value = AddOneSecond;
        }
        function TimelineShow(obj,Submitdate)
        {
            var today = document.getElementById("CurrentTime").value ;
            var Difference =  Submitdate - today;  
            var Day = 0, Hour = 0, Minute = 0, Second = 0;
            if(Difference > 0)
            {
                var DifferenceInSeconds = Difference/1000;
                Day = DifferenceInSeconds/(60*60*24);
                DifferenceInSeconds = DifferenceInSeconds % (60*60*24);
                Hour = DifferenceInSeconds/(60*60);
                DifferenceInSeconds = DifferenceInSeconds % (60*60);
                Minute = DifferenceInSeconds/(60);
                Second = DifferenceInSeconds % (60);
                obj.innerHTML = "<b>" + Math.floor(Day) + "D " + Math.floor(Hour) + "H " + Math.floor(Minute) + "M "+ Math.floor(Second) + "S </b>";
            }
            else
            {
                obj.innerHTML = "<b>Time Elapsed</b>";

            }
            
        }
        
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        
    </script>
</html>

