<%-- 
    Document   : ContractInfoBar
    Created on : Aug 1, 2011, 3:59:00 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariContractVal"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%
            String c_tenderid = "";
            if (pageContext.getAttribute("tenderId") != null && !"".equals(pageContext.getAttribute("tenderId"))) {
                c_tenderid = pageContext.getAttribute("tenderId").toString();
            }
            String c_lotId = "";
            if (pageContext.getAttribute("lotId") != null && !"".equals(pageContext.getAttribute("lotId"))) {
                c_lotId = pageContext.getAttribute("lotId").toString();
            }
            /* Added for download contract document START 23rd Dec 2011 */
            String st_forDownloadLink = "Package";
            boolean isTenderPkgWise = false;
            if (pageContext.getAttribute("isTenPackageWise") != null) {
                isTenderPkgWise = Boolean.parseBoolean(pageContext.getAttribute("isTenPackageWise").toString());
            }
            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
            List<TblTenderLotSecurity> lots = null;
            if (!isTenderPkgWise) {
                lots = tenderDocumentSrBean.getLotDetailsByLotId(Integer.parseInt(c_tenderid), Integer.parseInt(c_lotId));
                if (lots.size() > 0) {
                    TblTenderLotSecurity tenderLotSecurity = lots.get(0);
                    st_forDownloadLink = "Lot_" + tenderLotSecurity.getLotNo();
                }
            }
            /* Added for download contract document END 23rd Dec 2011 */

            String c_ispdf = "";
            if (pageContext.getAttribute("isPDF") != null && !"".equals(pageContext.getAttribute("isPDF"))) {
                c_ispdf = pageContext.getAttribute("isPDF").toString();
            }
            String c_struserid = "";
            if ("true".equalsIgnoreCase(c_ispdf)) {
                c_struserid = pageContext.getAttribute("userId").toString();
            } else {
                if (session.getAttribute("userId") != null) {
                    c_struserid = session.getAttribute("userId").toString();
                }
            }
            int c_procNatureId = 0;
            ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            List<TblCmsVariContractVal> displayNewCV = c_ConsSrv.displayNewContractValOrNot(Integer.parseInt(c_lotId));
            int c_cntId = 0;
            List<Object[]> c_list = null;
            if (request.getParameter("cntId") != null) {
                c_cntId = Integer.parseInt(request.getParameter("cntId"));
            }
            boolean c_isPhysicalPrComplete = false;
            if (c_cntId == 0) {
                c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(c_lotId));
            } else {
                c_isPhysicalPrComplete = c_ConsSrv.isCntCmptForRO(Integer.parseInt(c_lotId), c_cntId);
            }

%>

<!--Dashboard Header Start--><!--Dashboard Header End-->
<!--Dashboard Content Part Start-->

<div class="tableHead_1 t_space">Contract Detail</div>
<%
            ResourceBundle resbdl = ResourceBundle.getBundle("properties.cmsproperty");
            CmsConfigDateService c_cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");

            if (c_cntId == 0) {
                c_list = c_cmsConfigDateService.getContractInfoBar(Integer.parseInt(c_tenderid), Integer.parseInt(c_lotId));
            } else {
                c_list = c_cmsConfigDateService.getContractInfoBar(Integer.parseInt(c_tenderid), Integer.parseInt(c_lotId), c_cntId);
            }

            boolean cIsServices = false;
            Object[] c_obj = null;
            if (!c_list.isEmpty()) {
                c_obj = c_list.get(0);
                c_procNatureId = Integer.parseInt(c_obj[6].toString());
                String str_consName = "";
                if ("1".equals(c_obj[6].toString())) {
                    str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                } else if ("2".equalsIgnoreCase(c_obj[6].toString())) {
                    str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                } else {
                    cIsServices = true;
                    str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
                }
%>
<table width="100%" cellspacing="10" class="tableView_1 infoBarBorder">
    <%--<tr>
      <td width="20%" class="ff">Notice ID :</td>
      <td width="30%"><%=c_tenderid%></td>
      <td width="20%" class="ff">Ref. No.:</td>
      <td width="30%"><%=c_obj[0].toString()%></td>
    </tr>--%>
    <tr>
        <td class="ff">Contract No (Contract ID) :</td>
        <td><%=c_obj[1].toString() + "&nbsp;(" + c_obj[15].toString() + ")"%></td>
        <td class="ff">Contract Value (in Nu.):</td>
        <td><%=new BigDecimal(c_obj[2].toString()).setScale(3)%></td>
    </tr>

    <%if (displayNewCV != null && !displayNewCV.isEmpty()) {%>
      <%if (!cIsServices) {%>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="ff">Variation Order Contract Value (in Nu.):</td>
        <td><%=displayNewCV.get(0).getContractValue()%></td>
    </tr>
    <%}%>
    <%}%>
    <%if (cIsServices) {
                boolean flagForType = false;
                CommonService commService1 = (CommonService) AppContext.getSpringBean("CommonService");
                String serviceType1 = commService1.getServiceTypeForTender(Integer.parseInt(c_tenderid));
                if (!"Time based".equalsIgnoreCase(serviceType1.toString())) {
                    flagForType = true;
                }
                if(flagForType){
    %>
     <%if (displayNewCV != null && !displayNewCV.isEmpty()) {%>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="ff">Revised Contract Value (in Nu.):</td>
        <td><%=displayNewCV.get(0).getContractValue()%></td>
    </tr>

    <%}%>
    <%}%>
    <%}%>
    <tr>
        <td class="ff">Contract Start Date :</td>
        <td><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(c_obj[3].toString(), "yyyy-MM-dd HH:mm:ss"))%></td>
        <td class="ff">Contract End Date :</td>
        <td><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(c_obj[4].toString(), "yyyy-MM-dd HH:mm:ss"))%></td>
    </tr>
    <%
            if (cIsServices) {
    %>
    <tr>
        <td class="ff">Work Status : </td>
        <% if (c_isPhysicalPrComplete) {%>
        <td>Completed</td>
        <% } else {%>
        <td>Pending</td>
        <% }%>
    </tr>
    <%
            } else {
    %>
    <tr>
        <td class="ff">Payment Terms :
        </td>
        <td>
            <%
                        String c_strPayTerms = "";
                        if (c_obj[5] == null) {
                            c_strPayTerms = "-";
                        } else if ("anyitemanyp".equalsIgnoreCase(c_obj[5].toString())) {
                            c_strPayTerms = "Any Item Any Percent";
                        } else if ("allitem100p".equalsIgnoreCase(c_obj[5].toString())) {
                            c_strPayTerms = "All Item 100 Percent";
                        } else if ("itemwise100p".equalsIgnoreCase(c_obj[5].toString())) {
                            c_strPayTerms = "Item Wise 100 Percent";
                        }
            %>
            <%=c_strPayTerms%>
        </td>
        <td class="ff">Work Status : </td>
        <% if (c_isPhysicalPrComplete) {%>
        <td>Completed</td>
        <% } else {%>
        <td>Pending</td>
        <% }%>
    </tr>
    <%
            }
    %>
    <tr>
        <%if ("1".equals(c_obj[12].toString())) {%>
        <td class="ff"><%=str_consName%></td>
        <%} else {%>
        <td class="ff"><%=str_consName%></td>
        <%}%>
        <!--a href="#">
            < %
                if("1".equals(c_obj[12].toString())){
                 out.print(c_obj[10].toString()+" "+c_obj[11].toString());
            }else{out.print(c_obj[8].toString());}
            %></a-->


        <%
                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                String companyName = null;
                if ("1".equals(c_obj[12].toString())) {
                    companyName = c_obj[10].toString() + " " + c_obj[11].toString();
                } else {
                    companyName = c_obj[8].toString();
                }
                //14 userId,15 tendererId,16 companyId
                String[] jvSubData = creationService.jvSubContractChk(c_tenderid, c_obj[14].toString());
                StringBuilder name_link = new StringBuilder();
                name_link.append("<a view='link' href='" + request.getContextPath() + "/tenderer/ViewRegistrationDetail.jsp?uId=" + c_obj[14].toString() + "&tId=" + c_obj[13].toString() + "&cId=" + c_obj[12].toString() + "&top=no' target='_blank'>" + companyName + "</a>");
                if (jvSubData[0] != null && jvSubData[0].equals("jvyes")) {
                    name_link.delete(0, name_link.length());
                    name_link.append("<a view='link' href='" + request.getContextPath() + "/tenderer/ViewJVCADetails.jsp?uId=" + c_obj[14].toString() + "' target='_blank'>" + companyName + "</a>");
                    name_link.append("<br/>");
                    name_link.append("<a view='link' href='" + request.getContextPath() + "/tenderer/ViewJVCADetails.jsp?uId=" + c_obj[14].toString() + "' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                }
                if (jvSubData[1] != null && jvSubData[1].equals("subyes")) {
                    name_link.append("<br/>");
                    name_link.append("<a view='link' href='" + request.getContextPath() + "/tenderer/ViewTenderSubContractor.jsp?uId=" + c_obj[14].toString() + "&tenderId=" + c_tenderid + "&tId=" + c_obj[13].toString() + "&cId=" + c_obj[12].toString() + "' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                }
        %>
        <td><%out.print(name_link.toString());%></td>

        <td class="ff">email ID :</td>
        <td><%=c_obj[9].toString()%></td>
    </tr>
    <tr>
        <td colspan="4">
            <div class="noprint t_space">
                <span class="c-alignment-right">
                    <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=c_tenderid%>&lotNo=<%= st_forDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Contract Document">Download Contract Document</a>
                </span>
            </div>
        </td>
    </tr>
</table>
<%}%>