<%-- 
    Document   : PriDissForumPostTopic
    Created on : Mar 14, 2012, 2:18:05 PM
    Author     : dipal.shah
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post Topic</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <script src="../js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/form/CommonValidation.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
        <script type="text/javascript">

            $(document).ready(function() {

                $("#frmPublicProcForum").validate({
                    rules:
                    {
                        txtTopic:{required: true},
                        uploadDocFile:
                            {required:
                            function()
                                {
                                        var val = $('#documentBrief').val().length;
                                        if( val> 0)
                                        {
                                            return true;
                                        }
                                        else{
                                            return false;
                                        }
                                }
                            },
                        documentBrief:
                            {required:
                            function()
                                {
                                        var val = $('#uploadDocFile').val().length;
                                        if( val> 0)
                                        {
                                            return true;
                                        }
                                        else{
                                            return false;
                                        }
                                }
                            ,maxlength:100},
                        txtarea_topicdetails:
                        {
                            required :
                                function()
                                {
                                    var val=isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtarea_topicdetails.getData().replace(/<[^>]*>|\s/g, '')));
                                    
                                        if(val)
                                        {
                                            return true;
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                }
                           
                        }
                    },
                        messages:
                        {
                            txtTopic:{ required: "<div class='reqF_1'> Please enter Topic.</div>"},
                            uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                            documentBrief: { required: "<div class='reqF_1'>Please Enter Document Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"},
                            txtarea_topicdetails: { required: "<div class='reqF_1'>Please Enter Description.</div>"}
                        },
                        errorPlacement:function(error ,element)
                        {
                            if(element.attr("name")=="txtarea_topicdetails")
                            {
                                error.insertAfter("#DescpErMsg");
                            }
                            else
                            {
                                error.insertAfter(element);
                            }
                        }

                });
            });

            $(function() {
                $('#frmPublicProcForum').submit(function() {
                    if($('#frmPublicProcForum').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });

            function validate()
            {
                $(".err").remove();
                if($.trim(CKEDITOR.instances.txtarea_topicdetails.getData().replace(/<[^>]*>|\s/g, '')).length > 2000)
                {
                    $("#txtarea_topicdetails").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    return false;
                }
                else
                {
                     if($('#frmPublicProcForum').valid())
                     {
                         document.getElementById('btnlabel').setAttribute('style','visibility: hidden');
                     }
                    return true;
                }
            }
            
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <%@include file="AfterLoginTop.jsp" %>

            <%
                int userId=0;
                if(session.getAttribute("userId")!= null)
                    userId=Integer.parseInt(session.getAttribute("userId").toString());
                int tenderId=0;
                if(request.getParameter("tenderId")!= null)
                    tenderId=Integer.parseInt(request.getParameter("tenderId"));

            %>
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Post Topic<span class="c-alignment-right"><a href="PriDissForumTopicList.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a></span> </div>
                <form id="frmPublicProcForum" method="post" enctype="multipart/form-data" name="frmPublicProcForum" action="<%=request.getContextPath()%>/PriDissForumServlet?funName=postTopic&tenderId=<%=tenderId%>">
                     <%
                    if (request.getParameter("fq") != null)
                    {
                    %>

                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                    <%
                    }
                    if (request.getParameter("fs") != null)
                    {
                    %>

                    <div class="responseMsg errorMsg"  style="margin-top: 10px;">
                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                    }

                    if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("true"))
                    {
                    %>
                        <div id="succMsg" class="responseMsg successMsg">Topic Posted Successfully.</div>

                    <%
                    }
                    if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("false"))
                    {
                    %>
                             <div id="errMsg" class="responseMsg errorMsg">Some Problem occur during Post Topic.</div>

                    <%
                    }
                    %>
                    <div style="font-style: italic" class="formStyle_1 ff t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                    <table width="97%" cellspacing="5" class="formStyle_1">
                        <tr>
                            <td width="10%" class="ff"></td>
                            <td width="90%" class="ff"></td>
                        </tr>
                        <tr>
                            <td class="ff"> Topic : <span class="mandatory">*</span></td>
                            <td><input type="text" class="formTxtBox_1" id="txtTopic" name="txtTopic" maxlength="100" style="width: 100%;"/> </td>
                        </tr>

                        <tr>
                            <td class="ff">Description :  <span class="mandatory">*</span></td>
                            <td>
                                
                                <textarea cols="80" id="txtarea_topicdetails" name="txtarea_topicdetails"  rows="10"></textarea>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'txtarea_topicdetails',
                                    {
                                        fullPage : false
                                    });
                                </script>
                                
                                <div id="DescpErMsg">
                                </div>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" nowrap> Upload Document : </td>
                            <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" nowrap>Document Description : </td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff"></td>
                            <td class="ff"></td>
                        </tr>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td><label class="formBtn_1" id="btnlabel"><input name="btnSubmit" type="submit" id="btnSubmit" value="Submit" onclick="return validate();"/></label></td>
                        </tr>
                    </table>
                   
                </form>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>

    </body>
</html>