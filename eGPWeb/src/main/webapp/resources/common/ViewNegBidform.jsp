<%--
    Document   : BidForm
    Created on : Nov 16, 2010, 5:18:59 PM
    Author     : Sanjay
--%>

<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>



        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
        <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="AfterLoginTop.jsp" %>
                <div class="fixDiv">
                
                    <!--Middle Content Table Start-->
<%
                int userId = 0;
               

                 int negBidformId = 0;
                if (request.getParameter("negBidformId") != null) {
                    negBidformId = Integer.parseInt(request.getParameter("negBidformId"));
                }

                int uId = 0;
                if(!"".equalsIgnoreCase(request.getParameter("uId"))) {
                    userId = Integer.parseInt(request.getParameter("uId"));
                }

                int suserTypeId = 0;
                if(session.getAttribute("userTypeId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                        suserTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                }

                int formId = 0;
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }

                int bidId = 0;
                if (request.getParameter("bidId") != null) {
                    bidId = Integer.parseInt(request.getParameter("bidId"));
                }

                int lotId = 0;
                if (request.getParameter("lotId") != null) {
                    lotId = Integer.parseInt(request.getParameter("lotId"));
                }

                int negId = 0;
                if (request.getParameter("negId") != null) {
                    negId = Integer.parseInt(request.getParameter("negId"));
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
                     action = request.getParameter("action");
                }

                String type = "";
                if(request.getParameter("type")!= null && !"".equalsIgnoreCase(request.getParameter("type"))){
                    type = request.getParameter("type");
                }
                
               
                boolean isView = false;
                if("View".equalsIgnoreCase(action)){
                    isView = true;
                }

                

                int tableCount = 0;
                tableCount = tenderBidSrBean.getNoOfTable(formId);

                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                String idType="tenderId";
                int auditId=tenderId;
                String auditAction="Officer has View Negotiation Form";

                if(suserTypeId != 2)
                {
                    auditAction="Officer has View Negotiation Form";
                }
                else
                {
                    auditAction="Bidder has View Negotiation Form";
                }
                String moduleName=EgpModule.Negotiation.getName();
                String remarks="User Id:"+session.getAttribute("userId")+" has view Negotiation Form Id:"+formId+" for Tender Id:"+tenderId;
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

       

                        tableCount = tenderBidSrBean.getNoOfTable(formId);
%>
                <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="NegGetBidData.jsp">
                    
                    
                    <input type="hidden" name="hdnBidId" id="hdnBidId" value="<%=bidId%>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
<%
        String formName = "";
        String formHeader = "";
        String formFooter = "";
        String isMultipleFormFeeling = "";
        String isEncryption = "";
        String isPriceBid = "";
        String isMandatory = "";

        
        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(formId);
        for(CommonFormData formData : formDetails){
            formName = formData.getFormName();
            formHeader = formData.getFormHeader();
            formFooter = formData.getFormFooter();
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            isEncryption = formData.getIsEncryption();
            isPriceBid = formData.getIsPriceBid();
            isMandatory = formData.getIsMandatory();
        }
%>
                                <div class="t_space">
                                <table width="100%"   id="divFormName">
                                    <tr>
                                    <td width="80%" class="pageHead_1">
                                         <%=formName%>
                                   </td>
                                    <td width="20%" align="right" class="pageHead_1">
                                        <a class="action-button-goback" href="<%=request.getHeader("Referer")%>" title="Bid Dashboard">Go Back To Dashboard</a>
                                    </td>
                                        </tr>
                                </table>
                                <div>&nbsp;</div>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td>

                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="hdnFormId" id="hdnFormId" value="<%=formId%>">
                                            <input type="hidden" name="hdnLotId" id="hdnLotId" value="<%=lotId%>">
<%
        int tableId = 0;
        int tblCnt1 = 0;
        short cols = 0;
        short rows = 0;
        String tableName = "";
        String isMultiTable = "";

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();

            List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
            if (tableInfo != null) {
                if (tableInfo.size() >= 0) {
                    tableName = tableInfo.get(0).getTableName();
                    cols = tableInfo.get(0).getNoOfCols();
                    rows = tableInfo.get(0).getNoOfRows();
                    isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                }
                tableInfo = null;
            }

            cols = (tenderBidSrBean.getNoOfColsInTable(tableId)).shortValue();
            rows = (tenderBidSrBean.getNoOfRowsInTable(tableId, (short) 1)).shortValue();

%>

                                                <div id="divMsg" class="responseMsg successMsg" style="display:none">&nbsp;</div>
                                                <table width="100%" cellspacing="10" class="tableView_1 t_space">
                                                    <tr>
                                                        <td width="100" class="ff">Table Name : </td>
                                                        <td><%=tableName%></td>
                                                        <td class="t-align-right">

                                                        </td>
                                                    </tr>
                                                </table>
                                                <jsp:include page="ViewNegBidformTable.jsp" flush="true" >
                                                    <jsp:param name="tableId" value="<%=tableId%>" />
                                                    <jsp:param name="cols" value="<%=cols%>" />
                                                    <jsp:param name="rows" value="<%=rows%>" />
                                                    <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                    <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                    <jsp:param name="type" value="<%=type%>" />
                                                    <jsp:param name="negBidformId" value="<%=negBidformId%>" />
                                                </jsp:include>


<%
            tblCnt1++;
            formData = null;
        }
%>

                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                </div>
               
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </head>
</html>

<script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>