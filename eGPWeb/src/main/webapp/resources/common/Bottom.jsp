<%--
    Document   : Bottom.jsp
    Created on : Oct 21, 2010, 6:04:11 PM
    Author     : yanki
--%>

<%@page import="java.net.InetAddress"%>
<%@page import="com.cptu.egp.eps.model.table.TblAuditTrailMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<jsp:useBean id="auditTrailMaster" class="com.cptu.egp.eps.web.servicebean.AuditTrailMasterSrBean"></jsp:useBean>

<%
    String home = null,lang = null,aboutegp = null,contactus = null,newFeature = null;
    String rssfeed = null,termscondition =null,privacypolicy = null,serviceLevel = null;

    if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
        lang = request.getParameter("lang");
    }else{
        lang = "en_US";
    }

    MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
    List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"bottom");

    if(!langContentList.isEmpty())
    {
        for(TblMultiLangContent tblMultiLangContent:langContentList)
        {
            if(tblMultiLangContent.getSubTitle().equals("lbl_home")){
                if("bn_IN".equals(lang)){
                    home = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    home = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_aboutegp")){
                if("en_US".equals(lang)){
                    aboutegp = new String(tblMultiLangContent.getValue());
                }else{
                    aboutegp = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_contactus")){
                if("en_US".equals(lang)){
                    contactus = new String(tblMultiLangContent.getValue());
                }else{
                    contactus = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_rssfeed")){
                if("bn_IN".equals(lang)){
                    rssfeed = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    rssfeed = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_termscondition")){
                if("bn_IN".equals(lang)){
                    termscondition = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    termscondition = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_serviceLevel")){
                serviceLevel = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_privacypolicy")){
                privacypolicy = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_newFeature")){
                newFeature = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
            }
            
        }
    }
%>
<%
    if(false && session.getAttribute("userTypeId") == null){
%>
        <div class="footerCss">
            <a href="#"><%=aboutegp%></a>&nbsp; | &nbsp;
            <a href="#"><%=contactus%></a>&nbsp; | &nbsp;
            <!--<a href="<%=request.getContextPath() %>/SubscribeToRSS.jsp"><%=rssfeed%></a>&nbsp; | &nbsp;-->
            <a href="<%= request.getContextPath() %>/Terms.jsp"><%=termscondition%></a>&nbsp; | &nbsp;
            <a href="<%= request.getContextPath() %>/ServiceLevel.jsp"><%=serviceLevel%></a>&nbsp; | &nbsp;
            <a href="<%= request.getContextPath() %>/PrivacyPolicy.jsp"><%=privacypolicy%></a>&nbsp; | &nbsp;
            <a href="<%= request.getContextPath() %>/NewFeature.jsp"><%=newFeature%></a>
            
        </div>
<%
    }else{
%>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="footerCss">
    <tr valign="top">
        <td style="width: 100%;">
            
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding:0 22px;">
                
                 <tr>
                
                
                <td align="left" style="padding: 10px 0;">
                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/Index.jsp?lang=<%=request.getParameter("lang")%>"><%=home%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/Index.jsp"><%=home%></a> &nbsp;|&nbsp;
                    <% }%>

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/AboutEGP.jsp?lang=<%=request.getParameter("lang")%>"><%=aboutegp%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/AboutEGP.jsp"><%=aboutegp%></a> &nbsp;|&nbsp;
                    <% }%>

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/Contact.jsp?lang=<%=request.getParameter("lang")%>"><%=contactus%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/Contact.jsp"><%=contactus%></a> &nbsp;|&nbsp;
                    <% }%>

                    <!--<a href="<%=request.getContextPath() %>/SubscribeToRSS.jsp"><%=rssfeed%></a> &nbsp;|&nbsp;-->

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/Terms.jsp?lang=<%=request.getParameter("lang")%>"><%=termscondition%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/Terms.jsp"><%=termscondition%></a> 
                    <% }%>
                    | <a href="<%= request.getContextPath() %>/eLearning.jsp">e-Learning</a> 
                </td>
            </tr>
            <tr>
                    <td align="left"  style="padding:10px 10px 10px 0;border-top: 1px solid #CCC;border-bottom: 1px solid #CCC">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <div id="social">
                                      <a class="facebookBtn smGlobalBtn" href="#" ></a>
                                      <a class="twitterBtn smGlobalBtn" href="#" ></a>
                                      <a class="googleplusBtn smGlobalBtn" href="#" ></a>			
                                      <a class="rssBtn smGlobalBtn" href="#" ></a>
                                    </div>
                                </td>
                                <td align="right">
                                    <div id="version" align="right"  style="padding-top:5px; font-size: 10px;">
                                        Version: 1.1.0.2545
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
           
           <!-- <tr>
                <td align="center" style=" line-height: 16px;">
                    <div class="msg">
                        Best viewed in 1024 x 768 and above resolution. Browsers Tested & Certified by CPTU: Internet Explorer 8.x, 9.x, 10.x and Mozila Firefox 3.6x, 13x, 14x, 29.x,40.x
                    </div>
                </td>
            </tr>
           <tr>
                <td height="10"></td>
            </tr>-->
            <tr>
                <td width="64%" align="center" style="padding-bottom: 0px; padding-top: 5px; font-weight: normal; line-height: 18px;color: black;font-size: 11px;">
                          Government Procurement and Property Management Division (GPPMD), Department of National Properties, Ministry of Finance, Royal Government of Bhutan<br />
                         &copy; 2017 Dohatec New Media, Bangladesh, All Rights Reserved. 
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>

        </table></td>
        <td width="100">
        <% 
                String str_bottom_url = request.getRequestURL().toString();
                 if(str_bottom_url.indexOf("www.eprocure.gov.bd")!=-1 || str_bottom_url.indexOf("www.staging.eprocure.gov.bd")!= -1 || str_bottom_url.indexOf("www.training.eprocure.gov.bd")!= -1) {
                    String strURL = "www.eprocure.gov.bd";
                    if(str_bottom_url.indexOf("www.staging.eprocure.gov.bd")!= -1){
                        strURL = "www.staging.eprocure.gov.bd";
                   }else if(str_bottom_url.indexOf("www.training.eprocure.gov.bd")!= -1){
                        strURL = "www.training.eprocure.gov.bd";
                    }
        %>
        <%--<a href="http://www.verisign.com" target="_blank"><img src="<%= request.getContextPath() %>/resources/images/Dashboard/verisign.gif" style="border: none; width: 58px; height: 25px; margin-top: 10px;" /></a>--%>               
                <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers.">
                <tr>
                    <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=<%=strURL%>&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
                    <a href="http://www.verisign.com/verisign-trust-seal" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT TRUST ONLINE</a></td>
                </tr>
            </table>
            <% } %>
        </td>
    </tr>
</table>

<%
    }
%>

<%
    String sessionId = "";

    if(session.getAttribute("sessionId")!=null)
        sessionId = session.getAttribute("sessionId").toString();

    if(sessionId !=null && !"".equalsIgnoreCase(sessionId))
    {
        TblAuditTrailMaster tblAuditTrailMaster = new TblAuditTrailMaster();

        String b_ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (b_ipAddress == null) {
            b_ipAddress = request.getRemoteAddr();
        }

        String b_pageName= "";
        b_pageName = request.getRequestURL().toString();

        String b_pageReferer = "";
        if(request.getHeader("referer")!=null && !"".equalsIgnoreCase(request.getHeader("referer"))){
            b_pageReferer = request.getHeader("referer");
            b_pageName = b_pageName + " , " + b_pageReferer;
        }

        int objectId = 0;
        String module1 = "";

        if(request.getParameter("appId")!=null && !"".equalsIgnoreCase(request.getParameter("appId"))){
            objectId = Integer.parseInt(request.getParameter("appId"));
            module1 = "APP";
        }else if(request.getParameter("appID")!=null && !"".equalsIgnoreCase(request.getParameter("appID"))){
            objectId = Integer.parseInt(request.getParameter("appID"));
            module1 = "APP";
        }else if(request.getParameter("tenderId")!=null && !"".equalsIgnoreCase(request.getParameter("tenderId"))){
            objectId = Integer.parseInt(request.getParameter("tenderId"));
            module1 = "Tender";
        }else if(request.getParameter("tenderID")!=null && !"".equalsIgnoreCase(request.getParameter("tenderID"))){
            objectId = Integer.parseInt(request.getParameter("tenderID"));
            module1 = "Tender";
        }

        tblAuditTrailMaster.setSessionId(Integer.parseInt(sessionId));
        tblAuditTrailMaster.setIpAddress(b_ipAddress);
        tblAuditTrailMaster.setAuditDt(new java.util.Date());
        tblAuditTrailMaster.setPageName(b_pageName);
        tblAuditTrailMaster.setObjectId(objectId);
        tblAuditTrailMaster.setModule(module1);

        auditTrailMaster.addData(tblAuditTrailMaster);
    }

%>