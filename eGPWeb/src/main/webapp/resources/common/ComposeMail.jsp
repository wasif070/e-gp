<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.ViewMessageDtBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.MsgListing"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Compose Mail</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../js/pngFix.js"></script>-->
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <!--        <script type="text/javascript">
                    $(document).ready(function() {
                        $("#frmcomposmess").validate({
                            rules: {
                                textfield5: {required: true},
                                subject:{required:true,maxlength:100}
                             },
                            messages: {
                                textfield5: { required: "<div class='reqF_1'>Please Enter e-mail ID</div>"
                                },
                                subject:{required: "<div class='reqF_1'>Please Enter Subject.</div>",
                                    maxlength: "<div>Maximum 100 characters are allowed.</div>"
                                }
                            }
                        }
                    );
                    });
                </script>-->
        <script type="text/javascript">
            function Validate()
            {
                $(".formBtn_1").hide();
                var validatebool="";
                if(document.getElementById("textfield4").value=='')
                {
                    document.getElementById("to").innerHTML="<br/>Please enter e-mail ID.";
                    validatebool="false";
                }                
                if(document.getElementById("subject").value=='')
                {
                    document.getElementById("sub").innerHTML="<br/>Please enter Subject.";
                    validatebool="false";
                }
                else
                    {
                        if(document.getElementById("subject").value.length <= 100)
                    {
                            document.getElementById("sub").innerHTML="";
                    }
                    else
                    {
                           document.getElementById("sub").innerHTML="<br/>Maximum 100 characters are allowed.";
                           validatebool="false";
                    }
                    }
                //if(CKEDITOR.instances["textarea"].getData().trim() == 0){
                  //  document.getElementById("s_textarea").innerHTML="Message Text is not Available.";
                  //  validatebool="false";
               // }
               if(CKEDITOR.instances.textarea.getData() == 0){
                    document.getElementById("s_textarea").innerHTML="Please enter Message Text.";
                    validatebool="false";
                }
                else
                    {
                         if(CKEDITOR.instances.textarea.getData().length <= 5000)
                        {
                            document.getElementById("s_textarea").innerHTML="";
                        }
                        else
                        {
                           document.getElementById("s_textarea").innerHTML="Maximum 5000 characters are allowed.";
                           validatebool="false";
                        }
                    }
                if (validatebool=='false'){
                    $(".formBtn_1").show();
                    return false;
                }
            }
            function clearTo(){
                if(document.getElementById("textfield4").value!='')
                {
                    document.getElementById("to").innerHTML="";
                }
            }           
            function clearSub(){
                if(document.getElementById("subject").value!='')
                {
                    var validatebool="";
                    if(document.getElementById("subject").value.length <= 100)
                    {
                            document.getElementById("sub").innerHTML="";
                    }
                    else
                    {
                           document.getElementById("sub").innerHTML="<br/>Maximum 100 characters are allowed.";
                           validatebool="false";
                    }
                }else{
                    document.getElementById("sub").innerHTML = "<br/>Please enter Subject.";
                   validatebool="false";
                }
                if (validatebool=='false'){
                    return false;
            }
            }
            $(document).ready(function() {
                CKEDITOR.instances.textarea.on('blur', function()
                {
                    var validatebool="";
                    if(CKEDITOR.instances.textarea.getData() != 0)
                    {
                        if(CKEDITOR.instances.textarea.getData().length <= 5000)
                        {
                            document.getElementById("s_textarea").innerHTML="";
                        }
                        else
                        {
                           document.getElementById("s_textarea").innerHTML="Maximum 5000 characters are allowed.";
                           validatebool="false";
                        }
                    }else{
                        document.getElementById("s_textarea").innerHTML = "Please enter Message Text.";
                        validatebool="false";
                    }
                    if (validatebool=='false'){
                    return false;
                }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmcomposmess').submit(function(){
                    if( $('span.#mailMsg').html()=="OK" && $('span.#mailMsg1').html() == "OK")
                        return true;
                    else if($("#messageBoxType:input").val() == "Draft"){
                        return true;
                    }else if($('#textfield5').val() == "" &&  $('span.#mailMsg').html()=="OK")
                        return true;
                    else{
                       $(".formBtn_1").show();
                       return false;
                    }
                            
                });
            });
            $(function() {
                $('#textfield4').blur(function() {
                    if($('#textfield4').val() != ""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#textfield4').val(),funName:'verifyMulitipleMails'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg').css("color","green");

                            }
                            else if(j.toString().indexOf("Mail", 0)!=-1){
                                $('span.#mailMsg').css("color","red");

                            }
                            $('span.#mailMsg').html(j);
                        });
                    }else{
                        $('span.#mailMsg').html("");
                    }
                });

                $('#textfield5').blur(function() {
                    if($('#textfield5').val() != ""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#textfield5').val(),funName:'verifyMulitipleMails'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg1').css("color","green");

                            }
                            else if(j.toString().indexOf("Mail", 0)!=-1){
                                $('span.#mailMsg1').css("color","red");

                            }
                            $('span.#mailMsg1').html(j);
                        });
                    }else{
                        $('span.#mailMsg1').html("");
                    }
                });

            });
        </script>
    </head>
    <body>
       
        <%
                    
                    int uid = 0;
                   if(session.getAttribute("userId") != null){
                       Integer ob1 = (Integer)session.getAttribute("userId");
                       uid = ob1.intValue();
                   }


                    // here i have added static uid,if u have id in session then
                    // you can delete below line and enable above two lines.

                    //int id = 1;
                    String procURL = "";
                    String msfBoxType = request.getParameter("messageBoxType");
                    String action = "";
                    String msgStatus = "Live";
                    Integer folderid = 0;
                    String msgReadStatus = "no";
                    String isMsgReply = "no";
                    String msgDocId = "0";
                    Integer updmsgId = 0;
                    String from = "";
                    Integer msgBoxTypeId = 0;
                    String to = "";
                    String cc = "";
                    String subject = "";
                    String priority = "";
                    String messagetext = "";
                    Integer msgid = 0;
                    Integer noOfRecords = 5;
                    Integer pageNumber = 1;
                    Integer totalPages = 0;
                    String searching = "";
                    String keywords = "";
                    String emailId = "";
                    String dateFrom = "";
                    String dateTo = "";
                    String msg = "";
                    List status = null;
                    msg = request.getParameter("messageid");
                    String click = null;
                    ViewMessageDtBean viewMessageDataBean = new ViewMessageDtBean();
                    if ("Send".equals(request.getParameter("button1"))) {
                        to = request.getParameter("textfield5");
                        cc = request.getParameter("textfield6");
                        subject = request.getParameter("subject");
                        priority = request.getParameter("select2");
                        messagetext = request.getParameter("textarea");
                        msfBoxType = request.getParameter("messageBoxType");
                        if (msfBoxType.equalsIgnoreCase("Draft")) {
                            msg = request.getParameter("messageid");
                            msgid = Integer.valueOf(msg);
                            action = "Edit";
                            msfBoxType = "Inbox";
                        } else {
                            action = "create";
                        }
                        MessageProcessSrBean messageProcessSrBean =
                                new MessageProcessSrBean();
                        messageProcessSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        messageProcessSrBean.setLogUserId(""+uid);
                        status = messageProcessSrBean.composeMail(uid, to, from, cc, subject, priority, messagetext,
                                procURL, msfBoxType, action, msgStatus, folderid, msgReadStatus, isMsgReply,
                                msgDocId, msgid, msgBoxTypeId);
                        request.setAttribute("status", status);
                        if (status != null) {
                            click = "Send";
                            CommonMsgChk msgchk = (CommonMsgChk) status.get(0);
                            request.setAttribute("click", click);
                            if (msgchk.getFlag() == true) {
                                response.sendRedirect("InboxMessage.jsp?inbox=Sent&click=Send&status=Yes");

                            }
                        }

                    } else if ("Save As Draft".equals(request.getParameter("button2"))) {
                        to = request.getParameter("textfield5");
                        cc = request.getParameter("textfield6");
                        subject = request.getParameter("subject");
                        priority = request.getParameter("select2");
                        messagetext = request.getParameter("textarea");
                        msfBoxType = request.getParameter("messageBoxType");
                        if (msfBoxType.equalsIgnoreCase("Draft")) {
                            msg = request.getParameter("messageid");
                            msgid = Integer.valueOf(msg);
                            action = "Edit";
                        } else if (msfBoxType.equalsIgnoreCase("Inbox")) {
                            msfBoxType = "Draft";
                            action = "Create";
                        }


                        MessageProcessSrBean messageProcessSrBean =
                                new MessageProcessSrBean();
                        messageProcessSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        messageProcessSrBean.setLogUserId(""+uid);
                        status = messageProcessSrBean.composeMail(uid, to, from, cc,
                                subject, priority, messagetext,
                                procURL, msfBoxType, action, msgStatus, folderid, msgReadStatus, isMsgReply,
                                msgDocId, msgid, msgBoxTypeId);
                        request.setAttribute("status", status);
                        if (status != null) {
                            click = "Draft";
                            request.setAttribute("click", click);
                            CommonMsgChk msgchk = (CommonMsgChk) status.get(0);
                            if (msgchk.getFlag() == true) {
                                response.sendRedirect("InboxMessage.jsp?inbox=Draft&click=Draft&status=Yes");

                            }
                        }
                    } else if (msg != null) {
                        msg = request.getParameter("messageid");
                        msgid = Integer.valueOf(msg);
                        msfBoxType = request.getParameter("messageBoxType");
                        List<MsgListing> listdata = null;
                        MessageProcessSrBean mpsrb = new MessageProcessSrBean();
                        mpsrb.setLogUserId(""+uid);
                        listdata = mpsrb.getInboxData(msfBoxType, uid, msgReadStatus, totalPages, pageNumber, noOfRecords, folderid, searching, keywords, emailId, dateFrom, dateTo, "Yes", msgid, "creationDate", "asc", "eq");

                        ViewMessageDtBean viewMesageDatBean = mpsrb.addToViewMessageDtBean(listdata, viewMessageDataBean);
                        List ldata = new ArrayList();
                        ldata.add(viewMesageDatBean);
                        request.setAttribute("msgdata", ldata);
                        request.setAttribute("viewMesageDatBean", viewMesageDatBean);
                    }


        %>
        <div class="dashboard_div">

            <div class="topHeader">
                <%@include file="AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <table width="100%" cellspacing="0">
                <tr valign="top">
                    <td class="lftNav">
                        <jsp:include page="MsgBoxLeft.jsp" ></jsp:include></td>
                        <%  if (status != null) {
                                        CommonMsgChk msgchk = (CommonMsgChk) status.get(0);
                                        if (msgchk.getFlag() == false && click.equalsIgnoreCase("Send")) {
                        %>
                    <div class="responseMsg errorMsg">Message has Not Sent Successfully.</div>
                    <% } else {
                    %>
                    <div class="responseMsg errorMsg">Message has Not Saved Successfully.</div>
                    <%                                        }
                                }%>
                    <td class="contentArea"><div class="pageHead_1">Compose Mail</div>
                        <table width="800px" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <form id="frmcomposmess"  action="ComposeMail.jsp" method="post">
                                <%
                                            List listdata = (List) request.getAttribute("msgdata");
                                            if (listdata != null) {


                                %>

                                <%  if (viewMessageDataBean.getTo() != null) {%>
                                <tr>
                                    <td  class="ff">To : <span>*</span></td>
                                    <td><input name="textfield5" type="text" class="formTxtBox_1" id="textfield4" style="width:500px;" value="<%= viewMessageDataBean.getTo()%>" onblur="clearTo();"/>
                                        <span id="mailMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                        <span id="to" style="color: red; ">&nbsp;</span>
                                        <br/><span style="color: gray;">Use comma(,) to separate e-mail IDs</span>
                                    </td>
                                </tr>
                                <% }
                                 if (viewMessageDataBean.getCc() != null) {%>
                                <tr>
                                    <td class="ff">Cc : </td>
                                    <td><input name="textfield6" type="text" class="formTxtBox_1" id="textfield5" style="width:500px;" value="<%= viewMessageDataBean.getCc()%>" />
                                        <span id="mailMsg1" style="color: red; font-weight: bold">&nbsp;</span>

                                    </td>
                                </tr>
                                <% }%>
                                <tr>
                                    <td class="ff">Subject : <span>*</span></td>
                                    <td><input name="subject" id="subject" type="text"  class="formTxtBox_1" style="width:500px;" maxlength="2" onblur="show()" value="<%= viewMessageDataBean.getSubject()%>" onblur="clearSub();"/>
                                        <span id="sub" style="color: red; ">&nbsp;</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="ff">Priority : <span>*</span></td>
                                    <td><select  name="select2"  class="formTxtBox_1" id="select2">

                                            <option>Low</option>
                                            <option>Medium</option>
                                            <option>High</option>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td class="ff">Message Text : <span>*</span></td>
                                    <td><textarea name="textarea" rows="3" class="formTxtBox_1" id="textarea" style="width:600px;" > <%= viewMessageDataBean.getTextArea()%></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[

                                            CKEDITOR.replace( 'textarea',
                                            {
                                                toolbar : "egpToolbar"
                                               
                                            });

                                            //]]>
                                        </script>
                                        <span id="s_textarea" style="color: red; ">&nbsp;</span>
                                    </td>
                                </tr>
                                <!--
                                  <tr>
                                      <td class="ff">&nbsp;</td>
                                      <td colspan="3"><img src="../images/Dashboard/viewIcn.png" alt="Tender Notice" class="linkIcon_1" /><a href="#" title="Attachmets">Attachmets</a> </td>
                                    </tr>
                                -->
                                <tr>

                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="button2" id="btnsaveasdraft" value="Save As Draft" onclick="return Validate();"/>
                                            <input type="hidden" id="messageBoxType" name="messageBoxType" value="<%=msfBoxType%>" />
                                            <input type="hidden" name="messageid" value="<%=msg%>" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="button1" id="btnsend" value="Send" onclick="return Validate();"/>
                                        </label></td>
                                </tr>


                                <%          } else {
                                %>

                                <tr>
                                    <td width="100px" class="ff">To : <span>*</span></td>
                                    <td width="700px"><input name="textfield5" type="text" class="formTxtBox_1" id="textfield4" style="width:500px;" onblur="clearTo();" />
                                        <span id="to" style="color: red; ">&nbsp;</span>
                                        <span id="mailMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                     <br/><span style="color: gray;">Use comma(,) to separate e-mail IDs</span></td>
                                </tr>
                                <tr>
                                    <td class="ff">Cc : </td>
                                    <td><input name="textfield6" type="text" class="formTxtBox_1" id="textfield5" style="width:500px;"  />
                                        <span id="mailMsg1" style="color: red; font-weight: bold">&nbsp;</span></td>
                                </tr>
                                <tr>
                                    <td class="ff">Subject : <span>*</span></td>
                                    <td><input name="subject" id="subject" type="text"  class="formTxtBox_1" style="width:500px;" onblur="clearSub();"  />
                                        <span id="sub" style="color: red; ">&nbsp;</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="ff">Priority : <span>*</span></td>
                                    <td><select name="select2"  class="formTxtBox_1" id="select2">
                                            <option>Low</option>
                                            <option>Medium</option>
                                            <option>High</option>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td class="ff">Message Text : <span>*</span></td>
                                    <td><textarea name="textarea" rows="3" class="formTxtBox_1" id="textarea" style="width:600px;" ></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[

                                            CKEDITOR.replace( 'textarea',
                                            {
                                                toolbar : "egpToolbar"
                                            });

                                            //]]>
                                        </script>
                                        <span id="s_textarea" style="color: red; ">&nbsp;</span>
                                    </td>
                                </tr>
                                <!--
                                 <tr>
                                     <td class="ff">&nbsp;</td>
                                     <td colspan="3">
                                         <div class="t_space"><img src="../images/Dashboard/viewIcn.png" alt="Tender Notice" class="linkIcon_1" />
                                          <a href="#" title="Attachmets" >Attachmets</a></div>
                                          </td>
                                   </tr>
                                -->
                                <tr>

                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="button2" id="button2" value="Save As Draft" onclick="return Validate(this.id);"/>
                                            <input type="hidden" id="messageBoxType" name="messageBoxType" value="<%=msfBoxType%>" />
                                            <input type="hidden" name="messageid" value="<%=msg%>" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="button1" id="button" value="Send" onclick="return Validate();"/>
                                        </label></td>
                                </tr>
                            </form>
                        </table>
                        <%      }
                        %>

                        <div>&nbsp;</div></td>
                </tr>
            </table>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
          <jsp:include page="Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>