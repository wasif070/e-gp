<%-- 
    Document   : DeptTree
    Created on : 2-Dec-2010, 3:00:16 PM
    Author     : yagnesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CPV Category</title>
        <%--<script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>--%>
        <script type="text/javascript" src="../js/jstreecomp/jquery.js"></script>
        <script type="text/javascript" src="../js/jstreecomp/jquery.jstree.js"></script>
	<script type="text/javascript" src="../js/jstreecomp/jquery.cookie.js"></script>
	<script type="text/javascript" src="../js/jstreecomp/jquery.hotkeys.js"></script>

        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />        
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script language="javascript">
            function validateSearch(){

                document.getElementById('msgKeyword').innerHTML = "";
                
                var flag = true;
                if(document.getElementById('keyword').value==""){
                   document.getElementById('msgKeyword').innerHTML = "Please enter Keyword to search";
                   flag = false;
                }
                return flag;
            }
        </script>
    </head>
    <body>
        <%
            String searchCriteria = "";
            String keyword="";
            String searchBy="";

            if(request.getParameter("action")!=null){
                searchCriteria = request.getParameter("action");
            }
            if("search".equalsIgnoreCase(searchCriteria)){
                if(request.getParameter("keyword")!=null && !"".equalsIgnoreCase(request.getParameter("keyword"))){
                       keyword = request.getParameter("keyword");
                }
                if(request.getParameter("searchBy")!=null && !"".equalsIgnoreCase(request.getParameter("searchBy"))){
                       searchBy = request.getParameter("searchBy");
                }
            }
        %>
        <form action="CPVTree.jsp" name="frmsearch" method="post">
        <input type="hidden" name="action" id="action" value="search" />
            <table width="100%" cellspacing="10" cellpadding="0" border="0" class="formBg_1 formStyle_1 t_space">
                <tbody>
                    <tr>
                        <td width="17%" class="ff">Search by Keyword :</td>
<!--                        <td width="15%">
                            <select name="searchBy" id="searchBy">
                                <option value="">--Select--</option>
                                <option value="keyword"<% if("keyword".equalsIgnoreCase(searchBy)){ %>selected="selected"<% } %>>Keyword</option>
                                <option value="code" <% if("code".equalsIgnoreCase(searchBy)){ %>selected="selected"<% } %>>Code</option>
                            </select>
                            <span id="msgSearchBy" class="reqF_1"></span>
                        </td>-->
                        <td width="20%"><input type="text" id="keyword" class="formTxtBox_1" name="keyword" value="<%=keyword%>"> <span id="msgKeyword" class="reqF_1"></span></td>
                        <td width="50%" class="t-align-left">
                            <label class="formBtn_1">
                                <input type="submit" value="Search" id="button" name="srchbutton" onclick="return validateSearch();">&nbsp;                                
                            </label>&nbsp;
                            <label class="formBtn_1">
                                <input type="button" value="Reset" id="button" onclick="location.href='<%=request.getContextPath()%>/resources/common/CPVTree.jsp'">
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
        <div>&nbsp;</div>
        <div id="treeMsg" style="width: 100%;display:none" align="center"></div>
        <div id="jsTreeComponent" class="demo" style="width: 100%"></div>
        <div>&nbsp;</div>
        <div class="t-align-center">
            <label class="formBtn_1">
                <input type="button" value="Submit" id="btnGetCheckedNode" name="btnGetCheckedNode" />
            </label>
        </div>
        <div>&nbsp;</div>
	<script>
        var searchCriteria = "<%=searchCriteria%>";
        var keyword = "<%=keyword%>";
        var searchBy = "<%=searchBy%>";
        var gbl_checked_ids = [];

        if($("#searchBy").val()!=""){
            searchCriteria = "search";
            searchBy = $("#searchBy").val();
            keyword = $("#keyword").val();
        }
        
	$(function () {
		$("#jsTreeComponent").jstree({
			"json_data" : {
                            "ajax" : {
                                "url" : '<%=request.getContextPath()%>/GetCpvTree?searchBy='+searchBy+'&action='+searchCriteria+'&keyword='+keyword,
                                "data" : function (n) {
                                    return {
                                        "id" : n.attr ? n.attr("id") : 0,
                                        "searchBy" : searchBy,
                                        "action" : searchCriteria,
                                        "keyword" : keyword
                                    };
                                    
                                },
                                success : function (r) {
                                    if(r==null && keyword !=""){                                   
                                        document.getElementById("jsTreeComponent").innerHTML = 
                                            '<div style="margin-top: 3px; font-size:12px; color:red;" class="tableHead_1 t_space">No Records Found</div>';
                                    }
                                    keyword = "";
                                }
                             }
			},
                        "checkbox": {"two_state": false},
                        "themes" : {
                            "theme" : "apple",
                            "dots" : true,
                            "icons" : true
                        },
			"plugins" : [ "themes", "json_data", "ui", "checkbox" ]
		});
                $("#jsTreeComponent").bind("select_node.jstree", function (e, data) {
                    gbl_checked_ids.push(data.rslt.obj.attr("cpvname"));
                });
	});

        $(document).ready(function () {
                $("#btnGetCheckedNode").bind("click", function(e) {
                        var checked_ids = [];
                        var parent1 = false;
                        var parent2 = false;
                        var strcheckids = "";
                        
                        $("#jsTreeComponent").jstree("get_checked").each(function (){

                            var level2 = false;
                            $.ajax({
                              url: "<%=request.getContextPath()%>/GetCpvTree?action=getChildData&cpvdesc="+$("#jsTreeComponent").jstree("get_text", this),
                              async: false,
                              success: function(j){
                                  if(j!=""){
                                    level2 = true;
                                  }
                              }
                            });

                            //for level1
                            if(jQuery.jstree._reference("#jsTreeComponent")._get_parent(this)== -1)
                            {
                                checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                if(jQuery.jstree._reference("#jsTreeComponent")._get_children(this).size()==0){
                                    $.ajax({
                                      url: "<%=request.getContextPath()%>/GetCpvTree?action=getChildData&cpvdesc="+$("#jsTreeComponent").jstree("get_text", this),
                                      async: false,
                                      success: function(j){
                                        strcheckids = strcheckids + j;
                                      }
                                    });
                                }else{
                                    jQuery.jstree._reference("#jsTreeComponent")._get_children(this).each(function (){
                                        checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                        if(jQuery.jstree._reference("#jsTreeComponent")._get_children(this).size()==0){
                                              $.ajax({
                                                  url: "<%=request.getContextPath()%>/GetCpvTree?action=getChildData&cpvdesc="+$("#jsTreeComponent").jstree("get_text", this),
                                                  async: false,
                                                  success: function(j){
                                                    strcheckids = strcheckids + j;
                                                  }
                                                });
                                        }else{
                                            jQuery.jstree._reference("#jsTreeComponent")._get_children(this).each(function (){
                                                checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                            });
                                        }
                                    });
                                }
                            }
                            else if(!(jQuery.jstree._reference("#jsTreeComponent")._get_children(this).size()==0 || level2))
                            { // for level 3
                                checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                jQuery.jstree._reference("#jsTreeComponent")._get_parent(this).each(function (){
                                    if(!parent2){
                                        checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                         jQuery.jstree._reference("#jsTreeComponent")._get_parent(this).each(function (){
                                            checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                              if(!parent1){
                                                parent1 = true;
                                              }else{
                                                parent1 = false;
                                              }
                                        });
                                        parent2= true;
                                    }else{
                                        parent2 = false;
                                    }
                                });
                            }
                            else{
                                //for level2
                                checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                jQuery.jstree._reference("#jsTreeComponent")._get_parent(this).each(function (){
                                    if(!parent1){
                                        checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                        parent1= true;
                                    }else{
                                        parent1 = false;
                                    }
                                });
                                    if(jQuery.jstree._reference("#jsTreeComponent")._get_children(this).size()==0){
                                        $.ajax({
                                          url: "<%=request.getContextPath()%>/GetCpvTree?action=getChildData&cpvdesc="+$("#jsTreeComponent").jstree("get_text", this),
                                          async: false,
                                          success: function(j){
                                            strcheckids = strcheckids + j;
                                          }
                                        });
                                    }else{
                                       jQuery.jstree._reference("#jsTreeComponent")._get_children(this).each(function (){
                                            checked_ids.push($("#jsTreeComponent").jstree("get_text", this));
                                       });
                                    }
                                }
                            
                        });
                          var checkedcpvCategotries;

                          if(checked_ids.length==0){
                            checkedcpvCategotries = strcheckids.toString().substring(0, strcheckids.length-1);
                          }else{
                            checkedcpvCategotries = checked_ids.join("; ")+ ";" + strcheckids.toString().substring(0, strcheckids.length-1);
                          }

                            if(window.opener.document.getElementById('txtaCPV'))
                            {
                            window.opener.document.getElementById('txtaCPV').value= checkedcpvCategotries;
                            window.opener.document.getElementById('txtaCPV').focus();
                            }
                            else
                            {
                                alert('Unable to complete your request, press OK to close.');
                            }
                            window.close();
                });
        });
	</script>
    </body>
</html>
