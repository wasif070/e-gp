<%-- 
    Document   : AdvContractListing
    Created on : Apr 8, 2018, 5:00:25 PM
    Author     : MD. Emtazul Haque
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
  boolean isLoggedIn = false;
  Object objUsrTypeId = session.getAttribute("userTypeId");
  Object objUName = session.getAttribute("userName");
  
  if (objUName != null) {
                isLoggedIn = true;
  }
%>

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                   
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Advanced Search for Contracts</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../css/border-radius.css" />
        <script type="text/javascript" src="../js/jscal2.js"></script>
        <script type="text/javascript" src="../js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>

        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        //loadTenderTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTenderTable();
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                /*var flag = CompareDate();
                if(flag){*/
                $("#progressBar").show();
                $("#progressBar").css("visibility","visible");
                var advtDtSplit = '';
                if($("#advDt") != null && $("#advDt").val() != ''){
                    var adDtS = $("#advDt").val().toString().split("/", 100);
                    advtDtSplit = adDtS[2] + '-' + adDtS[1] + '-' +adDtS[0];
                }
                //Code by Proshanto Kumar Saha
                var advtDtToSplit = '';
                if($("#advDtTo") != null && $("#advDtTo").val() != ''){
                    var adDtToS = $("#advDtTo").val().toString().split("/", 100);
                    advtDtToSplit = adDtToS[2] + '-' + adDtToS[1] + '-' +adDtToS[0];
                }

                var noaDtSplit = '';
                if($("#noaDt") != null && $("#noaDt").val() != ''){
                    var adDtS = $("#noaDt").val().toString().split("/", 100);
                    noaDtSplit = adDtS[2] + '-' + adDtS[1] + '-' +adDtS[0];
                }
                //Code by Proshanto Kumar Saha
                var noaDtToSplit = '';
                if($("#noaDtTo") != null && $("#noaDtTo").val() != ''){
                    var adDtToS = $("#noaDtTo").val().toString().split("/", 100);
                    noaDtToSplit = adDtToS[2] + '-' + adDtToS[1] + '-' +adDtToS[0];
                }
                //Added 2 parametre as advDtTo and noaDtTo by Proshanto Kumar Saha
                $.post("<%=request.getContextPath()%>/AdvSearchNOAServlet", {keyword: $("#txtKWord").val(),officeId: $("#cmbOffice").val(),contractAwardTo:$('#txtContractAwardTo').val(),noaDt:noaDtSplit,noaDtTo:noaDtToSplit,stateName: $('#cmbDistrict').val(),departmentId: $("#txtdepartmentid").val(),tenderId: $("#txtTenderId").val(),contractNo: $("#txtContractNo").val(),contractDtFrom: $("#closeDtFrm").val(),contractDtTo: $("#closeDtTo").val(),tenderRefNo: $("#txtTenderRefNo").val(),contractAmount: $("#txtContractAmt").val(),cpvCode: $("#txtaCPV").val(),advDt:advtDtSplit,advDtTo:advtDtToSplit,procurementMethod: $('#cmbProcurementMethod').val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){

                     $("#progressBar").hide();
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }

                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    chkdisble($("#pageNo").val());
                    //alert($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());

                      // Dynamically shortened text with “Read More” Link
                    var showChar = 25;  // How many characters are shown by default
                    var ellipsestext = "...";
                    var moretext = "more";
                    var lesstext = "less";

                    $('.more').each(function() {
                        var content = $(this).html();

                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);
                            var html = c + '<span class="moreellipses">' + ellipsestext+ '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        }
                        else
                            {
                                 var c = content+'</br>';
                                 var html = c;
                                 $(this).html(html);
                            }

                    });

                    $(".morelink").click(function(){
                        if($(this).hasClass("less")) {
                            $(this).removeClass("less");
                            $(this).html(moretext);
                        } else {
                            $(this).addClass("less");
                            $(this).html(lesstext);
                        }
                        $(this).parent().prev().toggle();
                        $(this).prev().toggle();
                        return false;
                    });

                });
                //}
            }
        </script>
        <script type="text/javascript">
            function loadCPVTree()
            {
                window.open('CPVTree.jsp','CPVTree','menubar=0,scrollbars=1,width=700px');
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            function loadOffice() {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }
        </script>
        <script type ="text/javascript">
            function callReset(){
                location.href='<%=request.getContextPath()%>/resources/common/AdvContractListing.jsp';
            }
            function CompareDate(){
                var pubDtfrm = document.getElementById('closeDtFrm').value;
                var pubDtTo = document.getElementById('closeDtTo').value;
                if(pubDtfrm!=null && pubDtTo!=null){
                    if(pubDtfrm!='' && pubDtTo!=''){
                        var mdy = pubDtfrm.split('/')  //Date and month split
                        var mdyhr= mdy[2].split(' ');  //Year and time split

                        var mdy1 = pubDtTo.split('/')  //Date and month split
                        var mdyhr1= mdy1[2].split(' ');  //Year and time split

                        if(mdyhr[1] == undefined){
                            var dt_pubDateFrm = new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                        }

                        if(mdyhr1[1] == undefined){
                            var dt_pubDateTo =new Date(mdyhr1[0], mdy1[1]-1, mdy1[0]);
                        }

                        if(Date.parse(dt_pubDateFrm) < Date.parse(dt_pubDateTo)){
                            document.getElementById('dtErrClose').innerHTML='';
                            return true;
                        }else{
                            document.getElementById('dtErrClose').innerHTML='<br/>Contract Date From should be less then Contract Date To';
                            return false;
                        }
                    }
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:'150',funName:'stateComboNOA'},  function(j){
                    $("select#cmbDistrict").html(j);
                });
            });
        </script>
    </head>
    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
    <body onload="loadTable(); <%if (session.getAttribute("userId") == null) {%> chngTab(3); <% }%>">
        <div class="mainDiv">
             <%
                        if (isLoggedIn) {
            %>
               <div class="dashboard_div">
                <%@include file="AfterLoginTop.jsp" %> <%} else {%>
                <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include> <%}%>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Advanced Search for Contracts
                                <%if(objUsrTypeId!=null && objUsrTypeId!=""){%>
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('8');">Save as PDF</a></span>
                                    <%}%>
                                </div>
                                <div id="resultDiv">
                                    <form id="frmAdvSearchNOA" name="frmAdvSearchNOA" method="post" action="">
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 formBg_1 t_space" width="100%">
                                            <tr>
                                                <td class="ff">Select Hierarchy Node :</td>
                                                <td>
                                                    <input type="text" style="width: 200px;" readonly onblur="showHide();checkCondition();" id="txtdepartment" name="txtdepartment" class="formTxtBox_1">
                                                    <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                    <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                        <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                    </a>
                                                </td>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <!--<td class="ff">Procuring Entity :</td>-->
                                                <!--Change Procuring Entity to Procuring Agency by Proshanto Kumar Saha-->
                                                <td class="ff">Procuring Agency :</td>
                                                <td>
                                                    <select style="width: 208px;" id="cmbOffice" class="formTxtBox_1" name="office">
                                                        <option value="0">-- Select Procuring Agency --
                                                        </option>
                                                    </select>
                                                </td>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
<!--                                                <td width="23%" class="ff">Dzongkhag / District :</td>
                                                <td width="29%">
                                                    <select style="width: 208px;" id="cmbDistrict" class="formTxtBox_1" name="district">
                                                    </select>
                                                </td>-->
                                               <input type="hidden" value=" " style="width: 208px;" id="cmbDistrict" class="formTxtBox_1" name="district">
                                               <td width="12%" class="ff">Contract No. :</td>
                                                <td width="36%">
                                                    <input name="txtContractNo" type="text" class="formTxtBox_1" id="txtContractNo" style="width:200px;" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="ff">Tender ID :</td>
                                                <td>
                                                    <input name="txtTenderId" type="text" class="formTxtBox_1" id="txtTenderId" style="width:200px;" /></td>
                                                <td class="ff">Tender Ref. No :</td>
                                                <td>
                                                    <input name="txtTenderRefNo" type="text" class="formTxtBox_1" id="txtTenderRefNo" style="width:200px;" /></td>
                                            </tr>
                                            <tr>
                                                <!--Change BTN to BTN  by Proshanto Kumar Saha-->
                                                <!--<td class="ff">Value (In BTN) :</td>-->
                                                <td class="ff">Value (In Nu.) :</td>
                                                <td>
                                                    <input name="txtContractAmt" type="text" class="formTxtBox_1" id="txtContractAmt" style="width:200px;" /></td>
                                                <td class="ff">Keyword :</td>
                                                <td>
                                                    <input name="txtKWord" type="text" class="formTxtBox_1" id="txtKWord" style="width:200px;" /></td>
                                            </tr>
                                            <!--Code by Proshanto Kumar Saha-->
                                            <tr>
                                                <td class="ff">Advertisement Date From :</td>
                                                <td>
                                                    <input name="advDt" id="advDt" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('advDt','advDt');"/>&nbsp;
                                                    <a href="javascript:void(0);" title="Calender"><img id="advDtImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('advDt','advDtImg');"/>
                                                    </a>
                                                </td>
                                                <td class="ff">Advertisement Date To :</td>
                                                <td>
                                                    <input name="advDtTo" id="advDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('advDtTo','advDtTo');"/>&nbsp;
                                                    <a href="javascript:void(0);" title="Calender"><img id="advDtImg2" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('advDtTo','advDtImg2');"/>
                                                    </a>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td class="ff">Date of Notification of <br/> Award From :</td>
                                                <td>
                                                    <input name="noaDt" id="noaDt" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('noaDt','noaDt');"/>&nbsp;
                                                    <a href="javascript:void(0);" title="Calender"><img id="noaDtImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('noaDt','noaDtImg');"/>
                                                    </a>
                                                </td>
                                                <td class="ff">Date of Letter of Acceptance To :</td>
                                                <td>
                                                    <input name="noaDtTo" id="noaDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('noaDtTo','noaDtTo');"/>&nbsp;
                                                    <a href="javascript:void(0);" title="Calender"><img id="noaDtImg2" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('noaDtTo','noaDtImg2');"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <!--Code end by Proshanto Kumar Saha-->
                                            <tr>
                                                <td class="ff">Procurement Method</td>
                                                <td>
                                                    <select style="width: 208px;" id="cmbProcurementMethod" class="formTxtBox_1" name="procurementMethod">
                                                        <option value="" selected="selected">- Select Procurement Method -</option>
                                                        <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                                            <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                                        </c:forEach>
                                                    </select>
                                                </td>
                                                <td class="ff">Contract Award to</td>
                                                <td><input name="contractAwardTo" type="text" class="formTxtBox_1" id="txtContractAwardTo" style="width:200px;" /></td>
                                            </tr>
                                            <!--                                            <tr>
                                                                                            <td class="ff">From Contract Date :</td>
                                                                                            <td>
                                                                                                <input name="closeDtFrm" id="closeDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('closeDtFrm','closeDtFrm');"/>&nbsp;
                                                                                                <a href="javascript:void(0);" title="Calender"><img id="closeDtFrmImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtFrm','closeDtFrmImg');"/>
                                                                                                </a>
                                                                                                <span id="dtErrClose" class="reqF_1"><br /></span>
                                                                                            </td>
                                                                                            <td class="ff">To Contract Date :</td>
                                                                                            <td>
                                                                                                <input name="closeDtTo" id="closeDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('closeDtTo','closeDtTo');"/>&nbsp;
                                                                                                <a href="javascript:void(0);" title="Calender"><img id="closeDtToImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtTo','closeDtToImg');"/>
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>-->
                                            <!--                                            <tr>
                                                                                            <td class="ff">Category :</td>
                                                                                            <td colspan="3" class="t-align-left">
                                                                                                <textarea class="formTxtBox_1" style="width: 38%;" rows="3" id="txtaCPV" name="CPV"></textarea>
                                                                                                <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()">Select Category</a>
                                                                                            </td>
                                                                                        </tr>-->
                                            <tr>
                                                <td colspan="4" class="t-align-center">
                                                    <label class="formBtn_1 t_space">
                                                        <input type="button" name="btnSearch" onclick="loadTable();" id="btnSearch" value="Search" /></label>
                                                    <label class="formBtn_1 t_space">    <input type="reset" name="btnReset" id="btnReset" value="Reset" onclick="callReset();" /></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                    <div class="pageHead_1">Contracts Search Results</div>
                                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space">
                                        <div id="progressBar" class="tableHead_1 t_space" align="center" style="visibility:hidden"  >
                                            <img id="theImg" alt="ddd" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                                        </div>
                                        <tr>
                                            <th width="3%" class="t-align-center">Sl. No.</th>
                                            <th width="20%" class="t-align-center">Hierarchy Node<!--,<br />Agency--></th>
                                            <th width="10%" class="t-align-center">Tender ID, Ref No., <br/> Title & Advertisement Date</th>
                                            <!--<th width="20%" class="t-align-center">Procuring Entity<br/>Procurement Method</th>-->
                                            <th width="20%" class="t-align-center">Procuring Agency,<br/>Procurement Method</th>
<!--                                            <th width="10%" class="t-align-center">Dzongkhag/ District</th>-->
                                            <th width="15%" class="t-align-center">Date of<br/>Notification<br/>of Award</th>
                                            <th width="15%" class="t-align-center">Contract<br/>Award to</th>
                                            <!--Change BTN to BTN  by Proshanto Kumar Saha-->
                                            <!--<th width="7%" class="t-align-center">Value<br/>(In BTN)</th>-->
                                            <th width="7%" class="t-align-center">Value<br/>(In Nu.)</th>
                                        </tr>
                                    </table>
                                    <table width="100%" id="pagination" border="0" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                             <%if(objUsrTypeId!=null && objUsrTypeId!=""){%>
                                                 Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/>
                                                 <%}else{%>
                                                 <input type="hidden" name="size" id="size" value="10"/>
                                                 <%}%>
                                            </td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td align="right" class="prevNext-container"><ul>
                                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <!--<input type="hidden" name="size" id="size" value="10"/>-->
                                    </div>
                                    <form id="formstyle" action="" method="post" name="formstyle">

                                           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                                           <%
                                             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                             String appenddate = dateFormat1.format(new Date());
                                           %>
                                           <input type="hidden" name="fileName" id="fileName" value="AdvSearchNOA_<%=appenddate%>" />
                                            <input type="hidden" name="id" id="id" value="AdvSearchNOA" />
                                        </form>
                                        <div>&nbsp;</div>
                                </div>
                                <script type="text/javascript">
                                    chngTab("3");
                                </script>
                                <div>&nbsp;</div>
                            </div>
                        </td>
                        <% if (objUName == null) { %>
                        <td width="266" class="td-background">
                            <jsp:include page="Left.jsp" ></jsp:include>
                        </td>
                         <% }%>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
          </div>
        </div>
    </body>

</html>
<%
            advAppSearchSrBean = null;
%>
