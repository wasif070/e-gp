<%--
    Document   : StdSearch
    Created on : Nov 3, 2010, 7:32:25 PM
    Author     : Administrator,rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
            StringBuilder mode = new StringBuilder();

            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");

            String userId="0";
            if(session.getAttribute("userId")!=null){
                userId=session.getAttribute("userId").toString();
            }
            
            
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }

            if (isLoggedIn) {
                mode.append("WatchList");
            } else {
                mode.append("Search");
            }
            StringBuilder userType = new StringBuilder();
            if (request.getParameter("hdnUserType") != null) {
                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                } else {
                    userType.append("org");
                }
            } else {
                userType.append("org");
            }
            String query = request.getParameter("keyword");
            if ("null".equals(query)) {
                query = " ";
            }
            String officeId = "";
            if(request.getParameter("officeId") != null && !request.getParameter("officeId").equalsIgnoreCase("")){
                officeId = request.getParameter("officeId");
            }
            String keyword = null;
            if(request.getParameter("keyword") != null){
                keyword = request.getParameter("keyword");
            }
            /*String appId = "";
            if(request.getParameter("appId") != null && !request.getParameter("appId").equalsIgnoreCase("")){
                appId = request.getParameter("appId");
            }*/
            String bTypeId = "";
            if(request.getParameter("bTypeId") != null && !request.getParameter("bTypeId").equalsIgnoreCase("")){
                bTypeId = request.getParameter("bTypeId");
            }
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Annual Procurement Plan Search Results</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../js/pngFix.js"></script>--%>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {
//                if($("#keyWord").val() == undefined)
//                    $("#keyWord").val('');
                //$.post("<%//request.getContextPath()%>/SearchServlet", {keyWord: $("#keyWord").val(),appId:<%//appId %>,pageNo: $("#pageNo").val(),office: <%//officeId %>,action:'Search',size: $("#size").val()},  function(j){
                $.post("<%=request.getContextPath()%>/SearchAPPServlet", {bTypeId: '<%=bTypeId%>',pageNo: $("#pageNo").val(),office: '<%=officeId %>',action:'advSearch',size: $("#size").val(),keyWord:'<%=keyword %>'},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }

            function validateSearch(){
                var stateId = $("#cmbDistrict").val();
                var deptId = $("#txtdepartmentid").val();
                var officeId = $("#cmbOffice").val();

                var err_state = 0;
                var err_org = 0;
                var err_office = 0;

                document.getElementById('msgdistrict').innerHTML = '';
                document.getElementById('msgdivision').innerHTML = '';
                document.getElementById('msgoffice').innerHTML = '';
                
                if(stateId==0){
                    document.getElementById('msgdistrict').innerHTML = 'Please Select Dzongkhag / District';
                    err_state = 1;
                }
                if(deptId==''){
                    document.getElementById('msgdivision').innerHTML = 'Please Select Organization';
                    err_org = 1;
                }
                if(officeId==0){
                    document.getElementById('msgoffice').innerHTML = 'Please Select PE Office';
                    err_office = 1;
                }
                
                if(err_state==0 && err_org==0 && err_office==0)
                {
                    loadTable();
                }
            }
            
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">

            function loadOffice() {
                var deptId= $('#txtdepartmentid').val();
                var districtId = $('#cmbDistrict').val();

                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId, districtId: districtId, funName:'peofficeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
    <input type="hidden" name="keyWord" id="keyWord" value="<%=query%>"/>

</head>
<body onload="loadTable();">
    <div class="mainDiv">
        <%
                    if (isLoggedIn) {
        %>
        <div class="dashboard_div">
            <%@include file="AfterLoginTop.jsp" %> <%} else {%>
            <div class="fixDiv">
                <jsp:include page="Top.jsp" ></jsp:include> <%}%>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            <%
                                TenderCommonService commonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                commonService.setLogUserId(userId);
                                List<SPTenderCommonData> details = commonService.returndata("GetAppDetailById", officeId, "0");
                            %>
                                <div class="pageHead_1">Annual Procurement Plan Search Results</div>
                                <%if(!details.isEmpty()){%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space" width="100%">
                                    <tr>
                                        <td class="ff" nowrap>Hierarchy Node :</td>
                                        <td><%=details.get(0).getFieldName1() %></td>
<!--                                        <td class="ff" nowrap>Division :</td>
                                        <td><%=details.get(0).getFieldName2() %></td>-->
                                    </tr>
                                    <tr>
<!--                                        <td class="ff" nowrap>Organization :</td>
                                        <td><%=details.get(0).getFieldName3() %></td>-->
                                        <!--Change PE office to PA office by Proshanto Kumar Saha-->
                                        <td class="ff" nowrap>PA Office :</td>
                                        <td><%=details.get(0).getFieldName4() %></td>
                                    </tr>
                                    <tr>
<!--                                        <td class="ff" nowrap>Dzongkhag / District :</td>
                                        <td><%=details.get(0).getFieldName6() %></td>-->
                                        <td class="ff" nowrap>Budget Type :</td>
                                        <td>
                                            <!--Change Development to Capital and Revenue to Recurrent by Proshanto Kumar Saha-->
                                            <%
                                            if(bTypeId.equalsIgnoreCase("1")){%>Capital Budget<%
                                            }else if(bTypeId.equalsIgnoreCase("2")){%>Recurrent Budget<%
                                            }else if(bTypeId.equalsIgnoreCase("3")){%>Own Fund<% }
                                            %>
                                        </td>
                                    </tr>
                                </table>
                                <% } %>
<!--                                <div class="formBg_1 t_space">
                                    <form action="<%//request.getContextPath()%>/SearchServlet" id="frmAdvSerach" method="POST">
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                            <tr>
                                                <td class="ff">District : <span class="mandatory">*</span></td>
                                                <%
                                                            /*CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                                            short countryId = 136;
                                                            List<TblStateMaster> liststate = cservice.getState(countryId);*/
                                                %>
                                                <td>
                                                    <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:208px;">
                                                        <option value="0">-- Select --</option>
                                                        <%
                                                                    /*for (TblStateMaster state : liststate) {
                                                                        out.println("<option value='" + state.getStateId() + "'>" + state.getStateName() + "</option>");
                                                                    }*/
                                                        %>
                                                    </select>
                                                    <span id="msgdistrict" class="reqF_1"></span>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="ff" width="150px">Select Hierarchy Node : <span class="mandatory">*</span></td>
                                                <td>
                                                    <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 200px;"
                                                           id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                    <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                    <a href="#" onclick="javascript:window.open('<%//request.getContextPath()%>/resources/common/DeptTree.jsp?operation=AdvAPPSearch', '', 'width=350px,height=400px,scrollbars=1','');">
                                                        <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%//request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                    </a>
                                                    <span id="msgdivision" class="reqF_1"></span>
                                                </td>
                                                <td class="ff">PE Office : <span class="mandatory">*</span></td>
                                                <td>
                                                    <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:208px;">
                                                        <option value="0">-- Select --</option>
                                                    </select>
                                                    <span id="msgoffice" class="reqF_1"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">&nbsp;</td>
                                                <td colspan="3"><label class="formBtn_1"><input type="button" name="search" id="btnSearch" value="Search" onClick="validateSearch();"/></label>
                                                    <label>&nbsp;</label>
                                                    <label class="formBtn_1"><input type="reset" name="reset" id="btnReset" value="Reset"/></label></td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>-->
                                <div id="resultDiv" style="display: block;">
                                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space">
                                        <tr>
                                            <th width="5%" class="t-align-center">Sl. No.</th>
<!--                                            <th width="5%" class="t-align-center">APP ID, <br />APP Code</th>-->
                                            <th width="10%" class="t-align-center">APP ID</th>
                                            <th width="15%" class="t-align-center">Letter Ref. No.</th>
<!--                                            <th width="20%" class="t-align-center">Ministry, Division, Agency, PE</th>-->
<!--                                            <th width="5%" class="t-align-center">District</th>-->
                                            <th width="20%" class="t-align-center">Procurement Category,<br />
                                                Project Name</th>
                                            <th width="30%" class="t-align-center">Package No.,<br /> Package Description</th>
                                            <th width="20%" class="t-align-center"><p>Estimated Cost/Official Cost Estimate,<br/></p>
                                                <p> Procurement Method</p></th>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td align="right" class="prevNext-container"><ul>
                                                    <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" id="size" value="10"/>
                                </div>
                                <script type="text/javascript">
                                    //loadTable();
                                    /*if ($.trim($("#keyWord").val()) == '' || $.trim($("#keyWord").val()) == 'Type your Keyword here') {

                                    }else{
                                        $("#txtKeyword").val($("#keyWord").val());
                                    }*/
                                    chngTab("2");
                                </script>
                                <div>&nbsp;</div>
                        </td>
                        <% if (objUName == null) {%>
                        <td width="266"><jsp:include page="Left.jsp" ></jsp:include></td> <% }%>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </div>
</body>
</html>
