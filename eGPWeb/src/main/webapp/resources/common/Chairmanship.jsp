<%-- 
    Document   : Chairmanship
    Created on : May 10, 2016, 6:52:17 PM
    Author     : PROSHANTO
--%>

<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chairmanship</title>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" /> 
        <link href="<%=request.getContextPath()%>/resources/css/accordian.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.validate.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/accordion.js" type="text/javascript"></script>
    </head>
    <body>
         <%
            BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
            List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = bhutanDebarmentCommitteeService.getAllTblBhutanDebarmentCommittee();
          %>
         <div class="mainDiv">
            <div class="fixDiv">
                <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                    <%@include file="AfterLoginTop.jsp" %>
                <% } else { %>
                    <jsp:include page="Top.jsp" ></jsp:include>
                <% } %>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         <div class="pageHead_1">Chairmanship</div>
                         <%
                                if(tblBhutanDebarmentCommittee.isEmpty())
                                {
                                    %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                                }
                                else{
                            %>
                         <table border="0" class="tableList_1 t_space" width="100%">
                             <tr>
                                 <th style="width: 4%">
                                     <strong>Sl. <br/> No</strong> 
                                 </th>
                                 <th>
                                     <strong>Name</strong>  
                                 </th>
                                 <th>
                                     <strong>Designation</strong>  
                                 </th>
                                 <th>
                                     <strong>From</strong>  
                                 </th>
                                 <th>
                                     <strong>To</strong>  
                                 </th>
                                 <th>
                                     <strong>Remarks</strong>
                                 </th>
                             </tr>
                             <!--<tr>
                                 <td>
                                    1 
                                 </td>
                                 <td>
                                     Dasho Nima Tshering / 1st  Chairman 
                                 </td>
                                 <td>
                                     Director General
                                 </td>
                                 <td>
                                     July, 2015
                                 </td>
                                 <td>
                                     December, 2015
                                 </td>
                                 <td>
                                     Superannuated
                                 </td>
                             </tr>-->
                             <%
                                  int cnt = 0;
                                  for(TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeThis : tblBhutanDebarmentCommittee){
                                      if(tblBhutanDebarmentCommitteeThis.getRole().equals("Chairman") && tblBhutanDebarmentCommitteeThis.isIsActive()){
                                        cnt++;
                                        %>
                                        <tr>
                                              <td>
                                                  <%out.print(cnt);%>
                                              </td>
                                              <td>
                                                  <%out.print(tblBhutanDebarmentCommitteeThis.getMemberName());%>
                                              </td>
                                              <td>
                                                  <%out.print(tblBhutanDebarmentCommitteeThis.getDesignation());%>
                                              </td>
                                              <td>
                                                  <%
                                                      DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");
                                                      out.print(dF.format(tblBhutanDebarmentCommitteeThis.getFromDate()));%>
                                              </td>
                                              <td>
                                                  <%out.print(dF.format(tblBhutanDebarmentCommitteeThis.getToDate()));%>
                                              </td>
                                              <td>
                                                  <%out.print(tblBhutanDebarmentCommitteeThis.getRemarks());%>
                                              </td>
                                        </tr>
                                <%}}%>
                         </table>
                                    <%}%>
                            <!--Page Content End-->     
                        </td>
                        <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                            
                        <% } else { %>
                            <td width="266">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                        <% } %>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
