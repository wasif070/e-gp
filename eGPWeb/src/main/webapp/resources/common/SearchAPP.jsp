<%--
    Document   : searchAPP
    Created on : Feb 4, 2011, 12:23:34 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
            StringBuilder mode = new StringBuilder();

            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");

            boolean isLoggedIn = false;
            ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
            Object objApprovedUserID = new Object();
            
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
                objApprovedUserID = cas.getUserApprovedTenderId(objUserId.toString());
            }
            if (objUName != null) {
                isLoggedIn = true;
            }

            if (isLoggedIn) {
                mode.append("WatchList");
            } else {
                mode.append("Search");
            }
            StringBuilder userType = new StringBuilder();
            if (request.getParameter("hdnUserType") != null) {
                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                } else {
                    userType.append("org");
                }
            } else {
                userType.append("org");
            }
            String query = request.getParameter("keyword");
            if ("null".equals(query)) {
                query = " ";
            }
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Annual Procurement Plan Search</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../js/pngFix.js"></script>--%>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
        <%
            String logUserId="0";
            if(session.getAttribute("userId")!=null){
                logUserId=session.getAttribute("userId").toString();
            }
            appServlet.setLogUserId(logUserId);
        %>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function searchTable()
            {
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/SearchAPPServlet", {stateId : $('#cmbDistrict').val(),departmentid:$("#txtdepartmentid").val(),financialYear: $("#cmbFinancialYear").val(),pageNo: $("#pageNo").val(),officeId: $("#cmbOffice").val(),action:'Search',size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
            function loadTable()
            {
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/SearchAPPServlet", {stateId : $('#cmbDistrict').val() , financialYear: $('#cmbFinancialYear').val(), departmentid:$("#txtdepartmentid").val(),pageNo: $("#pageNo").val(),officeId: $("#cmbOffice").val(),action:'Search',size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }

            function validateSearch()
            {
                var stateId = $("#cmbDistrict").val();
                var deptId = $("#txtdepartment").val();
                //var officeId = $("#cmbOffice").val();
                
                var err_state = 0;
                var err_org = 0;
                var err_office = 0;

                document.getElementById('msgdistrict').innerHTML = '';
                document.getElementById('msgdivision').innerHTML = '';
                document.getElementById('msgoffice').innerHTML = '';

//                if(stateId==0){
//                    document.getElementById('msgdistrict').innerHTML = 'Please select District';
//                    err_state = 1;
//                }
//                if(deptId==''){
//                    document.getElementById('msgdivision').innerHTML = 'Please select Organization';
//                    err_org = 1;
//                }
                //                if(officeId==0){
                //                    document.getElementById('msgoffice').innerHTML = 'Please Select PE Office';
                //                    err_office = 1;
                //                }

//                if(err_state==0 && err_org==0 && err_office==0)
//                {
                    //loadTable();
                    searchTable();
                //}
            }

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
                    function loadOffice() {
                        var deptId= $('#txtdepartmentid').val();
                        var districtId = $('#cmbDistrict').val();
                
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId, districtId: districtId, funName:'peofficeCombo'},  function(j){
                            $('#cmbOffice').children().remove().end()
                            $("select#cmbOffice").html(j);
                        });
                    }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
<!--    <input type="text" name="keyWord" id="keyWord" value="<%//query%>"/>-->

    </head>
    <body onload="loadTable()">
        <div class="mainDiv">
            <!--While the user is logged in and clicks on the "APP" tab it was redirecting to advanced menu -->
            
            <%if (isLoggedIn) {
                if(!objApprovedUserID.toString().equalsIgnoreCase("Approved")){
                %>
                    <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include>
                <%} else {%>
                    <div class="dashboard_div">
                    <%@include file="AfterLoginTop.jsp" %>
            <%}} else {%>
                <div class="fixDiv">
                <jsp:include page="Top.jsp" ></jsp:include>
            <%}%>        <!--Middle Content Table Start-->
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea-Blogin">
                                <!--Page Content Start-->
                                <div class="pageHead_1">Annual Procurement Plan (APP) Search Results</div>
                                <div class="formBg_1 t_space">
                                    <form action="<%=request.getContextPath()%>/SearchServlet" id="frmAdvSerach" method="POST">
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                            <tr>
<!--                                                <td width="20%" class="ff">Dzongkhag / District : </td>
                                                <%
                                                            CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                                            //Code by Proshanto
                                                            short countryId = 150;//136
                                                            List<TblStateMaster> liststate = cservice.getState(countryId);
                                                %>
                                                <td width="30%">
                                                    <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                                        <option value="0">-- Select --</option>
                                                        <%
                                                                    for (TblStateMaster state : liststate) {
                                                                        out.println("<option value='" + state.getStateId() + "'>" + state.getStateName() + "</option>");
                                                                    }
                                                        %>
                                                    </select>-->
                                                    <input type="hidden" value="0" name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                                    <span id="msgdistrict" class="reqF_1"></span>
                                                </td>
                                                <%
                                                            java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                                            appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");%>
                                                <td width="20%" class="ff">Financial Year :</td>
                                                <td width="30%">
                                                    <select name="financialYear" class="formTxtBox_1" id="cmbFinancialYear" style="width:95%;">
                                                        <%
                                                                    for (CommonAppData commonApp : appListDtBean) {
                                                                        if (commonApp.getFieldName3().equals("Yes")) {
                                                        %>
                                                        <option selected="selected" value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                                        <%
                                                                                                                            } else {
                                                        %>
                                                        <option value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                                        <% }
                                                                }%>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Hierarchy Node :</td>
                                                <td>
                                                    <input class="formTxtBox_1" name="txtdepartment" id="txtdepartment" type="text" style="width: 75%;"
                                                           id="txtdepartment"   readonly style="width: 75%;"/>
                                                    <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" value="0" onchange="loadOffice();" />

                                                    <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=AdvAPPSearch', '', 'width=350px,height=400px,scrollbars=1','');">
                                                        <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                    </a>
                                                    <span id="msgdivision" class="reqF_1"></span>
                                                </td>
                                                <!--Change PE to PA by Proshanto Kumar Saha-->
                                                <td class="ff">PA Office : <!--<span class="mandatory">*</span>--></td>
                                                <td>
                                                    <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:95%;" onfocus="loadOffice();">
                                                        <option value="0">-- Select --</option>
                                                    </select>
                                                    <span id="msgoffice" class="reqF_1"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="t-align-center"><label class="formBtn_1"><input type="button" name="search" id="btnSearch" value="Search" onClick="validateSearch();"/></label>
                                                    <label>&nbsp;</label>
                                                    <label class="formBtn_1">
                                                        <input type="button" name="reset" id="btnReset" value="Reset" onclick="document.getElementById('frmReset').submit();" />
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                    <form name="frmReset" id="frmReset" method="post"></form>
                                </div>
                                <div id="resultDiv" class="t_space" style="display: block;">
                                    <!--<label>DB = Development Budget, RB = Revenue Budget, OF = Own Fund</label>-->
                                    <!--Code by Proshanto Kumar Saha-->
                                    <div style="text-align: right; font-style: italic; font-size: 12px">CB = Capital Budget, RB = Recurrent Budget, OF = Own Fund</div>
                                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                                        <tr>
                                            <th class="t-align-center">Hierarchy Node</th>
<!--                                            <th class="t-align-center">Department</th>
                                            <th class="t-align-center">Organization</th>-->
                                            <!--Change PE to PA by Proshanto Kumar Saha-->
                                            <th class="t-align-center">PA Office</th>
                                            <th class="t-align-center">Annual Procurement Plan (APP)</th>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td align="right" class="prevNext-container"><ul>
                                                    <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" id="size" value="10"/>
                                </div>
                                <script type="text/javascript">
                                    //loadTable();
                                    if ($.trim($("#keyWord").val()) == '' || $.trim($("#keyWord").val()) == 'Type your Keyword here') {

                                    }else{
                                        $("#txtKeyword").val($("#keyWord").val());
                                    }
                                    chngTab("2");
                                </script>
                                <div>&nbsp;</div>
                            </td>
                            <% if (objUName == null) {%>
                            <td width="266"><jsp:include page="Left.jsp" ></jsp:include></td> <% }%>
                        </tr>
                    </table>
                    <!--Middle Content Table End-->
                    <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </div>
    </body>
</html>
<%
            appServlet = null;

%>