<%-- 
    Document   : ViewWCCertificateDetails
    Created on : Aug 3, 2011, 12:21:36 PM
    Author     : Rikin
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<jsp:useBean id="cmsWcCertificateServiceBean" class="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean" />
<jsp:useBean id="cmsVendorRatingMasterServiceBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorRatingMasterServiceBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
        %>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../js/jQuery/print/jquery.txt"></script>
        <script src="../js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <title>Work completion certificate</title>
    </head>
    <%
                String tenderId = "";
                String userId = "";
                String lotId ="";
                String wcCertId = "";
                
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int wcCertHistId = 0;
                if (request.getParameter("wcCertHistId") != null) {
                    wcCertHistId = Integer.parseInt(request.getParameter("wcCertHistId"));
                }
                if (session.getAttribute("userId") == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                } else {
                    userId = session.getAttribute("userId").toString();
                }                
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                }
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                if (request.getParameter("wcCertId") != null) {
                    wcCertId = request.getParameter("wcCertId");
                }
                
                List<Object[]> certHistDetail = cmsWcCertificateServiceBean.getWcCetificateHistoryDetails(wcCertHistId);
    %>
    <body onload="setHdnStar()">
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <div  id="print_area">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                              <%@include  file="ContractInfoBar.jsp"%>
                            <br/>
                            <div class="pageHead_1">View Work Completion Certificate
                                <span style="float: right; text-align: right;" class="noprint">
                                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    <%
                                        if(request.getParameter("msg")!=null)
                                        {
                                        //if(request.getHeader("referer")!=null){
                                        //String referrer = request.getHeader("referer").toString();
                                        //if(referrer.contains("officer")){
                                    //if(request.getHeader("referer")!=null){
                                        //if(request.getHeader("referer").contains("officer")){
                                    %>
                                        <a class="action-button-goback" href="../../officer/ViewWCCertificate.jsp?wcCertId=<%=wcCertId%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>">Go back</a>
                                    <% }else{ %>
                                        <a class="action-button-goback" href="../../tenderer/ViewWCCertificate.jsp?wcCertId=<%=wcCertId%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>">Go back</a>
                                    <% }%>
                                    
                                </span>
                            </div>                            
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                <tr>
                                    <td class='ff' class='t-align-left' width="30%">
                                        Actual Contract Completion Date :
                                    </td>      
                                    <td class='t-align-left'>
                                        <%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(certHistDetail.get(0)[0].toString(),"yyyy-MM-dd HH:mm:ss"))%>
                                    </td> 
                                </tr>
                                <tr>
                                    <td class='ff' class='t-align-left'>
                                        Is Work Completed :
                                    </td>      
                                    <td class='t-align-left'>
                                        <%=certHistDetail.get(0)[1]%>
                                    </td> 
                                </tr>
                                <%if(!"6".equalsIgnoreCase(certHistDetail.get(0)[2].toString())){%>
                                <tr>
                                    <td class='ff' class='t-align-left'>
                                        Vendor Rating :
                                    </td>      
                                    <td class='t-align-left'>
                                        <%
                                            TblCmsVendorRatingMaster tblCmsVendorRatingMaster = cmsVendorRatingMasterServiceBean.getCmsVendorRatingMaster(Integer.parseInt(certHistDetail.get(0)[2].toString()));
                                        %>
                                       <% out.print(tblCmsVendorRatingMaster.getVendorRatings()); %> &nbsp;&nbsp;
<!--                                       <b><% out.print(tblCmsVendorRatingMaster.getVendorStars()); %></b>-->
                                       <span id="ratingId"></span>
                                    </td> 
                                </tr>
                                <%}%>
                                <%--<tr>
                                    <td class='ff' class='t-align-left'>
                                        Send To Tenderer:
                                    </td>      
                                    <td class='t-align-left'>
                                        <%
                                                    if ("Y".equalsIgnoreCase(certHistDetail.get(0)[4].toString())){
                                                        out.print("Yes");
                                                    } else {
                                                        out.print("NO");
                                                    }
                                        %>
                                    </td> 
                                </tr>--%>
                                <tr>
                                    <td class='ff' class='t-align-left'>
                                        Remarks:
                                    </td>      
                                    <td class='t-align-left'>
                                        <%=certHistDetail.get(0)[3]%>
                                    </td> 
                                </tr>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End--></div>
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <script type="text/javascript">
           function setHdnStar(){
            var hdnStarObj = document.getElementById("ratingId");
            if("<%=certHistDetail.get(0)[2].toString()%>"=="1"){
                hdnStarObj.innerHTML = "<img src=/resources/images/star.jpeg />";
            }else if("<%=certHistDetail.get(0)[2].toString()%>"=="2"){
                hdnStarObj.innerHTML = "<img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg />";
            }else if("<%=certHistDetail.get(0)[2].toString()%>"=="3"){
                hdnStarObj.innerHTML = "<img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg />";
            }else if("<%=certHistDetail.get(0)[2].toString()%>"=="4"){
                hdnStarObj.innerHTML = "<img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg />";
            }else if("<%=certHistDetail.get(0)[2].toString()%>"=="5"){
                hdnStarObj.innerHTML = "<img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg /><img src=/resources/images/star.jpeg />";            
            }else{
                hdnStarObj.innerHTML = "";
            } 
        } 
        </script>        
    </body>
<!--            <script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>-->

</html>

