<%-- 
    Document   : CommonViewInvoice
    Created on : Aug 9, 2011, 12:00:59 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
       String iv_invoiceid = pageContext.getAttribute("invoiceId").toString();
       String invoiceNo = "";
       int iv_wpId = Integer.parseInt(pageContext.getAttribute("wpId").toString());
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jQuery/jquery.validate.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../js/common.js" type="text/javascript"></script>
                <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/InvoiceGenerationServlet", {action:'viewInvoice',invId:'<%=iv_invoiceid%>' },  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    $('#resultDiv').show();
                });
            }
        </script>
    </head>
    <%
     CommonService commonService1 = (CommonService) AppContext.getSpringBean("CommonService");
                String procnature1 = commonService1.getProcNature(request.getParameter("tenderId")).toString();                
    %>
    <body>
      <div id="resultDiv" style="display: block;">
                            <div  id="print_area">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <%

                                TenderTablesSrBean beanCommon = new TenderTablesSrBean();
                                String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<Object[]> list = null;
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()),conId, "contractId", EgpModule.Payment.getName(), "View Invoice", "");
                                if(procnature1.equalsIgnoreCase("goods") || procnature1.equalsIgnoreCase("services")){
                                    if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                        {
                                        invoiceNo = pageContext.getAttribute("invoiceNo").toString();
                                      //  list = service.getListForICTInvoice(invoiceNo,iv_wpId);
                                         list = service.returndata("getListForICTInvoice",invoiceNo,String.valueOf(iv_wpId));
                                        }
                                    else
                                        list = service.getListForInvoice(Integer.parseInt(iv_invoiceid));
                                    if(list!=null && !list.isEmpty()){%>
                                     <th width="3%" class="t-align-center">S.No</th>
                                        <th width="20%" class="t-align-center">Description</th>
                                        <th width="15%" class="t-align-center">Unit<br />
                                            of Measurement
                                            <br />
                                        </th>
                                        <th width="10%" class="t-align-center">Invoice Qty
                                        </th>
                                        <th width="10%" class="t-align-center">Qty</th>
                                        <th width="10%" class="t-align-center">Rate</th>
                                         <%      
                                                 if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                             {
                                        %>
                                        <th width="10%" class="t-align-center">Currency
                                        </th>
                                         <th width="12%" class="t-align-center ICT">Supplier VAT + Other Local Cost (in Nu.)
                                             </th>
                                       <th width="18%" class="t-align-center">Invoiced Amount(currency wise)
                                        </th>
                                             
                                             <% }else{ %>
                                        <th width="18%" class="t-align-center">Invoice Amount
                                        </th>
                                    <%}for (Object[] oob : list) {
                                        if(oob[0] != null){
                                %>
                                <tr>
                                    <td class="t-align-center"><%=oob[0].toString()%></td>
                                    <td class="t-align-center"><%=oob[1].toString()%></td>
                                    <td class="t-align-center"><%=oob[2].toString()%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[3].toString()).setScale(2, 0)%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[8].toString()).setScale(2, 0)%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[7].toString()).setScale(3, 0)%></td>
                                    <%
                                       if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                       {
                                    %>
                                         <td class="t-align-right" style="text-align: right;"><%=oob[9].toString()%></td>
                                        <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[10].toString()).setScale(3,0)%></td>
                                    <% } %>
                                    <td class="t-align-right" style="text-align: right;"><%=oob[4].toString()%></td>
                                <tr>
                                <%}}%>
                                <% 
                              List<String> listStr = new ArrayList<String>();
                              listStr.add("BTN");
                                if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                {int i=0;int j=0;
                                for (Object[] oob : list) {
                                    if(oob[9].toString().equals("BTN"))
                                        {
                                        j=i;
                                        continue;
                                        }
                                    else if(!listStr.contains(oob[9].toString())){
                                        i = i+1;
                                        listStr.add(oob[9].toString());
                                %>
                                <tr><td class="t-align-center" colspan=7></td>
                                    <td class="t-align-center ff">Total Amount (In <%=oob[9].toString()%>)</td>
                                    <td class="t-align-right ff" style="text-align: right;"><%=new BigDecimal(oob[5].toString()).setScale(3, 0)%></td>
                                <tr/>
                                 <% }}%>
                                   <tr><td class="t-align-center" colspan=7></td>
                                    <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                    <td class="t-align-right ff" style="text-align: right;"><%=new BigDecimal(list.get(j)[5].toString()).setScale(3, 0)%></td>
                                <tr/>

                            <%} else { %>
                                <tr><td class="t-align-center" colspan=5></td>
                                    <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                    <td class="t-align-right ff" style="text-align: right;"><%=new BigDecimal(list.get(0)[5].toString()).setScale(3, 0)%></td>
                                <tr/>

                                 <% } %>
                                
                                <input type="hidden" id="wpId" name="wpId" value = "<%=list.get(0)[6].toString()%>" />
                                <%}}else{
                                     list = service.getListForInvoiceForWorks(Integer.parseInt(iv_invoiceid));
                                    
                                    if(list!=null && !list.isEmpty()){%>
                                    <th width="3%" class="t-align-center">S.No</th>
                                        <th width="10%" class="t-align-center">Group</th>
                                        <th width="20%" class="t-align-center">Description</th>
                                        <th width="15%" class="t-align-center">Unit<br />
                                            of Measurement
                                            <br />
                                        </th>
                                        <th width="10%" class="t-align-center">Qty</th>
                                        <th width="15%" class="t-align-center">Invoice Qty
                                        </th>
                                        <th width="10%" class="t-align-center">Rate</th>
                                        <th width="18%" class="t-align-center">Invoice Amount (in Nu.)
                                        </th>
                                <%for (Object[] oob : list) {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=oob[0].toString()%></td>
                                    <td class="t-align-center"><%=oob[1].toString()%></td>
                                    <td class="t-align-center"><%=oob[2].toString()%></td>
                                    <td class="t-align-center"><%=oob[3].toString()%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[9].toString()).setScale(2, 0)%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[4].toString()).setScale(2, 0)%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=new BigDecimal(oob[8].toString()).setScale(3, 0)%></td>
                                    <td class="t-align-right" style="text-align: right;"><%=oob[5].toString()%></td>
                                <tr>
                                <%}%>
                                <tr><td class="t-align-center" colspan=6></td>
                                    <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                    <td class="t-align-right ff" style="text-align: right;"><%=list.get(0)[6]%></td>
                                <tr/>
                                <%}}%>
                                </table>
                            </div>
                        </div>
    </body>
</html>
