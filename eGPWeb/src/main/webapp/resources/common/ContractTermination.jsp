<%--
    Document   : ContractTermination
    Created on : Aug 5, 2011, 4:16:33 PM
    Author     : Sreenu.Durga
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <link href="../../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Contract Termination Page</title>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int contractUserId = 0;
                if (request.getParameter("contractUserId") != null) {
                    contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                }
                int contractSignId = 0;
                if (request.getParameter("contractSignId") != null) {
                    contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    lotId = request.getParameter("lotId");
                }
                CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
                cmsCTReasonTypeBean.setLogUserId(userId);
                List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();
                String perSAmt = "";
                NOAServiceImpl noaService = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                noaService.setLogUserId(userId);
                List<TblNoaIssueDetails> lstNoa = noaService.getPerformanceSecAmt(Integer.parseInt(tenderId), contractUserId, Integer.parseInt(lotId));
                if (!lstNoa.isEmpty() && lstNoa.size() > 0) {
                    perSAmt = lstNoa.get(0).getPerfSecAmt().toString();
                }
    %>
    <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
    %>
    <script language="javascript">
        /* Call Calendar function */
//        function GetCalendar(txtname,controlname)
//        {
//            new Calendar({
//                inputField: txtname,
//                trigger: controlname,
//                showTime: true,
//                dateFormat:"%d-%b-%Y",
//                onSelect: function() {
//                    var date = Calendar.intToDate(this.selection.get());
//                    alert(date.getDate());
//                    var DateVar = document.getElementById("curDate").value;
//                    var currentDate =Date.parse(DateVar);
//                    if(date > currentDate){
//                        jAlert("Date should be past or Current","Warning!");
//                        document.getElementById(txtname).value = "";
//                    }else{
//                        LEFT_CAL.args.min = date;
//                        LEFT_CAL.redraw();
//                    }
//                    this.hide();
//                }
//            });
//            var LEFT_CAL = Calendar.setup({
//                weekNumbers: false
//            })
//        }
function compareForToday(){
    
    var value = document.getElementById('txtDateOfTermination').value;
    var flag = "";
    if(value!=null && value!=''){
                    flag = true;
                var mdy = value.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split

                if(mdyhr[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }
                var d = new Date();
                if(mdyhr[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                if(Date.parse(valuedate) <= Date.parse(todaydate)){
                        flag =  true;
                }else{
                    $('#txtDateOfTermination').val('');
                        jAlert("Date should be past or Current","Warning!");
                                flag =  false;
                        }
                }
                return flag;
}
        function GetCalendar(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        compareForToday(date);
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });
                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
    </script>
    <script type="text/javascript">
//        $(document).ready(function(){
//            $("#formContractTermination").validate({
//                rules: {
//                    txtDateOfTermination:{required: true},
//                    checkReasonType:{required: true},
//                    radioReleasePG:{required: true}//,
//                    //txtReason:{required: true}
//                },
//                messages: {
//                    txtDateOfTermination: {
//                        required: "<div class='reqF_1'>Please select Date</div>"
//                    },
//                    checkReasonType:{
//                        required: "<div class='reqF_1'>Please check atleast one reason for Contract Termination</div>"
//                    },
//                    radioReleasePG: {
//                        required: "<div class='reqF_1'>Please select any one option</div>"
//                    }//,
//                    //txtReason: {
//                     //   required: "<div class='reqF_1'>Please enter description for Contract Termination</div>"
//                    //}
//                },
//                errorPlacement:function(error ,element){
//                    //error.insertBefore(element);
//                    error.insertAfter(element);
//                }
//            });
//        });                
    </script>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <%@include  file="../../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Contract Termination Page
                                <span style="float: right; text-align: right;">
                                    <%if(userTypeId==3){%>
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/officer/TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}else{%>
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/tenderer/TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                </span>
                            </div>
                            <form method="POST" id="formContractTermination" name="frm" action="<%= request.getContextPath() %>/ContractTerminationServlet?action=add">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                    <tr id="tblRow_mandatoryMessage">
                                        <td style="font-style: italic" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                                            <input type="hidden" name="contractUserId" id="contractUserId" value="<%=contractUserId%>" />
                                            <input type="hidden" name="contractSignId" id="contractSignId" value="<%=contractSignId%>" />
                                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>" />
                                        </td>
                                    </tr>
                                    <tr id="tblRow_dateOfTermination">
                                        <td class="ff">Select Contract Termination Date : <span>*</span></td>                                        
                                        <td class="ff">
                                            <input name="txtDateOfTermination" type="text" class="formTxtBox_1" id="txtDateOfTermination" readonly="true" onClick="GetCalendar('txtDateOfTermination','txtDateOfTermination');" />
                                            &nbsp;
                                            <a href="javascript:void(0);" onclick ="GetCalendar('txtDateOfTermination','imgCal');" title="Calender">
                                                <img id="imgCal" src="../../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                                            </a>
                                            <span id="txtDateOfTerminationspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_typeOfReason">
                                        <td class="ff">Select Reason for Contract Termination : <span>*</span></td>
                                        <td class="ff">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                                <%
                                                            for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                                                out.print("<tr>"
                                                                        + "<td class='ff'>"
                                                                        + " <input type='checkbox' name='checkReasonType' id='checkReasonType' value='"
                                                                        + cmsCTReasonType.getCtReasonTypeId() + "'>&nbsp;&nbsp;" + cmsCTReasonType.getCtReason()
                                                                        + "&nbsp;&nbsp;&nbsp;&nbsp;"
                                                                        + "</td>"
                                                                        + "</tr>");
                                                            }
                                                %>
                                            </table>
                                            <span id="checkReasonTypespan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <%--<%
                                        if(userTypeId==3)
                                        {
                                    %>
                                    <tr id="tblRow_releasePerformanceGuarantee" >
                                        <td  class="ff">Performance Security : <span>*</span></td>
                                        <td class="ff">
                                            <input type="radio" name="radioReleasePG" id="radioReleasePG"  value="release" >&nbsp;<b>Release</b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="radioReleasePG" id="radioReleasePG" value="forfeit" >&nbsp;<b>Compensiate</b>
                                            <span id="radioReleasePGspan" class="reqF_1"></span>
                                        </td>
                                        <%
                                                    if (!"".equals(perSAmt)) {
                                                        out.print("<td>");
                                                        out.print("Performance Security (In BTN): " + perSAmt);
                                                        out.print(" </td>");
                                                    }
                                        %>
                                    </tr>
                                    <%}%>--%>
                                    <%
                                        if(userTypeId==3)
                                        {
                                    %>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left">Payment Settlement Amount (In Nu.) :</td>
                                            <td   class="t-align-left">
                                                <input type="text" class="formTxtBox_1" name="PSAtxt" id="PSAtxtid" maxlength="12"/>
                                            <span id="PSAtxtspan" class="reqF_1"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17%" class="t-align-left ff">Remarks for Payment Settlement Amount :</td>
                                            <td width="83%" class="t-align-left">
                                                <label>
                                                    <textarea name="txtRemarks" rows="3" class="formTxtBox_1" id="txtRemarks" style="width:360px;"></textarea>
                                                </label>
                                                <span id="txtRemarksspan" class="reqF_1"></span>
                                            </td>
                                        </tr>
                                    <%}%>
                                    <tr id="tblRow_reason">
                                        <td  class="ff">Reason For Termination : <span>*</span></td>
                                        <td >
                                            <textarea name="txtReason" rows="5" cols="50" style="width:90%;" class="formTxtBox_1" id="txtReason" ></textarea>
                                            <script type="text/javascript">
                                                CKEDITOR.replace( 'txtReason',{toolbar : "egpToolbar"});
                                            </script>
                                            <span id="SPtxtNewsDetl" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_formButton">
                                        <td class="t-align-center" colspan="2" >
                                            <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Terminate" onclick="return chkSubmit();"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
      <script type="text/javascript">
       function chkSubmit()
        {
            var j=0; var m=0;
            var vbool = true;
            if(document.getElementById("txtDateOfTermination").value == "")
            {
                document.getElementById("txtDateOfTerminationspan").innerHTML = "Please select Date";
                vbool= false;
            }
            else
            {
               try{
                $.ajax({
                    url: "<%=request.getContextPath()%>/ContractTerminationServlet?param1="+$('#txtDateOfTermination').val()+"&tenderId="+$('#tenderId').val()+"&lotId="+$('#lotId').val()+"&action=validateTermidate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        if(j.toString()!="")
                        {
                            //document.getElementById("txtDateOfTerminationspan").innerHTML=j;
                            jAlert("Contract Termination Date must be greater than Actual Contract Start Date","Warning!");
                            flag = false;
                        }else{document.getElementById("txtDateOfTerminationspan").innerHTML="";}
                    }
                });
               }catch(e){
               }
            }

            for (i=0; i<document.frm.checkReasonType.length; i++){
            if (document.frm.checkReasonType[i].checked==true)
            {j++;}
            }
            if(j==0)
            {
               document.getElementById("checkReasonTypespan").innerHTML = "Please check atleast one reason for Contract Termination";
               vbool= false;
            }
            else{document.getElementById("checkReasonTypespan").innerHTML ="";}
            if(document.getElementById("PSAtxtid").value!="")
            {
               if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("PSAtxtid").value))
               {
                  if(/^\d*\.?\d*$/.test(document.getElementById("PSAtxtid").value))
                  {
                      if(validateDecimal(document.getElementById("PSAtxtid").value))
                      {
                        document.getElementById("PSAtxtspan").innerHTML= "";
                      }
                      else
                      {
                            vbool= false;
                            document.getElementById("PSAtxtspan").innerHTML = "only 3 digits after decimal are allowed";
                      }
                  }
                  else
                  {
                       vbool= false;
                       document.getElementById("PSAtxtspan").innerHTML = "special characters are not allowed";
                  }
               }
               else
               {
                    vbool= false;
                    document.getElementById("PSAtxtspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
               }
            }            
            if(document.getElementById("txtRemarks").value!="")
            {
                if(document.getElementById("txtRemarks").value.length<1000)
                {
                    document.getElementById("txtRemarksspan").innerHTML = "";
                }else{
                    document.getElementById("txtRemarksspan").innerHTML = "Maximum 1000 characters are allowed.";
                }
            }
            <%--<%
            if(userTypeId==3)
            {
            %>
                for( i = 0; i < document.frm.radioReleasePG.length; i++ )
                {
                 if( document.frm.radioReleasePG[i].checked == false )
                 {
                    m++;
                 }
                }
                if(m==2)
                {
                    document.getElementById("radioReleasePGspan").innerHTML = "Please select any one option";
                    vbool= false;
                }
                else{document.getElementById("radioReleasePGspan").innerHTML = "";}
            <%}%>--%>

            if($.trim(CKEDITOR.instances.txtReason.getData()) == ""){
                document.getElementById("SPtxtNewsDetl").innerHTML = "Please enter description for Contract Termination";
                vbool= false;
            }
            return vbool;
        }
        $(document).ready(function() {
                $(document).ready(function() {
                CKEDITOR.instances.txtReason.on('blur', function()
                {
                    if(CKEDITOR.instances.txtReason.getData() != 0)
                    {
                            document.getElementById("SPtxtNewsDetl").innerHTML="";
                    }
                });
            });
            });
            function validateDecimal(value)
            {
                var flag =false;
                var a = value;
                var b = a.indexOf('.');
                if(b>0)
                {
                    a = a.substring(b+1, a.length);
                    if(a.length<=3)
                    {
                        flag=true;
                    }
                }
                else
                {
                    flag =true;
                }
                return flag;
            }
      </script>
    </body>
</html>
