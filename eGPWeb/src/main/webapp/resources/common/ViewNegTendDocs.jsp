<%--
    Document   : NegotiationDocsUpload
    Created on : Dec 24, 2010, 11:45:06 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation Documents</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function checkfile()
            {
                var check = false;
                value1=document.getElementById('uploadDocFile').value;
                param="document";
                var ext = value1.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();
                if (value1 != '' && param=="uploadDocFile" && ( fileType =='pdf'|| fileType =='zip'|| fileType =='jpeg'||
                    fileType =='jpg' || fileType =='gif' || fileType =='doc' || fileType =='docx' ||fileType =='xls' || fileType =='xlsx' ||
                    fileType =='dwg' || fileType =='dwg' || fileType =='dxf')){
                    document.getElementById('filemsg').innerHTML='';
                    check = true;

                }
                if (document.getElementById('documentBrief').value=="" && check == false)
                {
                    document.getElementById('filemsg').innerHTML="Please select file.";
                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }
                if (document.getElementById('documentBrief').value=='' && check == true){

                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }else if (document.getElementById('documentBrief').value != '' && check == false){

                    document.getElementById('filemsg').innerHTML="Please select file.";
                    return false;
                }else{
                    document.getElementById('filemsg').innerHTML = '';
                    document.getElementById('dvDescpErMsg').innerHTML='';
                    return true;
                }

            }

        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
            
            <!--Dashboard Content Part Start-->
            <%
                String negId = "1";
                int tenderId = 0;
                int userType = 0;
                int userId=0;
                String negQueryId ="0";
                
                if (request.getParameter("negId") != null) {
                    negId = request.getParameter("negId");
                }
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if(request.getParameter("negQueryId")!=null){

                }

                session = request.getSession();
                userType = Integer.parseInt(session.getAttribute("userTypeId").toString());
                userId= Integer.parseInt(session.getAttribute("userId").toString());
                pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="AfterLoginTop.jsp" %>
            <div class="contentArea">
                <div class="pageHead_1">Negotiation Documents
                    <span style="float:right;">
                        <% if(userType==2){ %>
                            <a href="<%=request.getContextPath()%>/tenderer/NegoTendererProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                        <% }else{ %>
                            <a href="<%=request.getContextPath() %>/officer/NegotiationProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                        <% } %>
                    </span>
                </div>
                     <%@include file="TenderInfoBar.jsp" %>
                     
                          
                    
                <%  
                    if(userType==2)
                    { %>
                    <%pageContext.setAttribute("tab", "6");%>
                     <%@include file="../../tenderer/TendererTabPanel.jsp" %>
                        <div class="tabPanelArea_1">
                        <jsp:include page="../../tenderer/EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="4" />
                            <jsp:param name="tenderId" value="<%=tenderId%>" />
                        </jsp:include>
                <%  }
                    else
                    { 
                    
                %>
                         <% pageContext.setAttribute("tab", "7");%>
                        <%@include  file="../../officer/officerTabPanel.jsp"%>
                        <%-- Start: Common Evaluation Table --%>
                    <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                        <div class="tabPanelArea_1">
                        <%  pageContext.setAttribute("TSCtab", "7"); %>
                        <%@include file="AfterLoginTSC.jsp" %>
                    <% } %>    
                     <div class="tabPanelArea_1">

                   <div class="t-align-left ff">
                        Document uploaded during Negotiation Bid Process:
                   </div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="28%">File Uploaded by</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%
                            int docCnt = 0;
                            TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            for (SPTenderCommonData sptcd : tenderCS.returndata("NegotiationDocInfo", negId, "revisedoc")) {
                                docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-left">
                        <%
                            if(userId != Integer.parseInt(sptcd.getFieldName5()))
                            {
                                out.println("Evaluation Committee");
                            }
                            else
                            {
                                out.println("Bidder/Consultant ");
                            }
                        %>
                        </td>
                         <td class="t-align-center">
                             <%
                             if(userId != Integer.parseInt(sptcd.getFieldName5()))
                                 {
                                 %>
                                    <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&ub=2" title="Download"><img src="../images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                 <%
                                 }
                                else
                                    {
                                %>
                                    <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download" title="Download"><img src="../images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                 <%
                                    }
                                %>

                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                             sptcd = null;
                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="6" class="t-align-center">No records found.</td>
                        </tr>
                    <%}%>
                </table>
                <BR>
                   <div class="t-align-left ff">
                        Document uploaded by Evaluation Committee during Negotiation Submission:
                   </div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%
                            int docCnt1 = 0;
                            for (SPTenderCommonData sptcd : tenderCS.returndata("NegotiationDocInfo", negId, "negotiation")) {
                                docCnt1++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt1%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                         <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&docType=negotiation" title="Download"><img src="../images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                             sptcd = null;
                         }
                                     }%>
                    <% if (docCnt1 == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                    <%}%>
                </table>

                     </div>
                <div>&nbsp;</div>
            </div>
            <%@include file="Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            </div></div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            if (tenderCS != null) {
                tenderCS = null;
            }

%>