<%--
    Document   : Bottom.jsp
    Created on : Oct 21, 2010, 6:04:11 PM
    Author     : yanki
--%>

<%@page import="java.net.InetAddress"%>
<%@page import="com.cptu.egp.eps.model.table.TblAuditTrailMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<jsp:useBean id="auditTrailMaster" class="com.cptu.egp.eps.web.servicebean.AuditTrailMasterSrBean"></jsp:useBean>

<%
    String home = null,lang = null,aboutegp = null,contactus = null;
    String rssfeed = null,termscondition =null,privacypolicy = null;

    if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
        lang = request.getParameter("lang");
    }else{
        lang = "en_US";
    }

    MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
    List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"bottom");

    if(!langContentList.isEmpty())
    {
        for(TblMultiLangContent tblMultiLangContent:langContentList)
        {
            if(tblMultiLangContent.getSubTitle().equals("lbl_home")){
                if("bn_IN".equals(lang)){
                    home = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    home = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_aboutegp")){
                if("en_US".equals(lang)){
                    aboutegp = new String(tblMultiLangContent.getValue());
                }else{
                    aboutegp = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_contactus")){
                if("en_US".equals(lang)){
                    contactus = new String(tblMultiLangContent.getValue());
                }else{
                    contactus = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_rssfeed")){
                if("bn_IN".equals(lang)){
                    rssfeed = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    rssfeed = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_termscondition")){
                if("bn_IN".equals(lang)){
                    termscondition = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                }else{
                    termscondition = new String(tblMultiLangContent.getValue());
                }
            }
            if(tblMultiLangContent.getSubTitle().equals("lbl_privacypolicy")){
                privacypolicy = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
            }
        }
    }
%>
<%
    if(false && session.getAttribute("userTypeId") == null){
%>
        <div class="footerCss">
            <a href="#"><%=aboutegp%></a>&nbsp; | &nbsp;
            <a href="#"><%=contactus%></a>&nbsp; | &nbsp;
            <a href="<%=request.getContextPath() %>/SubscribeToRSS.jsp"><%=rssfeed%></a>&nbsp; | &nbsp;
            <a href="<%= request.getContextPath() %>/TermsNConditions.jsp"><%=termscondition%></a>&nbsp; | &nbsp;
            <a href="<%= request.getContextPath() %>/PrivacyPolicy.jsp"><%=privacypolicy%></a>
        </div>
<%
    }else{
%>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="footerCss">
    <tr valign="top">
        <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td align="center" style="padding-top: 10px;">
                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/Index.jsp?lang=<%=request.getParameter("lang")%>"><%=home%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/Index.jsp"><%=home%></a> &nbsp;|&nbsp;
                    <% }%>

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/aboutUs.jsp?lang=<%=request.getParameter("lang")%>"><%=aboutegp%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/aboutUs.jsp"><%=aboutegp%></a> &nbsp;|&nbsp;
                    <% }%>

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/contactUs.jsp?lang=<%=request.getParameter("lang")%>"><%=contactus%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/contactUs.jsp"><%=contactus%></a> &nbsp;|&nbsp;
                    <% }%>

                    <a href="<%=request.getContextPath() %>/SubscribeToRSS.jsp"><%=rssfeed%></a> &nbsp;|&nbsp;

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/TermsNConditions.jsp?lang=<%=request.getParameter("lang")%>"><%=termscondition%></a>&nbsp;|&nbsp;
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/TermsNConditions.jsp"><%=termscondition%></a> &nbsp;|&nbsp;
                    <% }%>

                    <% if (request.getParameter("lang") != null) {%>
                        <a href="<%= request.getContextPath() %>/PrivacyPolicy.jsp?lang=<%=request.getParameter("lang")%>"><%=privacypolicy%></a>
                    <% } else {%>
                        <a href="<%= request.getContextPath() %>/PrivacyPolicy.jsp"><%=privacypolicy%></a>
                    <% }%>

                </td>
            </tr>
            <tr>
                <td align="center" style=" line-height: 16px;">
                    <div class="msg">
                        Best viewed in 1024 x 768 and above resolution. Browsers Tested & Certified by GPPMD: Internet Explorer 8.x, Internet Explorer 9.x, Mozila Firefox 3.6x
                    </div>
                </td>
            </tr>
           <tr>
                <td height="10"></td>
            </tr>
            <tr>
                 <td width="64%" align="center" style="padding-bottom: 0px; padding-top: 5px; font-weight: normal; line-height: 18px;color: black;font-size: 11px;">
                          Government Procurement and Property Management Division, Department of National Properties, Ministry of Finance<br />
                         &copy; 2016 Dohatec New Media, Bangladesh, All Rights Reserved. 
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>

        </table></td>
        <td width="100"><%--<a href="http://www.verisign.com" target="_blank"><img src="<%= request.getContextPath() %>/resources/images/Dashboard/verisign.gif" style="border: none; width: 58px; height: 25px; margin-top: 10px;" /></a>--%>
            <% 
              String str_bottom_url = request.getRequestURL().toString();
              if(str_bottom_url.indexOf("www.eprocure.gov.bd")!=-1){
            %>
            
             <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign Trust Seal to promote trust online with consumers.">
                <tr>
                    <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.eprocure.gov.bd&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
                    <a href="http://www.verisign.com/verisign-trust-seal" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT TRUST ONLINE</a></td>
                </tr>
            </table>
            <% } %>
        </td>
    </tr>
</table>

<%
    }
%>