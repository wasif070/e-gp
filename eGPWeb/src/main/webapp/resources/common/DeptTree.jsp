<%--
    Document   : DeptTree
    Created on : 2-Dec-2010, 3:00:16 PM
    Author     : dipti
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%--<%@page import="javafx.scene.control.Alert"%>--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Department Tree</title>
        <%--<script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>--%>
        <script type="text/javascript" src="../js/jstreecomp/jquery.js"></script>
        <script type="text/javascript" src="../js/jstreecomp/jquery.jstree.js"></script>
        <script type="text/javascript" src="../js/jstreecomp/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/jstreecomp/jquery.hotkeys.js"></script>

        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../css/!style.css"/>
    </head>
    <body>
        <%
            short i = -1;
            String dType = "";
            String operation = "";
            boolean show = true;
            if (request.getParameter("operation") != null) {
                operation = request.getParameter("operation");
            }
            if ("PackageReport".equals(operation) || "AdvAPPSearch".equals(operation) || "createCommitee".equals(operation) || "homeallTenders".equals(operation) || "offcre".equals(operation) || "peadmin".equals(operation) || "govuser".equals(operation) || "workflow".equals(operation) || "searchUsrPrj".equals(operation) || "createdesignation".equals(operation) || "SearchUser".equals(operation)) {
                show = true;
            } 
            else 
            {
                show = false;
            }
            if (request.getParameter("aLvlOff") != null) {
                i = Short.parseShort(request.getParameter("aLvlOff"));
                switch (i) {
                    case 1:
                        show = true;
                        dType = "Ministry";
                        break;
                    case 2:
                        show = true;
                        dType = "Division";
                        break;
                    case 3:
                        show = true;
                        dType = "Organization";
                        break;
                    case 4:
                        show = true;
                        dType = "SubDistrict";
                        break;
                    case 5:
                        show = true;
                        dType = "Gewog";
                        break;
                    case 6:
                        show = true;
                        dType = "District";
                        break;
                    case 7:
                        show = true;
                        dType = "Autonomus";
                        break;
                }
            }
        %>
        <div id="treeMsg" style="width: 100%;display:none" align="center"></div>
        <div id="jsTreeComponent" class="demo" style="width: 100%"></div>
        <script>
            $(function () {
                $("#jsTreeComponent").jstree({
                    "json_data": {
                        "ajax": {
                            //  _json_data.jsp   /getDataForTree
                            "url": '<%=request.getContextPath()%>/getDataForTree',
                            "data": function (n) {
                                return {
                                    "id": n.attr ? n.attr("id").replace("deptid_", "") : 0,
                                    "showPrNd": "<%= show%>"
                                };
                            },
                            "cache": false
                        }
                    },
                    "themes": {
                        "theme": "apple",
                        "dots": true,
                        "icons": true
                    },
                    "plugins": ["themes", "json_data", "ui"]
                });
                $("#jsTreeComponent").bind("select_node.jstree", function (e, data) {
                    //alert("hi m here" + e + " === " + data);
                    //alert("--"+data.rslt.obj.attr("dname")+"--");
            <%if ("offcre".equals(operation)) {%>
                    //alert(data.rslt.obj.attr("dtype"));
                    //alert("<%= dType%>");
                    if (data.rslt.obj.attr("dtype") == "<%= dType%>") {
                        if(duplicateOffice(data.rslt.obj.attr("id").replace("deptid_", "")))
                        {
                            window.opener.document.getElementById("txtchk").value = data.rslt.obj.attr("id").replace("deptid_", "");
                            window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                            window.opener.document.getElementById("deptName").value = data.rslt.obj.attr("dname");
                            window.opener.document.getElementById("ErrTree").innerHTML = "";
                            window.opener.document.getElementById('txtOfficeName').removeAttribute('readonly', 'false');//rishita - unique office
                            window.opener.document.getElementById('readonlyMsg').innerHTML = '';//rishita - unique office
                            //getDistrictSubDistrict(data.rslt.obj.attr("id").replace("deptid_", ""));
                            //alert(district);
                            // window.opener.document.getElementById("cmbState").innerHTML = district;
                            //  window.opener.document.getElementById('txtCity').innerHTML =district;
                            //  window.opener.document.getElementById('txtupJilla').innerHTML = district;
                            window.close();
                            
                        }
                        else
                        {
                            jAlert("PA Office already exists.", "Alert", function (bool) {
                                return bool;
                            });
                            
                        }
                        
                    } else {
                        if (data.rslt.obj.attr("dtype") == "Division")
                        {
                            jAlert("You can't select " + "Department", "Alert", function (bool) {
                                return bool;
                            });
                        } else if (data.rslt.obj.attr("dtype") == "Organization")
                        {
                            jAlert("You can't select " + "Division", "Alert", function (bool) {
                                return bool;
                            });
                        } else
                        {
                            jAlert("You can't select " + data.rslt.obj.attr("dtype"), "Alert", function (bool) {
                                return bool;
                            });
                        }
                    }
            <%} else if ("SearchUser".equals(operation)) {%>
//                By Emtaz on 15/May/2016
                    if (data.rslt.obj.attr("dtype") == "<%= dType%>") {
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.close();
                    } else {
                        if (data.rslt.obj.attr("dtype") == "Division")
                        {
                            jAlert("You can't select " + "Department", "Alert", function (bool) {
                                return bool;
                            });
                        } else if (data.rslt.obj.attr("dtype") == "Organization")
                        {
                            jAlert("You can't select " + "Division", "Alert", function (bool) {
                                return bool;
                            });
                        } else
                        {
                            jAlert("You can't select " + data.rslt.obj.attr("dtype"), "Alert", function (bool) {
                                return bool;
                            });
                        }
                    }
            <%} else if ("govuser".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.document.getElementById("txtDepartmentType").value = data.rslt.obj.attr("dtype");

                        window.opener.onSelectionOfTreeOff();
                        window.opener.onSelectionOfTreeDesig();
                       // window.opener.showHide();
                        window.opener.checkCondition();
                        window.opener.checkHOPE();
                        
                        window.close();
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("workflow".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.getOffices();
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("peadmin".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("searchUsrPrj".equals(operation)) {%>
                    window.opener.document.getElementById("deptName").value = data.rslt.obj.attr("dname");
                    window.opener.document.getElementById("cmbOrganization").value = data.rslt.obj.attr("id").replace("deptid_", "");
                    window.opener.getOfficeData();
                    window.close();
            <%} else if ("createCommitee".equals(operation)) {%>
                    window.opener.document.getElementById("txtDeptName").value = data.rslt.obj.attr("dname");
                    window.opener.document.getElementById("cmborg").value = data.rslt.obj.attr("id").replace("deptid_", "");
                    window.opener.getOfficesForDept();
                    window.close();
            <%} else if ("createdesignation".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.document.getElementById("txtDesignation").focus();
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("AdvAPPSearch".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        //window.opener.loadOffice(); -- search app
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("PackageReport".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.getOfficesForDept(); 
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("homeallTenders".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.loadOffice();
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else if ("addpackage".equals(operation)) {%>
                    if (window.opener.document.getElementById("txtdepartment"))
                    {
                        window.opener.document.getElementById("txtdepartment").value = data.rslt.obj.attr("dname");
                        window.opener.document.getElementById("txtdepartmentid").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.loadOffice();
                    } else
                    {
                        alert('Unable to complete the request, press OK to close.');
                    }
                    window.close();
            <%} else {%>
                    //alert(data.rslt.obj.attr("dtype"));
                    //alert("<%= dType%>");
                    if (("SubDistrict" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "District") ||("Gewog" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "SubDistrict") ||("Gewog" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "District") ||("District" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "District") ||("Division" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "Ministry" || data.rslt.obj.attr("dtype") == "Autonomus") || ("Organization" == "<%= dType%>" && data.rslt.obj.attr("dtype") == "Division")) {
                        window.opener.document.getElementById("deptParentId").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        window.opener.document.getElementById("parentDepartmentId").value = data.rslt.obj.attr("id").replace("deptid_", "");
                        //window.opener.document.getElementById("deptHirarchy").value = data.rslt.obj.attr("dname");--rishita-validation-start
                        if ("Organization" == "<%= dType%>" || "SubDistrict" == "<%= dType%>"|| "District" == "<%= dType%>"|| "Gewog" == "<%= dType%>") {
                            window.opener.document.getElementById("deptHirarchyMD").value = data.rslt.obj.attr("dname");
                        } else {
                            window.opener.document.getElementById("deptHirarchy").value = data.rslt.obj.attr("dname");
                        }//rishita-validation-end

                        if (window.opener.document.getElementById("ErrTree")) {
                            window.opener.document.getElementById("ErrTree").innerHTML = "";
                        }
                        window.close();
                    } else {
                        //$("#treeMsg").text("You can't select " + data.rslt.obj.attr("dtype"));
                        jAlert("You can't select " + data.rslt.obj.attr("dtype"), "Alert", function (bool) {
                            return bool;
                        });
                        //$("#treeMsg").show("slow");
                    }
            <%}%>
                });
            });

            function getDistrictSubDistrict(deptId) {
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: deptId, funName: 'getDistrictSubDistrict'}, function (j) {
                    var splitdata = j.split(",");
                    window.opener.document.getElementById("cmbState").selected = splitdata[0].toString();
                    window.opener.document.getElementById("txtCity").value = splitdata[1].toString();
                    window.opener.document.getElementById("txtupJilla").value = splitdata[2].toString();
                    window.close();
                });
            }
            function duplicateOffice(depId)
            {
                <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> officeList = tenderCommonService.returndata("GetOfficeList", null, null);
                    for (SPTenderCommonData officeListNew : officeList)
                    {%>
                        if(depId == "<%=officeListNew.getFieldName1()%>")
                        {
                            return false;
                        }
                    <%}%>
                      return true;
            }
        </script>
    </body>
</html>
