<%-- 
    Document   : DebarmentList
    Created on : May 29, 2016, 11:02:58 AM
    Author     : aprojit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Debarment List</title>
        
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jQuery/jquery.tablesorter.js"></script>
    </head>
    <body>
            <div class="dashboard_div">
                <div class="mainDiv">
                    <div class="fixDiv">
                        <jsp:include page="Top.jsp" ></jsp:include>
                        <div class="pageHead_1" style="padding-left: 12px;">Debarment List</div>
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,4">
                                    <tr>
                                        <th width="5%" class="t-align-center">
                                            Sl. <br/>
                                            No.
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Name of Bidder                               
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Legal Address
                                        </th>
                                       
                                        <th width="10%" class="t-align-center">
                                           Debarred From
                                        </th>
                                        <th width="10%" class="t-align-center">
                                            Debarred To
                                        </th>
                                       
                                        <th width="20%" class="t-align-center">
                                            Grounds
                                        </th>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                        <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">

                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" name="size" id="size" value="10"/>
                                    <!--<input type="hidden" name="DeptType" id="DeptType" value="<%//=deptType %>" />-->
                                    <!--<input type="hidden" name="UserTypeId" id="UserTypeId" value="<%//=userTId %>" />-->
                                    
                                </div>
                            </div>
                   </div>
                </div>
            </div>
         <jsp:include page="Bottom.jsp" ></jsp:include>
    </body>
</html>
