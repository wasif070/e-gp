<%--
    Document   : OnlinePayment
    Created on : Feb 16, 2012, 10:53:57 AM
    Author     : nishit
--%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="ipgclient2.CShroff2"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collection"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%
            String pyamentUrl = XMLReader.getBankMessage("PaymentGatewayURLDBBL");
             System.out.println(pyamentUrl);
            if (pyamentUrl != null && pyamentUrl.trim().length() > 0) {
                OnlinePaymentDtBean onlinePaymentDtBean = (OnlinePaymentDtBean) session.getAttribute("payment");
                if (onlinePaymentDtBean != null) {

                    /*String errmsg = ""; // error message
                    String strAmount = onlinePaymentDtBean.getAmt();
                    String strDesc = "";
                    String strTranID = "";
                    String stripAddress = request.getHeader("X-FORWARDED-FOR");
                    boolean onlinePaymentStatusSuccess = false;

                    if (stripAddress == null) {
                    stripAddress = request.getRemoteAddr();
                    }
                    if (request.getParameter("btnSubmit") != null && request.getParameter("btnSubmit").equalsIgnoreCase("Pay Online")) {
                    if("nur".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){
                    strDesc = "Online Payment For New User Registration";
                    }
                    else if("renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){
                    strDesc = "Online Payment For User Profile Renewal";
                    }
                    else if("nur".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){
                    strDesc = "Online Payment For  Tender/Proposal Document Fees";
                    }

                    if (strAmount != null && !strAmount.trim().equals("")) {
                    if(strAmount.indexOf(".") == -1)
                    {
                    strAmount = ""+Integer.parseInt(strAmount)*100;
                    }
                    else if((strAmount.indexOf(".") == strAmount.lastIndexOf(".")) && strAmount.substring((strAmount.indexOf(".")+1), strAmount.length()).length() == 2)
                    {
                    strAmount = strAmount.replace(".","");
                    }
                    else
                    {
                    errmsg = "Please enter valid amount (max 2 digit after decimal)";
                    }
                    }
                    else
                    {
                    errmsg = "Please enter valid amount";
                    }

                    if (strDesc.trim() == "") {
                    errmsg = "Please enter description";
                    }

                    try {
                    String strExecCMD = "java -jar C:/paymentGatewayTest/ecomm_merchant.jar  C:/paymentGatewayTest/merchant.properties -v " + strAmount + " 050 " + stripAddress + "  amt_" + strAmount + "_desc_" + strDesc;// + " > C:/test/result.trns";

                    Writer output = null;
                    File file = new File("C:/paymentGatewayTest/paymetTest.cmd");
                    if (file.exists()) {
                    file.delete();
                    }
                    file.createNewFile();
                    output = new BufferedWriter(new FileWriter(file));
                    output.write(strExecCMD);
                    output.close();

                    Process p = Runtime.getRuntime().exec("C:/paymentGatewayTest/paymetTest.cmd");
                    BufferedReader brSucc = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    BufferedReader brFail = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                    /*Success reponse from Process*/
                    /* try {
                    while ((strTranID = brSucc.readLine()) != null) {
                    // System.out.println("Sucess strTranID = " + strTranID+" strTranID.indexOf(TRANSACTION_ID)  "+strTranID.startsWith("TRANSACTION_ID") );
                    if (strTranID.indexOf("TRANSACTION_ID") >= 0) {
                    onlinePaymentStatusSuccess = true;
                    break;
                    }
                    }
                    System.out.println("Org strTranID = " + strTranID);
                    strTranID = URLEncoder.encode(strTranID.substring(16), "UTF-8");
                    } catch (IOException e) {
                    onlinePaymentStatusSuccess = false;
                    e.printStackTrace();
                    }
                    brSucc.close();
                    System.out.println("After success and encode strTranID = " + strTranID);

                    /*Fail reponse from Process*/
                    /* if(!onlinePaymentStatusSuccess){
                    try {
                    while ((strTranID = brFail.readLine()) != null) {
                    onlinePaymentStatusSuccess = false;
                    }
                    } catch (IOException e) {
                    onlinePaymentStatusSuccess = false;
                    e.printStackTrace();
                    }
                    brFail.close();
                    }
                    } catch (IOException e1) {
                    onlinePaymentStatusSuccess = false;
                    e1.printStackTrace();
                    }

                    /*Final response recirect page on base of "onlinePaymentStatusSuccess"*/
                    /*if (onlinePaymentStatusSuccess) {
                    System.out.println("strTranID = " + strTranID);
                    response.sendRedirect("https://ecomtest.dutchbanglabank.com/ecomm2/ClientHandler?trans_id=" + strTranID);
                    } else {
                    response.sendRedirect(request.getContextPath()+"/tenderer/merchant_client.jsp?er=1");
                    }
                    } else {*/

%>
    <%
        String expiryDt="";
        Calendar calExpiry= Calendar.getInstance() ,calCurrent= Calendar.getInstance();
        TblLoginMaster tblLoginMaster = null;
        Date expiryDate=null;
        List<TblLoginMaster> tblLoginMasters = null;
        if(session.getAttribute("userId")!=null && session.getAttribute("userId")!=""){
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        tblLoginMasters = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(session.getAttribute("userId").toString()));
        if(tblLoginMasters!=null && (!tblLoginMasters.isEmpty())){
            tblLoginMaster = tblLoginMasters.get(0);
            if(tblLoginMaster.getTblUserTypeMaster().getUserTypeId()==2){
            expiryDate = tblLoginMaster.getValidUpTo();
            expiryDt=DateUtils.customDateFormate(expiryDate);
            calExpiry.setTime(expiryDate);
            calCurrent.add(Calendar.DATE, +30);
    %>
<div class="t_space">
    <div class="noticeMsg responseMsg t_space">
            <%if(calExpiry.equals(calCurrent) || calExpiry.before(calCurrent)|| !"renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){%>
                Acceptable cards are DBBL Nexus Card, DBBL MasterDebit Card, DBBL VisaDebit Card, Master Card,Visa Card, Brac Card
            <%}else{%>
                You can renewal only from 30 days before your registration expires.
            <%}}}}%>
    </div>
    <%if (request.getParameter("er") != null) {%>
    <div class="responseMsg errorMsg t_space" >
        <%if (onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLDBBL")) {%>
                Your transaction is failed. Please try again or go to the scheduled member banks of e-GP to make payment.
        <%} else if(onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLBRAC")){%>
        Transaction fail due to <%if ((onlinePaymentDtBean.getResponse().indexOf("<reason>") > 0) && (onlinePaymentDtBean.getResponse().indexOf("</reason>") > onlinePaymentDtBean.getResponse().indexOf("<reason>"))) {
            out.println(onlinePaymentDtBean.getResponse().substring(onlinePaymentDtBean.getResponse().indexOf("<reason>") + 8, onlinePaymentDtBean.getResponse().indexOf("</reason>")));
        }%>
        <%}%>
    </div>
    <%}%>
</div>
<form name="frmOnlinePayment" action="/OnlinePaymentServlet" method="POST" id="frmOnlinePayment" >
    <table border="0" cellspacing="20" cellpadding="20" class="tableList_1 c_t_space" id="tb2" width="100%">
        <tr>
            <td class="ff" width="15%">
                Bank Payment :
            </td>
            <td>
                <%
                                Collection<String> bankval = null;
                                Set<String> bankkey = null;
                                bankval = XMLReader.getBnkMsg().values();
                                bankkey = XMLReader.getBnkMsg().keySet();
                                Object[] valArr = bankval.toArray();
                                Object[] keyArr = bankkey.toArray();

                                for (int i = 0; i < valArr.length; i++) {
                                    if (i % 2 == 1) {
                %>
                <input type="Radio" name="BankList" value=<%=keyArr[i - 1]%>  /> <% out.println(valArr[i]);%>&nbsp;&nbsp;<%
                                    }
                                }
                %>
            </td>
        </tr>
        <tr id="trCardType" style="display: none;">
            <td class="ff" width="15%">
                Card Type :
            </td>
            <td>
                <input type="radio" name="cardType" value="1" checked /> DBBL Nexus
                <input type="radio" name="cardType" value="2" /> DBBL MasterDebit
                <input type="radio" name="cardType" value="3" /> DBBL VisaDebit
                
            </td>
        </tr>
        <tr>
            <td class="ff" width="15%">
                Amount (in Nu.) :
            </td>
            <td><%=onlinePaymentDtBean.getAmt()%></td>

        </tr>

        <tr>
            <td class="ff" width="15%">
                Service Charge (%) :
            </td>
            <td><%=onlinePaymentDtBean.getServiceCharge()%></td>

        </tr>
        <tr>
            <td class="ff" width="15%">
                Total Amount (in Nu.) :
            </td>
            <td><%=onlinePaymentDtBean.getTotal()%></td>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
            <%
            if(tblLoginMasters!=null && (!tblLoginMasters.isEmpty())){
            if(calExpiry.equals(calCurrent) || calExpiry.before(calCurrent)|| !"renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){%>
                <label class="formBtn_1"><input type="submit" name="btnSubmit" value="Pay Online" /></label>
            <%}else{%>
                <label class="formBtn_1" ><input type="button" name="btnSubmit" value="Pay Online" /></label>
            <%}}%>
            </td>
        </tr>
    </table>
</form>
<%--}}else{--%>
<%} else {%>
<label>There is Some Problem</label>
<%}
    } else {%>
<table width="100%" cellpadding="0" cellspacing="10" class="t_space">
    <tr>

        <td height="400" align="center" valign="middle" style="font-size:16px; color:#6e6e6e; font-weight:bold;">
            <img src="<%= request.getContextPath()%>/resources/images/Dashboard/Button_Warning.png" width="128" height="128" style="margin-bottom:10px;" /> <br />
    <legend class="heading-text">This feature will be available shortly.</legend><br />
</td>
</tr>
</table>
<%}%>
<script type="text/javascript">
    $("input:radio[name=BankList]").click(function() {
    var value = $(this).val();
    if(value == "PaymentGatewayURLDBBL")
        $("#trCardType").show();
    else
        $("#trCardType").hide();
    });
    
</script>
