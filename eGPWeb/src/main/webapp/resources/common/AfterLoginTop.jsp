<%--
    Document   : AfterLoginTop
    Created on : 24-Oct-2010, 1:25:23 AM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserTypeMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.UserTypeServiceImpl"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.web.utility.BrowserRecognitionModel"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.model.table.TblLocationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderSubRightServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<jsp:useBean id="userTypeService" class="com.cptu.egp.eps.service.serviceimpl.UserTypeServiceImpl"/>
<noscript>
<!--    <meta http-equiv="refresh" content="0; URL=<%= request.getContextPath()%>/JSInstruction.jsp">-->
</noscript>

<form id="frmAfterLoginTop" name="frmAfterLoginTop" method="POST">
    <input type="hidden" name="hiddenUserType" id="hiddenUserType">
</form>
<%

    long lTimeOutMilliSec = session.getMaxInactiveInterval() * 1000;
    // String pName = request.getRequestURI();
    // pName = pName.substring(pName.lastIndexOf("/")+1);?pn=" + pName
    // String strErrPage = request.getContextPath() + "/Abc.jsp";

    if (request.getHeader("referer") == null) {
        //String chkUrl = request.getRequestURI();
        ///if(chkUrl.contains("InboxMessage.jsp")){}else{
        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp?rs=t");
        //}
    }

    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
%>
<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: 150%;
    left: 50%;
    margin-left: -60px;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    bottom: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: transparent transparent black transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>
<script src="<%= request.getContextPath()%>/resources/js/navig.js" type="text/javascript"></script>
<script src="<%= request.getContextPath()%>/resources/js/browser.scripts.js" type="text/javascript"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function sessionTimedOut()
    {
        window.location = "<%=request.getContextPath()%>/SessionTimedOut.jsp";
    }

    function redirect(type) {
        document.getElementById("hiddenUserType").value = type;
        document.forms[0].action = "<%=request.getContextPath()%>/admin/AdminDashboard.jsp";
        document.forms[0].submit();
    }

    var browserName = "";
    var browserVer = "";
    var allowBrowser = true;


    $(document).ready(function () {

        //var t;
        //clearTimeout(t);
        //   setTimeout("sessionTimedOut()", '<%//lTimeOutMilliSec%>');
        /*
         jQuery.each(jQuery.browser, function(i, val) {
         browserVer = jQuery.browser.version;
         
         if(i == "mozilla") { // && (browserVer.substr(0,3) == "1.9" || browserVer.substr(0,2) == "13" || browserVer.substr(0,2) == "14")) {
         browserName = i;
         browserVer = nomVersionNavig();
         //allowBrowser = true;
         }
         if(i == "msie"){ //&& (browserVer.substr(0,3) == "8.0" || browserVer.substr(0,3) == "9.0" || browserVer.substr(0,3) == "7.0")){
         browserName = i;
         browserVer = browserVer.substr(0,3);
         
         //allowBrowser = true;
         }
         
         if(i == "version" )
         {
         if(browserName=='mozilla')
         {
         if(browserVer.substr(0,3) == "1.9"){
         browserVer = nomVersionNavig();
         } else {
         browserVer = browserVer.substr(0,2);
         }
         } else if (browserName== "msie") {
         browserVer = browserVer.substr(0,3);
         }
         }
         });*/

        /*
         if(browserName == "mozilla" && (nomVersionNavig().substr(0, 3) == "29." || nomVersionNavig().substr(0, 3) == "3.6" || parseInt(browserVer.substr(0, 2)) <= 14)){
         allowBrowser = true;
         }
         if(browserName == "msie" && (browserVer.substr(0, 3) == "8.0" || browserVer.substr(0, 3) == "9.0") || (jQuery.browser.version.substr(0, 4) == "10.0")){
         
         allowBrowser = true;
         }
         if (!!navigator.userAgent.match(/Trident.*rv\:11\./)){
         allowBrowser = false;
         }
         
         if(!allowBrowser){
         window.location.href = "<%= request.getContextPath()%>/BrowserInst.jsp";
         //document.getElementById("browserDiv").style.display = "block";
         }*/

        $("input[type=submit], a, button").dblclick(function () {
            alert('double click is not allowed');
            return false;
        });
    });
</script>
<%! DateFormat formatter; //Formats the date displayed
    public String lastdate = "1"; //String to hold date displayed
    Date currentDate; //Used to get date to display
    String contextPath = "";
%>

<%
    HttpSession sn = request.getSession();
    if ((sn.getAttribute("userId") == null || sn.getAttribute("userName") == null || sn.getAttribute("userTypeId") == null)) {
        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp"); //Index.jsp
    }
    CommonService commonService_media = (CommonService) AppContext.getSpringBean("CommonService");
    contextPath = request.getContextPath();
    int userTypeId = 0;
    int loguserId = 0;
    Object objUserTypeId = sn.getAttribute("userTypeId");
    Object objlogUserId = sn.getAttribute("userId");
    if (objUserTypeId != null) {
        userTypeId = Integer.parseInt(objUserTypeId.toString());
    }

    if (objlogUserId != null) {
        loguserId = Integer.parseInt(objlogUserId.toString());
    }

    Object objUserName = null;
    Object objGovUserId = "0";
    if (sn.getAttribute("userName") != null) {
        objUserName = sn.getAttribute("userName");

        //System.out.println("Taher1 : " + objUserName);
    }
    if (sn.getAttribute("govUserId") != null) {
        objGovUserId = sn.getAttribute("govUserId");
        //System.out.println("Taher1 : " + objGovUserId);
    }
    if (objUserName == null) {
        objUserName = "";
    }

    if (userTypeId == 1) {
        if (loguserId == 1) {
            objUserName = XMLReader.getMessage("egpadmin");
        }
        sn.setAttribute("userName", objUserName.toString());
    } else if (userTypeId == 16) {
        objUserName = XMLReader.getMessage("dgcptu");
        sn.setAttribute("userName", objUserName.toString());
    }

    Object lastLogin = null;
    if (sn.getAttribute("lastLogin") != null) {
        lastLogin = sn.getAttribute("lastLogin");
    }

    Date lastLogin_dt = null;
    if (lastLogin == null) {
        lastLogin = "";
    } else {
        lastLogin_dt = (Date) lastLogin;
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMMM, yyyy, HH:mm:ss");
    String logoutPage = request.getContextPath() + "/Logout.jsp";

    formatter = DateFormat.getDateTimeInstance(DateFormat.FULL,
            DateFormat.MEDIUM);
    currentDate = new Date();
    lastdate = formatter.format(currentDate);

    String objCompIdTend = null;

    AppMessage appMessage = new AppMessage();

    Map<Integer, String> objUserRightsMap = null;
    ManageUserRightsImpl objManageUserRights = (ManageUserRightsImpl) AppContext.getSpringBean("ManageUserRigths");
    if (userTypeId == 19 || userTypeId == 20) {
        objUserRightsMap = objManageUserRights.getUserMenuRightsMap(loguserId, userTypeId);
    }
    List<Object[]> listPD;
    listPD = commonService_media.checkPD(String.valueOf(loguserId));
    //out.println(loguserId);
%>
<!--Dashboard Header Start-->
<link REL="SHORTCUT ICON" HREF="<%=contextPath%>/resources/favicon2.ico">
<script type="text/javascript" src="<%=contextPath%>/resources/js/ddlevelsmenu.js"></script>
<div class="topHeader">
    <div>
        <a href="/Index.jsp">
            <img alt="EGP" style="width:100%;height: auto;" src="/resources/images/header.jpg">
            <div id="innerHover" title="hover text" style="font-size: 11px;"><div id="popupdiv">
                The dark green power button symbolizes procuring at the click of the button ensuring Value for Money (VfM) in Government Procurement.<br/><br/>

                The red burning flame overarching the whole logo signifies the elimination of any fraudulent and corrupt practices in procurement. <br/><br/>

                The letters 'e-GP' on a white background symbolizes integrity and transparency in government procurement. The open book at the bottom of the logo signifies transition from manual to electronic (paperless) procurement system.


            </div></div>
        </a>
    </div>

    <table width="100%" cellspacing="0">


        <tr valign="top">
            <td><div id="ddtopmenubar" class="topMenu_1">
                    <ul>
                        <%
                            if (userTypeId != 18) {
                        %>
                        <li><a title="Home" href="<%=contextPath%>/Index.jsp" id="headTabHomePage">Home</a></li>
                            <%
                                }
                                if (userTypeId == 1) {%>
                        <li><a title="Dashboard" href="<%=contextPath%>/resources/common/Dashboard.jsp" id="headTabDashboard">Dashboard</a></li>
                        <li><a title="Configuration" href="#" onClick="redirect('config');" id="headTabConfig">Configuration</a></li>
                        <li><a title="SBD" href="<%=contextPath%>/admin/ListSTD.jsp" id="headTabSTD">SBD</a></li>
                        <li><a title="Content" href="javascript:void(0);" onclick="redirect('content');" id="headTabContent">Content</a></li><!--APP Serach(Rishita)-->
                        <li><a title="Manage Users" href="#" rel="ddsubmenu2" id="headTabMngUser">Manage Users</a></li>
<!--                        <li><a title="Report" href="<%=contextPath%>/officer/SearchPackageReport.jsp" rel="ddsubmenu4" id="headTabReport"><img src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Reports</a></li>-->
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                        <li><a title="Debarment" href="#" rel="debarmentsubmenu" id="headTabDebar">Debarment</a></li>
                        <li><a href="#" title="My Account" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                            <%--<li><a title="Help" target="_blank" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 2) {

                                String regType = commonService_media.getRegType(session.getAttribute("userId").toString());
                                if (regType != null && regType.equalsIgnoreCase("media")) {

                            %>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="Press Release" id="headTabMediaPress" href="<%=contextPath%>/tenderer/MediaContList.jsp?contentType=Press" >Press Release</a></li>
                        <%--<li><a title="Complaint Resolution" id="headTabMediaComplaint" href="<%=contextPath%>/tenderer/MediaContList.jsp?contentType=Complaint" >Complaint Resolution</a></li>--%>
                        <li><a title="Other Information" id="headTabMediaOther" href="<%=contextPath%>/tenderer/MediaContList.jsp?contentType=Other" >Other Information</a></li>
                            <%} else {%>

                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="APP" href="#" rel="submenu-configuration" id="headTabApp">APP</a></li>
                        <li><a title="Tender" href="#" rel="ddsubmenu2" id="headTabTender">Tender</a></li>
                        <li><a title="Doc. Library" href="#" rel="docMenu" id="headTabDocLib">Doc. Library</a></li>
                            <%%><%--<li><a href="#" id="headTabReport"><img alt="" src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Reports</a></li>--%>
                            <%
                                ContentAdminService adminChkCmpId = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                                Object cmpIdChk = adminChkCmpId.getCompanyId(session.getAttribute("userId").toString());

                                if (cmpIdChk != null) {
                                    objCompIdTend = cmpIdChk.toString();
                                    if (regType.equalsIgnoreCase("contractor") || regType.equalsIgnoreCase("supplier") || regType.equalsIgnoreCase("contractsupplier")) {
                            %>
                        <!--CompanyVerification removed from below link-->
                        <li><a title="Administration" rel="adminTenderer" href="javascript:void(0);" id="headTabCompVerify">Administration</a></li>
                        <%--<li><a title="Debarment" rel="" href="<%=contextPath%>/tenderer/TendererDebarListing.jsp" rel="ddddd" id="headTabDebar">Debarment</a></li>--%>
                            <%}
                                    }
                                }%>

                        <li><a title="My Account" rel="myAccountSub" href="javascript:void(0);" id="headTabMyAcc">My Account</a></li>
                            <%--<li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%
                            } else if (userTypeId == 3) {
                                boolean isAccountant = false;
                                boolean isPMC = false;
                                boolean isHOPA = false;
                                boolean isPA = false;
                                boolean isAU = false;
                                Object acc_objProRole = session.getAttribute("procurementRole");
                                String acc_strProRole = "";
                                if (acc_objProRole != null) {
                                    acc_strProRole = acc_objProRole.toString();
                                }
                                String acc_chk = acc_strProRole;

                                String acc_ck1[] = acc_chk.split(",");
                                for (int acc_iAfterLoginTop = 0; acc_iAfterLoginTop < acc_ck1.length; acc_iAfterLoginTop++) {
                                    if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("Account Officer")) {
                                        isAccountant = true;
                                        break;
                                    }
                                    if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("PMC")) {
                                        isPMC = true;

                                    }
                                }
                                for (int acc_iAfterLoginTop = 0; acc_iAfterLoginTop < acc_ck1.length; acc_iAfterLoginTop++) {
                                    if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("HOPE")) {
                                        isHOPA = true;
                                        break;
                                    }
                                    else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("PE")) {
                                        isPA = true;
                                        break;
                                    }
                                    else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("AU")) {
                                        isAU = true;
                                        break;
                                    }
                                }
                            %>
                        <% if(isHOPA || isPA || isAU) { %>
                        <li><a title="Officer Dashboard" href="<%=contextPath%>/resources/common/PaDashboard.jsp?viewtype=pending" id="headTabMsgBox">Dashboard</a></li>
                        <% } else { %> 
                        <li><a title="Officer Dashboard" href="<%=contextPath%>/resources/common/Dashboard.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <% } if (isAccountant) {%>
                        <li><a title="Make Payment" href="<%=contextPath%>/officer/MakePayment.jsp" id="headTabMakePay">Make Payment</a></li>
                            <% } else if (isPMC) {
                            %>
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                            <%
                            } else {%>
                        <li><a title="APP" href="#" rel="submenu-configuration" id="headTabApp">APP</a></li>
                        <li><a title="Tender" href="#" rel="ddsubmenu2" id="headTabTender">Tender</a></li>
                        <% if(isHOPA || isPA || isAU) { %>
                        <li><a title="Workflow" href="#" rel="ddsubmenu3" id="headTabWorkFlow">Workflow</a></li>
                        <%  } %>
                        <li><a title="Evaluation" href="#" rel="evalMenu" id="headTabEval">Committees</a></li>
                            <%
                                String[] pRole = null;
                                String debarPage = null;
                                if (session.getAttribute("procurementRole") != null) {
                                    pRole = session.getAttribute("procurementRole").toString().split(",");
                                    boolean isPe = false;
                                    boolean isHope = false;
                                    boolean isAOAU = false;

                                    listPD = commonService_media.checkPD(String.valueOf(loguserId));
                                    for (int debarI = 0; debarI < pRole.length; debarI++) {
                                        if (acc_ck1[debarI].equalsIgnoreCase("PE")) {
                                            isPe = true;
                                        } else if (acc_ck1[debarI].equalsIgnoreCase("HOPE")) {
                                            isHope = true;
                                        } else if (acc_ck1[debarI].equalsIgnoreCase("AO") || acc_ck1[debarI].equalsIgnoreCase("AU")) {
                                            isAOAU = true;
                                        }
                                    }
                                    if (isPe) {
                                        debarPage = "DebarmentListing.jsp";
                                    } else if (isHope) {
                                        debarPage = "HopeDebarListing.jsp";
                                    } else if (isAOAU) {
                                        debarPage = "ComDebarListing.jsp";
                                    }
                                }
                                if (debarPage != null) {
                            %>
                       <%-- <li><a title="Debarment" href="<%=contextPath%>/officer/<%=debarPage%>" id="headTabMsgBox">Debarment</a></li>--%>
                            <%}

                                }

                                if (session.getAttribute("userTypeId").toString().equals("3")) {
                                    Object objProRole = session.getAttribute("procurementRole");
                                    String strProRole = "";
                                    if (objProRole != null) {
                                        strProRole = objProRole.toString();
                                    }
                                    String chk = strProRole;
                                    String ck[] = chk.split(",");
                                    for (int iAfterLoginTop = 0; iAfterLoginTop < ck.length; iAfterLoginTop++) {
                                        if (acc_ck1[iAfterLoginTop].equalsIgnoreCase("HOPE") || acc_ck1[iAfterLoginTop].equalsIgnoreCase("Secretary")) {
                            %>
                        <%--<li class="">
                            <a href="<%= request.getContextPath()%>/resources/common/complaintOfficerHope.htm">
                                Complaint Mgmt</a>
                        </li>--%>
                        <%
                            if (acc_ck1[iAfterLoginTop].equalsIgnoreCase("HOPE")) {
                        %>
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                            <%
                                }

                                break;
                            } else if ((listPD.size() > 0) && !isPA) {%>
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                            <% break;
                                        }

                                    }
                                }//end
                            %>
                        <li><a title="My Account" href="#" rel="myAcntMnu" id="headTabMyAcc">My Account</a></li>
<!--                    <li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp" ><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>-->
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 4) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                            <%--<li><a title="Content" href="#" onclick="redirect('content');" id="headTabContent"><img alt="Content" src="<%=contextPath%>/resources/images/Dashboard/DocLibIcn_1.png" />Content</a></li>--%>
                        <li><a title="Manage Users" href="#" onClick="redirect('gov');" id="headTabMngUser">Manage Users</a></li>                        <%--<li><a href="#" id="headTabReport"><img alt="" src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Report</a></li>--%>
                        <li><a title="Report" href="javascript:void(0);" rel="repMenu" id="headTabReport">Reports</a></li>
                        <li><a title="My Account" href="#" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
<!--                        <li><a title="Report" href="<%=contextPath%>/officer/SearchPackageReport.jsp" rel="repMenu" id="headTabReport"><img src="<-%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Reports</a></li> bugID = 1200-->
                        <%--<li><a title="Help" target="_blank" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" id="headTabHelp"><img src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" alt="Help" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%    } else if (userTypeId == 5) {%>
                    <!--<li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox"><img alt="Message Box" src="<%=contextPath%>/resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>-->
                        <li><a title="Dashboard" onclick="redirect('gov');" rel="" href="#" id="headTabMngUser">Dashboard</a></li>
                        <li><a title="Content" href="#" onclick="redirect('content');" id="headTabContent">Content</a></li>

                        <!-- Solved bug id: 5982 [by removing href set value.]-->


<!--                        <li><a title="Report" href="<%=contextPath%>/officer/SearchPackageReport.jsp" rel="" id="headTabReport"><img src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Reports</a></li>-->
                        <li><a title="Report" href="javascript:void(0);" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                        <li><a title="My Account" href="#" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                            <%--<li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp" ><img src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" alt="Help" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <% } else if (userTypeId == 6) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                            <%
                                Object objDpUsrId = sn.getAttribute("userId");
                                int intUsrId = 0;
                                boolean isAdmin = true;
                                if (objDpUsrId != null && !objDpUsrId.toString().isEmpty()) {
                                    intUsrId = Integer.parseInt(objDpUsrId.toString());
                                    NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                                    isAdmin = navRuleUtil.isAdmin(intUsrId);
                                }
                                if (isAdmin) {
                            %>
                        <li><a title="Manage Users" href="<%=contextPath%>/admin/AdminDashboard.jsp" id="headTabMngUser">Manage Users</a></li>
                            <%--<li><a href="#" id="headTabReport"><img alt="" src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Report</a></li>--%>
                            <%} else {%>
                        <li><a title="Workflow" href="#" rel="ddsubmenu3">Workflow</a></li>
                            <%}%>
                        <li><a title="My Account" href="#" rel="ddsubmenu1" id="headTabMyAcc">My Account</a></li>
<!--                        <li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Helpxxx</a></li>-->
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <% } else if (userTypeId == 7) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                            <%
                                Object objSbUsrId = sn.getAttribute("userId");
                                int intUsrId = 0;
                                boolean isAdmin = true;
                                if (objSbUsrId != null && !objSbUsrId.toString().isEmpty()) {
                                    intUsrId = Integer.parseInt(objSbUsrId.toString());
                                    NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                                    isAdmin = navRuleUtil.isAdmin(intUsrId);
                                }
                                if (isAdmin) {
                            %>
                        <li><a title="Manage Users" href="<%=contextPath%>/admin/AdminDashboard.jsp" id="headTabMngUser">Manage Users</a></li>
                        <li><a title="Report" href="#" rel="paymentrepsubmenu" id="headTabReport">Reports</a></li>
                            <%--<li><a href="#" id="headTabReport"><img alt="" src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Report</a></li>--%>


                        <%} else {%>
                        <li><a title="Payment" rel="paymentsubmenu" href="#" id="headTabPayment">Payment</a></li>
                            <%}%>

                        <li><a href="#" title="My Account" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <% } else if (userTypeId == 8) {%>

                        <li><a title="Company Verification" href="<%=contextPath%>/admin/CompanyVerification.jsp" id="headTabCompVerify">Company Verification</a></li>
                        <!--<li><a title="Content Verification" href="<%=contextPath%>/contentPublicForumBoard.jsp" rel="" id="headTabContentVerify">Content Verification</a></li>-->

                        <!--<li><a title="STD" href="<%=contextPath%>/admin/ListSTD.jsp" id="headTabSTD">SBD</a></li>-->
    <!--//                     <li><a href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>-->
                        <li><a title="My Account" href="#" rel="ddsubmenu1" id="headTabMyAcc">My Account</a></li>
                            <%--<li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" alt="Help" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 11) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="APP" href="#" rel="submenu-configuration" id="headTabApp">APP</a></li>
                        <li><a title="Tender" href="#" rel="ddsubmenu2" id="headTabTender">Tender</a></li>
                        <li><a title="Doc. Library" href="#" id="headTabDocLib">Doc. Library</a></li>
                        <li><a title="Administration" href="#" id="headTabAdmin">Administration</a></li>
                            <%--<li><a href="#" id="headTabReport"><img alt="" src="<%=contextPath%>/resources/images/Dashboard/reportIcn.png" />Reports</a></li>--%>
                        <li><a title="My Account" href="#" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>
                                <%} else if (userTypeId == 12) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="Question" rel="" href="<%=contextPath%>/officer/Question.jsp" rel="ddsubmenu2" id="headTabQuestion">Procurement Questions</a></li>
                        <li><a title="My Account" href="#" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                            <%--<li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>--%>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 14) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="Evaluation" href="#" rel="evalMenu" id="headTabEva">Evaluation</a></li>
                        <li><a title="My Account" href="#" rel="ddsubmenu2" id="headTabMyAccExt">My Account</a></li>
<!--                    <li><a title="Help" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank" id="headTabHelp"><img alt="Help" src="<%=contextPath%>/resources/images/Dashboard/helpIcn.png" />Help</a></li>-->
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>

                        <% } else if (userTypeId == 15) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="Manage Users" href="<%=contextPath%>/admin/AdminDashboard.jsp" id="headTabMngUser">Manage Users</a></li>
                        <li><a href="#" title="My Account" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 16) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <%--<li><a href="ViewComplaintsCPTU.htm" title="Complaint Management" id="headTabCmptMgmt">Complaint Mgmt</a></li>--%>
                        <li><a href="#" title="My Account" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" target="_blank" href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" id="headTabHelp">Help</a></li>
                            <%} else if (userTypeId == 17) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a href="<%= request.getContextPath()%>/resources/common/TendererDashboard.htm" title="Complaint Management" id="headTabCmptMgmt">Complaint Mgmt</a></li>
                        <li><a title="My Account" href="#" rel="myAcntMnu" id="headTabMyAcc">My Account</a></li>
                            <%} else if (userTypeId == 18) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard </a></li>
                        <li><a href="#" rel="ddsubmenurhd" title="RHD Report In Excel" id="headTabRHD">RHD Report In Excel</a></li>
                            <%} else if (userTypeId == 19 || userTypeId == 20) {%>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard </a></li>
                            <%
                                if (objUserRightsMap != null && objUserRightsMap.containsValue("Configuration")) {
                            %>
                        <li><a title="Configuration" href="#" onClick="redirect('config');" id="headTabConfig">Configuration</a></li>
                            <%
                                }
                                if (objUserRightsMap != null && objUserRightsMap.containsValue("STD")) {
                            %>
                        <li><a title="STD" href="<%=contextPath%>/admin/ListSTD.jsp" id="headTabSTD">SBD</a></li>

                        <%
                            }
                            if (objUserRightsMap != null && objUserRightsMap.containsValue("Content")) {
                        %>
                        <li><a title="Content" href="javascript:void(0);" onclick="redirect('content');" id="headTabContent">Content</a></li><!--APP Serach(Rishita)-->
                            <%
                                }
                                if (objUserRightsMap != null && objUserRightsMap.containsValue("Manage Users")) {
                            %>
                        <li><a title="Manage Users" href="#" rel="ddsubmenu2" id="headTabMngUser">Manage Users</a></li>
                            <%
                                }
                                if (objUserRightsMap != null && objUserRightsMap.containsValue("Report")) {
                            %>
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                            <%
                                }
                                if (objUserRightsMap != null && objUserRightsMap.containsValue("Verification")) {
                            %>
                        <li><a title="Verification" href="#" rel="verificationsubmenu" id="headTabVerification">Verification</a></li>
                            <%
                                }
                            %>
                        <li><a href="#" title="My Account" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                        <li><a title="Offline Data" href="#" rel="onmsubmenuofflinedata" id="headTabOfflineData">Offline Data</a></li>


                        <%
                        } else if (userTypeId == 21) {
                        %>
                        <li><a title="Message Box" href="<%=contextPath%>/resources/common/InboxMessage.jsp" id="headTabMsgBox">Dashboard</a></li>
                        <li><a title="Report" href="#" rel="ddsubmenu4" id="headTabReport">Reports</a></li>
                        <li><a title="My Account" href="#" rel="ddsubmenu3" id="headTabMyAcc">My Account</a></li>
                        <li><a title="Help" rel="helpsubmenubankuser" href="#" id="headTabHelp">Help</a></li>
                            <% }%>
                    </ul>
                </div>
                <script type="text/javascript">
                    ddlevelsmenu.setup("ddtopmenubar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
                </script>
                <!--Multilevel Menu 1 (rel="ddsubmenu1") e-GP Admin-->
                <%
                    if (userTypeId == 1 || userTypeId == 16) {%>
                    
                <ul id="debarmentsubmenu" class="ddsubmenustyle">
                    <li><a title="CreateDebarment" href="<%=contextPath%>/admin/InitiateDebarmentProcessNew.jsp" id="headTabDebar">Create Debarment</a></li>
                    <li><a title="DebarmentList" href="<%=contextPath%>/admin/DebarmentListingNew.jsp" id="headTabDebar">Debarment List</a></li>
                    <!--<li><a href="<%=contextPath%>/resources/common/Chairmanship.jsp?lang=en_US&Isadmin=1">Chairman</a></li>
                    <li><a href="<%=contextPath%>/resources/common/DebarmentCommitteeMember.jsp?lang=en_US&Isadmin=1">Debarment Committee</a></li>-->                          
                    <li><a href="#" onClick="redirect('manageDebar');">Manage Debarment Committee</a></li>
                    <!--<li><a href="<%=contextPath%>/resources/common/DebarmentDecisions.jsp?lang=en_US&Isadmin=1">Debarment Decision</a></li>-->
                    <li><a href="<%=contextPath%>/resources/common/DebarmentRules.jsp?lang=en_US&Isadmin=1">Debarment Rule</a></li>
                            
                </ul>
                
                <ul id="ddsubmenu2" class="ddsubmenustyle">

                    <li><a href="#" onClick="redirect('admin');">Admin Users</a></li>
                    <li><a href="#" onClick="redirect('org');">Department Hierarchy</a></li>
                    <li><a href="#" onClick="redirect('dev');">Development Partner Users</a></li>
                    <li><a href="#" onClick="redirect('sb');">Financial Institution Users</a></li>
                    <li><a href="#" onClick="redirect('gov');">Government Users</a></li>
                        <%--<li><a href="#" onClick="redirect('org');">Review Panel</a></li>--%>
                </ul>

                <%--<ul id="configSubMenu" class="ddsubmenustyle">
                </ul>--%>

                <ul id="ddsubmenu4" class="ddsubmenustyle">

                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                    <li><a href="<%=contextPath%>/report/AnnualProcPro.jsp">Annual Procurement Progress Report</a></li>
                    <li><a href="<%=contextPath%>/admin/ViewAuditTrail.jsp">Audit Trail Report</a></li>
                    <li><a href="<%=contextPath%>/report/SaveCalculation.jsp">Dynamic saving Calculation Report</a></li>
                    <li><a href="<%=contextPath%>/report/eGPStatistics.jsp">e-GP Statistics Report</a></li>
                    <li><a href="<%=contextPath%>/resources/common/LoginReport.jsp">Login Report</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/MiscPaymentReport.jsp">Misc Payment Report</a></li>-->

                    <li style="display: none;"><a href="<%=contextPath%>/admin/UserRegReport.jsp?fromWhere=MyAccount">Payment Report</a></li>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransReport.jsp">Procurement Transaction Report (Category wise)</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransRpt.jsp">Procurement Transaction Report (Status wise)</a></li>

                    <li><a href="<%=contextPath%>/report/TenderParticipationRpt.jsp">Tender Participation Report</a></li>
                    <li style="display: none;"><a href="<%=contextPath%>/report/TenderPaymentReport.jsp">Tender Payment Report</a></li>

                    <!-- removed 
                    <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                    <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                    -->
                </ul>


                <ul id="ddsubmenu3" class="ddsubmenustyle">
                    <!--                    <li><a href="#">View Profile</a></li>
                                        <li><a href="#">Edit Profile</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>

                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <!--                    <li><a href="< %//contextPath%>/resources/common/AskProcurement.jsp?fromWhere=MyAccount">Ask Procurement Expert</a></li>-->
                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                    <!--                    <li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Sub Delegate</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 2) {
                    //CommonService commonService_media = (CommonService) AppContext.getSpringBean("CommonService");
                    String regType = commonService_media.getRegType(session.getAttribute("userId").toString());
                    if (regType != null && regType.equalsIgnoreCase("media")) {
                        regType = "m";
                    } else {
                        regType = "t";
                    }

                    ContentAdminService casrv = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    String uId_alt = session.getAttribute("userId").toString();
                    Object[] obj = new Object[3];
                    obj = casrv.getCompanyTendererId(uId_alt);
                %>
                <!--Multilevel Menu 1 (rel="ddsubmenu1") For Tenderer-->
                <ul id="submenu-configuration" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/resources/common/AdvAPPSearch.jsp?from=watchlist">Watch List</a></li>
                    <li><a href="<%=contextPath%>/resources/common/AdvAPPSearch.jsp?from=search">All APPs</a></li>
                </ul>

                <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/tenderer/TenderWatchList.jsp">Watch List</a></li>--%>
                    <li><a href="<%=contextPath%>/tenderer/MyTenders.jsp">My Tenders</a></li>
                    <li><a href="<%=contextPath%>/tenderer/RestrictedTenders.jsp?rfp=no">Limited Tenders</a></li>
                    <li><a href="<%=contextPath%>/tenderer/RestrictedTenders.jsp?rfp=yes">RFP</a></li>
                    <li><a href="<%=contextPath%>/resources/common/AllTenders.jsp?h=f">All Tenders</a></li>
                    <li><a href="<%=contextPath%>/resources/common/ownCategory.jsp?h=f&filter=ownCategory">Own Category</a></li>
                        <% if ("no".equalsIgnoreCase(obj[3].toString()) && ("contractor".equalsIgnoreCase(obj[2].toString()) || "govtundertaking".equalsIgnoreCase(obj[2].toString()) || "supplier".equalsIgnoreCase(obj[2].toString()))) {%>
                    <li><a href="<%=contextPath%>/tenderer/JvcaList.jsp">Propose JVCA</a></li>
                        <% }%>
                </ul>
                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->

                <ul id="submenu-content" class="ddsubmenustyle">
                    <li><a href="#">Programme Details</a></li>
                </ul>
                <ul id="docMenu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/tenderer/CommonDocLib.jsp">Upload Documents</a>
                    </li>
                    <li><a href="<%=contextPath%>/tenderer/DocFolderMgt.jsp">Folder Management</a></li>
                </ul>
                <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                <ul id="myAccountSub" class="ddsubmenustyle">
                    <%if (!obj[0].toString().equals("1") && obj[4].toString().equalsIgnoreCase("yes")) { %>
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%if (obj[0].toString().equals("1")) {
                            out.print(contextPath + "/tenderer/EditPersonalDetail.jsp?tId=" + obj[1]);
                        } else {
                            out.print(contextPath + "/tenderer/EditCompanyDetail.jsp?cId=" + obj[0] + "&tId=" + obj[1]);
                        }%>">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%out.print(contextPath + "/tenderer/Reregistration.jsp?cId=" + obj[0] + "&tId=" + obj[1]);%>">Request for New Procurement Category</a></li> <%}%>
                    <li>
                        <%if ("no".equalsIgnoreCase(obj[3].toString())) {%>
                        <a href="<%=contextPath%>/tenderer/ViewRegistrationDetail.jsp?uId=<%=uId_alt%>&cId=<%=obj[0]%>&tId=<%=obj[1]%>">View Profile</a>
                        <%} else {%>
                        <a href="<%=contextPath%>/tenderer/ViewJVCADetails.jsp?uId=<%=uId_alt%>&noback=y">View Profile</a>
                        <%}%>
                    </li>
                    
                    
                    
                    
                        <% if ("no".equalsIgnoreCase(obj[3].toString()) && ("contractor".equalsIgnoreCase(obj[2].toString()) || "govtundertaking".equalsIgnoreCase(obj[2].toString()) || "supplier".equalsIgnoreCase(obj[2].toString()) || "individualconsultant".equalsIgnoreCase(obj[2].toString()))) {%>
                        <!--<li><a href="<%--<%=contextPath%>--%>/OnlinePmtForRenewal.jsp">Renew Profile Online</a></li>-->
                    <%}%>
                </ul>

                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/tenderer/CommonDocLib.jsp">Upload Document</a></li>
                    <li><a href="<%=contextPath%>/tenderer/DocFolderMgt.jsp">Folder Management</a></li>
                </ul>
                <ul id="adminTenderer" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/tenderer/CompanyAdminListing.jsp?cId=<%=objCompIdTend%>">Manage Users</a></li>
                        <%
                            TenderSubRightServiceImpl tenderSubRightServiceImpl = (TenderSubRightServiceImpl) AppContext.getSpringBean("TenderSubRightService");
                            boolean isJvcaUser = tenderSubRightServiceImpl.isJvcaUser(sn.getAttribute("userId").toString());
                            if (!isJvcaUser) {
                        %>
                    <li><a href="<%=contextPath%>/tenderer/TenderSubRight.jsp">Tender Submission Right</a></li>
                        <%}%>
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                   <%-- <li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp?ut=<%=regType%>">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 3) {%>
                <!--Multilevel Menu 1 (rel="ddsubmenu1") For Govt User-->
                <ul id="submenu-configuration" class="ddsubmenustyle">
                    <li>
                        <%
                            boolean isHOPE = false;
                            boolean roleHopa = false;
                            Object objProRole = session.getAttribute("procurementRole");
                            String strProRole = "";
                            if (objProRole != null) {
                                strProRole = objProRole.toString();
                            }
                            String chk = strProRole;
                            String ck1[] = chk.split(",");
                            for (int iAfterLoginTop = 0; iAfterLoginTop < ck1.length; iAfterLoginTop++) {
                                if (ck1[iAfterLoginTop].equalsIgnoreCase("PE") || ck1[iAfterLoginTop].equalsIgnoreCase("AU")) {
                        %>
                        <a href="<%=contextPath%>/officer/CreateAPP.jsp">Create APP</a>
                        <%
                                    break;
                                }
                                //Nitish Start
                                if(ck1[iAfterLoginTop].equalsIgnoreCase("HOPE"))
                                {
                                    roleHopa = true;
                                }
                            }
                            //Nitish End 28/03/2017
                        %>
                    </li>
                    <li><a href="<%=contextPath%>/officer/MyAPP.jsp">My APP</a></li>
                    <li><a href="<%=contextPath%>/resources/common/AdvAPPSearch.jsp">All APPs</a></li>
                </ul>
                <ul id="evalMenu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/CommListing.jsp?comType=TOC">Opening Committee</a>
                    </li>
                    <% if(roleHopa == false){ %>
                    <li><a href="<%=contextPath%>/officer/CommListing.jsp?comType=TEC">Evaluation Committee</a></li>
                    <%    }%> <!-- For HOPA  Evaluation Committee link will not be visible! ::Nitish 28-03-2017 --> 
    <!--//          <li><a href="<%=contextPath%>/officer/CommListing.jsp?comType=TSC">Tech. Sub. Committee</a></li>-->
                        <%--<li><a href="#">Tender Validaity Ext. Req.</a></li>--%>
                    <li><a href="<%=contextPath%>/officer/TenderExtReqListing.jsp">Tender Validity Date Req</a></li>
                        <% //rishita
                            if (userTypeId == 3) {
                        %>
                    <!--                                                        <li><a href="#">Tender Committee</a>
                                                                                <ul class ="ddsubmenustyle">
                                                                                    <li><a href="javascript:void(0);">Evaluation Process</a></li>
                                                                                    <li><a href="<%=contextPath%>/officer/CommTenderListing.jsp?comType=TEC&status=pending">Evaluation Committee Formation</a></li>
                                                                                </ul>
                                                                            </li>-->

                    <%
                        objProRole = session.getAttribute("procurementRole");
                        strProRole = "";
                        if (objProRole != null) {
                            strProRole = objProRole.toString();
                        }
                        chk = strProRole;
                        String ck[] = chk.split(",");

                        for (int iAfterLoginTop = 0; iAfterLoginTop < ck.length; iAfterLoginTop++) {
                            if (ck[iAfterLoginTop].equalsIgnoreCase("PE")) {
                                if (false) {   // Tender Committee Formation hide by aprojit
                    %>
                    <li><a href="<%=contextPath%>/officer/CommTenderListing.jsp?comType=TC&status=pending">Tender Committee Formation</a></li>
                    <!--<li><a href="<%=contextPath%>/officer/CommTenderListing.jsp?comType=TOC&status=pending">Opening Committee Formation</a></li>-->
                    <%
                            }
                        }
                        if (ck[iAfterLoginTop].equalsIgnoreCase("HOPE") || ck[iAfterLoginTop].equalsIgnoreCase("Secretary")) {
                            isHOPE = true;
                    %><li><a href="<%=contextPath%>/officer/TOExtReqListing.jsp">Opening Date Ext Req</a></li><%
                                    break;
                                }
                            }
                        }//end
%>
                        <%-- <li><a href="<%=contextPath%>/officer/ReviewReportListing.jsp">Evaluation Report Review</a></li> --%>
                    <!--<li><a href="<%=contextPath%>/officer/TECReportListing.jsp">Evaluation Report Approval for HOPA</a></li>-->
                </ul>
                <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <% //rishita - bug id ::0004745
                        boolean pmc = false;
                        if (userTypeId == 3) {
                            objProRole = session.getAttribute("procurementRole");
                            strProRole = "";
                            if (objProRole != null) {
                                strProRole = objProRole.toString();
                            }
                            chk = strProRole;
                            String ck[] = chk.split(",");

                            for (int iAfterLoginTop = 0; iAfterLoginTop < ck.length; iAfterLoginTop++) {
                                if (ck[iAfterLoginTop].equalsIgnoreCase("PE") || ck[iAfterLoginTop].equalsIgnoreCase("AU")) {
                    %><li><a href="<%=contextPath%>/officer/APPForTender.jsp">Create Tender</a></li><%
                                    break;
                                } else if (ck[iAfterLoginTop].equalsIgnoreCase("PMC")) {
                                    pmc = true;
                                }
                            }
                        }//end
%>
                                                                                                                                                        <!--                    <li><a href="<%=contextPath%>/officer/APPForTender.jsp">Create Tender</a></li>-->
                    <li><a href="<%=contextPath%>/officer/MyTenders.jsp">My Tender</a></li>
                    <li><a href="<%=contextPath%>/resources/common/AllTenders.jsp?h=f">All Tenders</a></li>
                </ul>
                <ul id="ddsubmenu3" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=pending">Pending task</a></li>
                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=processed">Processed task</a></li>
                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=defaultworkflow">Approved Workflow</a></li>
                </ul>
                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                <ul id="submenu-content" class="ddsubmenustyle">
                    <li><a href="#">Programme Details</a></li>
                </ul>
                <ul id="myAcntMnu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ViewGovtUserDetail.jsp?fromWhere=ViewProfile">View Profile</a></li>

                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <!-- Report Link for HOPE-->
                <%

                    if ((listPD.size() > 0 || pmc == true) && isHOPE == false) {%>
                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <!-- Removed 
                    <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                    <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                    -->
                </ul>
                <% } else {
                %>
                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransReport.jsp">Procurement Transaction Report (Category wise)</a></li>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>


                    <!--Remnoved 
                    <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                    <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                     !-->
                </ul>
                <%}
                } else if (userTypeId == 5) {%>
                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransReport.jsp">Procurement Transaction Report</a></li>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>

                    <!--Removed 
                                <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                                <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                                <li><a href="<%=contextPath%>/resources/common/AllTenders.jsp">DLI1 Report</a></li>
                    -->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                     <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 4) {%>
                <ul id="repMenu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>

                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                     <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 6) {
                    int intUsrId = 0;
                    NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                    if (sn.getAttribute("userId") != null) {
                        intUsrId = Integer.parseInt(sn.getAttribute("userId").toString());
                    }

                    boolean isAdmin = navRuleUtil.isAdmin(intUsrId);
                %>
                <%}
                    if (userTypeId == 6) {
                        int intUsrId = 0;
                        NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                        if (sn.getAttribute("userId") != null) {
                            intUsrId = Integer.parseInt(sn.getAttribute("userId").toString());
                        }

                        boolean isAdmin = navRuleUtil.isAdmin(intUsrId);
                %>
                <ul id="ddsubmenu3" class="ddsubmenustyle">

                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=pending">Pending task</a></li>
                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=processed">Processed task</a></li>
                    <li><a href="<%=contextPath%>/officer/PendingProcessing.jsp?viewtype=defaultworkflow">Approved Workflow</a></li>
                </ul>
                <ul id="ddsubmenu1" class="ddsubmenustyle">
                    <%if (isAdmin) {%>
                    <li><a href="<%=contextPath%>/admin/ViewSbDevPartAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=Development&userTypeId=6">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/EditSbDevPartAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=Development&userTypeId=6&mode=edit">Edit Profile</a></li>
                        <%} else {%>
                    <li><a href="<%=contextPath%>/admin/ViewSbDevPartUser.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=Development&userTypeId=6">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/EditSbDevPartUser.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=Development&userTypeId=6&mode=edit">Edit Profile</a></li>
                        <%}%>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <ul id="ddsubmenu4" class="ddsubmenustyle">
<!--                    <li><a href="<%=contextPath%>/resources/common/LoginReport.jsp">Login Report</a></li>-->
                </ul>
                <%
                } else if (userTypeId == 4 || userTypeId == 5 || userTypeId == 7 || userTypeId == 15) {
                    int intUsrId = 0;
                    NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                    if (sn.getAttribute("userId") != null) {
                        intUsrId = Integer.parseInt(sn.getAttribute("userId").toString());
                    }
                    boolean isAdmin = navRuleUtil.isAdmin(intUsrId);
                %>
                <ul id="ddsubmenu3" class="ddsubmenustyle">
                    <%if (userTypeId == 5) {%>
                    <li><a href="<%=contextPath%>/admin/ViewManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>&msg=">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                        <%} else if (userTypeId == 7) {
                            if (isAdmin) {%>
                    <li><a href="<%=contextPath%>/admin/ViewSbDevPartAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/EditSbDevPartAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>&mode=edit">Edit Profile</a></li>
                        <%} else {%>
                    <li><a href="<%=contextPath%>/admin/ViewSbDevPartUser.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/EditSbDevPartUser.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>&mode=edit">Edit Profile</a></li>
                        <%}/*Condtion ends of admin and username*/
                        } else if (userTypeId == 15) {%>
                    <li><a href="<%=contextPath%>/admin/ViewSbBranchAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/EditSbBranchAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&partnerType=ScheduleBank&userTypeId=<%=userTypeId%>&mode=edit">Edit Profile</a></li>
                        <%} else {%>
                    <li><a href="<%=contextPath%>/admin/ViewPEAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ManagePEAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                        <%}%>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>

                <ul id="ddsubmenu4" class="ddsubmenustyle">
<!--                    <li><a href="<%=contextPath%>/resources/common/LoginReport.jsp">Login Report</a></li>-->
                </ul>

                <%if (userTypeId == 7 || userTypeId == 15) {%>
                <ul id="paymentsubmenu" class="ddsubmenustyle">
                    <%
                        TenderCommonService tscBankUserRole = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> lstBankUserRole = tscBankUserRole.returndata("getBankUserRole", session.getAttribute("userId").toString(), null);

                        int inSchBnkAdminId = 0;
                        if (sn.getAttribute("userId") != null) {
                            inSchBnkAdminId = Integer.parseInt(sn.getAttribute("userId").toString());
                        }
                        boolean isSchBnkAdmin = navRuleUtil.isAdmin(inSchBnkAdminId);
                        //out.println(isSchBnkAdmin);
                        if (!lstBankUserRole.isEmpty()) {
                            if ("BranchMaker".equalsIgnoreCase(lstBankUserRole.get(0).getFieldName1())) {%>
                    <%-- <li><a href="<%=contextPath%>/partner/SearchTendererForRegistration.jsp">Registration Fee</a></li> --%> <%-- Disbale Under Mohsina Apu --%>
                    <li><a href="<%=contextPath%>/partner/SearchTenPayment.jsp">Tender Payment</a></li>
                    <!--li><a href="<%=contextPath%>/resources/common/searchFirstForComplaintPayments.htm?feetype=Registration Fee">Complaint Registration Fee</a></li-->
                    <!--li><a href="<%=contextPath%>/resources/common/searchFirstForComplaintPayments.htm?feetype=Security Deposit Fee">Complaint Security Fee</a></li-->
                        <%} else if ("BranchChecker".equalsIgnoreCase(lstBankUserRole.get(0).getFieldName1())) {%>
                    <%-- <li><a href="<%=contextPath%>/partner/VerifyPayment.jsp">Registration Fee</a></li> --%> <%-- Disbale Under Mohsina Apu --%>
                    <li><a href="<%=contextPath%>/partner/TenPaymentListing.jsp">Tender Payment</a></li>
                    <!--li><a href="<%=contextPath%>/resources/common/searchFirstForComplaintPayments.htm?feetype=Registration Fee&verify=verify">Complaint Registration Fee</a></li-->
                    <!--li><a href="<%=contextPath%>/resources/common/searchFirstForComplaintPayments.htm?feetype=Security Deposit Fee&verify=verify">Complaint Security Fee</a></li-->
                        <%} else if ("BankChecker".equalsIgnoreCase(lstBankUserRole.get(0).getFieldName1())) {%>
                    <li><a href="<%=contextPath%>/partner/VerifyPayment.jsp">Registration Fee</a></li>
                    <li><a href="<%=contextPath%>/partner/TenPaymentListing.jsp">Tender Payment</a></li>

                    <%}%>
                    <%}%>


<!--                        <li><a href="<%=contextPath%>/partner/SearchTenPayment.jsp">Tender Payment</a></li>-->
                </ul>
                <%if (userTypeId == 7) {%>
                <ul id="paymentrepsubmenu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/partner/RegFeeReport.jsp">Registration Fee</a></li>
                    <li style="display: none;"><a href="<%=contextPath%>/partner/TenderPaymentReport.jsp">Tender Payment</a></li>
                        <%}%>
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp?schAd=<%=isSchBnkAdmin%>">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%}%>
                <%} else if (userTypeId == 8) {%>
                <!-- Content Admin-->
                <ul id="submenu-configuration" class="ddsubmenustyle">
                    <li><a href="#">Financial Year Details</a>
                    </li>
                    <li><a href="#">Configure Calendar</a></li>
                    <li><a href="#">Item 3</a></li>
                </ul>
                <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                <!--<ul id="ddsubmenu2" class="ddsubmenustyle">
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                    <li><a href="#">Menu 4</a></li>
                </ul>-->

                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                <ul id="submenu-content" class="ddsubmenustyle">
                    <li><a href="#">Programme Details</a></li>
                </ul>

                <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                <!--                <ul id="submenu-manageuser" class="ddsubmenustyle">
                    <li><a href="#">Organization Structure</a></li>
                    <li><a href="#">Admin Users</a></li>
                    <li><a href="#">Government users</a></li>
                    <li><a href="#">Development partner users</a></li>
                    <li><a href="#">Sch. Bank users</a></li>
                </ul>-->

                <ul id="ddsubmenu1" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/admin/ViewManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>&msg=">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                </ul>

                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <li style="display: none;">><a href="<%=contextPath%>/admin/UserRegReport.jsp?fromWhere=MyAccount">Payment Report</a></li>
                    <li style="display: none;"><a href="<%=contextPath%>/report/TenderPaymentReport.jsp">Tender Payment Report</a></li>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>
                    <li><a href="<%=contextPath%>/resources/common/LoginReport.jsp">Login Report</a></li>
                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransReport.jsp">Procurement Transaction Report (Category wise)</a></li>
                    <li><a href="<%=contextPath%>/report/ProcTransRpt.jsp">Procurement Transaction Report (Status wise)</a></li>
                    <li><a href="<%=contextPath%>/report/SaveCalculation.jsp">Dynamic saving Calculation Report</a></li>
                    <li><a href="<%=contextPath%>/report/TenderParticipationRpt.jsp">Tender Participation Report</a></li>
                    <li><a href="<%=contextPath%>/report/eGPStatistics.jsp">e-GP Statistics Report</a></li>
                    <li><a href="<%=contextPath%>/report/AnnualProcPro.jsp">Annual Procurement Progress Report</a></li>
<!--                    <li><a href="<%=contextPath%>/admin/ViewAuditTrail.jsp">Audit Trail Report</a></li>-->

                    <!--Remnoved
                   <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                   <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                    -->
                    <!--<li><a href="<%=contextPath%>/admin/MiscPaymentReport.jsp">Misc Payment Report</a></li>-->
                    <li><a href="<%=contextPath%>/report/TendererReport.jsp">Bidder Report</a></li>
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                   <%-- <li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%-- <li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 11) {%>
                <ul id="submenu-configuration" class="ddsubmenustyle">
                    <li><a href="#">Watch list</a>
                        <ul>
                            <li><a href="#">Sub Menu 1</a></li>
                            <li><a href="#">Sub Menu 2</a></li>
                            <li><a href="#">Sub Menu 3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">All APPs</a></li>
                </ul>
                <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <li><a href="#">Watch list</a></li>
                    <li><a href="#">All Tenders</a></li>
                </ul>

                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                <ul id="submenu-content" class="ddsubmenustyle">
                    <li><a href="#">Programme Details</a></li>
                </ul>

                <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                <ul id="ddsubmenu3" class="ddsubmenustyle">

                    <li><a href="#">Edit Profile</a></li>
                    <li><a href="#">View Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <%} else if (userTypeId == 12) {%>
                <ul id="ddsubmenu3" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                    <li><a href="<%=contextPath%>/admin/ViewManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>&msg=">View Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                   <%-- <li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                   <%-- <li><a href="javascript:void(0);" >Page Help</a></li>--%>
                   <%-- <li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <%} else if (userTypeId == 14) {%>
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/EditExtEvalCommMember.jsp?uId=<%=session.getAttribute("userId").toString()%>">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ViewRegExtEvalCommMember.jsp?uId=<%=session.getAttribute("userId").toString()%>">View Profile</a></li>
                </ul>
                <ul id="evalMenu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/officer/CommListing.jsp?comType=TSC">Tech. Sub. Committee</a></li>
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                   <%-- <li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                   <%--<li><a href="javascript:void(0);" >Page Help</a></li>--%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>
                <% } else if (userTypeId == 17) {%>
                <ul id="myAcntMnu" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ViewGovtUserDetail.jsp?fromWhere=ViewProfile">View Profile</a></li>

                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <% } else if (userTypeId == 18) {%>
                <ul id="ddsubmenurhd" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/resources/common/RHDCmsReport.jsp">RHD Over All Report</a></li>
                    <li><a href="<%=contextPath%>/resources/common/RHDCmsBoQForRHD.jsp">RHD BOQ Details</a></li>
                    <li><a href="<%=contextPath%>/resources/common/RHDPEReport.jsp">RHD PE Sharing Report</a></li>
                </ul>
                <% } else if (userTypeId == 21) {%>
                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <!--Remnoved
                   <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>
                   <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li>
                    -->
                </ul>
                <ul id="ddsubmenu3" class="ddsubmenustyle">
                    <!--<li><a href="#">View Profile</a></li>
                    <li><a href="#">Edit Profile</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ViewManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>&frm=vp&msg=">View Profile</a></li>



                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>

                <% } else if (userTypeId == 19 || userTypeId == 20) {%>
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Department Hierarchy")) {
                    %>
                    <li><a href="#" onClick="redirect('org');">Department Hierarchy</a></li>
                        <%                                                    }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Admin Users")) {
                    %>
                    <li><a href="#" onClick="redirect('admin');">Admin Users</a></li>
                        <%                                                    }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Government Users")) {
                    %>
                    <li><a href="#" onClick="redirect('gov');">Government Users</a></li>
                        <%                                                    }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Development Partner Users")) {
                    %>
                    <li><a href="#" onClick="redirect('dev');">Development Partner Users</a></li>
                        <%                                                    }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Scheduled Bank Users")) {
                    %>
                    <li><a href="#" onClick="redirect('sb');">Scheduled Bank Users</a></li>
                        <%                                                    }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Review Panel")) {
                    %>
                    <li><a href="#" onClick="redirect('org');">Review Panel</a></li>
                        <%                                                    }
                        %>

                </ul>
                <ul id="onmsubmenuofflinedata" class="ddsubmenustyle">
                    <li><a href="<%=contextPath%>/offlinedata/CreateTenderWithPQ.jsp">Create Tender With PQ</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/CreateTenderWithoutPQ.jsp">Create Tender Without PQ</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/CreateREOI.jsp">Create REOI</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/CreateContractAward.jsp">Create Contract Award</a></li>
                   <!-- <li><a href="<%=contextPath%>/offlinedata/viewAwardedContractOffline.jsp">View Contract Award Offline</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/EditContractAward.jsp?action=Edit&awardolid=2">Edit Contract Award Offline</a></li>
                    -->
                    <li><a href="<%=contextPath%>/offlinedata/TenderDashboardOfflineApproval.jsp">Edit/Delete Tender/REOI</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/SearchAwardedContractOffline.jsp">Edit/Delete Awarded Contract</a></li>
                        <% if (userTypeId == 19) {%>
                    <li><a href="<%=contextPath%>/offlinedata/TenderDashboardOfflineApproval.jsp">Approve Tender/REOI</a></li>
                    <li><a href="<%=contextPath%>/offlinedata/SearchAwardedContractOffline.jsp">Approve Awarded Contract</a></li>
                        <%}%>
                </ul>
                <%--<ul id="configSubMenu" class="ddsubmenustyle">
                </ul>--%>

                <ul id="ddsubmenu4" class="ddsubmenustyle">
                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Payment Report")) {
                    %>

                    <li style="display: none;">><a href="<%=contextPath%>/admin/UserRegReport.jsp?fromWhere=MyAccount">Payment Report</a></li>
                        <%
                            }
                        %>
                        <%
                            if (objUserRightsMap != null && objUserRightsMap.containsValue("Tender Payment Report")) {
                        %>
                    <li style="display: none;"><a href="<%=contextPath%>/report/TenderPaymentReport.jsp">Tender Payment Report</a></li>
                        <%
                            }
                        %>
                        <%
                            if (objUserRightsMap != null && objUserRightsMap.containsValue("Procurement Statistics")) {
                        %>
                    <li><a href="<%=contextPath%>/report/AppMIS.jsp">Procurement Statistics</a></li>
                        <%
                            }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Login Report")) {
                    %>
                    <li><a href="<%=contextPath%>/resources/common/LoginReport.jsp">Login Report</a></li>
                        <%
                            }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Annual Procurement Plan")) {
                    %>
                    <li><a href="<%=contextPath%>/officer/SearchPackageReport.jsp">Annual Procurement Plan</a></li>
                        <%
                            }
                        %>

                    <%if (objUserRightsMap != null && objUserRightsMap.containsValue("Procurement Transaction Report")) {
                    %>
                    <li><a href="<%=contextPath%>/report/ProcTransReport.jsp">Procurement Transaction Report</a></li>
                        <%
                            }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Audit Trail Report")) {
                    %>
<!--                    <li><a href="<%=contextPath%>/admin/ViewAuditTrail.jsp">Audit Trail Report</a></li>-->
                        <%
                            }
                        %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("PROMIS Report")) {
                    %>
                    <!--Removed  <li><a href="<%=contextPath%>/admin/PromiseIndicator.jsp">PROMIS Report</a></li>-->
                    <%
                        }
                    %>

                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("PROMIS Overall Report")) {
                    %>
                     <!--Remnoved  <li><a href="<%=contextPath%>/admin/PromiseOverallReport.jsp">PROMIS Overall Report</a></li> -->
                    <%
                        }
                    %>
                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Misc Payment Report")) {
                    %>
                    <!--<li><a href="<%=contextPath%>/admin/MiscPaymentReport.jsp">Misc Payment Report</a></li>-->
                        <%
                            }
                        %>

                </ul>

                <ul id="verificationsubmenu" class="ddsubmenustyle">
                    <%
                        if (objUserRightsMap != null && objUserRightsMap.containsValue("Company Verification")) {
                    %>
                    <li><a href="<%=contextPath%>/admin/CompanyVerification.jsp" >Company Verification</a></li>
                        <%
                            }
                            if (objUserRightsMap != null && objUserRightsMap.containsValue("Content Verification")) {
                        %>
                    <li><a href="<%=contextPath%>/contentPublicForumBoard.jsp" >Content Verification</a></li>
                        <%
                            }
                        %>

                </ul>

                <ul id="ddsubmenu3" class="ddsubmenustyle">

                    <li><a href="<%=contextPath%>/admin/ChangeHintQusAns.jsp?fromWhere=MyAccount">Change Hint Question and Answer</a></li>
                    <li><a href="<%=contextPath%>/admin/ChangePassword.jsp?fromWhere=MyAccount">Change Password</a></li>
                    <li><a href="<%=contextPath%>/admin/ManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>">Edit Profile</a></li>
                    <!--<li><a href="<%=contextPath%>/admin/UserPreference.jsp?fromWhere=MyAccount">Preference</a></li>-->
                    <li><a href="<%=contextPath%>/admin/ViewManageAdmin.jsp?userId=<%=session.getAttribute("userId").toString()%>&userTypeid=<%=userTypeId%>&frm=vp&msg=">View Profile</a></li>
                    <!--                    <li><a href="< %=contextPath%>/resources/common/QuestionListing.jsp">Ask Procurement Expert</a></li>-->
                </ul>
                <ul id="helpsubmenubankuser" class="ddsubmenustyle">
                    <%--<li><a href="<%=contextPath%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1]%>" target="_blank">Page Help</a></li> --%>
                    <%-- <li><a href="javascript:void(0);" >Page Help</a></li> --%>
                    <%--<li><a href="<%=contextPath%>/resources/common/userManual.jsp">User Manual</a></li>
                    <li><a href="http://helpdesk.eprocure.gov.bd:8081" target="_blank">Help Desk</a></li>--%>
                    <li><a href="javascript:void(0);">User Manual</a></li>
                </ul>


                <%}
                %>
                <%--<ul id="ddsubmenu1" class="ddsubmenustyle">
                    <li><a href="#">Item 1</a>
                        <ul>
                            <li><a href="#">Sub Menu 1</a></li>
                            <li><a href="#">Sub Menu 2</a></li>
                            <li><a href="#">Sub Menu 3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Item 2</a></li>
                    <li><a href="<%=request.getContextPath()%>/admin/CreateForm.jsp">Create Form</a></li>
                </ul>
                <!--APP Serach(Rishita)-->
                <ul id="contentsubmenu" class="ddsubmenustyle">
                    <li><a href="<%=request.getContextPath()%>/officer/MyAPP.jsp">APP Search</a></li>
                </ul>
                <!--Multilevel Menu 1 (rel="ddsubmenu2")-->
                <ul id="ddsubmenu2" class="ddsubmenustyle">
                    <li><a href="#" onClick="redirect('org');">Organization Structure</a></li>
                    <li><a href="#" onClick="redirect('admin');">Admin Users</a></li>
                    <li><a href="#" onClick="redirect('gov');">Government users</a></li>
                    <li><a href="#" onClick="redirect('dev');">Development partner users</a></li>
                    <li><a href="#" onClick="redirect('sb');">Sch. Bank users</a></li>
                </ul>--%>
                <table width="100%" cellspacing="6" class="loginInfoBar">
                    <tr>
                        <td align="left" id="lblTime" style="font-weight: bold"></td>
                        <td align="center">
                            <%if (lastLogin_dt != null) {%>
                            <b>Last Login :</b> <%= dateFormat.format(lastLogin_dt)%> BST
                            <%}%>
                        </td>
                        <td align="right">
                            <img alt="Logout" src="<%=contextPath%>/resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>
                                <span style="font-size: 14px">Welcome,
                                    <%
                                        out.print(objUserName);

                                        List<TblUserTypeMaster> lstUsertype = commonService_media.getUserTypeByUserTypeId("userTypeId", Operation_enum.EQ, (byte) userTypeId);
                                        String role = "";
                                        if (userTypeId==3) { //User Type= Officer
                                            role = commonService_media.getUserRoleByUserId(Integer.parseInt(session.getAttribute("userId").toString()));                       
                                        }
                                        else if (userTypeId==7){ //User Type= ScheduleBank
                                            role = commonService_media.getBankUserRoleByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                                        }
                                        else{
                                            role = lstUsertype.get(0).getUserType();
                                            if(role.equalsIgnoreCase("Tenderer"))
                                            {
                                                if("1".equalsIgnoreCase(commonService_media.getCompanyIdByUserId(Integer.parseInt(session.getAttribute("userId").toString()))))                       
                                                    role="Individual Consultant";
                                                else
                                                    role="Bidder";
                                            }
                                            
                                            if(role.equalsIgnoreCase("PEAdmin"))role="PAAdmin";
                                        }
                                        
                                    %>
                                    [<div class="tooltip"><%
                                        if(role.contains(","))out.print("Roles"); else out.print("Role");
                                        %><span class="tooltiptext"><%=role%></span></div>]
                                </span> </b> &nbsp;|&nbsp;
                            <img src="<%=contextPath%>/resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" />
                            <a href=<%=logoutPage%> title="Logout">Logout</a>
                            <input type="hidden" id="curDate" value="<%=lastdate%>"/></td>
                    </tr>
                </table></td>

<!--            <td width="141"><img src="<%=contextPath%>/resources/images/cptuEgpLogo.gif" width="141" height="64" alt="e-GP" /></td>-->
        </tr>
    </table>
</div>
<%
    String aftLTop_uTId = "";
    if (userTypeId != 0) {
        aftLTop_uTId = userTypeId + "";
    }
    TenderCommonService aftLTop_tCommSrv = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> aftLTop_list = aftLTop_tCommSrv.returndata("getmarqueedisplay", "Dashboard", aftLTop_uTId);
    if (!aftLTop_list.isEmpty()) {
%>
<div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td width="79%">
                <div id="cascadeDiv" style="font-weight: bold"></div>
                <script>
                    $.post('<%=request.getContextPath()%>/CascadeLinkServlet', {
                        pageName: "<%=request.getRequestURL().toString()%>",
                        parentLink: '<%=request.getParameter("parentLink")%>'},
                            function (data) {
                                if (data != null && data != "") {
                                    document.getElementById('cascadeDiv').className = "stepWiz_1 t_space";
                                }
                                var res = data.replace("Send to PE", "Send to PA");
                                document.getElementById('cascadeDiv').innerHTML = res;
                            });
                </script>
        <marquee behavior=scroll scrollamount="3" style="height:25px; line-height:25px; color:#ff0000; font-size: 14px; font-weight: bold;">
            <%=URLDecoder.decode(aftLTop_list.get(0).getFieldName1(), "UTF-8")%>
        </marquee>
        </td>
        <td width="1%"></td>
        <td width="20%" align="right" >
            <!--Removed 
            <div class="ReadMore">
                 <strong><a href="<%=request.getContextPath()%>/resources/common/ViewMarqueeList.jsp">View All Notifications</a>&nbsp;&nbsp;&nbsp;&nbsp;</strong> 
            </div>
            -->
        </td>
        </tr>
    </table>
</div>
<%
    }
%>
<script type="text/javascript" src="<%=contextPath%>/resources/js/DatenTime.js"></script>
<script type="text/javascript">
                    Start();
</script>
<!--Dashboard Header End-->
<body ></body>
<!--onmousedown="return isKeyPressed1(event);"-->