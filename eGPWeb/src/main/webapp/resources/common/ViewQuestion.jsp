<%--
    Document   : ViewQuestion
    Created on : Jan 12, 2011, 4:15:17 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Query</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
       <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
       -->
        <jsp:useBean scope="request" id="askProcurementSrBean" class="com.cptu.egp.eps.web.servicebean.AskProcurementSrBean" />

    </head>
    <%
                int procQueId = 0;
                if (request.getParameter("procQueId") != null) {
                    procQueId = Integer.parseInt(request.getParameter("procQueId"));
                    askProcurementSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                }
                if(session.getAttribute("userId") != null){
                    askProcurementSrBean.setLogUserId(session.getAttribute("userId").toString());
                }
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="Top.jsp"%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="contentArea_1">
                            <div class="pageHead_1">View Query
                                <span class="c-alignment-right">
                                    <a href="QuestionListing.jsp" class="action-button-goback">Go Back To Dashboard</a>
                                </span>
                            </div>
                            <form id="frmQuestionAnswer" name="frmQuestionAnswer" action="" method="post">
                                <table class="tableList_1 t_space" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <% for (Object[] obj : askProcurementSrBean.getListQuestion(procQueId)) {%>
                                    <tr>
                                        <td>Category</td>
                                        <td><%=obj[1]%></td>
                                    </tr>
                                    <tr>
                                        <td>Query</td>
                                        <td><%=obj[3]%></td>
                                    </tr>
                                    <tr>
                                        <td>Answer</td>
                                        <td><%=obj[6]%></td>
                                    </tr>
                                    <% }%>
                                </table>
                            </form>
                        </td>
                        <jsp:include page="Bottom.jsp" ></jsp:include>
                    </tr>
                </table>
                <!--Middle Content Table End-->

            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%
                askProcurementSrBean = null;
    %>
</html>
