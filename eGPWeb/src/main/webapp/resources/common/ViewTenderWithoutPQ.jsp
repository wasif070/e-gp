<%-- 
    Document   : ViewTenderWithoutPQ
    Created on : Sep 5, 2012, 11:52:38 AM
    Author     : Istiak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<jsp:useBean id ="ViewTender" class="com.cptu.egp.eps.web.servicebean.TenderDashboardOfflineSrBean"/>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>      

        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Tender without Pre-qualification (PQ)</title>

        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../js/form/CommonValidation.js"type="text/javascript"></script>

        <!-- jQuery Datatable -->
        <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
        <link href="../css/demo_table.css" type="text/css" rel="stylesheet">

 
    </head>



    <body>

         <div class="mainDiv">
            <div class="fixDiv">

               
        <!--Dashboard Header End-->
                <br/>
                <div class="contentArea_1">

                    <form id="frmViewTenderWithoutPQ" name="frmViewTenderWithoutPQ" method="POST" action="">

                    <div class="pageHead_1">View Tender without Pre-qualification (PQ)</div>

                    <%
                            String id = request.getParameter("ID");
                            for (TenderDashboardOfflineDetails tenderDashboardOfflineDetails : ViewTender.getTenderDashboardOfflineDetails(id)) {

                    %>

                    <div class="tableHead_22 t_space">PROCURING Agency (PA) INFORMATION</div>
                   
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">

                            <tbody>
                                <tr>
                                    <td colspan="4" class="ff t-align-left" align="left"></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="25%">Ministry/Division :</td>
                                    <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getMinistryOrDivision())%></td>
                                    <td class="ff" width="25%">Agency :</td>
                                    <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getAgency())%></td>
                                </tr>
                                <tr> 
                                    <td class="ff">Procuring Agency Name :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeName())%></td>

                                    <td class="ff">Procuring Agency Code :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeCode())%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Procuring Agency Dzongkhag / District :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeDistrict())%></td>

                                    <td class="ff">Procurement Category :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProcurementNature())%></td>

                                </tr>
                                <tr>
                                    <td class="ff">Procurement Type :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProcurementType())%></td>

                                    <td class="ff">Event Type :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getEventType())%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Invitation for :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getInvitationFor())%></td>
                                    <td class="ff">Invitation Reference No. :  </td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getReoiRfpRefNo())%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Date :  </td>
                                    <td>
                                        <%
                                            String date = "";
                                            if(tenderDashboardOfflineDetails.getIssueDate() != null)
                                            {
                                                date = DateUtils.customDateFormate(tenderDashboardOfflineDetails.getIssueDate());
                                            }
                                        %>
                                        <%=date%>
                                    </td>
                                    <td class="ff"></td>
                                    <td class="formStyle_1">
                                        <input type="hidden" id="tenderOfflineId" name="tenderOfflineId" value="<%=id%>"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="tableHead_22 ">Key Information and Funding Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody>
                                <tr>
                                   <td class="ff" width="25%">Procurement Method : </td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProcurementMethod())%></td>
                                   <td class="ff" width="25%">Budget Type :</td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getBudgetType())%></td>
                                </tr>
                                <tr>
                                   <td class="ff" width="25%">Source of Funds :</td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getSourceOfFund())%></td>

                                   <td class="ff" width="25%">Development Partner : </td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getDevPartners())%></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="tableHead_22 ">Particular Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Project Code : </td>
                                <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProjectCode())%></td>
                                <td class="ff" width="25%">Project Name : </td>
                                 <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProjectName())%></td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Tender Package No. :</td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPackageNo())%></td>
                                <td class="ff" width="25%">Tender Package Name :</td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPackageName())%></td>
                            </tr>
                            <tr>
                                <td class="ff">Tender Publication Date :  </td>
                                <td class="formStyle_1">
                                    <%
                                        String tenPubDate = "";
                                        if(tenderDashboardOfflineDetails.getTenderPubDate() != null)
                                        {
                                            tenPubDate = DateUtils.customDateFormate(tenderDashboardOfflineDetails.getTenderPubDate());
                                        }
                                    %>
                                    <%=tenPubDate%>
                                </td>

                                <td class="ff">Tender Last Selling Date :  </td>
                                <td class="formStyle_1">
                                    <%
                                        String lastSellingDate = "";
                                        if(tenderDashboardOfflineDetails.getLastSellingDate() != null)
                                        {
                                            lastSellingDate = DateUtils.customDateFormate(tenderDashboardOfflineDetails.getLastSellingDate());
                                        }
                                    %>
                                    <%=lastSellingDate%>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Tender Closing Date and Time :  </td>
                                <td class="formStyle_1">
                                    <%
                                        String tenClosingDateTime = "";
                                        if(tenderDashboardOfflineDetails.getClosingDate() != null)
                                        {
                                            tenClosingDateTime = DateUtils.gridDateToStrWithoutSec(tenderDashboardOfflineDetails.getClosingDate());
                                        }
                                    %>
                                    <%=tenClosingDateTime%>
                                </td>
                                <td class="ff">Tender Opening Date and Time :  </td>
                                <td class="formStyle_1">
                                    <%
                                        String tenOpenDateTime = "";
                                        if(tenderDashboardOfflineDetails.getOpeningDate() != null)
                                        {
                                            tenOpenDateTime = DateUtils.gridDateToStrWithoutSec(tenderDashboardOfflineDetails.getOpeningDate());
                                        }
                                    %>
                                    <%=tenOpenDateTime%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Selling Tender Document(Principal) : </td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getSellingAddPrinciple())%></td>

                                <td class="ff" width="25%">Selling Tender Document(Others):</td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getSellingAddOthers())%></td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Receiving Tender Document : </td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getReceivingAdd())%></td>


                                <td class="ff" width="25%">Opening Tender Document: </td>
                                <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getOpeningAdd())%></td>
                            </tr>
                            <tr>
                                <td class="ff">Place of Pre-Tender Meeting :</td>
                                <td class="formStyle_1"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPreTenderREOIPlace())%></td>

                                <td class="ff">Date and Time of Pre-Tender Meeting : </td>
                                <td class="formStyle_1">
                                    <%
                                        String tenMeetingDateTime = "";
                                        if(tenderDashboardOfflineDetails.getPreTenderREOIDate() != null)
                                        {
                                            tenMeetingDateTime = DateUtils.gridDateToStrWithoutSec(tenderDashboardOfflineDetails.getPreTenderREOIDate());
                                        }
                                    %>
                                    <%=tenMeetingDateTime%>
                                </td>
                            </tr>

                        </tbody></table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody>
                                <tr>
                                    <td class="ff" width="25%">Eligibility of Bidder/Consultant :  </td>
                                    <td colspan="2"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getEligibilityCriteria())%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Brief Description of Works :  </td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getBriefDescription())%></td><td>&nbsp;</td>
                                </tr>


                                <tr style="display: table-row;" id="docsprice">
                                    <td class="ff">Tender Document Price (In Nu.) :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getDocumentPrice().toString())%></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>

                        <%
                            String  lotTable = ViewTender.lotInfo(id, "wpq");
                        %>

                        <%=lotTable%>   <!--Load Lot Table-->
                        
                        <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody>
                                <tr>
                                   <td class="ff" width="25%">Name of Official Inviting  Tender :</td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeOfficeName())%></td>
                                    <td class="ff" width="26%"> Designation of Official Inviting  Tender :</td>
                                   <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeDesignation())%></td>
                                </tr>

                                <tr>
                                    <td class="ff">Address of Official Inviting  Tender : </td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeAddress())%></td>
                                    <td class="ff">Contact details of Official Inviting  Tender :</td>
                                    <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeContactDetails())%></td>
                                </tr>
                            </tbody>
                        </table>

                    <%}%>                   

                        <div>&nbsp;</div>


                        <label class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</label>
                        
                        <%
                            String  corTable = ViewTender.corInfo(id, "tender");
                        %>

                        <%=corTable%>   <!--Load Corrigendum Table-->


                    </form>
                    
                </div>                                

            </div>

        </div>

        <div>
            <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                <tbody>
                    <tr>
                        <td class="ff" width="25%"></td>
                        <td width="25%"></td>

                        <td class="ff" width="26%"></td>
                        <td width="25%"></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </body>
</html>
