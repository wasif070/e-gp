<%-- 
    Document   : ViewAwardedContracts
    Created on : Aug 9, 2011, 2:52:46 PM
    Author     : rishita
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Awards Details</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">
                    <div class="pageHead_1">Contract Awards Details</div>
                    <%
                                String tenderId = "";
                                String thisContractNo = request.getParameter("contractNo");
                                String[] cNoArray = thisContractNo.split("<br>");
                                if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                                    tenderId = request.getParameter("tenderid");
                                }
                                String pkgLotId = "";
                                if (request.getParameter("pkgLotId") != null && !"".equals(request.getParameter("pkgLotId"))) {
                                    pkgLotId = request.getParameter("pkgLotId");
                                }
                                String userId = "";
                                if (request.getParameter("userId") != null && !"".equals(request.getParameter("userId"))) {
                                    userId = request.getParameter("userId");
                                }
                                
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> details = commonSearchDataMoreService.geteGPData("getAwardedContractsDetails", tenderId, pkgLotId,userId);
                                List<SPCommonSearchDataMore> detailsOne = commonSearchDataMoreService.geteGPData("getAwardedContractsDetailsTwo", tenderId, "0",userId);
                                List<SPCommonSearchDataMore> detailsThree = commonSearchDataMoreService.geteGPData("getAwardedContractsDetailsThree", tenderId, pkgLotId,userId);
                                List<SPCommonSearchDataMore> detailsFour = commonSearchDataMoreService.geteGPData("getAwardedContractsDetailsFour", tenderId, pkgLotId,userId);
                                List<SPCommonSearchDataMore> detailsPerSec = commonSearchDataMoreService.geteGPData("chkAwardedContractsPerSec", tenderId, pkgLotId,userId);
                                
                                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                                List<SPCommonSearchDataMore> tendDocument = creationService.getOpeningReportData("getTenderDocument", tenderId, pkgLotId, null);
                                String noaId = "";
                                if(!detailsOne.isEmpty()){
                                    noaId = detailsOne.get(0).getFieldName5();
                                }
                                List<SPCommonSearchDataMore> detailsDueDate = commonSearchDataMoreService.geteGPData("chkAwardedContractsDueTime", noaId);
                    %>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Hierarchy Node: </td>
                            <td width="70%"><%=details.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Agency:</td>
                            <td><%=details.get(0).getFieldName2()%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency Name:</td>
                            <td><%=details.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency Code:</td>
                            <td><%=details.get(0).getFieldName4()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency Dzongkhag / District:</td>
                            <td><%=details.get(0).getFieldName5()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Contract No. :</td>
                            <td><%out.print(cNoArray[1]);%></td>
                        </tr><tr>
                            <td class="ff">Contract Award for:</td>
                            <td><%=details.get(0).getFieldName6()%></td>
                        </tr>
                          <tr>
                            <td class="ff">Tender ID:</td>
                            <td><%=tenderId%></td>
                        </tr>
                        <tr>
                            <td class="ff">Invitation Reference No.:</td>
                            <td><%=details.get(0).getFieldName7()%></td>
                        </tr>
                    </table>
                    <div class="tableHead_22 t_space">KEY INFORMATION</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Procurement Method:</td>
                            <td width="70%"><%=details.get(0).getFieldName8()%></td>
                        </tr>
                    </table>
                    <div class="tableHead_22 t_space">FUNDING INFORMATION</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Budget Type:</td>
                            <td width="70%"><%if(details.get(0).getFieldName10().equalsIgnoreCase("Development")){out.print("Capital");}else if(details.get(0).getFieldName10().equalsIgnoreCase("Revenue")){out.print("Recurrent");}else{out.print(details.get(0).getFieldName10());}%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Source of Funds:</td>
                            <td width="70%"><%=details.get(0).getFieldName9()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Development Partner (if applicable):</td>
                            <td width="70%"><%=details.get(0).getFieldName11()==null?"NA":details.get(0).getFieldName11()%></td>
                        </tr>
                    </table>
                    <div class="tableHead_22 t_space">PARTICULAR INFORMATION</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Project/Activity Name :</td>
                            <td width="70%"><%=details.get(0).getFieldName12()==null?"NA":details.get(0).getFieldName12()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Tender Package No.:</td>
                            <td width="70%"><%=details.get(0).getFieldName13()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Tender Package Description:</td>
                            <td width="70%"><%=details.get(0).getFieldName14()%></td>
                        </tr>
                        <% int i=0; 
                        if(detailsOne.size()>0) {
                        for ( i=0; detailsOne.size()>i; i++){%>
                        <tr>
                            <td width="30%" class="ff"><%out.print(i+1);%>. Tender Lot No. and Description:</td>
                            <td width="70%"><%=detailsOne.get(i).getFieldName6()%> <span><b> and </b></span><%=detailsOne.get(i).getFieldName7()%></td>
                        </tr>
                        <%}}%>
<!--                        <tr>
                            <td width="30%" class="ff">Tender Lot Description:</td>
                            <td width="70%"><%--<%=detailsOne.get(0).getFieldName7()%>--%></td>
                        </tr>-->
                        <tr>
                            <td width="30%" class="ff">Date of Advertisement:</td>
                            <td width="70%"><%=details.get(0).getFieldName15()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Date of Letter of Acceptance:</td>
                            <td width="70%"><%=details.get(0).getFieldName16()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Date of Contract Signing:</td>
                            <td width="70%"><%=details.get(0).getFieldName17()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Proposed Date of Contract Completion:</td>
                            <td width="70%"><%=details.get(0).getFieldName18()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">No. of Tenders Sold:</td>
                            <td width="70%"><%=tendDocument.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">No. of Tenders Received:</td>
                            <td width="70%"><%=tendDocument.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Tenders Responsive:</td>
                            <td width="70%"><%=detailsThree.get(0).getFieldName1()%></td>
                        </tr>
                    </table>
                    <div class="tableHead_22 t_space">INFORMATION ON AWARD</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Brief Description of Contract:</td>
                            <td width="70%"><%=detailsOne.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Contract Value (Nu.):</td>
                            <td width="70%"><%=details.get(0).getFieldName19()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Company Name of Bidder/Consultant:</td>
                            <td width="70%"><%=detailsOne.get(0).getFieldName3()%></td>
                        </tr>
<!--                        <tr>
                            <td width="30%" class="ff">Location of Supplier/Contractor/Consultant:</td>
                            <td width="70%"><%=detailsFour.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Location of Delivery/Works/Consultancy:</td>
                            <td width="70%"><%=detailsOne.get(0).getFieldName4()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Was the Performance Security provided in due time?</td>
                            <td width="70%"><%=detailsPerSec.get(0).getFieldName1()%></td>
                        </tr>-->
                        <tr>
                            <td width="30%" class="ff">Was the Contract Singed in due time?</td>
                            <td width="70%"><%=detailsDueDate.get(0).getFieldName1()%></td>
                        </tr>
                    </table>
                    <div class="tableHead_22 t_space">AGENCY DETAILS</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="30%" class="ff">Name of Authorized Officer:</td>
                            <td width="70%"><%=detailsOne.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td width="30%" class="ff">Designation of Authorized Officer:</td>
                            <td width="70%"><%=details.get(0).getFieldName20()%></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
