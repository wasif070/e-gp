<%-- 
    Document   : ViewBOQForms
    Created on : Nov 15, 2011, 6:45:45 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPReportCommonData"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCommonService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%if(request.getParameter("formId")!=null){%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        
        <title>View Form </title>
        <jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        String isPDF = "false";
        String comNature = request.getParameter("nture");
        String fId = request.getParameter("formId");
        String bidderId = request.getParameter("userId");
        String tenId = request.getParameter("tenId");
        String commonprocMtd = request.getParameter("procMtd");
        if("true".equalsIgnoreCase(request.getParameter("isPDF"))){
            isPDF = "true";
        }
        String reqURL = request.getRequestURL().toString() ;
            String reqQuery = request.getQueryString() ;
            reqQuery = reqQuery.replaceAll("=", "@@");
            reqQuery = reqQuery.replaceAll("&", "!!");
        String folderName = pdfConstant.NOAFORMS;
        if(!("Services".equalsIgnoreCase(comNature) || "DPM".equalsIgnoreCase(commonprocMtd))){
        ReportCommonService reportCommonService = (ReportCommonService) AppContext.getSpringBean("ReportCommonService");
        List<SPReportCommonData> reportList = reportCommonService.returndata(Integer.parseInt(fId), Integer.parseInt(bidderId));
             //List<SPReportCommonData> reportList = reportCommonService.returndata(19, 7);
          if (!reportList.isEmpty()) {
            for(SPReportCommonData sprcd : reportList){%>
            <div id="print_area">
                <%out.println(sprcd.getReportHtml().replaceAll("\n", "<br />"));
            }
        }%>
            </div>
        <script>
            function printElem(options){
             $('#print_area').printElement(options);
}  
        </script>
           <%    }else{
            int negBidformId=0;
            CommonSearchService comcommonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> neglist=comcommonSearchService.searchData("GetNegFormDtl", tenId, bidderId,fId , null, null,null, null, null,null);
            
            for (SPCommonSearchData negoformdetails : neglist)
            {
               //out.println(negoformdetails.getFieldName5()+"=====test  ");
                    %>
                        <jsp:include page="ViewNegBidCompare.jsp" >
                            <jsp:param name="tenderId" value="<%=tenId%>" />
                            <jsp:param name="formId" value="<%=fId%>" />
                            <jsp:param name="bidId" value="0" />
                            <jsp:param name="uId" value="<%=bidderId%>" />
                            <jsp:param name="negId" value="<%=negoformdetails.getFieldName2()%>" />
                            <jsp:param name="lotId" value="0" />
                            <jsp:param name="type" value="negbiddata" />
                            <jsp:param name="action" value="View" />
                            <jsp:param name="negBidformId" value="<%=negoformdetails.getFieldName1()%>" />
                            <jsp:param name="acn" value="view" />
                        </jsp:include>
                        
                    <%
                }
                          }
        if (!(isPDF.equalsIgnoreCase("true"))) {%>
        </div>
<!--            <span style="float:right;" class="t_space">
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                    &nbsp; <a class="action-button-savepdf"
                                               href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=fId%>">Save As PDF</a>
            </span>-->
        <%}}else{
        String tenderID = request.getParameter("tenderId");
        String tenderSTDID = request.getParameter("tenderStdId");
        String sectionID = request.getParameter("sectionId");
        String prolID = request.getParameter("porlId");
%>
<%--?tenderStdId=<%=request.getParameter("tenderStdId")%>&tenderId=<%=request.getParameter("tenderId")%>&sectionId=<%=request.getParameter("sectionId")%>&porlId=<%=request.getParameter("porlId")%>"--%>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />

<jsp:include page="../../officer/TenderTDSView.jsp" >
    <jsp:param name="tenderId" value="<%=tenderID%>" />
    <jsp:param name="tenderStdId" value="<%=tenderSTDID%>" />
    <jsp:param name="sectionId" value="<%=sectionID%>" />
    <jsp:param name="porlId" value="<%=prolID%>" />
    <jsp:param name="isPDF" value="true" />
</jsp:include>
<%}%>

<script type="text/javascript">
    $(document).ready(function() {
        
                                    $("a[view='link']").each(function(){
                                        var alink = $(this).attr('href');
                                        $(this).attr('href',alink.replace('../','../../'));
                                    });
    });
 $(document).ready(function() {
    $("#print").click(function() {
        $("a[view='link']").each(function(){
            var temp = $(this).html().toString();
                $(this).parent().append("<span class='disp_link'>-</span>");
                $(this).hide();
                //$('.reqF_1').remove();
        })
        $('#hdnLink').hide();
        printElem({ leaveOpen: true, printMode: 'popup' });
        $("a[view='link']").each(function(){
            var temp = $(this).html().toString();
                $('.disp_link').remove();
                //$('.reqF_1').remove();
                $(this).show();
        })
        $('#hdnLink').show();
    });
 });

     

        </script>                             
    </body>
</html>