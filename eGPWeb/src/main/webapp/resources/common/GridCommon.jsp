<%--
    Document   : GridCommon
    Created on : Oct 23, 2010, 7:31:07 PM
    Author     : dixit
--%>
<%

            String caption = request.getParameter("caption");
            String url = request.getParameter("url");
            //String url = "/ManageSbDpAdminServlet?action=fetchData&partnerType=ScheduleBank";
            String colHeader = request.getParameter("colHeader");
            String colName[] = request.getParameter("colName").split(",");
            String sortColumn = request.getParameter("sortColumn");
            colHeader = "'" + colHeader.replaceAll(",", "','") + "'";

            String searchOp = "";
            if(request.getParameter("searchOpt")!=null)
            {
                searchOp =request.getParameter("searchOpt");
            }

            
            
            String width[] = new String[colName.length];
            if(request.getParameter("width") == null){
                int w = (100 / colName.length);
                for(int i=0;i<colName.length ; i++){
                    width[i] = w+"";
                }
            } else {
                width = request.getParameter("width").toString().split(",");
            }

            String aling[] = new String[colName.length];
            if(request.getParameter("aling") == null){
                int w = (100 / colName.length);
                for(int i=0;i<colName.length ; i++){
                    aling[i] = "left";
                }
            } else {
                aling = request.getParameter("aling").toString().split(",");
            }
            String searchable[];
             String soptOptions[] = new String[colName.length];
            if(request.getParameter("search") != null && request.getParameter("soptOptions") != null){
                searchable = request.getParameter("search").toString().split(",");
                soptOptions = request.getParameter("soptOptions").toString().split("@");
            } else {
                searchable = new String[colName.length];
                for(int i=0;i<searchable.length; i++){
                    searchable[i] = "false";
                }
            }
             
                    
            
%>

<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
<link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    jQuery().ready(function (){
        jQuery("#list").jqGrid({
            url:'<%=url%>',
            datatype: "xml",
            height: 250,
            colNames:[<%=colHeader%>],
            colModel:[
<%
                for (int i = 0; i < colName.length; i++) {

                    //if(i==2 || i==0){
                  out.print("{name:'" + colName[i] + "',index:'" + colName[i] +
                          "', align:'" + aling[i] +"', width:'" + width[i] + "%',sortable:"+sortColumn.split(",")[i]+
                          ", search: " + searchable[i]);
                    //}else{
                    ///    out.print("{name:'" + colName[i] + "',index:'" + colName[i] +
                     //     "', align:'" + aling[i] +"', width:'" + width[i] + "%',sortable:"+sortColumn.split(",")[i]+
                     //     ", search: " + searchable[i]);
                    //}
                  
                  if(searchable[i].equals("true")){
                     
                      out.print(", searchoptions: { sopt: [" +soptOptions[i]+ "] }");
                  }
                  out.print("},");
                }
%>
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                caption: "<%=caption%>",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                <%if(!searchOp.equals("false")){%>
            }).navGrid('#page',{edit:false,add:false,del:false});
            <%}else{%>
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});
            <%}%>
        });
</script>
<table id="list"></table>
<div id="page"></div>
