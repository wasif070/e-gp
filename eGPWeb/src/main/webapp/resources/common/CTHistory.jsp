<%-- 
    Document   : CTHistory
    Created on : Sep 27, 2011, 4:00:34 PM
    Author     : dixit
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Termination History</title>
    </head>
    <%
                pageContext.setAttribute("TSCtab", "4");
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    lotId = request.getParameter("lotId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                CmsContractTerminationService cmsContractTerminationService =
                        (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Contract Termination History
                                <span style="float: right; text-align: right;">
                                    <%if (userTypeId == 3) {%>
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/officer/TabContractTermination.jsp?tenderId=<%=tenderId%>" title="Contract Termination">Go Back</a>
                                    <%} else {%>
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/tenderer/TabContractTermination.jsp?tenderId=<%=tenderId%>" title="Contract Termination">Go Back</a>
                                    <%}%>
                                </span>
                            </div>
                            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
                            <%@include file="../../resources/common/TenderInfoBar.jsp" %>
                            <%@include file="../../resources/common/ContractInfoBar.jsp" %>
                            <%
                                        pageContext.setAttribute("tab", "14");
                            %>                                                        
                            <div class="tabPanelArea_1 t_space">
                                <div align="center">
                                    <%
                                                String strLotNo = "Lot No.";
                                                String strLotDes = "Lot Description";
                                                final int ZERO = 0;
                                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                                //Added for bug Id 5178
                                                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                                                List<SPCommonSearchDataMore> packageLotList = null;
                                                if("services".equalsIgnoreCase(procnature)){
                                                    strLotNo = "Package No.";
                                                    strLotDes = "Package Description";
                                                    packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                }else{
                                                    packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                }
                                                //Added for bug Id 5178
                                                for (SPCommonSearchDataMore lotList : packageLotList) {
                                    %>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                        <tr>
                                            <td width="20%"><%=strLotNo%></td>
                                            <td width="80%"><%=lotList.getFieldName3()%></td>
                                        </tr>
                                        <tr>
                                            <td><%=strLotDes%></td>
                                            <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                        </tr>
                                        <%
                                                                                                int contractSignId = 0;
                                                                                                Object[] object = issueNOASrBean.getDetails(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5())).get(ZERO);
                                                                                                if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                                                                                    contractSignId = issueNOASrBean.getContractSignId();
                                                                                                    CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                                                                                    cmsContractTerminationBean.setLogUserId(userId);
                                                                                                    /*TblCmsContractTermination tblCmsContractTermination =
                                                                                                    cmsContractTerminationBean.getCmsContractTerminationForContractSignId(contractSignId);*/
                                                                                                }
                                        %>
                                    </table>
                                    <div class="tableHead_1 t_space" align="left">View History</div>
                                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                                        <tr>
                                            <th style="text-align: left">No. of History</th>
                                            <!--                                                <th>Record</th>-->
                                        </tr>
                                        <%
                                                                                                List<TblCmsContractTermination> getsize = cmsContractTerminationService.getCTHistory(contractSignId);
                                                                                                if (!getsize.isEmpty()) {
                                                                                                    for (int i = 0; i < getsize.size(); i++) {
                                        %>
                                        <tr>
                                            <td class="t-align-left">
                                                <a href="ViewCTHistory.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&ContractTerminationId=<%=getsize.get(i).getContractTerminationId()%>">View History <%=(i + 1)%></a>
                                            </td>
<!--                                                <td class="t-align-center"><%if (i == 0) {
                                                            out.print("First Time Insertion");
                                                        } else if (i == getsize.size() - 1) {
                                                            out.print("Latest Edited");
                                                        } else {
                                                            out.print("edited");
                                                        }%></td>-->
                                        </tr>
                                        <%}
                                                                                                    }%>
                                    </table>
                                    <br>
                                    <%
                                                }
                                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(c_obj[7].toString()), "ContractId", EgpModule.Contract_Termination.getName(), "View Terminated Contract History", "");
                                    %>
                                </div>
                            </div>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>      
    </body>
</html>

