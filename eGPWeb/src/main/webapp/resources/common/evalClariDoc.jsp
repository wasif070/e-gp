<%--
    Document   : evalClariDoc
    Created on : Dec 30, 2010, 7:39:36 PM
    Author     : Administrator
for evealtion moduel file uploading downloading and cheking
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.validate.js"></script>

        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    if (session.getAttribute("userId") == null) {
                        response.sendRedirect("SessionTimedOut.jsp");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Clarification Document</title>


        <script type="text/javascript">
/*For validting txt file*/
            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
        
        <%
                    String formId = "1", st = "";
                    String tenderId = "1";
                    String docType = "";
                    String pageId = "";
                    st = request.getParameter("st");

                    if (request.getParameter("formId") != null) {
                        formId = request.getParameter("formId");
                    }
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    if (request.getParameter("pId") != null) {
                        pageId = request.getParameter("pId");
                    }
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    int evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tenderId").toString()));
        %>
    </head>
    <body>
        <%  
                    String useId = session.getAttribute("userId").toString();
                    boolean forCompany = false;
                    if (request.getParameter("uId") != null) {
                        useId = request.getParameter("uId");
                            if(!useId.equalsIgnoreCase(session.getAttribute("userId").toString())){
                                forCompany = true;
                            }
                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%
                        pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Clarification Document
                    <%if (pageId.equalsIgnoreCase("office")) {%>
                    <span style="float:right;"><a href="../../officer/SeekEvalClari.jsp?tenderId=<%=tenderId%>&st=<%=st%>&uId=<%=useId%>" class="action-button-goback">Go Back</a></span>
                    <%} else {%>
                    <span style="float:right;"><a href="../../tenderer/EvalBidderClari.jsp?tenderId=<%=tenderId%>&uId=<%=useId%>" class="action-button-goback">Go Back</a></span>
                    <%}%>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->



                <%@include file="TenderInfoBar.jsp" %>


                <% if (forCompany) {
                                for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", useId, "0")) {
                %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="2" class="t_align_left ff">Company Details</th>
                    </tr>
                    <tr>
                        <td width="22%" class="t-align-left ff">Company Name :</td>
                        <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                    </tr>
                </table>
                <%
                                    }
                                }%>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/ServletEvalClariDocs" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                    <input type="hidden" name="formId" value="<%=formId%>"/>
                    <input type="hidden" name="uId" value="<%=useId%>"/>
                    <input type="hidden" name="st" value="<%=st%>"/>
                     <input type="hidden" name="evalCount" value="<%=evalCount%>"/>



                    <%
                                if (request.getParameter("fq") != null) {
                                    if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Sucessfully</div>
                    <%} else {%>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                    <%
                                    }
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg"  style="margin-top: 10px;">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>

                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" onclick="return checkfile()" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            
                            <%
                            String s_userType = "officer";
                                if("2".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
                                s_userType = "tenderer";
                               }
                                TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster(s_userType);
                            %>
                            <td class="t-align-left">
                                Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%=tblConfigurationMaster.getAllowedExtension()%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
/*For listing of data*/
                                int docCnt = 0;
                                CommonSearchService CommonSD = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                String userid = session.getAttribute("userId").toString();
                                boolean flag = false;
                                for (SPCommonSearchData spcsd : CommonSD.searchData("EvalClariDocs", tenderId, formId, useId, userid, String.valueOf(evalCount), null, null, null, null)) {
                                    docCnt++;
                                    // HIGHLIGHT THE ALTERNATE ROW
                                    if(Math.IEEEremainder(docCnt,2)==0) {
                    %>
                     <tr class="bgColor-Green">
                     <% } else { %>
                    <tr>
                    <% } %>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=spcsd.getFieldName1()%></td>
                        <td class="t-align-left"><%=spcsd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(spcsd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletEvalClariDocs?docName=<%=spcsd.getFieldName1()%>&docSize=<%=spcsd.getFieldName3()%>&tenderId=<%=tenderId%>&formId=<%=formId%>&funName=download&fs=<%=spcsd.getFieldName7()%>&docType=<%=spcsd.getFieldName5()%>&evalCount=<%=evalCount%>" title="Download"><img src="../images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <a onclick="return window.confirm('Do you really want to delete this file?');" href="<%=request.getContextPath()%>/ServletEvalClariDocs?&docName=<%=spcsd.getFieldName1()%>&st=<%=st%>&docId=<%=spcsd.getFieldName4()%>&tenderId=<%=tenderId%>&formId=<%=formId%>&funName=remove&fs=<%=spcsd.getFieldName7()%>&docType=<%=spcsd.getFieldName5()%>&uId=<%=useId%>&evalCount=<%=evalCount%>"><img src="../images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                    </tr>
                    <%   if (spcsd != null) {
                                        spcsd = null;
                                    }
                                }//Officer Loop Ends here
%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>

                <div>&nbsp;</div>
                <div align="center"><%@include file="Bottom.jsp" %></div>
            </div>
            <!--Dashboard Footer End-->
        </div>
    </body>
<script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
</html>
<%

            if (CommonSD != null) {
                CommonSD = null;
            }
            if (tblConfigurationMaster != null) {
                tblConfigurationMaster = null;
            }

%>
