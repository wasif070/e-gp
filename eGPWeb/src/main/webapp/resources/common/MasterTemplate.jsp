<%-- 
    Document   : MasterTemplate
    Created on : Oct 21, 2010, 6:04:43 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="Include/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="Left.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>

