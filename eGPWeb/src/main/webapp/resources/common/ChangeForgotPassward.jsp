<%-- 
    Document   : ChangeForgotPassward
    Created on : Jun 24, 2011, 5:07:31 PM
    Author     : dixit
--%>


<%@page import="com.cptu.egp.eps.model.table.TblForgotPassword"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ForgotPasswordService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="java.io.IOException"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="java.security.spec.InvalidKeySpecException"%>
<%@page import="java.security.InvalidAlgorithmParameterException"%>
<%@page import="javax.crypto.NoSuchPaddingException"%>
<%@page import="java.security.InvalidKeyException"%>
<%@page import="javax.crypto.BadPaddingException"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="javax.crypto.spec.PBEParameterSpec"%>
<%@page import="javax.crypto.SecretKey"%>
<%@page import="javax.crypto.SecretKeyFactory"%>
<%@page import="javax.crypto.spec.PBEKeySpec"%>
<%@page import="sun.misc.BASE64Decoder"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
            Logger LOGGER = Logger.getLogger("ChngPassword.jsp");
            Object mailObject = session.getAttribute("emailId");
            Object previous = session.getAttribute("previous");

            String mailId = "";
            String action = "";
            if (previous == null) {
                if(mailObject==null || "".equals(mailObject)){
                    String encrypt = "";
                    if(request.getParameter("mId")!=null){
                        String data = request.getQueryString();
                        encrypt = data.substring(4,data.length());
                        data=null;
                    }
                    String password="egp@forgotpwd";
                     try {
                        String salt = encrypt.substring(0, 12);
                        String ciphertext = encrypt.substring(12, encrypt.length());
                        BASE64Decoder decoder = new BASE64Decoder();
                        byte[] saltArray = decoder.decodeBuffer(salt);
                        byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
                        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
                        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
                        SecretKey key = keyFactory.generateSecret(keySpec);
                        PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
                        Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                        cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
                        byte[] plaintextArray = cipher.doFinal(ciphertextArray);
                        mailId=  new String(plaintextArray);
                    } catch (IllegalBlockSizeException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (BadPaddingException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (InvalidKeyException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (InvalidAlgorithmParameterException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (NoSuchPaddingException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (InvalidKeySpecException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (NoSuchAlgorithmException ex) {
                        LOGGER.error("Error API : " + ex);
                    } catch (IOException ex) {
                        LOGGER.error("Error API : " + ex);
                    }
                }else{
                    mailId = mailObject.toString();
                }
                action = "forgotPassword";
            } else {
                mailId = mailObject.toString();
                action = "resetPassword";
            }
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Password</title>
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

    </head>
    <body>
        <%
            String userId="0";
            if(session.getAttribute("userId")!=null){
                userId=session.getAttribute("userId").toString();
            }
            LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(userId);
            userId=null;
            String msg = request.getParameter("msg");
            if (msg != null && msg.equalsIgnoreCase("success")){
                session.removeAttribute("emailId");
        %>
        <form name="frmCngPwd" id="frmCngPwd" method="post"></form>
        <script src="../../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            jAlert('Your Password changed Successfully','Change Password', function(r) {
                if(r){
                    document.getElementById("frmCngPwd").action = "<%=request.getContextPath()%>/Index.jsp";
                    document.getElementById("frmCngPwd").submit();
                }
            });
        </script>
        <%
            }else{
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="Top.jsp" ></jsp:include>
                <script src="../../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td>
                            <!--Page Content Start-->
                            <%
                            String s_serverMsg  = "";
                                    if(mailId!=null && !"".equalsIgnoreCase(mailId)){
                                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                                    s_serverMsg = commonService.verifyMail(mailId);
                                   }
                            if("Ok".equalsIgnoreCase(s_serverMsg) || "".equalsIgnoreCase(s_serverMsg)){%>
                            <div>
                                <div class="t_space"><div class="responseMsg errorMsg">e-mail ID is not registered with e-GP</div></div>
                            </div>
                            <%
                            }else{
                            %>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">Change Password</div>
                                        <%
                                            if (msg != null && msg.equals("fail")) {
                                        %>
                                                <div class="t_space"><div class="responseMsg errorMsg">Changes Password Failed</div></div>
                                        <%
                                            }
                                        %>
                                        <%
                                            String requestHascode ="";
                                            if(request.getParameter("mId")!=null)
                                            {
                                              String str = request.getQueryString();
                                              requestHascode = str.substring(4, str.length());                                                                                            
                                            }
                                            ForgotPasswordService forgotPasswordService = (ForgotPasswordService) AppContext.getSpringBean("ForgotPasswordService");
                                            List<TblForgotPassword> list = forgotPasswordService.getForgotData(mailId);
                                            if(!list.isEmpty())
                                            {   System.out.println("request hash :: "+requestHascode);System.out.println("db hash :: "+list.get(0).getUserHash());
                                                String databaseHashcode = list.get(0).getUserHash();                                                
                                                if(databaseHashcode.equals(requestHascode))
                                                {
                                        %>
                                        <form id="frmChangePassword" name="ChangePassword" method="post" action='<%=request.getContextPath()%>/LoginSrBean?action=<%=action%>'>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <%if("forgotPassword".equals(action)){%>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td><%=mailId%></td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff" width="15%">New Password : <span>*</span></td>
                                                    <td width="85%">
                                                        <input class="formTxtBox_1" type="password" id="txtNewPass" name="newPass" style="width : 200px" onblur="chkPasswordMatches();"/>
                                                        <div id="pwdMsg" class="reqF_1"></div>
                                                        <span id="passnote" class="formNoteTxt">
                                                        <br/>(Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added.)</span>
                                                        <input type="hidden" name="mailId" value="<%=mailId%>" id="hdnEmailId"/>
                                                        <input type="hidden" name="mId" value="<%=request.getParameter("mId")%>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="password" id="txtConfPassword" name="confPassword" style="width : 200px" onblur="chkPasswordMatches();" onpaste="return errorPaste();"/>
                                                        <div id="confMsg" style="font-weight: bold;color: red;"></div>
                                                         <div style="color: green" id="pwdMatchMsg"></div>
                                                    </td>
                                                </tr>
                                                <%if (previous != null) {
                                                    if(!previous.toString().trim().equalsIgnoreCase("reset")){%>
                                                <tr>
                                                    <td class="ff">Hint Question : <span>*</span></td>
                                                    <td>
                                                        <select class="formTxtBox_1" name="hintQuestion" id="cmbHintQuestion" onchange="showHide()">
                                                            <%for(SelectItem hintQus : loginMasterSrBean.getHintQueList()){%>
                                                                <option value="<%=hintQus.getObjectId()%>"><%=hintQus.getObjectValue()%></option>
                                                            <%}%>
                                                                <option value="other">Create Your own Question</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr id="trOtherHint" style="display:none">
                                                    <td class="ff">Create your own hint question : <span>*</span></td>
                                                    <td>
                                                        <textarea cols="20" rows="3" id="txtOtherHint" name="otherHint"  style="width : 200px"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Hint Answer : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtHintAns" name="hintAns"  style="width : 200px"/>
                                                    </td>
                                                </tr>
                                                <% }
                                                    }%>
                                                <tr>
                                                    <td> </td>
                                                    <td colspan="2" align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="submit" id="btnAdd" value="Submit" disabled/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                        <%
                                            }
                                         }else
                                         {
                                        %>                                                                                        
                                            <table width="100%" cellpadding="0" cellspacing="10" class="t_space">
                                                <tr>
                                                    <td height="230" align="center" valign="middle" style="font-size:16px; color:#6e6e6e; font-weight:bold;">
                                                        <img src="<%= request.getContextPath()%>/resources/images/Dashboard/Button_Warning.png" width="128" height="128" style="margin-bottom:10px;" /> <br />
                                                        Page not found<br/>
                                                        <div class="reason">You have already changed your password using this URL. You can't use this URL now </div>
                                                    </td>
                                                </tr>
                                            </table>                                            
                                        <%
                                         }
                                         //forgotPasswordService.deleteRandomeCode(mailId);
                                        %>
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Page Content End-->
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%
            }
        %>

<script type="text/javascript">
            $j(document).ready(function() {
                $j("#btnAdd").attr('disabled', false);
                // jQuery.validator.addMethod("noSpace", function(value, element) {
                //   return value.indexOf(" ") < 0 && value != "";
                //}, "<div class='reqF_1'>No space allowed in Password</div>");

                // jQuery.validator.addMethod('alphanum', function (value) {
                // return /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value);
                // }, 'Password should be Alphanumeric.');

                 $j("#frmChangePassword").validate({
                    rules: {
                        newPass:{required: true, minlength: 8, maxlength:25, alphaForPassword:true},
                        confPassword:{required:true,equalTo:"#txtNewPass"},
                        //,ConfPassword:{required:true,equalTo:"#txtNewPass"}
                        hintQuestion:{required:true},
                        hintAns:{required:true,maxlength:50}
                    },
                    messages: {
                        newPass:{required:"<div class='reqF_1'>Please enter Password</div>",
                            minlength:"<div class='reqF_1'>Please enter at least 8 characters required and must contain both alphabets and numbers</div>",
                            maxlength:"<div class='reqF_1'>Please enter no more than 25 characters.</div>",
                            alphaForPassword:"<div class='reqF_1'>Please enter only alphanumeric value.</div>"
                            //spacevalidate:"<div class='reqF_1'>Spaces are not allowed.</div>"
                        },
                        confPassword:{required: "<div class='reqF_1'>Please enter Confirm Password</div>",
                            equalTo: "<div class='reqF_1' id='msgPwdNotMatch'>Password does not match. Please try again</div>"
                        },
                        hintQuestion:{required:"<div class='reqF_1'>Please select hint Question</div>"},
                        hintAns:{required:"<div class='reqF_1'>Please enter Hint Answer.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 50 Characters are to be allowed.</div>"}
                    }
                }
            );
            });

</script>
<script type="text/javascript">
        function chkPasswordMatches(){
            var objPwd = document.getElementById("txtNewPass");
            var objConfirmPwd = document.getElementById("txtConfPassword");
            if(objPwd != null && objConfirmPwd != null){
                if($j.trim(objPwd.value) == "" || $j.trim(objConfirmPwd.value) == ""){}else{
                    var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
                    if(objPwd.value == objConfirmPwd.value){
                        var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
                        if(msgPwdNotMatch!=null){
                            if(msgPwdNotMatch.innerHTML == "Password does not match. Please try again"){
                                msgPwdNotMatch.style.diplay = "none";
                                msgPwdNotMatch.innerHTML = "";
                            }
                        }
                        msgPwdMatchObjDiv.innerHTML = "<b>Password Matches</b>";
                    }else{
                        msgPwdMatchObjDiv.innerHTML = "";
                    }
                }
            }
        }
        $j(function() {
            $j('#txtNewPass').blur(function() {
                //var passRE=/^[\sa-zA-Z0-9\@\#\$\%\*\_\.\'\-]+$/;
                //if(passRE.test($("#txtNewPass").val())) {
                    $j('span.#pwdMsg').html("Checking for Password...");
                    if($j("#txtNewPass").val().length >= 8 && $j("#txtNewPass").val().length <= 25){
                        $j.post("<%=request.getContextPath()%>/ForgotPasswordSrBean", {emailId: $j('#hdnEmailId').val(),newPass:$j('#txtNewPass').val(),funName:'chkPwd'},  function(j){
                            if(j.toString().length>0)
                                $j("#pwdMsg").html(j);
                            else
                                $j("#pwdMsg").html(' ');
                        });
                    }
                    //else
                    //{
                        //if( $(pwdMsg.innerHTML==""))
                          //  $("#pwdMsg").html('Password should be of Min 8 & Max 25 Characters');
                    //}
                //}
                //else
                //{
                 //    if( $(pwdMsg.innerHTML=="")){
                         //$("#pwdMsg").html('Please Enter Valid password');
                    //}
                //}
            });
        });
</script>
<script type="text/javascript">
            $j(function() {
                $j('#frmChangePassword').submit(function() {
                    //|| $('#confMsg').html().length != 1
                    if($j("#pwdMsg").html().length != 1 )
                    {
                        return false;
                    }

                });
            });
            $j(function showHide(){
                if($j('#cmbHintQuestion').val() == "other"){
                $j('#trOtherHint').show();
                }else{
                    $j('#trOtherHint').hide();
                }


            });
</script>
<script type="text/javascript">
function errorPaste(){
             jAlert("Copy and Paste not allowed.",'Change Password', function(RetVal) {
            });
            return false;
}
</script>
</body>
</html>
