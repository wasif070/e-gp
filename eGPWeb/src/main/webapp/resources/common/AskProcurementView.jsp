<%-- 
    Document   : AskProcurementView
    Created on : Jan 12, 2011, 1:21:31 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblAskProcurement"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AskProcurementSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ask Procurement Expert</title>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
</head>
<body>
<div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
               <%-- <jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <%--<jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>--%>
                    <td class="contentArea">

                        <div class="pageHead_1">Ask Procurement Expert</div>
                        <div>&nbsp;</div>
                        

  <table width="100%" cellspacing="0" class="tableList_1 t_space">
      <%
                 AskProcurementSrBean askProcurementSrBean = new AskProcurementSrBean();
                 String msg = "";
                 int id = Integer.parseInt(request.getParameter("id"));
                 List<TblAskProcurement> lst = askProcurementSrBean.getAskProcurement(id);
                 if(!lst.isEmpty()){
                     Iterator it = lst.iterator();
                     SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                     while(it.hasNext()){
                         TblAskProcurement tblAskProcurement = (TblAskProcurement)it.next();
                         Date postedDate = tblAskProcurement.getPostedDate();
                         Date answerDate = tblAskProcurement.getAnswerDate();
                        


      %>
          <tr>
                <td width="11%" class="t-align-left ff">Question :</td>
                <td width="89%" class="t-align-left"><%=tblAskProcurement.getQuestion()%></td>
          </tr>
            <tr>
              <td class="t-align-left ff">Posted Date :</td>
              <td class="t-align-left"><%=sdf.format(postedDate)%></td>
            </tr>
            <tr>
              <td class="t-align-left ff">Answer :</td>
              <td class="t-align-left"><%=tblAskProcurement.getAnswer()%></td>
            </tr>
            <tr>
              <td class="t-align-left ff">Answer  Date :</td>
              <td class="t-align-left"><%=tblAskProcurement.getAnswerDate()%></td>
            </tr>

            <%
                            }
                    }else
                        msg ="No Record Found";
            %>
        </table>
                    </td>
                </tr>
            </table>
      
            <div>&nbsp;</div>
           <%-- <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>--%>
        </div>
    </body>
</html>