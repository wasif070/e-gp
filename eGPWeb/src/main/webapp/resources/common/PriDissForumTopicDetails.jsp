<%--
    Document   : PriDissForumTopicDetails
    Created on : Mar 14, 2012, 12:32:44 PM
    Author     : dipal.shah
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.PrivateDissForumServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicReplyDocument"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicReply"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblPriTopicMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.PrivateDissForumService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <title>View Topic Details</title>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <script src="../js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/form/CommonValidation.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
         <%
                            String topicId = "";
                            if(request.getParameter("topicId")!=null)
                            {
                                topicId = request.getParameter("topicId");
                            }
                            String tenderId = "";
                            if(request.getParameter("tenderId")!=null)
                            {
                                tenderId = request.getParameter("tenderId");
                            }
                            int userId=0;
                            if(session.getAttribute("userId")!= null)
                            {
                                userId=Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            String type="all";
                            if(request.getParameter("type")!=null && !request.getParameter("type").equalsIgnoreCase(""))
                            {
                                type=request.getParameter("type");
                            }
                            String title="View Topic Details";
                            if(type!= null && type.equalsIgnoreCase("unread"))
                                title="View Unread Topic Details";
                            String uploadedBy="";
                            int uploadUserTypeId=0;
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            Map obUserName=new HashMap();

                            PrivateDissForumServiceImpl privateDissForumService = (PrivateDissForumServiceImpl) AppContext.getSpringBean("PrivateDissForumService");
                            privateDissForumService.setLogUserId(userId+"");
                            List<TblPriTopicMaster> listTopic = privateDissForumService.getTopic(Integer.parseInt(topicId));
                            List<TblPriTopicReplyDocument> listDocument=null;

                            int j=0;
                            List<TblPriTopicReply> getTReply = privateDissForumService.getReply(Integer.parseInt(topicId), 1);

                            for (int i = 0; i < getTReply.size(); i++)
                                {
                                    if(type != null && type.equalsIgnoreCase("unread") && ( getTReply.get(i).getHasSeen()== 1 || userId == getTReply.get(i).getPostedBy()))
                                        continue;
                                    j++;
                                }
                            
        %>
    <body>
        <div class="dashboard_div">
            <div class="dashboard_div">

                <%@include  file="AfterLoginTop.jsp" %>
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1"><%=title%>
                            <span class="c-alignment-right"><a href="PriDissForumTopicList.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a></span> </div>
                            <%
                                if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("true"))
                                {
                                %>
                                    <div id="succMsg" class="responseMsg successMsg">Topic Respond Successfully.</div>
                                <%
                                }
                                if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("false"))
                                {
                                %>
                                    <div id="errMsg" class="responseMsg errorMsg">Some Problem occur during Topic Respond.</div>
                                <%
                                }
                                %>
                                <BR/>
                                <table width="100%">
                                    <tr>
                                        <td class="formStyle_1 ff t_space">
                                            Response marked with (<span class="mandatory">*</span>) are unread. 
                                        </td>
                                        <td style="text-align: right;">
                                            <B>Total
                                                <% if(type!=null && type.equalsIgnoreCase("unread"))
                                                     out.println("Unread ");
                                                 %>
                                                Post &nbsp;(<%=j%>)</B>
                                        </td>
                                    </tr>
                                </table>

                    

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">

                        <tr class="bgColor-Green">
                            <td width="30%">
                               <div class=" ff">
                                    Posted By:
                                    <span style="font-size:11px; font-weight:normal;" >
                                        <%
                                            uploadedBy="";
                                            if(listTopic!=null && !listTopic.isEmpty() && obUserName != null && !obUserName.containsKey(listTopic.get(0).getPostedBy()))
                                            {
                                                uploadUserTypeId=commonService.getUserType(listTopic.get(0).getPostedBy()).get(0).getTblUserTypeMaster().getUserTypeId();
                                                uploadedBy=commonService.getUserName(listTopic.get(0).getPostedBy(), uploadUserTypeId);
                                                if(uploadedBy != null && uploadedBy.length()>0 && uploadedBy.indexOf("%$") != -1)
                                                {
                                                    uploadedBy = uploadedBy.substring(uploadedBy.indexOf("%$")+2,uploadedBy.length());
                                                }
                                                obUserName.put(listTopic.get(0).getPostedBy(), uploadedBy);
                                            }
                                            else
                                            {
                                                uploadedBy = obUserName.get(listTopic.get(0).getPostedBy()).toString();
                                            }
                                                out.println(uploadedBy);
                                        %>
                                    </span>
                                    <BR/>
                                    Date Time:
                                    <span style="font-size:11px; font-weight:normal;">
                                    <%
                                        if(listTopic!=null && !listTopic.isEmpty())
                                        {
                                             out.print(DateUtils.gridDateToStr(listTopic.get(0).getPostedDate()));
                                        }
                                    %>
                                    </span>
                               </div>
                            </td>
                            <td width="70%" class="ff">
                                <table width="100%" class="no-border">
                                    <tr>
                                        <td width="40%">
                                            <a href="PriDissForumViewTopicReply.jsp?tenderId=<%=tenderId%>&topicId=<%=topicId%>&flag=true&type=<%=type%>">
                                                <font color="black">
                                                    <%
                                                        if(listTopic!=null && !listTopic.isEmpty())
                                                        {
                                                             out.print((listTopic.get(0).getTopic()));
                                                        }
                                                    %>
                                                </font>
                                            </a>
                                        </td>
                                        <td style="text-align: right;" width="40%">
                                            <%
                                                listDocument=privateDissForumService.getDocumentDetail(Integer.parseInt(topicId), "Topic");
                                                if(listDocument != null && listDocument.size()>0)
                                                {
                                                    %>
                                                    <%=listDocument.get(0).getDocName()%>&nbsp;<a href="<%=request.getContextPath()%>/PriDissForumServlet?funName=downloadDocument&tenderId=<%=tenderId%>&uploadedBy=<%=listDocument.get(0).getUploadedBy()%>&docName=<%=listDocument.get(0).getDocName()%>&docSize=<%=listDocument.get(0).getDocSize()%>"><img src="../images/Dashboard/Download.png" alt="Download" /></a>
                                                    <%
                                                }
                                            %>
                                        </td>
                                        <td style="text-align: right;" width="20%">
                                            <% //if (type != null && !type.equalsIgnoreCase("unread"))
                                                {
                                            %>
                                                <a href="PriDissForumPostReply.jsp?topicId=<%=topicId%>&tenderId=<%=tenderId%>" class="action-button-add">Post Response</a>
                                             <%
                                                }
                                             %>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                                        
                        <div id="tmpArea" style="display: none;">
                        
                        <textarea cols="80" id="txtarea_replydetails" name="txtarea_replydetails"  rows="10">
                            
                        </textarea>
                        <script type="text/javascript">
                            CKEDITOR.replace('txtarea_replydetails',
                            {
                                fullPage : false
                            });
                        </script>
                        </div>

                        <%
                            if(getTReply!=null && !getTReply.isEmpty())
                            {
                                j=0;
                                for (int i = 0; i < getTReply.size(); i++)
                                {
                                    if(type != null && type.equalsIgnoreCase("unread") && ( getTReply.get(i).getHasSeen()== 1 || userId == getTReply.get(i).getPostedBy()))
                                        continue;

                        %>
                        <tr <%if(j%2==0){%>class="bgColor-white"<%}else{%>class="bgColor-Green"<%} j++;%> >
                            <td >
                               <div class=" ff" id="reply">

                                    Respond By:
                                    <span style="font-size:11px; font-weight:normal;">
                                    <%
                                            uploadedBy="";
                                            if(obUserName != null && !obUserName.containsKey(getTReply.get(i).getPostedBy()))
                                            {
                                                uploadUserTypeId=commonService.getUserType(getTReply.get(i).getPostedBy()).get(0).getTblUserTypeMaster().getUserTypeId();
                                                uploadedBy=commonService.getUserName(getTReply.get(i).getPostedBy(), uploadUserTypeId);
                                                if(uploadedBy != null && uploadedBy.length()>0 && uploadedBy.indexOf("%$") != -1)
                                                {
                                                    uploadedBy = uploadedBy.substring(uploadedBy.indexOf("%$")+2,uploadedBy.length());
                                                }
                                                obUserName.put(getTReply.get(i).getPostedBy(), uploadedBy);
                                            }
                                            else
                                            {
                                                uploadedBy = obUserName.get(getTReply.get(i).getPostedBy()).toString();
                                            }
                                            out.println(uploadedBy);
                                    %>
                                    </span>
                                    <%
                                    if(getTReply.get(i).getHasSeen()== 0 && loguserId != getTReply.get(i).getPostedBy())
                                    {
                                    %>
                                    <span class="mandatory">*</span>
                                    <%
                                    }
                                    %>
                                    <BR/>
                                    Date Time:
                                    <span style="font-size:11px; font-weight:normal;">
                                    <%
                                             out.print(DateUtils.gridDateToStr(getTReply.get(i).getPostedDate()));
                                    %>
                                    </span>
                               </div>
                            </td>
                            <td width="70%" class="ff">
                                <table width="100%" class="no-border">
                                    <tr>
                                        <td width="40%" nowrap>
                                            
                                            <div id="txtarea_<%=i%>" style="display: none;">
                                              <%=getTReply.get(i).getDescription()%>
                                            </div>

                                            <script type="text/javascript">
                                                document.getElementById('txtarea_replydetails').value = document.getElementById('txtarea_<%=i%>').innerHTML;
                                            </script>

                                            <a href="PriDissForumViewTopicReply.jsp?tenderId=<%=tenderId%>&topicId=<%=topicId%>&topicReplyId=<%=getTReply.get(i).getTopicReplyId()%>&type=<%=type%>">
                                                <div id="t_<%=i%>">
                                                </div>

                                                <%
                                                /*
                                                     if(getTReply.get(i).getDescription()!= null && getTReply.get(i).getDescription().length() > 20)
                                                     {
                                                        out.println(getTReply.get(i).getDescription().substring(0,20)+".....");
                                                     }
                                                     else
                                                     {
                                                        out.println(getTReply.get(i).getDescription()+"......");
                                                     }
                                                */
                                                %>
                                            </a>

                                            <script type="text/javascript">
                                                var data= $.trim(CKEDITOR.instances.txtarea_replydetails.getData().replace(/<[^>]*>|\s/g,' '));
                                                if(data.length > 25)
                                                   {
                                                       document.getElementById("t_<%=i%>").innerHTML=data.toString().substr(0,25)+".....";
                                                   }
                                                else
                                                   {
                                                       document.getElementById("t_<%=i%>").innerHTML=data;
                                                   }
                                            </script>

                                        </td>
                                        <td style="text-align: right;" width="40%">
                                            <%
                                                listDocument=privateDissForumService.getDocumentDetail(getTReply.get(i).getTopicReplyId(), "Response");

                                                if(listDocument != null && listDocument.size()>0)
                                                {
                                                    %>
                                                    <%=listDocument.get(0).getDocName()%>&nbsp;<a href="<%=request.getContextPath()%>/PriDissForumServlet?funName=downloadDocument&tenderId=<%=tenderId%>&uploadedBy=<%=listDocument.get(0).getUploadedBy()%>&docName=<%=listDocument.get(0).getDocName()%>&docSize=<%=listDocument.get(0).getDocSize()%>"><img src="../images/Dashboard/Download.png" alt="Download" /></a>
                                                    <%
                                                }
                                            %>
                                        </td>
                                        <td  width="20%">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%
                                }
                            }
                            obUserName = null;
                        %>

                    </table>

                    <div>&nbsp;</div>

                    <!--Dashboard Content Part End-->


                </div>
            </div>
            <!--Dashboard Footer Start-->

            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
