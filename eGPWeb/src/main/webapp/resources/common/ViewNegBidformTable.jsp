<%--
    Document   : ViewNegBidFormTable.jsp
    Created on : Nov 18, 2010, 3:08:48 PM
    Author     : Sanjay,Dipal
    Modify on  : Dec 14 2011 
    Bug id     : 4963
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderBidDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationProcssSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<div style="overflow: auto; min-width: 964px;">
    <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
    <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

<%

   
    int userId = 0;
    if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
        userId = Integer.parseInt(request.getParameter("uId"));
    }
    //out.println("<BR>user id="+userId+"<BR>");
   
%>
<%
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;
    int cellId = 0;
    int formId = 0;
    int tenderId = 0;
    int negBidformId=0;
    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";
   
%>
<%
   
    if(request.getParameter("negBidformId")!=null && !"".equalsIgnoreCase(request.getParameter("negBidformId"))){
        negBidformId = Integer.parseInt(request.getParameter("negBidformId"));
    }
    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
     if(request.getParameter("formId")!=null && !"".equalsIgnoreCase(request.getParameter("formId"))){
        formId = Integer.parseInt(request.getParameter("formId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }
    
    String action = "";
    if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
         action = request.getParameter("action");
    }

  
    boolean isView = true;
    

      boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }
    
      
  
 
%>
<tbody id="MainTable<%=tableId%>">
    <input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
    <input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">


<%
        short fillBy[] = new short[cols];
        int arrColId[] = new int[cols];
        String arrColType[] = new String[cols];

        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }
      

        ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getNegTableColumns(tableId,bidId, true).listIterator();
        ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();
        int grandTotalCell = tenderBidSrBean.getCellForTotalWordsCaption(formId, tableId);

        ListIterator<CommonFormData> tblBidTableId = null;
        ListIterator<CommonFormData> tblBidData = null;
        ListIterator<CommonFormData> tblBidCellData = null;

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();
        
        String type = "";
        if(request.getParameter("type")!= null && !"".equalsIgnoreCase(request.getParameter("type"))){
               type = request.getParameter("type");
        }
        
        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
        listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId);

         while(tblFormulaDtl.hasNext()){
            CommonFormData formulaData = tblFormulaDtl.next();
            if(formulaData.getFormula().contains("TOTAL")){
                isTotal = 1;
            }
        }
        while(tblFormulaDtl.hasPrevious()){
            tblFormulaDtl.previous();
        }
        if(isView){
           
            
            if("negbiddata".equalsIgnoreCase(type))
            {
                tblBidTableId = tenderBidSrBean.GetNegBidTableId(negBidformId, tableId).listIterator();
                tblBidData = tenderBidSrBean.getNegBidSrcData(negBidformId).listIterator();
               // tblBidData = tenderBidSrBean.getNegBidPlainData(bidId,userId).listIterator();
            }else if("biddata".equalsIgnoreCase(type)){
                tblBidTableId = tenderBidSrBean.getBidTableId(bidId, tableId,userId).listIterator();
                tblBidData = tenderBidSrBean.getNegBidPlainData(bidId,userId).listIterator();
            }
           // out.println("nego bid data size ="+tenderBidSrBean.getNegBidSrcData(bidId, userId).size());
           // out.println("bid data size ="+tenderBidSrBean.getNegBidPlainData(bidId, userId).size());
           

            while(tblBidTableId.hasNext()){
                //tblBidTableId.next();
                CommonFormData bidTableIdDt = tblBidTableId.next();
                bidTableIds.append(bidTableIdDt.getBidTableId() + " , ");
                cntRow++;
            }
            if(tblBidTableId.hasPrevious())
                tblBidTableId.previous();

                if(tblBidTableId.hasNext()){
                    tblBidTableId.next();
                    if(rows > 1 && isTotal == 1){
                        editedRowCount = (cntRow * (rows-1)) + 1;
                        //out.println("<BR>cntrow="+cntRow+" rows="+rows);
                    }else{
                        editedRowCount = (rows*cntRow);
                    }
                }
                TotalBidCount= cntRow;
         }else{
            cntRow = 1;
         }

         int noOfRowsWOGT = 0;

         if(rows > 1 && isTotal == 1){
             noOfRowsWOGT = rows -1;
         }else{
             noOfRowsWOGT = rows;
         }
%>
            
        
            <input type="hidden" value="<%=editedRowCount%>" name="eRowCount<%=tableId%>" id="editRowCount">
<%
                while(tblBidTableId.hasPrevious()){
                    tblBidTableId.previous();
                }
        
        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                }
                cnt++;
                FormulaCount = cnt;
            }

            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){

			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }

            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();

                counter++;
            }

        }
        if(isView ){
            if(isMultiTable){
                //rows = editedRowCount;
                while(tblBidTableId.hasPrevious())
                {
                    tblBidTableId.previous();
                }
            }
        }
        
            int colId = 0;
            int cntBidTable = 0;
            int tmpRowId = 0;
            String currentCellValue="";
           
            while(tblBidTableId.hasNext())// Loop for list of table added multiple time
            {
                CommonFormData bidTableIdDt = tblBidTableId.next();
                cntBidTable++;
                while(tblCellsDtl.hasPrevious())
                {
                        tblCellsDtl.previous();
                }
               
                
                for (short i = -1; i <= rows; i++)  // Loop for row dispaly including Headers
                {
                    
                        if((i == 0) && (cntBidTable==1))
                        { // Displaying Header row
        %>
                        <tr id="ColumnRow">
        <%              // Loop for column Header
                        for(short j=0;j<cols;j++)
                        {
                            
                           
                                if(tblColumnsDtl.hasNext())
                                {
                                    CommonFormData colData = tblColumnsDtl.next();

                                    colHeader = colData.getColumnHeader();
                                    colType = colData.getColumnType();
                                    filledBy = colData.getFillBy();
                                    showOrHide = Integer.parseInt(colData.getShowOrHide());
                                    dataType = colData.getDataType();
                                    colId = colData.getColumnId();
                                    colData = null;
                                }
                                arrColType[j] = colType;
                                fillBy[j] = filledBy;
                                arrColId[j] = colId;
        %>
                                <th id="addTD<%= j + 1 %>">
                                    <%= colHeader %>
                                </th>
        <%
                        }
%>
                    </tr>
<%                  } // End Displaying Header row

       
              if(i > 0) // Dispalying Rows start.....
              {
                  %>
        <tr id="row<%=tableId%>_<%=tmpRowId + 1%>"> 
<%
                        if(isTotal==1 && isMultiTable){
                            if(cntBidTable != cntRow && i==rows){
                                continue;
                            }else{
                                tmpRowId++;
                            }
                        }else{
                            tmpRowId++;
                        }
                        
                      //  out.println("===================================tmpRowId="+tmpRowId);
%>    
           
<%
            int cnt = 0;
            for(int j=0; j<cols; j++) // Loop For Column Loop
            {
                String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" align="center">
<%
             
             
                            if(tblCellsDtl.hasNext())  // Extractiong List of table cell Data
                            {
                                cnt++;
                                CommonFormData cellData = tblCellsDtl.next();
                                dataType = cellData.getCellDataType();
                                cellValue = cellData.getCellValue();
                                columnId = cellData.getColumnId();
                                cellId = cellData.getCellId();
                                

                                if(true){
                                String cValue = "";
                                    cValue = cellValue.replaceAll("\r\n", "");
                                    
                                    if(grandTotalCell==cellId){
                                        out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                                    }
                                }
                                
                                
                                if(fillBy[j] == 1)
                                { // If Fill by PE

%>                                              <%=cellValue.replaceAll("\n", "<br />")%>
<%
                                } 
                                else if(fillBy[j] == 2 || fillBy[j] == 3) // If Fill by Tenderer or Auto
                                {
                                   
                                    if(dataType == 3 || dataType == 4 || dataType == 8 || dataType == 11 || dataType == 13)
                                    {
                                        %>
                                        <div align="right">
                                        <%
                                    }
                                    else if(dataType == 12)
                                    {
                                        %>
                                        <div align="center">
                                        <%    
                                    }
                                    else
                                    {
                                        %>
                                        <div align="left">
                                        <%  
                                    }
                                        
                                                
                                        if(tmpRowId == (editedRowCount)) // if current row is last row? // grand sum co
                                        {
                                            
                                            if(isTotal == 1)
                                            { 
                                                if(cntRow < TotalBidCount)
                                                    if(i%(rows - 1)==0)
                                                        break;

                                                if(arrFormulaColid[columnId-1] != columnId) // if column is not formula column
                                                { 
                                                    if(arrWordFormulaColid[columnId-1] == columnId) // If column is word formula
                                                    {
                                                        if(fillBy[j] == 3 && dataType == 3) // Auto column
                                                        {  
                                                                while(tblBidData.hasPrevious()){
                                                                    tblBidData.previous();
                                                                }
                                                                while(tblBidData.hasNext()){
                                                                    CommonFormData bidData = tblBidData.next();
                                                                    if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                        if(!"".equals(bidData.getCellValue())){
                                                                            out.print(bidData.getCellValue().replaceAll("\n", "<br />"));
                                                                           // break;
                                                                        }
                                                                     }
                                                                }
                                                        } 
                                                    } // End of word formula
                                                    out.print("</td>");
                                                    continue;
                                                }
                                            }
                                        } // end of if(tmpRowId == (editedRowCount)) ////end of  if current row is last row?

                                       
                                      
                                              
    %>
                                            
                                           <%     while(tblBidData.hasPrevious()){
                                                    tblBidData.previous();
                                                }
                                           
                                                while(tblBidData.hasNext())
                                                {
                                                    CommonFormData bidData = tblBidData.next();
                                                    if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
                                                    {
                                                        currentCellValue=bidData.getCellValue();
                                                      if(!(dataType==9 || dataType==10)) 
                                                       {                                                           
                                                        %>  
                                                         <%=bidData.getCellValue().replaceAll("\n", "<br />")%>
                                                        <%   
                                                       }
                                                        break;
                                                    }
                                                }
                                               
                                                if((dataType==9 || dataType==10))
                                                { // if data type is combo and fill by tenderer
                                                         
                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0)
                                                    {
                                                        
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                  
                                                     for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ 
                                                         
                                                               if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue()))
                                                               { %>
                                                                 <%=tblTenderListDetail.getItemText().replaceAll("\n", "<br />")%>
                                                            <% } %>
                                                           
                                                        <% } %>
                                                    
    <%                                             }
                                           }
                                    %>
                                        </div>
                                    <%
                                    
                                } // End of If Fill by Tenderer or Auto
                            } // celldtl
%>
                </td>
<%
                        } // End Loop For Column Loop
%>
            </tr>
<%
                    }  // End Loop for row dispaly including Headers
                
                }  // End Loop for list of table added multiple time
            }
        
%>
        </tbody>
        
    </table>
                </div>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>