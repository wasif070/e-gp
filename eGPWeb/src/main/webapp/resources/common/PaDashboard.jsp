<%--
    Document   : AllTenders
    Created on : Nov 15, 2010, 2:16:52 PM
    Author     : Malhar,rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblActivityMaster"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.web.servlet.PendingProcessedServlet" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dashboard</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <!-- <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
       <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />-->
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <!--        <script  type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>

        <script type="text/javascript">
            
            function profileupdatealert(){
            var msg = "<%=request.getParameter("proup")%>";
                if(msg != "null"){
                    if(msg == "true"){
                        jAlert("Your profile has been successfully updated","Sucessfull", function(RetVal) {
                                       });   
                    }
                }
            }
            
            function GetCal(txtname, controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function validate() {
                var moduleName = document.getElementById("module").value;
                var processName = document.getElementById("process").value;
                var moduleId = document.getElementById("workflowId").value;
                var processFrom = document.getElementById("dtStartDate").value;
                var processTo = document.getElementById("dtEndDate").value;

                if (moduleName == "" && processName == "" && moduleId == "" && processFrom == "" && processTo == "") {
                    document.getElementById("error").innerHTML = "Please enter any search criteria";
                    return false;
                }
                return true;
            }

            function resetMe() {
                document.getElementById('frmsubmit').action = window.location;
                document.getElementById('frmsubmit').submit();
            }
        </script>
    </head>
    <body onload="profileupdatealert();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->

            <%@include file="AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Dashboard</div>
                <div>&nbsp;</div>
                <div align="center">
                    <%                    String viewType = "pending";
                        if (request.getParameter("viewtype") != null && !request.getParameter("viewtype").isEmpty()) {
                            viewType = request.getParameter("viewtype");
                        }
                        String colName = null;
                        String colHeader = null;
                        String caption = null;
                        String sortColumn = null;
                        String widths = "";
                        String aling = "";

                        String moduleName = "";
                        String processName = "";
                        String workflowId = "0";
                        String processesBy = "";
                        String processedStDt = "";
                        String processedEndDt = "";
                        if (viewType.equalsIgnoreCase("pending")) {
                            colHeader = "Sl.No.,Module Name,Process Name,ID,Processed By,Processed Date and Time,Previous Action,To be Processed By,Action";
                            colName = "S.No,moduleName,eventName,objectId,fileSentFrom,processDate,action,fileSentTo,process";
                            caption = " ";
                            sortColumn = "false,true,true,true,true,true,true,true,false";
                            widths = "5,15,20,10,20,15,10,15,10";
                            aling = "center,left,left,center,left,left,center,left,center";
                        } else if (viewType.equalsIgnoreCase("defaultworkflow")) {
                            colHeader = "S.No,Module Name,Process Name,ID,Action";
                            colName = "S.No,moduleName,eventName,objectId,View";
                            caption = " ";
                            sortColumn = "false,true,true,true,false";
                            widths = "10,30,30,20,10";
                            aling = "center,left,left,center,center";
                        } else {
                            colHeader = "S.No,Module Name,Process Name,ID,Processed By,Processed Date and Time,Action,To be Processed By,Action";
                            colName = "S.No,moduleName,eventName,objectId,fileSentFrom,processDate,action,fileSentTo,History";
                            caption = " ";
                            sortColumn = "false,true,true,true,true,true,true,true,false";
                            widths = "5,15,10,10,20,10,10,20,5";
                            aling = "center,left,left,center,left,left,center,left,center";
                        }

                        if ("Search".equalsIgnoreCase(request.getParameter("btnSearchwf"))) {
                            if (request.getParameter("module") != null) {
                                moduleName = request.getParameter("module");
                            }
                            if (request.getParameter("process") != null) {
                                processName = request.getParameter("process");
                            }
                            if (request.getParameter("workflowId") != null) {
                                workflowId = request.getParameter("workflowId");
                            }
                            if (request.getParameter("processesBy") != null) {
                                processesBy = request.getParameter("processesBy");
                            }
                            if (request.getParameter("dtStartDate") != null) {
                                processedStDt = request.getParameter("dtStartDate");
                            }
                            if (request.getParameter("dtEndDate") != null) {
                                processedEndDt = request.getParameter("dtEndDate");
                            }
                        }

                        String expiryDt = "";
                        if (session.getAttribute("userId") != null && session.getAttribute("userId") != "") {
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            List<TblLoginMaster> tblLoginMasters = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(session.getAttribute("userId").toString()));
                            TblLoginMaster tblLoginMaster = null;
                            Date expiryDate = null;
                            Calendar calExpiry = Calendar.getInstance(), calCurrent = Calendar.getInstance();
                            if (tblLoginMasters != null && (!tblLoginMasters.isEmpty())) {
                                tblLoginMaster = tblLoginMasters.get(0);
                                if (tblLoginMaster.getTblUserTypeMaster().getUserTypeId() == 2 && tblLoginMaster.getStatus().equalsIgnoreCase("approved")) {
                                    expiryDate = tblLoginMaster.getValidUpTo();
                                    expiryDt = DateUtils.customDateFormate(expiryDate);
                                    calExpiry.setTime(expiryDate);
                                    calCurrent.add(Calendar.DATE, +30);
                                    if (calExpiry.equals(calCurrent) || calExpiry.before(calCurrent)) {%>
                    <blink> <strong style="height:15px; line-height:15px;  color:#ff0000; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong> </blink>
                        <%} else {%>
                    <strong style="height:15px; line-height:15px; color:#78A951; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong><br/>
                    <%} %>

                    <%}
                            }
                        }%>
                </div>
                <!--Dashboard Header End-->
                <form id="frmworkflow" name="frmMyApp" method="post">
                    <div class="formBg_1 t_space">
                        <table cellspacing="10" class="formStyle_1" width="100%">
                            <tr>
                                <td width="20%" class="ff">Module Name :</td>
                                <td width="30%">
                                    <%--<input type="text" name="module" id="module" value="<%=moduleName%>" class="formTxtBox_1" /> --%>
                                    <%

                                        if (request.getParameter("module") != null) {
                                            moduleName = request.getParameter("module");
                                        } else {
                                            moduleName = "";
                                        }

                                    %>
                                    <select name="module" class="formTxtBox_1" id="module" style="width:160px;" onchange="ChangeIDName(this.value);">
                                        <option  value="" <%if (moduleName.equals("")) {%> selected <%}%> >-- Select Module Name --</option>
                                        <option value="Tender" <%if (moduleName.equals("Tender")) {%> selected <%}%> >Tender</option>
                                        <option value="Annual Procurement Plan (APP)" <%if (moduleName.equals("Annual Procurement Plan (APP)")) {%> selected <%}%> >Annual Procurement Plan (APP)</option>
                                        <option value="Contract Management System (CMS)" <%if (moduleName.equals("Contract Management System (CMS)")) {%> selected <%}%> >Contract Management System (CMS)</option>
                                    </select>

                                </td>
                                <td width="20%" class="ff">Process Name : </td>
                                <td width="30%">
                                    <%
                                        CommonService serv = (CommonService) AppContext.getSpringBean("CommonService");
                                        List<TblActivityMaster> activityList = serv.getActivityMasterData();
                                    %>

                                    <%-- <input type="text" name="process" id="process" value="<%=processName%>" class="formTxtBox_1" />--%>

                                    <select name="process" class="formTxtBox_1" id="process" style="width:160px;">
                                        <option value="">-- Select Process --</option>
                                        <%
                                            if (request.getParameter("process") != null) {
                                                processName = request.getParameter("process");
                                            } else {
                                                processName = "";
                                            }
                                            for (TblActivityMaster am : activityList) {
                                                String temp = "";
                                                if (processName.equals(am.getActivityName())) {
                                                    temp = "Selected";
                                                }

                                                out.write("<option value='" + am.getActivityName() + "' "
                                                        + temp + ">" + am.getActivityName() + "</option>");
                                            }
                                        %>


                                    </select>

                                </td>
                            </tr>

                            <tr>
                                <td class="ff" width="20%" ><span id="moduleNameId" style="color: black"></span> ID : </td>
                                <td width="30%%">
                                       <input type="text" name="workflowId" id="workflowId" value="<% if (!"0".equals(workflowId)) {
                                               out.print(workflowId);
                                           } %>" class="formTxtBox_1" />
                                </td>
                                <% if (!"defaultworkflow".equalsIgnoreCase(viewType)) { %>
                                <td class="ff" width="20%" >
                                    <% if ("pending".equalsIgnoreCase(viewType)) { %>
                                    Processed By :
                                    <% } else { %>
                                    To be Processed By :
                                    <% }%>
                                </td>
                                <td width="30%">
                                    <input type="text" name="processesBy" id="processesBy" value="<%=processesBy%>" class="formTxtBox_1" />
                                </td>
                                <% } else { %>
                                <td class="ff" width="20%">&nbsp;</td>
                                <td width="30%">&nbsp;</td>
                                <% }%>
                            </tr>

                            <tr>
                                <td width="20%" class="ff">From Processed Date and Time : </td>
                                <td width="30%">
                                    <input name="dtStartDate" type="text" class="formTxtBox_1" id="dtStartDate" readonly="true" onfocus="GetCal('dtStartDate', 'dtStartDate');" value="<%=processedStDt%>" />
                                    <img id="dtStartDate1" name="dtStartDate1" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('dtStartDate', 'dtStartDate1');"/>&nbsp;&nbsp;
                                </td>
                                <td width="20%" class="ff">To Processed Date and Time</td>
                                <td width="30%">
                                    <input name="dtEndDate" type="text" class="formTxtBox_1" id="dtEndDate" readonly="true"  onfocus="GetCal('dtEndDate', 'dtEndDate');" value="<%=processedEndDt%>"/>
                                    <img id="dtEndDate1" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dtEndDate', 'dtEndDate1');"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="t-align-center"><span id="error" style="color: red;"></span></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="btnSearchwf" id="btnSearchwf" value="Search" onclick="return validate();" />
                                    </label>
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="button" name="Reset" id="button" value="Reset" onclick="resetMe()"/>
                                    </label>
                                </td>
                            </tr>
                        </table></div>
                </form>
                <form name="frmsubmit" id="frmsubmit"  action="<%=request.getContextPath()%>/officer/PendingProcessing.jsp" method="post">
                    <input type="hidden" name="viewtype" id="viewtype" value="<%=viewType%>" />
                </form>
                <div>&nbsp;</div>

                <jsp:include page="PADashboardTop.jsp"></jsp:include>
                    <div class="tabPanelArea_1">
                        <div align="center">
                        <jsp:include page="GridCommon.jsp" >
                            <jsp:param name="caption" value="<%=caption%>" />
                            <jsp:param name="url" value='<%=request.getContextPath() + "/PendingProcessedServlet?viewtype=" + viewType + "&module=" + moduleName + "&process=" + processName + "&workflowId=" + workflowId + "&processesBy=" + processesBy + "&dtStartDate=" + processedStDt + "&dtEndDate=" + processedEndDt%>' />
                            <jsp:param name="colHeader" value='<%=colHeader%>' />
                            <jsp:param name="colName" value='<%=colName%>' />
                            <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                            <jsp:param name="width" value="<%=widths%>" />
                            <jsp:param name="aling" value="<%=aling%>" />
                            <jsp:param name="searchOpt" value="false" />
                        </jsp:include>
                    </div>
                </div>

            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        function ChangeIDName(value){
           $('#moduleNameId').html(value);
        }
        $(function () {
            $('#btnFirst').click(function () {
                var totalPages = parseInt($('#totalPages').val(), 10);

                if (totalPages > 0 && $('#pageNo').val() != "1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if (parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnLast').click(function () {
                var totalPages = parseInt($('#totalPages').val(), 10);
                if (totalPages > 0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if (parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnNext').click(function () {
                var pageNo = parseInt($('#pageNo').val(), 10);
                var totalPages = parseInt($('#totalPages').val(), 10);

                if (pageNo != totalPages) {
                    $('#pageNo').val(Number(pageNo) + 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) + 1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnPrevious').click(function () {
                var pageNo = $('#pageNo').val();

                if (parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if (parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnGoto').click(function () {
                var pageNo = parseInt($('#dispPage').val(), 10);
                var totalPages = parseInt($('#totalPages').val(), 10);
                if (pageNo > 0)
                {
                    if (pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if (parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if (parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        function showHide()
        {
            if (document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML == '- Collapse') {
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Expand';
            } else {
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Collapse';
            }
        }

        function checkKeyGoTo(e)
        {
            var keyValue = (window.event) ? e.keyCode : e.which;
            if (keyValue == 13) {
                //Validate();
                $(function () {
                    //$('#btnGoto').click(function() {
                    var pageNo = parseInt($('#dispPage').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (pageNo > 0)
                    {
                        if (pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
    <script type="text/javascript">
        function changeTab(tabNo) {
            if (tabNo == 1) {
                /*  $("#liveTab").removeClass("sMenu");
                 $("#pendingTab").addClass("sMenu");
                 $("#archivedTab").removeClass("sMenu");
                 $("#cancelledTab").removeClass("sMenu");
                 $("#processingTab").removeClass("sMenu");
                 $("#statusTab").val("Under Preparation");
                 $("#status").val("Pending");
                 $('#pageNo').val('1');*/
            } else if (tabNo == 2) {
                $("#pendingTab").removeClass("sMenu");
                $("#liveTab").addClass("sMenu");
                $("#archivedTab").removeClass("sMenu");
                $("#cancelledTab").removeClass("sMenu");
                $("#processingTab").removeClass("sMenu");
                $("#statusTab").val("Live");
                $("#status").val("Approved");
                $('#pageNo').val('1');
            } else if (tabNo == 3) {
                $("#liveTab").removeClass("sMenu");
                $("#pendingTab").removeClass("sMenu");
                $("#archivedTab").addClass("sMenu");
                $("#cancelledTab").removeClass("sMenu");
                $("#processingTab").removeClass("sMenu");
                $("#statusTab").val("Archive");
                $("#status").val("Approved");
                $('#pageNo').val('1');
            } else if (tabNo == 4) {
                $("#liveTab").removeClass("sMenu");
                $("#pendingTab").removeClass("sMenu");
                $("#archivedTab").removeClass("sMenu");
                $("#cancelledTab").addClass("sMenu");
                $("#processingTab").removeClass("sMenu");
                $("#statusTab").val("Cancelled");
                $("#status").val("Cancelled");
                $('#pageNo').val('1');
            } else if (tabNo == 5) {
                $("#liveTab").removeClass("sMenu");
                $("#pendingTab").removeClass("sMenu");
                $("#archivedTab").removeClass("sMenu");
                $("#cancelledTab").removeClass("sMenu");
                $("#processingTab").addClass("sMenu");
                $("#statusTab").val("Processing");
                $("#status").val("Approved");
                $('#pageNo').val('1');
            }

            loadTenderTable();
        }
        function chngTab(tabNo)
        {
            if (tabNo == 1)
            {
                $("#pendingTab").addClass("sMenu");
                $("#approvedTab").removeClass("sMenu");
                $("#status").val("Pending");
            } else if (tabNo == 2)
            {
                //$("#pendingTab").removeClass("sMenu");
                $("#approvedTab").addClass("sMenu");
                $("#status").val("Approved");
            }
            $('#pageNo').val("1");
            $('#dispPage').val("1");
            loadTenderTable();
        }

        //            $(function() {
        //                $('#cmbStatus').change(function() {
        //
        //                    if($('#cmbStatus').val() == "Pending")
        //                    {
        //                        chngTab(1);
        //                        $('#tabForApproved').hide();
        //                    }
        //                    else if($('#cmbStatus').val() == "Approved")
        //                    {
        //                        chngTab(2);
        //                        $('#tabForApproved').show();
        //                    }
        //                });
        //            });
        $(function () {
            $('#cmbStatus').change(function () {
                if ($('#cmbStatus').val() == "Live")
                {
                    $("#statusTab").val("Live");
                    $("#status").val("Approved");
                } else if ($('#cmbStatus').val() == "Pending")
                {
                    //                        $("#statusTab").val("Live");
                    //                        $("#status").val("Approved");
                    $("#statusTab").val("Under Preparation");
                    $("#status").val("Pending");
                } else if ($('#cmbStatus').val() == "Archive")
                {
                    $("#statusTab").val("Archived");
                    $("#status").val("Approved");
                } else if ($('#cmbStatus').val() == "Cancelled")
                {
                    $("#statusTab").val("Cancelled");
                    $("#status").val("Cancelled");
                } else if ($('#cmbStatus').val() == "Processing")
                {
                    $("#statusTab").val("Processing");
                    $("#status").val("Approved");
                }
                //$("#status").val("Cancelled");
            });
        });
    </script>
    <script type="text/javascript">
        loadTenderTable();
        function loadTenderTableone()
        {
            $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {
                funName: 'MyTenders',
                action: 'get mytenders',
                statusTab: $("#statusTab").val(),
                status: $("#status").val(),
                tenderId: $("#tenderId").val(),
                refNo: $("#refNo").val(), procNature: $("#procNature").val(),
                procType: $("#cmbType").val(),
                procMethod: $("#cmbProcMethod").val(),
                tenderId: $("#tenderId").val(),
                        refNo: $("#refNo").val(),
                        pubDtFrm: $("#pubDtFrm").val(),
                pubDtTo: $("#pubDtTo").val(),
                pageNo: $("#pageNo").val(),
                size: $("#size").val()
            },
                    function (j) {
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();
                        if ($('#noRecordFound').attr('value') == "noRecordFound") {
                            $('#pagination').hide();
                        } else {
                            $('#pagination').show();
                        }
                        //chkdisble($("#pageNo").val());
                        if ($("#totalPages").val() == 1) {
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        } else {
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();

                        var counter = $('#cntTenBrief').val();
                        for (var i = 0; i < counter; i++) {
                            try
                            {
                                if ($('#tenderBrief_' + i).html() != null) {
                                    //alert($('#tenderBrief_'+i).html());
                                    //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                    var temp = $('#tenderBrief_' + i).html().replace(/<[^>]*>|\s/g, '');
                                    var temp1 = $('#tenderBrief_' + i).html();
                                    if (temp.length > 250) {
                                        temp = temp1.substr(0, 250);
                                        $('#tenderBrief_' + i).html(temp + '...');
                                    }
                                }
                            } catch (e) {
                            }
                        }
                    });
        }
        function loadTenderTable()
        {
            $.post("<%=request.getContextPath()%>/PendingProcessedServlet", {
                viewtype: 'pending',
                module: '',
                process: '',
                rows: '10',
                workflowId: '0',
                processesBy: '',
                dtStartDate: '',
                dtEndDate: '',
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                page: 1,
                //pager: $("#page"),
                caption: "pending"
            },
                    function (j) {
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();

                        if ($('#noRecordFound').attr('value') == "noRecordFound") {
                            $('#pagination').hide();
                        } else {
                            $('#pagination').show();
                        }
                        //chkdisble($("#pageNo").val());
                        if ($("#totalPages").val() == 1) {
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        } else {
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();

                        var counter = $('#cntTenBrief').val();
                        for (var i = 0; i < counter; i++) {
                            try
                            {
                                var temp = $('#tenderBrief_' + i).html().replace(/<[^>]*>|\s/g, '');
                                var temp1 = $('#tenderBrief_' + i).html();
                                if (temp.length > 250) {
                                    temp = temp1.substr(0, 250);
                                    $('#tenderBrief_' + i).html(temp + '...');
                                }
                            } catch (e) {
                            }
                            //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                        }
                    });
        }
    </script>

    <script type="text/javascript">
        $(function () {
            var count;
            $('#btnSearch').click(function () {
                count = 0;
                if (document.getElementById('procNature') != null && document.getElementById('procNature').value != '') {
                    count = 1;
                } else if (document.getElementById('cmbType') != null && document.getElementById('cmbType').value != '') {
                    count = 1;
                } else if (document.getElementById('cmbProcMethod') != null && document.getElementById('cmbProcMethod').value != '0') {
                    count = 1;
                } else if (document.getElementById('tenderId') != null && document.getElementById('tenderId').value != '') {
                    count = 1;
                } else if (document.getElementById('pubDtFrm') != null && document.getElementById('pubDtFrm').value != '') {
                    count = 1;
                } else if (document.getElementById('cmbStatus') != null && document.getElementById('cmbStatus').value != '') {
                    count = 1;
                } else if (document.getElementById('refNo') != null && document.getElementById('refNo').value != '') {
                    count = 1;
                } else if (document.getElementById('pubDtTo') != null && document.getElementById('pubDtTo').value != '') {
                    count = 1;
                }
                if (count != '0') {
                    if ($("#statusTab").val() == 'Cancelled') {
                        changeTab(4);
                    } else if ($("#statusTab").val() == 'Live') {
                        changeTab(2);
                    } else if ($("#statusTab").val() == 'Archived') {
                        changeTab(3);
                    } else if ($("#statusTab").val() == 'Processing') {
                        changeTab(5);
                    } else {
                        changeTab(1);
                    }
                    $("#valAll").html('');
                    $("#pageNo").val("1");
                    $.post("<%=request.getContextPath()%>/TenderDetailsServlet",
                            {
                                funName: 'MyTenders',
                                statusTab: $("#statusTab").val(),
                                action: 'get mytenders',
                                status: $("#status").val(),
                                tenderId: $("#tenderId").val(),
                                refNo: $("#refNo").val(),
                                procNature: $("#procNature").val(),
                                procType: $("#cmbType").val(),
                                procMethod: $("#cmbProcMethod").val(),
                                tenderId: $("#tenderId").val(),
                                        refNo: $("#refNo").val(),
                                        pubDtFrm: $("#pubDtFrm").val(),
                                pubDtTo: $("#pubDtTo").val(),
                                pageNo: $("#pageNo").val(),
                                size: $("#size").val()},
                            function (j) {
                                $('#resultTable').find("tr:gt(0)").remove();
                                $('#resultTable tr:last').after(j);
                                sortTable();
                                if ($('#noRecordFound').attr('value') == "noRecordFound") {
                                    $('#pagination').hide();
                                } else {
                                    $('#pagination').show();
                                }
                                //chkdisble($("#pageNo").val());
                                if ($("#totalPages").val() == 1) {
                                    $('#btnNext').attr("disabled", "true");
                                    $('#btnLast').attr("disabled", "true");
                                } else {
                                    $('#btnNext').removeAttr("disabled");
                                    $('#btnLast').removeAttr("disabled");
                                }
                                $("#pageTot").html($("#totalPages").val());
                                $("#pageNoTot").html($("#pageNo").val());
                                $('#resultDiv').show();

                                var counter = $('#cntTenBrief').val();
                                for (var i = 0; i < counter; i++)
                                {
                                    try
                                    {
                                        if ($('#tenderBrief_' + i).html() != null) {
                                            //alert($('#tenderBrief_'+i).html());
                                            //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                            var temp = $('#tenderBrief_' + i).html().replace(/<[^>]*>|\s/g, '');
                                            var temp1 = $('#tenderBrief_' + i).html();
                                            if (temp.length > 250) {
                                                temp = temp1.substr(0, 250);
                                                $('#tenderBrief_' + i).html(temp + '...');
                                            }
                                        }
                                    } catch (e) {
                                    }
                                }
                            });

                } else {
                    $("#valAll").html('Please enter at least One search criteria');
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnReset').click(function () {
                $("#pageNo").val("1");
                $("#refNo").val('');
                $("#tenderId").val('');
                $("#procNature").val('');
                $("#cmbType").val('');
                $("select#cmbProcMethod option[selected]").removeAttr("selected");
                $("select#cmbProcMethod option[value='0']").attr("selected", "selected");
//$("#cmbProcMethod").val('');
                $("#pubDtFrm").val('');
                $("#pubDtTo").val('');
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet",
                        {
                            funName: 'MyTenders',
                            action: 'get mytenders',
                            status: 'Pending',
                            tenderId: '0', refNo: '',
                            procNature: '',
                            procType: '',
                            procMethod: '0',
                            pubDtFrm: $("#pubDtFrm").val(),
                            pubDtTo: $("#pubDtTo").val(),
                            pageNo: $("#pageNo").val(),
                            size: $("#size").val()},
                        function (j) {
                            $('#resultTable').find("tr:gt(0)").remove();
                            $('#resultTable tr:last').after(j);
                            sortTable();
                            if ($('#noRecordFound').attr('value') == "noRecordFound") {
                                $('#pagination').hide();
                            } else {
                                $('#pagination').show();
                            }
                            //chkdisble($("#pageNo").val());
                            if ($("#totalPages").val() == 1) {
                                $('#btnNext').attr("disabled", "true");
                                $('#btnLast').attr("disabled", "true");
                            } else {
                                $('#btnNext').removeAttr("disabled");
                                $('#btnLast').removeAttr("disabled");
                            }
                            $("#pageTot").html($("#totalPages").val());
                            $("#pageNoTot").html($("#pageNo").val());
                            $('#resultDiv').show();

                            var counter = $('#cntTenBrief').val();
                            for (var i = 0; i < counter; i++) {
                                try
                                {
                                    if ($('#tenderBrief_' + i).html() != null) {
                                        //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                        var temp = $('#tenderBrief_' + i).html().replace(/<[^>]*>|\s/g, '');
                                        var temp1 = $('#tenderBrief_' + i).html();
                                        if (temp.length > 250) {
                                            temp = temp1.substr(0, 250);
                                            $('#tenderBrief_' + i).html(temp + '...');
                                        }
                                    }
                                } catch (e) {
                                }
                            }

                        });
            });
        });
    </script>
       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabMsgBox");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
       </script>
</html>

