<%-- 
    Document   : NOADocInclude
    Created on : Dec 22, 2011, 11:11:39 AM
    Author     : nishit
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblContractSignDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaDocuments"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderSectionDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderSection"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<jsp:useBean id="comDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE html>
<html>
    <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
<%
    String strforDownloadLink = "Package";
    boolean isTenderPkgWise = false;
    String comUserId = "0";
        if(request.getParameter("comuserId")!=null && !"".equalsIgnoreCase(request.getParameter("comuserId"))){
            comUserId = request.getParameter("comuserId");                        
        }
    
    CommonService comcommonService = (CommonService)AppContext.getSpringBean("CommonService");
        String commonTenderId = request.getParameter("comtenderId");
        String commonpkgId = request.getParameter("comlotId");
        String st_forDownloadLink = "Lot_"+commonpkgId;
        if("Package".equalsIgnoreCase(comcommonService.getDocAvlMethod(commonTenderId).toString())){
            isTenderPkgWise = true;
            st_forDownloadLink = "Package";
        }
        String comProcMethod = comcommonService.getProcMethod(commonTenderId).toString();
        
        List<TblTenderLotSecurity> lots = null;
            if (isTenderPkgWise == false) {
                lots = comDocSrBean.getLotDetailsByLotId(Integer.parseInt(commonTenderId), Integer.parseInt(commonpkgId));
                if (lots.size() > 0) {
                      TblTenderLotSecurity tenderLotSecurity =  lots.get(0);
                      strforDownloadLink = "Lot_"+tenderLotSecurity.getLotNo();
                  }
             }
        List<TblNoaDocuments> listNoaDoc = comDocSrBean.getNoaDoc(Integer.parseInt(commonTenderId), Integer.parseInt(comUserId));
        int comNoaId = 0;
        comNoaId = comDocSrBean.getNoaId(Integer.parseInt(commonTenderId), Integer.parseInt(comUserId));
        List<TblContractSignDocs> contractDocList = new ArrayList<TblContractSignDocs>();
        if(comNoaId!=0){
            contractDocList = comDocSrBean.getContractDocList(comNoaId);
        }
        String comProcNature = comcommonService.getProcNature(commonTenderId).toString();
        String comEventType = comcommonService.getEventType(commonTenderId).toString();
        int comTenderStdId = comDocSrBean.getTenderSTDId(Integer.parseInt(commonTenderId));
        List<TblTenderSection> tSections = comDocSrBean.getTenderSections(comTenderStdId);
        List<Object[]> comDoc = comDocSrBean.commonDocMapList(commonTenderId, comUserId);
        List<TblTenderSectionDocs> listDocs = comDocSrBean.getTenderDocsTenderId(Short.parseShort(commonTenderId));
        boolean comIsPdf = false;
        if("true".equalsIgnoreCase(request.getParameter("isPDF"))){
            comIsPdf = true;
        }
        if(comIsPdf){%>
            <script type="text/javascript">
                    $(document).ready(function() {
                                    $("a[view='link']").each(function(){
                                            $(this).parent().append("<span class='disp_link'>-</span>");
                                            $(this).hide();
                                    })
                            });
                            
        </script>  
        <%}%>
    <body>
        <%--<div class="t_space"><span class="c-alignment-right">
                                 <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=commonTenderId%>&lotNo=<%=strforDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Tender Dcoument">Download Tender Document</a>
            </span></div>--%>
        <div>
            <div class="t-align-left t_space" style="background-color: #f6f6f6; padding: 5px;">
                <%if("goods".equalsIgnoreCase(comProcNature)){%>
                <strong>The following documents (but for RFQ Submission Letter and Sl. a & h from below only) forming the integral part of the contract shall be interpreted in the order of priority : </strong>
                <%}else if("works".equalsIgnoreCase(comProcNature)){%>
                <strong>The following documents (but for RFQ Submission Letter and Sl. a & i from below only) forming the integral part of the contract shall be interpreted in the order of priority : </strong>
                <%}else{%>
                <strong>The following documents forming the integral part of the contract shall be interpreted in the order of priority : </strong>
                <%}%>
            <ol type="a"  class="t_space b_space" style="margin-left: 30px; line-height: 20px;" >
                <%
                     if(!"Services".equalsIgnoreCase(comProcNature)){   
                %>
                
                        <li>The signed Form of Contract Agreement</li>
                        <li>The  Letter of Acceptance</li>
                        <li>The Tender/Proposal and the appendices to the Tender/Proposal</li>
                        <li>Particular Conditions of Contract</li>
                        <li>General Conditions of Contract</li>
                        <li>Technical Specifications</li>
                        <%if(!"Goods".equalsIgnoreCase(comProcNature)){%>
                        <li>The General Specifications</li>
                        <%}%>
                        <li>Drawings</li>
                        <%if("Goods".equalsIgnoreCase(comProcNature)){%>
                        <li>Price and Delivery Schedule for Goods</li>
                        <li>Price and Completion Schedule for Related Services</li>
                        <%}else{%>
                        <li>Bill of Quantities</li>
                        <li>Schedule of Dayworks</li>
                        <%}%>
                        <li>Other document including correspondences listed in the PCC forming part of the Contract</li>
                <%}else{%>
                
                        <li>The Form of Contract</li>
                        <li>The Particular Conditions of Contract (PCC)</li>
                        <li>The General Conditions of Contract (GCC)</li>
                        <li>The Appendices:
                            <ol type="i" style="margin-left: 20px; line-height: 18px;">
                                <li>Description of the Services</li>
                                <li>Reporting Requirements</li>
                                <li>Key Personnel and Sub Consultants</li>
                                <li>Services and Facilities provided by the Client</li>
                                <li>Cost Estimates</li>
                                <li>Form of Bank Guarantee for Advance Payment</li>
                            </ol>
                        </li>
                <%}%>
                </ol>
                </div>
                <%
                int noaDocCnt=1;
                    if(contractDocList!=null && !contractDocList.isEmpty()){
                %>
                <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                    <tr>
                        <td colspan="5" class="ff">
                            Contract Documents
                        </td>
                    </tr>
                    <tr>
                        <th>S.No.</th>
                        <th>File Name</th>
                        <th>File Description</th>
                        <th>File Size</th>
                        <th>Action</th>
                    </tr>
                    <%
                        for(TblContractSignDocs tcsd : contractDocList){
                    %>
                    <tr>
                        <td class="t-align-center"><%=noaDocCnt++%></td>
                        <td class="t-align-left"><%=tcsd.getDocumentName()%></td>
                        <td class="t-align-left"><%=tcsd.getDocDescription()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(tcsd.getDocSize()) / 1024)%></td>
                        <td class="t-align-center">
                                <a view='link' href="<%=request.getContextPath()%>/ServletContractSignDoc?docName=<%=tcsd.getDocumentName()%>&docSize=<%=tcsd.getDocSize()%>&noaIssueId=<%=tcsd.getNoaId()%>&tenderId=<%=commonTenderId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <%--<%if(Integer.parseInt(session.getAttribute("userId").toString())==tcsd.getUploadedBy()){%>
                                <a href="<%=request.getContextPath()%>/ServletContractSignDoc?&docName=<%=tcsd.getDocumentName()%>&docId=<%=tcsd.getContractSignDocId()%>&noaIssueId=<%=tcsd.getNoaId()%>&tenderId=<%=commonTenderId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                <%}%>--%>
                        </td>
                    </tr>
                    <%
                        if(tcsd!=null){
                            tcsd = null;
                        }
                        }%>
                </table>
                <%}%>
                <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                    <tr>
                        <td colspan="5" class="ff">
                            LOA Documents
                        </td>
                    </tr>
                </table>
                <div id="dataTable">
                <%if(listNoaDoc!=null && !listNoaDoc.isEmpty()){%>
                <table width='100%' cellspacing='0' class='tableList_1'>
                    
                    <tr>
                        <th>S.No.</th>
                        <th>File Name</th>
                        <th>File Description</th>
                        <th>File Size</th>
                        <th>Action</th>
                    </tr>
                    <%          noaDocCnt=1;
                                for(TblNoaDocuments tnd : listNoaDoc){
                    %>
                    <tr>
                            <td class="t-align-center"><%=noaDocCnt++%></td>
                            <td class="t-align-left"><%=tnd.getDocumentName()%></td>
                            <td class="t-align-left"><%=tnd.getDocDescription()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(tnd.getDocSize()) / 1024)%></td>
                            <td class="t-align-center">
                                <a view='link' href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=tnd.getDocumentName()%>&tenderid=<%=tnd.getTenderId() %>&roundId=<%=tnd.getRoundId() %>&userid=<%=tnd.getUserId() %>&pckLotid=<%=tnd.getPkgLotId() %>&docSize=<%=tnd.getDocSize() %>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <%--<%if(Integer.parseInt(session.getAttribute("userId").toString())==tnd.getUploadedBy()){%>
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=tnd.getDocumentName()%>&tenderid=<%=tnd.getTenderId()%>&roundId=<%=tnd.getRoundId()%>&userid=<%=tnd.getUserId()%>&pckLotid=<%=tnd.getPkgLotId() %>&docId=<%=tnd.getDocSize()%>&funName=remove" title="Remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                <%}%>--%>
                            </td>
                        </tr>
                    <%if(tnd!=null){
                        tnd=null;
                    }}%>
                </table>
                <%}%>
                </div>
                <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                <tr>
                    <th width='80%' class='t-align-left ff' >Section Name: </th>
                    <th  class='t-align-center ff' >Action </th>
                </tr>
                <%
                int comTemplateId = 0;
                int comDisp = 0;
                int sectionId = 0;
                String str_FolderName = "";
                short disp = 0;
                for (TblTenderSection tts : tSections){
                    disp++;
                    
                    String strFolderName = "Section"+disp+"_"+tts.getSectionName();
                    if(("gcc".equalsIgnoreCase(tts.getContentType()) || ("PCC".equalsIgnoreCase(tts.getContentType())) && (!("rfqu".equalsIgnoreCase(comEventType)|| "rfql".equalsIgnoreCase(comEventType) || "rfq".equalsIgnoreCase(comEventType))))){
                        
                        if("gcc".equalsIgnoreCase(tts.getContentType())){
                            sectionId = tts.getTenderSectionId();
            %>
                           <tr>
                                <td ><%=tts.getSectionName()%>
                                </td>
                                <td class="t-align-center" colspan="3"><a view='link' onclick="javascript:window.open('TenderITTDashboard.jsp?tenderStdId=<%=comTenderStdId%>&tenderId=<%=commonTenderId%>&sectionId=<%=tts.getTenderSectionId()%>&noa=yes', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>&nbsp;&nbsp;
                                | &nbsp;&nbsp;<a view='link' class="action-button-savepdf"
                                                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=strFolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=commonTenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download">Save As PDF </a></td>
                            </tr> 
                        <%}else if("PCC".equalsIgnoreCase(tts.getContentType()) && "Services".equalsIgnoreCase(comProcNature)){%>
                           <tr>
                                <td ><%=tts.getSectionName()%></td>
                                <td class="t-align-center" colspan="3"><a view='link' onclick="javascript:window.open('<%=request.getContextPath()%>/officer/TenderTDSView.jsp?tenderStdId=<%=comTenderStdId%>&tenderId=<%=commonTenderId%>&sectionId=<%=sectionId%>&porlId=<%=commonpkgId%>&isPDF=true', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>&nbsp;&nbsp;
                                | &nbsp;&nbsp;<a view='link' class="action-button-savepdf"
                                                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=strFolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=commonTenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download">Save As PDF </a></td>
                                <%--tenderStdId=2428&tenderId=3000&sectionId=11975&porlId=-1--%>
                            </tr> 

                        <%}}else if("Form".equalsIgnoreCase(tts.getContentType())){
                %>
                <tr>
                    <td colspan="2" ><%=tts.getSectionName()%>
                    </td>
                </tr>
                <tr><td colspan="2">
                <table width="100%" cellspacing="0" class="tableList_1 ">
                    <tr>
                        <th style="text-align:center; width:4%;">Sl. No.</th>
                        <th width="32%">Form Name </th>
                        <th width="11%" class="t-align-center">Action</th>
                        <th width="63%">Map Document List</th>
                        
                    </tr>
                <%
                   java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = comDocSrBean.getTenderForm(tts.getTenderSectionId()); 
                   int count=0;
                   for(int i=0;i<forms.size();i++){
                       if(!("c".equalsIgnoreCase(forms.get(i).getFormStatus())|| "createp".equalsIgnoreCase(forms.get(i).getFormStatus()))){
                       count++;
                %>
                    <tr>
                        <td><%=count%></td>
                        <td><%out.print(forms.get(i).getFormName().replace("?s", "'s"));%></td>
                        <td class="t-align-center">
                            <%if(!"0".equalsIgnoreCase(comUserId)){%>
                           <a view='link' onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/ViewBOQForms.jsp?formId=<%= forms.get(i).getTenderFormId()%>&userId=<%=comUserId%>&nture=<%=comProcNature%>&tenId=<%=commonTenderId%>&procMtd=<%=comProcMethod%>', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>
                           <%}else{out.print("-");}%>
                        </td>
                    
                    <%
                        boolean isMapped = false;
                            if(comDoc!=null && !comDoc.isEmpty()){
                                for(int j=0;j<comDoc.size();j++){
                                    if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                                        isMapped = true;
                                    }
                                }
                            }if(isMapped){
                    %>
                    
                        <td>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                        <th width="85%">Document Name </th>
                        <th width="10%" class="t-align-center">Action</th>
                        </tr>
                        <%for(int j=0;j<comDoc.size();j++){
                        
                                    if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                        %>
                                        <tr>
                                            <td><%=comDoc.get(j)[1]%></td>
                                            <%--<%if(!request.getSession().getAttribute("userTypeId").equals("2")){%>
                                                <td class="t-align-center"><a href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=comDoc.get(j)[1]%>&fileLen=<%=comDoc.get(j)[2]%>&userId=<%=comUserId%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                             <%}else{%>--%>
                                                <td class="t-align-center"><a view='link' href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=comDoc.get(j)[1]%>&fileLen=<%=comDoc.get(j)[2]%>&uIdDoc=<%=comUserId%>&docUid=<%=comUserId%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                        </tr>
                                    <%}
                        }%>
                        
                    </table>
                        </td></tr>
                    <%}/*Mapped Doc List*/else{%>
                    <td>-</td></tr>
                    <%}%>
                <%}/*Cancled form is not displayed*/}/*Form loop Ends here*/%>
                </table>
                <%}/*Form Condition Ends here*/%>
              
                <%}/*For loop ends of tenderSection*/%>
                </td></tr>
            
                <%
                    if(listDocs!=null && !listDocs.isEmpty()){
                %>
                <tr><td colspan="2">
                  <table width="100%" cellspacing="0" class="tableList_1">
                      <tr>
                          <td class="ff" colspan="3">Tender/Proposal Document</td>
                      </tr>
                        <tr>
                        <th width="40%">Document Name</th>
                        <th width="50%">Document Description</th>
                        <th width="10%" class="t-align-center">Action</th>
                </tr>
                <%
                for (TblTenderSection tts : tSections){
                    comDisp++;
                    for(TblTenderSectionDocs ttsd : listDocs){
                        
                        str_FolderName = tts.getSectionName().replace("&", "^");
                        str_FolderName = "Section"+comDisp+"_"+str_FolderName;
                        if(ttsd.getTenderSectionId() == tts.getTenderSectionId()){%>
                                        <tr>
                                            <td><%=ttsd.getDocName()%></td>
                                            <td><%=ttsd.getDescription()%></td>
                                            <td class="t-align-center"><a view='link' href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=ttsd.getDocName()%>&docSize=<%=ttsd.getDocSize()%>&tenderId=<%=commonTenderId%>&lotNo=<%=strforDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                        </tr>
                        <%}
                    }//End of TenderSectionDoc Loop%>
                    <%}%>
                </table>
                    </td></tr>
                <%}%>
                </table>
        </div>
    </body>   
</html>
