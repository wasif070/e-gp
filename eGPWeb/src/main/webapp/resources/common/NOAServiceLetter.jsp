<%-- 
    Document   : NOAServiceLetter
    Created on : Jan 3, 2012, 3:57:55 PM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <body>
        <script src="<%=request.getContextPath()%>/resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        
        <%
        String cmNoaIssueId = request.getParameter("noaId");
        String dop = request.getParameter("dop");
        String tendBrife = request.getParameter("brief");
        String fundDetail = request.getParameter("amt");
        String contractSignDay = request.getParameter("contractSignDay");
        String perSecAmt = request.getParameter("perSecAmt");
        String perSecDays = request.getParameter("perSecDays");
        String perSecDt = request.getParameter("perSecDt");
        
        CommonSearchDataMoreService comcommonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> subContractor = comcommonSearchDataMoreService.geteGPData("getSubContractor", cmNoaIssueId);
         %>
        
        <p> 
            This is to notify you that your proposal
            <strong> 
             <%
                    if(subContractor!=null && !subContractor.isEmpty()){
                        StringBuffer sb = new StringBuffer();
                        for(int i=0;i<subContractor.size();i++){
                            sb.append(subContractor.get(i).getFieldName1()+",");
                        }
                        //sb.replace(0, sb.length()-1, "");
                        sb.replace(sb.length()-1,1 ,",");
                        out.print(sb);
                    }
             %>
             </strong>
             dated <b><%=dop%></b> have been approved by the competent authority of the government for 
             the contract <b><%=tendBrife.trim().replace("</p>", "").replace("<p>","") %></b> at the total Contract amount including
             all Taxes, VAT, custom duties and all charges of Nu. <b><%=fundDetail%></b>.<br /><br />

             You are thus requested to take following actions : <br /><br />
              <ul style="padding-left: 15px;">
             <li>
             Accept in writing the Notification Letter within 
             <strong><input type="hidden" id="contractSignDays" value="<%=contractSignDay%>"/><label id="contractSignDaysWords"></label> <em>(<%=contractSignDay%>) </strong></em> 
             working days.</li><br />
        <%if(perSecDt!=null && !"0.00 (Taka )".equalsIgnoreCase(perSecAmt)){%>
        <br />
        <li>
             Furnish a Performance Security in the specified format and in the amount of Nu. 
             <em><strong><%=perSecAmt%></strong></em> <Amount in words>, within <strong><input type="hidden" id="perSecDays" value="<%=perSecDays%>"/><label id="perSecDaysWords"></label><em> (<%=perSecDays%>)</strong></em> days of acceptance 
             of this Letter of Acceptance but not later than <em><strong><%=perSecDt%></strong></em>.
          </li><br />
          <script type="text/javascript">
              document.getElementById('perSecDaysWords').innerHTML =WORD(document.getElementById('perSecDays').value);
          </script>
          <%}%>
          <li>
              Sign the Contract within <strong><input type="hidden" id="contractSignDays" value="<%=contractSignDay%>"/><label id="contractSignDaysWords1"></label> (<%=contractSignDay%>) </strong> days of issuance of this Notification Letter but not later than <b><%=perSecDt%></b><date>.
           </li>
         
         </ul><br />
         <%if(subContractor!=null && !subContractor.isEmpty()){%>
         <p>
            You are requested also to submit the agreement/MOU with all sub-consultants and nominated sub-consultant before signing the Contract.
         <br /><br />
         </p>
         <%}%>
         <p>
            We attach the draft Contract and all other documents for your perusal and signature.
         </p>
          <script src="<%=request.getContextPath()%>/resources/js/form/ConvertToWord.js" type="text/javascript"></script>
    <script type="text/javascript">
        document.getElementById('contractSignDaysWords').innerHTML =WORD(document.getElementById('contractSignDays').value);
        document.getElementById('contractSignDaysWords1').innerHTML =WORD(document.getElementById('contractSignDays').value);
        
    </script>
    </body>
</html>
