<%-- 
    Document   : DebarmentDecisions
    Created on : May 11, 2016, 11:47:20 AM
    Author     : PROSHANTO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Debarment Decisions</title>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" /> 
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
               <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                    <%@include file="AfterLoginTop.jsp" %>
                <% } else { %>
                    <jsp:include page="Top.jsp" ></jsp:include>
                <% } %>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="ff contentArea-Blogin formStyle_1"><!--Page Content Start-->   
                           <div class="pageHead_1">Debarment Decisions</div>
                            <!--Page Content End-->     
                        </td>
                        <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                            
                        <% } else { %>
                            <td width="266">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                        <% } %>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
