<%-- 
    Document   : QuestionAnswer
    Created on : Dec 11, 2010, 3:01:12 PM
    Author     : parag
--%>

<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Queries And Replies</title>
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
               <%-- <%@include file="AfterLoginTop.jsp" %>--%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;
                            int queryId = 1;
                            String viewType = "";
                            String tab = "11";

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("queryId") != null) {
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                            if (request.getParameter("viewType") != null) {
                                viewType = request.getParameter("viewType");
                            }

                            List<SPTenderCommonData> getRepliedQuery = null;                            
                            getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuestion", tenderId, 0);
                            
                            // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            String idType="tenderId";
                            int auditId=tenderId;
                            String auditAction="Bidder View All Query/Answer Clarification from PA";
                            String moduleName=EgpModule.Pre_Tender_Query_Meeting.getName();
                            String remarks="";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">View Queries And Replies</div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%--<%@include file="TenderInfoBar.jsp" %>--%>
                        <div>&nbsp;</div>
                        <% //pageContext.setAttribute("tab",4); %>
                        <%--
                        <%
                           if (suserTypeId == 3)
                           {
                        %>
                        <jsp:include page="../../officer/officerTabPanel.jsp" >
                            <jsp:param name="tab" value="<%=tab%>" />
                        </jsp:include>
                        <%}else{%>
                        <jsp:include page="../../tenderer/TendererTabPanel.jsp" >
                            <jsp:param name="tab" value="10" />
                        <jsp:param name="tenderId" value="<%=tenderId%>" />
                        </jsp:include>
                        <%}%>
                        --%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th class="t-align-left" width="48%">Query</th>
                                <th class="t-align-left" width="48%">Reply</th>
                            </tr>
                            <%if (getRepliedQuery.size() > 0) {%>
                            <%for (int iR = 0; iR < getRepliedQuery.size(); iR++) {%>
                            <tr>
                                <td class="t-align-center"><%=iR + 1%></td>
                                <td class="t-align-left"><%=URLDecoder.decode(getRepliedQuery.get(iR).getFieldName2(),"UTF-8")%>
                                </td>
                                <td class="t-align-left"><%=URLDecoder.decode(getRepliedQuery.get(iR).getFieldName4(),"UTF-8")%></td>
                            </tr>
                            <%}%>
                            <%} else {%>
                            <tr>
                                <td colspan="3" class="t-align-left">No Record Found.</td>
                            </tr>
                            <%}%>
                        </table>                        

                        <div>&nbsp;</div>
                        <!--Dashboard Content Part End-->
                        <!--Dashboard Footer Start-->
                        <%-- <%@include file="Bottom.jsp" %>--%>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
