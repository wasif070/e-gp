<%--
    Document   : Left.jsp
    Created on : Oct 21, 2010, 6:04:11 PM
    Author     : yanki
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvertisementService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<jsp:useBean id="login" class="com.cptu.egp.eps.web.servicebean.LoginSrBean" />
<%
    String str = request.getContextPath();
    String returnvalue = "";
%>
<link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="<%=str%>/resources/css/accordian.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<%=str%>/resources/js/jQuery/jquery.validate.js"></script>
<script src="<%=str%>/resources/js/accordion.js" type="text/javascript"></script>
<script type="text/javascript">
    $j(document).ready(function () {
        $j("#frmLogin").validate({
            rules: {
                emailId: {required: true, email: true},
                password: {required: true}
            },
            messages: {
                emailId: {required: "<div class='reqF_1'>Please enter e-mail ID</div>",
                    email: "<div class='reqF_1'>Please enter valid e-mail ID</div>"
                },
                password: {required: "<div class='reqF_1'>Please enter Password</div>"
                }
            }
        });
    });

    function swHidMailPwd() {
        var nur_MailId = document.getElementById("txtEmailId");
        var nur_Pwd = document.getElementById("txtPassword");
        if ($j.trim(nur_MailId.value) == "") {
            nur_MailId.value = "e-mail ID";
        }
        if ($j.trim(nur_Pwd.value) == "") {
            nur_Pwd.value = "password";
        }
    }
    $j(function () {
        $j("#stadard-documents-message").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                Ok: function () {
                    $j(this).dialog("close");
                }

            }
        });

        $j("#stadard-documents-message").bind("dialogclose", function (event, ui) {
            window.open('http://cptu.gov.bd/STD.aspx', "_blank", "");
        });

    });
    function ClickESTDs()
    {
        $j("#stadard-documents-message").dialog("open");
    }
</script>

<%
    if (request.getParameter("returnValue") != null) {
        returnvalue = request.getParameter("returnValue");
        String msg = "";
        boolean flag = false;

//1=E-mail Verification is pending.
//2=Invalid Password.
//3=e-mail ID Does Not Exists.
        if ("1".equalsIgnoreCase(returnvalue)) {
            msg = "Email verification is pending";
        }
        if ("2".equalsIgnoreCase(returnvalue)) {
            msg = "Invalid Password";
        }
        if ("3".equalsIgnoreCase(returnvalue)) {
            msg = "e-mail ID is not registered with e-GP";
        }
        if ("4".equalsIgnoreCase(returnvalue)) {
            // msg="Your e-mail ID has been locked <br/>due to subsequent failed login <br/>attempts. To reset Password <a href=\""+request.getContextPath()+"/ResetPassword.jsp\">Click Here</a>";
            // Someone has been trying to login into your e-GP account.
            msg = "Your e-mail ID has been locked <br/>due to subsequent failed login <br/>attempts. Please give your correct password.";
        }
        if ("5".equalsIgnoreCase(returnvalue)) {
            msg = " Your profile is yet not approved";
        }
        if ("6".equalsIgnoreCase(returnvalue)) {
            msg = "Your registration validity period is expired. Please renew your registration";
        }
        if ("7".equalsIgnoreCase(returnvalue)) {
            msg = " You are debarred from eGP System";
        }
        if ("8".equalsIgnoreCase(returnvalue)) {
            msg = " Your account is deactivated. Please contact your Admin user";
        }
        if ("9".equalsIgnoreCase(returnvalue)) {
            msg = " Your renewal period has been lapsed  and can't be renewed again. Please register as a new user with new email id";
        }
        if ("error".equalsIgnoreCase(returnvalue)) {
            msg = "Error Occured";
        }
        if ("over".equalsIgnoreCase(returnvalue)) {
            msg = "Maximum limit of requesting verification code has been exceeded.";
        }

        if ("Pass".equalsIgnoreCase(returnvalue)) {
            msg = "Password changed successfully";
            //session.invalidate();
            flag = true;
        }

        if (flag) {
%>
<div id="successMsg" class="responseMsg successMsg" style="text-align: justify; margin-left:17px; margin-right: 2px; margin-top: 15px;"><%=msg%></div>
<%
} else if (request.getSession().getAttribute("userId") == null && !flag) {
%>
<div id="errMsg" class="responseMsg errorMsg" style="text-align: justify; margin-left:17px; margin-right: 2px; margin-top: 15px;"><%=msg%></div>
<%
        }
    }
%>
<%
    String lang = null, userlogin = null, forgotpassword = null, btnlogin = null, registration = null, faq = null, helpdesk = null, egpguide = null, resources = null, debarredtender = null, publicprocplan = null, askprocexpert = null, standardTenderDocument = null;
    String cptu = null, worldbankbangla = null, gedwebsite = null, rebwebsite = null, rhdwebsite = null, bwdbwebsite = null, termscondition = null,
            privacypolicy = null, memschbanks = null, mandocs = null;
    String userregflowchart = null, userregmanualbangla = null, userregmanualenglish = null, winzip = null, pdfviewer = null, downloads = null, externallinks = null, otherlinks = null, regsteps = null, helpstr = null, peuserreg = null;
    String banglaFont = null;
    String requirement = null;

    if (request.getParameter("lang") != null) {
        lang = request.getParameter("lang");
    } else {
        lang = "en_US";
    }

    MultiLingualService multiLingualService = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
    List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang, "left");

    if (!langContentList.isEmpty()) {
        for (TblMultiLangContent tblMultiLangContent : langContentList) {

            if (tblMultiLangContent.getSubTitle().equals("lbl_userlogin")) {
                if ("bn_IN".equals(lang)) {
                    userlogin = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    userlogin = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_forgotpassword")) {
                if ("bn_IN".equals(lang)) {
                    forgotpassword = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    forgotpassword = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_login")) {
                if ("bn_IN".equals(lang)) {
                    btnlogin = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    btnlogin = "Login";
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_registration")) {
                if ("bn_IN".equals(lang)) {
                    registration = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    registration = "Bidder Registration";
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_faq")) {
                if ("bn_IN".equals(lang)) {
                    faq = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    faq = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_helpdesk")) {
                if ("bn_IN".equals(lang)) {
                    helpdesk = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    helpdesk = new String(tblMultiLangContent.getValue());
                }
            }
            //add by dohatec
            if (tblMultiLangContent.getSubTitle().equals("lbl_requirement")) {
                if ("bn_IN".equals(lang)) {
                    requirement = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    requirement = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_egpguide")) {
                if ("bn_IN".equals(lang)) {
                    egpguide = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    egpguide = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_resources")) {
                if ("bn_IN".equals(lang)) {
                    resources = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    resources = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_debarredtender")) {
                if ("bn_IN".equals(lang)) {
                    debarredtender = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    debarredtender = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_cptu")) {

                if ("bn_IN".equals(lang)) {
                    cptu = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    cptu = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_worldbankbangla")) {
                if ("bn_IN".equals(lang)) {
                    worldbankbangla = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    worldbankbangla = "World Bank - Bangladesh";
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_lgedwebsite")) {

                if ("bn_IN".equals(lang)) {
                    gedwebsite = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    gedwebsite = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_rebwebsite")) {
                if ("bn_IN".equals(lang)) {
                    rebwebsite = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    rebwebsite = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_rhdwebsite")) {
                if ("bn_IN".equals(lang)) {
                    rhdwebsite = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    rhdwebsite = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_bwdbwebsite")) {
                if ("bn_IN".equals(lang)) {
                    bwdbwebsite = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    bwdbwebsite = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_termscondition")) {
                if ("bn_IN".equals(lang)) {
                    termscondition = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    termscondition = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_privacypolicy")) {
                if ("bn_IN".equals(lang)) {
                    privacypolicy = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    privacypolicy = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_mandocs")) {
                if ("bn_IN".equals(lang)) {
                    mandocs = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    mandocs = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_memschbanks")) {
                if ("bn_IN".equals(lang)) {
                    memschbanks = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    memschbanks = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_userregflowchart")) {
                if ("bn_IN".equals(lang)) {
                    userregflowchart = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    userregflowchart = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_userregmanualbangla")) {
                if ("bn_IN".equals(lang)) {
                    userregmanualbangla = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    userregmanualbangla = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_userregmanualenglish")) {
                if ("bn_IN".equals(lang)) {
                    userregmanualenglish = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    userregmanualenglish = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_winzip")) {
                if ("bn_IN".equals(lang)) {
                    winzip = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    winzip = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_pdfviewer")) {
                if ("bn_IN".equals(lang)) {
                    pdfviewer = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    pdfviewer = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_mandocs")) {
                if ("bn_IN".equals(lang)) {
                    mandocs = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    mandocs = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_memschbanks")) {
                if ("bn_IN".equals(lang)) {
                    memschbanks = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    memschbanks = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_download")) {
                if ("bn_IN".equals(lang)) {
                    downloads = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    downloads = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_resources")) {
                if ("bn_IN".equals(lang)) {
                    resources = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    resources = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_exernallink")) {
                if ("bn_IN".equals(lang)) {
                    externallinks = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    externallinks = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_otherlinks")) {
                if ("bn_IN".equals(lang)) {
                    otherlinks = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    otherlinks = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_askprocexpert")) {
                if ("bn_IN".equals(lang)) {
                    askprocexpert = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    askprocexpert = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_standardTenderDocument")) {
                if ("bn_IN".equals(lang)) {
                    standardTenderDocument = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    standardTenderDocument = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_publicprocplan")) {
                if ("bn_IN".equals(lang)) {
                    publicprocplan = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    publicprocplan = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_nursteps")) {
                if ("bn_IN".equals(lang)) {
                    regsteps = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    regsteps = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_help")) {
                if ("bn_IN".equals(lang)) {
                    helpstr = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    helpstr = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_peuserreg")) {
                if ("bn_IN".equals(lang)) {
                    peuserreg = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    peuserreg = new String(tblMultiLangContent.getValue());
                }
            }
            if (tblMultiLangContent.getSubTitle().equals("lbl_banglafont")) {
                if ("bn_IN".equals(lang)) {
                    banglaFont = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                } else {
                    banglaFont = new String(tblMultiLangContent.getValue());
                }
            }
        }
    }

    boolean isBeforeLogin = true;
    HttpSession sn = request.getSession();
    if (sn.getAttribute("userTypeId") != null) {
        isBeforeLogin = false;
    }
%>
<table class="leftnavbarContainer">
    <tr>
        <td class="lftNav_1">
            <%//if("y".equals(request.getParameter("succMsg"))){%>
            <!--<div id="succMsg" class="responseMsg successMsg">An e-mail has been sent to your registered e-mail ID for verification. It has to be verified within <%=XMLReader.getMessage("FinalSubMsg")%> from the date of submission otherwise your profile will be removed.</div>-->
            <%//}%>
            <%if(isBeforeLogin){%>
            <form id="frmLogin" method="post" action='<%=str%>/LoginSrBean?action=checkLogin' >
                <%try {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
                        List<TblAdvertisement> ads = advertisementService.findTblAdvertisement("status", Operation_enum.EQ, "published", "location", Operation_enum.EQ, "TopLeft", "expiryDate", Operation_enum.GE, format.parse(format.format(new Date())));
                    if (ads.size() > 0) {%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space" style="margin-top:px;">
                    <tr valign="top">
                        <td width="50%">
                            <div class="" style="width: 96%; height: 150px;">
                                <%if (ads.size() > 1) {%>
                                <marquee style="width: 96%; height: 150px;" behavior="scroll" scrollamount="3" direction="down">
                                    <div class="">
                                        <%for (TblAdvertisement tbl : ads) {
                                                String[] a = tbl.getDimension().split("x");%>
                                        <a href="<%=tbl.getUrl()%>" target="_blank">
                                            <img class="linkIcon_1" title="<%=tbl.getDescription()%>" alt="<%=tbl.getDescription()%>" src="<%=request.getContextPath()%>/MakeAdvertisement?funcName=image&adId=<%=tbl.getAdId()%>"  HEIGHT="<%=a[0]%>" WIDTH="<%=a[1]%>"/>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <%}%>
                                </marquee>
                            </div>
                            <%} else {%>
                            <div class="">
                                <%for (TblAdvertisement tbl : ads) {
                                        String[] a = tbl.getDimension().split("x");%>
                                <a href="<%=tbl.getUrl()%>" target="_blank">
                                    <img class="linkIcon_1" title="<%=tbl.getDescription()%>" alt="<%=tbl.getDescription()%>" src="<%=request.getContextPath()%>/MakeAdvertisement?funcName=image&adId=<%=tbl.getAdId()%>"  HEIGHT="<%=a[0]%>" WIDTH="<%=a[1]%>"/>
                                </a>
                                &nbsp;&nbsp;&nbsp;
                                <%}%>
                            </div>
                            <%}%>
                            </div>
                        </td>
                    </tr>
                </table>
                <%}
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                %>
                <table width="100%" border="0" cellpadding="0" cellspacing="12" class="loginBox t_space" <%if (!isBeforeLogin) {%> style="display: none" <%}%>>

                    <!-- Dohatec Start -->
                    <!--<tr align="center">
                        <a href="/Index.jsp">
                            <img style="width: 245px; height: 70px; margin-left: 2px;" alt="eGP" src="/resources/images/e-GPBannerAnim.gif"/>
                        </a>
                    </tr>-->
                    <!-- Dohatec End -->

                    <tr>
                        <td>
                            <div class="loginBoxHead"><img src="<%= request.getContextPath()%>/resources/images/userLogin.png" alt="" class="linkIcon_1" /><%=userlogin%></div>
                        </td>
                    </tr>
                    <tr>
                        <td><input name="emailId" type="text" class="txtInput_1" id="txtEmailId" style="width:216px;" value="e-mail ID" onfocus="if (this.value == 'e-mail ID') {
                                this.value = '';
                            }" onblur="swHidMailPwd();" /></td>
                    </tr>
                    <tr>
                        <td><input name="password" type="password" class="txtInput_1" id="txtPassword" style="width:216px;" value="password" onfocus="this.value = '';" onblur="swHidMailPwd();" autocomplete="off" /></td>
                    </tr>
                    <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">
                                        <% if ("bn_IN".equals(lang)) { %>
                                        <label class="btnInput_1_bn">
                                            <% } else { %>
                                            <label class="btnInput_1">
                                                <% }%>
                                                <input type="submit" name="Login" id="btnLogin" value="<%=btnlogin%>" onclick="return checkLogin();"/>
                                            </label>
                                    </td>
                                    <td class="txt_1" style="text-align:right;">
                                        <a href="<%=request.getContextPath()%>/PasswordRecovery.jsp" style="font-weight:bold;"><%=forgotpassword%></a>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-top: 20px;">
                                <input name="button2" type="button" 
                                       <% if ("bn_IN".equals(lang)) { %>
                                       class="btnNewReg_bn" 
                                       <% } else { %>
                                       class="btnNewReg"
                                       <% }%>
                                       id="button2" value="<%=registration%>" onclick="window.location.href = '<%= request.getContextPath()%>/BidderRegistration.jsp'"/>

                                <%--out.print(request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/")).replace("https", "http"));--%>
                            </div>

                            <!-- Removed
                            <div style="margin-top: 5px;">
                                <input name="button3" type="button" 
                            <% if ("bn_IN".equals(lang)) { %>
                            class="btnNewReg_bn" 
                            <% } else { %>
                            class="btnNewReg"
                            <% }%>
                            id="button3" value="<%= peuserreg%>"
                            onclick="window.open('<%=request.getContextPath()%>/help/peregistration/pe_registration_process.pdf')"
                            />
                 </div>
                            -->
                        </td>
                    </tr>
                </table>
                            <table width="100%" border="0" cellpadding="0" cellspacing="12" class="loginBox t_space">
                                <tr>
                                    <td>
                                        <a href="<%=request.getContextPath()%>/eLearning.jsp" class="btnForLearning">e-Learning</a>
                                    </td>                                    
                                </tr>                              
                                
                            </table>
                            
            </form>
            <%}%>
            <!-- 
            
                Cut from here
            
            
            -->
            

            <div class="accordion">
                <div class="accordion-section">
                    <a class="accordion-section-title" id="defaultopen" href="#accordion-4"><%=helpstr%></a>
                    <div id="accordion-4" class="accordion-section-content">
                        <div class="linkBox_1">
                            <ul>
                                
                                
                                <!--Disabled
                               <li><a href="<%= request.getContextPath()%>/help/manuals/eGP_NewUserRegManual_Bangla.pdf" target="_blank"><%//=userregmanualbangla%>User Registration Manual - Dzongkha</a></li>
                               <li><a href="<%= request.getContextPath()%>/help/manuals/eGP_NewUserRegManual_English.pdf" target="_blank"><%=userregmanualenglish%></a></li>
                                -->
                                <li><a href="javascript:void(0)"><%//=userregmanualbangla%>User Registration Manual</a></li>
                <!--                <li><a href="#" target="_blank"><%=userregmanualenglish%></a></li>-->

                                <li>
                                    <% if (request.getParameter ( 
                             "lang") != null) {%>
                                    
                                   <a href="<%= request.getContextPath()%>/FAQ.jsp?lang=<%=request.getParameter("lang")%>"><%=faq%></a>
                                  
                                   
                                    <% }

                        
                            else {%>
                                    
                                   <a href="<%= request.getContextPath()%>/FAQ.jsp"><%=faq%></a>

                                    <% }%>

                                </li>
                            </ul>
                        </div>

                    </div><!--end .accordion-section-content-->
                </div><!--end .accordion-section-->





                <div class="accordion-section">
                    <a class="accordion-section-title" href="#accordion-1"><%=downloads%></a>
                    <div id="accordion-1" class="accordion-section-content">
                        <div  class="linkBox_1">
                            <ul>
                                <!--Disabled
                                    <li class="bNone"><a href="http://www.winzip.com/downwz.htm" target="_blank"><%=winzip%></a></li>
                                    <li><a href="http://get.adobe.com/reader/" target="_blank"><%=pdfviewer%></a></li>
                                    <li><a href="/resources/fonts/solaimanlipi.ttf"><%//=banglaFont%>Dzongkha Font</a></li>
                                -->
<!--                                <li class="bNone"><a href="javascript:void(0)" ><%=winzip%></a></li>
                                <li><a href="javascript:void(0)" ><%=pdfviewer%></a></li>
                                <li><a href="javascript:void(0)"><%//=banglaFont%>Dzongkha Font</a></li>-->
                                <li class="bNone"><a href="<%=request.getContextPath()%>/resources/BhutanDocuments/Procurement Rules and Regulations.pdf" >Procurement Rules & Regulations 2009</a></li>
                                <li><a href="<%=request.getContextPath()%>/resources/BhutanDocuments/Goods.pdf" >Standard Bidding Documents (Goods)</a></li>
                                <li><a href="<%=request.getContextPath()%>/resources/BhutanDocuments/Works.pdf">Standard Bidding Documents (SBD) Works</a></li>
                                <li><a href="<%=request.getContextPath()%>/resources/BhutanDocuments/SRFP.pdf">Standard Request for Proposal (SRFP)</a></li>
                            </ul>
                        </div>
                    </div><!--end .accordion-section-content-->
                </div><!--end .accordion-section-->

                <div class="accordion-section">
                    <a class="accordion-section-title" href="#accordion-2"><%=resources%></a>
                    <div id="accordion-2" class="accordion-section-content">
                        <div class="linkBox_1">
                            <ul>
                                <li class="bNone">
                                    <% if (request.getParameter ( 
                             "lang") != null) {%>
                                    <!--Disabled
                                       <a href="<%= request.getContextPath()%>/help/guidelines/eGP_Guidelines.pdf" target="_blank"><%=egpguide%></a>
                                    -->
                                    <a href="javascript:void(0)" ><%=egpguide%></a>
                                    <% }

                        
                            else {%>
                                    <!--Disabled 
                                       <a href="<%= request.getContextPath()%>/help/guidelines/eGP_Guidelines.pdf"  target="_blank" ><%=egpguide%></a>
                                    -->
                                    <a href="javascript:void(0)"   ><%=egpguide%></a>
                                    <% }%>
                                </li>
                                <!--Disabled
                               <li>
                                <% if (request.getParameter ( 
                            "lang") != null) {%>
                                    <a href="<%= request.getContextPath()%>/MandRegDoc.jsp?lang=<%=request.getParameter("lang")%>"><%=mandocs%></a>
                                <% }

                        
                            else {%>
                                    <a href="<%= request.getContextPath()%>/MandRegDoc.jsp"><%=mandocs%></a>
                                <% }%>
                            </li>
                                -->
                                <li>
                                    <% if (request.getParameter ( 
                            "lang") != null) {%>
                                    <a href="javascript:void(0)"><%=mandocs%></a>
                                    <% }

                        
                            else {%>
                                    <a href="javascript:void(0)"><%=mandocs%></a>
                                    <% }%>
                                </li>

                                <!--Code Start by Proshanto Kumar Saha,Dohatec-->
                               
                            
                                <!--Code End by Proshanto Kumar Saha,Dohatec-->
                                <li>
                                    <% if (request.getParameter ( 
                            "lang") != null) {%>
                                    <a href="javascript:void(0)"><%=memschbanks%></a>
                                    <% }

                        
                            else {%>
                                    <a href="javascript:void(0)"><%=memschbanks%></a>
                                    <% }%>
                                </li> 
                                <!--Code Comment Start by Proshanto Kumar Saha,Dohatec-->
                                <%--! if (request.getParameter("lang") != null) {%>
                                   <a href="<%= request.getContextPath() %>/MemScheduleBank.jsp?lang=<%=request.getParameter("lang")%>"><%=memschbanks%></a>
                               <% } else {%>
                                   <a href="<%= request.getContextPath() %>/MemScheduleBank.jsp"><%=memschbanks%></a>
                               <% }--%>
                                <!--Code comment End by Proshanto Kumar Saha,Dohatec-->
                                
                              
                            
                                
                               
                                
                                <li>
                                    <% if (request.getParameter ( 
                            "lang") != null) {%>
                                    <a href="<%= request.getContextPath()%>/eSBDs.jsp" ><%//=standardTenderDocument%>PRR and Standard Bidding Documents</a>
                                    <% } else {%>
                                    <a href="<%= request.getContextPath()%>/eSBDs.jsp" ><%//=standardTenderDocument%>PRR and Standard Bidding Documents</a>
                                    <% }%>
                                </li>
                                
                                <!-- -->
                            </ul>
                        </div>
                    </div><!--end .accordion-section-content-->
                </div><!--end .accordion-section-->

                <div class="accordion-section">
                    <a class="accordion-section-title" href="#accordion-3"><%=externallinks%></a>
                    <div id="accordion-3" class="accordion-section-content">
                        <div class="linkBox_1">
                            <!--<ul>-->
<!--                              <li class="bNone"><a href="javascript:void(0)" ><%//=cptu%></a></li>

                                <li><a href="javascript:void(0)" ><%//=bwdbwebsite%></a></li>
                                <li><a href="javascript:void(0)" ><%//=gedwebsite%></a></li>
                                <li><a href="javascript:void(0)" ><%//=rebwebsite%></a></li>
                                <li><a href="javascript:void(0)" ><%//=rhdwebsite%></a></li>
                                <li><a href="javascript:void(0)" ><%//=worldbankbangla%>World Bank</a></li>
                                 Removed 
                                <li>
                                <%// if (request.getParameter("lang") != null) {%>
                                    <a href="<%//= request.getContextPath()%>/OtherLinks.jsp?lang=<%//=request.getParameter("lang")%>"><%//=otherlinks%></a>
                                <% //}

                        
                          //  else {%>
                                    <a href="<%//= request.getContextPath()%>/OtherLinks.jsp" ><%//=otherlinks%></a>
                                <% //}%>
                            </li>
                                

                                <li>
                                    <% /*if (request.getParameter ( 
                            "lang") != null) { */%>
                                    <a href="javascript:void(0)"><%//=otherlinks%></a>
                                    <% //}

                        
                           // else {%>
                                    <a href="javascript:void(0)" ><%//=otherlinks%></a>
                                    <%// }%>
                                </li>-->
                            <!--</ul>-->
                            <ul>
                                <li><a href="http://www.procurementlearning.org/" target="_blank">Online Procurement Training (MOOC)</a></li>
                                <li><a href="http://www.mof.gov.bt/" target="_blank">Ministry of Finance</a></li>
                                <li><a href="http://www.cdb.gov.bt/" target="_blank">Construction Development Board</a></li>
                                <li><a href="http://www.rim.edu.bt/" target="_blank">Royal Institute of Management</a></li>
                            </ul>
                        </div>
                    </div><!--end .accordion-section-content-->
                </div><!--end .accordion-section-->
            </div><!--end .accordion-->
        </td>
    </tr>
</table>
<div id="stadard-documents-message" title="Instruction" style="display:none;">
    <p>
        Dear User,
    </p><br />
    <p>
        Click on 'Ok' button to see or download Electronic Standard Bidding Documents (eSBD)
    </p><br />
</div>
<script>
    function checkLogin() {
        if ($j('#frmLogin').valid()) {
            $j('#btnLogin').attr("disabled", true);
            document.getElementById("frmLogin").action = "<%=str%>/LoginSrBean?action=checkLogin";
            document.getElementById("frmLogin").submit();
            return true;
        }
    }
</script>


<script type="text/javascript">
        var flag;
        $(function () {
            $('#btnLogin').blur(function () {
                //alert("The input value has changed.");
                
                flag=true;
                $('#Msg').css("color", "red");
                $('#Msg').html("Checking for debarred ...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {emilId: $('#txtEmailId').val(), funName: 'verifyDebar'},
                        function (j)
                        {
                            if (j.toString().indexOf("OK", 0) != -1) {
                                $('#Msg').css("color", "green");
                                $('#errMsg').html('');
                            } else if (j.toString().indexOf("You", 0) != -1) {
                                flag=false;
                                $('#Msg').css("color", "red");
                                $('#errMsg').html('');

                            }
                            //$('span.#Msg').html(j);

                            //alert(document.getElementById('oName'));
                            if (document.getElementById('oName') == null) {
                                $('#Msg').html(j);
                                $('#errMsg').html('');
                            } else {
                                document.getElementById('oName').removeAttribute('id');
                                $('#Msg').html('');
                                $('#errMsg').html('');
                            }
                        });
                
                
            });
        });
        


    </script>

