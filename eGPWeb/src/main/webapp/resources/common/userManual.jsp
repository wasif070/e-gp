<%--
    Document   : userManual.jsp
    Created on : May 27, 2011, 5:59:54 PM
    Author     : darshan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    //out.println(userTypeId);
                    boolean scheduleBankBranchAdmin = false;
                    if (request.getParameter("schAd") != null && !"".equals(request.getParameter("schAd").toString().trim())) {
                        scheduleBankBranchAdmin = (request.getParameter("schAd").toString().equals("true")) ? true : false;
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>User Manual</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <!--Dashboard Header Start-->
            <div class="fixDiv">
                <%@include file="AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                        <tr valign="top">
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr valign="top">
                                        <!-- Success failure -->
                                        <!-- Result Display End-->
                                        <%
                                                    boolean isAO = false, isAU = false, isBOD = false, isCCGP = false, isEECU = false, isHOPE = false, isMinister = false, isPE = false, isSecretary = false, isTEC = false, isTOC = false, isAccountOffice = false;
                                                    boolean isContentAdmin = false, isDevelopment = false, iseGPAdmin = false, isOrgAdmin = false, isPEAdmin = false, isProcExpert = false, isTenderer = false;

                                                    Object objProRoleManual = session.getAttribute("procurementRole");
                                                    String strProRoleManual = "";
                                                    if (objProRoleManual != null) {
                                                        strProRoleManual = objProRoleManual.toString();
                                                    }

                                                    String userTypeManual = session.getAttribute("userTypeId").toString();
                                                    if ("14".equals(userTypeManual)) {
                                                        isEECU = true;
                                                    }
                                                    if ("8".equals(userTypeManual)) {
                                                        isContentAdmin = true;
                                                    }
                                                    if ("6".equals(userTypeManual)) {
                                                        isDevelopment = true;
                                                    }
                                                    if ("1".equals(userTypeManual)) {
                                                        iseGPAdmin = true;
                                                    }
                                                    if ("5".equals(userTypeManual)) {
                                                        isOrgAdmin = true;
                                                    }
                                                    if ("4".equals(userTypeManual)) {
                                                        isPEAdmin = true;
                                                    }
                                                    if ("12".equals(userTypeManual)) {
                                                        isProcExpert = true;
                                                    }
                                                    if ("2".equals(userTypeManual)) {
                                                        isTenderer = true;
                                                    }

                                                    String acc_ck2[] = strProRoleManual.split(",");
                                                    for (int acc_iManual = 0; acc_iManual < acc_ck2.length; acc_iManual++) {
                                                        if (("PE").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isPE = true;
                                                        }
                                                        if (("Secretary").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isSecretary = true;
                                                        }
                                                        if (("Minister").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isMinister = true;
                                                        }
                                                        if (("BOD").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isBOD = true;
                                                        }
                                                        if (("AU").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isAU = true;
                                                        }
                                                        if (("HOPE").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isHOPE = true;
                                                        }
                                                        if (("CCGP").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isCCGP = true;
                                                        }
                                                        if (("AO").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isAO = true;
                                                        }
                                                        if (("TOC/POC").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isTOC = true;
                                                        }
                                                        if (("TEC/PEC").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isTEC = true;
                                                        }
                                                        if (("Accout Officer").equalsIgnoreCase(acc_ck2[acc_iManual])) {
                                                            isAccountOffice = true;
                                                        }
                                                    }
                                        %>
                                        <td>
                                            <div class="pageHead_1">Download User Manual</div>
                                            <div class="t_space">
                                            </div>
                                            <%
                                                        if (userTypeId == 19 || userTypeId == 20) {
                                            %>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/bankmanual/e-GP System User Manual for Bank User.pdf" class="" target="_blank">User Manual for Financial Institution User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/bankmanual/e-GP System User Manual for Bank Admin.pdf"  target="_blank" class="">User Manual for Financial Institution Admin</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Authorized Officer User.pdf"  target="_blank" class="">User Manual for Authorized Officer User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Authorized Users.pdf"  target="_blank" class="">User Manual for Authorized Users</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - BOD User.pdf"  target="_blank" class="">User Manual for BOD User</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - CCGP User.pdf"  target="_blank" class="">User Manual for CCGP User</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - External Evaluation Committee User.pdf"  target="_blank" class="">User Manual for External Evaluation Committee User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Head of Procuring Entity User.pdf"  target="_blank" class="">User Manual for Head of Procuring Agency User</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Minister User.pdf"  target="_blank" class="">User Manual for Minister User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Procuring Entity User.pdf"  target="_blank" class="">User Manual for Procuring Agency User</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Secretary User.pdf"  target="_blank" class="">User Manual for Secretary User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tender Evaluation Committee User.pdf"  target="_blank" class="">User Manual for Tender Evaluation Committee User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tender Opening Committee User.pdf"  target="_blank" class="">User Manual for Tender Opening Committee User</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Content Admin.pdf"  target="_blank" class="">User Manual for Content Admin</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Development Partner Admin - Users.pdf"  target="_blank" class="">User Manual for Development Partner Admin - Users</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - e-GP Admin.pdf"  target="_blank" class="">User Manual for e-GP Admin</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Organization Admin.pdf"  target="_blank" class="">User Manual for Organization Admin</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - PE Admin.pdf"  target="_blank" class="">User Manual for PA Admin</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Procurement Expert Users.pdf"  target="_blank" class="">User Manual for Procurement Expert Users</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Media Users.pdf"  target="_blank" class="">User Manual for Media Users</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tenderer - Consultant.pdf"  target="_blank" class="">User Manual for Bidder - Consultant</a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Account Officer.pdf"  target="_blank" class="">User Manual for Account Officer</a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <% } else {%>

                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <%
                                                                                                            if (userTypeId == 7 && !scheduleBankBranchAdmin) {
                                                %>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/bankmanual/e-GP System User Manual for Bank User.pdf" class="" target="_blank">User Manual for Financial Institution User</a>
                                                    </td>
                                                </tr>
                                                <%          } else if (scheduleBankBranchAdmin || userTypeId == 15) {
                                                %>
                                                <tr>
                                                    <td class="ff t-align-left">

                                                        <a href="../../help/manuals/bankmanual/e-GP System User Manual for Bank Admin.pdf"  target="_blank" class="">User Manual for Financial Institution Admin</a>

                                                    </td>
                                                </tr>
                                                <%                                                                                                    }
                                                %>
                                                <% if (isAO) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Authorized Officer User.pdf"  target="_blank" class="">User Manual for Authorized Officer User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isAU) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Authorized Users.pdf"  target="_blank" class="">User Manual for Authorized Users</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isBOD) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - BOD User.pdf"  target="_blank" class="">User Manual for BOD User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isCCGP) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - CCGP User.pdf"  target="_blank" class="">User Manual for CCGP User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isEECU) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - External Evaluation Committee User.pdf"  target="_blank" class="">User Manual for External Evaluation Committee User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isHOPE) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Head of Procuring Entity User.pdf"  target="_blank" class="">User Manual for Head of Procuring Agency User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isMinister) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Minister User.pdf"  target="_blank" class="">User Manual for Minister User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isPE) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Procuring Entity User.pdf"  target="_blank" class="">User Manual for Procuring Agency User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isSecretary) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Secretary User.pdf"  target="_blank" class="">User Manual for Secretary User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isTEC) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tender Evaluation Committee User.pdf"  target="_blank" class="">User Manual for Tender Evaluation Committee User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isTOC) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tender Opening Committee User.pdf"  target="_blank" class="">User Manual for Tender Opening Committee User</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isContentAdmin) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Content Admin.pdf"  target="_blank" class="">User Manual for Content Admin</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isDevelopment) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Development Partner Admin - Users.pdf"  target="_blank" class="">User Manual for Development Partner Admin - Users</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (iseGPAdmin) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - e-GP Admin.pdf"  target="_blank" class="">User Manual for e-GP Admin</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isOrgAdmin) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Organization Admin.pdf"  target="_blank" class="">User Manual for Organization Admin</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isPEAdmin) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - PE Admin.pdf"  target="_blank" class="">User Manual for PA Admin</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isProcExpert) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Procurement Expert Users.pdf"  target="_blank" class="">User Manual for Procurement Expert Users</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isAccountOffice) {%>
                                                <tr style="display: none">
                                                    <td class="ff t-align-left">
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Account Officer.pdf"  target="_blank" class="">User Manual for Account Officer</a>
                                                    </td>
                                                </tr>
                                                <% }%>
                                                <% if (isTenderer) {%>
                                                <tr>
                                                    <td class="ff t-align-left">
                                                        <% if (request.getParameter("ut").toString().equalsIgnoreCase("m")) {%>
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Media Users.pdf"  target="_blank" class="">User Manual for Media Users</a>
                                                        <% } else {%>
                                                        <a href="../../help/manuals/usermanuals/e-GP System User Manual - Tenderer - Consultant.pdf"  target="_blank" class="">User Manual for Bidder - Consultant</a>
                                                        <% }%>
                                                    </td>
                                                </tr>
                                                <% }%>
                                            </table>
                                            <% }%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>


                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>

    </body>
</html>
