<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.ViewMessageDtBean"%>
<%@page import="java.util.List"%>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Message</title>
        <style type="text/css">
            .butstd{
            }
            .richdisable{

                display: none;
            }
            .foldercomb{

            }


        </style>

        <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript">
            function Validate()
            {
                var validatebool="";
                if(document.getElementById("txt3").value=='')
                {   document.getElementById("to").innerHTML="<br/>Please Enter e-mail ID.";
                    validatebool="false";
                }
                if(document.frmviewmsg.resubject.value=='' || document.frmviewmsg.fwsubject.value=='' )
                {

                     document.getElementById("sub").innerHTML="<br/>Please enter Subject.";
                    validatebool="false";
                } else {
                     document.getElementById("sub").innerHTML="";
                }
                if (validatebool=='false'){
                    return false;
                }
            }
            function clearTo(){
                if(document.getElementById("txt3").value!='')
                {
                    document.getElementById("to").innerHTML="";
                }
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#sendbutton").hide();
                //$(".formTxtBox_1").hide();
                $("#txt1").hide();
                $("#txt2").hide();
                $("#txt3").hide();
                $("#txt4").hide();
                $("#txt5").hide();
                $("#txt6").hide();
                $("#txtfw").hide();
                $("#send").hide();
                $("#button4").click(function(){
                    $(".butstd").hide();
                    $("#sendbutton").show();
                    $("#send").show();
                    $(".richdisable").show();
                    //$(".formTxtBox_1").show();
                    $("#txt1").show();
                    $("#txt2").show();
                    //$("#txt3").show();
                    $("#strReply").show();
                    $("#txt4").show();
                    $("#txt5").show();
                    $("#txt6").show();
                    $("#date").hide();
                    $("#cc").hide();
                    $("#txt3").attr("disabled", "disabled");
                    $("#msg").hide();
                    $("#msg2").hide();
                    $("#msg3").hide();
                    $("#msg4").hide();
                    $("#msg5").hide();
                    $("#msg6").hide();
                    $("#msg7").hide();
                    $(".foldercomb").hide();
                    $("#headermsg").html("Reply to message");
    
                });

                $("#button5").click(function(){
                    $(".butstd").hide();
                    $("#sendbutton").show();
                    $(".richdisable").show();
                    $("#send").show();
                    $("#txt1").hide();
                    $("#txt2").show();
                    $("#txt3").show();
                    $("#txt4").show();
                    $("#txt5").show();
                    $("#txt6").show();
                    //$(".formTxtBox_1").show();
                    $("#txt3").attr("disabled", "disabled");
                    $("#txt4").attr("disabled", "disabled");
                    $("#date").hide();
                    $("#msg").hide();
                    $("#msg2").hide();
                    $("#msg3").hide();
                    $("#msg4").hide();
                    $("#msg5").hide();
                    $("#msg6").hide();
                    $("#msg7").hide();
                    $(".foldercomb").hide();
                    $("#headermsg").html("Reply to message");
                });
                $("#button6").click(function(){
                    $(".butstd").hide();
                    $("#sendbutton").show();
                    //$(".formTxtBox_1").show();
                    $("#txtfw").show();
                    $("#send").show();
                    $("#txt1").hide();
                    $("#txt3:input").attr('disabled',false);
                    $("#txt4:input").attr('disabled',false);
     
                    $("#txt2").hide();
                    $("#txt3").val("");
                    $("#txt4").val("");
                    $("#txt3").show();
                    $("#txt4").show();
                    $("#trfrom").hide();
                    $("#txt6").show();
                    $("#txtfw").show();
                    $("#date").hide();
                    $("#msg").hide();
                    $("#msg2").hide();
                    $("#msg3").hide();
                    $("#msg4").hide();
                    $("#msg5").hide();
                    $("#msg6").hide();
                    $("#msg7").hide();
                    $(".foldercomb").hide();
                    $(".richdisable").show();
                    $("#headermsg").html("Forward message");
    
                });

   
            });



        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmviewmsg').submit(function(){
                    if( $('span.#mailMsg').html()=="OK" && $('span.#mailMsg1').html() == "OK")
                        return true;
                    else if($('#txt4').val() == "" &&  $('span.#mailMsg').html()=="OK")
                        return true;
                    else if($('#txt4').val() == "" &&  $('span.#mailMsg').html()=="" && $('#txt3').val()!="" )
                        return true;
                    else return false;

                });
            });


            $(function() {
                $('#txt3').blur(function() {
                    if($('#txt3').val() != ""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#txt3').val(),funName:'verifyMulitipleMails'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg').css("color","green");
                         
                            }
                            else if(j.toString().indexOf("Mail", 0)!=-1){
                                $('span.#mailMsg').css("color","red");
                       
                            }
                            $('span.#mailMsg').html(j);
                        });
                    }else{
                        $('span.#mailMsg').html("");
                    }
                });

                $('#txt4').blur(function() {
                    if($('#txt4').val() !=""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#txt4').val(),funName:'verifyMulitipleMails'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg1').css("color","green");

                            }
                            else if(j.toString().indexOf("Mail", 0)!=-1){
                                $('span.#mailMsg1').css("color","red");

                            }
                            $('span.#mailMsg1').html(j);
                        });
                    }else{
                        $('span.#mailMsg1').html("");
                    }
                });

            });        
        </script>
        <script type="text/javascript" language="Javascript">
            function getvalue(val){
                // var butval = document.getElementById("button3").value;
                //alert(val);
                document.frmviewmsg.buttonvalue.value = val;
                if(document.frmviewmsg.buttonvalue.value == "ReplyToAll" ||document.frmviewmsg.buttonvalue.value == "Reply")
                {document.getElementById("hidden1").value = "true";}                
            }
            function disablerich(){
                if(document.getElementById("cke_textArea1")) {
                var v = document.getElementById("cke_textArea1");
                v.style.display = none;
                }
            }

    

        </script>
    </head>
    <body onload="disablerich()">
        <%

                    int uid = 0;
                    if (session.getAttribute("userId") != null) {
                        Integer ob1 = (Integer) session.getAttribute("userId");
                        uid = ob1.intValue();
                    }

                    //Integer uid = 1;
                    String buttonType = null;
                    String messageid = null;
                    String date = null;
                    String from = null;
                    String to = null;
                    String cc = null;
                    String subject = null;
                    String priority = null;
                    String txtarea = null;
                    String status = "Live";
                    String reply = "NO";
                    Integer totalpages = 1;
                    Integer pageNum = 1;
                    Integer noOfRecords = 50;
                    Integer folderId = 0;
                    String searching = "";
                    String keyword = "";
                    String emailId = "";
                    String dateFrom = "";
                    String dateTo = "";
                    String viewMsg = "YES";
                    Integer msgId = 0;
                    String procURL = "";
                    String action = null;
                    String msgStatus = "";
                    Integer folderid = 0;
                    String msgReadstatus = "";
                    String isMsgReply = "";
                    String msgDocId = "";
                    Integer updMsgId = 0;
                    String msgBoxType = null;
                    String listMsgBoxType = null;
                    String msgBType = null;
                    boolean check = false;
                    Integer messageInboxId = 0;
                    String folName = null;
                    TblLoginMaster tbllogin = null;
                    MessageProcessSrBean mpsb = new MessageProcessSrBean();
                    mpsb.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    mpsb.setLogUserId(""+uid);
                    List folderData = mpsb.getFolderDataByUserId(uid);
                    request.setAttribute("fdata", folderData);
                    buttonType = request.getParameter("buttonvalue");

                    String mboxid = request.getParameter("messageInboxid");

                    if (!"Reply".equals(buttonType) && !"ReplyToAll".equals(buttonType)
                            && !"Forward".equals(buttonType) && !"Move to Folder".equals(request.getParameter("movetofolder"))
                            && mboxid != null) {
                        mpsb.updateReadMessage("update tbl_MessageInBox SET isMsgRead='Yes' where msgBoxId=" + mboxid);
                    }

                    if ("Trash".equals(request.getParameter("button3"))) {
                        buttonType = request.getParameter("button3");
                        action = "Trash";
                        msgStatus = "Trash";
                        isMsgReply = "NO";
                        msgReadstatus = "Yes";
                        Object ob = session.getAttribute("msgboxtype");
                        listMsgBoxType = ob.toString();
                        msgBType = "Inbox";
                        Object mbId = session.getAttribute("msgInboxid");
                        messageInboxId = Integer.valueOf(mbId.toString());
                        check = true;
                    } else if ("Reply".equals(buttonType)) {
                        reply = "YES";
                        action = "Reply";
                        msgStatus = "Live";
                        isMsgReply = "YES";
                        msgReadstatus = "Yes";
                        msgBType = "Inbox";
                        check = true;
                        cc = "";
                    } else if ("ReplyToAll".equals(buttonType)) {
                        action = "Reply To All";
                        msgStatus = "Live";
                        isMsgReply = "YES";
                        msgReadstatus = "Yes";
                        msgBType = "Inbox";
                        check = true;
                        cc = request.getParameter("txtcc");
                    } else if ("Forward".equals(buttonType)) {
                        action = "Forward";
                        msgStatus = "Live";
                        isMsgReply = "YES";
                        msgReadstatus = "Yes";
                        check = true;
                        msgBType = "Inbox";
                        cc = request.getParameter("txtcc");
                    } else if ("Move to Folder".equals(request.getParameter("movetofolder"))) {
                        action = "Update";
                        msgStatus = "Live";
                        isMsgReply = "No";
                        msgReadstatus = "Yes";
                        check = true;
                        msgBType = "Inbox";
                        Object ob1 = session.getAttribute("msgInboxid");
                        messageInboxId = Integer.valueOf(ob1.toString());
                        String fidname = request.getParameter("select");
                        String[] fidAndName = fidname.split("_");
                        folderid = Integer.valueOf(fidAndName[0]);
                        folName = fidAndName[1];
                        cc = request.getParameter("txtcc");
                    }

                    messageid = request.getParameter("messageid");
                    if (messageid != null) {
                        msgId = Integer.valueOf(messageid);
                        if("InboxUnReadpre".equals(request.getParameter("messageBoxType"))){
                            msgBoxType = "Inboxpre";
                        }else{
                            msgBoxType = request.getParameter("messageBoxType");
                        }
                        session.setAttribute("msgboxtype", msgBoxType);
                        String msgInboxid = request.getParameter("messageInboxid");
                        session.setAttribute("msgInboxid", msgInboxid);
                    }
                    if (check) {
                        date = request.getParameter("Date");
                        from = request.getParameter("txtfrom");
                        if("Reply".equalsIgnoreCase(buttonType)){
                            to = request.getParameter("txttoReply");                        
                        }
                        else if("Forward".equals(buttonType))
                        {
                            to = request.getParameter("to");
                        }
                        else{
                         to = request.getParameter("txtto");                        
                        }
                        if("true".equalsIgnoreCase(request.getParameter("hiddenname")))
                        {   
                            subject = request.getParameter("resubject");
                        }
                        else
                        {   
                            subject = request.getParameter("fwsubject");
                        }
                        priority = request.getParameter("priority");
                        txtarea = request.getParameter("textArea");
                    }

                    List listData = null;
                    if (msgBoxType != null) {
                        if (msgBoxType.equals("Inboxpre") || msgBoxType.equals("Sentpre")
                                || msgBoxType.equals("Trashpre")) {

                            //msgBType = msgBoxType.substring(0, msgBoxType.length() - 3);
                            msgBType = msgBoxType;
                            if (msgBType.equalsIgnoreCase("Trashpre")) {
                                status = "Trash";
                            }
                            if(request.getParameter("folderId")!= null && !request.getParameter("folderId").equals("0")) {
                                folderId = Integer.parseInt(request.getParameter("folderId").toString());
                            }
                            listData = mpsb.getInboxData(msgBType, uid, status, totalpages,
                                    pageNum, noOfRecords, folderId, searching,
                                    keyword, emailId, dateFrom, dateTo, viewMsg, msgId, "creationDate", "asc","eq");
                        }
                    } else {
                        if ("Trash".equals(request.getParameter("button3"))) {
                            int s = subject.indexOf(":");
                            subject = subject.substring(s + 1);
                        }
                        if ("Move to Folder".equals(request.getParameter("movetofolder"))) {
                            int s = subject.indexOf(":");
                            subject = subject.substring(s + 1);
                        }
                        listData = mpsb.composeMail(uid, to, from, cc, subject, priority,
                                txtarea, procURL, msgBType, action, msgStatus,
                                folderid, msgReadstatus, isMsgReply, msgDocId, updMsgId, messageInboxId);

                        if (listData != null) {
                            CommonMsgChk msgchk = (CommonMsgChk) listData.get(0);
                            if (msgchk.getFlag() == true) {
                                request.setAttribute("status", listData);
                                if (buttonType.equals("Trash")) {
                                    msgBType = "Trash";
                                    response.sendRedirect("InboxMessage.jsp?inbox=" + msgBType + "&click=Trash&status=" + listData);
                                } else if ("Move to Folder".equals(request.getParameter("movetofolder"))) {
                                    msgBType = "Folder";
                                    response.sendRedirect("InboxMessage.jsp?inbox=" + msgBType + "&folderid=" + folderid + "&foldername=" + folName + "&click=moveto&status=" + listData);
                                } else {
                                    msgBType = "Sent";
                                    response.sendRedirect("InboxMessage.jsp?inbox=" + msgBType + "&click=Send&status=" + listData);
                                }


                            }
                        }
                    }
                    ViewMessageDtBean viewMessageDataBean = new ViewMessageDtBean();
                    if (msgBoxType != null) {

                        ViewMessageDtBean viewMesageDatBean = mpsb.addToViewMessageDtBean(listData, viewMessageDataBean);
                        request.setAttribute("msgdata", viewMesageDatBean);

                        List loginmastor = mpsb.getLoginmailIdByuserId(uid);
                        if (loginmastor != null) {
                            tbllogin = (TblLoginMaster) loginmastor.get(0);
                            request.setAttribute("tbllogin", tbllogin);
                        }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <table width="100%" cellspacing="0">
                <tr valign="top">
                    <td class="lftNav"><%--<ul class="lftLinkBtn_1"></ul>--%>
                        <!-- Left content of the page -->
                        <jsp:include page="MsgBoxLeft.jsp" ></jsp:include>
                    </td>
                    <td class="contentArea"><div class="pageHead_1"><label id="headermsg">View Message</label></div>


                        <form name="frmviewmsg" action="ViewMessage.jsp" method="post">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 t_space" width="100%" style="border: 1px solid #a9db80;">
                                <tr class="butstd">
                                    <td height="40" valign="middle" colspan="3" align="left" style="background-color: #a9db80; vertical-align: middle; padding-left: 5px; ">
                                        <%  Object ob = session.getAttribute("msgboxtype");

                                                                if ("Inboxpre".equals(ob.toString())) {%>
                                        <label class="formBtn_1">
                                            <input type="submit" name="button3" id="button3" value="Trash" /></label>&nbsp;
                                        <label class="formBtn_1"><input type="button" name="button3" id="button4" onclick="getvalue('Reply')"  value="Reply" /></label>&nbsp;
                                        <label class="formBtn_1"><input type="button" name="button3" id="button5" onclick="getvalue('ReplyToAll')" value="Reply To All" /></label>&nbsp;
                                        <label class="formBtn_1"><input type="button" name="button3" id="button6" onclick="getvalue('Forward')" value="Forward" /></label>&nbsp;
                                        <input type="hidden" value="false" id="hidden1" name="hiddenname"/>
                                        <%

                                                                                                            if (folderData.size() > 0) {
                                        %>
                                        <select  name="select" class="foldercomb" id="select" style="width:100px;" >
                                            <c:forEach items="${fdata}" var="fd" varStatus = "status">
                                                <option value="${fd.msgFolderId}_${fd.folderName}">${fd.folderName}</option>
                                            </c:forEach>
                                        </select> &nbsp;
                                        <label class="formBtn_1"><input type="submit" name="movetofolder" id="button7" value="Move to Folder" /></label>
                                        <%  }
                                                                }//else if ("Trashpre".equals(ob.toString())) { // Modified By Dohatec to restrict the message delete from trash
                                                                else if ("Trashpre1".equals(ob.toString())) {%>
                                                                <span class="anchorLink">
                                                                    <a  href="<%=request.getContextPath()%>/CommonServlet?funName=MsgBoxDelete&messageid=<%=messageid%>" style="text-decoration: none; color: #fff;" onclick="return confirm('Do you really want to delete Permanently it?');">Delete</a>&nbsp;
                                                                </span>
                                                                <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff email-LeftPart" style="height: 25px; padding-top: 10px; width: 10%;">Subject :</td>
                                    <td colspan="2" class="email-RightPart" style="height: 25px; padding-bottom: 10px; width: 90%;">
                                        <div class="email-Subject c-alignment-left t_space">
                                            <input type="text" value="Re: <% out.println(viewMessageDataBean.getSubject());%>" style="width:400px;" name="resubject" class="formTxtBox_1" id="txt5"  />
                                            <input type="text" value="Fw: <% out.println(viewMessageDataBean.getSubject());%>" style="width:400px;" name="fwsubject" class="formTxtBox_1" id="txtfw" />
                                            <label id="msg5" ><% out.println(viewMessageDataBean.getSubject());%></label>
                                            <span id="sub" style="color: red; font-weight:normal; font-size: 12px;">&nbsp;</span>
                                        </div>
                                        <div id="date" class="c-alignment-right ff t_space">
                                            <input value="  <%
                                                                    String str3 = "";
                                                                    if (viewMessageDataBean.getViewMsgDate() != null) {
                                                                        str3 = DateUtils.gridDateToStr(viewMessageDataBean.getViewMsgDate());
                                                                        out.print(str3);
                                                                    }
                                                   %>" type="text" style="width:400px;" name="Date" id="txt1"  class="formTxtBox_1" readonly>

                                            </input>
                                            <label id="msg" > <%

                                                                    out.print(str3);%></label>
                                        </div>
                                    </td>
                                    
                                </tr>
                                <%--<tr id="date">
                                    <td class="ff" width="90px">Date & Time :</td>

                                    <td height="30" colspan="3" align="right" class="ff email-TopPart">
                                        <input value="  <%
                                                                String str3 = "";
                                                                if (viewMessageDataBean.getViewMsgDate() != null) {
                                                                    str3 = DateUtils.gridDateToStr(viewMessageDataBean.getViewMsgDate());
                                                                    out.print(str3);
                                                                }
                                               %>" type="text" style="width:400px;" name="Date" id="txt1"  class="formTxtBox_1" readonly>

                                        </input>
                                            <label id="msg" > <%

                                                                    out.print(str3);%></label>
                                    </td>

                                </tr>--%>

                                <tr id="trfrom">
                                    <td width="10%" class="ff email-LeftPart" height="25">From : </td>
                                       <%
                                           String mystr = viewMessageDataBean.getCc().trim();
                                       %>
                                       <%
                                        String strcc ="";
                                        String fromstr = tbllogin.getEmailId();
                                        String ccstr = viewMessageDataBean.getCc();
                                        String arraystr[] = ccstr.split(",");
                                        List test = new ArrayList();
                                        for(int i =0; i<arraystr.length; i++)
                                        {   test.add(arraystr[i]);
                                        }
                                        for(int i =0; i<arraystr.length; i++){
                                                if(fromstr.equals(arraystr[i])){
                                                    test.remove(arraystr[i]);
                                             }
                                        }
                                        for(Object mail : test){
                                            strcc+=mail.toString()+",";
                                        }
                                        if(strcc!=null && strcc!="")
                                        {
                                            viewMessageDataBean.setCc(strcc.substring(0,strcc.length()-1));
                                        }
                                        %>
                                    <td width="90%" colspan="2" class="email-RightPart">
                                        <input  value="<%out.println(tbllogin.getEmailId());%>" type="text" style="width:400px;" name="from" id="txt2" class="formTxtBox_1" disabled />

                                        <input type="hidden" name="txtfrom" id="txtfrom" value=" <%out.println(tbllogin.getEmailId());%>" />
                                        <label id="msg2" > <% out.println(viewMessageDataBean.getFrom().trim());%></label>
                                    </td>

                                </tr>
                                <%
                                        String str_replyName = viewMesageDatBean.getFrom().trim();
                                        String stringto ="";
                                        String tostring = tbllogin.getEmailId();
                                        String gettostring = viewMessageDataBean.getTo();
                                        String arraystring[] = gettostring.split(",");
                                        List testlist = new ArrayList();
                                        for(int i =0; i<arraystring.length; i++)
                                        {   testlist.add(arraystring[i]);
                                        }
                                        for(int i =0; i<arraystring.length; i++){
                                                if(tostring.equals(arraystring[i])){
                                                    testlist.remove(arraystring[i]);
                                             }
                                        }
                                        for(Object mailobj : testlist){
                                            stringto+=","+mailobj.toString();
                                        }
                                        String joinstrto = stringto.substring(0,stringto.length());
                                        String stringgetfrom = viewMessageDataBean.getFrom()+joinstrto;
                                        viewMesageDatBean.setFrom(stringgetfrom);
                                        %>
                                <%  if (viewMessageDataBean.getTo() != null) {%>
                                <tr>
                                    <td height="20" class="ff email-LeftPart" width="10%">To :</td>

                                    <td colspan="2" class="email-RightPart" width="90%">
                                        <input value="<% out.println(viewMessageDataBean.getFrom());%>" type="text" style="width:400px;" name="to" id="txt3" class="formTxtBox_1" onblur="clearTo();"/>
                                        <input value="<% out.println(str_replyName);%>" type="text" style="width:400px;display: none;" name="to" id="strReply" class="formTxtBox_1" onblur="clearTo();"/>
                                        <input type="hidden" name="txtto" id="txtto" value="<% out.println(viewMessageDataBean.getFrom());%>" />
                                        <input type="hidden" name="txttoReply" id="txtto" value="<% out.println(str_replyName);%>" />
                                        

                                        <label id="msg3" ><% out.println(viewMessageDataBean.getTo().trim());%></label>
                                        <span id="mailMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                        <span id="to" style="color: red;">&nbsp;</span>
                                    </td>
                                    
                                </tr>
                                
                                <% }
                                                        if (viewMessageDataBean.getCc() != null) {%>
                                <tr id="cc">
                                    <td height="20" class="ff email-LeftPart" width="10%">Cc :</td>

                                    <td colspan="2" class="email-RightPart" width="90%">
                                        <input  type="text" name="cc" style="width:400px;" value="<%out.println(viewMessageDataBean.getCc());%>" id="txt4" class="formTxtBox_1" disabled/>

                                        <label id="msg4" ><%= mystr %></label>
                                        <input type="hidden" name="txtcc" id="txtcc" value="<%out.println(viewMessageDataBean.getCc());%>" />
                                        <span id="mailMsg1" style="color: red; font-weight: bold">&nbsp;</span>
                                    </td>

                                </tr>
                                
                                <% }%>

                                <tr>
                                    <td  class="email-LeftPart ff" width="10%">Priority :</td>

                                    <td colspan="2" class="email-RightPart" width="90%" > <label id="msg6" ><% out.println(viewMessageDataBean.getPriority());%></label>
                                        <select id="txt6" name="priority" class="formTxtBox_1">
                                            <%
                                                                    String[] str = {"Low", "Medium", "High"};
                                                                    for (int i = 0; i < 3; i++) {
                                                                        if (str[i].equalsIgnoreCase(viewMessageDataBean.getPriority())) {
                                                                            out.write("<option selected='true' value='" + viewMessageDataBean.getPriority() + "'>" + viewMessageDataBean.getPriority() + "</option>");
                                                                        } else {
                                                                            out.write("<option value='" + str[i] + "'>" + str[i] + "</option>");
                                                                        }
                                                                    }
                                            %>

                                        </select>

                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td colspan="3" height="15"></td>
                                </tr>

                                <tr>
                                    <%--<td class="ff" width="90px">Message Text :</td>--%>
                                    <td width="100%" colspan="3" class="PaddLeft" id="msg7"><% out.println(viewMessageDataBean.getTextArea());%></td>
                                    <td class="richdisable" colspan="3"><textarea name="textArea"  rows="3" id="txt7" style="display:none; width:600px; height:300px" ><% out.println(viewMessageDataBean.getTextArea());%>
                                        </textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[

                                            CKEDITOR.replace( 'textArea',
                                            {
                                                toolbar : "egpToolbar"
                   
                                            });
                   

                                            //]]>
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" height="5"></td>
                                </tr>
                                <!--
                                 <tr>

                                   <td>&nbsp;</td>
                                    <td align="right"><label class="formBtn_1"><input type="submit" name="button3" id="button3" value="Trash" /></label>&nbsp;
                                       <label class="formBtn_1"><input type="button" name="button3" id="button4" onclick="getvalue('Reply')"  value="Reply" /></label>&nbsp;
                                       <label class="formBtn_1"><input type="button" name="button3" id="button5" onclick="getvalue('ReplyToAll')" value="Reply To All" /></label>&nbsp;
                                     <label class="formBtn_1"><input type="button" name="button3" id="button6" onclick="getvalue('Forward')" value="Forward" /></label>&nbsp;

                                     <label class="formBtn_1"><input type="submit" name="movetofolder" id="button7" value="Move to Folder" /></label>

                                      </td>
                                 </tr> -->
                                <tr id="send">
                                    <td colspan="3"> <label  class="formBtn_1">
                                            <input type="submit" id="sendbutton" name="send" value="Send" onclick="return Validate();"/>
                                        </label>
                                    </td>
                                    <input type="hidden" name="buttonvalue" id="buttonvalue" value="null" />
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <% }%>
    <script>
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }        
    </script>    
</html>
