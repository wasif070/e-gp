<%--
    Document   : deBriefView
    Created on : Jun 20, 2011, 5:13:12 PM
    Author     : dixit
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDebriefing"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderDebriefingService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Query and Reply</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body onload="getQueryData('replyQuery')">

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="AfterLoginTop.jsp" %>
            <%
                        response.setHeader("Expires", "-1");
                        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                        response.setHeader("Pragma", "no-cache");
            %>
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%
                                byte suserTypeId = 0;
                                if (session.getAttribute("userTypeId") != null) {
                                    suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                                }
                    %>
                    View Query and Reply

                    <span style="float:right;">
                        <!--              <a href="<a%=referer%>" class="action-button-goback">Go back</a>-->
                    </span>
                </div>
                <%
                            String strtenderID = "";
                            if (request.getParameter("tenderId") != null) {
                                strtenderID = request.getParameter("tenderId");
                            }
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            String debriefId = "";
                            if (request.getParameter("debriefId") != null) {
                                debriefId = request.getParameter("debriefId");
                            }
                            TenderDebriefingService tenderDebriefingService = (TenderDebriefingService) AppContext.getSpringBean("TenderDebriefingService");
                            if (session.getAttribute("userId") != null) {
                                tenderDebriefingService.setLogUserId(session.getAttribute("userId").toString());
                            }

                            // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            String idType = "tenderId";
                            int auditId = Integer.parseInt(request.getParameter("tenderId"));
                            String userType = "Tenderer";
                            if (session.getAttribute("userTypeId") != null && !session.getAttribute("userTypeId").toString().equalsIgnoreCase("2")) {
                                userType = "Officer";
                            } else {
                                userType = "Tenderer";
                            }
                            String auditAction = "View Query/Reply of Debriefing on Tender by " + userType;
                            String moduleName = EgpModule.Evaluation.getName();
                            String remarks = "";
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                            List<TblTenderDebriefing> list = tenderDebriefingService.getDebriefData(Integer.parseInt(debriefId));
                            List<TblTenderDebriefdetailDoc> listDocs = tenderDebriefingService.getDebriefDetailData(Integer.parseInt(debriefId));
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                %>
                <%@include file="TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%
                    if(request.getParameter("fq")!=null)
                    {
                        %>
                        <div  class='responseMsg successMsg t_space'>File Removed Successfully</div><br/>
                        <%
                    }
                %>
                <%
                            if (suserTypeId == 3) {
                %>
                <% pageContext.setAttribute("tab", "7");%>
                <%@include file="../../officer/officerTabPanel.jsp"%>
                <%} else {%>
                <%pageContext.setAttribute("tab", "6");%>
                <%@include file="../../tenderer/TendererTabPanel.jsp" %>
                <jsp:include page="../../tenderer/EvalInnerTendererTab.jsp" >
                    <jsp:param name="EvalInnerTab" value="5" />
                    <jsp:param name="tenderId" value="<%=strtenderID%>" />
                </jsp:include>

                <%}%>
                <%--<%pageContext.setAttribute("tab", "6");%>--%>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="9%" class="t-align-left ff">Query :</td>
                            <td width="84%" class="t-align-left">
                                <%
                                            if (!list.isEmpty()) {
                                                out.print(list.get(0).getQueryText());
                                            }
                                %>
                            </td>
                        </tr>
                    </table>
                    
                        <%      boolean tenside = false;boolean Penside = false;
                                for (TblTenderDebriefdetailDoc objDoc : listDocs) {
                                    if(objDoc.getUserTypeId()==2)
                                    {
                                        tenside = true;
                                    }
                                    if(objDoc.getUserTypeId()==3)
                                    {
                                        Penside = true;
                                    }
                                }
                                if(tenside){
                                    int docCnt = 0;
                                    if(!listDocs.isEmpty()){
                        %>
                                <div class="tableHead_1 t_space" align="left">Bidders Documents</div>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th class="t-align-left" width="23%">File Name</th>
                                    <th class="t-align-left" width="32%">File Description</th>
                                    <th class="t-align-left" width="7%">File Size <br />
                                        (in KB)</th>
                                    <th class="t-align-left" width="28%">File Uploaded by</th>
                                    <th class="t-align-left" width="18%">Action</th>
                                </tr>
                        <%
                                    for (TblTenderDebriefdetailDoc objDoc : listDocs) {
                                        docCnt++;
                                        if(objDoc.getUserTypeId()==2)
                                        {
                        %>
                        <tr>
                            <td class="t-align-center"><%=docCnt%></td>
                            <td class="t-align-left"><%=objDoc.getDocumentName()%></td>
                            <td class="t-align-left"><%=objDoc.getDocDescription()%></td>
                            <td class="t-align-center"><%= new BigDecimal(Double.parseDouble(objDoc.getDocSize()) / 1024).setScale(2, 0)%></td>
                            <td class="t-align-center">
                                <%
                                    if (2 == objDoc.getUserTypeId()) {
                                        out.print("tenderer");
                                    }else{
                                        out.print("PE Officer");
                                    }
                                %>
                            </td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=downloadDocument&tenderId=<%=strtenderID%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>"><img src="../images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                <%
                                    if("2".equalsIgnoreCase(session.getAttribute("userTypeId").toString()))
                                    {
                                    %>
                                        <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=removeDocument&tenderId=<%=strtenderID%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>&DetailDocId=<%=objDoc.getDebDetailDocId()%>"><img src="../images/Dashboard/Delete.png" alt="Remove" /></a>
                                    <%
                                    }
                                %>                                
                            </td>
                        </tr>

                        <%   if (objDoc != null) {
                                objDoc = null;
                            }
                        }}}}
                        %>
                    </table>
                    <div class="t_space">&nbsp;</div><div class="t_space">&nbsp;</div>
                    <%
                                if (!list.isEmpty()) {
                                    if (list.get(0).getReplyText() != null && !"".equalsIgnoreCase(list.get(0).getReplyText())) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="9%" class="t-align-left ff">Reply :</td>
                            <td width="84%" class="t-align-left">
                                <%
                                                    out.print(list.get(0).getReplyText());
                                                }
                                            }
                                %>
                            </td>
                        </tr>
                    </table>
                    <%
                                    if(Penside){
                                    int docCntt = 0;
                                    if(!listDocs.isEmpty()){
                        %>
                                <div class="tableHead_1 t_space" align="left">Officer Documents</div>
                                <table width="100%" cellspacing="0" class="tableList_1 ">
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th class="t-align-left" width="23%">File Name</th>
                                    <th class="t-align-left" width="32%">File Description</th>
                                    <th class="t-align-left" width="7%">File Size <br />
                                        (in KB)</th>
                                    <th class="t-align-left" width="28%">File Uploaded by</th>
                                    <th class="t-align-left" width="18%">Action</th>
                                </tr>
                        <%
                                    for (TblTenderDebriefdetailDoc objDoc : listDocs) {
                                        docCntt++;
                                        if(objDoc.getUserTypeId()==3)
                                        {
                        %>
                        <tr>
                            <td class="t-align-center"><%=docCntt%></td>
                            <td class="t-align-left"><%=objDoc.getDocumentName()%></td>
                            <td class="t-align-left"><%=objDoc.getDocDescription()%></td>
                            <td class="t-align-center"><%= new BigDecimal(Double.parseDouble(objDoc.getDocSize()) / 1024).setScale(2, 0)%></td>
                            <td class="t-align-center">
                                <%
                                    if (2 == objDoc.getUserTypeId()) {
                                        out.print("tenderer");
                                    }else{
                                        out.print("PE Officer");
                                    }
                                %>
                            </td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=downloadDocument&tenderId=<%=strtenderID%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>"><img src="../images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                <%
                                    if("3".equalsIgnoreCase(session.getAttribute("userTypeId").toString()))
                                    {
                                    %>
                                        <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=removeDocument&tenderId=<%=strtenderID%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>&DetailDocId=<%=objDoc.getDebDetailDocId()%>"><img src="../images/Dashboard/Delete.png" alt="Remove" /></a>
                                    <%
                                    }
                                %>
                            </td>
                        </tr>

                        <%   if (objDoc != null) {
                                objDoc = null;
                            }
                        }}}}
                        %>
                    </table>
                </div>
            </div>
        </div>
        <div align="center"><%@include file="Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
