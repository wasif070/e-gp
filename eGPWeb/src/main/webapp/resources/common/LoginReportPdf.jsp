<%-- 
    Document   : LoginReportPdf
    Created on : Feb 14, 2011, 1:08:37 AM
    Author     : Swati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%@page import="com.cptu.egp.eps.service.serviceimpl.LoginReportImpl,com.cptu.egp.eps.dao.storedprocedure.LoginReportDtBean"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Report </title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>

        <script src="../js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
         <%
                int userid = 0;
                int userTypeId1=0;
                String searchDate = null;
                String dateFrom = null;
                String dateTo = null;
                String emailId = null;
                String ipaddress = null;
                Integer page1 = 0;
                Integer limit = 0;
                String TypeUser = null;
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                if(request.getParameter("userid") != null)
                    userid = Integer.parseInt(request.getParameter("userid"));
                if(request.getParameter("usertypeId") != null)
                    userTypeId1 = Integer.parseInt((request.getParameter("usertypeId")));
                if(request.getParameter("searchDate") != null)
                    searchDate = request.getParameter("searchDate");
                if(request.getParameter("dateFrom") != null)
                    dateFrom = request.getParameter("dateFrom");
                if(request.getParameter("dateTo") != null)
                    dateTo = request.getParameter("dateTo");
                if(request.getParameter("emailId") != null)
                    emailId = request.getParameter("emailId");
                if(request.getParameter("ipaddress") != null)
                    ipaddress = request.getParameter("ipaddress");
                if(request.getParameter("page") != null)
                    if("0".equals(request.getParameter("page"))){
                        page1 = 0;
                    }else{
                        page1 = Integer.parseInt(request.getParameter("page"));
                    }
              
                if(request.getParameter("limit") != null)
                    limit = Integer.parseInt(request.getParameter("limit"));
                if(request.getParameter("TypeUser") != null)
                    TypeUser = (request.getParameter("TypeUser"));

                int typeUserInt = Integer.parseInt(TypeUser);
               // page1 = 0;
                 String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
             
    %>
        <form id="frmremark" name="frmremark" action="LoginReportPdf.jsp?userid=<%=userid%>&usertypeId=<%=userTypeId1%>" method="POST">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <%@include file="AfterLoginTop.jsp" %>
                <% } %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Login Report</div>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr valign="top">
                    <td class="contentArea">
                        <div class="formBg_1 t_space">
                            <table cellspacing="5" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <% if(request.getParameter("TypeUser") != "") { %>
                                    <td width="10%" class="ff">Type of user :</td>
                                    <td width="44%">
                                     <%if(typeUserInt==0){%>
                                            All
                                     <%}else if(typeUserInt==2){%>
                                            Bidder/Consultant
                                    <%}else if(typeUserInt==6){%>
                                            DP
                                    <%}else if(typeUserInt==7){%>
                                            Schedule Bank User
                                    <%}else if(typeUserInt==8){%>
                                            Admin User
                                    <%}else if(typeUserInt==3){%>
                                            Government Users
                                    <% } %>
                                    </td>
                                    <% } %>
                                    <% if(request.getParameter("searchDate") != "") { %>
                                    <td width="13%"><span class="ff">Search for a Date : </span></td>
                                    <td width="33%"><%=sdf.format(searchDate)%></td>
                                    <% } %>
                                </tr>
                                <% if(request.getParameter("dateFrom") != "" || request.getParameter("dateTo") != "") { %>
                                <tr>
                                    <% if(request.getParameter("dateFrom") != "") { %>
                                    <td class="ff">Date From : </td>
                                    <td><%=sdf.format(dateFrom)%></td>
                                    <% } %>
                                    <% if(request.getParameter("dateTo") != "") { %>
                                    <td width="13%"><span class="ff">Date To : </span></td>
                                    <td width="33%"><%=sdf.format(dateTo)%></td>
                                    <% } %>
                                </tr>
                                <% } %>
                                 <% if(request.getParameter("emailId") != "") { %>
                                <tr>
                                    <td class="ff">e-mail ID :</td>
                                    <td><%=emailId%></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                 <% } %>
                            </table>
                        </div>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td class="contentArea">
                         <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                               <th width="4%" class="t-align-center">Sl. No.</th>
                                <th width="32%" class="t-align-center">e-mail ID</th>
                                <th width="22%" class="t-align-center">Log In Date and Time</th>
                                <th width="22%" class="t-align-center">Log Out Date and Time</th>
                                <th width="20%" class="t-align-center">IP Address</th>
                            </tr>
                           <% 
                             LoginReportImpl loginReport = (LoginReportImpl) AppContext.getSpringBean("LoginReportImpl");
                             loginReport.setAuditTrail(null);
                             List<LoginReportDtBean> reportDetails = loginReport.loginReportDetails(TypeUser, searchDate, dateFrom, dateTo, emailId, userid, (userTypeId1), ((page1)), limit,ipaddress);

                            for(int i = 0; i < reportDetails.size(); i++){ %>
                            <tr>
                                <td class="t-align-center"><%= reportDetails.get(i).getRowNumber().toString() %></td>
                                <td class="t-align-left"><%= reportDetails.get(i).getEmailId() %></td>
                                <td class="t-align-center"><%= sdf.format(reportDetails.get(i).getSessionStartDt())  %></td>
                                <td class="t-align-center"><%= sdf.format(reportDetails.get(i).getSessionEndDt()) %></td>
                                <td class="t-align-center"><%= reportDetails.get(i).getIpAddress() %></td>
                            </tr>
                           <% }
                           %>
                        </table>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>

            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
            <jsp:include page="Bottom.jsp" ></jsp:include>\
            <% }  %>
            <!--Dashboard Footer End-->
        </div>
        </form>
    </body>
</html>
