<%-- 
    Document   : DebarmentRules
    Created on : May 10, 2016, 12:17:00 PM
    Author     : PROSHANTO
--%>
<%@page import="java.io.File"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Debarment Rules</title>
            <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
            <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" /> 
    </head>
    <body>

        <%--
            Nitish Start 
            Created on : May 3, 2017
        --%>

        <div class="mainDiv">
            <div class="fixDiv">

                <%if (request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) {%>
                <%@include file="AfterLoginTop.jsp" %>

                <%  if (request.getParameter("success") != null && request.getParameter("success").equalsIgnoreCase("1")) {
                %>
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="ff contentArea-Blogin formStyle_1">  
                            <div id="docMsg" class="responseMsg successMsg" >File Uploaded Successfully.</div>
                        </td>
                    </tr>    
                </table>        
                <%
                    }
                %>
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="ff contentArea-Blogin formStyle_1">  
                            <div class="pageHead_1">Upload Debarment Rules</div>
                            <div class="responseMsg noticeMsg"  style='margin-top: 12px;vertical-align: top;'>
                                Once e-GP Admin upload a file it will override on previous file
                            </div>
                        </td>
                    </tr>    
                </table>    
                <form  id="frmUploadDoc" method="post" action="..\..\debarmentRuleUpload" enctype="multipart/form-data" name="frmUploadDoc" ><%-- onsubmit="startProgress()"--%>
                    <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%" >
                        <tr>
                            <td class="ff" width="40%" > Select Debarment Rules Document   : <span>*</span></td>
                            <td width="89%" >
                                <input name="fileUpload" id="fileUpload" type="file" class="formTxtBox_1" style="width:450px; background:none;"/>
                                <br/>
                                Acceptable File Types Only PDF Format
                                <br/>Maximum file size of a single file should not exceed 5 MB.
                                <input type="hidden" value="5" id="fileSize"/>
                                <div id="fileerror" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <label class="formBtn_1" style="visibility:visible;" id="lbUpload" >
                                    <input type="submit" name="upload" id="btnUpload" value="Upload" onclick="return checkfile();"/>
                                </label>                                            
                            </td>
                        </tr>
                    </table>
                </form>

                <%--Nitish END --%>

                <% } else { %>
                <jsp:include page="Top.jsp" ></jsp:include>
                <% } %>

                <%-- File Check Option :: Nitish Oritro--%>
                <%
                    boolean hasFile = false;
                    File index = new File(getServletContext().getRealPath("\\")+"resources\\common\\Debarment_Rules_2013.pdf");
                    if (index.exists()) 
                    {
                        hasFile = true;
                        %>
                        <%--<td><b>There are file Exists:</b></td>--%>
                        <%
                        //index.delete();
                    } 
                    else 
                    {
                        hasFile = false;
                %>
                <%--<td><b>There are No file Exists:</b></td>--%>
                <%
                    }
                %>

                <%-- File Check Option END:: Nitish Oritro--%>

                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="ff contentArea-Blogin formStyle_1"><!--Page Content Start-->   
                            <div class="pageHead_1">Download Debarment Rules</div>     
                            <a href="Debarment_Rules_2013.pdf"  target="_blank" class="" onclick="return fileExists(<%=hasFile%>);">Debarment Rules</a>
                            <!--Page Content End-->     
                        </td>
                        <%if (request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>

                        <% } else { %>
                        <td width="266">
                            <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                        <% }%>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </body>
    </html>

<%--Nitish Start JavaScript--%>
<script type="text/javascript">
    
    function fileExists(hasFile)
    {
        //alert(hasFile);
        if(hasFile == true)
        {
            return true;
        }
        else 
        {
            alert("There are No file Exists:");
            return false;
        }
    }
    
    
    
    function checkfile()
    {
        var fvalue = document.getElementById('fileUpload').value;
        var param = "document";
        var ext = fvalue.split(".");
        var fileType = ext[ext.length - 1];
        fileType = fileType.toLowerCase();


        if (fvalue != "")
        {
            var fileSize = document.getElementById('fileUpload').files[0].size;
            if (fileSize > 5100000)
            {
                document.getElementById('fileerror').innerHTML = "<div class='reqF_1'>File Size Limit Exceeded</div>";
                return false;
            }
        }

        if (param == "document" && fileType == 'pdf')
        {
            var toCheck = fvalue;
            var index1 = toCheck.indexOf("\\");
            var onlyFilePath = toCheck.substring(index1 + 1, toCheck.length);
            var reExp1 = /^[A-Za-z0-9\s\.\-\_\\]+$/
            if (!reExp1.test(onlyFilePath)) {
                document.getElementById('fileerror').innerHTML = "<div class='reqF_1'>A file path may contain only Space, - , _ , \\ special characters !</div>";
                return false;
            }
            return true;

            document.getElementById('fileerror').innerHTML = '';

        } else
        {
            if (fvalue == "") {
                document.getElementById('fileerror').innerHTML = "<div class='reqF_1'>Please select a file.</div>";
                return false;
            } else {
                document.getElementById('fileerror').innerHTML = "<div class='reqF_1'>File type is not allowed.</div>";
                return false;
            }
        }
    }
</script>    


<%--Nitish END --%>