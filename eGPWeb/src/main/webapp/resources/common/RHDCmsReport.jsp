<%-- 
    Document   : index
    Created on : Feb 16, 2012, 12:17:28 PM
    Author     : shreyansh.shah
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSBoQDetails"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.RHDIntegrationCMSDetailsService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RHD CMS OverAll Report</title>
    </head>
    <body>

        <!--Header Table-->

        <noscript>
            <meta http-equiv="refresh" content="">
        </noscript>
        <%
                    Boolean isexport = false;
                    if (request.getParameter("Submit") != null) {
                        response.setContentType("application/vnd.ms-excel");
                        response.setHeader("content-disposition", "attachment; filename=RHDCmsReport.xls");
                        isexport = true;
                    }
        %>

        <form name="frmRHDCms" action="">
            <table border="1"    cellpadding="0" cellspacing="0" width="100%" style="margin-top:21px;" >
                <%
                            if (isexport == false) {
                %>
                <tr>
                    <td align="left" colspan="51"> <input type="Submit" value="Export To Excel" name="Submit">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
                <tr  align="center">
                    <td align="center" bgcolor="#CCFF99">PA Id</td>
                    <td align="center" bgcolor="#CCFF99">PA Name</td>
                    <td align="center" bgcolor="#CCFF99">PA Code</td>
                    <td align="center" bgcolor="#CCFF99">Letter of Acceptance (LOA) Name</td>
                    <td align="center" bgcolor="#CCFF99">TenderId</td>
                    <td align="center" bgcolor="#CCFF99">Procurement Category</td>
                    <td align="center" bgcolor="#CCFF99">Procurement Method</td>
                    <td align="center" bgcolor="#CCFF99">Procurement Type</td>
                    <td align="center" bgcolor="#CCFF99">Budget Type</td>
                    <td align="center" bgcolor="#CCFF99">Source of Funds</td>
                    <td align="center" bgcolor="#CCFF99">Project Code</td>
                    <td align="center" bgcolor="#CCFF99">Project Name</td>
                    <td align="center" bgcolor="#CCFF99">Tender RefNo</td>
                    <td align="center" bgcolor="#CCFF99">Tender Invitation For</td>
                    <td align="center" bgcolor="#CCFF99">Tender PkgNo</td>
                    <td align="center" bgcolor="#CCFF99">Tender PkgDesc</td>
                    <td align="center" bgcolor="#CCFF99">Tender Category</td>
                    <td align="center" bgcolor="#CCFF99">Tender Publish Date</td>
                    <td align="center" bgcolor="#CCFF99">Tender DocLstSelD</td>
                    <td align="center" bgcolor="#CCFF99">Tender PreMeeting Start Date</td>
                    <td align="center" bgcolor="#CCFF99">Tender PreMeeting End Date</td>
                    <td align="center" bgcolor="#CCFF99">Tender Closing Date</td>
                    <td align="center" bgcolor="#CCFF99">Tender Opening Date</td>
                    <td align="center" bgcolor="#CCFF99">Tender SecLstSubDt</td>
                    <td align="center" bgcolor="#CCFF99">Tender Brief Desc</td>
                    <td align="center" bgcolor="#CCFF99">Tender EvaluationType</td>
                    <td align="center" bgcolor="#CCFF99">Tender DocAvailable</td>
                    <td align="center" bgcolor="#CCFF99">Tender DocFees</td>
                    <td align="center" bgcolor="#CCFF99">Tender DocPrice</td>
                    <td align="center" bgcolor="#CCFF99">Tender SecAmt</td>
                    <td align="center" bgcolor="#CCFF99">Tender SecPayMode</td>
                    <td align="center" bgcolor="#CCFF99">Tender SecValidUpToDt</td>
                    <td align="center" bgcolor="#CCFF99">Tender ValidUpToDt</td>
                    <td align="center" bgcolor="#CCFF99">Per SecValidUpToDt</td>
                    <td align="center" bgcolor="#CCFF99">Per SecBankName</td>
                    <td align="center" bgcolor="#CCFF99">Per SecBankBranchName</td>
                    <td align="center" bgcolor="#CCFF99">Per PayAmount</td>
                    <td align="center" bgcolor="#CCFF99">Per PayPaidDt</td>
                    <td align="center" bgcolor="#CCFF99">Per SecPayMode</td>
                    <td align="center" bgcolor="#CCFF99">Per PayInstrumentNo</td>
                    <td align="center" bgcolor="#CCFF99">Per PayIssuingBank</td>
                    <td align="center" bgcolor="#CCFF99">Per PayIssuingBranch</td>
                    <td align="center" bgcolor="#CCFF99">Per PayIssuanceDt</td>
                    <td align="center" bgcolor="#CCFF99">Contract No</td>
                    <td align="center" bgcolor="#CCFF99">Contract LotNo</td>
                    <td align="center" bgcolor="#CCFF99">Contract DescOfLot</td>
                    <td align="center" bgcolor="#CCFF99">Contract StartDate</td>
                    <td align="center" bgcolor="#CCFF99">Contract CompletionDate</td>
                    <td align="center" bgcolor="#CCFF99">Contract Value</td>
                    <td align="center" bgcolor="#CCFF99">Letter of Acceptance (LOA) Date</td>
                    <td align="center" bgcolor="#CCFF99">Letter of Acceptance (LOA) PA Name</td>
                </tr>
                <%
                            Map mapObj = new LinkedHashMap();
                            Format dtformater;
                            dtformater = new SimpleDateFormat("yyyy-MM-dd");
                            List<String> cmsList = new ArrayList<String>();
                            List<RHDIntegrationCMSDetails> RHDIntegrationDetailsList = new ArrayList<RHDIntegrationCMSDetails>();
                            try {
                                RHDIntegrationCMSDetailsService RHDIntegrationDetailsService =
                                        (RHDIntegrationCMSDetailsService) AppContext.getSpringBean("RHDIntegrationCMSDetailsService");
                                RHDIntegrationDetailsList = RHDIntegrationDetailsService.getSharedCMSDetailForRHD();
                                if (RHDIntegrationDetailsList != null && RHDIntegrationDetailsList.size() > 0) {
                                    for (RHDIntegrationCMSDetails rhdIntegrationDetails : RHDIntegrationDetailsList) {
                %>
                <tr>
                    <td align="center" > <%=rhdIntegrationDetails.getPE_Id()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPE_Name()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPE_Code()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getNoA_Name()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_Id()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getProc_Nature()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getProc_Method()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getProc_Type()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getBudget_Type()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getSource_Of_Funds()%></td>
                    <td align="center" > <%
                        if(rhdIntegrationDetails.getProject_Code() == null || rhdIntegrationDetails.getProject_Code() == "null")
                            {
                                out.print("-");
                            }
                        else
                            {
                                out.print(rhdIntegrationDetails.getProject_Code());
                            }
                    %></td>
                    <td align="center" > <%
                        if(rhdIntegrationDetails.getProject_Name() == null || rhdIntegrationDetails.getProject_Name() == "null")
                            {
                                out.print("-");
                            }
                        else
                            {
                                out.print(rhdIntegrationDetails.getProject_Name());
                            }
                    %></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_RefNo()==null?"-":rhdIntegrationDetails.getTender_RefNo()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_InvitationFor()==null?"-":rhdIntegrationDetails.getTender_InvitationFor()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_PkgNo()==null?"-":rhdIntegrationDetails.getTender_PkgNo()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_PkgDesc()==null?"-":rhdIntegrationDetails.getTender_PkgDesc()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_Category()==null?"-":rhdIntegrationDetails.getTender_Category()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_PubDt()==null?"-":rhdIntegrationDetails.getTender_PubDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_DocLstSelD()==null?"-":rhdIntegrationDetails.getTender_DocLstSelD()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_PreMeetingStrtDt()==null?"-":rhdIntegrationDetails.getTender_PreMeetingStrtDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_PreMeetingEndDt()==null?"-":rhdIntegrationDetails.getTender_PreMeetingEndDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_ClosingDt()==null?"-":rhdIntegrationDetails.getTender_ClosingDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_OpeningDt()==null?"-":rhdIntegrationDetails.getTender_OpeningDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_SecLstSubDt()==null?"-":rhdIntegrationDetails.getTender_SecLstSubDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_BriefDesc()==null?"-":rhdIntegrationDetails.getTender_BriefDesc()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_EvaluationType()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_DocAvailable()==null?"-":rhdIntegrationDetails.getTender_DocAvailable()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_DocFees()==null?"-":rhdIntegrationDetails.getTender_DocFees()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_DocPrice()==null?"-":rhdIntegrationDetails.getTender_DocPrice()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_SecAmt()==null?"-":rhdIntegrationDetails.getTender_SecAmt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getTender_SecPayMode()==null?"-":rhdIntegrationDetails.getTender_SecPayMode()%></td>
                    <td align="center" > <%
                                if (rhdIntegrationDetails.getTender_SecValidUpToDt() == null) {
                                out.println("-");
                            } else {
                                out.println(dtformater.format(rhdIntegrationDetails.getTender_SecValidUpToDt()));
                            }%></td>
                    <td align="center" > <%
                            if (rhdIntegrationDetails.getTender_ValidUpToDt() == null) {
                                out.println("-");
                            } else {
                                out.println(rhdIntegrationDetails.getTender_ValidUpToDt().split(" ")[0]);
                            }%></td>
                    <td align="center" > <%
                            if (rhdIntegrationDetails.getPer_SecValidUpToDt() == null) {
                                out.println("-");
                            } else {
                                out.println(rhdIntegrationDetails.getPer_SecValidUpToDt().split(" ")[0]);
                            }%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_SecBankName()==null?"-":rhdIntegrationDetails.getPer_SecBankName()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_SecBankBranchName()==null?"-":rhdIntegrationDetails.getPer_SecBankBranchName()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_PayAmount()==null?"-":rhdIntegrationDetails.getPer_PayAmount()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_PayPaidDt()==null?"-":rhdIntegrationDetails.getPer_PayPaidDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_SecPayMode()==null?"-":rhdIntegrationDetails.getPer_SecPayMode()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_PayInstrumentNo()==null?"-":rhdIntegrationDetails.getPer_PayInstrumentNo()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_PayIssuingBank()==null?"-":rhdIntegrationDetails.getPer_PayIssuingBank()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getPer_PayIssuingBranch()==null?"-":rhdIntegrationDetails.getPer_PayIssuingBranch()%></td>
                    <td align="center" > <%
                            if (rhdIntegrationDetails.getPer_PayIssuanceDt() == null) {
                                out.println("-");
                            } else {
                                out.println(dtformater.format(rhdIntegrationDetails.getPer_PayIssuanceDt()));
                            }
                            %></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_No()==null?"-":rhdIntegrationDetails.getContract_No()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_LotNo()==null?"-":rhdIntegrationDetails.getContract_LotNo()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_DescOfLot()==null?"-":rhdIntegrationDetails.getContract_DescOfLot()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_StartDt()==null?"-":rhdIntegrationDetails.getContract_StartDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_CompletionDt()==null?"-":rhdIntegrationDetails.getContract_CompletionDt()%></td>
                    <td align="center" > <%=rhdIntegrationDetails.getContract_Value()==null?"-":rhdIntegrationDetails.getContract_Value()%></td>
                    <td align="center" > <%
                            if (rhdIntegrationDetails.getNoA_Dt() == null) {
                                out.print("-");
                            } else {
                                out.print(dtformater.format(rhdIntegrationDetails.getNoA_Dt()));
                            }
                        %></td>
                    <td align="center" > <%=rhdIntegrationDetails.getNoA_PEName()%></td>
                </tr>
                <%
                                                    }//end FOR loop
                                                }//end IF condition
                                                else {%>
                <tr>
                    <td align="center" colspan="51"> No Records Found </td>
                </tr>
                <%                        }
                            } catch (Exception e) {
                                //System.out.println("Exception getRequiredListBoQ :-"+e.toString());
                            }


                %>
                <%
                            if (isexport == false) {
                %>
                <tr>
                    <td align="left" colspan="51"> <input type="Submit" value="Export To Excel" name="Submit">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
            </table>
        </form>
    </body>
</html>
