<%-- 
    Document   : DebarmentListing
    Created on : Apr 8, 2018, 5:18:24 PM
    Author     : Aprojit
--%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <title>Debarment List</title>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
        <script type="text/javascript">
            /* Call Calendar function */
            function GetCal(txtname, controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: true,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            /*Check the disable*/
            function chkdisble(pageNo) {
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if (parseInt($('#pageNo').val(), 10) != 1) {
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if (parseInt($('#pageNo').val(), 10) > 1) {
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                } else {
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                } else {
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
            /*Load The Table*/
            function loadTable()
            {
                if ($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/InitDebarment", {pageNo: $("#pageNo").val(), size: $("#size").val(), searchFor: $('#idsearchFor').val(), cmpName: $('#txtcmpName').val(), dtFrom: $('#txtDateFrom').val(), dtTo: $('#txtDateTo').val(), action: 'getDebarListCommon'}, function (j) {
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if ($('#noRecordFound').attr('value') == "noRecordFound") {
                        $('#pagination').hide();
                    } else {
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if ($("#totalPages").val() == 1) {
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    } else {
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                    // Dynamically shortened text with ?Read More? Link
                    var showChar = 150;  // How many characters are shown by default
                    var ellipsestext = "...";
                    var moretext = "more";
                    var lesstext = "less";

                    $('.more').each(function () {
                        var content = $(this).html();

                        if (content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);
                            var html = c + '<span class="moreellipses">' + ellipsestext + '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        } else
                        {
                            var c = content + '</br>';
                            var html = c;
                            $(this).html(html);
                        }

                    });

                    $(".morelink").click(function () {
                        if ($(this).hasClass("less")) {
                            $(this).removeClass("less");
                            $(this).html(moretext);
                        } else {
                            $(this).addClass("less");
                            $(this).html(lesstext);
                        }
                        $(this).parent().prev().toggle();
                        $(this).prev().toggle();
                        return false;
                    });
                });
            }
            /*  Handle country Combo Event*/
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val().toString().split('_', 100)[0], funName: 'stateComboValue'}, function (j) {
                        //alert(j.toSource());
                        $('#cmbState').children().remove().end().append('<option value="">--Select Dzongkhag / District --</option>');
                        $("select#cmbState").append(j.toString().replace('selected', ''));
                        //toggleUpzillaView();
                    });
                });
            });

            $(function () {
                $('#btnGoto').click(function () {
                    var pageNo = parseInt($('#dispPage').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (pageNo > 0)
                    {
                        if (pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
            $(function () {
                $('#btnPrevious').click(function () {
                    var pageNo = $('#pageNo').val();
                    if (parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
            $(function () {
                $('#btnNext').click(function () {
                    var pageNo = parseInt($('#pageNo').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);

                    if (pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo) + 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) + 1);

                        $('#dispPage').val(Number(pageNo) + 1);
                    }
                });
            });
            $(function () {
                $('#btnLast').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (totalPages > 0) {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
            $(function () {
                $('#btnFirst').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    var pageNo = $('#pageNo').val();
                    if (totalPages > 0 && pageNo != "1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
            $(function () {
                $('#idsearchFor').change(function () {
                    if ($('#idsearchFor').val() == 'company')
                    {
                        $('#lvlCompanyName').html('Company Name :');
                    } else if ($('#idsearchFor').val() == 'individual')
                    {
                        $('#lvlCompanyName').html('Consultant Name :');
                    }
                });
            });
        </script>

    </head>
    <body onload="loadTable();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->

            <div class="mainDiv">
                <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include>
                        <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">                     
                                <td class="contentArea_1">
                                    <div class="contentArea_1">
                                        <div class="pageHead_1">Debarment List</div>
                                        <div class="formBg_1 t_space">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                                <tr>
                                                    <td class="ff" width="15%" >Search For : </td>
                                                    <td width="35%">
                                                        <select name="idsearchFor" class="formTxtBox_1" id="idsearchFor" style="width: auto;">
                                                            <option value="company" selected>Company</option>
                                                            <option value="individual">Individual Consultant</option>
                                                        </select> 
                                                    </td>
                                                    <td width="15%"></td>
                                                    <td width="35%">&nbsp;</td>
                                                </tr>
                                                <tr id="companyTr">
                                                    <td class="ff" width="15%" id = 'lvlCompanyName'>Company Name : </td>
                                                    <td width="35%">
                                                        <input name="txtcmpName" type="text" class="formTxtBox_1" id="txtcmpName" style="width:200px;" />
                                                    </td>
                                                    <td width="15%"></td>
                                                    <td width="35%">&nbsp;</td>
                                                </tr>

<!--                                                <tr>
                                                    <td class="ff">Debar Period <br/> (Date From) : </td>
                                                    <td>
                                                        <input name="txtDateFrom" type="text" class="formTxtBox_1" id="txtDateFrom" style="width:100px;" readonly="true" onClick="GetCal('txtDateFrom', 'imgCal');" />
                                                        &nbsp;
                                                        <a href="javascript:void(0);" onclick ="GetCal('txtDateFrom', 'imgCal');" title="Calender">
                                                            <img id="imgCal" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                                                    </a>
                                                </td>
                                                <td class="ff">Debar Period <br/> (Date To) :</td>
                                                <td>
                                                    <input name="txtDateTo" type="text" class="formTxtBox_1" id="txtDateTo" style="width:100px;" readonly="true" onClick="GetCal('txtDateTo', 'imgCal1');" />
                                                    &nbsp;
                                                    <a href="javascript:void(0);" onclick ="GetCal('txtDateTo', 'imgCal1');" title="Calender">
                                                        <img id="imgCal1" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                                                    </a>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td colspan="3" class="t-align-center">
                                                    <label class="formBtn_1">
                                                        <input type="button" name="button" id="button" value="Search" onclick="javascript:{
                                                                    document.getElementById('pageNo').value = 1;
                                                                    loadTable();
                                                                }" />
                                                    </label>
                                                    <label class="formBtn_1">
                                                        <input type="submit" name="btnReset" id="btnReset" value="Reset" onclick="window.location.reload();"/>
                                                    </label>
                                                </td>
                                                <td class="t-align-right">

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                        <tr>
                                            <th width="5%" class="t-align-center">Sl. No.</th>
                                            <th width="12%" class="t-align-center">Consultant/Company</th>
                                            <th width="12%" class="t-align-center">Dzongkhag/District</th>
                                            <th width="15%" class="t-align-center">Category of the Debarment</th>
                                            <th width="10%" class="t-align-center">Debarred From</th>
                                            <th width="10%" class="t-align-center">Debarred To</th>
                                            <th width="28%" class="t-align-center">Grounds</th>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/>
                                            </td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td  class="prevNext-container">
                                                <ul>
                                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <input type="hidden" id="pageNo" value="1"/>
                                </div>
                            </td>
                            <td width="266"><jsp:include page="Left.jsp" ></jsp:include></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        <jsp:include page="Bottom.jsp" ></jsp:include>



    </body>
</html>
