<%-- 
    Document   : RedirectToEditPeOffice
    Created on : Mar 3, 2016, 10:06:16 AM
    Author     : NAHID
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                document.getElementById("hiddenUserType").value = "gov";
                document.forms[0].action = "<%=request.getContextPath()%>/admin/EditPEOfficeGrid.jsp?deptType=Organization";
                document.forms[0].submit();
            });

        </script>
    </head>
    <body>
        <form id="frmAfterLoginTop" name="frmAfterLoginTop" method="POST">
            <input type="hidden" name="hiddenUserType" id="hiddenUserType">
        </form>
    </body>
</html>
