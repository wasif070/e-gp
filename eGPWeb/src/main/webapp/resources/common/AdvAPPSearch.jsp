<%--
    Document   : AdvAPPSearch
    Created on : Nov 2, 2010, 3:27:24 PM
    Author     : Administrator
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%

            StringBuilder mode = new StringBuilder();

            String struserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            String title = "Advanced Search for Annual Procurement Plan";
            String from = "Search";
            
            boolean isLoggedIn = false;
            if (objUserId != null) {
                struserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }

            if (request.getParameter("from") != null && !"".equalsIgnoreCase(request.getParameter("from"))) {
                 from = request.getParameter("from");
            }
            
            boolean flagWatchList = false;
            if (request.getParameter("from") != null && "watchlist".equalsIgnoreCase(request.getParameter("from"))) {
                flagWatchList = true;
            }

            if (isLoggedIn) {
                mode.append("WatchList");
                if (struserTypeId.equals("2")) {
                    if("search".equalsIgnoreCase(from)){
                        title = "APP Advance Search";
                    }else{
                        title = "APP WatchList";
                    }
                }
            } else {
                mode.append("Search");
            }

            StringBuilder userType = new StringBuilder();
            if (request.getParameter("hdnUserType") != null) {
                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                } else {
                    userType.append("org");
                }
            } else {
                userType.append("org");
            }
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../js/pngFix.js"></script>--%>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
    </head>
    <body <% if("search".equalsIgnoreCase(from)){ %>onload="loadTable(); hide(); <%if(session.getAttribute("userId") == null){%>chngTab(2);<% } %>"<%}else{ %>onload="loadWatchListTable();hide(); <%if(session.getAttribute("userId") == null){%> chngTab(2);<% } %>"<% } %>>
        <div class="mainDiv">
            
            <%
                        if (isLoggedIn) {
            %>
            <div class="dashboard_div">
                <%@include file="AfterLoginTop.jsp" %>
                <%} else {%>
                <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include> <%}%>
                    <!--Middle Content Table Start-->
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                           
                            <td class="contentArea">
                                <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                                <%
                                    String logUserId="0";
                                    if(session.getAttribute("userId")!=null){
                                        logUserId=session.getAttribute("userId").toString();
                                    }
                                    advAppSearchSrBean.setLogUserId(logUserId);
                                %>
                                <!--Page Content Start-->
                                <div class="t_space">
                                    <div class="pageHead_1"><%=title%></div>
                                    <div class="formBg_1 t_space">
                                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();"></a>
                                            <input type="hidden" id="ExpCol" value =""/></div>
                                        <form action="<%=request.getContextPath()%>/SearchServlet" id="frmAdvSerach" method="POST">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tblSearchBox">
                                                
                                                <%if (flagWatchList)
                                                {%>
                                                    <input type="hidden" name="userId" id="userId" value="<%=objUserId.toString()%>">
                                                <%}%>
                                                <% if("search".equalsIgnoreCase(from)){ %>
                                                <input type="hidden" name="mode" id="mode" value="Search" />
                                                <% }else{ %>
                                                <input type="hidden" name="mode" id="mode" value="" />
                                                <% } %>
                                                <tr>
                                                    <td class="ff" >Select Hierarchy Node :</td>
                                                    <td colspan="3">
                                                        <input type="hidden" name="viewType" id="viewType" value="Live"/>
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text"
                                                               id="txtdepartment"  readonly style="width: 450px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" onchange="loadOffice();" />
                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procuring Agency :</td>
                                                    <td><select name="office" class="formTxtBox_1" id="cmbOffice" style="width:208px;">
                                                            <option value="0">-- Select Procuring Agency --</option>
                                                        </select>
                                                    </td>
                                                    <td class="ff">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Project Name :</td>
                                                    <td><select name="project" class="formTxtBox_1" id="cmbProject" style="width:208px;">
                                                            <option value=" ">-- Select Project --</option>
                                                            <c:forEach var="project" items="${advAppSearchSrBean.projectList}">
                                                                <option value="${project.objectId}">${project.objectValue}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Financial Year :</td>
                                                    <td><select name="financialYear" class="formTxtBox_1" id="cmbYear" style="width:208px;">
                                                            <option value=" " selected="selected ">-- Select Year --</option>
                                                            <%
                                                                for (SelectItem finYear : advAppSearchSrBean.getFinancialYearList()){
                                                                    // if(advAppSearchSrBean.getDefaultFinYear()!=null){
                                                               %>
                                                               <option value="<%=finYear.getObjectValue()%>"
                                                                <%
                                                                       if (advAppSearchSrBean.getDefaultFinYear()!=null && advAppSearchSrBean.getDefaultFinYear().equalsIgnoreCase(finYear.getObjectId().toString())) {
                                                                 %>
                                                                        selected="selected" 
                                                              <%    }%>
                                                                        ><%=finYear.getObjectValue()%></option>
                                                               <% } %>
                                                        </select>
                                                    </td>
                                                    <td class="ff">Budget Type : </td>
                                                    <td><select name="budgetType" class="formTxtBox_1" id="cmbBudget" style="width:208px;">
                                                            <option value="" selected="selected">- Select Budget Type -</option>
                                                            <option value="1">Capital</option>
                                                            <option value="2">Recurrent</option>
                                                            <option value="3">Own fund</option>
                                                        </select>
                                                        <span id="SearchValError2" class="reqF_1"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procurement Category : </td>
                                                    <td><select name="procNature" class="formTxtBox_1" id="cmbNature" style="width:208px;">
                                                            <option value="" selected="selected">-- Select Category --</option>
                                                            <option value="Goods">Goods</option>
                                                            <option value="Works">Works</option>
                                                            <option value="Service">Service</option>
                                                        </select>
                                                        <span id="SearchValError1" class="reqF_1"></span>
                                                    </td>
                                                    <td class="ff">Procurement Type:</td>
                                                    <td><select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                                            <option value="">-- Select Type --</option>
                                                            <option value="NCT">NCB</option>
                                                            <option value="ICT">ICB</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">APP ID :</td>
                                                    <td><input name="appId" type="text" class="formTxtBox_1" id="txtAppId" style="width:194px;" />
                                                        <br /><span id="msgappid" style="color: red;"></span></td>
                                                    <td class="ff">Letter Ref. No. :</td>
                                                    <td><input name="appCode" type="text" class="formTxtBox_1" id="txtAppCode" style="width:194px;" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Package No. :</td>
                                                    <td><input name="pkgNo" type="text" class="formTxtBox_1" id="txtPkgNo" style="width:194px" /></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Package Estimated Cost :</td>
                                                    <td><select name="operation" class="formTxtBox_1" id="cmbOperator" style="width:208px;" onchange="chkVal2()">
                                                            <option value="">-- Select Operation --</option>
                                                            <option value="=">=</option>
                                                            <option value="&lt;=">&lt;=</option>
                                                            <option value="&gt;=">&gt;=</option>
                                                            <option value="Between">Between</option>
                                                        </select>
                                                    </td>
                                                    <td class="ff" id="lbl_val1">Value :</td>
                                                    <td><input name="value" type="text" class="formTxtBox_1" id="txtVal1" style="width:194px;" onblur="chkdigits1()"/>
                                                        <input type="hidden" name="pageNo" id="pageNo" value="1"/>
                                                        <input type="hidden" name="size" id="size" value="10"/>
                                                        <span id="CompareError1" class="reqF_1"></span>
                                                    </td>
                                                </tr>
                                                <tr id="val2Row">
                                                    <td class="ff" id="lbl_val2" style="display: none">Value2 :</td>
                                                    <td><input name="value2" type="text" class="formTxtBox_1" id="txtVal2" style="width:200px;display: none" onblur="chkdigits2()"/>
                                                        <span id="CompareError2" class="reqF_1"></span>
                                                        <span id="CompareError" class="reqF_1"></span>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                
                                                <tr style="display: none">
                                                    <td class="ff">Category :</td>
                                                    <td colspan="3">
                                                        <textarea id="txtaCPV" name="cpvCat" rows="4" style="width: 40%;" class="formTxtBox_1" readonly></textarea>
                                                        &nbsp;&nbsp;<a href="#" class="action-button-select" onclick="loadCPVTree()">Select Categories</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">&nbsp;</td>
                                                    <td colspan="3"><label class="formBtn_1"><input type="button" name="search" id="btnSearch" value="Search"/></label>
                                                        <label>&nbsp;</label>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnReset" id="btnReset" value="Reset" onclick="document.getElementById('frmReset').submit();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                    <form name="frmReset" id="frmReset" method="post"></form>
                                    <div>&nbsp;</div>
                                    <div id="resultDiv" style="display: none">
                                        <div class="pageHead_1">Annual Procurement Plan Search Results
                                        <%if(struserTypeId!=null && struserTypeId!=""){%>
                                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);"
                                            onclick="exportDataToPDF('7');">Save as PDF</a></span>
                                        <%}%>
                                        </div>

                                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_1 t_space" cols="@0">
                                            <tr>
                                                <th width="4%" class="t-align-center">Sl. <br/> No.</th>
                                                <th width="12%" class="t-align-center">APP ID, <br />Letter Ref. No.</th>
                                                <th width="25%" class="t-align-center">Hierarchy Node</th>
                                                <%--<th width="5%" class="t-align-center">Dzongkhag/ District</th>--%>
                                                <th width="15%" class="t-align-center">Procurement Category,<br/>Project Name</th>
                                                <th width="28%" class="t-align-center">Package No,<br/> Description</th>
                                                <th width="15%" class="t-align-center">Estimated Cost/Official Cost Estimate <br />(in Nu.),<br/>Procurement Method</th>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1" id="pagination">
                                            <tr>
                                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                                <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                    &nbsp;
                                                    <label class="formBtn_1">
                                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                    </label>
                                                </td>
                                                <td class="prevNext-container">
                                                    <ul>
                                                        <li>&laquo; <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                        <li>&#8249; <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <div align="center">
                                            <input type="hidden" id="pageNo" value="1"/>
                                            <input type="hidden" name="size" id="size" value="10"/>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                </div>
                            </td>
                             <%
                                        if (objUName == null) {
                            %>
                            <td class="td-background"  width="266">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                            <%                                        }
                            %>
                        </tr>
                    </table>
                    <%
                                if (isLoggedIn && struserTypeId.equals("2")){ %>
                    <script type="text/javascript">
                        loadWatchListTable();
                    </script>
                    <%}%>
                    <!--Middle Content Table End-->

                </div>
                <form name="formstyle" id="formstyle" method="post">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value='<%=title.replaceAll(" ","")%>_<%=appenddate%>' />
                    <input type="hidden" name="id" id="id" value='<%=title.replaceAll(" ","")%>' />
                </form>
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabApp");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadCPVTree()
            {
                window.open('CPVTree.jsp','CPVTree','menubar=0,scrollbars=1,width=700px');
            }
        </script>


        <script type="text/javascript">

            function numeric(value) {
                return /^\d+$/.test(value);
            }
            function isDecimal(str) {
                return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(str);
            }

             function ltrim(str){
                return str.replace(/^\s+/, '');
            }
            function rtrim(str) {
                return str.replace(/\s+$/, '');
            }
             function alltrim(str) {
                return str.replace(/^\s+|\s+$/g, '');
            }
            function chkdigits1(){
                if($("#txtVal1").val().length >0) {
                    if(isDecimal(document.getElementById('txtVal1').value)==false){
                        $('#CompareError1').html('Please enter digits only.');
                        return false;
                    }else{
                        $('#CompareError1').html('');
                        return true;
                    }
                }
                else
                {
                    if($('#cmbOperator').val()=='Between'){
                         $('#CompareError1').html('Please enter Value1');
                         return false;
                     }else{
                         $('#CompareError1').html('');
                         return true;
                     }
                }
            }

            function chkdigits2(){
                if($("#txtVal2").val().length > 0) {
                    if(isDecimal(document.getElementById('txtVal2').value)==false){
                        $('#CompareError2').html('Please enter digits only.');
                        return false;
                    }else{
                        $('#CompareError2').html('');
                        return true;
                    }
                }
                else
                {
                     if($('#cmbOperator').val()=='Between'){
                         $('#CompareError2').html('Please enter Value2');
                         return false;
                     }else{
                         $('#CompareError2').html('');
                         return true;
                     }

                }
            }

            function chkMinistry(){
                var errorChk=true;
                /*
                if(document.getElementById('cmbNature').value=='')
                {
                    $('#SearchValError1').html('Please select Procurement Category.');
                    errorChk=false;
                }
                if(document.getElementById('cmbBudget').value=='')
                {
                    $('#SearchValError2').html('Please select Budget Type.');
                    errorChk=false;
                }
                */
                if($('#cmbOperator').val()=='Between'){
                    var flag1 = chkdigits1();
                    var flag2 = chkdigits2();

                    if(flag1 && flag2){
                        if(eval($('#txtVal1').val()) > eval($('#txtVal2').val())){
                            $('#CompareError').html('Value2 must be greater than Value1');
                            errorChk=false;
                        }
                        else{
                            $('#CompareError').html('');
                            errorChk=true;
                        }
                    }
                }
                return errorChk;
            }

            function chkVal2(){
                if($('#cmbOperator').val()=='Between'){
                    document.getElementById("lbl_val2").style.display = "block";
                    document.getElementById("txtVal2").style.display = "block";
                    document.getElementById("lbl_val1").innerHTML = "Value1 :";

                }else{
                    document.getElementById("lbl_val2").style.display = "none";
                    document.getElementById("txtVal2").style.display = "none";
                    document.getElementById("lbl_val1").innerHTML = "Value :";
                }
            }




        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $('#CompareError1').html('');
                    $('#CompareError2').html('');
                    $('#CompareError').html('');
                    $('#SearchValError2').html('');
                    $('#SearchValError1').html('');
                    $('#SearchValError').html('');
                    $("#mode").val("Search");
                    $("#pageNo").val("1");
                    var deptId='0';
                    $("#cmbOffice").val('');
                    $("#cmbProject").val('');
                    $("select#cmbYear option[selected]").removeAttr("selected");
                    $("select#cmbYear option[value='']").attr("selected", "selected");
                    $("#cmbBudget").val('');
                    $("#cmbNature").val('');
                    $("#cmbType").val('');
                    $("#txtAppId").val('');
                    $("#txtAppCode").val('')
                    $("#txtPkgNo").val('');
                    $("#cmbOperator").val();
                    $("#txtVal1").val('');
                    $("#txtVal2").val('');
                    $("#txtaCPV").val('');
                    $.post("<%=request.getContextPath()%>/SearchServlet", {action: "<%=from%>",departmentId: deptId,office: '0',project: '',financialYear: '',budgetType:'',procNature:'',procType:'',appId:'0',appCode:'',pkgNo:'',operation: '',value: '0',value2: '0',pageNo: $("#pageNo").val(),size: $("#size").val(),cpvCat: ''},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();
                        if($('#noRecordFound').attr("value") == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }

                        $("#pageNoTot").html($("#pageNo").val());
                        $("#pageTot").html($("#totalPages").val());
                        $('#resultDiv').show();
                    });
                });
            });

            function chkappID()
            {
                if(document.getElementById("txtAppId").value !="")
                {
                    if(isNaN(document.getElementById("txtAppId").value)){
                        document.getElementById("msgappid").innerHTML = "Please enter numeric data.";
                        return false;
                    }
                }
                return true;
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    if(chkMinistry() && chkdigits1() && chkdigits2() && chkappID()) {
                        $("#mode").val("Search");
                        $("#pageNo").val("1");
                        var deptId=$('#txtdepartmentid').val();
                        $.post("<%=request.getContextPath()%>/SearchServlet", {action: "<%=from%>",userId: $("#userId").val(),departmentId: deptId,office: $("#cmbOffice").val(),project: $("#cmbProject").val(),financialYear: $("#cmbYear").val(),budgetType:$("#cmbBudget").val(),procNature:$("#cmbNature").val(),procType:$("#cmbType").val(),appId:$("#txtAppId").val(),appCode: $("#txtAppCode").val(),pkgNo: $("#txtPkgNo").val(),operation: $("#cmbOperator").val(),value: $("#txtVal1").val(),value2: $("#txtVal2").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),cpvCat: $("#txtaCPV").val()},  function(j){
                            $('#resultTable').find("tr:gt(0)").remove();
                            $('#resultTable tr:last').after(j);
                             sortTable();
                            if($('#noRecordFound').attr('value') == "noRecordFound"){
                                $('#pagination').hide();
                            }else{
                                $('#pagination').show();
                            }

                            chkdisble($("#pageNo").val());
                            if($("#totalPages").val() == 1){
                                $('#btnNext').attr("disabled", "true");
                                $('#btnLast').attr("disabled", "true");
                            }else{
                                $('#btnNext').removeAttr("disabled");
                                $('#btnLast').removeAttr("disabled");
                            }

                            $("#pageNoTot").html($("#pageNo").val());
                            $("#pageTot").html($("#totalPages").val());
                            $('#resultDiv').show();
                        });
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadWatchListTable()
            {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/SearchServlet", {action: "WatchList",departmentId: deptId,office: $("#cmbOffice").val(),project: $("#cmbProject").val(),financialYear: $("#cmbYear").val(),budgetType:$("#cmbBudget").val(),procNature:$("#cmbNature").val(),procType:$("#cmbType").val(),appId:$("#txtAppId").val(),appCode: $("#txtAppCode").val(),pkgNo: $("#txtPkgNo").val(),operation: $("#cmbOperator").val(),value: $("#txtVal1").val(),value2: $("#txtVal2").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),userTypeId: $("#userTypeId").val(),userId: $("#userId").val(),cpvCat: $("#hdnCPV").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    $("#pageNoTot").html($("#pageNo").val());

                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }

                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
                //chkdisble($("#pageNo").val());
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/SearchServlet", {action: "Search",departmentId: deptId, office: $("#cmbOffice").val(),project: $("#cmbProject").val(),financialYear: $("#cmbYear").val(),budgetType:$("#cmbBudget").val(),procNature:$("#cmbNature").val(),procType:$("#cmbType").val(),appId:$("#txtAppId").val(),appCode: $("#txtAppCode").val(),pkgNo: $("#txtPkgNo").val(),operation: $("#cmbOperator").val(),value: $("#txtVal1").val(),value2: $("#txtVal2").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),cpvCat: $("#txtaCPV").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    $("#pageNoTot").html($("#pageNo").val());

                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }

                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        if($("#mode").val() == "Search")
                            loadTable();
                        else
                            loadWatchListTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        if($("#mode").val() == "Search")
                            loadTable();
                        else
                            loadWatchListTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        if($("#mode").val() == "Search"){
                            loadTable();
                        }else{
                            loadWatchListTable();
                        }
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        if($("#mode").val() == "Search"){
                            loadTable();
                        }else{
                            loadWatchListTable();
                        }
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            if($("#mode").val() == "Search"){
                                loadTable();
                            }else{
                                loadWatchListTable();
                            }
                            $('#dispPage').val(Number(pageNo));

                            chkdisble($("#pageNo").val());
                        }
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#cmbOperator').change(function() {
                    if( $("#cmbOperator option:selected").val().toString() == "Between")
                    {
                        $("#val2Row").show();
                    }
                    else
                    {
                        $("#val2Row").hide();
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadOffice() {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }

        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbNature').change(function() {
                    if($('#cmbNature').val()!="") {
                        if($("#SearchValError1").html().toString().length > 0)
                            $("#SearchValError1").html('');
                        $("#SearchValError1").hide();
                    }
//                  else
//                  {
//                      $('#SearchValError1').html('Please Select Procurement Nature.');
//                      $('#SearchValError1').show();
//                      errorchk=false;
//                  }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbBudget').change(function() {
                    if($('#cmbBudget').val()!="") {
                        if($("#SearchValError2").html().toString().length > 0)
                            $("#SearchValError2").html('');
                        $("#SearchValError2").hide();
                    }
//                  else
//                  {
//                      $('#SearchValError2').html('Please Select Budget Type');
//                      $('#SearchValError2').show();
//                      errorchk=false;
//                  }
                });
            });
        </script>
        <script type="text/javascript">
            
            var Interval;
            clearInterval(Interval);
            Interval = setInterval(function(){
                var PageNo = document.getElementById('pageNo').value;
                var Size = document.getElementById('size').value;
                CorrectSerialNumber(PageNo-1,Size); 
            }, 100);

            function showHide()
            {
                if(document.getElementById('collExp') != null && document.getElementById('ExpCol').value =='hide'){
                    document.getElementById('tblSearchBox').style.display = 'table'; 
                    document.getElementById('collExp').innerHTML = '- Advanced Search';
                    document.getElementById('ExpCol').value = 'show'
                }else{
                    hide();
                }
            }
            function hide(){
               document.getElementById('tblSearchBox').style.display = 'none';
               document.getElementById('collExp').innerHTML = '+ Advanced Search';
               document.getElementById('ExpCol').value = 'hide' ;
            }
        </script>
</html>
