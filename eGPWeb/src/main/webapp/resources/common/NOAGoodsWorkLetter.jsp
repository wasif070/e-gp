<%-- 
    Document   : NOAGoodsWorkLetter
    Created on : Jan 3, 2012, 6:57:35 PM
    Author     : nishit
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <body>
        <script src="<%=request.getContextPath()%>/resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        
        <%
        String dop = request.getParameter("dop");
        String tendBrife = request.getParameter("brief");
        String fundDetail = request.getParameter("amt");
        String contractSignDay = request.getParameter("contractSignDay");
        String perSecAmt = request.getParameter("perSecAmt");
        String perSecDays = request.getParameter("perSecDays");
        String perSecDt = request.getParameter("perSecDt");
        String nature = request.getParameter("nature");
        
         %>
         <p>                           
             This is to notify you that your e-Quotation dated <b><%=dop%></b> for the 
             <%if("works".equalsIgnoreCase(nature)){%>
             execution of the Works and physical services
             <%}else{%>
             supply and delivery of the Goods and related services 
             <%}%>
             named <b><%=tendBrife.trim().replace("</p>", "").replace("<p>","") %></b> for the Contract Price of Nu. <b><%=fundDetail%></b> has been approved by the competent authority.<br /><br /></p>
         <ul style="padding-left: 15px;">
             <li>
             You are thus requested to attend the office of the undersigned to sign the <%if("works".equalsIgnoreCase(nature)){%>Contract <%}else{%>Purchase Order<%}%> within 
             <input type="hidden" id="contractSignDays" value="<%=contractSignDay%>"/><b><label id="contractSignDaysWords"></label></b> <em><strong>(<%=contractSignDay%>) </strong></em> 
             Days of issuing this Letter of Invitation; but in no case later than. <b><%=perSecDt%></b>
         </li><br />
         <li>
             You may proceed with the
             <%if("works".equalsIgnoreCase(nature)){%>
             execution of the Works
             <%}else{%>
              supply and delivery of the Goods
             <%}%>
             only upon signing the <%if("works".equalsIgnoreCase(nature)){%>Contract <%}else{%>Purchase Order<%}%>. You may also please note that this invitation shall constitute the formation of this
             <%if("works".equalsIgnoreCase(nature)){%>
             Contract 
             <%}else{%>
             Purchase Order 
             <%}%>
             which shall become binding upon you. 
        </li>
            </ul><br />
        <p>
             We attach the draft 
             <%if("works".equalsIgnoreCase(nature)){%>
             Contract 
             <%}else{%>
             Purchase Order 
             <%}%>
             and all other documents for your perusal.<br />
        </p>
        <script type="text/javascript">
        
        //document.getElementById('noaIssueDaysWords').innerHTML =WORD(document.getElementById('noaIssueDays').value);
        //document.getElementById('perSecDaysWords').innerHTML =WORD(document.getElementById('perSecDays').value);
        document.getElementById('contractSignDaysWords').innerHTML =WORD(document.getElementById('contractSignDays').value);
    </script>
    </body>
</html>

