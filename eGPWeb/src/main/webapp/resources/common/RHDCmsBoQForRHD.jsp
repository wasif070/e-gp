<%-- 
    Document   : index
    Created on : Feb 16, 2012, 12:17:28 PM
    Author     : shreyansh.shah
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.math.MathContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPSharedCMSDetailBoQForRHD"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSBoQDetails"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.RHDIntegrationCMSDetailsService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RHD CMSBoQ Details</title>
        <%
                    String tenderId = "";
                    Boolean isexport = false;
                    if (request.getParameter("Submit") != null) {
                        tenderId = request.getParameter("txtTenderId");
                    }
        %>

        <%
                    if (request.getParameter("Export") != null) {
                        //  if (request.getParameter("exportTenId") != "" && request.getParameter("exportTenId") != null) {
                        response.setContentType("application/vnd.ms-excel");
                        response.setHeader("content-disposition", "attachment; filename=RHDCmsBoQReport.xls");
                        tenderId = request.getParameter("exportTenId");
                        isexport = true;
                        //   }
                    }
        %>

    </head>
    <body>


        <!--Header Table-->

        <noscript>
            <meta http-equiv="refresh" content="">
        </noscript>


        <form action="" name="frmBoq">
            <table border="1"    cellpadding="0" cellspacing="0" width="100%" style="margin-top:21px;" >
                <% if (isexport == false) {%>
                <tr>
                    <td colspan="5" align="left">Tender Id : </td>
                    <td colspan="6" align="left"> <input type="text" value="" name="txtTenderId"/> </td>
                </tr>
                <tr>
                    <td colspan="11" align="left"> <input type="Submit" value="Search" name="Submit"/>
                        <input type="Submit" value="Export To Excel" name="Export" >
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
                <tr  align="center">
                    <td align="center" bgcolor="#CCFF99">Tender Id</td>
                    <td align="center" bgcolor="#CCFF99">Item Group</td>
                    <td align="center" bgcolor="#CCFF99">Item SrNo</td>
                    <td align="center" bgcolor="#CCFF99">Item Description</td>
                    <td align="center" bgcolor="#CCFF99">Unit Of Measure</td>
                    <td align="center" bgcolor="#CCFF99">Item Quantity</td>
                    <td align="center" bgcolor="#CCFF99">Start Date</td>
                    <td align="center" bgcolor="#CCFF99">End Date</td>
                    <td align="center" bgcolor="#CCFF99">No Of Days</td>
                    <td align="center" bgcolor="#CCFF99">Unit Rate</td>
                    <td align="center" bgcolor="#CCFF99">Total Rate</td>
                </tr>
                <%
                            Map mapObj = new LinkedHashMap();
                            String jsonText = null;
                            BigDecimal totalRate;
                            Format dtformater;
                            dtformater = new SimpleDateFormat("yyyy-MM-dd");
                            List<String> BoQList = new ArrayList<String>();
                            List<RHDIntegrationCMSBoQDetails> RHDIntegrationDetailsBoQList = new ArrayList<RHDIntegrationCMSBoQDetails>();
                            try {
                                RHDIntegrationCMSDetailsService RHDIntegrationDetailsService =
                                        (RHDIntegrationCMSDetailsService) AppContext.getSpringBean("RHDIntegrationCMSDetailsService");
                                RHDIntegrationDetailsBoQList = RHDIntegrationDetailsService.getBoQForRHD(tenderId);
                                if (RHDIntegrationDetailsBoQList != null && RHDIntegrationDetailsBoQList.size() > 0) {
                                    for (RHDIntegrationCMSBoQDetails rhdIntegrationDetailsBoQ : RHDIntegrationDetailsBoQList) {

                                        totalRate = rhdIntegrationDetailsBoQ.getBoQ_ItemQnty().multiply(rhdIntegrationDetailsBoQ.getBoQ_UnitRate());
                %>
                <tr>
                    <td align="center" > <%=tenderId%></td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_ItemGroup()%></td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_ItemSrNo()%></td>
                    <td align="center" >
                        <%
                                if (rhdIntegrationDetailsBoQ.getBoQ_ItemDesc() == null || rhdIntegrationDetailsBoQ.getBoQ_ItemDesc() == "null") {
                                    out.println("-");
                                } else {
                                    out.println(rhdIntegrationDetailsBoQ.getBoQ_ItemDesc());
                                }

                        %>
                    </td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_UnitofMeasur()%></td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_ItemQnty()%></td>
                    <td align="center" > <%
                                if (rhdIntegrationDetailsBoQ.getBoQ_WPstartDt() == null) {
                                    out.println("-");
                                } else {
                                    out.println(dtformater.format(rhdIntegrationDetailsBoQ.getBoQ_WPstartDt()));
                                }
                        %></td>
                    <td align="center" > <%
                                if (rhdIntegrationDetailsBoQ.getBoQ_WPEndDt() == null) {
                                    out.println("-");
                                } else {
                                    out.println(dtformater.format(rhdIntegrationDetailsBoQ.getBoQ_WPEndDt()));
                                }
                        %></td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_WPNoOfDays()%></td>
                    <td align="center" > <%=rhdIntegrationDetailsBoQ.getBoQ_UnitRate()%></td>
                    <td align="center" > <%=totalRate.setScale(3, BigDecimal.ROUND_HALF_UP)%></td>
                </tr>
                <%
                                                    }//end FOR loop
                                                }//end IF condition
                                                else {
                %>
                <tr>
                    <td align="center" colspan="9"> No Records Found </td>
                </tr>
                <%                            }
                            } catch (Exception e) {
                                //System.out.println("Exception getRequiredListBoQ :-"+e.toString());
                            }


                %>
                <% if (isexport == false) {%>
                <tr>
                    <td align="left" colspan="51"> <input type="Submit" value="Export To Excel" name="Export" >
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath() + "InboxMessage.jsp"%>">Go Back</a></td>
                </tr>
                <% }%>
                <input type="hidden" name="exportTenId" value="<%=tenderId%>"/>
            </table>
        </form>
    </body>
</html>
