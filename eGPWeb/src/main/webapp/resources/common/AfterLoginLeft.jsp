<%-- 
    Document   : AfterLoginLeft
    Created on : Oct 28, 2010, 10:44:25 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<%@page import="java.util.Map"%>
<%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
%>
<%@page import="org.hibernate.usertype.UserType"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/simpletreemenu.js"></script>
<%
    int userTypeId = 0;
    int loguserId = 0;
    HttpSession sn = request.getSession();
    if (sn.getAttribute("userTypeId") != null) {
        userTypeId = Integer.parseInt(sn.getAttribute("userTypeId").toString());
    }
    if (sn.getAttribute("userId") != null) {
        loguserId = Integer.parseInt(sn.getAttribute("userId").toString());
    }

    Map<Integer, String> objUserRightsMapTop = null;
    ManageUserRightsImpl objManageUserRights = (ManageUserRightsImpl) AppContext.getSpringBean("ManageUserRigths");
    if (userTypeId == 19 || userTypeId == 20) {
        objUserRightsMapTop = objManageUserRights.getUserMenuRightsMap(loguserId, userTypeId);
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--Dashboard Content Part Start-->
<form id="frmAfterLoginLeft" name="frmAfterLoginLeft" method="POST">
    <input type="hidden" name="hdnUserType" id="hdnUserType">
    <input type="hidden" id="treeStatus" name="treeStatus" value="collapse">
</form>

<script type="text/javascript">

    $(document).ready(function () {

        var status = ddtreemenu.getCookie('treemenu1');
        if (status == "none open")
        {
            document.getElementById("status").innerHTML = '+ Expand';
        } else
        {
            document.getElementById("status").innerHTML = '- Collapse';
        }
    });
</script>
<script>
    function redirectToPage(action, whereToGo) {
        document.getElementById("hdnUserType").value = action;
        document.forms[0].action = whereToGo;
        document.forms[0].submit();
    }

    function toggleTree()
    {
        var status = document.getElementById("treeStatus").value;
        if (status == "collapse")
        {
            status = "expand";
            document.getElementById("status").innerHTML = '- Collapse';
        } else if (status == "expand")
        {
            status = "collapse";
            document.getElementById("status").innerHTML = '+ Expand';
        }

        document.getElementById("treeStatus").value = status;
        ddtreemenu.flatten('treemenu1', status);
    }
</script>
<td class="lftNav">
    <table width="100%" cellspacing="0">
        <tr valign="top">
            <td class="lftNav" nowrap>
                <div class="treemenu-controler"><a onclick="toggleTree()" href="javascript:void(0);"><span id="status">+ Expand</span></a> <%--&nbsp; | &nbsp; <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">- Collapse</a>--%></div>
                <%
                    StringBuilder userType = new StringBuilder();
                    Object objUserType = "";
                    if (request.getParameter("hiddenUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hiddenUserType"))) {
                            userType.append(request.getParameter("hiddenUserType"));
                            session.setAttribute("hiddenUserType", userType.toString());
                        } else {
                            objUserType = session.getAttribute("hiddenUserType");
                            if (objUserType != null) {
                                userType.append(objUserType);
                            }
                        }
                    } else {
                        objUserType = session.getAttribute("hiddenUserType");
                        if (objUserType != null) {
                            userType.append(objUserType);
                        }
                    }

                    /*if (request.getParameter("userTypeId") != null) {
                            if (!"".equalsIgnoreCase(request.getParameter("userTypeId"))) {
                            userTypeId = Integer.parseInt(request.getParameter("userTypeId"));
                            }
                            }*/
                    if (userTypeId == 1) { // eGPEdmin
                        if ("org".equalsIgnoreCase(userType.toString())) { // Organization Strucuture
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <%-- <li><a href="#">CCGP</a>
                         <ul>
                             <li><a href="#">View</a></li>
                         </ul>
                     </li>
                     <li><a href="#">CD</a>
                         <ul>
                             <li><a href="#">View</a></li>
                         </ul>
                     </li>--%>
                    <!--<li><a href="#">CCGP</a>
                        <ul>
                            <li><a id="lblCCGPPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=CCGP">Create PE Office</a></li>
                            <li><a id="lblCCGPPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=CCGP">View PE Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Cabinet Division</a>
                        <ul>
                            <li><a id="lblCabinetPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Cabinet Division">Create PE Office</a></li>
                            <li><a id="lblCabinetPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Cabinet Div">View PE Offices</a></li>
                        </ul>
                    </li>-->
                    <li><a href="#">Autonomous Body</a>
                        <ul>
                            <li><a id="lblAutonomousBodyCreation" href="DepartmentCreation.jsp?deptType=Autonomus">Create Autonomous Body</a></li>
                            <li><a id="lblViewAutonomousBody" href="ManageEmployee.jsp?deptType=Autonomus">View Autonomous Body</a></li>
                            <li><a id="lblAutonomousBodyPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Autonomus">Create PA Office</a></li>
                            <li><a id="lblAutonomousBodyPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Autonomus">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Ministry</a>
                        <ul>
                            <li><a id="lblMinistryCreation" href="DepartmentCreation.jsp?deptType=Ministry">Create Ministry</a></li>
                            <li><a id="lblViewMinistry" href="ManageEmployee.jsp?deptType=Ministry">View Ministries</a></li>
                            <li><a id="lblMinistryPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Ministry">Create PA Office</a></li>
                            <li><a id="lblMinistryPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Ministry">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Department</a>
                        <ul>
                            <li><a id="lblDivisionCreation" href="DepartmentCreation.jsp?deptType=Division">Create Department</a></li>
                            <li><a id="lblViewDivision" href="ManageEmployee.jsp?deptType=Division">View Department</a></li>
                            <li><a id="lblDivisionPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Division">Create PA Office</a></li>
                            <li><a id="lblDivisionPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Division">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Divisions</a>
                        <ul>
                            <li><a id="lblOrganizationCreation" href="DepartmentCreation.jsp?deptType=Organization">Create Divisions</a></li>
                            <li><a id="lblOrganizationView" href="ManageEmployee.jsp?deptType=Organization">View Divisions</a></li>
                            <li><a id="lblOrganizationPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Organization">Create PA Office</a></li>
                            <li><a id="lblOrganizationPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Organization">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Dzongkhag</a>
                        <ul>
                            <li><a id="lblDistrictCreation" href="DepartmentCreation.jsp?deptType=District">Create Dzongkhag</a></li>
                            <li><a id="lblViewDistrict" href="ManageEmployee.jsp?deptType=District">View Dzongkhag</a></li>
                            <li><a id="lblDistrictPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=District">Create PA Office</a></li>
                            <li><a id="lblDistrictPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=District">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Dungkhag</a>
                        <ul>
                            <li><a id="lblSubDistrictCreation" href="DepartmentCreation.jsp?deptType=SubDistrict">Create Dungkhag</a></li>
                            <li><a id="lblViewSubDistrict" href="ManageEmployee.jsp?deptType=SubDistrict">View Dungkhag</a></li>
                            <li><a id="lblSubDistrictPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=SubDistrict">Create PA Office</a></li>
                            <li><a id="lblSubDistrictPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=SubDistrict">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Gewog</a>
                        <ul>
                            <li><a id="lblGewogCreation" href="DepartmentCreation.jsp?deptType=Gewog">Create Gewog</a></li>
                            <li><a id="lblViewGewog" href="ManageEmployee.jsp?deptType=Gewog">View Gewog</a></li>
                            <li><a id="lblGewogPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Gewog">Create PA Office</a></li>
                            <li><a id="lblGewogPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Gewog">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Development Partner</a>
                        <ul>
                            <li><a id="lbldevPartnerCreation" href="SchBankDevPartnerCreation.jsp?partnerType=Development">Create Development Partner Organization</a></li>
                            <li><a id="lbldevPartnerManage" href="ManageSbDpOffice.jsp?partnerType=Development">View Development Partner Organizations</a></li>
                            <li><a href="#">Regional/Country Office</a>
                                <ul>
                                    <li><a id="lblDevPartnerSchBankCreation" href="SchBankDevPartnerBranchCreation.jsp?partnerType=Development">Create Regional/Country Office</a></li>
                                    <li><a id="lblDevPartnerManageSbDpView" href="ManageSbDpBranchOffice.jsp?partnerType=Development">View Regional/Country Offices</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution</a>
                        <ul>
                            <li><a id="lblSchBankCreation" href="SchBankDevPartnerCreation.jsp">Create Financial Institution</a></li>
                            <li><a id="lblSchBankView" href="ManageSbDpOffice.jsp">View Financial Institutions</a></li>
                            <li><a href="#">Financial Institution Branch</a>
                                <ul>
                                    <li><a id="lblSchBankDevCreation" href="SchBankDevPartnerBranchCreation.jsp">Create Financial Institution Branch</a></li>
                                    <li><a id="lblManageSbDpView" href="ManageSbDpBranchOffice.jsp">View Financial Institution Branches</a></li>
                                </ul>
                            </li>
                            <!--                             <li><a href="#">Financial Institution Admin</a>
                                                            <ul>
                                                                <li><a id="lblScheBankBranchAdminCreation" href="#" onClick="redirectToPage('admin','ScheduledBranchAdmin.jsp');">Create Scheduled Bank Branch Admin</a></li>
                                                                <li><a id="lblScheBankBranchAdminView" href="ManageSbBranchAdmin.jsp">View Scheduled Bank Branch Admins</a></li>

                                                    </ul>
                                                        </li>-->
                        </ul>

                    </li>
                    <!--<li><a href="#">Review Panel</a>
                        <ul>
                            <li><a id="lblreveiwpanelcreatioin" href="reviewPanel.htm">Create</a></li>
                            <li><a id="lblreveiwpanelview" href="ViewreviewPanel.htm">View</a></li>
                        </ul>
                    </li>-->
                </ul>
                <%    } else if ("manageDebar".equalsIgnoreCase(userType.toString())) {%>
                        <ul id="treemenu1" class="treeview TreeViewBorder">
                            <li><a href="#">Manage Debarment Committee</a>
                                <ul>
                                    <li><a id="lblChairDebar" href="DebarComChair.jsp">Chairman</a></li>
                                    <li><a id="lblViewDebarCom" href="ActiveMembers.jsp">View Debarment Committee</a></li>
                                    <li><a id="lblCreateDebarMem" href="CreateDebarMember.jsp">Create Debarment Committee Member</a></li>
                                    
                                </ul>
                            </li>
                        </ul>
                    
                    <%}else if ("admin".equalsIgnoreCase(userType.toString())) {  // Admin Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <!--<li><a href="#">Organization Admin</a>
                        <ul>
                            <li><a id="lblOrgAdminCreation" href="#" onClick="redirectToPage('admin','OrgAdminReg.jsp');">Create Organization Admin</a></li>
                    <%--<li><a href="#" onClick="redirectToPage('admin','ManageOrgAdmin.jsp');">View</a></li>--%>
                    <li><a id="lblOrgAdminView" href="#" onClick="redirectToPage('admin','OrganizationAdminGrid.jsp?userTypeid=5');">View Organization Admins</a></li>

                </ul>
            </li>-->
                    <li><a href="#">PA Admin</a>
                        <ul>
                            <li><a id="lblPEAdminCreation" href="#" onClick="redirectToPage('admin', 'PEAdminRegister.jsp');">Create PA Admin</a></li>
                            <li><a id="lblPEAdminView" href="#" onClick="redirectToPage('admin', 'PEAdminGrid.jsp?userTypeid=4');">View PA Admins</a></li>

                        </ul>
                    </li>
                    <!--<li><a href="#">Development Partner Admin</a>
                        <ul>
                            <li><a id="lblDevPartnerAdminCreation" href="#" onClick="redirectToPage('admin','DevPartnerAdminRegister.jsp');">Create Development Partner Admin</a></li>
                            <li><a id="lblDevPartnerAdminView" href="ManageSbDpAdmin.jsp?partnerType=Development">View Development Partner Admins</a></li>

                        </ul>
                    </li>-->
                    <li><a href="#">Financial Institution Admin</a>
                        <ul>
                            <li><a id="lblScheBankAdminCreation" href="#" onClick="redirectToPage('admin', 'ScheduledBankAdmin.jsp');">Create Financial Institution Admin</a></li>
                            <li><a id="lblScheBankAdminView" href="ManageSbDpAdmin.jsp">View Financial Institution Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Branch Admin</a>
                        <ul>
                            <li><a id="lblScheBankBranchAdminCreation" href="#" onClick="redirectToPage('admin', 'ScheduledBranchAdmin.jsp');">Create Financial Institution Branch Admin</a></li>
                            <li><a id="lblScheBankBranchAdminView" href="ManageSbBranchAdmin.jsp">View Financial Institution Branch Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Content Admin</a>
                        <ul>
                            <li><a id="lblContentAdminCreation" href="#" onClick="redirectToPage('admin', 'ContentAdminRegister.jsp');">Create Content Admin</a></li>
                            <li><a id="lblContentAdminView" href="#" onClick="redirectToPage('admin', 'ContentAdminGrid.jsp?userTypeid=8');">View Content Admins</a></li>

                        </ul>
                    </li>
                    <!--  Hide 
                    <li><a href="#">Procurement Expert User</a>
                        <ul>
                            <li><a id="lblProcureExpAdminCreation" href="#" onClick="redirectToPage('admin','ProcExpertAdminRegister.jsp');">Create Procurement Expert User</a></li>
                            <li><a id="lblProcureExpAdminView" href="#" onClick="redirectToPage('admin','ProcExpertAdminGrid.jsp?userTypeid=12');">View Procurement Expert Users</a></li>

                        </ul>
                    </li>
                    <li><a href="#">O & M Admin</a>
                        <ul>
                            <li><a id="lblOANDMAdminCreate" href="#" onClick="redirectToPage('admin','OANDMRegister.jsp');">Create O & M Admin</a></li>
                            <li><a id="lblOANDMAdminView" href="#" onClick="redirectToPage('admin','OAndMAdminGrid.jsp?userTypeid=19');">View O & M Admin</a></li>

                        </ul>
                    </li>
                    <li><a href="#">O & M User</a>
                        <ul>
                            <li><a id="lblOANDMRoleView" href="#" onClick="redirectToPage('admin','OAndMRoleGrid.jsp?userTypeid=20');">Create & View User Rights Roles</a></li>
                            <li><a id="lblOANDMUserCreate" href="#" onClick="redirectToPage('admin','OANDMRegister.jsp?isUser=Yes');">Create O & M User</a></li>
                            <li><a id="lblOANDMUserView" href="#" onClick="redirectToPage('admin','OAndMAdminGrid.jsp?userTypeid=20&isUser=Yes');">View O & M User</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Performance Monitoring User</a>
                        <ul>
                            <li><a id="lblOANDMUserCreate" href="#" onClick="redirectToPage('admin','OANDMRegister.jsp?isUser=DLI');">Create Performance Monitoring User</a></li>
                            <li><a id="lblOANDMUserView" href="#" onClick="redirectToPage('admin','OAndMAdminGrid.jsp?userTypeid=21&isUser=DLI');">View Performance Monitoring User</a></li>
                        </ul>
                    </li>
                    End -->
                    <%--<li><a href="#">Company Admin</a>
                        <ul>
                            <li><a href="#">Create</a></li>
                            <li><a href="#">View</a></li>
                        </ul>
                    </li>--%>

                </ul>
                <%               } else if (userType.toString().equalsIgnoreCase("gov")) {  // Governement Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li Style = "display: none;"><a href="#">Class</a>
                        <ul>
                            <li><a id="lblClassCreation" href="CreateGrade.jsp">Create Class</a></li>
                            <li><a id="lblClassView" href="ManageGrade.jsp">View Classes</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Designation</a>
                        <ul>
                            <%-- <li><a id="lblDesignationCreation" href="CreateDesignation.jsp">Create Designation</a></li>--%>
                            <li><a id="lblDesignationView" href="ManageDesignation.jsp">View Designations</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Government Users</a>
                        <ul>
                            <%-- <li><a id="lblGovUsersCreation" href="GovtUserCreation.jsp?userType=gov">Create Government User</a></li> --%>
                            <li><a id="lblGovUsersView" href="ManageGovtUser.jsp">View Government Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%--<li><a href="#">External Evaluation Committee Member</a>
                        <ul>
                            <!--<li><a id="lblExtEvalCommCreation" href="RegExtEvalCommMember.jsp">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="ViewExtEvalCommMember.jsp">View Users</a></li>-->
                            <!--Commented out for phase 1 (Feroz Ahmed, 19th April, 2016)-->
                            <li><a id="lblExtEvalCommCreation" href="#">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="#">View Users</a></li>
                        </ul>
                    </li>--%>
                </ul>
                <% } else if (userType.toString().equalsIgnoreCase("dev")) {  // Dev. Partner Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Development Partner Organization</a>
                        <ul>
                            <li><a id="lbldevPartnerCreation" href="SchBankDevPartnerCreation.jsp?partnerType=Development">Create Development Partner Organization</a></li>
                            <li><a id="lblDevlopPartnerOrgView" href="ManageSbDpOffice.jsp?partnerType=Development">View Development Partner Organizations</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Regional/Country office</a>
                        <ul>
                            <li><a id="lblRegionalOfficeCreation" href="SchBankDevPartnerBranchCreation.jsp?partnerType=Development">Create Regional/Country Office</a></li>
                            <li><a id="lblDevPartnerManageSbDpView" href="ManageSbDpBranchOffice.jsp?partnerType=Development">View Regional/Country Offices</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Development Partner Users</a>
                        <ul>
                            <li><a id="lblDevPartnerUsersCreation" href="SchedBankCreateUser.jsp?partnerType=Development">Create Development Partner User</a></li>
                            <li><a id="lblDevPartnerUsersView" href="ManageSbDpUser.jsp?partnerType=Development">View Development Partner Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%                     } else if (userType.toString().equalsIgnoreCase("sb")) {  // Sch. Bank Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Financial Institution</a>
                        <ul>
                            <li><a id="lblDevlopPartnerOrgCreation" href="SchBankDevPartnerCreation.jsp">Create Financial Institution</a></li>
                            <li><a id="lblDevlopPartnerOrgView" href="ManageSbDpOffice.jsp">View Financial Institutions</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Branch Offices</a>
                        <ul>
                            <li><a id="lblRegionalOfficeCreation" href="SchBankDevPartnerBranchCreation.jsp">Create Financial Institution Branch Office</a></li>
                            <li><a id="lblBranchOfficeView" href="ManageSbDpBranchOffice.jsp">View Financial Institution Branch Offices</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Users</a>
                        <ul>
                            <li><a id="lblDevPartnerUsersCreation" href="SchedBankCreateUser.jsp">Create Financial Institution User</a></li>
                            <li><a id="lblDevPartnerUsersView" href="ManageSbDpUser.jsp">View Financial Institution Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%                                    } else if (userType.toString().equalsIgnoreCase("content")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">

                    <li><a href="#">Programme Information</a>
                        <ul>
                            <!-- <li><a id="lblProgInfoCreate" href="ProgrammeMaster.jsp">Create Programme</a></li>-->
                            <!-- <li><a id="lblProgInfoEdit" href="ProgrammeMasterDetails.jsp">Edit</a></li>-->
                            <li><a id="lblProgInfoView" href="ProgrammeMasterView.jsp">View Programmes</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <!--li><a href="#">Quiz</a>
                        <ul>
                            <li><a id="lblViewQM" href="QuestionModule.jsp">View Question Module</a></li>
                            <li><a id="lblCreateQM" href="CreateQuestionModule.jsp">Create Question Module</a></li>
                            <li><a id="lblViewQuestion" href="moduleForViewQuestion.jsp">View Question</a></li>
                            <li><a id="lblSetQuestion" href="setQuestion.jsp">Set Question</a></li>
                        </ul>
                    </li-->
                    
                    <li><a href="#">Project Information</a>
                        <ul>
                            <!--                            <li><a id="lblProjInfoCreate" href="CreateEditProject.jsp">Create</a></li>-->
                            <li><a id="lblProjInfoView" href="ProjectDetail.jsp">View Projects</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <!--                     <li><a href="#">Content Verification</a>
                                            <ul>
                                                <li><a href="#">Configure</a></li>
                                                <li><a href="#">View</a></li>
                                            </ul>
                                        </li>-->

                    <li><a href="#">News/Advertisement</a>
                        <ul>
                            <li><a id="lblNewsCreate" href="News.jsp?newsType=N&nId=0">Create</a></li>
                            <li><a id="lblNewsView" href="NewsManagement.jsp?newsType=N">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Circulars</a>
                        <ul>
                            <li><a id="lblCircularCreate" href="News.jsp?newsType=C&nId=0">Create</a></li>
                            <li><a id="lblCircularView" href="NewsManagement.jsp?newsType=C">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Amendments</a>
                        <ul>
                            <li><a id="lblAmendmentCreate" href="News.jsp?newsType=A&nId=0">Create</a></li>
                            <li><a id="lblAmendmentView" href="NewsManagement.jsp?newsType=A">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Notification</a>
                        <ul>
                            <li><a id="lblInstructionCreate" href="News.jsp?newsType=I&nId=0">Create</a></li>
                            <li><a id="lblInstructionView" href="NewsManagement.jsp?newsType=I">View</a></li>
                        </ul>
                    </li>
                    
                    <!--                    <li><a href="#">Announcements</a>
                                            <ul>
                                                <li><a id="lblNewsCreateA" href="News.jsp?newsType=A&nId=0">Create</a></li>
                                                <li><a id="lblNewsViewA" href="NewsManagement.jsp?newsType=A">View</a></li>
                                            </ul>
                                        </li>-->
<!--                    <li><a href="#">Marquee</a>
                        <ul>
                            <li><a id="lblMarqueeCreate" href="Marquee.jsp?mid=0&op=">Create</a></li>
                            <li><a id="lblMarqueeView" href="MarqueeListing.jsp">View</a></li>
                        </ul>
                    </li>-->


                    <li style="display: none;"><a href="#">Help</a>
                        <ul>
                            <li><a id="lblHelpAddContent" href="<%=request.getContextPath()%>/admin/HelpContent.jsp">Create</a></li>
                            <li><a id="lblHelpView" href="<%=request.getContextPath()%>/admin/HelpListing.jsp">View</a></li>                             
                        </ul>
                    </li>

                    <li><a href="#">Multilingual</a>
                        <ul>
                            <li><a id="lblMultilingualView" href="<%=request.getContextPath()%>/admin/LanguageListing.jsp">View</a></li>

                            <%--previously Hide<li><a href="#">Search</a></li>--%>
                        </ul>
                  </li>
                  <%-- Start Nitish --%>
                  <li><a href="#">FAQ</a>
                        <ul>
                            <li><a id="lblFaqAddContent" href="<%=request.getContextPath()%>/admin/addFaqContent.jsp">Create</a></li>
                            <li><a id="lblFaqView" href="<%=request.getContextPath()%>/admin/viewFaqContent.jsp">View</a></li>                             
                        </ul>
                    </li>
                <%-- END Nitish --%>
                   <%-- <li><a href="#">BOQ</a>
                        <ul>
                            <li><a id="lblBOQAdd" href="#" onClick="redirectToPage('admin', 'AddBOQ.jsp');">Add</a></li>
                            <li><a id="lblBOQList" href="#" onClick="redirectToPage('admin', 'BOQList.jsp');">View</a></li>
                        </ul>
                    </li>
                  </li>    --%>                

                    <%--Hide
                   <li><a href="#">Media</a>
                       <ul>
                           <li><a href="#">Press Release</a>
                               <ul>
                                   <li><a id="lblPressCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Press">Post</a></li>
                                   <li><a id="lblPressView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Press">View</a></li>
                               </ul>
                           </li>

                            <li><a href="#">Complaint Resolution</a>
                                <ul>
                                    <li><a id="lblComplaintCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Complaint">Post</a></li>
                                    <li><a id="lblComplaintView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Complaint">View</a></li>
                                </ul>
                            </li>

                            <li><a href="#">Other Information</a>
                                <ul>
                                    <li><a id="lblOtherInfoCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Other">Post</a></li>
                                    <li><a id="lblOtherInfoView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Other">View</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    
                  end --%>

                    <!-- Edit by Palash, Dohatec -->

                    <!--
                   <li><a href="#">Performance Indicators Report</a>
                        <ul>
                            <li><a id="lblNewsCreate" href="#" onClick="redirectToPage('admin','IndicatorsDocsUpload.jsp?userId=<%=loguserId%>');">Upload and View</a></li>
                        </ul>
                    </li> -->

                </ul>
                <!-- Edit by Palash, Dohatec -->
                <% } else if (userType.toString().equalsIgnoreCase("config")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                  <%--  <li><a href="#">Workflow Configuration</a>
                        <ul>
                            <li><a id="lbWorkflowConfig" href="WorkflowAdmin.jsp">Configure</a></li>
                        </ul>
                    </li>--%>
                    <li><a href="#">Business Rule Configuration</a>
                        <%
                            NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                            long cnt = navRuleUtil.getProcCnt();
                        %>
                        <ul>
                            <li><a href="#">Procurement method Rules</a>
                                <ul>
                                    <%
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblProcMethodAdd" href="ProcMethodRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblProcMethodAdd" href="ProcMethodRule.jsp">Add</a></li>
                                    <!--                                    <li><a id="lblProcMethodEdit" href="ProcMethodRuleDetails.jsp">Edit</a></li>-->
                                    <%}%>
                                    <li><a id="lblProcMethodView" href="ProcMethodRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Threshold Configuration</a>
                                <ul>
                                    <li><a id="lblConfigPaThreshold" href="ViewConfigPaThreshold.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pre-Bid Meeting Rules</a>

                                <ul>
                                    <%
                                        cnt = navRuleUtil.getPreTenderCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblConfigPreTenderMettingRuleAdd" href="ConfigPreTenderMettingRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblConfigPreTenderMettingRuleEdit" href="ConfigPreTenderMettingRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblConfigPreTenderMettingRuleView" href="ConfigPreTenderMettingRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Amendment Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getAmendmentCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblAmendmentRuleAdd" href="AmendmentRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblAmendmentRuleEdit" href="AmendmentRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblAmendmentRuleView" href="AmendmentRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TOC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TOC", "POC");
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTOCFormationRuleAdd" href="TOCFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTOCFormationRuleEdit" href="TOCFormationRuleDetails.jsp">Edit</a></li>
                                        <% }%>
                                    <li><a id="lblTOCFormationRuleView" href="TOCFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TEC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TEC", "PEC");
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTECFormationRuleAdd" href="TECFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTECFormationRuleEdit" href="TECFormationRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblTECFormationRuleView" href="TECFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TC", "PC");
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTSCFormationRuleAdd" href="TSCFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTSCFormationRuleEdit" href="TSCFormationRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblTSCFormationRuleView" href="TSCFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">SBD Selection Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getSTDCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblSTDRuleAdd" href="STDRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblSTDRuleEdit" href="STDRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblSTDRuleView" href="STDRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                         <li><a href="#">Letter of Acceptance (LOA) Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getNoaCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblNOARuleAdd" href="NOABusinessRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblNOARuleEdit" href="NOABusinessRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblNOARuleView" href="NOABusinessRuleView.jsp">View</a></li>
                                </ul>
                            </li>

                            <li><a href="javascript:void(0);">Evaluation Method</a>
                                <ul>
                                    <%
                                        long evalCnt = navRuleUtil.getEvalCnt();
                                        if (evalCnt == 0) {
                                    %>
                                    <li><a id="lblEvalMethodAdd" href="EvalMethodConfiguration.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblEvalMethodEdit" href="EvalMethodConfiguration.jsp?isedit=y">Edit</a></li>
                                    <li><a id="lblEvalMethodView" href="ViewEvalMethodConfiguration.jsp">View</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);">Clarification Days</a>
                                <ul>
                                    <%
//                                        long ClarificationCnt = navRuleUtil.getClarificationCnt();
//                                        if (ClarificationCnt == 0) {
                                    %>
                                    <!--<li><a id="lblConfigClarificationAdd" href="ConfigureClarificationDays.jsp">Add</a></li>-->
                                        <%//} else {%>
                                    <!--<li><a id="lblConfigClarificationEdit" href="ConfigureClarificationDays.jsp?isedit=y">Edit</a></li>-->
                                    <li><a id="lblConfigClarificationView" href="ViewConfigureClarificationDays.jsp">View and Edit</a></li>
                                        <%// }%>
                                </ul>
                            </li>

                           <%--  <li><a href="javascript:void(0);">Tender/Proposal Payment Configuration</a>
                                <ul>
                                    <%
                                        long payCnt = navRuleUtil.getPayCnt();
                                        if (payCnt == 0) {
                                    %>
                                    <li><a id="lblTenPayAdd" href="TenPaymentConfiguration.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTenPayEdit" href="TenPaymentConfiguration.jsp?isedit=y">Edit</a></li>
                                    <li><a id="lblTenPayView" href="ViewTenPaymentConfig.jsp">View</a></li>
                                        <%}%>
                                </ul>
                            </li>--%>
                            <%--<li><a href="#">DoFP Rules</a>
                                <ul>
                                    <%
                                            cnt = navRuleUtil.getFinPowCnt();
                                            if(cnt == 0){
                                    %>
                                    <li><a id="lblAssignFinePowAdd" href="AssignFinPower.jsp">Add</a></li>
                                    <%
                                            }else{
                                    %>
                                    <li><a id="lblAssignFinePowEdit" href="AssignFinPowerDetails.jsp">Edit</a></li>
                                    <%}%>
                                    <li><a id="lblAssignFinePowView" href="AssignFinPowerView.jsp">View</a></li>
                                </ul>
                            </li>--%>

                            <!-- Dohatec : Istiak  -->
                            <%--<li>    <!-- 25.May.15 -->
                                <a href="javascript:void(0);">Procurement Approval Timeline</a>                                
                                <ul>
                                    <%
                                        String isEdit = "";
                                        if (navRuleUtil.getRuleCount() > 0) {
                                            isEdit = "isEdit=y";
                                    %>
                                    <li><a id="procAppTimeEdit" href="ProcurementApprovalTimelineConfig.jsp?<%=isEdit%>">Edit</a></li>
                                    <li><a id="procAppTimeView" href="ViewProcurementApprovalTimeline.jsp">View</a></li>
                                        <%} else {%>
                                    <li><a id="procAppTimeEdit" href="ProcurementApprovalTimelineConfig.jsp">Add</a></li>
                                        <%}%>
                                </ul>
                            </li>

                            <li>    <!-- 14.Jun.15 -->
                                <a href="javascript:void(0);">Financial Delegation</a>
                                <ul>
                                    <%
                                        if (navRuleUtil.getFinancialDelegationCount() > 0) {
                                    %>
                                    <li><a id="finDelEdit" href="FinancialDelegationConfig.jsp?isEdit=y">Edit</a></li>
                                    <li><a id="finDelView" href="ViewFinancialDelegation.jsp">View</a></li>
                                        <%} else {%>
                                    <li><a id="finDelAdd" href="FinancialDelegationConfig.jsp">Add</a></li>
                                        <%}%>

                                    <li><a href="javascript:void(0);">Rank Configuration</a>
                                        <ul>
                                            <%
                                                if (navRuleUtil.getDelegationRankCnt() > 0) {
                                            %>
                                            <li><a id="designationRankEdit" href="DelegationDesignationRank.jsp?f=e">Edit</a></li>
                                            <li><a id="designationRankView" href="DelegationDesignationRank.jsp?f=v">View</a></li>
                                                <%  } else {%>
                                            <li><a id="designationRankAdd" href="DelegationDesignationRank.jsp?f=a">Add</a></li>
                                                <%  }%>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- End Dohatec -->--%>

                        </ul>
                    </li>
                    <li><a href="#">Financial Year Configuration</a>
                        <ul>
                            <li><a id="lblFinancialYear" href="FinancialYear.jsp">Configure</a></li>
                            <li><a id="lblFinancialYearView" href="FinancialYearDetails.jsp">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">APP Revise Date Configuration</a>
                        <ul>
                            <li><a href="AppReviseConfiguration.jsp">Configure</a></li>
                            <li><a href="AppReviseConfigurationView.jsp">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Holiday Configuration</a>
                        <ul>
                            <li><a id="lblHolidyConfig" href="ConfigureCalendar.jsp">Configure</a></li>
                            <li><a id="lblHolidyConfigView" href="HolidayConfigurationView.jsp">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Document Size Configuration</a>
                        <ul>
                            <li><a id="lblConfigDoc" href="DocConfiguration.jsp">Configure</a></li>
                            <li><a id="lblConfigDocView" href="DocConfigurationView.jsp">View</a></li>
                        </ul>
                    </li>
                    <li><a href="#">BSR Configuration</a>
                        <ul>
                            <li><a id="lblBOQAdd" href="#" onClick="redirectToPage('admin', 'AddBOQ.jsp');">Add</a></li>
                            <li><a id="lblBOQList" href="#" onClick="redirectToPage('admin', 'BOQList.jsp');">View</a></li>
                      </ul>
                    </li>
                    <!-- Begin hide 
                    <li><a href="#">Web-Service Configuration</a>
                        <ul>
                            <li><a href="#">Web-Service Detail</a>
                                <ul>
                    <%--<li><a id="lblWsDetailEdit" href="WSList.jsp?mode=edit">Edit</a></li>--%>
                    <li><a id="lblWsDetailView" href="WSList.jsp?mode=view">View</a></li>
                </ul>
            </li>
            <li><a href="#">Web-Service Organization</a>
                <ul>
                    <%--<li><a id="lblWsOrganizationEdit" href="WSConsumersList.jsp?mode=edit">Edit</a></li>--%>
                    <li><a id="lblWsOrganizationView" href="WSConsumersList.jsp?mode=view">View</a></li>
                </ul>
            </li>
            <li><a href="#">VAS Configuration</a>
                <ul>
                    <li><a id="lblWsOrganizationView" href="/AdvertisementConfig?funct=EGPadminConf">Advertisement</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    end  -->
                    <!--
                    <li><a href="#">Tax Configurations</a>
                        <ul>
                            <li><a href="#">Tax-Configuration</a>
                                <ul>
                                    <li>
                                        <a id="lblTaxConfiguration" href="ViewTaxConfiguration.jsp">View Tax Configuration</a>
                                    </li>
                                </ul>
                            </li>
                      <li><a href="#">AIT-Configuration</a>
                                <ul>
                                    <li>
                                        <a id="lblAITConfiguration" href="AITConfigutation.jsp">View</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>-->

                    <!--                           BugId 0005127: Please hide this option for the time being from the left panel.

                                         <li><a href="#">Content Verification</a>
                                            <ul>
                                                <li><a href="#">Configure</a></li>
                                                <li><a href="#">View</a></li>
                                            </ul>
                                        </li>-->

                    <!--                    <li><a href="#">Content Verification</a>
                                            <ul>
                                                <li><a href="#">Configure</a></li>
                                                <li><a href="#">View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">News</a>
                                            <ul>
                                                <li><a id="lblNewsCreate" href="News.jsp?newsType=N&nId=0">Create</a></li>
                                                <li><a id="lblNewsView" href="NewsManagement.jsp?newsType=N">View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Announcements</a>
                                            <ul>
                                                <li><a id="lblNewsCreateA" href="News.jsp?newsType=A&nId=0">Create</a></li>
                                                <li><a id="lblNewsViewA" href="NewsManagement.jsp?newsType=A">View</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Marquee</a>
                                            <ul>
                                                <li><a id="lblMarqueeCreate" href="Marquee.jsp?mid=0&op=">Create</a></li>
                                                <li><a id="lblMarqueeView" href="MarqueeListing.jsp">View</a></li>
                                            </ul>
                                        </li>-->
                </ul>
                <% }
                } else if (userTypeId == 4) {
                    // PEAdmin
                    if ("gov".equalsIgnoreCase(userType.toString())) {
                %>
                <ul id="treemenu1" class="treeview">
                    <li><a href="#">Designation</a>
                        <ul>
                            <li><a id="lblDesignationCreation" href="CreateDesignation.jsp">Create Designation</a></li>
                            <li><a id="lblDesignationView" href="ManageDesignation.jsp">View Designations</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Government Users</a>
                        <ul>
                            <li><a id="lblGovUsersCreation" href="GovtUserCreation.jsp">Create Government Users</a></li>
                            <li><a id="lblGovUsersView" href="ManageGovtUser.jsp">View Government Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%--<li><a href="#">External Evaluation Committee Member</a>
                        <ul>
                            <!--<li><a id="lblExtEvalCommCreation" href="RegExtEvalCommMember.jsp">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="ViewExtEvalCommMember.jsp">View Users</a></li>-->
                            <!--Commented out for phase 1 (Feroz Ahmed, 19th April, 2016)-->
                            <li><a id="lblExtEvalCommCreation" href="#">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="#">View Users</a></li>
                        </ul>
                    </li>--%>
                    <li><a href="#">Programme Information</a>
                        <ul>
                            <li><a id="lblProgInfoCreate" href="ProgrammeMaster.jsp">Create Programme</a></li>
                            <!--<li><a id="lblProgInfoEdit" href="ProgrammeMasterDetails.jsp">Edit</a></li>-->
                            <li><a id="lblProgInfoView" href="ProgrammeMasterView.jsp">View Programmes</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Project Information</a>
                        <ul>
                            <li><a id="lblProjInfoCreate" href="CreateEditProject.jsp">Create Project</a></li>
                            <li><a id="lblProjInfoView" href="ProjectDetail.jsp">View Projects</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%}%>
                <%--                                                  if (userType.toString().equalsIgnoreCase("content")) {%>
                                        <ul id="treemenu1" class="treeview TreeViewBorder">

                            <li><a href="#">Programme Information</a>
                                <ul>
                                    <li><a id="lblProgInfoCreate" href="ProgrammeMaster.jsp">Create Programme</a></li>
                                    <!--<li><a id="lblProgInfoEdit" href="ProgrammeMasterDetails.jsp">Edit</a></li>-->
                                    <li><a id="lblProgInfoView" href="ProgrammeMasterView.jsp">View Programmes</a></li>
                <%--<li><a href="#">Search</a></li>
            </ul>
        </li>
        <li><a href="#">Project Information</a>
            <ul>
                <li><a id="lblProjInfoCreate" href="CreateEditProject.jsp">Create Project</a></li>
                <li><a id="lblProjInfoView" href="ProjectDetail.jsp">View Projects</a></li>
                <%--<li><a href="#">Search</a></li>
            </ul>
        </li>
    </ul>
    <%}%>--%>
                <%} else if (userTypeId == 5) {  //Org Admin%>
                <%if (userType.toString().equalsIgnoreCase("content")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">

                    <li><a href="#">Programme Information</a>
                        <ul>
                            <li><a id="lblProgInfoCreate" href="ProgrammeMaster.jsp">Create Programme</a></li>
                            <!--<li><a id="lblProgInfoEdit" href="ProgrammeMasterDetails.jsp">Edit</a></li>-->
                            <li><a id="lblProgInfoView" href="ProgrammeMasterView.jsp">View Programmes</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Project Information</a>
                        <ul>
                            <li><a id="lblProjInfoCreate" href="CreateEditProject.jsp">Create Project</a></li>
                            <li><a id="lblProjInfoView" href="ProjectDetail.jsp">View Projects</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%} else if (userType.toString().equalsIgnoreCase("gov")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">PE Offices</a>
                        <ul>
                            <li><a href="PEOfficeCreation.jsp?deptType=Organization">Create PE Office</a></li>
                            <li><a href="EditPEOfficeGrid.jsp?deptType=Organization">View PE Offices</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">PE Admin Users</a>
                        <ul>
                            <li><a href="PEAdminRegister.jsp">Create PE Admin</a></li>
                            <li><a href="PEAdminGrid.jsp?userTypeid=4">View PE Admins</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <!--                    <li><a href="#">Grades</a>
                                            <ul>
                                                <li><a href="CreateGrade.jsp">Create</a></li>
                                                <li><a href="ManageGrade.jsp">View</a></li>
                                            </ul>
                                        </li>bug id :: 5247 -->
                    <li><a href="#">Designation</a>
                        <ul>
                            <li><a href="CreateDesignation.jsp">Create Designation</a></li>
                            <li><a href="ManageDesignation.jsp">View Designations</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Government Users</a>
                        <ul>
                            <li><a id="lblGovUsersCreation" href="GovtUserCreation.jsp?userType=gov">Create User</a></li>
                            <li><a id="lblGovUsersView" href="ManageGovtUser.jsp">View Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%-- <li><a href="#">External Evaluation Committee Member</a>
                        <ul>
                            <!--<li><a id="lblExtEvalCommCreation" href="RegExtEvalCommMember.jsp">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="ViewExtEvalCommMember.jsp">View Users</a></li>-->
                            <!--Commented out for phase 1 (Feroz Ahmed, 19th April, 2016)-->
                            <li><a id="lblExtEvalCommCreation" href="#">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="#">View Users</a></li>
                        </ul>
                    </li>--%>
                </ul>
                <%}%>

                <%                                } else if (userTypeId == 6) {  // Development
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Regional/Country Office</a>
                        <ul>
                            <li><a href="SchBankDevPartnerBranchCreation.jsp?partnerType=Development">Create Regional/Country Office</a></li>
                            <li><a href="ManageSbDpBranchOffice.jsp?partnerType=Development">View Regional/Country Office</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Users</a>
                        <ul>
                            <li><a href="SchedBankCreateUser.jsp?partnerType=Development">Create User</a></li>
                            <li><a href="ManageSbDpUser.jsp?partnerType=Development">View User</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%                                } else if (userTypeId == 7) {  // ScheduleBank
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Financial Institution Branch Offices</a>
                        <ul>
                            <li><a href="SchBankDevPartnerBranchCreation.jsp">Create Financial Institution Branch Office</a></li>
                            <li><a href="ManageSbDpBranchOffice.jsp">View Financial Institution Branch Offices</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Branch Admin</a>
                        <ul>
                            <li><a id="lblScheBankBranchAdminCreation" href="#" onClick="redirectToPage('admin', 'ScheduledBranchAdmin.jsp');">Create Financial Institution Branch Admin</a></li>
                            <li><a id="lblScheBankBranchAdminView" href="ManageSbBranchAdmin.jsp">View Financial Institution Branch Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Users</a>
                        <ul>
                            <li><a href="SchedBankCreateUser.jsp">Create Financial Institution User</a></li>
                            <li><a href="ManageSbDpUser.jsp">View Financial Institution Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%                                } else if (userTypeId == 8) { //Content Admin %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Company Profile Verification</a></li>
                </ul>
                <%} else if (userTypeId == 11) { //Company Admin
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Approval of Company’s User Profile</a></li>
                    <li><a href="#">Assignment of tender participation rights</a></li>
                </ul>
                <%} else if (userTypeId == 15) { //Schedule Branch Admin
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Users</a>
                        <ul>
                            <li><a href="SchedBankCreateUser.jsp">Create User</a></li>
                            <li><a href="ManageSbDpUser.jsp">View Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                </ul>
                <%                                            } else if (userTypeId == 19 || userTypeId == 20) { // O & M Admin
                    if ("org".equalsIgnoreCase(userType.toString())) { // Organization Strucuture
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Ministry</a>
                        <ul>
                            <li><a id="lblMinistryCreation" href="DepartmentCreation.jsp?deptType=Ministry">Create Ministry</a></li>
                            <li><a id="lblViewMinistry" href="ManageEmployee.jsp?deptType=Ministry">View Ministries</a></li>
                            <li><a id="lblMinistryPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Ministry">Create PE Office</a></li>
                            <li><a id="lblMinistryPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Ministry">View PE Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Division</a>
                        <ul>
                            <li><a id="lblDivisionCreation" href="DepartmentCreation.jsp?deptType=Division">Create Division</a></li>
                            <li><a id="lblViewDivision" href="ManageEmployee.jsp?deptType=Division">View Divisions</a></li>
                            <li><a id="lblDivisionPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Division">Create PA Office</a></li>
                            <li><a id="lblDivisionPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Division">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Organization</a>
                        <ul>
                            <li><a id="lblOrganizationCreation" href="DepartmentCreation.jsp?deptType=Organization">Create Organization</a></li>
                            <li><a id="lblOrganizationView" href="ManageEmployee.jsp?deptType=Organization">View Organizations</a></li>
                            <li><a id="lblOrganizationPEOfficeCreation" href="PEOfficeCreation.jsp?deptType=Organization">Create PA Office</a></li>
                            <li><a id="lblOrganizationPEOfficeUpdate" href="EditPEOfficeGrid.jsp?deptType=Organization">View PA Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Development Partner</a>
                        <ul>
                            <li><a id="lbldevPartnerCreation" href="SchBankDevPartnerCreation.jsp?partnerType=Development">Create Development Partner Organization</a></li>
                            <li><a id="lbldevPartnerManage" href="ManageSbDpOffice.jsp?partnerType=Development">View Development Partner Organizations</a></li>
                            <li><a href="#">Regional/Country Office</a>
                                <ul>
                                    <li><a id="lblDevPartnerSchBankCreation" href="SchBankDevPartnerBranchCreation.jsp?partnerType=Development">Create Regional/Country Office</a></li>
                                    <li><a id="lblDevPartnerManageSbDpView" href="ManageSbDpBranchOffice.jsp?partnerType=Development">View Regional/Country Offices</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution</a>
                        <ul>
                            <li><a id="lblSchBankCreation" href="SchBankDevPartnerCreation.jsp">Create Financial Institution</a></li>
                            <li><a id="lblSchBankView" href="ManageSbDpOffice.jsp">View Financial Institutions</a></li>
                            <li><a href="#">Branch</a>
                                <ul>
                                    <li><a id="lblSchBankDevCreation" href="SchBankDevPartnerBranchCreation.jsp">Create Financial Institution Branch</a></li>
                                    <li><a id="lblManageSbDpView" href="ManageSbDpBranchOffice.jsp">View Financial Institution Branches</a></li>
                                </ul>
                            </li>
                        </ul>

                    </li>
                    <li><a href="#">Review Panel</a>
                        <ul>
                            <li><a id="lblreveiwpanelcreatioin" href="reviewPanel.htm">Create</a></li>
                            <li><a id="lblreveiwpanelview" href="ViewreviewPanel.htm">View</a></li>
                        </ul>
                    </li>
                </ul>
                <%                                    } else if ("admin".equalsIgnoreCase(userType.toString())) {  // Admin Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Organization Admin</a>
                        <ul>
                            <li><a id="lblOrgAdminCreation" href="#" onClick="redirectToPage('admin', 'OrgAdminReg.jsp');">Create Organization Admin</a></li>
                                <%--<li><a href="#" onClick="redirectToPage('admin','ManageOrgAdmin.jsp');">View</a></li>--%>
                            <li><a id="lblOrgAdminView" href="#" onClick="redirectToPage('admin', 'OrganizationAdminGrid.jsp?userTypeid=5');">View Organization Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">PE Admin</a>
                        <ul>
                            <li><a id="lblPEAdminCreation" href="#" onClick="redirectToPage('admin', 'PEAdminRegister.jsp');">Create PE Admin</a></li>
                            <li><a id="lblPEAdminView" href="#" onClick="redirectToPage('admin', 'PEAdminGrid.jsp?userTypeid=4');">View PE Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Development Partner Admin</a>
                        <ul>
                            <li><a id="lblDevPartnerAdminCreation" href="#" onClick="redirectToPage('admin', 'DevPartnerAdminRegister.jsp');">Create Development Partner Admin</a></li>
                            <li><a id="lblDevPartnerAdminView" href="ManageSbDpAdmin.jsp?partnerType=Development">View Development Partner Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Admin</a>
                        <ul>
                            <li><a id="lblScheBankAdminCreation" href="#" onClick="redirectToPage('admin', 'ScheduledBankAdmin.jsp');">Create Financial Institution Admin</a></li>
                            <li><a id="lblScheBankAdminView" href="ManageSbDpAdmin.jsp">View Financial Institution Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Admin</a>
                        <ul>
                            <li><a id="lblScheBankBranchAdminCreation" href="#" onClick="redirectToPage('admin', 'ScheduledBranchAdmin.jsp');">Create Financial Institution Branch Admin</a></li>
                            <li><a id="lblScheBankBranchAdminView" href="ManageSbBranchAdmin.jsp">View Financial Institution Branch Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Content Admin</a>
                        <ul>
                            <li><a id="lblContentAdminCreation" href="#" onClick="redirectToPage('admin', 'ContentAdminRegister.jsp');">Create Content Admin</a></li>
                            <li><a id="lblContentAdminView" href="#" onClick="redirectToPage('admin', 'ContentAdminGrid.jsp?userTypeid=8');">View Content Admins</a></li>

                        </ul>
                    </li>
                    <li><a href="#">Procurement Expert User</a>
                        <ul>
                            <li><a id="lblProcureExpAdminCreation" href="#" onClick="redirectToPage('admin', 'ProcExpertAdminRegister.jsp');">Create Procurement Expert User</a></li>
                            <li><a id="lblProcureExpAdminView" href="#" onClick="redirectToPage('admin', 'ProcExpertAdminGrid.jsp?userTypeid=12');">View Procurement Expert Users</a></li>

                        </ul>
                    </li>

                    <%
                        if (userTypeId == 19) {
                    %>
                    <li><a href="#">O & M User</a>
                        <ul>
                            <li><a id="lblOANDMRoleView" href="#" onClick="redirectToPage('admin', 'OAndMRoleGrid.jsp?userTypeid=20');">Create & View User Rights Roles</a></li>
                            <li><a id="lblOANDMUserCreate" href="#" onClick="redirectToPage('admin', 'OANDMRegister.jsp?isUser=Yes');">Create O & M User</a></li>
                            <li><a id="lblOANDMUserView" href="#" onClick="redirectToPage('admin', 'OAndMAdminGrid.jsp?userTypeid=20&isUser=Yes');">View O & M User</a></li>
                        </ul>
                    </li>
                    <%                                                    }
                    %>
                </ul>
                <%
                } else if (userType.toString().equalsIgnoreCase("gov")) {  // Governement Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Class</a>
                        <ul>
                            <li><a id="lblClassCreation" href="CreateGrade.jsp">Create Class</a></li>
                            <li><a id="lblClassView" href="ManageGrade.jsp">View Classes</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Designation</a>
                        <ul>
                            <li><a id="lblDesignationCreation" href="CreateDesignation.jsp">Create Designation</a></li>
                            <li><a id="lblDesignationView" href="ManageDesignation.jsp">View Designations</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Government Users</a>
                        <ul>
                            <li><a id="lblGovUsersCreation" href="GovtUserCreation.jsp?userType=gov">Create Government User</a></li>
                            <li><a id="lblGovUsersView" href="ManageGovtUser.jsp">View Government Users</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%--<li><a href="#">External Evaluation Committee Member</a>
                        <ul>
                            <!--<li><a id="lblExtEvalCommCreation" href="RegExtEvalCommMember.jsp">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="ViewExtEvalCommMember.jsp">View Users</a></li>-->
                            <!--Commented out for phase 1 (Feroz Ahmed, 19th April, 2016)-->
                            <li><a id="lblExtEvalCommCreation" href="#">Create User</a></li>
                            <li><a id="lblExtEvalCommView" href="#">View Users</a></li>
                        </ul>
                    </li>--%>
                </ul>
                <%                                    } else if (userType.toString().equalsIgnoreCase("dev")) {  // Dev. Partner Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Development Partner Organization</a>
                        <ul>
                            <li><a id="lbldevPartnerCreation" href="SchBankDevPartnerCreation.jsp?partnerType=Development">Create Development Partner Organization</a></li>
                            <li><a id="lblDevlopPartnerOrgView" href="ManageSbDpOffice.jsp?partnerType=Development">View Development Partner Organizations</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Regional/Country office</a>
                        <ul>
                            <li><a id="lblRegionalOfficeCreation" href="SchBankDevPartnerBranchCreation.jsp?partnerType=Development">Create Regional/Country Office</a></li>
                            <li><a id="lblDevPartnerManageSbDpView" href="ManageSbDpBranchOffice.jsp?partnerType=Development">View Regional/Country Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Development Partner Users</a>
                        <ul>
                            <li><a id="lblDevPartnerUsersCreation" href="SchedBankCreateUser.jsp?partnerType=Development">Create Development Partner User</a></li>
                            <li><a id="lblDevPartnerUsersView" href="ManageSbDpUser.jsp?partnerType=Development">View Development Partner Users</a></li>
                        </ul>
                    </li>
                </ul>
                <%                                    } else if (userType.toString().equalsIgnoreCase("sb")) {  // Sch. Bank Users
                %>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Financial Institution</a>
                        <ul>
                            <li><a id="lblDevlopPartnerOrgCreation" href="SchBankDevPartnerCreation.jsp">Create Financial Institution</a></li>
                            <li><a id="lblDevlopPartnerOrgView" href="ManageSbDpOffice.jsp">View Financial Institutions</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Branch Offices</a>
                        <ul>
                            <li><a id="lblRegionalOfficeCreation" href="SchBankDevPartnerBranchCreation.jsp">Create Financial Institution Branch Office</a></li>
                            <li><a id="lblBranchOfficeView" href="ManageSbDpBranchOffice.jsp">View Financial Institution Branch Offices</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Financial Institution Users</a>
                        <ul>
                            <li><a id="lblDevPartnerUsersCreation" href="SchedBankCreateUser.jsp">Create Financial Institution User</a></li>
                            <li><a id="lblDevPartnerUsersView" href="ManageSbDpUser.jsp">View Financial Institution Users</a></li>
                        </ul>
                    </li>
                </ul>
                <%                                    } else if (userType.toString().equalsIgnoreCase("content")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Programme Information")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Programme Information</a>
                        <ul>
                            <li><a id="lblProgInfoView" href="ProgrammeMasterView.jsp">View Programmes</a></li>
                        </ul>
                    </li>
                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Project Information")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Project Information</a>
                        <ul>
                            <li><a id="lblProjInfoView" href="ProjectDetail.jsp">View Projects</a></li>
                        </ul>
                    </li>

                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("News/Events")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">News/Advertisement</a>
                        <ul>
                            <li><a id="lblNewsCreate" href="News.jsp?newsType=N&nId=0">Create</a></li>
                            <li><a id="lblNewsView" href="NewsManagement.jsp?newsType=N">View</a></li>
                        </ul>
                    </li>

                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Marquee")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Marquee</a>
                        <ul>
                            <li><a id="lblMarqueeCreate" href="Marquee.jsp?mid=0&op=">Create</a></li>
                            <li><a id="lblMarqueeView" href="MarqueeListing.jsp">View</a></li>
                        </ul>
                    </li>

                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Help")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Help</a>
                        <ul>
                            <li><a id="lblHelpAddContent" href="<%=request.getContextPath()%>/admin/HelpContent.jsp">Create</a></li>
                            <li><a id="lblHelpView" href="<%=request.getContextPath()%>/admin/HelpListing.jsp">View</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Multilingual")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Multilingual</a>
                        <ul>
                            <li><a id="lblMultilingualView" href="<%=request.getContextPath()%>/admin/LanguageListing.jsp">View</a></li>
                                <%--<li><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Media")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Media</a>
                        <ul>
                            <li><a href="#">Press Release</a>
                                <ul>
                                    <li><a id="lblPressCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Press">Post</a></li>
                                    <li><a id="lblPressView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Press">View</a></li>
                                </ul>
                            </li>

                            <li><a href="#">Complaint Resolution</a>
                                <ul>
                                    <li><a id="lblComplaintCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Complaint">Post</a></li>
                                    <li><a id="lblComplaintView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Complaint">View</a></li>
                                </ul>
                            </li>

                            <li><a href="#">Other Information</a>
                                <ul>
                                    <li><a id="lblOtherInfoCreate" href="<%=request.getContextPath()%>/admin/PostMediaContent.jsp?contentType=Other">Post</a></li>
                                    <li><a id="lblOtherInfoView" href="<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType=Other">View</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- Edit by Palash, Dohatec -->
                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Performance Indicators Report")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                          }

                        %>
                        <a href="#">Performance Indicators Report</a>
                        <ul>
                            <li><a id="lblNewsCreate" href="#" onClick="redirectToPage('admin', 'IndicatorsDocsUpload.jsp?userId=<%=loguserId%>');">Upload and View</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- End-->
                <%
                } else if (userType.toString().equalsIgnoreCase("config")) {%>
                <ul id="treemenu1" class="treeview TreeViewBorder">

                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Workflow Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Workflow Configuration</a>
                        <ul>
                            <li><a id="lbWorkflowConfig" href="WorkflowAdmin.jsp">Configure</a></li>
                        </ul>
                    </li>
                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Business Rule Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Business Rule Configuration</a>
                        <%                            NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                            long cnt = navRuleUtil.getProcCnt();
                        %>
                        <ul>
                            <li><a href="#">Procurement method Rules</a>
                                <ul>
                                    <%
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblProcMethodAdd" href="ProcMethodRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblProcMethodEdit" href="ProcMethodRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblProcMethodView" href="ProcMethodRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Threshold Configuration</a>
                                <ul>
                                    <li><a id="lblConfigPaThreshold" href="ViewConfigPaThreshold.jsp">View and Edit</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pre-Bid Meeting Rules</a>

                                <ul>
                                    <%
                                        cnt = navRuleUtil.getPreTenderCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblConfigPreTenderMettingRuleAdd" href="ConfigPreTenderMettingRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblConfigPreTenderMettingRuleEdit" href="ConfigPreTenderMettingRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblConfigPreTenderMettingRuleView" href="ConfigPreTenderMettingRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Amendment Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getAmendmentCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblAmendmentRuleAdd" href="AmendmentRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblAmendmentRuleEdit" href="AmendmentRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblAmendmentRuleView" href="AmendmentRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TOC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TOC", "POC");
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTOCFormationRuleAdd" href="TOCFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTOCFormationRuleEdit" href="TOCFormationRuleDetails.jsp">Edit</a></li>
                                        <% }%>
                                    <li><a id="lblTOCFormationRuleView" href="TOCFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TEC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TEC", "PEC");
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTECFormationRuleAdd" href="TECFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTECFormationRuleEdit" href="TECFormationRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblTECFormationRuleView" href="TECFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">TSC Formation Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getTECCnt("TSC", null);
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblTSCFormationRuleAdd" href="TSCFormationRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTSCFormationRuleEdit" href="TSCFormationRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblTSCFormationRuleView" href="TSCFormationRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">SBD Selection Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getSTDCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblSTDRuleAdd" href="STDRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblSTDRuleEdit" href="STDRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblSTDRuleView" href="STDRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Letter of Acceptance (LOA) Rules</a>
                                <ul>
                                    <%
                                        cnt = navRuleUtil.getNoaCnt();
                                        if (cnt == 0) {
                                    %>
                                    <li><a id="lblNOARuleAdd" href="NOABusinessRule.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblNOARuleEdit" href="NOABusinessRuleDetails.jsp">Edit</a></li>
                                        <%}%>
                                    <li><a id="lblNOARuleView" href="NOABusinessRuleView.jsp">View</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);">Evaluation Method</a>
                                <ul>
                                    <%
                                        long evalCnt = navRuleUtil.getEvalCnt();
                                        if (evalCnt == 0) {
                                    %>
                                    <li><a id="lblEvalMethodAdd" href="EvalMethodConfiguration.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblEvalMethodEdit" href="EvalMethodConfiguration.jsp?isedit=y">Edit</a></li>
                                    <li><a id="lblEvalMethodView" href="ViewEvalMethodConfiguration.jsp">View</a></li>
                                        <%}%>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);">Tender Payment Configuration</a>
                                <ul>
                                    <%
                                        long payCnt = navRuleUtil.getPayCnt();
                                        if (payCnt == 0) {
                                    %>
                                    <li><a id="lblTenPayAdd" href="TenPaymentConfiguration.jsp">Add</a></li>
                                        <%} else {%>
                                    <li><a id="lblTenPayEdit" href="TenPaymentConfiguration.jsp?isedit=y">Edit</a></li>
                                    <li><a id="lblTenPayView" href="ViewTenPaymentConfig.jsp">View</a></li>
                                        <%}%>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <%
                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Financial Year Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Financial Year Configuration</a>
                        <ul>
                            <li><a id="lblFinancialYear" href="FinancialYear.jsp">Configure</a></li>
                            <li><a id="lblFinancialYearView" href="FinancialYearDetails.jsp">View</a></li>
                        </ul>
                    </li>
                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Holiday Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Holiday Configuration</a>
                        <ul>
                            <li><a id="lblHolidyConfig" href="ConfigureCalendar.jsp">Configure</a></li>
                            <li><a id="lblHolidyConfigView" href="HolidayConfigurationView.jsp">View</a></li>
                        </ul>
                    </li>
                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Document Size Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Document Size Configuration</a>
                        <ul>
                            <li><a id="lblConfigDoc" href="DocConfiguration.jsp">Configure</a></li>
                            <li><a id="lblConfigDocView" href="DocConfigurationView.jsp">View</a></li>
                        </ul>
                    </li>
                    <%                        if (objUserRightsMapTop != null && objUserRightsMapTop.containsValue("Web-Service Configuration")) {
                    %>  <li>  <%                                                                } else {
                    %>  <li style="display: none;">  <%                                                         }

                        %>
                        <a href="#">Web-Service Configuration</a>
                        <ul>
                            <li><a href="#">Web-Service Detail</a>
                                <ul>
                                    <%--<li><a id="lblWsDetailEdit" href="WSList.jsp?mode=edit">Edit</a></li>--%>
                                    <li><a id="lblWsDetailView" href="WSList.jsp?mode=view">View</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Web-Service Organization</a>
                                <ul>
                                    <%--<li><a id="lblWsOrganizationEdit" href="WSConsumersList.jsp?mode=edit">Edit</a></li>--%>
                                    <li><a id="lblWsOrganizationView" href="WSConsumersList.jsp?mode=view">View</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                </ul>
                <%                        }
                    } // End fo userTypeId == 19 if condition
%>
                <script type="text/javascript">
                    //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
                    ddtreemenu.createTree("treemenu1", true)
                </script>

                <%-- <td >&nbsp;</td>--%>
        </tr>
    </table>
    <!--Dashboard Content Part End-->
</td>

