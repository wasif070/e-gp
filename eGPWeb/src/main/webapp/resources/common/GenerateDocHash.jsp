<%-- 
    Document   : GenerateDocHash
    Created on : Jun 30, 2011, 5:43:26 AM
    Author     : rikin.p
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.HashUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            
            List<SPCommonSearchDataMore> list =  commonSearchDataMoreService.geteGPData("getDocsforDocHash", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            String filepath = "";
            String hashval = "";

            for(SPCommonSearchDataMore l:list)
            {
                hashval = "";
                if(l.getFieldName4()==null || "".equals(l.getFieldName4()))
                {
                    filepath = "C:\\eGP\\Document\\VendorDocuments\\";
                    filepath += l.getFieldName2();
                    //filepath += "2582";
                    
                    File file = new File(filepath, l.getFieldName4());
                    //File file = new File(filepath, "STDPdf.pdf");

                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[(int)file.length()];
                    int offset = 0;
                    int numRead = 0;

                    while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                        offset += numRead;
                    }
                    fis.close();

                    //File Encyrption starts
                    FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                    //File Encyrption ends
                    hashval = HashUtil.getHash(new String(buf),"SHA-1");
                    commonService.updatedocHash(Integer.parseInt(l.getFieldName5()), hashval);
                }
            }
        %>
    </body>
</html>
