<%-- 
    Document   : ViewTenderDoc
    Created on : Dec 11, 2010, 8:17:03 PM
    Author     : Sanjay Prajapati
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCorrigendumDocs"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<div class="tableHead_1 t_space" id="divHead" style="background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;display:none;">Amendment / Corrigendum Detail</div>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<%
        int tenderId = 0;
        if(request.getParameter("tenderId")!=null)
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        
        int userId = 0;
        if(session.getAttribute("userId")!=null && !"".equalsIgnoreCase(session.getAttribute("userId").toString()))
               userId = Integer.parseInt(session.getAttribute("userId").toString());

        int userTypeId = 0;
        if(session.getAttribute("userTypeId")!=null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }

        boolean docFeePaid = true;
        

        String from = "";
        
        boolean isPDF = true;
        if((request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) || session.getAttribute("userId")==null){
            isPDF = false;
        }
        TenderCommonService  tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> spTenderCommon = tenderSrBean.getPublishCorriDetails(tenderId);

        int corrigendumCnt=0;
        if(spTenderCommon.size() > 0){
            corrigendumCnt = spTenderCommon.size() + 1;
%>
            <script>
                document.getElementById("divHead").style.display = "block";
            </script>
<%
        }
        for (int i=0,j=spTenderCommon.size();i<spTenderCommon.size();i++,j--) {
            corrigendumCnt--;
%>
                <table width="100%" cellspacing="10" class="tableView_1">
                    <tr>
                        <td colspan="2" width="100%" class="ff">
                            <div style="background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px; width: 25%;">&nbsp;Amendment / Corrigendum No. : &nbsp;<%=corrigendumCnt%>&nbsp;</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="22%" class="ff">Amendment / Corrigendum Text :</td>
                        <td width="78%" class="t-align-left"><%=spTenderCommon.get(i).getFieldName3()%></td>
                    </tr>
                </table>
<%
                        int detailCnt = 0, docCnt = 0;
                        List<SPTenderCommonData> spTenderCorriDetail = tenderSrBean.getPublishCorriValueDetails(spTenderCommon.get(i).getFieldName1());
                        if(spTenderCorriDetail.size()>0){
%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th class="t-align-center" width="20%">Field Name</th>
                        <th class="t-align-center" width="40%">Old Value</th>
                        <th class="t-align-center" width="40%">New Value</th>
                    </tr>
<%
                        
                        for (SPTenderCommonData sptcd1 : spTenderCorriDetail) {
                            detailCnt++;
%>
                    <tr>
                        <td class="t-align-center">
<%
                                if("securityLastDt".equals(sptcd1.getFieldName1())){
                                    out.print("Last date & time for Tender Security Submission");
                                }else if("preBidStartDt".equals(sptcd1.getFieldName1())){
                                    out.print("Meeting Start Date & Time");
                                }else if("openingDt".equals(sptcd1.getFieldName1())){
                                    out.print("Opening Date & Time");
                                }else if("preBidEndDt".equals(sptcd1.getFieldName1())){
                                    out.print("Meeting End Date & Time");
                                }else if("submissionDt".equals(sptcd1.getFieldName1())){
                                    out.print("Closing Date & Time");
                                }else if("securitySubOff".equals(sptcd1.getFieldName1())){
                                    out.print("Name & Address of the Office(s) for Tender security submission");
                                }else if("docEndDate".equals(sptcd1.getFieldName1())){
                                    out.print("Document last selling date & time");
                                }else if("pkgDocFees".equals(sptcd1.getFieldName1())){
                                    out.print("Tender Document  Price (TK)");
                                }else if("eligibilityCriteria".equals(sptcd1.getFieldName1())){
                                    out.print("Eligibility of Consultant");
                                }else if("tenderBrief".equals(sptcd1.getFieldName1())){
                                    out.print("Brief Description of Goods,Works or Service");
                                }else if(sptcd1.getFieldName1().contains("indStartDt")){
                                    out.print("Indicative Start Date(Ref No : "+tenderSrBean.phaseSecureDetail(true, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("indEndDt")){
                                    out.print("Indicative Completion Date(Ref No : "+tenderSrBean.phaseSecureDetail(true, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("docfess")){
                                    out.print("Document Fees(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("location")){
                                    out.print("Location(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("tenderSecurityAmt")){
                                    out.print("Tender Security (Amount in Nu.)(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("completionTime")){
                                    out.print("Completion Date(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("phasingRefNo")){
                                    out.print("Ref. No(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("phasingOfService")){
                                    out.print("Phasing of service(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("startTime")){
                                    out.print("Start Date(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                } else if(sptcd1.getFieldName1().contains("TDS/PDS")||sptcd1.getFieldName1().contains("PCC")){
                                    String[] corriStr=sptcd1.getFieldName1().split("_");
                                      for(int count=0;count<corriStr.length-2;count++)
                                          out.print(corriStr[count]+"--");

                                    }
%>
                        </td>
                        <td class="t-align-center"><%=sptcd1.getFieldName2()%></td>
                        <td class="t-align-center"><%=sptcd1.getFieldName3()%></td>
                    </tr>
<%
                        }
                        if (detailCnt == 0) {
%>
                    <tr>
                        <td colspan="3" class="t-align-center">No records found.</td>
                    </tr>
<%
                        }
%>
                </table>
<%
                        }
                        CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                        if(commonService.getDocAvlMethod(""+tenderId).toString().equalsIgnoreCase("Lot")){
                            List<CommonTenderDetails> spTenderLotDetail = tenderSrBean.getAPPDetails(tenderId,"Lot");
                            for (int l=0;l<spTenderLotDetail.size();l++) {
%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th class="t-align-center" width="34%">Lot No.</th>
                        <th class="t-align-center" width="34%">Lot Description</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%=spTenderLotDetail.get(l).getLotNo()%></td>
                        <td class="t-align-center"><%=spTenderLotDetail.get(l).getLotDesc()%></td>
                    </tr>
<%
                            docFeePaid = true;
                            if(userTypeId == 2){
                                docFeePaid =  tenderSrBean.chkTenderFeePaid("Document Fees",tenderId,spTenderLotDetail.get(l).getAppPkgLotId(),userId);
                            }
                            if(docFeePaid){
                                List<TblCorrigendumDocs> corriDoc = tenderSrBean.getCorrigendumDocs(tenderId, Integer.parseInt(spTenderCommon.get(i).getFieldName1()), spTenderLotDetail.get(l).getAppPkgLotId());
                                if(!corriDoc.isEmpty()){
%>
                    <tr>
                        <td colspan="2">
                            
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td class="t-align-left ff" colspan="2" >Amendment / Corrigendum / Addendum No. :</td>
                                    <td class="t-align-left" colspan="4" ><%=j%></td>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />
                                        (in KB)</th>
                                    <% if(isPDF){ %>
                                    <th class="t-align-center" id="thActionDownload" width="18%">Action</th>
                                    <% } %>
                                </tr>
    <%                                        
                                        for (int k=0;k<corriDoc.size();k++) {
                                            docCnt++;
    %>
                                <tr>
                                    <td class="t-align-center"><%=docCnt%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocumentName()%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocDescription()%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocSize()%></td>
                                    <% if(isPDF){ %>
                                    <td id="tdActionDownload" class="t-align-center err"><a href="<%=request.getContextPath()%>/ServletCorriDocUpload?docName=<%=corriDoc.get(k).getDocumentName()%>&docSize=<%=corriDoc.get(k).getDocSize()%>&tenderId=<%=tenderId%>&funName=download" title="Download">Download</a> </td>
                                    <% } %>
                                </tr>
<%
                                    }
                                    if (docCnt == 0) {
%>
                                <tr>
                                    <td colspan="5" class="t-align-center">No records found.</td>
                                </tr>
<%
                                    }
%>
                            </table>                            
                        </td>
                    </tr>
<%
                                }
                            }
%>
                </table>
<%
                            }
                        }else{
                            docFeePaid = true;
                            if(userTypeId == 2){
                                docFeePaid =  tenderSrBean.chkTenderFeePaid("Document Fees",tenderId,0,userId);
                            }
                            if(docFeePaid){
                             List<TblCorrigendumDocs> corriDoc = tenderSrBean.getCorrigendumDocs(tenderId, Integer.parseInt(spTenderCommon.get(i).getFieldName1()), 0);
                                if(!corriDoc.isEmpty()){
%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                    <td class="t-align-left ff" colspan="2" >Amendment / Corrigendum/ Addendum No. :</td>
                                    <td class="t-align-left" colspan="4" ><%=j%></td>
                                </tr>
                            
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th class="t-align-center" width="23%">File Name</th>
                                <th class="t-align-center" width="32%">File Description</th>
                                <th class="t-align-center" width="7%">File Size <br />
                                    (in KB)</th>
                                <% if(isPDF){ %>
                                <th class="t-align-center" id="thActionDownload" width="18%">Action</th>
                                <% } %>
                            </tr>
                            <%                                        
                                        for (int k=0;k<corriDoc.size();k++) {
                                            docCnt++;
                            %>
                                <tr>
                                    <td class="t-align-center"><%=docCnt%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocumentName()%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocDescription()%></td>
                                    <td class="t-align-center"><%=corriDoc.get(k).getDocSize()%></td>
                                    <% if(isPDF && userId>0){ %>
                                    <td id="tdActionDownload" class="t-align-center"><a href="<%=request.getContextPath()%>/ServletCorriDocUpload?docName=<%=corriDoc.get(k).getDocumentName()%>&docSize=<%=corriDoc.get(k).getDocSize()%>&tenderId=<%=tenderId%>&funName=download" title="Download">Download</a> </td>
                                    <% } %>
                            </tr>
<%
                                    }
                                    if (docCnt == 0) {
%>
                                <tr>
                                    <td colspan="5" class="t-align-center">No records found.</td>
                                </tr>
<%
                                    }                              
%>
                        </table>
<%
                                }
                            }
                        }
        }
%>