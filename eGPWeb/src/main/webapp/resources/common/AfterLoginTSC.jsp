<%--
    Document   : AfterLoginTSC
    Created on : Dec 5, 2010, 4:59:29 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalRoundMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"er
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Technical Sub Committee Formation Request</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>

            <%
            int uTypeID = 0;
            if(session.getAttribute("userTypeId") != null){
                uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
            }
            String tabRecCla = "";
            
            String tsctab="";
            String tsctenderid="";
            String comType="";
            String ispost="";
            String tabComType=request.getParameter("comType");
            TenderCommonService tenderEvalCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            EvaluationService evalService2 = (EvaluationService) AppContext.getSpringBean("EvaluationService");
            //out.println(pageContext.getAttribute("TSCtab"));
            if(pageContext.getAttribute("TSCtab") != null && !"null".equalsIgnoreCase((String)pageContext.getAttribute("TSCtab")) ){
                tsctab =   (String)pageContext.getAttribute("TSCtab");
            }

            if(request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid")) ){
                tsctenderid =   request.getParameter("tenderid");
            } else  if(request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId")) ){
                tsctenderid =   request.getParameter("tenderId");
            }

            if(request.getParameter("ispost") !=null && "null".equalsIgnoreCase(request.getParameter("ispost"))){
               ispost = request.getParameter("ispost");
            }

            if(pageContext.getAttribute("comType") != null && !"null".equalsIgnoreCase((String)pageContext.getAttribute("comType")) ){
                comType =   (String)pageContext.getAttribute("comType");
                pageContext.setAttribute("comType", comType);
            }
            

            boolean isCurTenPackageWis = false, isTenderConfigEntry=false, isMapped=false, isConfigCompleted=false, isTenderTSCReq=false;
            boolean showTSCTab = false, isPostQualReq=false;
            boolean isTSCMember=false;
            boolean isPriceComReportSave=false;
            String CurTenderType="";
            int evalCount = evalService2.getEvaluationNo(Integer.parseInt(tsctenderid));
            
            CommonService procNatureafterLoginTSC = (CommonService)AppContext.getSpringBean("CommonService");
            Object afterLoginTSCTabprocNature = procNatureafterLoginTSC.getProcNature(tsctenderid);
            Object afterLoginTSCTabprocMethod = procNatureafterLoginTSC.getProcMethod(tsctenderid);
            Object afterLoginTSCTabTenderType = procNatureafterLoginTSC.getEventType(tsctenderid);
            for(TblEvalRoundMaster tblEvalRoundMasterTSC : procNatureafterLoginTSC.getDataTblEvalRM(Integer.parseInt(tsctenderid),evalCount)){
                isPriceComReportSave = true;
            }

            if(pageContext.getAttribute("isTenPackageWise")!=null){
               isCurTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
            }
            if(isCurTenPackageWis){
                CurTenderType="Packagewise";
            } else {
                CurTenderType="Lotwise";
            }

            List<SPCommonSearchData> isTERequiredLst = cmnSearchService.searchData("Eval_isTscReq_Info", tsctenderid, CurTenderType,
                                                                                   null, null, null, null, null, null, null);

            if(!isTERequiredLst.isEmpty()){

                if ("yes".equalsIgnoreCase(isTERequiredLst.get(0).getFieldName3())){
                    isTenderConfigEntry=true;

                    if ("yes".equalsIgnoreCase(isTERequiredLst.get(0).getFieldName1())){
                        isTenderTSCReq=true;

                        if ("mapped".equalsIgnoreCase(isTERequiredLst.get(0).getFieldName2())){
                            isMapped=true;
                        }
                    }

                    if ("yes".equalsIgnoreCase(isTERequiredLst.get(0).getFieldName4())){
                        isPostQualReq=true;
                    }
                }
            }

            if(isTenderConfigEntry){
                isConfigCompleted=true;
                if(isTenderTSCReq){
                    showTSCTab=true;
                }
            }

             List<SPTenderCommonData> lstCheckIsTSCMember =
                     tenderEvalCS.returndata("CheckIsTSCMember", tsctenderid, session.getAttribute("userId").toString());

             if (!lstCheckIsTSCMember.isEmpty()) {
                 if ("Yes".equalsIgnoreCase(lstCheckIsTSCMember.get(0).getFieldName1())) {
                     isTSCMember = true;
                 }
             }

             if(isTSCMember){
                tabRecCla = "Recommendation";
            }else{
                tabRecCla = "Clarification";
            }

        %>
        <%--
        <input type="hidden" value="<%=tabComType%>" id="tabComType"/>
        
<!--        START : SCRIPT TO HIDE OFFICER TAB PANEL-->
<!--        <script type="text/javascript">
            if($('#tabComType').val()=="PEC" || $('#tabComType').val()=="TEC" || $('#tabComType').val()=="TSC"){
                $('#offTabPanel').hide();
            }
        </script>-->
<!--        END : SCRIPT TO HIDE OFFICER TAB PANEL-->

                <ul class="tabPanel_1">
                    <%if(isTSCMember){%>
                        <li class=""><a href="<%=request.getContextPath()%>/officer/EvalTSC.jsp?tenderId=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("1".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Declaration</a></li>
                    <%} else {%>
                        <li class=""><a href="<%=request.getContextPath()%>/officer/EvalComm.jsp?tenderid=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("1".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Declaration</a></li>
                    <%}%>
                    

                        <%if (showTSCTab){%>
<!--                        <li class="">
                            <a href="EvalCommTSC.jsp?tenderid=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("0".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>TSC </a>
                        </li>-->
                        <%}%>


                    <%
                    List<SPCommonSearchData> memRoleList =
                                cmnSearchService.searchData("EvalMemberRole", tsctenderid,session.getAttribute("userId").toString(),
                                null, null, null, null, null, null, null);

                        String memRole = "";
                        if (!memRoleList.isEmpty()){
                            memRole = memRoleList.get(0).getFieldName2();
                        }

                       // if ("cp".equalsIgnoreCase(memRole)) {
                        if (!isTSCMember) {
                            List<SPTenderCommonData> evalStatusCount =
                                    tenderEvalCS.returndata("GetEvalReportLinkStatus",tsctenderid,null);

                            if (Integer.parseInt(evalStatusCount.get(0).getFieldName1()) != 0) {
                             List<SPTenderCommonData> evalMemDeclarationStatus =
                                            tenderEvalCS.returndata("getMemDeclarationStatus", tsctenderid, session.getAttribute("userId").toString());
                     %>
                     <% if("approved".equalsIgnoreCase(evalMemDeclarationStatus.get(0).getFieldName1())) {%>
                     <%if ("cp".equalsIgnoreCase(memRole)) {%>
                     <li class=""><a href="<%=request.getContextPath()%>/officer/Evalclarify.jsp?tenderId=<%=tsctenderid%>&st=rp&comType=<%=tabComType%>" <%if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Eval. Report </a></li>
                     <%} else {%>
                     <li class=""><a href="<%=request.getContextPath()%>/officer/MemberTERReport.jsp?tenderId=<%=tsctenderid%>&st=rp&comType=<%=tabComType%>" <%if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Eval. Report </a></li>
                     <%}%>
                     <%} else {%>

                        <script>
                            $(document).ready(function(){
                                $('#tbEvalRpt').click(function(){
                                    jAlert('Please complete declaration first.');
                                    return false;
                                });
                            });
                        </script>
                        <li class=""><a id="tbEvalRpt" href="#"><%=tabRecCla%></a></li>

                        <%}%>
                    
                    <%
                            }
                        }  // END IF FOR evalStatusCount
                           // } // END IF FOR memRole
                    %>

                    <%
                    if (isConfigCompleted) {
                                    List<SPTenderCommonData> evalMemDeclarationStatus =
                                            tenderEvalCS.returndata("getMemDeclarationStatus", tsctenderid, session.getAttribute("userId").toString());

                                    if(!evalMemDeclarationStatus.isEmpty()){%>
                                        <% if("approved".equalsIgnoreCase(evalMemDeclarationStatus.get(0).getFieldName1())) {%>
                                            <li class="">
                                                <% if("1".equalsIgnoreCase(evalMemDeclarationStatus.get(0).getFieldName2())) {%>
                                                <a id="tbClari" href="<%=request.getContextPath()%>/officer/Evalclarify.jsp?tenderId=<%=tsctenderid%>&st=cl&comType=<%=tabComType%>" <%if("4".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>><%=tabRecCla%></a>
                                                <%} else {%>
                                                <a id="tbClari" href="<%=request.getContextPath()%>/officer/ViewClarify.jsp?tenderId=<%=tsctenderid%>&st=cl&comType=<%=tabComType%>" <%if("4".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>><%=tabRecCla%></a>
                                                <%}%>
                                            </li>
                                        <%} else {%>

                                        <script>
                                            $(document).ready(function(){
                                                $('#tbClari').click(function(){
                                                    jAlert('Please complete declaration first.');
                                                    return false;
                                                });
                                            });
                                        </script>
                                        <li class=""><a id="tbClari" href="#"><%=tabRecCla%></a></li>

                                        <%}%>
                                    <%} else {
                                       
                                    %>
                                    <%if (isTSCMember) {
                                        boolean isTScDecDone = false;
                                        List<SPTenderCommonData> lstTSCMemDecStatus =
                                            tenderEvalCS.returndata("getTenderTSCMemStatus", tsctenderid, session.getAttribute("userId").toString());
                                        if(!lstTSCMemDecStatus.isEmpty()) {
                                            if("yes".equalsIgnoreCase(lstTSCMemDecStatus.get(0).getFieldName1())){
                                                isTScDecDone = true;
                                            }
                                       }

                                        lstTSCMemDecStatus = null;
    %>
                    <%if (isTScDecDone) {%>
                                    <li class="">
                                        <a id="tbClari" href="<%=request.getContextPath()%>/officer/Evalclarify.jsp?tenderId=<%=tsctenderid%>&st=cl&comType=<%=tabComType%>" <%if("4".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>><%=tabRecCla%></a>
                                        </li>
                                        <%} else {%>
                                            <li class=""><a id="tbClari" href="#"><%=tabRecCla%></a></li>
                                              <script>
                                        $(document).ready(function(){
                                            $('#tbClari').click(function(){
                                                jAlert('Please complete declaration first.');
                                                return false;
                                            });
                                        });
                                    </script>
                                        <%}%>
                                    <%} else {%>
                                          <script>
                                        $(document).ready(function(){
                                            $('#tbClari').click(function(){
                                                jAlert('Please complete declaration first.');
                                                return false;
                                            });
                                        });
                                    </script>
                                        <li class=""><a id="tbClari" href="#"><%=tabRecCla%></a></li>
                                    <%}%>
                                    <%}%>

                    <%
                           } else {
                    %>
                    <script>
                         $(document).ready(function(){
                            $('#tbClari').click(function(){
                                jAlert('Please configure evaluation forms first.');
                            });
                        });
                    </script>
                    <li class=""><a id="tbClari" href="#"><%=tabRecCla%></a></li>
                    <%
                                } // END IF FOR TENDER FLAG
                    %>

                    <%
                        boolean isCaseTSC = false;
                        if(request.getAttribute("isCase1")!=null){
                            isCaseTSC = (Boolean)request.getAttribute("isCase1");
                        }
                        if(!isCaseTSC && !isTSCMember && "cp".equalsIgnoreCase(memRole) && isPriceComReportSave){
                    %>
                            <%if (!"Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString()) && (!(afterLoginTSCTabprocMethod.toString().equals("DPM"))) && isPostQualReq && (!(afterLoginTSCTabTenderType.toString().equalsIgnoreCase("PQ") || afterLoginTSCTabTenderType.toString().equalsIgnoreCase("REOI") || afterLoginTSCTabTenderType.toString().equalsIgnoreCase("1 stage-TSTM")))){%>
                                <li class=""><a href="<%=request.getContextPath()%>/officer/InitiatePQ.jsp?tenderId=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("6".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Post Qualification </a></li>
                            <%}%>
                            <%if("Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString()) && (!(afterLoginTSCTabTenderType.toString().equalsIgnoreCase("PQ") || afterLoginTSCTabTenderType.toString().equalsIgnoreCase("REOI") || afterLoginTSCTabTenderType.toString().equalsIgnoreCase("1 stage-TSTM")))){%>
                                <li class=""><a href="<%=request.getContextPath()%>/officer/NegotiationProcess.jsp?tenderId=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("7".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Negotiation</a></li>
                            <%}%>
                            <%if(!"Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString()) && (afterLoginTSCTabprocMethod.toString().equalsIgnoreCase("DPM"))){%>
                                <li class=""><a href="<%=request.getContextPath()%>/officer/NegotiationProcess.jsp?tenderId=<%=tsctenderid%>&comType=<%=tabComType%>" <%if("7".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Negotiation</a></li>
                            <%}%>
                        <%}%>
                        <%
                       
                        if(evalCount>0){
                        %>
                          <li class=""><a href="<%=request.getContextPath()%>/officer/ReEvaluation.jsp?tenderId=<%=tsctenderid%>" <%if("5".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Re-evaluation</a></li>
                        <%}%>
                </ul>
        --%>
<!--        <ul class="tabPanel_1"><li><a href="<%//=request.getContextPath()%>/officer/Evalclarify.jsp?tenderId=<%//=tsctenderid%>&st=rp&comType=<%//=tabComType%>" <%//if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%//}%>>Eval. Report </a></li>
            
        </ul>-->
    </body>
</html>
