<%-- 
    Document   : ViewAppDetails
    Created on : Apr 8, 2018, 4:03:56 PM
    Author     : Aprojit
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                  
            List<CommonAppData> ImplementingAgencies = new ArrayList<CommonAppData>();
            List<CommonAppData> ImplementingAgencyDetails = new ArrayList<CommonAppData>();
            //List<CommonAppData> ImplementingAgenciesPackages= new ArrayList<CommonAppData>();
            Object userId = "0";
            Object objUsrTypeId = "";
            Object objUName = "";
            boolean isLoggedIn = false;
            if(session.getAttribute("userId") != null){
                userId = session.getAttribute("userId");
            objUsrTypeId = session.getAttribute("userTypeId");
            objUName = session.getAttribute("userName");

            if (objUName != null) {
                isLoggedIn = true;
            }
            }
            StringBuilder userType = new StringBuilder();
            if (request.getParameter("hdnUserType") != null) {
                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                } else {
                    userType.append("org");
                }
            } else {
                userType.append("org");
            }
            String appPkgStatus = "";
            
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="appId";
            int auditId=Integer.parseInt(request.getParameter("appId"));
            String auditAction="View Package";
            String moduleName=EgpModule.APP.getName();
            String remarks="User id:"+session.getAttribute("userId")+" has view Package id:"+request.getParameter("pkgId");
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks); 


        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>View APP Package Details</title>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <%if (request.getParameter("print") == null) {%>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%}%>
<!--
        <script type="text/javascript" src="< %=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="< %=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="< %=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="< %=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="< %=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

    </head>
    <%if (request.getParameter("print") == null) {%>
    <script type="text/javascript">
        function printPage()
        {
            $("#viewForm").submit();
        }
           
    </script>
    <%}%>
    <body>
        <jsp:useBean id="appViewPkgDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
        <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
<%
                    
                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
                    
                    if (!(isPDF.equalsIgnoreCase("true"))) {
                        appServlet.setLogUserId(userId.toString());
                    }
                    String strAppId = request.getParameter("appId");
                    String strPkgId = request.getParameter("pkgId");
                    String strPkgType = request.getParameter("pkgType");
                    String devpartner ="";
                    int appId = 0;
                    int pkgId = 0;
                    if (strAppId != null && !strAppId.equals("")) {
                        appId = Integer.parseInt(strAppId);
                    }
                    if (strPkgId != null && !strPkgId.equals("")) {
                        pkgId = Integer.parseInt(strPkgId);
                    }
                    //out.write("AppId: " + appId + "Package Id:" + pkgId);

                     if (appId != 0 && pkgId != 0) {
                            appPkgStatus = appViewPkgDtBean.getPkgStatus(appId, pkgId);
                      }

        if (!isLoggedIn && !"Approved".equalsIgnoreCase(appPkgStatus) && !"BeingRevised".equalsIgnoreCase(appPkgStatus)) {
                         response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
        %>
        <script  type="text/javascript">
            function findDevPart(){
                
                if($('#hdnPrjId').val()!=0){
                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId:$('#hdnPrjId').val(),funName:'getDP'},  function(j){
                        $("span.#devPart").html(j);
                        //$("tr#trDevPrt").css("visibility","visible");
                    });
                }else{
                    $("span.#devPart").html("Not Applicable");
                    // $("tr#trDevPrt").css("visibility","collapse");
                }
                
            }

            function addToWatchList()
            {
                $.post("<%=request.getContextPath()%>/WatchListServlet", {appId: <%=appId%>,pkgId: <%=pkgId%>,userId: <%=userId%>,funName: 'Add'},  function(j){
                    if(j.toString() != "0")
                    {
                        jAlert("Added to your WatchList","Success");
                        $("#addCell_1").hide();
                        $("#addCell").hide();
                        $("#watchId").val(parseInt(j));
                        $("#removeCell_1").show();
                        $("#removeCell").show();
                    }
                    else
                    {
                        jAlert("Can not be added to WatchList","Failure");
                    }

                });
            }

            function removeFromWatchList()
            {
                $.post("<%=request.getContextPath()%>/WatchListServlet", {watchId: $("#watchId").val(),funName: 'Remove'},  function(j){
                    if(j.toString() == "true")
                    {
                        jAlert("Removed from your WatchList","Success");
                        $("#removeCell_1").hide();
                        $("#removeCell").hide();
                        $("#addCell_1").show();
                        $("#addCell").show();
                    }
                    else
                    {
                        jAlert("Can not be removed from WatchList","Failure");
                    }
                });
            }
        </script>
        <script  type="text/javascript">
            $(function() {
                $('#watchList').click(function() {
                    addToWatchList();
                });
            });
        </script>
        <script  type="text/javascript">
            $(function() {
                $('#watchListRem').click(function() {
                    removeFromWatchList();
                });
            });
        </script>
        <%if (request.getParameter("print") == null) {%>
        <div class="mainDiv">

            <% if (!(isPDF.equalsIgnoreCase("true"))) {
                    if (isLoggedIn) {
            %>
            <div class="dashboard_div">
                <%//@include  file="AfterLoginTop.jsp"%> <%} else {%>
              
                    <%}
                        } //end of isPDF
                        
                    %>
                    <!--Middle Content Table Start-->

                    <!--table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top"-->
                            <div class="contentArea_1">
                                <%}%>
                                <%
                                            boolean homeLink = false;
                                            if (request.getParameter("from") != null && "home".equalsIgnoreCase(request.getParameter("from"))) {
                                                homeLink = true;
                                            }
                                %>
                                <!--                                    <div class="pageHead_1">View APP: Package Details</div>-->
                                <div  id="print_area">
                                    <div class="pageHead_1">View APP Package Details : <% if("RevisePackage".equalsIgnoreCase(strPkgType)){ %> (old) <% } %>
                                        <%
                                                        
                                                        int totalNoofDays =0;
                                                        AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                                                        if (!(isPDF.equalsIgnoreCase("true"))) {
                                                            appAdvSearchService.setLogUserId(userId.toString());
                                                        }
                                                        
                                                        appViewPkgDtBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                        if (!((appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = " + appId + " and packageId = " + pkgId)) > 0)) {
                                                            strPkgType = "AppPackage";
                                                        }
                                                        else if("RevisePackage".equalsIgnoreCase(strPkgType)){
                                                            strPkgType="RevisePackage";
                                                        }
                                                        else {
                                                            strPkgType = "Package";
                                                        }

                        if (appId != 0 && pkgId != 0) {
                                                            if (isLoggedIn) {
                                                                int intUserId = Integer.parseInt(userId.toString());
                                                                appViewPkgDtBean.populateInfo(appId, pkgId, strPkgType, intUserId);
                                                            } else {
                                                                appViewPkgDtBean.populateInfo(appId, pkgId, strPkgType, 0);
                                                            }
                                                            appPkgStatus = appViewPkgDtBean.getPkgStatus(appId, pkgId);

                                                            String nature = appViewPkgDtBean.getProcurementnature();
                                                            String isPQReqd = appViewPkgDtBean.getIsPQRequired();
                                                            String methodName = appViewPkgDtBean.getProcMethodName((short) appAdvSearchService.getProcMethodId(appId, pkgId));
                                                            String methodFullName = appViewPkgDtBean.getProcMethodFullName((short) appAdvSearchService.getProcMethodId(appId, pkgId));
                                                            Integer aaEmpId = appViewPkgDtBean.getAaEmpId();
                                                            if("RevisePackage".equalsIgnoreCase(strPkgType)){
                                                                short procMethodId=(short)appAdvSearchService.getTempProcMethodId(appId, pkgId);
                                                                   methodName = appViewPkgDtBean.getProcMethodName(procMethodId );
                                                                  methodFullName = appViewPkgDtBean.getProcMethodFullName(procMethodId);
                                                               }

                                                            String REOreq = "REOI";
                                                            if (appViewPkgDtBean.getReoiRfaRequired() != null) {
                                                                REOreq = appViewPkgDtBean.getReoiRfaRequired();
                                                            }

                                                            isPQReqd = appAdvSearchService.getIsPQReq(appId, pkgId);
                                                            String type = "";
                                                            if ("Yes".equalsIgnoreCase(isPQReqd)) {
                                                                type = "Tender";
                                                            }
                                                            if (methodName.equalsIgnoreCase("RFQ") || methodName.equalsIgnoreCase("RFQL") || methodName.equalsIgnoreCase("RFQU")) {
                                                                type = "RFQ";
                                                            } else {
                                                                type = "Tender";
                                                            }

                                                            if (request.getParameter("print") == null) {
                                                                if (isLoggedIn && objUsrTypeId.toString().equals("2")) {
                                                                    if (appViewPkgDtBean.isInWatchList()) {
                                            %>
                                            <span class="noprint">
                                                <span id="addCell_1" style="display: none;">
                                                    <a id="watchList_1" onclick="addToWatchList()" class="action-button-add">Add to WatchList</a>
                                                </span>
                                                  <span id="removeCell_1">
                                                    <a id="watchListRem_1" onclick="removeFromWatchList()" class="action-button-delete">Remove from WatchList</a>
                                                </span>
                                            </span>
<%                                  } else {
%>
                                            <span class="noprint">
                                                <span id="addCell_1" >
                                                    <a id="watchList_1" onclick="addToWatchList()" class="action-button-add">Add to WatchList</a>
                                                </span>
                                                  <span id="removeCell_1" style="display: none;">
                                                    <a id="watchListRem_1" onclick="removeFromWatchList()" class="action-button-delete">Remove from WatchList</a>
                                                </span>
                                            </span>
                                            <%                                                    }
                                                                                            }
                                                                                        }
                                            %>
                                         <%  if (!(isPDF.equalsIgnoreCase("true"))) {%>
                                         <span style="float:right;" class="noprint">
                                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    <%if (!homeLink) {%>
                                   
                                    <a href="<%=request.getContextPath()%>/officer/APPDashboard.jsp?appID=<%=appId%>" class="action-button-goback">Go Back To Dashboard</a>
                                    <%}
                                   %>
                                         </span>
                                         <span style="float:right;" class="noprint">
                                        <%
                                              String reqURL = request.getRequestURL().toString();
                                              String reqQuery = "appId=" + strAppId + "&pkgId=" + strPkgId + "&pkgType=Package";
                                              String folderName = pdfConstant.APPVIEW;
                                              String id = strAppId + "_" + strPkgId;
                                        %>
                                        <% if("Approved".equalsIgnoreCase(appPkgStatus)){ %>
                                        <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=id%>" >Save As PDF </a>&nbsp;
                                        <% } %>
                                         </span>
                                    <!--/td-->
                                     <%   }    %>
                                    </div>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
<%
                                        AppAdvSearchService appAdvSearchService1 = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                                        if (!(isPDF.equalsIgnoreCase("true"))) {
                                            appAdvSearchService1.setLogUserId(userId.toString());
                                        }
                                        if(!((appAdvSearchService1.countForQuery("TblAppPqTenderDates", "appid = "+appId+" and packageId = "+pkgId))>0)){
                                            strPkgType = "AppPackage";
                                        }else{
                                            strPkgType = "Package";
                                        }
                                        if (appId != 0 && pkgId != 0) {
                                          //  appViewPkgDtBean.populateInfo(appId, pkgId, strPkgType, 0);
                                            if (appViewPkgDtBean.isDataExists()) {

                                                List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                                CommonAppData appData=new CommonAppData();
                                                
                                                int prjId = 0;
                                                List<CommonAppData> devPartnerBean = appServlet.getAPPDetails("App", String.valueOf(appId),"");
                                                ImplementingAgencies = appServlet.getAPPDetails("ImplementingAgency", String.valueOf(appId),"");
                                                //ImplementingAgenciesPackages = appServlet.getAPPDetails("ImplementingAgencyPackage", String.valueOf(appId),"");
                                                if(!devPartnerBean.isEmpty()){
                                                    prjId = Integer.parseInt(devPartnerBean.get(0).getFieldName2().toString());
                                                }
                                                
                                                devpartner = appServlet.getDPDetails("ProjectPartner",String.valueOf(prjId));
                                                appListDtBean = appServlet.getAPPDetails("GetAppMinistry", String.valueOf(appId), "");
                                                                                                if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                    appData=appListDtBean.get(0);
                                                                                                }
                                                TenderCommonService commonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                List<SPTenderCommonData> details = commonService.returndata("GetAppDetailById", appData.getFieldName6(), "0");
%>
                                    <tr>
                                        <td style="width: 180px" class="ff" align="left">Hierarchy Node	: </td>
                                        <td style="width: 287px" align="left" colspan="3"><%=details.get(0).getFieldName1() %></td>
                                    </tr>
<!--                                    <tr>
                                        <td class="ff" align="left">Department :</td>
                                        <td align="left" colspan="3"><%=details.get(0).getFieldName2() %></td>
                                    </tr>-->
<!--                                    <tr>
                                        <td class="ff" align="left">Division :</td>
                                        <td align="left"><%=details.get(0).getFieldName3() %></td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff" align="left">PA Office and Code :</td>
                                        <td align="left"><%=details.get(0).getFieldName4() %></td>
                                       
                                    </tr>
                                    <tr>
                                        <td class="ff" align="left" style="width: 180px" >Budget Type :</td>
                                        <td align="left" style="width: 287px" >${appViewPkgDtBean.budgetType}</td>
                                        <td class="ff" align="left" style="width: 180px" >Project Name :</td>
                                        <td align="left" style="width: 287px" >${appViewPkgDtBean.projectName}</td>
                                    </tr>
                                </table>
                                <%
                                    if(ImplementingAgencies.size() > 0)
                                    {
                                        ImplementingAgencyDetails = appServlet.getAPPDetails("ImplementingAgencyDetails", String.valueOf(appId),"");
                                    %>
                                        <div>&nbsp;</div>
                                        <div>
                                            <p style="text-align: center; font-size: 17px; color: green; "> This APP is Deposited to <b>'<%=ImplementingAgencyDetails.get(0).getFieldName3()%>'</b>. The Address of the Entrusted Agency is: <b>'<%=ImplementingAgencyDetails.get(0).getFieldName4()%>'</b>. </p>
                                        </div>
                                    <%
                                    }
                                %>
                                <div>&nbsp;</div>
                                <% }
                                        }
                                        
                                        
                                        
                                %>
                                    <form method="POST" action="<%=request.getContextPath()%>/PDFServlet" id="viewForm">
                                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1" width="100%">
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;">Key Fields Information:</span>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td width="28%" class="ff">APP ID :</td>
                                                <td width="72%" valign="top">${appViewPkgDtBean.appId}</td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Letter Ref. No. :</td>
                                                <td valign="top">${appViewPkgDtBean.appCode}
                                                    <input type="hidden" name="reportName" value="Package Detail"/>
                                                    <input type="hidden" name="reportPage" value="PackageDetailReport.jsp?<%=request.getQueryString()%>"/></td>
                                                    <% if (isLoggedIn) { %>
                                                <input type="hidden" id="watchId" value="<%=appViewPkgDtBean.getWatchId()%>" />
                                                <%  } %>
                                            </tr>
                                            <tr>
                                                <td class="ff">Financial Year :</td>
                                                <td valign="top">${appViewPkgDtBean.financialYear}</td>
                                            </tr>
                                           <tr>
                                                <td class="ff">Budget Type :</td>
                                                <td valign="top">${appViewPkgDtBean.budgetType}</td>
                                            </tr>
                                           
                                            <%if ((!"".equalsIgnoreCase(appViewPkgDtBean.getProjectName())) && (!"Not Applicable".equalsIgnoreCase(appViewPkgDtBean.getProjectName()))) {%>
                                            <tr>
                                                <td class="ff">Project Name :</td>
                                                <td valign="top">${appViewPkgDtBean.projectName}</td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <!--Changed Entity to Agency by Proshanto Kumar Saha-->
                                                <td class="ff">Procuring Agency :</td>
                                                <td valign="top">${appViewPkgDtBean.PE}</td>
                                            </tr>
<!--                                            <tr>
                                                <td class="ff">Dzongkhag / District :</td>
                                                <td valign="top">${appViewPkgDtBean.district}</td>
                                            </tr>-->
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;">Package Details:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Procurement Category</td>
                                                <td valign="top">${appViewPkgDtBean.procurementnature}</td>
                                            </tr>
                                            <tr style="display: none">
                                                <td class="ff">Type of Emergency</td>
                                                <td valign="top">${appViewPkgDtBean.pkgUrgency}</td>
                                            </tr>
                                            <%if ("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())) {%>
                                            <tr>
                                                <td class="ff">Service Type</td>
                                                <td valign="top">
                                                    ${appViewPkgDtBean.servicesType}
                                                </td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td class="ff">Package No</td>
                                                <td valign="top">${appViewPkgDtBean.packageNo}</td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Package Description</td>
                                                <td valign="top">${appViewPkgDtBean.packageDesc}</td>
                                            </tr>
                            <%--aprojit-Start --%>
                                            <tr>
                                                <td class="ff">Work Type</td>
                                                <td valign="top">${appViewPkgDtBean.workCategory}</td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Work Category</td>
                                                <td valign="top">${appViewPkgDtBean.bidderCategory}</td>
                                            </tr>
                                            <tr style="display: none">
                                                <td class="ff">Deposit work / Planned work</td>
                                                <td valign="top">${appViewPkgDtBean.depoplanWork}</td>
                                            </tr>
                                            <%if ("Deposit Work".equalsIgnoreCase(appViewPkgDtBean.getDepoplanWork())) {%>
                                            <tr style="display: none">
                                                <td class="ff">Entrusting Agency</td>
                                                <td valign="top">
                                                    ${appViewPkgDtBean.entrustingAgency}
                                                </td>
                                            </tr>
                                            <%}%>
                                            <%if ("Planned Work".equalsIgnoreCase(appViewPkgDtBean.getDepoplanWork())) {%>
                                            <tr style="display: none">
                                                <td class="ff">Time Frame</td>
                                                <td valign="top">
                                                    ${appViewPkgDtBean.timeFrame}
                                                </td>
                                            </tr>
                                            <%}%>
                           <%--aprojit-End --%>
                                           <%--<tr>
                                                <td class="ff">Allocated Budget (In BTN)</td>
                                                <td valign="top"><% if(appViewPkgDtBean.getAllocateBudget()!=null) { out.print(appViewPkgDtBean.getAllocateBudget().setScale(2,0)); }else{ out.print("-"); } %></td>
                                            </tr>--%>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;">Lot Details:</span></th>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <%
                                                                                                if (appViewPkgDtBean.getPkgLotDeail().size() > 0) {
                                                    %>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                        <tr>
                                                            <th class="t-align-center" style="width:15%; ">
                                                                <% if("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())){ %>
                                                                Package No.
                                                                <% }else{ %>
                                                                Lot No.
                                                                <% } %>
                                                            </th>
                                                            <th class="t-align-center" style="width:40% ">
                                                                <% if("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())){ %>
                                                                Package Description
                                                                <% }else{ %>
                                                                Lot Description
                                                                <% } %>
                                                            </th>
                                                            <th class="t-align-center" style="width:15% ">Qty</th>
                                                            <th class="t-align-center" style="width:10% ">Unit</th>
                                                            <!--Here changed BTN to Nu. by Proshanto Kumar Saha-->
                                                            <th class="t-align-center" style="width:20% ">Estimated Cost (In Nu.)</th>
                                                        </tr>
                                                         <% for(int i=0; i<appViewPkgDtBean.getPkgLotDeail().size();i++){ %>
                                            <tr class="<%if (i % 2 != 0) {
                                                                     out.print("bgColor-Green");
                                                                 } else {
                                                                     out.print("bgColor-white");
                                                                 }%>">
                                                                <td class="t-align-center"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotNo()%></td>
                                                                <td class="t-align-left"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotDesc()%></td>
                                                                <td class="t-align-center">
                                                                    <%
                                                                        if(appViewPkgDtBean.getPkgLotDeail().get(i).getQuantity().compareTo(new BigDecimal("0"))==0)
                                                                        {
                                                                            out.print("-");
                                                                        }
                                                                        else
                                                                        {
                                                                            out.print(appViewPkgDtBean.getPkgLotDeail().get(i).getQuantity()); 
                                                                        }
                                                                    %>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <% 
                                                                        if (!"".equals(appViewPkgDtBean.getPkgLotDeail().get(i).getUnit())) 
                                                                        {
                                                                            out.print(appViewPkgDtBean.getPkgLotDeail().get(i).getUnit());
                                                                        } 
                                                                        else 
                                                                        {
                                                                            out.print("-");
                                                                        }
                                                                    %>
                                                                </td>
                                                                <td class="t-align-center"><%=appViewPkgDtBean.getPkgLotDeail().get(i).getLotEstCost().setScale(2,0)%></td>
                                                            </tr>
                                                         <% } %>
                                                    </table>
                                                    <%                                                    } else {
                                                    %>
                                                    <label class="ff">No Data Found</label>
                                                    <%}%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <!--BTN changed to Nu. by Proshanto Kumar Saha-->
                                                <td class="ff">Package Official Cost Estimate (In Nu.)</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getPkgEstCost() != null) {
                                                                                                                out.print(appViewPkgDtBean.getPkgEstCost().setScale(2, 0));
                                                                                                            } else {
                                                                                                                out.print("-");
                                                                                                            }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Category</td>
                                                <td class="t-align-left">
                                                    ${appViewPkgDtBean.cpvCode}
                                                </td>
                                            </tr>
                                            <%if(true){%>
                                            <tr>
                                                <td class="ff">Approving Authority</td>
                                                <td class="t-align-left">
                                        <% if (appViewPkgDtBean.getAaEmpId() != null) {
                                        //Here Check condition to show HOPA instead of HOPE and PA instead of PE by Proshanto Kumar Saha
                                                       if(appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()).equalsIgnoreCase("Hope")){out.print("HOPA");}
                                    else if(appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()).equalsIgnoreCase("PE")){
                                              out.print("PA");
                                                  }
                                       else{out.print(appViewPkgDtBean.getApprovingAuthority(appViewPkgDtBean.getAaEmpId()));}
                                                        }%>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <%if ("Works".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())) {%>
                                            <tr>
                                                <td class="ff">PQ Requires</td>
                                                <td class="t-align-left">${appViewPkgDtBean.isPQRequired}</td>
                                            </tr>
                                            <%}%>
                                            <%if ("Services".equalsIgnoreCase(appViewPkgDtBean.getProcurementnature())) {%>
                                            <tr>
                                                <td class="ff">REOI / RFP Requires</td>
                                                <td class="t-align-left">${appViewPkgDtBean.reoiRfaRequired}</td>
                                            </tr>
                                            <%}%>
                                            <tr>
                                                <td class="ff">Procurement Method</td>
                                                <td class="t-align-left"><%
                                                    if(methodFullName.equalsIgnoreCase("Direct Procurement"))
                                                    {
                                                        out.print("Direct Contracting Method");
                                                    }
                                                    if(methodFullName.equalsIgnoreCase("Request For Quotation"))
                                                    {
                                                        out.print("Limited Enquiry Method ");
                                                    }
                                                    else
                                                    {
                                                        out.print(methodFullName);
                                                    }
                                                
                                                
                                                %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Procurement Type</td>
                                                <!--Check condition to NCB instead of NCT and ICB instead of ICT by Proshanto Kumar Saha-->
                                                <td class="t-align-left"><%if((appViewPkgDtBean.getProcurementType()).equalsIgnoreCase("NCT")){out.print("NCB");}
                                    else if((appViewPkgDtBean.getProcurementType()).equalsIgnoreCase("ICT")){
                                              out.print("ICB");
                                                  }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Source of Fund</td>
                                                <td class="t-align-left">${appViewPkgDtBean.sourceOfFund}</td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Development Partners</td>
                                    <td class="t-align-left"><% if (!"".equals(devpartner)) {
                                                                                                                out.print(devpartner);
                                                                                                            } else {
                                                                                                                out.print("-");
                                                                                                            }%></td>
                                            </tr>
                                            <%if (!"Not Applicable".equalsIgnoreCase(appViewPkgDtBean.getProjectName()) && ("revenue".equalsIgnoreCase(appViewPkgDtBean.getBudgetType()) || "own".equalsIgnoreCase(appViewPkgDtBean.getBudgetType())) ) {%>
                                            <tr>
                                                <td class="ff">Development Partners</td>
                                                <td class="t-align-left">
                                                    <span id="devPart"></span>
                                                    <input type="hidden" id="hdnPrjId" value="${appViewPkgDtBean.projectId}"/>
                                                    <script type="text/javascript">
                                                        //findDevPart();
                                                    </script>
                                                    <%=appServlet.getDPDetails("ProjectPartner",String.valueOf(appViewPkgDtBean.getProjectId()))%>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <%
                                                SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                                                SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
                                                
                                                if (appViewPkgDtBean.isDataExists()) {
                                            %>
                                            <%if ("Yes".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) {%>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;">PQ Tender Dates:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of Invitation on e-GP website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                                    }%> </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Applications  Submission</td>
                                    <td class="t-align-left">${appViewPkgDtBean.subDt} <% if (appViewPkgDtBean.getSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date Of Submission Of Evaluation Report With Recommended List</td>
                                    <td class="t-align-left">${appViewPkgDtBean.evalRptDt} <% if (appViewPkgDtBean.getEvalRptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getEvalRptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval Of List</td>
                                                <td class="t-align-left">${appViewPkgDtBean.appLstDt}</td>
                                            </tr>
                                            <%}%>
                                            <%if ((!"".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired()) && !"TSTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || (("".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) && (("OTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("OSTETM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("DPM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) && "".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) || ("LTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) && "".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) || ("RFQU".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQ".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQL".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())))))) 
                                              {
                                                  
                                                 //Nitish Start::: When select RFP view dates view is modified
                                                  if( appViewPkgDtBean.getReoiRfaRequired().equals("RFP") )
                                                  {
                                                        String strprocLabel = "";
                                                        if("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())){
                                                        strprocLabel = "RFA";
                                                        }else{
                                                            strprocLabel = "RFP";
                                                        }
                                            
                                            %>
                                            
                                            
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;"><%=strprocLabel%> Dates: </span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of <%=strprocLabel%> on e-GP Website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getRfaAdvertDt() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getRfaAdvertDt())));
                                                    }%><% if (appViewPkgDtBean.getRfaAdvertDays() != null) {
                                                            totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaAdvertDays();
                                                        }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Receipt of Application</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaReceiptDt} <% if (appViewPkgDtBean.getRfaReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Evaluation of Application</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaEvalDt} <% if (appViewPkgDtBean.getRfaReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Interview of Selected Individuals</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaInterviewDt} <% if (appViewPkgDtBean.getRfaInterviewDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaInterviewDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Evaluation of Final selection list</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaFinalSelDt} <% if (appViewPkgDtBean.getRfaFinalSelDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaFinalSelDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Submission of Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaEvalRptSubDt} <% if (appViewPkgDtBean.getRfaEvalRptSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaEvalRptSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Approval of Consultants</td>
                                                <td class="t-align-left">${appViewPkgDtBean.rfaAppConsultantDt}</td>
                                            </tr>
                                            
                                            
                                            <%}else if(appViewPkgDtBean.getReoiRfaRequired().equals("REOI")){  // show REOI dates ::nitish 
                                            %>
                                            
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">REOI Dates: </span></th>
                                            </tr>

                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of REOI on e-GP website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Last Date of Receipt of EOI</td>
                                    <td class="t-align-left">${appViewPkgDtBean.reoiReceiptDt} <% if (appViewPkgDtBean.getReoiReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getReoiReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Recommended Short-listed Firm</td>
                                                <td class="t-align-left">${appViewPkgDtBean.subDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of Recommended Shortlisted Firm</td>
                                                <td class="t-align-left">${appViewPkgDtBean.appLstDt}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">RFP Dates:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Issue of RFP</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Proposal</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderSubDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Technical proposal opening</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Technical Proposal Evaluation</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpTechEvalDt} <% if (appViewPkgDtBean.getRfpTechEvalDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpTechEvalDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Financial proposal opening</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpFinancialOpenDt} <% if (appViewPkgDtBean.getRfpFinancialOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpFinancialOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Combined Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptDt} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of Combined Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptAppDt} <% if (appViewPkgDtBean.getTenderEvalRptAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptAppDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Negotiation</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpNegCompDt} <% if (appViewPkgDtBean.getRfpNegCompDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpNegCompDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval for Award of Contract</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpContractAppDt} <% if (appViewPkgDtBean.getRfpContractAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpContractAppDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Signing of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays(); } %></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            </tr>
                                            
                                            <% } else {   //show goods/works  ::Nitish END::: %>
                                           
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;"><%=type%> Dates:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of <%=type%> on e-GP website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                        try
                                                        {
                                                            out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                                            totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                                        }
                                                        catch(Exception e)
                                                        {
                                                            
                                                        }
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of submission of <%=type%></td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderSubDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Opening of <%=type%></td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptDt} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval for Award of Contract</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderContractAppDt} <% if (appViewPkgDtBean.getTenderContractAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractAppDays();
                                                    }%></td>
                                            </tr>
                                            
                                             <% if(!methodFullName.equalsIgnoreCase("Direct Procurement")) {%>
                                                <tr>
                                                    <td class="ff">Expected Date of Letter of Intent to Award</td>
                                                    <td class="t-align-left">${appViewPkgDtBean.tenderLetterIntentDt} <% if (appViewPkgDtBean.getTnderLetterIntentDays() != null) {
                                                            totalNoofDays = totalNoofDays + appViewPkgDtBean.getTnderLetterIntentDays();
                                                        }%></td>
                                                </tr>
                                            <% } %>
                                            
                                            <tr>
                                                <td class="ff">Expected Date of Issuance of the Letter of Acceptance (LOA)</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderNoaIssueDt} <% if (appViewPkgDtBean.getTenderNoaIssueDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderNoaIssueDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Signing of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays(); } %></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            </tr>
                                            <%}} else if ("TSTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) 
                                                 {%>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left"> TSTM</span></th>
                                            </tr>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">1st Stage:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of IFB on e-GP website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Last Date of submission of Tenders</td>
                                    <td class="t-align-left">${appViewPkgDtBean.subDt} <% if (appViewPkgDtBean.getSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Opening of Tenders</td>
                                    <td class="t-align-left">${appViewPkgDtBean.openDt} <% if (appViewPkgDtBean.getOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.evalRptDt} <% if (appViewPkgDtBean.getEvalRptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getEvalRptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of the Evaluation Report</td>
                                                <td class="t-align-left">${appViewPkgDtBean.appLstDt}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">2nd Stage:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of issue of Final Tender Document to qualified tenderers</td>
                                    <td class="t-align-left"> <% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Last Date of Submission of Tender</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderSubDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Opening of Tender</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptDt} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of the Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptAppDt} <% if (appViewPkgDtBean.getTenderEvalRptAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptAppDays();
                                                    }%></td>
                                            </tr>
                                            <!--Code by Proshanto Kumar Saha-->
                                             <tr>
                                                <td class="ff">Expected Date of Letter of Intent to Award</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderLetterIntentDt} <% if (appViewPkgDtBean.getTnderLetterIntentDays() != null) {
                                                totalNoofDays = totalNoofDays + appViewPkgDtBean.getTnderLetterIntentDays();
                                                    }%></td>
                                            </tr>
                                            <!--End-->
                                            <tr>
                                                <td class="ff">Expected Date of Issue of Letter of Acceptance (LOA)</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderNoaIssueDt} <% if (appViewPkgDtBean.getTenderNoaIssueDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderNoaIssueDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Signing of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays();} %></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt} </td>
                                            </tr>
                                            <%} else if ("REOI".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) 
                                                {%>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">REOI Dates: </span></th>
                                            </tr>

                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of REOI on e-GP website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getAdvtDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getAdvtDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getAdvtDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Last Date of Receipt of EOI</td>
                                    <td class="t-align-left">${appViewPkgDtBean.reoiReceiptDt} <% if (appViewPkgDtBean.getReoiReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getReoiReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Recommended Short-listed Firm</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderSubDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of Recommended Shortlisted Firm</td>
                                                <td class="t-align-left">${appViewPkgDtBean.appLstDt}</td>
                                            </tr>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left">RFP Dates:</span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Issue of RFP</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getTenderAdvertDays() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getTenderAdvertDt())));
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderAdvertDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Proposal</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderSubDt} <% if (appViewPkgDtBean.getTenderSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Technical proposal opening</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt} <% if (appViewPkgDtBean.getTenderOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Technical Proposal Evaluation</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpTechEvalDt} <% if (appViewPkgDtBean.getRfpTechEvalDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpTechEvalDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Financial proposal opening</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpFinancialOpenDt} <% if (appViewPkgDtBean.getRfpFinancialOpenDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpFinancialOpenDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Submission of Combined Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptDt} <% if (appViewPkgDtBean.getTenderEvalRptdays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptdays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval of Combined Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.tenderEvalRptAppDt} <% if (appViewPkgDtBean.getTenderEvalRptAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderEvalRptAppDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Negotiation</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpNegCompDt} <% if (appViewPkgDtBean.getRfpNegCompDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpNegCompDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Approval for Award of Contract</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfpContractAppDt} <% if (appViewPkgDtBean.getRfpContractAppDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfpContractAppDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Signing of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt} <% //if(appViewPkgDtBean.getTenderContractSignDays()!=null){ totalNoofDays = totalNoofDays + appViewPkgDtBean.getTenderContractSignDays(); } %></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Completion of Contract</td>
                                                <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                                            </tr>
                                            <%} else if (appViewPkgDtBean.getReoiRfaRequired().equals("RFP") /*"RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired()) || "RFP".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired()*)*/ ) 
                                                {%>
                                             <% 
                                               String strprocLabel = "";
                                               if("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())){
                                                    strprocLabel = "RFA";
                                               }else{
                                                    strprocLabel = "RFP";
                                               }
                                            %>
                                            <tr>
                                                <th colspan="2" valign="top" class="table-section-header"><span style="float:left;"><%=strprocLabel%> Dates: </span></th>
                                            </tr>
                                            <tr>
                                                <td class="ff">Expected Date of Advertisement of <%=strprocLabel%> on e-GP Website</td>
                                    <td class="t-align-left"><% if (appViewPkgDtBean.getRfaAdvertDt() != null) {
                                                        out.print(sd1.format(sd.parse(appViewPkgDtBean.getRfaAdvertDt())));
                                                    }%><% if (appViewPkgDtBean.getRfaAdvertDays() != null) {
                                                            totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaAdvertDays();
                                                        }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Receipt of Application</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaReceiptDt} <% if (appViewPkgDtBean.getRfaReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Evaluation of Application</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaEvalDt} <% if (appViewPkgDtBean.getRfaReceiptDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaReceiptDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Interview of Selected Individuals</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaInterviewDt} <% if (appViewPkgDtBean.getRfaInterviewDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaInterviewDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Evaluation of Final selection list</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaFinalSelDt} <% if (appViewPkgDtBean.getRfaFinalSelDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaFinalSelDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Submission of Evaluation Report</td>
                                    <td class="t-align-left">${appViewPkgDtBean.rfaEvalRptSubDt} <% if (appViewPkgDtBean.getRfaEvalRptSubDays() != null) {
                                                        totalNoofDays = totalNoofDays + appViewPkgDtBean.getRfaEvalRptSubDays();
                                                    }%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Date of Approval of Consultants</td>
                                                <td class="t-align-left">${appViewPkgDtBean.rfaAppConsultantDt}</td>
                                            </tr>
                                            
                                            <%}
                                                                                        }
                                                                                       // if (request.getParameter("print") == null) {
                                            %>
                                            <!--tr>
                                                <%if (isLoggedIn && objUsrTypeId.toString().equals("2")) {
                                                                                                    if (appViewPkgDtBean.isInWatchList()) {
                                                %>
                                               
                                                <td align="center" id="addCell" style="display: none"><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                                                <td align="center" id="removeCell"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                                                <%} else {%>
                                                <td align="center" id="addCell" ><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                                                <td align="center" id="removeCell" style="display: none"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                                                <%}
                                                                                                }%>
                                            </tr-->
                                            <%
                                                                                     //   }
                                                                                    } else {
                                            %>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <div class="responseMsg errorMsg">Provide Valid App-Id, Package-Id & Package Type</div></td>
                                            </tr>
                                            <%            }
                                            %>
                                            <tr>
                                                <td class="ff">Total Time to Contract Signing </td>
                                                <td>
                                                    <%=totalNoofDays%>
                                                </td>
                                            </tr>
                                        </table>
                                         <%if(ImplementingAgencies.size()>0) {%>       
                                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1" width="100%">
                                            <tr>
                                                <th colspan="6" valign="top" class="table-section-header"><span style="float:left;">Implementing Agencies:</span></th>
                                            </tr>
                                            <tr>
                                                <th width="5%" class="t-align-center">Sl. No.</th>
                                                <th width="20%" class="t-align-center">APP ID,<br/>Letter Ref. No.</th>
                                                <th width="20%" class="t-align-center">Hierarchy Node,<br/>Agency Name</th>
                                                <th width="10%" class="t-align-center">Procurement Category</th>
                                                <th width="27%" class="t-align-center">Package No.,<br/>Description</th>
                                                 <th width="18%" class="t-align-center">Estimated cost/Official cost estimate (in Nu.)</th>
                                            </tr>
                                            <%
                                               for(int l=0; l< ImplementingAgencies.size(); l++)
                                               {
                                            %>
                                                   <tr>
                                                        <td class="t-align-center"><%=l+1%></td>
                                                        <td class="t-align-center"><%=ImplementingAgencies.get(l).getFieldName1()%><br/><%=ImplementingAgencies.get(l).getFieldName2()%></td>
                                                        <td class="t-align-center"><%=ImplementingAgencies.get(l).getFieldName3()%><br/><%=ImplementingAgencies.get(l).getFieldName4()%></td>
                                                        <td class="t-align-center"><%=ImplementingAgencies.get(l).getFieldName9()%></td>
                                                        <td class="t-align-center"><%=ImplementingAgencies.get(l).getFieldName6()%><br/><a href="ViewPackageDetail.jsp?appId=<%=ImplementingAgencies.get(l).getFieldName1()%>&pkgId=<%=ImplementingAgencies.get(l).getFieldName5()%>&pkgType=Package&from=home"><%=ImplementingAgencies.get(l).getFieldName7()%></a></td>
                                                        <td class="t-align-center"><%=ImplementingAgencies.get(l).getFieldName8()%></td>
                                                   </tr>
                                            <%
                                               }
                                            }
                                            %>
                                        </table>
                                    </form>
                               
                                </div>
                                                <table border="0" cellpadding="0" cellspacing="0" class="t_space c-alignment-right" >
                                <tr>
                                    <td>&nbsp;&nbsp;<span class="noprint">
<%                                      if (request.getParameter("print") == null) {
                                                if (isLoggedIn && objUsrTypeId.toString().equals("2")) {
                                                        if (appViewPkgDtBean.isInWatchList()) {
%>
<!--                                                <span id="addCell" style="display: none;">
                                                    <a id="watchList_1" onclick="addToWatchList()" class="action-button-add">Add to WatchList</a>
                                                </span>
                                                <span id="removeCell">
                                                    <a id="watchListRem_1" onclick="removeFromWatchList()" class="action-button-delete">Remove from WatchList</a>
                                                </span>-->
                                                <%}else{%>
<!--                                                <span id="addCell" >
                                                    <a id="watchList_1" onclick="addToWatchList()" class="action-button-add">Add to WatchList</a>
                                                </span>
                                                <span id="removeCell" style="display: none;">
                                                    <a id="watchListRem_1" onclick="removeFromWatchList()" class="action-button-delete">Remove from WatchList</a>
                                                </span>-->
<%                                                  }
                                                  }
                                            }
%>                                      </span>
                                    </td>
                                           <%  if (!(isPDF.equalsIgnoreCase("true"))) {%>
                                   <td> &nbsp;&nbsp;
                                        <%
                                              String reqURL = request.getRequestURL().toString();
                                              String reqQuery = "appId=" + strAppId + "&pkgId=" + strPkgId + "&pkgType=Package";
                                              String folderName = pdfConstant.APPVIEW;
                                              String id = strAppId + "_" + strPkgId;
                                              if (!(isPDF.equalsIgnoreCase("true"))) {
                                                pdfCmd.genrateCmd(reqURL, reqQuery, folderName, id);
                                                }
                                        %>
                                        <%if("Approved".equalsIgnoreCase(appPkgStatus)){%>
                                        <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=id%>" >Save As PDF </a>
                                        <%}%>
                                    </td>
                                     <%   }    %>
                                </tr>
                                </table>
                                </div>
                                </div>

   
                        <div>&#160;</div>
             
              <script type="text/javascript">
                            $(document).ready(function() {

                                $("#print").click(function() {
                                    //alert('sa');
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                });

                            });
                            function printElem(options){
                                //alert(options);

                                $('#print_area').printElement(options);
                                //$('#trLast').hide();
                            }

           </script>            
                    <%if (request.getParameter("print") == null) {%>
                        <!--Middle Content Table End-->
                    <%
                        if (!(isPDF.equalsIgnoreCase("true"))) {
                    %>
                    <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
                    <% }%>
                </div>
            </div>
           <%}%>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabApp");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
