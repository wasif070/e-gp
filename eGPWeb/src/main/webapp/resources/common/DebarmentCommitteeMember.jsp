<%-- 
    Document   : DebarmentCommitteeMember
    Created on : May 10, 2016, 4:35:21 PM
    Author     : PROSHANTO
--%>

<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Debarment Committee </title>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" /> 
    </head>
    <body>
        <%
            BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
            List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = bhutanDebarmentCommitteeService.getAllTblBhutanDebarmentCommittee();
        %>
        <div class="mainDiv">
            <div class="fixDiv">
               <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                    <%@include file="AfterLoginTop.jsp" %>
                <% } else { %>
                    <jsp:include page="Top.jsp" ></jsp:include>
                <% } %>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         <div class="pageHead_1">Debarment Committee Member</div>
                         <%
                                if(tblBhutanDebarmentCommittee.isEmpty())
                                {
                                    %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                                }
                                else{
                            %>
                         <table border="0" cellpadding="0" cellspacing="10" class="formBg_1 t_space" width="100%">
                             <!--<tr>
                                 <td>
                                     1. Mr. Rinzin Dorji, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     2. Mr. Karma Galay, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     3. Mr. Tandin Tshering, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     4. Mr. Dorji Thinley, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     5. Mr. Sonam Tashi, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     6. Mr. Chen Chen Dorji, Member
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     7. Mr. Karma Wangdi, Member Secretary
                                 </td>
                             </tr>-->
                             <%
                                  int cnt = 0;
                                  for(TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeThis : tblBhutanDebarmentCommittee){
                                      if(!tblBhutanDebarmentCommitteeThis.getRole().equals("Chairman") && tblBhutanDebarmentCommitteeThis.isIsActive())
                                      {
                                        cnt++;
                                        %>
                                        <tr>
                                            <td>
                                                  <%out.print(cnt);%>. <%out.print(tblBhutanDebarmentCommitteeThis.getMemberName());%>, <%out.print(tblBhutanDebarmentCommitteeThis.getRole());%>
                                              </td>
                                       </tr>
                                <%} }%>
                         </table>
                                    <%}%>
                            <!--Page Content End-->     
                        </td>
                        <%if(request.getParameter("Isadmin") != null && request.getParameter("Isadmin").equalsIgnoreCase("1")) { %>
                            
                        <% } else { %>
                            <td width="266">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                        <% } %>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                 <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
