<%--
    Document   : InboxMessage
    Created on : Oct 23, 2010, 7:31:07 PM
    Author     : dixit
--%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="java.util.*"%>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dashboard</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <%
        if(session.getAttribute("payment")!=null){
              session.removeAttribute("payment");
          }
                    Integer totalPages = 0;
                    String messageType = null;
                    messageType = "Live";
                    Integer folderid = 0;
                    String str = "Inbox";
                    String searching = "";
                    String keywords = "";
                    String emailId = "";
                    String dateFrom = "";
                    String dateTo = "";
                    String viewmsg = "NO";
                    Integer msgid = 0;
                    int totalRecords = 0;
                    String colHeader = null;
                    String colName = null;
                    String sortColumn = null;
                    String searchbtn = null;
                    String sctit = "Inbox";
                    String widths = "";
                    String emailIdSearchOpt = "";
                    MessageProcessSrBean mpsrb = new MessageProcessSrBean();
                    String logUserId="0";
                    if(session.getAttribute("userId")!=null){
                        logUserId=session.getAttribute("userId").toString();
                        mpsrb.setLogUserId(logUserId);
                    }
                    Object status = request.getParameter("status");
                    Object click = request.getParameter("click");
                    String searchOpt = "";
                    if ("Inbox".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        sctit = "Inbox";
                        searchOpt = "false";
                    } else if ("Sent".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        sctit = str;
                    }else if ("InboxUnRead".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        sctit = str;
                    } else if ("InboxRead".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        sctit = str;
                    }  else if ("Trash".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        messageType = "Trash";
                        sctit = str;
                    } else if ("Draft".equals(request.getParameter("inbox"))) {
                        str = request.getParameter("inbox");
                        messageType = "Draft";
                        sctit = str;
                    } else if ("Folder".equals(request.getParameter("inbox"))) {
                        str = "Folder";
                        String foldername = request.getParameter("foldername");
                        sctit = foldername;
                        folderid = Integer.valueOf(request.getParameter("folderid"));

                    } else if ("Reset".equals(request.getParameter("resbutton"))) {
                        str = "Reset";
                    }
                    String caption = "Messages";
                    String aling = "";
                    if (str.equals("Draft")) {
                        //String para[] = {"fromMailId", "subject", "isPriority", "creationDate","Edit"};
                        colHeader = "S.No,From/To,Subject,Priority,Date and Time,Action";
                        colName = "S.No,fromMailId,subject,isPriority,creationDate,Edit";
                        sortColumn = "false, true, true,true,true,false";
                        widths = "5,30,40,10,15,7";
                        aling = "center,left,left,center,center,left";
                        //data= mpsrb.getJSONArray(strList,para).toString();
                    } else {
                        colName = "S.No,fromMailId,subject,isPriority,creationDate";
                        sortColumn = "false,true,true,true,true";
                        widths = "5,30,45,10,20";
                        aling = "center,left,left,center,center";
                        if("inbox".equalsIgnoreCase(str)){
                            searchOpt = "false";
                        }
                        //String para[] = {"fromMailId", "subject", "isPriority", "creationDate"};
                        if (str.equalsIgnoreCase("Sent")) {
                            colHeader = " S.No,To,Subject,Priority,Date and Time ";
                        } else {
                            colHeader = " S.No,From,Subject,Priority,Date and Time";
                        }
                        //data= mpsrb.getJSONArray(strList,para).toString();
                    }
                    if ("Search".equalsIgnoreCase(request.getParameter("srchbutton"))) {
                        searchbtn = "Search";
                        str = request.getParameter("msgboxtype");
                        searching = request.getParameter("select");
                        keywords = request.getParameter("textfield5");
                        emailId = request.getParameter("textfield6");
                        dateFrom = request.getParameter("datefrom");
                        dateTo = request.getParameter("dateto");
                        emailIdSearchOpt = request.getParameter("txtemailSearchOption");
                    }

        %>
        <script type="text/javascript">
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function validatesearch()
            {   var flag = true;
                $('.err').remove();
                document.getElementById("todatespan").innerHTML="";
                if(document.getElementById("txtPublicationDate").value!="")
                {
                    if(document.getElementById("txtPublicationDate1").value=="")
                    {
                        document.getElementById("todatespan").innerHTML="Please select to date";
                        flag = false;
                    }
                }
                if(document.getElementById("textfieldd").value!="")
                {
                    if($('#idemailSearchOption').val()!='like'){
                        var emailid = document.getElementById("textfieldd").value;
                        var spaceTest =/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i
                        if(!spaceTest.test(emailid)){
                         document.getElementById("spanemail").innerHTML="Please enter valid e-mail ID";
                         flag = false;
                        }
                    }
                }
                if($("#select").val()=='--Select--' && $("#keyword").val()=='' && $("#textfieldd").val()=='' && $("#txtPublicationDate").val()=='' && $("#txtPublicationDate1").val()==''){
                    $('#errMsg').parent().append("<div class='reqF_1 err'><br />Pelase enter atleast one criteria</div>");
                    flag = false;
                }
//                alert(flag);
                return flag;
            }
        </script>
        <script type="text/javascript">
                    function doBlink() {
            var blink = document.all.tags("BLINK")
            for (var i=0; i<blink.length; i++)
            blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : ""
        }
        function startBlink() {
            if (document.all)
            setInterval("doBlink()",1000)
        }
        </script>
    </head>
    <body onload="startBlink();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
               <%@include file="AfterLoginTop.jsp" %>
                <div align="center">
                <%
                    String expiryDt="";
                    if(session.getAttribute("userId")!=null && session.getAttribute("userId")!=""){
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        List<TblLoginMaster> tblLoginMasters = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(session.getAttribute("userId").toString()));
                        TblLoginMaster tblLoginMaster = null;
                        Date expiryDate=null;
                        Calendar calExpiry= Calendar.getInstance() ,calCurrent= Calendar.getInstance();
                        if(tblLoginMasters!=null && (!tblLoginMasters.isEmpty())){
                            tblLoginMaster = tblLoginMasters.get(0);
                            if(tblLoginMaster.getTblUserTypeMaster().getUserTypeId()==2 && tblLoginMaster.getStatus().equalsIgnoreCase("approved")){
                            expiryDate = tblLoginMaster.getValidUpTo();
                            expiryDt=DateUtils.customDateFormate(expiryDate);
                            calExpiry.setTime(expiryDate);
                            calCurrent.add(Calendar.DATE, +30);
                                if(calExpiry.equals(calCurrent) || calExpiry.before(calCurrent)){%>
                                    <blink> <strong style="height:15px; line-height:15px;  color:#ff0000; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong> </blink>
                                <%}else{%>
                                    <strong style="height:15px; line-height:15px; color:#78A951; font-size: 12px; ">Bidder/Consultant Registration Expiry Date :<%=expiryDt%></strong><br/>
                                    <%} %>

                    <%}}}%>
                </div>
                
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <table width="100%" cellspacing="0">
                <tr valign="top">
                    <%--<td class="lftNav">
                        <jsp:include page="MsgBoxLeft.jsp" ></jsp:include></td>--%>
                    <td class="contentArea">

                       <%-- <%
                        String msg = "";
                        boolean flag=false;
                        if(request.getParameter("msg") != null){
                             msg = request.getParameter("msg");
                        }
                        if(request.getParameter("flag") != null){
                            if("true".equalsIgnoreCase(request.getParameter("flag"))){
                             flag = true;
                            }
                        }
                        if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("editGovtUser") && flag){
                        %>
                            <div class="responseMsg successMsg">Profile updated successfully</div>
                        <%
                        }
                        if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("hitsuccess")) {
                        %>
                            <div class="responseMsg successMsg">Hint Question and Answer changed successfully</div>
                        <%
                        }
                        if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("success")) {
                        %>
                            <div class="responseMsg successMsg">Password changed successfully</div>
                        <%
                        }
                        if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("succ")) {
                        %>
                            <div class="responseMsg successMsg">Message Deleted successfully</div>
                        <%
                        }
                        if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("fail")) {
                        %>
                            <div class="responseMsg errorMsg">Message Deletion Failed</div>
                        <%
                        }

                        else if (status != null && click != null) {
                                        String ck = click.toString();
                                        if (ck.equalsIgnoreCase("Send")) {
                        %>
                        <div class="responseMsg successMsg">Message Sent successfully</div>
                        <%
                                }else if (ck.equalsIgnoreCase("Trash")){
                        %>
                        <div class="responseMsg successMsg">Message Trashed successfully</div>
                        <%                  }else if (ck.equalsIgnoreCase("folder")) {
                        %>
                        <div class="responseMsg successMsg">Folder Created successfully</div>
                        <%                  }else if(ck.equalsIgnoreCase("moveto")){ %>
                        <div class="responseMsg successMsg">Message Moved successfully</div>
                        <%       }else{  %>
                         <div class="responseMsg successMsg">Message Saved successfully</div>
                        <%
                        }
                      }%>

                        <div class="pageHead_1"> <%=sctit%> </div>
                        <form name="frmsearch" action="InboxMessage.jsp?inbox=<%=sctit%>" method="post">
                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formBg_1 formStyle_1 t_space">
                                <tr>
                                    <td width="10%" class="ff">Search In : </td>
                                    <td width="40%"><select name="select" class="formTxtBox_1" id="select" style="width:150px;">
                                            <option>--Select--</option>
                                            <option>Subject</option>
                                            <option>Message</option>
                                        </select></td>
                                        <td width="10%">&nbsp;</td>
                                       <td width="40%">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="ff">Keyword :</td>
                                    <td><input name="textfield5" id="keyword" type="text" class="formTxtBox_1" id="textfield4" style="width:200px;" /> </td>
                                    <td class="ff">e-mail ID :</td>
                                    <td>
                                        <select name="txtemailSearchOption" class="formTxtBox_1" id="idemailSearchOption" style="width:70px;" >
                                              <option value="eq" selected>Equals</option>
                                              <option value="like">Contains</option>
                                        </select>
                                        <input name="textfield6" type="text" class="formTxtBox_1" id="textfieldd" style="width:180px;" />
                                        <span id="spanemail" class="reqF_1"></span>
                                     </td>
                                </tr>

                                <tr>
                                    <td class="ff">From Date : </td>

                                    <td colspan="1"><input name="datefrom" type="text" class="formTxtBox_1" readonly="true" id="txtPublicationDate" style="width:100px;" onclick="GetCal('txtPublicationDate','txtPublicationDate');"/>
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtPqdtsubevarptimg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtPublicationDate','txtPqdtsubevarptimg');"/></a>
                                    </td>
                                    <td class="ff">To Date :</td>
                                    <td>
                                        <input name="dateto" type="text" class="formTxtBox_1" readonly="true" id="txtPublicationDate1" style="width:100px;" onclick="GetCal('txtPublicationDate1','txtPublicationDate1');"/>
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtPqdtsubevarptimg1" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtPublicationDate1','txtPqdtsubevarptimg1');"/></a>
                                        <span id="todatespan" class="reqF_1"></span>
                                    </td>

                                </tr>


                                <tr>
                                    <td colspan="4"  class="t-align-center"><label class="formBtn_1">
                                            <input type="submit" name="srchbutton" id="button" value="Search" onclick="return validatesearch()"/>
                                            <input type="hidden" name="msgboxtype" id="msgboxtype" value="<%=str%>" />
                                        </label>&nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="resbutton" id="button" value="Reset" />
                                        </label>
                                        <label id="errMsg"></label>
                                    </td>
                                </tr>
                            </table>
                        </form>--%>


                        <%--<div align="left" class="t_space">
                            <jsp:include page="GridCommon.jsp" >
                                <jsp:param name="caption" value="<%=caption%>" />
                                <jsp:param name="url" value='<%=request.getContextPath() + "/MessageBoxServlet?inbox=" + str + "&select=" + searching + "&textfield5=" + keywords + "&textfield6=" + emailId + "&datefrom=" + dateFrom + "&dateto=" + dateTo + "&resbutton=" + str + "&msgboxtype=" + str + "&srchbutton=" + searchbtn + "&folderid=" + folderid + "&searchOpt=" +emailIdSearchOpt %>' />
                                <jsp:param name="colHeader" value='<%=colHeader%>' />
                                <jsp:param name="colName" value='<%=colName%>' />
                                <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                                <jsp:param name="width" value="<%=widths%>" />
                                <jsp:param name="aling" value="<%=aling%>" />
                                <jsp:param name="searchOpt" value="<%=searchOpt%>" />
                            </jsp:include>
                        </div>--%>


                        
                        <div align="left" class="t_space">
                            <%
                            int loginUserTypeId=Integer.parseInt(session.getAttribute("userTypeId").toString());
                            if(loginUserTypeId==2){%>
                            <jsp:include page="BidderDashboard.jsp" />
                            
                            <%} 
                             else { %>
                            <%--<p style="color: black; font-weight: bold; font-size:20pt ; text-align: center">WELCOME TO e-GP SYSTEM</p>--%>
                              <div align="center" style="margin-bottom: 200px;">
                                    <div style="width:50%; padding:20px;border: 1px solid #FF9326" >                        
                                        <h1> WELCOME </h1><br/>
                                        <p style="line-height:20px;"> Electronic Government Procurement (e-GP) System<br/>
                                        Government Procurement and Property Management Division (GPPMD)<br/>
                                            Department of National Properties<br/>
                                            Ministry of Finance<br/>
                                            Royal Government of Bhutan<br/>
                                       </p>
                                    </div>
                                </div>
                            <%}%>
                        </div>
                        
                        <div>&nbsp;</div></td>
                </tr>
            </table>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <%
      int userTypeforJRE = 0;
      if(session.getAttribute("userTypeId")!=null){
            userTypeforJRE= Integer.parseInt(session.getAttribute("userTypeId").toString());
      }
      if(userTypeforJRE==2){
%>
    <%--<script type="text/javascript">
        var siteUrl = "<%=request.getContextPath()%>/JREInstallation.jsp";
    </script>
    <script type="text/javascript" language="javascript" src="../js/form/deployJava.js"></script>
    <script type="text/javascript">
        deployJava.runApplet(
            {
                codebase:"/",
                archive:"Signer.jar",
                code:"com.cptu.egp.eps.SignerAPI.class",
                width:"0",
                Height:"0",
                ID: "SignerAPI",
                classloader_cache: "false"
            },
            null,
            "1.6"
        );
    </script> --%>
<% } %>
</html>
