<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="addFolderDtBean" class="com.cptu.egp.eps.web.databean.AddFolderDtBean"/>
<jsp:useBean id="addFolderSrBean" class="com.cptu.egp.eps.web.servicebean.AddFolderSrBean"/>
<jsp:setProperty property="*" name="addFolderDtBean"/>
<%@page import="java.util.List,java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblMessageFolder"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Folder</title>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="../js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.validate.js"></script>
     <script type="text/javascript">
            $(document).ready(function() {
                $("#frmAddFolder").validate({
                    rules: {
                        folderName: {required: true,maxlength:25}
                    },
                    messages: {
                        folderName: { required: "<div class='reqF_1'>Please enter Folder name.</div>",
                                    maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed.</div>"}
                           }

                });
            });
        </script>
</head>
<body>
    <%
            String logUserId="0";
            if(session.getAttribute("userId")!=null){
                logUserId=session.getAttribute("userId").toString();
                    addFolderSrBean.setLogUserId(logUserId);
             }
                 boolean val;String flag="";
                 
                    int uid = 0;
                   if(session.getAttribute("userId") != null){
                       Integer ob1 = (Integer)session.getAttribute("userId");
                       uid = ob1.intValue();
                   }

            if("Submit".equals(request.getParameter("submit")))
            {
                

                String folderName=request.getParameter("folderName");
                val=addFolderSrBean.checkFolderName(folderName,uid);                
                if(val)
                  {
                  addFolderSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL())); 
                  addFolderSrBean.addFolderName(addFolderDtBean,uid);
                  response.sendRedirect("InboxMessage.jsp?inbox=Inbox&click=folder&status=Yes");
                  }
                else
                {
                flag="<font color='red'>Folder Exists</font>";

                }
              
               }
         
    %>

<div class="dashboard_div">
  <!--Dashboard Header Start-->
          <div class="topHeader">
                <%@include file="AfterLoginTop.jsp" %>
            </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav">
      <!-- Left content of the page -->
      <jsp:include page="MsgBoxLeft.jsp" ></jsp:include>
    	</td>
      <td class="contentArea"><div class="pageHead_1">Add Folder</div>
          <form id="frmAddFolder" action="AddFolder.jsp" method="post">
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
            <tr>
              <td class="ff">Folder Name : <span>*</span></td>
              <td><input name="folderName" type="text" class="formTxtBox_1" id="txtfolderName" style="width:200px;" />
                 <%=flag%> <label id="status1" name="status"></label>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><label class="formBtn_1"><input type="submit" name="submit" id="btnsubmit" value="Submit" /></label></td>
            </tr>
         </table>
          </form>
        </td>
    </tr>
  </table>
              <div>&nbsp; </div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <jsp:include page="Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
    <script>
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
    