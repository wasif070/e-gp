<%--
    Document   : QuestionListing
    Created on : Jan 12, 2011, 4:10:19 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Cache-Control","no-cache");
            response.setHeader("Cache-Control","no-store");
            response.setHeader("Pragma","no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ask Procurement Expert</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />

<!--        <script src="../js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>-->
        
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">                
                <jsp:include  page="Top.jsp"/>
                <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
                <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
               <script type="text/javascript">
                    $j(function (){
                        $j("#list").jqGrid({
                            url:'<%=request.getContextPath()%>/AskProcurementServlet?q=1&action=fetchDataTender',
                            datatype: "xml",
                            height: 250,
                            colNames:['Sl. <br/> No.','Category','Query',"Action"],
                            colModel:[
                                {name:'srno',index:'srno', width:5,search:false,sortable:false,align :'center'},
                                {name:'category',index:'category', width:25,sortable:false,align :'center',searchoptions: { sopt: ['eq','cn']}},
                                {name:'question',index:'question', width:50,sortable:false,searchoptions: { sopt: ['eq','cn']}},                                
                                {name:'Action',index:'Action', width:10,sortable:false,align :'center',search:false}
                            ],

                            autowidth: true,
                            multiselect: false,
                            paging: true,
                            rowNum:10,
                            rowList:[10,20,30],
                            pager: $j("#page"),
                            caption: "Question",
                            gridComplete: function(){
                                $j("#list tr:nth-child(even)").css("background-color", "#fff");
                            }
                        }).navGrid('#page',{edit:false,add:false,del:false});
                    });
                </script>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Ask Procurement Expert</div>
                            <% if (request.getParameter("msg") != null && request.getParameter("msg").equals("create")) {%>
                            <br/><div align="left" id="sucMsg" class="responseMsg successMsg">Query posted successfully</div>
                            <% } %>
                            <div class="t_space t-align-right">
                                <a href="AskProcurement.jsp" class="action-button-add"><!--<img alt="AskProcurement" src="<-%=contextPath%>/resources/images/left-slider/plus.gif" />-->Post Query</a></div>
                            <form id="frmProcurementQuestion" name="frmProcurementQuestion" method="post" action="">
                                <div class="t_space">
                                    <table id="list">
                                    </table>
                                </div>
                                <div id="page" >
                                </div>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
