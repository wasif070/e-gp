<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblToDoList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.databean.AddTaskDtBean"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="addTaskDtBean" class="com.cptu.egp.eps.web.databean.AddTaskDtBean"/>
<jsp:useBean id="addTaskSrBean" class="com.cptu.egp.eps.web.servicebean.AddTaskSrBean"/>
<jsp:setProperty property="*" name="addTaskDtBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add New Task</title>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>


        <!-- rich text-->
         <script type="text/javascript" src="../../ckeditor/ckeditor.js"></script>
         <!--
        <script src="../../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
         -->
 <script type="text/javascript">
            $(document).ready(function() {               
                $("#frmAddNewTask").validate({

                    rules: {
                        taskBrief: {required: true,maxlength:25},
                        stDate:{required:true,CompareToForTodayAlso:"#egp"},
                        edDate:{required:true,CompareToForGreater:"#stDate"}
                    },
                    messages: {
                        taskBrief: { required: "<div class='reqF_1'>Please enter Task Brief</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        stDate:{required: "<div class='reqF_1'>Please select Start Date</div>",
                            CompareToForTodayAlso: "<div class='reqF_1'>Start Date should be equal or greater than Current Date</div>"
                        },
                        edDate:{required: "<div class='reqF_1'>Please select End Date</div>",
                            CompareToForGreater: "<div class='reqF_1'>End Date should be Greater than Start Date</div>"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "stDate")
                            error.insertAfter("#st");
                        else if (element.attr("name") == "edDate")
                            error.insertAfter("#ed");
                        else
                            error.insertAfter(element);
                    }
                });
                }
            );
   $(document).ready(function() {
    $('#frmAddNewTask').submit(function() {
            var vbool=true;
                if($.trim(CKEDITOR.instances.taskDetails.getData()) == ""){
                    document.getElementById("s_textarea").innerHTML="Please enter Task details";
                    vbool=false;
                }
                else
                {
                    if($.trim(CKEDITOR.instances.taskDetails.getData()).length <= 5000)
                    {     
                        document.getElementById("s_textarea").innerHTML="";
                    }
                    else
                    {
                        document.getElementById("s_textarea").innerHTML="Maximum 5000 characters are allowed.";
                        vbool=false;
                    }
                }
                if($('#frmAddNewTask').valid()){
                    if(!vbool){
                      return false;
                   }
                }
    });});
    $(document).ready(function() {
                $(document).ready(function() {
                CKEDITOR.instances.taskDetails.on('blur', function()
                {
                    if(CKEDITOR.instances.taskDetails.getData() != 0)
                    {
                            document.getElementById("s_textarea").innerHTML="";
                    }
                });
            });
            });
        </script>

    <%--<script type="text/javascript">
		$(document).ready(function (){
		$("#calendar1").calendar();
              });
     </script>

     <script type="text/javascript">
		$(document).ready(function (){
		$("#calendar2").calendar();
             });
	</script>--%>
        <script type="text/javascript">
            function GetCal(stDate,controlname)
            {
                new Calendar({
                    inputField: stDate,
                    trigger: controlname,
                    //showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });
                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>



</head>
<body>
    <%!
         AddTaskDtBean atd=new AddTaskDtBean();
    %>
     <%

                      int uid = 0;
                       if(session.getAttribute("userId") != null){
                           Integer ob1 = (Integer)session.getAttribute("userId");
                           uid = ob1.intValue();
                       }
              
            if ("Submit".equals(request.getParameter("button"))) {
                        String tb=request.getParameter("taskBrief");
                        String td=request.getParameter("taskDetails");
                        String pri=request.getParameter("priority");
                         String sDate=request.getParameter("stDate");
                          String eDate=request.getParameter("edDate");
                         SimpleDateFormat formatter ;
                        Date date;
                        Date date1;
                        formatter = new SimpleDateFormat("dd/MM/yy");
                       date = (Date)formatter.parse(sDate);
                       date1=(Date)formatter.parse(eDate);

                         atd.setTaskBrief(tb);
                         atd.setTaskDetails(td);
                         atd.setPriority(pri);
                         atd.setStartDate(date);
                         atd.setEndDate(date1);
                         addTaskSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                         addTaskSrBean.addAddTask(atd,uid);
                         response.sendRedirect("ViewTaskDetails.jsp?reply=newtask");
                         //RequestDispatcher rd = request.getRequestDispatcher("ViewTaskDetails.jsp?reply=newtask");
                        //rd.forward(request, response);

            }
      %>
          <div class="dashboard_div">
             <!--Dashboard Header Start-->
          <div class="topHeader">
    <%@include file="AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <form id="frmAddNewTask"  method="post" action="AddNewTask.jsp">
  <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav">
      <!-- Left content of the page -->
      <jsp:include page="MsgBoxLeft.jsp" ></jsp:include>
    	</td>

      <td class="contentArea"><div class="pageHead_1">Add New Task</div>
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
         <tr>
              <td class="ff">Task Brief : <span>*</span></td>
              <td><input type="text" class="formTxtBox_1" onchange="valid()" name="taskBrief" style="width:500px;"  id="textfield1" />
                   
               <label id="taskid"></label></td>
            </tr>
             <tr>
                 <td class="ff" width="90px">Task Details : <span>*</span></td>
                 <td width="610px"><textarea name="taskDetails" rows="6" class="formTxtBox_1" id="taskDetails" onchange="valids()" ></textarea>
                  <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'taskDetails',
                                                {
                                                    toolbar : "egpToolbar"
                                                   
                                                });

                                                //]]>
                                            </script>
                     <span id="s_textarea" style="color: red; ">&nbsp;</span>
             <label id="taskdetail"></label></td>
            </tr>
            <tr>
              <td class="ff">Priority : </td>
              <td><select name="priority" class="formTxtBox_1" id="select3">
                 <option selected="selected">High</option>
                <option selected="selected">Medium</option>
                <option selected="selected">Low</option>
                </select>
                </td>
            </tr>
            <tr>
              <td class="ff">Start Date : <span>*</span></td>
              <td> <input name="stDate" class="formTxtBox_1" id="stDate" type="text" style="width:100px;"  readonly="true"  onfocus="GetCal('stDate','stDate');"/>
                        <img id="st" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0"
                                        style="vertical-align:middle;"  onclick ="GetCal('stDate','st');"/></td>
            </tr>
            <tr>
              <td class="ff">End Date : <span>*</span></td>
              <td> <input name="edDate" class="formTxtBox_1" id="edDate" type="text" style="width:100px;"  readonly="true" onfocus="GetCal('edDate','edDate');"/>
                       <img id="ed" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0"
                            style="vertical-align:middle;"  onclick ="GetCal('edDate','ed');"/></td>

            </tr>
         <tr>
              <td>&nbsp;</td>
              <td><label class="formBtn_1">
                <input type="submit" name="button" id="button" value="Submit" />
                </label>
              </td>
            </tr>
          </table>
        </td>
    </tr>
  </table>
  </form>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
   <jsp:include page="Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
  <script>
        var headSel_Obj = document.getElementById("headTabMsgBox");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
