<%-- 
    Document   : PreTenderQueRep
    Created on : Nov 29, 2010, 5:26:00 PM
    Author     : parag
--%>

<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
        %>
        <%
                int userId = 0;
                byte suserTypeId = 0;
                int tenderId = 20;
                int queryId = 1;
                String viewType = "";
                String view = "";
                String tab = "4";
                String tableHead = "";
                String headClassName = "";

                if (session.getAttribute("userTypeId") != null) {
                    suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                }
                if (session.getAttribute("userId") != null) {
                    userId = Integer.parseInt(session.getAttribute("userId").toString());
                }
                if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if (request.getParameter("queryId") != null && !"null".equalsIgnoreCase(request.getParameter("queryId"))) {
                    queryId = Integer.parseInt(request.getParameter("queryId"));
                }
                if (request.getParameter("viewType") != null && !"null".equalsIgnoreCase(request.getParameter("viewType"))) {
                    viewType = request.getParameter("viewType");
                }
                if (request.getParameter("view") != null && !"".equalsIgnoreCase(request.getParameter("view"))) {
                    view = request.getParameter("view");
                }
                if (request.getParameter("tableHead") != null && !"null".equalsIgnoreCase(request.getParameter("tableHead"))) {
                    tableHead = request.getParameter("tableHead");
                    headClassName = "tableHead_1";
                } else {
                    headClassName = "pageHead_1";
                }

                List<SPTenderCommonData> getRepliedQuery = null;
                if ("Prebid".equalsIgnoreCase(viewType)) {
                    getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuery", tenderId, 0);
                    tab = "4";
                } else {
                    getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuestion", tenderId, 0);
                    tab = "11";
                }
                preTendDtBean.setTenderId(tenderId);
                boolean isPreBidPublished = preTendDtBean.isPrebidPublished(tenderId);
                pageContext.setAttribute("tenderId", tenderId);
                %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <% if(!"Docs".equals(view)) { %>
                Pre – Tender Meeting all Queries and Replies
            <% }else{ %>
                Pre – Tender Documents
            <% } %>
        </title>
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/js/ddlevelsmenu.js"></script>

        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
       -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if (!isPDF.equalsIgnoreCase("true")) {%>
                <%@include  file="AfterLoginTop.jsp" %>
                <%}%>
                

                <div class="dashboard_div">

                    

                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                   
                    <div class="contentArea_1"  <%if("hideDiv".equalsIgnoreCase( request.getParameter("hideDiv"))){%>style="display: none"<%}%>>
                            <%if (!isPDF.equalsIgnoreCase("true")) {%>
                    <%@include  file="TenderInfoBar.jsp" %>
                    <% if(!"Docs".equals(view)) { %>
                    
                    <span class="c-alignment-right" style="margin-top: 15px;">
                        <%if(request.getParameter("from") != null && "pubPreTenDoc".equalsIgnoreCase(request.getParameter("from"))){%>
                        <a href="<%=request.getContextPath()%>/officer/PublishPreTenderDoc.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        <%}else{%>
                        <a href="<%=request.getContextPath()%>/officer/PreTenderMeeting.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        <%}%>
                    </span>
                    <% } } %>
                    <div  id="print_area">
    <!-- Modified by Ketan : Defect Id : 2385 -->
                        <div class="<%=headClassName%>"
                        <%if(request.getParameter("hideDiv") == null){%>
                             style="margin-top: 15px;" >
                            <%}else if(getRepliedQuery.size() > 1 && isPreBidPublished){%>
                            style="margin-top: 15px;" >
                            <%}else{%>
                            style="margin-top: 15px; display: none;" >
                            <%}%>
                            <% if(!"Docs".equals(view)) { %>
                            Pre – Tender Meeting all Queries and Replies
                            <% }else{ %>
                            Pre – Tender Documents
                            <% } %>
                            </div>
    <!--End Defect Id : 2385 -->
                            <%
                                        // Variable tenderId is defined by u on ur current page.
                                        pageContext.setAttribute("tenderId", tenderId);
                            %>

                            <%--<%@include file="TenderInfoBar.jsp" %>
                            <div>&nbsp;</div>
                            <% //pageContext.setAttribute("tab",4); %>
                            <jsp:include page="../../officer/officerTabPanel.jsp" >
                                <jsp:param name="tab" value="<%=tab%>" />
                            </jsp:include>--%>

 <!-- Modified by Ketan : Defect Id : 2385 -->
 <%
    if(!"Docs".equals(view))
    {
                                                            
 %>
        <table width="100%" cellspacing="0" class="tableList_1 t_space"
               <%if(request.getParameter("hideDiv") == null){%>
               style="display: table" >
               <%}else if(getRepliedQuery.size() > 1 && isPreBidPublished){%>
               style="display: table" >
               <%}else{%>
               style="display: none;" >
               <%}%>

<!--End Defect Id : 2385 -->

            <tr>
                <th width="4%" class="t-align-left">Sl. No.</th>
                <th class="t-align-left" width="48%">Query</th>
                <th class="t-align-left" width="48%">Response</th>
            </tr>
            <%if (getRepliedQuery.size() > 0) {%>
            <%for (int iR = 0; iR < getRepliedQuery.size(); iR++) {%>
            <tr>
                <td class="t-align-left"><%=iR + 1%></td>
                <td class="t-align-left"><%=URLDecoder.decode(getRepliedQuery.get(iR).getFieldName2())%>
                    <br><br> <% out.print("\n\n <strong> Rephrased Query :</strong> " + getRepliedQuery.get(iR).getFieldName5());%></br></br>
                </td>
                <td class="t-align-left"><%=URLDecoder.decode(getRepliedQuery.get(iR).getFieldName4())%></td>
            </tr>
            <%}%>
            <%} else {%>
            <tr>
                <td colspan="3" class="t-align-left">No Record Found.</td>
            </tr>
            <%}%>
        </table>
        <% } %>
                    <%--            <%if(request.getParameter("hideDiv") == null){%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr><td>
                                        <div id="docData">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <%}%> --%>
<!-- Modified by Ketan : Defect Id : 2385 -->
                            <table width="100%" cellspacing="0" class="tableList_1 t_space"
                                    <%if(request.getParameter("hideDiv") == null){%>
                                   style="display: table" >
                                   <%}else if(isPreBidPublished){%>
                                   style="display: table" >
                                   <%}else{%>
                                   style="display: none;" >
                                   <%}%>
<!--End Defect Id : 2385 -->     <% if(!"Docs".equals(view)) { %>
                                <tr>
                                    <td width="68%" class="t-align-left ff">
                                        Pre Tender Meeting Document :
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space"
                                    <%if(request.getParameter("hideDiv") == null){%>
                                   style="display: table" >
                                   <%}else if(isPreBidPublished){%>
                                   style="display: table" >
                                   <%}else{%>
                                   style="display: none;" >
                                   <%}%>
                                <tr><td>
                                        <div id="doccData">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

 <%if(request.getParameter("hideDiv") == null){%>
                        <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                        <div class="t_space" align="right">
                            <%try {
                                     String reqQuery = "";
                                     String folderName = pdfConstant.PRETENDERMEETING + "_QueAns";
                                     String genId = Integer.toString(tenderId);
                                     String reqURL = request.getRequestURL().toString();
                                     reqQuery = "tenderId=" + tenderId + "&viewType=" + viewType;
                                     if (!(isPDF.equalsIgnoreCase("true"))) {
                                         pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                                     }
                            %>
                            <%if (!isPDF.equalsIgnoreCase("true")) {%>
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <%}%>
                            <a class="action-button-savepdf"
                               href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                            <% } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } //end if(isPdf)
                            %>
                            <!--Dashboard Content Part End-->
                            <!--Dashboard Footer Start-->
                            <%if (!isPDF.equalsIgnoreCase("true")) {%>
                            <jsp:include page="Bottom.jsp" ></jsp:include>
                            <%}%>
                            <!--Dashboard Footer End-->
                        </div>
                            <%}%>
                </div>
            </div>
        </div>
    </body>
</html>

<form id="form2" method="post">
</form>

<script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);

        $('#print_area').printElement(options);
        //$('#trLast').hide();
    }

</script>
<script>
    function gettDocData(){
        $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'ViewPreTenderMetsDocs'}, function(j){
            if(document.getElementById("doccData")){
                document.getElementById("doccData").innerHTML = j;
            }
        });
    }
    function downloaddFile(docName,docSize,tender){
        document.getElementById("form2").action= "<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?docName="+docName+"&docSize="+docSize+"&tenderId="+tender+"&funName=download";
        document.getElementById("form2").submit();
    }
    function deleteeFile(docName,docId,tender){
        $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if(r == true){
                $.post("<%=request.getContextPath()%>/PreTenderMetDocUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                    jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                        getDocData();
                    });
                });
            }else{
                getDocData();
            }
        });

    }
       

</script>
<script>
    gettDocData();
</script>
<script>
    function getDocData(){
        $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'PreTenderReplyMetDocs',viewAction:'view'}, function(j){
                if(document.getElementById("docData")){
                    document.getElementById("docData").innerHTML = j;
                }
        });
    }

    function downloadFile(docName,docSize,tender){
        document.getElementById("form2").action= "<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?docName="+docName+"&docSize="+docSize+"&tenderId="+tender+"&funName=download";
        document.getElementById("form2").submit();
    }

    function deleteFile(docName,docId,tender){
        $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if(r == true){
                $.post("<%=request.getContextPath()%>/PreTenderMetDocUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                    jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                        getDocData();
                    });
                });
            }else{
                getDocData();
            }
        });

    }
    getDocData();
</script>
<script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>