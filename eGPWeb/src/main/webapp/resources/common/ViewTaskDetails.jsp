<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AddTaskSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblToDoList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Task Details</title>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

 <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
 <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
 <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
  <script  type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>

  <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    //showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            
    </script>
  <script type="text/javascript">
      function CompareDate()
            {
                var firstDate = document.getElementById("DateFrom").value;
                var secondDate = document.getElementById("DateTo").value;

                var fdate = firstDate.split('/')  //Date and month split
                var fHr= fdate[2].split(' ');  //Year and time split

                var sDate = secondDate.split('/');
                var sHr = sDate[2].split(' ');

                if(fHr[1] == undefined){
                    var valueFirstDt= new Date(fHr[0], fdate[1]-1, fdate[0]);
                }
                if(sHr[1] == undefined){
                    var valueSecondDt= new Date(sHr[0], sDate[1]-1, sDate[0]);
                }
                var d = new Date();
                if(fHr[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                if(Date.parse(todaydate) < Date.parse(valueFirstDt))
                {
                    document.getElementById('fdtErr').innerHTML='<br/>Date From must not be greater than Today date';
                    return false;
                }
                
                if(Date.parse(valueFirstDt) > Date.parse(valueSecondDt)){
                    document.getElementById('dtErr').innerHTML='<br/>Date To must be greater than Date From';
                    return false;
                }else{
                    document.getElementById('dtErr').innerHTML='';
                    return true;
                    }
            }
  </script>
</head>
<body>
    <%!
           TblToDoList tdList=new TblToDoList();
          List list=null;
         String sDate;
         String eDate;

  %>
     <%
         String searchOpt = "";
         searchOpt = ",search:false";
         String colHeader = null;
         String colName = null;
         String sortColumn = null;
         String widths = null;
         String datefrom = null;
         String dateto = null;
         String status = "Pending";
         String reply = null;
          String aling = "";
         colHeader= "S.No,Task Brief,Priority,Start Date,End Date,Action";
            colName = "S.No,taskBrief,priority,startDate,endDate,View";
            sortColumn = "false, false, false,false,false,false";
            widths = "5,60,10,10,10,5";
            aling = "center,left,center,center,center,center";
            datefrom=request.getParameter("DateFrom");
            dateto=request.getParameter("DateTo");
         if(request.getParameter("select")!=null){
            status=request.getParameter("select");
         }
         if(request.getParameter("button")!=null && "Reset".equalsIgnoreCase(request.getParameter("button"))){
                    status = "Pending";
         }
            reply = request.getParameter("reply");
           
    %>


<div class="dashboard_div">
  <!--Dashboard Header Start-->
<div class="topHeader">
    <%@include file="AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav">
    	 <jsp:include page="MsgBoxLeft.jsp" ></jsp:include></td>
         <td class="contentArea"/><div class="pageHead_1">View Task Details </div>
        <% if(reply != null){
            if(reply.equals("newtask")){   %>

             <div class="responseMsg successMsg">Task added successfully.</div>
            <%
          }else if(reply.equals("completed")){ %>
        <div class="responseMsg successMsg">Task has been completed successfully.</div>
        <% } } %>
      <form action="ViewTaskDetails.jsp" method="post" id="frm">
        <table width="100%" cellspacing="10" class="formBg_1 formStyle_1 t_space">
            <!--adding code of search-->
            <tr>
                <td class="ff"> Date From: </td>
                <td> <input name="DateFrom" class="formTxtBox_1" id="DateFrom" type="text" style="width:100px;"  readonly="true" onfocus="GetCal('DateFrom','DateFrom');"/>
              <img id="DF" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0"
                        style="vertical-align:middle;"  onclick ="GetCal('DateFrom','DF');"/>
                        <span id="fdtErr" class="reqF_1"></span>
                </td>
              <td class="ff"> Date To: </td>
              <td> <input name="DateTo" class="formTxtBox_1" id="DateTo" type="text" style="width:100px;"  readonly="true" onfocus="GetCal('DateTo','DateTo');"/>
              <img id="DT" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0"
                        style="vertical-align:middle;"  onclick ="GetCal('DateTo','DT');"/>
                           <span id="dtErr" class="reqF_1"></span>
              </td>
            </tr>
            <tr>
              <td class="ff">Status : </td>
              <td><select name="select" class="formTxtBox_1" id="select" style="width:100px;">
                  <%
                  if(status.equalsIgnoreCase("completed")){ %>
                  <option selected="true">Completed</option>
                   <option>Pending </option>
                  <% }else{ %>
                  <option>Completed</option>
                  <option selected="true">Pending </option>
                  <%  } %>
                </select></td><td class="ff"> </td><td class="ff">   </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
               <td><label class="formBtn_1">
                       <input type="submit" name="button" id="button" value="Search" onclick="return CompareDate();"/>
                </label>&nbsp;
  		<label class="formBtn_1">
  		<input type="submit" name="button" id="button" value="Reset" />
 		</label>
		 </td><td class="ff">   </td><td class="ff">  </td>
            </tr>

        </table>
          <table width="100%" cellspacing="10" class="tableView_1">
                <tr>
                    <td align="right">
<!--                        <img src="../images/Dashboard/addIcn.png" alt="Create STD" class="linkIcon_1" />-->
                        <a style="color:#78A951;" class="action-button-add" href="AddNewTask.jsp">Add NewTask</a>
                       
                    </td>
                    
                </tr>
            </table>
            <div align="left">
            <jsp:include page="GridCommon.jsp" >
                <jsp:param name="caption" value=" " />
                <jsp:param name="url" value='<%=request.getContextPath()+"/TaskDetailsServlet?DateFrom="+datefrom+"&DateTo="+dateto+"&select="+status%>' />
                <jsp:param name="colHeader" value='<%=colHeader%>' />
                <jsp:param name="colName" value='<%=colName%>' />
                <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                 <jsp:param name="width" value="<%=widths%>" />
                  <jsp:param name="aling" value="<%=aling%>" />
                  <jsp:param name="searchOpt" value="<%=searchOpt%>" />
            </jsp:include>
        </div>

      </form>

    </tr></table>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <jsp:include page="Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
<script>
    var headSel_Obj = document.getElementById("headTabMsgBox");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
</html>