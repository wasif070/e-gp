<%--
    Document   : NegTendOfflineProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>
<%@page import="com.cptu.egp.eps.model.table.TblNegReply"%>
<%@page import="com.cptu.egp.eps.model.table.TblNegQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation Questions</title>
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="AfterLoginTop.jsp" %>
  <div class="contentArea_1">
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%
        int userType = 0;
        int tenderId = 0;
        int queryId = 0;
        int negId = 0;

        if (request.getParameter("negId") != null) {
            negId = Integer.parseInt(request.getParameter("negId"));
        }
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("queryId") != null) {
            queryId = Integer.parseInt(request.getParameter("queryId"));
        }
        pageContext.setAttribute("tenderId", tenderId);
        session = request.getSession();
        userType = Integer.parseInt(session.getAttribute("userTypeId").toString());
   %>
  <div class="pageHead_1">
      Negotiation Questions
      <span style="float:right;">
        <% if(userType==2){ %>
            <a href="<%=request.getContextPath()%>/tenderer/viewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>" class="action-button-goback">Go Back to Dashboard</a>
        <% }else{ %>
            <a href="<%=request.getContextPath() %>/officer/ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>" class="action-button-goback">Go Back to Dashboard</a>
        <% } %>
    </span>
  </div>
  <%@include file="TenderInfoBar.jsp" %>
  
   <%   if(userType==2)
                    { %>
                    <%pageContext.setAttribute("tab", "6");%>
                     <%@include file="../../tenderer/TendererTabPanel.jsp" %>
                        <div class="tabPanelArea_1">
                        <jsp:include page="../../tenderer/EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="4" />
                            <jsp:param name="tenderId" value="<%=tenderId%>" />
                        </jsp:include>
                <%  }
                    else
                    { 
                    
                %>
                         <% pageContext.setAttribute("tab", "7");%>
                        <%@include  file="../../officer/officerTabPanel.jsp"%>
                        <%-- Start: Common Evaluation Table --%>
                    <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                        <div class="tabPanelArea_1">
                        <%  pageContext.setAttribute("TSCtab", "7"); %>
                        <%@include file="AfterLoginTSC.jsp" %>
                    <% } %>  
        
     <div>&nbsp;</div>
  <div class="tabPanelArea_1">
                    <%
                    List<TblNegQuery> listQuery = negotiationdtbean.getQueryById(tenderId,negId,queryId);

                    String queryText = "";

                    if(!listQuery.isEmpty()){
                        queryText = listQuery.get(0).getQueryText();
                    }
                %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="10%" class="t-align-left ff">Question :</td>
                        <td width="90%" class="t-align-left">
                            <%=queryText%>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" class="t-align-left ff">Document :</td>
                        <td width="90%" class="t-align-left">
                            <a target="_blank" href="ViewNegQueryDocs.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>&queryId=<%=queryId%>">Documents</a>
                        </td>
                    </tr>
                    <%
                        List<TblNegReply> listReply = negotiationdtbean.viewReply(queryId);
                        String replyText = "";
                        if(!listReply.isEmpty()){
                            replyText = listReply.get(0).getReplyText();
                    %>
                     <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="10%" class="t-align-left ff">Reply :</td>
                        <td width="90%" class="t-align-left">
                            <%=replyText%>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" class="t-align-left ff">Document :</td>
                        <td width="90%" class="t-align-left">
                            <a target="_blank" href="ViewNegReplyDocs.jsp?tenderId=<%=tenderId%>&queryId=<%=queryId%>">Documents</a>
                        </td>
                    </tr>
                    <%   }
                    %>
                </table>
        </div>
   <div>&nbsp;</div>
                        </div>
   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center"
         <%@include file="Bottom.jsp" %>
   </div>
   <!--Dashboard Footer End-->
</div>
</body>
<script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
</html>