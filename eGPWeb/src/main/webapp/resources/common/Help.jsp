<%--
    Document   : Add NewClause
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>
<jsp:useBean id="helpManualSrBean" class="com.cptu.egp.eps.web.servicebean.HelpManualSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Help</title>
<link href="../css/home.css" rel="stylesheet" type="text/css" />
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="mainDiv">
  <div class="fixDiv">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td><!--Page Content Start-->
          <div class="t_space">
            <div class="pageHead_1">
                <div class="helpTitle">
                	Help
                </div>
            </div>

            <br/>
             
            <div class="txt_1 c_t_space">                
              <div class="listBox_aboutUs">
                 <%

                 String pageName = request.getParameter("pageName");
                 //out.println(pageName);
                 com.cptu.egp.eps.model.table.TblHelpManual tblHelpManual = helpManualSrBean.getHelpManuals(pageName);
                 if(tblHelpManual != null){
                     out.print(tblHelpManual.getHelpContent());
                 }else{
                     out.print("<i> No Help Content available </i>");
                 }
                 %>
              </div>
            </div>
          </div>
          
          <!--Page Content End-->
        </td>
      </tr>
    </table>
    
  </div>
</div>
</body>
</html>
