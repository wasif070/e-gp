<%-- 
    Document   : OnlinePaymentSuc
    Created on : Feb 17, 2012, 6:12:01 PM
    Author     : nishit
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
            OnlinePaymentDtBean onlinePaymentDtBean =(OnlinePaymentDtBean)session.getAttribute("payment");
            if(onlinePaymentDtBean!=null){
                if(onlinePaymentDtBean.isShowRespMsg()){
%>

<div class="responseMsg successMsg t_space" >
    Your transaction is successful. Transaction information is as below
</div>
<%}%>
<div style="float: right;padding-top: 5px;">
<a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
</div>
<div  id="print_area">
<%if ("nur".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){  %>
 <div style="color:red;padding-top: 8px;font-weight: bold;" id="hideWarning">Please take print out of this slip, scan this and upload as payment receipt.</div>
 <div class="pageHead_1" id="printTitle" style="display: none;">Registration fee payment slip</div>
 <%}%>
<table border="0" cellspacing="20" cellpadding="20" class="tableList_1 c_t_space" id="tb2" width="100%">
    <%if ("nur".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())){ %>
    <tr>
        <td class="ff" width="25%">E-mail Id: </td>
        <td><%=request.getSession().getAttribute("emailId") %></td>
    </tr>
    <%}%>
    <tr>
        <td class="ff" width="25%">
            Amount (in Nu.) :
        </td>
        <td><%=onlinePaymentDtBean.getAmt() %></td>
        
    </tr>
    <tr>
        <td class="ff" width="25%">
            Service Charge (%) :
        </td>
        <td><%=onlinePaymentDtBean.getServiceCharge() %></td>
        
    </tr>
    <tr>
        <td class="ff" width="25%">
            Total Amount (in Nu.) :
        </td>
        <td><%=onlinePaymentDtBean.getTotal() %></td>
        
    </tr>
    <tr>
        <td class="ff" width="25%">
            Transaction Id :
        </td>
        <td><%=URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") %></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Card Number :
        </td>
        <td><%=(onlinePaymentDtBean.getResponse().indexOf("CARD_NUMBER: ") > 0 ? onlinePaymentDtBean.getResponse().substring(onlinePaymentDtBean.getResponse().indexOf("CARD_NUMBER: ")+13, onlinePaymentDtBean.getResponse().lastIndexOf(",")): (onlinePaymentDtBean.getCardNumber().trim().equals("")?"":onlinePaymentDtBean.getCardNumber()))%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Payment Date Time :
        </td>
        <td><%=onlinePaymentDtBean.getResponse().substring(onlinePaymentDtBean.getResponse().indexOf("_DATETIME :")+11)  %></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Online Payment Gateway :
        </td>
        <td><%=XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) %></td>
    </tr>
</table>
</div>
<%}%>