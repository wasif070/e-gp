<%--
    Document   : ViewTender
    Created on : Nov 15, 2010, 3:18:02 PM
    Author     : rajesh, rishita, Swati
--%>




<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceWeightage"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <%
                    int lotno = 0;
                    String id = "";
                    if (!request.getParameter("id").equals("")) {
                        id = request.getParameter("id");
                        pageContext.setAttribute("tenderId", id );
                    }
                    Object userId = session.getAttribute("userId");
                    Object usrTypeId = session.getAttribute("userTypeId");

                    int userTId = 0;
                    if (session.getAttribute("userTypeId") != null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                        userTId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }

                    boolean isPreBidPublished = preTendDtBean.isPrebidPublished(Integer.parseInt(id));
                    String tenderStatus = "";
                    boolean action = false;
                    if(request.getParameter("action") != null && "cancel".equals(request.getParameter("action"))){
                        action = true;
                    }
                    boolean isShowPrint = true;
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View IFB /PQ / REOI / RFP Notice Details</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/js/ddlevelsmenu.js"></script>
        <script src="../../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <!--jalert -->
        <%if (request.getParameter("print") == null) {%>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%}%>
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script  type="text/javascript">
            $(function() {
                $('#taComments').blur(function() {
                    if($('#taComments').val() == ''){
                        $('#valComments').html('Please enter Comments');
                    }else if($('#taComments').val().length > 2000){
                        $('#valComments').html('Maximum 2000 characters are allowed');
                    }else{
                        $('#valComments').html('');
                    }
                });
                $('#Cancel').click(function() {
                    if($('#taComments').val() == ''){
                        $('#valComments').html('Please enter Comments');
                    }else if($('#taComments').val().length > 2000){
                        $('#valComments').html('Maximum 2000 characters are allowed');
                    }else{
                        alert("Please release the bid security money if any tenderer is already paid. \nYou can find the tenderer list on below TAB after approval of Cancellation Process through Workflow.\n\n ''Tender>>My Tender>>Cancelled>>Dashboard>>Payment>>Tender Security.''");
                        $.post("<%=request.getContextPath()%>/WatchListServlet", {tenderId : '<%=id%>',Comments: $('#taComments').val(),funName: 'Cancel'},  function(j){
                            if(j.toString() == 'cancel'){
                                var myform=document.getElementById("frm_notice");
                                myform.action = '<%=request.getContextPath()%>/officer/Notice.jsp?tenderid=<%=id %>&TenderCancel=true';
                                myform.submit();
                            }else if(j.toString() == 'not success'){
                                var myform=document.getElementById("frm_notice");
                                myform.action = '<%=request.getContextPath()%>/officer/Notice.jsp?tenderid=<%=id %>&TenderCancel=false';
                                myform.submit();
                            }
                        });
                    }

                });

                $('#watchList').click(function() {
                    $.post("<%=request.getContextPath()%>/WatchListServlet", {type: "Tender",tenderId: <%=id%>,userId: <%=userId%>,funName: 'Add'},  function(j){
                        if(j.toString() != "0")
                        {
                            jAlert("Added to your WatchList","Success");
                            $("#watchId").val(j.toString());
                            $("#addCell").hide();
                            $("#removeCell").show();

                        }
                        else
                        {
                            jAlert("Can not be added to WatchList","Failure");

                        }

                    });
                });
            });
        </script>
        <script  type="text/javascript">
            $(function() {
                $('#watchListRem').click(function() {
                    $.post("<%=request.getContextPath()%>/WatchListServlet", {type: "Tender",watchId: $("#watchId").val(),funName: 'Remove'},  function(j){
                        if(j.toString() == "true")
                        {
                            jAlert("Removed from your WatchList","Success");
                            $("#removeCell").hide();
                            $("#addCell").show();

                        }
                        else
                        {
                            jAlert("Can not be removed from WatchList","Failure");
                        }
                    });
                });
            });
        </script>
    </head>
    <%if (request.getParameter("print") == null) {%>
    <script type="text/javascript">
        function printPage()
        {
            $("#viewForm").submit();
        }

    </script>
    <%}%>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <% if(action){  %>
                <%@include  file="../../resources/common/AfterLoginTop.jsp"%>
                <!--Dashboard Header Start-->
                <%           }
                            boolean isLoggedIn = false;
                            String strProcureMethod ="";
                            Object objUsrId = session.getAttribute("userId");
                            if (objUsrId != null) {
                                preTendDtBean.setLogUserId(objUsrId.toString());
                                tenderSrBean.setLogUserId(objUsrId.toString());
                                isLoggedIn = true;
                            }
                            String isPDF= "abc";
                            if(request.getParameter("isPDF") != null)
                                    isPDF= request.getParameter("isPDF");

                             if(! (isPDF.equalsIgnoreCase("true")) ){
                            if (isLoggedIn) {
                %>
<!--                AfterLoginTop.jsp-->
                <%} else {
                                if (request.getParameter("print") == null) {%>
                <% } }
            }//end of isPDF
            %>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                 <div id="print_area">
                <div class="pageHead_1">&nbsp;
                    <span class="t-align-left" >View IFB /PQ / REOI / RFP Notice Details</span>
                    <% if(action){ %>
                    <span style="float:right;" id="printRem"><a href="<%=request.getContextPath() %>/officer/Notice.jsp?tenderid=<%=id %>" class="action-button-goback">Go Back to Dashboard</a></span>
                    <% } %>
                     <span class="t-align-right ff mandatory" style="float:right;"  >
                         <%if(isPreBidPublished){%>
                            (&nbsp;Meeting Doc. Available.&nbsp;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <%}%>
                     </span>
                </div>
                <%
                            String tenderInfoStatus = "";
                            List<CommonTenderDetails> list = tenderSrBean.getAPPDetails(Integer.parseInt(id), "tender");
                            if (!list.isEmpty()){
                            for (CommonTenderDetails commonTenderDetails : list) {
                                List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(id));
                                tenderStatus = commonTenderDetails.getTenderStatus();
                                if(!isLoggedIn && (tenderStatus.trim().equalsIgnoreCase("Pending") || commonTenderDetails.getEventType().trim().equalsIgnoreCase("LTM"))){
                                    isShowPrint = false;
                    %>
                                 <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                                    <tr>
                                        <td width="100%" class="ff" align="center"><b>This tender is not exists or You are un-authorized to access information for this tender</b></td>
                                    </tr>
                                 </table>
                                <%}else{
                               // tenderInfoStatus = commonTenderDetails.getTenderevalstatus();
                                List<SPCommonSearchDataMore> isTenderSecAmt = tenderSrBean.isAvalTenSecAmt(id);
                %>
                <form method="POST" id="frmviewForm" action="<%=request.getContextPath()%>/PDFServlet">
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Procuring Agency :
                                <input type="hidden" name="reportName" value="Tender_Detail"/>
                                <input type="hidden" name="reportPage" value="TenderDtlNoticeRpt.jsp?<%=request.getQueryString()%>"/></td>
                            <td width="25%"><%=commonTenderDetails.getPeOfficeName()%>, <%=commonTenderDetails.getMinistry()%>, <%=commonTenderDetails.getPeDistrict()%></td>
<%--                            <td width="25%" class="ff">Department :</td>
                            <td width="25%"><%=commonTenderDetails.getDivision()%></td>--%>
                        </tr>
                            <tr><%
                                                        if (commonTenderDetails.getAgency() != null) {
                                                            if (!commonTenderDetails.getAgency().trim().equalsIgnoreCase("")) {
                            %>
                            <td class="ff">Organization :</td>
                            <td><%=commonTenderDetails.getAgency()%></td>
                            <% }
                                                            }%>
                            <%--td class="ff">Procuring Agency Name :</td>
                            <td><%=commonTenderDetails.getPeOfficeName()%></td--%>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency Code :</td>
                            <td><%=commonTenderDetails.getPeCode()%></td>
<%--                            <td class="ff">Procuring Agency Dzongkhag / District :</td>
                            <td><%=commonTenderDetails.getPeDistrict()%></td>--%>
                        </tr>
                        <tr>
                            <td class="ff" width="25%">Procurement Category :</td>
                            <td width="25%"><%=commonTenderDetails.getProcurementNature()%></td>
                            <td class="ff" width="25%">Procurement Type :</td>
                            <td width="25%"><%if(commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");}%> <!--Edit by aprojit -->
                        </tr>
                        <%
                                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                                Object list2 = null;
                                Object[] list3 = null;
                                list2 = tenderDocumentSrBean.getPkgNo(id);
                                list3 = tenderDocumentSrBean.getBidderCategory(list2.toString());
                                if(commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works"))
                                {%>
                                <%-- Start Nitish --%>
                                    <tr>
                                        <td class="ff">Work Type :</td>
                                        <td> <%out.print(list3[1].toString());%></td>
                                    </tr>
                                
                                    <tr>
                                        <td class="ff">Work Category :</td>
                                        <td> <%out.print(list3[0].toString());%></td>
                                    </tr>    
                                <%-- Start Nitish --%> 
                               <%}%>
                        
                        <tr>
                            <!--td class="ff">Event Type :</td-->
                            <%--td><% if (commonTenderDetails.getEventType() != null) {%><%=commonTenderDetails.getEventType().toUpperCase() %><% }%></td--%>
                            <% strProcureMethod = commonTenderDetails.getProcurementNature();
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) { %> <td class="ff">Invitation for :</td>
                            <td><% if (commonTenderDetails.getInvitationFor() != null) {%><%=commonTenderDetails.getInvitationFor()%><% }%></td> <% }%>
                        </tr><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {

                                    String msg = "";
                                    if (commonTenderDetails.getEventType() != null) {
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                            msg = "Request for expression of interest for selection of";
                                        } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                            msg = "RFP for selection of";
                                        }
                                    }
                        %>
                        <tr><% if (!msg.equals("")) {%>
                            <td class="ff"><%=msg%></td>
                            <td><label><%=commonTenderDetails.getReoiRfpFor()%></label><%--<input name="expInterestSel" readonly type="text" class="formTxtBox_1" id="txtexpInterestSel" style="width:200px;" value="<%=commonTenderDetails.getReoiRfpFor()%>" />--%></td><% }%>
                            <td class="ff"></td>
                            <td></td>
                        </tr><% }%>
                        <!-- RFQ->LEM, DPM->DCM -->
                        <tr><%  if (commonTenderDetails.getContractType() != null && !commonTenderDetails.getContractType().equals("")) {%>
                            <td class="ff">Contract Type : </td>
                            <td><%=commonTenderDetails.getContractType()%></td>
<%                              }
                                String msgTender = "";
                                if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {
                                    msgTender = "Invitation Reference No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                    msgTender = "REOI No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                    msgTender = "RFP No.";
                                }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {
                                    msgTender = "RFA No.";
                                }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ")) {
                                    msgTender = "LEM No.";
                                }
%>
                            <td class="ff"><%=msgTender%><% if (!msgTender.equals("")) {%> :</td>
                            <td><%=commonTenderDetails.getReoiRfpRefNo()%></td><% }%>
                        <%
                            TenderService tenderStatusService = (TenderService) AppContext.getSpringBean("TenderService");
                           // CommonTenderDetails tenderStatusDetail =  tenderStatusService.gettenderinformationSP("get alltendersbytype", "AllTenders", commonTenderDetails.getTenderId(), 0, 0, 0, "", 0, "", null, null, "Approved", "", "", "" , "", null, null, 1, 1).get(0);
                            long cntForCT = tenderStatusService.getTenderCorriCnt(Integer.parseInt(id));
                            long cntForBeingP = tenderStatusService.getBeingProcessCnt(Integer.parseInt(id));
                            if(!commonTenderDetails.getTenderStatus().equalsIgnoreCase("pending"))
                            {
                                if(userTId == 3)
                                {
                                    if( (new Date().getTime() >= commonTenderDetails.getTenderPubDt().getTime()) &&
                                         (new Date().getTime() < commonTenderDetails.getSubmissionDt().getTime() && !("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())))
                                     ){
                                        if(request.getParameter("h") != null && request.getParameter("h").equalsIgnoreCase("t"))
                                        {
                                            tenderInfoStatus = "Live";
                                         }
                                        if(cntForCT != 0){
                                            tenderInfoStatus = "Amendment/Corrigendum issued : " + cntForCT ;
                                        }
                                    } else if("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())){
                                        //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                        tenderInfoStatus = "Cancelled";
                                        //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                                    }  else if ("Re-Tendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "To be Re-Tendered ";
                                    }  else if ("Rejected".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Rejected";
                                    }  else if ("Awarded".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Contract Awarded";
                                    }  else if ("Approved".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Approved";
                                    }  else if ("NOA declined".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "NOA declined";
                                    }  else if ("Being evaluated".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Being evaluated";
                                    }  else if ("Clarification Requested".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Clarification Requested";
                                    }  else if ("Opening Completed".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Opening Completed";
                                    }  else if ("ReTendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Re-Tendered";
                                    }else if( (new Date().getTime() > commonTenderDetails.getSubmissionDt().getTime()) &&
                                                (cntForBeingP == 0)
                                                //&& !("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())) &&
                                                //!("Re-Tendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim()))
                                      ){
                                            tenderInfoStatus = "Being processed";
                                      }
                                }
                                else
                                {
                                    if( (new Date().getTime() >= commonTenderDetails.getTenderPubDt().getTime()) &&
                                         (new Date().getTime() < commonTenderDetails.getSubmissionDt().getTime() && !("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())))
                                     ){
                                        if(request.getParameter("h") != null && request.getParameter("h").equalsIgnoreCase("t"))
                                        {
                                            tenderInfoStatus = "Live";
                                         }
                                        if(cntForCT != 0){
                                            tenderInfoStatus = "Amendment/Corrigendum issued : " + cntForCT ;
                                        }
                                    }  else if ("Re-Tendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "To be Re-Tendered";
                                    }  else if ("Rejected".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Rejected";
                                    }  else if ("Awarded".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Contract Awarded";
                                    }else if("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())){
                                        //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                        tenderInfoStatus = "Cancelled";
                                        //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                                    }  else if ("ReTendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim())) {
                                        tenderInfoStatus = "Re-Tendered";
                                    }else if( (new Date().getTime() > commonTenderDetails.getSubmissionDt().getTime()) &&
                                                (cntForBeingP == 0) //&&
                                                //!("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())) &&
                                                //!("Re-Tendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim()))
                                      ){
                                            tenderInfoStatus = "Being processed";
                                    }
                                }
                            }

                            if(!strProcureMethod.equalsIgnoreCase("Services") && !"".equals(tenderInfoStatus.trim())) {
%>
                                <td class="ff">Tender Status : </td>
                                <td><label style="color: red;font-weight: bold;"><%=tenderInfoStatus%></label></td>
<%                          } %>
                        </tr>
<%                       if(strProcureMethod.equalsIgnoreCase("Services") && !"".equals(tenderInfoStatus.trim())) {%>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                                <td class="ff">Tender Status : </td>
                                <td><label style="color: red;font-weight: bold;"><%=tenderInfoStatus%></label></td>
                          </tr>
<%                      }      
              //   Dohatech for showing re-tendered id  Start
    if(!"0".equalsIgnoreCase(commonTenderDetails.getReTenderId().toString())) {   %>
                        <tr>
                              <td class="ff">Re-Tendered ID : </td>
                                <td><label><%=commonTenderDetails.getReTenderId().toString()%></label></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>

                          </tr>
<%                      }        //  Dohatech for showing re-tendered id  End 
				  %>
                    </table>
                    <!--div class="tableHead_22 ">Key Information and Funding Information :</div-->
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Procurement Method : </td>
                            <td width="25%">
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQ")){%><%="Limited Enquiry Method (LEM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")){%><%="Open Tendering Method (OTM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM")){%><%="Limited Tendering Method (LTM)"%><%}%>
                              <%-- <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("TSTM")){%><%="Two Stage Tendering Method (TSTM)"%><%}%>--%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")){%><%="Quality Cost Based Selection (QCBS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")){%><%="Least Cost Selection (LCS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){%><%="Selection under a Fixed Budget (SFB)"%><%}%>
                              <%--   <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DC")){%><%="Design Contest (DC)"%><%}%>--%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")){%><%="Selection Based on Consultants Qualification (SBCQ)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SSS")){%><%="Single Source Selection (SSS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")){%><%="Individual Consultant (IC)"%><%}%>
                              <%--  <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("CSO")){%><%="Community Service Organisation (CSO)"%><%}%>--%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DPM")){%><%="Direct Contracting Method (DCM)"%><%}%>
                                <%--  <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OSTETM")){%><%="One stage Two Envelopes Tendering Method (OSTETM)"%><%}%>--%>
                                 <%-- <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQU")){%><%="Request For Quotation Unit Rate (RFQU)"%><%}%>--%>
                                 <%-- <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQL")){%><%="Request For Quotation Lump Sum (RFQL)"%><%}%>--%>
                                 <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("FC")){%><%="Framework Contract"%><%}%>
                            </td>
                            <td width="25%" class="ff">Budget Type :</td>
                            <td width="25%"><%if(commonTenderDetails.getBudgetType().equalsIgnoreCase("Revenue")){out.print("Recurrent");}else if(commonTenderDetails.getBudgetType().equalsIgnoreCase("Development")){out.print("Capital");} else {out.print(commonTenderDetails.getBudgetType());} %></td> <!--Edit by aprojit -->
                        </tr>
                        <tr>
                            <td width="25%" class="ff">Source of Funds :</td>
                            <td width="25%"><%=commonTenderDetails.getSourceOfFund()%></td>
                            <% if (commonTenderDetails.getDevPartners() != null) {%>
                                <td width="25%" class="ff">Development Partner :</td>
                                <td width="25%"><%=commonTenderDetails.getDevPartners()%></td>
                                <% } %>
                        </tr>
                        <% List<Object> allocatedCost=tenderSrBean.getAllocatedCost(Integer.parseInt(id));
                             if (!allocatedCost.isEmpty() && !(allocatedCost.get(0).toString().startsWith("0.00"))) {
                            if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM") || commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){
                            %>
                        <tr>
                            <td width="25%" class="ff">Allocated Budget Amount in Nu. :</td>
                            <td width="25%"><%=new BigDecimal(allocatedCost.get(0).toString()).setScale(2,0) %></td>
                            <td width="25%" class="ff">&nbsp;</td>
                            <td width="25%">&nbsp;</td>
                        </tr>
                            <% } }%>
                    </table>

                    <div class="tableHead_22 ">Particular Information :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Project Code : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectCode() != null) {%><%=commonTenderDetails.getProjectCode()%><% } else {%>Not applicable<%}%></td>
                            <td width="25%" class="ff">Project Name : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectName() != null) {%><%=commonTenderDetails.getProjectName()%><% } else {%>Not applicable<%}%></td>
                        </tr>
                        <tr>
                            <td  width="25%" class="ff"><% if (!(commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP"))) {%>Tender/Proposal <% }%>Package No. and Description :</td>
                            <td colspan="3" width="75%"><%=commonTenderDetails.getPackageNo()%><br/>
                            <%=commonTenderDetails.getPackageDescription()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Category : </td>
                            <td colspan="3"><%=commonTenderDetails.getCpvCode()%></td>
                        </tr>
                        <tr>
                            <%
                                                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm");
                                                            String msgpublication = "";
                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                                                msgpublication = "Scheduled Tender/Proposal";
                                                            } else if (!commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                                                msgpublication = commonTenderDetails.getEventType().toUpperCase().toString();
                                                            } else {
                                                                msgpublication = "Scheduled Pre-Qaulification";
                                                            }
                            %>
                            <td class="ff"><%//=msgpublication%> Publication<br/>Date and Time :</td>
                            <td class="formStyle_1"><%if (commonTenderDetails.getTenderPubDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getTenderPubDt()) %><% }%></td>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification<% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender<% }%> Document last selling /<br/>downloading Date and Time : </td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getDocEndDate() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getDocEndDate())%><% }%></td>
                        </tr>

                        <% if (commonTenderDetails.getPreBidStartDt() != null && !commonTenderDetails.getPreBidStartDt().toString().equals("01-Jan-1900 00:00")) {%>
                        <tr>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <% } else {%>Pre - bid <%}%>meeting Start<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getPreBidStartDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getPreBidStartDt())%><% }%></td>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <% } else {%>Pre - bid <%}%>meeting End<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getPreBidEndDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getPreBidEndDt())%><% }%></td>
                        </tr>
                        <% } %>
                        <% //if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                         if (commonTenderDetails.getSecurityLastDt() != null && !obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {%>
                        <tr>
                            <td class="ff">Last Date and Time for bid Security<br />Submission :</td>
                        <!--                        <tr>
                            <td class="ff"> <p>Name and Address<br />
                                    of the Office(s) for tender security submission :</p>      </td>
                            <td><%//commonTenderDetails.getSecuritySubOff()%></td>
                        </tr>-->
                            <td><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getSecurityLastDt())%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <% } %>
                        <tr>
                            <td class="ff"><%   if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }
                                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> <!--Proposal --> <% //}
                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Closing<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getOpeningDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getSubmissionDt())%><% }%></td>
                            <td class="ff"><%   if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }
                                                       // if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%><!--Proposal --> <% //}
                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Opening<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getSubmissionDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getOpeningDt())%><% }%></td>
                        </tr>
                         
                    </table>

                    <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <%
                                                        if (commonTenderDetails.getEligibilityCriteria() != null) {
                                                            if (!commonTenderDetails.getEligibilityCriteria().equals("")) {
                        %>
                        <tr>
                            <td width="25%" class="ff"><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Eligibility of Tenderer <% } else {%>Eligibility of Consultant <% }%> :</td>
                            <td width="75%"><%=commonTenderDetails.getEligibilityCriteria()%></td>
                        </tr>
                        <% }
                                                        }
                                                        if (commonTenderDetails.getTenderBrief() != null) {
                                                            if (!commonTenderDetails.getTenderBrief().equals("")) {
                        %>
                        <tr>
                            <td class="ff" >Brief Description of <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>Goods and Related Service<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Works<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>assignment<% }%> :</td>
                            <td><%=commonTenderDetails.getTenderBrief()%></td>
                        </tr>
                        <% }
                                                        }
                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {

                                                            if (commonTenderDetails.getDeliverables() != null) {
                                                                if (!commonTenderDetails.getDeliverables().equals("")) {
                        %>
                        <tr>
                            <td class="ff">Experience, Resources and<br />
                                delivery capacity required :</td>
                            <td><%=commonTenderDetails.getDeliverables()%></td>
                        </tr>
                        <% }
                                                                                    }
                                                                                    if (commonTenderDetails.getOtherDetails() != null) {
                                                                                        if (!commonTenderDetails.getOtherDetails().equals("")) {
                        %>
                        <tr>
                            <td class="ff">Other details (if  applicable) :</td>
                            <td><%=commonTenderDetails.getOtherDetails()%></td>
                        </tr>
                        <% } }
                                                                                    if (commonTenderDetails.getForeignFirm() != null) {
                                                                                        if (!commonTenderDetails.getForeignFirm().equals("")) {
                        %>
                        <tr>
                            <td class="ff"> Association with
                                foreign  firm :</td>
                            <td><%=commonTenderDetails.getForeignFirm()%></td>
                        </tr>
                        <% }
                                                            }
                                                        }
                        %>
                        <tr>
                            <td class="ff"> Evaluation Type :</td>
                            <td><%  //commonTenderDetails.getEvalType()
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) { %>
                                    Package wise
                                    <% } else { %>
                                    <%=commonTenderDetails.getEvalType()%>
                                    <% } %> 
                            </td>
                        </tr>
                        <%
                                                        if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>

                        <tr>
                            <%for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                                                                            lotno++;
                                                                                        }
                            %>
                            <td class="ff"> Document Available :</td>
                            <td><%
                                if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Lot")){
                            %>Lot wise<% }else if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Package")){ %>Package wise<% } %></td>
                        </tr>
                        <% } if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                        %>
                        <tr>
                            <td class="ff">Document Fees :</td>
                            <td><%=commonTenderDetails.getDocFeesMethod()%></td>
                        </tr>
                        <% }
                        //if(lotno == 1 && !commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){
                        /*if(!commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){
                                                            if(commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("1 STAGE-TSTM")){
                                                                if(commonTenderDetails.getPkgDocFees() != null){*/
                        %>
                        <!--<tr>
                                <td class="ff"><% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In BTN)<% //}
                                                                                                //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender Document Price (In BTN)<% //}%>
                                :</td>
                            <td><-%=commonTenderDetails.getPkgDocFees()%></td>
                        </tr>
                            <% //} }else if(commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") && commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getPqTenderId() ==0){
                                //if(commonTenderDetails.getPkgDocFees() != null){
                                %>
                                <tr>
                                    <td class="ff"><% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In BTN)<%// }
                                                                                                    //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender Document Price (In BDTBTN)<% //}%>
                                    :</td>
                                    <td><-%=commonTenderDetails.getPkgDocFees()%></td>
                                </tr>--><% /*}
                            }
                        }*/
                                if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if(commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise")){
                                %>
                        <tr>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In Nu.)<% }
                                                   if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender/Proposal Document Price (In Nu.)<% }
                                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("RFP")){%>RFP Document Price (In Nu.)<%}
                                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU") ||commonTenderDetails.getEventType().equalsIgnoreCase("RFQL")){%>RFQ Document Price (In Nu.)<% }
                                                       if(commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){%>REOI Document Price (In Nu.)<% }
                                                       if(commonTenderDetails.getEventType().equalsIgnoreCase("RFA")){%>RFA Document Price (In Nu.)<% }
                            %>
                            :</td>
                            <td><%=commonTenderDetails.getPkgDocFees().setScale(0, 0)%></td>
                        </tr>
                            <% } }
                                //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("1 STAGE-TSTM")) {
                                if(!obj.isEmpty()){
                                if(obj.get(0)[0].toString().equalsIgnoreCase("Yes") || obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                            %>
                        <!--  Start ICT Tender by Dohatec-->
                       <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                         <tr id="docspriceICT"> <td class="ff">Equivalent Tender Document Price (In USD) : </td>
                            <td><%=commonTenderDetails.getPkgDocFeesUSD().setScale(0, 0)%> </td>
                         </tr>
                        <% } %>
                        <!--  End ICT Tender by Dohatec-->
                        <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><label>Payment through Bank</label>
                                <%//commonTenderDetails.getDocFeesMode()%></td>
                        </tr>
                            <% }if(obj.get(0)[2].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")){%>
                                <td class="ff">
                                    <%
                                    List<TblTenderLotSecurity> listSecAmt = tenderSrBean.isMultilpleLot(Integer.parseInt(id));
                                    BigDecimal secAmt = new BigDecimal(0);
                                    if(listSecAmt!=null & !listSecAmt.isEmpty()){
                                    secAmt = listSecAmt.get(0).getTenderSecurityAmt();
                                    }
                                    String msgSecAmt = "";
                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {msgSecAmt="Please Enter Pre-Qualification Security Amount"; %>Pre-Qualification Security Amount (In Nu.)<% }
                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {msgSecAmt = "Please Enter Tender Security Amount";%>Tender Security Amount (In Nu.) <% }
                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("RFP")){ msgSecAmt="Please Enter RFP Security Amount";%>RFP Security Amount (In Nu.)<% }
                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQL") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU")){ msgSecAmt="Please Enter RFQ Security Amount";%>RFQ Security Amount (In Nu.)<% }
                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){ msgSecAmt="Please Enter REOI Security Amount";%>REOI Security Amount (In Nu.)<% }
                                    if(commonTenderDetails.getEventType().equalsIgnoreCase("RFA")){ msgSecAmt="Please Enter RFA Security Amount";%>RFA Security Amount (In Nu.)<% }

                                    %>
                                    :
                                </td>
                                <td>
                                    <%=secAmt.setScale(0,0)%>
                                </td>
                                <td>&nbsp;</td>
                            <%}}

                                                                       // if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") && commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getPqTenderId() == 0) {

                                                            // } }
                               if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                                   if(!commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){
                                   //EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                                //if(evalSerCertiSrBean.evalCertiLink(Integer.parseInt(id)) != 0){
                                   if(commonTenderDetails.getPassingMarks() != 0){
                            %>
                            <tr>
                                <td class="ff">Passing Points :</td>
                                <td><%=commonTenderDetails.getPassingMarks() %></td>
                                <td>&nbsp;</td>
                            </tr>
                                <% }}

                                    boolean isT1L1 = false;
                                    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", id, null, null);
                                    if(envDataMores!=null && (!envDataMores.isEmpty())){
                                        if(envDataMores.get(0).getFieldName2().equals("3")){
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                        isT1L1 = true;
                                        }
                                    }
                                if(isT1L1){
                                    EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                                    TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
                                    tblEvalServiceWeightage = evalSerCertiSrBean.getTblEvalServiceByTenderId(Integer.parseInt(id));

                            %>
                            <tr>

                                <td class="ff">Weightage For Technical Evaluation (%) : </td>
                                <td><%= tblEvalServiceWeightage.getTechWeight()%>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff">Weightage For Financial Evaluation (%) : </td>
                                <td><%= tblEvalServiceWeightage.getFinancialWeight()%>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <%}/*Condition for QCBS*/ } //} %>
                             <%                               if(commonTenderDetails.getTenderStatus().equalsIgnoreCase("Approved") && commonTenderDetails.getTenderSecurityDt() != null && commonTenderDetails.getTenderValidityDt() != null){
                                    java.text.SimpleDateFormat format2 = new java.text.SimpleDateFormat("dd-MMM-yyyy");
                                    if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                                %>
                            <tr>
                                <td class="ff"><%if(commonTenderDetails.getProcurementNature().equalsIgnoreCase("Service")){%>Proposal <% }else{ %>Tender/Proposal <% } %>Security Valid Up to :</td>
                                <td><%=format2.format(commonTenderDetails.getTenderSecurityDt())%></td>
                            </tr>
                            <% } if(!obj.isEmpty() && obj.get(0)[3].toString().equalsIgnoreCase("Yes")){%>
                            <tr>
                                <td class="ff"><%if(commonTenderDetails.getProcurementNature().equalsIgnoreCase("Service")){%>Proposal <% }else{ %>Tender/Proposal <% } %>Valid Up to :</td>
                                <td><%=format2.format(commonTenderDetails.getTenderValidityDt()) %></td>
                            </tr>
                                <% } } %>

                    </table>
                    <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>
                    <table width="100%" cellspacing="0" class="tableList_1 ">
                        <tr>
                            <th class="t-align-center">Ref. No. <br /></th>
                            <th class="t-align-left">Phasing of service <br /></th>
                            <th class="t-align-center">Location</th>
                            <th class="t-align-center" >Indicative Contract Start Date <br /></th>
                            <th class="t-align-center">Indicative Contract End Date <br /></th>
                        </tr>
                        <% int i = 0;
                             for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "Phase")) {
                                 java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("dd-MMM-yyyy");//remove hh:mm
                                 i++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=commonTdetails.getPhasingRefNo() %></td>
                            <td class="t-align-left"><%=commonTdetails.getPhasingOfService()%></td>
                            <td class="t-align-center"><% if (commonTdetails.getLocation() != null) {%><%=commonTdetails.getLocation()%><% }%></td>
                            <td class="t-align-center"><%=format1.format(commonTdetails.getIndStartDt())%></td>
                            <td class="t-align-center"><%=format1.format(commonTdetails.getIndEndDt())%></td>
                        </tr>
                        <%}%>
                    </table>
                    <% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {
                    %>

                    <table width="100%" cellspacing="0" class="tableList_1 ">
                        <tr>
                            <th width="6%" class="t-align-left">Lot No.</th>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise") && !obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                            %>
                            <th width="34%" class="t-align-left">Identification of Lot</th>
                            <% }else if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){%>
                            <th width="44%" class="t-align-left">Identification of Lot</th>
                            <%}else if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){%>
                            <th width="44%" class="t-align-left">Identification of Lot</th>
                            <%}else{%>
                            <th width="54%" class="t-align-left">Identification of Lot</th>
                            <% } %>
                            <th width="10%" class="t-align-left">Location</th>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if (commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")) {%>
                            <th width="10%" class="t-align-left">Document Fees<br />(Amount in Nu.)</th>
                            <% } }

                                 //if(!isTenderSecAmt.isEmpty() && isTenderSecAmt.get(0).getFieldName1().equals("1")){
                                if(!isTenderSecAmt.isEmpty() && isTenderSecAmt.get(0).getFieldName1().contains("1")){
                            %>
                                    <!-- ICT Start Nazmul-->
                                    <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT") && isTenderSecAmt.get(0).getFieldName1().equals("10")) {%>
                                        <th width="20%" class="t-align-center">Bid security (Amount in Nu.)<br /> and Type</th>
                                    <%}
                                       else if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {
                                            if(isTenderSecAmt.get(0).getFieldName1().equals("11")) {
                                    %>
                                                <th width="20%" class="t-align-center">Bid Security (Amount in Nu.) and Type</th>
                                                <th width="5%" class="t-align-center">Equivalent Bid Security (Amount in USD)</th>
                                    <%      }
                                            else if(isTenderSecAmt.get(0).getFieldName1().equals("10"))   {
                                    %>
                                                 <th width="15%" class="t-align-center">Bid security (Amount in Nu.)<br /> and Type</th>
                                    <%      }
                                            else if(isTenderSecAmt.get(0).getFieldName1().equals("01"))   {
                                    %>
                                                 <th width="10%" class="t-align-center">Equivalent Bid Security (Amount in USD)</th>
                                    <%      }
                                    }%>
                                   <!-- ICT end Nazmul-->
                                <% }%>
                            <th width="10%" class="t-align-center">Contract Start Date</th>
                            <th width="10%" class="t-align-left">Contract End Date</th>
                        </tr>
                        <% int j = 0;
                             for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                 j++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=commonTdetails.getLotNo()%></td>
                            <td class="t-align-left"><%=commonTdetails.getLotDesc()%></td>
                            <td class="t-align-center"><% if (commonTdetails.getLocationSec() != null) {%><%=commonTdetails.getLocationSec()%><% }%></td>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if (commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")) {%>
                            <td class="t-align-center"><%=commonTdetails.getDocFess().setScale(0,0)%></td>
                            <% } }
                                   //if(!isTenderSecAmt.isEmpty() && isTenderSecAmt.get(0).getFieldName1().equals("1")){
                               if(!isTenderSecAmt.isEmpty() && isTenderSecAmt.get(0).getFieldName1().contains("1")){
                            %>
                            <!--<td class="t-align-center"><%=commonTdetails.getTenderSecurityAmt().setScale(0,0)%></td>-->
                            <!-- ICT Start Dohatec-->
                                <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT") && isTenderSecAmt.get(0).getFieldName1().equals("10")) {%>
                                        <td class="t-align-center">
                                            <div> <%=commonTdetails.getTenderSecurityAmt().setScale(0,0)%> </div>
                                            <div style="text-align: left; padding: 2px 0 0 22px;"> 
                                            <%if(commonTdetails.getBidSecurityType().equals("1") || commonTdetails.getBidSecurityType().equals("3")){
                                              out.print("Financial Institution Payment");
                                              %><br/><%}%>
                                            <% 
                                            if(commonTdetails.getBidSecurityType().equals("2") || commonTdetails.getBidSecurityType().equals("3")){
                                              out.print("Bid Securing Declaration");
                                             }
                                             %>
                                            </div>
                                        </td>
                                <%}
                                   else if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {
                                        if(isTenderSecAmt.get(0).getFieldName1().equals("11")) {
                                %>
                                            <td class="t-align-center">
                                            <div> <%=commonTdetails.getTenderSecurityAmt().setScale(0,0)%> </div>
                                            <div style="text-align: left; padding: 2px 0 0 22px;"> 
                                            <%if(commonTdetails.getBidSecurityType().equals("1") || commonTdetails.getBidSecurityType().equals("3")){
                                              out.print("Financial Institution Payment");
                                              %><br/><%}%>
                                            <% 
                                            if(commonTdetails.getBidSecurityType().equals("2") || commonTdetails.getBidSecurityType().equals("3")){
                                              out.print("Bid Securing Declaration");
                                             }
                                             %>
                                            </div>
                                            </td>
                                            <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmtUSD().setScale(0,0)%> </td>
                                <%      }
                                        else if(isTenderSecAmt.get(0).getFieldName1().equals("10"))   {
                                %>
                                             <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmt().setScale(0,0)%></td>
                                <%      }
                                        else if(isTenderSecAmt.get(0).getFieldName1().equals("01"))   {
                                %>
                                             <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmtUSD().setScale(0,0)%> </td>
                                <%      }
                                }%>
                            <!-- ICT End Dohatec-->
                            <% }
                                 java.text.SimpleDateFormat format3 = new java.text.SimpleDateFormat("dd/MM/yyyy");
                                 java.text.SimpleDateFormat format4 = new java.text.SimpleDateFormat("dd-MMM-yyyy");

                            %>
                            <td class="t-align-center"><%if(commonTdetails.getStartTime() != null && !"".equals(commonTdetails.getStartTime())){%><%=format4.format(format3.parse(commonTdetails.getStartTime()))%><% } %></td>
                            <td class="t-align-center"><%if(!"".equals(commonTdetails.getCompletionTime())){%><%=format4.format(format3.parse(commonTdetails.getCompletionTime()))%><% } %></td>
                        </tr>
                        <% }
                         }%>
                    </table>
                    <br/> <br/>
                     <%
                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                int iTenderId = Integer.parseInt( (String) pageContext.getAttribute("tenderId"));

                Date lastUpdateDate = tenderCurrencyService.getLastDateOfCurrencyExchangeRates(iTenderId);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, d MMM, yyyy HH:mm:ss");
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                
                
                if(tenderCommonService1.getSBDCurrency(iTenderId).equalsIgnoreCase("Yes")){
                    
                    %>
            
            <div class="tableHead_1 t_space" style="margin-top: 3px;">Currency conversion rate as of <%= dateFormatter.format(lastUpdateDate) %></div>
            <div class="tabPanelArea_1 ">
                <table id="tblCurrency" width="100%" cellspacing="0" class="tableList_1">
                    <thead>
                        <tr>
                            <th width="15%" class="t-align-center">No.</th>
                            <th width="43%" class="t-align-center">Currency Name</th>
                            <th width="42%" class="t-align-center">Conversion Rate in Nu.</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <%
                           String currencyRows = ""; //will hold the result

                            List<Object[]> listCurrencyObj2 = new ArrayList<Object[]>();
                                                     
                            listCurrencyObj2 = tenderCurrencyService.getCurrencyTenderwise(iTenderId);

                            if(listCurrencyObj2.size() > 0){
                                int rowNo = 0;
                                for(Object[] obj1 :listCurrencyObj2){
                                    rowNo +=1;
                                    currencyRows+= "<tr>";
                                    currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                                    currencyRows+= "<td width='43%' class='t-align-left'>" + obj1[3] + " - " + obj1[0] + "</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                    String str = obj1[1].toString();
                                    if(Double.valueOf(str).doubleValue() == 0){
                                        currencyRows+= "<td width='42%' class='t-align-center'></td>";
                                    }
                                    else {
                                        currencyRows+= "<td width='42%' class='t-align-center'>" + obj1[1] + "</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                    }
                                }

                                    currencyRows+= "</tr>";
                                }
                           
                            out.print(currencyRows);
                            
                            }
                        %>
                    </tbody>
                </table>
            </div>
                    
                    
                    

                    <div class="tableHead_22 t_space">Procuring Agency Details:</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td class="ff" width="20%">Name of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender<% }
                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td width="30%" ><%=commonTenderDetails.getPeName()%></td>
                            <td nowrap class="ff" width="20%" nowrap> 
                                Designation :
                            </td>
                            <td width="30%"><%=commonTenderDetails.getPeDesignation()%></td>
                        </tr>

                        <tr>
                            <td class="ff">
                                Official Address :
                            </td>
                            <td><%
                                    if(commonTenderDetails.getPeAddress().contains("Thana")){
                                        out.print(commonTenderDetails.getPeAddress().replace("Thana", "Gewog"));              
                                      }else{
                                        out.print(commonTenderDetails.getPeAddress());  
                                    }
                                %></td>
                            <td class="ff" nowrap>
                                Contact details :
                            </td>
                            <td><%=commonTenderDetails.getPeContactDetails()%>
                            </td>
                        </tr>
                            <% if(action){%>
                        <tr>
                                <td class="ff">Comment : <span>*</span></td>
                                <td colspan="3"><textarea rows="5" class="formTxtBox_1" id="taComments" name="comments" style="width: 400px;" onkeypress="return imposeMaxLength(this, 2001);" onpaste="return imposeMaxLength(this, 2001);"></textarea>
                                    <div id="valComments" class="reqF_1"></div>
                                </td>
                            </tr>
                            <% } %>
                        <tr>
                            <td colspan="4" style="text-align: center;" class="ff mandatory">The procuring Agency reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center;" class="mandatory ff">Note: Financial Institute will update the payment transactions only at the end of the day, so the bidders should make sure the securities and other payments are made at least one day before the submission date. </td>
                        </tr>

                        <tr id="tdPrintDocument">
                            <td class="t-align-left">
                                <%
                                if(! (isPDF.equalsIgnoreCase("true")) ){
                                    if(userTId == 2){
                                        %><a class="action-button-document" href="<%=request.getContextPath()%>/tenderer/LotPckDocs.jsp?tenderId=<%=id %>" >Documents</a>
                                        <%
                                    }else if(session.getAttribute("userTypeId") == null ){
                                    %><a class="action-button-document" href="<%=request.getContextPath()%>/LotPckDocs.jsp?tenderId=<%=id %>" >Documents</a>
                                    <%
                                   }
                                    }
                                %>

                        <!--After Document link -->

                </td>
                                 <td class="t-align-right" colspan="3">
                                     <% if(request.getParameter("h") != null && request.getParameter("h").equalsIgnoreCase("t")) {
                                            if (isLoggedIn && (usrTypeId.toString().equals("2"))) {
                                                int watchId = tenderSrBean.checkWatchList(Integer.parseInt(userId.toString()), Integer.parseInt(id));
                                                if (tenderSrBean.isInWatchList()) {
                                    %>
                                    <div id="addCell" style="display: none">
                                        <a id="watchList" class="action-button-add">Add to WatchList</a>
                                    </div>
                                    <div id="removeCell"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></div>
                                    <%      } else {    %>
                                    <div  id="addCell" ><a id="watchList" class="action-button-add">Add to WatchList</a></div>
                                    <div  id="removeCell" style="display: none"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></div>
                                    <%      }%>
                                    <div><input type="hidden" id="watchId" value="<%=watchId%>" /></div>
                                    <%      }
                                        }   %>
                                 </td>
            </tr>

                    </table>
<!--                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space" width="100%">
                        <tr>

                            <%//if (isLoggedIn && (usrTypeId.toString().equals("2"))) {
                                                                //int watchId = tenderSrBean.checkWatchList(Integer.parseInt(userId.toString()), Integer.parseInt(id));
                                                                //if (tenderSrBean.isInWatchList()) {
                            %>
                            <td align="center" id="addCell" style="display: none"><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                            <td align="center" id="removeCell"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                            <%//} else {%>
                            <td align="center" id="addCell" ><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                            <td align="center" id="removeCell" style="display: none"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                            <%//}%>
                            <td><input type="hidden" id="watchId" value="<%//watchId%>" /></td>
                            <%//}%>
                        </tr>
                    </table>-->
                </form>
                <%      }// complete else of loging and tenderstatus or eventype check
                    }
            }else{// IN case of list is empty then it will show below message
                    isShowPrint = false;
                %>
                 <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                    <tr>
                        <td width="100%" class="ff" align="center"><b>This tender is not exists or You are un-authorized to access information for this tender</b></td>
                    </tr>
                 </table>
                <%}
                if(isPreBidPublished)
                {%>
                <div>
                    <jsp:include page="PreTenderQueRep.jsp">
                        <jsp:param  name="tenderId" value="<%=id%>" />
                        <jsp:param  name="tableHead" value="tableHead" />
                        <jsp:param  name="viewType" value="Prebid" />
                        <jsp:param  name="isPDF" value="true" />
                        <jsp:param  name="hideDiv" value="hideDiv" />
                    </jsp:include>
                </div>
                <%}%>
                <div>
                    <jsp:include page="ViewTenderDoc.jsp" flush="true">
                        <jsp:param name="tenderId" value="<%=id%>"/>
                    </jsp:include>
                </div>
                <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                </div>
                <% if(! (isPDF.equalsIgnoreCase("true")) ){%>
                <div class="t-align-center">

  <%                    String reqURL = request.getRequestURL().toString() ;
                        //reqURL = reqURL.replace("eprocure.gov.bd", "egpdev");
                        String reqQuery = request.getQueryString() ;
                        String folderName = pdfConstant.TENDERNOTICE;
                         String genId = id;
                         if(tenderStatus.equalsIgnoreCase("Approved")){
  %>
                <a class="action-button-savepdf"
                href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                <% } %>
                </div>
                <br/><br/>
                <% if(action){%>
                <form name="frm_notice" id="frm_notice" action="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>&msgPublish=msgPublish" method="post">
                </form>
                <div class="t-align-center">
                <label class="formBtn_1">
                    <input name="Cancel" id="Cancel" type="submit" value="Cancel" class="button" />
                </label></div>
                <br/><br/>
                <% }
                if(isShowPrint){
                %>

                <div class="t-align-center">
                <label class="formBtn_1">
                     <input name="print" id="print" type="button" value="Print" class="button" />
                </label></div>
                <% }
                } %>
                <%if (request.getParameter("print") == null) {
                if(! (isPDF.equalsIgnoreCase("true")) ){
                 %>
                <!--jsp:include page="Bottom.jsp"/-->
                <% } } %>
                <!--Dashboard Footer End-->
                <% if(action){%>
                <jsp:include page="../../resources/common/Bottom.jsp" ></jsp:include>
                <%} %>
            </div>
        </div>
                <script type="text/javascript">
                    $(document).ready(function() {

                        $("#print").click(function() {
                            printElem({ leaveOpen: true, printMode: 'popup' });
                        });

                    });
                    function printElem(options){
                        $('#printRem').hide();
                        $('#tdPrintDocument').hide();
                        $('#tdActionDownload').hide();
                        $('#thActionDownload').hide();
                        $('#print_area').printElement(options);
                        $('#printRem').show();
                        $('#tdPrintDocument').show();
                        $('#tdActionDownload').show();
                        $('#thActionDownload').show();
                    }

                </script>
        <%
                    tenderSrBean = null;
                    preTendDtBean = null;
        %>
    </body>
</html>
