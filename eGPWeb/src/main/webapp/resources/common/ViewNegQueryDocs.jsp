<%--
    Document   : NegotiationDocsUpload
    Created on : Dec 24, 2010, 11:45:06 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Reference Documents</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/ddlevelsmenu.js"></script>
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                String negId = "1";
                int tenderId = 0;
                int userType = 0;
                int negQueryId = 0;

                if (request.getParameter("negId") != null) {
                    negId = request.getParameter("negId");
                }
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if (request.getParameter("queryId") != null) {
                    negQueryId = Integer.parseInt(request.getParameter("queryId"));
                }

                session = request.getSession();
                userType = Integer.parseInt(session.getAttribute("userTypeId").toString());

                
            %>
            <div class="contentArea_1">
                <div class="pageHead_1">Reference Documents</div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%

                        int docCnt = 0;
                        TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                        for (SPTenderCommonData sptcd : tenderCS.returndata("NegQryDocs",""+negId, ""+negQueryId)) {
                            docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-left">
                            <a href="<%=request.getContextPath()%>/ServletNegQueryDocs?docName=<%=sptcd.getFieldName1()%>&negId=<%=negId%>&docSize=<%=sptcd.getFieldName3()%>&negQueryId=<%=negQueryId%>&tenderId=<%=tenderId%>&funName=download" title="Download"><img src="../images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <%--<a href="<%=request.getContextPath()%>/ServletNegQueryDocs?&docName=<%=sptcd.getFieldName1()%>&negId=<%=negId%>&docId=<%=sptcd.getFieldName4()%>&negQueryId=<%=negQueryId%>tenderId=<%=tenderId%>&funName=remove"><img src="../images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>--%>
                        </td>
                    </tr>
                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            if (tenderCS != null) {
                tenderCS = null;
            }

%>