<%--
    Document   : TenderListing
    Created on : Dec 2, 2010, 8:04:12 PM
    Author     : Administrator,rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String query = "";
                    if(request.getParameter("keyword")!=null && !"".equals(request.getParameter("keyword"))){
                        query = request.getParameter("keyword");
                    }

                    int t_uId = 0;
                    if (session.getAttribute("userId") != null) {
                        t_uId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                    boolean t_userApproved = false;
                    String t_status = "";
                    TenderCommonService t_tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> t_list = t_tenderCommonService.returndata("getStatusOfUser", t_uId + "", "");
                    if (t_list != null) {
                        if (t_list.size() > 0) {
                            t_status = t_list.get(0).getFieldName1();
                            if ("approved".equalsIgnoreCase(t_list.get(0).getFieldName1())) {
                                t_userApproved = true;
                            }
                        }
                        t_list = null;
                    }
                    String isApprove = request.getParameter("approve");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Search Results</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.tablesorter.js"  type="text/javascript"></script>
        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        
        
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "AllTenders",keyword: $("#keyWord").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),homeWSearch: 'homeWSearch',approve:<%=t_userApproved%>,h:"<%=request.getParameter("h")%>" },  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }

                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                        //chkdisble(pageNo);
                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(totalPages);

                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);

                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo) - 1);

                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                            chkdisble(pageNo);
                        }
                    }
                });
            });

             function doBlink() {
            var blink = document.all.tags("BLINK")
            for (var i=0; i<blink.length; i++)
            blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : ""
            }

            function startBlink() {
            if (document.all)
            setInterval("doBlink()",1000)
            }
            window.onload = startBlink;
        </script>

    <input type="hidden" name="keyWord" id="keyWord" value="<%=query%>"/>
</head>
<body>
    <div class="mainDiv">
        <%
                    ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    Object objUsrTypeId = session.getAttribute("userTypeId");
                    Object objUserId = session.getAttribute("userId");
                    Object objuName = session.getAttribute("userName");
                    Object objApprovedUserID = new Object();
                    boolean isLoggedIn = false;
                    if (objUserId != null) {
                        isLoggedIn = true;
                        objApprovedUserID = cas.getUserApprovedTenderId(objUserId.toString());
                        System.out.println(" obj   "+objApprovedUserID.toString());
                    }%>
            <div class="dashboard_div">
            <%if (isLoggedIn) {
                if(!objApprovedUserID.toString().equalsIgnoreCase("Approved")){
                %>
                    <jsp:include page="Top.jsp" ></jsp:include>
                <%} else {%>
                    <%@include file="AfterLoginTop.jsp" %>
            <%}} else {%>
                <jsp:include page="Top.jsp" ></jsp:include>
            <%}%>
            <div class ="fixDiv">
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->


                            <div>
                                <div class="pageHead_1">Tender Search Results</div>

                                <div id="resultDiv" style="display: none">
                                    <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space">
                                        <tr>
                                            <th width="4%" class="t-align-center">Sl.<br/>No.</th>
                                            <th width="20%" class="t-align-center">Tender ID,<br />
                                                Reference No,<br/>
                                                Public Status
                                            </th>
                                            <th width="22%" class="t-align-center">Procurement Category,
                                                 Title</th>
                                            <th width="25%" class="t-align-center">Hierarchy Node</th>
                                            <th width="10%" class="t-align-center">Type,<br />
                                                Method</th>
                                            <th width="19%" class="t-align-center">Publishing Date & Time | <br />
                                                Closing Date & Time</th>
                                            <%
                                                            /* Changes has been done for issue #5041*/
//                                                            if (objUsrTypeId != null) {
                                            if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && t_userApproved == true) {
                                                %>
                                            <th width="7%" class="t-align-center"><div align="left">Dashboard</div></th>
                                            <%}%>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1 t-align-center">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td align="right" class="prevNext-container"><ul>
                                                    <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <input type="hidden" name="size" id="size" value="10"/>
                                    </div>
                                    <script type="text/javascript">
                                            loadTable();
                                            //chkdisble(1);
                                            if ($.trim($("#keyWord").val()) == '' || $.trim($("#keyWord").val()) == 'Type your Keyword here') {

                                            }else{
                                                $("#txtKeyword").val($("#keyWord").val());
                                            }
                                            chngTab("1");
                                    </script>
                                    <div>&nbsp;</div>
                                </div>
                            </div>
                        </td>
                        
                        <% if (objuName == null) {%>
                        <td width="266">
                            <jsp:include page="Left.jsp" ></jsp:include>
                        </td>
                        <% }%>
                    </tr>
                </table>
                <jsp:include page="Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </div>
</body>
</html>
