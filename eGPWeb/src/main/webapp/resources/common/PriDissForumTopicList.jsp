<%-- 
    Document   : PriDissForumTopicList
    Created on : Mar 14, 2012, 12:18:31 PM
    Author     : dixit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PrivateDissForumServiceImpl"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Private Discussion Forum</title>
        <link href="../../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/js/ddlevelsmenu.js"></script>
        <script src="../../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<%

    String TenderID = "";
    if(request.getParameter("tenderId")!=null)
    {
        TenderID =request.getParameter("tenderId");
    }
    CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
    List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
    Object[] objj = null;
    if(lotObj!=null && !lotObj.isEmpty())
    {
        objj = lotObj.get(0);
        pageContext.setAttribute("lotId", objj[0].toString());
    }
    int userTypeIdd = 0;
    if (session.getAttribute("userTypeId") != null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
        userTypeIdd = Integer.parseInt(session.getAttribute("userTypeId").toString());
    }            
    StringBuffer colHeader = new StringBuffer();
    colHeader.append("'Sl. No.','Topics','New | Total'");

    PrivateDissForumServiceImpl privateDissForumService = (PrivateDissForumServiceImpl) AppContext.getSpringBean("PrivateDissForumService");
    privateDissForumService.setLogUserId(session.getAttribute("userId").toString());
    long totalPost=0;
    totalPost=privateDissForumService.getTotalPostedReply(Integer.parseInt(TenderID));
%>
        <script type="text/javascript">
            /* jquery function for displaying jqgrid  */
            function loadGrid(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    datatype: 'xml',
                    url : '<%=request.getContextPath()%>/PriDissForumServlet?funName=topicListing&tenderId=<%=TenderID%>',
                    height: 225,
                    colNames:[<%=colHeader %>],
                    colModel:[
                        {name:'Sr. No',index:'Sr. No', width:10,sortable: false, search: false, align:'center'},
                        {name:'Topic',index:'topic', width:100, search: true,sortable: true,searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'NewTotal',index:'NewTotal', width:30,sortable: false, search: false,align:'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: " ",
                    searchGrid: {sopt:['cn','bw','eq','ne','lt','gt','ew']},
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            $(document).ready(function(){
                loadGrid();
            });
        </script>

    </head>
    <body>
            <div class="fixDiv">
                <%@include  file="../../resources/common/AfterLoginTop.jsp"%>                
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1 ">Private Discussion Forum
                            <span class="c-alignment-right">
<!--                                <a class="action-button-goback " href="deBriefQuery.jsp?tenderId=<%=TenderID%>" title="Go Back">Go Back</a>-->
                            </span>
                        </div>
                        <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            
                        %>

                        <%@include file="../../resources/common/TenderInfoBar.jsp" %>                         
                        <div>&nbsp;</div>
                        <%--<%@include file="../../resources/common/ContractInfoBar.jsp" %>--%>
                        <div>&nbsp;</div>
                        <%if(userTypeIdd==2){
                            //pageContext.setAttribute("TSCtab", "7");
                            pageContext.setAttribute("tab", "17");
                        %>
                        <%@include file="../../tenderer/TendererTabPanel.jsp" %>
                        <%}else{
                            //pageContext.setAttribute("TSCtab", "8");
                            pageContext.setAttribute("tab", "17");
                        %>
                            <%@include file="../../officer/officerTabPanel.jsp" %>
                        <%}%>
                       
                        <div class="tabPanelArea_1">
                            <%--<%if(userTypeIdd==2){%>
                            <%@include  file="../../tenderer/cmsTab.jsp"%>
                        <%}else{%>
                            <%@include  file="../../resources/common/CMSTab.jsp"%>
                        <%}%>--%>
                        
                        <form method="post" id="frm" action="<%=request.getContextPath()%>/deBriefQueryServlet?tenderId=<%=TenderID%>&action=addQuery">
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td>
                                        <table class="no-border" width="100%">
                                            <tr>
                                            <td>
                                                <%
                                                if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("true"))
                                                {
                                                %>
                                                    <div id="succMsg" class="responseMsg successMsg">Topic Posted Successfully.</div>
                                                
                                                <%
                                                }
                                                if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("false"))
                                                {
                                                %>
                                                         <div id="errMsg" class="responseMsg errorMsg">Some Problem occur during Post Topic.</div>
                                                
                                                <%
                                                }
                                                %>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td  style="text-align: right">
                                                    <a href="PriDissForumPostTopic.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="anchorLink" style="text-decoration: none; color: #fff;">Post New Topic</a>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                            <td  style="text-align: right;" >
                                                <B>Total Post&nbsp; (<%=totalPost%>)</B>
                                            </td>
                                            </tr>
                                        </table>
                                    </td>
                                                    
                                </tr>
                                <tr valign="top" >
                                    <td>
                                        <div id="jQGrid" align="center" >
                                            <div><table id="list"></table></div>
                                            <div id="page"></div>
                                        </div>
                                    <!--Bottom controls-->
                                </td>
                                </tr>
                                </table>
                                <div>&nbsp;</div>                            
                            
                        </form>
                        
                    </div></div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <jsp:include page="../../resources/common/Bottom.jsp" ></jsp:include>
                    <!--Dashboard Footer End-->
                </div>
            </div>
        
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>