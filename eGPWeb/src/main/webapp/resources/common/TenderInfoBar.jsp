<%--
    Document   : TenderInfoBar
    Created on : Nov 16, 2010, 7:01:36 PM
    Author     : Kinjal Shah
    Purpose    : Display commmon info related to tender notice

--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%
         // get tenderid and set in string
         String id = "";
            String tenderProcureNature = "";
         if(pageContext.getAttribute("tenderId")!=null && !"".equals(pageContext.getAttribute("tenderId"))){
             id = pageContext.getAttribute("tenderId").toString();
         }
          if(id == null && request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid")) ){
                id =   request.getParameter("tenderid");
          } else  if(id == null && request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId")) ){
                id =   request.getParameter("tenderId");
          }
         //System.out.println("id "+ id);
        ///if (!request.getParameter("tenderId").equals("")) {
          //  id = request.getParameter("tenderId");
        //}

         String ispdf = "";
         if(pageContext.getAttribute("isPDF")!=null && !"".equals(pageContext.getAttribute("isPDF"))){
             ispdf = pageContext.getAttribute("isPDF").toString();
         }


         String struserid = "";
             if("true".equalsIgnoreCase(ispdf)){
                struserid = pageContext.getAttribute("userId").toString();
            }else{
             if(session.getAttribute("userId") != null){
                struserid = session.getAttribute("userId").toString();
                }
            }

%>

<!--Dashboard Header Start--><!--Dashboard Header End-->
<!--Dashboard Content Part Start-->

<div class="tableHead_1 t_space">Tender Detail</div>
<%
                    // create instance of Service layer. Call proc with required parameters
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CommonSearchDataMoreService secdataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

            if("true".equalsIgnoreCase(ispdf)){
                tenderCommonService.setLogUserId(struserid);
            }else{
                if (session.getAttribute("userId") != null) {
                      tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                }
            }


            boolean isTenPackageWise = false, isThisTenderApproved = false;
            String packageDescription = "", currTenderOpeningDt = "";
            String toextTenderRefNo = null;

            java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("getBankInfo", id, null);

            for (SPTenderCommonData sptcd : tenderCommonService.returndata("tenderinfobar", id, null)) {
                currTenderOpeningDt = sptcd.getFieldName9();
                             toextTenderRefNo = sptcd.getFieldName2();
                if ("Package".equalsIgnoreCase(sptcd.getFieldName7())) {
                                isTenPackageWise = true;
                                packageDescription = sptcd.getFieldName8();
                             }

                if ("approved".equalsIgnoreCase(sptcd.getFieldName10())) {
                                isThisTenderApproved = true;
                             }
                String tendinfoSubDate = "";
                tendinfoSubDate = sptcd.getFieldName4();
                pageContext.setAttribute("isThisTenderApproved", isThisTenderApproved);
                pageContext.setAttribute("isTenPackageWise", isTenPackageWise);
                pageContext.setAttribute("tenderRefNo", toextTenderRefNo);
                pageContext.setAttribute("tendinfoSubDate", tendinfoSubDate);
                // start binding logic
%>
<table width="100%" cellspacing="10" class="tableView_1 infoBarBorder">
    <tr>
      <td width="20%" class="ff">Tender ID :</td>
      <td width="30%"><%=sptcd.getFieldName1()%></td>

      <%
      List<SPTenderCommonData> sptcdMore = tenderCommonService.returndata("tenderinfobarMore", id, null);

      String msgTender = "";
        if (sptcdMore.get(0).getFieldName1().equalsIgnoreCase("PQ") || sptcdMore.get(0).getFieldName1().equalsIgnoreCase("TENDER") || sptcdMore.get(0).getFieldName1().contains("TSTM")) {
            msgTender = "Invitation Reference No. : ";
        } else if (sptcdMore.get(0).getFieldName1().equalsIgnoreCase("REOI")) {
            msgTender = "REOI No. : ";
        } else if (sptcdMore.get(0).getFieldName1().equalsIgnoreCase("RFP")) {
            msgTender = "RFP No. : ";
        }else if (sptcdMore.get(0).getFieldName1().equalsIgnoreCase("RFA")) {
            msgTender = "RFA No. : ";
        }else if (sptcdMore.get(0).getFieldName1().equalsIgnoreCase("RFQ") || sptcdMore.get(0).getFieldName1().equalsIgnoreCase("RFQU") || sptcdMore.get(0).getFieldName1().equalsIgnoreCase("RFQL")) {
            msgTender = "LEM No. : ";
        }
      if(sptcdMore!= null && sptcdMore.get(0)!= null){
          tenderProcureNature=sptcdMore.get(0).getFieldName2();
          
        }
      %>

      <input type="hidden" id="hdnProcureNature" name="hdnProcureNature" value="<%= tenderProcureNature%>"/>
        <td width="20%" class="ff"><%=msgTender %></td>
      <td width="30%"><%=sptcd.getFieldName2()%></td>
    </tr>
    <tr>
      <td class="ff">Closing Date and Time :</td>
      <td><%=sptcd.getFieldName4()%></td>
      <td class="ff">Opening Date and Time : </td>
      <td><%=sptcd.getFieldName3()%></td>
    </tr>
    <tr>
      <td class="ff">Procuring Agency :</td>
      <td><%=sptcd.getFieldName5()%></td>
      <td class="ff">Procurement Category :</td>
      <td><%=tenderProcureNature%></td>
    </tr>
    <tr>
      <td class="ff">Brief :</td>
      <td colspan="3"><%=sptcd.getFieldName6()%></td>
    </tr>
    <tr class="noprint">
        <%  if(!"true".equalsIgnoreCase(ispdf)){
            HttpSession snTIB = request.getSession();
            Object objUserTypeIdTIB = snTIB.getAttribute("userTypeId");
            String userTypeIdForTIB = "";
            String tenderInfoStatus = "";
            if (objUserTypeIdTIB != null) {
                userTypeIdForTIB = objUserTypeIdTIB.toString();
            }

            boolean isPEForTS = false;
           // out.print("request.getParameter(tenderid) "+id +" struserid "+struserid);
            List<SPTenderCommonData> checkPEForTS = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", id, struserid);
            //System.out.println("checkPEForTS "+checkPEForTS.size());
            if (!checkPEForTS.isEmpty() && checkPEForTS.size() > 0 && struserid.equals(checkPEForTS.get(0).getFieldName1())) {
                isPEForTS = true;
            }
           // out.print("isPEForTS "+isPEForTS);
            boolean isCommitteeMem = false;
            List<SPTenderCommonData> checkCommitteeMem = tenderCommonService.returndata("checkEvaluationCommMem", id, struserid);
            if (!checkCommitteeMem.isEmpty() && "Yes".equalsIgnoreCase(checkCommitteeMem.get(0).getFieldName1())) {
                isCommitteeMem = true;
            }
            boolean isHOPEForTS = false;
            List<SPTenderCommonData> checkHOPEForTS = tenderCommonService.returndata("checkHopeFromTenderId", id, "HOPE");
            if (!checkHOPEForTS.isEmpty() && struserid.equals(checkHOPEForTS.get(0).getFieldName1())) {
                isHOPEForTS = true;
            }
                        boolean isCurUserSecretory = false;
                        List<SPCommonSearchDataMore> lstChkUserType =
                                secdataMore.geteGPData("getTenderSecratory", id, struserid, "Secretary", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if (!lstChkUserType.isEmpty()) {
                            if (struserid.equalsIgnoreCase(lstChkUserType.get(0).getFieldName1())) {
                                isCurUserSecretory = true;
                            }
                        }
                        lstChkUserType = null;
                        boolean isAAMinister = false;
                        lstChkUserType = secdataMore.geteGPData("getTenderAAMinister", id, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if (lstChkUserType != null && !lstChkUserType.isEmpty() && struserid.equalsIgnoreCase(lstChkUserType.get(0).getFieldName2()) ) {
                            isAAMinister = true;
                        }
            //System.out.println("Hopeee "+struserid+isHOPEForTS+checkHOPEForTS.get(0).getFieldName1());
            if("2".equalsIgnoreCase(userTypeIdForTIB)){
                List<SPTenderCommonData> sptcdMoreTIB = tenderCommonService.returndata("tenderStatusForTenderer", id, null);
                if(!sptcdMoreTIB.isEmpty() && sptcdMoreTIB.get(0).getFieldName1() != null && !"".equals(sptcdMoreTIB.get(0).getFieldName1())){
                    tenderInfoStatus = sptcdMoreTIB.get(0).getFieldName1();
                }
              //  System.out.println("TTTTTTTTTTTTT 1");
                        } else if (isPEForTS || isHOPEForTS || isCurUserSecretory || isAAMinister) {
                List<SPTenderCommonData> sptcdMorePEHOPE = tenderCommonService.returndata("tenderStatusForPEHOPE", id, null);
               // System.out.println("isPEForTS "+sptcdMorePEHOPE.size());
                //out.print("test 1234 "+sptcdMorePEHOPE.get(0).getFieldName1());
                 if(!sptcdMorePEHOPE.isEmpty() && sptcdMorePEHOPE.get(0).getFieldName1() != null && !"".equals(sptcdMorePEHOPE.get(0).getFieldName1())){
                    tenderInfoStatus=sptcdMorePEHOPE.get(0).getFieldName1();
                }
                //System.out.println("TTTTTTTTTTTTT 2");
            }else if(isCommitteeMem){
                List<SPTenderCommonData> sptcdMoreCM = tenderCommonService.returndata("tenderStatusForCM", id, null);
                if(!sptcdMoreCM.isEmpty() && sptcdMoreCM.get(0).getFieldName1() != null && !"".equals(sptcdMoreCM.get(0).getFieldName1())){
                    tenderInfoStatus=sptcdMoreCM.get(0).getFieldName1();
                }
                //System.out.println("TTTTTTTTTTTTT 3");
            }
                        if (!"".equals(tenderInfoStatus.trim())) {
                    out.print("<td class=\"ff\">");
                    out.print("Tender Status :");
                    out.print("</td>");
                    out.print("<td>");
                    out.print("<label style=\"color: red;font-weight: bold;\">"+tenderInfoStatus+"</label>");
                    out.print("</td>");
             }
%>
        <% //if ("Cancelled".equals(sptcd.getFieldName10())) {%>
<!--        <td style="color: red;" ><b><-%=sptcd.getFieldName10()%></b></td>
        <td class="t-align-right" colspan="3"><a class="action-button-viewTender" onclick="javascript:window.open('<-%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<-%=id%>', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View Notice</a></td>-->
        <% //}else {%>
        <td class="t-align-right" colspan="4"><a class="action-button-viewTender" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<%=id%>', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View Notice</a></td>
        <% //} %>
        <% } %>
    </tr>
</table>
<%}%>

<div>&nbsp;</div>
<%
    String tIdForMsg = "";
    if(session.getAttribute("TenderIdForResponse") != null)
    {
        tIdForMsg = session.getAttribute("TenderIdForResponse").toString();
    }
    if(tIdForMsg!=null && !tIdForMsg.equals(""))
    {
        List<SPTenderCommonData> lstOfficerUserId2 = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tIdForMsg, null);
        List<SPTenderCommonData> creatorRole2 = tenderCommonService.returndata("getCreatorRole", lstOfficerUserId2.get(0).getFieldName1().toString(),null);
        String paAu = "";
        if(creatorRole2.get(0).getFieldName1().equals("1"))
        {
            paAu = "PA";
        }
        else if(creatorRole2.get(0).getFieldName1().equals("5"))
        {
            paAu = "AU";
        }
%>
<div class='responseMsg noticeMsg' style="color: red;"><b>Tender responsibility: <%=lstOfficerUserId2.get(0).getFieldName5()%> (<%=paAu%>)</b></div>
<%}%>
            
