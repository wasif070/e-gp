<%@page import="com.cptu.egp.eps.model.table.TblToDoList"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
   <body>
   <%
    
         int uid = 0;
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
       }


   // here i have added static uid if it has in session then
   // delete below line and enable above two lines.
     //int uid = 1;
     MessageProcessSrBean mps = new MessageProcessSrBean();
     mps.setLogUserId(""+uid);

     /**********/
     int archiveFolderId = mps.createDefaultArchiveFolder(uid);
    /***********/

     List folderData = mps.getFolderDataByUserId(uid);
     request.setAttribute("fdata", folderData);

     List<TblToDoList> toDoList = mps.getToDoList(uid);
     long totalrecievedmsgs = 0;
     long totalunreadmsgs = 0;
     totalrecievedmsgs =  mps.getTotalRecievedMsgs("TblMessageInBox", "msgToUserId ="+uid+" and msgStatus='Live' and folderId= 0 AND msgStatus<>'trash'");
     totalunreadmsgs = mps.getTotalUnreadMsgs("TblMessageInBox", "msgToUserId ="+uid+" and isMsgRead='No' and msgStatus='Live' and folderId= 0 and msgStatus<>'trash'");
%>
<table width="100%" cellspacing="0" >
    <tr valign="top">
      <td class="lftNav"><ul class="lftLinkBtn_1 BorderRight">
          <li><a href="ViewTaskDetails.jsp" title="To Do list"><img src="../images/Dashboard/toDoListIcn.png" alt='To Do list' />To Do list</a></li>
          <li><a href="AddFolder.jsp" title="Add Folder"><img src="../images/Dashboard/addIcn.png" alt='add Folder' />Add Folder</a></li>
    	  <li><a href="ComposeMail.jsp?messageBoxType=Inbox" title="Compose Message"><img src="../images/Dashboard/composeMsg.png" alt='Compose Message' />Compose Message</a></li>
          </ul>
        <div class="t_space sublinks_1">
          <ul class="BorderRight">
            <li ><a href="InboxMessage.jsp?inbox=Inbox&folderid=0" name="Inbox" id="Inbox">Inbox(<%=totalrecievedmsgs%>)</a></li>
            <li ><a href="InboxMessage.jsp?inbox=InboxUnRead&folderid=0" name="Inbox" id="Inbox">Unread(<%=totalunreadmsgs%>)</a></li>
            <li><a href="InboxMessage.jsp?inbox=Sent&folderid=0">Sent</a></li>
            <li><a href="InboxMessage.jsp?inbox=Trash&folderid=0">Trash</a></li>
            <li><a href="InboxMessage.jsp?inbox=Draft&folderid=0">Draft</a>
            <li><a href="InboxMessage.jsp?inbox=Folder&folderid=<%=archiveFolderId%>&foldername=Archive">Archive</a></li>
            <c:forEach items="${fdata}" var="fd" varStatus = "status">
                <c:if test="${!fn:containsIgnoreCase(fd.folderName,'Archive')}">
                    <li><a href="InboxMessage.jsp?inbox=Folder&folderid=${fd.msgFolderId}&foldername=${fd.folderName}"> <c:out value="${fd.folderName}" /></a></li>
                </c:if>
            </c:forEach>
            <!-- <li><a href="#">My Xyz Messages</a>
              <div class="lIcn"><img src="../images/Dashboard/removeIcn.png" title="Delete Folder" onclick="" /></div>
            </li> -->
          </ul>
        </div>
          
      <table class="tableList_1 t_space" width="100%">
          <thead>
          <th style="text-align: left;">Today's To Do List</th>
          </thead>
              
              <%
              for(int i=0;i<toDoList.size(); i++){
                  %>
                  <tr><td>
                  <a href="<%=request.getContextPath()%>/resources/common/ViewTask.jsp?taskid=<%=toDoList.get(i).getTaskId()%>&select=Pending">
                  <%=toDoList.get(i).getTaskBrief()%></a>
                  </td></tr>
                  <%
              }
              %>
      </table>
        </td>
            </tr>
          
        </table>
</body>