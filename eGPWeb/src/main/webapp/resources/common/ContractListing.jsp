<%-- 
    Document   : ContractListing
    Created on : Apr 8, 2018, 4:26:11 PM
    Author     : Aprojit
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.SearchNOAService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SearchNOAData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    // Get the keyword
                    String keyword = "";
                    if(request.getParameter("keyword")!=null && !"".equals(request.getParameter("keyword"))){
                        keyword = request.getParameter("keyword");
                    }

                    StringBuilder mode = new StringBuilder();

                    String strUserTypeId = "";
                    Object objUserId = session.getAttribute("userId");
                    Object objUName = session.getAttribute("userName");
                    boolean isLoggedIn = false;
                    ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    Object objApprovedUserID = new Object();
            
                    if (objUserId != null) {
                        strUserTypeId = session.getAttribute("userTypeId").toString();
                        objApprovedUserID = cas.getUserApprovedTenderId(objUserId.toString());
                    }
                    if (objUName != null) {
                        isLoggedIn = true;
                    }
                    if (isLoggedIn) {
                        mode.append("WatchList");
                    } else {
                        mode.append("Search");
                    }
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("hdnUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                            userType.append(request.getParameter("hdnUserType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Search Results</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
           
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            { 
                $.post("<%=request.getContextPath()%>/SearchNoaServlet", {keyword: $("#keyword").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    // Dynamically shortened text with “Read More” Link
                    var showChar = 25;  // How many characters are shown by default
                    var ellipsestext = "...";
                    var moretext = "more";
                    var lesstext = "less";

                    $('.more').each(function() {
                        var content = $(this).html();

                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);
                            var html = c + '<span class="moreellipses">' + ellipsestext+ '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        }
                        else
                            {
                                 var c = content+'</br>';
                                 var html = c;
                                 $(this).html(html);
                            }

                    });

                    $(".morelink").click(function(){
                        if($(this).hasClass("less")) {
                            $(this).removeClass("less");
                            $(this).html(moretext);
                        } else {
                            $(this).addClass("less");
                            $(this).html(lesstext);
                        }
                        $(this).parent().prev().toggle();
                        $(this).prev().toggle();
                        return false;
                    });

                }); 
                          
            }
        </script>
                  


                  
        <link href="../js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
    </head>
    <body >
        <div class="mainDiv">
            <!--While the user is logged in and clicks on the "Contracts" tab it was redirecting to advanced menu -->
            <%if (isLoggedIn) {
                if(!objApprovedUserID.toString().equalsIgnoreCase("Approved")){
                %>
                    <div class="fixDiv">
                    <jsp:include page="Top.jsp" ></jsp:include>
                <%} else {%>
                    <div class="dashboard_div">
                    <%@include file="AfterLoginTop.jsp" %>
            <%}} else {%>
                <div class="fixDiv">
                <jsp:include page="Top.jsp" ></jsp:include>
            <%}%>
            
            <!--Middle Content Table Start-->
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea-Blogin">
                                <!--Page Content Start-->

                                <div class="t_space">
                                    <div class="pageHead_1">Contract Search Results</div>
                                    <div>&nbsp;</div>
                                    <div id="resultDiv">
                                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                                            <tr>
                                            <th width="3%" class="t-align-center">Sl. No.</th>
                                            <th width="15%" class="t-align-center">Hierarchy Node<!--,<br />Agency--></th>
                                            <th width="30%" class="t-align-center">Tender ID, Ref No., Title &<br/> Advertisement Date</th>
                                            <th width="15%" class="t-align-center">Procuring Agency<br/>Procurement Method</th>
<!--                                            <th width="10%" class="t-align-center">Dzongkhag / District</th>-->
                                            <th width="10%" class="t-align-center">Date of<br/>Notification<br/>of Award</th>
                                            <th width="10%" class="t-align-center">Contract<br/>Awarded to</th>
                                            <th width="7%" class="t-align-center">Value<br/>(Million Nu.)</th>
                                            </tr>
                                        </table>

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                            <tr>
                                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                                <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                    &nbsp;
                                                    <label class="formBtn_1">
                                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                    </label></td>
                                                <td  class="prevNext-container"><ul>
                                                        <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                        <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                    </ul></td>
                                            </tr>
                                        </table>
                                        <div align="center">
                                            <input type="hidden" id="pageNo" value="1"/>
                                            <input type="hidden" name="size" id="size" value="10"/>
                                            <input type="hidden" name="keyword" id="keyword" value="<%=keyword%>"/>
                                        </div>
                                    </div>
                                    <!--                                <script type="text/javascript">
                                                                        chngTab("3");
                                                                        //loadTable();
                                                                    </script>-->
                                    <script type="text/javascript">
                                        loadTable();
                                        chngTab("3");
                                        if ($.trim($("#keyword").val()) == '' || $.trim($("#keyword").val()) == 'Type your Keyword here') {

                                        }else{
                                            $("#txtKeyword").val($("#keyword").val());
                                        }
                                    </script>

                                    <div>&nbsp;</div>
                                </div>
                            </td>
                            <% if (objUName == null) {%>
                            <td width="250">
                                <jsp:include page="Left.jsp" ></jsp:include>
                            </td>
                            <% }%>
                        </tr>
                    </table>
                    <!--Middle Content Table End-->
                    <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </div>
    </body>
</html>
