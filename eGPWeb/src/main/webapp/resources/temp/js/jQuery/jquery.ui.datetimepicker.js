//(function(d){function E(){this.debug=false;this._nextId=0;this._inst=[];this._curInst=null;this._disabledInputs=[];this._inDialog=this._datetimepickerShowing=false;this.regional=[];this.regional[""]={clearText:"Clear",clearStatus:"Erase the current date",closeText:"Close",closeStatus:"Close without change",prevText:"&#x3c;Prev",prevStatus:"Show the previous month",nextText:"Next&#x3e;",nextStatus:"Show the next month",currentText:"Today",currentStatus:"Show the current month",monthNames:["January",  "February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],monthStatus:"Show a different month",yearStatus:"Show a different year",weekHeader:"Wk",weekStatus:"Week of the year",dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],dayStatus:"Set DD as first week day",  dateStatus:"Select DD, M d",dateFormats:["yy-mm-dd","mm/dd/yy"],timeFormat:"hh:ii",firstDay:0,initStatus:"Select a date",isRTL:false};this._defaults={pickDateOnly:false,showOn:"focus",showAnim:"show",defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:false,closeAtTop:true,mandatory:false,hideIfNoPrevNext:false,changeMonth:true,changeYear:true,yearRange:"-10:+10",changeFirstDay:true,showOtherMonths:false,showWeeks:false,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",  showStatus:false,statusForDate:this.dateStatus,minDate:null,maxDate:null,speed:"normal",beforeShowDay:null,beforeShow:null,onSelect:null,onClose:null,numberOfMonths:1,stepMonths:1,rangeSelect:false,rangeSeparator:" - ",dateFormat:"yy-mm-dd"};d.extend(this._defaults,this.regional[""]);this._datetimepickerDiv=d('<div id="datetimepicker_div"></div>')}function C(a,b){this._id=d.datetimepicker._register(this);this._drawMinute=this._drawHour=this._drawYear=this._drawMonth=this._selectedYear=this._selectedMonth=  this._selectedDay=0;this._input=null;this._inline=b;this._datetimepickerDiv=!b?d.datetimepicker._datetimepickerDiv:d('<div id="datetimepicker_div_'+this._id+'" class="datetimepicker_inline">');this._settings=B(a||{});b&&this._setDate(this._getDefaultDate())}function B(a,b){d.extend(a,b);for(var c in b)if(b[c]==null)a[c]=null;return a}d.extend(E.prototype,{markerClassName:"hasDatepicker",log:function(){this.debug&&console.log.apply("",arguments)},_register:function(a){var b=this._nextId++;this._inst[b]=  a;return b},_getInst:function(a){return this._inst[a]||a},setDefaults:function(a){B(this._defaults,a||{});return this},_attachDatepicker:function(a,b){var c=null;for(attrName in this._defaults){var e=a.getAttribute("date:"+attrName);if(e){c=c||{};try{c[attrName]=eval(e)}catch(f){c[attrName]=e}}}e=a.nodeName.toLowerCase();b=c?d.extend(b||{},c||{}):b;if(e=="input"){var i=i&&!c?i:new C(b,false);this._connectDatepicker(a,i)}else if(e=="div"||e=="span"){i=new C(b,true);this._inlineDatepicker(a,i)}},_destroyDatepicker:function(a){var b=  a.nodeName.toLowerCase(),c=a._calId;a._calId=null;a=d(a);if(b=="input"){a.siblings(".datetimepicker_append").replaceWith("").end().siblings(".datetimepicker_trigger").replaceWith("").end().removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress);(b=a.parents(".datetimepicker_wrap"))&&b.replaceWith(b.html())}else if(b=="div"||b=="span")a.removeClass(this.markerClassName).empty();if(d("input[_calId="+c+"]").length==  0)this._inst[c]=null},_enableDatepicker:function(a){a.disabled=false;d(a).siblings("button.datetimepicker_trigger").each(function(){this.disabled=false}).end().siblings("img.datetimepicker_trigger").css({opacity:"1.0",cursor:""});this._disabledInputs=d.map(this._disabledInputs,function(b){return b==a?null:b})},_disableDatepicker:function(a){a.disabled=true;d(a).siblings("button.datetimepicker_trigger").each(function(){this.disabled=true}).end().siblings("img.datetimepicker_trigger").css({opacity:"0.5",  cursor:"default"});this._disabledInputs=d.map(d.datetimepicker._disabledInputs,function(b){return b==a?null:b});this._disabledInputs[d.datetimepicker._disabledInputs.length]=a},_isDisabledDatepicker:function(a){if(!a)return false;for(var b=0;b<this._disabledInputs.length;b++)if(this._disabledInputs[b]==a)return true;return false},_changeDatepicker:function(a,b,c){var e=b||{};if(typeof b=="string"){e={};e[b]=c}if(inst=this._getInst(a._calId)){B(inst._settings,e);this._updateDatepicker(inst)}},_setDateDatepicker:function(a,  b,c){if(inst=this._getInst(a._calId)){inst._setDate(b,c);this._updateDatepicker(inst)}},_getDateDatepicker:function(a){return(a=this._getInst(a._calId))?a._getDate():null},_doKeyDown:function(a){var b=d.datetimepicker._getInst(this._calId);if(d.datetimepicker._datetimepickerShowing)switch(a.keyCode){case 9:d.datetimepicker._hideDatepicker(null,"");break;case 13:d.datetimepicker._selectDay(b,b._selectedMonth,b._selectedYear,d("td.datetimepicker_daysCellOver",b._datetimepickerDiv)[0]);return false;  case 27:d.datetimepicker._hideDatepicker(null,b._get("speed"));break;case 33:d.datetimepicker._adjustDate(b,a.ctrlKey?-1:-b._get("stepMonths"),a.ctrlKey?"Y":"M");break;case 34:d.datetimepicker._adjustDate(b,a.ctrlKey?+1:+b._get("stepMonths"),a.ctrlKey?"Y":"M");break;case 35:a.ctrlKey&&d.datetimepicker._clearDate(b);break;case 36:a.ctrlKey&&d.datetimepicker._gotoToday(b);break;case 37:a.ctrlKey&&d.datetimepicker._adjustDate(b,-1,"D");break;case 38:a.ctrlKey&&d.datetimepicker._adjustDate(b,-7,"D");  break;case 39:a.ctrlKey&&d.datetimepicker._adjustDate(b,+1,"D");break;case 40:a.ctrlKey&&d.datetimepicker._adjustDate(b,+7,"D");break}else a.keyCode==36&&a.ctrlKey&&d.datetimepicker._showDatepicker(this)},_doKeyPress:function(a){var b=d.datetimepicker._getInst(this._calId),c=" "+b._get("timeFormat");b=b._get("dateFormats");for(var e=0;e<b.length;e++)c+=b[e];c=d.datetimepicker._possibleChars(c);b=String.fromCharCode(a.charCode==undefined?a.keyCode:a.charCode);return a.ctrlKey||b<" "||!c||c.indexOf(b)>  -1},_connectDatepicker:function(a,b){var c=d(a);if(!c.is("."+this.markerClassName)){var e=b._get("appendText"),f=b._get("isRTL");if(e)f?c.before('<span class="datetimepicker_append">'+e):c.after('<span class="datetimepicker_append">'+e);e=b._get("showOn");if(e=="focus"||e=="both")c.focus(this._showDatepicker);if(e=="button"||e=="both"){c.wrap('<span class="datetimepicker_wrap">');e=b._get("buttonText");var i=b._get("buttonImage");e=d(b._get("buttonImageOnly")?d("<img>").addClass("datetimepicker_trigger").attr({src:i,  alt:e,title:e}):d("<button>").addClass("datetimepicker_trigger").attr({type:"button"}).html(i!=""?d("<img>").attr({src:i,alt:e,title:e}):e));f?c.before(e):c.after(e);e.click(function(){d.datetimepicker._datetimepickerShowing&&d.datetimepicker._lastInput==a?d.datetimepicker._hideDatepicker():d.datetimepicker._showDatepicker(a)})}c.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).bind("setData.datetimepicker",function(g,k,h){b._settings[k]=h}).bind("getData.datetimepicker",  function(g,k){return b._get(k)});c[0]._calId=b._id}},_inlineDatepicker:function(a,b){a=d(a);if(!a.is("."+this.markerClassName)){a.addClass(this.markerClassName).append(b._datetimepickerDiv).bind("setData.datetimepicker",function(c,e,f){b._settings[e]=f}).bind("getData.datetimepicker",function(c,e){return b._get(e)});a[0]._calId=b._id;this._updateDatepicker(b)}},_inlineShow:function(a){var b=a._getNumberOfMonths();a._datetimepickerDiv.width(b[1]*d(".datetimepicker",a._datetimepickerDiv[0]).width())},  _dialogDatepicker:function(a,b,c,e,f){a=this._dialogInst;if(!a){a=this._dialogInst=new C({},false);this._dialogInput=d('<input type="text" size="1" style="position: absolute; top: -100px;"/>');this._dialogInput.keydown(this._doKeyDown);d("body").append(this._dialogInput);this._dialogInput[0]._calId=a._id}B(a._settings,e||{});this._dialogInput.val(b);this._pos=f?f.length?f:[f.pageX,f.pageY]:null;if(!this._pos)this._pos=[(window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth)/  2-100+(document.documentElement.scrollLeft||document.body.scrollLeft),(window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight)/2-150+(document.documentElement.scrollTop||document.body.scrollTop)];this._dialogInput.css("left",this._pos[0]+"px").css("top",this._pos[1]+"px");a._settings.onSelect=c;this._inDialog=true;this._datetimepickerDiv.addClass("datetimepicker_dialog");this._showDatepicker(this._dialogInput[0]);d.blockUI&&d.blockUI(this._datetimepickerDiv);return this},  _showDatepicker:function(a){a=a.target||a;if(a.nodeName.toLowerCase()!="input")a=d("input",a.parentNode)[0];if(!(d.datetimepicker._isDisabledDatepicker(a)||d.datetimepicker._lastInput==a)){var b=d.datetimepicker._getInst(a._calId),c=b._get("beforeShow");B(b._settings,c?c.apply(a,[a,b]):{});d.datetimepicker._hideDatepicker(null,"");d.datetimepicker._lastInput=a;b._setDateFromField(a);if(d.datetimepicker._inDialog)a.value="";if(!d.datetimepicker._pos){d.datetimepicker._pos=d.datetimepicker._findPos(a);  d.datetimepicker._pos[1]+=a.offsetHeight}var e=false;d(a).parents().each(function(){e|=d(this).css("position")=="fixed"});if(e&&d.browser.opera){d.datetimepicker._pos[0]-=document.documentElement.scrollLeft;d.datetimepicker._pos[1]-=document.documentElement.scrollTop}b._datetimepickerDiv.css("position",d.datetimepicker._inDialog&&d.blockUI?"static":e?"fixed":"absolute").css({left:d.datetimepicker._pos[0]+"px",top:d.datetimepicker._pos[1]+"px"});d.datetimepicker._pos=null;b._rangeStart=null;d.datetimepicker._updateDatepicker(b);  if(!b._inline){a=b._get("speed");c=function(){d.datetimepicker._datetimepickerShowing=true;d.datetimepicker._afterShow(b)};var f=b._get("showAnim")||"show";b._datetimepickerDiv[f](a,c);a==""&&c();b._input[0].type!="hidden"&&b._input[0].focus();d.datetimepicker._curInst=b}}},_updateDatepicker:function(a){a._datetimepickerDiv.empty().append(a._generateDatepicker());var b=a._getNumberOfMonths();b[0]!=1||b[1]!=1?a._datetimepickerDiv.addClass("datetimepicker_multi"):a._datetimepickerDiv.removeClass("datetimepicker_multi");  a._get("isRTL")?a._datetimepickerDiv.addClass("datetimepicker_rtl"):a._datetimepickerDiv.removeClass("datetimepicker_rtl");a._input&&a._input[0].type!="hidden"&&d(a._input[0]).focus()},_afterShow:function(a){var b=a._getNumberOfMonths();a._datetimepickerDiv.width(b[1]*d(".datetimepicker",a._datetimepickerDiv[0])[0].offsetWidth);d.browser.msie&&parseInt(d.browser.version)<7&&d("iframe.datetimepicker_cover").css({width:a._datetimepickerDiv.width()+4,height:a._datetimepickerDiv.height()+4});b=a._datetimepickerDiv.css("position")==  "fixed";var c=a._input?d.datetimepicker._findPos(a._input[0]):null,e=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,f=window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight,i=b?0:document.documentElement.scrollLeft||document.body.scrollLeft,g=b?0:document.documentElement.scrollTop||document.body.scrollTop;if(a._datetimepickerDiv.offset().left+a._datetimepickerDiv.width()-(b&&d.browser.msie?document.documentElement.scrollLeft:0)>e+  i)a._datetimepickerDiv.css("left",Math.max(i,c[0]+(a._input?d(a._input[0]).width():null)-a._datetimepickerDiv.width()-(b&&d.browser.opera?document.documentElement.scrollLeft:0))+"px");if(a._datetimepickerDiv.offset().top+a._datetimepickerDiv.height()-(b&&d.browser.msie?document.documentElement.scrollTop:0)>f+g)a._datetimepickerDiv.css("top",Math.max(g,c[1]-(this._inDialog?0:a._datetimepickerDiv.height())-(b&&d.browser.opera?document.documentElement.scrollTop:0))+"px")},_findPos:function(a){for(;a&&  (a.type=="hidden"||a.nodeType!=1);)a=a.nextSibling;a=d(a).offset();return[a.left,a.top]},_hideDatepicker:function(a,b){var c=this._curInst;if(c){c._get("rangeSelect")&&this._stayOpen&&this._selectDate(c,c._formatDateTime(c._currentDay,c._currentMonth,c._currentYear,c._currentHour,c.currentMinute));this._stayOpen=false;if(this._datetimepickerShowing){b=b!=null?b:c._get("speed");a=c._get("showAnim");c._datetimepickerDiv[a=="slideDown"?"slideUp":a=="fadeIn"?"fadeOut":"hide"](b,function(){d.datetimepicker._tidyDialog(c)});  b==""&&this._tidyDialog(c);if(b=c._get("onClose"))b.apply(c._input?c._input[0]:null,[c._getDate(),c]);this._datetimepickerShowing=false;this._lastInput=null;c._settings.prompt=null;if(this._inDialog){this._dialogInput.css({position:"absolute",left:"0",top:"-100px"});if(d.blockUI){d.unblockUI();d("body").append(this._datetimepickerDiv)}}this._inDialog=false}this._curInst=null}},_tidyDialog:function(a){a._datetimepickerDiv.removeClass("datetimepicker_dialog").unbind(".datetimepicker");d(".datetimepicker_prompt",  a._datetimepickerDiv).remove()},_checkExternalClick:function(a){if(d.datetimepicker._curInst){a=d(a.target);a.parents("#datetimepicker_div").length==0&&a.attr("class")!="datetimepicker_trigger"&&d.datetimepicker._datetimepickerShowing&&!(d.datetimepicker._inDialog&&d.blockUI)&&d.datetimepicker._hideDatepicker(null,"")}},_adjustDate:function(a,b,c){a=this._getInst(a);a._adjustDate(b,c);this._updateDatepicker(a)},_gotoToday:function(a){var b=new Date;a=this._getInst(a);a._selectedDay=b.getDate();a._drawMonth=  a._selectedMonth=b.getMonth();a._drawYear=a._selectedYear=b.getFullYear();a._drawHour=a._selectedHour=b.getHours();a._drawMinute=a._selectedMinute=b.getMinutes();this._adjustDate(a)},_selectMonthYear:function(a,b,c){var e=this._getInst(a);e._selectingMonthYear=false;e[c=="M"?"_drawMonth":"_drawYear"]=b.options[b.selectedIndex].value-0;this._adjustDate(e);this._doNotHide=true;d("td.datetimepicker_currentDay").each(function(){d.datetimepicker._selectDay(e,e._selectedMonth,e._selectedYear,d(this))});  this._doNotHide=false},_selectTime:function(a,b,c){var e=this._getInst(a);e._selectingMonthYear=false;e[c=="M"?"_drawMinute":"_drawHour"]=b.options[b.selectedIndex].value-0;this._adjustDate(e);this._doNotHide=true;d("td.datetimepicker_currentDay").each(function(){d.datetimepicker._selectDay(e,e._selectedMonth,e._selectedYear,d(this))});this._doNotHide=false},_clickMonthYear:function(a){a=this._getInst(a);a._input&&a._selectingMonthYear&&!d.browser.msie&&a._input[0].focus();a._selectingMonthYear=!a._selectingMonthYear},  _clickTime:function(a){a=this._getInst(a);a._input&&a._selectingTime&&!d.browser.msie&&a._input[0].focus();a._selectingTime=!a._selectingTime},_changeFirstDay:function(a,b){a=this._getInst(a);a._settings.firstDay=b;this._updateDatepicker(a)},_selectDay:function(a,b,c,e){if(!d(e).is(".datetimepicker_unselectable")){var f=this._getInst(a),i=f._get("rangeSelect");if(i){if(!this._stayOpen){d(".datetimepicker td").removeClass("datetimepicker_currentDay");d(e).addClass("datetimepicker_currentDay")}this._stayOpen=  !this._stayOpen}f._selectedDay=f._currentDay=d("a",e).html();f._selectedMonth=f._currentMonth=b;f._selectedYear=f._currentYear=c;f._currentHour=d("select.datetimepicker_newHour option:selected").val();if(f._currentHour==undefined)f._currentHour=0;f._selectedHour=f._currentHour;f._currentMinute=d("select.datetimepicker_newMinute option:selected").val();if(f._currentMinute==undefined)f._currentMinute=0;f._selectedMinute=f._currentMinute;this._selectDate(a,f._formatDateTime(f._currentDay,f._currentMonth,  f._currentYear,f._currentHour,f._currentMinute));if(this._stayOpen){f._endDay=f._endMonth=f._endYear=null;f._rangeStart=new Date(f._currentYear,f._currentMonth,f._currentDay);this._updateDatepicker(f)}else if(i){f._endDay=f._currentDay;f._endMonth=f._currentMonth;f._endYear=f._currentYear;f._selectedDay=f._currentDay=f._rangeStart.getDate();f._selectedMonth=f._currentMonth=f._rangeStart.getMonth();f._selectedYear=f._currentYear=f._rangeStart.getFullYear();f._rangeStart=null;f._inline&&this._updateDatepicker(f)}}},  _clearDate:function(a){a=this._getInst(a);if(!a._get("mandatory")){this._stayOpen=false;a._endDay=a._endMonth=a._endYear=a._rangeStart=null;this._selectDate(a,"")}},_selectDate:function(a,b){a=this._getInst(a);b=b!=null?b:a._formatDateTime();if(a._rangeStart)b=a._formatDateTime(a._rangeStart)+a._get("rangeSeparator")+b;a._input&&a._input.val(b);var c=a._get("onSelect");if(c)c.apply(a._input?a._input[0]:null,[b,a]);else a._input&&a._input.trigger("change");if(a._inline)this._updateDatepicker(a);else if(!this._stayOpen)if(!this._doNotHide){this._hideDatepicker(null,  a._get("speed"));this._lastInput=a._input[0];typeof a._input[0]!="object"&&a._input[0].focus();this._lastInput=null}},noWeekends:function(a){a=a.getDay();return[a>0&&a<6,""]},iso8601Week:function(a){a=new Date(a.getFullYear(),a.getMonth(),a.getDate(),a.getTimezoneOffset()/-60);var b=new Date(a.getFullYear(),0,4),c=b.getDay()||7;b.setDate(b.getDate()+1-c);if(c<4&&a<b){a.setDate(a.getDate()-3);return d.datetimepicker.iso8601Week(a)}else if(a>new Date(a.getFullYear(),11,28)){c=(new Date(a.getFullYear()+  1,0,4)).getDay()||7;if(c>4&&(a.getDay()||7)<c-3){a.setDate(a.getDate()+3);return d.datetimepicker.iso8601Week(a)}}return Math.floor((a-b)/864E5/7)+1},dateStatus:function(a,b){return d.datetimepicker.formatDate(b._get("dateStatus"),a,b._getFormatConfig())},parseDate:function(a,b,c){if(a==null||b==null)throw"Invalid arguments";b=typeof b=="object"?b.toString():b+"";if(b=="")return null;for(var e=(c?c.shortYearCutoff:null)||this._defaults.shortYearCutoff,f=(c?c.dayNamesShort:null)||this._defaults.dayNamesShort,  i=(c?c.dayNames:null)||this._defaults.dayNames,g=(c?c.monthNamesShort:null)||this._defaults.monthNamesShort,k=(c?c.monthNames:null)||this._defaults.monthNames,h=c=-1,l=-1,s=0,o=0,j=false,n=function(p){(p=t+1<a.length&&a.charAt(t+1)==p)&&t++;return p},x=function(p){matches=n(p);for(var m=p=="y"&&matches?4:2,q=0;m>0&&r<b.length&&b.charAt(r)>="0"&&b.charAt(r)<="9";){q=q*10+(b.charAt(r++)-"0");m--}if(m==(p=="y"?4:2))throw"Missing number at position "+r;return q},z=function(p,m,q){p=n(p)?q:m;for(q=m=0;q<  p.length;q++)m=Math.max(m,p[q].length);q="";for(var v=r;m>0&&r<b.length;){q+=b.charAt(r++);for(var w=0;w<p.length;w++)if(q==p[w])return w+1;m--}throw"Unknown name at position "+v;},u=function(){if(r<b.length&&b.charAt(r)!=a.charAt(t))throw"Unexpected literal at position "+r;r++},r=0,t=0;t<a.length;t++)if(j)if(a.charAt(t)=="'"&&!n("'"))j=false;else u();else switch(a.charAt(t)){case "h":s=x("h");break;case "i":o=x("i");break;case "d":l=x("d");break;case "D":z("D",f,i);break;case "m":h=x("m");break;  case "M":h=z("M",g,k);break;case "y":c=x("y");break;case "'":if(n("'"))u();else j=true;break;default:u()}if(c<100)c+=(new Date).getFullYear()-(new Date).getFullYear()%100+(c<=e?0:-100);e=new Date(c,h-1,l,s,o);if(e.getFullYear()!=c||e.getMonth()+1!=h||e.getDate()!=l)throw"Invalid date";return e},formatDate:function(a,b,c){if(!b)return"";var e=(c?c.dayNamesShort:null)||this._defaults.dayNamesShort,f=(c?c.dayNames:null)||this._defaults.dayNames,i=(c?c.monthNamesShort:null)||this._defaults.monthNamesShort;  c=(c?c.monthNames:null)||this._defaults.monthNames;var g=function(j){(j=o+1<a.length&&a.charAt(o+1)==j)&&o++;return j},k=function(j,n){return(g(j)&&n<10?"0":"")+n},h=function(j,n,x,z){return g(j)?z[n]:x[n]},l="",s=false;if(b)for(var o=0;o<a.length;o++)if(s)if(a.charAt(o)=="'"&&!g("'"))s=false;else l+=a.charAt(o);else switch(a.charAt(o)){case "h":l+=k("h",b.getHours());break;case "i":l+=k("i",b.getMinutes());break;case "d":l+=k("d",b.getDate());break;case "D":l+=h("D",b.getDay(),e,f);break;case "m":l+=  k("m",b.getMonth()+1);break;case "M":l+=h("M",b.getMonth(),i,c);break;case "y":l+=g("y")?b.getFullYear():(b.getYear()%100<10?"0":"")+b.getYear()%100;break;case "'":if(g("'"))l+="'";else s=true;break;default:l+=a.charAt(o)}return l},_possibleChars:function(a){for(var b="",c=false,e=0;e<a.length;e++)if(c)if(a.charAt(e)=="'"&&!lookAhead("'"))c=false;else b+=a.charAt(e);else switch(a.charAt(e)){case "d":b+="0123456789";break;case "D":return null;case "'":if(lookAhead("'"))b+="'";else c=true;break;default:b+=  a.charAt(e)}return b}});d.extend(C.prototype,{_get:function(a){return this._settings[a]!==undefined?this._settings[a]:d.datetimepicker._defaults[a]},_setDateFromField:function(a){a=(this._input=d(a))?this._input.val().split(this._get("rangeSeparator")):null;this._endDay=this._endMonth=this._endYear=null;var b=defaultDate=this._getDefaultDate();if(a.length>0){var c=this._getFormatConfig();if(a.length>1){var e=this._get("dateFormat")+" "+this._get("timeFormat");b=d.datetimepicker.parseDate(e,a[1],c)||  defaultDate;this._endDay=b.getDate();this._endMonth=b.getMonth();this._endYear=b.getFullYear()}b=null;for(var f=this._get("dateFormats"),i=0;i<f.length;i++){e=f[i]+" "+this._get("timeFormat");try{b=d.datetimepicker.parseDate(e,a[0],c)||defaultDate}catch(g){}if(b!=null){d.datetimepicker._defaults.dateFormat=f[i];break}}if(b==null)b=defaultDate}this._selectedDay=b.getDate();this._drawMonth=this._selectedMonth=b.getMonth();this._drawYear=this._selectedYear=b.getFullYear();this._drawHour=this._selectedHour=  b.getHours();this._drawMinute=this._selectedMinute=b.getMinutes();this._currentDay=a[0]?b.getDate():0;this._currentMonth=a[0]?b.getMonth():0;this._currentYear=a[0]?b.getFullYear():0;this._adjustDate()},_getDefaultDate:function(){var a=this._determineDate("defaultDate",new Date),b=this._getMinMaxDate("min",true),c=this._getMinMaxDate("max");a=b&&a<b?b:a;return a=c&&a>c?c:a},_determineDate:function(a,b){var c=function(f){var i=new Date;i.setDate(i.getDate()+f);return i},e=function(f,i){var g=new Date;  if(f=/^([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?$/.exec(f)){var k=g.getFullYear(),h=g.getMonth();g=g.getDate();switch(f[2]||"d"){case "d":case "D":g+=f[1]-0;break;case "w":case "W":g+=f[1]*7;break;case "m":case "M":h+=f[1]-0;g=Math.min(g,i(k,h));break;case "y":case "Y":k+=f[1]-0;g=Math.min(g,i(k,h));break}g=new Date(k,h,g)}return g};a=this._get(a);return a==null?b:typeof a=="string"?e(a,this._getDaysInMonth):typeof a=="number"?c(a):a},_setDate:function(a,b){this._selectedDay=this._currentDay=a.getDate();  this._drawMonth=this._selectedMonth=this._currentMonth=a.getMonth();this._drawYear=this._selectedYear=this._currentYear=a.getFullYear();this._drawHour=this._selectedHour=this._currentHour=a.getHours();this._drawMinute=this._selectedMinute=this._currentMinute=a.getMinutes();if(this._get("rangeSelect"))if(b){this._endDay=b.getDate();this._endMonth=b.getMonth();this._endYear=b.getFullYear()}else{this._endDay=this._currentDay;this._endMonth=this._currentMonth;this._endYear=this._currentYear}this._adjustDate()},  _getDate:function(){var a=!this._currentYear||this._input&&this._input.val()==""?null:new Date(this._currentYear,this._currentMonth,this._currentDay);return this._get("rangeSelect")?[a,!this._endYear?null:new Date(this._endYear,this._endMonth,this._endDay)]:a},_generateDatepicker:function(){var a=new Date;a=new Date(a.getFullYear(),a.getMonth(),a.getDate());var b=this._get("showStatus"),c=this._get("isRTL"),e=this._get("mandatory")?"":'<div class="datetimepicker_clear"><a onclick="jQuery.datetimepicker._clearDate('+  this._id+');"'+(b?this._addStatus(this._get("clearStatus")||"&#xa0;"):"")+">"+this._get("clearText")+"</a></div>";e='<div class="datetimepicker_control">'+(c?"":e)+'<div class="datetimepicker_close"><a onclick="jQuery.datetimepicker._hideDatepicker();"'+(b?this._addStatus(this._get("closeStatus")||"&#xa0;"):"")+">"+this._get("closeText")+"</a></div>"+(c?e:"")+"</div>";var f=this._get("prompt"),i=this._get("closeAtTop"),g=this._get("hideIfNoPrevNext"),k=this._getNumberOfMonths(),h=this._get("stepMonths"),  l=k[0]!=1||k[1]!=1,s=this._getMinMaxDate("min",true),o=this._getMinMaxDate("max"),j=this._drawMonth,n=this._drawYear,x=this._drawHour,z=this._drawMinute;if(o){var u=new Date(o.getFullYear(),o.getMonth()-k[1]+1,o.getDate());for(u=s&&u<s?s:u;new Date(n,j,1)>u;){j--;if(j<0){j=11;n--}}}u='<div class="datetimepicker_prev">'+(this._canAdjustMonth(-1,n,j)?'<a onclick="jQuery.datetimepicker._adjustDate('+this._id+", -"+h+", 'M');\""+(b?this._addStatus(this._get("prevStatus")||"&#xa0;"):"")+">"+this._get("prevText")+  "</a>":g?"":"<label>"+this._get("prevText")+"</label>")+"</div>";g='<div class="datetimepicker_next">'+(this._canAdjustMonth(+1,n,j)?'<a onclick="jQuery.datetimepicker._adjustDate('+this._id+", +"+h+", 'M');\""+(b?this._addStatus(this._get("nextStatus")||"&#xa0;"):"")+">"+this._get("nextText")+"</a>":g?">":"<label>"+this._get("nextText")+"</label>")+"</div>";c=(f?'<div class="datetimepicker_prompt">'+f+"</div>":"")+(i&&!this._inline?e:"")+'<div class="datetimepicker_links">'+(c?g:u)+(this._isInRange(a)?  '<div class="datetimepicker_current"><a onclick="jQuery.datetimepicker._gotoToday('+this._id+');"'+(b?this._addStatus(this._get("currentStatus")||"&#xa0;"):"")+">"+this._get("currentText")+"</a></div>":"")+(c?u:g)+"</div>";f=this._get("showWeeks");for(g=0;g<k[0];g++)for(h=0;h<k[1];h++){u=new Date(n,j,this._selectedDay,x,z);c+='<div class="datetimepicker_oneMonth'+(h==0?" datetimepicker_newRow":"")+'">'+this._generateMonthYearHeader(z,x,j,n,s,o,u,g>0||h>0)+'<table class="datetimepicker" cellpadding="0" cellspacing="0"><thead><tr class="datetimepicker_titleRow">'+  (f?"<td>"+this._get("weekHeader")+"</td>":"");for(var r=this._get("firstDay"),t=this._get("changeFirstDay"),p=this._get("dayNames"),m=this._get("dayNamesShort"),q=this._get("dayNamesMin"),v=0;v<7;v++){var w=(v+r)%7,y=this._get("dayStatus")||"&#xa0;";y=y.indexOf("DD")>-1?y.replace(/DD/,p[w]):y.replace(/D/,m[w]);c+="<td"+((v+r+6)%7>=5?' class="datetimepicker_weekEndCell"':"")+">"+(!t?"<span":'<a onclick="jQuery.datetimepicker._changeFirstDay('+this._id+", "+w+');"')+(b?this._addStatus(y):"")+' title="'+  p[w]+'">'+q[w]+(t?"</a>":"</span>")+"</td>"}c+="</tr></thead><tbody>";v=this._getDaysInMonth(n,j);if(n==this._selectedYear&&j==this._selectedMonth)this._selectedDay=Math.min(this._selectedDay,v);q=(this._getFirstDayOfMonth(n,j)-r+7)%7;t=!this._currentDay?new Date(9999,9,9):new Date(this._currentYear,this._currentMonth,this._currentDay);p=this._endDay?new Date(this._endYear,this._endMonth,this._endDay):t;m=new Date(n,j,1-q);q=l?6:Math.ceil((q+v)/7);w=this._get("beforeShowDay");y=this._get("showOtherMonths");  for(var H=this._get("calculateWeek")||d.datetimepicker.iso8601Week,I=this._get("statusForDate")||d.datetimepicker.dateStatus,F=0;F<q;F++){c+='<tr class="datetimepicker_daysRow">'+(f?'<td class="datetimepicker_weekCol">'+H(m)+"</td>":"");for(v=0;v<7;v++){var G=w?w.apply(this._input?this._input[0]:null,[m]):[true,""],A=m.getMonth()!=j,D=A||!G[0]||s&&m<s||o&&m>o;c+='<td class="datetimepicker_daysCell'+((v+r+6)%7>=5?" datetimepicker_weekEndCell":"")+(A?" datetimepicker_otherMonth":"")+(m.getTime()==u.getTime()&&  j==this._selectedMonth?" datetimepicker_daysCellOver":"")+(D?" datetimepicker_unselectable":"")+(A&&!y?"":" "+G[1]+(m.getTime()>=t.getTime()&&m.getTime()<=p.getTime()?" datetimepicker_currentDay":"")+(m.getTime()==a.getTime()?" datetimepicker_today":""))+'"'+(D?"":" onmouseover=\"jQuery(this).addClass('datetimepicker_daysCellOver');"+(!b||A&&!y?"":"jQuery('#datetimepicker_status_"+this._id+"').html('"+(I.apply(this._input?this._input[0]:null,[m,this])||"&#xa0;")+"');")+"\" onmouseout=\"jQuery(this).removeClass('datetimepicker_daysCellOver');"+  (!b||A&&!y?"":"jQuery('#datetimepicker_status_"+this._id+"').html('&#xa0;');")+'" onclick="jQuery.datetimepicker._selectDay('+this._id+","+j+","+n+', this);"')+">"+(A?y?m.getDate():"&#xa0;":D?m.getDate():"<a>"+m.getDate()+"</a>")+"</td>";m.setDate(m.getDate()+1)}c+="</tr>"}j++;if(j>11){j=0;n++}c+="</tbody></table></div>"}c+=(b?'<div style="clear: both;"></div><div id="datetimepicker_status_'+this._id+'" class="datetimepicker_status">'+(this._get("initStatus")||"&#xa0;")+"</div>":"")+(!i&&!this._inline?  e:"")+'<div style="clear: both;"></div>'+(d.browser.msie&&parseInt(d.browser.version)<7&&!this._inline?'<iframe src="javascript:false;" class="datetimepicker_cover"></iframe>':"");return c},_generateMonthYearHeader:function(a,b,c,e,f,i,g,k){f=this._rangeStart&&f&&g<f?g:f;g=this._get("showStatus");var h='<div class="datetimepicker_header">',l=this._get("monthNames");if(k||!this._get("changeMonth"))h+=l[c]+"&#xa0;";else{var s=f&&f.getFullYear()==e,o=i&&i.getFullYear()==e;h+='<select class="datetimepicker_newMonth" onchange="jQuery.datetimepicker._selectMonthYear('+  this._id+", this, 'M');\" onclick=\"jQuery.datetimepicker._clickMonthYear("+this._id+');"'+(g?this._addStatus(this._get("monthStatus")||"&#xa0;"):"")+">";for(var j=0;j<12;j++)if((!s||j>=f.getMonth())&&(!o||j<=i.getMonth()))h+='<option value="'+j+'"'+(j==c?' selected="selected"':"")+">"+l[j]+"</option>";h+="</select>"}if(k||!this._get("changeYear"))h+=e;else{c=this._get("yearRange").split(":");l=k=0;if(c.length!=2){k=e-10;l=e+10}else if(c[0].charAt(0)=="+"||c[0].charAt(0)=="-"){k=e+parseInt(c[0],10);  l=e+parseInt(c[1],10)}else{k=parseInt(c[0],10);l=parseInt(c[1],10)}k=f?Math.max(k,f.getFullYear()):k;l=i?Math.min(l,i.getFullYear()):l;for(h+='<select class="datetimepicker_newYear" onchange="jQuery.datetimepicker._selectMonthYear('+this._id+", this, 'Y');\" onclick=\"jQuery.datetimepicker._clickMonthYear("+this._id+');"'+(g?this._addStatus(this._get("yearStatus")||"&#xa0;"):"")+">";k<=l;k++)h+='<option value="'+k+'"'+(k==e?' selected="selected"':"")+">"+k+"</option>";h+="</select>"}if(!this._get("pickDateOnly")){h+=  "<br />";h+='<select class="datetimepicker_newHour" onchange="jQuery.datetimepicker._selectTime('+this._id+", this, 'H');\" onclick=\"jQuery.datetimepicker._clickMonthYear("+this._id+');"'+(g?this._addStatus(this._get("hourStatus")||"&#xa0;"):"")+">";for(hour=0;hour<24;hour++)h+='<option value="'+hour+'"'+(hour==b?' selected="selected"':"")+">"+(hour<10?"0"+hour:hour)+"</option>";h+="</select>";h+="&nbsp;:&nbsp;";h+='<select class="datetimepicker_newMinute" onchange="jQuery.datetimepicker._selectTime('+  this._id+", this, 'M');\" onclick=\"jQuery.datetimepicker._clickMonthYear("+this._id+');"'+(g?this._addStatus(this._get("minuteStatus")||"&#xa0;"):"")+">";for(minute=0;minute<60;minute++)h+='<option value="'+minute+'"'+(minute==a?' selected="selected"':"")+">"+(minute<10?"0"+minute:minute)+"</option>";h+="</select>"}h+="</div>";return h},_addStatus:function(a){return" onmouseover=\"jQuery('#datetimepicker_status_"+this._id+"').html('"+a+"');\" onmouseout=\"jQuery('#datetimepicker_status_"+this._id+  "').html('&#xa0;');\""},_adjustDate:function(a,b){var c=this._drawYear+(b=="Y"?a:0),e=this._drawMonth+(b=="M"?a:0),f=Math.min(this._selectedDay,this._getDaysInMonth(c,e))+(b=="D"?a:0);a=new Date(c,e,f,this._drawHour+(b=="H"?a:0),this._drawMinute+(b=="I"?a:0));b=this._getMinMaxDate("min",true);c=this._getMinMaxDate("max");a=b&&a<b?b:a;a=c&&a>c?c:a;this._selectedDay=a.getDate();this._currentMonth=this._drawMonth=this._selectedMonth=a.getMonth();this._currentYear=this._drawYear=this._selectedYear=a.getFullYear();  this._currentHour=this._drawHour=this._selectedHour=a.getHours();this._currentMinute=this._drawMinute=this._selectedMinute=a.getMinutes()},_getNumberOfMonths:function(){var a=this._get("numberOfMonths");return a==null?[1,1]:typeof a=="number"?[1,a]:a},_getMinMaxDate:function(a,b){if(a=this._determineDate(a+"Date",null)){a.setHours(0);a.setMinutes(0);a.setSeconds(0);a.setMilliseconds(0)}return a||(b?this._rangeStart:null)},_getDaysInMonth:function(a,b){return 32-(new Date(a,b,32)).getDate()},_getFirstDayOfMonth:function(a,  b){return(new Date(a,b,1)).getDay()},_canAdjustMonth:function(a,b,c){var e=this._getNumberOfMonths();b=new Date(b,c+(a<0?a:e[1]),1);a<0&&b.setDate(this._getDaysInMonth(b.getFullYear(),b.getMonth()));return this._isInRange(b)},_isInRange:function(a){var b=!this._rangeStart?null:new Date(this._selectedYear,this._selectedMonth,this._selectedDay);b=(b=b&&this._rangeStart<b?this._rangeStart:b)||this._getMinMaxDate("min");var c=this._getMinMaxDate("max");return(!b||a>=b)&&(!c||a<=c)},_getFormatConfig:function(){var a=  this._get("shortYearCutoff");a=typeof a!="string"?a:(new Date).getFullYear()%100+parseInt(a,10);return{shortYearCutoff:a,dayNamesShort:this._get("dayNamesShort"),dayNames:this._get("dayNames"),monthNamesShort:this._get("monthNamesShort"),monthNames:this._get("monthNames")}},_formatDateTime:function(a,b,c,e,f){if(!a){this._currentDay=this._selectedDay;this._currentMonth=this._selectedMonth;this._currentYear=this._selectedYear;this._currentHour=this._selectedHour;this._currentMinute=this._selectedMinute}a=  a?typeof a=="object"?a:new Date(c,b,a,e,f):new Date(this._currentYear,this._currentMonth,this._currentDay,this._currentHour,this._currentMinute);if(this._get("pickDateOnly"))return d.datetimepicker.formatDate(this._get("dateFormat"),a,this._getFormatConfig());return d.datetimepicker.formatDate(this._get("dateFormat")+" "+this._get("timeFormat"),a,this._getFormatConfig())}});d.fn.datetimepicker=function(a){var b=Array.prototype.slice.call(arguments,1);if(typeof a=="string"&&(a=="isDisabled"||a=="getDate"))return d.datetimepicker["_"+  a+"Datepicker"].apply(d.datetimepicker,[this[0]].concat(b));return this.each(function(){typeof a=="string"?d.datetimepicker["_"+a+"Datepicker"].apply(d.datetimepicker,[this].concat(b)):d.datetimepicker._attachDatepicker(this,a)})};d.datetimepicker=new E;d(document).ready(function(){d(document.body).append(d.datetimepicker._datetimepickerDiv).mousedown(d.datetimepicker._checkExternalClick)})})(jQuery);

/*
 *  JQuery UI DateTimePicker v1.0.0
 *
 *  Copyright (c) 2010 Daniel Harrod (http://www.projectcodegen.com/JQueryDateTimePicker.aspx)
 *  Dual licensed under the MIT License (http://www.opensource.org/licenses/mit-license.php)
 *  and GPL ( http://www.gnu.org/copyleft/gpl.html )
 *
 *  Portions of [JQuery UI Datepicker 1.8rc3] were modified on March 10, 2010.
 *  Portions of [Date Format 1.2.3] were modified on March 10, 2010.
 *
 *  http://www.projectcodegen.com/JQueryDateTimePicker.aspx
 *
 *  Depends:
 *    jquery.ui.core.js
**/

/*
 *  Overview:
 *     Additions:
 *        Incude Hours / Minutes / AMPM dropdowns.
 *        Absolute year determination based on absolute minimum variance to present year.
 *
 *     Replacements:
 *        Replaced [JQuery UI Datepicker 1.8rc3] inbound parsing with a simple date check Date(inbound)
 *
 *        Replaced outbound formatting with format string based on [Date Format 1.2.3]
 *
 *        Changed [Date Format 1.2.3] function names to play nice with [JQuery UI Datepicker 1.8rc3]
 *
 *        Minor tweaks
**/

/*
* jQuery UI Datepicker 1.8rc3
*
* Copyright (c) 2010 AUTHORS.txt (http://jqueryui.com/about)
* Dual licensed under the MIT (MIT-LICENSE.txt)
* and GPL (GPL-LICENSE.txt) licenses.
*
* http://docs.jquery.com/UI/Datepicker
*
* Depends:
*	jquery.ui.core.js
*/

/*
* Date Format 1.2.3
* (c) 2007-2009 Steven Levithan <stevenlevithan.com>
* MIT license
*
* Includes enhancements by Scott Trenda <scott.trenda.net>
* and Kris Kowal <cixar.com/~kris.kowal/>
*
* Accepts a date, a mask, or a date and a mask.
* Returns a formatted version of the given date.
* The date defaults to the current date/time.
* The mask defaults to dateFormat.masks.default.
*/


(function($) { // hide the namespace

$.extend($.ui, { datetimepicker: { version: "1.0.0"} });

    var PROP_NAME = 'datetimepicker';
    var dpuuid = new Date().getTime();

    /* Date picker manager.
    Use the singleton instance of this class, $.datetimepicker, to interact with the date picker.
    Settings for (groups of) date pickers are maintained in an instance object,
    allowing multiple different settings on the same page. */

    function Datetimepicker() {
        this.debug = false; // Change this to true to start debugging
        this._curInst = null; // The current instance in use
        this._keyEvent = false; // If the last event was a key event
        this._disabledInputs = []; // List of date picker inputs that have been disabled
        this._datepickerShowing = false; // True if the popup picker is showing , false if not
        this._inDialog = false; // True if showing within a "dialog", false if not
        this._mainDivId = 'ui-datepicker-div'; // The ID of the main datepicker division
        this._inlineClass = 'ui-datepicker-inline'; // The name of the inline marker class
        this._appendClass = 'ui-datepicker-append'; // The name of the append marker class
        this._triggerClass = 'ui-datepicker-trigger'; // The name of the trigger marker class
        this._dialogClass = 'ui-datepicker-dialog'; // The name of the dialog marker class
        this._disableClass = 'ui-datepicker-disabled'; // The name of the disabled covering marker class
        this._unselectableClass = 'ui-datepicker-unselectable'; // The name of the unselectable cell marker class
        this._currentClass = 'ui-datepicker-current-day'; // The name of the current day marker class
        this._dayOverClass = 'ui-datepicker-days-cell-over'; // The name of the day hover marker class
        this.regional = []; // Available regional settings, indexed by language code
        this.regional[''] = { // Default regional settings
            closeText: 'Close', // Display text for close link
            prevText: 'Prev', // Display text for previous month link
            nextText: 'Next', // Display text for next month link
            currentText: 'Now', // Display text for current month link
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December'], // Names of months for drop-down and formatting
            monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // For formatting
            dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'], // For formatting
            dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'], // For formatting
            dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'], // Column headings for days starting at Sunday
            weekHeader: 'Wk', // Column header for week of the year
            dateFormat: 'mm/dd/yyyy HH:MM TT', // See format options on parseDate
            /*  format options.
            dd     - Day of the month as digits; leading zero for single-digit days.
            ddd    - Day of the week as a three-letter abbreviation.
            dddd   - Day of the week as its full name.
            m      - Month as digits; no leading zero for single-digit months.
            mm     - Month as digits; leading zero for single-digit months.
            mmm    - Month as a three-letter abbreviation.
            mmmm   - Month as its full name.
            yy     - Year as last two digits; leading zero for years less than 10.
            yyyy   - Year represented by four digits.
            h      - Hours; no leading zero for single-digit hours (12-hour clock).
            hh     - Hours; leading zero for single-digit hours (12-hour clock).
            H      - Hours; no leading zero for single-digit hours (24-hour clock).
            HH     - Hours; leading zero for single-digit hours (24-hour clock).
            M      - Minutes; no leading zero for single-digit minutes.
            Uppercase M unlike CF timeFormat's m to avoid conflict with months.
            MM     - Minutes; leading zero for single-digit minutes.
            Uppercase MM unlike CF timeFormat's mm to avoid conflict with months.
            s      - Seconds; no leading zero for single-digit seconds.
            ss     - Seconds; leading zero for single-digit seconds.
            l or L - Milliseconds. l gives 3 digits. L gives 2 digits.
            t      - Lowercase, single-character time marker string: a or p.
            No equivalent in CF.
            tt     - Lowercase, two-character time marker string: am or pm.
            No equivalent in CF.
            T      - Uppercase, single-character time marker string: A or P.
            Uppercase T unlike CF's t to allow for user-specified casing.
            TT     - Uppercase, two-character time marker string: AM or PM.
            Uppercase TT unlike CF's tt to allow for user-specified casing.
            Z      - US timezone abbreviation, e.g. EST or MDT. With non-US timezones or in the Opera browser, the GMT/UTC offset is returned, e.g. GMT-0500
            No equivalent in CF.
            o      - GMT/UTC timezone offset, e.g. -0500 or +0230.
            No equivalent in CF.
            S      - The date's ordinal suffix (st, nd, rd, or th). Works well with d.
            No equivalent in CF.
            '' or "" - Literal character sequence. Surrounding quotes are removed.
            No equivalent in CF.
            UTC:   - Must be the first four characters of the mask. Converts the date from local time to UTC/GMT/Zulu time before applying the mask. The "UTC:" prefix is removed.
            No equivalent in CF.

            "default": "ddd mmm dd yyyy HH:MM:ss",
            shortDate: "m/d/yy",
            mediumDate: "mmm d, yyyy",
            longDate: "mmmm d, yyyy",
            fullDate: "dddd, mmmm d, yyyy",
            shortTime: "h:MM TT",
            mediumTime: "h:MM:ss TT",
            longTime: "h:MM:ss TT Z",
            isoDate: "yyyy-mm-dd",
            isoTime: "HH:MM:ss",
            isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
            isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
            */
            firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
            isRTL: false, // True if right-to-left language, false if left-to-right
            showMonthAfterYear: false, // True if the year select precedes month, false for month then year
            yearSuffix: '' // Additional text to append to the year in the month headers
        };
        this._defaults = { // Global defaults for all the date picker instances
            showOn: 'focus', // 'focus' for popup on focus,
            // 'button' for trigger button, or 'both' for either
            showAnim: 'show', // Name of jQuery animation for popup
            showOptions: {}, // Options for enhanced animations
            defaultDate: null, // Used when field is blank: actual date,
            // +/-number for offset from today, null for today
            appendText: '', // Display text following the input box, e.g. showing the format
            buttonText: '...', // Text for trigger button
            buttonImage: '', // URL for trigger button image
            buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
            hideIfNoPrevNext: false, // True to hide next/previous month links
            // if not applicable, false to just disable them
            navigationAsDateFormat: false, // True if date formatting applied to prev/today/next links
            gotoCurrent: false, // True if today link goes back to current selection instead
            changeMonth: false, // True if month can be selected directly, false if only prev/next
            changeYear: false, // True if year can be selected directly, false if only prev/next
            yearRange: 'c-10:c+10', // Range of years to display in drop-down,
            // either relative to today's year (-nn:+nn), relative to currently displayed year
            // (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
            showOtherMonths: false, // True to show dates in other months, false to leave blank
            selectOtherMonths: false, // True to allow selection of dates in other months, false for unselectable
            showWeek: false, // True to show week of the year, false to not show it
            calculateWeek: this.iso8601Week, // How to calculate the week of the year,
            // takes a Date and returns the number of the week for it
            shortYearCutoff: '+10', // Short year values < this are in the current century,
            // > this are in the previous century,
            // string value starting with '+' for current year + value
            minDate: null, // The earliest selectable date, or null for no limit
            maxDate: null, // The latest selectable date, or null for no limit
            duration: '_default', // Duration of display/closure
            beforeShowDay: null, // Function that takes a date and returns an array with
            // [0] = true if selectable, false if not, [1] = custom CSS class name(s) or '',
            // [2] = cell title (optional), e.g. $.datetimepicker.noWeekends
            beforeShow: null, // Function that takes an input field and
            // returns a set of custom settings for the date picker
            onSelect: null, // Define a callback function when a date is selected
            onChangeMonthYear: null, // Define a callback function when the month or year is changed
            onClose: null, // Define a callback function when the datepicker is closed
            numberOfMonths: 1, // Number of months to show at a time
            showCurrentAtPos: 0, // The position in multipe months at which to show the current month (starting at 0)
            stepMonths: 1, // Number of months to step back/forward
            stepBigMonths: 12, // Number of months to step back/forward for the big links
            altField: '', // Selector for an alternate field to store selected dates into
            altFormat: '', // The date format to use for the alternate field
            constrainInput: false, // The input is constrained by the current date format
            showButtonPanel: false, // True to show button panel, false to not show it
            autoSize: false // True to size the input for the date format, false to leave as is
        };
        $.extend(this._defaults, this.regional['']);
        this.dpDiv = $('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ui-helper-hidden-accessible"></div>');
    }

    $.extend(Datetimepicker.prototype, {
        /* Class name added to elements to indicate already configured with a date picker. */
        markerClassName: 'hasDatetimepicker',

        /* Debug logging (if enabled). */
        log: function() {
            if (this.debug)
                console.log.apply('', arguments);
        },

        // TODO rename to "widget" when switching to widget factory
        _widgetDatepicker: function() {
            return this.dpDiv;
        },

        /* Override the default settings for all instances of the date picker.
        @param  settings  object - the new settings to use as defaults (anonymous object)
        @return the manager object */
        setDefaults: function(settings) {
            extendRemove(this._defaults, settings || {});
            return this;
        },

        /* Attach the date picker to a jQuery selection.
        @param  target    element - the target input field or division or span
        @param  settings  object - the new settings to use for this date picker instance (anonymous) */
        _attachDatepicker: function(target, settings) {
            // check for settings on the control itself - in namespace 'date:'
            var inlineSettings = null;
            for (var attrName in this._defaults) {
                var attrValue = target.getAttribute('date:' + attrName);
                if (attrValue) {
                    inlineSettings = inlineSettings || {};
                    try {
                        inlineSettings[attrName] = eval(attrValue);
                    } catch (err) {
                        inlineSettings[attrName] = attrValue;
                    }
                }
            }
            var nodeName = target.nodeName.toLowerCase();
            var inline = (nodeName == 'div' || nodeName == 'span');
            if (!target.id)
                target.id = 'dp' + (++this.uuid);
            var inst = this._newInst($(target), inline);
            inst.settings = $.extend({}, settings || {}, inlineSettings || {});
            if (nodeName == 'input') {
                this._connectDatepicker(target, inst);
            } else if (inline) {
                this._inlineDatepicker(target, inst);
            }
        },

        /* Create a new instance object. */
        _newInst: function(target, inline) {
            var id = target[0].id.replace(/([^A-Za-z0-9_])/g, '\\\\$1'); // escape jQuery meta chars
            return { id: id, input: target, // associated target
                selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
                drawMonth: 0, drawYear: 0, // month being drawn
                inline: inline, // is datepicker inline or not
                dpDiv: (!inline ? this.dpDiv : // presentation div
			$('<div class="' + this._inlineClass + ' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))
            };
        },

        /* Attach the date picker to an input field. */
        _connectDatepicker: function(target, inst) {
            var input = $(target);
            inst.append = $([]);
            inst.trigger = $([]);
            if (input.hasClass(this.markerClassName))
                return;
            this._attachments(input, inst);
            input.addClass(this.markerClassName).keydown(this._doKeyDown).
			keyup(this._doKeyUp).
			bind("setData.datepicker", function(event, key, value) {
			    inst.settings[key] = value;
			}).bind("getData.datepicker", function(event, key) {
			    return this._get(inst, key);
			});
            this._autoSize(inst);
            $.data(target, PROP_NAME, inst);
        },

        /* Make attachments based on settings. */
        _attachments: function(input, inst) {
            var appendText = this._get(inst, 'appendText');
            var isRTL = this._get(inst, 'isRTL');
            if (inst.append)
                inst.append.remove();
            if (appendText) {
                inst.append = $('<span class="' + this._appendClass + '">' + appendText + '</span>');
                input[isRTL ? 'before' : 'after'](inst.append);
            }
            input.unbind('focus', this._showDatepicker);
            if (inst.trigger)
                inst.trigger.remove();
            var showOn = this._get(inst, 'showOn');
            if (showOn == 'focus' || showOn == 'both') // pop-up date picker when in the marked field
                input.focus(this._showDatepicker);
            if (showOn == 'button' || showOn == 'both') { // pop-up date picker when button clicked
                var buttonText = this._get(inst, 'buttonText');
                var buttonImage = this._get(inst, 'buttonImage');
                inst.trigger = $(this._get(inst, 'buttonImageOnly') ?
				$('<img/>').addClass(this._triggerClass).
					attr({ src: buttonImage, alt: buttonText, title: buttonText }) :
				$('<button type="button"></button>').addClass(this._triggerClass).
					html(buttonImage == '' ? buttonText : $('<img/>').attr(
					{ src: buttonImage, alt: buttonText, title: buttonText })));
                input[isRTL ? 'before' : 'after'](inst.trigger);
                inst.trigger.click(function() {
                    if ($.datetimepicker._datepickerShowing && $.datetimepicker._lastInput == input[0])
                        $.datetimepicker._hideDatepicker();
                    else
                        $.datetimepicker._showDatepicker(input[0]);
                    return false;
                });
            }
        },

        /* Apply the maximum length for the date format. */
        _autoSize: function(inst) {
            if (this._get(inst, 'autoSize') && !inst.inline) {
                var date = new Date(2009, 12 - 1, 20); // Ensure double digits
                var dateFormat = this._get(inst, 'dateFormat');
                if (dateFormat.match(/[DM]/)) {
                    var findMax = function(names) {
                        var max = 0;
                        var maxI = 0;
                        for (var i = 0; i < names.length; i++) {
                            if (names[i].length > max) {
                                max = names[i].length;
                                maxI = i;
                            }
                        }
                        return maxI;
                    };
                    date.setMonth(findMax(this._get(inst, (dateFormat.match(/MM/) ?
					'monthNames' : 'monthNamesShort'))));
                    date.setDate(findMax(this._get(inst, (dateFormat.match(/DD/) ?
					'dayNames' : 'dayNamesShort'))) + 20 - date.getDay());
                }
                inst.input.attr('size', this._formatDate(inst, date).length);
            }
        },

        /* Attach an inline date picker to a div. */
        _inlineDatepicker: function(target, inst) {
            var divSpan = $(target);
            if (divSpan.hasClass(this.markerClassName))
                return;
            divSpan.addClass(this.markerClassName).append(inst.dpDiv).
			bind("setData.datepicker", function(event, key, value) {
			    inst.settings[key] = value;
			}).bind("getData.datepicker", function(event, key) {
			    return this._get(inst, key);
			});
            $.data(target, PROP_NAME, inst);
            this._setDate(inst, this._getDefaultDate(inst), true);
            this._updateDatepicker(inst);
            this._updateAlternate(inst);
        },

        /* Pop-up the date picker in a "dialog" box.
        @param  input     element - ignored
        @param  date      string or Date - the initial date to display
        @param  onSelect  function - the function to call when a date is selected
        @param  settings  object - update the dialog date picker instance's settings (anonymous object)
        @param  pos       int[2] - coordinates for the dialog's position within the screen or
        event - with x/y coordinates or
        leave empty for default (screen centre)
        @return the manager object */
        _dialogDatepicker: function(input, date, onSelect, settings, pos) {
            var inst = this._dialogInst; // internal instance
            if (!inst) {
                var id = 'dp' + (++this.uuid);
                this._dialogInput = $('<input type="text" id="' + id +
				'" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>');
                this._dialogInput.keydown(this._doKeyDown);
                $('body').append(this._dialogInput);
                inst = this._dialogInst = this._newInst(this._dialogInput, false);
                inst.settings = {};
                $.data(this._dialogInput[0], PROP_NAME, inst);
            }
            extendRemove(inst.settings, settings || {});
            date = (date && date.constructor == Date ? this._formatDate(inst, date) : date);
            this._dialogInput.val(date);

            this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null);
            if (!this._pos) {
                var browserWidth = document.documentElement.clientWidth;
                var browserHeight = document.documentElement.clientHeight;
                var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
                var scrollY = document.documentElement.scrollTop || document.body.scrollTop;
                this._pos = // should use actual width/height below
				[(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY];
            }

            // move input on screen for focus, but hidden behind dialog
            this._dialogInput.css('left', (this._pos[0] + 20) + 'px').css('top', this._pos[1] + 'px');
            inst.settings.onSelect = onSelect;
            this._inDialog = true;
            this.dpDiv.addClass(this._dialogClass);
            this._showDatepicker(this._dialogInput[0]);
            if ($.blockUI)
                $.blockUI(this.dpDiv);
            $.data(this._dialogInput[0], PROP_NAME, inst);
            return this;
        },

        /* Detach a datepicker from its control.
        @param  target    element - the target input field or division or span */
        _destroyDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            $.removeData(target, PROP_NAME);
            if (nodeName == 'input') {
                inst.append.remove();
                inst.trigger.remove();
                $target.removeClass(this.markerClassName).
				unbind('focus', this._showDatepicker).
				unbind('keydown', this._doKeyDown).
				unbind('keyup', this._doKeyUp);
            } else if (nodeName == 'div' || nodeName == 'span')
                $target.removeClass(this.markerClassName).empty();
        },

        /* Enable the date picker to a jQuery selection.
        @param  target    element - the target input field or division or span */
        _enableDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            if (nodeName == 'input') {
                target.disabled = false;
                inst.trigger.filter('button').
				each(function() { this.disabled = false; }).end().
				filter('img').css({ opacity: '1.0', cursor: '' });
            }
            else if (nodeName == 'div' || nodeName == 'span') {
                var inline = $target.children('.' + this._inlineClass);
                inline.children().removeClass('ui-state-disabled');
            }
            this._disabledInputs = $.map(this._disabledInputs,
			function(value) { return (value == target ? null : value); }); // delete entry
        },

        /* Disable the date picker to a jQuery selection.
        @param  target    element - the target input field or division or span */
        _disableDatepicker: function(target) {
            var $target = $(target);
            var inst = $.data(target, PROP_NAME);
            if (!$target.hasClass(this.markerClassName)) {
                return;
            }
            var nodeName = target.nodeName.toLowerCase();
            if (nodeName == 'input') {
                target.disabled = true;
                inst.trigger.filter('button').
				each(function() { this.disabled = true; }).end().
				filter('img').css({ opacity: '0.5', cursor: 'default' });
            }
            else if (nodeName == 'div' || nodeName == 'span') {
                var inline = $target.children('.' + this._inlineClass);
                inline.children().addClass('ui-state-disabled');
            }
            this._disabledInputs = $.map(this._disabledInputs,
			function(value) { return (value == target ? null : value); }); // delete entry
            this._disabledInputs[this._disabledInputs.length] = target;
        },

        /* Is the first field in a jQuery collection disabled as a datepicker?
        @param  target    element - the target input field or division or span
        @return boolean - true if disabled, false if enabled */
        _isDisabledDatepicker: function(target) {
            if (!target) {
                return false;
            }
            for (var i = 0; i < this._disabledInputs.length; i++) {
                if (this._disabledInputs[i] == target)
                    return true;
            }
            return false;
        },

        /* Retrieve the instance data for the target control.
        @param  target  element - the target input field or division or span
        @return  object - the associated instance data
        @throws  error if a jQuery problem getting data */
        _getInst: function(target) {
            try {
                return $.data(target, PROP_NAME);
            }
            catch (err) {
                throw 'Missing instance data for this datepicker';
            }
        },

        /* Update or retrieve the settings for a date picker attached to an input field or division.
        @param  target  element - the target input field or division or span
        @param  name    object - the new settings to update or
        string - the name of the setting to change or retrieve,
        when retrieving also 'all' for all instance settings or
        'defaults' for all global defaults
        @param  value   any - the new value for the setting
        (omit if above is an object or to retrieve a value) */
        _optionDatepicker: function(target, name, value) {
            var inst = this._getInst(target);
            if (arguments.length == 2 && typeof name == 'string') {
                return (name == 'defaults' ? $.extend({}, $.datetimepicker._defaults) :
				(inst ? (name == 'all' ? $.extend({}, inst.settings) :
				this._get(inst, name)) : null));
            }
            var settings = name || {};
            if (typeof name == 'string') {
                settings = {};
                settings[name] = value;
            }
            if (inst) {
                if (this._curInst == inst) {
                    this._hideDatepicker();
                }
                var date = this._getDateDatepicker(target, true);
                extendRemove(inst.settings, settings);
                this._attachments($(target), inst);
                this._autoSize(inst);
                this._setDateDatepicker(target, date);
                this._updateDatepicker(inst);
            }
        },

        // change method deprecated
        _changeDatepicker: function(target, name, value) {
            this._optionDatepicker(target, name, value);
        },

        /* Redraw the date picker attached to an input field or division.
        @param  target  element - the target input field or division or span */
        _refreshDatepicker: function(target) {
            var inst = this._getInst(target);
            if (inst) {
                this._updateDatepicker(inst);
            }
        },

        /* Set the dates for a jQuery selection.
        @param  target   element - the target input field or division or span
        @param  date     Date - the new date */
        _setDateDatepicker: function(target, date) {
            var inst = this._getInst(target);
            if (inst) {
                this._setDate(inst, date);
                this._updateDatepicker(inst);
                this._updateAlternate(inst);
            }
        },

        /* Get the date(s) for the first entry in a jQuery selection.
        @param  target     element - the target input field or division or span
        @param  noDefault  boolean - true if no default date is to be used
        @return Date - the current date */
        _getDateDatepicker: function(target, noDefault) {
            var inst = this._getInst(target);
            if (inst && !inst.inline)
                this._setDateFromField(inst, noDefault);
            return (inst ? this._getDate(inst) : null);
        },

        /* Handle keystrokes. */
        _doKeyDown: function(event) {
            var inst = $.datetimepicker._getInst(event.target);
            var handled = true;
            var isRTL = inst.dpDiv.is('.ui-datepicker-rtl');
            inst._keyEvent = true;
            if ($.datetimepicker._datepickerShowing)
                switch (event.keyCode) {
                case 9: $.datetimepicker._hideDatepicker();
                    handled = false;
                    break; // hide on tab out
                case 13: var sel = $('td.' + $.datetimepicker._dayOverClass, inst.dpDiv).
							add($('td.' + $.datetimepicker._currentClass, inst.dpDiv));
                    if (sel[0])
                        $.datetimepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0], inst.currentHour, inst.currentMinute, inst.currentAMPM);
                    else
                        $.datetimepicker._hideDatepicker();
                    return false; // don't submit the form
                    break; // select the value on enter
                case 27: $.datetimepicker._hideDatepicker();
                    break; // hide on escape
                case 33: $.datetimepicker._adjustDate(event.target, (event.ctrlKey ?
							-$.datetimepicker._get(inst, 'stepBigMonths') :
							-$.datetimepicker._get(inst, 'stepMonths')), 'M');
                    break; // previous month/year on page up/+ ctrl
                case 34: $.datetimepicker._adjustDate(event.target, (event.ctrlKey ?
							+$.datetimepicker._get(inst, 'stepBigMonths') :
							+$.datetimepicker._get(inst, 'stepMonths')), 'M');
                    break; // next month/year on page down/+ ctrl
                case 35: if (event.ctrlKey || event.metaKey) $.datetimepicker._clearDate(event.target);
                    handled = event.ctrlKey || event.metaKey;
                    break; // clear on ctrl or command +end
                case 36: if (event.ctrlKey || event.metaKey) $.datetimepicker._gotoToday(event.target);
                    handled = event.ctrlKey || event.metaKey;
                    break; // current on ctrl or command +home
                case 37: if (event.ctrlKey || event.metaKey) $.datetimepicker._adjustDate(event.target, (isRTL ? +1 : -1), 'D');
                    handled = event.ctrlKey || event.metaKey;
                    // -1 day on ctrl or command +left
                    if (event.originalEvent.altKey) $.datetimepicker._adjustDate(event.target, (event.ctrlKey ?
									-$.datetimepicker._get(inst, 'stepBigMonths') :
									-$.datetimepicker._get(inst, 'stepMonths')), 'M');
                    // next month/year on alt +left on Mac
                    break;
                case 38: if (event.ctrlKey || event.metaKey) $.datetimepicker._adjustDate(event.target, -7, 'D');
                    handled = event.ctrlKey || event.metaKey;
                    break; // -1 week on ctrl or command +up
                case 39: if (event.ctrlKey || event.metaKey) $.datetimepicker._adjustDate(event.target, (isRTL ? -1 : +1), 'D');
                    handled = event.ctrlKey || event.metaKey;
                    // +1 day on ctrl or command +right
                    if (event.originalEvent.altKey) $.datetimepicker._adjustDate(event.target, (event.ctrlKey ?
									+$.datetimepicker._get(inst, 'stepBigMonths') :
									+$.datetimepicker._get(inst, 'stepMonths')), 'M');
                    // next month/year on alt +right
                    break;
                case 40: if (event.ctrlKey || event.metaKey) $.datetimepicker._adjustDate(event.target, +7, 'D');
                    handled = event.ctrlKey || event.metaKey;
                    break; // +1 week on ctrl or command +down
                default: handled = false;
            }
            else if (event.keyCode == 36 && event.ctrlKey) // display the date picker on ctrl+home
                $.datetimepicker._showDatepicker(this);
            else {
                handled = false;
            }
            if (handled) {
                event.preventDefault();
                event.stopPropagation();
            }
        },

        // Javascript does not offer Left function
        _Left: function(str, name) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else
                return String(str).substring(0, n);
        },

        // Determines Year, primarily for the century, by picking the closest.
        // 77 will choose 1977, 10 will choose 2010
        _DetermineYear: function(year) {
            var yearLength = String(year).length;
            var CurrentDate = new Date();
            var PresentYear = CurrentDate.getFullYear();
            switch (yearLength) {
                case 0:
                    return CurrentDate.getFullYear();
                    break;
                case 1:
                    return '200' + year;
                    break;
                case 2:
                    var FutureYear = parseInt('20' + year);
                    var PastYear = parseInt('19' + year);
                    var FutureDiff = Math.abs(FutureYear - PresentYear);
                    var PastDiff = Math.abs(PastYear - PresentYear);
                    if (PastDiff < FutureDiff) {
                        return PastYear;
                    } else {
                        return FutureYear;
                    }
                case 3:
                    return '2' + year;
                    break;
                case 4:
                    return year;
                default:
                    return _Left(year, 4);
            }
        },
        //\Date-Time-Picker

        /* Synchronise manual entry and field/alternate field. */
        _doKeyUp: function(event) {
            var inst = $.datetimepicker._getInst(event.target);
            if (inst.input.val() != inst.lastVal) {
                try {

                    //Date-Time-Picker
                    var date = new Date(formatDate(inst.input.val()));
                    if (date != "NaN") {
                        date.setYear($.datetimepicker._DetermineYear(date.getYear()));
                        if (date) { // only if valid
                            $.datetimepicker._setDateFromField(inst);
                            $.datetimepicker._updateAlternate(inst);
                            $.datetimepicker._updateDatepicker(inst);
                        }
                    }
                }
                catch (event) {
                    $.datetimepicker.log(event);
                }
            }
            return true;
        },

        /* Pop-up the date picker for a given input field.
        @param  input  element - the input field attached to the date picker or
        event - if triggered by focus */
        _showDatepicker: function(input) {
            input = input.target || input;
            if (input.nodeName.toLowerCase() != 'input') // find from button/image trigger
                input = $('input', input.parentNode)[0];
            if ($.datetimepicker._isDisabledDatepicker(input) || $.datetimepicker._lastInput == input) // already here
                return;
            var inst = $.datetimepicker._getInst(input);
            if ($.datetimepicker._curInst && $.datetimepicker._curInst != inst) {
                $.datetimepicker._curInst.dpDiv.stop(true, true);
            }
            var beforeShow = $.datetimepicker._get(inst, 'beforeShow');
            extendRemove(inst.settings, (beforeShow ? beforeShow.apply(input, [input, inst]) : {}));
            inst.lastVal = null;
            $.datetimepicker._lastInput = input;
            $.datetimepicker._setDateFromField(inst);
            if ($.datetimepicker._inDialog) // hide cursor
                input.value = '';
            if (!$.datetimepicker._pos) { // position below input
                $.datetimepicker._pos = $.datetimepicker._findPos(input);
                $.datetimepicker._pos[1] += input.offsetHeight; // add the height
            }
            var isFixed = false;
            $(input).parents().each(function() {
                isFixed |= $(this).css('position') == 'fixed';
                return !isFixed;
            });
            if (isFixed && $.browser.opera) { // correction for Opera when fixed and scrolled
                $.datetimepicker._pos[0] -= document.documentElement.scrollLeft;
                $.datetimepicker._pos[1] -= document.documentElement.scrollTop;
            }
            var offset = { left: $.datetimepicker._pos[0], top: $.datetimepicker._pos[1] };
            $.datetimepicker._pos = null;
            // determine sizing offscreen
            inst.dpDiv.css({ position: 'absolute', display: 'block', top: '-1000px' });
            $.datetimepicker._updateDatepicker(inst);
            // fix width for dynamic number of date pickers
            // and adjust position before showing
            offset = $.datetimepicker._checkOffset(inst, offset, isFixed);
            inst.dpDiv.css({ position: ($.datetimepicker._inDialog && $.blockUI ?
			'static' : (isFixed ? 'fixed' : 'absolute')), display: 'none',
                left: offset.left + 'px', top: offset.top + 'px'
            });
            if (!inst.inline) {
                var showAnim = $.datetimepicker._get(inst, 'showAnim');
                var duration = $.datetimepicker._get(inst, 'duration');
                var postProcess = function() {
                    $.datetimepicker._datepickerShowing = true;
                    var borders = $.datetimepicker._getBorders(inst.dpDiv);
                    inst.dpDiv.find('iframe.ui-datepicker-cover'). // IE6- only
					css({ left: -borders[0], top: -borders[1],
					    width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()
					});
                };
                inst.dpDiv.zIndex($(input).zIndex() + 1);
                if ($.effects && $.effects[showAnim])
                    inst.dpDiv.show(showAnim, $.datetimepicker._get(inst, 'showOptions'), duration, postProcess);
                else
                    inst.dpDiv[showAnim || 'show']((showAnim ? duration : null), postProcess);
                if (!showAnim)
                    postProcess();
                if (inst.input.is(':visible') && !inst.input.is(':disabled'))
                    inst.input.focus();
                $.datetimepicker._curInst = inst;
            }
        },

        /* Generate the date picker content. */
        _updateDatepicker: function(inst) {
            var self = this;
            var borders = $.datetimepicker._getBorders(inst.dpDiv);
            inst.dpDiv.empty().append(this._generateHTML(inst))
			.find('iframe.ui-datepicker-cover') // IE6- only
				.css({ left: -borders[0], top: -borders[1],
				    width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()
				})
			.end()
			.find('button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a')
				.bind('mouseout', function() {
				    $(this).removeClass('ui-state-hover');
				    if (this.className.indexOf('ui-datepicker-prev') != -1) $(this).removeClass('ui-datepicker-prev-hover');
				    if (this.className.indexOf('ui-datepicker-next') != -1) $(this).removeClass('ui-datepicker-next-hover');
				})
				.bind('mouseover', function() {
				    if (!self._isDisabledDatepicker(inst.inline ? inst.dpDiv.parent()[0] : inst.input[0])) {
				        $(this).parents('.ui-datepicker-calendar').find('a').removeClass('ui-state-hover');
				        $(this).addClass('ui-state-hover');
				        if (this.className.indexOf('ui-datepicker-prev') != -1) $(this).addClass('ui-datepicker-prev-hover');
				        if (this.className.indexOf('ui-datepicker-next') != -1) $(this).addClass('ui-datepicker-next-hover');
				    }
				})
			.end()
			.find('.' + this._dayOverClass + ' a')
				.trigger('mouseover')
			.end();
            var numMonths = this._getNumberOfMonths(inst);
            var cols = numMonths[1];
            var width = 17;
            if (cols > 1)
                inst.dpDiv.addClass('ui-datepicker-multi-' + cols).css('width', (width * cols) + 'em');
            else
                inst.dpDiv.removeClass('ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4').width('');
            inst.dpDiv[(numMonths[0] != 1 || numMonths[1] != 1 ? 'add' : 'remove') +
			'Class']('ui-datepicker-multi');
            inst.dpDiv[(this._get(inst, 'isRTL') ? 'add' : 'remove') +
			'Class']('ui-datepicker-rtl');

            $("#DP_jQuery_Hour_" + dpuuid).val(inst.selectedHour);
            $("#DP_jQuery_Minute_" + dpuuid).val(inst.selectedMinute);
            $("#DP_jQuery_AMPM_" + dpuuid).val(inst.selectedAMPM);

            if (inst == $.datetimepicker._curInst && $.datetimepicker._datepickerShowing && inst.input &&
				inst.input.is(':visible') && !inst.input.is(':disabled'))
                inst.input.focus();


        },

        /* Retrieve the size of left and top borders for an element.
        @param  elem  (jQuery object) the element of interest
        @return  (number[2]) the left and top borders */
        _getBorders: function(elem) {
            var convert = function(value) {
                return { thin: 1, medium: 2, thick: 3}[value] || value;
            };
            return [parseFloat(convert(elem.css('border-left-width'))),
			parseFloat(convert(elem.css('border-top-width')))];
        },

        /* Check positioning to remain on screen. */
        _checkOffset: function(inst, offset, isFixed) {
            var dpWidth = inst.dpDiv.outerWidth();
            var dpHeight = inst.dpDiv.outerHeight();
            var inputWidth = inst.input ? inst.input.outerWidth() : 0;
            var inputHeight = inst.input ? inst.input.outerHeight() : 0;
            var viewWidth = document.documentElement.clientWidth + $(document).scrollLeft();
            var viewHeight = document.documentElement.clientHeight + $(document).scrollTop();

            offset.left -= (this._get(inst, 'isRTL') ? (dpWidth - inputWidth) : 0);
            offset.left -= (isFixed && offset.left == inst.input.offset().left) ? $(document).scrollLeft() : 0;
            offset.top -= (isFixed && offset.top == (inst.input.offset().top + inputHeight)) ? $(document).scrollTop() : 0;

            // now check if datepicker is showing outside window viewport - move to a better place if so.
            offset.left -= Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
			Math.abs(offset.left + dpWidth - viewWidth) : 0);
            offset.top -= Math.min(offset.top, (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
			Math.abs(dpHeight + inputHeight) : 0);

            return offset;
        },

        /* Find an object's position on the screen. */
        _findPos: function(obj) {
            var inst = this._getInst(obj);
            var isRTL = this._get(inst, 'isRTL');
            while (obj && (obj.type == 'hidden' || obj.nodeType != 1)) {
                obj = obj[isRTL ? 'previousSibling' : 'nextSibling'];
            }
            var position = $(obj).offset();
            return [position.left, position.top];
        },

        /* Hide the date picker from view.
        @param  input  element - the input field attached to the date picker */
        _hideDatepicker: function(input) {
            var inst = this._curInst;
            if (!inst || (input && inst != $.data(input, PROP_NAME)))
                return;
            if (this._datepickerShowing) {
                var showAnim = this._get(inst, 'showAnim');
                var duration = this._get(inst, 'duration');
                var postProcess = function() {
                    $.datetimepicker._tidyDialog(inst);
                    this._curInst = null;
                };
                if ($.effects && $.effects[showAnim])
                    inst.dpDiv.hide(showAnim, $.datetimepicker._get(inst, 'showOptions'), duration, postProcess);
                else
                    inst.dpDiv[(showAnim == 'slideDown' ? 'slideUp' :
					(showAnim == 'fadeIn' ? 'fadeOut' : 'hide'))]((showAnim ? duration : null), postProcess);
                if (!showAnim)
                    postProcess();
                var onClose = this._get(inst, 'onClose');
                if (onClose)
                    onClose.apply((inst.input ? inst.input[0] : null),
					[(inst.input ? inst.input.val() : ''), inst]);  // trigger custom callback
                this._datepickerShowing = false;
                this._lastInput = null;
                if (this._inDialog) {
                    this._dialogInput.css({ position: 'absolute', left: '0', top: '-100px' });
                    if ($.blockUI) {
                        $.unblockUI();
                        $('body').append(this.dpDiv);
                    }
                }
                this._inDialog = false;
            }
        },

        /* Tidy up after a dialog display. */
        _tidyDialog: function(inst) {
            inst.dpDiv.removeClass(this._dialogClass).unbind('.ui-datepicker-calendar');
        },

        /* Close date picker if clicked elsewhere. */
        _checkExternalClick: function(event) {
            if (!$.datetimepicker._curInst)
                return;
            var $target = $(event.target);
            if ($target[0].id != $.datetimepicker._mainDivId &&
				$target.parents('#' + $.datetimepicker._mainDivId).length == 0 &&
				!$target.hasClass($.datetimepicker.markerClassName) &&
				!$target.hasClass($.datetimepicker._triggerClass) &&
				$.datetimepicker._datepickerShowing && !($.datetimepicker._inDialog && $.blockUI))
                $.datetimepicker._hideDatepicker();
        },

        /* Adjust one of the date sub-fields. */
        _adjustDate: function(id, offset, period) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (this._isDisabledDatepicker(target[0])) {
                return;
            }
            this._adjustInstDate(inst, offset +
			(period == 'M' ? this._get(inst, 'showCurrentAtPos') : 0), // undo positioning
			period);
            this._updateDatepicker(inst);
        },

        /* Action for current link. */
        _gotoToday: function(id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
                inst.selectedDay = inst.currentDay;
                inst.drawMonth = inst.selectedMonth = inst.currentMonth;
                inst.drawYear = inst.selectedYear = inst.currentYear;
                inst.selectedHour = inst.currentHour;
                inst.selectedMinute = inst.currentMinute;
                inst.selectedAMPM = inst.currentAMPM;
            }
            else {
                var date = new Date();
                inst.selectedDay = date.getDate();
                inst.drawMonth = inst.selectedMonth = date.getMonth();
                inst.drawYear = inst.selectedYear = date.getFullYear();
                if (date.getHours() > 12) {
                    inst.selectedHour = date.getHours() - 12;
                    inst.selectedAMPM = "PM";
                }
                else {
                    inst.selectedHour = date.getHours();
                    inst.selectedAMPM = "AM";
                }

                if (date.getHours() == 12) {
                    inst.selectedAMPM = "PM";
                }

                if (date.getHours() == 00) {
                    inst.selectedHour = 12;
                }
                inst.selectedMinute = date.getMinutes();
            }
            this._notifyChange(inst);
            this._adjustDate(target);
        },

        /* Action for selecting a new month/year. */
        _selectMonthYear: function(id, select, period) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            inst._selectingMonthYear = false;
            inst['selected' + (period == 'M' ? 'Month' : 'Year')] =
		inst['draw' + (period == 'M' ? 'Month' : 'Year')] =
			parseInt(select.options[select.selectedIndex].value, 10);
            this._notifyChange(inst);
            this._adjustDate(target);
        },

        /* Restore input focus after not changing month/year. */
        _clickMonthYear: function(id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            if (inst.input && inst._selectingMonthYear && !$.browser.msie)
                inst.input.focus();
            inst._selectingMonthYear = !inst._selectingMonthYear;
        },

        /* Action for selecting a day. */
        _selectDay: function(id, month, year, td, hh, mm, am) {
            var target = $(id);
            if ($(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) {
                return;
            }
            var inst = this._getInst(target[0]);
            inst.selectedDay = inst.currentDay = $('a', td).html();
            inst.selectedMonth = inst.currentMonth = month;
            inst.selectedYear = inst.currentYear = year;
            inst.selectedHour = inst.currentHour = $("#DP_jQuery_Hour_" + dpuuid).val()
            inst.selectedMinute = inst.currentMinute = $("#DP_jQuery_Minute_" + dpuuid).val();
            inst.selectedAMPM = inst.currentAMPM = $("#DP_jQuery_AMPM_" + dpuuid).val();
            this._selectDate(id, this._formatDate(inst,
			inst.currentDay, inst.currentMonth, inst.currentYear));
        },

        /* Erase the input field and hide the date picker. */
        _clearDate: function(id) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            this._selectDate(target, '');
        },

        /* Update the input field with the selected date. */
        _selectDate: function(id, dateStr) {
            var target = $(id);
            var inst = this._getInst(target[0]);
            dateStr = (dateStr != null ? dateStr : this._formatDate(inst));
            if (inst.input)
                inst.input.val(dateStr);
            this._updateAlternate(inst);
            var onSelect = this._get(inst, 'onSelect');
            if (onSelect)
                onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
            else if (inst.input)
                inst.input.trigger('change'); // fire the change event
            if (inst.inline)
                this._updateDatepicker(inst);
            else {
                this._hideDatepicker();
                this._lastInput = inst.input[0];
                if (typeof (inst.input[0]) != 'object')
                    inst.input.focus(); // restore focus
                this._lastInput = null;
            }
        },

        /* Update any alternate field to synchronise with the main field. */
        _updateAlternate: function(inst) {
            var altField = this._get(inst, 'altField');
            if (altField) { // update alternate field too
                var altFormat = this._get(inst, 'altFormat') || this._get(inst, 'dateFormat');
                var date = this._getDate(inst);
                var dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst));
                $(altField).each(function() { $(this).val(dateStr); });
            }
        },

        /* Set as beforeShowDay function to prevent selection of weekends.
        @param  date  Date - the date to customise
        @return [boolean, string] - is this date selectable?, what is its CSS class? */
        noWeekends: function(date) {
            var day = date.getDay();
            return [(day > 0 && day < 6), ''];
        },

        /* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
        @param  date  Date - the date to get the week for
        @return  number - the number of the week within the year that contains this date */
        iso8601Week: function(date) {
            var checkDate = new Date(date.getTime());
            // Find Thursday of this week starting on Monday
            checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
            var time = checkDate.getTime();
            checkDate.setMonth(0); // Compare with Jan 1
            checkDate.setDate(1);
            return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
        },

        /* Parse a string value into a date object.
        See formatDate below for the possible formats.

	   @param  format    string - the expected format of the date
        @param  value     string - the date in the above format
        @param  settings  Object - attributes include:
        shortYearCutoff  number - the cutoff year for determining the century (optional)
        dayNamesShort    string[7] - abbreviated names of the days from Sunday (optional)
        dayNames         string[7] - names of the days from Sunday (optional)
        monthNamesShort  string[12] - abbreviated names of the months (optional)
        monthNames       string[12] - names of the months (optional)
        @return  Date - the extracted date value or null if value is blank */
        parseDate: function(format, value, settings) {
            if (format == null || value == null)
                throw 'Invalid arguments';
            value = (typeof value == 'object' ? value.toString() : value + '');
            if (value == '')
                return null;

            var dte = new Date(fomatDate(value));
            dte.setYear(this._DetermineYear(dte.getYear()));

            if (dte == "NaN") {
                return null;
            } else {
                return dte;
            }
            return null;
        },



        /* Format a date object into a string value.

	    @param   format    see formating
        @param   date      Date - the date value to format
        @Param   settings igonred
        @return  string - the date in the above format

        d Day of the month as digits; no leading zero for single-digit days.
        */
        formatDate: function(format, date, settings) {
            if (!date)
                return '';
            return date.format(format);
        },

        /* Extract all possible characters from the date format. */
        _possibleChars: function(format) {
            var chars = '';
            var literal = false;
            // Check whether a format character is doubled
            var lookAhead = function(match) {
                var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
                if (matches)
                    iFormat++;
                return matches;
            };
            for (var iFormat = 0; iFormat < format.length; iFormat++)
                if (literal)
                if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                literal = false;
            else
                chars += format.charAt(iFormat);
            else
                switch (format.charAt(iFormat)) {
                case 'd': case 'm': case 'y': case '@':
                    chars += '0123456789';
                    break;
                case 'D': case 'M':
                    return null; // Accept anything
                case "'":
                    if (lookAhead("'"))
                        chars += "'";
                    else
                        literal = true;
                    break;
                default:
                    chars += format.charAt(iFormat);
            }
            return chars;
        },

        /* Get a setting value, defaulting if necessary. */
        _get: function(inst, name) {
            return inst.settings[name] !== undefined ?
			inst.settings[name] : this._defaults[name];
        },

        /* Parse existing date and initialise date picker. */
        _setDateFromField: function(inst, noDefault) {
            if (inst.input.val() == inst.lastVal) {
                return;
            }
            var dateFormat = this._get(inst, 'dateFormat');
            var dates = inst.lastVal = inst.input ? inst.input.val() : null;
            var date, defaultDate;
            date = defaultDate = this._getDefaultDate(inst);
            var settings = this._getFormatConfig(inst);
            try {
                date = this.parseDate(dateFormat, dates, settings) || defaultDate;
            } catch (event) {
                this.log(event);
                dates = (noDefault ? '' : dates);
            }
            inst.selectedDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = date.getFullYear();
            inst.currentDay = (dates ? date.getDate() : 0);
            inst.currentMonth = (dates ? date.getMonth() : 0);
            if (!dates)
                dates = date;

            if (date.getHours() > 12) {
                inst.currentHour = date.getHours() - 12;
                inst.currentAMPM = "PM";
            }
            else {
                inst.currentHour = date.getHours();
                inst.currentAMPM = "AM";
            }

            if (date.getHours() == 12) {
                inst.currentAMPM = "PM";
            }

            if (date.getHours() == 00) {
                inst.currentHour = 12;
            }

            inst.currentMinute = date.getMinutes();

            inst.currentYear = (dates ? date.getFullYear() : 0);
            this._adjustInstDate(inst);
        },

        /* Retrieve the default date shown on opening. */
        _getDefaultDate: function(inst) {
            return this._restrictMinMax(inst,
			this._determineDate(inst, this._get(inst, 'defaultDate'), new Date()));
        },

        /* A date may be specified as an exact value or a relative one. */
        _determineDate: function(inst, date, defaultDate) {
            var offsetNumeric = function(offset) {
                var date = new Date();
                date.setDate(date.getDate() + offset);
                return date;
            };
            var offsetString = function(offset) {
                try {
                    return $.datetimepicker.parseDate($.datetimepicker._get(inst, 'dateFormat'),
					offset, $.datetimepicker._getFormatConfig(inst));
                }
                catch (e) {
                    // Ignore
                }
                var date = (offset.toLowerCase().match(/^c/) ?
				$.datetimepicker._getDate(inst) : null) || new Date();
                var year = date.getFullYear();
                var month = date.getMonth();
                var day = date.getDate();
                var pattern = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g;
                var matches = pattern.exec(offset);
                while (matches) {
                    switch (matches[2] || 'd') {
                        case 'd': case 'D':
                            day += parseInt(matches[1], 10); break;
                        case 'w': case 'W':
                            day += parseInt(matches[1], 10) * 7; break;
                        case 'm': case 'M':
                            month += parseInt(matches[1], 10);
                            day = Math.min(day, $.datetimepicker._getDaysInMonth(year, month));
                            break;
                        case 'y': case 'Y':
                            year += parseInt(matches[1], 10);
                            day = Math.min(day, $.datetimepicker._getDaysInMonth(year, month));
                            break;
                    }
                    matches = pattern.exec(offset);
                }
                return new Date(year, month, day);
            };
            date = (date == null ? defaultDate : (typeof date == 'string' ? offsetString(date) :
			(typeof date == 'number' ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : date)));
            date = (date && date.toString() == 'Invalid Date' ? defaultDate : date);
            /////////////////////=^..^=
            //            if (date) {
            //                date.setHours(0);
            //                date.setMinutes(0);
            //                date.setSeconds(0);
            //                date.setMilliseconds(0);
            //            }
            return this._daylightSavingAdjust(date);
        },

        /* Handle switch to/from daylight saving.
        Hours may be non-zero on daylight saving cut-over:
        > 12 when midnight changeover, but then cannot generate
        midnight datetime, so jump to 1AM, otherwise reset.
        @param  date  (Date) the date to check
        @return  (Date) the corrected date */
        _daylightSavingAdjust: function(date) {
            if (!date) return null;
            //////////////////////////=^..^=
            //date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0);
            return date;
        },

        /* Set the date(s) directly. */
        _setDate: function(inst, date, noChange) {
            var clear = !(date);
            var origMonth = inst.selectedMonth;
            var origYear = inst.selectedYear;
            date = this._restrictMinMax(inst, this._determineDate(inst, date, new Date()));
            inst.selectedDay = inst.currentDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = inst.currentMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = inst.currentYear = date.getFullYear();
            if ((origMonth != inst.selectedMonth || origYear != inst.selectedYear) && !noChange)
                this._notifyChange(inst);
            this._adjustInstDate(inst);
            if (inst.input) {
                inst.input.val(clear ? '' : this._formatDate(inst));
            }
        },

        /* Retrieve the date(s) directly. */
        _getDate: function(inst) {
            var startDate = (!inst.currentYear || (inst.input && inst.input.val() == '') ? null :
			this._daylightSavingAdjust(new Date(
			inst.currentYear, inst.currentMonth, inst.currentDay)));
            return startDate;
        },

        /* Generate the HTML for the current state of the date picker. */
        _generateHTML: function(inst) {
            var today = new Date();
            today = this._daylightSavingAdjust(
			new Date(today.getFullYear(), today.getMonth(), today.getDate())); // clear time
            var isRTL = this._get(inst, 'isRTL');
            var showButtonPanel = this._get(inst, 'showButtonPanel');
            var hideIfNoPrevNext = this._get(inst, 'hideIfNoPrevNext');
            var navigationAsDateFormat = this._get(inst, 'navigationAsDateFormat');
            var numMonths = this._getNumberOfMonths(inst);
            var showCurrentAtPos = this._get(inst, 'showCurrentAtPos');
            var stepMonths = this._get(inst, 'stepMonths');
            var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1);
            var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) :
			new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            var drawMonth = inst.drawMonth - showCurrentAtPos;
            var drawYear = inst.drawYear;
            if (drawMonth < 0) {
                drawMonth += 12;
                drawYear--;
            }
            if (maxDate) {
                var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(),
				maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate()));
                maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw);
                while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) {
                    drawMonth--;
                    if (drawMonth < 0) {
                        drawMonth = 11;
                        drawYear--;
                    }
                }
            }
            inst.drawMonth = drawMonth;
            inst.drawYear = drawYear;
            var prevText = this._get(inst, 'prevText');
            prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText,
			this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)),
			this._getFormatConfig(inst)));
            var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ?
			'<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.datetimepicker._adjustDate(\'#' + inst.id + '\', -' + stepMonths + ', \'M\');"' +
			' title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>' :
			(hideIfNoPrevNext ? '' : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>'));
            var nextText = this._get(inst, 'nextText');
            nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText,
			this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)),
			this._getFormatConfig(inst)));
            var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ?
			'<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.datetimepicker._adjustDate(\'#' + inst.id + '\', +' + stepMonths + ', \'M\');"' +
			' title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>' :
			(hideIfNoPrevNext ? '' : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + (isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>'));
            var currentText = this._get(inst, 'currentText');
            var gotoDate = (this._get(inst, 'gotoCurrent') && inst.currentDay ? currentDate : today);
            currentText = (!navigationAsDateFormat ? currentText :
			this.formatDate(currentText, gotoDate, this._getFormatConfig(inst)));
            var controls = (!inst.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.datetimepicker._hideDatepicker();">' + this._get(inst, 'closeText') + '</button>' : '');
            var buttonPanel = (showButtonPanel) ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (isRTL ? controls : '') +
			(this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.datetimepicker._gotoToday(\'#' + inst.id + '\');"' +
			'>' + currentText + '</button>' : '') + (isRTL ? '' : controls) + '</div>' : '';
            var firstDay = parseInt(this._get(inst, 'firstDay'), 10);
            firstDay = (isNaN(firstDay) ? 0 : firstDay);
            var showWeek = this._get(inst, 'showWeek');
            var dayNames = this._get(inst, 'dayNames');
            var dayNamesShort = this._get(inst, 'dayNamesShort');
            var dayNamesMin = this._get(inst, 'dayNamesMin');
            var monthNames = this._get(inst, 'monthNames');
            var monthNamesShort = this._get(inst, 'monthNamesShort');
            var beforeShowDay = this._get(inst, 'beforeShowDay');
            var showOtherMonths = this._get(inst, 'showOtherMonths');
            var selectOtherMonths = this._get(inst, 'selectOtherMonths');
            var calculateWeek = this._get(inst, 'calculateWeek') || this.iso8601Week;
            var defaultDate = this._getDefaultDate(inst);
            var html = '';
            for (var row = 0; row < numMonths[0]; row++) {
                var group = '';
                for (var col = 0; col < numMonths[1]; col++) {
                    var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay));
                    var cornerClass = ' ui-corner-all';
                    var calender = '';
                    if (isMultiMonth) {
                        calender += '<div class="ui-datepicker-group';
                        if (numMonths[1] > 1)
                            switch (col) {
                            case 0: calender += ' ui-datepicker-group-first';
                                cornerClass = ' ui-corner-' + (isRTL ? 'right' : 'left'); break;
                            case numMonths[1] - 1: calender += ' ui-datepicker-group-last';
                                cornerClass = ' ui-corner-' + (isRTL ? 'left' : 'right'); break;
                            default: calender += ' ui-datepicker-group-middle'; cornerClass = ''; break;
                        }
                        calender += '">';
                    }
                    calender += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + cornerClass + '">' +
					(/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : '') +
					(/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : '') +
					this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate,
					row > 0 || col > 0, monthNames, monthNamesShort) + // draw month headers
					'</div><table class="ui-datepicker-calendar"><thead>' +
					'<tr>';
                    var thead = (showWeek ? '<th class="ui-datepicker-week-col">' + this._get(inst, 'weekHeader') + '</th>' : '');
                    for (var dow = 0; dow < 7; dow++) { // days of the week
                        var day = (dow + firstDay) % 7;
                        thead += '<th' + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : '') + '>' +
						'<span title="' + dayNames[day] + '">' + dayNamesMin[day] + '</span></th>';
                    }
                    calender += thead + '</tr></thead><tbody>';
                    var daysInMonth = this._getDaysInMonth(drawYear, drawMonth);
                    if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth)
                        inst.selectedDay = Math.min(inst.selectedDay, daysInMonth);
                    var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7;
                    var numRows = (isMultiMonth ? 6 : Math.ceil((leadDays + daysInMonth) / 7)); // calculate the number of rows to generate
                    var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays));
                    for (var dRow = 0; dRow < numRows; dRow++) { // create date picker rows
                        calender += '<tr>';
                        var tbody = (!showWeek ? '' : '<td class="ui-datepicker-week-col">' +
						this._get(inst, 'calculateWeek')(printDate) + '</td>');
                        for (var dow = 0; dow < 7; dow++) { // create date picker days
                            var daySettings = (beforeShowDay ?
							beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, '']);
                            var otherMonth = (printDate.getMonth() != drawMonth);
                            var unselectable = (otherMonth && !selectOtherMonths) || !daySettings[0] ||
							(minDate && printDate < minDate) || (maxDate && printDate > maxDate);
                            tbody += '<td class="' +
							((dow + firstDay + 6) % 7 >= 5 ? ' ui-datepicker-week-end' : '') + // highlight weekends
							(otherMonth ? ' ui-datepicker-other-month' : '') + // highlight days from other months
							((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || // user pressed key
							(defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ?
                            // or defaultDate is current printedDate and defaultDate is selectedDate
							' ' + this._dayOverClass : '') + // highlight selected day
							(unselectable ? ' ' + this._unselectableClass + ' ui-state-disabled' : '') +  // highlight unselectable days
							(otherMonth && !showOtherMonths ? '' : ' ' + daySettings[1] + // highlight custom dates
							(printDate.getTime() == currentDate.getTime() ? ' ' + this._currentClass : '') + // highlight selected day
							(printDate.getTime() == today.getTime() ? ' ui-datepicker-today' : '')) + '"' + // highlight today (if different)
							((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : '') + // cell title
							(unselectable ? '' : ' onclick="DP_jQuery_' + dpuuid + '.datetimepicker._selectDay(\'#' +
							inst.id + '\',' + printDate.getMonth() + ',' + printDate.getFullYear() + ', this);return false;"') + '>' + // actions
							(otherMonth && !showOtherMonths ? '&#xa0;' : // display for other months
							(unselectable ? '<span class="ui-state-default">' + printDate.getDate() + '</span>' : '<a class="ui-state-default' +
							(printDate.getTime() == today.getTime() ? ' ui-state-highlight' : '') +
							(printDate.getTime() == currentDate.getTime() ? ' ui-state-active' : '') + // highlight selected day
							(otherMonth ? ' ui-priority-secondary' : '') + // distinguish dates from other months
							'" href="#">' + printDate.getDate() + '</a>')) + '</td>'; // display selectable date
                            printDate.setDate(printDate.getDate() + 1);
                            printDate = this._daylightSavingAdjust(printDate);
                        }
                        calender += tbody + '</tr>';
                    }
                    drawMonth++;
                    if (drawMonth > 11) {
                        drawMonth = 0;
                        drawYear++;
                    }
                    calender += '</tbody></table>' + (isMultiMonth ? '</div>' +
							((numMonths[0] > 0 && col == numMonths[1] - 1) ? '<div class="ui-datepicker-row-break"></div>' : '') : '');

                    group += calender;
                }

                html += group;

                // Hour Drop Down
                html += 'Time <select id="DP_jQuery_Hour_' + dpuuid + '">';
                for (i = 1; i < 24; i++) {
                    html += '<option value="' + i + '"';

                    if (inst.currentHour == i) {
                        html += ' selected="selected"';
                    }

                    html += '>';
                    if (i < 10) {
                        html += '0';
                    }
                    html += i + '</option>';
                }

                html += '</select>';

                // Minute Drop Down
                html += '&nbsp;: <select id="DP_jQuery_Minute_' + dpuuid + '">';
                for (i = 0; i < 60; i++) {

                    html += '<option value="' + i + '"';
                    if (inst.currentMinute == i) {
                        html += ' selected="selected"';
                    }
                    html += '>';
                    if (i < 10) {
                        html += '0';
                    }
                    html += i + '</option>';
                }
                html += '</select>';

                //AM/PM drop Down
                html += ' <select id="DP_jQuery_AMPM_' + dpuuid + '"><option value="AM"';
                if (inst.currentAMPM == "AM")
                    html += ' selected="selected"';
                html += '>AM</option><option value="PM"';
                if (inst.currentAMPM == "PM")
                    html += ' selected="selected"';
                html += '>PM</option></select>';

            }

            html += buttonPanel + ($.browser.msie && parseInt($.browser.version, 10) < 7 && !inst.inline ?
			'<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : '');
            inst._keyEvent = false;
            return html;
        },

        /* Generate the month and year header. */
        _generateMonthYearHeader: function(inst, drawMonth, drawYear, minDate, maxDate,
			secondary, monthNames, monthNamesShort) {
            var changeMonth = this._get(inst, 'changeMonth');
            var changeYear = this._get(inst, 'changeYear');
            var showMonthAfterYear = this._get(inst, 'showMonthAfterYear');
            var html = '<div class="ui-datepicker-title">';
            var monthHtml = '';
            // month selection
            if (secondary || !changeMonth)
                monthHtml += '<span class="ui-datepicker-month">' + monthNames[drawMonth] + '</span>';
            else {
                var inMinYear = (minDate && minDate.getFullYear() == drawYear);
                var inMaxYear = (maxDate && maxDate.getFullYear() == drawYear);
                monthHtml += '<select class="ui-datepicker-month" ' +
				'onchange="DP_jQuery_' + dpuuid + '.datetimepicker._selectMonthYear(\'#' + inst.id + '\', this, \'M\');" ' +
				'onclick="DP_jQuery_' + dpuuid + '.datetimepicker._clickMonthYear(\'#' + inst.id + '\');"' +
			 	'>';
                for (var month = 0; month < 12; month++) {
                    if ((!inMinYear || month >= minDate.getMonth()) &&
						(!inMaxYear || month <= maxDate.getMonth()))
                        monthHtml += '<option value="' + month + '"' +
						(month == drawMonth ? ' selected="selected"' : '') +
						'>' + monthNamesShort[month] + '</option>';
                }
                monthHtml += '</select>';
            }
            if (!showMonthAfterYear)
                html += monthHtml + (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '');
            // year selection
            if (secondary || !changeYear)
                html += '<span class="ui-datepicker-year">' + drawYear + '</span>';
            else {
                // determine range of years to display
                var years = this._get(inst, 'yearRange').split(':');
                var thisYear = new Date().getFullYear();
                var determineYear = function(value) {
                    var year = (value.match(/c[+-].*/) ? drawYear + parseInt(value.substring(1), 10) :
					(value.match(/[+-].*/) ? thisYear + parseInt(value, 10) :
					parseInt(value, 10)));
                    return (isNaN(year) ? thisYear : year);
                };
                var year = determineYear(years[0]);
                var endYear = Math.max(year, determineYear(years[1] || ''));
                year = (minDate ? Math.max(year, minDate.getFullYear()) : year);
                endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear);
                html += '<select class="ui-datepicker-year" ' +
				'onchange="DP_jQuery_' + dpuuid + '.datetimepicker._selectMonthYear(\'#' + inst.id + '\', this, \'Y\');" ' +
				'onclick="DP_jQuery_' + dpuuid + '.datetimepicker._clickMonthYear(\'#' + inst.id + '\');"' +
				'>';
                for (; year <= endYear; year++) {
                    html += '<option value="' + year + '"' +
					(year == drawYear ? ' selected="selected"' : '') +
					'>' + year + '</option>';
                }
                html += '</select>';
            }
            html += this._get(inst, 'yearSuffix');
            if (showMonthAfterYear)
                html += (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '') + monthHtml;
            html += '</div>'; // Close datepicker_header
            return html;
        },

        /* Adjust one of the date sub-fields. */
        _adjustInstDate: function(inst, offset, period) {
            var year = inst.drawYear + (period == 'Y' ? offset : 0);
            var month = inst.drawMonth + (period == 'M' ? offset : 0);
            var day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) +
			(period == 'D' ? offset : 0);
            var date = this._restrictMinMax(inst,
			this._daylightSavingAdjust(new Date(year, month, day)));
            inst.selectedDay = date.getDate();
            inst.drawMonth = inst.selectedMonth = date.getMonth();
            inst.drawYear = inst.selectedYear = date.getFullYear();
            if (period == 'M' || period == 'Y')
                this._notifyChange(inst);
        },

        /* Ensure a date is within any min/max bounds. */
        _restrictMinMax: function(inst, date) {
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            date = (minDate && date < minDate ? minDate : date);
            date = (maxDate && date > maxDate ? maxDate : date);
            return date;
        },

        /* Notify change of month/year. */
        _notifyChange: function(inst) {
            var onChange = this._get(inst, 'onChangeMonthYear');
            if (onChange)
                onChange.apply((inst.input ? inst.input[0] : null),
				[inst.selectedYear, inst.selectedMonth + 1, inst]);
        },

        /* Determine the number of months to show. */
        _getNumberOfMonths: function(inst) {
            var numMonths = this._get(inst, 'numberOfMonths');
            return (numMonths == null ? [1, 1] : (typeof numMonths == 'number' ? [1, numMonths] : numMonths));
        },

        /* Determine the current maximum date - ensure no time components are set. */
        _getMinMaxDate: function(inst, minMax) {
            return this._determineDate(inst, this._get(inst, minMax + 'Date'), null);
        },

        /* Find the number of days in a given month. */
        _getDaysInMonth: function(year, month) {
            return 32 - new Date(year, month, 32).getDate();
        },

        /* Find the day of the week of the first of a month. */
        _getFirstDayOfMonth: function(year, month) {
            return new Date(year, month, 1).getDay();
        },

        /* Determines if we should allow a "next/prev" month display change. */
        _canAdjustMonth: function(inst, offset, curYear, curMonth) {
            var numMonths = this._getNumberOfMonths(inst);
            var date = this._daylightSavingAdjust(new Date(curYear,
			curMonth + (offset < 0 ? offset : numMonths[0] * numMonths[1]), 1));
            if (offset < 0)
                date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth()));
            return this._isInRange(inst, date);
        },

        /* Is the given date in the accepted range? */
        _isInRange: function(inst, date) {
            var minDate = this._getMinMaxDate(inst, 'min');
            var maxDate = this._getMinMaxDate(inst, 'max');
            return ((!minDate || date.getTime() >= minDate.getTime()) &&
			(!maxDate || date.getTime() <= maxDate.getTime()));
        },

        /* Provide the configuration settings for formatting/parsing. */
        _getFormatConfig: function(inst) {
            var shortYearCutoff = this._get(inst, 'shortYearCutoff');
            shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
			new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
            return { shortYearCutoff: shortYearCutoff,
                dayNamesShort: this._get(inst, 'dayNamesShort'), dayNames: this._get(inst, 'dayNames'),
                monthNamesShort: this._get(inst, 'monthNamesShort'), monthNames: this._get(inst, 'monthNames')
            };
        },

        /* Format the given date for display. */
        _formatDate: function(inst, day, month, year) {
            if (!day) {
                inst.currentDay = inst.selectedDay;
                inst.currentMonth = inst.selectedMonth;
                inst.currentYear = inst.selectedYear;
                inst.currentHour = inst.selectedHour;
                inst.currentAMPM = inst.selectedAMPM;
                inst.currentMinute = inst.selectedMinute;
            }

            var Hour = inst.currentHour;
            if (Hour > 12)
                Hour = Hour - 12;
            inst.currentMonth += 1;
            var MinuteString = inst.currentMinute;
            if (MinuteString.length == 1)
                MinuteString = "0" + MinuteString;
            var DateString = '' + inst.currentMonth + '/' + inst.selectedDay + '/' + inst.selectedYear + ' ' + Hour + ':' + MinuteString + ' ' + inst.currentAMPM;
            var date = new Date(DateString);
            return this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst));
        }
    });

    /* jQuery extend now ignores nulls! */
    function extendRemove(target, props) {
        $.extend(target, props);
        for (var name in props)
            if (props[name] == null || props[name] == undefined)
            target[name] = props[name];
        return target;
    };

    /* Determine whether an object is an array. */
    function isArray(a) {
        return (a && (($.browser.safari && typeof a == 'object' && a.length) ||
		(a.constructor && a.constructor.toString().match(/\Array\(\)/))));
    };

    /* Invoke the datepicker functionality.
    @param  options  string - a command, optionally followed by additional parameters or
    Object - settings for attaching new datepicker functionality
    @return  jQuery object */
    $.fn.datetimepicker = function(options) {

        /* Initialise the date picker. */
        if (!$.datetimepicker.initialized) {
            $(document).mousedown($.datetimepicker._checkExternalClick).
			find('body').append($.datetimepicker.dpDiv);
            $.datetimepicker.initialized = true;
        }

        var otherArgs = Array.prototype.slice.call(arguments, 1);
        if (typeof options == 'string' && (options == 'isDisabled' || options == 'getDate' || options == 'widget'))
            return $.datetimepicker['_' + options + 'Datepicker'].
			apply($.datetimepicker, [this[0]].concat(otherArgs));
        if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string')
            return $.datetimepicker['_' + options + 'Datepicker'].
			apply($.datetimepicker, [this[0]].concat(otherArgs));
        return this.each(function() {
            typeof options == 'string' ?
			$.datetimepicker['_' + options + 'Datepicker'].
				apply($.datetimepicker, [this].concat(otherArgs)) :
			$.datetimepicker._attachDatepicker(this, options);
        });
    };

    $.datetimepicker = new Datetimepicker(); // singleton instance
    $.datetimepicker.initialized = false;
    $.datetimepicker.uuid = new Date().getTime();
    $.datetimepicker.version = "1.8rc3";

    // Workaround for #4055
    // Add another global to avoid noConflict issues with inline event handlers
    window['DP_jQuery_' + dpuuid] = $;

})(jQuery);

/*
* Date Format 1.2.3
*/

var RegexDateFormat = function() {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function(val, len) {
		    val = String(val);
		    len = len || 2;
		    while (val.length < len) val = "0" + val;
		    return val;
		};

    // Regexes and supporting functions are cached through closure
    return function(date, mask, utc) {
        var dF = RegexDateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad(d),
			    ddd: dF.i18n.dayNames[D],
			    dddd: dF.i18n.dayNames[D + 7],
			    m: m + 1,
			    mm: pad(m + 1),
			    mmm: dF.i18n.monthNames[m],
			    mmmm: dF.i18n.monthNames[m + 12],
			    yy: String(y).slice(2),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad(H % 12 || 12),
			    H: H,
			    HH: pad(H),
			    M: M,
			    MM: pad(M),
			    s: s,
			    ss: pad(s),
			    l: pad(L, 3),
			    L: pad(L > 99 ? Math.round(L / 10) : L),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

        return mask.replace(token, function($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
} ();

// Some common format strings
RegexDateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
RegexDateFormat.i18n = {
    dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
    monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};


// For convenience...
Date.prototype.format = function(mask, utc) {
    return RegexDateFormat(this, mask, utc);
};

