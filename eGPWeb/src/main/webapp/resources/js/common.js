/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function dynamicFromSubmit(action,targetName)
{
    //alert('action '+action);
    var myform;
    var isFormExists = 0;
    if(targetName == null || targetName == undefined || targetName == '')
    {
     targetName = '_self';
    }

    if(document.getElementById("dynamicSubmitForm"))
    {
        //alert('Form Exists');
         myform=document.getElementById("dynamicSubmitForm");
         isFormExists = 1;
    }
    else
    {
        //alert('Create new form');
        myform=document.createElement("form");
        myform.name = "dynamicSubmitForm";
        myform.id = "dynamicSubmitForm";
    }
    myform.method = "post";
    myform.target = targetName;
    myform.action = action;
    if(isFormExists == 0)
    {
        document.documentElement.appendChild(myform);
    }
    myform.submit();
}
