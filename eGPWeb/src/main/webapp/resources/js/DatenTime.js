/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    var DateVar = document.getElementById("curDate").value;
    var intDate = Date.parse(DateVar);

    var MonthName=["January", "February", "March", "April", "May", "June","July",
	"August", "September", "October", "November", "December"];

    //to convert time from UTC to IST. the difference between UTC and IST is 3.5Hrs.
    //if want to convert from GMT to IST replace 3.5 with 5.5.
    //intDate = intDate + (3600000*3.5);

    var timerID = 0;
    var cnt=0;

    var Hours = 0;
    var mins=0;

    function UpdateTimer()
    {
        cnt = cnt+1;
        if(cnt==2)
        {
            cnt=0;
        }
        intDate = intDate + 1000;

        var tDate = new Date(intDate);
        var yr = tDate.getYear();

        if(yr < 1900)
        {
            yr = yr + 1900;		//coz mozilla browser counts year from 1900.
        }

        Hours = tDate.getHours();
        mins= tDate.getMinutes();

        Hours = getFullNumber(Hours);
        mins= getFullNumber(mins);

        //document.getElementById("lblTime").innerHTML = "" + getDayName(tDate.getDay()) + " "  + getFullNumber(tDate.getDate()) + "/" + getFullNumber((parseInt(tDate.getMonth())+1)) + "/" + yr + " " + Hours + ":" + mins + ":" + getFullNumber(tDate.getSeconds()) + " BST";
        var lblTime = document.getElementById("lblTime");
        if (typeof(lblTime) !== 'undefined' && lblTime !== null)
        {
            document.getElementById("lblTime").innerHTML = "" + getDayName(tDate.getDay()) + ", "  + getFullNumber(tDate.getDate()) + " " + MonthName[parseInt(tDate.getMonth())] + ", " + yr + ", " + Hours + ":" + mins + ":" + getFullNumber(tDate.getSeconds())+" BST";
        }
        
        timerID = setTimeout("UpdateTimer()", 1000);
    }

    function Start()
    {

        var tStart = new Date(intDate);
        var yr = tStart.getYear();

        if(yr < 1900)
        {
            yr = yr + 1900;		//coz mozilla browser counts year from 1900.
        }

        Hours = tStart.getHours();
        mins= tStart.getMinutes();

        Hours = getFullNumber(Hours);
        mins= getFullNumber(mins);

        var lblTime = document.getElementById("lblTime");
        if (typeof(lblTime) !== 'undefined' && lblTime !== null)
        {
            document.getElementById("lblTime").innerHTML = "" + getDayName(tStart.getDay()) + ", "  + getFullNumber(tStart.getDate()) + " " + MonthName[parseInt(tStart.getMonth())] + ", " + yr + " " + Hours + ":" + mins + ":" + getFullNumber(tStart.getSeconds());
        }
        timerID  = setTimeout("UpdateTimer()", 1000);
    }

    function getDayName(dayCount)
    {
        switch(dayCount)
        {
            case 0:return "Sunday";
            case 1:return "Monday";
            case 2:return "Tuesday";
            case 3:return "Wednesday";
            case 4:return "Thursday";
            case 5:return "Friday";
            case 6:return "Saturday";
        }
    }

    function getFullNumber(number)
    {
        if(number < 10)
            return '0' + number;
        else
            return number;
    }