/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function AddNewTable(isBOQForm){
    /*jConfirm('Confirm It', 'Confirm', function(RetVal) {
        if (RetVal) {

        }
    });*/
    var CountTable = parseInt(document.getElementById("TableCount").value) + 1;
    var row = document.getElementById("TableRow");
    document.getElementById("TableCount").value = parseInt(document.getElementById("TableCount").value) + 1;

var str = "<td id='tdTable_"+CountTable+"'>";
    str = str + "<div class='formSubHead_1'>Enter Table Details";
    str = str + "<input type='checkbox' name='delTable' id='delTable_"+CountTable+"' />";
    str = str + "</div>";
    str = str + "<table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>";
    str = str + "<tr>";
    str = str + "<td width='20%' class='ff'>Table Name : </td>";
    str = str + " <td width='80%'><textarea name='TableName"+CountTable+"' rows='3' class='formTxtBox_1' id='TableName"+CountTable+"' style='width:200px;'></textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>Header : </td>";
    str = str + "<td><textarea cols='80' name='TableHeader"+CountTable+"' rows='10' id='TableHeader"+CountTable+"'></textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>Footer : </td>";
    str = str + "<td><textarea cols='80' name='TableFooter"+CountTable+"'  rows='10' id='TableFooter"+CountTable+"'></textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>To be Filled multiple time : <span>*</span></td>";
    str = str + "<td><input type='radio' name='MultipleFilling"+CountTable+"' id='MultipleFillingY"+CountTable+"' value='radio' />";
    str = str + "Yes <input type='radio' name='MultipleFilling"+CountTable+"' id='MultipleFillingN"+CountTable+"' value='radio' checked/>No</td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>No of Columns required : <span>*</span></td>";
    str = str + "<td><input name='NoOfCols"+CountTable+"' type='text' class='formTxtBox_1' id='NoOfCols"+CountTable+"' maxlength='2' style='width:50px;' /></td>";
    str = str + "</tr>";
    if(isBOQForm == 'true'){
        str = str + "<tr style='display:none'>";
    }else{
        str = str + "<tr>";
    }
    str = str + "<td width='125' class='ff'>No of Rows Required : <span>*</span></td>";
    str = str + "<td><input name='NoOfRows"+CountTable+"' type='text' class='formTxtBox_1' id='NoOfRows"+CountTable+"'";
    if(isBOQForm == 'true'){
        str = str + " value='0' ";
    }
    str = str + " maxlength='2' style='width:50px;' /></td>";
//  Added by Ketan :  defect id : 2378
    str = str + "<td colspan='2' style='display: none;'> Save : <input type='checkbox' name='Check' value='"+CountTable+"' id='Check_"+CountTable+"' checked /></td>";
//  End : defect id : 2378
    str = str + "</tr>";
    str = str + "</table>";
    str = str + "</td>";
    addTD(str,row,0,"tdTable_"+CountTable);

    var instanceTblH = CKEDITOR.instances["TableHeader"+CountTable];
    if(instanceTblH)
    {
        CKEDITOR.remove(instanceTblH);
    }
    CKEDITOR.replace("TableHeader"+CountTable,
    {
        fullPage : false
        //toolbar : "Basic"
    });

    var instanceTblF = CKEDITOR.instances["TableFooter"+CountTable];
    if(instanceTblF)
    {
        CKEDITOR.remove(instanceTblF);
    }
    CKEDITOR.replace("TableFooter"+CountTable,
    {
        fullPage : false
        //toolbar : "Basic"
    });
}

function DelTable(theform){
    var trTable = document.getElementById("TableRow");
    var tables = theform.TableCount.value;
    var chk, updChk;
    var tdTable;

    var tblHeader;
    var tblFooter;
    var noOfCol;
    var noOfRow;
    var delTableId;

    var valueTblHeader = new Array();
    var valueTblFooter = new Array();
    var valueNoOfCol = new Array();
    var valueNoOfRow = new Array();
    var valueDelTableId = new Array();

    var Counter=0;
    var cntDltTbl = 0;

    for(var i=0;i<tables;i++){
        chk = document.getElementById("delTable_"+(parseInt(i)+1));
        updChk = document.getElementById("Check_"+(parseInt(i)+1));
        if(chk != null){
            if(chk.checked){
                Counter++;
            }
        }
    }

    if(Counter == 0){
        jAlert("Please check at least one table to delete.",'Alert', function(bool){
            if(bool){
                return bool;
            }else{
                return bool;
            }
        });
        return false;
    }

    if(Counter == tables){
        jAlert("At least one table should remain present in form.",'Alert', function(bool){
            if(bool){
                return bool;
            }else{
                return bool;
            }
        });
        chk.checked = false;
        updChk.checked = false;
        return false;
    }

    Counter = 0;
    for(var i=0;i<tables;i++){
        chk = document.getElementById("delTable_"+(parseInt(i)+1));
        if(chk != null){
            if(chk.checked){
                cntDltTbl++;
                tdTable = document.getElementById("tdTable_"+(parseInt(i)+1));
                //alert(" tdTable " + tdTable);
                trTable.removeChild(tdTable);
            }else{
                tblHeader = document.getElementById("TableHeader"+(parseInt(i)+1));
                if(tblHeader!=null){
                    valueTblHeader[Counter] = tblHeader.value;
                }
                tblFooter = document.getElementById("TableFooter"+(parseInt(i)+1));
                if(tblFooter!=null){
                    valueTblFooter[Counter] = tblFooter.value;
                }
                noOfCol = document.getElementById("NoOfCols"+(parseInt(i)+1));
                if(noOfCol!=null){
                    valueNoOfCol[Counter] = noOfCol.value;
                }
                noOfRow = document.getElementById("NoOfRows"+(parseInt(i)+1));
                if(noOfRow!=null){
                    valueNoOfRow[Counter] = noOfRow.value;
                }
                delTableId = document.getElementById("delTable_"+(parseInt(i)+1));
                if(delTableId!=null){
                    valueDelTableId[Counter] = delTableId.value;
                }
                Counter++;
            }
        }
    }

    //alert(valueTblHeader + " :: " + valueTblFooter + " :: " + valueNoOfCol + " :: " + valueNoOfRow + " :: " + valueDelTableId);
    SwapTable(theform,valueTblHeader,valueTblFooter,valueNoOfCol,valueNoOfRow,valueDelTableId);
    theform.TableCount.value = eval(theform.TableCount.value) - eval(cntDltTbl);
    return true;
}

function SwapTable(theform,valueTblHeader,valueTblFooter,valueNoOfCol,valueNoOfRow,valueDelTableId){
    var trTable = document.getElementById("TableRow");
    var tables = theform.TableCount.value;
    var tdTable;

    var chk;
    var Counter = 0;
    for(var iTable=0;iTable<tables;iTable++){
        tdTable = document.getElementById("tdTable_"+(parseInt(iTable)+1));
        if(tdTable != null){
            tdTable.setAttribute("id","tdTable_"+(parseInt(Counter)+1));

            chk = document.getElementById("delTable_"+(parseInt(iTable)+1));
            if(chk != null){
                Counter = parseInt(Counter) +1;
                chk.removeAttribute("id");
                chk.setAttribute("id","delTable_"+parseInt(Counter))
            }
            
            tdTable.innerHTML = generateHTML(Counter,valueTblHeader[Counter-1],valueTblFooter[Counter-1],valueNoOfCol[Counter-1],valueNoOfRow[Counter-1],valueDelTableId[Counter-1]);

            try{
                var instanceTblH = CKEDITOR.instances["TableHeader"+Counter];
                if(instanceTblH)
                {
                    CKEDITOR.remove(instanceTblH);
                }
                CKEDITOR.replace("TableHeader"+Counter,
                {
                    fullPage : false
                    //toolbar : "Basic"
                });
                var instanceTblF = CKEDITOR.instances["TableFooter"+Counter];
                if(instanceTblF)
                {
                    CKEDITOR.remove(instanceTblF);
                }
                CKEDITOR.replace("TableFooter"+Counter,
                {
                    fullPage : false
                    //toolbar : "Basic"
                });
            }catch(e){
                //alert("eeee " + e);
            }
        }
    }
}

function generateHTML(CCount,valueTblHeader,valueTblFooter,valueNoOfCol,valueNoOfRow,valueDelTableId){
    var CountTable = CCount;
    var str = "<td id='tdTable_"+CountTable+"'>";
    str = str + "<div class='formSubHead_1'>Enter Table Details";
    str = str + "<input type='checkbox' name='delTable' id='delTable_"+CountTable+"' />";
    str = str + "</div>";
    str = str + "<table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>";
    str = str + "<tr>";
    str = str + "<td width='20%' class='ff'>Table Name : </td>";
    str = str + "<td width='80%'><textarea name='TableName"+CountTable+"' rows='3' class='formTxtBox_1' id='TableName"+CountTable+"' style='width:200px;'>" + valueTblHeader + "</textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>Header : </td>";
    str = str + "<td><textarea cols='80' name='TableHeader"+CountTable+"' rows='10' id='TableHeader"+CountTable+"'>" + valueTblHeader + "</textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>Footer : </td>";
    str = str + "<td><textarea cols='80' name='TableFooter"+CountTable+"' rows='10' id='TableFooter"+CountTable+"'>" + valueTblFooter + "</textarea></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>To be Filled multiple time : <span>*</span></td>";
    str = str + "<td><input type='radio' name='MultipleFilling"+CountTable+"' id='MultipleFillingY"+CountTable+"' value='radio'  />";
    str = str + "Yes <input type='radio' name='MultipleFilling"+CountTable+"' id='MultipleFillingN"+CountTable+"' value='radio' checked />No</td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>No of Columns required : <span>*</span></td>";
    str = str + "<td><input name='NoOfCols"+CountTable+"' type='text' class='formTxtBox_1' id='NoOfCols"+CountTable+"' value='"+valueNoOfCol+"'  maxlength='2' style='width:50px;' /></td>";
    str = str + "</tr>";
    str = str + "<tr>";
    str = str + "<td width='125' class='ff'>No of Rows Required : <span>*</span></td>";
    str = str + "<td><input name='NoOfRows"+CountTable+"' type='text' class='formTxtBox_1' id='NoOfRows"+CountTable+"' value='"+valueNoOfRow+"' maxlength='2' style='width:50px;' /></td>";
    str = str + "<td colspan='2' style='display: none;'> Save : <input type='checkbox' name='Check' value='"+CountTable+"' id='Check_"+CountTable+"' checked /></td>";
    str = str + "</tr>";
    str = str + "</table>";
    str = str + "</td>";
    return str;
}

function checkTableEntryOk(theform, isPriceBid){
    var noOfTables = theform.TableCount.value;
    for(var i=1;i<=eval(noOfTables);i++){
        if(document.getElementById("Check_"+i).checked){
            var cols;
            if(document.getElementById("NoOfCols"+i) != null)
                cols = document.getElementById("NoOfCols"+i);
            var rows;
            if(document.getElementById("NoOfRows"+i) != null)
                rows = document.getElementById("NoOfRows"+i);
            var Thead = document.getElementById("TableHeader"+i);
            var Tfoot = document.getElementById("TableFooter"+i);
            var TName = document.getElementById("TableName"+i);

            /*if(TName != null){
                if(trim(TName.value) == ""){
                    jAlert("Table Name Cannot Be Left Blank", 'Alert');
                    TName.focus();
                    return false;
                }
            }*/

            if(cols != null)
            {
                if(trim(cols.value)=="")
                {
                    jAlert("Please enter No. of Columns for Table", 'Alert');
                    cols.value = trim(cols.value);
                    cols.focus();
                    return false;
                }else{
                    var string=trim(cols.value);
                    var re5digit=/^[1-9][0-9]?/
                    if (!re5digit.test(string)){
                        jAlert("Please enter numbers (1-99) only", 'Alert');
                        cols.value = "";
                        cols.focus();
                        return false;
                    }
                }
            }

            if(isPriceBid == "false"){
                if(rows != null)
                {
                    if(trim(rows.value)=="")
                    {
                        jAlert("Please enter no. of rows for table.", 'Alert');
                        rows.value = trim(rows.value);
                        rows.focus();
                        return false;
                    }else{
                        var string=trim(rows.value);
                        var re5digit=/^[1-9][0-9]?/;
                        if (!re5digit.test(string)){
                            jAlert("Please Enter Numbers (1-99) Only", 'Alert');
                            rows.value = trim(rows.value);
                            rows.focus();
                            return false;
                        }
                    }
                }
            }
            
            /*if((Thead.value).length > 4000)
            {
                jAlert("Table Header Cannot be More than 4000 Characters.", 'Alert');
                Thead.value = trim(Thead.value);
                Thead.focus();
                return false;
            }
            else if((Tfoot.value).length > 4000)
            {
                jAlert("Table Footer Cannot be More than 4000 Characters.", 'Alert');
                Tfoot.value = trim(Tfoot.value);
                Tfoot.focus();
                return false;
            }*/
            if(cols != null)
            {
                if(parseInt(cols.value) == 0){
                    jAlert("Columns for Table Cannot Be Zero", 'Alert');
                    cols.value = trim(cols.value);
                    cols.focus();
                    return false;
                }
            }
        }
        }

    for(var i=1;i<=eval(noOfTables);i++){
        document.getElementById("Check_"+i).checked = true;
        }
    return true;
}