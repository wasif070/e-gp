function setFormulaTo(theform,cmb)
{
	if(cmb.value!=0)
		ApplyTo = cmb.options[cmb.selectedIndex].text  + " = ";		
	else
		ApplyTo = "";

/*	theform.ReportFormula.value=ApplyTo;	
	ReportFormula = "";
	ReportFormulaToSave="";
	ResultStr = "";
	OpenedBrace = 0;
	ClosedBrace	= 0;
	arrIds = null; 
	arrIds = new Array();	*/
	clearAll(theform);
}



function clearAll(theform)
{
	theform.ReportFormula.value=ApplyTo;
	ReportFormula = "";
	ReportFormulaToSave="";
	theform.hidReportFormula.value = "";
        theform.hideReportFormula.value = "";
	ResultStr = "";
	OpenedBrace = 0;
	ClosedBrace	= 0;
	arrIds = null; 
	arrIds = new Array();
}



function BuildFormula(theform,tBox,Tid) //When a textBox is Clicked
{
//alert(tBox.value);
	//alert("k" + tBox.location);
	if(theform.ReportFormula.value!=0)
	{
		if(arrIds.length==0)
		{                 
			setValues(theform,tBox.getAttribute('nametodisplay'),tBox.id,tBox.id);
			return;
		}
		else if(arrIds[arrIds.length-1]=="+"||arrIds[arrIds.length-1]=="*"||arrIds[arrIds.length-1]=="/"||arrIds[arrIds.length-1]=="-"||arrIds[arrIds.length-1]=="(")
		{                        
//			setValues(theform,document.getElementById("td"+tableId+"_"+cnt).title,tBox.id,tBox.id.substring(tBox.id.indexOf("Col")+3,tBox.id.length));			
			setValues(theform,tBox.getAttribute('nametodisplay'),tBox.id,tBox.id);
			return;
		}
	}
}



function setValues(theform,value1,value2,value3)
{
        theform.ReportFormula.value += value1;
	theform.hideReportFormula.value += value2;
	//alert(theform.ReportFormula.value);
	ReportFormula += value2;
	//alert(ReportFormula);
	arrIds.push(value2);
	ReportFormulaToSave += value3;
}


function addExpression(theform,btn) //When a Expression is Selected
{    
	if(theform.ReportFormula.value!=0)
	{
		if(btn.value=="(")
		{
			if(ReportFormula=="")
			{
				OpenedBrace++;
				setValues(theform,btn.value,btn.value,btn.value);
				return;
			}	
			else if(arrIds[arrIds.length-1]=="+"||arrIds[arrIds.length-1]=="*"||arrIds[arrIds.length-1]=="/"||arrIds[arrIds.length-1]=="-")
			{		
				OpenedBrace++;
				setValues(theform,btn.value,btn.value,btn.value);
				return;
			}
		}
		else if(btn.value==")")
		{
			if(arrIds[arrIds.length-1]!="+"||arrIds[arrIds.length-1]!="*"||arrIds[arrIds.length-1]!="/"||arrIds[arrIds.length-1]!="-"||arrIds[arrIds.length-1]!="("||arrIds[arrIds.length-1]!=")")
			{
				if(ClosedBrace<OpenedBrace)
				{
					ClosedBrace++;
					setValues(theform,btn.value,btn.value,btn.value);
				}
				return;
			}		
		}
		else if(btn.value=="Number")
		{
			//alert("number"+btn.value);
			var answer = prompt("Enter Number","");                        
			setValues(theform,answer,"N"+answer,"N_"+answer);
		}
		else if(arrIds[arrIds.length-1]!="+"&&arrIds[arrIds.length-1]!="*"&&arrIds[arrIds.length-1]!="/"&&arrIds[arrIds.length-1]!="-"&&arrIds[arrIds.length-1]!="Number"&&arrIds[arrIds.length-1]!="("&&arrIds.length>0)
		{
			setValues(theform,btn.value,btn.value,btn.value);
			return;
		}		
		else if(btn.value=="%")
		{
			setValues(theform,'p','p','p');
			return;
		}
	}
}


function testformula(theform)
{

	if(document.getElementById("ReportFormula").value=="")
	{		
                jAlert("Please Create Formula.","Report Alert", function(RetVal) {
                });
		return false;
	}
	
	var	cmb = document.getElementById("ApplyTo");
	ResultStr = "";

	if(theform.ReportFormula.value=="")
	{
                jAlert("Please build a formula first....","Report Alert", function(RetVal) {
                });
		return false;
	}
	else if(OpenedBrace!=ClosedBrace)	
	{
                jAlert("Some brackets r missing..","Report Alert", function(RetVal) {
                });
		return false;
	}
	else
	{
		for(var i=0;i<arrIds.length;i++)
		{
			if((arrIds[i]).match("_"))
			{                                
				if(document.getElementById(arrIds[i])!=null)
				{
					if(document.getElementById(arrIds[i]).value!="")
					{                                            
						if(CheckFloat1(document.getElementById(arrIds[i]).id.substring(0,document.getElementById(arrIds[i]).id.indexOf("_")),document.getElementById(arrIds[i]))){
							ResultStr += trim(document.getElementById(arrIds[i]).value.replace(/^[0]+/g,""));
						}else{
                                                    //TaherT july12 Taher added(return false;) for not proper formula selected
                                                    return false;
						}
					}	
					else
					{
                                                jAlert("Please fill in values in all required fields to test formula","Report Alert", function(RetVal) {
                                                });
						ResultStr = "";
						return false;
					}				
				}
				else
				{
					ResultStr += trim(arrIds[i]);
					ResultStr = ResultStr.replace('p','100');
				}
			}
			else
			{
				ResultStr += arrIds[i]; 
			}
		}
                ResultStr = ResultStr.replace(/N/g,'');                                
		if(isNaN(eval(ResultStr)))
		{
			//alert(ResultStr);
                        jAlert("Not proper formula.","Report Alert", function(RetVal) {
                        });
			return false;
		}
		else
		{			                        
                        if(theform.inToWords.checked)
                        {
                                document.getElementById("gov_"+cmb.value).value = DoIt(Math.round(eval(eval(ResultStr)*1000))/1000);
                                TestReportFormula="OK";
                        }
                        else
                        {                         
                                document.getElementById("gov_"+cmb.value).value = Math.round(eval(eval(ResultStr)*1000))/1000;
                                TestReportFormula="OK";
                        }
		}

        }  // end of for	
}


function FormulaSave(theform)
{
        if(theform.hideReportFormula.value=="")
        {
            jAlert("Please build a formula first...", 'Alert');
            return false;
        }
        
        if(TestReportFormula=="OK"&&theform.ApplyTo.value!="0")
        {
            document.getElementById("SaveFormula").style.display = "none";
            document.getElementById("hiddenSubmit").value = "Save Formula";
            return true;
        }
        else
        {
            jAlert("No Formula Set or Tested", 'Alert');
            return false;
        }
	//theform.hidReportFormula.value += ReportFormulaToSave;
	//theform.SaveTo.value = theform.ApplyTo.value;
}


function UndoChange(theform)
{
	var len = theform.ReportFormula.value.length;
	var str = new String(theform.ReportFormula.value);
	var lastChar = str.charAt(len-1);

        //for hidden field starts
	var len2 = theform.hideReportFormula.value.length;
	var str2 = new String(theform.hideReportFormula.value);
	var lastChar2 = str.charAt(len-1);
        //for hidden field ends

	if(arrIds.length>0)
	{
		if(lastChar=="+"||lastChar=="*"||lastChar=="-"||lastChar=="/")
		{
			ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str = str.substring(0,len-1);
			arrIds.pop(arrIds.length-1);
			ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		else if(lastChar=="(")
		{
			ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str = str.substring(0,len-1);
			OpenedBrace--;
			arrIds.pop(arrIds.length-1);
			ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		else if(lastChar==")")
		{
			ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str = str.substring(0,len-1);
			ClosedBrace--;
			arrIds.pop(arrIds.length-1);
			ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		else
		{
			var a = arrIds[arrIds.length-1];
			var index = arrIds[arrIds.length-1].substring(3,arrIds[arrIds.length-1].length);
			var title =document.getElementById(a).title;

			a = a.substring(3,a.length);
			str = str.substring(0,str.lastIndexOf(title));
			ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.lastIndexOf(index));
			arrIds.pop(arrIds.length-1);
		}
		theform.ReportFormula.value = str;
                //for hidden field
                if(lastChar2=="+"||lastChar2=="*"||lastChar2=="-"||lastChar2=="/")
		{
			//ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str2 = str2.substring(0,len2-1);
			//arrIds.pop(arrIds.length-1);
			//ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		else if(lastChar2=="(")
		{
			//ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str2 = str2.substring(0,len2-1);
			OpenedBrace--;
			//arrIds.pop(arrIds.length-1);
			//ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		else if(lastChar2==")")
		{
			//ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			str2 = str2.substring(0,len2-1);
			ClosedBrace--;
			//arrIds.pop(arrIds.length-1);
			//ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.length-2);
		}
		/*else
		{
			var a2 = arrIds[arrIds.length-1];
			var index2 = arrIds[arrIds.length-1].substring(3,arrIds[arrIds.length-1].length);
			var title2 =document.getElementById(a2).title;

			a2 = a2.substring(3,a2.length);
			str2 = str2.substring(0,str2.lastIndexOf(title2));
			//ReportFormula = ReportFormula.substring(0,ReportFormula.length-4);
			//ReportFormulaToSave = ReportFormulaToSave.substring(0,ReportFormulaToSave.lastIndexOf(index2));
			//arrIds.pop(arrIds.length-1);
		}*/
		theform.hideReportFormula.value = str2;
	}
}

