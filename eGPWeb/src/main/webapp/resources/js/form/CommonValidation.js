function rtbValidation(rtb){
    var rtbVal = trim(rtb.value);
    rtbVal = rtbVal.toLowerCase();
    var ret1 = rtbVal.replace(/\s/g,"");
    ret1 = ret1.replace(/\&nbsp\;/g,"");
    ret1 = ret1.replace(/align=center/g,"");
    ret1 = ret1.replace(/align=left/g,"");
    ret1 = ret1.replace(/align=right/g,"");
    ret1 = ret1.replace(/\<p\>\<\/p\>/g,"");
    if(ret1 == ""){
        return false;
    }
    return true;
}
function startWithAlphabet(textbox)
{
    var val = textbox.value;
    if (val== "") return true;
    var re  = /^[a-zA-Z]/
    var temp = val.match(re);
    if(temp == null)
        return false;
    else
        return true;
}
function imposeMaxLength(Object, MaxLen, e)// RISHITA - setting max length for text area
{
    var keyValue = (window.event)? e.keyCode : e.which;
    //var ctrl = e.ctrlKey;
//    alert('keyValue '+ keyValue + ' Ctrl '+ctrl);
    if((keyValue == 8 || (Object.value.length < MaxLen)))
     {
         return true;
     }
    return false;
}
function integerOnly(textbox)
{
    var str= textbox.value;
    var v_length = str.length;
    var str1 =str.substring(v_length-1,v_length)
    var pat = /[0-9]*/
    var ret = str1.match(pat);
    if(ret != "")
    {
        textbox.value=str;
    }
    else
    {
        jAlert("Allows Value in Numbers (0-9) & In form of (0000.00) Only",'Alert');
        textbox.value= str.match(pat);
        textbox.focus();
        return false;
    }
}

function floatOnly(textbox,fillby)
{

    if(fillby != 3)
    {
        var str= textbox.value;
        var v_length = str.length;
        var str1 =str.substring(v_length-1,v_length)
        var pat = /[0-9\.?]*/
        var ret = str1.match(pat);
        if(ret != "")
        {
            textbox.value=str;
        }
        else
        {
            jAlert("Enter valid amount",'Alert');
            textbox.value="";   // str.match(pat);
            //textbox.value=str.substring(0,v_length-1);
            textbox.focus();
            return false;
        }
    }
    return true;
}

function characterOnly(textbox)
{
   
    var str= trim(textbox.value);

    var pat = /^[a-zA-Z\s]*$/
	
    if(!pat.test(str))
    {
		
        jAlert("Alphabet only !",'Alert');
        textbox.focus();
        return false;
                
    }
    return true;
}

        
function phoneOnly(textbox){
    var string=trim(textbox.value);

    var re5digit=/^[0-9]+[-0-9]*[^\W]$/
            
            
    if (!re5digit.test(string))
    {
        jAlert("Please enter a valid Phone/Fax Number",'Alert')
        textbox.focus();
        return false;
    }

    else{
        return consecutive(string);
    }
}

function moblieOnly(textbox)
{
    var string=trim(textbox.value);
    var re5digit=/^[0-9]+[-0-9]*[^\W]$/
    if (!re5digit.test(string))
    {
        jAlert("Please enter a valid Mobile Number",'Alert')
        textbox.focus();
        return false;
    }
    else
    {
        return consecutive(string);
    }
}

function validate(field,n) 
{
    // n = 1 for only digits
    // n = 2 for digits and chars
    // n = 3 for only chars
    if(trim(field.value).length == 0)
    {
        jAlert(" Cannot be left Blank ",'Alert');
        field.value="";
        field.focus();
        return true;
    }
    var str;
    if (n==1)
    {
        var valid = "0123456789.";
        var message = "Invalid entry!  Only numbers are accepted!"
    }
    else if (n==2)
    {
        var valid = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-().@ * "
        var message = "Invalid entry!  Only characters and numbers are accepted!"
    }
    else if (n==5)
    {
        var valid = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ."
        var message = "Invalid entry!  Only characters and (.) are accepted!"
    }
    else
    {
        var valid = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var message = "Invalid entry!  Only characters are accepted!"
    }


    var ok = "yes";
    var temp;
    for (var i=0; i<field.value.length; i++)
    {
        temp = "" + field.value.substring(i, i+1);
		
        if (valid.indexOf(temp) == "-1")
        {
            alert(message);
            str = field.value.length;
            var tst = field.value.substring(0, (str) - 1);
            //field.value = tst;
			
            field.value="";
            field.focus();
            return true;
        }
    }
}

function trim(s)
{
    while (s.substring(0,1) == ' ')
    {
        s = s.substring(1,s.length);
    }

    while (s.substring(s.length-1,s.length) == ' ')
    {
        s = s.substring(0,s.length-1);
    }
    return s;
}

function checkEmail(textbox)
{
    var string=trim(textbox.value);
    var re5digit= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\_\-])+\.)+([a-zA-Z]{2,4})+$/

    if(!re5digit.test(string))
    {
        jAlert("Please enter correct email address",'Alert')
        textbox.focus();
        return false;
    }
    return true;
}

function numeric(textbox)
{

    var string=trim(textbox.value);
    var re5digit=/^([0-9])+$/

    if(string.indexOf('0') == 0 && string.indexOf('.') != 1 && string != 0)
    {
        jAlert("please remove all leading zeroes.",'Alert')
        textbox.value="";
        textbox.focus();
        return false;
    }


    if(string.length > 0)
        if (!re5digit.test(string))
        {
            jAlert("Please enter numeric value (0 to 9) only ",'Alert');
            textbox.value="";
            textbox.focus();
            return false;
        }else{
            if(eval('string') == 0){
                jAlert("Zero Value are not allowed",'Alert');
                textbox.value="";
                textbox.focus();
                return false;
            }
        }
    return true;

}

function moneywithminus1(textbox)
{
    var val=textbox.value;
    var re5digit=/^([0-9])+$/
    var v2;
    if(val == "")
        return;

    var val1;
    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && val != 0)
    {
        jAlert("please remove all leading zeros.",'Alert')
        textbox.value="";
        textbox.focus();
        return false;
    }
    if(val.indexOf('-') == 0 )
    {

        if(val.indexOf('.') != 1)
            if(val.indexOf('0') == 1 && val.indexOf('.') != 2)
            {
                jAlert("please remove all leading zeros.",'Alert')
                textbox.value="";
                textbox.focus();
                return false;
            }
        val1 = val.substring(val.indexOf('-')+1,val.length);
        if(val1.indexOf('-') > -1)
        {
            jAlert("Invalid Number Format ",'Alert')
            textbox.value="";
            textbox.focus();
            return false;
        }

    }
    if(val.indexOf('.') > -1)
    {
        val1 = val.substring(val.indexOf('.')+1,val.length);
        var val2;
        if(val1.indexOf('.') > -1)
        {
            jAlert("Please remove additional decimal (.) ",'Alert')
            textbox.value="";
            textbox.focus();
            return false;
        }
    }


    if(val.indexOf('-') != 0)
    {
        if(val.indexOf('.') != -1)
            v2 = val.substring(0,val.indexOf('.'));
        else
            v2 = val.substring(0,val.length);

        if(val.length > 0)
            if (!re5digit.test(v2))
            {
                jAlert("Please enter a  proper value ! ",'Alert');
                textbox.value="";
                textbox.focus();
                return false;
            }
    }
    if(val.indexOf('-') == 0)
    {
        if(val.indexOf('.') == -1)
        {
            v2 = val.substring(1,val.length);
        }
        else
        {
            v2 = val.substring(1,val.indexOf('.'));
        }

        if(val.length > 0)
            if (!re5digit.test(v2))
            {
                jAlert("Please enter a  proper value ! ",'Alert');
                textbox.value="";
                textbox.focus();
                return false;
            }
    }





    re5digit=/^[-][0-9]*$/

    if(!re5digit.test(val))
    {

        if(CheckFloatMoneyAll(textbox))
        {

            return true;

        }
        else
        {
            textbox.value="";
            textbox.focus();
            return false;
        }

    }

}

function CheckFloatMoneyAll(textbox)
{
    var val= textbox.value;
    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0){
        jAlert("please remove all leading zeros.",'Alert')
        textbox.value="";
        textbox.focus();
        return false;
    }
    var re5digit=/^-?[0-9]+[\.][0-9]{1,3}$/
    if(!re5digit.test(val))
    {
        if(numerictestforfloat(textbox))
        {
            return true;
        }
        else
        {
            jAlert("Allowed only Numbers and 3 digits after decimal point",'Alert');
            textbox.value="";
            textbox.focus();
            return false;
        }
    }
    else
    {
        return true;
    }

}

function numerictestforfloat(textbox)
{
    var string=textbox.value;
    var re5digit=/^([0-9])+$/

    if (!re5digit.test(string))
    {
        return false;
    }
    return true;
}

function CheckFloat(textbox)
{
    var val= textbox.value;
    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0){
        jAlert("please remove all leading zeros.",'Alert')
        textbox.value="";
        textbox.focus();
        return false;
    }
    //var re5digit=/[1-9]*\.{1}[0-9]{1,5}$/
    var re5digit=/^[0-9]+[\.][0-9]{1,3}$/
    if(!re5digit.test(val))
    {
        if(numerictestforfloat(textbox))
        {
            if(eval('val') == 0){
                jAlert("Zero Value are not allowed",'Alert');
                textbox.value="";
                textbox.focus();
                return false;
            }
            return true;
        }
        else
        {
            jAlert("Allowed only Numbers and 3 digits after decimal point",'Alert');
            textbox.value="";
            textbox.focus();
            return false;
        }
    }
    else
    {
        if(eval('val') == 0){
            jAlert("Zero Value are not allowed",'Alert');
            textbox.value="";
            textbox.focus();
            return false;
        }
        return true;
    }

}

function CheckFloatTwoDigit(tBox){
	var val= tBox.value;
	var re5digit=/^[0-9]+[\.][0-9]{1,2}$/

	if(val=="")
		return false;

	if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && val != 0){
		alert("please remove all leading zeros.")
		tBox.value="";
		tBox.focus();
		return false;
	}

	if(!re5digit.test(val) ){
		if(numerictestforfloat(tBox)){
 			return true;
		}else{
			alert("Allows only positive numerals & 2 digits after decimal (.) point.");
			tBox.value="";
			tBox.focus();
			return false;
		}
	}else{
		return true;
	}
}
function isCKEditorFieldBlank(field)
{
    var vArray = new Array();
    vArray = field.split("&nbsp;");
    var vFlag = 0;
    for(var i=0;i<vArray.length;i++)
    {
        if(vArray[i] == '' || vArray[i] == "")
        {
            continue;
        }
        else
        {
            vFlag = 1;
            break;
        }
    }
    if(vFlag == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
function checkValufor3Decimal(tBox){
    var val= tBox.value;
    var re = new RegExp("^\\d+\\.\\d{3}?$");

    if(!re.test(val)){
        jAlert("- Please enter Positive Numbers (0-9) only.<br /> - 3 Digits after decimal (.) are required.<br /> - 3rd Digit after Decimal (.) must not be Zero.<br /> - For example 1001.123","Report Alert", function(retVal) {
            tBox.value = "";
            tBox.focus();
        });
        return false;
    }

    if(val.substring(val.length - 1,val.length) == 0){
        jAlert("- Please enter Positive Numbers (0-9) only.<br /> - 3 Digits after decimal (.) are required.<br /> - 3rd Digit after Decimal (.) must not be Zero.<br /> - For example 1001.123","Report Alert", function(retVal) {
            tBox.value = "";
            tBox.focus();
        });
        return false;
    }
    return true;

}
