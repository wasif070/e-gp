/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function setFormulaTo(theform,cmb)
{
    if(parseInt(cmb.value) != 0)
        doDataTypeNeedful(cmb.value);

    if(cmb.value!=0){
        ApplyTo = cmb.options[cmb.selectedIndex].text  + " = ";
    }else{
        ApplyTo = "";
    }
    clearAll(theform);
}

function clearAll(theform)
{
    theform.Formula.value=ApplyTo;
    ForMula = "";
    FormulaToSave="";
    theform.hidFormula.value = "";
    ResultStr = "";
    OpenedBrace = 0;
    ClosedBrace	= 0;
    arrIds = null;
    arrIds = new Array();
    document.getElementById("dateButton").style.display = "none";
    document.getElementById("genButton").style.display = "table-cell";
    var checkBoxArray = new Array();
    checkBoxArray = document.getElementsByTagName('input');
    for (var i = 0; i < checkBoxArray.length; i++)
    {
        if (checkBoxArray[i].type == 'checkbox')
        {
            checkBoxArray[i].disabled = false;
        }
    }
}

function doDataTypeNeedful(colid){

}
function BuildFormula(theform,tBox,cnt,dataType) //When a textBox is Clicked
{
    var cmb =  document.getElementById("ApplyTo");
    if(eval(cnt) == eval(cmb.options[cmb.selectedIndex].value)){
        jAlert("You can not select the column on which you are building formula", 'Alert');
        return false;
    }
    if(dataType == 12)
    {
        GetCal('Col'+cnt,'Col'+cnt);
    }
    if(theform.Formula.value != 0)
    {
        if(dataType == 12)
        {
            document.getElementById("dateButton").style.display = "table-cell";
            document.getElementById("genButton").style.display = "none";
            GetCal('Col'+cnt,'Col'+cnt);
            var checkBoxArray = new Array();
            checkBoxArray = document.getElementsByTagName('input');
            for (var i = 0; i < checkBoxArray.length; i++)
            {
                if (checkBoxArray[i].type == 'checkbox')
                {
                    checkBoxArray[i].disabled = true;
                }
            }
        }
        if(arrIds.length==0)
        {
            setValues(theform,document.getElementById(cnt).getAttribute("textToDisplay"),tBox.id,tBox.id.substring(tBox.id.indexOf('Col')+3,tBox.id.length));
            return;
        }
        else if(arrIds[arrIds.length-1]=="+"||arrIds[arrIds.length-1]=="*"||arrIds[arrIds.length-1]=="/"||arrIds[arrIds.length-1]=="-"||arrIds[arrIds.length-1]=="(")
        {
            setValues(theform,document.getElementById(cnt).getAttribute("textToDisplay"),tBox.id,tBox.id.substring(tBox.id.indexOf("Col")+3,tBox.id.length));
            return;
        }
    }
}

function setValues(theform,value1,value2,value3)
{
    theform.Formula.value += value1;
    ForMula += value2;
    arrIds.push(value2);
    FormulaToSave += value3;
}

function addExpression(theform,btn) //When a Expression is Selected
{
    if(theform.Formula.value!=0)
    {
        //alert("-->>>"+btn.value+"<--");
        if(btn.value=="(")
        {
            if(ForMula=="")
            {
                OpenedBrace++;
                setValues(theform,btn.value,btn.value,btn.value);
                return;
            }
            else if(arrIds[arrIds.length-1]=="+"||arrIds[arrIds.length-1]=="*"||arrIds[arrIds.length-1]=="/"||arrIds[arrIds.length-1]=="-")
            {
                OpenedBrace++;
                setValues(theform,btn.value,btn.value,btn.value);
                return;
            }
        }
        else if(btn.value==")")
        {
            if(arrIds[arrIds.length-1]!="+"||arrIds[arrIds.length-1]!="*"||arrIds[arrIds.length-1]!="/"||arrIds[arrIds.length-1]!="-"||arrIds[arrIds.length-1]!="("||arrIds[arrIds.length-1]!=")")
            {
                if(ClosedBrace<OpenedBrace)
                {
                    ClosedBrace++;
                    setValues(theform,btn.value,btn.value,btn.value);
                }
                return;
            }
        }
        else if(btn.value=="Number")
        {
            //alert("number"+btn.value);
            var answer = prompt("Enter Number","");
            if(answer == null){
            //UndoChange(theform);
            }else{
                if(isNaN(answer)){
                    jAlert("Please Enter Number.", 'Alert');
                }else{
                    setValues(theform,answer,answer,"N_"+answer);
                }
            }
        }
        else if(arrIds[arrIds.length-1]!="+"&&arrIds[arrIds.length-1]!="*"&&arrIds[arrIds.length-1]!="/"&&arrIds[arrIds.length-1]!="-"&&arrIds[arrIds.length-1]!="Number"&&arrIds[arrIds.length-1]!="("&&arrIds.length>0)
        {
            //alert("+++++++"+btn.value);
            setValues(theform,btn.value,btn.value,btn.value);
            return;
        }
        else if(btn.value=="%")
        {
            setValues(theform,'p','p','p');
            return;
        }
    }
}

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY)

}

function testformula(theform)
{
    document.getElementById("testedornot").value = 'y';
    var	cmb = document.getElementById("ApplyTo");
    var isDate = false;
    var dateField =  new Array();
    var days;
    var dateOperator;
    var count = 0;
    ResultStr = "";
    if(theform.Formula.value=="")
    {
        jAlert("Please build a formula first...", 'Alert');
    }
    else if(OpenedBrace!=ClosedBrace)
    {
        jAlert("Some brackets are missing..", 'Alert');
    }
    else
    {
        for(var i=0;i<arrIds.length;i++)
        {
            if(document.getElementById(arrIds[i])!=null)
            {
                if(document.getElementById(arrIds[i]).value!="")
                    if(isNaN(document.getElementById(arrIds[i]).value)){
                        if(document.getElementById(arrIds[i]).value == "thisIsColIdOnly"){
                        }else{
                            var temp = arrIds[i].substring(3, arrIds[i].length);
                            if(ColDataType[eval(temp) - 1] == 12){
                                isDate = true;
                            }
                        }
                    }else if(document.getElementById(arrIds[i]).value < 0){
                    }else{
                        var temp = arrIds[i].substring(3, arrIds[i].length);
                        if(ColDataType[eval(temp) - 1] == 12){
                                isDate = true;
                        }
                    }
            }
        }
        for(var i=0;i<arrIds.length;i++)
        {
            if(document.getElementById(arrIds[i])!=null)
            {
                if(document.getElementById(arrIds[i]).value!="")
                    if(isNaN(document.getElementById(arrIds[i]).value)){
                        if(document.getElementById(arrIds[i]).value == "thisIsColIdOnly"){
                            ResultStr += arrIds[i];
                        }else{
                            var temp = arrIds[i].substring(3, arrIds[i].length);
                            if(ColDataType[eval(temp) - 1] == 3){
                                if(!CheckFloatTwoDigit(document.getElementById(arrIds[i]))){
                                    return false;
                                }
                            }else if(ColDataType[eval(temp) - 1] == 4){
                                if(!numeric(document.getElementById(arrIds[i]))){
                                    return false;
                                }
                            }else if(ColDataType[eval(temp) - 1] == 8){
                                if(!moneywithminus1(document.getElementById(arrIds[i]))){
                                    return false;
                                }
                            }
                            else if(ColDataType[eval(temp) - 1] == 12){
                                var dateArray = document.getElementById(arrIds[i]).value.split("-");
                                var date = dateArray[0];
                                var month = dateArray[1];
                                var year = dateArray[2];
//                                alert("date   : "+date);
//                                alert("month   : "+month);
//                                alert("year   : "+year);
//                                alert("final dare c: "+Date.parse(month+" "+date+", "+year));
                                isDate = true;
                                dateField[count] = Date.parse(month+" "+date+", "+year);
                                count++;
                            }
                            else if(ColDataType[eval(temp) - 1] == 13){
                                if(!checkValufor3Decimal(document.getElementById(arrIds[i]))){
                                    return false;
                                }
                            }
                            if(ColDataType[eval(temp) - 1] != 12){
                                ResultStr += document.getElementById(arrIds[i]).value;
                            }
                        }
                    }else if(document.getElementById(arrIds[i]).value < 0){
                        ResultStr += (parseFloat(document.getElementById(arrIds[i]).value));
                    }else{
                        var temp = arrIds[i].substring(3, arrIds[i].length);
                        if(ColDataType[eval(temp) - 1] == 3){
                            if(!CheckFloatTwoDigit(document.getElementById(arrIds[i]))){
                                return false;
                            }
                        }else if(ColDataType[eval(temp) - 1] == 4){
                            if(!numeric(document.getElementById(arrIds[i]))){
                                return false;
                            }
                        }else if(ColDataType[eval(temp) - 1] == 8){
                            if(!moneywithminus1(document.getElementById(arrIds[i]))){
                                return false;
                            }
                        }
                        else if(ColDataType[eval(temp) - 1] == 12){
                                var dateArray = document.getElementById(arrIds[i]).value.split("-");
                                var date = dateArray[0];
                                var month = dateArray[1];
                                var year = dateArray[2];
//                                alert("date   : "+date);
//                                alert("month   : "+month);
//                                alert("year   : "+year);
//                                alert("final dare c: "+Date.parse(month+" "+date+", "+year));
                                isDate = true;
                                dateField[count] = Date.parse(month+" "+date+", "+year);
                                count++;
                        }
                        else if(ColDataType[eval(temp) - 1] == 13){
                                if(!checkValufor3Decimal(document.getElementById(arrIds[i]))){
                                    return false;
                                }
                        }
                        if(ColDataType[eval(temp) - 1] != 12){
                                ResultStr += document.getElementById(arrIds[i]).value;
                        }
                    }
                else
                {
                    jAlert("Please fill value to test formula", 'Alert');
                    ResultStr = "";
                    return;
                }
            }
            else
            {
                if(isDate)
                {
                    dateOperator = arrIds[i];
                }
                else
                {
                    ResultStr += arrIds[i];
                    ResultStr = ResultStr.replace('p','100');
                }
            }

        }
        
        try{

            if(isDate)
            {
                var tds = document.getElementsByTagName('td');

                var cmbText1 = cmb.options[cmb.selectedIndex].text;
                var hch = cmb.options[cmb.selectedIndex].value;

                var indexOfCol;
                for(a=0;a<=colIdOrderByColId.length;a++){
                    if(hch == colIdOrderByColId[a]){
                        indexOfCol = a;
                    }
                }

                var cmbText = hch;
                var cmbText2;
                var tdTitle;
                var tdTitle1;
                var tdTitle2;

                for(var i=1;i<=totalNoOfColumns;i++)
                {
                    var objCol = document.getElementById(i);
                    tdTitle = objCol.getAttribute("title");
                    if(trim(tdTitle.replace(' ',''))==trim(cmbText.replace(' ','')))
                    {

                        if(theform.inToWords.checked)
                        {
                            if(colDataTypeOrderByColId[indexOfCol]!=3 && colDataTypeOrderByColId[indexOfCol]!=4 && colDataTypeOrderByColId[indexOfCol]!=8)
                            {
                                document.getElementById("Col"+objCol.id).value = DoIt(parseFloat(eval(ResultStr)));
                                document.getElementById("Col"+objCol.id).focus();
                                theform.hidFormula.value = "WORD(";
                                theform.hidFormula.value += FormulaToSave;
                                theform.hidFormula.value += ")";
                            }
                            else
                            {
                                jAlert("Column DataType is Number or Money..", 'Alert');
                                document.getElementById("Col"+objCol.id).value = eval(ResultStr);
                                theform.hidFormula.value += FormulaToSave;
                                theform.inToWords.checked = false;
                            }
                        }
                        else
                        {
                            if(dateField.length > 1)
                            {
                                var firstDate = dateField[0];
                                var secondDate = dateField[1];
                                var finalResult;

                                if(dateOperator == "-")
                                {
                                    finalResult =  days_between(new Date(firstDate),new Date(secondDate));
                                }
                                document.getElementById("Col"+objCol.id).value = finalResult;
                                theform.hidFormula.value = FormulaToSave;
                            }
                            else
                            {
                                var millisecondsToAdd = ResultStr  * 86400000;
                                var endDt;
                                if(dateOperator == "+")
                                endDt = new Date(dateField[0] + millisecondsToAdd);
                                else
                                endDt = new Date(dateField[0] - millisecondsToAdd);
                                var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                                var finalDate = endDt.getDate() + "-" + monthname[endDt.getMonth()] + "-" + endDt.getFullYear();
                                document.getElementById("Col"+objCol.id).value = finalDate;
                                theform.hidFormula.value = FormulaToSave;
                            }
                        }
                        theform.SaveTo.value = objCol.id;
                        break;
                    }
                }
            }
            else
            {
                if(isNaN(eval(ResultStr)))
                {
                    jAlert("Not proper formula.", 'Alert');
                    return false;
                }
                else
                {
                    var tds = document.getElementsByTagName('td');

                    var cmbText1 = cmb.options[cmb.selectedIndex].text;
                    var hch = cmb.options[cmb.selectedIndex].value;

                    var indexOfCol;
                    for(a=0;a<=colIdOrderByColId.length;a++){
                        if(hch == colIdOrderByColId[a]){
                            indexOfCol = a;
                        }
                    }

                    var cmbText = hch;
                    var cmbText2;
                    var tdTitle;
                    var tdTitle1;
                    var tdTitle2;

                    for(var i=1;i<=totalNoOfColumns;i++)
                    {
                        var objCol = document.getElementById(i);
                        tdTitle = objCol.getAttribute("title");
                        if(trim(tdTitle.replace(' ',''))==trim(cmbText.replace(' ','')))
                        {

                            if(theform.inToWords.checked)
                            {
                                if(colDataTypeOrderByColId[indexOfCol]!=3 && colDataTypeOrderByColId[indexOfCol]!=4 && colDataTypeOrderByColId[indexOfCol]!=8)
                                {
                                    document.getElementById("Col"+objCol.id).value = DoIt(parseFloat(eval(ResultStr)));
                                    document.getElementById("Col"+objCol.id).focus();
                                    theform.hidFormula.value = "WORD(";
                                    theform.hidFormula.value += FormulaToSave;
                                    theform.hidFormula.value += ")";
                                }
                                else
                                {
                                    jAlert("Column DataType is Number or Money..", 'Alert');
                                    document.getElementById("Col"+objCol.id).value = eval(ResultStr);
                                    theform.hidFormula.value += FormulaToSave;
                                    theform.inToWords.checked = false;
                                }
                            }
                            else
                            {
                                document.getElementById("Col"+objCol.id).value = Math.round(eval(parseFloat(eval(ResultStr))*100))/100;
                                theform.hidFormula.value = FormulaToSave;
                            }

                            theform.SaveTo.value = objCol.id;
                            break;
                        }
                    }
                }
            }
        }catch(ex){
            jAlert("Not Proper Formula." + ex, 'Alert');
            return false;
        }
    }

    Tested = "OK";
}

function FormulaSave(theform)
{
    if(theform.Formula.value=="")
    {
        jAlert("Please build a formula First", 'Alert');
        return false;
    }

    if(Tested=="OK"||theform.AddComboTo.value!="")
    {
        document.getElementById("SaveFormula").style.display = "none";
        document.getElementById("hiddenSubmit").value = "Save Formula";
        return true;
    }
    else
    {
        jAlert("No Formula Set or Tested", 'Alert');
        return false;
    }
}

function UndoChange(theform)
{
    var len = theform.Formula.value.length;
    var str = new String(theform.Formula.value);
    var lastChar = str.charAt(len-1);
    if(arrIds.length>0)
    {
        if(lastChar=="+"||lastChar=="*"||lastChar=="-"||lastChar=="/")
        {
            ForMula = ForMula.substring(0,ForMula.length-4);
            str = str.substring(0,len-1);
            arrIds.pop(arrIds.length-1);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.length-1);
        }
        else if(lastChar=="(")
        {
            ForMula = ForMula.substring(0,ForMula.length-4);
            str = str.substring(0,len-1);
            OpenedBrace--;
            arrIds.pop(arrIds.length-1);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.length-1);
        }
        else if(lastChar==")")
        {
            ForMula = ForMula.substring(0,ForMula.length-4);
            str = str.substring(0,len-1);
            ClosedBrace--;
            arrIds.pop(arrIds.length-1);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.length-1);
        }else if(!isNaN(lastChar)){
            var number = arrIds[arrIds.length-1];
            var numberlength = number.toString().length;
            str = str.substring(0,str.lastIndexOf(arrIds[arrIds.length-1]));
            ForMula = ForMula.substring(0,ForMula.length-numberlength);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.length-(numberlength+2));
            arrIds.pop(arrIds.length-1);
        }else if(lastChar == '%'){
            ForMula = ForMula.substring(0,ForMula.length-4);
            str = str.substring(0,len-1);
            arrIds.pop(arrIds.length-1);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.length-1);
        }else{
            var a = arrIds[arrIds.length-1];
            var index = arrIds[arrIds.length-1].substring(3,arrIds[arrIds.length-1].length);
            var title =document.getElementById(index).getAttribute("texttodisplay");

            a = a.substring(3,a.length);
            str = str.substring(0,str.lastIndexOf(title));
            ForMula = ForMula.substring(0,ForMula.length-4);
            FormulaToSave = FormulaToSave.substring(0,FormulaToSave.lastIndexOf(index));
            arrIds.pop(arrIds.length-1);
        }
        theform.Formula.value = str;
    //document.getElementById('ApplyTo').options[0].selected=true;
    }
}


function testformulaAndTotal(theform){
    var cmb = document.getElementById("ApplyTo");
    ResultStr = "";
    if(theform.Formula.value==""){
        //alert("Please build a formula first...");
        return false;
    }else if(OpenedBrace!=ClosedBrace){
        //alert("Some brackets r missing..");
        return false;
    }else{
        for(var i=0;i<arrIds.length;i++){
            if(document.getElementById(arrIds[i])!=null){
                if(document.getElementById(arrIds[i]).value!=""){
                    if(isNaN(document.getElementById(arrIds[i]).value)){
                        if(document.getElementById(arrIds[i]).value == "thisIsColIdOnly"){
                            ResultStr += eval(arrIds[i]);
                        }else{
                            ResultStr += document.getElementById(arrIds[i]).value;
                        }
                    }else{
                        ResultStr += parseFloat(document.getElementById(arrIds[i]).value);
                    }
                }else{
                    //alert("Fill All the details to Formulae.");
                    ResultStr = "";
                    return false;
                }
            }else{
                ResultStr += arrIds[i];
                ResultStr = ResultStr.replace('p','100');
            }
        }

        try{
            if(isNaN(eval(ResultStr))){
                //alert("Not proper formula.");
                return false;
            }else{
                var tds = document.getElementsByTagName('td');
                var cmbText1 = cmb.options[cmb.selectedIndex].text;
                var hch = cmb.options[cmb.selectedIndex].value;
                //added by yrj for solving sort order change during edit time problem.

                var indexOfCol;
                for(a=0;a<=colIdOrderByColId.length;a++){
                    if(hch == colIdOrderByColId[a]){
                        indexOfCol = a;
                    }
                }

                var cmbText = hch;
                var cmbText2;
                var tdTitle;
                var tdTitle1;
                var tdTitle2;

                for(var i=0;i<tds.length;i++){
                    tdTitle = tds[i].title;
                    if(trim(tdTitle.replace(' ',''))==trim(cmbText.replace(' ',''))){
                        if(theform.inToWords.checked){
                            if(colDataTypeOrderByColId[indexOfCol]!=3 && colDataTypeOrderByColId[indexOfCol]!=4 && colDataTypeOrderByColId[indexOfCol]!=8){
                                document.getElementById("Col"+tds[i].id).value = DoIt(parseFloat(eval(ResultStr)));
                                theform.hidFormula.value = "WORD(";
                                theform.hidFormula.value += FormulaToSave;
                                theform.hidFormula.value += ")";
                            }else{
                                //alert("Column DataType is Number or Money..");
                                document.getElementById("Col"+tds[i].id).value = eval(ResultStr);
                                theform.hidFormula.value += FormulaToSave;
                                theform.inToWords.checked = false;
                                return false;
                            }
                        }else{
                            document.getElementById("Col"+tds[i].id).value = parseFloat(eval(ResultStr));
                            theform.hidFormula.value = FormulaToSave;
                        }
                        theform.SaveTo.value = tds[i].id;
                        break;
                    }
                }
            }
        }catch(ex){
            return false;
        }
    }
    Tested = "nottested";
    return true;
}

function addComboInFormula(theform,check,cnt,tableId)
{
    var tbody = document.getElementById("table"+tableId).getElementsByTagName("tbody")[0];
    if(check.checked)
    {
        if(testformulaAndTotal(theform)){
            if(document.getElementById("testedornot").value == 'n'){
                jAlert("First Create And Test Formula Then Select This CheckBox And Save", 'Alert');
                check.checked = false;
                return false;
            }
        }else{
            jAlert("First Create And Test Formula Then Select This CheckBox And Save", 'Alert');
            check.checked = false;
            return false;
        }

        var Row = document.createElement("TR");
        Row.setAttribute("id","TR"+cnt);

        var Cell;
        var comp;

        for(var i=1;i<=totalNoOfColumns;i++)
        {
            if(parseInt(cnt) != parseInt(arrColIds[i-1]))
                addTD("   ",Row,1);
            else
            {
                Cell = document.createElement("TD");
                comp = document.createElement("SELECT");
                comp.options[0] = new Option("Sum","1");
                comp.setAttribute("name","Select"+cnt);
                comp.setAttribute("id",check.name);

                Cell.appendChild(comp);
                Row.appendChild(Cell);
            }
        }
        Row.setAttribute("style", "display:none");
        tbody.appendChild(Row);
        theform.AddComboTo.value += comp.name + "," ;
    }
    else
    {
        var Row = document.getElementById("TR"+cnt);
        theform.AddComboTo.value = theform.AddComboTo.value.replace('Select'+cnt+',','');
        tbody.removeChild(Row);
    }
}

function confirmDelete(theform,count)
{
    var isOneCheckboxChecked = false;
    var checkedFormula = "";

    for(var i=1;i<=count;i++)
    {
        if(document.getElementById("chkDelete"+i)){
            if(document.getElementById("chkDelete"+i).checked){
                isOneCheckboxChecked = true;
                checkedFormula += i + " , ";
            }
        }
    }

    if(isOneCheckboxChecked){
        checkedFormula = checkedFormula.substring(0,checkedFormula.length-2);
        jConfirm("Are You sure You want to delete formula no(s) : " + checkedFormula, "Confirm", function(bool){
            if(bool == true){
                theform.submit();
            //return true;
            }else{
                return false;
            }
        });
    }else{
        jAlert("Please select at least one formula to Delete", 'Alert');
        return false;
    }
}

function chkMinus5Plus5(tBox)
{
    var val= tBox.value;
    //alert(val);

    if(val=="")
    {
        return false;
    }

    var tmpArray = val.split('.');
    //alert('length '+tmpArray.length);
    //    alert(Math.round(parseFloat(tmpArray[0])));
    if(isNaN(val) || tmpArray.length > 2 ||
        (tmpArray.length == 2 && tmpArray[1].length > 3) ||
        (parseInt(tmpArray[0]) > 5 || parseInt(tmpArray[0]) < -5) ||
        ((parseInt(tmpArray[0]) >= 5 || parseInt(tmpArray[0]) <= -5) && parseInt(tmpArray[1]) > 0)
        )
        {
        jAlert("Allows -5 to +5 numerals and maximum 3 digits after decimal (.) point.","", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }

    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0)
    {
        jAlert("please remove all leading zeros.","", function(retVal) {
            //tBox.value = "";
            tBox.focus();
        });
        return false;
    }
}
