/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function buttonViewOfColumn(){
//    var anyChecked = false;
//    var inputtag = document.getElementsByTagName("input");
//    for(var inputcount=0;inputcount<inputtag.length;inputcount++){
//        if(inputtag[inputcount].type == "checkbox"){
//            if((inputtag[inputcount].id).substring(0,6) == "ChkDel"){
//                if(inputtag[inputcount].checked){
//                    anyChecked = true;
//                }
//            }
//        }
//    }
}

function addRow(theform,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue,isWorks,isDumpedorNot, isAdmin)
{
    var colCount = theform.cols.value ;
   //alert(cmbWithCalcText+"---"+cmbWithCalcValue+"---"+cmbWoCalcText+"---"+cmbWoCalcValue);
    var bCmb;
    var autoIdArray = new Array();
    theform.rows.value=parseInt(theform.rows.value) + 1;
    //alert(theform.rows.value);
    var rowCount = theform.rows.value;
    var cmb = document.getElementById("rowsort"+eval(rowCount-1));
    var tbody = document.getElementById("FormMatrix").getElementsByTagName("tbody")[0];
    var Row = document.createElement("TR");
    var tdalign;
    Row.setAttribute("id","TR"+rowCount);

    addInput(theform,"checkbox","chk"+rowCount,Row,"TD");
    addSelect(theform,"rowsort"+rowCount,cmb,Row,"td");

    for(var i=1;i<=colCount;i++)
    {
        //alert("Filled By  : "+document.getElementById("FillBy"+(i-1)).value);
        //alert("DataType  : "+document.getElementById("DataType"+(i-1)).value);
                
        if(parseInt(document.getElementById("FillBy"+(i-1)).value) == 1)
        { // Filled By - PE User
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 1)
            { //Small Text
                if(i==2 && isWorks){
                    tdalign=addInputWithBtn(theform,"text","Cell"+rowCount+"_"+i,"btn"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                }else{
                    tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                }
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 2)
            { // Long Text
                tdalign=addTextArea(theform,"Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 3)
            { // Money Positive
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 4)
            { // Numeric
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 5)
            { //
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 6)
            {

            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 8)
            { // Money All
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 7)
            { //
                tdalign=addDate(theform,"Cell"+rowCount+"_"+i,Row," ","TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 9)
            { // Combo Box with Calculation
                tdalign=addComboDataType(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i,cmbWithCalcText,cmbWithCalcValue,"comboWithCalc");
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 10)
            { // Combo Box without Calculation
                tdalign=addComboDataType(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i,cmbWoCalcText,cmbWoCalcValue,"selComboWithOutCalc");
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 11)
            { // Money All(-5 to +5)
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 12)
            { // Date
                tdalign=addDate(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 13)
            { // Money All(3 digits after decimal)
                tdalign=addInput(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
            }

            //tdalign = addHiddenVar(tdalign,"comboWithCalc"+rowCount+"_"+i,"selComboWithCalc"+rowCount+"_"+i,"comboWithOutCalc"+rowCount+"_"+i,"selComboWithOutCalc"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);
            //tdalign.setAttribute("align","center");

        }
        if(parseInt(document.getElementById("FillBy"+(i-1)).value) == 2)
        { // Filled By Tenderer
            bCmb = document.getElementById("DataType"+(i-1));
            
            if(parseInt(document.getElementById("DataType"+(i-1)).value) == 9 || parseInt(document.getElementById("DataType"+(i-1)).value) == 10){
                if(isDumpedorNot=="Dumped"){
                    tdalign = addSelectWithConditionDumped(theform,"DataType"+rowCount+"_"+i,bCmb,Row,"TD"+rowCount+"_"+i,"center",rowCount,i);
                }else{
                    tdalign = addSelectWithCondition(theform,"DataType"+rowCount+"_"+i,bCmb,Row,"TD"+rowCount+"_"+i,"center",rowCount,i, isAdmin);
                }
            }else{
                tdalign = addSelectWithCondition(theform,"DataType"+rowCount+"_"+i,bCmb,Row,"TD"+rowCount+"_"+i,"center",rowCount,i, isAdmin);
            }          
            tdalign = addHiddenVar(tdalign,"comboWithCalc"+rowCount+"_"+i,"selComboWithCalc"+rowCount+"_"+i,"comboWithOutCalc"+rowCount+"_"+i,"selComboWithOutCalc"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue, bCmb.value);
            tdalign.setAttribute("class","formTxtBox_1");
        }

        if(parseInt(document.getElementById("FillBy"+(i-1)).value) == 3)
        { // Filled By Auto

            if(document.getElementById("Cell"+(rowCount-1)+"_"+i) != null)
            {
                //alert("Cell : "+document.getElementById("Cell"+(rowCount-1)+"_"+i).type);
                if(document.getElementById("Cell"+(rowCount-1)+"_"+i).type == "text")
                {
                    addInputWithAlign(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                }
                else
                {
                    tdalign=addTextArea(theform,"Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                    tdalign.setAttribute("align","center");
                }
            }
            else
            {
                if(parseInt(document.getElementById("DataType"+(i-1)).value) == 2)
                {

                    tdalign=addTextArea(theform,"Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                    tdalign.setAttribute("align","center");
                }
                else
                {
                    addInputWithAlign(theform,"text","Cell"+rowCount+"_"+i,Row,"TD"+rowCount+"_"+i);
                }
            }
            autoIdArray.push(i);
        }
    }

    tbody.appendChild(Row);

    //    if(document.getElementById("chk"+(parseInt(rowCount))) != null){
    //        document.getElementById("chk"+(parseInt(rowCount))).onclick = function() {
    //            buttonViewOfRow()
    //        };
    //    }


    if(document.getElementById("rowsort"+rowCount) != null)
        document.getElementById("rowsort"+rowCount).setAttribute("class","formTxtBox_1");

    for(var j=0;j<autoIdArray.length;j++)
    {
        document.getElementById("Cell"+rowCount+"_"+autoIdArray[j]).disabled=true;
        document.getElementById("Cell"+rowCount+"_"+autoIdArray[j]).value="disabled";
    }

    var cmb1 = document.getElementById("rowsort"+eval(rowCount));

    cmb1.options[cmb1.length] = new Option(parseInt(rowCount),parseInt(rowCount));

    cmb1.options[cmb1.length-1].selected = true;

    for(var i=1;i<rowCount;i++)
    {
        cmb1 = document.getElementById("rowsort"+eval(i));
        cmb1.options[cmb1.length] = new Option(parseInt(cmb1.length+1),parseInt(cmb1.length+1));
        cmb1.options[i-1].selected = true;

    }
}

function delTableRow(theform)
{
   // alert(theform);
    var RowCount = parseInt(theform.rows.value);
    var ColCount = parseInt(theform.cols.value);
    var con = true;
    var Counter;
    var StartRowCount = parseInt(theform.rows.value);
    var SwapRow;
    var Cmb;
    var Chk;
    var Row;
    var delRowValue;

    var tbody = document.getElementById("FormMatrix").getElementsByTagName("tbody")[0];
    Counter = -1;

    //alert("RowCount : "+RowCount + "ColCount :  "+ColCount);
    for(var iRow=1;iRow<=parseInt(RowCount);iRow++)
    {
        Chk = document.getElementById("chk"+iRow);
        //alert("Chk  :"+Chk.checked);
        if(Chk.checked)
        {
            Counter = parseInt(Counter) + 1;
            Row = document.getElementById("TR"+iRow);

            if(parseInt(Counter+1) == parseInt(RowCount))
            {
                jAlert("No More Rows Can Be Deleted ", 'Alert' );
                Chk.checked = false;
            }
            else
            {
                if(document.getElementById("delRow").value != 0)
                    document.getElementById("delRow").value = document.getElementById("delRow").value + "_" +iRow;
                else
                    document.getElementById("delRow").value = iRow;

                tbody.removeChild(Row);
                theform.rows.value = parseInt(theform.rows.value) - 1;
            }
        }
    }
    Counter = 0;
    for(var jRow=1;jRow<=parseInt(StartRowCount);jRow++)
    {
        if(document.getElementById("TR"+jRow) != null)
        {
            Counter = Counter+1;
            SwapRow = document.getElementById("TR"+eval(jRow));
            SwapRow.setAttribute("id","TR"+eval(Counter));
            Chk = document.getElementById("chk"+jRow);
            Chk.setAttribute("id","chk"+(Counter));
            Cmb = document.getElementById("rowsort"+jRow);
            Cmb.removeAttribute("id");
            Cmb.removeAttribute("name");
            Cmb.setAttribute("id","rowsort"+(Counter));
            Cmb.setAttribute("name","rowsort"+Counter);
            Cmb.setAttribute("style","class  :formTxtBox_1;");
            var cell;
            for(var jCol=0;jCol<ColCount;jCol++)
            {
                //alert("Filled By : "+document.getElementById("FillBy"+jCol).value);
                cell = document.getElementById("TD"+parseInt(jRow)+"_"+parseInt(jCol+1));
                if(cell != null)
                {
                    cell.setAttribute("id","TD"+Counter+"_"+eval(jCol+1));

                    if(parseInt(document.getElementById("FillBy"+jCol).value)  == 1 || parseInt(document.getElementById("FillBy"+jCol).value)  == 3)
                    { // Filled By PE User or Auto

                        if(document.getElementById("Cell"+jRow+"_"+(jCol+1)) != null)
                        {
                            cell = document.getElementById("Cell"+jRow+"_"+(jCol+1));
                            cell.setAttribute("name","Cell"+Counter+"_"+eval(jCol+1));
                            cell.setAttribute("id","Cell"+Counter+"_"+eval(jCol+1));
                            if(cell.tagName == 'TEXTAREA'){
                                cell.setAttribute("class","formTxtBox_1 newTAwidth");
                            }
                            else{
                                cell.setAttribute("class","formTxtBox_1 newinputwidth");
                                cell.setAttribute("style","padding: 2px; margin: 5px;");
                            }
                        }
                    }
                    else if(parseInt(document.getElementById("FillBy"+jCol).value)  == 2)
                    { // Filled By Tenderer
                        var cmbchange;
                        if(document.getElementById("Cell"+jRow+"_"+(jCol+1)) != null)
                        {
                            cmbChange = document.getElementById("DataType"+jRow+"_"+(jCol+1));
                            cmbChange.setAttribute("name","DataType"+Counter+"_"+eval(jCol+1));
                            cmbChange.setAttribute("id","DataType"+Counter+"_"+eval(jCol+1));
                            cmbChange.setAttribute("class","formTxtBox_1");
                        }
                    }
                }
            }//end of for jcol
        }//end of if
    }//end of for jrow

    for(var iRow=1;iRow<=parseInt(theform.rows.value);iRow++)
    {
        Cmb = document.getElementById("rowsort"+iRow);
        var Opt;
        //Cmb.options.innerHTML="";
        Cmb.options.length = 0;
        for (var tempRow=1;tempRow<=parseInt(theform.rows.value);tempRow++)
        {
            Opt=document.createElement("OPTION");
            Opt.setAttribute("value",tempRow);
            Opt.innerHTML=tempRow;
            if(parseInt(tempRow) == parseInt(iRow))
            {
                Opt.setAttribute("selected","true");
            }
            Cmb.appendChild(Opt);
        }
    }
//document.getElementById("delRow1").disabled = true;
}

function addCol(theform, isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue)
{
    var tbody = document.getElementById("FormMatrix").getElementsByTagName("tbody")[0];
    var Row = document.getElementById("ChkCol");
    var Row1;

    addInputWithAlign(theform,"checkbox","ChkDel"+(parseInt(theform.cols.value) + 1),Row,"TDChkDel"+(parseInt(theform.cols.value) + 1),"center", "th");

    if(document.getElementById("TDChkDel"+(parseInt(theform.cols.value) + 1)) != null){
        document.getElementById("TDChkDel"+(parseInt(theform.cols.value) + 1)).onclick = function() {
            buttonViewOfColumn()
        };
    }

    Row=null;
    Row = document.getElementById("ColumnRow");

    var colCount = theform.cols.value = parseInt(theform.cols.value) + 1;
    var tenp = colCount+1;
    var inHT="";
    if(document.getElementById("SelectList1") != null)
    {
        inHT=document.getElementById("SelectList1").innerHTML;
    }
    else if(document.getElementById("SelectList2") != null)
    {
        inHT=document.getElementById("SelectList2").innerHTML;
    }

    var strin = "";
    strin = strin + "<table width='100%' id='TB1' border='0' cellspacing='5' cellpadding='0' class='formStyle_1'>";
    strin = strin + "<tr width='30%' id='TBR"+(colCount-1)+"'>";
    strin = strin + "<td width='70%' class='ff' id='TBD1'>Column Header :</td>";
    strin = strin + "<td id='TBD2'><textarea rows='3' class='formTxtBox_1' name=Header"+(colCount-1)+" id=Header"+(colCount-1)+ " style='width:98%;'></textarea></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Filled By :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='FillBy"+(colCount-1)+"' id='FillBy"+(colCount-1)+"' onChange='setDisable(this.form,this,"+colCount+");' style='width:150px' >";
    strin = strin + "<option value='1'>PA User</option>";
    strin = strin + "<option value='2'>Bidder/Consultant</option>";
    strin = strin + "<option value='3'>Auto</option>";
    strin = strin + "<option value='4'>Bidder Quoted Amount</option>";
    strin = strin + "<option value='5'>Discount Amount Percentage</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr id='dt"+(colCount-1)+"'>";
    strin = strin + "<td class='ff'>Data Type :</td>";
    strin = strin + "<td id='DataTypeTd"+(colCount-1)+"'><select class='formTxtBox_1' style='width:100%; padding: 2px; margin: 5px' name='DataType"+(colCount-1)+"' id='DataType"+(colCount-1)+"' onChange='changeAllCombo(this.form,this,"+colCount+")' style='width:150px'>";
    strin = strin + "<option value='1'>Small Text</option>";
    strin = strin + "<option value='2'>Long Text</option>";
    strin = strin + "<option value='3'>Money Positive</option>";
    strin = strin + "<option value='4'>Numeric</option>";
    strin = strin + "<option value='8'>Money All</option>";
    strin = strin + "<option value='9'>Combo Box with Calculation</option>";
    strin = strin + "<option value='10'>Combo Box w/o Calculation</option>";
    strin = strin + "<option value='11'>Money All (-5 to +5)</option>";
    strin = strin + "<option value='12'>Date</option>";
    strin = strin + "<option value='13'>Money Positive(3 digits after decimal)</option>";
    strin = strin + "</select>";
    strin = strin + "<span id='comboWithCalc"+(colCount-1)+"' style='display: none;'>";
    strin = strin + "<select name='selComboWithCalc"+(colCount-1)+"' id='selComboWithCalc"+(colCount-1)+"'  class='formTxtBox_1'>"

    var len1 = cmbWithCalcText.length;
    for(i=0;i<len1;i++)
    {
        strin = strin + "<option value='"+cmbWithCalcValue[i]+"'>"+cmbWithCalcText[i]+"</option>";
    }
    strin = strin + "</select>";
    strin = strin + "</span>";
    strin = strin + "<span id='comboWithOutCalc"+(colCount-1)+"' style='display: none;'>";
    strin = strin + "<select name='selComboWithOutCalc"+(colCount-1)+"' id='selComboWithOutCalc"+(colCount-1)+"' onchange='changecomboWOCal(this.selectedIndex,"+(colCount-1)+")'  class='formTxtBox_1'>"
    var len2 = cmbWoCalcText.length;
    for(i=0;i<len2;i++)
    {
        strin = strin + "<option value='"+cmbWoCalcValue+"'>"+cmbWoCalcText[i]+"</option>";
    }
    strin = strin + "</select>";
    strin = strin + "</span>";
    strin = strin + "</td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Show / Hide :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='ShowOrHide"+(colCount-1)+"' id='ShowOrHide"+(colCount-1)+"' onchange='changecombowithCal(this.selectedIndex,"+(colCount-1)+")' style='width:150px'>";
    strin = strin + "<option value='1'>Show</option>";
    strin = strin + "<option value='2'>Hide</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    if(isBoqForm == 'false'){
        strin = strin + "<tr style='display:none'>";
    }else{
        strin = strin + "<tr>";
    }
    strin = strin + "<td class='ff'>Add Column Type :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='columnType"+(colCount-1)+"' id='columnType"+(colCount-1)+"' style='width:150px'>";
    strin = strin + "<option value='1'>Normal</option>";
    strin = strin + "<option value='2'>Qty</option>";
    strin = strin + "<option value='3'>Qty by Tenderer</option>";
    strin = strin + "<option value='4'>EE Unit Rate</option>";
    strin = strin + "<option value='5'>EE Total Rate</option>";
    strin = strin + "<option value='6'>Unit Rate</option>";
    strin = strin + "<option value='7'>Total Rate</option>";
    strin = strin + "<option value='8'>Currency</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Sort Order :</td>";
    strin = strin + "<td><input type='text' class='formTxtBox_1' id='SortOrder"+(colCount-1)+"' name='SortOrder"+(colCount-1)+"' value='"+colCount+"' style='width:25px;' /></td>";
    strin = strin + "</tr>";
    strin = strin + "</table>";

    addTD(strin,Row,1,"addTD"+colCount);

    //var listtd = document.getElementById("ListBoxValue0");
    /*if(listtd != null)
	{
		Row1 = document.getElementById("dt"+(colCount-1));

		addTD(listtd.innerHTML,Row1,1,"ListBoxValue"+(colCount-1));
		document.getElementById("ListBoxValue"+(colCount-1)).style.display='none';
	}*/
    var RowCount = theform.rows.value;
    var tdalign;
    for(var i=1;i<=RowCount;i++)
    {
        Row = document.getElementById("TR"+i);
        tdalign = addInput(theform,"text","Cell"+i+"_"+colCount,Row,"TD"+i+"_"+colCount);
        tdalign = addHiddenVar(tdalign,"comboWithCalc"+i+"_"+colCount,"selComboWithCalc"+i+"_"+colCount,"comboWithOutCalc"+i+"_"+colCount,"selComboWithOutCalc"+i+"_"+colCount,Row,"TD"+i+"_"+colCount,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);
        document.getElementById("TD"+i+"_"+colCount).align="center";
    }
    document.getElementById("addTD"+colCount).colid = colCount;
}

function delTableCol(theform,isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue)
{
    var tbody = document.getElementById("FormMatrix").getElementsByTagName("tbody")[0];

    var Row;
    var cols = theform.cols.value;
    var rows = theform.rows.value;
    var chk;
    var td;
    var filby;
    var dType;
    var sType;
    var HText;
    var colType;
    var valueFillBy = new Array();
    var valueDataType = new Array();
    var valueShowHide = new Array();
    var valueHeader = new Array();
    var valueColType = new Array();
    var Counter=0;
    var colIdDeleted;

    alert("Please check formula table for selected column(s) before delete column(s). \nFor check formula table click on CANCEL in next confirmation box. Then click on Form dashboard and goto Edit Table matrix to check.\nNOTE:\nIf any column(s) are exists in formula from selected column, then first remove formula for all those column(s).\nIf you are keep formula as it in formula table then View Form, Test form and View table Matrix will not work.");
    if(confirm("Are you sure you want to delete?"))
    {
            for(var i=0;i<cols;i++)
            {

                chk = document.getElementById("ChkDel"+(parseInt(i)+1));
                Row = document.getElementById("ColumnRow");
                if(chk != null)
                {
                    if(chk.checked)
                    {
                        td = document.getElementById("addTD"+(parseInt(i)+1));
                        colIdDeleted = document.getElementById("addTD"+(parseInt(i)+1)).getAttribute("colid");

                        if(td != null)
                        {
                            Row.removeChild(td);
                        }
                        for(var j=0;j<rows;j++)
                        {
                            Row = document.getElementById("TR"+(parseInt(j)+1));
                            td = document.getElementById("TD"+(parseInt(j)+1)+"_"+(parseInt(i)+1));
                            if(td != null)
                            {
                                Row.removeChild(td);
                            }
                        }
                        Row = document.getElementById("ChkCol");
                        td = document.getElementById("TDChkDel"+(parseInt(i)+1));

                        Row.removeChild(td);
                        theform.delCol.value = parseInt(theform.delCol.value) + 1;
                        if(document.getElementById("delColArr").value != 0)
                        {
                            document.getElementById("delColArr").value = document.getElementById("delColArr").value + "_" + (parseInt(colIdDeleted));
                        }
                        else
                        {
                            document.getElementById("delColArr").value=(parseInt(colIdDeleted));
                        }
                    }
                    else
                    {
                        filby = document.getElementById("FillBy"+i);
                        if(filby != null)
                        {
                            valueFillBy[Counter] = filby.value;
                        }

                        dType = document.getElementById("DataType"+i);
                        if(dType != null)
                        {
                            valueDataType[Counter] = dType.value;
                        }
                        sType = document.getElementById("ShowOrHide"+i);
                        if(sType != null)
                        {
                            valueShowHide[Counter] = sType.value;
                        }
                        HText = document.getElementById("Header"+i);
                        if(HText != null)
                        {
                            valueHeader[Counter] = HText.value;
                        }
                        Counter = parseInt(Counter) +1;
                    }

                }
        }

        swapCol(theform,valueFillBy,valueDataType,valueShowHide,valueHeader, valueColType,isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);

        theform.cols.value = parseInt(theform.cols.value) - parseInt(theform.delCol.value);
        theform.delCol.value = 0;
    }
    else
    {
        return false;
    }
}

function swapCol(theform,valueFillBy,valueDataType,valueShowHide,valueHeader, valueColType,isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue)
{

    var tbody = document.getElementById("FormMatrix").getElementsByTagName("tbody")[0];
    var Cols = theform.cols.value;
    var td;
    var Rows = theform.rows.value;
    var Counter=0;


    for(var jRow=1;jRow<=Rows;jRow++)
    {
        Counter=0;

        if(document.getElementById("TR"+jRow) != null)
        {
            for(var jCol=0;jCol<Cols;jCol++)
            {
                if(document.getElementById("FillBy"+jCol) != null)
                {
                    Counter = Counter+1;
                    if(parseInt(document.getElementById("FillBy"+jCol).value)  == 1 || parseInt(document.getElementById("FillBy"+jCol).value)  == 3)
                    {
                        var cell;
                        if(document.getElementById("Cell"+jRow+"_"+(jCol+1)) != null)
                        {
                            cell = document.getElementById("Cell"+jRow+"_"+(jCol+1));
                            cell.setAttribute("name","Cell"+jRow+"_"+Counter);
                            cell.setAttribute("id","Cell"+jRow+"_"+Counter);
                            cell.setAttribute("class","formTxtBox_1");
                        }

                    }
                    else if(parseInt(document.getElementById("FillBy"+jCol).value)  == 2)
                    {
                        var cmbChange;
                        if(document.getElementById("Cell"+jRow+"_"+(jCol+1)) != null)
                        {
                            cmbChange = document.getElementById("DataType"+jRow+"_"+(jCol+1));
                            cmbChange.setAttribute("name","DataType"+jRow+"_"+Counter);
                            cmbChange.setAttribute("id","DataType"+jRow+"_"+Counter);
                            cmbChange.setAttribute("class","formTxtBox_1;t-align-center");
                        }
                        else if(document.getElementById("DataType"+jRow+"_"+(jCol+1)) != null)
                        {
                            cmbChange = document.getElementById("DataType"+jRow+"_"+(jCol+1));
                            cmbChange.setAttribute("name","DataType"+jRow+"_"+Counter);
                            cmbChange.setAttribute("id","DataType"+jRow+"_"+Counter);
                            cmbChange.setAttribute("class","formTxtBox_1");
                        }

                    }

                    if(document.getElementById("TD"+jRow+"_"+(jCol+1)) != null)
                    {
                        td = document.getElementById("TD"+jRow+"_"+(jCol+1));
                        td.setAttribute("id","TD"+jRow+"_"+Counter);
                    }

                }//end of if null
            }//end of for jcol
        }//end of if
    }//end of Row

    var td;
    var Row;
    var chk;
    Counter = 0;
    for(var iCol=0;iCol<Cols;iCol++)
    {

        td = document.getElementById("TDChkDel"+(parseInt(iCol)+1));
        if(td != null)
        {
            td.setAttribute("id","TDChkDel"+(parseInt(Counter)+1));

            chk = document.getElementById("ChkDel"+(parseInt(iCol)+1));

            // STARTED FROM HERE FOR LAST BUG OF DELETE COLUMN
            td.removeChild(chk);
            var chkNew = document.createElement("input");
            chkNew.setAttribute("type", "checkbox");
            chkNew.setAttribute("id","ChkDel"+(parseInt(Counter)+1));
            chkNew.setAttribute("name","ChkDel"+(parseInt(Counter)+1));
            chkNew.onclick = function() {
                buttonViewOfColumn()
            };
            td.appendChild(chkNew);
            // ENDEDED HERE FOR LAST BUG OF DELETE COLUMN

            if(chk != null)
            {
                Counter = parseInt(Counter) +1;
                chk.removeAttribute("id");
                chk.setAttribute("id","ChkDel"+parseInt(Counter))
            //chk.innerHTML = "id = 'ChkDel"+parseInt(Counter) +"'";

            }
            td = document.getElementById("addTD"+(parseInt(iCol)+1));

            if(td != null)
            {
                td.setAttribute("id","addTD"+parseInt(Counter));

                //var listtd = document.getElementById("ListBoxValue0");

                //var abc = listtd.innerHTML;
                td.innerHTML = generateHTML(Counter,valueFillBy[Counter-1],valueDataType[Counter-1],valueShowHide[Counter-1],valueHeader[Counter-1], valueColType[Counter-1],isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);
            /*if(listtd != null)
				{
					var Row1 = document.getElementById("dt"+(Counter-1));
					//alert(listtd.innerHTML + " ::in  " + abc);
					addTD(abc,Row1,1,"ListBoxValue"+(Counter-1),false);
					document.getElementById("ListBoxValue"+(Counter-1)).style.display='none';
				}*/

            }

        }
    }

}

function generateHTML(CCount,valueFillBy,valueDataType,valueShowHide,valueHeader, valueColType,isBoqForm,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue)
{
    var colCount = CCount;
    var inHt="";

    var a = document.getElementById("delColArr").value;
    var arrdel = new Array();
    var arrNotDel = new Array();
    arrdel = a.split("_");

    for(var iCount=1;iCount<=document.frmTableCreation.cols.value;iCount++)
    {
        arrNotDel[iCount] = 0;
        for(var inloop=0;inloop<arrdel.length;inloop++)
        {
            if(iCount == arrdel[inloop])
                arrNotDel[iCount] = arrdel[inloop];
        }
    }


    for(var newCount=colCount;newCount<=document.frmTableCreation.cols.value;newCount++)
    {

        if(arrNotDel[newCount] == 0)
        {

            if(document.getElementById("SelectList"+newCount) != null)
            {
                inHt = document.getElementById("SelectList"+newCount).innerHTML;
                break;
            }

        }
    }

    var strin = "";
    strin = strin + "<table width='100%' id='TB1' border='0' cellspacing='5' cellpadding='0' class='formStyle_1'>";
    strin = strin + "<tr id='TBR"+colCount+"'>";
    strin = strin + "<td width='30%' class='ff' id='TBD1'>Column Header :</td>";
    strin = strin + "<td width='70%' id='TBD2'><textarea rows='3' class='formTxtBox_1' name=Header"+(colCount-1)+" id=Header"+(colCount-1)+ " style='width:98%;'>"+valueHeader +"</textarea></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Filled By :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='FillBy"+(colCount-1)+"' id='FillBy"+(colCount-1)+"' onChange='setDisable(this.form,this,"+colCount+");' style='width:150px;'>";
    strin = strin + "<option value='1' " + ((parseInt(valueFillBy) == 1)?' selected ':' ')+">PA User</option>";
    strin = strin + "<option value='2' " + ((parseInt(valueFillBy) == 2)?' selected ':' ')+">Bidder/Consultant</option>";
    strin = strin + "<option value='3' " + ((parseInt(valueFillBy) == 3)?' selected ':' ')+">Auto</option>";
    strin = strin + "<option value='4' " + ((parseInt(valueFillBy) == 4)?' selected ':' ')+">Bidder Quoted Amount</option>";
    strin = strin + "<option value='5' " + ((parseInt(valueFillBy) == 5)?' selected ':' ')+">Discount Amount Percentage</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr id='dt"+(colCount-1)+"'>";
    strin = strin + "<td class='ff'>Data Type :</td>";
    strin = strin + "<td id='DataTypeTd"+(colCount-1)+"'><select class='formTxtBox_1' name='DataType"+(colCount-1)+"' id='DataType"+(colCount-1)+"' onChange='changeAllCombo(this.form,this,"+colCount+")' style='width:150px;'>";
    strin = strin + "<option value='1' "+ ((parseInt(valueDataType) == 1)?' selected ':' ') +">Small Text</option>";
    strin = strin + "<option value='2' "+ ((parseInt(valueDataType) == 2)?' selected ':' ') +">Long Text</option>";
    strin = strin + "<option value='3' "+ ((parseInt(valueDataType) == 3)?' selected ':' ') +">Money Positive</option>";
    strin = strin + "<option value='4' "+ ((parseInt(valueDataType) == 4)?' selected ':' ') +">Numeric</option>";
    strin = strin + "<option value='8' "+ ((parseInt(valueDataType) == 8)?' selected ':' ') +">Money All</option>";
    strin = strin + "<option value='9'>Combo Box with Calculation</option>";
    strin = strin + "<option value='10'>Combo Box w/o Calculation</option>";
    strin = strin + "<option value='11' "+ ((parseInt(valueDataType) == 11)?' selected ':' ') +">Money All (-5 to +5)</option>";
    strin = strin + "<option value='12' "+ ((parseInt(valueDataType) == 12)?' selected ':' ') +">Date</option>";
    strin = strin + "<option value='13' "+ ((parseInt(valueDataType) == 13)?' selected ':' ') +">Money Positive(3 digits after decimal)</option>";
    strin = strin + "</select>";
    strin = strin + "<span id='comboWithCalc"+(colCount-1)+"' style='display: none;'>";
    strin = strin + "<select name='selComboWithCalc"+(colCount-1)+"' id='selComboWithCalc"+(colCount-1)+"'  class='formTxtBox_1'>"

    var len1 = cmbWithCalcText.length;
    for(i=0;i<len1;i++)
    {
        strin = strin + "<option value='"+cmbWithCalcValue[i]+"'>"+cmbWithCalcText[i]+"</option>";
    }
    strin = strin + "</select>";
    strin = strin + "</span>";
    strin = strin + "<span id='comboWithOutCalc"+(colCount-1)+"' style='display: none;'>";
    strin = strin + "<select name='selComboWithOutCalc"+(colCount-1)+"' id='selComboWithOutCalc"+(colCount-1)+"' onchange='changecomboWOCal(this.selectedIndex,"+(colCount-1)+")'  class='formTxtBox_1'>"
    var len2 = cmbWoCalcText.length;
    for(i=0;i<len2;i++)
    {
        strin = strin + "<option value='"+cmbWoCalcValue+"'>"+cmbWoCalcText[i]+"</option>";
    }
    strin = strin + "</select>";
    strin = strin + "</span>";
    strin = strin + "</td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Show / Hide :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='ShowOrHide"+(colCount-1)+"' id='ShowOrHide"+(colCount-1)+"' onchange='changecombowithCal(this.selectedIndex,"+(colCount-1)+")' style='width:150px'>";
    strin = strin + "<option value='1' "+ ((parseInt(valueShowHide) == 1)? ' selected ':' ') +">Show</option>";
    strin = strin + "<option value='2' "+ ((parseInt(valueShowHide) == 2)? ' selected ':' ') +">Hide</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    if(isBoqForm == 'false'){
        strin = strin + "<tr style='display:none'>";
    }else{
        strin = strin + "<tr>";
    }
    strin = strin + "<td class='ff'>Add Column Type :</td>";
    strin = strin + "<td><select class='formTxtBox_1' name='columnType"+(colCount-1)+"' id='columnType"+(colCount-1)+"' style='width:150px;'>";
    strin = strin + "<option value='1' "+ ((parseInt(valueColType) == 1)? ' selected ':' ') +">Normal</option>";
    strin = strin + "<option value='2' "+ ((parseInt(valueColType) == 2)? ' selected ':' ') +">Qty</option>";
    strin = strin + "<option value='3' "+ ((parseInt(valueColType) == 3)? ' selected ':' ') +">Qty by Tenderer</option>";
    strin = strin + "<option value='4' "+ ((parseInt(valueColType) == 4)? ' selected ':' ') +">EE Unit Rate</option>";
    strin = strin + "<option value='5' "+ ((parseInt(valueColType) == 5)? ' selected ':' ') +">EE Total Rate</option>";
    strin = strin + "<option value='6' "+ ((parseInt(valueColType) == 6)? ' selected ':' ') +">Unit Rate</option>";
    strin = strin + "<option value='7' "+ ((parseInt(valueColType) == 7)? ' selected ':' ') +">Total Rate</option>";
    strin = strin + "<option value='8' "+ ((parseInt(valueColType) == 8)? ' selected ':' ') +">Currency</option>";
    strin = strin + "</select></td>";
    strin = strin + "</tr>";
    strin = strin + "<tr>";
    strin = strin + "<td class='ff'>Sort Order :</td>";
    strin = strin + "<td><input type='text' class='formTxtBox_1' id='SortOrder"+(colCount-1)+"' name='SortOrder"+(colCount-1)+"' value='"+colCount+"' style='width:25px;' /></td>";
    strin = strin + "</tr>";
    strin = strin + "</table>";

    return strin;
}

function getTempStringForId(obj){
    var objId = obj.id;
    var tempStrforId = objId.substr(8, objId.length);
    return tempStrforId;
}

function setDisable(theform,cmb,cnt)
{
    var tBoxName,tBox,imageBox;
    var RowCount = document.getElementById("rows").value;

    var Cell;
    var cmb1,originalCmb;
    var dataTypeCmb = document.getElementById("DataType"+eval(cnt-1));
    originalCmb = document.getElementById("DataType"+(cnt-1));
    /*
    if(cmb.value!=2)
    {
        dataTypeCmb.options[5].disabled = true;
        dataTypeCmb.options[6].disabled = true;
    }
    else
    {
        dataTypeCmb.options[5].disabled = false;
        dataTypeCmb.options[6].disabled = false;
    }
*/
    if(cmb.value!=2 && dataTypeCmb.value==6){
        jAlert("You can't select Combo box", "Alert");
        cmb.value=2;
        return false;
    }

    if(cmb.value==2) //FillBy = Tenderer
    {
        var i;
        //if(document.getElementById("ListBoxValue"+(parseInt(cnt)-1)) != null)
        //{
        //    document.getElementById("ListBoxValue"+(parseInt(cnt)-1)).style.display='none';
        //}
        //alert("Filled BY  : Tenderer  : RowCount  : "+RowCount);
        for(i=1;i<=RowCount;i++)
        {

            if(document.getElementById('comboWithCalc'+i+"_"+cnt) && dataTypeCmb.value==9)
            {
                document.getElementById('comboWithCalc'+i+"_"+cnt).style.display = '';
            }
            if(document.getElementById('comboWithOutCalc'+i+"_"+cnt) && dataTypeCmb.value==10)
            {
                document.getElementById('comboWithOutCalc'+i+"_"+cnt).style.display = '';
            }

            if(originalCmb.value == 6)
            {
                tBoxName = "DataType"+i+"_"+cnt;
                tBox = document.getElementById(tBoxName);
                if(tBox == null)
                {
                    tBoxName = "Cell"+i+"_"+cnt;
                    tBox = document.getElementById(tBoxName);
                }
            }
            else
            {
                tBoxName = "Cell"+i+"_"+cnt;
                tBox = document.getElementById(tBoxName);
                if(tBox == null)
                {
                    tBoxName = "DataType"+i+"_"+cnt;
                    tBox = document.getElementById(tBoxName);
                }
                if(originalCmb.value == 12)
                {
                    imageBox = "imgCell"+i+"_"+cnt;
                    if(document.getElementById(imageBox)){
                        document.getElementById(imageBox).style.display = 'none';
                    }
                }
            }

            cmb1 = document.createElement("SELECT");
            cmb1.setAttribute("name","DataType"+i+"_"+cnt);
            cmb1.setAttribute("id","DataType"+i+"_"+cnt);
            cmb1.setAttribute("class","formTxtBox_1");
            cmb1.setAttribute("style","width: 98%");

            var mainCmbId = parseInt(cnt)-1;
            cmb1.onchange = function(){
                checkParentVal("DataType"+(i-1)+"_"+cnt,'DataType'+mainCmbId,this.value,"'"+i+"_"+cnt+"'")
                };

            for(var j=0;j<originalCmb.length;j++)
            {
                cmb1.options[j] = new Option(originalCmb.options[j].text,originalCmb.options[j].value);
            }

            //cmb1.options[originalCmb.value-1].selected=true;		// Commented for removing combobox and listbox... yrj
            cmb1.options[originalCmb.selectedIndex].selected=true;				// added for above reason.
            cmb1.onchange = function () {
                checkParentVal("DataType"+getTempStringForId(this),"DataType"+(cnt-1),this.value, getTempStringForId(this));
            }
            Cell = document.getElementById("TD"+i+"_"+cnt);
            Cell.replaceChild(cmb1,tBox);
        }

    }

    if(cmb.value == 1) // Fillby  = PE
    {
        var i;
        tBoxName = "Cell"+cnt;
        tBox = document.getElementById(tBoxName);
        // when change from Tender to PE then set default data type as small text
        if(document.getElementById('comboWithCalc'+(cnt-1)))
        {
            document.getElementById('comboWithCalc'+(cnt-1)).style.display = 'none';
        }
        if(document.getElementById('comboWithOutCalc'+(cnt-1)))
        {
            document.getElementById('comboWithOutCalc'+(cnt-1)).style.display = 'none';
        }
        dataTypeCmb.value = 1; // small text

        //if(document.getElementById("ListBoxValue"+(parseInt(cnt)-1)) != null)
        //{
        //    document.getElementById("ListBoxValue"+(parseInt(cnt)-1)).style.display='none';
        //}
        if(tBox == null)
        {

            for(i=1;i<=RowCount;i++)
            {
                cmb1 = document.getElementById("DataType"+i+"_"+cnt);
                if(document.getElementById('comboWithCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithCalc'+i+"_"+cnt).style.display = 'none';
                }
                if(document.getElementById('comboWithOutCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithOutCalc'+i+"_"+cnt).style.display = 'none';
                }

                if(cmb1 != null)
                {
                    if(parseInt(document.getElementById("DataType"+(cnt-1)).value) == 2)
                    {
                        tBoxName = "Cell"+i+"_"+cnt;
                        tBox = document.createElement("TEXTAREA");
                        tBox.setAttribute("name","Cell"+i+"_"+cnt);
                        tBox.setAttribute("id","Cell"+i+"_"+cnt);
                        tBox.setAttribute("class","formTxtBox_1");
                        tBox.setAttribute("rows","3");
                        tBox.setAttribute("style","width:98%;");
                    }
                    else
                    {
                        tBoxName = "Cell"+i+"_"+cnt;
                        tBox = document.createElement("INPUT");
                        tBox.setAttribute("type","text");
                        tBox.setAttribute("name","Cell"+i+"_"+cnt);
                        tBox.setAttribute("id","Cell"+i+"_"+cnt);
                        tBox.setAttribute("class","formTxtBox_1");
                        tBox.setAttribute("style","width:98%; height: 30px");
                        tBox.onblur = function() {
                            validateThisObject(this);
                        };
                    }

                    Cell = document.getElementById("TD"+i+"_"+cnt);
                    Cell.replaceChild(tBox,cmb1);

                }
                else
                {
                    tBoxName = "Cell"+i+"_"+cnt;
                    tBox = document.getElementById(tBoxName);
                    tBox.value="";
                    tBox.disabled=false;
                    tBox.onblur = function() {
                        validateThisObject(this);
                    };

                    var tNewBox;
                    tNewBox    = document.createElement("INPUT");
                    tNewBox.setAttribute("type","text");
                    tNewBox.setAttribute("name","Cell"+i+"_"+cnt);
                    tNewBox.setAttribute("id","Cell"+i+"_"+cnt);
                    tNewBox.setAttribute("class","formTxtBox_1");
                    tNewBox.setAttribute("style","width:98%; height: 30px");

                  //  tNewBox = "<input type='text' class='formTxtBox_1' name='Cell"+i+"_"+cnt+"' id='Cell"+i+"_"+cnt+"' style='width:98%; height: 30px'> "

                    Cell = document.getElementById("TD"+i+"_"+cnt);
                    //Cell1_1
                    Cell.replaceChild(tNewBox, tBox);
                }
            }
        }
        else
        {
            for(i=1;i<=RowCount;i++)
            {
                tBoxName = "Cell"+i+"_"+cnt;
                tBox = document.getElementById(tBoxName);
                tBox.value="";
                tBox.disabled=false;
                tBox.onblur = function() {
                    validateThisObject(this);
                };
            }
        }
    }

    if(cmb.value == 3) // Filby = Auto
    {
        tBoxName = "Cell1_"+cnt;

        tBox = document.getElementById(tBoxName);
        // when change from Tender to Auto then set default data type as small text
        if(document.getElementById('comboWithCalc'+(cnt-1)))
        {
            document.getElementById('comboWithCalc'+(cnt-1)).style.display = 'none';
        }
        if(document.getElementById('comboWithOutCalc'+(cnt-1)))
        {
            document.getElementById('comboWithOutCalc'+(cnt-1)).style.display = 'none';
        }

        dataTypeCmb.value = 1; // small text

        //if(document.getElementById("ListBoxValue"+(parseInt(cnt)-1)) != null)
        //{
        //    document.getElementById("ListBoxValue"+(parseInt(cnt)-1)).style.display='none';
        //}

        if(tBox!=null)
        {

            for(i=1;i<=RowCount;i++)
            {
                if(document.getElementById('comboWithCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithCalc'+i+"_"+cnt).style.display = 'none';
                }
                if(document.getElementById('comboWithOutCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithOutCalc'+i+"_"+cnt).style.display = 'none';
                }

                tBoxName = "Cell"+i+"_"+cnt;
                tBox = document.getElementById(tBoxName);
                tBox.value="disabled";
                tBox.disabled=true;
                tBox.setAttribute("class","formTxtBox_1 t-align-center");

                var tNewBox;
                tNewBox    = document.createElement("INPUT");
                tNewBox.setAttribute("type","text");
                tNewBox.setAttribute("name","Cell"+i+"_"+cnt);
                tNewBox.setAttribute("id","Cell"+i+"_"+cnt);
                tNewBox.setAttribute("class","formTxtBox_1");
                tNewBox.setAttribute("style","width:98%; height: 30px");


                Cell = document.getElementById("TD"+i+"_"+cnt);
                Cell.replaceChild(tNewBox,tBox);
                tNewBox.disabled=true;
                tNewBox.value="disabled";

                imageBox = "imgCell"+i+"_"+cnt;
                if(document.getElementById(imageBox)){
                    document.getElementById('imgCell'+i+'_'+cnt).style.display = 'none';
                //document.getElementById("TD"+i+"_"+cnt).removeChild(document.getElementById('imgCell'+i+'_'+cnt));
                }

            }
        }
        else
        {
            var i;

            for(i=1;i<=RowCount;i++)
            {
                if(document.getElementById('comboWithCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithCalc'+i+"_"+cnt).style.display = 'none';
                }
                if(document.getElementById('comboWithOutCalc'+i+"_"+cnt))
                {
                    document.getElementById('comboWithOutCalc'+i+"_"+cnt).style.display = 'none';
                }

                tBoxName = "Cell"+i+"_"+cnt;
                tBox = document.createElement("INPUT");
                tBox.setAttribute("type","text");
                tBox.setAttribute("name","Cell"+i+"_"+cnt);
                tBox.setAttribute("id","Cell"+i+"_"+cnt);
                tBox.setAttribute("class","formTxtBox_1");
                tBox.setAttribute("style","width:98%; height: 30px");

                cmb1 = document.getElementById("DataType"+i+"_"+cnt);

                Cell = document.getElementById("TD"+i+"_"+cnt);
                Cell.replaceChild(tBox,cmb1);
                tBox.disabled=true;
                tBox.value="disabled";

                imageBox = "imgCell"+i+"_"+cnt;
                if(document.getElementById(imageBox)){
                    document.getElementById('imgCell'+i+'_'+cnt).style.display = 'none';
                //document.getElementById("TD"+i+"_"+cnt).removeChild(document.getElementById('imgCell'+i+'_'+cnt));
                }
            }
        }

    }
}

function changeAllCombo(theform,cmb,index)
{

    var i;
    var RowCount = document.getElementById("rows").value;
    var ColCount = document.getElementById("cols").value;
    var Row;
    var FillBy;
    var FillValue;
    var td;
    var originalCmb;
    var cmb1;
    var tBoxName;
    var tBox;
    var Cell;
    var DataType;
    var imageBox;
    //document.getElementById("ListBoxValue"+(parseInt(index)-1)).style.display='none';
    FillBy = document.getElementById("FillBy"+(index-1));

    //if((cmb.value==9 || cmb.value==10) && FillBy.value!=2){
    if((cmb.value==9 || cmb.value==10) && FillBy.value!=2){
        jAlert("You can't select Combo box", "Alert");
        cmb.value=1;
        return false;
    }

        if(FillBy != null){
            if(FillBy.value == 1){ // Fillby = PE
                for(var iRow=1;iRow<=RowCount;iRow++){
                    Row = document.getElementById("TR"+iRow);
                    td = document.getElementById("TD"+iRow+"_"+index);
                    DataType = document.getElementById("DataType"+(index-1));
                    //alert("DataType : "+DataType.value);

                    //btn = document.getElementById("btnCombo");
                    if(DataType.value == 1 || DataType.value == 3 || DataType.value == 4 || DataType.value == 8 || DataType.value == 11 || DataType.value  == 13){
                        td.innerHTML="";
                        if(DataType.value == 1){
                            td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%; height: 30px'> "
                        }else{
                            td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%; height: 30px' onBlur='validateThisObject(this);' >"
                        }
                    }

                    if(DataType.value == 2){
                        td.innerHTML="";
                        td.innerHTML = "<textarea rows='3' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%;'></textarea>"
                    }

                    if(DataType.value == 7){
                        td.innerHTML="";
                        var strin;
                        strin = "<input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' size='10' maxlength='11'> ";
                        strin = strin + "<a href= javascript:NewCal('Cell"+iRow+"_"+index+"','ddmmyyyy',false,12);>";
                        strin = strin + " <IMG height=20 src='images/calendar.gif' width=22 border=0></a>";

                        td.innerHTML = strin;
                    }
                    if(DataType.value == 12){
                        td.innerHTML="";
                        var strin;
                        strin = "<table border='1' width='100%' cellspacing='0' cellpadding='0'><tr><td width='70%'><input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' class='formTxtBox_1' ";
                        strin = strin + "style='width:80%;height: 20px;align:left' readonly='true' onfocus=GetCal('Cell"+iRow+"_"+index+"','Cell"+iRow+"_"+index+"') /></td>";
                        strin = strin + "<td><a onclick=GetCal('Cell"+iRow+"_"+index+"','Cell"+iRow+"_"+index+"') id='imgCell"+iRow+"_"+index+"' name='imgCell"+iRow+"_"+index+"' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick=GetCal('Cell"+iRow+"_"+index+"','imgCell"+iRow+"_"+index+"') id='imgCell"+iRow+"_"+index+"' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a></td></tr></table>";

                        td.innerHTML = strin;
                    }
                }
            }

            if(FillBy.value == 3){ // Fillby = Auto
                for(var iRow=1;iRow<=RowCount;iRow++){
                    Row = document.getElementById("TR"+iRow);
                    td = document.getElementById("TD"+iRow+"_"+index);
                    DataType = document.getElementById("DataType"+(index-1));

                    //btn = document.getElementById("btnCombo");
                    if(DataType.value == 1 || DataType.value == 3 || DataType.value == 4 || DataType.value == 8 || DataType.value == 11 || DataType.value == 12 || DataType.value == 13){
                        td.innerHTML="";
                        td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' value='disabled' disabled style='width:98%; height: 30px'> "
                        imageBox = "imgCell"+i+"_"+index;
                        if(document.getElementById(imageBox)){
                            document.getElementById('imgCell'+i+'_'+index).style.display = 'none';
                            //document.getElementById("TD"+i+"_"+cnt).removeChild(document.getElementById('imgCell'+i+'_'+cnt));
                        }
                    }

                    if(DataType.value == 2){
                        td.innerHTML="";
                        td.innerHTML = "<textarea rows='3' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' disabled style='width:98%;'>disabled</textarea>"
                    }

                    if(DataType.value == 7){
                        td.innerHTML="";
                        var strin;
                        strin = "<input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' size='10' maxlength='11'> ";
                        strin = strin + "<a href= javascript:NewCal('Cell"+iRow+"_"+index+"','ddmmyyyy',false,12);>";
                        strin = strin + " <IMG height=20 src='images/calendar.gif' width=22 border=0></a>";

                        td.innerHTML = strin;
                    }
                }


            }
        }

      //  return false;
    //}

    var i =0;
    if(FillBy.value==2 || FillBy.value==1){ // when FillBy Tenderer
   // if(FillBy.value==2){ // when FillBy Tenderer
        var parentNode = '';
        // comboWithCalc
        if(document.getElementById("comboWithCalc"+(index-1))){
            document.getElementById("comboWithCalc"+(index-1)).style.display = "none";
        }
        for(i=1;i<=RowCount;i++){
            var cellIndex = i+"_"+index;
            if(document.getElementById("comboWithCalc"+cellIndex))
            {
                document.getElementById("comboWithCalc"+cellIndex).style.display = "none";
            }
            else  //when combo with calculation is removed
            {
                if(document.getElementById("DataType"+cellIndex)!=null){
                parentNode = document.getElementById("DataType"+cellIndex).parentNode;
                    if(parentNode.nodeName == 'TD')
                    {
                        var childId = 'DataType'+cellIndex;
                        var parentId = 'DataType'+(index-1);
                        var appendData = '<br /><span id="comboWithCalc'+cellIndex+'" style="display:none;">';
                        appendData = appendData + "<select style='display: inline;' name='selComboWithCalc"+cellIndex+"' id='selComboWithCalc"+cellIndex+"' class='formTxtBox_1' onchange=checkParentVal('"+childId+"','"+parentId+"',this.value,'"+i+"_"+index+"');></select></span>";

                        parentNode.innerHTML = parentNode.innerHTML + appendData;

                        originalCmb = document.getElementById("selComboWithCalc"+(index-1));
                        var tmpCmb  = document.getElementById("selComboWithCalc"+cellIndex);
                        for(var j=0;j<originalCmb.length;j++){
                            tmpCmb.options[j] = new Option(originalCmb.options[j].text,originalCmb.options[j].value);
                        }
                        document.getElementById("selComboWithCalc"+cellIndex).onclick = function () {
                        }
                    }
                }
            }
        }

        // comboWithOutCalc
        if(document.getElementById("comboWithOutCalc"+(index-1))){
            document.getElementById("comboWithOutCalc"+(index-1)).style.display = "none";
        }
        for(i=1;i<=RowCount;i++){
            var cellIndex = i+"_"+index;
            if(document.getElementById("comboWithOutCalc"+cellIndex))
            {
                document.getElementById("comboWithOutCalc"+cellIndex).style.display = "none";
            }
            else //when combo without calculation is removed
            {
                if(document.getElementById("DataType"+cellIndex)!=null){
                parentNode = document.getElementById("DataType"+cellIndex).parentNode;
                    if(parentNode.nodeName == 'TD')
                    {
                        var childId = 'DataType'+cellIndex;
                        var parentId = 'DataType'+(index-1);
                        var appendData = '<span id="comboWithOutCalc'+cellIndex+'" style="display:none;">';
                        appendData = appendData + "<select style='display: inline;' name='selComboWithOutCalc"+cellIndex+"' id='selComboWithOutCalc"+cellIndex+"' class='formTxtBox_1' onchange=checkParentVal('"+childId+"','"+parentId+"',this.value,'"+i+"_"+index+"');></select></span>";

                        parentNode.innerHTML = parentNode.innerHTML + appendData;

                        originalCmb = document.getElementById("selComboWithOutCalc"+(index-1));
                        var tmpCmb  = document.getElementById("selComboWithOutCalc"+cellIndex);
                        for(var j=0;j<originalCmb.length;j++){
                            tmpCmb.options[j] = new Option(originalCmb.options[j].text,originalCmb.options[j].value);
                        }
                    }
                }
            }
        }

        //alert(parentNode.innerHTML);
        if(cmb.value==9){// when combo with calculation
            if(document.getElementById("comboWithCalc"+(index-1))){
                document.getElementById("comboWithCalc"+(index-1)).style.display = '';
            }
            for(i=1;i<=RowCount;i++){
                if(document.getElementById("comboWithCalc"+i+"_"+index))
                {

                    document.getElementById("comboWithCalc"+i+"_"+index).style.display = '';
                }
            }
        }else if(cmb.value==10){ //when combo without calculation
            document.getElementById("comboWithOutCalc"+(index-1)).style.display = '';
            for(i=1;i<=RowCount;i++){
                if(document.getElementById("comboWithOutCalc"+i+"_"+index))
                {
                    document.getElementById("comboWithOutCalc"+i+"_"+index).style.display = '';
                }
            }
        }
    }

    //	if(cmb.selectedIndex==5)			Commented because checkbox and listbox is removed... yrj
    if(cmb.options[cmb.selectedIndex].value==6){	// Added because of above reason.
        FillBy = document.getElementById("FillBy"+(index-1));
        //document.getElementById("ListBoxValue"+(parseInt(index)-1)).style.display='block';

        if(FillBy.value == 1){ // Fillby = PE
            for(i=1;i<=RowCount;i++){
                originalCmb = document.getElementById("DataType"+(index-1));

                tBoxName = "Cell"+i+"_"+index;
                tBox = document.getElementById(tBoxName);

                cmb1 = document.createElement("SELECT");
                cmb1.setAttribute("name","DataType"+i+"_"+index);
                cmb1.setAttribute("id","DataType"+i+"_"+index);
                cmb1.setAttribute("className","formTxtBox_1");
                cmb1.setAttribute("style","width:98%");

                for(var j=0;j<originalCmb.length;j++){
                    cmb1.options[j] = new Option(originalCmb.options[j].text,originalCmb.options[j].value);
                }
                cmb1.options[originalCmb.value-1].selected=true;

                Cell = document.getElementById("TD"+i+"_"+index);
                Cell.replaceChild(cmb1,tBox);
            }
        }
    }

    for(i=1;i<=RowCount;i++){
        for(var j=1;j<=ColCount;j++){
            var cmb1 = document.getElementById("DataType"+i+"_"+j);

            if(cmb1!=null){
                //                var mainCmbId = parseInt(cnt)-1;
                //                cmb1.onchange = function(){checkParentVal("DataType"+i+"_"+cnt,'DataType'+mainCmbId,this.value)};
                if(parseInt(j) == parseInt(index)){
                    //cmb1.options[cmb.value-1].selected=true;		Commented because checkbox and listbox is removed... yrj
                    cmb1.options[cmb.selectedIndex].selected=true;		// Added because of above reason.
                }
            }
        }
    }

    FillBy = document.getElementById("FillBy"+(index-1));
    if(FillBy != null){
        if(FillBy.value == 1){ // Fillby = PE
            for(var iRow=1;iRow<=RowCount;iRow++){
                Row = document.getElementById("TR"+iRow);
                td = document.getElementById("TD"+iRow+"_"+index);
                DataType = document.getElementById("DataType"+(index-1));

                //btn = document.getElementById("btnCombo");
                if(DataType.value == 1 || DataType.value == 3 || DataType.value == 4 || DataType.value == 8 || DataType.value == 11 || DataType.value == 13){
                    td.innerHTML="";
                    if(DataType.value == 1){
                        td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%; height: 30px'> "
                    }else{
                        td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%; height: 30px' onBlur='validateThisObject(this);' >"
                    }
                }

                if(DataType.value == 2){
                    td.innerHTML="";
                    td.innerHTML = "<textarea rows='3' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' style='width:98%;'></textarea>"
                }

                if(DataType.value == 7){
                    td.innerHTML="";
                    var strin;
                    strin = "<input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' size='10' maxlength='11'> ";
                    strin = strin + "<a href= javascript:NewCal('Cell"+iRow+"_"+index+"','ddmmyyyy',false,12);>";
                    strin = strin + " <IMG height=20 src='images/calendar.gif' width=22 border=0></a>";

                    td.innerHTML = strin;
                }
                if(DataType.value == 12){
                    td.innerHTML="";
                    var strin;
                    strin = "<input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' class='formTxtBox_1' ";
                    strin = strin + "style='width:80%;height: 20px' readonly='true' onfocus=GetCal('Cell"+iRow+"_"+index+"','Cell"+iRow+"_"+index+"') />";
                    strin = strin + "&nbsp;<a onclick=GetCal('Cell"+iRow+"_"+index+"','Cell"+iRow+"_"+index+"') id='imgCell"+iRow+"_"+index+"' name='imgCell"+iRow+"_"+index+"' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick=GetCal('Cell"+iRow+"_"+index+"','imgCell"+iRow+"_"+index+"') id='imgCell"+iRow+"_"+index+"' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>";
                    td.innerHTML = strin;
                }
            }
        }

        if(FillBy.value == 3){ // Fillby = Auto
            for(var iRow=1;iRow<=RowCount;iRow++){
                Row = document.getElementById("TR"+iRow);
                td = document.getElementById("TD"+iRow+"_"+index);
                DataType = document.getElementById("DataType"+(index-1));

                //btn = document.getElementById("btnCombo");
                if(DataType.value == 1 || DataType.value == 3 || DataType.value == 4 || DataType.value == 8 || DataType.value == 11 || DataType.value == 12 || DataType.value == 13){
                    td.innerHTML="";
                    td.innerHTML = "<input type='text' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' value='disabled' disabled style='width:98%; height: 30px'> "
                }

                if(DataType.value == 2){
                    td.innerHTML="";
                    td.innerHTML = "<textarea rows='3' class='formTxtBox_1' name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' disabled style='width:98%;'>disabled</textarea>"
                }

                if(DataType.value == 7){
                    td.innerHTML="";
                    var strin;
                    strin = "<input name='Cell"+iRow+"_"+index+"' id='Cell"+iRow+"_"+index+"' type='text' size='10' maxlength='11'> ";
                    strin = strin + "<a href= javascript:NewCal('Cell"+iRow+"_"+index+"','ddmmyyyy',false,12);>";
                    strin = strin + " <IMG height=20 src='images/calendar.gif' width=22 border=0></a>";

                    td.innerHTML = strin;
                }
            }


        }
    }
}

function validateThisObject(obj){
    if(trim(obj.value) == ""){
        return false;
    }
    var colId = obj.id.substring(obj.id.indexOf("_")+1, obj.id.length);
    var fillBy = document.getElementById("FillBy"+(colId-1)).value;
    var dataType = document.getElementById("DataType"+(colId-1)).value;
    if(eval(fillBy) == 1){
        if(eval(dataType) == 3){
            if(!CheckFloat(obj)){
                return false;
            }
        }
        if(eval(dataType) == 4){
            if(!numeric(obj)){
                return false;
            }
        }
        if(eval(dataType) == 8){
                if(!moneywithminus1(obj)){
                return false;
            }
        }
        if(eval(dataType) == 11){
            if(!chkMinus5Plus5(obj)){
                return false;
            }
        }
        if(eval(dataType) == 13){
            if(!checkValufor3Decimal(obj)){
                return false;
            }
        }
    }
}



function checkValidData(theform)
{
    var count1=parseInt(theform.cols.value);
    var ftemp,dtemp,ctemp;
    var trow = parseInt(theform.rows.value);
    var atemp = 0;
    var val;
    var val1;
    var c= count1-1;
    var d= count1-2;
    var coun = 0;
    var count = 0;
    var con = 0;
    if(document.getElementById("columnType"+c)){
        val = document.getElementById("columnType"+c).value;
    }
    if(document.getElementById("columnType"+d)){
        val1 = document.getElementById("columnType"+d).value;
    }
    var trow = parseInt(theform.rows.value);
    for(var i=0;i<parseInt(count1);i++)
    {
        if(document.getElementById("Header"+i).style.display!="none")
        {
            temp=document.getElementById("Header"+i);
            if(trim(temp.value) == "")
            {
                jAlert("Please enter Column Title",'Alert');
                document.getElementById("Header"+i).focus();

                return false;
            }
            else if((temp.value.length) > 200)
            {
                jAlert("Header for Column " + (i+1) + " Cannot Be More than 200 Characters",'Alert');
                return false;
            }
        }
    }

    for(i=0;i<parseInt(count1);i++)
    {
        if(document.getElementById("Header"+i).style.display!="none")
        {
            temp=document.getElementById("Header"+i);
            if(trim(temp.value) == "")
            {
                jAlert("Please enter Column Title",'Alert');
                document.getElementById("Header"+i).focus();

                return false;
            }
            else if((temp.value.length) > 200)
            {
                jAlert("Header for Column " + (i+1) + " Cannot Be More than 200 Characters",'Alert');
                return false;
            }
        }
    }


    //    alert(val);
    //    alert(val1);
    if(parseInt(val) == 8 || parseInt(val1) == 8)
    {
        jAlert("you need to add column.",'Alert');
        return false;

    }

    for(var i=0;i<parseInt(count1);i++)
    {
        con++;

        if(document.getElementById("columnType"+i).value==8)
        {
            coun= i+1;
            count=i+2;
            var column1 = document.getElementById("Header"+coun).value;
            var column2 = document.getElementById("Header"+count).value
            if(document.getElementById("FillBy"+coun).value==2 && document.getElementById("FillBy"+count).value==3){

            }
            else
            {
                jAlert("column " + column1 +" must be tenderer fill and column "+ column2 +" must be auto fill",'Alert');
                return false;
            }
            if(document.getElementById("columnType"+i).value==document.getElementById("columnType"+con).value){
                jAlert("no same consecutive column type allows",'Alert');
                return false;

            }
        }

        coun = 0;
        count=0;


    }

    for(var i=1;i<=parseInt(trow);i++)
    {
        for(var j=1;j<=parseInt(count1);j++)
        {
            if(parseInt(document.getElementById("FillBy"+(j-1)).value) == 1)
            { // Filled By PE User
                temp=document.getElementById("Cell"+i+"_"+j).value;
                if(trim(temp) == "")
                {
                    jAlert("Please enter row description",'Alert');
                    document.getElementById("Cell"+i+"_"+j).focus();
                    return false;
                }
            }
        }
    }



    for(var i=1;i<=parseInt(trow);i++) // row wise loop
    {
        for(var j=1;j<=parseInt(count1);j++) // column wise loop
        {
            if(parseInt(document.getElementById("DataType"+(j-1)).value) == 9)
            {
                if(parseInt(document.getElementById("selComboWithCalc"+i+"_"+j).value) == 0)
                {
                  //  alert("(selComboWithCalc"+i+"_"+j+")===="+document.getElementById("selComboWithCalc"+i+"_"+j).value);
                    jAlert("Please Select Combo Value",'Alert');
                    document.getElementById("selComboWithCalc"+i+"_"+j).focus();
                        return false;
                }
            }
           
            if(parseInt(document.getElementById("DataType"+(j-1)).value) == 10)
            {
                if(parseInt(document.getElementById("selComboWithOutCalc"+i+"_"+j).value) == 0)
                {
                   // alert("(selComboWithOutCalc"+i+"_"+j+")===="+document.getElementById("selComboWithOutCalc"+i+"_"+j).value);
                    jAlert("Please Select Combo Value",'Alert');
                    document.getElementById("selComboWithOutCalc"+i+"_"+j).focus();
                        return false;
                }
            }
            
        }
    }

    

    for(var i=0;i<parseInt(count1);i++)
    {
        temp=document.getElementById("FillBy"+i).value;
        if(temp==1)
        { // Filled By PE User
            dtemp=document.getElementById("DataType"+i).value;
            if(dtemp == 1){ // DataType - Small Text
                for(var j=1;j<=parseInt(trow);j++){
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));
                    var val= trim(ctemp.value);
                    if(val.length > 1000){
                        jAlert("Characters size must not exceed 1000 characters",'Alert');
                        ctemp.value = val.substring(0, 1000);
                        ctemp.focus();
                        return false;
                    }
                }
            }

            if(dtemp == 2){ // DataType - Long Text
                for(var j=1;j<=parseInt(trow);j++){
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));
                    var val= trim(ctemp.value);
                    if(val.length > 5000){
                        jAlert("Characters size must not exceed 5000 characters",'Alert');
                        ctemp.value = val.substring(0, 2000);
                        ctemp.focus();
                        return false;
                    }
                }
            }

            if(dtemp == 3)
            { // DataType - Money Positive
                for(var j=1;j<=parseInt(trow);j++)
                {
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));

                    if(!CheckFloat(ctemp))
                    {
                        return false;
                    }
                }
            }
            if(dtemp == 8)
            { // DataType - Money All
                for(var j=1;j<=parseInt(trow);j++)
                {
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));

                    if(!moneywithminus1(ctemp))
                    {
                        //ctemp.focus();
                        return false;
                    }
                }
            }
            if(dtemp == 4)
            { // // DataType - Numeric
                for(var j=1;j<=parseInt(trow);j++)
                {
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));

                    if(numeric(ctemp) == false)
                    {
                        ctemp.focus();
                        return false;
                    }
                }
            }
            if(dtemp == 12){ // DataType - Date
                for(var j=1;j<=parseInt(trow);j++){
                    ctemp=document.getElementById("Cell"+j+"_"+(i+1));
                    var val= ctemp.value;
                    if(val == null){
                        jAlert("Please select Date",'Alert');
                        ctemp.focus();
                        return false;
                    }
                }
            }
        }
        else if(temp==2)
        { // Filled By Tenderer
            dtemp=document.getElementById("DataType"+i).value;
            for(var j=1;j<=parseInt(trow);j++)
            {
//                if(dtemp==9){
//                    ctemp=document.getElementById("selComboWithCalc"+j+"_"+(i+1));
//                }
//                if(dtemp==10){
//                    ctemp=document.getElementById("selComboWithOutCalc"+j+"_"+(i+1));
//                }
//                if(ctemp){
//                    if(ctemp.value==0)
//                    {
//                        jAlert("Please Select Combo Value",'Alert');
//                        if(dtemp==9){
//                            document.getElementById("selComboWithCalc"+j+"_"+(i+1)).focus();
//                        }
//                        if(dtemp==10){
//                            document.getElementById("selComboWithOutCalc"+j+"_"+(i+1)).focus();
//                        }
//                        return false;
//                    }
//                }
            }
        }
    }

    for(var i=0;i<parseInt(count1);i++)
    {
        var SortOrder = document.getElementById("SortOrder"+i);
        if(numeric(SortOrder)==false)
            return false;
    }

    if(checkSortOrder(count1) == true){

    }else{
        return false;
    }

    if(checkSortOrderRow(trow) == true){

    }else{
        for(i=1;i<=parseInt(trow);i++){
            document.getElementById("rowsort"+i).value = i;
        }
        return false;
    }
         /* Doahtec Start */
    var alertMessage="Are You sure You want to edit this Table?";
    if(document.getElementById("FormTypeTender").value == 'manufactured' || document.getElementById("FormTypeTender").value == 'imported')
        {
           // alertMessage="Filled-in Data in form 3-A must be the same in form 3-B and vice versa.";
        }
    else
        {
        /* Doahtec End */
                if(document.getElementById("isInEditMode").value != 'true' && document.getElementById("isInEditMode").value != true)
                {
                    alertMessage="Are You sure You want to add this Table?";
                }

                if((document.getElementById("hdnTblChange").value == 'true' || document.getElementById("hdnTblChange").value == true)
                    && document.getElementById("hdnGrandSumExist").value != null && document.getElementById("hdnGrandSumExist").value != 0)
                {
                    alertMessage= "You have already prepared a Grand Summary Report. Changes in the table will remove Grand Summary Report. Do you want to Continue?";
                }
        }


   
    if(confirm(alertMessage))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function checkSortOrderRow(rowCnt){
    for(i=1;i<=parseInt(rowCnt);i++){
        for(j=1;j<=parseInt(rowCnt);j++){
            if(i!=j){
                var cmb1 = document.getElementById("rowsort"+i)
                var cmb2 = document.getElementById("rowsort"+j)
                //jAlert(" i -> " + i + " : j -> " + j + " ------------- " + cmb1.options[cmb1.selectedIndex].value + " == " + cmb2.options[cmb2.selectedIndex].value,'Alert');
                //if(document.getElementById("rowsort"+i).value == document.getElementById("rowsort"+j).value){
                if(cmb1.options[cmb1.selectedIndex].value == cmb2.options[cmb2.selectedIndex].value){
                    jAlert("Please correct Sort Order of Row.",'Alert');
                    document.getElementById("rowsort"+j).focus();
                    return false;
                }
            }
        }
    }
    return true;
}

function checkSortOrder(colCnt){
    for(i=0;i<colCnt;i++){
        if(trim(document.getElementById("SortOrder"+i).value) > colCnt){
            jAlert("Sort order can not be greater than No. of Columns.",'Alert');
            document.getElementById("SortOrder"+i).value = "";
            document.getElementById("SortOrder"+i).focus();
            return false;
        }

        if(trim(document.getElementById("SortOrder"+i).value) == 0){
            jAlert("Sort order can not be zero",'Alert');
            document.getElementById("SortOrder"+i).value = "";
            document.getElementById("SortOrder"+i).focus();
            return false;
        }
    }

    for(i=0;i<colCnt;i++){
        //alert(" sort order " + document.getElementById("SortOrder"+i).value);
        if(trim(document.getElementById("SortOrder"+i).value) == ""){
            jAlert("Please input sort order of Column.",'Alert');
            document.getElementById("SortOrder"+i).value = "";
            document.getElementById("SortOrder"+i).focus();
            return false;
        }else{
            for(j=0;j<colCnt;j++){
                if(i!=j){
                    if(document.getElementById("SortOrder"+i).value == document.getElementById("SortOrder"+j).value){
                        jAlert("Please correct Sort Order of Column.",'Alert');
                        document.getElementById("SortOrder"+j).focus();
                        return false;
                    }
                }
            }
        }
    }

    return true;
}

function validateExcel(theform, isBoqForm)
{
    var flag=checkExcel();
    var colCount = theform.cols.value = parseInt(theform.cols.value);
    var rowcount = parseInt(theform.rows.value);
    rowcount = rowcount+1;
    if(flag==true)
    {
        var path = document.getElementById("path").value;
        var objectExcel;
        try{
            objectExcel = new ActiveXObject("Excel.Application");
        }catch(ex){
            jAlert("Please enable \"Initialize and script ActiveX controls not marked as safe for scripting\" \n ActiveX control in your browser. You may change \n the setting from : Tools ->Internet Options -> Security -> initialize and \n script ActiveX controls not marked as safe for scripting -> Enable", 'Alert');
            return false;
        }
        var objectWorkbook = objectExcel.workbooks.open(path);
        var objectWorksheet = objectWorkbook.worksheets(1);
        var LastRow = objectWorksheet.UsedRange.Rows.Count;
        var LastCol = objectWorksheet.UsedRange.columns.Count;
        objectWorksheet.Visible = true;
        //alert("colcount-->"+colCount);
        //alert("rowcount-->"+rowcount);
        //alert("lastcolumn-->"+LastCol);
        //alert("lastrow-->"+LastRow);
        //alert("LastRow-rowcount"+(eval(LastRow)-eval(rowcount)));
        //alert("LastCol-colCount"+(eval(LastCol)-eval(colCount)));

        if(confirm("Are you sure to upload "+LastRow+" row and "+LastCol+" col"))
        {
            var temprowcount = rowcount;
            var tempcolcount = colCount;
            if(eval(rowcount)!=eval(LastRow))
            {
                for(i=0;i < (eval(LastRow)-eval(rowcount));i++)
                {
                    addRow(theform, cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);
                    temprowcount++;
                }
            }
            if(eval(colCount)!=eval(LastCol))
            {
                for(i=0;i < (eval(LastCol)-eval(colCount));i++)
                {
                    addCol(theform, isBoqForm, cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);
                    tempcolcount++;
                }
            }
            colCount = tempcolcount;
            rowcount= temprowcount;

            var i;
            var j;

            for(i=0;i<rowcount;i++)
            {
                for(j=0;j<colCount;j++)
                {
                    if(objectWorksheet.Cells(i+1,j+1).value==null)
                    {
                    //document.getElementById("Cell"+i+(j+1)).value = "";
                    }
                    else
                    {
                        if(i!=0)
                        {
                            document.getElementById("Cell"+i+"_"+(j+1)).value = objectWorksheet.Cells(i+1,j+1).value;
                        }
                        else
                        {
                            document.getElementById("Header"+j).value = objectWorksheet.Cells(i+1,j+1).value;
                        }
                    }
                }
            }
            objectExcel.quit();
        }
    }
    else
    {
        return false;
    }
}

function checkExcel()
{
    if(document.getElementById("path").value=="")
    {
        jAlert('Please Select An Excel File To Be Uploaded', 'Alert');
        return false;
    }

    var ext = document.getElementById("path").value;
    ext = ext.substring(ext.lastIndexOf(".")+1,ext.length);
    ext = ext.toLowerCase();
    if(ext == 'xls' || ext == 'xlsx'){
        return true;
    }else{
        jAlert('Please Select Excel file only', 'Alert');
        return false;
    }
}