/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Author : Sanjay Prajapati
 * Date : 18/11/2010
 */

var arr=new Array(); //Stores the Array of the TableIds
var first = false;
var LoopCounter = 0;
var rst = 0.0 ;
var newArr = new Array();
var chkEdit = false;
var chkBidTableId = new Array();
var arrBidCount = new Array();
//var addRowAfterEdit = new Array();
var signClick = false;
var verifyClick = 1;

var formulaArr=new Array();
var resultArr = new Array();
var templateFormulaArr = new Array();
var templateColIdArr = new Array();
var templateResultArr = new Array();

var isDataCorrupted = false;
var ifShowAlert = true;


function checkForAnySpaceOrNull()
{
    var inputtag = document.getElementsByTagName("input");
    var textareatag = document.getElementsByTagName("textarea");

    for(var inputcount=0;inputcount<inputtag.length;inputcount++)
    {
        if(inputtag[inputcount].value != null)
        {
            if(inputtag[inputcount].id == "signedData")
                continue;

            if(trim(inputtag[inputcount].type) == 'text' && trim(inputtag[inputcount].value) =="")
            {
                inputtag[inputcount].focus();
                return false;
            }
            else
            {
                if(trim(inputtag[inputcount].type) == 'text')
                {
            /*if(trim(inputtag[inputcount].value).indexOf(' ') > -1)
					{
						inputtag[inputcount].focus();
						//alert(" in " + inputtag[inputcount].id);
						return false;
					}*/
            }
            }//else of trim
        }
    }//for loop of input

    for(var textareacount=0;textareacount<textareatag.length;textareacount++)
    {
        if(textareatag[textareacount].value != null)
        {
            if(trim(textareatag[textareacount].value) =="")
            {
                textareatag[textareacount].focus();
                return false;
            }
        /*else
			{
				if(trim(textareatag[textareacount].value).indexOf(' ') > -1)
				{
					textareatag[textareacount].focus();
					return false;
				}
			}//else of trim*/;
        }
    }//for loop of textarea.
    return true;
}

function checkForAnyNull()
{
    var inputtag = document.getElementsByTagName("input");
    var textareatag = document.getElementsByTagName("textarea");

    for(var inputcount=0;inputcount<inputtag.length;inputcount++)
    {
        if(trim(inputtag[inputcount].type) == 'text' && trim(inputtag[inputcount].value) =="")
        {
            inputtag[inputcount].focus();
            return false;
        }
    }//for loop of input

    for(var textareacount=0;textareacount<textareatag.length;textareacount++)
    {
        if(trim(textareatag[textareacount].value) =="")
        {
            textareatag[textareacount].focus();
            return false;
        }
    }//for loop of textarea.
    return true;
}

function getDate(date)
{
    var splitedDate = date.split("-");
    return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
}

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY)

}

function countFormula_New(tableId,rowId,colId,tableIndex) // Implemented with new logic - Ravi.
{
    //alert("countFormula_New  tableIndex : "+tableIndex);
    //alert("arrDataTypesforCell : "+arrDataTypesforCell);
    //alert("arrDataTypesforCell with tableIndex : "+arrDataTypesforCell[tableIndex]);
    var arrayDataType = ""+arrDataTypesforCell[tableIndex]+"";
    var operand1DataType;
    var operand2DataType;
    var dataTypeFatched = false;
    var isDateField = false; // Added by Ketan Prajapati for new data type -  Date
    if(arrayDataType.indexOf(12) != -1) // Data Type = 12 ---> Date
    {
        isDateField = true;
        var splitedDataType = arrayDataType.split(",");
        for(var i=0;i<splitedDataType.length;i++)
        {
            if(splitedDataType[i] != "" && splitedDataType[i] !='' && splitedDataType[i] != null)
            {
                operand1DataType = splitedDataType[i];
                operand2DataType = splitedDataType[i+1];
                dataTypeFatched = true;
            }
            if(dataTypeFatched)
            {
                break;
            }
        }
    }

    if(!templateFormulaArr[tableId+"_"+colId] && colId==6){
	colId=parseInt(colId)-2;
    }

    //alert("isDateField : "+isDateField);
    //alert("operand1DataType : "+operand1DataType);
    //alert("operand2DataType : "+operand2DataType);
    //alert('<br/><br/>****templateFormulaArr '+templateFormulaArr+" <br/>tableId : "+tableId+" <br/>colId : "+colId);
    var firstIndex;
    var secondIndex;
    var thirdIndex;
    var isFirst = true;
    var strValue;
    var component;
    var putOperator;
    var lastOperator;
    var finalOperator;
    var tempString;
    var operator;

    if(templateFormulaArr[tableId+"_"+colId])
    {
        for(var i=0;i<templateFormulaArr[tableId+"_"+colId].length;i++)
        {
            lastOperaotr = "";
            isFirst = true;
            LoopCounter++;
            //var otherOperandDataType = arrDataTypesforCell[tableIndex][rowId][templateColIdArr[tableId+"_"+colId][i]];
            //alert('<br/><br/>templateFormulaArr[tableId+"_"+colId][i]'+templateFormulaArr[tableId+"_"+colId][i]);
            if(templateFormulaArr[tableId+"_"+colId][i].indexOf("TOTAL")<0)
            {
                var str1 =  templateFormulaArr[tableId+"_"+colId][i];
                var resultComp = templateResultArr[tableId+"_"+colId][i];
                var strResult = resultComp;
                while(str1.indexOf("^ROWID^")>0)
                {
                    str1 = str1.replace("^ROWID^",rowId);
                }
                if(str1.search("/p") != -1)
                {
                    str1 = str1.replace("p",100);
                }
                if(!isDateField){
                    
                    str1 = eval(str1);
                    strResult = strResult.replace("^ROWID^",rowId);
                    if(document.getElementById(strResult) != null)
                    {
                        document.getElementById(strResult).readOnly = true;
                        if(isNaN(str1))
                        {
                            var flag="false";
                            for(var k=0;k<=i;k++)
                            {
                            //if(arrForLabelDisp[k] != null)
                            //{
                            //    flag="true";
                            //    break;
                            //}
                            }
                            if(flag == "true")
                            {
                                document.getElementById(strResult).value=str1;
                                document.getElementById(strResult+"_"+tableId).innerText=str1;
                            }
                            else
                            {
                                document.getElementById(strResult).value=str1;
                            }
                        }
                        else
                        { 
                            if (str1 == 0) //Code to calculate discount amount 0% discount rate
                            {
                              document.getElementById(strResult).value = '0.000'; 
                            }
                            else{
                                if(eval(str1) != 'undefined' && eval(str1) != '' && eval(str1) != "" && eval(str1) != null &&  eval(str1) != 'NaN' && eval(str1) != "NaN")
                                {
                                    document.getElementById(strResult).value=(Math.round(eval(eval(str1)*1000))/1000).toFixed(3);
                                }
                            }
                        }
                        document.getElementById(strResult).tabIndex=-1;
                    //alert("str  " + str1);
                    }
                    //alert('mast '+resultComp);
                    var id = resultComp.replace("_^ROWID^","");
                    id = id.replace("row","");
                    var newColId = id.substring(id.indexOf("_")+1,id.length);
                    //alert('newColId : '+newColId);
                    if(templateResultArr[id])
                    {
                        for(var j=0;j<templateResultArr[id].length;j++)
                        {
                            LoopCounter++;
                            try{
                                countFormula_New(tableId,rowId,newColId,tableIndex);
                            }catch(err){
                              //alert("err @ countFormula_New::" + err);
                            }
                        }
                    }
                }
                else
                {
                    var finalResult;
                    if(str1.indexOf("+") != -1)
                    {
                        operator = "+";
                    }
                    else
                    {
                        operator = "-";
                    }

                    var formulaArray = str1.split(operator);
                    var operand1 = formulaArray[0];
                    var operand2 = formulaArray[1];
                    //alert("formulaArray : "+formulaArray+"operand1 : "+operand1+" operand2 : "+operand2);
                    //var otherOperandDataType = arrDataTypesforCell[tableIndex][rowId][templateColIdArr[tableId+"_"+colId][i]];
                    strResult = strResult.replace("^ROWID^",rowId);
                    if(document.getElementById(strResult) != null)
                    {
                        document.getElementById(strResult).readOnly = true;

                        if(operand2DataType == 12) // (Formla =  Date - Date)
                        {
                            var firstDate = getDate(eval(operand1));
                            var secondDate = getDate(eval(operand2));
                            if(operator == "-")
                            {
                                finalResult =  days_between(new Date(firstDate),new Date(secondDate));
                            }
                            if(!isNaN(finalResult)){
                                document.getElementById(strResult).value=finalResult;
                            }
                        }
                        else // (Formula = Date +  Days) OR (Formula = Date -  Days)
                        {
                            var millisecondsToAdd = eval(operand2)  * 86400000;
                            var endDt;
                            if(operator == "+")
                                endDt = new Date(getDate(eval(operand1)) + millisecondsToAdd);
                            else
                                endDt = new Date(getDate(eval(operand1)) - millisecondsToAdd);
                            var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                            finalResult = endDt.getDate() + "-" + monthname[endDt.getMonth()] + "-" + endDt.getFullYear();
                            //alert("finalResult " +finalResult);
                            if(finalResult != 'undefined' && finalResult != '' && finalResult != "" && finalResult != null &&  finalResult != 'NaN' && finalResult != "NaN" && finalResult.search("NaN")){
                                document.getElementById(strResult).value=finalResult;
                            }
                        }

                        document.getElementById(strResult).tabIndex=-1;
                    }
                    var id = resultComp.replace("_^ROWID^","");
                    id = id.replace("row","");
                    var newColId = id.substring(id.indexOf("_")+1,id.length);
                    if(templateResultArr[id])
                    {
                        for(var j=0;j<templateResultArr[id].length;j++)
                        {
                            LoopCounter++;
                            try{
                                countFormula_New(tableId,rowId,newColId,tableIndex);
                            }catch(err){
                            }
                        }
                    }
                }
            }
            else
            {
                try{
                    //alert('colId : '+colId)
                    //alert('applyColumnFunctions ');
                    applyColumnFunctions(tableId,colId,"TOTAL");
                }catch(err){
                 //alert("err @ applyColumnFunctions::" + err + ", colId:" + colId);
                }
            }
        }
    }
}


function applyColumnFunctions(tableid,colId,funcType)
{
    //alert(" :: " + arrRowsKey[tableid] + " :: " + arrTableAddedKey[tableid]+ " toe");
    var cmpTotal="",Total=0,noOfRows =0;
    var SelectedTable;


    for(var j=0;j<arr.length;j++)
        if(arr[j]==tableid)
        {
            SelectedTable = j;
            LoopCounter++;
            break;
        }

    //alert(" sdkf "  + arrBidCount[SelectedTable]);

    if(chkEdit != true){
        //alert('bf noOfRows : '+noOfRows);
        noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid])-parseInt(arrTableAddedKey[tableid]));
    //alert('af noOfRows : '+noOfRows);
    }
    else
    {
        if(arrTableAddedKey[tableid] > 2)
        {
            if(arrTableAddedKey[tableid] == (arrRowsKey[tableid]-1))
            {
                if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
                {
                    noOfRows = (parseInt(parseInt(arrRowsKey[tableid]-1)*arrTableAddedKey[tableid]));
                }
                else
                {
                   noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid]));
                }
            }
            else if(arrTableAddedKey[tableid] == (arrRowsKey[tableid]))
            {
                if(totalInWordAt > -1){ // condition set by hirav ==> to undo just remove if ..
                    noOfRows = (parseInt(arrRowsKey[tableid]*(arrTableAddedKey[tableid] - 1)));
                }else{
                    //noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid]) - 1);
                    //alert(isColTotalforTable[SelectedTable]);
                    if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
                    {
                        noOfRows = parseInt(parseInt(arrRowsKey[tableid]-1)*arrTableAddedKey[tableid]);
                    }else{
                        noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid]) - 1);
                    }
                }
            }
            else if(arrTableAddedKey[tableid] > (arrRowsKey[tableid]))
            {
                if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
                {
                    var i1 = parseInt(arrRowsKey[tableid]);
                    var i2 = parseInt(arrTableAddedKey[tableid]);
                    noOfRows = (i1-1) * (i2);
                }else{
                    noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid]) - parseInt((arrTableAddedKey[tableid] - parseInt(arrRowsKey[tableid]))+1));
                }
            }
            else
            {
                noOfRows = (parseInt(arrRowsKey[tableid]*arrTableAddedKey[tableid]) + parseInt(arrTableAddedKey[tableid]- parseInt(arrTableAddedKey[tableid]-1)));
            }
        }
        else //if(arrTableAddedKey[tableid] == 1)
        {
            noOfRows = (parseInt(arrRowsKey[tableid]*arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable]));

        //alert(SelectedTable + " arrRowsKey "  + arrRowsKey[tableid] + " arrBidCount " + arrBidCount[SelectedTable]);
        }
    }

    //alert(funcType);
    if(funcType=="TOTAL") // logic for TOTAL of all fields (in case of other functions do it in if else)
    {
        for(var i=1;i<=noOfRows;i++)
        {
            cmpTotal = "row"+tableid+"_"+i+"_"+colId;
            //alert("cmp " + cmpTotal);
            //alert(document.getElementById(cmpTotal).value);
            if(trim(document.getElementById(cmpTotal).value)!="")
            {
                //alert('bf Total : '+Total);
                Total = parseFloat(Total) + parseFloat(document.getElementById(cmpTotal).value);
            //alert("out if " + i)
            //alert('af Total : '+Total);
            }

        }

        document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+colId).value = Math.round(eval(eval(Total)*10000))/10000;
        document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+colId).readOnly = true;
        document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+colId).tabIndex=-1;

        //alert(colId);
        //if(colId == 8){
        //alert("isColTotalforTable::==>" + isColTotalforTable);
        //alert("arrColTotalIds::==>" + arrColTotalIds);
        //alert("arrColTotalWordsIds::==>" + arrColTotalWordsIds);
        //}
        for(i=1;i<=isColTotalforTable.length;i++){
            for(j=1;j<=arrColTotalIds[i-1].length;j++){
                //alert("1: colId" + colId + ",arrColTotalIds["+(i-1)+"]["+(j-1)+"] -> " + arrColTotalIds[i-1][j-1]);
                if(colId == arrColTotalIds[i-1][j-1] && arrColTotalWordsIds[i-1][j-1] != 0){
                    //alert("2: ID:" + "row"+tableid+"_"+(noOfRows+1)+"_"+eval(arrColTotalWordsIds[i-1][j-1]));
                    //alert(document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+eval(arrColTotalWordsIds[i-1][j-1])));
                    //alert(WORD(Math.round(eval(eval(Total)*100000))/100000));

                    /*
                            try catch below set by
                            Hirav...
                    */

                    try{
                        document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+eval(arrColTotalWordsIds[i-1][j-1])).innerHTML = WORD(Math.round(eval(eval(Total)*10000))/10000);
                        document.getElementById("row"+tableid+"_"+(noOfRows+1)+"_"+eval(arrColTotalWordsIds[i-1][j-1])).value = WORD(Math.round(eval(eval(Total)*10000))/10000);
                    //if(document.getElementById("row"+tableid+"_"+(noOfRows)+"_"+eval(arrColTotalWordsIds[i-1][j-1])+"_"+tableid) != null)
                    //{
                    //alert("==> element::row"+tableid+"_"+(noOfRows)+"_"+eval(arrColTotalWordsIds[i-1][j-1])+"_"+tableid);
                    //  document.getElementById("row"+tableid+"_"+(noOfRows)+"_"+eval(arrColTotalWordsIds[i-1][j-1])+"_"+tableid).innerText=document.getElementById("row"+tableid+"_"+(noOfRows)+"_"+eval(arrColTotalWordsIds[i-1][j-1])).innerText;
                    //}
                    }catch(err){
                    //alert("!@#$%..err:" + err);
                    //document.getElementById("row"+tableid+"_"+(noOfRows)+"_"+eval(arrColTotalWordsIds[i-1][j-1])).value = WORD(Math.round(eval(eval(Total)*100000))/100000);
                    }
                }
            }
        }
    }
}

function prepareTemplateFormula(formula,arrId,tableId,storeIn,cnt,arrDataType)
{
    //str1,arrIds[k][i],arr[k],arrFormulaFor[k][i],k+i

    var vDataType = ""+arrDataType+"";
    //alert("vdATATYPE : "+vDataType);
    var isDatePresent = false; // Added by Ketan Prajapati for new data type -  Date
    if(vDataType.indexOf(12) != -1)
    {
        isDatePresent = true;
    }
    //alert("isDatePresents : "+isDatePresent);
    //alert('formula : '+formula);
    var newFormula="",SelectedTable="";
    var newFormulaColId = "";
    storeIn = "row"+tableId+"_^ROWID^_"+storeIn;

    for(var j=0;j<arr.length;j++)
        if(arr[j]==tableId)
        {
            SelectedTable = j;
            LoopCounter++;
            break;
        }

    var tempStr = arrStaticColIds[SelectedTable]+"";

    for(var i=0;i<arrId.length;i++)
    {
        var rowOrLbl = ((tempStr.indexOf(arrId[i])!=-1)?"lbl@":"row"); // decides whether the column has labels or other components.
        var valueOrInnerHMTL = ((tempStr.indexOf(arrId[i])!=-1)?"innerHTML":"value"); // if it is label then .innerHTML otherwise .value
        //newFormula+= ((!isNaN(arrId[i]))?"document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL:arrId[i]);
        //alert(newFormula);
        if(tempStr.indexOf(arrId[i])!=-1)
        {
            var newTempStr = tempStr.split(",");
            for(var abc=0;abc<newTempStr.length;abc++)
            {
                if(arrId[i] == newTempStr[abc])
                {
                    rowOrLbl = "lbl@";
                    valueOrInnerHMTL = "innerHTML";
                    break;
                }
                else
                {
                    rowOrLbl = "row";
                    valueOrInnerHMTL = "value";
                }

            }
        }

        if(!isNaN(arrId[i]))
        {
            if(i>0)
            {
                if(!isDatePresent){
                    if((arrId[i+1]=="/") || (arrId[i-1] == "/")){
                        newFormula+="(isNaN(eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))?1:eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))";
                    }else if((arrId[i+1]=="+") || (arrId[i-1] == "+")){
                        newFormula+="(isNaN(eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))?0:eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))";
                    }else{
                        newFormula+="(isNaN(eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))?0:eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))";
                    }
                }
                else
                {
                    if((arrId[i+1]=="/") || (arrId[i-1] == "/")){
                        newFormula+="document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL;
                        newFormulaColId+=arrId[i];
                    }else if((arrId[i+1]=="+") || (arrId[i-1] == "+")){
                        newFormula+="document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL;
                        newFormulaColId+=arrId[i];
                    }else{
                        newFormula+="document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL;
                        newFormulaColId+=arrId[i];
                    }
                }
            }
            else
            {
                if(!isDatePresent){
                    newFormula+="(isNaN(eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))?0:eval(document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL+"))";
                    newFormulaColId+=arrId[i];
                }
                else
                {
                    newFormula+="document.getElementById('"+rowOrLbl+tableId+"_^ROWID^_"+arrId[i]+"')."+valueOrInnerHMTL;
                }
            }
        }
        else if(arrId[i].indexOf("N_")==0)
        {
            //alert("before : " + newFormula);
            newFormula+=arrId[i].replace("N_","");
        	//alert("after : " + newFormula);
        }
        else
        {
            newFormula+=arrId[i];
        }
    }

    //alert('len : '+arrId.length);
    for(var j=0;j<arrId.length;j++)
    {
        //alert(arrId[j]);
        if(isNaN(arrId[j])){}
        else
        {
            var chktemformula = tableId+"_"+arrId[j];
            //document.write('chktemformula '+chktemformula +'<br/>');
            if(templateFormulaArr[chktemformula] == null)
                templateFormulaArr[chktemformula] = new Array();
            if(templateResultArr[chktemformula] == null)
                templateResultArr[chktemformula] = new Array();
            if(templateColIdArr[chktemformula] == null)
                templateColIdArr[chktemformula] = new Array();

            templateFormulaArr[chktemformula].push(newFormula);
            templateColIdArr[chktemformula].push(newFormulaColId);
            templateResultArr[chktemformula].push(storeIn);

            //document.write('templateFormulaArr0 ' +templateFormulaArr+' newFormula '+newFormula);
        }
         //document.write('templateFormulaArr1 ' +templateFormulaArr+' newFormula '+newFormula);
    }
    //document.write('templateFormulaArr2 ' +templateFormulaArr+' newFormula '+newFormula+'<br/>');
}

function breakFormulas(arrDataTypesforCell){
    //alert("in bREAKformulas L  : arr.length"+arr.length);
    for(var k=0;k<arr.length;k++){
        for(var i=0;i<arrTableFormula[k].length;i++){
            var str = arrTableFormula[k][i];

            var str1 = str;
            var index=0;
            var char1="";
            var ch="";

            arrIds[k][i] = new Array();
            //alert("---->"+str+"<----" + " :: " + "---->"+trim(str)+"<----");
            for(;str.length>0;){
                if(index==0){
                    char1 = str.charAt(index);
                    ch = char1;
                }else{
                    char1 = str.substring(0,index);
                }

                ch = char1.charAt(char1.length-1);
                if(ch=="+" || ch=="*" || ch=="-" || ch=="/" || ch== "(" || ch==")" || ch=="p"  || ch=="N"){
                    if(ch=="N"){
                        while(true){
                            var ch = str.charAt(index);
                            if(ch=="+" || ch=="*" || ch=="-" || ch=="/" || ch== "(" || ch==")" || ch=="p"){
                                arrIds[k][i].push(str.substring(0,index));
                                str = str.substring(index,str.length);
                                index=0;
                                break;
                            }else if(index == str.length){
                                arrIds[k][i].push(str.substring(0,index));
                                str = str.substring(index,str.length);
                                index=0;
                                break;
                            }
                            index++;
                        }
                    }else{
                        if(index>0){
                            arrIds[k][i].push(str.substring(0,index-1));
                            str = str.substring(index-1,str.length);
                            index=0;
                        }
                    }
                    //alert("4--->" + ch + "<---");
                    if(ch != ""){
                        arrIds[k][i].push(ch);
                    }
                    str = str.substring(1,str.length);
                }else if(isNaN(char1)){
                    index++;
                }else{
                    char1 += str.charAt(1);
                    if(isNaN(char1)){
                        arrIds[k][i].push(char1.charAt(0));
                        str = str.substring(1,str.length);
                    }else{
                        arrIds[k][i].push(char1);
                        str = str.substring(2,str.length);
                    }
                }
                LoopCounter++;
            }
            prepareTemplateFormula(str1,arrIds[k][i],arr[k],arrFormulaFor[k][i],k+i,arrDataTypesforCell[k]);
        }
    }
}


function checkForFunctions()
{
    var ColId = 0;
    var tableId = 0;
    var SelectedTable=0;

    for(var k=0;k<arr.length;k++)
        for(var i=0;i<arrTableFormula[k].length;i++)
            for(var j=0;j<arrIds[k][i].length;j++)
                if(arrIds[k][i][j].toUpperCase()=="TOTAL")
                    newArr.push(arrIds[k][i][j+2]);

    for(var k=0;k<arr.length;k++)
        for(var i=0;i<arrTableFormula[k].length;i++)
            for(var j=0;j<arrIds[k][i].length;j++)
            {
                var r=0;
                for(r=0;r<arr.length;r++)
                    if(r==k)
                    {
                        SelectedTable = r;
                        tableId = arr[r];
                        break;
                    }

                var NoofTables = parseInt(arrTableAdded[SelectedTable]);
                var row = (arrRow[SelectedTable]*NoofTables)-1;

                if(newArr.length > 0)
                    for(var m=1;m<=arrCol[SelectedTable];m++)
                    {
                        var bool = true;

                        for(var z=0;z<newArr.length;z++)
                            if(m==newArr[z])
                            {
                                LoopCounter++;
                                bool = false; // if m found in newArr[] , it has column total formula , so no need to delete it.
                                break;
                            }

                        if(bool)
                        {
                            var comp = document.getElementById("row"+tableId+"_"+row+"_"+arrColIds[m]);
                            var lastRow = document.getElementById("td"+tableId+"_"+row+"_"+m);
                            if(comp != null && lastRow != null)
                            {
                                //alert("com " + comp + " L " + lastRow + " :: " +"td"+tableId+"_"+row+"_"+m + " :: " + "row"+tableId+"_"+row+"_"+arrColIds[m]);
                                lastRow.removeChild(comp);
                            }
                        }
                    }
                LoopCounter++;
            }

//breakFormulas();
}


function DelTable(theform,tableId,button1){

    var SelectedTable; //for getting the id of table selected
    var NoofTables; //number of tables added
    var tdId; //id of the td being created
    var CompId; //id of the component being created
    var RowIndex; //id of row being created
    var tr; //row created
    var SortOrder; //for Bidder Side Components
    var StaticIndex; //for Vendor Side Components
    var ColIndex;
    var readOnly;
    var tableCnt = 0;
    var noofRows = 0;

    for(var i=0;i<arr.length;i++)
    {
        if(arr[i]==tableId)
        {
            SelectedTable = i;
            if(chkEdit){
                tableCnt = parseInt(parseInt(totalBidTable[i])+1);
            }
            break;
        }
    }
    var TotalFormulaSize = arrColTotalIds[SelectedTable].length;
    var selAllFlag = true;
    var isTotal = false;
    noofRows = arrRow[SelectedTable];

    for(var index=1;index<TotalFormulaSize;index++)
    {
        if(arrColTotalIds[SelectedTable][index]!=0){
            isTotal = true;
            noofRows = noofRows-1;
        }
    }

    $(':checkbox[id^=chk'+tableId+']').each(function(){
        if(!$(this).attr("checked")){
            selAllFlag = false;
        }
    });

    if(selAllFlag==true)
    {
        jAlert("Atleast one record required","Tender Preparation", function(retVal) {});
        return false;
    }
    else
    {
        var delRowCnt = 0;
        var noRecSelected = true; // Added by Ketan Prajapati to solve issue id :  3336        
        $(':checkbox[id^=chk'+tableId+']').each(function()
        {
            if($(this).attr("checked"))
            {
                var row_DataTypeCnt = 0; //  Added By Dohatec to  Encrypted data save  Prevent
                noRecSelected = false;
                var curRow = $(this).closest('tr');
                var rowId = $(this).closest('tr').attr("id");

                for(var x=1;x<=noofRows;x++)
                {
                    var curRowIndex = rowId.substring(rowId.indexOf("_")+1, rowId.length);
                    /* Added By Dohatec to  Encrypted data save  Prevent Start */
                    for(var y=curRowIndex;y>=1;y--){   if ($('#row_DataType'+tableId+'_'+y).length > 0) { row_DataTypeCnt++; }  }                    
                     var curRowIndex1 = parseInt(curRowIndex) + parseInt(row_DataTypeCnt);
                      var curRowIndex2 = parseInt(curRowIndex) + parseInt(row_DataTypeCnt) - parseInt(1);
                      /*  Added By Dohatec to  Encrypted data save  Prevent End */

                    document.getElementById("MainTable"+tableId).deleteRow(curRowIndex1); //  Modified By Dohatec to  Encrypted data save  Prevent
                    if(row_DataTypeCnt>0){document.getElementById("MainTable"+tableId).deleteRow(curRowIndex2);} //  Modified By Dohatec to  Encrypted data save  Prevent 
                    row_DataTypeCnt = 0; //  Added By Dohatec to  Encrypted data save  Prevent
                    var totalRows =  eval(arrRow[SelectedTable]) * eval(document.getElementById("tableCount"+tableId).value);

                    for(var i=curRowIndex;i<totalRows;i++)
                    {
                        for(var j=1;j<=arrCol[SelectedTable];j++)
                        {
                            $('#row'+tableId+'_'+(eval(i)+eval(1))).attr("name",'row'+tableId+'_'+i);

                            if(document.getElementById("idcombodetail"+tableId+'_'+(eval(i)+eval(1))+"_"+j)!=null){
                                $('#idcombodetail'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("name",'namecombodetail'+tableId+'_'+i+'_'+j);
                                $('#idcombodetail'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("id",'idcombodetail'+tableId+'_'+i+'_'+j);
                            }

                            $('#row'+tableId+'_'+(eval(i)+eval(1))).attr("id",'row'+tableId+'_'+i);

                            $('#row'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("name",'row'+tableId+'_'+i+'_'+j);
                            $('#row'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("id",'row'+tableId+'_'+i+'_'+j);

                            $('#td'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("id",'td'+tableId+'_'+i+'_'+j);
                            /*  Added By Dohatec to  Encrypted data save  Prevent Start */
                            $('#row_DataType'+tableId+'_'+(eval(i)+eval(1))).attr("id",'row_DataType'+tableId+'_'+i);
                            $('#row_DataType'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("name",'row_DataType'+tableId+'_'+i+'_'+j);
                            $('#row_DataType'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("id",'row_DataType'+tableId+'_'+i+'_'+j);
                            /*  Added By Dohatec to  Encrypted data save  Prevent End */
                            //alert("before ->" + '#lbl@'+tableId+'_'+(eval(i)+eval(1))+'_'+j +  " after ->" + 'lbl@'+tableId+'_'+i+'_'+j);
                            //$('#lbl@'+tableId+'_'+(eval(i)+eval(1))+'_'+j).attr("id",'lbl@'+tableId+'_'+i+'_'+j);
                        }
                    }
                }
                //alert(arrTableAddedKey[tableId]);
                arrTableAdded[SelectedTable]--;
                arrTableAddedKey[tableId]--;
                arrBidCount[SelectedTable]--;
                delRowCnt++;
            }
        });
        for(index=1;index<TotalFormulaSize;index++)
        {
            if(arrColTotalIds[SelectedTable][index]!=0){
                applyColumnFunctions(tableId,arrColTotalIds[SelectedTable][index],"TOTAL");
            }
        }

        if(noRecSelected)
        {
            jAlert("Please select a record to delete","Tender Preparation", function(retVal) {});
            return false;
        }
        else
        {
            document.getElementById("tableCount"+tableId).value = parseInt(document.getElementById("tableCount"+tableId).value) - parseInt(delRowCnt);
        }
    }

}
/*
 old commented AddTable
 function removed from here...
*/



function AddTable(theform,tableId,button1)
{
    //alert('theForm : '+theform);
    //alert('tableId : '+tableId);
    //alert('button1 : '+button1);
    //alert('AddTable');
    var SelectedTable; //for getting the id of table selected
    var NoofTables; //number of tables added
    var tdId; //id of the td being created
    var CompId; //id of the component being created
    var RowIndex; //id of row being created
    var tr; //row created
    var SortOrder; //for Bidder Side Components
    var StaticIndex; //for Vendor Side Components
    var ColIndex;
    var readOnly;
    var tableCnt = 0;

    arrTableAddedKey[tableId]++;

    //alert(arr.length);
    for(var i=0;i<arr.length;i++)
    {
        //alert('arr : '+arr[i]);
        if(arr[i]==tableId)
        {
            SelectedTable = i;
            arrTableAdded[i]++;
            if(chkEdit)
                tableCnt = parseInt(parseInt(totalBidTable[i])+1);
            //alert('tableCnt : '+tableCnt);
            LoopCounter++;
            break;
        }
    }
    //alert('SelectedTable : '+SelectedTable);

    document.getElementById("tableCount"+tableId).value = parseInt(document.getElementById("tableCount"+tableId).value) + 1;

    NoofTables = parseInt(arrTableAdded[i]);
    //alert('NoofTables : '+NoofTables);

    var tempBody = document.getElementById("MainTable"+tableId);
    //alert(tempBody);


    //for removing the column of total
    //alert(isColTotalforTable[SelectedTable]);
    if(isColTotalforTable[SelectedTable] != 0)
    {
        var removeColId;
        var retr;
        var removetr;

        if(chkEdit != true)
        {
            removeColId = ((arrRow[SelectedTable] *(NoofTables-1))-(NoofTables-1));
            retr = "row"+tableId+"_"+(removeColId+1);
            //alert(retr);
            removetr = document.getElementById(retr);
        //alert(removetr);
        }
        else if(chkEdit == true)
        {
            if(arrBidCount[SelectedTable] > 1)
            {
                removeColId = parseInt(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]))-(parseInt(arrBidCount[SelectedTable]));
                retr = "row"+tableId+"_"+removeColId;
                removetr = document.getElementById(retr);
            }
            else
            {
                removeColId = ((arrRow[SelectedTable] *(NoofTables-1))-(NoofTables-1));
                retr = "row"+tableId+"_"+removeColId;
                removetr = document.getElementById(retr);
            }
        }
        //alert(" re " + removeColId + " : rert "  + retr + " : : removetr  "+ removetr + " :: " + arrRow[SelectedTable] + " : no " + NoofTables);
        if(removetr != null){
            tempBody.removeChild(removetr);
        }


    }

    //alert('Row : '+arrRow[SelectedTable]);
    for(var addRow=1;addRow<=arrRow[SelectedTable];addRow++)
    {
        //alert('addRow : '+addRow);
        tr = document.createElement("TR");
        SortOrder = 0;
        StaticIndex = 0;
        tr.setAttribute("align","center");

        checkBoxCell = document.createElement("TD");
        checkBoxCell.setAttribute("class","t-align-center");

        //for setting rowids where there is no column total
        //alert(isColTotalforTable[SelectedTable]);
//        if(isColTotalforTable[SelectedTable] == 0)
//        {
//            if(chkEdit == true)
//            {
//                if(arrBidCount[SelectedTable] > 1)
//                {
//                    //alert("  in bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
//                    //alert('SelectedTable : '+SelectedTable);
//                    //alert('addRow : '+addRow);
//                    //alert('arrRow[SelectedTable] : '+arrRow[SelectedTable]);
//                    //alert('NoofTables : '+NoofTables);
//                    //alert('actRow : '+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables))));
//                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables))));
//                    if(addRow==1){
//                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables)))+"' />";
//                    }
//                }
//                else
//                {
//                    //alert(" bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
//                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
//                    if(addRow==1){
//                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))+"' />";
//                    }
//                }
//            //alert(" tr id " + tr.id);
//            //alert(" no " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
//            }
//            else
//            {
//                //alert(parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
//                tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
//                if(addRow==1){
//                    checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))+"' />";
//                }
//            //alert(" no " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
//            }
//        }

        //for setting rowids where there is column total
        if(isColTotalforTable[SelectedTable] != 0)
        {
            if(chkEdit == true)
            {
                if(arrBidCount[SelectedTable] > 1)
                {
                    //alert("  in bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable]))));
                    if(addRow==1){
                        //checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable])))+"' />";
                    }
                }
                else
                {
                    //alert(" bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables + " : " +addRow);
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1))));
                    if(addRow==1){
                        //checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1)))+"' />";
                    }
                }
            //alert(" no 1 " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
            else
            {
                //alert('row : '+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
                tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1)))));
                if(addRow==1){
                   // checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1))))+"' />";
                }
            //alert(" no 2 " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
        }
        //tr.appendChild(checkBoxCell);
        //for generating columns and cells

        //alert('Col : '+arrCol[SelectedTable]);
        for(var addCol=1;addCol<=arrCol[SelectedTable];addCol++)
        {
            readOnly="";
            for(var itext=0;itext<arrFormulaFor[SelectedTable].length;itext++)
            {
                if(arrFormulaFor[SelectedTable][itext] == (parseInt(addCol)))
                {
                    readOnly = "readOnly";
                    break;
                }
            }

            //alert('col : '+parseInt(addCol));
            //alert('row : '+parseInt(addRow));
            //alert('last row : '+parseInt(arrRow[SelectedTable] ));
            if(parseInt(addRow) == parseInt(arrRow[SelectedTable])) //if last row
            {
                //alert("addRow:" + parseInt(addRow) + ", arrRow[SelectedTable] :" + parseInt(arrRow[SelectedTable]));
                //alert('colTotal : '+isColTotalforTable[SelectedTable]);
                if(isColTotalforTable[SelectedTable] != 0) //if that table has column total
                {
                    //alert("b4 checking if that col has total...");
                    //alert("arrColTotalIds==>:" + arrColTotalIds);
                    //alert("==>arrColTotalIds[SelectedTable][addCol-1]:" + arrColTotalIds[SelectedTable][addCol-1]);
                    //alert("==>parseInt(addCol)" + parseInt(addCol));
                    if(arrColTotalIds[SelectedTable][addCol-1] == parseInt(addCol)) // if that column has column total
                    {
                        if(arrColIds[SelectedTable][SortOrder] != addCol )
                        {

                            var inBreak = false;
                            var count = 0;
                            while(arrColIds[SelectedTable][SortOrder] != addCol)
                            {
                                SortOrder++;
                                count++;
                                if((count > 30) && (arrColIds[SelectedTable][SortOrder] == null))
                                {
                                    inBreak = true;
                                    break;
                                }

                            }
                        }

                        var ColIndex ;
                        if(inBreak)
                            ColIndex = addCol;
                        else
                            ColIndex = arrColIds[SelectedTable][SortOrder];

                        if(chkEdit == true)
                        {
                            if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                if(inBreak)
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                            else
                            {
                                if(inBreak)
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                    //RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable]))- parseInt(NoofTables - arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                    //RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable]))- parseInt(NoofTables - arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                        }
                        else
                        {
                            if(inBreak)
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+addCol;
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+addCol;
                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        }

                        if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                        {
                            /*alert("CompId" + CompId);
							alert("tr==>" + tr.innerHTML);
							alert("arrCompType[SelectedTable][addRow][addCol]==>" + arrCompType[SelectedTable][addRow][addCol]);*/
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this)",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this)",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                        {
                            addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                    //alert("tr==>" + tr.innerHTML);

                    }

                    else if(totalInWordAt == parseInt(addCol)) // else if ==> table has total in words column
                    {
                        //alert("total in words col...hiravhirav");

                        SortOrder++;

                        CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                        RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                        tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];


                        /*CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))
								+"_"+arrColIds[SelectedTable][SortOrder];*/
                        //alert("CompId:" + CompId);
                        /*tdId = "td"+tableId+"_"+
						(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))
						+"_"+arrColIds[SelectedTable][SortOrder];*/
                        //alert("CompId:" + CompId);
                        addTextArea(theform, CompId, tr, tdId);
                    }
                    //else if not col total for that column
                    else
                    {
                        var q = 0;
                        for(var p = 0;p<totalWordArr.length;p++){
                            if(totalWordArr[p] == parseInt(addCol)){
                                SortOrder++;
                                q++;
                                CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                addTextArea(theform, CompId, tr, tdId);
                            }
                        }
                        if(q == 0){
                            if(chkEdit == true)
                            {
                                if(arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+((parseInt(arrRow[SelectedTable]) * parseInt(NoofTables)) - parseInt(NoofTables))+"_"+addCol;
                                //alert(" in " + tdId);
                                }
                            }
                            else
                            {
                                //tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+addCol;
                            }
                            addTD(" ",tr,1,tdId,false);
                        }

                    //alert(" in else " + tdId);
                    }//end of else if not col total for that column
                //alert("123 .. tr==>" + tr.innerHTML);
                }//if col table
                else
                {
                    //for creating GovSide cells
                    //alert('else 1');
                    //alert('SelectedTable: '+SelectedTable);
                    //alert('addRow: '+addRow);
                    //alert('addCol: '+addCol);
                    //alert(arrCellData[SelectedTable][parseInt(addRow-1)][addCol]);
                    //alert('else 2');
                    if(arrCellData[SelectedTable][addRow][addCol] != null && arrCellData[SelectedTable][addRow][addCol] != "" && arrCellData[SelectedTable][addRow][addCol] != "null")
                    {
                        if(isColTotalforTable[SelectedTable] != 0)
                        {
                            if(chkEdit == true)
                            {
                                if(arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }

                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                        //alert(" in " + tdId);
                        }

                        if(isColTotalforTable[SelectedTable] == 0)
                        {
                            if(chkEdit == true)
                            {
                                if(arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                        }
                        //alert(" in " + tdId + " : " + arrBidCount[SelectedTable]);
                        //alert(" td " + tdId +" row " + arrRow[SelectedTable] + " Nof " + NoofTables + " " + arrStaticColIds[SelectedTable][StaticIndex]);

                        addTD(new String(arrCellData[SelectedTable][addRow][addCol]),tr,1,tdId,true);
                        lbl_ids[lbl_ids.length] = "lbl@"+tdId.substring(2,tdId.length);

                        StaticIndex++;

                    }//end of if govdata
                    //for All Other Cells
                    else
                    {
                        if(isColTotalforTable[SelectedTable] != 0)
                        {

                            if(chkEdit == true)
                            {

                                if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];

                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];

                                }

                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                RowIndex = (addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        //alert(" in if com " + CompId + " : row " + RowIndex + " : tdId " + tdId);
                        }


                        if(isColTotalforTable[SelectedTable] == 0)
                        {
                            if(chkEdit == true) // if edit
                            {
                                //alert('else else else ');
                                if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                                {
                                    //alert('row : '+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable]))))));
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+addCol;
                                    //alert('CompId : '+CompId);
                                    RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))));
                                    tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+addCol;
                                }
                                else
                                {
                                    //alert('else 4');
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                    //alert('CompId : '+CompId);
                                    RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))));
                                    tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        }

                        ColIndex = arrColIds[SelectedTable][SortOrder];
                        //alert("dd " + arrCompType[SelectedTable][addRow][addCol]);
                        //alert(arrDataTypesforCell[SelectedTable][addRow][addCol]);
                        if(arrCompType[SelectedTable][addRow][addCol]=="TEXT")
                        {
                            if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onChange","CheckNumeric("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "1")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur1","validateNonUsed(this,2)",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                            {
                                var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                                var cmbdefaultVal = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol).value;
                                addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId,cmbdefaultVal);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                            {
                                var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                                var cmbdefaultVal = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol).value;
                                addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId,cmbdefaultVal);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                            {
                                addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                        }
                        else if(arrCompType[SelectedTable][addRow][addCol]=="SELECT")
                        {
                            var originalCmb = document.getElementById("row"+tableId+"_"+addRow+"_"+addCol);
                            addSelect(theform,CompId,originalCmb,tr,tdId);
                        }
                        else if(arrCompType[SelectedTable][addRow][addCol]=="TEXTAREA")
                        {
                            //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);
                            if(readOnly == "readOnly")
                            {
                                addTextAreawithReadOnly(theform,CompId,tr,tdId,true);
                            }
                            else
                            {
                                addTextArea(theform,CompId,tr,tdId);
                            }

                        }


                        SortOrder++;

                    //alert("Com " + CompId + " :: row " + arrRow[SelectedTable] + " no " + NoofTables );
                    //alert("Row " + RowIndex);



                    }//end of else other cells
                }//end of else maitrak


            }
            //if not last row
            else
            {

                //alert("text area td  " + tdId + " :: " + CompId+ " :: " + tr.id);

                //alert('SelectedTable : '+SelectedTable);
                //alert('addRow : '+addRow);
                //alert('addCol : '+addCol);

                //alert('arrCellData['+SelectedTable+']['+addRow+']['+addCol+'] : '+arrCellData[SelectedTable][addRow][addCol]);

                //for creating GovSide cells
                if(arrCellData[SelectedTable][addRow][addCol] != null && arrCellData[SelectedTable][addRow][addCol] != "" && arrCellData[SelectedTable][addRow][addCol] != "null")
                {
                    if(isColTotalforTable[SelectedTable] != 0)
                    {
                        if(chkEdit == true)
                        {
                            //alert(" bi " + arrBidCount[SelectedTable] + " : row " + arrRow[SelectedTable] + " : no " + NoofTables);
                            if(arrBidCount[SelectedTable] > 1)
                            {
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];

                            }
                        //alert(" td " +tdId);
                        }
                        else
                        {
                            tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                        }
                    }

                    if(isColTotalforTable[SelectedTable] == 0)
                    {
                        if(chkEdit == true)
                        {
                            if(arrBidCount[SelectedTable] > 1)
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                        }
                        else
                        {
                            tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                        }
                    }

                    //alert(" td  in edit " + tdId +" row " + arrRow[SelectedTable] + " Nof " + NoofTables + " " + arrStaticColIds[SelectedTable][StaticIndex]);
                    addTD(new String(arrCellData[SelectedTable][addRow][addCol]),tr,1,tdId,true);
                    lbl_ids[lbl_ids.length] = "lbl@"+tdId.substring(2,tdId.length);
                    StaticIndex++;
                }//end of if govdata
                //for All Other Cells
                else
                {

                    if(isColTotalforTable[SelectedTable] != 0)
                    {

                        if(chkEdit == true)
                        {

                            if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                /*CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
								RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
								tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];*/


                                CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;

                            }
                            else
                            {
                                //alert(" ro " + addRow + " : arr " + arrRow[SelectedTable] + " : bid " + arrBidCount[SelectedTable] + " :: nof "  + NoofTables + " :dd " +  arrColIds[SelectedTable][SortOrder] + " :ss " + SortOrder + " : " +addCol);
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;

                            }
                        //alert(" co " + CompId + " : R " + RowIndex + " : td " + tdId);

                        }
                        else
                        {
                            CompId = "row"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            RowIndex = (addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))));
                            tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                        }
                    //alert(" before com " + CompId + " : row " + RowIndex + " : tdId " + tdId);
                    }


                    if(isColTotalforTable[SelectedTable] == 0)
                    {
                        //alert(chkEdit);
                        if(chkEdit == true) // if edit
                        {
                            //alert('NoofTables : '+NoofTables);
                            //alert('arrRow[SelectedTable] : '+arrRow[SelectedTable]);
                            //alert('addRow : '+addRow);
                            //alert('totalBidTable : '+totalBidTable[SelectedTable]);
                            if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                //alert(totalBidTable[SelectedTable] + " ---- " + addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))));
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))))+"_"+arrColIds[SelectedTable][SortOrder];
                                //alert('CompId if : '+CompId);
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                            else
                            {
                                //alert(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))));
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))))+"_"+arrColIds[SelectedTable][SortOrder];
                                //alert('CompId else : '+CompId);
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        //totalBidTable[SelectedTable] = parseInt(parseInt(totalBidTable[SelectedTable])+ 1);

                        }
                        else
                        {
                            CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];

                            RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))));

                            tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];

                        }
                    }
                    //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);

                    //ColIndex = arrColIds[SelectedTable][SortOrder];
                    ColIndex = addCol;
                   // alert(" columne fill by ["+addCol+"]="+arrColFillBy[SelectedTable][addCol]);
                  // alert(arrCompType[SelectedTable][addRow][addCol]);
                    if(arrCompType[SelectedTable][addRow][addCol]=="TEXT")
                    {
                       // alert(arrDataTypesforCell[SelectedTable][addRow][addCol]);
                         //alert(arrCompType[SelectedTable][addRow][addCol]);
                        // alert(" columne fill by ["+addCol+"]="+arrColFillBy[SelectedTable][addCol]);
                        if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onChange","CheckNumeric("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "1")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur1","validateNonUsed(this,2)",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                        {
                            addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                    }
                    else if(arrCompType[SelectedTable][addRow][addCol]=="SELECT")
                    {
                        var originalCmb = document.getElementById("row"+tableId+"_"+addRow+"_"+addCol);
                        addSelect(theform,CompId,originalCmb,tr,tdId);
                    }
                    else if(arrCompType[SelectedTable][addRow][addCol]=="TEXTAREA")
                    {
                        //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);

                        if(readOnly == "readOnly")
                        {
                            addTextAreawithReadOnly(theform,CompId,tr,tdId,true);
                        }
                        else
                        {
                            addTextArea(theform,CompId,tr,tdId);
                        }

                    }

                    SortOrder++;

                //alert("Com " + CompId + " :: row " + arrRow[SelectedTable] + " no " + NoofTables );
                //alert("Row " + RowIndex);
                }//end of else other cells
            }//else for if not last row
        //alert(" before com " + CompId + " : row " + RowIndex + " : tdId " + tdId + " : arrco " + addCol);
        }//end of for arrCol
        //alert("tr.innerHTML:==>" + tr.innerHTML);
        tempBody.appendChild(tr);

    }//end of for arrRow

    //alert("chkEdit --->"+chkEdit+"<---------");
    if(chkEdit){
        totalBidTable[SelectedTable] = tableCnt;
    //alert(" aT the end " + totalBidTable[SelectedTable]);
    }
    if(arrBidCount[SelectedTable] != 0 )
        arrBidCount[SelectedTable] = parseInt(arrBidCount[SelectedTable]) + parseInt(1);

    checkForFunctions();
    for(var z=0;z<lbl_ids.length;z++)
    {
        var first_delim=lbl_ids[z].indexOf('@');
        var tblid=lbl_ids[z].substring(first_delim+1,lbl_ids[z].length);
        var second_delim=tblid.indexOf('_');
        var tableid=tblid.substring(0,second_delim);
        countFormula(tableid,document.getElementById(lbl_ids[z]));
    }

}//end of function



function AddBidTable(theform,tableId,button1){
    //alert('theForm : '+theform);
    //alert('tableId : '+tableId);
    //alert('button1 : '+button1);
    //alert('AddTable');
    var SelectedTable; //for getting the id of table selected
    var NoofTables; //number of tables added
    var tdId; //id of the td being created
    var CompId; //id of the component being created
    var RowIndex; //id of row being created
    var tr; //row created
    var SortOrder; //for Bidder Side Components
    var StaticIndex; //for Vendor Side Components
    var ColIndex;
    var readOnly;
    var tableCnt = 0;
    var row_DataType = 0;  // Added By Dohatec to  Encrypted data save  Prevent
    var row_DataType_Value = 0;  // Added By Dohatec to  Encrypted data save  Prevent
    var tr_row_DataType=0;  // Added By Dohatec to  Encrypted data save  Prevent

    arrTableAddedKey[tableId]++;

    //alert(arr.length);
    for(var i=0;i<arr.length;i++)
    {
        //alert('arr : '+arr[i]);
        if(arr[i]==tableId)
        {
            SelectedTable = i;
            arrTableAdded[i]++;
            if(chkEdit)
                tableCnt = parseInt(parseInt(totalBidTable[i])+1);
            //alert('tableCnt : '+tableCnt);
            LoopCounter++;
            break;
        }
    }
    //alert('SelectedTable : '+SelectedTable);

    document.getElementById("tableCount"+tableId).value = parseInt(document.getElementById("tableCount"+tableId).value) + 1;

    NoofTables = parseInt(arrTableAdded[i]);
    //alert('NoofTables : '+NoofTables);

    var tempBody = document.getElementById("MainTable"+tableId);
    //alert(tempBody);


    //for removing the column of total
    //alert(isColTotalforTable[SelectedTable]);
    if(isColTotalforTable[SelectedTable] != 0)
    {
        var removeColId;
        var retr;
        var removetr;

        if(chkEdit != true)
        {

            removeColId = ((arrRow[SelectedTable] *(NoofTables-1))-(NoofTables-1));
            if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
            {
                retr = "row"+tableId+"_"+(removeColId+1);
            }
            else
            {
                retr = "row"+tableId+"_"+removeColId;
            }
            removetr = document.getElementById(retr);

        }
        else if(chkEdit == true)
        {
            if(arrBidCount[SelectedTable] > 1)
            {
                removeColId = parseInt(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]))-(parseInt(arrBidCount[SelectedTable]));
                if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
                {
                    retr = "row"+tableId+"_"+(removeColId+1);
                }
                else
                {
                    retr = "row"+tableId+"_"+removeColId;
                }
            }
            else
            {
                removeColId = ((arrRow[SelectedTable] *(NoofTables-1))-(NoofTables-1));
                if(isColTotalforTable[SelectedTable] != 0) // if Total formulaPresent
                {
                    retr = "row"+tableId+"_"+(parseInt(removeColId+1));
                }
                else
                {
                    retr = "row"+tableId+"_"+removeColId;
                }
                //alert(retr);
            }
            removetr = document.getElementById(retr);
        }
        //alert(" re " + removeColId + " : rert "  + retr + " : : removetr  "+ removetr + " :: " + arrRow[SelectedTable] + " : no " + NoofTables);
        if(removetr != null){
            tempBody.removeChild(removetr);
        }


    }

    //alert('Row : '+arrRow[SelectedTable]);
    for(var addRow=1;addRow<=arrRow[SelectedTable];addRow++)
    {
        //alert('addRow : '+addRow);
        tr = document.createElement("TR");
        /* Added By Dohatec to  Encrypted data save  Prevent Start */
        tr_row_DataType=document.createElement("TR");
        tr_row_DataType.setAttribute("type","hidden");
        row_DataType = "";
        /*Added By Dohatec to  Encrypted data save  Prevent End */
        SortOrder = 0;
        StaticIndex = 0;
        tr.setAttribute("align","center");

        checkBoxCell = document.createElement("TD");
        checkBoxCell.setAttribute("class","t-align-center");

        //for setting rowids where there is no column total
        //alert(isColTotalforTable[SelectedTable]);
        if(isColTotalforTable[SelectedTable] == 0)
        {
            if(chkEdit == true)
            {
                if(false && arrBidCount[SelectedTable] > 1)
                {
                    //alert("  in bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
                    //alert('SelectedTable : '+SelectedTable);
                    //alert('addRow : '+addRow);
                   // alert('arrRow[SelectedTable] : '+arrRow[SelectedTable]);
                   // alert('NoofTables : '+NoofTables);
                    //alert('actRow : '+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables))));
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables))));
                    row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables))); // Added By Dohatec to  Encrypted data save  Prevent
                    tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables)))); // Added By Dohatec to  Encrypted data save  Prevent
                    if(addRow==1){
                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables)))+"' />";                        
                    }
                }
                else
                {
                    //alert(" bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
                     tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))); // Added By Dohatec to  Encrypted data save  Prevent
                     row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))); // Added By Dohatec to  Encrypted data save  Prevent
                    if(addRow==1){
                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))+"' />";                        
                    }
                }
            //alert(" tr id " + tr.id);
            //alert(" no " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
            else
            {
                //alert(parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
                tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
                tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))); // Added By Dohatec to  Encrypted data save  Prevent
                row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))); // Added By Dohatec to  Encrypted data save  Prevent
                if(addRow==1){
                    checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1)))+"' />";                    
                }
            //alert(" no " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
        }

        //for setting rowids where there is column total
        if(isColTotalforTable[SelectedTable] != 0)
        {
            if(chkEdit == true)
            {
                if(arrBidCount[SelectedTable] > 1)
                {
                    //alert("  in bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables);
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable]))));
                    tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable])))); // Added By Dohatec to  Encrypted data save  Prevent
                    row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable]))); // Added By Dohatec to  Encrypted data save  Prevent
                    if(addRow==1){
                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable]) - parseInt(arrBidCount[SelectedTable])))+"' />";                        
                    }
                }
                else
                {
                    //alert(" bid " + arrBidCount[SelectedTable] + " row " + arrRow[SelectedTable] + " no " + NoofTables + " : " +addRow);
                    tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1))));
                    tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1)))); // Added By Dohatec to  Encrypted data save  Prevent
                    row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1))); // Added By Dohatec to  Encrypted data save  Prevent
                    if(addRow==1){
                        checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable]) - parseInt(NoofTables-1)))+"' />";                        
                    }
                }
            //alert(" no 1 " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
            else
            {
                //alert('row : '+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1))));
                tr.setAttribute("id","row"+tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1)))));
                tr_row_DataType.setAttribute("id","row_DataType"+tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1))))); // Added By Dohatec to  Encrypted data save  Prevent
                row_DataType = tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1)))); // Added By Dohatec to  Encrypted data save  Prevent
                if(addRow==1){
                  checkBoxCell.innerHTML="<input type='checkbox' id='"+"chk"+tableId+"_"+parseInt(addRow+(  parseInt(parseInt(arrRow[SelectedTable])*parseInt(NoofTables-1) - parseInt(NoofTables-1))))+"' />";                  
                }
            //alert(" no 2 " + NoofTables + " rows " + arrRow[SelectedTable] + " id " + tr.id);
            }
        }
        tr.appendChild(checkBoxCell);
        addHiddenInput(row_DataType,1,tr_row_DataType); // Added By Dohatec to  Encrypted data save  Prevent
        //for generating columns and cells

        for(var addCol=1;addCol<=arrCol[SelectedTable];addCol++)
        {
            readOnly="";
            for(var itext=0;itext<arrFormulaFor[SelectedTable].length;itext++)
            {
                if(arrFormulaFor[SelectedTable][itext] == (parseInt(addCol)))
                {
                    readOnly = "readOnly";
                    break;
                }
            }

            //alert('col : '+parseInt(addCol));
            //alert('row : '+parseInt(addRow));
            //alert('last row : '+parseInt(arrRow[SelectedTable] ));
            if(parseInt(addRow) == parseInt(arrRow[SelectedTable])) //if last row
            {
                //alert("addRow:" + parseInt(addRow) + ", arrRow[SelectedTable] :" + parseInt(arrRow[SelectedTable]));
                //alert('colTotal : '+isColTotalforTable[SelectedTable]);
                if(isColTotalforTable[SelectedTable] != 0) //if that table has column total
                {
                    //alert("b4 checking if that col has total...");
                    //alert("arrColTotalIds==>:" + arrColTotalIds);
                    //alert("==>arrColTotalIds[SelectedTable][addCol-1]:" + arrColTotalIds[SelectedTable][addCol-1]);
                    //alert("==>parseInt(addCol)" + parseInt(addCol));
                    if(arrColTotalIds[SelectedTable][addCol-1] == parseInt(addCol)) // if that column has column total
                    {
                        if(arrColIds[SelectedTable][SortOrder] != addCol )
                        {

                            var inBreak = false;
                            var count = 0;
                            while(arrColIds[SelectedTable][SortOrder] != addCol)
                            {
                                SortOrder++;
                                count++;
                                if((count > 30) && (arrColIds[SelectedTable][SortOrder] == null))
                                {
                                    inBreak = true;
                                    break;
                                }

                            }
                        }

                        var ColIndex ;
                        if(inBreak)
                            ColIndex = addCol;
                        else
                            ColIndex = arrColIds[SelectedTable][SortOrder];

                        tr.getElementsByTagName("td")[ColIndex-1].innerHTML="<div style='text-align:center; font-weight:bold;'>Grand Total :</div>";
                        if(chkEdit == true)
                        {
                            if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                if(inBreak)
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                    row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder]; // Added By Dohatec to  Encrypted data save  Prevent
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                            else
                            {
                                if(inBreak)
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                    //RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable]))- parseInt(NoofTables - arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder]; // Added By Dohatec to  Encrypted data save  Prevent
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                    //RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable]))- parseInt(NoofTables - arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                        }
                        else
                        {
                            if(inBreak)
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+addCol;
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+addCol;
                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder]; // Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        }

                        if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                        {
                            /*alert("CompId" + CompId);
							alert("tr==>" + tr.innerHTML);
							alert("arrCompType[SelectedTable][addRow][addCol]==>" + arrCompType[SelectedTable][addRow][addCol]);*/
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus('"+tableId+"',this);",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this)",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this)",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                        {
                            addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        /* Added By Dohatec to  Encrypted data save  Prevent Start*/
                        row_DataType_Value = arrDataTypesforCell[SelectedTable][addRow][addCol];
                        addHiddenInput(row_DataType,row_DataType_Value,tr_row_DataType);
                        /* Added By Dohatec to  Encrypted data save  Prevent End*/
                    //alert("tr==>" + tr.innerHTML);
                    }

                    else if(totalInWordAt == parseInt(addCol)) // else if ==> table has total in words column
                    {
                        //alert("total in words col...hiravhirav");

                        SortOrder++;

                        CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                        row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder]; // Added By Dohatec to  Encrypted data save  Prevent
                        RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                        tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];


                        /*CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))-(parseInt(NoofTables-1))))
								+"_"+arrColIds[SelectedTable][SortOrder];*/
                        //alert("CompId:" + CompId);
                        /*tdId = "td"+tableId+"_"+
						(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))
						+"_"+arrColIds[SelectedTable][SortOrder];*/
                        //alert("CompId:" + CompId);
                        addTextArea(theform, CompId, tr, tdId);
                        addHiddenInput(row_DataType,1,tr_row_DataType); //*/ Added By Dohatec to  Encrypted data save  Prevent
                    }
                    //else if not col total for that column
                    else
                    {
                        var q = 0;
                        for(var p = 0;p<totalWordArr.length;p++){
                            if(totalWordArr[p] == parseInt(addCol)){
                                SortOrder++;
                                q++;
                                CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder]; // Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                addTextArea(theform, CompId, tr, tdId);
                                addHiddenInput(row_DataType,1,tr_row_DataType); // Added By Dohatec to  Encrypted data save  Prevent
                            }
                        }
                        if(q == 0){
                            if(chkEdit == true)
                            {
                                if(arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(arrBidCount[SelectedTable])))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+((parseInt(arrRow[SelectedTable]) * parseInt(NoofTables)) - parseInt(NoofTables))+"_"+addCol;
                                    row_DataType = tableId+"_"+((parseInt(arrRow[SelectedTable]) * parseInt(NoofTables)) - parseInt(NoofTables))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                                //alert(" in " + tdId);
                                }
                            }
                            else
                            {
                                //tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+addCol;
                                row_DataType = tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+addCol; // Added By Dohatec to  Encrypted data save  Prevent
                            }
                            addTD(" ",tr,1,tdId,false);                            
                            addHiddenInput(row_DataType,1,tr_row_DataType); //*/ Added By Dohatec to  Encrypted data save  Prevent
                        }

                    //alert(" in else " + tdId);
                    }//end of else if not col total for that column
                //alert("123 .. tr==>" + tr.innerHTML);
                }//if col table
                else
                {
                    //for creating GovSide cells
                    //alert('else 1');
                    //alert('SelectedTable: '+SelectedTable);
                    //alert('addRow: '+addRow);
                    //alert('addCol: '+addCol);
                    //alert(arrCellData[SelectedTable][parseInt(addRow-1)][addCol]);
                    //alert('else 2');
                    if(arrCellData[SelectedTable][addRow][addCol] != null && arrCellData[SelectedTable][addRow][addCol] != "" && arrCellData[SelectedTable][addRow][addCol] != "null")
                    {
                        if(isColTotalforTable[SelectedTable] != 0)
                        {
                            if(chkEdit == true)
                            {
                                if(arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }

                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                        //alert(" in " + tdId);
                        }

                        if(isColTotalforTable[SelectedTable] == 0)
                        {
                            if(chkEdit == true)
                            {
                                if(false && arrBidCount[SelectedTable] > 1)
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                                else
                                {
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                }
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            }
                        }
                        //alert(" in " + tdId + " : " + arrBidCount[SelectedTable]);
                        //alert(" td " + tdId +" row " + arrRow[SelectedTable] + " Nof " + NoofTables + " " + arrStaticColIds[SelectedTable][StaticIndex]);

                        addTD(new String(arrCellData[SelectedTable][addRow][addCol]),tr,1,tdId,true);
                        lbl_ids[lbl_ids.length] = "lbl@"+tdId.substring(2,tdId.length);
                        addHiddenInput(row_DataType,1,tr_row_DataType); //*/ Added By Dohatec to  Encrypted data save  Prevent

                        StaticIndex++;

                    }//end of if govdata
                    //for All Other Cells
                    else
                    {
                        if(isColTotalforTable[SelectedTable] != 0)
                        {

                            if(chkEdit == true)
                            {

                                if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                                {
                                    CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];

                                }
                                else
                                {
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
                                    RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])));
                                    tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];

                                }

                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                RowIndex = (addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))));
                                tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        //alert(" in if com " + CompId + " : row " + RowIndex + " : tdId " + tdId);
                        }


                        if(isColTotalforTable[SelectedTable] == 0)
                        {
                            if(chkEdit == true) // if edit
                            {
                                //alert('else else else ');
                                if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                                {
                                    //alert('row : '+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable]))))));
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+addCol;
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+addCol; //Added By Dohatec to  Encrypted data save  Prevent
                                    //alert('CompId : '+CompId);
                                    RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))));
                                    tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(arrBidCount[SelectedTable])))))+"_"+addCol;
                                }
                                else
                                {
                                    //alert('else 4');
                                    CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                    row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];  //Added By Dohatec to  Encrypted data save  Prevent
                                    //alert('CompId : '+CompId);
                                    RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))));
                                    tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                }
                            }
                            else
                            {
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                 row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder]; //Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        }

                        ColIndex = arrColIds[SelectedTable][SortOrder];
                        //alert("dd " + arrCompType[SelectedTable][addRow][addCol]);
                        //alert(arrDataTypesforCell[SelectedTable][addRow][addCol]);
                        /* Added By Dohatec to  Encrypted data save  Prevent Start*/
                        row_DataType_Value = arrDataTypesforCell[SelectedTable][addRow][addCol];
                        addHiddenInput(row_DataType,row_DataType_Value,tr_row_DataType);
                        /* Added By Dohatec to  Encrypted data save  Prevent End*/
                        if(arrCompType[SelectedTable][addRow][addCol]=="TEXT")
                        {
                            if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onChange","CheckNumeric("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "1")
                            {
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur1","validateNonUsed(this,2)",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                            {
                                var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                                var cmbdefaultVal = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol).value;
                                addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId,cmbdefaultVal);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                            {
                                var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                                var cmbdefaultVal = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol).value;
                                addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId,cmbdefaultVal);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                            {
                                addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                            }
                            else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                                addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                            }
                        }
                        else if(arrCompType[SelectedTable][addRow][addCol]=="SELECT")
                        {
                            var originalCmb = document.getElementById("row"+tableId+"_"+addRow+"_"+addCol);
                            addSelect(theform,CompId,originalCmb,tr,tdId);
                        }
                        else if(arrCompType[SelectedTable][addRow][addCol]=="TEXTAREA")
                        {
                            //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);
                            if(readOnly == "readOnly")
                            {
                                addTextAreawithReadOnly(theform,CompId,tr,tdId,true);
                            }
                            else
                            {
                                addTextArea(theform,CompId,tr,tdId);
                            }

                        }


                        SortOrder++;

                    //alert("Com " + CompId + " :: row " + arrRow[SelectedTable] + " no " + NoofTables );
                    //alert("Row " + RowIndex);



                    }//end of else other cells
                }//end of else maitrak


            }
            //if not last row
            else
            {


                //alert("text area td  " + tdId + " :: " + CompId+ " :: " + tr.id);

                //alert('SelectedTable : '+SelectedTable);
                //alert('addRow : '+addRow);
                //alert('addCol : '+addCol);

                //alert('arrCellData['+SelectedTable+']['+addRow+']['+addCol+'] : '+arrCellData[SelectedTable][addRow][addCol]);

                //for creating GovSide cells
                if(arrCellData[SelectedTable][addRow][addCol] != null && arrCellData[SelectedTable][addRow][addCol] != "" && arrCellData[SelectedTable][addRow][addCol] != "null")
                {
                    if(isColTotalforTable[SelectedTable] != 0)
                    {
                        if(chkEdit == true)
                        {
                            //alert(" bi " + arrBidCount[SelectedTable] + " : row " + arrRow[SelectedTable] + " : no " + NoofTables);
                            if(arrBidCount[SelectedTable] > 1)
                            {
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent

                            }
                        //alert(" td " +tdId);
                        }
                        else
                        {
                            tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                            row_DataType = tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent
                        }
                    }

                    if(isColTotalforTable[SelectedTable] == 0)
                    {
                        if(chkEdit == true)
                        {
                            if(false && arrBidCount[SelectedTable] > 1)
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables))))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent
                            }
                            else
                            {
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent
                            }
                        }
                        else
                        {
                            tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex];
                             row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrStaticColIds[SelectedTable][StaticIndex]; // Added By Dohatec to  Encrypted data save  Prevent
                        }
                    }

                    //alert(" td  in edit " + tdId +" row " + arrRow[SelectedTable] + " Nof " + NoofTables + " " + arrStaticColIds[SelectedTable][StaticIndex]);
                    addTD(new String(arrCellData[SelectedTable][addRow][addCol]),tr,1,tdId,true);
                    addHiddenInput(row_DataType,"1",tr_row_DataType); // Added By Dohatec to  Encrypted data save  Prevent
                    lbl_ids[lbl_ids.length] = "lbl@"+tdId.substring(2,tdId.length);
                    StaticIndex++;
                }//end of if govdata
                //for All Other Cells
                else
                {

                    if(isColTotalforTable[SelectedTable] != 0)
                    {

                        if(chkEdit == true)
                        {

                            if(arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                /*CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];
								RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
								tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+arrColIds[SelectedTable][SortOrder];*/


                                CompId = "row"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;
                                row_DataType = tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol; //Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])));
                                tdId = "td"+tableId+"_"+parseInt(addRow+(parseInt(arrRow[SelectedTable])*parseInt(arrBidCount[SelectedTable])-parseInt(arrBidCount[SelectedTable])))+"_"+addCol;

                            }
                            else
                            {
                                //alert(" ro " + addRow + " : arr " + arrRow[SelectedTable] + " : bid " + arrBidCount[SelectedTable] + " :: nof "  + NoofTables + " :dd " +  arrColIds[SelectedTable][SortOrder] + " :ss " + SortOrder + " : " +addCol);
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol; //Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable]));
                                tdId = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*parseInt(arrBidCount[SelectedTable])) - parseInt(NoofTables - arrBidCount[SelectedTable])))+"_"+addCol;

                            }
                        //alert(" co " + CompId + " : R " + RowIndex + " : td " + tdId);

                        }
                        else
                        {
                            //alert("SortOrder " + SortOrder);
                            CompId = "row"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            row_DataType = tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder]; //Added By Dohatec to  Encrypted data save  Prevent
                            RowIndex = (addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))));
                            tdId = "td"+tableId+"_"+(addRow+parseInt(parseInt(parseInt(arrRow[SelectedTable])*parseInt(parseInt(NoofTables) - 1) - parseInt(parseInt(NoofTables) - 1))))+"_"+arrColIds[SelectedTable][SortOrder];
                        }
                    //alert(" before com " + CompId + " : row " + RowIndex + " : tdId " + tdId);
                    }


                    if(isColTotalforTable[SelectedTable] == 0)
                    {
                        //alert(chkEdit);
                        if(chkEdit == true) // if edit
                        {
                            //alert('NoofTables : '+NoofTables);
                            //alert('arrRow[SelectedTable] : '+arrRow[SelectedTable]);
                            //alert('addRow : '+addRow);
                            //alert('totalBidTable : '+totalBidTable[SelectedTable]);
                            if(false && arrBidCount[SelectedTable] > 1) //if in edit table added more than once
                            {
                                //alert(totalBidTable[SelectedTable] + " ---- " + addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))));
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))))+"_"+arrColIds[SelectedTable][SortOrder];
                                //alert('CompId if : '+CompId);
                                row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))))+"_"+arrColIds[SelectedTable][SortOrder]; //Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                            else
                            {
                                //alert(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))));
                                //CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(totalBidTable[SelectedTable])))))+"_"+arrColIds[SelectedTable][SortOrder];
                                CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                                //alert('CompId else : '+CompId);
                                row_DataType =  "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder]; //Added By Dohatec to  Encrypted data save  Prevent
                                RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))));
                                tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(parseInt(NoofTables)-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            }
                        //totalBidTable[SelectedTable] = parseInt(parseInt(totalBidTable[SelectedTable])+ 1);

                        }
                        else
                        {
                            CompId = "row"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];
                            row_DataType = tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder]; //Added By Dohatec to  Encrypted data save  Prevent

                            RowIndex = (addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))));

                            tdId  = "td"+tableId+"_"+(addRow+(parseInt(arrRow[SelectedTable]*(NoofTables-1))))+"_"+arrColIds[SelectedTable][SortOrder];

                        }
                    }
                    //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);

                    //ColIndex = arrColIds[SelectedTable][SortOrder];
                    ColIndex = addCol;
                    /* Added By Dohatec to  Encrypted data save  Prevent Start*/
                        row_DataType_Value = arrDataTypesforCell[SelectedTable][addRow][addCol];
                        addHiddenInput(row_DataType,row_DataType_Value,tr_row_DataType);
                        /* Added By Dohatec to  Encrypted data save  Prevent End*/

                    //alert(arrCompType[SelectedTable][addRow][addCol]);
                    if(arrCompType[SelectedTable][addRow][addCol]=="TEXT")
                    {
                        //alert(arrDataTypesforCell[SelectedTable][addRow][addCol]);
                        if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "3")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckFloat1("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "4")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onChange","CheckNumeric("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "8")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "1")
                        {
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur1","validateNonUsed(this,2)",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "9")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "10")
                        {
                            var originalCmb = document.getElementById("idcombodetail"+tableId+"_"+addRow+"_"+addCol);
                            addHiddenInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","moneywithminus("+tableId+",this,"+SelectedTable+")",tdId,originalCmb,tableId);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "12")
                        {
                            addInputDateWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onclick","GetCal('"+CompId+"','"+CompId+"','"+tableId+"',this,'"+SelectedTable+"')",tdId,tableId,arrColFillBy[SelectedTable][addCol]);
                        }
                        else if(arrDataTypesforCell[SelectedTable][addRow][addCol] == "13"){ //yrj bugID# 5284
                            addInputWithEvent(theform,arrCompType[SelectedTable][addRow][addCol],CompId,tr,"onBlur","CheckNumeric3Decimal("+tableId+",this,"+SelectedTable+")",tdId,arrColFillBy[SelectedTable][addCol]);
                        }
                    }
                    else if(arrCompType[SelectedTable][addRow][addCol]=="SELECT")
                    {
                        var originalCmb = document.getElementById("row"+tableId+"_"+addRow+"_"+addCol);
                        addSelect(theform,CompId,originalCmb,tr,tdId);
                    }
                    else if(arrCompType[SelectedTable][addRow][addCol]=="TEXTAREA")
                    {
                        //alert(" co " + CompId + " : " + RowIndex + " : " + tdId);

                        if(readOnly == "readOnly")
                        {
                            addTextAreawithReadOnly(theform,CompId,tr,tdId,true);
                        }
                        else
                        {
                            addTextArea(theform,CompId,tr,tdId);
                        }

                    }

                    SortOrder++;

                //alert("Com " + CompId + " :: row " + arrRow[SelectedTable] + " no " + NoofTables );
                //alert("Row " + RowIndex);
                }//end of else other cells
            }//else for if not last row
        //alert(" before com " + CompId + " : row " + RowIndex + " : tdId " + tdId + " : arrco " + addCol);
        }//end of for arrCol
        //alert("tr.innerHTML:==>" + tr.innerHTML);
        /* Added By Dohatec to  Encrypted data save  Prevent Start*/
        //alert("tr_row_DataType.innerHTML:==>" + tr_row_DataType.innerHTML);
        tempBody.appendChild(tr_row_DataType);
        /* Added By Dohatec to  Encrypted data save  Prevent End*/
        tempBody.appendChild(tr);

    }//end of for arrRow

    //alert("chkEdit --->"+chkEdit+"<---------");
    if(chkEdit){
        totalBidTable[SelectedTable] = tableCnt;
    //alert(" aT the end " + totalBidTable[SelectedTable]);
    }
    if(arrBidCount[SelectedTable] != 0 )
        arrBidCount[SelectedTable] = parseInt(arrBidCount[SelectedTable]) + parseInt(1);

    checkForFunctions();
    for(var z=0;z<lbl_ids.length;z++)
    {
        var first_delim=lbl_ids[z].indexOf('@');
        var tblid=lbl_ids[z].substring(first_delim+1,lbl_ids[z].length);
        var second_delim=tblid.indexOf('_');
        var tableid=tblid.substring(0,second_delim);
        countFormula(tableid,document.getElementById(lbl_ids[z]));
    }
}

function applyRowFunction(IdList,tabIndex,tableId,storeIn,Rowid)
{
    //alert("In Row");
    var str = 0.0;
    if(IdList[0].toUpperCase()=='WORD')
    {
        var tBox = document.getElementById("row"+tableId+"_"+Rowid+"_"+IdList[IdList.length-2]);
        LoopCounter++;
        if(tBox!=null)
        {
            //      str += ((tBox.value=="")?0:parseInt(tBox.value));
            // Changed - Ravi. 8.5.06 : 11.07 am
            str += ((tBox.value=="")?0:tBox.value);
            document.getElementById("row"+tableId+"_"+Rowid+"_"+IdList[IdList.length-2]).readOnly=true;
            document.getElementById("row"+tableId+"_"+Rowid+"_"+IdList[IdList.length-2]).tabIndex=-1;
        }
    }
    else
        for(var j=0;j<arrCol[tabIndex];j++)
        {
            str = "";
            for(var i=0;i<IdList.length;i++)
            {
                var tBox ="";
                if(IdList[i] != 'p')
                {
                    tBox = document.getElementById("row"+tableId+"_"+Rowid+"_"+IdList[i]);
                    if(tBox!=null)
                    {
                        if(tBox.value=="")
                            str += 0;
                        else
                            str += tBox.value;
                    }
                    else if(isNaN(IdList[i]))
                    {
                        str += IdList[i];
                    }
                    else
                    {
                        if(document.getElementById("lbl@"+tableId+"_"+Rowid+"_"+IdList[i]) !=null)
                            str += document.getElementById("lbl@"+tableId+"_"+Rowid+"_"+IdList[i]).innerHTML;
                    }
                }
                else
                {
                    str += '100';
                }
                LoopCounter++;
            }
        }

    var resultTbox = document.getElementById("row"+tableId+"_"+Rowid+"_"+storeIn);

    if(resultTbox!=null)
    {
        if(IdList[0].toUpperCase() != 'WORD')
        {
            try
            {
                resultTbox.value = eval(Math.round(eval(eval(str)*100000))/100000);
            }
            catch(e)
            {
                resultTbox.value = "";
                resultTbox.style.color = "red";
            }
        }
        else
        {

            var res=DoIt(eval(parseFloat(str)));
            resultTbox.value=res;
        }
        resultTbox.readOnly=true;
        resultTbox.tabIndex=-1;
        countFormula(tableId,resultTbox);
    }
    str=0.0;
}


function applyColumnFunction(IdList,tabIndex,tableId,storeIn,Rowid,funcType)
{

    //alert('applyColumnFunction');
    var NoofTables = parseInt(arrTableAdded[tabIndex]);
    var str=0;
    var NoofRows=0;

    if(chkEdit != true)
    {
        NoofRows = parseInt(arrRow[tabIndex]*NoofTables)-parseInt(NoofTables);
    }
    else
    {
        //alert(" :: " + arrBidCount[tabIndex] + " :: " + arrRow[tabIndex] + " :: " + NoofTables);
        //NoofRows = parseInt(((parseInt(arrBidCount[tabIndex])-1)*arrRow[tabIndex])*NoofTables)-parseInt(NoofTables);
        NoofRows = parseInt((arrBidCount[tabIndex]*arrRow[tabIndex])-parseInt(arrBidCount[tabIndex]));
    }
    //alert(" N " + NoofRows);
    if(funcType.toUpperCase()=="TOTAL")
        for(var j=1;j<=NoofRows;j++)
        {
            var tBox = document.getElementById("row"+tableId+"_"+(j-1)+"_"+IdList[IdList.length-2]);
            //alert("document.getElementById(row"+tableId+"_"+(j-1)+"_"+IdList[IdList.length-2]);
            if(tBox!=null)
            {
                str += ((tBox.value=="")?0.0:parseFloat(tBox.value));
                try
                {
                    str=Math.round(eval(eval(str)*100000))/100000;
                }
                catch(e)
                {
                    str="987";
                }
            }
            LoopCounter++;

            if((j%(parseInt(NoofRows))) ==0)
            {
                // When adding rows gt is already getting calculated in second last column. so commented below line so that it wont happen bugid #5284
                //document.getElementById("row"+tableId+"_"+(j)+"_"+IdList[IdList.length-2]).value = eval(str);
                document.getElementById("row"+tableId+"_"+(j)+"_"+IdList[IdList.length-2]).readOnly=true;
                document.getElementById("row"+tableId+"_"+(j)+"_"+IdList[IdList.length-2]).tabIndex=-1;
                str=0;
                continue;
            }

        }
/*//if((j%(arrRow[tabIndex]*NoofTables))==0)

    //{
        document.getElementById("row"+tableId+"_"+(arrRow[tabIndex]*NoofTables)-1+"_"+IdList[IdList.length-2]).value = eval(str);
        str=0;
	//continue;
      //}
    */
}

function countFormula(tableId,tBox)
{
    //alert(" in countformula : " + tBox.id);
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));
    var SelectedTable;

    //alert('Rowid : '+Rowid);
    //alert('Colid : '+Colid);

    //alert(signClick);
    //if(signClick == false)
    //{
    for(var i=0;i<arr.length;i++)
    {
        if(arr[i]==tableId)
        {
            SelectedTable = i;
            LoopCounter++;
            break;
        }
    }

    if(arrTableFormula[SelectedTable] != undefined)
    {
        for(var i=0;i<arrTableFormula[SelectedTable].length;i++)
        {
            for(var j=0;j<arrIds[SelectedTable][i].length;j++)
            {
                if(arrIds[SelectedTable][i][j]==Colid) // Improve Place 1
                {
                    LoopCounter++;
                    var temp = arrIds[SelectedTable][i][0];
                    var  storeIn = arrFormulaFor[SelectedTable][i];

                    if(temp.toUpperCase()=='TOTAL'||temp.toUpperCase()=='AVG')
                    {
                        applyColumnFunction(arrIds[SelectedTable][i],SelectedTable,tableId,storeIn,Colid,temp.toUpperCase());
                        break;
                    }
                    else
                    {
                        applyRowFunction(arrIds[SelectedTable][i],SelectedTable,tableId,storeIn,Rowid);
                        //alert(" after " + Rowid);
                        break;
                    }
                }
            }
        }
    }
//}
}

function TestCalculate()
{
    var d1=new Date();
    for(var ii=0;ii<document.all.length;ii++)
    {
        try
        {
            var element=document.all[ii];
            var elementName=element.name;
            if(element.type !="hidden" && elementName.substring(0,3)=="row")
            {
                var tableId = elementName.substring(3,elementName.indexOf("_"));
                countFormula(tableId, element);
            }
        }
        catch(e)
        {
        //alert(document.all[ii].name);
        }
    }
}

function numerictestforfloat(textbox)
{

    var string=textbox.value;
    var re5digit=/^([0-9])+$/

    if (!re5digit.test(string))
    {
        return false;
    }
    return true;

}

function CheckSmallText(tableid, tBox){
    var val= trim(tBox.value);
    if(val.length > 1000){
        jAlert("Characters size must not exceed 1000 characters","Title", function(retVal) {});
        tBox.value = val.substring(0, 1000);
        tBox.focus();
        return false;
    }
}

function CheckLongText(tableid, tBox){
    var val= trim(tBox.value);
    if(val.length > 10000){
        alert("Characters size must not exceed 10000 characters");
        tBox.value = val.substring(0, 10000);
        tBox.focus();
        return false;
    }
}

function ChangeAllinnerHtml(theform)
{
    //alert("in fuction");
    var inputtag = document.getElementsByTagName("input");
    var textareatag = document.getElementsByTagName("textarea");
    var listtag = document.getElementsByTagName("select");
    var imglist = document.getElementsByTagName("img");
    var fillbycomp = 0;
    var tagid="";
    var datatypecomp=0;
    var celltableid = 0;
    var td;
    var func;
    var tindex;
    var inpvalue;
    var inptype;
    var ronly="";
    var rowid = "";
    var colid="";
    var arrrowcolid = new Array();


    for(var inputcount=0;inputcount<inputtag.length;inputcount++)
    {

        tagid = inputtag[inputcount].id;


        if(tagid.indexOf("_") != -1 && inputtag[inputcount].type=="text")
        {
            fillbycomp = parseInt(tagid.substring(tagid.lastIndexOf("_")+1,tagid.length));
            tagid = tagid.substring(0,tagid.lastIndexOf("_"));

            inpvalue = inputtag[inputcount].value;
            inptype = trim(inputtag[inputcount].type);

            datatypecomp = tagid.substring(tagid.lastIndexOf("_")+1,tagid.length);
            tagid = tagid.substring(0,tagid.lastIndexOf("_"));

            tindex = inputtag[inputcount].tabIndex;

            inputtag[inputcount].setAttribute("id",tagid);
            celltableid = tagid.substring(3,tagid.indexOf("_"));
            arrrowcolid = tagid.split("_");
            //alert(" arrowcolid " + arrrowcolid[0]);

            td = "td"+tagid.substring(3,tagid.length);

            // alert(td + " : " + inpvalue + " : " + datatypecomp + " : " + inputcount);
            // td = inputtag[inputcount].id;
            //alert( "Td " + tagid.substring(3,tagid.length));
            // alert(td);

            document.getElementById(td).innerHTML="";
            document.getElementById(td).setAttribute("class","normaltext");

            if(parseInt(datatypecomp) == 3 )
            {
                if(fillbycomp == 3)
                {
                    func = "countFormula_New(" + celltableid + ","+arrrowcolid[1]+","+arrrowcolid[2]+")";
                    ronly = "readonly";
                }
                else
                {
                    func = "CheckFloat1(" + celltableid + ",this)";
                    ronly="";
                }
            }

            if(parseInt(datatypecomp) == 1 )
            {
                //alert(" in " + tagid );
                //func = "validate(this,2)";
                func="";
                if(fillbycomp == 3)
                    ronly="readonly";
                else
                    ronly = "";

            }

            if(parseInt(datatypecomp) == 8 )
            {
                if(fillbycomp == 3)
                {
                    func = "countFormula_New(" + celltableid +  ","+arrrowcolid[1]+","+arrrowcolid[2]+")";
                    ronly = "readonly";
                }
                else
                {
                    func = "moneywithminus1(this);countFormula_New("+celltableid+","+arrrowcolid[1]+","+arrrowcolid[2]+")";
                    ronly="";

                }
            }

            if(parseInt(datatypecomp) == 4 )
            {
                if(fillbycomp == 3)
                {
                    func = "countFormula_New(" + celltableid + ","+arrrowcolid[1]+","+arrrowcolid[2]+")";
                    ronly = "readonly";
                }
                else
                {
                    func = "numeric(this);countFormula_New("+celltableid+","+arrrowcolid[1]+","+arrrowcolid[2]+")";
                    ronly="";
                }
            }

            if(parseInt(datatypecomp) == 7)
            {
                //alert(" in if " + inputtag[inputcount].value + " : " + inputcount);
                var row = document.getElementById(td).parentNode;
                addDate(theform,tagid,row,inpvalue,td);

            }
            else
            {

                //alert(" in else " + tagid + " :: " + datatypecomp);
                addInputWithEventInTdwithValue(theform,inptype,tagid,"onBlur",func,td,inpvalue,tindex,ronly);
            }

            document.getElementById(tagid).setAttribute("class","text-box");
        }


    }//for loop of input



    for(var textareacount=0;textareacount<textareatag.length;textareacount++)
    {

        tagid = textareatag[textareacount].id;

        if(tagid.indexOf("_") != -1)
        {
            fillbycomp = parseInt(tagid.substring(tagid.lastIndexOf("_")+1,tagid.length));
            tagid = tagid.substring(0,tagid.lastIndexOf("_"));

            textareatag[textareacount].setAttribute("id",tagid);

            if(parseInt(fillbycomp) == 3 )
            {
                textareatag[textareacount].readOnly = true;
            //alert(" in if");
            }
            else
            {
                textareatag[textareacount].readOnly = false;
            }
        }

    }//for loop of input


    for(var listcount=0;listcount<listtag.length;listcount++)
    {
        listtag[listcount].disabled = false;
        listtag[listcount].attachEvent("onChange",xyz);
    }

    return true;
}


function Setup()
{
    // set Globals
    Affix = new Array('Units', 'Thousand', 'Lakh', 'Crore')
    for(i=1;i<5;i++)
    {
        Affix[3+i] = Affix[0+i]
    }
    Name = new Array
    ('Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six',
        'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve',
        'Thirteen', 'Fourteen', 'Fifteen',  'Sixteen', 'Seventeen',
        'Eighteen', 'Nineteen')
    Namety = new Array('Twenty', 'Thirty', 'Forty',
        'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety')
    PointName = new Array
    ('Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six',
        'Seven', 'Eight', 'Nine')
}

function DoIt(Numeric){
    Setup();
    Q = Numeric;
    if (isNaN(parseFloat(Q))){
        Word = '';
    }else{
        //-----------Code by umesh ------------------
        Word=''
        var tempStr1=new String();
        var tempStr3=new String();
        tempStr1=Q;
        tempStr1=tempStr1.toString();
        var tempstr2=tempStr1.search('-');
        if (tempstr2==0){
            Word = 'Minus ';
        //tempStr3='Minus ';
        //tempStr3.fontcolor("red");
        //Word=tempStr3;
        }
        Q=tempStr1.replace('-','')

        //---------------------comment old---------------------
        /*P = Math.round(((Q - Math.floor(Q))*100)*Math.pow(10,5))/Math.pow(10,5);
		//P = Math.round((Q - Math.floor(Q))*100)
		Q = Math.floor(Q)
	  	if (P<=9){
	  		P='0'+P;
	  	}

		if (P==0){
			//Word = 'Rupee' + (Q==1?'':'s') + ' ' + TextCash(Q, 0) + 'Only'
			Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0) + '';
		}else{
    			//Word = 'Rupee' + (Q==1?'':'s') + ' ' + TextCash(Q, 0) + 'and Paisa' + ' ' + TextCash1(P, 0) + 'Only'
    			Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0) + 'point' + ' ' + TextCash2(P, 0) + '';
		}*/
        //----------------------comment old---------------------

        //-------------------yrj change-------------------------
        var P, pointStr3;
        qStr = Q.toString();
        if(qStr.indexOf(".") != -1){
            qStrAfterPoint = qStr.substring(qStr.indexOf(".")+1, qStr.length);
            P = eval(qStrAfterPoint);
            if(P == 0){
                pointStr3 = '';
            }else{
                var pointStr1= new String();
                var pointlen1,pointStr2;
                pointStr2="";
                pointStr3="";
                pointStr1=qStrAfterPoint;
                pointlen1= pointStr1.length;

                for(pointi=0;pointi<pointlen1;pointi++){
                    pointStr2=pointStr1.substr(pointi,1);
                    if(PointName[pointStr2] != undefined){
                        pointStr3=pointStr3+PointName[pointStr2]+' ';
                    }
                }
            }
        }else{
            P = 0;
        }

        Q = Math.floor(Q)
        if (P<=9){
            P='0'+P;
        }

        R = Q%10000000000;
        if (P==0){
            if(Q < 10000000000){
                Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0) + '';
            }else{
                Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0) + '';
                if(R == 0){
                    Word = Word + 'Crore';
                }

                X = Math.floor(Q/10000000);
                if(R!=0 && (X%10) == 0){
                    Word = '';
                    Word = Word + '' + (X==1?'':'') + '' + TextCash(X, 0) + 'Crore and ';
                    Word = Word + '' + (R==1?'':'') + '' + TextCash(R, 0) + '';
                }
            }
        }else{
            //TextCash2(P, 0)
            if(Q < 10000000000){
                Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0) + 'point' + ' ' + pointStr3 + '';
            }else{
                Word = Word + '' + (Q==1?'':'') + '' + TextCash(Q, 0);
                if(R == 0){
                    Word = Word + 'Crore ';
                }
                Word = Word + 'point' + ' ' + pointStr3 + '';

                X = Math.floor(Q/10000000);
                if(R!=0 && (X%10) == 0){
                    Word = '';
                    Word = Word + '' + (X==1?'':'') + '' + TextCash(X, 0) + 'Crore and ';
                    Word = Word + '' + (R==1?'':'') + '' + TextCash(R, 0) + 'point' + ' ' + pointStr3 + '';
                }
            }
        }
    //-------------------yrj change-------------------------
    }
    return Word
}

function WORD(no)
{
    //alert(no);
    //return DoIt(eval(no));
    return CurrencyConverter(eval(no));
}

function Small(TC, J, K)
{
    if (J==0) return TC
    if (J>999) return ' Internal ERROR: J = ' + J + ' (>999)'
    var S = TC
    if (J>99)
    {
        S += Name[Math.floor(J/100)] + ' Hundred ' ;
        J %= 100
        if (J>0) S += 'and '
    }
    else if ((S>'') && (J>0) && (K==0)) S += 'AND '
    if (J>19)
    {
        S += Namety[Math.floor(J/10)-2] ;
        J %= 10 ;
        S += ( J>0 ? '-' : ' ')
    }
    if (J>0) S += Name[J] + ' '
    if (K>0) S += Affix[K] + ' '
    return S
}

function TextCash(L, K)
{
    if (L==0) return (K>0 ? '' : 'Zero ')
    if (K==0 || K==3)
    {
        return Small(TextCash(Math.floor(L/1000), K+1), L%1000, K)
    }
    else
    {
        return Small(TextCash(Math.floor(L/100), K+1), L%100, K)
    }
}

function TextCash1(L, K)
{
    if (L==0) return (K>0 ? '' : 'NIL ')
    return Small(TextCash1(Math.floor(L/100), K+1), L%100, K)
}

function TextCash2(L, K)
{
    if (L==0) return (K>0 ? '' : 'NIL ')
    {
        var pointStr1= new String();
        var pointlen1,pointStr2,pointStr3;
        pointStr2="";
        pointStr3="";
        pointStr1=L;
        pointStr1=pointStr1.toString();
        pointlen1= pointStr1.length;
        //alert(pointStr1);
        for(pointi=0;pointi<pointlen1;pointi++)
        {
            pointStr2=pointStr1.substr(pointi,1);
            pointStr3=pointStr3+PointName[pointStr2]+' ';
        //alert(pointStr3);
        }
        return pointStr3;
    }
}

function trim(s)
{
    while (s.substring(0,1) == ' ')
    {
        s = s.substring(1,s.length);
    }
    while (s.substring(s.length-1,s.length) == ' ')
    {
        s = s.substring(0,s.length-1);
    }
    return s;
}

function CheckFloat1(tableid,tBox,tableIndex)
{

    var val= tBox.value;
    //alert(val);
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));
    var re5digit=/^[0-9]+[\.][0-9]{1,3}$/

    if(!verified)
        return false;

    if(val=="")
        return false;

    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0)
    {
        jAlert("please remove all leading zeros.","", function(retVal) {
            tBox.value = "";
            tBox.focus();
        });

        return false;
    }


    if(!re5digit.test(val) )
    {
        //if bytype added by Taher ID: 3907
        if(tBox.getAttribute('btype')!=null && tBox.getAttribute('btype')=='5m5'){
            return chkFloatMinus5Plus5(tableid, tBox, tableIndex);
        }else{
            if(numerictestforfloat(tBox))
            {
                countFormula_New(tableid,Rowid,Colid,tableIndex);
                return true;
            }
            else
            {
                jAlert("Allows only positive numerals and up to maximum 3 decimal places.","Report Alert", function(retVal) {
                   // tBox.value="";
                    tBox.focus();
                });
                return false;;
            }
        }
    }
    else
    {
        countFormula_New(tableid,Rowid,Colid,tableIndex);
        return true;
    }
}

function CheckFloat2(tableid,tBox,tableIndex)
{

    var val= tBox.value;
    //alert(val);
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));
    var re5digit=/^[0-9]+[\.][0-9]{1,3}$/

    if(!verified)
        return false;

    if(val=="")
        return false;

    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0)
    {
        jAlert("please remove all leading zeros.","", function(retVal) {
            tBox.value = "";
            tBox.focus();
        });

        return false;
    }


    if(!re5digit.test(val) )
    {
        if(numerictestforfloat(tBox))
        {
            countFormula_New(tableid,Rowid,Colid,tableIndex);
            return true;
        }
        else
        {
            jAlert("Allows only positive numerals and up to maximum 3 decimal places.","Report Alert", function(retVal) {
                tBox.value="";
                tBox.focus();
            });
            return false;;
        }
    }
    else
    {
        countFormula_New(tableid,Rowid,Colid,tableIndex);
        return true;
    }
}



function chkFloatMinus5Plus5(tableid,tBox,tableIndex)
{
    var val= tBox.value;
    //alert(val);
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));
    var re5digit=/^[-+]?[0-9]+[.]?[0-9]*$/

    if(!re5digit.test(val)){
        jAlert("Allows -5 to +5 numerals and maximum 3 digits after decimal (.) point.","", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }

    if(!verified || val=="")
    {
        return false;
    }

    var tmpArray = val.split('.');
    //alert('length '+tmpArray.length);
    //    alert(Math.round(parseFloat(tmpArray[0])));
    if( tmpArray.length > 2 ||
        (tmpArray.length == 2 && tmpArray[1].length > 3) ||
        (parseInt(tmpArray[0]) > 5 || parseInt(tmpArray[0]) < -5) ||
        ((parseInt(tmpArray[0]) >= 5 || parseInt(tmpArray[0]) <= -5) && parseInt(tmpArray[1]) > 0)
        )
        {
        jAlert("Allows -5 to +5 numerals and maximum 3 digits after decimal (.) point.","", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }

    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && eval('val') != 0)
    {
        jAlert("please remove all leading zeros.","", function(retVal) {
            //tBox.value = "";
            tBox.focus();
        });
        return false;
    }

    countFormula_New(tableid,Rowid,Colid,tableIndex);
    return true;
/*if(!re5digit.test(val) )
    {
        if(numerictestforfloat(tBox))
        {
            countFormula_New(tableid,Rowid,Colid);
            return true;
        }
        else
        {
            jAlert("Allows -5 to +5 numerals & 3 digits after decimal (.) point.","", function(retVal) {
                tBox.value="";
                tBox.focus();
            });
            return false;
        }
    }
    else
    {
        countFormula_New(tableid,Rowid,Colid);
        return true;
    }*/
}

function chkSizeTextBox(tableId,tBox){
    if(tBox.value.length > 1000){
        jAlert("Please enter maximum 1000 characters.","Characters Allowed", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }
}
function chkSizeTextArea(tableId,tBox){
    if(tBox.value > 5000){
        jAlert("Please enter maximum 5000 characters.","Characters Allowed", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }
}
function CheckNumeric(tableId,tBox,tableIndex)
{
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));

    var string=trim(tBox.value);
    var re5digit=/^([0-9])+$/

    if(!verified)
        return false;

    if(string.indexOf('0') == 0 && string.indexOf('.') != 1 && string != 0)
    {
        jAlert("Please remove all leading zeroes.","Title", function(retVal) {
            tBox.value="";
            tBox.focus();
        });

        return false;
    }


    if(string.length > 0){
        if (!re5digit.test(string)){
            jAlert("Please enter numeric value (0 to 9) only.","Title", function(retVal) {
                tBox.value="";
                tBox.focus();
            });

            return false;
        }
    }

    countFormula_New(tableId,Rowid,Colid,tableIndex);
    return true;
}

function CheckNumeric3Decimal(tableId,tBox,tableIndex)
{
    // Dohatec Start
    // If the fields are readonly it will return true.
    // Field is not need to be checked.
    if($('#'+tBox.id).attr('readonly'))
    {
        return true;
    }
    // Dohatec End

    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));

    var string=trim(tBox.value);

    if(!verified)
        return false;

    if(string.indexOf('0') == 0 && string.indexOf('.') != 1 && string != 0)
    {
        jAlert("Please remove all leading zeroes.","Title", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }

    if(string.length > 0){

        var re = new RegExp("^\\d+\\.\\d{3}?$");

        if(!re.test(string)){
            jAlert("- Please enter Positive Numbers (0-9) only.<br /> - 3 Digits after decimal (.) are required.<br /> - 3rd Digit after Decimal (.) must not be Zero.<br /> - For example 1001.123","Report Alert", function(retVal) {
                //tBox.value = "";
                tBox.focus();
            });
            return false;
        }

        if(string.substring(string.length - 1,string.length) == 0){
            jAlert("- Please enter Positive Numbers (0-9) only.<br /> - 3 Digits after decimal (.) are required.<br /> - 3rd Digit after Decimal (.) must not be Zero.<br /> - For example 1001.123","Report Alert", function(retVal) {
                //tBox.value = "";
                tBox.focus();
            });
            return false;
        }
    }
//    alert(tableId);
//    alert(Rowid);
//    alert(Colid);
//    alert(tableIndex);
    countFormula_New(tableId,Rowid,Colid,tableIndex);
    return true;
}

function CheckDate(tableId,tBox,txtname,controlname,tableIndex)
{
    //alert("CheckDate tableId : "+tableId+"tBox : "+tBox+"txtName : "+txtname+"controlName : "+controlname+"tableIndex"+tableIndex);
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));

    countFormula_New(tableId,Rowid,Colid,tableIndex);
    return true;
}


function moneywithminus(tableId,tBox,tableIndex)
{
    var Colid = tBox.id.substring(tBox.id.lastIndexOf("_")+1,tBox.id.length);
    var Rowid = tBox.id.substring(tBox.id.indexOf("_")+1,tBox.id.lastIndexOf("_"));
    var val=tBox.value;

    var re5digit=/^([0-9])+$/
    var v2;

    if(!verified)
        return false;

    if(val == "")
        return false;
    ;

    var val1;
    if(val.indexOf('0') == 0 && val.indexOf('.') != 1 && val != 0)
    {
        jAlert("Please remove all leading zeros.","Title", function(retVal) {
            tBox.value="";
            tBox.focus();
        });
        return false;
    }
    if(val.indexOf('-') == 0 )
    {
        if(val.indexOf('.') != 1)
        {
            if(val.indexOf('0') == 1 && val.indexOf('.') != 2)
            {
                jAlert("Please remove all leading zeros.","Title", function(retVal) {
                    tBox.value="";
                    tBox.focus();
                });
                return false;
            }
        }
        val1 = val.substring(val.indexOf('-')+1,val.length);
        if(val1.indexOf('-') > -1)
        {
            jAlert("Invalid Number Format","Title", function(retVal) {
                tBox.value="";
                tBox.focus();
            });
            return false;
        }
    }
    if(val.indexOf('.') > -1)
    {
        val1 = val.substring(val.indexOf('.')+1,val.length);
        var val2;
        if(val1.indexOf('.') > -1)
        {
            jAlert("Please remove additional decimal (.)","Title", function(retVal) {
                tBox.value="";
                tBox.focus();
            });
            return false;
        }
    }

    if(val.indexOf('-') != 0)
    {
        if(val.indexOf('.') != -1)
            v2 = val.substring(0,val.indexOf('.'));
        else
            v2 = val.substring(0,val.length);

        if(val.length > 0)
        {
            if (!re5digit.test(v2))
            {
                jAlert("Please enter a  proper value !","Title", function(retVal) {
                    tBox.value="";
                    tBox.focus();
                });
                return false;
            }
        }
    }
    if(val.indexOf('-') == 0)
    {
        if(val.indexOf('.') == -1)
        {
            v2 = val.substring(1,val.length);
        }
        else
        {
            v2 = val.substring(1,val.indexOf('.'));
        }

        if(val.length > 0)
        {
            if (!re5digit.test(v2))
            {
                jAlert("Please enter a  proper value !","Title", function(retVal) {
                    tBox.value="";
                    tBox.focus();
                });
                return false;
            }
        }
    }

    re5digit=/^[-][0-9]*$/

    if(!re5digit.test(val))
    {
        if(CheckFloat2(tableId, tBox,tableIndex))
        {
            return true;
        }
        else
        {
            tBox.value="";
            tBox.focus();
            return false;
        }
    }
    countFormula_New(tableId,Rowid,Colid,tableIndex);
    return true;
}

function changeTextVal(combo,tableid,tableIndex,conversionRate){
/*    if(combo.options[combo.options.selectedIndex].text != 'Yes'){
        alert('Please select value from combo box');        
        return false
    } else {
*/
        var textId=combo.id;
        textId = textId.replace("idcombodetail","row");

        var Colid = textId.substring(textId.lastIndexOf("_")+1,textId.length);
        var Rowid = textId.substring(textId.indexOf("_")+1,textId.lastIndexOf("_"));

        document.getElementById(textId).value=combo.value;
        if(conversionRate != null){
            var json = JSON.parse(JSON.stringify(conversionRate));
            var newField = "row"+tableid+"_"+Rowid+"_"+(Number(Colid)+1);
            if(document.getElementById(newField)){
               document.getElementById(newField).value = json[combo.value];
            }
            countFormula_New(tableid,Rowid,Number(Colid)+1,tableIndex);
        }
        else
            countFormula_New(tableid,Rowid,Colid,tableIndex);
            
        
//    }
}

function changeCurrencyValue(textId,value,tableid){

    var Colid = textId.substring(textId.lastIndexOf("_")+1,textId.length);
    var Rowid = textId.substring(textId.indexOf("_")+1,textId.lastIndexOf("_"));

    document.getElementById(textId).value=value;
    countFormula_New(tableid,Rowid,Colid);
}

function CurrencyConverter(Numeric)
{
    var WordForm = '';
    var NumericValue = parseFloat(Numeric);
    var th = ['','THOUSAND','MILLION', 'BILLION','TRILLION'];
    var dg = ['ZERO','ONE','TWO','THREE','FOUR', 'FIVE','SIX','SEVEN','EIGHT','NINE'];
    var tn = ['TEN','ELEVEN','TWELVE','THIRTEEN', 'FOURTEEN','FIFTEEN','SIXTEEN', 'SEVENTEEN','EIGHTEEN','NINETEEN'];
    var tw = ['TWENTY','THIRTY','FORTY','FIFTY', 'SIXTY','SEVENTY','EIGHTY','NINETY'];


    var ShouldConvertFlag = 1;
    var StringFormatNumber = NumericValue.toString();
    StringFormatNumber = StringFormatNumber.replace(/[\, ]/g,'');
    if (StringFormatNumber != parseFloat(StringFormatNumber)) 
    {
        WordForm += "NOT A NUMBER";
        ShouldConvertFlag = 0;
    }

    if(ShouldConvertFlag==1)
    {
        var LengthUptoPrecision = StringFormatNumber.indexOf('.');
        if (LengthUptoPrecision == -1)
            LengthUptoPrecision = StringFormatNumber.length;
        if (LengthUptoPrecision > 15)
        {
            WordForm += "TOO BIG NUMBER";
            ShouldConvertFlag = 0;
        }
    }

    if(ShouldConvertFlag==1)
    {
        var EachDigit = StringFormatNumber.split(''); 
        var TraversStart = 0;
        if(EachDigit[0] == '-')
        {
            TraversStart++;
            WordForm += "MINUS ";
        }
        var sk = 0;
        for (var i=TraversStart;   i < LengthUptoPrecision;  i++) 
        {
            if ((LengthUptoPrecision-i)%3==2) 
            { 
                if (EachDigit[i] == '1') 
                {
                    WordForm += tn[Number(EachDigit[i+1])] + ' ';
                    i++;
                    sk=1;
                } 
                else if (EachDigit[i]!=0) 
                {
                    WordForm += tw[EachDigit[i]-2] + ' ';
                    sk=1;
                }
            } 
            else if (EachDigit[i]!=0) 
            { // 0235
                WordForm += dg[EachDigit[i]] +' ';
                if ((LengthUptoPrecision-i)%3==0) 
                    WordForm += 'HUNDRED ';
                sk=1;
            }
            else if (EachDigit[i]==0 && (LengthUptoPrecision-TraversStart)==1) 
            { // 0.00
                WordForm += 'ZERO';
            }
            if ((LengthUptoPrecision-i)%3==1) 
            {
                if (sk)
                    WordForm += th[(LengthUptoPrecision-i-1)/3] + ' ';
                sk=0;
            }
        }

        if (LengthUptoPrecision != StringFormatNumber.length) 
        {
            var y = StringFormatNumber.length;
            WordForm += 'POINT ';
            for (var i=LengthUptoPrecision+1; i<y; i++)
                WordForm += dg[EachDigit[i]] +' ';
        }
    }
    WordForm.replace(/\s+/g,' ');

    //document.getElementById(msgPlace).innerHTML = WordForm;
    return WordForm;
}