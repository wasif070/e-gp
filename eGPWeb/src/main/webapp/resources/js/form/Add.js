function addComponent(theform,row,compType,compName)
{
    var Cell;
    var comp ;

    Cell = document.createElement("TD");

    if(compType=="SELECT")
    {
        comp = document.createElement(compType);

        var len = theform.cmbItemName1.length;

        for(var i=0;i<len-1;i++)
        {
            comp.options[i] = new Option(theform.cmbItemName1.options[i].text,theform.cmbItemName1.options[i].value)
        }

    }
    else
    {
        comp = document.createElement("INPUT");
        comp.setAttribute("type",compType);
        comp.setAttribute("size",10);
    }

    comp.setAttribute("name",compName);
    comp.setAttribute("id",compName);
    comp.setAttribute("style","padding: 2px; margin: 5px");
    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addSelect(theform,Name,cmb,row,tdId)
{  
    var Cell;
    var comp ;

    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);

    comp = document.createElement("SELECT");

    if(cmb!=null){
        var len = cmb.length;
        for(var i=0;i<len;i++)
        {
            comp.options[i] = new Option(cmb.options[i].text,cmb.options[i].value);
            if(cmb.options[i].selected){
                comp.options[i].selected = true;
            }
        }
    }


    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("class","formTxtBox_1");
    comp.setAttribute("style","padding: 2px; margin: 5px");
    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addCombo(theform,Name,cmb,row,tdId)
{
    var Cell;
    var comp ;

    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);

    comp = document.createElement("SELECT");
    var len = cmb.length;

    for(var i=0;i<len;i++)
    {
        comp.options[i] = new Option(cmb.options[i].text,cmb.options[i].value);
        if(cmb.options[i].selected){
            comp.options[i].selected = true;
        }
    }

    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("class","formTxtBox_1");
    comp.setAttribute("style","padding: 2px; margin: 5px");
    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addHiddenVar(Cell,cmbWithCalcName,selCmbWithCalcName,cmbWOCalcName,selCmbWOCalcName,row,tdId,cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue,bCmbval){
    var span1;
    var span2;
    var comp1;
    var comp2;
    var i=0;

    span1 = document.createElement("span");
    span1.setAttribute("id",cmbWithCalcName);
    if(bCmbval!=9){
        span1.style.display="none";
    }


    comp1 = document.createElement("SELECT");
    var len1 = cmbWithCalcText.length;

    for(i=0;i<len1;i++)
    {
       if(i==1) comp1.options[i] = new Option(cmbWithCalcText[i],cmbWithCalcValue[i],"","selected");
       else comp1.options[i] = new Option(cmbWithCalcText[i],cmbWithCalcValue[i]);
    }
    comp1.setAttribute("name",selCmbWithCalcName);
    comp1.setAttribute("id",selCmbWithCalcName);
    comp1.setAttribute("class","formTxtBox_1");
    comp1.setAttribute("style","padding: 2px; margin: 5px");
   // comp1.setAttribute("style","width:98%");
    span1.appendChild(comp1);
    Cell.appendChild(span1);

    span2 = document.createElement("span");
    span2.setAttribute("id",cmbWOCalcName);
    span2.setAttribute("style","display: block;text-align:center;");
    if(bCmbval!=10){
        span2.style.display="none";
    }

    comp2 = document.createElement("SELECT");
    var len2 = cmbWoCalcText.length;

    for(i=0;i<len2;i++)
    {
        if(i==1) comp2.options[i] = new Option(cmbWoCalcText[i],cmbWoCalcValue[i],"","selected");
        else comp2.options[i] = new Option(cmbWoCalcText[i],cmbWoCalcValue[i]);
    }
    comp2.setAttribute("name",selCmbWOCalcName);
    comp2.setAttribute("id",selCmbWOCalcName);
    comp2.setAttribute("class","formTxtBox_1");
    comp2.setAttribute("style","width:80%;");

    span2.appendChild(comp2);
    Cell.appendChild(span2);

    return Cell;

}
function addSelectWithAlign(theform,Name,cmb,row,tdId,allign,rowCount,i)
{
    var Cell;
    var comp ;

    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);

    comp = document.createElement("SELECT");

    var len = cmb.length;

    for(var i=0;i<len;i++)
    {
        comp.options[i] = new Option(cmb.options[i].text,cmb.options[i].value);
        if(cmb.options[i].selected)
            comp.options[i].selected = true;
    }

    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("class","formTxtBox_1");
    comp.setAttribute("style","padding: 2px; margin: 5px");
    //var selObj = document.getElementById("DataType"+rowCount+"_"+i);
    // var mainCmbId = parseInt(rowCount)+1;
    //comp.onchange = function(){ checkParentVal("DataType"+rowCount+"_"+i,'DataType'+,this.value) };


    if(allign != "")
    {
        Cell.setAttribute("align","center");
    }

    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addSelectWithCondition(theform,Name,cmb,row,tdId,allign,rowCount,j, isAdmin)
{

    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    comp = document.createElement("SELECT");

    var len = cmb.length;

    for(var i=0;i<len;i++)
    {
        comp.options[i] = new Option(cmb.options[i].text,cmb.options[i].value);
        if(cmb.options[i].selected)
            comp.options[i].selected = true;
    }

    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("class","formTxtBox_1");
    comp.setAttribute("style","padding: 2px; margin: 5px");
   
    if (isAdmin=='No'){ // if PA User, data type cannot be changed. if e-GP Admin, data type can be changed
        comp.disabled = true;
    }
    var mainCmbId = parseInt(j)-1;
    //  if(document.getElementById("isBOQForm")){
    //   var isBOQ = document.getElementById("isBOQForm").value;

    //  if(isBOQ == "true"){
    comp.onchange = function(){

        checkParentVal("DataType"+rowCount+"_"+j,'DataType'+mainCmbId,this.value)
    };
    //   }
    //  }

    if(allign != "")
    {
        Cell.setAttribute("align","center");
        
    }

    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addSelectWithConditionDumped(theform,Name,cmb,row,tdId,allign,rowCount,j){
    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);

    if(allign != "")
    {
        Cell.setAttribute("align","center");
    }

    //Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}
function addDate(theform,compType,compName,row,tdId)
{
    var Cell;
    var strin;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    strin = "<input name='"+compName+"' id='"+compName+"' type='text' class='formTxtBox_1' ";
    strin = strin + "style='width:80%;height: 20px' readonly='true' onfocus=GetCal('"+compName+"','"+compName+"') />";
    strin = strin + "&nbsp;<a onclick=GetCal('"+compName+"','"+compName+"') id='img"+compName+"' name='img"+compName+"' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick=GetCal('"+compName+"','img"+compName+"') id='img"+compName+"' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>";
    Cell.innerHTML = strin;
    row.appendChild(Cell);
    return Cell;
}

function addInput(theform,compType,compName,row,tdId)
{
    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    comp = document.createElement("INPUT");
    comp.setAttribute("type",compType);
    if(compType == "hidden" || compType == "checkbox" || compType == "radio"){
        comp.setAttribute("class","formTxtBox_1 ");

    }else{
        comp.setAttribute("class","formTxtBox_1 newinputwidth");
        comp.setAttribute("style","padding: 2px; margin: 5px;");
    }
    comp.setAttribute("name",compName);
    comp.setAttribute("id",compName);

    if(compType == "text")
    {
        comp.setAttribute("onBlur","validateThisObject(this);");
    }

    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addInputWithBtn(theform,compType,compName,btnName,row,tdId)
{
    var Cell;
    var comp ;
    Cell = document.createElement("TD");	    
    Cell.setAttribute("id",tdId);
    comp = document.createElement("INPUT");
    comp.setAttribute("type",compType);
    if(compType == "hidden" || compType == "checkbox" || compType == "radio"){
        comp.setAttribute("class","formTxtBox_1");
        
    }else{
        comp.setAttribute("class","formTxtBox_1");
        comp.setAttribute("style","padding: 2px; margin: 5px");
    }
    comp.setAttribute("name",compName);
    comp.setAttribute("id",compName);

    if(compType == "text")
    {
        comp.setAttribute("onBlur","validateThisObject(this);");
    }
    
    Cell.appendChild(comp);

    var boqObjVal = false;
    if(document.getElementById("isBOQForm") != null){
        boqObjVal = document.getElementById("isBOQForm").value;
    }
//hide copy group button
//    if(boqObjVal == "true" && false){ 
//        var btn = document.createElement("INPUT");
//        btn.setAttribute("type","BUTTON");
//        btn.setAttribute("name",btnName);
//        btn.setAttribute("id",btnName);
//        btn.setAttribute("value","Copy Group");
//        btn.setAttribute("onClick","changeGroupVal(this)");
//        Cell.appendChild(btn);
//    }
    
    row.appendChild(Cell);
    return Cell;
}

function addInputWithAlign(theform,compType,compName,row,tdId,align, tdOrTh)
{
    if(tdOrTh == undefined){
        tdOrTh = "TD";
    }
    var Cell;
    var comp ;

    Cell = document.createElement(tdOrTh);
    Cell.setAttribute("id",tdId);
    //Cell.setAttribute("style","text-align:center");
    comp = document.createElement("INPUT");
    comp.setAttribute("type",compType);
    comp.setAttribute("name",compName);
    comp.setAttribute("id",compName);
    comp.setAttribute("className","formTxtBox_1");
    if(compType == "hidden" || compType == "checkbox" || compType == "radio"){

    }else{
      //  comp.setAttribute("style","width:98%; height: 30px");
        comp.setAttribute("style","padding: 2px; margin: 5px");
    }
    if(compType == "checkbox"){
        Cell.setAttribute("align", "center");
    }else{

    }
    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}



function addTextArea(theform,Name,row,tdId)
{
    var Cell;
    var comp ;

    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    //Cell.setAttribute("style","text-align: center;");
    comp = document.createElement("TEXTAREA");
    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("rows","4");
    comp.setAttribute("class","formTxtBox_1 newTAwidth");
    //comp.setAttribute("class","newTAwidth");
    Cell.appendChild(comp);
    //alert("a");
    row.appendChild(Cell);
    return Cell;
}

function addTextAreawithReadOnly(theform,Name,row,tdId,rOnly)
{
    var Cell;
    var comp ;

    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    comp = document.createElement("TEXTAREA");
    comp.setAttribute("name",Name);
    comp.setAttribute("id",Name);
    comp.setAttribute("rows","4");
    comp.setAttribute("readOnly",true);
   // comp.setAttribute("class","formTxtArea_1");
    comp.setAttribute("style","padding: 2px; margin: 5px");
    comp.setAttribute("tabindex","-1");
    Cell.appendChild(comp);
    row.appendChild(Cell);
    return Cell;
}

function addTD(inHTML,row,colspan,id,isLabel)
{
    var Cell;
    var lbl;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",id);
    if(colspan == 0){
        Cell.setAttribute("valign","top");
    } else {
        Cell.setAttribute("colspan",parseInt(colspan));
    }
    //Cell.innerHTML=inHTML;

    if(isLabel == true)
    {
        lbl = "<label id='lbl@" + id.substring(2,id.length) + "' name='" +id.substring(2,id.length) +"' > " + inHTML  + " </label>";
        Cell.innerHTML = lbl;
    }
    else
    {
        Cell.innerHTML=inHTML;
    }
    row.appendChild(Cell);
    return Cell;
}

//**********************Added By Ravi***********
function addInputWithEvent(theform,compType,compName,row,eventName,eventFunction,tdId,fillBy)
{
      
    var Cell;
    var comp ;
    var inHtml="";
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    if(compType == "HIDDEN" || compType == "CHECKBOX" || compType == "RADIO")
    {
        inHtml = "<input type='"+compType+"'  name='"+compName+"'  id='"+compName+"'  "+eventName+"='"+eventFunction+"' class='formTxtBox_2' ";
    }
    else
    {
        inHtml="<input type='"+compType+"'  name='"+compName+"'  id='"+compName+"'  "+eventName+"='"+eventFunction+"' class='formTxtBox_2' ";
    }
    
    if(fillBy != undefined && fillBy == 3)
    {
         inHtml = inHtml +  " readonly ";
    }   
        inHtml =  inHtml +  " >";
        
    Cell.innerHTML=inHtml;
    row.appendChild(Cell);
}

function addInputDateWithEvent(theform,compType,compName,row,eventName,eventFunction,tdId,tableId,fillBy)
{
    var inHtml="";
    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);
    inHtml= "<input type=\""+compType+"\"  name=\""+compName+"\"  id=\""+compName+"\"  readonly=\"true\"  class=\"formTxtDate_1\"  ";
   
    if(fillBy == undefined || fillBy != 3)
    {
        inHtml = inHtml + eventName+ "= \""+eventFunction+"\"  "; 
    }
    
    inHtml = inHtml +" >"
    Cell.innerHTML =inHtml;
    if(fillBy == undefined || fillBy != 3)
    {
        Cell.innerHTML+="<img border='0' style='vertical-align:middle;' alt='Select a Date' name='calendarIcn' id='img"+compName+"' onclick=\"GetCal('"+compName+"','"+compName+"','"+tableId+"',this,'0');\" src='../resources/images/Dashboard/calendarIcn.png'>";
    }
    //alert(Cell.innerHTML);
    row.appendChild(Cell);
}


function addHiddenInputWithEvent(theform,compType,compName,row,eventName,eventFunction,tdId,cmb,tableId,defaultVal)
{
    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);


    comp = document.createElement("SELECT");
    var len = cmb.length;
    var defaultVal = "";

    for(var i=0;i<len;i++)
    {
        comp.options[i] = new Option(cmb.options[i].text,cmb.options[i].value);
        if(defaultVal==cmb.options[i].value){
            comp.options[i].selected= true;
            defaultVal = cmb.options[i].value;
        }
    }
    if(defaultVal==""){
        defaultVal = cmb.options[0].value;
    }

    Cell.innerHTML="<input type='"+compType+"'  name='"+compName+"'  id='"+compName+"'  "+eventName+"='"+eventFunction+"' class='formTxtBox_1' style='display:none;' value='"+defaultVal+"'>";

    comp.setAttribute("name",compName.replace("row", "namecombodetail"));
    comp.setAttribute("id",compName.replace("row", "idcombodetail"));
    comp.setAttribute("onChange","changeTextVal(this,'"+tableId+"');");
    comp.setAttribute("class","formSelect_1");
    Cell.appendChild(comp);

    //Cell.innerHTML="<select onchange=\"changeTextVal(this,'"+compName+"','7370');\" class=\"formTxtBox_1\" name=\"namecombodetail230\" id=\"idcombodetail\"><option value=\"\">--Select--</option><option value=\"100\">100</option><option value=\"200\">200</option><option value=\"300\">300</option></select>";
    ////alert(Cell.innerHTML);
    row.appendChild(Cell);

}

function addComboDataType(theform,compType,compName,row,tdId,cmbWoCalcText,cmbWoCalcValue,replacewith)
{ 
    var Cell;
    var comp ;
    Cell = document.createElement("TD");
    Cell.setAttribute("id",tdId);

    comp = document.createElement("INPUT");
    comp.setAttribute("type",compType);
    comp.setAttribute("style","display:none;");
    comp.setAttribute("name",compName);
    comp.setAttribute("id",compName);
    comp.setAttribute("value","0");

    Cell.appendChild(comp);

    var combo;
    combo = document.createElement("SELECT");
    var len = cmbWoCalcText.length;
    //var defaultVal = "";

    for(var i=0;i<len;i++)
    {
        combo.options[i] = new Option(cmbWoCalcText[i],cmbWoCalcValue[i]);
        //if(defaultVal==cmb.options[i].value){
        //    combo.options[i].selected= true;
        //    defaultVal = cmb.options[i].value;
        //}
    }
    combo.setAttribute("name",compName.replace("Cell", "selComboWithOutCalc"));
    combo.setAttribute("id",compName.replace("Cell", "selComboWithOutCalc"));
    combo.setAttribute("onChange","changeTextVal(this);");
    combo.setAttribute("class","formTxtBox_1");
    Cell.appendChild(combo);

    row.appendChild(Cell);
    return Cell;
}

/*
 * This method is used when an Input Element is supposed to be added in an Existing TD.
 */
function addInputWithEventInTd(theform,compType,compName,eventName,eventFunction,CellId) //Author : Ravi (13-5-06)
{

    var Cell;
    var comp ;
    Cell = document.getElementById(CellId);
    //	alert(Cell + ":" + CellId)
    //	Cell.setAttribute("id",tdId);
    if(compType == "HIDDEN" || compType == "CHECKBOX" || compType == "RADIO"){
        Cell.innerHTML+="<input type='"+compType+"' name='"+compName+"' id='"+compName+"' "+eventName+"='"+eventFunction+"' class='formTxtBox_1'>";
    }
    else
    {
        Cell.innerHTML+="<input type='"+compType+"' name='"+compName+"' id='"+compName+"' "+eventName+"='"+eventFunction+"' class='formTxtBox_1' style='width: 115px;' > ";
    }
//   row.appendChild(Cell);
}



function addInputWithEventInTdwithValue(theform,compType,compName,eventName,eventFunction,CellId,CValue,TIndex,Ronly) //Author : Ravi (13-5-06)
{
    var Cell;
    var comp ;
    Cell = document.getElementById(CellId);
    //	alert(Cell + ":" + CellId)
    //	Cell.setAttribute("id",tdId);
    if(compType == "HIDDEN" || compType == "CHECKBOX" || compType == "RADIO"){
        Cell.innerHTML+="<input type='"+compType+"' name='"+compName+"' id='"+compName+"' "+eventName+"='"+eventFunction+"' tabIndex = " + TIndex + " value='" + CValue + "'  " + Ronly + " funct = '"+ eventFunction +"' class='formTxtBox_1'>";
    }
    else
    {
        Cell.innerHTML+="<input type='"+compType+"' name='"+compName+"' id='"+compName+"' "+eventName+"='"+eventFunction+"' tabIndex = " + TIndex + " value='" + CValue + "'  " + Ronly + " funct = '"+ eventFunction +"' class='formTxtBox_1'  style='width: 115px;' >";
    }

//   row.appendChild(Cell);
}

function addLabel(inHTML,tdId)
{
    var Cell;
    var td = document.getElementById(tdId);
    //alert(td + " td " );
    Cell = document.createElement("label");
    Cell.setAttribute("id",tdId);
    Cell.innerHTML=inHTML;
    //alert(Cell.innerHTML);
    td.appendChild(Cell);
}

/* Added By Dohatec to  Encrypted data save  Prevent Start*/
function addHiddenInput(inputName ,inputValue ,row)
{    
    var lbl;
    var inHtml="";
    lbl = document.createElement("td");
    inHtml = "<input type='hidden' name='row_DataType"+inputName+"' id='row_DataType"+inputName+"' value='"+inputValue+"' />";
    lbl.innerHTML = inHtml;
    row.appendChild(lbl);
}
/* Added By Dohatec to  Encrypted data save  Prevent End*/