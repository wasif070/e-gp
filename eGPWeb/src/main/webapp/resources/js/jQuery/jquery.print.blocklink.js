/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    $(document).ready(function() {
        $("#print").click(function() {
            $("a[view='link']").each(function(){
                var temp = $(this).html().toString();
                if(temp=="(Sub Contractor/Consultant - View Details)"){
                    $(this).hide();
                    $(this).parent().append("<div class='reqF_1'>(Sub Contracting/Consultancy)</div>");
                }else if(temp=="(JVCA - View Details)"){
                    $(this).hide();
                    $(this).parent().append("<div class='reqF_1'>(JVCA)</div>");
                }else{
                    $(this).parent().append("<span class='disp_link'>"+temp+"</span>");
                    $(this).hide();
                    $('.reqF_1').remove();
                }                                      
            })
            $("th[view='print']").each(function(){
                    $(this).hide();
            })
            $("td[view='print']").each(function(){
                    $(this).hide();
            })

            printElem({ leaveOpen: true, printMode: 'popup' });
            $("a[view='link']").each(function(){
                var temp = $(this).html().toString();
                if(temp=="(Sub Contractor/Consultant - View Details)"){
                    $(this).show();
                }else if(temp=="(JVCA - View Details)"){
                    $(this).show();
                }else{
                    $('.disp_link').remove();
                    $('.reqF_1').remove();
                    $(this).show();
                }
            })
            $("th[view='print']").each(function(){
                    $(this).show();
            })
            $("td[view='print']").each(function(){
                    $(this).show();
            })
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }


