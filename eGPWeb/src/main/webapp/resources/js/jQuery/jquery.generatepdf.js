/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Author : TaherT
 */
function exportToPDF(cols){
    var columns = cols.toString().split(",");
    var columnNms = $("#list").jqGrid('getGridParam','colNames');
    var theads = "";
    for(var cc=0;cc<columnNms.length;cc++){
        var isAdd = true;
        for(var p=0;p<columns.length;p++){
            if(cc==columns[p]){
                isAdd = false;
                break;
            }
        }
        if(isAdd){
            theads = theads +"<th>"+columnNms[cc]+"</th>"
        }
    }
    theads = "<tr>"+theads+"</tr>";
    var mya=new Array();
    mya=jQuery("#list").getDataIDs();  // Get All IDs
    var data=jQuery("#list").getRowData(mya[0]);     // Get First row to get the labels
    //alert(data['id']);
    var colNames=new Array();
    var ii=0;
    for (var i in data){
        colNames[ii++]=i;
    }
    //alert(colNames[ii++]);}    // capture col names
    var header = "<div style = 'text-align: center'>Electronic Government Procurement (e-GP) System <br> Government Procurement and Property Management Division, Department of National Properties <br> Ministry of Finance, Royal Government of Bhutan<br></div><br/>";
     var pageHead = $(".pageHead_1").html();

    if(pageHead.indexOf("<span", 0)!=-1){
        pageHead = pageHead.substring(0, pageHead.indexOf("<span", 0));
    }
    else if(pageHead.indexOf("<SPAN", 0)!=-1){
        pageHead = pageHead.substring(0, pageHead.indexOf("<SPAN", 0));
    }
    
    //alert(pageHead);
    //pageHead = pageHead.
    var date = new Date(Date.now());
    var time = date.getDate() + "-" +date.getMonth()+"-"+date.getFullYear();
    var html=header+"<div><div class='pageHead_1' style='float:left'>"+pageHead+"</div><div style='float:right'>"+ time +"</div><div style='clear:both'></div></div><table border='0' class='tableList_1 t_space' cellspacing='10' cellpadding='0'>"+theads;
    //alert('len'+mya.length);
    for(i=0;i<mya.length;i++)
    {
        html=html+"<tr>";
        data=jQuery("#list").getRowData(mya[i]); // get each row
        for(j=0;j<colNames.length;j++){
            var isjAdd = true;
            for(var pj=0;pj<columns.length;pj++){
                if(j==columns[pj]){
                    isjAdd = false;
                    break;
                }
            }
            if(isjAdd){
                html=html+"<td><div>"+data[colNames[j]]+"</div></td>"; // output each column as tab delimited
            }
        }
        html=html+"</tr>";  // output each row with end of line

    }
    html=html+"</table>";  // end of line at the end
    //alert(html);
    document.formstyle.pdfBuffer.value=html;
    document.formstyle.method='POST';
    document.formstyle.action='/GenerateGridPDFs';  // send it to server which will open this contents in excel file
    document.formstyle.submit();
}

function ChangeSpecialCharacter(HTMLText) {
    var ChangedHTML = HTMLText.replace(/&gt;/g, '>');
    ChangedHTML = ChangedHTML.replace(/&lt;/g, '<');
    ChangedHTML = ChangedHTML.replace(/&quot;/g, '"');
    ChangedHTML = ChangedHTML.replace(/&apos;/g, "'");
    ChangedHTML = ChangedHTML.replace(/&amp;/g, '&');
    return ChangedHTML;
};

function exportDataToPDF(cols){
    var searchCriteria = $("#searchCriteria").html();
    if(searchCriteria == null)
        {
            searchCriteria = "";
        }
    var columns = cols.toString().split(",");
    var header = "<div style = 'text-align: center'>Electronic Government Procurement (e-GP) System <br> Government Procurement and Property Management Division, Department of National Properties <br> Ministry of Finance, Royal Government of Bhutan<br></div><br/>";
    var pageHead = $(".pageHead_1").html();
    if(pageHead.indexOf("<span", 0)!=-1){
        pageHead = pageHead.substring(0, pageHead.indexOf("<span", 0));
    }
    else if(pageHead.indexOf("<SPAN", 0)!=-1){
        pageHead = pageHead.substring(0, pageHead.indexOf("<SPAN", 0));
    }
    var tableHTML = header + "<div class='pageHead_1'>"+pageHead+"</div><div style='color:#6E6E6E;line-height:19px;font-size:14px;'>"+searchCriteria + "</div><table border='0' class='tableList_1 t_space' cellspacing='10' cellpadding='0'>";
    counter=0;
    $('#resultTable tr').each(function() {
        tableHTML = tableHTML+"<tr>";
        for(var j=0;j<this.cells.length;j++){
            var isAdd = true;
            for(var p=0;p<columns.length;p++){
                if(j==columns[p]){
                    isAdd = false;
                    break;
                }
            }
            if(isAdd){
                if(counter==0)
                {
                    tableHTML=tableHTML+"<th>"+this.cells[j].innerHTML+"</th>";
                }
                else
                {
                    tableHTML=tableHTML+"<td>"+this.cells[j].innerHTML+"</td>";
                }
                /*else{
                    if(this.cells[j].innerHTML.toString().indexOf('<a') == -1
                        && this.cells[j].innerHTML.toString().indexOf('<p') == -1
                        && this.cells[j].innerHTML.toString().indexOf('<A') == -1
                        && this.cells[j].innerHTML.toString().indexOf('<P') == -1
                        ){
                        tableHTML=tableHTML+"<td>"+this.cells[j].innerHTML+"</td>";
                    }else{
                        var ahref = this.cells[j].innerHTML;
                        var anchorContent = ahref.substr(ahref.indexOf('<a'),ahref.indexOf('</a>'));
			if(anchorContent == '') {
                        	anchorText = ahref.substr(ahref.indexOf('<A'),ahref.indexOf('</A>'));
                        }
			var anchorText = ahref.substr(ahref.indexOf('<a'),ahref.indexOf('>'+1)+1).replace("</a>","");
                        if(anchorText == '') {
                        	anchorText = ahref.substr(ahref.indexOf('<A'),ahref.indexOf('>'+1)+1).replace("</A>","");
                        }
                        if(ahref.indexOf('<p>') > 0 || ahref.indexOf('<P>') > 0){
                            ahref = ahref.replace("<p>","").replace("</p>","").replace("<p>","").replace("</P>","");
                        }
                        ahref = ahref.replace(anchorContent,anchorText);
                        //ahref = ahref.substring(0, ahref.indexOf(',', 0))+ahref.substring(ahref.indexOf("<p>", 0)+3, ahref.indexOf("</p>", 0));

                        tableHTML=tableHTML+"<td>"+ahref+"</td>";
                        //alert('tableHTML '+tableHTML);
                    }
                }*/
                
            }
            
        }
        counter=1;
        tableHTML = tableHTML+"</tr>";
        
    });
    tableHTML = tableHTML.replace("<tr></tr>", "");
    tableHTML=tableHTML+"</table>";

    //alert(tableHTML);
    document.formstyle.pdfBuffer.value = ChangeSpecialCharacter(tableHTML);
    document.formstyle.method='POST';
    document.formstyle.action='/GenerateGridPDFs';
    document.formstyle.submit();
}

