/*
 * jQuery validation plug-in 1.5.1
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2008 Jörn Zaefferer
 *
 * $Id: jquery.validate.js 6096 2009-01-12 14:12:04Z joern.zaefferer $
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function($) {

    $.extend($.fn, {
        // http://docs.jquery.com/Plugins/Validation/validate
        validate: function( options ) {
            // if nothing is selected, return nothing; can't chain anyway
            if (!this.length) {
                options && options.debug && window.console && console.warn( "nothing selected, can't validate, returning nothing" );
                return;
            }

            // check if a validator for this form was already created
            var validator = $.data(this[0], 'validator');
            if ( validator ) {
                return validator;
            }

            validator = new $.validator( options, this[0] );
            $.data(this[0], 'validator', validator);

            if ( validator.settings.onsubmit ) {
                // allow suppresing validation by adding a cancel class to the submit button
                this.find("input, button").filter(".cancel").click(function() {
                    validator.cancelSubmit = true;
                });

                // validate the form on submit
                this.submit( function( event ) {
                    //rajesh click
                    //this.submit.disabled=true;
                    
                    if ( validator.settings.debug )
                        // prevent form submit to be able to see console output
                        event.preventDefault();

                    function handle() {
                        if ( validator.settings.submitHandler ) {
                            validator.settings.submitHandler.call( validator, validator.currentForm );
                            return false;
                        }
                        return true;
                    }

                    // prevent submit for invalid forms or custom submit handlers
                    if ( validator.cancelSubmit ) {
                        validator.cancelSubmit = false;
                        return handle();
                    }
                    if ( validator.form() ) {
                        if ( validator.pendingRequest ) {
                            validator.formSubmitted = true;
                            return false;
                        }
                        return handle();
                    } else {
                        validator.focusInvalid();
                        return false;
                    }
                });
            }

            return validator;
        },
        // http://docs.jquery.com/Plugins/Validation/valid
        valid: function() {
            if ( $(this[0]).is('form')) {
                return this.validate().form();
            } else {
                var valid = false;
                var validator = $(this[0].form).validate();
                this.each(function() {
                    valid |= validator.element(this);
                });
                return valid;
            }
        },
        // attributes: space seperated list of attributes to retrieve and remove
        removeAttrs: function(attributes) {
            var result = {},
            $element = this;
            $.each(attributes.split(/\s/), function(index, value) {
                result[value] = $element.attr(value);
                $element.removeAttr(value);
            });
            return result;
        },
        // http://docs.jquery.com/Plugins/Validation/rules
        rules: function(command, argument) {
            var element = this[0];

            if (command) {
                var settings = $.data(element.form, 'validator').settings;
                var staticRules = settings.rules;
                var existingRules = $.validator.staticRules(element);
                switch(command) {
                    case "add":
                        $.extend(existingRules, $.validator.normalizeRule(argument));
                        staticRules[element.name] = existingRules;
                        if (argument.messages)
                            settings.messages[element.name] = $.extend( settings.messages[element.name], argument.messages );
                        break;
                    case "remove":
                        if (!argument) {
                            delete staticRules[element.name];
                            return existingRules;
                        }
                        var filtered = {};
                        $.each(argument.split(/\s/), function(index, method) {
                            filtered[method] = existingRules[method];
                            delete existingRules[method];
                        });
                        return filtered;
                }
            }

            var data = $.validator.normalizeRules(
                $.extend(
                {},
                    $.validator.metadataRules(element),
                    $.validator.classRules(element),
                    $.validator.attributeRules(element),
                    $.validator.staticRules(element)
                    ), element);

            // make sure required is at front
            if (data.required) {
                var param = data.required;
                delete data.required;
                data = $.extend({
                    required: param
                }, data);
            }

            return data;
        }
    });

    // Custom selectors
    $.extend($.expr[":"], {
        // http://docs.jquery.com/Plugins/Validation/blank
        blank: function(a) {
            return !$.trim(a.value);
        },
        // http://docs.jquery.com/Plugins/Validation/filled
        filled: function(a) {
            return !!$.trim(a.value);
        },
        // http://docs.jquery.com/Plugins/Validation/unchecked
        unchecked: function(a) {
            return !a.checked;
        }
    });


    $.format = function(source, params) {
        if ( arguments.length == 1 )
            return function() {
                var args = $.makeArray(arguments);
                args.unshift(source);
                return $.format.apply( this, args );
            };
        if ( arguments.length > 2 && params.constructor != Array  ) {
            params = $.makeArray(arguments).slice(1);
        }
        if ( params.constructor != Array ) {
            params = [ params ];
        }
        $.each(params, function(i, n) {
            source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
        });
        return source;
    };

    // constructor for validator
    $.validator = function( options, form ) {
        this.settings = $.extend( {}, $.validator.defaults, options );
        this.currentForm = form;
        this.init();
    };

    $.extend($.validator, {

        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: $( [] ),
            errorLabelContainer: $( [] ),
            onsubmit: true,
            ignore: [],
            ignoreTitle: false,
            onfocusin: function(element) {
                this.lastActive = element;

                // hide error label and remove error class on focus if enabled
                if ( this.settings.focusCleanup && !this.blockFocusCleanup ) {
                    this.settings.unhighlight && this.settings.unhighlight.call( this, element, this.settings.errorClass );
                    this.errorsFor(element).hide();
                }
            },
            onfocusout: function(element) {
                if ( !this.checkable(element) && (element.name in this.submitted || !this.optional(element)) ) {
                    this.element(element);
                }
            },
            onkeyup: function(element) {
                if ( element.name in this.submitted || element == this.lastElement ) {
            //rajesh
            //this.element(element);
            }
            },
            onclick: function(element) {
                if ( element.name in this.submitted )
                    this.element(element);
            },
            highlight: function( element, errorClass ) {
                $( element ).addClass( errorClass );
            },
            unhighlight: function( element, errorClass ) {
                $( element ).removeClass( errorClass );
            }
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/setDefaults
        setDefaults: function(settings) {
            $.extend( $.validator.defaults, settings );
        },

        messages: {
            required: "",
            requiredWithoutSpace: "Only Space is not allowed",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            namnivalue:"please enter name abhishek",
            url: "Please enter a valid URL.",
            wwwUrl: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            dateDE: "Bitte geben Sie ein gültiges Datum ein.",
            decimal:"Enter 2 digits after Decimal.",
            number: "Please enter a valid number.",
            numberWithHyphen: "Please enter a valid number.",
            numberZero:"Please enter valid number without zero.",
            alpha1:"Please enter only alphabets.",
            alphaWithSpecial:"Please enter valid characters",
            alphaWithSpecial1:"Please enter valid characters",
            alphaWithSpecial2:"Please enter valid characters",
            alphaWithSpecial3:"Please enter valid characters",
            FullNameAlpha:"Please enter valid characters",
            FirstLastName:"Please enter valid characters",
            phoneWithSpecial:"Please enter valid Phone number.",
            alphanumeric:"<br />Please enter only alphabet or numeric value.",
            DigAndNum:"<br/>Please enter alphanumeric values only.",
            DigAndNumThana:"<br/>Please enter alphanumeric values only.",
            nonumeric:"<br />Please enter only alphabet or special characters.",
            alNumRegex:"<br/>Please enter only alphabet and special characters.",
            alphaCompanyName:"<br/>Please enter combination of only alphabet and special characters and numbers.",
            alphaSpecialEditProj:"<br/>Please enter combination of only alphabet and special characters and numbers.",
            alphaNationalId:"<div class='reqF_1'>Only special characters not allowed</div>",
            alphaName:"<div class='reqF_1'>Only special characters not allowed.</div>",
            alphaCity:"<div class='reqF_1'>Only special characters not allowed.</div>",
            PhoneFax:"<br />Please enter valid Phone No.",
            year:"Please enter Year in this Format(2009-2010).",
            varifyage:"Persons above the age of 15 are allowed.",
            numberDE: "Bitte geben Sie eine Nummer ein.",
            digits: "Please enter only digits",
            threetenchk: "Minimum 3 and maximum 10 allowed",
            timeformat:"Please enter valid time",
            imagetype:"Please enter only jpe,jpeg or gif image.",
            creditcard: "Please enter a valid credit card number.",
            creditcard2:"Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            NotequalTo:"Please select another value",
            CompareText:"",
            cardexpirydt:"Please enter valid date",
            eventdate:"Select valid date",
            validenddate:"Please enter valid end date",
            CompareTo:"Wrong input value",
            CompareWithoutEqual:"Wrong input value",
            CompareToForLess:"Value should be less",
            CompareToForGreater:"Value should be Greater",
            CompareToForToday:"Value should be Greater than current date",
            filevalue:"Please enter the valid {0} file",
            accept: "Please enter a value with a valid extension.",
            validdate:"Invalid date, must be in MM-DD-YYYY format",
            validdatealbum:"Invalid date, must be in MM-DD-YYYY format",
            spacevalidate:"Spaces are not allowed.",
            maxlength: $.format("Please enter no more than {0} characters."),
            minlength: $.format("Please enter at least {0} characters."),
            rangelength: $.format("Please enter a value between {0} and {1} characters long."),
            range: $.format("Please enter a value between {0} and {1}."),
            max: $.format("Please enter a value less than or equal to {0}."),
            min: $.format("Please enter a value greater than or equal to {0}."),
            max4Digitsmust:"Must Enter 4 digits",
            CompareYear:"Must be grater then current year.",
            isrequired:"Please enter value"
        },

        autoCreateRanges: false,

        prototype: {

            init: function() {
                this.labelContainer = $(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
                this.containers = $(this.settings.errorContainer).add( this.settings.errorLabelContainer );
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();

                var groups = (this.groups = {});
                $.each(this.settings.groups, function(key, value) {
                    $.each(value.split(/\s/), function(index, name) {
                        groups[name] = key;
                    });
                });
                var rules = this.settings.rules;
                $.each(rules, function(key, value) {
                    rules[key] = $.validator.normalizeRule(value);
                });

                function delegate(event) {
                    var validator = $.data(this[0].form, "validator");
                    validator.settings["on" + event.type] && validator.settings["on" + event.type].call(validator, this[0] );
                }
                $(this.currentForm)
                .delegate("focusin focusout keyup", ":text, :password, :file, select, textarea", delegate)
                .delegate("click", ":radio, :checkbox", delegate);

                if (this.settings.invalidHandler)
                    $(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/form
            form: function() {
                this.checkForm();
                $.extend(this.submitted, this.errorMap);
                this.invalid = $.extend({}, this.errorMap);
                if (!this.valid())
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                this.showErrors();
                return this.valid();
            },

            checkForm: function() {
                this.prepareForm();
                for ( var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++ ) {
                    this.check( elements[i] );
                }
                return this.valid();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/element
            element: function( element ) {
                element = this.clean( element );
                this.lastElement = element;
                this.prepareElement( element );
                this.currentElements = $(element);
                var result = this.check( element );
                if ( result ) {
                    delete this.invalid[element.name];
                } else {
                    this.invalid[element.name] = true;
                }
                if(document.getElementById("hdnerrorcount")!=null)
                {
                    document.getElementById("hdnerrorcount").value=this.numberOfInvalids();
                }
                
                if ( !this.numberOfInvalids() ) {
                    // Hide error containers on last error
                    this.toHide = this.toHide.add( this.containers );
                }
                this.showErrors();
                return result;
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/showErrors
            showErrors: function(errors) {
                if(errors) {
                    // add items to error list and map
                    $.extend( this.errorMap, errors );
                    this.errorList = [];
                    for ( var name in errors ) {
                        this.errorList.push({
                            message: errors[name],
                            element: this.findByName(name)[0]
                        });
                    }
                    // remove items from success list
                    this.successList = $.grep( this.successList, function(element) {
                        return !(element.name in errors);
                    });
                }
                this.settings.showErrors
                ? this.settings.showErrors.call( this, this.errorMap, this.errorList )
                : this.defaultShowErrors();
            },

            // http://docs.jquery.com/Plugins/Validation/Validator/resetForm
            resetForm: function() {
                if ( $.fn.resetForm )
                    $( this.currentForm ).resetForm();
                this.submitted = {};
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass( this.settings.errorClass );
            },

            numberOfInvalids: function() {
                //code by rajesh
                 
                return this.objectLength(this.invalid);
            },

            objectLength: function( obj ) {
                var count = 0;
                for ( var i in obj )
                    count++;
                return count;
            },

            hideErrors: function() {
                this.addWrapper( this.toHide ).hide();
            },

            valid: function() {
                return this.size() == 0;
            },

            size: function() {
                
                return this.errorList.length;
            },

            focusInvalid: function() {
                if( this.settings.focusInvalid ) {
                    try {
                        $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus();
                    } catch(e) {
                    // ignore IE throwing errors when focusing hidden elements
                    }
                }
            },

            findLastActive: function() {
                var lastActive = this.lastActive;
                return lastActive && $.grep(this.errorList, function(n) {
                    return n.element.name == lastActive.name;
                }).length == 1 && lastActive;
            },

            elements: function() {
                var validator = this,
                rulesCache = {};

                // select all valid inputs inside the form (no submit or reset buttons)
                // workaround $Query([]).add until http://dev.jquery.com/ticket/2114 is solved
                return $([]).add(this.currentForm.elements)
                .filter(":input")
                .not(":submit, :reset, :image, [disabled]")
                .not( this.settings.ignore )
                .filter(function() {
                    !this.name && validator.settings.debug && window.console && console.error( "%o has no name assigned", this);

                    // select only the first element for each name, and only those with rules specified
                    if ( this.name in rulesCache || !validator.objectLength($(this).rules()) )
                        return false;

                    rulesCache[this.name] = true;
                    return true;
                });
            },

            clean: function( selector ) {
                return $( selector )[0];
            },

            errors: function() {
                return $( this.settings.errorElement + "." + this.settings.errorClass, this.errorContext );
            },

            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = $([]);
                this.toHide = $([]);
                this.formSubmitted = false;
                this.currentElements = $([]);
            },

            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add( this.containers );
            },

            prepareElement: function( element ) {
                this.reset();
                this.toHide = this.errorsFor(element);
            },

            check: function( element ) {
                element = this.clean( element );

                // if radio/checkbox, validate first element in group instead
                if (this.checkable(element)) {
                    element = this.findByName( element.name )[0];
                }

                var rules = $(element).rules();
                var dependencyMismatch = false;
                for( method in rules ) {
                    var rule = {
                        method: method,
                        parameters: rules[method]
                    };
                    try {
                        var result = $.validator.methods[method].call( this, element.value, element, rule.parameters );

                        // if a method indicates that the field is optional and therefore valid,
                        // don't mark it as valid when there are no other rules
                        if ( result == "dependency-mismatch" ) {
                            dependencyMismatch = true;
                            continue;
                        }
                        dependencyMismatch = false;

                        if ( result == "pending" ) {
                            this.toHide = this.toHide.not( this.errorsFor(element) );
                            return;
                        }

                        if( !result ) {
                            this.formatAndAdd( element, rule );
                            return false;
                        }
                    } catch(e) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + element.id
                            + ", check the '" + rule.method + "' method");
                        throw e;
                    }
                }
                if (dependencyMismatch)
                    return;
                if ( this.objectLength(rules) )
                    this.successList.push(element);
                return true;
            },

            // return the custom message for the given element and validation method
            // specified in the element's "messages" metadata
            customMetaMessage: function(element, method) {
                if (!$.metadata)
                    return;

                var meta = this.settings.meta
                ? $(element).metadata()[this.settings.meta]
                : $(element).metadata();

                return meta && meta.messages && meta.messages[method];
            },

            // return the custom message for the given element name and validation method
            customMessage: function( name, method ) {
                var m = this.settings.messages[name];
                return m && (m.constructor == String
                    ? m
                    : m[method]);
            },

            // return the first defined argument, allowing empty strings
            findDefined: function() {
                for(var i = 0; i < arguments.length; i++) {
                    if (arguments[i] !== undefined)
                        return arguments[i];
                }
                return undefined;
            },

            defaultMessage: function( element, method) {
                return this.findDefined(
                    this.customMessage( element.name, method ),
                    this.customMetaMessage( element, method ),
                    // title is never undefined, so handle empty string as undefined
                    !this.settings.ignoreTitle && element.title || undefined,
                    $.validator.messages[method],
                    "<strong>Warning: No message defined for " + element.name + "</strong>"
                    );
            },

            formatAndAdd: function( element, rule ) {
                var message = this.defaultMessage( element, rule.method );
                if ( typeof message == "function" )
                    message = message.call(this, rule.parameters, element);
                this.errorList.push({
                    message: message,
                    element: element
                });
                this.errorMap[element.name] = message;
                this.submitted[element.name] = message;
            },

            addWrapper: function(toToggle) {
                if ( this.settings.wrapper )
                    toToggle = toToggle.add( toToggle.parents( this.settings.wrapper ) );
                return toToggle;
            },

            defaultShowErrors: function() {
                for ( var i = 0; this.errorList[i]; i++ ) {
                    var error = this.errorList[i];
                    this.settings.highlight && this.settings.highlight.call( this, error.element, this.settings.errorClass );
                    this.showLabel( error.element, error.message );
                }
                if( this.errorList.length ) {
                    this.toShow = this.toShow.add( this.containers );
                }
                if (this.settings.success) {
                    for ( var i = 0; this.successList[i]; i++ ) {
                        this.showLabel( this.successList[i] );
                    }
                }
                if (this.settings.unhighlight) {
                    for ( var i = 0, elements = this.validElements(); elements[i]; i++ ) {
                        this.settings.unhighlight.call( this, elements[i], this.settings.errorClass );
                    }
                }
                this.toHide = this.toHide.not( this.toShow );
                this.hideErrors();
                this.addWrapper( this.toShow ).show();
            },

            validElements: function() {
                return this.currentElements.not(this.invalidElements());
            },

            invalidElements: function() {
                return $(this.errorList).map(function() {
                    return this.element;
                });
            },

            showLabel: function(element, message) {
                var label = this.errorsFor( element );
                if ( label.length ) {
                    // refresh error/success class
                    label.removeClass().addClass( this.settings.errorClass );

                    // check if we have a generated label, replace the message then
                    label.attr("generated") && label.html(message);
                } else {

                    //    alert($(this.idOrName(element)).next().attr('id'));

                    // create label
                    label = $("<" + this.settings.errorElement + "/>")
                    .attr({
                        "for":  this.idOrName(element),
                        generated: true
                    })
                    .addClass(this.settings.errorClass)
                    .html(message || "");
                    if ( this.settings.wrapper ) {
                        // make sure the element is visible, even in IE
                        // actually showing the wrapped element is handled elsewhere
                        label = label.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    }
                    if ( !this.labelContainer.append(label).length )
                        this.settings.errorPlacement
                        ? this.settings.errorPlacement(label, $(element) )
                        : label.insertAfter(element);
                }
                if ( !message && this.settings.success ) {
                    label.text("");
                    typeof this.settings.success == "string"
                    ? label.addClass( this.settings.success )
                    : this.settings.success( label );
                }
                this.toShow = this.toShow.add(label);
            },

            errorsFor: function(element) {
                return this.errors().filter("[for='" + this.idOrName(element) + "']");
            },

            idOrName: function(element) {
                return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
            },

            checkable: function( element ) {
                return /radio|checkbox/i.test(element.type);
            },

            findByName: function( name ) {
                // select by name and filter by form for performance over form.find("[name=...]")
                var form = this.currentForm;
                return $(document.getElementsByName(name)).map(function(index, element) {
                    return element.form == form && element.name == name && element  || null;
                });
            },

            getLength: function(value, element) {
                switch( element.nodeName.toLowerCase() ) {
                    case 'select':
                        return $("option:selected", element).length;
                    case 'input':
                        if( this.checkable( element) )
                            return this.findByName(element.name).filter(':checked').length;
                }
                return value.length;
            },

            depend: function(param, element) {
                return this.dependTypes[typeof param]
                ? this.dependTypes[typeof param](param, element)
                : true;
            },

            dependTypes: {
                "boolean": function(param, element) {
                    return param;
                },
                "string": function(param, element) {
                    return !!$(param, element.form).length;
                },
                "function": function(param, element) {
                    return param(element);
                }
            },

            optional: function(element) {
                return !$.validator.methods.required.call(this, $.trim(element.value), element) && "dependency-mismatch";
            },

            startRequest: function(element) {
                if (!this.pending[element.name]) {
                    this.pendingRequest++;
                    this.pending[element.name] = true;
                }
            },

            stopRequest: function(element, valid) {
                this.pendingRequest--;
                // sometimes synchronization fails, make sure pendingRequest is never < 0
                if (this.pendingRequest < 0)
                    this.pendingRequest = 0;
                delete this.pending[element.name];
                if ( valid && this.pendingRequest == 0 && this.formSubmitted && this.form() ) {
                    $(this.currentForm).submit();
                } else if (!valid && this.pendingRequest == 0 && this.formSubmitted) {
                    $(this.currentForm).triggerHandler("invalid-form", [this]);
                }
            },

            previousValue: function(element) {
                return $.data(element, "previousValue") || $.data(element, "previousValue", previous = {
                    old: null,
                    valid: true,
                    message: this.defaultMessage( element, "remote" )
                });
            }

        },

        classRuleSettings: {
            required: {
                required: true
            },
            requiredWithoutSpace: {
                requiredWithoutSpace: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            wwwUrl: {
                wwwUrl: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            dateDE: {
                dateDE: true
            },
            number: {
                number: true
            },
            numberWithHyphen: {
                numberWithHyphen: true
            },
            numberZero:{
                numberZero:true
            },
            alpha1:{
                alpha1: true
            },
            alphaWithSpecial:{
                alphaWithSpecial:true
            },
            alphaWithSpecial1:{
                alphaWithSpecial1:true
            },
            alphaWithSpecial2:{
                alphaWithSpecial2:true
            },
            alphaWithSpecial3:{
                alphaWithSpecial3:true
            },
            FullNameAlpha:{
                FullNameAlpha:true
            },
            FirstLastName:{
                FirstLastName:true
            },
            phoneWithSpecial:{
                phoneWithSpecial:true
            },
            alphanumeric:{
                alphanumeric: true
            },
            nonumeric:{
                nonumeric: true
            },
            alNumRegex:{
                alNumRegex: true
            },
            alphaCompanyName:{
                alphaCompanyName: true
            },
            alphaSpecialEditProj:{
                alphaSpecialEditProj: true
            },
            alphaNationalId:{
                alphaNationalId: true
            },
            alphaName:{
                alphaName: true
            },
            alphaCity:{
                alphaCity: true
            },
            DigAndNum:{
                DigAndNum:true
            },
            DigAndNumThana:{
                DigAndNumThana:true
            },
            varifyage:{
                varifyage: true
            },
            numberDE: {
                numberDE: true
            },
            PhoneFax:{
                PhoneFax:true
            },
            digits: {
                digits: true
            },
            threetenchk: {
                threetenchk: true
            },
            year: {
                year: true
            },
            timeformat:{
                timeformat:true
            },
            imagetype:{
                imagetype:true
            },
            creditcard: {
                creditcard: true
            },
            //added by abhishek
            namnivalue:{
                namnivalue:true
            },
            cardexpirydt:{
                cardexpirydt:true
            },
            eventdate:{
                eventdate:true
            },
            validenddate:{
                validenddate:true
            },
            creditcard2: {
                creditcard2: true
            },
            isrequired:true,
            validdate:{
                validdate:true
            },
            validdatealbum:{
                validdatealbum:true
            },
            max4Digitsmust:{
                max4Digitsmust:true
            },
            CompareYear:{
                CompareYear:true
            },
            spacevalidate:{
                spacevalidate:true
            }

        },

        addClassRules: function(className, rules) {
            className.constructor == String ?
            this.classRuleSettings[className] = rules :
            $.extend(this.classRuleSettings, className);
        },

        classRules: function(element) {
            var rules = {};
            var classes = $(element).attr('class');
            classes && $.each(classes.split(' '), function() {
                if (this in $.validator.classRuleSettings) {
                    $.extend(rules, $.validator.classRuleSettings[this]);
                }
            });
            return rules;
        },

        attributeRules: function(element) {
            var rules = {};
            var $element = $(element);

            for (method in $.validator.methods) {
                var value = $element.attr(method);
                if (value) {
                    rules[method] = value;
                }
            }

            // maxlength may be returned as -1, 2147483647 (IE) and 524288 (safari) for text inputs
            if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
                delete rules.maxlength;
            }

            return rules;
        },

        metadataRules: function(element) {
            if (!$.metadata) return {};

            var meta = $.data(element.form, 'validator').settings.meta;
            return meta ?
            $(element).metadata()[meta] :
            $(element).metadata();
        },

        staticRules: function(element) {
            var rules = {};
            var validator = $.data(element.form, 'validator');
            if (validator.settings.rules) {
                rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
            }
            return rules;
        },

        normalizeRules: function(rules, element) {
            // handle dependency check
            $.each(rules, function(prop, val) {
                // ignore rule when param is explicitly false, eg. required:false
                if (val === false) {
                    delete rules[prop];
                    return;
                }
                if (val.param || val.depends) {
                    var keepRule = true;
                    switch (typeof val.depends) {
                        case "string":
                            keepRule = !!$(val.depends, element.form).length;
                            break;
                        case "function":
                            keepRule = val.depends.call(element, element);
                            break;
                    }
                    if (keepRule) {
                        rules[prop] = val.param !== undefined ? val.param : true;
                    } else {
                        delete rules[prop];
                    }
                }
            });

            // evaluate parameters
            $.each(rules, function(rule, parameter) {
                rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
            });

            // clean number parameters
            $.each(['minlength', 'maxlength', 'min', 'max'], function() {
                if (rules[this]) {
                    rules[this] = Number(rules[this]);
                }
            });
            $.each(['rangelength', 'range'], function() {
                if (rules[this]) {
                    rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
                }
            });

            if ($.validator.autoCreateRanges) {
                // auto-create ranges
                if (rules.min && rules.max) {
                    rules.range = [rules.min, rules.max];
                    delete rules.min;
                    delete rules.max;
                }
                if (rules.minlength && rules.maxlength) {
                    rules.rangelength = [rules.minlength, rules.maxlength];
                    delete rules.minlength;
                    delete rules.maxlength;
                }
            }

            // To support custom messages in metadata ignore rule methods titled "messages"
            if (rules.messages) {
                delete rules.messages
            }

            return rules;
        },

        // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
        normalizeRule: function(data) {
            if( typeof data == "string" ) {
                var transformed = {};
                $.each(data.split(/\s/), function() {
                    transformed[this] = true;
                });
                data = transformed;
            }
            return data;
        },

        // http://docs.jquery.com/Plugins/Validation/Validator/addMethod
        addMethod: function(name, method, message) {
            $.validator.methods[name] = method;
            $.validator.messages[name] = message;
            if (method.length < 3) {
                $.validator.addClassRules(name, $.validator.normalizeRule(name));
            }
        },

        methods: {

            // http://docs.jquery.com/Plugins/Validation/Methods/required
            required: function(value, element, param) {
                // check if dependency is met
                if ( !this.depend(param, element) )
                    return "dependency-mismatch";
                switch( element.nodeName.toLowerCase() ) {
                    case 'select':
                        var options = $("option:selected", element);
                        return options.length > 0 && ( element.type == "select-multiple" || ($.browser.msie && !(options[0].attributes['value'].specified) ? options[0].text : options[0].value).length > 0);
                    case 'input':
                        if ( this.checkable(element) )
                            return this.getLength(value, element) > 0;
                    default:
                        return $.trim(value).length > 0;
                }
            },
            requiredWithoutSpace: function(value, element, param) {
                // check if dependency is met number: function(value, element) {
                if ( !this.depend(param, element) )
                    return "dependency-mismatch";
                switch( element.nodeName.toLowerCase() ) {
                    case 'select':
                        var options = $("option:selected", element);
                        return options.length > 0 && ( element.type == "select-multiple" || ($.browser.msie && !(options[0].attributes['value'].specified) ? options[0].text : options[0].value).length > 0);
                    case 'input':
                        if ( this.checkable(element) )
                            return this.getLength(value, element) > 0;
                    default:
                        return value.length > 0;
                }
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/remote
            remote: function(value, element, param) {
                if ( this.optional(element) )
                    return "dependency-mismatch";

                var previous = this.previousValue(element);

                if (!this.settings.messages[element.name] )
                    this.settings.messages[element.name] = {};
                this.settings.messages[element.name].remote = typeof previous.message == "function" ? previous.message(value) : previous.message;

                param = typeof param == "string" && {
                    url:param
                } || param;

                if ( previous.old !== value ) {
                    previous.old = value;
                    var validator = this;
                    this.startRequest(element);
                    var data = {};
                    data[element.name] = value;
                    $.ajax($.extend(true, {
                        url: param,
                        mode: "abort",
                        port: "validate" + element.name,
                        dataType: "json",
                        data: data,
                        success: function(response) {
                            if ( response ) {
                                var submitted = validator.formSubmitted;
                                validator.prepareElement(element);
                                validator.formSubmitted = submitted;
                                validator.successList.push(element);
                                validator.showErrors();
                            } else {
                                var errors = {};
                                errors[element.name] =  response || validator.defaultMessage( element, "remote" );
                                validator.showErrors(errors);
                            }
                            previous.valid = response;
                            validator.stopRequest(element, response);
                        }
                    }, param));
                    return "pending";
                } else if( this.pending[element.name] ) {
                    return "pending";
                }
                return previous.valid;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/minlength
            minlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) >= param;
            },

            spacevalidate: function(value, element) {
                //return this.optional(element) || value.indexOf(" ") < 0 && value != "";
                if(/^\s+|\s+$/g.test(value)){
                    return false;
                }else{
                    return true;
                }
            },
            // http://docs.jquery.com/Plugins/Validation/Methods/minlength
            
            // http://docs.jquery.com/Plugins/Validation/Methods/maxlength
            maxlength: function(value, element, param) {
                return this.optional(element) || this.getLength($.trim(value), element) <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/rangelength
            rangelength: function(value, element, param) {
                var length = this.getLength($.trim(value), element);
                return this.optional(element) || ( length >= param[0] && length <= param[1] );
            },
            
            // http://docs.jquery.com/Plugins/Validation/Methods/min
            min: function( value, element, param ) {
                return this.optional(element) || value >= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/max
            max: function( value, element, param ) {
                return this.optional(element) || value <= param;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/range
            range: function( value, element, param ) {
                return this.optional(element) || ( value >= param[0] && value <= param[1] );
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/email
            email: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                return this.optional(element) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
            //return this.optional(element) || /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(value);
            //return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/url
            url: function(value, element) {
                // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
                //return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
                //return this.optional(element) || /^(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
                return this.optional(element) || /^([a-zA-Z0-9_\-]+(?:\.[a-zA-Z0-9_\-]+)(?:\.[a-zA-Z0-9_\-]+)*\.[a-zA-Z]{2,4}(?:\/[a-zA-Z0-9_]+)*(?:\/[a-zA-Z0-9_]+\.[a-zA-Z]{2,4}(?:\?[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)?)?(?:\&[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)*)$/.test(value);
            },
            //FOLLOWING VALIDATION IS USED WHEN URL NEEDS TO BE CHECKED WITHOUT ANY SPECIAL CHARACTERS IN STARING LIKE "-"
            // DONE BY HARMONY & RISHITA.
            wwwUrl: function(value, element) {
                var chk = value.charAt(0)=="-";
                if(chk){
                    return false;
                }else{
                    return this.optional(element) || /^([a-zA-Z0-9_\-]+(?:\.[a-zA-Z0-9_\-]+)(?:\.[a-zA-Z0-9_\-]+)*\.[a-zA-Z]{2,4}(?:\/[a-zA-Z0-9_]+)*(?:\/[a-zA-Z0-9_]+\.[a-zA-Z]{2,4}(?:\?[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)?)?(?:\&[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)*)$/.test(value);
                }
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/date
            date: function(value, element) {
                return this.optional(element) || !/Invalid|NaN/.test(new Date(value));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/dateISO
            dateISO: function(value, element) {
                return this.optional(element) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/dateDE
            dateDE: function(value, element) {
                return this.optional(element) || /^\d\d?\.\d\d?\.\d\d\d?\d?$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/number
            number: function(value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            },
           
            numberZero:function(value,element){
                var flag ;
                //alert(value.indexOf("."));
                if(isNaN(value)==false)
                {
                    if(value.indexOf(".")==-1)
                    {
                        if(value>0)
                        {
                            flag=true;
                        }
                        else
                        {
                            flag=false;
                        }
                    }
                    else
                    {
                        flag=false;
                    }
                }
                else
                {
                    flag=false;
                }
                if(flag==false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            },
            decimal: function(value, element) {
                //return this.optional(element) || /^(\d+(\.\d{1,2})?)$/.test(value);
                //as this allowed 1 as well as 2 digits after decimal.
                return this.optional(element) || /^(\d+(\.\d{2})?)$/.test(value);
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },
            decimalWith2: function(value, element) {
                //return this.optional(element) || /^(\d+(\.\d{1,2})?)$/.test(value);
                //as this allowed 1 as well as 2 digits after decimal.
                return this.optional(element) || /^(\d+(\.\d{1,2})?)$/.test(value);
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },

            max4Digitsmust: function(value, element) {
                var data=false;
                if(parseInt(value)!=0){
                    data = this.optional(element) || /^(\d{4})?$/.test(value);
                }
                return data;
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },
            //For comparing input Year with current Year - by Harmony & Taher
            //used in CompanyDetails.jsp
            CompareYear: function(value, element,params) {
                var data=false;                
                if(parseInt(value)<=parseInt($(params).val())){
                    data = true;
                }
                return data;
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },
            year: function(value, element) {
                return this.optional(element) || /^(\d{4}\-\d{4})?$/.test(value);
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },
            PhoneFax: function(value, element) {
                return this.optional(element)
                || /^\d{2,5}-\d{2,5}-\d{3,10}$/.test(value)
                || /^\d{2,5}-\d{3,10}$/.test(value);

            //return this.optional(element) || /^[0-9]{2,}[\- ]?[0-9]{2,}[\- ]?[0-9]{2,}$/.test(value);
            //return this.optional(element) || /^(\d+(\-\d+)?)$/.test(value);
            //return this.optional(element) || /^([0-9]+)|([0-9]+\.\d{2})$/.test(value);
            },
            DigAndNum: function(value, element) {
                //return this.optional(element) || /^([\a-zA-Z]+([\a-zA-Z]+\d+)?)+$/.test(value) || /^((\d+[\a-zA-Z]+)([\a-zA-Z]+)?)+$/.test(value);
                
                // original
                //return this.optional(element) || /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-.&,\(\)\']+)?)+$/.test(value);

                //old, was not allowing "hsgdfhs,.-" ,multiple occurence of special characters
                 //return this.optional(element) || /^([\a-zA-Z]?([\a-zA-Z\s.]+[\s-.&,\(\)\']?))+$/.test(value);
                
                  return this.optional(element) || /^([\a-zA-Z]?([\a-zA-Z\s.]+[\s-.&,\(\)']+)?)+$/.test(value);

            },
            DigAndNumThana: function(value, element) {
                  return this.optional(element) || /^([\a-zA-Z0-9]+([\a-zA-Z]+[\s-.&,\(\)\']+)?)+$/.test(value);

            },
            numberWithHyphen: function(value, element) {
                //return this.optional(element) || /^([0-9]\-\+\/)$/.test(value);
                   //return this.optional(element) || /^(\d\-\+\/)+$/.test(value);
                   return this.optional(element) || /^[0-9\-]+$/.test(value);
            },
            alpha1: function(value, element) {
                // return this.optional(element) || /^[a-z A-Z]+$/.test(value);
                return this.optional(element) || /^[\sa-zA-Z\'\-]+$/.test(value);

            },
            alphaWithSpecial:function(value,element){
                //return this.optional(element) || /^[\sa-zA-Z\'\-][\.\,\&\'\-]+$/.test(value);
                return this.optional(element) || /^[\sa-zA-Z!,&.\'\-]+$/.test(value);
            //return this.optional(element) || /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/.test(value);
            },
            alphaWithSpecial1:function(value,element){
                //return this.optional(element) || /^[\sa-zA-Z\'\-][\.\,\&\'\-]+$/.test(value);
                return this.optional(element) || /^[\sa-zA-Z-&/]+$/.test(value);
            //return this.optional(element) || /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/.test(value);
            },
            alphaWithSpecial2:function(value,element){
                //return this.optional(element) || /^[\sa-zA-Z\'\-][\.\,\&\'\-]+$/.test(value);
                return this.optional(element) || /^[\sa-zA-Z-.\-\(\)&/]+$/.test(value);
            //return this.optional(element) || /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/.test(value);
            },
            alphaWithSpecial3:function(value,element){
                //return this.optional(element) || /^[\sa-zA-Z\'\-][\.\,\&\'\-]+$/.test(value);
                return this.optional(element) || /^[\sa-zA-Z./]+$/.test(value);
            //return this.optional(element) || /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/.test(value);
            },
            FullNameAlpha:function(value,element)
            {
                return this.optional(element) || /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-/._,\(\)]+)?)+$/.test(value);
            },
            FirstLastName:function(value,element)
            {
                return this.optional(element) || /^([\a-zA-Z]+([\a-zA-Z\s.]+)?)+$/.test(value);
            },

            phoneWithSpecial:function(value,element){
                
                
                //return this.optional(element) || /^[\d!-+/]+$/.test(value);

                //alert(value);
                return this.optional(element) || /^(\d\-\+\/)+$/.test(value);
                return false;

            //return this.optional(element) || /^[\sa-zA-Z!,&.\'\-]+$/.test(value);
            },
            // alphanumeric: function(value, element) {
            //   return this.optional(element) || /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value);
            //},
            alphanumeric: function(value, element) {
                return this.optional(element) || /^[\sa-zA-Z0-9\'\-]+$/.test(value);
            // return this.optional(element) || /^[\sa-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);

            },
            nonumeric: function(value, element) {
                return this.optional(element) || /[^0-9]/.test(value);
            },
            alNumRegex: function(value, element) {
                //return this.optional(element) || /[^0-9]/.test(value);
                return this.optional(element) || /^[\sa-zA-Z-.\-\(\)&/]+$/.test(value);
            },
            //alphanumericPass:function(value, element) {
            //use this when u dont want to allow only alphabets, or only numerics.,both must be together.
            //return this.optional(element) || /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value);
            //},

            //
            alphaCompanyName: function(value, element) {
                //allows alphabets
                //alphabets+numeric
                //alphabets+few special
                // (&/-.)
                return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\.\-\(\)/]+$/.test(value);
                

            },
            alphaSpecialEditProj: function(value, element) {
                //allows alphabets
                //alphabets+numeric
                //alphabets+few special
                // (&/-.)
               return this.optional(element) || /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*])+$)/;


            },
            alphaNationalId: function(value, element) {
                //allows alphabets
                //alphabets+numeric
                //alphabets+few special
                // following was not allowing only Digits
                //return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);
                //Final
                return this.optional(element) || /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\!\(\)\-\.\&\*\#\@/\\(/)/)])+$)/.test(value);
                //Latest FINAL
                //allows even single occurence of DIGITS AND CHARACTERS
                //return this.optional(element) || /^([a-zA-Z 0-9]([a-zA-Z 0-9\!\@\#\$\/\%\^\&\*\(\)\{\}\[\]\'\-])?)+$/.test(value);
                //changed final
//final
                //return this.optional(element) || /^(?:[a-z\&\*\(\)\'\-)])|(?:^[^\&\*\(\)\-])+$/.test(value);

                //return this.optional(element) || /^[/\d{1,}/]+[/\d{1,}/]+[a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);

                //return this.optional(element) || /^\(d{1,})([a-zA-Z]{1,})$/.test(value);

            //return this.optional(element) || /^(\w+(\.\w{1})|([a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-])+)$/.test(value);

            //return this.optional(element) || /^(\d+(\.\d{1,2})?)$/.test(value);

            //return this.optional(element) || /^(\w{1,})([a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-])+$/.test(value);


            },
            alphaName: function(value, element) {
                //used for Name(Ministry / Division / Organization / Office Name.)
                //allows alphabets
                //alphabets+numeric
                //alphabets+few special
                // following was not allowing only Digits
                //& , ‘ “ ( ) } { - . _ should be allowed for name
                //
                //OLD,WAS NOT ALLOWING SINGLE OCCURENCE OF CHARACTERS.
                //return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);
                //
                //LATEST TEST,()REFERENCED from National Id regexp
                // - 09-06-2011 - return this.optional(element) || /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/.test(value);
                // - 16-06-2011 - return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);
                return this.optional(element) || /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/.test(value);
                //return this.optional(element) || /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-])?)+$/.test(value);
                //
                //NEW, ALLOWS SINGLE OCCURENFCE OF CHARACTERS,NOT FOR DIGITS(ALL OTHER CONDITIONS SAME AS BEFORE)
                //return /^([\a-zA-Z]+([\a-zA-Z\s.]+[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+[\s-.&,\(\)']+)?)+$/.test(value);
                
                //following dont allow single occurence of characters and digits
                //return this.optional(element) || /^[a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);
                //allows even single occurence of DIGITS AND CHARACTERS
                //return this.optional(element) || /^([a-zA-Z 0-9]([a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-])?)+$/.test(value);
                //return this.optional(element) || /^[/\d{1,}/]+[/\d{1,}/]+[a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);
            },
            alphaCity: function(value, element) {
                //allows alphabets
                //alphabets+numeric
                //alphabets+few special
                // following was not allowing only Digits
                //o	& , ‘ “ ( ) } { - . _should be allowed for name
                return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);
                //following dont allow single occurence of characters and digits
                //return this.optional(element) || /^[a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);
                //Latest FINAL
                //allows even single occurence of DIGITS AND CHARACTERS
                //return this.optional(element) || /^([a-zA-Z 0-9]([a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-])?)+$/.test(value);
                //return this.optional(element) || /^[/\d{1,}/]+[/\d{1,}/]+[a-zA-Z 0-9][a-zA-Z 0-9\!\@\#\$\%\^\&\*\(\)\{\}\[\]\'\-]+$/.test(value);
            },
            alphaForPassword: function(value, element) {
                //return this.optional(element) || /^[\sa-zA-Z0-9\@\#\$\%\*\_\.\'\-]+$/.test(value);
                //return this.optional(element) || /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value); - to keep this
                return this.optional(element) || /^(?![a-zA-Z\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)(?![0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)([a-zA-Z 0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)/.test(value);
            // Regex for Password with Special caracters ---- return this.optional(element) || /^(?![a-zA-Z]+$)(?![0-9]+$)(?![a-zA-Z 0-9]+$)[a-zA-Z 0-9\@\#\$\%\&\*\(\)\-\_\.]+$/.test(value);

            },            
            validdate:function(value,element)
            {
                if(value.length == 10)
                {
                    var newdate= new Date(value);

                    if(newdate != "Invalid date")
                    {
                        var year=parseInt(value.split('/')[2]);
                        var month=(value.split('/')[0]);

                        var day=(value.split('/')[1]);

                        var date =  new Date(year, (month-1), day);

                        var DateYear = date.getFullYear();
                        var DateMonth = date.getMonth();
                        var DateDay = date.getDate();
                        if (DateYear == year && DateMonth == (month-1) && DateDay == day)
                        {
                            return true;
                        }
                        else
                            return false;
                    }

                }
                else
                {
                    return false;
                }

            },


            validdatealbum:function(value,element)
            {
                if(value.length == 10)
                {
                    var newdate= new Date(value);

                    if(newdate != "Invalid date")
                    {
                        var year=parseInt(value.split('/')[2]);
                        var month=(value.split('/')[0]);

                        var day=(value.split('/')[1]);

                        var date =  new Date(year, (month-1), day);

                        var DateYear = date.getFullYear();
                        var DateMonth = date.getMonth();
                        var DateDay = date.getDate();
                        if (DateYear == year && DateMonth == (month-1) && DateDay == day)
                        {
                            return true;
                        }
                        else
                            return false;
                    }

                }
                else if(value.length==0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            },
            varifyage: function(value, element) {
                var age=13;
                var mydate = new Date(value);
                var currdate = new Date();
                currdate.setFullYear(currdate.getFullYear() - age);
                if ((currdate - mydate) < 0){
                    return false;
                }

                return true;
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/numberDE
            numberDE: function(value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/digits
            digits: function(value, element) {
                return this.optional(element) || /^\d+$/.test(value);
            },
            threetenchk: function(value, element) {
                var vbool = true;                
                if($(element).val()<3 || $(element).val()>10){
                    vbool = false;
                }
                return vbool;
            },
            //function to check if time eneterd is valid or not
            timeformat:function(value,element){
                return this.optional(element)||/^([0-1][0-9]|2[0-3]):[0-5][0-9]$/.test(value);

            },
            //Function to check valid image extension
            imagetype:function(value,element)
            {
                return this.optional(element)||/^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(j|J)(p|P)(g|G))$/.test(value);

            },


            // http://docs.jquery.com/Plugins/Validation/Methods/creditcard
            // based on http://en.wikipedia.org/wiki/Luhn
            creditcard: function(value, element) {
                if ( this.optional(element) )
                    return "dependency-mismatch";
                // accept only digits and dashes
                if (/[^0-9-]+/.test(value))
                    return false;
                var nCheck = 0,
                nDigit = 0,
                bEven = false;

                value = value.replace(/\D/g, "");

                for (n = value.length - 1; n >= 0; n--) {
                    var cDigit = value.charAt(n);
                    var nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9)
                            nDigit -= 9;
                    }
                    nCheck += nDigit;
                    bEven = !bEven;
                }

                return (nCheck % 10) == 0;
            },



            creditcard2 :function(value, element, param) {
                var cardName = $(param).val();

                var cards = new Array();
                cards [0] = {
                    cardName: "Visa",
                    lengths: "13,16",
                    prefixes: "4",
                    checkdigit: true
                };
                cards [1] = {
                    cardName: "MasterCard",
                    lengths: "16",
                    prefixes: "51,52,53,54,55",
                    checkdigit: true
                };
                cards [2] = {
                    cardName: "DinersClub",
                    lengths: "14,16",
                    prefixes: "300,301,302,303,304,305,36,38,55",
                    checkdigit: true
                };
                cards [3] = {
                    cardName: "CarteBlanche",
                    lengths: "14",
                    prefixes: "300,301,302,303,304,305,36,38",
                    checkdigit: true
                };
                cards [4] = {
                    cardName: "AmEx",
                    lengths: "15",
                    prefixes: "34,37",
                    checkdigit: true
                };
                cards [5] = {
                    cardName: "Discover",
                    lengths: "16",
                    prefixes: "6011,650",
                    checkdigit: true
                };
                cards [6] = {
                    cardName: "JCB",
                    lengths: "15,16",
                    prefixes: "3,1800,2131",
                    checkdigit: true
                };
                cards [7] = {
                    cardName: "enRoute",
                    lengths: "15",
                    prefixes: "2014,2149",
                    checkdigit: true
                };
                cards [8] = {
                    cardName: "Solo",
                    lengths: "16,18,19",
                    prefixes: "6334, 6767",
                    checkdigit: true
                };
                cards [9] = {
                    cardName: "Switch",
                    lengths: "16,18,19",
                    prefixes: "4903,4905,4911,4936,564182,633110,6333,6759",
                    checkdigit: true
                };
                cards [10] = {
                    cardName: "Maestro",
                    lengths: "16,18",
                    prefixes: "5020,6",
                    checkdigit: true
                };
                cards [11] = {
                    cardName: "VisaElectron",
                    lengths: "16",
                    prefixes: "417500,4917,4913",
                    checkdigit: true
                };

                var cardType = -1;
                for (var i=0; i<cards.length; i++) {
                    if (cardName.toLowerCase() == cards[i].cardName.toLowerCase()) {
                        cardType = i;
                        break;
                    }
                }
                if (cardType == -1) {
                    return false;
                } // card type not found

                value = value.replace (/[\s-]/g, ""); // remove spaces and dashes
                if (value.length == 0) {
                    return false;
                } // no length

                var cardNo = value;
                var cardexp = /^[0-9]{13,19}$/;
                if (!cardexp.exec(cardNo)) {
                    return false;
                } // has chars or wrong length

                cardNo = cardNo.replace(/\D/g, ""); // strip down to digits

                if (cards[cardType].checkdigit){
                    var checksum = 0;
                    var mychar = "";
                    var j = 1;

                    var calc;
                    for (i = cardNo.length - 1; i >= 0; i--) {
                        calc = Number(cardNo.charAt(i)) * j;
                        if (calc > 9) {
                            checksum = checksum + 1;
                            calc = calc - 10;
                        }
                        checksum = checksum + calc;
                        if (j ==1) {
                            j = 2
                        } else {
                            j = 1
                        };
                    }

                    if (checksum % 10 != 0) {
                        return false;
                    } // not mod10
                }

                var lengthValid = false;
                var prefixValid = false;
                var prefix = new Array ();
                var lengths = new Array ();

                prefix = cards[cardType].prefixes.split(",");
                for (i=0; i<prefix.length; i++) {
                    var exp = new RegExp ("^" + prefix[i]);
                    if (exp.test (cardNo)) prefixValid = true;
                }
                if (!prefixValid) {
                    return false;
                } // invalid prefix

                lengths = cards[cardType].lengths.split(",");
                for (j=0; j<lengths.length; j++) {
                    if (cardNo.length == lengths[j]) lengthValid = true;
                }
                if (!lengthValid) {
                    return false;
                } // wrong length

                return true;
            },


            // http://docs.jquery.com/Plugins/Validation/Methods/accept
            accept: function(value, element, param) {
                param = typeof param == "string" ? param : "png|jpe?g|gif";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            },

            // http://docs.jquery.com/Plugins/Validation/Methods/equalTo
            equalTo: function(value, element, param) {
                return value == $(param).val();
            },
            NotequalTo: function(value, element, param) {
                return value != $(param).val();
            //                if(value == "0")
            //                    return false;
            //                else
            //                    return true;
            },

            isrequired: function(value, element, param) {
                if($(param).checked)
                    return false;
                else
                    return true;
            },

            //This condition is made by rajesh singh
            CompareText: function(value, element, param) {
                //alert(param);
                return $.trim(value) == param;
            },

            cardexpirydt: function(value, element,param) {

                //var mydate = new Date(value).format("MM/dd/yyyy");
                //var currdate = new Date().format("MM/dd/yyyy");

                var month = $(param).val();
                var d=new Date();
                var intMonth = d.getMonth()+1;


                var curyear = d.getFullYear();

                if(value == curyear)
                {

                    var diff = month - intMonth;


                    if(diff > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }


            },


            eventdate: function(value, element) {

                var mydate = new Date(value);
                var currdate = new Date();
                if ((currdate - mydate) < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            //       var useroffset = currdate.getTimezoneOffset()/60;
            //
            //       var currentdateUtc;
            //       currentdateUtc =  currdate + useroffset;
            //       var currentarray=currentdateUtc.split(" ");
            //       var currentdateUtcDate =new Date(currentarray[1] + " " + currentarray[2] + " " +  currentarray[3]);
            //       alert(currentdateUtcDate);
            //
            //        var enetereddate=new Date(value);
            //        var enetereddateUtc = enetereddate + useroffset;
            //        var eneteredarray=enetereddateUtc.split(" ");
            //        var enetereddateUtcDate =new Date(eneteredarray[1] + " " + eneteredarray[2] + " " +  eneteredarray[3]);
            //        alert(enetereddateUtcDate);
            //
            //       if(enetereddateUtcDate >= currentdateUtcDate)
            //           {
            //                return true;
            //           }
            //           else
            //           {
            //                return false;
            //           }

            },

            validenddate: function(value, element) {

                //var mydate = new Date(value).format("MM/dd/yyyy");
                //var currdate = new Date().format("MM/dd/yyyy");

                var mydate = new Date(value);
                var currdate = new Date($(param).val());
                var diff = mydate - currdate;

                diff = Math.ceil(diff/1000/60/60/24);

                diff= diff*1;


                if(diff > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            },





            //            CompareTo:function(value,element,params)
            //            {
            //                var object = eval( '(' + params + ')' );
            //                var controltovalidate=object.controltovalidate;
            //                var operator=object.operator;
            //                switch(operator)
            //                {
            //                    case "==":
            //                        return Date.parse(value)==Date.parse($(controltovalidate).val());
            //                        break;
            //                    case "!=":
            //                        return Date.parse(value)!=Date.parse($(controltovalidate).val());
            //                        break;
            //                    case "<":
            //                        return Date.parse(value)< Date.parse($(controltovalidate).val());
            //                  CompareTo      break;
            //                    case ">":
            //                        return Date.parse(value)>Date.parse($(controltovalidate).val());
            //                        break;
            //                    case ">=":
            //                        return Date.parse(value)>=Date.parse($(controltovalidate).val());
            //                        break;
            //                    case "<=":
            //                        return Date.parse(value)<=Date.parse($(controltovalidate).val());
            //                        break;
            //                    default :
            //                        return true;
            //                }
            //            },
            CompareWithoutEqual:function(value,element,params)
            {
                //                var mydate = new Date(value);
                //                var currdate = new Date($(params).val());
                var mydate = new Date(value)
                var currdate = new Date($(params).val());
                var diff = mydate - currdate;

                diff = Math.ceil(diff/1000/60/60/24);

                diff= diff*1;


                if(diff > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            //                var d = new Date(params);
            //                var date= d.getDate();
            //                var month= d.getMonth();
            //                var year= d.getFullYear();
            //                params = month + "/"+ date + "/" + year ;
            //
            //
            //                var dv = new Date(value);
            //                var datev= dv.getDate();
            //                var monthv= dv.getMonth();
            //                var yearv= dv.getFullYear();
            //                valuev = monthv + "/"+ datev + "/" + yearv ;
            //
            //                return this.optional(element) || Date.parse(valuev) > Date.parse($(params).val());

            },
            CompareToWithoutEqual:function(value,element,params)
            {
                if(value!='' && $(params).val()!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = $(params).val().split('/')
                    var mdyphr=mdyp[2].split(' ');


                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }
                    return this.optional(element) || Date.parse(date) > Date.parse(datep);
                //return this.optional(element) || isNaN(value) && isNaN($(params).val()) || (parseFloat(value) > parseFloat($(params).val()));
                //return this.optional(element) || isNaN(value) && isNaN($(params).val()) || (parseFloat(value) > parseFloat($(params).val()));
                }
                else
                {
                    return true;
                }
            },
            CompareToForGreater:function(value,element,params)
            {
                if(value!='' && $(params).val()!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = $(params).val().split('/')
                    var mdyphr=mdyp[2].split(' ');

                
                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time
                            
                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }

                    return this.optional(element) || Date.parse(date) >= Date.parse(datep);
                }else
                {
                    return true;
                }
            },
            CompareToForLess:function(value,element,params)
            {
                if(value!='' && $(params).val()!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = $(params).val().split('/')
                    var mdyphr=mdyp[2].split(' ');


                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }
                    return this.optional(element) || Date.parse(date) <= Date.parse(datep);
                }
                else
                {
                    return true;
                }
            },
            CompareToForToday:function(value,element,params)
            {
                if(value!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr= mdy[2].split(' ');  //Year and time split
                   
                    
                    //var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                    
                    if(mdyhr[1] == undefined){
                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                    }else
                    {
                        var mdyhrtime=mdyhr[1].split(':');
                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                    }

                    var d = new Date();
                    if(mdyhr[1] == undefined){
                        var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                    }
                    else
                    {
                        var mdyhrtime=mdyhr[1].split(':');
                        var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                    }

                    return this.optional(element) || Date.parse(valuedate) > Date.parse(todaydate);
                }
                else
                {
                    return true;
                }
            },

            CompareToForTodayAlso:function(value,element,params)
            {
                if(value!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr= mdy[2].split(' ');  //Year and time split


                    //var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                    if(mdyhr[1] == undefined){
                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                    }else
                    {
                        var mdyhrtime=mdyhr[1].split(':');
                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                    }

                    var d = new Date();
                    if(mdyhr[1] == undefined){
                        var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                    }
                    else
                    {
                        var mdyhrtime=mdyhr[1].split(':');
                        var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                    }

                    return this.optional(element) || Date.parse(valuedate) >= Date.parse(todaydate);
                }
                else
                {
                    return true;
                }
            },

            TodayOrLessThenToday:function(value,element,params)
            {
                if(value!=''){
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr= mdy[2].split(' ');  //Year and time split
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                    var d = new Date();
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                    //alert(valuedate);
                    //alert(todaydate);
                    return this.optional(element) || Date.parse(valuedate) <= Date.parse(todaydate);
                }
                else
                {
                    return true;
                }
            },

            namnivalue: function(value, element) {

                if(value=="abhishek")
                    return true;
                else
                    return false;
            },
            filevalue: function(value,element,param){
                param=$("#filetype").val();
                var ext = value.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();
                if(param=="image" && (fileType !='gif' && fileType !='jpg'))
                {
                    return false;
                }
                else if (param=="flash" && (fileType !='flv' && fileType !='swf'))
                {
                    return false;
                }
                else
                    return true;
            }
        }

    });

})(jQuery);

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()
;
(function($) {
    var ajax = $.ajax;
    var pendingRequests = {};
    $.ajax = function(settings) {
        // create settings for compatibility with ajaxSetup
        settings = $.extend(settings, $.extend({}, $.ajaxSettings, settings));
        var port = settings.port;
        if (settings.mode == "abort") {
            if ( pendingRequests[port] ) {
                pendingRequests[port].abort();
            }
            return (pendingRequests[port] = ajax.apply(this, arguments));
        }
        return ajax.apply(this, arguments);
    };
})(jQuery);

// provides cross-browser focusin and focusout events
// IE has native support, in other browsers, use event caputuring (neither bubbles)

// provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
// handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target

// provides triggerEvent(type: String, target: Element) to trigger delegated events
;
(function($) {
    $.each({
        focus: 'focusin',
        blur: 'focusout'
    }, function( original, fix ){
        $.event.special[fix] = {
            setup:function() {
                if ( $.browser.msie ) return false;
                this.addEventListener( original, $.event.special[fix].handler, true );
            },
            teardown:function() {
                if ( $.browser.msie ) return false;
                this.removeEventListener( original,
                    $.event.special[fix].handler, true );
            },
            handler: function(e) {
                arguments[0] = $.event.fix(e);
                arguments[0].type = fix;
                return $.event.handle.apply(this, arguments);
            }
        };
    });
    $.extend($.fn, {
        delegate: function(type, delegate, handler) {
            return this.bind(type, function(event) {
                var target = $(event.target);
                if (target.is(delegate)) {
                    return handler.apply(target, arguments);
                }
            });
        },
        triggerEvent: function(type, target) {
            return this.triggerHandler(type, [$.event.fix({
                type: type,
                target: target
            })]);
        }
    })
})(jQuery);
