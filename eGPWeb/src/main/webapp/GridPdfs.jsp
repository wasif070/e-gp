<%--
    Document   : GridPdfs
    Created on : Jul 5, 2012, 12:33:11 PM
    Author     : taher.tinwala
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">
                    <%
                                String buf = request.getParameter("pdfBuffer");
                                out.println(URLDecoder.decode(buf, "UTF-8"));
                    %>
                </div>
            </div>
        </div>
    </body>
</body>
</html>
