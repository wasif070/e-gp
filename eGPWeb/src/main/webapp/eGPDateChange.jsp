<%-- 
    Document   : eGPDateChange
    Created on : Jan 12, 2012, 12:47:55 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP Dates Update Interface</title>
        <link REL="SHORTCUT ICON" HREF="/resources/favicon2.ico">
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#dateFrmId').submit(function() {
                    if($.trim($('#tenderId').val())==""){
                        alert("Please Enter TenderId.");
                        return false;
                    }else{
                        if(isNaN($('#tenderId').val())){
                            alert("Please Enter Numerics in TenderId.");
                            return false;
                        }
                    }
                    if($.trim($('#addDays').val())==""){
                        alert("Please Enter Days atleast zero.");
                        return false;
                    }else{
                        if(isNaN($('#addDays').val())){
                            alert("Please Enter Numerics in Days atleast zero.");
                            return false;
                        }
                    }
                    if($('#dateType').val()=="openingDt"){
                        return confirm("Are You sure you want to update Opening Date");
                    }
                    if($('#dateType').val()=="submissionDt"){
                        return confirm("Are You sure you want to update Submission/Closing Date");
                    }
                    if($('#dateType').val()=="tenderPubDt"){
                        return confirm("Are You sure you want to update Publication Date");
                    }
                    if($('#dateType').val()=="pretendermeetingstart"){
                        return confirm("Are You sure you want to update Pre - Tender Start Date");
                    }
                    if($('#dateType').val()=="pretendermeetingend"){
                        return confirm("Are You sure you want to update Pre - Tender End Date");
                    }
                    if($('#dateType').val()=="documentsellingdate"){
                        return confirm("Are You sure you want to update Document Selling Date");
                    }
                    if($('#dateType').val()=="naoacceptdate"){
                        return confirm("Are You sure you want to update Letter of Acceptance (LOA) Accept Date");
                    }
                    if($('#dateType').val()=="negstartdate"){
                        return confirm("Are You sure you want to update Negotiation Start Date");
                    }
                    if($('#dateType').val()=="tendervaliditydate"){
                        return confirm("Are You sure you want to update Tender Validity Ext Date");
                    }
                    
                });
            });
        </script>
    </head>
    <%
                if ((request.getSession().getAttribute("userTypeId")!=null && !request.getSession().getAttribute("userTypeId").equals("2")) && true || request.getRemoteAddr().equals("192.168.100.152") || request.getRemoteAddr().equals("10.1.4.21")) {
                    String tenderId = request.getParameter("tenderId");
                    String addDays = request.getParameter("addDays");
                    String dateType = request.getParameter("dateType");
                    String msgId = request.getParameter("msgId");
                    String tId = request.getParameter("tId");
                    if ("Submit".equals(request.getParameter("submit")) && tenderId != null && addDays != null) {
                        HibernateQueryDao queryDao = (HibernateQueryDao) AppContext.getSpringBean("HibernateQueryDao");
                        StringBuilder query = new StringBuilder();
                        msgId = "0";
                        tId = tenderId;
                        if (dateType.equals("openingDt")) {
                            query.append("update TblTenderDetails set openingDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            query.delete(0, query.length());
                            query.append("update TblTenderOpenDates set openingDt=(current_timestamp()+" + addDays + ") where tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "1";
                        } else if (dateType.equals("submissionDt")) {
                            query.append("update TblTenderDetails set submissionDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "2";
                        } else if (dateType.equals("tenderPubDt")) {
                            query.append("update TblTenderDetails set tenderPubDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "3";
                        } else if (dateType.equals("pretendermeetingstart")) {
                            query.append("update TblTenderDetails set preBidStartDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "4";
                        } else if (dateType.equals("pretendermeetingend")) {
                            query.append("update TblTenderDetails set preBidEndDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "5";
                        } else if (dateType.equals("documentsellingdate")) {
                            query.append("update TblTenderDetails set docEndDate=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "6";                        
                        } else if (dateType.equals("naoacceptdate")) {
                            query.append("update TblNoaIssueDetails set noaAcceptDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "7";
                        } else if (dateType.equals("negstartdate")) {
                            query.append("update TblNegotiation set negStartDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "8";
                        }else if (dateType.equals("tendervaliditydate")) {
                            query.append("update TblTenderDetails set tenderValidityDt=(current_timestamp()+" + addDays + ") where tblTenderMaster.tenderId=" + tenderId);
                            queryDao.updateDeleteNewQuery(query.toString());
                            msgId = "9";
                        }
                        else if (dateType.equals("noaIssueDate")) {
                            String daysToNOA = "";
                            String procMethodId = "";
                            String procNature = "";
                            String procType = "";
                            String procTypeId = "";
                            String procNatureId = "";
                            List<CommonTenderDetails> list = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
                            if (!list.isEmpty()){
                                 procMethodId = list.get(0).getProcurementMethodId().toString();
                                 procNature = list.get(0).getProcurementNature();
                                 procType = list.get(0).getProcurementType();
                             }
                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            procTypeId = procType.equalsIgnoreCase("NCT")? "1" : "2" ;
                            procNatureId = procNature.equalsIgnoreCase("Goods")?"1":procNature.equalsIgnoreCase("Works")?"2":"3";
                            List<SPCommonSearchDataMore> getNOAConfig = commonSearchDataMoreService.geteGPData("GetNOAConfig", tenderId, procMethodId, procNatureId, procTypeId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            daysToNOA = getNOAConfig.isEmpty()?"0":getNOAConfig.get(0).getFieldName2();
                            
                            
                            int newAddDays = Integer.parseInt(addDays) - Integer.parseInt(daysToNOA);
                            String newAddDays2 = ""+newAddDays;
                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            tenderCommonService.returndata("updateNoaIssueDate", tenderId, newAddDays2);
                            msgId = "10";
                        }
                        response.sendRedirect(request.getContextPath() + "/eGPDateChange.jsp?msgId=" + msgId + "&tId=" + tId);
                    }
    %>
    <body>
        <form action="eGPDateChange.jsp" method="post" id="dateFrmId">
            <div align="center">
                <%
                                    if (tId != null && msgId != null && !msgId.equals("0")) {
                                        out.print("<div class='responseMsg successMsg t_space'>");
                                        if (msgId.equals("1")) {
                                            out.print("Opening Date");
                                        } else if (msgId.equals("2")) {
                                            out.print("Submission/Closing Date");
                                        } else if (msgId.equals("3")) {
                                            out.print("Publication Date");
                                        } else if (msgId.equals("4")) {
                                            out.print("Pre - Tender Start Date");
                                        } else if (msgId.equals("5")) {
                                            out.print("Pre - Tender End Date");
                                        } else if (msgId.equals("6")) {
                                            out.print("Document Selling Date");                                        
                                        } else if (msgId.equals("7")) {
                                            out.print("LOA Accept Date");
                                        } else if (msgId.equals("8")) {
                                            out.print("Negotiation Start Date");
                                        }else if (msgId.equals("9")) {
                                            out.print("Tender Validity Ext Date");
                                        }
                                        else if (msgId.equals("10")) {
                                            out.print("LOA Issue Date");
                                        }
                                        out.print(" Successfully Updated for TenderID : " + tId + "</div>");
                                    }
                                    if (msgId != null && msgId.equals("0")) {
                                        out.print("<div class='responseMsg errorMsg t_space'>Your Request has not been processed please contact TaherT</div>");
                                    }
                %>
                <table border="1" cellspacing="10" cellpadding="0" class="tableList_1 t_space" width="50%">
                    <tr>
                        <th>Tender ID</th>
                        <td>
                            <input type="text" class="formTxtBox_1"  style="width: 200px;" id="tenderId" name="tenderId"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Date Type</th>
                        <td>
                            <select class="formTxtBox_1" style="width: 200px;" id="dateType" name="dateType">
                                <option value="openingDt">Opening Date</option>
                                <option value="submissionDt">Submission/Closing Date</option>
                                <option value="tenderPubDt">Publication Date</option>
                                <option value="pretendermeetingstart">Pre - Tender Start Date</option>
                                <option value="pretendermeetingend">Pre - Tender End Date</option>
                                <option value="documentsellingdate">Document Selling Date</option>
                                <option value="naoacceptdate">LOA Accept Date</option>
                                <option value="negstartdate">Negotiation Start Date</option>
                                <option value="tendervaliditydate">Tender Validity Ext Date</option>
                                <option value="noaIssueDate">LOA Issue Date</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Days to be Added</th>
                        <td>
                            <input type="text" value="0" class="formTxtBox_1"  style="width: 200px;" id="addDays" name="addDays"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-center">
                            <input type="submit" value="Submit" name="submit" class="formTxtBox_1"/>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
    <%
                } else {
                    response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                }
    %>
</html>
