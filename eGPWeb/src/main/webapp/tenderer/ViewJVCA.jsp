<%--
    Document   : CreateJVCA
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : Rikin
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblJointVenture"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="jvcSrBean" class="com.cptu.egp.eps.web.servicebean.JvcaSrBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            if(session.getAttribute("userId")!=null){
             jvcSrBean.setLogUserId(session.getAttribute("userId").toString());
           }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure JVCA (Joint Venture Consortium Association)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>Configure JVCA (Joint Venture Consortium Association)</title>
    </head>
    <body>
        <%
            if(request.getParameter("action")!=null && "Complete".equalsIgnoreCase(request.getParameter("action"))){ 
                jvcSrBean.setAuditTrail(null);
            }else{
                jvcSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            }
            String contextPath = request.getContextPath();
            String jvcId = "";
            String JVName ="";
            
            if(request.getParameter("jvcId")!=null && !"".equalsIgnoreCase(request.getParameter("jvcId"))){
                jvcId = request.getParameter("jvcId");

                List<TblJointVenture> jvcDetails = jvcSrBean.getJVCADetails(Integer.parseInt(jvcId));
                JVName = jvcDetails.get(0).getJvname();
            }
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">View JVCA
            <span style="float: right;">
                <a class="action-button-goback" href="JvcaList.jsp">Go Back to Dashboard</a>
            </span>
        </div>
        <div id="users-contain" class="ui-widget">
            <form action="<%=contextPath%>/JVCAServlet" method="post" id="frmComm">
                <input type="hidden" name="funName" id="funName" value="completeJVCA" />
                <input type="hidden" name="jvcId" id="jvcId" value="<%=jvcId%>" />
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="15%" class="t-align-left ff">JVCA Name :</td>
                            <td width="85%" class="t-align-left">
                                <%=JVName%>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Partners :
                            </td>
                            <td>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                                    <thead>
                                        <tr>
                                            <th width="25%" class="t-align-left ff">Company Name</th>
                                            <th width="30%" class="t-align-left ff">e-mail ID </th>
                                            <th width="25%" class="t-align-left ff">Lead or Secondary Partner</th>
                                            <th width="20%" class="t-align-left ff">Nominated Partner</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            List<Object[]> listPartner = jvcSrBean.getJvcaPartnerDetailList(Integer.parseInt(jvcId));
                                            for(Object[] Partner:listPartner){
                                        %>
                                        <tr>
                                            <td><%=Partner[0]%></td>
                                            <td><%=Partner[4]%></td>
                                            <td class="t-align-center"><%=Partner[1]%></td>
                                            <td class="t-align-center"><%=Partner[2]%></td>
                                        </tr>
                                        <% } %>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div>&nbsp;</div>
                    <% if(request.getParameter("action")!=null && "Complete".equalsIgnoreCase(request.getParameter("action"))){ %>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="complete" type="submit" value="Send Invitation" id="btnComplete"/>
                        </label>
                    </div>
                    <% } %>
                </div>
            </form>
        </div>
        <div>&nbsp;</div>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
