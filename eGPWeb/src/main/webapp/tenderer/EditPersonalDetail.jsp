<%--
    Document   : EditPersonalDetail
    Created on : Jan 5, 2011
    Author     : TaherT
--%>

<!--page formation changed by Emtaz on 29/May/2016-->

<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@ page buffer="15kb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tendererMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Company Contact Person Details</title>
         <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmSignup").validate({
                    rules: {
                        title: { required: false },
                        country:{required:true},
                        state:{required:true},
                        firstName: { required: true,maxlength: 50 },
                        middleName: { maxlength: 50},
                        lastName: { required: false,maxlength: 50},
                        nationalIdNo: { required: false,alphaNationalId:true,maxlength: 25},
                        address1: { required: true,maxlength: 250 },
                        address2: { maxlength: 250 },
                        city: { required: false,maxlength: 50,DigAndNum:true},
                        upJilla: { maxlength: 50},
                        postcode: { number:true,minlength: 4 },
                        phoneNo: {required: false, number: true,maxlength: 20 },
                        phoneSTD: {required: false,number: true,maxlength: 10 },
                        faxNo: { number: true,maxlength: 20 },
                        faxSTD: { number: true,maxlength: 10 },
                        mobileNo: { required: true, number: true },
                        specialization: { required: false},
                        website: { url: true }
                    },
                    messages: {
                        //title: { required: "<div class='reqF_1'>Please select Title</div>" },
                        country: { required: "<div class='reqF_1'>Please Select Country</div>" },
                        state: { required: "<div class='reqF_1'>Please Enter Dzongkhag / District Name</div>" },

                        firstName: {
                            required: "<div class='reqF_1'>Please Enter  First Name</div>",
                            maxlength: "<div class='reqF_1'>Only 50 characters are allowed</div>"
                        },
                        middleName: {
                            maxlength: "<div class='reqF_1'>Only 50 characters are allowed</div>"
                        },
                        lastName: {
                            //required: "<div class='reqF_1'>Please Enter Last Name</div>",
                            maxlength: "<div class='reqF_1'>Only 50 characters are allowed</div>"
                        },
                        nationalIdNo: {
                            //required: "<div class='reqF_1'>Please enter National ID / Passport No. / Driving License No.</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        address1: {
                            required: "<div class='reqF_1'>Please Enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        address2: {
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        city: {
                            //required: "<div class='reqF_1'>Please select City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only.</div>"
                        },
                        upJilla: {
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            //alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
                        },
                        postcode: {
                            //required: "<div class='reqF_1'>Please Enter Post/Zip Code</div>",
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 characters are required</div>"
                        },
                        phoneNo: { //required: "<div class='reqF_1'>Please Enter Phone Number</div>",
                            number: "<div class='reqF_1'>Please Enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>"
                        },
                        phoneSTD: {
                            //required: "<div class='reqF_1'>Please Enter STD code</div>",
                            number: "<div class='reqF_1'>Please Enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 digits are allowed</div>"
                        },
                        faxNo: {
                            number: "<div class='reqF_1'>Please Enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>"
                        },
                        faxSTD: {
                            number: "<div class='reqF_1'>Please Enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 digits are allowed"
                        },
                        mobileNo: {required: "<div class='reqF_1'>Please Enter Mobile Number</div>",
                            number: "<div class='reqF_1'>Please Enter Numbers only</div>"
                        },
                        specialization: {
                            //required: "<div class='reqF_1'>Please Select Specialization / Business Categories</div>"
                        },
                        website: {
                            url: "<div class='reqF_1'>Please Enter Valid Website Name</div>"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "specialization")
                            error.insertAfter("#aTree");
                        else
                            error.insertAfter(element);
                        if (element.attr("name") == "faxSTD")
                            error.insertAfter("#txtFax");
                        else
                            error.insertAfter(element);
                        if (element.attr("name") == "phoneSTD")
                            error.insertAfter("#txtPhone");
                        else
                            error.insertAfter(element);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function SubDistrictANDAreaCodeLoad()
            {// Automatic SubDistrict & Area code selection by Emtaz 29/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbSubDistrict').val()}, function (j) {
                    $('#cmbSubDistrict').html(j);
                });
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                    $('#phoneSTD').val(j);
                    $('#faxSTD').val(j);
                });
            }
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCountry').val(),funName:'countryCode'},  function(j){
                        $('#cntCode').val(j.toString());
                    });
                });
            });
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateComboValue'},  function(j){
                        $("select#cmbState").html(j);
                        //Change 136 to 150,Proshanto
                        if($('#cmbCountry').val()=="150"){
                            $('#trBangla').css("display","table-row")
                            $('#trRthana').css("display","table-row")
                            $('#trRsubdistrict').css("display","table-row")
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#phoneSTD').val(j);
                                $('#faxSTD').val(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbSubDistrict').html(j);
                            });
                        }else{
                            $('#trBangla').css("display","none")
                            $('#trRthana').css("display","none")
                            $('#trRsubdistrict').css("display","none")
                        }
                        $('#txtCity').val(null);
                        $('#txtPost').val(null);
                        $('#txtMob').val(null);
                        $('#txtPhone').val(null);
                        $('#txtFax').val(null);
                        $('#phoneSTD').val(null);
                        $('#faxSTD').val(null);
                    });
                });
            });

            $(function() {
                $('#txtNationalId').blur(function() {
                    if($('#txtNationalId').val()!=""){
                        $('span.#natMsg').css("color","red");
                        $('span.#natMsg').html("Checking for Unique National Id...");                        
                        if($('#txtNationalId').val()!=$('#hdnNat').val()){                        
                        $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId:$('#txtNationalId').val(),funName:'verifyNationId'},
                        function(j){
                            if(j.toString()==""){
                                $('span.#natMsg').html(null);
                            }else{
                                $('span.#natMsg').css("color","red");
                                $('span.#natMsg').html(j);
                            }
                        });
                    }else{
                        $('span.#natMsg').html(null);
                    }
                    }
                });
            });

            $(function() {
                $('#txtMob').blur(function() {
                    if($('#txtMob').val()!=""){
                        $('span.#mobMsg').css("color","red");
                        $('span.#mobMsg').html("Checking for Unique Mobile Number...");
                        if($('#txtMob').val()!=$('#hdnMob').val()){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobile'},
                        function(j){
                            if(j.toString().indexOf("M", 0)!=-1){
                                $('span.#mobMsg').css("color","red");
                                $('span.#mobMsg').html(j);
                            }
                            else{
                                $('span.#mobMsg').css("color","green");
//                                $('span.#mobMsg').html("Ok");
                            }
                        });
                        }else{
                        $('span.#mobMsg').css("color","green");
//                                $('span.#mobMsg').html("Ok");
                    }
                    }
                });
            });
            $(function() {
                $('#cmbTin').change(function() {
                    if($('#cmbTin').val()=="tin"){
                        document.getElementById('divdescription').style.display="None";
                        $('#txtaODoc').val(null);
                    }else{
                        document.getElementById('divdescription').style.display="inline";
                    }
                });
            });
            
            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbSubDistrict').change(function () {
                    if ($('#cmbSubDistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    } else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    }
                });
            });

            $(function() {
                $('#frmSignup').submit(function() {
                    if($("#cmbTin").val()=="tin"){
                        if($.trim($("#tinNoNo").val())==""){
                            $("#tinNoNospan").html("Please Enter Tin No");
                            $("#tinNoNo").focus();
                            return false;
                        }else{
                            if($("#tinNoNo").val().length>30){
                                $("#tinNoNospan").html("Please Enter Tin No less than 30");
                                $("#tinNoNo").focus();
                                return false;
                            }else{
                                $("#tinNoNospan").html(null);
                            }
                        }
                    }else if($("#cmbTin").val()=="other"){
                        if($.trim($("#tinNoNo").val())==""){
                            $("#tinNoNospan").html("Please Enter Tin No");
                            $("#tinNoNo").focus();
                            return false;
                        }else{
                            if($("#tinNoNo").val().length>30){
                                $("#tinNoNospan").html("Please Enter Tin No less than 30");
                                $("#tinNoNo").focus();
                                return false;
                            }else{
                                $("#tinNoNospan").html(null);
                            }
                        }
                        if($.trim($("#txtaODoc").val())==""){
                            $("#txtaODocspan").html("Please Enter Description");
                            $("#txtaODoc").focus();
                            return false;
                        }else{
                            if($("#txtaODoc").val().length>100){
                                $("#txtaODocspan").html("Please Enter Description less than 100");
                                $("#txtaODoc").focus();
                                return false;
                            }else{
                                $("#txtaODocspan").html(null);
                            }
                        }
                    }
                    //Change 136 to 150,Proshanto
//                    if($('#cmbCountry').val()=="150"){
//                        if($.trim($('#txtThana').val())==""){
//                            $('#upErr').html("Please Enter Thana/UpaZilla");
//                            return false;
//                        }else{
//                            $('#upErr').html(null);
//                        }
//                    }
                    if(($('span.#mobMsg').html()=="Mobile no. already exist")||($('span.#natMsg').html()=="National Id./Passport No./Driving License No. already exist")){
                        return false;
                    }
                    if(document.getElementById("ValidityCheck").value == '0')
                    {
                        return false;
                    }

                    if($('#frmSignup').valid()){ 
                               ChangeTitleValue();
                            $('#btnUpdate').attr("disabled","true");
                            $('#hdnedit').val("Update");                        
                    }
                });
            });
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCountry').val(),funName:'countryCode'},  function(j){
                        $('#phoneCode').val(j.toString());
                        $('#faxCode').val(j.toString());
                    });
                });
            });
            function loadCPVTree()
            {
                window.open('../resources/common/CPVTree.jsp','CPVTree','menubar=0,scrollbars=1,width=700px');
            }
        </script>
        <script>
            //Emtaz 29/May/2016.
            function ToUpperCase(obj)
            {
                obj.value = obj.value.toUpperCase();
            }
            function TitleChange(TitleValue)
            {
                if(document.getElementById("cmbTitle").value == 'Other')
                {
                    document.getElementById('OtherTitleName').innerHTML = 'Other Title :<span>*</span>';
                    document.getElementById('OtherTitleValue').innerHTML = '<input class="formTxtBox_1" maxlength="50" type="text" id="OtherTitleValueInput" name="OtherTitleValueInput" onblur="TitleValidity()" value="'+TitleValue+'" />';
                }
                else
                {
                    document.getElementById('OtherTitleName').innerHTML = '';
                    document.getElementById('OtherTitleValue').innerHTML = '';
                    $('#OtherTitleMsg').html("");
                    document.getElementById("ValidityCheck").value = "1";
                }
            }
            function ChangeTitleValue()
            {
                if(document.getElementById("cmbTitle").value == 'Other')
                {
                    var OtherTitleValue = document.getElementById("OtherTitleValueInput").value;
                    document.getElementById("OtherTitle").value = OtherTitleValue;
                }
            }
            function TitleValidity()
            {
                if($("#OtherTitleValueInput").val().length>5)
                {
                    $('#OtherTitleMsg').html("Maximum 5 characters are allowed.");
                    document.getElementById("ValidityCheck").value = "0";
                }
                else if($("#OtherTitleValueInput").val()=="")
                {
                    $('#OtherTitleMsg').html("Enter Title.");
                    document.getElementById("ValidityCheck").value = "0";
                }
                else
                {
                    $('#OtherTitleMsg').html("");
                    document.getElementById("ValidityCheck").value = "1";
                }
            }
            //Emtaz
        </script>
    <%
        tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        boolean ifEdit = false;
        ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
        TblTendererMaster dtBean = contentAdminService.findTblTendererMasters("tendererId", Operation_enum.EQ, Integer.parseInt(request.getParameter("tId"))).get(0);
        if (dtBean != null) {
            ifEdit = true;
        }
        String msg = null;    
    %>
    </head>
    
    <body onload="SubDistrictANDAreaCodeLoad();<%if (ifEdit) {%>TitleChange('<%=dtBean.getTitle()%>') <% } else { %>TitleChange('') <%}%>" >
        <%
                                    
                    if ("Update".equals(request.getParameter("hdnedit"))) {
//                        if (tendererMasterDtBean.getMobileNo() != null && tendererMasterDtBean.getNationalIdNo() != null) {
//                            //UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
//                            boolean bol=true;
                            /*if(!tendererMasterDtBean.getMobileNo().equals(request.getParameter("hdnMob"))){
                                if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='" + tendererMasterDtBean.getMobileNo() + "'") != 0) {
                                    msg = "Mobile no. already exist";
                                    bol=false;
                                }
                            }
                            if(bol){
                                if(!tendererMasterDtBean.getNationalIdNo().equals(request.getParameter("hdnNat"))){
                                    if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='" + tendererMasterDtBean.getNationalIdNo() + "'") != 0) {
                                        msg = "National Id./Passport No./Driving License No. already exist";
                                        bol=false;
                                    }
                                }
                            }*/
                            //if(bol){
                                tendererMasterDtBean.setTendererId(dtBean.getTendererId());
                                    tendererMasterDtBean.setCmpId(dtBean.getTblCompanyMaster().getCompanyId());
                                    tendererMasterDtBean.setUserId(dtBean.getTblLoginMaster().getUserId());
                                    tendererMasterDtBean.setIsAdmin(dtBean.getIsAdmin());
                                    if(tendererMasterSrBean.updateTblTenderMaster(tendererMasterDtBean, true)){
                                    response.sendRedirect("EditPersonalDetail.jsp?tId="+request.getParameter("tId")+"&msg=s");
                                    }else{
                                        msg="Problem in updating Company Contact Person Details";
                                    }
                           // }                        
                        }
             //   }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <form id="frmSignup" name="signupForm" method="post" action="EditPersonalDetail.jsp?tId=<%=request.getParameter("tId")%>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
                                <!--Page Content Start-->
                                <div class="tableHead_1 t_space"><% if(dtBean.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%>Edit Personal Details <% } else { %>Edit Company Contact Person Details<% } %></div>
                                <!--jsp:include page="EditNavigation.jsp" /jsp:include-->
                                <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                                <%if (msg != null) {%><br/><div class="responseMsg errorMsg"><%=msg%>.</div><%}%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" align="right">
                                    <tr>
                                        <td style="font-style: italic" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Title : <span>*</span></td>
                                        <td><select name="title" class="formTxtBox_1" id="cmbTitle" <%if (ifEdit) {%>onchange="TitleChange('<%=dtBean.getTitle()%>')" <% } else { %>onchange="TitleChange('')" <%}%> >
                                                <%if (!ifEdit) {%><option value="">Select</option><%}%>
                                                <option value="Mr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                            }%>>Mr.</option>
                                                <option value="Ms." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Ms.")) {%>selected<%}
                                                                            }%>>Ms.</option>
                                                <option value="Mrs." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mrs.")) {%>selected<%}
                                                                            }%>>Mrs.</option>
                                                <option value="Dasho" <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dasho")) {%>selected<%}
                                                                            }%>>Dasho</option>                           
                                                 <option value="Dr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dr.")) {%>selected<%}
                                                                            }%>>Dr.</option>
                                                 <option id="OtherTitle" value="Other" <%if (ifEdit) {
                                                                if (!dtBean.getTitle().equals("Dasho") && !dtBean.getTitle().equals("Dr.")&&!dtBean.getTitle().equals("Mrs.")&&!dtBean.getTitle().equals("Ms.")&&!dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                    }%>>Other</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff"><label id="OtherTitleName"></label></td>
                                        <td>
                                            <label id="OtherTitleValue"></label>
                                            <span id="OtherTitleMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                        
                                    </tr>
                                   
                                    <tr>
                                        <td class="ff">First Name : <span>*</span></td>
                                        <td><input name="firstName" type="text" class="formTxtBox_1" maxlength="50" id="txtFirstName" onblur="ToUpperCase(this)" style="width:200px;" value="<%if (ifEdit) {
                                                        out.print(dtBean.getFirstName());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Middle Name : </td>
                                        <td><input name="middleName" type="text" class="formTxtBox_1" maxlength="50" id="txtMiddleName" onblur="ToUpperCase(this)" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getMiddleName());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Last Name : </td>
                                        <td><input name="lastName" type="text" class="formTxtBox_1" maxlength="50" id="txtLastName" onblur="ToUpperCase(this)" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getLastName());
                                                    }%>"/></td>
                                    </tr>
<!--                                    <tr id="trBangla">
                                        <td class="ff">Name in Bangla :</td>-->
                                        <td><input name="banglaName" type="hidden" class="formTxtBox_1" maxlength="300" id="txtNameBangla" style="width:200px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getFullNameInBangla() != null) {
                                                            out.print(BanglaNameUtils.getUTFString(dtBean.getFullNameInBangla()));
                                                        }
                                                    }%>"/>
                                            <!--Change Bangladesh to Bhutan,Proshanto-->
<!--                                            <div class="formNoteTxt">(if Bhutanese)</div></td>
                                    </tr>-->
<!--                                    <tr>
                                        <td class="ff">National Id / Passport No. / <br />
                                            Driving License No : <span>*</span></td>
                                        <td><input name="nationalIdNo" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;"  value="<%--<%if (ifEdit) {
                                                        out.print(dtBean.getNationalIdNo());
                                                    }%>--%>"/>
                                                    <input type="hidden" value="<%//if (ifEdit){out.print(dtBean.getNationalIdNo());}%>" id="hdnNat" name="hdnNat"/>
                                            <br/><span id="natMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff">Address : <span>*</span></td>
                                        <td><textarea name="address1" rows="3" class="formTxtBox_1" maxlength="250" id="txtaAddr1" style="width:400px;"><%if (ifEdit) {
                                                        out.print(dtBean.getAddress1());
                                                    }%></textarea></td>
                                    </tr>
<!--                                    <tr>
                                        <td class="ff">Address 2 : </td>
                                        <td><textarea name="address2" rows="3" class="formTxtBox_1" maxlength="250" id="txtaAddr2" style="width:400px;"><%--<%if (ifEdit) {
                                                        out.print(dtBean.getAddress2());
                                                    }%>--%></textarea></td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="country" class="formTxtBox_1" id="cmbCountry">
                                                <%
                                                    //Change Bangladesh to Bhutan,Proshanto
                                                            String countryName = "Bhutan";
                                                            if (ifEdit) {
                                                                countryName = dtBean.getCountry();
                                                            }
                                                            for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryName)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td>
                                            <select name="state" class="formTxtBox_1" id="cmbState" >
                                                <%String cntName = "";
                                                            if (ifEdit) {
                                                                cntName = dtBean.getCountry();
                                                            } else {
                                                              
                                                                cntName = "Bhutan";
                                                            }
                                                            for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) 
                                                            {%>
                                                                <option value="<%=state.getObjectValue()%>" <%if (ifEdit) 
                                                                    {
                                                                        if (state.getObjectValue().equals(dtBean.getState())) 
                                                                       {%>
                                                                           selected
                                                                       <%}
                                                                    } 
                                                                    else 
                                                                    {
                                                                        //Change Dhaka to Thimphu
                                                                        if (state.getObjectValue().equals("Thimphu")) 
                                                                        {%>
                                                                            selected
                                                                        <%}
                                                                    }%>><%=state.getObjectValue()%>
                                                                </option>
                                                            <%}%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="trRsubdistrict">
                                        <td class="ff">Dungkhag / Sub-district : </td>
                                        <td><select name="subDistrict" class="formTxtBox_1" id="cmbSubDistrict">

                                                <%String regsubdistrictName = "";
                                                    if (ifEdit) {
                                                        regsubdistrictName = dtBean.getState();
                                                    } else {
                                                        regsubdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regsubdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                        } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                        selected<%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>

                                    </tr>
                                    
                                    <tr id="trRthana">
<!--                                        <td class="ff">Gewog : </td>
                                        <td><input name="upJilla" type="text" class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;"  value="<%/*if (ifEdit) {
                                                        out.print(dtBean.getUpJilla());
                                                    }*/%>"/>
                                            <div id="upErr" style="color: red;"></div></td>-->
                                        <td class="ff" height="30">Gewog : </td>
                                        <td>
                                            <select name="upJilla" class="formTxtBox_1" id="txtThana" style="width:218px;">
                                                <%String stateforGeowg = "Thimphu";
                                                    String subdistrictforGeowg = "";
                                                    if (ifEdit) 
                                                    {
                                                        stateforGeowg = dtBean.getState();
                                                        subdistrictforGeowg = dtBean.getSubDistrict();
                                                    }
                                                    if ( stateforGeowg!= null && subdistrictforGeowg!= null && !subdistrictforGeowg.toLowerCase().contains("Select Dungkhag".toLowerCase())) 
                                                    {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(subdistrictforGeowg)) 
                                                        {%>
                                                            <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) 
                                                                {
                                                                    if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) 
                                                                    {
                                                                    %>selected<%
                                                                    }
                                                                } 
                                                                else if (gewoglist.getObjectValue().equals("")) 
                                                                {%>
                                                                    selected<%
                                                                }%>><%=gewoglist.getObjectValue()%>
                                                            </option>
                                                        <%}
                                                    } 
                                                    else 
                                                    {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(stateforGeowg)) 
                                                        {%>
                                                            <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) 
                                                                {
                                                                    if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) 
                                                                    {
                                                                        %>selected<%
                                                                    }
                                                                }%>

                                                                ><%=gewoglist.getObjectValue()%>
                                                            </option>
                                                <%}
                                                    }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town :</td>
                                        <td><input name="city" type="text" class="formTxtBox_1" maxlength="50" id="txtCity" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getCity());
                                                    }%>"/></td>
                                    </tr>
                                    <%if (ifEdit) {
                                                    //Change Bangladesh to Bhutan,Proshanto
                                                    if (!dtBean.getCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trBangla').css("display","none");
                                        $('#trRthana').css("display","none");
                                        $('#trRsubdistrict').css("display","none");
                                    </script>
                                    <%}
                                                }%>
                                    <tr>
                                        <td class="ff">Post Code: </td>
                                        <td><input name="postcode" type="text" class="formTxtBox_1" maxlength="10" id="txtPost" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getPostcode());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="phoneCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                    } else {
                                                        //Change Bangladesh to Bhutan,Proshanto
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                                    if(dtBean.getPhoneNo().contains("-"))
                                                                    out.print(dtBean.getPhoneNo().split("-")[0]);
                                                                }%>" name="phoneSTD" id="phoneSTD" class="formTxtBox_1" style="width:50px;" />-<input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhone" style="width:100px;"  value="<%if (ifEdit) {
                                                                                if(dtBean.getPhoneNo().contains("-"))
                                                                                out.print(dtBean.getPhoneNo().split("-")[1]);
                                                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="faxCode" value="<%
                                                    if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                    } else {
                                                        //Change Bangladesh to Bhutan,Proshanto
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                        if(dtBean.getFaxNo().contains("-"))
                                                                    out.print(dtBean.getFaxNo().split("-")[0]);
                                                                }%>" name="faxSTD" id="faxSTD" class="formTxtBox_1" style="width:50px;" />-<input name="faxNo" type="text" class="formTxtBox_1" maxlength="20" id="txtFax" style="width:100px;"  value="<%if (ifEdit) {
                                                                                if(dtBean.getFaxNo().contains("-"))
                                                                                out.print(dtBean.getFaxNo().split("-")[1]);
                                                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile No : <span>*</span></td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="cntCode" value="<%if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                    } else {
                                                        //Change Bangladesh to Bhutan,Proshanto
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMob" style="width:160px;"  value="<%if (ifEdit) {
                                                                    out.print(dtBean.getMobileNo());
                                                                }%>"/>
                                            <input type="hidden" value="<%if (ifEdit){out.print(dtBean.getMobileNo());}%>" id="hdnMob" name="hdnMob"/>
                                            <span id="mobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Tax Payment Number : <span>*</span></td>
                                        <td>
<!--                                            <select name="tinNo" class="formTxtBox_1" id="cmbTin">
                                                <option <%/*if (ifEdit) {
                                                                if (dtBean.getTinNo().equals("tin")) {*/%>selected<%/*}
                                                                            }*/%> value="tin">TIN</option>
                                                <option value="other" <%/*if (ifEdit) {
                                                                if (dtBean.getTinNo().equals("other")) {*/%>selected<%/*}
                                                                            }*/%>>Other Similar Document</option>
                                            </select>-->
                                            <input type="hidden" name="tinNo" id="cmbTin" value="tin" />

                                            
                                                <%
                                                            if (ifEdit) {
                                                                if (dtBean.getTinDocName().contains("$")) {
                                                %>
                                                <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%=dtBean.getTinDocName().substring(0, dtBean.getTinDocName().indexOf("$"))%>"  class="formTxtBox_1"/><br/>
                                                <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                                <div id="divdescription">
                                                    <textarea name="otherDoc" cols="10" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"><%=dtBean.getTinDocName().substring(dtBean.getTinDocName().indexOf("$") + 1, dtBean.getTinDocName().length())%></textarea>
                                                    <div class="formNoteTxt" id="2n">(Description)<br/><span id="txtaODocspan"></span></div>
                                                </div>
                                                <%
                                                                                                                } else {
                                                %>
                                                       <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%if (ifEdit) {
                                                                                                                        out.print(dtBean.getTinDocName());
                                                                                                                    }%>"  class="formTxtBox_1"/>
                                                <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                                <div id="divdescription" style="display: none;">
                                                    <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"></textarea>
                                                    <div class="formNoteTxt" id="2n" >(Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                                </div>
                                                <%
                                                                                                                }
                                                                                                            } else {
                                                %>
                                                <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" class="formTxtBox_1"/><br/>
                                                <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                                <div id="divdescription" style="display: none;">
                                                    <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"></textarea>
                                                    <div class="formNoteTxt" id="2n">(Document Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                                </div>
                                                <%                                                        }
                                                %>
                                            
                                        </td>
                                    </tr>
<!--                                    <tr>
                                        <td class="ff">Specialization : <span>*</span></td>
                                        <td><textarea name="specialization" cols="10" rows="3" class="formTxtBox_1" id="txtaCPV" style="width:400px;" readonly><%/*if (ifEdit) {
                                                        out.print(dtBean.getSpecialization());
                                                    }*/%></textarea> <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()" id="aTree">Select Category</a></td>
                                    </tr>-->
                                    <input type="hidden" name="specialization" class="formTxtBox_1" id="txtaCPV" value="disabled"/>
                                    <tr>
                                        <td class="ff">Website : </td>
                                        <td><input name="website" type="text" class="formTxtBox_1" maxlength="100" id="txtWebsite" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getWebsite());
                                                    }%>"/><br/>(Enter website name without 'http://' e.g. pppd.gov.bt)</td>
                                    </tr>
                                    <%if (ifEdit) {%>
                                    <tr>
                                        <td>&nbsp;<input name="comments" type="hidden" class="formTxtBox_1" maxlength="100" id="txtComment" style="width:200px;"  value="user"/></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            &nbsp;                                            
                                             <label class="formBtn_1">
                                                <input type="submit" name="edit" id="btnUpdate" value="Update" />
                                                <input type="hidden" name="hdnedit" id="hdnedit" value=""/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                                <!--Page Content End-->
                            </td>
                        </tr>
                        <input type="hidden" id="ValidityCheck" value="1" />
                    </table>
                </form>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
