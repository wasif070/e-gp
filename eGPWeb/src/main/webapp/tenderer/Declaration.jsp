<%-- 
    Document   : Declaration
    Created on : Nov 20, 2010, 2:22:49 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document read confirmation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#errMsg').html('');
                $('#btnAgree').click(function(){
                    elements= document.getElementsByName('cbCurrency');
                    var checkedCnt=0;
                    for(i=0;i<elements.length;i++){
                        if (elements[i].checked==true){
                            checkedCnt=checkedCnt+1;
                            if ($('#hdnCurrencyId').val()=="0")
                                $('#hdnCurrencyId').val(elements[i].value)
                            else
                                $('#hdnCurrencyId').val($('#hdnCurrencyId').val()+ ','+elements[i].value);
                        }
                    }
            
                    if (checkedCnt==0){
                        $('#errMsg').html('Select atleast 1 currency!');
                        return false;
                    }
                    else $('#errMsg').html('');

                }) ;
            });

        </script>
    </head>
    <body>
           <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
        <form id="frmDeclaration" action="Declaration.jsp?tab=1&tenderId=<%=request.getParameter("tenderId")%>" method="POST">

            <%
                        String userId = "";
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userId = hs.getAttribute("userId").toString();
                        }
                        if (request.getParameter("btnAgree") != null && userId != "") {
                            if (request.getParameter("hdnCurrencyId") != "" && request.getParameter("hdnCurrencyId") != "0") {
                                String dtXml = "", hdnCurrencyId = "";

                                hdnCurrencyId = request.getParameter("hdnCurrencyId");
                                String aryCurrency[] = hdnCurrencyId.split(",");

                                for (String currencyId : aryCurrency) {
                                    if (dtXml == "") {
                                        dtXml = "<tbl_BidderCurrency tenderId=\"" + request.getParameter("tenderId").toString() + "\" currencyId=\"" + currencyId.toString() + "\" userId=\"" + userId.toString() + "\" />";
                                    } else {
                                        dtXml += "<tbl_BidderCurrency tenderId=\"" + request.getParameter("tenderId").toString() + "\" currencyId=\"" + currencyId.toString() + "\" userId=\"" + userId.toString() + "\" />";
                                    }
                                }

                                dtXml = "<root>" + dtXml + "</root>";
                                

                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_BidderCurrency", dtXml, "").get(0);

                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                dtXml = "<tbl_BidConfirmation tenderId=\"" + request.getParameter("tenderId").toString() + "\" maskName=\"\" confirmationDt=\"" + format.format(new Date()) + "\" eSignature=\"\" digitalSignature=\"\" userId=\"" + userId.toString() + "\" />";
                                dtXml = "<root>" + dtXml + "</root>";
                                commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_BidConfirmation", dtXml, "").get(0);
                                //out.println(commonMsgChk.getMsg());
                            }
                        }
            %>
         
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">Document Read Confirmation</div>
                <div>&nbsp;</div>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <%@include file="TendererTabPanel.jsp" %>
             
                 <%-- <jsp:include page="TendererTabPanel.jsp" >
                    <jsp:param name="tab" value="1" />
                </jsp:include>--%>
                <%if(!is_debared){%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="9%" class="t-align-center ff">Select</th>
                        <th width="91%" class="t-align-center ff">Currency</th>
                    </tr>
                    <%
                                String tenderId = "";
                                int counterId = 0;
                                if (!request.getParameter("tenderId").equals("")) {
                                    tenderId = request.getParameter("tenderId");
                                }

                                tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                for (SPTenderCommonData sptcd : tenderCommonService.returndata("getTenderCurrency", tenderId, null)) {
                                    counterId = counterId + 1;
                    %>
                    <tr  <%if(Math.IEEEremainder(counterId,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>>
                        <td class="t-align-left ff"><label><input name="cbCurrency" id="cbCurrencyId_<%=counterId%>" type="checkbox" value="<%=sptcd.getFieldName3()%>" /></label></td>
                        <td class="t-align-left"><%=sptcd.getFieldName4()%></td>
                    </tr>
                    <%}%>
                </table><%}%>
                <div style="float: left;position: relative"><span id="errMsg" class='reqF_1'></span></div>
                <div>&nbsp;</div>
                <div class="t-align-center">
                    <label class="formBtn_1">
                        <input type="submit" name="btnAgree" id="btnAgree" value="I Agree" />

                    </label>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
                <input type="hidden" id="hdnCurrencyId" name="hdnCurrencyId" value="0" />
            
        </form>
                </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
