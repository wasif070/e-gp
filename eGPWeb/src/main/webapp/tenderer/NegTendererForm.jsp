<%--
    Document   : NegTendererForm
    Created on : Jan 11, 2011, 12:28:23 PM
    Author     : Administrator
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<jsp:useBean id="negotiationFormsSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationFormsSrBean"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="neg" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean" />
    
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
       
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%
                
                    TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    String tenderId;
                    tenderId = request.getParameter("tenderId");
                    int formCnt=0;

                    String usrId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        usrId = session.getAttribute("userId").toString();
                    }
                    //out.println("userid="+usrId);

                    int negId = 0;
                    if(request.getParameter("negId")!=null){
                        negId = Integer.parseInt(request.getParameter("negId"));
                    }

                    boolean isFinalSubmissionDone =false;
                    String bidderstatus = negotiationProcessSrBean.checkFinalNegoFinalSubmissionDone(negId);
                    if(bidderstatus != null && !bidderstatus.equalsIgnoreCase(""))
                        isFinalSubmissionDone = true;
                    int negFormCount=0;
            %>
                <div class="dashboard_div">
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea">
                        <div class="pageHead_1">Negotiation</div>
                        <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", tenderId);
                        %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        
                        <%pageContext.setAttribute("tab", "6");%>
                          <%@include file="../tenderer/TendererTabPanel.jsp" %>

                        <div class="tabPanelArea_1">
                            
                        <jsp:include page="EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="4" />
                        </jsp:include>
                            
                            
                            
                        <div class="tabPanelArea_1">
                        <%
                        
                        if(request.getParameter("msg")!=null)
                        {
                            if("add".equalsIgnoreCase(request.getParameter("msg"))){
            %>
                            <div class="responseMsg successMsg t_space">Form saved successfully</div>
            <%
                            }
                            if("edit".equalsIgnoreCase(request.getParameter("msg"))){
            %>
                            <div class="responseMsg successMsg t_space">Form updated successfully</div>
            <%
                            }
                            if("delete".equalsIgnoreCase(request.getParameter("msg"))){
            %>
                            <div class="responseMsg successMsg t_space">Form deleted successfully</div>
            <%
                            }
                        }
                            
                        
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) 
                        {
                    %>
                    
                    
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                        </tr>
                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="56%" class="t-align-left ff">Form Name</th>
                            <th width="22%" class="t-align-center">Action</th>
                            <th width="22%" class="t-align-center">View Mapped Documents</th>
                        </tr>
                        <%
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsList", tenderId, usrId, null, null, null,null, null, null,null)) 
                            {
                                formCnt++;
                        %>
                        <tr>
                            <td class="t-align-left ff">
                                <%=formdetails.getFieldName2()%>
                                <%
                                if(formdetails.getFieldName7() != null && formdetails.getFieldName7().equalsIgnoreCase("yes"))
                                    out.println("<font style='color:red;'> * </font>");
                                %>
                            </td>
                            <td class="t-align-left">
                                <%
                               int cnt=0;
                               List<SPCommonSearchData> neglist=commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null);
                                for (SPCommonSearchData negoformdetails : neglist)
                                {
                                    cnt ++;
                                    String type="";
                                    int negBidformId=0;

                                    if(negoformdetails.getFieldName4() != null)
                                    {
                                        negBidformId=Integer.parseInt(negoformdetails.getFieldName4());
                                        type="negbiddata";
                                        negFormCount++;
                                    }
                                    else
                                    {
                                        type="biddata";
                                    }
                                    if(!isFinalSubmissionDone)
                                        {
                                            %>
                                                <a href="NegTenderBidForm.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=negoformdetails.getFieldName3()%>&uId=<%=usrId%>&negId=<%=negId%>&negBidformId=<%=negBidformId%>&lotId=0&type=<%=type%>&action=Edit&boqId=<%=formdetails.getFieldName3()%>&ContractType=<%=formdetails.getFieldName5()%>">

                                                    Edit
                                                <%
                                                if(type != null && type.equalsIgnoreCase("negbiddata"))
                                                    out.println(" Negotiation Bid");
                                                %>

                                                </a>
                                                &nbsp;|&nbsp;
                                                <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=negoformdetails.getFieldName3()%>&uId=<%=usrId%>&negId=<%=negId%>&negBidformId=<%=negBidformId%>&lotId=0&type=<%=type%>&action=View">
                                                    View
                                                </a>
                                            <%

                                                if(negoformdetails.getFieldName3()!= null && negoformdetails.getFieldName3().equalsIgnoreCase("0"))
                                                {
                                            %>
                                                &nbsp;|&nbsp;
                                                <a href="NegTenderGetBidData.jsp?action=Delete&negBidformId=<%=negoformdetails.getFieldName4()%>&hdnTenderId=<%=tenderId%>&hiduId=<%=usrId%>&hidnegId=<%=negId%>&save=Delete" onclick="var con = window.confirm('Do you want to delete this form?'); if(con){return true;}else{return false;}">
                                                    Delete
                                                </a>
                                            <%
                                                }
                                            %>
                                                    <BR>
                                            <%

                                            if(cnt == neglist.size() && formdetails.getFieldName6() != null && formdetails.getFieldName6().equalsIgnoreCase("yes"))
                                                {
                                                    %>
                                                    <a href="NegTenderBidForm.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&uId=<%=usrId%>&negId=<%=negId%>&lotId=0&type=Fill&action=Fill&boqId=<%=formdetails.getFieldName3()%>&ContractType=<%=formdetails.getFieldName5()%>">Fill</a>                                    
                                                    <%
                                                }
                                       }
                                    else
                                    {
                                     %>
                                        <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=negoformdetails.getFieldName3()%>&uId=<%=usrId%>&negId=<%=negId%>&negBidformId=<%=negBidformId%>&lotId=0&type=<%=type%>&action=View">
                                        View
                                        </a>        <BR>
                                    <%   
                                    } 
                                }
                                
                            %>
                            </td>
                            <td class="t-align-center">
                                <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>" target="_blank">View</a>
                            </td>
                        </tr>
                        <%
                        
                            
                           
                            
                } // End of Form loop
                            
                // Dispalying Grand Summary. 
                // Commented due to bug id: 5721
                //List<SPTenderCommonData> grandsumlink = objTenderCommonService.returndata("getGrandSummaryLinkNego", tenderId, null);
                //if(!"0".equals(grandsumlink.get(0).getFieldName1()))
                //{
                          %>
     <!--
                          <tr>
                              <td class="t-align-left" colspan="2"><span style="font-weight: bold;">Grand Summary</span>&nbsp;&nbsp;:&nbsp;&nbsp;
                              <a href="<%=request.getContextPath()%>/tenderer/NegoGrandSumm.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>"> View </a>
                              </td>
                          </tr>

     -->
                        <%
               //}
    
                out.println("</table>");
                 
                                
              }%>
              <%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<SPTenderCommonData> sptcdList = tenderCS.returndata("NegotiationDocInfo", String.valueOf(negId), "revisedoc");
                                List<SPTenderCommonData> sptcdListOfficer = new ArrayList<SPTenderCommonData>();
                                List<SPTenderCommonData> sptcdListBidder = new ArrayList<SPTenderCommonData>();
                                if(sptcdList!=null && !sptcdList.isEmpty()){
                                    for(int i=0;i<sptcdList.size();i++){
                                        if(sptcdList.get(i).getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){
                                            sptcdListBidder.add(sptcdList.get(i));
                                        }else{
                                            sptcdListOfficer.add(sptcdList.get(i));
                                        }
                                    }
                                    
                                    if(sptcdListOfficer!=null && !sptcdListOfficer.isEmpty()){
                                
                            %>
                          <table width="100%" cellpadding="0" cellspacing="0" class="tableList_1">   
                            <tr>
                                <td class="t-align-left ff" width="22%">Reference Documents Uploaded by Evaluation Committee :</td>
                                <td>
                                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListOfficer) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&ub=3" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if(!isFinalSubmissionDone && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&ub=3"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                                </td>
                            </tr>
                            <%}
                                    docCnt = 0;
                         if(sptcdListBidder!=null && !sptcdListBidder.isEmpty()){%>           
                            <tr>
                                <td class="t-align-left ff">Reference Documents Uploaded by Bidder/Consultant :</td>
                                <td>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListBidder) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&docType=revisedoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if(!isFinalSubmissionDone && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&docType=revisedoc"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                    </table>
                                </td>
                            </tr>
                          </table>
                                <%}}
                    if(!isFinalSubmissionDone)
                    {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td class="t-align-left ff" width="22%">
                                     Upload Reference Document : 
                                    </td>
                                    <td>
                                        <a href="NegotiationDocsUpload.jsp?negId=<%=negId%>&tenderId=<%=request.getParameter("tenderId")%>&uId=<%=usrId%>&docType=revisedoc">Upload</a>
                                    </td>
                                </tr>
                   </table>
                 <%
                    }
                        // out.println("Form Count="+formCnt+" neg fourm count="+negFormCount);
                        if(negFormCount >= formCnt && !isFinalSubmissionDone)
                       {
                        %>
                        <form name="frmapproveTenderNegBid" id="idfrmapproveTenderNegBid" method="post" action="<%=request.getContextPath()%>/NegotiationFormsSrBean">
                            <input type="hidden" name="hidnegId" id="hidnegId" value="<%=negId%>"/>
                            <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=tenderId%>"/>
                            <input type="hidden" name="action" id="action" value="approveServiceFormBid" />
                            <input type="hidden" name="tenderRefNo" id="tenderRefNo" value="<%=toextTenderRefNo%>" />
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td  class="t-align-center" >
                                        <label class="formBtn_1">
                                        <input name="btnFinalSub" id="btnFinalSub" type="Submit" value="Submit" onclick="return checkValidate();"/></label>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <%
                        }
                       else if(isFinalSubmissionDone)
                        {
                            %>
                            <div class="mandatory"><center>Final Submission for Negotiation Completed Successfully</center></div>
                            <%
                        }
                        else
                        {
                            %>
                            <div class="responseMsg msgInRed t-align-center">Please Edit Mandatory forms</div>
                            <%
                        }
                             
                        %>
                        </div></div></div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
              <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>