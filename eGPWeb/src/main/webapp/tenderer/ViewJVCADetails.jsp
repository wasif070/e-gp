<%-- 
    Document   : ViewJVCADetails
    Created on : Mar 3, 2011, 3:44:19 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.JvcapartnersService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblBiddingPermission"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <title>View JVCA Details</title>
    </head>
    <body>
        <%
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    TblLoginMaster tblLoginMaster = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId"))).get(0);
                    
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                <%if(!"y".equals(request.getParameter("noback"))){%>
                <div class="pageHead_1">JVCA - Details<span style="float: right;"><a class="action-button-goback" href="JvcaList.jsp" >Go Back</a></span>
<!--                    <span style="float:right;"> <a class="action-button-goback" href="JvcaList.jsp">Go back</a> </span>-->
                </div>
                <%}%>
        <div class="tableHead_22 t_space">Invitation Details
            
        </div>
        <table width="100%" cellspacing="0" class="tableList_1" id="members">
                <thead>
                    <tr>
                        <th width="40%" class="t-align-left ff">Company Name</th>
                        <th width="40%" class="t-align-left ff">JVCA Role</th>
                        <!--<th width="20%" class="t-align-left ff">JVCA Status</th>-->
                        <th width="20%" class="t-align-left ff">Nominated Partner</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        JvcapartnersService jvcapartnersService = (JvcapartnersService)AppContext.getSpringBean("JvcapartnersService");
                        jvcapartnersService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        List<Object[]> listPartner = jvcapartnersService.getJvcaPartnerDetailList(request.getParameter("uId"));
                        for(Object[] Partner:listPartner){
                    %>
                    <tr>
                        <td><a target="_blank" href="ViewRegistrationDetail.jsp?cId=<%=Partner[5]%>&uId=<%=Partner[6]%>&tId=<%=Partner[7]%>&top=no"><%=Partner[0]%></a></td>
                        <td class="t-align-center"><%=Partner[1]%></td>
                        <!--<td class="t-align-center">< %=Partner[3]%></td>-->
                        <td class="t-align-center"><%=Partner[2]%></td>
                    </tr>
                    <% } %>
                </tbody>
        </table>
        <div class="tableHead_22 t_space">Registration Details</div>
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
            <tr>
                <td width="18%" class="ff">e-mail ID : </td>
                <td width="82%"><%=tblLoginMaster.getEmailId()%></td>
            </tr>
            <tr>
                <td class="ff">Nationality : </td>
                <td><%=tblLoginMaster.getNationality()%></td>
            </tr>
            <tr>
                <td class="ff">Country of Business : </td>
                <td><%=tblLoginMaster.getBusinessCountryName()%></td>
            </tr>
            <tr>
                <td class="ff">Registration Date & Time : </td>
                <td><%if (tblLoginMaster.getRegisteredDate() != null) {
                                String date = DateUtils.gridDateToStr(tblLoginMaster.getRegisteredDate());
                                out.print(date.substring(0, date.lastIndexOf(":")));
                            }%></td>
            </tr>
            <tr>
                <td class="ff">Registration Type : </td>
                <td>
                    <%if (tblLoginMaster.getRegistrationType().equals("contractor")) {%>Bidder / Consultant<%}%>
                    <%if (tblLoginMaster.getRegistrationType().equals("individualconsultant")) {%>Individual Consultant<%}%>
                    <%if (tblLoginMaster.getRegistrationType().equals("govtundertaking")) {%> Government owned Enterprise<%}%>
                    <%if (tblLoginMaster.getRegistrationType().equals("media")) {%>Media<%}%>
                </td>
            </tr>
        </table>
        <%
                    TblCompanyMaster tblCompanyMaster = contentAdminService.findCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId")))).get(0);
                    List<TblBiddingPermission> biddingPermission = contentAdminService.findBiddingpermission("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId").toString()), "isReapplied", Operation_enum.EQ, 0);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        %>
        <div class="tableHead_22 t_space">JVCA Details</div>
         <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">
            <tr style="display: none;">
                <td class="ff">Company Registration No. : </td>
                <td><%=tblCompanyMaster.getCompanyRegNumber()%></td>
            </tr>
            <tr>
                <td width="18%" class="ff">Company Name : </td>
                <td width="82%"><%=tblCompanyMaster.getCompanyName()%></td>
            </tr>
            <tr> 
                <td width="18%" class="ff">Procurement Category :</td>
                <td width="82%">
                    <% if (biddingPermission.size() != 0) 
                        {
                            for (int i = 0; i < biddingPermission.size(); i++) {                                        
                                out.println(biddingPermission.get(i).getProcurementCategory().toString());
                                if (biddingPermission.get(i).getWorkType() != null && !biddingPermission.get(i).getWorkType().isEmpty()) {
                                    out.print(", " + biddingPermission.get(i).getWorkType().toString());
                                }
                                if (biddingPermission.get(i).getWorkCategroy() != null && !biddingPermission.get(i).getWorkCategroy().isEmpty()) {
                                    out.print(", " + biddingPermission.get(i).getWorkCategroy().toString());
                                }
                                out.print("<br/>");
                            }
                        }
                    %>
                </td>
            </tr>
            <%--<tr style="display: none;">
                <td class="ff">Company Name in Bangla : </td>
                <td><%if (tblCompanyMaster.getCompanyNameInBangla() != null) {
                                out.print(BanglaNameUtils.getUTFString(tblCompanyMaster.getCompanyNameInBangla()));
                            }%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Company's Legal Status : </td>
                <td>
                    <%if (tblCompanyMaster.getLegalStatus().equals("public")) {%>Public Ltd.<%}%>
                    <%if (tblCompanyMaster.getLegalStatus().equals("private")) {%>Private Ltd.<%}%>
                    <%if (tblCompanyMaster.getLegalStatus().equals("partnership")) {%>Partnership.<%}%>
                    <%if (tblCompanyMaster.getLegalStatus().equals("proprietor")) {%>Proprietor.<%}%>
                    <%if (tblCompanyMaster.getLegalStatus().equals("government")) {%>Government Undertaking.<%}%>
                </td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Company's<br />
                    Establishment Year : </td>
                <td><%=tblCompanyMaster.getEstablishmentYear()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Trade License Issue Date : </td>
                <td><%if (tblCompanyMaster.getLicIssueDate() != null) {
                                out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicIssueDate()));
                            }%>
                </td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Trade License Expiry Date : </td>
                <td><%if (tblCompanyMaster.getLicExpiryDate() != null) {
                                out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicExpiryDate()));
                            }%>
                </td>
            </tr>
            <tr>
                <td class="ff">Tax Payment Number : </td>
                <td>
                    <%if (tblCompanyMaster.getTinNo().equalsIgnoreCase("other")) {
                                    out.print(tblCompanyMaster.getTinDocName().substring(0, tblCompanyMaster.getTinDocName().indexOf("$")) + "<div class=\"formNoteTxt\">(Other Document No.)</div>" + tblCompanyMaster.getTinDocName().substring(tblCompanyMaster.getTinDocName().indexOf("$") + 1, tblCompanyMaster.getTinDocName().length()) + "<div class=\"formNoteTxt\">(Document Description)</div>");
                                } else {
                                    if(tblCompanyMaster.getTinDocName()!=null)
                                    out.print(tblCompanyMaster.getTinDocName());
                                }%>

                </td>
            </tr>
            <tr>
                <td class="ff">Nature of Business : </td>
                <td><%=tblCompanyMaster.getSpecialization()%></td>
            </tr>--%>
            <tr>
                <td class="ff">Address : </td>
                <td><%=tblCompanyMaster.getRegOffAddress()%></td>
            </tr>
            <tr>
                <td class="ff">Country : </td>
                <td><%=tblCompanyMaster.getRegOffCountry()%></td>
            </tr>
            <tr>
                <td class="ff">Dzongkhag / District : </td>
                <td><%=tblCompanyMaster.getRegOffState()%></td>
            </tr>
            
            <tr>
                <td class="ff">Dungkhag/ Sub-District : </td>
                <td><%=tblCompanyMaster.getRegOffSubDistrict()%></td>
            </tr>
            
            <tr id="trRthana">
                <td class="ff">Gewog : </td>
                <td><%=tblCompanyMaster.getRegOffUpjilla()%></td>
            </tr>
            <tr>
                <td class="ff">City / Town : </td>
                <td><%=tblCompanyMaster.getRegOffCity()%></td>
            </tr>
            
            <tr>
                <td class="ff">Post Code : </td>
                <td><%=tblCompanyMaster.getRegOffPostcode()%></td>
            </tr>
            <tr>
                <td class="ff">Mobile No. : </td>
                <td><%if (!tblCompanyMaster.getRegOffMobileNo().equals("")) {
                                out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(), false) + "-" + tblCompanyMaster.getRegOffMobileNo());
                            }%></td>
            </tr>
            <tr>
                <td class="ff">Phone No. : </td>
                <td><%if (!tblCompanyMaster.getRegOffPhoneNo().equals("")) {
                                out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(), false) + "-" + tblCompanyMaster.getRegOffPhoneNo());
                            }%></td>
            </tr>
            <tr>
                <td class="ff">Fax No. : </td>
                <td><%if (!tblCompanyMaster.getRegOffFaxNo().equals("")) {
                                out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(), false) + "-" + tblCompanyMaster.getRegOffFaxNo());
                            }%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Corporate / Head office<br />
                    Address : </td>
                <td><%=tblCompanyMaster.getCorpOffAddress()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Country : </td>
                <td><%=tblCompanyMaster.getCorpOffCountry()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Dzongkhag / District : </td>
                <td><%=tblCompanyMaster.getCorpOffState()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">City / Town : </td>
                <td><%=tblCompanyMaster.getCorpOffCity()%></td>
            </tr>
            <tr id="trCthana" style="display: none;">
                <td class="ff">Gewog : </td>
                <td><%=tblCompanyMaster.getCorpOffUpjilla()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Post Code / Zip Code : </td>
                <td><%=tblCompanyMaster.getCorpOffPostcode()%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Phone No. : </td>
                <td><%if (!tblCompanyMaster.getCorpOffPhoneno().equals("")) {
                                out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(), false) + "-" + tblCompanyMaster.getCorpOffPhoneno());
                            }%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Fax No. : </td>
                <td><%if (!tblCompanyMaster.getCorpOffFaxNo().equals("")) {
                                out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(), false) + "-" + tblCompanyMaster.getCorpOffFaxNo());
                            }%></td>
            </tr>
            <tr style="display: none;">
                <td class="ff">Company's Website : </td>
                <td><%=tblCompanyMaster.getWebsite()%></td>
            </tr>
        </table>
        <%
                    TblTendererMaster tblTendererMaster = contentAdminService.findTblTendererMasters("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId")))).get(0);
                    
        %>
        <div class="tableHead_22 t_space">Contact Person Details</div>
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
            <tr>
                <td width="18%" class="ff">Title : </td>
                <td width="82%"><%=tblTendererMaster.getTitle()%></td>
            </tr>
            <tr>
                <td class="ff">First Name : </td>
                <td><%=tblTendererMaster.getFirstName()%></td>
            </tr>
            <tr>
                <td class="ff">Middle Name : </td>
                <td><%=tblTendererMaster.getMiddleName()%></td>
            </tr>
            <tr>
                <td class="ff">Last Name : </td>
                <td><%=tblTendererMaster.getLastName()%></td>
            </tr>
            <%--<tr>
                <td class="ff">Name in Dzongkha :</td>
                <td><%if (tblTendererMaster.getFullNameInBangla() != null) {
                                        out.print(BanglaNameUtils.getUTFString(tblTendererMaster.getFullNameInBangla()));
                                    }%></td>
            </tr>
            <tr>
                <td class="ff">CID No. : </td>
                <td><%=tblTendererMaster.getNationalIdNo()%>
                </td>
            </tr>--%>
            <%if (!"no".equals(request.getParameter("jv"))) {%>
            <tr>
                <td class="ff">Designation : </td>
                <td><%=tblTendererMaster.getDesignation()%></td>
            </tr>
            <tr>
                <td class="ff">Department : </td>
                <td><%=tblTendererMaster.getDepartment()%></td>
            </tr>
            <%}%>
            <tr>
                <td class="ff">Address : </td>
                <td><%=tblTendererMaster.getAddress1()%></td>
            </tr>
            <%--<tr>
                <td class="ff">Address Line L2 : </td>
                <td><%=tblTendererMaster.getAddress2()%></td>
            </tr>--%>
            <tr>
                <td class="ff">Country : </td>
                <td><%=tblTendererMaster.getCountry()%></td>
            </tr>
            <tr>
                <td class="ff">Dzongkhag / District : </td>
                <td><%=tblTendererMaster.getState()%></td>
            </tr>
            <tr id="trsubdistrict">
                <td class="ff">Dungkhag/ Sub-district : </td>
                <td><%=tblTendererMaster.getSubDistrict()%></td>
            </tr>
            <tr id="trthana">
                <td class="ff">Gewog : </td>
                <td><%=tblTendererMaster.getUpJilla()%></td>
            </tr>
            <tr>
                <td class="ff">City / Town : </td>
                <td><%=tblTendererMaster.getCity()%></td>
            </tr>
            
            <tr>
                <td class="ff">Post Code : </td>
                <td><%=tblTendererMaster.getPostcode()%></td>
            </tr>
            <tr>
                <td class="ff">Mobile No. : </td>
                <td><%=commonService.countryCode(tblTendererMaster.getCountry(), false) + "-" + tblTendererMaster.getMobileNo()%>
                </td>
            </tr>
            <tr>
                <td class="ff">Phone No. : </td>
                <td><%if (!tblTendererMaster.getPhoneNo().equals("")) {
                                        out.print(commonService.countryCode(tblTendererMaster.getCountry(), false) + "-" + tblTendererMaster.getPhoneNo());
                                    }%></td>
            </tr>
            <tr>
                <td class="ff">Fax No. : </td>
                <td><%if (!tblTendererMaster.getFaxNo().equals("")) {
                                        out.print(commonService.countryCode(tblTendererMaster.getCountry(), false) + "-" + tblTendererMaster.getFaxNo());
                                    }%></td>
            </tr>
            
            <%if ("no".equals(request.getParameter("jv"))) {%>
            <tr>
                <td class="ff">Tax Identification Number : </td>
                <td>
                    <%if (tblTendererMaster.getTinNo().equalsIgnoreCase("other")) {
                                     out.print(tblTendererMaster.getTinDocName().substring(0, tblTendererMaster.getTinDocName().indexOf("$")) + "<div class=\"formNoteTxt\">(Other No)</div>" + tblTendererMaster.getTinDocName().substring(tblTendererMaster.getTinDocName().indexOf("$") + 1, tblTendererMaster.getTinDocName().length()) + "<div class=\"formNoteTxt\">(Description)</div>");
                                 } else {
                                     out.print(tblTendererMaster.getTinDocName());
                                 }%>
                </td>
            </tr>
            <tr>
                <td class="ff">Specialization : </td>
                <td>
                    <%=tblTendererMaster.getSpecialization()%>
                </td>
            </tr>
            <tr>
                <td class="ff">Website : </td>
                <td><%=tblTendererMaster.getWebsite()%></td>
            </tr>
            <%}%>
        </table>
                </div>
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
