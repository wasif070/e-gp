<%-- 
    Document   : LotteryResult
    Created on : October 21, 2015, 2:00:00 PM
    Author     : G. M. Rokibul Hasan
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LTM Wining Bidder History</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
   <!--<body onload="getQueryData('postQuery')"> -->
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
<div class="contentArea_1">
      <div class="pageHead_1">Debriefing  on Tender
          <span style="float:right;">
<!--              <a href="<a%=referer%>" class="action-button-goback">Go back</a>-->
          </span>
      </div>
      <%
            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
            String message ="";
            if(request.getParameter("message")!=null)
            {
                message = request.getParameter("message");
            }
            String strTenderId = "";
            if(request.getParameter("tenderId")!=null);
            {
                strTenderId = request.getParameter("tenderId");
            }
      %>
      <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../tenderer/TendererTabPanel.jsp" %>
      <div class="tabPanelArea_1">
      <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="6" />
        <jsp:param name="tenderId" value="<%=strTenderId%>" />
      </jsp:include>
      <table width="100%" cellspacing="0" class="tableList_1">
      <tr>
          <td colspan="3" width="85%">
              <%
                    if("success".equalsIgnoreCase(message))
                    {
              %>
                        <div class="responseMsg successMsg t_space"><span>Your Query Posted Successfully</span></div>
              <%
                    }
                    if("fail".equalsIgnoreCase(message))
                    {
              %>
                        <div class="responseMsg errorMsg t_space"><span>Your Query is not Posted Successfully</span></div>
              <%
                    }
        %>
          </td>

      </tr>
           </table>
          <%
      CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
      //  CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
       //     CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
             // End


      List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();
       bidderRounds = dataMore.getEvalProcessInclude("getLTMwinningBidder",strTenderId,"L1");
       if(bidderRounds.size()>0){%>
       <div>
       <table width="100%" cellspacing="0" class="tableList_1">
           <th class="t-align-center" width="7%">Round</th>
           <th class="t-align-center" width="30%">Winning Bidder</th>
           <th class="t-align-center" width="63%">Post Qualification Status</th>
           <%   int cnt = 1;
                for(SPCommonSearchDataMore round : bidderRounds){%>
           <tr <%if (Math.IEEEremainder(cnt, 2) == 0) {%> class="bgColor-Green" <%}%> >
           <td class="t-align-center" width="7%"> <%=cnt%></td>
           <td class="t-align-center" width="30%"><%=round.getFieldName1()%></td>
           <td class="t-align-center" width="63%"><%=round.getFieldName2()%></td>
           </tr>
           <% cnt++;
            }
         %>
         </table>
          </div>
      <%
            }

              %>
          
           
      </div>
      </div>
      </div>
      <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
</body>

<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
