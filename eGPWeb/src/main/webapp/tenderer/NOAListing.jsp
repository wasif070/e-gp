<%-- 
    Document   : NOAListing
    Created on : Dec 24, 2010, 4:32:51 PM
    Author     : rajesh,rishita
--%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="OfficerTabId" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Notice Preparation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script>

            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&noa=noa");
            }
        </script>
    </head>

    <body>
<%
    boolean isIctTender = false; String tenderId=""; String pkgLotID=""; String roundID="";
    tenderId = request.getParameter("tenderid");

    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> listDP1 = tenderCommonService1.returndata("chkDomesticPreference", tenderId, "");
    System.out.println("**********************************SIZE"+listDP1.size());
    if(!listDP1.isEmpty() && listDP1.get(0).getFieldName1().equalsIgnoreCase("true"))
    {
        isIctTender = true;
    }
    
%>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <%
                List<TblTenderDetails> ttd = OfficerTabId.getProcurementNature(Integer.parseInt(request.getParameter("tenderid")));
                    String procNature = ttd.get(0).getProcurementNature();
                    String procMethod = ttd.get(0).getProcurementMethod();
                    if(!procNature.equalsIgnoreCase("Services")){%>
                    <div class="pageHead_1">Notification  of Award</div>
                    <% }else{ %>
                <div class="pageHead_1">Draft Agreement</div>
                <% } %>
                <%if (request.getParameter("flag") != null && request.getParameter("flag").equals("true")) {%>
                <div class="responseMsg successMsg t_space">Letter of Acceptance (LOA) Action taken successfully</div>
                <%}%>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            pageContext.setAttribute("tab", "8");
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp"%>
                <div>&nbsp;</div>
                <%@include file="TendererTabPanel.jsp"%>
                <%if (!is_debared) {%>
                <div class="tabPanelArea_1">
                    <%
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        String userid = "";
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userid = hs.getAttribute("userId").toString();
                        }

                        List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetPackageDetails", request.getParameter("tenderid"), userid);
                        if (!tenderLotList.isEmpty()) {
                    %>
                    <!--Dashboard Content Part Start-->
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">

                        <tr>
                            <td width="20%">Package No :</td>
                            <td width="80%"><%=tenderLotList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td width="20%">Package Description :</td>
                            <td width="80%"><%=tenderLotList.get(0).getFieldName2()%></td>
                        </tr>

                    </table>

                    <%
                                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                                List<SPTenderCommonData> LotList = tenderCommonService.returndata("GetLotDetails", request.getParameter("tenderid"), userid);
                                                for (SPTenderCommonData lotlists : LotList) {
                                                    //added by Salahuddin
                                                    pkgLotID= lotlists.getFieldName2();
                                                    if (!lotlists.getFieldName2().equalsIgnoreCase("0")) {
                                                        List<SPTenderCommonData> SingleLotList = tenderCommonService.returndata("SingleLotDetails", lotlists.getFieldName2(), null);
                                                        if (!SingleLotList.isEmpty()) {
                                                          //  List<Object> wpid = service.getWpId(Integer.parseInt(lotlists.getFieldName2()));
                                                            /* Dohatec Start */
                                                                  List<Object> wpid ;
                                                                if(isIctTender)
                                                                {
                                                                    wpid = service.getWpIdForTenderForms(Integer.parseInt(lotlists.getFieldName2()),tenderId);
                                                                }
                                                                else
                                                                {
                                                                    wpid = service.getWpId(Integer.parseInt(lotlists.getFieldName2()));
                                                                }
                                                            /* Dohatec End*/
                    %>
                    <%if(!"services".equalsIgnoreCase(procNature)){%>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="20%">Lot No :</td>
                            <td width="80%"><%=SingleLotList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td width="20%">Lot Description :</td>
                            <td width="80%"><%=SingleLotList.get(0).getFieldName2()%></td>
                        </tr>
                    </table>
                    <%}%>

                    
                    

<%

if(procNature.equalsIgnoreCase("services")){
CMSService cmss = (CMSService)AppContext.getSpringBean("CMSService");
//List<Object[]> serviceForms = cmss.getAllFormsForService(Integer.parseInt(request.getParameter("tenderid")));
%>



                    <%}else{

                                         if (!wpid.isEmpty() && wpid != null) {%>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr><td width="20%">Consolidate Forms</td>
                            <td>

 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="2%" class="t-align-center">S.No</th>
                                            <th width="17%" class="t-align-center">Consolidate</th>
                                            <th width="5%" class="t-align-center">Action</th>
                                        </tr>
                                        <%
                                        int i=1;
                                        for (Object obj : wpid) {
                                        String toDisplay = service.getConsolidate(obj.toString());

                                        %>


                                        <tr>
                                            <td style="text-align: center;"><%=i%></td>
                                            <td><%=toDisplay%></td>
                                            <td><a href="#" onclick="view(<%=obj.toString()%>,<%=request.getParameter("tenderid")%>,<%=lotlists.getFieldName2()%>);">View</a>
                                            </td>



                                        </tr>


                                        <%
                                                               i++;
                                                           }
}
                                         }
%>

                                    </table>

                            </td>
                        </tr>
                    </table>

                    <% }%>

                    <div class="tabPanelArea_1 t_space">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="5%" class="t-align-left">Contract  No.</th>
                                <%if(!isIctTender){%>
                                    <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                                    <th width="15%" class="t-align-left">Advance Contract Amount in Figure <br/> (in Nu.)</th>
                                <%}else{%>
                                    <th width="30%" class="t-align-left">Contract Details</th>
                                <%}%>
                                <th width="11%" class="t-align-left">Date of issue of Letter of Acceptance (LOA)</th>
                                <th width="11%" class="t-align-left">Deadline of Acceptance of Letter of Acceptance (LOA)</th>
                                <th width="18%" class="t-align-left">Letter of Acceptance (LOA) Acceptance Status</th>
                                <th width="11%" class="t-align-left">
                                    <%--<%=(procNature.equals("Services") || procMethod.equals("DPM"))? "Accept" : "Accept / Decline"%>--%> Accept Date and Time</th>
                                <th width="11%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                                                                            List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("ContractListingTenderSide", userid, tenderid, lotlists.getFieldName2());
                                                                                            //List<SPTenderCommonData> contractlist = tenderCommonService.returndata("ContractListing", userid, tenderid);
                                                                                            for (SPCommonSearchDataMore contractlists : sPCommonSearchDataMores) {
                                                                                                String temp = contractlists.getFieldName6();
                            %>
                            <tr>
                                <td class="t-align-center"><%=contractlists.getFieldName1()%></td>
                                <%if(!isIctTender){%>
                                <td style="text-align: right;"><%=contractlists.getFieldName2()%></td>
                                <td style="text-align: right;">
                                <%if(contractlists.getFieldName10()!=null){%>
                                    <%--<%=contractlists.getFieldName10()%> (in % )<br/>--%>
                                    <%
                                      BigDecimal advAmtInBDT = new BigDecimal((new BigDecimal(contractlists.getFieldName2()).doubleValue()*new BigDecimal(contractlists.getFieldName10()).doubleValue())/100).setScale(3,0);
                                      out.print(advAmtInBDT);
                                    %>
                                <%}else{%>
                                -
                                <%}%>
                                </td>

                                <%}else{%>
                                <%
                                    for (SPCommonSearchDataMore perSecListing : commonSearchDataMoreService.geteGPData("getDataForPerSec",tenderId,pkgLotID))
                                    {
                                        roundID = perSecListing.getFieldName2();
                                    }
                                %>


                                    <td class="t-align-center"><a onclick="javascript:window.open('../officer/ViewContractAmountNOA.jsp?tenderid=<%=tenderId %>&roundId=<%=roundID%>&pckLotId=<%=pkgLotID%>', '', 'resizable=yes,scrollbars=1','');" href="javascript:void(0);">View</a> </td>
                                
                                <%}%>
                                <td class="t-align-center"><%=contractlists.getFieldName3()%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName9()%></td>
                                <td class="t-align-center"><% if (contractlists.getFieldName6().equalsIgnoreCase("pending")) {%>Pending<% } else if (contractlists.getFieldName6().equalsIgnoreCase("approved")) {%>Accepted<%} else {%>Declined<%}%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName4()%></td>
                                <td class="t-align-center">
                                    <a href="ViewNOA.jsp?tenderid=<%=contractlists.getFieldName7()%>&pcklotid=<%=contractlists.getFieldName8()%>&NOAID=<%=contractlists.getFieldName5()%>&app=no">View</a>
                                    <%if (!temp.equals("pending") || temp.equals("decline")) {%>

                                    <%} else {%>
                                    &nbsp;|&nbsp;
                                    <%--<a href="ViewNOA.jsp?tenderid=<%=contractlists.getFieldName7()%>&pcklotid=<%=contractlists.getFieldName8()%>&NOAID=<%=contractlists.getFieldName5()%>&app=yes"><%=(procNature.equals("Services") || procMethod.equals("DPM"))? "Accept" : "Accept / Decline"%></a>--%>
                                    <a href="ViewNOA.jsp?tenderid=<%=contractlists.getFieldName7()%>&pcklotid=<%=contractlists.getFieldName8()%>&NOAID=<%=contractlists.getFieldName5()%>&app=yes"><%=(procNature.equals("Services") || procMethod.equals("DPM"))? "Accept" : "Accept"%></a>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                    </div>
                    <%} else {%>
                    <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="5%" class="t-align-left">Contract  No.</th>
                            <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                            <th width="11%" class="t-align-left">Date of issue of Letter of Acceptance (LOA)</th>
                            <th width="11%" class="t-align-left">Deadline of Acceptance of Letter of Acceptance (LOA)</th>
                            <th width="18%" class="t-align-left">Letter of Acceptance (LOA) Acceptance Status</th>
                            <th width="11%" class="t-align-left">Accept Date and Time</th>
                            <th width="11%" class="t-align-left">Action</th>
                        </tr>
                        <%                                 //List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("ContractListing", tenderId, "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                           List<SPTenderCommonData> contractlist = tenderCommonService.returndata("ContractListing", userid, null);
                                                           for (SPTenderCommonData contractlists : contractlist) {
                                                               String temp = contractlists.getFieldName6();
                        %>
                        <tr>
                            <td class="t-align-left"><%=contractlists.getFieldName1()%></td>
                            <td class="t-align-left"><%=contractlists.getFieldName2()%></td>
                            <td class="t-align-left"><%=contractlists.getFieldName3()%></td>
                            <td class="t-align-left"><%=contractlists.getFieldName9()%></td>
                            <td class="t-align-center"><% if(contractlists.getFieldName6().equalsIgnoreCase("pending")){%>Pending<% }else if(contractlists.getFieldName6().equalsIgnoreCase("approved")){%>Accepted<%}else {%>Declined<%} %></td>
                            <td class="t-align-center"><%=contractlists.getFieldName4()%></td>
                            <td class="t-align-left">
                                <a href="ViewNOA.jsp?tenderid=<%=contractlists.getFieldName7()%>&NOAID=<%=contractlists.getFieldName5()%>&app=no">View</a>
                                <%if (!temp.equals("pending") || temp.equals("decline")) {%>

                                <%} else {%>
                                &nbsp;|&nbsp;
                                <a href="ViewNOA.jsp?tenderid=<%=contractlists.getFieldName7()%>&NOAID=<%=contractlists.getFieldName5()%>&app=yes">Accept</a>
                                <%}%>
                            </td>
                        </tr>
                        <%}%>
                    </table>
                    </div>
                    <%}
                            }
                        }%>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div><%}%>
            </div>
        </div>
        <!--Dashboard Content Part End-->
        <div>&nbsp;</div>
        <!--Dashboard Footer Start-->
        <%@include file="../resources/common/Bottom.jsp" %>
        <!--Dashboard Footer End-->
</body>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }

</script>

</html>
