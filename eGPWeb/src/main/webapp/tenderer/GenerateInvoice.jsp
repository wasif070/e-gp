<%--
    Document   : GenerateInvoice
    Created on : Aug 5, 2011, 10:57:07 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    pageContext.setAttribute("tab", "14");
                    String prId = "";
                    String tenderId = request.getParameter("tenderId");
                    String wpId = request.getParameter("wpId");
                    if (request.getParameter("prId") != null) {
                        prId = request.getParameter("prId");
                    }
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    service.setLogUserId(session.getAttribute("userId").toString());
                    TenderTablesSrBean beanCommon = new TenderTablesSrBean();
                    String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));

                    ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generate Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            var totalAmount1 = 0;
            var totalAmount2 = 0;
            var totalAmount3 = 0;
            var totalAmount4 = 0;
            var curencyArr = new Array();
            var totalSupply = 0;
            var b=0;
            var totalAmount = 0.00;
            function Calculate(id){

                var count = document.getElementById("listcount").value;


                if(document.getElementById("inv_"+id).checked){
                    // var rate = Math.round(eval(document.getElementById("rate_"+id).value)*Math.pow(10,3))/Math.pow(10,3)
                    totalAmount =   totalAmount + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                    var totalrateofqty = eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value);
                    var a = (Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3))+"";
                    if(a.indexOf(".")<0){
                        a = a+".000";
                    }
                    document.getElementById("totalrateofqty_"+id).innerHTML=a;//Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3)
                }else{
                    totalAmount =   totalAmount - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                    document.getElementById("totalrateofqty_"+id).innerHTML="0.000";
                }
                var b = (Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3))+"";
                if(b.indexOf(".")<0){
                    b = b+".000";
                }
                document.getElementById("totamt").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                document.getElementById("totalamount").value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);

            }
            
            function CalculateICT(id){

                var count = document.getElementById("listcount").value;


                 var curName = document.getElementById("cur_"+id).value;

                 if(jQuery.inArray(curName, curencyArr)<0)
                      curencyArr.push(curName);
                if(document.getElementById("inv_"+id).checked){
                    b=0;
                    // var rate = Math.round(eval(document.getElementById("rate_"+id).value)*Math.pow(10,3))/Math.pow(10,3)
                          //  var index = curencyArr.indexOf(curName);
                            var index = $.inArray(curName, curencyArr); // 2
                                if(index == 0)
                                          totalAmount1 =   totalAmount1 + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                else if(index == 1)
                                          totalAmount2 =   totalAmount2 + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                else if(index == 2)
                                          totalAmount3 =   totalAmount3 + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                else if(index == 3)
                                          totalAmount4 =   totalAmount4 + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                    totalSupply = totalSupply + eval(document.getElementById("sup_"+id).value);
                    var totalrateofqty = eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value);
                    var a = (Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3))+"";
                    if(a.indexOf(".")<0){
                        a = a+".000";
                    }
                    document.getElementById("totalrateofqty_"+id).innerHTML=a;//Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3)

                    var BDTIndex = -1;


                           //var index = curencyArr.indexOf(curName);
                            var index = $.inArray(curName, curencyArr);
                            if(curName == "Taka" || curName == "BTN")
                                {
                                // BDTIndex = curencyArr.indexOf(curName);
                                 BDTIndex = $.inArray(curName, curencyArr);
                                 //BDTCur = curName;
                                }
                            if(index == 0)
                                        {
                                            b = (Math.round(eval(totalAmount1)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }
                                         }
                                else if(index == 1)
                                {
                                 b = (Math.round(eval(totalAmount2)*Math.pow(10,3))/Math.pow(10,3))+"";
                                 if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                }
                               else if(index == 2)
                                {
                                b = (Math.round(eval(totalAmount3)*Math.pow(10,3))/Math.pow(10,3))+"";
                                if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                }
                               else if(index == 3)
                                {
                                b = (Math.round(eval(totalAmount4)*Math.pow(10,3))/Math.pow(10,3))+"";
                                if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                 }
                                if(index == BDTIndex)
                                    {
                                        b = (Math.round(eval(b)+eval(totalSupply)))+"";
                                        if(b.indexOf(".")<0){
                                        b = b+".000";
                                        }
                                        document.getElementById("totamt_BDT").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_BDT").value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);

                                    }
                                else
                                    {
                                        //if(totalSupply.indexOf(".")<0){
                                       // totalSupply = totalSupply+".000";
                                       // }
                                       var c  =0;
                                         var totalBDT = eval(document.getElementById("totalamount_BDT").value);
                                         if(totalBDT == null)
                                             totalBDT = 0;
                                          c = (Math.round((totalBDT + eval(document.getElementById("sup_"+id).value))*Math.pow(10,3))/Math.pow(10,3))+"";

                                        //c = (Math.round(eval(totalSupply)*Math.pow(10,3))/Math.pow(10,3))+"";
                                        if(c.indexOf(".")<0){
                                        c = c+".000";
                                        }


                                        document.getElementById("totamt_"+curName).innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_"+curName).value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);

                                        document.getElementById("totamt_BDT").innerHTML=c;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_BDT").value=c;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);


                       
                                    }
                }else{
                            b=0;
                            var index = $.inArray(curName, curencyArr);
                          // var index = curencyArr.indexOf(curName);
                            if(index == 0)
                                    {
                                       totalAmount1 =   totalAmount1 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                       b = (Math.round(eval(totalAmount1)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }
                                    }
                            else if(index == 1)
                                    {
                                       totalAmount2 =   totalAmount2 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                         b = (Math.round(eval(totalAmount2)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }

                                     }
                            else if(index == 2)
                                    {
                                       totalAmount3 =   totalAmount3 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                         b = (Math.round(eval(totalAmount3)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }

                                    }
                              else if(index == 3)
                                    {
                                      totalAmount4 =   totalAmount4 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                                      b = (Math.round(eval(totalAmount4)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }
                                    }
                            totalSupply = eval(totalSupply) - eval(document.getElementById("sup_"+id).value);
                            document.getElementById("totalrateofqty_"+id).innerHTML="0.000";
                            if(curName != "Taka" && curName != "BTN")
                                {
                                        document.getElementById("totamt_"+curName).innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_"+curName).value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);

                                        b = (Math.round((eval(document.getElementById("totalamount_BDT").value) - eval(document.getElementById("sup_"+id).value))*Math.pow(10,3))/Math.pow(10,3))+"";
                                        if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }
                                        document.getElementById("totamt_BDT").innerHTML=b
                                        document.getElementById("totalamount_BDT").value=b;

                                }
                              else
                                  {
                                        b = (eval(b)+eval(totalSupply))+"";
                                        if(b.indexOf(".")<0){
                                        b = b+".000";
                                        }
                                        document.getElementById("totamt_BDT").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_BDT").value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);

                                  }

                            
                }
                            
           
            }

            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>

        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
            function checkall(){
                var count = document.getElementById("listcount").value;
                if(document.getElementById("allcheck").checked){
                    totalAmount=0;
                }
                for(var i=0;i<count;i++){
                    if(document.getElementById("allcheck").checked){

                        document.getElementById("inv_"+i).checked=true;
                        totalAmount =   totalAmount + (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                        var totalrateofqty = eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value);
                        var a = (Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3))+"";
                        if(a.indexOf(".")<0){
                            a = a+".000";
                        }
                        document.getElementById("totalrateofqty_"+i).innerHTML=a;
                    }else{
                        document.getElementById("inv_"+i).checked=false;
                        //totalAmount =   totalAmount - (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                        document.getElementById("totalrateofqty_"+i).innerHTML="0.000";
                    }
                    if(document.getElementById("allcheck").checked){
                        var b = (Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3))+"";
                        if(b.indexOf(".")<0){
                            b = b+".000";
                        }
                        document.getElementById("totamt").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                        document.getElementById("totalamount").value=b;
                    }

                }
                if(!document.getElementById("allcheck").checked){
                    totalAmount=0;
                    var b = (Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3))+"";
                    if(b.indexOf(".")<0){
                        b = b+".000";
                    }
                    document.getElementById("totamt").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                    document.getElementById("totalamount").value=b;
                }


            }
            
            function checkallICT(){
                var count = document.getElementById("listcount").value;
               
                if(document.getElementById("allcheck").checked){
                    totalAmount=0;
                    totalAmount1=0;
                    totalAmount2=0;
                    totalAmount3=0;
                    totalAmount4=0;
                    totalSupply = 0;
                    //curencyArr = new Array();
                    curencyArr = [];
                     var bdtTotal = 0;
                }
                for(var i=0;i<count;i++){
                    if(document.getElementById("allcheck").checked){
                         
                        var curName = document.getElementById("cur_"+i).value;
                         if(jQuery.inArray(curName, curencyArr)<0)
                                curencyArr.push(curName);
                        document.getElementById("inv_"+i).checked=true;
                         // var index = curencyArr.indexOf(curName);
                           var index = $.inArray(curName, curencyArr);
                                if(index == 0)
                                          totalAmount1 =   totalAmount1 + (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                                else if(index == 1)
                                          totalAmount2 =   totalAmount2 + (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                                else if(index == 2)
                                          totalAmount3 =   totalAmount3 + (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                                else if(index == 3)
                                          totalAmount4 =   totalAmount4 + (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                         totalSupply = totalSupply + eval(document.getElementById("sup_"+i).value);
                        var totalrateofqty = eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value);
                        var a = (Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3))+"";
                        if(a.indexOf(".")<0){
                            a = a+".000";
                        }
                        document.getElementById("totalrateofqty_"+i).innerHTML=a;
                  //  }//else{
                     //   
                        //totalAmount =   totalAmount - (eval(document.getElementById("rate_"+i).value)*eval(document.getElementById("qty_"+i).value));
                    //    
                   // }
                    var BDTIndex = -1;
                            var b = 0;
                           
                            if(curName == "Taka" || curName == "BTN")
                                {
                                 //BDTIndex = curencyArr.indexOf(curName);
                                  BDTIndex= $.inArray(curName, curencyArr);
                                 //BDTCur = curName;
                                }
                                 var index = $.inArray(curName, curencyArr);
                   //   var index = curencyArr.indexOf(curName);
                            if(index == 0)
                                        {
                                            b = (Math.round(eval(totalAmount1)*Math.pow(10,3))/Math.pow(10,3))+"";
                                            if(b.indexOf(".")<0){
                                                b = b+".000";
                                                }
                         
                                        }
                                     // totalAmount1 =   totalAmount1 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                            else if(index == 1)
                                {
                                b = (Math.round(eval(totalAmount2)*Math.pow(10,3))/Math.pow(10,3))+"";
                                if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                }
                                     // totalAmount2 =   totalAmount2 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                            else if(index == 2)
                                {
                                b = (Math.round(eval(totalAmount3)*Math.pow(10,3))/Math.pow(10,3))+"";
                                if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                 }
                                     // totalAmount3 =   totalAmount3 - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                            else if(index == 3)
                                {
                                b = (Math.round(eval(totalAmount4)*Math.pow(10,3))/Math.pow(10,3))+"";
                                if(b.indexOf(".")<0){
                                    b = b+".000";
                                    }
                                }
                                 if(index == BDTIndex)
                                    {
                                        bdtTotal = eval(b);
                                    
                                    }
                               // else
                    
                   //else
                   //{
                                       var c  =0;
                                        c = (Math.round((eval(totalSupply)+eval(bdtTotal))*Math.pow(10,3))/Math.pow(10,3))+"";
                                        if(c.indexOf(".")<0){
                                        c = c+".000";
                                        }
                                        if(curName != "BTN" && curName != "Taka"){
                                        document.getElementById("totamt_"+curName).innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_"+curName).value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        }
                                        document.getElementById("totamt_BDT").innerHTML=c;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                        document.getElementById("totalamount_BDT").value=c;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                               //     }
                
                }
                if(!document.getElementById("allcheck").checked){
                    totalAmount=0;
                    totalAmount1=0;
                    totalAmount2=0;
                    totalAmount3=0;
                    totalAmount4=0;
                    totalSupply = 0;
                    var b = (Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3))+"";
                    document.getElementById("inv_"+i).checked=false;
                    document.getElementById("totalrateofqty_"+i).innerHTML="0.000";
                    if(b.indexOf(".")<0){
                        b = b+".000";
                    }
                    document.getElementById("totamt_BDT").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                    document.getElementById("totalamount_BDT").value=b;

                    for(var k=0;k<curencyArr.length;k++)
                        {
                            if(curencyArr[k] == "BTN" || curencyArr[k] == "Taka")
                                continue;
                            else
                                {
                                 document.getElementById("totamt_"+curencyArr[k]).innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                                 document.getElementById("totalamount_"+curencyArr[k]).value=b;
                                }
                        }
                   }
                }

            }
            function onFire(){
                var checkedCur = new Array();
            
                var count = document.getElementById("listcount").value;
                var flag = true;
                var j = 0;
                if(document.getElementById("cur_0") != null)
                {
                checkedCur.push("BTN");
                for(var i=0;i<count;i++){
                    if(document.getElementById("inv_"+i).checked){
                        flag=false;
                        var curName = document.getElementById("cur_"+i).value;
                        if(jQuery.inArray(curName, checkedCur)<0)
                                checkedCur.push(curName);
                        }
                    }
                }
                else
                    {
                        for(var i=0;i<count;i++){
                            if(document.getElementById("inv_"+i).checked)
                            flag=false;
                            checkedCur.push("BTN");
                        }
                    }
                if(flag){
                    jAlert("Please select at least one item to generate Invoice","Invoice", function(RetVal) {
                    });
                }else{
                    //                    document.frm.action = "<%//=request.getContextPath()%>/InvoiceGenerationServlet";
                    //                    document.frm.method="post";
                    //                    document.frm.submit();
                    for(j=0;j<checkedCur.length;j++)
                        {
                        var input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("name", "curcheck_"+j);
                        input.setAttribute("value", checkedCur[j]);
                        document.getElementById("frm").appendChild(input);
                        }
                    var input2 = document.createElement("input");
                    input2.setAttribute("type", "hidden");
                    input2.setAttribute("name", "checkedCount");
                    input2.setAttribute("value", j);
                    document.getElementById("frm").appendChild(input2);
                    document.forms["frm"].submit();
                }
            }
            function onFireForAdvAmt(){

                    document.forms["frm"].submit();

            }
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Invoice
                <span class="c-alignment-right"><a href="Invoice.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>


            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
                        pageContext.setAttribute("TSCtab", "3");

                        String lotId = "";
                        if (request.getParameter("lotId") != null) {
                            pageContext.setAttribute("lotId", request.getParameter("lotId"));
                            lotId = request.getParameter("lotId");
                        }
                        boolean isWCC = service.isWorkCompleteOrNot(lotId);

            %>
            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%@include  file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">


                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">

                    <%if (isWCC) {%>
                    <div  class='responseMsg noticeMsg t-align-left'>Current Invoice will be considered as a Final Invoice as Work Completion Certificate has been issued by Procuring Entity</div>
                    <%}%>


                    <div align="center">

                        <%


                            boolean InvAmtflag = false;
                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            int i = 0;
                            boolean flags = false;
                            CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                            for (SPCommonSearchDataMore lotList : packageLotList) {

                            flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));

                        %>
                        <form id ="frm" name="frm" action="<%=request.getContextPath()%>/InvoiceGenerationServlet" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%">Lot No.</td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>Lot Description</td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                <%}%>
                            </table>
                            <%if(!"hideinv".equalsIgnoreCase(request.getParameter("inv"))){%>
                            <div id="resultDiv" style="display: block;">
                                <div  id="print_area">

                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                        <%      if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                         %>
                                         <th width="3%" class="t-align-center"><input type="checkbox" name="allcheck" id="allcheck" onclick="checkallICT();"/></th>
                                         <input type="hidden" name="action" id="action" value="submitICT"  />
                                         <% } else{ %>
                                            <th width="3%" class="t-align-center"><input type="checkbox" name="allcheck" id="allcheck" onclick="checkall();" /></th>
                                             <input type="hidden" name="action" id="action" value="submit"  />
                                                <% }if (procnature.equalsIgnoreCase("goods")) {%>

                                            <th width="3%" class="t-align-center">S.No</th>
                                            <th width="20%" class="t-align-center">Description</th>
                                            <th width="10%" class="t-align-center">Unit<br />
                                                of Measurement
                                                <br />
                                            </th>
                                            <th width="10%" class="t-align-center">Qty
                                            </th>
                                            <th width="8%" class="t-align-center">Rate
                                            </th>
                                            <%
                                                    if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                             {
                                        %>
                                        <th width="10%" class="t-align-center">Currency
                                        </th>
                                         <th width="15%" class="t-align-center ICT">Supplier VAT + Other Local Cost (in Nu.)
                                             </th>
                                        <% } %>
                                          
                                            <th width="10%" class="t-align-center">Qty for Invoice
                                            </th>
                                             <%
                                                    if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                             {
                                        %>
                                            <th width="18%" class="t-align-center">Invoiced Amount (currency wise)
                                            </th>
                                             <%
                                                   }else{
                                             %>
                                            <th width="18%" class="t-align-center">Item Invoice Amount (in Nu.)
                                            </th>
                                              <%
                                                   }
                                             %>


                                            <%} else {%>

                                            <th width="3%" class="t-align-center">S.No</th>
                                            <th width="10%" class="t-align-center">Group</th>
                                            <th width="20%" class="t-align-center">Description</th>
                                            <th width="15%" class="t-align-center">Unit<br />
                                                of Measurement
                                                <br />
                                            </th>
                                            <th width="10%" class="t-align-center">Qty
                                            </th>
                                            <th width="18%" class="t-align-center">Rate
                                            </th>
                                            <th width="18%" class="t-align-center">Qty for Invoice
                                            </th>
                                            <th width="25%" class="t-align-center">Item Invoice Amount (in Nu.)
                                            </th>
                                            <%}%>
                                        </tr>
                                        <tbody id="resultTable"></tbody>
                                    </table>

                                </div>
                            </div>
                            <div id="tohide">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                                <option value="1000" selected>10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>

                                            </select>
                                        </td>
                                        <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                            </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <input type="hidden" id="pageNo" value="1"/>
                            <input type="hidden" id="first" value="0"/>
                            <input type="hidden" name="invAmt" value="No">

                            <%}else{ %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                 <%
                                NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                List<Object[]> noalist = null;
                                if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                noalist = noaServiceImpl.getDetailsNOAforInvoiceICT(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                else
                                                noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                             //   Object[] noaObj  = null;
                                //if(!noalist.isEmpty())
                               // {
                                 //   noaObj = noalist.get(0);
                             //   System.out.println("noalist"+noalist.size());
                                int advCount = 0;
                                    for(Object[] noaObj:noalist){
                                    if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                    {
                                    BigDecimal big = BigDecimal.ZERO;
                                     InvAmtflag =true;
                                 //    BigDecimal big = new BigDecimal(c_obj[2].toString()).multiply(new BigDecimal(noaObj[12].toString()));
                                 //    BigDecimal big = new BigDecimal(noaObj[13].toString()).multiply(new BigDecimal(noaObj[12].toString()));
                            %>
                                       <tr>
                                           <%
                                           if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                            %>
                                           <td width='50%'>Advance Amount (In <%=noaObj[14].toString()%>)</td>
                                           
                                        <%  } else { %>
                                           <td width='50%'>Advance Amount (In Nu. )</td>
                                           <%}%>
                                                <%
                                                    if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                    {out.print("<td width='25%'>"+noaObj[12].toString()+"( In %)</td>");}
                                                %>
                                                 <td width="20%">
                                                 <% if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) 
                                                   { %>
                                                   <input type="hidden" name="curcheck_<%=advCount%>" value="<%=noaObj[14].toString()%>"  />
                                                     <%
                                                   big = new BigDecimal(noaObj[13].toString()).multiply(new BigDecimal(noaObj[12].toString())); %>
                                                    <%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,RoundingMode.HALF_UP)%> (In <%=noaObj[14].toString()%>)
                                                    <% }else{
                                                    %>
                                                   <input type="hidden" name="curcheck_<%=advCount%>" value="BTN"  />
                                                     <%
                                                    big = new BigDecimal(c_obj[2].toString()).multiply(new BigDecimal(noaObj[12].toString()));
                                                    %>
                                                    <%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,0)%><br/><%=bdl.getString("CMS.Inv.InBDT")%>
                                                    <% } %>
                                                </td>
                                                <input type="hidden" name="totalamount_<%=advCount%>" value="<%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,RoundingMode.HALF_UP)%>">
                                                <input type="hidden" name="invAmt" value="Yes">
                                                <input type="hidden" name="action" id="action" value="invAmtSubmit"  />
                                                <input type="hidden" id="pageNo" value="1"/>
                                                <input type="hidden" id="first" value="0"/>
                                                <input type="hidden" name="totalAdvCount" id="totalAdvCount" value="<%=noalist.size()%>"  />
                                        </tr>              
                            <%}     advCount = advCount+1;}}%>
                            </table>
                            <br />
                            <div  class='responseMsg noticeMsg t-align-left'>Once the Invoice has been send to PE, it cannot be edited</div>
                            <br />

                            <center>

                                <label class="formBtn_1">
                                    <%if(InvAmtflag){%>
                                    <input type="button" name="Boqbutton" id="Boqbutton" value="Send To PE" onclick="onFireForAdvAmt();" />
                                    <%}else{%>
                                    <input type="button" name="Boqbutton" id="Boqbutton" value="Send To PE" onclick="onFire();" />
                                    <%}%>

                                    <input type="hidden" name="cms" id="cms" value="cms"  />
                                    <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>"  />
                                    <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>"  />
                                    <input type="hidden" name="prId" id="prId" value="<%=request.getParameter("prId")%>"  />                                    
                                    <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>"  />
                                </label>

                            </center>
                        </form>
                    </div>

                </div>
            </div></div></div>

    <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        function Edit(wpId,tenderId,lotId){


            dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

        }
        function view(wpId,tenderId,lotId){
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
    </script>
    <script type="text/javascript">
        /*  load Grid for the User Registration Report */
        function loadTable()
        {
            $('#tohide').hide();
            if($("#keyWord").val() == undefined)
                $("#keyWord").val('');
            $.post("<%=request.getContextPath()%>/InvoiceGenerationServlet", {size: $("#size").val(),pageNo: $("#first").val(),action:'displayItem',wpId: '<%=wpId%>',paymentType:'<%=c_obj[5].toString()%>',lotId:'<%=lotId%>',prId:'<%=prId%>',tenderId:'<%= request.getParameter("tenderId")%>' },  function(j){
                $('#resultTable').append(j.toString());
                if($('#noRecordFound').val() == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
                $('#resultDiv').show();
            });
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
