<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="templateFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TemplateFormSrBean" />
<jsp:useBean id="frmDtl"  class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Form</title>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                int tenderId = 0;
                int sectionId = 0;
                int formId = 0;
                int porlId = -1;
                frmView.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));

                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }
                if (request.getParameter("porlId") != null) {
                    porlId = Integer.parseInt(request.getParameter("porlId"));
                }


                String frmName = "";
                StringBuffer frmHeader = new StringBuffer();
                StringBuffer frmFooter = new StringBuffer();

                List<com.cptu.egp.eps.model.table.TblTenderForms> frm = frmDtl.getFormDetail(formId);

                if (frm != null) {
                    if (frm.size() > 0) {
                        frmName = frm.get(0).getFormName();
                        frmHeader.append(frm.get(0).getFormHeader());
                        frmFooter.append(frm.get(0).getFormFooter());
                    }
                    frm = null;
                }                 
    %>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
            <div class="pageHead_1">
                Form View
                <span style="float: right; text-align: right;">
                    <a href="TenderDocView.jsp?tenderId=<%= tenderId %>&porlId=<%=porlId %>" class="action-button-goback" title="Tender/Proposal Document">Go Back to Tender Document</a>
                </span>
            </div>
            <%
                pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <!--Middle Content Table Start-->
            <% if(frmName != null && !"".equals(frmName.trim())){%>
            <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Name : </td>
                        <td style="font-weight: normal;"><%= frmName %></td>
                    </tr>
                </table>
                    <%}%>
                <% if(frmHeader != null && frmHeader.length() > 0 && !"".equals(frmHeader.toString().trim())){%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Header : </td>
                        <td style="font-weight: normal; line-height: 1.75; "><%= frmHeader %></td>
                    </tr>
                </table>
                    <%}
                
                     String tableHeader = "";
                            String tableFooter = "";
                        List<TblTenderTables> tblTenderTables = frmView.getTenderTables(formId,String.valueOf(tenderId));
                        for(TblTenderTables tbl : tblTenderTables){
                            java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = frmView.getTenderTablesDetail(tbl.getTenderTableId());
                            short cols = 0;
                            short rows = 0;
                            String tableName = "";
                            if (tblInfo != null) {
                                if (tblInfo.size() >= 0) {
                                    tableName = tblInfo.get(0).getTableName();
                                    cols = tblInfo.get(0).getNoOfCols();
                                    rows = tblInfo.get(0).getNoOfRows();
                                    tableHeader = tblInfo.get(0).getTableHeader();
                                    tableFooter = tblInfo.get(0).getTableFooter();
                                }
                                tblInfo = null;
                            }
                            //cols = frmView.getNoOfColsInTable(tbl.getTenderTableId());
                            //rows = frmView.getNoOfRowsInTable(tbl.getTenderTableId(), (short) 1);

                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderColumns> tblColumnsDtl = frmView.getColumnsDtls(tbl.getTenderTableId(), true).listIterator();
                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = frmView.getCellsDtls(tbl.getTenderTableId()).listIterator();

                            String colHeader = "";
                            byte filledBy = 0;
                            byte dataType = 0;
                            int showOrHide=0;
                            String colType = "";

                            short fillBy[] = new short[cols];
                            int arrShowOrHide[]=new int[cols];
                            if (tableName != null && !"".equals(tableName.trim())) {%>
                            <table width="100%" cellspacing="10" class="tableHead_1 t_space">
                                <tr>
                                    <td width="100" class="ff">Table Name : </td>
                                    <td style="font-weight: normal;"><%=tableName%></td>
                                </tr>
                            </table>
<%                      }
                        if (tableHeader != null && !"".equals(tableHeader.trim())) {%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td class="ff">Table Header : </td>
                            <td style="font-weight: normal; line-height: 1.75;"><%= tableHeader%></td>
                        </tr>
                    </table>
                        <%}%>
                            <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
                                <tbody>
                                    <%
                                        String strDtType = "";
                                        for (short i = -1; i <= rows; i++) {
                                            if(i == 0){
                                    %>
                                     <tr id="ColumnRow">
                                    <%
                                                for(short j=0;j<cols;j++){
                                                    if(tblColumnsDtl.hasNext()){
                                                        TblTenderColumns ttc = tblColumnsDtl.next();
                                                        colHeader = ttc.getColumnHeader();
                                                        colType = ttc.getColumnType();
                                                        filledBy = ttc.getFilledBy();
                                                        dataType = ttc.getDataType();
                                                        showOrHide=Integer.parseInt(ttc.getShoworhide());
                                                        ttc = null;
                                                        
                                                    }
                                                    fillBy[j] = filledBy;
                                                    arrShowOrHide[j]=showOrHide;
                                    %>
                                                <th id="addTD<%= j + 1 %>">
                                                    <%= colHeader %>
                                                    <%if(arrShowOrHide[j]==2){%>
                                            <script>
                                                 document.getElementById("addTD<%= j + 1 %>").style.display = "none";
                                            </script>
                                            <%}%>
                                                </th>
                                    <%
                                                }
                                    %>
                                     </tr>
                                    <%
                                            }
                                            
                                            if(i > 0){

                                    %>
                                        <tr id="TR<%=i%>">

                                            <%
                                                int cnt = 0;
                                                short columnId;
                                                int cellId = 0;
                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                for(int j=0; j<cols; j++){
                                                        String cellValue = "";
                                            %>
                                            <td id="TD<%= i %>_<%= j + 1 %>" align="center">
                                                <% if(arrShowOrHide[j]==2){ %>
                    <script>
                        document.getElementById("TD<%=i%>_<%=j+1%>").style.display = "none";
                    </script>
                <%}%>
                                            <%
                                                        if(tblCellsDtl.hasNext()){
                                                            cnt++;
                                                            TblTenderCells cells = tblCellsDtl.next();
                                                            dataType = cells.getCellDatatype();
                                                            filledBy = cells.getCellDatatype();
                                                            cellValue = cells.getCellvalue();
                                                            columnId = cells.getColumnId();
                                                            cellId = cells.getCellId();
                                                            listCellDetail = frmView.getTenderListCellDetail(tbl.getTenderTableId(), columnId, cellId);

                                                            if(dataType == 1){strDtType = "Small Text";}
                                                            if(dataType == 2){strDtType = "Long Text";}
                                                            if(dataType == 3){strDtType = "Money Positive";}
                                                            if(dataType == 4){strDtType = "Numeric";}
                                                            if(dataType == 8){strDtType = "Money All";}
                                                            if (dataType == 9) {strDtType = "Combo Box with Calculation";}
                                                            if (dataType == 10) {strDtType = "Combo Box w/o Calculation";}
                                                            if(dataType == 11){strDtType = "Money All (+5 to +5)";}
                                                            if (dataType == 12) {strDtType = "Date";}
                                                            if (dataType == 13) {strDtType = "Money Positive(3 digits after decimal)";}

                                                            if(fillBy[j] == 2){
                                                                out.print(" Fill By Bidder/Consultant - " + strDtType);
                                                            }else if(fillBy[j] == 1){
                                                                out.print(cellValue);
                                                            }else{
                                                                out.print("Auto");
                                                            }
                                                            cells = null;
                                                        }
                                                        if (dataType == 9 || dataType == 10) {
                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();
                                                if (listCellDetail.size() > 0) {
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();
                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") || tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                        isIctTender = true;
                                                    }

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
%>
                                                    <select id="idcombodetail" name="namecombodetail<%=tblListBoxMaster.getTenderListId()%>" class="formTxtBox_1">
                                                        <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    String selected = "";
                                                                    //if(obj[3].toString().equalsIgnoreCase("BTN")){selected="'selected=selected'";} //BTN is the default currency
                                                                    out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "'>" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
<%
                                                }
                                            }
                                            %>
                                                    </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                    <%
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>
                            
                           <%-- <jsp:include page="ViewTableMatrix.jsp">
                                <jsp:param name="tenderId" value="<%= tenderId %>" />
                                <jsp:param name="sectionId" value="<%= sectionId %>" />
                                <jsp:param name="formId" value="<%= formId %>" />
                                <jsp:param name="tableId" value="<%= tbl.getTableId() %>" />
                            </jsp:include>--%>
                    <%
                            tbl = null;
                            if (tblColumnsDtl != null) {
                                tblColumnsDtl = null;
                            }
                            if (tblCellsDtl != null) {
                                tblCellsDtl = null;
                            }
                        }
                    if (tableFooter != null && !"".equals(tableFooter.trim())) {%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td class="ff">Table Footer : </td>
                            <td style="font-weight: normal; line-height: 1.75;"><%= tableFooter%></td>
                        </tr>
                    </table>
                    <%}
                        if(frmFooter != null && frmFooter.length() > 0 && !"".equals(frmFooter.toString().trim())){%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Form Footer : </td>
                            <td style="font-weight: normal; line-height: 1.75;"><%= frmFooter%></td>
                        </tr>
                    </table>
                 <%}%>
            <!--Middle Content Table End-->
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%
    if (tblTenderTables != null) {
        tblTenderTables = null;
    }
    if (frmView != null) {
        frmView = null;
    }
    if (templateFrmSrBean != null) {
        templateFrmSrBean = null;
    }
    %>
</html>