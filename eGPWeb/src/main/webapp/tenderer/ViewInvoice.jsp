<%--
    Document   : ViewInvoice
    Created on : Aug 9, 2011, 12:33:34 PM
    Author     : shreyansh
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceMaster"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvRemarks"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>

        <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
    </head>
    <div class="dashboard_div">
        <%  CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
            String procCase = commService.getProcNature(request.getParameter("tenderId")).toString();
            String strProcNature = "";
            if("Services".equalsIgnoreCase(procCase))
            {
                strProcNature = "Consultant";
            }else if("goods".equalsIgnoreCase(procCase)){
                strProcNature = "Supplier";
            }else{
                strProcNature = "Contractor";
            }
            /*double strVatPlusAit = 0;
            double strAdvVatPlusAdvAit = 0;
            double netVatandAitStr = 0;*/
           
            String wpId = "";
            if(request.getParameter("wpId")!=null)
            {
                wpId = request.getParameter("wpId");
                pageContext.setAttribute("wpId", request.getParameter("wpId"));
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }
            ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
             pageContext.setAttribute("TSCtab", "3");
             TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
             String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
             List<Object> objInvoiceId = null;

             String invoiceId = "";

             int total = 0;
             int c = 0;
             String[] invoiceIds = new String[4];

             if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
             {
                //invoiceId = request.getParameter("invoiceId");
                invoiceIds[0] = request.getParameter("invoiceId");
                invoiceId = invoiceIds[0];
                total = 1;
                pageContext.setAttribute("invoiceId", request.getParameter("invoiceId"));
             }
             else
             {

                pageContext.setAttribute("invoiceNo", request.getParameter("invoiceNo"));
                objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceNo"), Integer.parseInt(wpId));
                // Dohatec Start
                total = objInvoiceId.size();
                for(c = 0; c < total; c++)
                {
                        invoiceIds[c] = objInvoiceId.get(c).toString();
                }
                // Dohatec End
                invoiceId = invoiceIds[0];
                //invoiceId = objInvoiceId.get(0).toString();
                pageContext.setAttribute("invoiceId", invoiceIds[0]);
             }

            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            accPaymentService.setLogUserId(session.getAttribute("userId").toString());

            //List<TblCmsInvoiceAccDetails> invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceId));

            //  Dohatec Start
          /*  String[] curArr = new String[5];
            String[] totalArr = new String[5];
            String[] cntrAmnt = new String[5];*/
            List<Object[]> totalAmt = null;
            List<TblCmsInvoiceAccDetails> invoicedetails = null;
            List<List<TblCmsInvoiceAccDetails>> invoicedetailsAll = new ArrayList<List<TblCmsInvoiceAccDetails>>();
            /*if(tenderType1.equals("ICT"))
            {*/
                for(c = 0; c < total; c++)
                {
                    invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceIds[c]));
                    invoicedetailsAll.add(c, invoicedetails);
                }
                //invoicedetails = invoicedetailsAll.get(0);

            //}
            /*else
            {
                invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceId));
            }*/
                boolean isEdit = false;
                if (!invoicedetailsAll.isEmpty()) {
                    isEdit = true;
                }
                
            // Dohatec End

            
            boolean HideActionScenario = false;
            boolean createdtenflag = false;
            /*boolean isEdit = false;
            if(!invoicedetails.isEmpty())
            {
                isEdit = true;
            }*/
            List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(invoiceId));
            if(!invstatusObject.isEmpty())
            {
                if("sendtope".equalsIgnoreCase(invstatusObject.get(0).toString()) || "sendtotenderer".equalsIgnoreCase(invstatusObject.get(0).toString()))
                {
                    HideActionScenario = true;
                }
                if("createdbyten".equalsIgnoreCase(invstatusObject.get(0).toString()))
                {
                    createdtenflag = true;
                }
            }
         pageContext.setAttribute("lotId", request.getParameter("lotId"));
         pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
         String conType = commService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
        %>
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div  id="print_area">
        <div class="contentArea_1">
            <div class="pageHead_1"><%=bdl.getString("CMS.Inv.View.Tenderer.Title")%>
                <span style="float: right; text-align: right;" class="noprint">
                <%if(HideActionScenario){%>
                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                <%}%>
                <%
                if("services".equalsIgnoreCase(procCase)){%>
                <a class="action-button-goback" href="InvoiceServiceCase.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                <%}else{%>
                <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
                <a class="action-button-goback" href="Invoice.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                <%}else{%>
                <a href="<%=request.getContextPath()%>/resources/common/ViewTotalInvoice.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>" class="action-button-goback">Go Back</a>
                <%}}%>
            </span>
            </div>
             <%


                %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <%
                        pageContext.setAttribute("tab", "14");

            %>
           <%@include file="TendererTabPanel.jsp"%>
            <%

                                    cs.setLogUserId(session.getAttribute("userId").toString());
                                    String tenderId = request.getParameter("tenderId");
                                    boolean isInvGeneratedOrNot = cs.checkInvGeneratedOrNot(invoiceId);

                                    //  Dohatec Start
                                      List<TblCmsInvoiceMaster> InvMasdata =null;
                                        InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(invoiceIds[0]));
                                    if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                        //totalAmt = cs.getInvoiceTotalAmtForICT(Integer.parseInt(invoiceIds[0]),Integer.parseInt(request.getParameter("wpId")));
                                        totalAmt = cs.returndata("getInvoiceTotalAmtForICT", invoiceIds[0], request.getParameter("wpId"));
                                    else
                            if("no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                               totalAmt =  cs.getInvoiceTotalAmt(invoiceIds[0]);
                           else
                               totalAmt =  cs. getInvoiceTotalAmtAdv(invoiceIds[0]);

                        String[] curArr = new String[totalAmt.size()];
                        String[] totalArr = new String[totalAmt.size()];
                        String[] cntrAmnt = new String[totalAmt.size()];
                        double strVatPlusAit[] = new double[totalAmt.size()];
                        double strAdvVatPlusAdvAit[] = new double[totalAmt.size()];
                        double netVatandAitStr[] = new double[totalAmt.size()];
                                    c = 0;
                                    for(Object[] oob : totalAmt)
                                    {
                                        if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                            curArr[c] = oob[1].toString();
                                            totalArr[c] = oob[0].toString();
                                            cntrAmnt[c] =  oob[3].toString();
                                        }else{
                                            curArr[c] = "BTN";
                                            cntrAmnt[c] =  c_obj[2].toString();
                                        }
                                        c++;
                                    }
                                    //  Dohatec End


                        %>
            <div class="tabPanelArea_1">


                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1 t_space">
                     <% if(request.getParameter("msg") != null){
                     if("gnratinv".equalsIgnoreCase(request.getParameter("msg"))){

    %>
                            <div>&nbsp;</div><div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.Generated.Msg")%></div>
                   <%}
                     if ("crinv".equalsIgnoreCase(request.getParameter("msg")))
                     {
    %>
                            <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.Generated.Msg")%></div>
    <%
                     }
                     if ("InvGenerated".equalsIgnoreCase(request.getParameter("msg"))) {
    %>
                         <div class='responseMsg successMsg'>Invoice Generated Successfully</div>
    <%
                     }
                     }

 %>
                    <div align="center">


                        <form name="frmcons" action="<%=request.getContextPath()%>/InvoiceGenerationServlet"  method="post">
                            <%
                              NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                               List<Object[]> noalist = null;
                               if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                         noalist = noaServiceImpl.getDetailsNOAforInvoiceICT(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                               else
                                         noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                              Object[] noaObj = null;

                              //{noaObj = noalist.get(0);}
                             // List<TblCmsInvoiceMaster> InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(invoiceId));
                              if(!"services".equalsIgnoreCase(procnature)){
                               if(!InvMasdata.isEmpty())
                               {
                                   if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                   {
                         %>
                              <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%
                                      int incr = 0;
                                      if(noalist!=null && !noalist.isEmpty())
                                       for(Object[] noaObjj:noalist){
                            %>
                                <tr>
                                    <td class="ff" width="60%">Advance Amount</td>
                                    <%
                                        if(noaObjj[12]!=null && !"0.000".equalsIgnoreCase(noaObjj[12].toString())){
                                            out.print("<td width='20%'>"+noaObjj[12].toString()+" (In %)</td>");
                                        }
                                        BigDecimal big = BigDecimal.ZERO;
                                        if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                            big = new BigDecimal(noaObjj[13].toString()).multiply(new BigDecimal(noaObjj[12].toString()));
                                        %>
                                    <td width="20%"><%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,RoundingMode.HALF_UP)%> (In <%=noaObjj[14].toString()%>)
                                        <% } else { %>
                                     <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                        <% } %>
                                </tr> <% incr ++ ;} %>
                            </table>
                            <%}else{%>
                            <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                            <%}}}else{
                               if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                               {
                            %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="ff" width="88%">Advance Amount</td>
                                        <%
                                            if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString())){
                                                out.print("<td width='5%'>"+noaObj[12].toString()+"<br/>(In %)</td>");
                                            }
                                        %>
                                        <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    </tr>
                                </table>
                             <%}else{
                                   if("time based".equalsIgnoreCase(contType)){
                             %>
                                            <%
                                                List<Object[]> AttSheetslist = cmss.ViewAttSheetDtls(InvMasdata.get(0).getTillPrid());
                                                String weeks_days[] = new String[7];
                                                MonthName monthName = new MonthName();
                                                BigDecimal TotalAmount = new BigDecimal(0);
                                                if(AttSheetslist!=null && !AttSheetslist.isEmpty()){
                                            %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="3%" class="t-align-left">Sr.No.</th>
                                                <th width="20%" class="t-align-left">Name of Employees</th>
                                                <th width="15%" class="t-align-left">Position Assigned</th>
                                                <th width="9%" class="t-align-left">Home / Field</th>
                                                <th width="5%" class="t-align-left">No. of Days</th>
                                                <th width="5%" class="t-align-left">Rate</th>
                                                <th width="18%" class="t-align-left">Month</th>
                                                <th width="18%" class="t-align-left">Year</th>
                                                <th width="18%" class="t-align-left">Invoice Amount</th>
                                            </tr>
                                            <%
                                                for(Object obj[] : AttSheetslist){
                                                TotalAmount = TotalAmount.add((BigDecimal)obj[10]) ;
                                            %>
                                            <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%></td>
                                                <td width="9%" class="t-align-left"><%=obj[7]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[3]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[4]%></td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                <td width="5%" style="text-align: right"><%=obj[6]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[10]%></td>
                                           </tr>
                                               <%}%>
                                           <tr><td class="t-align-left" colspan=7></td>
                                               <td class="t-align-left ff">Total Amount (In Nu.)</td>
                                               <td class="t-align-right ff" style="text-align: right;"><%=TotalAmount%></td>
                                           </tr>
                                            </table>
                                           <%}%>
                              <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                              <%}else{%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>
                                <th>Milestone Name </th>
                                <th>Description</th>
                                <th>Payment as % of Contract Value</th>
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>
                                <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                <%
                                    List<TblCmsSrvPaymentSch> listPaymentData = cmss.getPaymentScheduleDatabySrvPSId(InvMasdata.get(0).getTillPrid());
                                    if(!listPaymentData.isEmpty()){
                                %>
                                <tr>
                                    <td class="t-align-left"><%=listPaymentData.get(0).getSrNo()%></td>
                                    <td class="t-align-left"><%=listPaymentData.get(0).getMilestone()%></td>
                                    <td class="t-align-left"><%=listPaymentData.get(0).getDescription()%></td>
                                    <td class="t-align-left"><%=listPaymentData.get(0).getPercentOfCtrVal()%></td>
                                    <td class="t-align-left"><%=DateUtils.customDateFormate(listPaymentData.get(0).getPeenddate())%></td>
                                    <td class="t-align-left"><%=DateUtils.customDateFormate(listPaymentData.get(0).getEndDate())%></td>
                                    <td><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*listPaymentData.get(0).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                </tr>
                            </table>
                            <%}}}
                               if(!InvMasdata.isEmpty())
                               {
                                   if(InvMasdata.get(0).getConRemarks()!=null)
                                   {
                                       if("No".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                       {
                            %>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr><td width="20%" class="ff">Remarks by Consultant</td>
                                    <td>
                                        <%
                                            out.print(InvMasdata.get(0).getConRemarks());
                                        %>
                                    </td>
                                </tr>
                            </table>
                            <%}}}
                                ConsolodateService servicee = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                MakeAuditTrailService makeAuditTrailServicee = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                int conIdd = servicee.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                                makeAuditTrailServicee.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()),conIdd, "contractId", EgpModule.Payment.getName(), "View Invoice", "");
                            }%>
                             <%if("rejected".equalsIgnoreCase(invstatusObject.get(0).toString()) || "acceptedbype".equalsIgnoreCase(invstatusObject.get(0).toString())){
                                       List<Object> inv = accPaymentService.getInvoiceRemarks(Integer.parseInt(invoiceId));
    %>                                  <br />
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr><td class="t_align_left ff" width="15%">
                                              Remarks By PE
                                            </td>
                                            <td><%=inv.get(0)%></td>
                                        </tr>
                                    </table>
                                    <%}%>
                            <%if(HideActionScenario){
                        %>
                        <table cellspacing="0" class="tableList_1 t_space" width="100%">

                            <%
                                    if(isEdit){
                                            for(c = 0; c < total; c++)
                                            {
                            %>
                                    <tr>
                                            <td class="ff" width="25%" class="t-align-left">Invoice Amount (In <%=curArr[c].toString()%>) : </td>
                                            <td class="t-align-left"><%=invoicedetailsAll.get(c).get(0).getInvoiceAmt()%>
                                                    <input type="hidden" id="invoiceAmount_<%=c%>" name ="invoiceAmount" <%if(isEdit) {%>value ="<%=invoicedetailsAll.get(c).get(0).getInvoiceAmt()%>"<%}%>>
                                                    &nbsp;<span id="invoiceAmountWrd_<%=c%>" class="ff"></span>
                                            </td>
                                    </tr>
                            <%}}%>


                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.deduction")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advAmount")%></td>
                                <td   class="t-align-left">
                                <%
                                    double strAdvanceAmount = 0;
                                    String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(request.getParameter("lotId")));
                                    String salvageAmountStr = accPaymentService.getSalavageContractAmount(Integer.parseInt(tenderId), Integer.parseInt(lotId));
                                    BigDecimal salvageAmount=new BigDecimal(salvageAmountStr).setScale(3,0);
                                    if(!"".equalsIgnoreCase(AdvanceAmount))
                                    {   strAdvanceAmount = new BigDecimal(AdvanceAmount.toString()).doubleValue();
                                    }else{strAdvanceAmount = new BigDecimal(0).doubleValue();}
                                    BigDecimal bigadvadjAmt= new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0);
                                %>
                                    <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvanceAmount).setScale(3,0)%></td>
                                            <td style="text-align: right;border:none;padding:0px;">
                                                <%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>
                                                    <%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%>
                                                <%} else {
                                                    for(c=0; c < total; c++){
                                                %>
                                                        <%=new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                <%}}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAmount")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvAdjAmt()%><%}%></td>
                                            <td style="text-align: right;border:none;padding:0px;">
                                                    <%if(isEdit){
                                                         if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){ %>
                                                            <%=new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(0).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[0].toString() +") <br/>"%>
                                                        <%}else {
                                                            for(c = 0; c < total; c++) {
                                                                bigadvadjAmt= new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP);
                                                        %>
                                                        <%=new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>

                                                    <%}}}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                             <% if(salvageAmount.compareTo(BigDecimal.ZERO)>0 && !tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes") ){ %>

                            <tr>
                                 <td class="ff" width="25%">Total Salvage Amount:</td>
                                <td>

                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <%= salvageAmount %> <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    </tr>
                                 </table>
                                </td>
                                <td class="ff" width="25%"> Salvage Adjustment :</td>
                                <td class="t-align-left">
                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"><span id="SalvageAdjAmtPercentage0"><%= invoicedetailsAll.get(0).get(0).getSalvageAdjAmt() %></span></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <span id="SalvageAdjAmtinbdt0"><%= invoicedetailsAll.get(0).get(0).getSalvageAdjAmt().multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %><%=bdl.getString("CMS.Inv.InBDT")%></span></td>
                                    </tr>
                                 </table>


                                </td>
                            </tr>
                            <% } %>



                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VAT")%>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                       <td <%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>style="text-align: left;border:none;padding:0px;"<%}else{%>style="text-align: right;border:none;padding:0px;"<%}%>><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatAmt().toString() + " (In " + curArr[0].toString() + ")"%><%}%></td>
                                        <%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>
                                            <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                        <%}%>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjVAT")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvVatAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++) {
                                            %>
                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.AIT")%>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                                    if((tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) && curArr[c].equals("BTN") ){
                                            %>
                                                        <%=new BigDecimal(((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue() - invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue())*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                    <%}else{%>
                                                        <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAIT")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                            %>
                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                 <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VATAIT")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <%
                                        if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                            if (isEdit) {
                                                if (invoicedetailsAll.get(0).get(0).getVatAmt() != null && invoicedetailsAll.get(0).get(0).getAitAmt() != null) {
                                                    strVatPlusAit[0] = (invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue() + invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue());
                                                } else if (invoicedetailsAll.get(0).get(0).getVatAmt() != null && invoicedetailsAll.get(0).get(0).getAitAmt() == null) {
                                                    strVatPlusAit[0] = invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue();
                                                } else if (invoicedetailsAll.get(0).get(0).getVatAmt() == null && invoicedetailsAll.get(0).get(0).getAitAmt() != null) {
                                                    strVatPlusAit[0] = invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue();
                                                }
                                            }
                                        %>

                                                <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strVatPlusAit[0]).setScale(3,0)%></td>
                                                <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*strVatPlusAit[0])/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>

                                        <%}else{%>
                                            <td style="text-align: left;border:none;padding:0px;"></td>
                                                <td style="text-align: right;border:none;padding:0px;">
                                                    <%if(isEdit){
                                                        for(c = 0; c < total; c++){
                                                            if(curArr[c].equals("BTN") || curArr[c].equals("Taka")){
                                                                strVatPlusAit[c] = invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue() + (((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()- invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue())*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100);
                                                    %>
                                                                <%=new BigDecimal(strVatPlusAit[c]).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                            <%}else{
                                                                strVatPlusAit[c] = (invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100;
                                                            %>
                                                                <%=new BigDecimal(strVatPlusAit[c]).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                    <%}}}%>
                                                </td>
                                        <%}%>
                                    </tr>
                                    </table>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.advVATAIT")%>
                                </td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">

                                    <%
                                        if (isEdit) {
                                            for(c = 0; c< total; c++){
                                            if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() != null && invoicedetailsAll.get(c).get(0).getAdvAitAmt() != null) {
                                                strAdvVatPlusAdvAit[c] = (invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue() + invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue());
                                            } else if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() != null && invoicedetailsAll.get(c).get(0).getAdvAitAmt() == null) {
                                                strAdvVatPlusAdvAit[c] = invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue();
                                            } else if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() == null && invoicedetailsAll.get(c).get(0).getAitAmt() != null) {
                                                strAdvVatPlusAdvAit[c] = invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue();
                                            }
                                    %>
                                    <tr>
                                        <%if(c < 1){%>
                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvVatPlusAdvAit[0]).setScale(3,0)%></td>
                                        <%}else{%>
                                            <td style="text-align: left;border:none;padding:0px;"></td>
                                        <%}%>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*strAdvVatPlusAdvAit[0])/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%><%}%></td>
                                    </tr>
                                    <%}}%>
                                    </table>
                                </td>
                            </tr>
                            <!-- Dohatec Start -->
                            <tr style="display:none;">
                                <td class="ff"><%=bdl.getString("CMS.Inv.netVATAIT")%></td>
                                <td class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                    <%
                                        if (isEdit) {
                                            for(c = 0; c < total; c++){
                                                netVatandAitStr[c] = strVatPlusAit[c] - strAdvVatPlusAdvAit[c];

                                    %>

                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(netVatandAitStr[c]).setScale(3,0)%></td>
                                            <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*netVatandAitStr[c])/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%><%}%></td>
                                        <%}}%>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff"></td>
                                <td class="ff"></td>
                            </tr>
                            <!-- Dohatec End -->
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.liquditydamage")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getldAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                            %>
                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getldAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.penalty")%>
                                </td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getPenaltyAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                            %>
                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getPenaltyAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.addition")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left">Bonus :</td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getBonusAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                            %>
                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getBonusAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="26%"  class="t-align-left"><%=bdl.getString("CMS.Inv.InterestOnDP")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit && invoicedetailsAll.get(0).get(0).getInterestonDP()!=null){%><%=invoicedetailsAll.get(0).get(0).getInterestonDP()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                            <%if(isEdit && invoicedetailsAll.get(0).get(0).getInterestonDP()!=null){%>
                                                <%if(isEdit){
                                                for(c = 0; c < total; c++){
                                            %>
                                                    <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getInterestonDP().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                            <%}}}%>
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.retention")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoney")%></td>
                                <td   class="t-align-left">
                                        <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){if(invoicedetailsAll.get(0).get(0).getRetentionMadd()!=null){%><%=invoicedetailsAll.get(0).get(0).getRetentionMadd()%>&nbsp;<span style="color: #f00;">(Added)</span><%}else if(invoicedetailsAll.get(0).get(0).getRetentionMdeduct()!=null){%><%=invoicedetailsAll.get(0).get(0).getRetentionMdeduct()%>&nbsp;<span style="color: #f00;">(Deducted)</span><%}}%></td>
                                            <td style="text-align: right;border:none;padding:0px;">
                                                <%if(isEdit){
                                                    for(c = 0; c < total; c++){
                                                        if(invoicedetailsAll.get(c).get(0).getRetentionMadd()!=null){
                                                            out.print(new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMadd().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>");
                                                        }
                                                        else if(invoicedetailsAll.get(c).get(0).getRetentionMdeduct()!=null){
                                                            out.print(new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMdeduct().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>");
                                                        }
                                                    }
                                                }%></td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr> <td colspan="2"><ol style="padding-right: 5px;margin-left: 20px;"><li>Retention money at a percentage as specified in Schedule II will be
                                         deductable from each bill due to a Contractor until completion of the whole Works or delivery.</li>
                                     <li>On completion of the whole Works, half the total amount retained shall be repaid to
                                         the Contractor and the remaining amount may also be paid to the Contractor if an unconditional
                                         Financial Institute guarantee is furnished for that remaining amount.</li>
                                     <li >The remaining amount or the Financial Institute guarantee, under Sub-Rule (2), shall be returned,
                                         within the period specified in schedule II , after issuance of all Defects Correction Certificate
                                         under Rule 39(29) by the Project Manager or any other appropriate Authority ..</li>
                                     <li >Deduction of retention money shall not be applied to small Works Contracts if no
                                         advance payment has been made to the Contractor and in such case the provisions of Sub-Rule
                                         (7) and (8) of Rule 27 shall be applied.</li></ol>
                                 </td><td></td>
                            </tr>
                        </table>
                                <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails")%><%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>&nbsp;&nbsp;<%=invoicedetailsAll.get(0).get(0).getGrossAmt()+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getModeOfPayment()%><%}%>
                                <span id="modeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getDateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getBankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getBranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getInstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%
                            double NetVat = 0;
                            if(invoicedetailsAll.get(0).get(0).getVatAmt()!=null)
                            {
                                if(invoicedetailsAll.get(0).get(0).getAdvVatAmt()!=null)
                                {
                                    NetVat = (((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue())/100) - ((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAdvVatAmt().doubleValue())/100)) ;
                                }
                            }
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.vat")%><%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>&nbsp;&nbsp;<%=new BigDecimal(NetVat).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <%if(invoicedetailsAll.get(0).get(0).getVatdateOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatmodeOfPayment()%><%}%>
                                <span id="vatmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getVatdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <%
                            double NetAit = 0;
                            if(invoicedetailsAll.get(0).get(0).getVatAmt()!=null)
                            {
                                if(invoicedetailsAll.get(0).get(0).getAdvVatAmt()!=null)
                                {
                                    NetAit = (((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue())/100) - ((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAdvAitAmt().doubleValue())/100)) ;
                                }
                            }
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.ait")%><%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>&nbsp;&nbsp;<%=new BigDecimal(NetAit).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <%if(invoicedetailsAll.get(0).get(0).getAitdateOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitmodeOfPayment()%><%}%>
                                <span id="aitmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getAitdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <!--<div class="tableHead_1 t_space" align="left"><%--<%=bdl.getString("CMS.Inv.gross")%>--%></div>-->
                        <div class="tableHead_1 t_space" align="left">Net AMOUNT</div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                        <%for(c = 0; c < total; c++){%>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left">Net Amount <%="(In "+ curArr[c].toString() +")"%></td>
                                <td   class="t-align-left" style="text-align: right"><%if (isEdit) {%><%=invoicedetailsAll.get(c).get(0).getGrossAmt()%><%}%>
                                    <input type="hidden" id="grosshiddenamt_<%=c%>" name ="netAmount" <%if(isEdit) {%>value ="<%=invoicedetailsAll.get(c).get(0).getGrossAmt()%>"<%}%>>
                                    &nbsp;<span id="grossinWords_<%=c%>" class="ff"></span>
                                </td>
                            </tr>
                        <%}%>
                        </table>


                        <%
                             List<TblCmsInvRemarks> getRemarks = accPaymentService.getRemarksDetails(Integer.parseInt(invoiceId));
                             if(!getRemarks.isEmpty())
                             {
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.Remarks.ViewCaps")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                        <%
                                 for(int i=0; i<getRemarks.size(); i++)
                                 {
                        %>
                                <tr>
                                <td class="t-align-left" width="15%">
                                    <%=(i+1)%>
                                </td>
                                <td>
                                    <%=getRemarks.get(i).getRemarks()%>
                                </td>
                                </tr>
                           <%}}%>
                        </table>
                        <%}%>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td class="t-align-left ff" width="15%">
                                    <%if(createdtenflag){%>
                                    <%=bdl.getString("CMS.Inv.Upload")%>
                                    <%}else{%>
                                    <%=bdl.getString("CMS.Inv.Download")%>
                                    <%}%>
                                </td>
                                <td>
                                    <%if(createdtenflag){%>
                                      <%if(!tenderType1.equals("ICT") && !tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>
                                                    <a class="noprint" href="TendererPaymentDocUpload.jsp?tenderId=<%=request.getParameter("tenderId")%>&invoiceId=<%=request.getParameter("invoiceId")%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Upload</a>
                                                <%}else{%>
                                                    <a class="noprint" href="TendererPaymentDocUpload.jsp?tenderId=<%=request.getParameter("tenderId")%>&invoiceNo=<%=request.getParameter("invoiceNo")%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Upload</a>
                                                <%}%>
                                     <%}%>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                        <th width="8%" class="t-align-left"><%=bdl.getString("CMS.Srno")%></th>
                                        <th class="t-align-left" width="23%"><%=bdl.getString("CMS.Inv.FileName")%></th>
                                        <th class="t-align-left" width="28%"><%=bdl.getString("CMS.Inv.FileDescription")%></th>
                                        <th class="t-align-left" width="9%"><%=bdl.getString("CMS.Inv.FileSize")%><br />
                                            <%=bdl.getString("CMS.Inv.inKB")%></th>
                                        <th class="t-align-left" width="">Uploaded By</th>
                                        <th class="t-align-left" width="18%"><%=bdl.getString("CMS.action")%></th>
                                        </tr>
                                        <%
                                            String userId = "1";
                                            if (session.getAttribute("userId") != null) {
                                                userId = session.getAttribute("userId").toString();//Integer.parseInt(InvoiceId)
                                            }
                                            List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetails(Integer.parseInt(invoiceId));
                                            if(!getInvoiceDocData.isEmpty())
                                            {
                                                for(int i =0; i<getInvoiceDocData.size(); i++)
                                                {
                                        %>
                                            <tr>
                                                <td class="t-align-left"><%=(i+1)%></td>
                                                <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocumentName()%></td>
                                                <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocDescription()%></td>
                                                <td class="t-align-left"><%=(Long.parseLong(getInvoiceDocData.get(i).getDocSize())/1024)%></td>
                                                <td class="t-align-left">
                                                    <%String str = "";
                                                    if (2 == getInvoiceDocData.get(i).getUserTypeId()) {
                                                        str = "supplier";
                                                        out.print(strProcNature);
                                                    } else {
                                                        if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(Integer.toString(getInvoiceDocData.get(i).getUploadedBy()))))
                                                        {
                                                            str = "accountant";
                                                            out.print("Account Officer");
                                                        }else{
                                                            str = "PE";
                                                            out.print("PE Officer");
                                                        }

                                                    }%>
                                                </td>
                                                <td class="t-align-left">
                                                    <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&accountant=<%if(3==getInvoiceDocData.get(i).getUserTypeId()){%>yes<%}%>&str=<%=str%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                    &nbsp;
                                                    <%--<%if(isInvGeneratedOrNot){%>
                                                    <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?&docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&str=<%=str%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                    <%}%>--%>
                                                </td>
                                            </tr>
                                             <%}
                                            }else{%>
                                            <tr>
                                                <td colspan="6" class="t-align-left"><%=bdl.getString("CMS.Inv.NoRecord")%></td>
                                            </tr>
                                            <%}%>
                                    </table>
                                </td>
                            </tr>
                        </table>

                                <br /><input type="hidden" name="action" id="action" value="generate"  />
                                <input type="hidden" name="lotId" id="action" value="<%=request.getParameter("lotId")%>"  />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>"  />
                                <% if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                    %>
                                    <input type="hidden" name="invId" id="invId" value="<%=request.getParameter("invoiceNo")%>" />
                                    <% }else{ %>
                                <input type="hidden" name="invId" id="invId" value="<%=invoiceId%>"  />
                                <% }%>
                                  <%if(isInvGeneratedOrNot){%>
                        <label class="formBtn_1">

                                <input type="submit" name="Boqbutton" id="Boqbutton" value="Generate Invoice" />
                                 </label>
                                <%}%>


                        </form>

                    </div></div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        //document.getElementById("grossinWords").innerHTML = '('+DoIt(document.getElementById("grosshiddenamt").value)+')';

        // Dohatec Start
        AmntInWord();

        function AmntInWord()
        {
            var totalNetAmt = document.getElementsByName("netAmount").length;
            for(var i = 0; i < totalNetAmt; i++)
            {
                //document.getElementById("grossinWords_"+i).innerHTML = '('+(DoIt(document.getElementById("grosshiddenamt_"+i).value))+')';
                //Numeric to word currency conversion by Emtaz on 20/April/2016
                document.getElementById("grossinWords_"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("grosshiddenamt_"+i).value))+')';
            }

            var totalInvoice = document.getElementsByName("invoiceAmount").length;
            for(var i = 0; i < totalNetAmt; i++)
            {
                document.getElementById("invoiceAmountWrd_"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("invoiceAmount_"+i).value))+')';
            }
        }
        // Dohatec End

    </script>

</html>
