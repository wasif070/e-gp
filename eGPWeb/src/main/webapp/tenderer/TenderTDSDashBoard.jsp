<%--
    Document   : TDSDashBoard
    Created on : Nov 10, 2010, 7:57:34 PM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />

<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            int tenderId = Integer.parseInt(request.getParameter("tenderId"));
            int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));


            List<com.cptu.egp.eps.model.table.TblTenderIttHeader> subSectionDetail = createSubSectionSrBean.getSubSection_TDSAppl(sectionId);
            
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            if(contentType.equalsIgnoreCase("ITT")){
                contentType = "BDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType = "SCC";
            }

            int packageOrLotId = -1;
            if (request.getParameter("porlId") != null) {
                packageOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=contentType%> Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td>
                                        <a href="TenderDocView.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%=packageOrLotId%>" class="action-button-goback">Go Back to Tender Document</a>
                                    </td>
                                </tr>
                            </table>
                            <div class="t_space">
                                <div class="pageHead_1"><%=contentType%> Dashboard</div>
                            </div>
                            <form action="DefineSTDInDtl.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                for(int i=0;i<subSectionDetail.size();i++){
                                %>
                                    <tr>
                                        <td align="center" width="30%"><b>Sub Section No.</b></td>
                                        <td width="70%"><%=(i+1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name Of Sub Section</b></td>
                                        <td><%=subSectionDetail.get(i).getIttHeaderName() %></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td>
                                            <%
                                            if(!createSubSectionSrBean.isTdsSubClauseGenerated(subSectionDetail.get(i).getTenderIttHeaderId())){
                                            %>
                                                <%--<a href="#">Create</a>--%>
                                            <%
                                            } else {
                                            %>
                                                <%--<a href="<%=request.getContextPath()%>/officer/PrepareTenderTDS.jsp?ittHeaderId=<%=subSectionDetail.get(i).getTenderIttHeaderId()%>&sectionId=<%=sectionId%>&tenderId=<%=request.getParameter("tenderId")%>&tenderStdId=<%=tenderStdId%>&edit=true">Edit</a> |--%>
                                                <a href="<%=request.getContextPath()%>/tenderer/ViewTenderTDS.jsp?ittHeaderId=<%=subSectionDetail.get(i).getTenderIttHeaderId()%>&sectionId=<%=sectionId%>&tenderId=<%=request.getParameter("tenderId")%>&tenderStdId=<%=tenderStdId%>&porlId=<%=packageOrLotId%>">View</a>
                                            <%
                                            }
                                            %>


                                        </td>
                                    </tr>
                                <%
                                }
                                subSectionDetail=null;
                                createSubSectionSrBean=null;
                                %>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
