<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                String strUserTypeId = "";
                Object objUserId = session.getAttribute("userId");
                Object objUName = session.getAttribute("userName");
                boolean isLoggedIn = false;
                if (objUserId != null) {
                    strUserTypeId = session.getAttribute("userTypeId").toString();
                }
                if (objUName != null) {
                    isLoggedIn = true;
                }
                String action = request.getParameter("contentType");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <%

                    if (action.equalsIgnoreCase("press")) {
        %>
        <title>Press Releases</title>
        <% } else if (action.equalsIgnoreCase("complaint")) {
        %>
        <title>Complaint Resolution</title>

        <%} else {%>
        <title>Other Information</title>
        <%}

        %>


        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            function getDataOnEnter(){
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();

                        }
                    }
            }
            $(function() {
                $('#btnReset').click(function() {
                    $("#pageNo").val("1");
                    $("#keyword").val('');
                   $('span.#mailMsg').html('');
                    $("#textfield5").val('');
                    $.post("../GetPressRelease", {funName: "AllTopics",keyword:$("#keyword").val(),textfield6:$("#textfield6").val(),hdn_action:$("#action").val() ,viewType:$("#displayaction").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){

                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        if($('#noRecordFound').val() == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                });
            });
        </script>


        <script type="text/javascript">
            function loadTable()
            {
                $.post("../GetPressRelease", {funName: "AllTopics",keyword:$("#keyword").val(),textfield6:$("#textfield6").val(),hdn_action:$("#action").val() ,viewType:$("#displayaction").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">

            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {

                $('#btnSearch').click(function() {
                    $("#pageNo").val("1");
                    if($.trim($("#keyword").val()).length==0 && $.trim($("#textfield5").val()).length==0)
                    {
                        $('span.#mailMsg').css("color","red");
                        $('span.#mailMsg').html("Please enter any search criteria");
                    }else{

                        $('span.#mailMsg').html("");
                        $.post("../GetPressRelease", {funName: "AllTopics",keyword:$("#keyword").val(),textfield5:$("#textfield5").val(),hdn_action:$("#action").val() ,viewType:$("#displayaction").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){

                            $('#resultTable').find("tr:gt(0)").remove();

                            $('#resultTable tr:last').after(j);
                            if($('#noRecordFound').val() == "noRecordFound"){
                                $('#pagination').hide();
                            }else{
                                $('#pagination').show();
                            }
                            chkdisble($("#pageNo").val());
                            if($("#totalPages").val() == 1){
                                $('#btnNext').attr("disabled", "true");
                                $('#btnLast').attr("disabled", "true");
                            }else{
                                $('#btnNext').removeAttr("disabled");
                                $('#btnLast').removeAttr("disabled");
                            }
                            $("#pageTot").html($("#totalPages").val());
                            $("#pageNoTot").html($("#pageNo").val());
                            $('#resultDiv').show();
                        }

                    )
                    };
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");


                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>1)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);

                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {

                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);

                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);

                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();

                        }
                    }
                });
            });
        </script>
    </head>
    <body onload="loadTable();">

            <div class="dashboard_div">
                <!--Dashboard Header Start-->
            
                <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %> 

                  
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                        <%if (strUserTypeId.equals("1")) {%>
                        <td>
                            <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        </td>
                        <%}%>
                            <td class="contentArea">
                                <div class="formBg_1 t_space">
                                <form id="alltenderFrm" method="post">
                                    <input type="hidden" value="searchMediaContent" name="hdn_action" id="action" />
                                    <input type="hidden" value=<%=action%> name="displayaction" id="displayaction" />
                                    <table align="center" cellspacing="10" class="formStyle_1" width="100%">
                                        <tr>
                                            <td width="12%" class="ff">Subject :</td>
                                            <td width="35%"><input type="text" class="formTxtBox_1" name ="keyword" id="keyword" style="width:200px;" /></td>
                                            <td width="20%" class="ff">Date of Posting : </td>
                                            <td><input name="textfield6" type="text" class="formTxtBox_1"  name="textfield5" id="textfield5" style="width:100px;" readonly="readonly" onClick="GetCal('textfield5','textfield5');" />
                                                <a href="javascript:void(0);" onclick="" title="Calender"><img src="../resources/images/Dashboard/calendarIcn.png" id="calendarIcn" name ="calendarIcn" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('textfield5','calendarIcn');" /></a></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                       
                                        <tr>
                                            <td colspan="4" class="t-align-center ff">
                                                <span class="formBtn_1">
                                                    <input type="button" name="btnSearch" id="btnSearch" value="Search" /></span>
                                                <span class="formBtn_1">
                                                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
                                                </span></td>
                                        </tr>
                                         <tr>
                                             <td colspan="4" class="t-align-center"><span id="mailMsg" style="color: red; ">&nbsp;</span></td>
                                        </tr>
                                    </table>
                                </form>
                                </div>
                                <br/>
                                <%

                                            if (action.equalsIgnoreCase("press")) {
                                %>
                                <div class="pageHead_1"> List of Press Releases </div>
                                <% } else if (action.equalsIgnoreCase("complaint")) {
                                %>
                                <div class="pageHead_1"> List of Complaint Resolution </div>

                                <%} else {%>
                                <div class="pageHead_1"> List of Other Information </div>
                            <%}%>

                            <%if ("y".equals(request.getParameter("succFlag"))) {
                                                if ("Press".equals(action)) {%>
                            <div id="succMsg" class="responseMsg successMsg">Press Release posted successfully.</div>
                            <%} else if ("Complaint".equals(action)) {%>
                            <div id="succMsg" class="responseMsg successMsg">Complaint Resolution posted successfully.</div>
                            <%} else {%>
                            <div id="succMsg" class="responseMsg successMsg">Other Information posted successfully.</div>
                                <%}
                                            }%>


                                <div class="t_space b_space t-align-right">
                                    <%
                                                if (strUserTypeId.equals("1")) {
                                                    if (action.equalsIgnoreCase("press")) {
                                    %>
                                    <a href="../admin/PostMediaContent.jsp?contentType=Press" title="New Press Release" class="action-button-add">New Press Release</a></div>
                                    <% } else if (action.equalsIgnoreCase("complaint")) {
                                    %>
                                <a href="../admin/PostMediaContent.jsp?contentType=Complaint" title="New Complaint Resolution" class="action-button-add">New Complaint Resolution</a></div>

                                <%} else {%>
                                <a href="../admin/PostMediaContent.jsp?contentType=Other" title="New Other Information" class="action-button-add">New Other Information</a></div>
                                <%}
                                            }
                                %>


                                <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space">
                                    <tr>
                                        <th width="4%">Sl. No.</th>
                                    <% if (action.equalsIgnoreCase("Press")) {%>
                                        <th width="41%">Press Release Brief</th>
                                    <%} else if (action.equalsIgnoreCase("Complaint")) {%>
                                    <th width="41%">Complaint Resolution Brief</th>
                                    <%} else {%>
                                    <th width="50%">Other Information Brief</th>
                                    <%}%>
                                        <th width="8%">Date and Time</th>
                                        <th width="7%">Detail</th>
                                    </tr>
                                </table>

                                <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                     <td align="center"><input name="textfield3" type="text" onKeydown="Javascript: if (event.keyCode==13) getDataOnEnter();" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                            &nbsp;
                                            <label class="formBtn_1 t-align-center">

                                                <input type="button" name="button" id="btnGoto" value="Go To Page" />


                                            </label>


                                        </td>
                                        <td align="right" class="prevNext-container"><ul>
                                                <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul></td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" name="size" id="size" value="10"/>
                                </div>




                            <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>



                                </body>
                                <script>
                                    var vContentType="<%=action%>";
                                    var headSel_Obj;
                                    if(vContentType == "Press")
                                    {
                                        headSel_Obj = document.getElementById("headTabMediaPress");
                                    }
                                    else if(vContentType == "Complaint")
                                    {
                                        headSel_Obj = document.getElementById("headTabMediaComplaint");
                                    }
                                    else
                                    {
                                        headSel_Obj = document.getElementById("headTabMediaOther");
                                    }
                                    headSel_Obj.setAttribute("class", "selected");
                                </script>
                                </html>
