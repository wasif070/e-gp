<%-- 
    Document   : ViewPostQualiDetail
    Created on : Jun 24, 2011, 5:38:51 PM
    Author     : shreyansh
--%>


<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblPostQualification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblPostQualificationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
                   
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Post Qualification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%  String tenderId;
            tenderId = request.getParameter("tenderId");
                    
              // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";

            int auditId=Integer.parseInt(tenderId);
            String auditAction="View PQ by Tenderer";
            String moduleName=EgpModule.Evaluation.getName();
            String remarks="";
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Post Qualification
<!--                    <span class="c-alignment-right"><a href="InitiatePQ.jsp?tenderId=<-%=tenderId%>" class="action-button-goback">Go Back</a></span>-->
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
              
                          <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../tenderer/TendererTabPanel.jsp" %>
                <div>&nbsp;</div>
                
                <div class="tabPanelArea_1">
                    <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="3" />
        <jsp:param name="tenderId" value="<%=tenderId%>" />
     </jsp:include>

<div>&nbsp;</div>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <%
                                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                    EvaluationService es = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                                    int evalCount = es.getEvaluationNo(Integer.parseInt(tenderId));
                                    List<Object[]> os = cs.getData(tenderId,evalCount);
                                    List<Object[]> list = null;
                                    if(!os.isEmpty() && os!=null){
                                    list = cs.getLotDetailsByTenderIdAndPkgIdforEval(tenderId, os.get(0)[0].toString());
                                    }
                                    //TblPostQualificationService service = (TblPostQualificationService)AppContext.getSpringBean("PostQualificationService");


                        %>
                        <tr>
                            <th  class="ff" widtd="10%" valign="middle" class="t-align-center">Lot No.</th>
                            <th class="ff" width="40%" valign="middle" class="t-align-center">Lot Description</th>
                            <th class="ff" width="40%" valign="middle" class="t-align-center">Site Visit Date and Time</th>
                            
                        </tr>
                        <%if(os!=null && (!os.isEmpty())){%>
                        <tr>
                            <td class="t-align-center"><%=list.get(0)[0]%></td>
                            <td class="t-align-center"><%=list.get(0)[1]%></td>
                            <td class="t-align-center"><%
                            if(os.get(0)[1] != null){
                                String temp[] = os.get(0)[1].toString().split("-");
                                 if("1900".equalsIgnoreCase(temp[0])){
                                 out.print("-");
                                }else{
                                    out.print(DateUtils.gridDateToStr((Date)os.get(0)[1]));
                                }
                            }
                            %></td>
                        </tr>
                        <%}else{%>
                        <td colspan="3" class="t-align-center">No records Found</td>
                        <%}%>
                        <tr>
                            
                            
                        </tr>
                    </table>
                    <br />
                    
                    <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
   <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
