<%--
    Document   : BidPreperation
    Created on : Nov 20, 2010, 5:52:22 PM
    Author     : Administrator
--%>


<%--<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>--%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%--<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.cptu.egp.eps.service.serviceimpl.BriefcaseDocImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        BriefcaseDocImpl briefcaseDocImpl = (BriefcaseDocImpl) AppContext.getSpringBean("BriefcaseDocImpl");
        String tenderId = "", docFeesMode="";
        int lotNo = 0, formNo = 0;
        String strDeleteForm = "";

        /* Dohatec Start*/
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String procType = commonService.getProcurementType(request.getParameter("tenderId")).toString();
        /* Dohatec End*/
            boolean isPaid = false;
            boolean isSubDt = false;
            boolean isDocPaid = false;
              boolean iswid = false;
              boolean isfinalSub = false;

            if (request.getParameter("tenderId") != null) {
                tenderId = request.getParameter("tenderId");
            }

            List<SPTenderCommonData> lstTenderInfo = tenderCommonService1.returndata("getTenderInfoForDocView", tenderId, null);
            if (!lstTenderInfo.isEmpty() && lstTenderInfo.size()>0){
                docFeesMode=lstTenderInfo.get(0).getFieldName2();

                /*
                if (docFeesMode.equalsIgnoreCase("offline")){
                    isDocPaid= true;
                } else if (docFeesMode.equalsIgnoreCase("bank")){
                    isDocPaid = true;
                } else if (docFeesMode.equalsIgnoreCase("offline/bank")) {
                    isDocPaid = true;
                } else {
                    isDocPaid = false;
                }
                */

                if ("Free".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())) {
                    isDocPaid = false;
                } else if ("Paid".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())) {
                    isDocPaid = true;
            }
            }


            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);

            if (submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true")) {
                isSubDt = true;
            } else {
                isSubDt = false;
            }

            String userIdBidPrep = "";
            HttpSession sessionBidPrep = request.getSession();
            userIdBidPrep = sessionBidPrep.getAttribute("userId").toString();

            if (request.getParameter("lotId") != null) {
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <link href="<%= request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/resources/js/poshytipLib/jquery.poshytip.js"></script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End--
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
            <div class="pageHead_1">Tender Preparation<span style="float: right;"><a class="action-button-goback" href="LotTendPrep.jsp?tab=4&tenderId=<%=tenderId%>">Go Back to Tender Dashboard</a></span></div>
            <div class="mainDiv">
                <%
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>
            <%@include file="TendererTabPanel.jsp" %>
            <%if(!is_debared){%>
            <div>&nbsp;</div>
            <div class="tabPanelArea_1">
                <%
                                List<SPCommonSearchData> lotList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Lot", request.getParameter("lotId"), null, null, null, null, null, null);

                                //List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getlotorpackagebytenderid", tenderId,
                                //                                                              "Lot," + request.getParameter("lotId"));
                                if (!lotList.isEmpty()) {

                %>
                <div class="t-align-right formStyle_1"><a href="LotTendPrep.jsp?tab=4&tenderId=<%=tenderId%>">Lot Selection</a></div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="15%" class="t-align-left ff">Lot No. :</td>
                        <td width="85%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Lot Description :</td>
                        <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
                    </tr>

                </table>
                <%}%>
                <div class="t_space">Please click on 'Map' link available in front of respective form to upload reference/supporting documents as mentioned in Tender Document
                    </div>
                <div class="t_space">
                    Forms marked with (*) sign are Mandatory forms
                </div>

<%
            if(request.getParameter("msg")!=null){
                if("del".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form Deleted successfully</div>
<%
                }
                if("add".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form saved successfully</div>
<%
                }
                if("edit".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form updated successfully</div>
<%
                }
                if("exist".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form already saved</div>
<%
                }
                if("existEncrypt".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form already encrypted</div>
<%
                }
            }
%>

                <% List<SPCommonSearchData> formList = commonSearchService.searchData("getformnamebytenderidandlotid", tenderId, request.getParameter("lotId"), null, null, null, null, null, null, null);%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="61%" valign="middle" class="t-align-center ff">Form Name</th>
                        <th width="21%" valign="middle" class="t-align-center ff">Action</th>
                        <th width="18%" valign="middle" class="t-align-center ff">
                            <!--Reference Document-->
                            Map the documents<br/>from your Common Document Library, if requested
                        </th>
                    </tr>
                    <%              String encrypt;
                                    String multiFill;

                                    String chkMandatory;
                                    if (!formList.isEmpty()) {
                                        int frmCnt=0;
                                        for (SPCommonSearchData formname : formList) {
                                            frmCnt=frmCnt+1;
                                            encrypt = formname.getFieldName2();
                                            multiFill = formname.getFieldName9();
                                            chkMandatory = formname.getFieldName4();
                                            String userId;
                                             userId = "";
                                               if (session.getAttribute("userId") != null) {
                                                  userId = session.getAttribute("userId").toString();
                                               }

                                             // STRAT  BID WITHDRAWAL
                                              List<SPCommonSearchData> btnBidwid = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                                if (!btnBidwid.isEmpty()) {
                                                    if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("withdrawal")){
                                                        iswid = true;
                                                    }
                                                     if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("finalsubmission")){
                                                        isfinalSub = true;
                                                    }
                                                }
                                            // END BID WITHDRAWAL

                                             //out.println(formname.getFieldName5());
                                            //out.println("Userid:" + userId);
                                            List<SPCommonSearchData> bidFormList = commonSearchService.searchData("GetBidformInfo", tenderId, formname.getFieldName5(), userId, null, null, null, null, null, null);


                                            boolean isDocumentMapped = briefcaseDocImpl.isDocumentMapped(Integer.parseInt(formname.getFieldName5()),Integer.parseInt(userId), Integer.parseInt(tenderId));
                    %>
                    <tr

                         <%if(Math.IEEEremainder(frmCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                        >
                        <td class="t-align-left"><%=formname.getFieldName1()%><%if("c".equals(formname.getFieldName6()) || "cp".equals(formname.getFieldName6())){out.print("  <font color='red'>(Form Canceled)</font>");}else{if(chkMandatory.equalsIgnoreCase("yes")){%> <font style="color:red;"> * <%if("c".equalsIgnoreCase( formname.getFieldName4())){out.print("(cancelled)");}%></font> <%}}%></td>
                            <%
                            if("c".equals(formname.getFieldName6())){
                                out.print("<td>-</td><td>-</td>");
                        %>
                        <%}else{%>
                        <td class="t-align-left">
                            <%if ((!isSubDt) || (iswid )|| (isfinalSub )) {%>

                            <a href="#">View</a>

                            <%} else {
                                if (!bidFormList.isEmpty()) {
                                    int cnt;
                                    cnt = 0;
                                    for (SPCommonSearchData bidForm : bidFormList) {
                                        List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatus", tenderId, formname.getFieldName5(), userId, bidForm.getFieldName1(), null, null, null, null, null);
                                        if (cnt != 0) {%>
                            <br/><br/>
                            <%}%>
                            <%
                                if (encrypt.equals("yes")) {
                                    if (encryptStatus.isEmpty()) {
                            %>
                                        <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&lotId=<%=request.getParameter("lotId")%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                            <%
                                    }else{

                                    }
                                }else{
                            %>
                                <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&lotId=<%=request.getParameter("lotId")%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                            <%
                                }
                                        strDeleteForm = "DelBidForm.jsp?tenderId="+request.getParameter("tenderId")+"&formId="+formname.getFieldName5()+"&bidId="+bidForm.getFieldName1()+"&lotId="+request.getParameter("lotId")+"&action=Delete";
                            %>
                            <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&lotId=<%=request.getParameter("lotId")%>&action=View&parentLink=525">View</a>  &nbsp;|&nbsp;
                            <a onclick="return checkFinalSub(this,'<%=strDeleteForm%>');" style="cursor:pointer;">Delete</a>
                            <%if (encrypt.equals("yes")) {
                                  if (encryptStatus.isEmpty()) {%>
                             &nbsp;|&nbsp; <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&lotId=<%=request.getParameter("lotId")%>&action=Encrypt&parentLink=525">Encrypt</a>
                            <%
                              }else{
                                    out.print("&nbsp;|&nbsp; <b>Encrypted</b> ");
                              }
                                                                }
                                                                cnt = cnt + 1;
                                                            }
                                                            if (multiFill.equals("yes")) {%>
                                                            <br/><br/><%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Fill Again</a><%}else{out.print("-");}%>
                            <%}%>
                            <%} else {%>
                            <%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Fill</a><%}else{out.print("-");}%><%}%>
                            <%}%>
                        </td>

                        <td class="t-align-center">

                            <%if((isSubDt) && (!iswid ) && (!isfinalSub )) {%>
                            <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))
                            {
                                int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formname.getFieldName5(), "" + userId);
                                if(doc_pending==0)
                                { %>
                                <div class="tooltipDiv">
                                    <img id="DocumentUploadDone" src="../resources/images/done.ico" border="0" alt="All required documents uploaded" style="vertical-align:middle;" />
                                    <span class="tooltiptext">All required documents uploaded</span>
                                </div>
                                <% }
                                else if(doc_pending > 0)
                                { %>
                                <div class="tooltipDiv">
                                    <img id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" alt="One or more documents are not uploaded yet" style="vertical-align:middle;" />
                                   <span class="tooltiptext">One or more documents are not uploaded yet</span>
                               </div>
                                <% }
                            %>
                                <a href="TendererFormDocMap.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Map</a> 
                            <%}else
                               {
                                    out.print("-");
                               }
                            %>
                            <%if(isDocumentMapped){
                            %>
                            &nbsp; | &nbsp;
                            <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                            <%}else{
                                //out.print(" - ");
                              } 
                            } else {%>
                            <%if(isDocumentMapped){%>
                            <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                            <%}else{
                                //out.print(" - ");
                            }
                            %>
                            <%}%>
                        </td>
                        <%}%>
                    </tr>
                    <%

                                                            }
                     %>

                    <%
                        List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", request.getParameter("lotId").toString(), null);
                        if(!"0".equals(grandsumlink.get(0).getFieldName1()) && !("ICT".equalsIgnoreCase(procType))){
                    %>
                    <tr <%if(Math.IEEEremainder(frmCnt,2)!=0) {%>
                            class="bgColor-Green"
                        <%} else {%>
                        class="bgColor-white"
                        <%   }%>>
                        <td class="t-align-left" colspan="3"><span style="font-weight: bold;">Grand Summary</span>&nbsp;&nbsp;:&nbsp;&nbsp;
                            <a href="<%=request.getContextPath()%>/tenderer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=request.getParameter("lotId").toString()%>">View </a>
                        </td>
                    </tr>
                <%      }
                         } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><B>No form name found!</B></td>
                    </tr>
                    <% }%>
                </table>


            </div>
            <%}%>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
        <%
                    } else { // PACKAGE CASE

                        //List<SPCommonSearchData> lotList = commonSearchService.searchData("getlotorpackagebytenderid",tenderId,"Package", "1", null, null, null, null, null, null);
                        String currUserId = "";
                        if (session.getAttribute("userId") != null) {
                            currUserId = session.getAttribute("userId").toString();
                        }
                        List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getPackageDescriptonForDocView", tenderId, currUserId);

                        if (!lotList.isEmpty()) {

                            if (!lotList.get(0).getFieldName3().equalsIgnoreCase("0") && !lotList.get(0).getFieldName3().equalsIgnoreCase("N.A.")) {
                                isPaid = true;
                            }
        %>
        <table width="100%" cellspacing="0" class="tableList_1">

            <tr>
                <td width="15%" class="t-align-left ff">Package No. :</td>
                <td width="85%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
            </tr>
            <tr>
                <td class="t-align-left ff">Package Description :</td>
                <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
            </tr>
            <%if (!isPaid && isDocPaid) {%>
            <tr>
                <td class="t-align-left ff">Payment Status :</td>
                <td class="t-align-left">Pending</td>
            </tr>
            <%}%>
        </table>
        <div class="t_space">Please click on 'Map' link available in front of respective form to upload reference/supporting documents as mentioned in Tender Document
        </div>
        <%}%>
<%
            if(request.getParameter("msg")!=null){
                if("del".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form Deleted successfully</div>
<%
                }
                if("add".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form saved successfully</div>
<%
                }
                if("edit".equalsIgnoreCase(request.getParameter("msg"))){
%>
                <div class="responseMsg successMsg t_space">Form updated successfully</div>
<%
                }
            }
%>

        <%if (isPaid || !isDocPaid){%>
        <%
            List<SPCommonSearchData> techFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Technical", session.getAttribute("userId").toString(), null, "Bid", null, null, null);
        %>
        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblMain">
<!--            <tr>
                <th colspan="3" class="t-align-center ff">Technical Forms</th>
            </tr>-->
            <tr>
            <input type="hidden" id="totalLotNo" value="" />
            </tr>
            <tr>
                <th width="61%"valign="middle" class="t-align-center ff">Form Name</th>
                <th width="21%" valign="middle" class="t-align-center ff">Action</th>
                <th width="18%" valign="middle" class="t-align-center ff">
                            <!--Reference Document-->
                            Map the documents<br/>from your Common Document Library, if requested
                </th>
            </tr>
            <%  String encrypt;
                            String multiFill;
                            String userId;
                             String chkMandatory;
                             String FormType;
                            //if (!techFormList.isEmpty()) {
                                int frmCount=0;
                                for (SPCommonSearchData formname : techFormList) {
                                    frmCount=frmCount+1;
                                    encrypt = formname.getFieldName2();
                                    multiFill = formname.getFieldName9();
                                     chkMandatory = formname.getFieldName4();
                                    userId = "";
                                    if (session.getAttribute("userId") != null) {
                                        userId = session.getAttribute("userId").toString();
                                    }

                                    // STRAT  BID WITHDRAWAL
                                              List<SPCommonSearchData> btnBidwid = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                                if (!btnBidwid.isEmpty()) {
                                                    if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("withdrawal")){
                                                        iswid = true;
                                                    }
                                                     if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("finalsubmission")){
                                                        isfinalSub = true;
                                                    }
                                                }
                                            // END BID WITHDRAWAL

                                    //out.println("Form Name:" + formname.getFieldName1());
                                    //out.println("Userid:" + userId);
                                    List<SPCommonSearchData> bidFormList = commonSearchService.searchData("GetBidformInfo", tenderId, formname.getFieldName5(), userId, null, null, null, null, null, null);

                                    boolean isDocumentMapped = briefcaseDocImpl.isDocumentMapped(Integer.parseInt(formname.getFieldName5()),Integer.parseInt(userId), Integer.parseInt(tenderId));

            %>
            <tr
                <%if(Math.IEEEremainder(frmCount,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                >
                <td class="t-align-left"><%=formname.getFieldName1()%><%if("c".equals(formname.getFieldName6())){out.print("  <font color='red'>(Form Canceled)</font>");}else{if(chkMandatory.equalsIgnoreCase("yes")){%> <font style="color:red;"> * <%if("c".equalsIgnoreCase( formname.getFieldName4())){out.print("(cancelled)");}%></font> <%}}%></td>
                <%
                    if("c".equals(formname.getFieldName6())){
                        out.print("<td>-</td><td>-</td>");
                %>
                <%}else{%>
                <td class="t-align-left">
                   <%if ((!isSubDt) || (iswid )|| (isfinalSub )) {%>

                    <a href="#">View</a>

                    <% } else {
                        if (!bidFormList.isEmpty()) {
                            int cnt;
                            cnt = 0;
                    %>
                    <%  for (SPCommonSearchData bidForm : bidFormList) {
                            List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, formname.getFieldName5(), userId, bidForm.getFieldName1(), null, null, null, null, null);
                                                if (cnt != 0) {%>
                    <br/><br/>
                    <%}%>
                    <%
                        if (encrypt.equals("yes")) {
                            if (encryptStatus.isEmpty()) {
                    %>
                                <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                    <%
                            }else{

                            }
                        }else{
                    %>
                        <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                    <%
                        }
                            strDeleteForm = "DelBidForm.jsp?tenderId="+request.getParameter("tenderId")+"&formId="+formname.getFieldName5()+"&bidId="+bidForm.getFieldName1()+"&action=Delete";
                    %>
                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=View&parentLink=525">View</a>  &nbsp;|&nbsp;
                    <a onclick="checkFinalSub(this,'<%=strDeleteForm%>');" style="cursor: pointer;">Delete</a>
                    <%if (encrypt.equals("yes")) {
                               if (encryptStatus.isEmpty()) {%>
                    &nbsp;|&nbsp; <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Encrypt&parentLink=525">Encrypt</a>
                    <%
                      }else{
                          out.print(" &nbsp;|&nbsp; <b>Encrypted</b>");
                      }
                                                }
                                                cnt = cnt + 1;
                                            }
                                            if (multiFill.equals("yes")) {%>
                    <br/><br/><%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill Again </a><%}else{out.print("-");}%>
                    <%}%>
                    <%} else {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a><%}else{out.print("-");}%><%}%>
                    <%}%>
                </td>

                <td class="t-align-center">
                    <%if((isSubDt) || (!iswid )|| (!isfinalSub )) {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))
                    {
                        int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formname.getFieldName5(), "" + userId);
                        if(doc_pending==0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadDone" src="../resources/images/done.ico" border="0" alt="All required documents uploaded" style="vertical-align:middle;" />
                            <span class="tooltiptext">All required documents uploaded</span>
                        </div>
                         <% }
                        else if(doc_pending > 0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" alt="One or more documents are not uploaded yet" style="vertical-align:middle;" />
                            <span class="tooltiptext">One or more documents are not uploaded yet</span>
                        </div>
                        <% }
                    %>
                        <a href="TendererFormDocMap.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Map</a>
                    <%}else
                        {
                            out.print("-");
                        }%>
                    <%if(isDocumentMapped){%>
                    &nbsp; | &nbsp;
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    

                    %>
                    <%} else {%>
                    <%if(isDocumentMapped){%>
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    %>
                    <%}%>
                </td>
                <%}%>
            </tr>
            <%}%>
<!--        </table>-->
<!--            <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <tr>
                <th colspan="3" class="t-align-center ff">PriceBid Forms</th>
            </tr>
            <tr>
                <th width="61%"valign="middle" class="t-align-center ff">Form Name</th>
                <th width="21%" valign="middle" class="t-align-center ff">Action</th>
                <th width="18%" valign="middle" class="t-align-center ff">Reference Document</th>
            </tr>-->
            <%
              //Taher
                frmCount=0;
                boolean isNoForm = true;
                CommonService cs = (CommonService)AppContext.getSpringBean("CommonService");
                String pNature = cs.getProcNature(tenderId).toString();  
                List<SPCommonSearchData> lotIdDesc  = commonSearchService.searchData("getlotid_lotdesc", tenderId, session.getAttribute("userId").toString(), null, null, null, null, null, null, null);
                for (SPCommonSearchData lotdsc : lotIdDesc) {
                    if(!"Services".equalsIgnoreCase(pNature)){
                        out.print("<tr><td colspan='3'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Lot No.</td><td width='85%'>"+lotdsc.getFieldName4()+"</td></tr><tr><td class='t-align-left ff'>Lot Description</td><td>"+lotdsc.getFieldName2()+"</td></tr></table></td></tr>");
                        lotNo = lotNo+1 ;
                        formNo = 1 ;
                }

                List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", lotdsc.getFieldName1(), null);
                if(!"0".equals(grandsumlink.get(0).getFieldName1()) && (!"ICT".equalsIgnoreCase(procType))){
                %>
                <tr>
                    <td class="t-align-left" colspan="3"><span style="font-weight: bold;">Grand Summary</span>&nbsp;&nbsp;:&nbsp;&nbsp;
                        <a href="<%=request.getContextPath()%>/tenderer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=lotdsc.getFieldName1()%>">View </a>
                    </td>
                </tr>
                <!-- <tr>
                    <td class="t-align-left" colspan="3"><span style="font-weight: bold;">Tenderer Category</span>&nbsp;&nbsp;:&nbsp;&nbsp;
                              <select name="cmbTndrCat" class="formTxtBox_1" id="cmbTndrCat">
                                        <option value="">Select</option>
                                        <option value="Manufacturer">Manufacturer</option>
                                        <option value="Importer">Importer</option>
                                    </select>
                    </td>
                </tr>-->
                 <%                 }
                                    userId = "";
                                    if (session.getAttribute("userId") != null) {
                                        userId = session.getAttribute("userId").toString();
                                    }
                 //For ICT Goods Tenderer
                //CommonService cs = (CommonService)AppContext.getSpringBean("CommonService");
                //String pNature = cs.getProcNature(tenderId).toString();
                List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
                List<SPTenderCommonData> TendererHeadOffAd = tenderCommonService1.returndata("chkTendererHeadOffAd",userId, null);
                    if("Goods".equalsIgnoreCase(pNature)){
                        //out.print("<tr><td colspan='3'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Lot No.</td><td width='85%'>"+lotdsc.getFieldName4()+"</td></tr><tr><td class='t-align-left ff'>Lot Description</td><td>"+lotdsc.getFieldName2()+"</td></tr></table></td></tr>");
             // if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") && TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bangladesh") && formname.getFieldName12().equalsIgnoreCase("Manufacturer"))
            if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") && TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan")) || (tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes") && TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan")))
             {
                 String tgID = "cmbTndrgroup"+String.valueOf(lotNo);

                 %>
               <tr>
                   <td class="t-align-left" colspan="3"><span style="font-weight: bold;">Tender Group :</span><span class="mandatory" style="color:red;"> *</span>&nbsp;&nbsp;
                       <select name="cmbTndrgroup" class="formTxtBox_1" id=<%=tgID%> onchange="getSelectedValue(this.value,'<%=lotNo%>')">
                                   <option value="Select">-- Select --</option>
                                   <option value="Manufactured">Goods Manufactured in Bhutan</option>
                                   <option value="Imported">Goods Manufactured outside Bhutan (to be imported, already imported) </option>
                              </select>
                    </td>
                </tr>
            <%
            } %>    
          <%   }
                //End ICT Goods Tenderer
                List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Pricebid", session.getAttribute("userId").toString(), lotdsc.getFieldName1(), "Bid", null, null, null);
                int encryptedFormCount=0, isMandatoryCount=0, importedCount=0, manufacturedCount=0;
                int countImportedOrManufactured=0;
                boolean isICT=false, isImported=false, isManufactured=false, isDiscountFormEncrypted=false, isDiscountFormFilled=false;

                for (SPCommonSearchData financialForm : priceFormList) {
                    if("Discount Form".equalsIgnoreCase(financialForm.getFieldName1()))
                    {
                        priceFormList.remove(financialForm);
                        priceFormList.add(financialForm);
                        String isDiscountFormencrypt="";
                        isDiscountFormencrypt=financialForm.getFieldName2();
                        List<SPCommonSearchData> bidFormList = commonSearchService.searchData("GetBidformInfo", tenderId, financialForm.getFieldName5(), userId, null, null, null, null, null, null);
                        if(!bidFormList.isEmpty()){
                            List<SPCommonSearchData> discountFormEncryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, financialForm.getFieldName5(), userId, bidFormList.get(0).getFieldName1(), null, null, null, null, null);
                            if(!discountFormEncryptStatus.isEmpty())
                                isDiscountFormEncrypted=true;
                            List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, bidFormList.get(0).getFieldName5(), userId, bidFormList.get(0).getFieldName1(), null, null, null, null, null);
                            if("yes".equalsIgnoreCase(isDiscountFormencrypt)){
                                if(encryptStatus.isEmpty())
                                    isDiscountFormFilled=true;
                            }
                        }
                        break;
                    }
                }

                if (tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    isICT=true;
                else
                    isICT=false;
                if (!priceFormList.isEmpty()) {//taher
                out.print("<tr>");
                out.print("<th width='61%'valign='middle' class='t-align-center ff'>Form Name</th>");
                out.print("<th width='21%' valign='middle' class='t-align-center ff'>Action</th>");
                out.print("<th width='18%' valign='middle' class='t-align-center ff'>Map the documents<br/>from your Common Document Library, if requested</th>");
                out.print("</tr>");
                                for (SPCommonSearchData formname : priceFormList) {
                                    frmCount=frmCount+1;
                                    isNoForm = false;
                                    encrypt = formname.getFieldName2();
                                    multiFill = formname.getFieldName9();
                                    chkMandatory = formname.getFieldName4();
                                    if(!"Discount Form".equalsIgnoreCase(formname.getFieldName1()) && !"Discount Amount".equalsIgnoreCase(formname.getFieldName1()))
                                    {
                                        if("yes".equalsIgnoreCase(chkMandatory))
                                        {
                                            isMandatoryCount = isMandatoryCount+1;
                                            if(formname.getFieldName6()!=null)
                                            {
                                                if("c".equalsIgnoreCase(formname.getFieldName6()))
                                                    isMandatoryCount = isMandatoryCount-1;
                                            }
                                        }
                                        
                                        if("no".equalsIgnoreCase(chkMandatory) && isICT==true)
                                        {
                                            if("manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                            {
                                                manufacturedCount = manufacturedCount+1;
                                                if("c".equalsIgnoreCase(formname.getFieldName6()))
                                                    manufacturedCount = manufacturedCount-1;
                                            }
                                            if("imported".equalsIgnoreCase(formname.getFieldName12()))
                                            {
                                                importedCount = importedCount+1;
                                                if("c".equalsIgnoreCase(formname.getFieldName6()))
                                                    importedCount = importedCount-1;
                                            }
                                        }
                                        
                                    }
                                    
                                    FormType=formname.getFieldName12();
                                    userId = "";
                                    if (session.getAttribute("userId") != null) {
                                        userId = session.getAttribute("userId").toString();
                                    }
                // For ICT Tenderer
                // boolean ICTTenderer=false;

                //End ICT Tenderer
                                    // STRAT  BID WITHDRAWAL
                                              List<SPCommonSearchData> btnBidwid = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                                if (!btnBidwid.isEmpty()) {
                                                    if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("withdrawal")){
                                                        iswid = true;
                                                    }
                                                     if (btnBidwid.get(0).getFieldName1().equalsIgnoreCase("finalsubmission")){
                                                        isfinalSub = true;
                                                    }
                                                }
                                            // END BID WITHDRAWAL

                                   //out.println("Form Name: "+formname.getFieldName5()+"  ");
                                   //out.println("DP Name: "+listDP.get(0).getFieldName1()+"  ");
                                    //out.println("Userid:" + userId);
                                    List<SPCommonSearchData> bidFormList = commonSearchService.searchData("GetBidformInfo", tenderId, formname.getFieldName5(), userId, null, null, null, null, null, null);

                                    boolean isDocumentMapped = briefcaseDocImpl.isDocumentMapped(Integer.parseInt(formname.getFieldName5()),Integer.parseInt(userId), Integer.parseInt(tenderId));

            %>
            <!-- Dohatec Start-->
                <input type="hidden" id="hdnICTorNCT" <%
                    if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") && TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan")) || ((tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) && TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan"))){
                    %>
                        value="ICT"
                    <%
                    }else{%>
                        value="NCT"
                <%}%>>
                <!--    Dohatec Comments
                    In this page, there was a function called "getFieldName11()" for ICT. This function is replaced by "getFieldName12()" function in all place of this page.
                -->
            <!-- Dohatec End-->
            <% // Start For ICT Tenderer by Nazmul
           // out.println("ICT Status: "+listDP.get(0).getFieldName1()+"  ");
            //out.println("Form Name: "+formname.getFieldName1()+"  ");
             //out.println("Form Name: "+formname.getFieldName5()+"  ");
           //  out.println("Tenderer Country: "+TendererHeadOffAd.get(0).getFieldName1()+"  ");
            //  out.println("FormType: "+formname.getFieldName12()+"  ");
               %>
               <% 
                   String tenderID=request.getParameter("tenderId");
                   if("Goods".equalsIgnoreCase(pNature) && (!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") || tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))){
               if(TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan") && (formname.getFieldName12().equalsIgnoreCase("imported")|| formname.getFieldName12().equalsIgnoreCase("manufactured") ||  formname.getFieldName12().equalsIgnoreCase("na") || formname.getFieldName12().equalsIgnoreCase("BOQsalvage") || formname.getFieldName12().contains("Copied form")))
              //if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true"))
                                         {%>
               <!--Dohatec Start-->
               <tr
                <%if(Math.IEEEremainder(frmCount,2)==0) {%>
                                            class="bgColor-Green <%=formname.getFieldName12()%> <%=formname.getFieldName12()+lotNo%>"
                                        <%} else {%>
                                            class="bgColor-white <%=formname.getFieldName12()%> <%=formname.getFieldName12()+lotNo%>"
                                        <%}%>
                >
                   <!--<td class="t-align-left <%=formname.getFieldName12() + "FormName"%>"><%=formname.getFieldName1()%><%if("c".equals(formname.getFieldName6())){out.print("  <font color='red'>(Form Canceled)</font>");}else{if(chkMandatory.equalsIgnoreCase("yes")){%> <font style="color:red;"> * <%if("c".equalsIgnoreCase( formname.getFieldName4())){out.print("(cancelled)");}%></font> <%}}%></td>-->
                <td class="t-align-left <%=formname.getFieldName12() + "FormName"%>"><%=formname.getFieldName1()%>  <font style="color:red;"> * </font></td>
                <%
                    if("c".equals(formname.getFieldName6()) ){
                        out.print("<td>-</td><td>-</td>");
                %>
                <%}else{%>
                <td class="t-align-left <%=formname.getFieldName12()%> <%=formname.getFieldName12()+lotNo%>">
                <!--Dohatec End-->
                   <%if ((!isSubDt) || (iswid )|| (isfinalSub )) {%>

                    <a href="#">View</a>

                    <% } else {
                        if (!bidFormList.isEmpty()) {
                            int cnt;
                            cnt = 0;
                    %>
                    <%  for (SPCommonSearchData bidForm : bidFormList) {
                                    List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, formname.getFieldName5(), userId, bidForm.getFieldName1(), null, null, null, null, null);
                                                if (cnt != 0) {%>
                    <br/><br/>
                    <%}%>
                    <%
                        if (encrypt.equals("yes")) {
                            if (encryptStatus.isEmpty()) {
                              
                    %>
                                <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit">Edit</a> &nbsp;|&nbsp;
                    <%
                            }else{

                            }
                        }else{


                    %>
                        <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit">Edit</a> &nbsp;|&nbsp;
                    <%

                            }
                                    strDeleteForm = "DelBidForm.jsp?tenderId="+request.getParameter("tenderId")+"&formId="+formname.getFieldName5()+"&bidId="+bidForm.getFieldName1()+"&action=Delete";
                    %>
                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=View">View</a>  &nbsp;|&nbsp;
                    <% if(!"Discount Form".equalsIgnoreCase(formname.getFieldName1()) && (isDiscountFormEncrypted==true || isDiscountFormFilled==true ) && "yes".equalsIgnoreCase(formname.getFieldName3())){%>
                    <a onclick="ShowMessage();" style="cursor:pointer; ">Delete</a>
                    <%} else { %>
                        <a onclick="checkFinalSub(this,'<%=strDeleteForm%>');" style="cursor:pointer;">Delete</a>
                    <% }%>
                    <%if (encrypt.equals("yes")) {
                               if (encryptStatus.isEmpty()) {%>
                    &nbsp;|&nbsp; <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Encrypt">Encrypt</a>
                    <%
                      }else{
                          out.print(" &nbsp;|&nbsp; <b>Encrypted</b>");
                          if("yes".equalsIgnoreCase(chkMandatory)||"imported".equalsIgnoreCase(formname.getFieldName12())||"manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                encryptedFormCount=encryptedFormCount+1;
                          if("imported".equalsIgnoreCase(formname.getFieldName12()))
                                isImported=true;
                          else if("manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                isManufactured=true;                          
                      }
                                                }
                                                cnt = cnt + 1;
                                            }
                                            if (multiFill.equals("yes")) {%>
                    <br/><br/><%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill Again </a><%}else{out.print("-");}%>
                    <%}%>
                    <%} else {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))

                    {
                    
                    if(isICT==true){
                        
                        if(isImported==true)
                            countImportedOrManufactured=importedCount;
                        else if (isManufactured=true)
                            countImportedOrManufactured=manufacturedCount;
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && (encryptedFormCount != isMandatoryCount + countImportedOrManufactured))
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();">Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    else if(isICT==false){
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && encryptedFormCount != isMandatoryCount )
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();">Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    }
                    else{out.print("-");}%><%}%>
                    <%}%>
                </td>

                <td class="t-align-center">
                    <%if((isSubDt) || (!iswid )|| (!isfinalSub )) {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))
                    {
                        int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formname.getFieldName5(), "" + userId);
                        if(doc_pending==0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadDone" src="../resources/images/done.ico" border="0" alt="All required documents uploaded" style="vertical-align:middle;" />
                            <span class="tooltiptext">All required documents uploaded</span>
                        </div>
                        <% }
                        else if(doc_pending > 0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" alt="One or more documents are not uploaded yet" style="vertical-align:middle;" />
                           <span class="tooltiptext">One or more documents are not uploaded yet</span>
                        </div>
                        <% }
                     %>

                    <a href="TendererFormDocMap.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Map</a><%}else{out.print("-");}%>


                    <%if(isDocumentMapped){%>
                    &nbsp; | &nbsp;
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    
                    %>
                    <%} else {%>
                    <%if(isDocumentMapped){%>
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    %>
                    <%}%>
                </td>
                <%}%>
            </tr>
             <% }
                else if(!TendererHeadOffAd.get(0).getFieldName1().equalsIgnoreCase("Bhutan") && (formname.getFieldName12().equalsIgnoreCase("imported") ||  formname.getFieldName12().equalsIgnoreCase("na") || formname.getFieldName12().equalsIgnoreCase("BOQsalvage")))
                                            {%>
   <tr
                <%if(Math.IEEEremainder(frmCount,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                >
                <!--<td class="t-align-right"><%=formname.getFieldName1()%><%if("c".equals(formname.getFieldName6())){out.print("  <font color='red'>(Form Canceled)</font>");}else{if(chkMandatory.equalsIgnoreCase("yes")){%> <font style="color:red;"> * <%if("c".equalsIgnoreCase( formname.getFieldName4())){out.print("(cancelled)");}%></font> <%}}%></td>-->
                <td class="t-align-left"><%=formname.getFieldName1()%>  <font style="color:red;"> * </font> </td>
                <%
                    if("c".equals(formname.getFieldName6()) ){
                        out.print("<td>-</td><td>-</td>");
                %>
                <%}else{%>

                <td class="t-align-left">
                   <%if ((!isSubDt) || (iswid )|| (isfinalSub )) {%>

                    <a href="#">View</a>

                    <% } else {
                        if (!bidFormList.isEmpty()) {
                            int cnt;
                            cnt = 0;
                    %>
                    <%  for (SPCommonSearchData bidForm : bidFormList) {
                                    List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, formname.getFieldName5(), userId, bidForm.getFieldName1(), null, null, null, null, null);
                                                if (cnt != 0) {%>
                    <br/><br/>
                    <%}%>
                    <%
                        if (encrypt.equals("yes")) {
                            if (encryptStatus.isEmpty()) {
                    %>
                                <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                    <%
                            }else{

                            }
                        }else{


                    %>
                        <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit&parentLink=525">Edit</a> &nbsp;|&nbsp;
                    <%

                            }
                                    strDeleteForm = "DelBidForm.jsp?tenderId="+request.getParameter("tenderId")+"&formId="+formname.getFieldName5()+"&bidId="+bidForm.getFieldName1()+"&action=Delete";
                    %>
                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=View&parentLink=525">View</a>  &nbsp;|&nbsp;
                    <% if(!"Discount Form".equalsIgnoreCase(formname.getFieldName1()) && (isDiscountFormEncrypted==true || isDiscountFormFilled==true ) && "yes".equalsIgnoreCase(formname.getFieldName3())){%>
                        <a onclick="ShowMessage();" style="cursor:pointer;">Delete</a>
                    <%} else { %>
                        <a onclick="checkFinalSub(this,'<%=strDeleteForm%>');" style="cursor:pointer;">Delete</a>
                    <% }%>                    
                    <%if (encrypt.equals("yes")) {
                               if (encryptStatus.isEmpty()) {%>
                    &nbsp;|&nbsp; <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Encrypt&parentLink=525">Encrypt</a>
                    <%
                      }else{
                          out.print(" &nbsp;|&nbsp; <b>Encrypted</b>");
                          if("yes".equalsIgnoreCase(chkMandatory)||"imported".equalsIgnoreCase(formname.getFieldName12())||"manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                encryptedFormCount=encryptedFormCount+1;
                          if("imported".equalsIgnoreCase(formname.getFieldName12()))
                                isImported=true;
                          else if("manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                isManufactured=true;
                      }
                                                }
                                                cnt = cnt + 1;
                                            }
                                            if (multiFill.equals("yes")) {%>
                    <br/><br/><%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill Again </a><%}else{out.print("-");}%>
                    <%}%>
                    <%} else {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))

                    {
                    if(isICT==true){
                        if(isImported==true)
                            countImportedOrManufactured=importedCount;
                        else if (isManufactured=true)
                            countImportedOrManufactured=manufacturedCount;
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && (encryptedFormCount != isMandatoryCount + countImportedOrManufactured))
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();">Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    else if(isICT==false){
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && encryptedFormCount != isMandatoryCount )
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();">Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    }
                    else{out.print("-");}%><%}%>
                    <%}%>
                </td>

                <td class="t-align-center">
                    <%if((isSubDt) || (!iswid )|| (!isfinalSub )) {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4())){
                       int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formname.getFieldName5(), "" + userId);
                        if(doc_pending==0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadDone" src="../resources/images/done.ico" border="0" alt="All required documents uploaded" style="vertical-align:middle;" />
                            <span class="tooltiptext">All required documents uploaded</span>
                        </div>
                        <% }
                        else if(doc_pending > 0)
                        { %>
                        <div class="tooltipDiv">
                           <img id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" alt="One or more documents are not uploaded yet" style="vertical-align:middle;" />
                           <span class="tooltiptext">One or more documents are not uploaded yet</span>
                        </div>
                        <% } 
                        
                    %>

                    <a href="TendererFormDocMap.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Map</a><%}else{out.print("-");}%>


                    <%if(isDocumentMapped){%>
                    &nbsp; | &nbsp;
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    
                    %>
                    <%} else {%>
                    <%if(isDocumentMapped){%>
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    %>
                    <%}%>
                </td>
                <%}%>
            </tr>
                                            <%
                }
            }
            else
            {%>
            <tr id="formRow_<%=lotNo%>_<%=formNo%>"
                <%if(Math.IEEEremainder(frmCount,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                         <%}%>
                >
                <td class="t-align-left" id="formNamehtml_<%=lotNo%>_<%=formNo%>"><%=formname.getFieldName1()%><%if("c".equals(formname.getFieldName6())){out.print("  <font color='red'>(Form Canceled)</font>");}else{if(chkMandatory.equalsIgnoreCase("yes")){%> <font style="color:red;"> * <%if("c".equalsIgnoreCase( formname.getFieldName4())){out.print("(cancelled)");}%></font> <%}}%></td>
                <!--<td class="t-align-left"><%=formname.getFieldName1()%>  <font style="color:red;"> * </font> </td>-->
                <%
                    if("c".equals(formname.getFieldName6()) ){
                        out.print("<td>-</td><td>-</td>");
                %>
                <%}else{%>

                <td class="t-align-left">
                   <%if ((!isSubDt) || (iswid )|| (isfinalSub )) {%>

                    <a href="#">View</a>

                    <% } else {
                        if (!bidFormList.isEmpty()) {
                            int cnt;
                            cnt = 0;                
                    %>                
                    <%  for (SPCommonSearchData bidForm : bidFormList) {
                                    List<SPCommonSearchData> encryptStatus = commonSearchService.searchData("getEncryptStatusLink", tenderId, formname.getFieldName5(), userId, bidForm.getFieldName1(), null, null, null, null, null);
                                                if (cnt != 0) {%>
                    <br/><br/>
                    <%}%>
                    <%
                        if (encrypt.equals("yes")) {
                            if (encryptStatus.isEmpty()) {
                    %>
                                <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit">Edit</a> &nbsp;|&nbsp;
                    <%
                            }//else{

                           // }
                        }else{


                    %>
                        <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Edit">Edit</a> &nbsp;|&nbsp;
                    <%

                            }
                                    strDeleteForm = "DelBidForm.jsp?tenderId="+request.getParameter("tenderId")+"&formId="+formname.getFieldName5()+"&bidId="+bidForm.getFieldName1()+"&action=Delete";
                    %>
                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=View">View</a>  &nbsp;|&nbsp;
                    <% if(!"Discount Form".equalsIgnoreCase(formname.getFieldName1()) && (isDiscountFormEncrypted==true || isDiscountFormFilled==true ) && "yes".equalsIgnoreCase(formname.getFieldName3())){%>
                        <a onclick="ShowMessage();" style="cursor:pointer;">Delete</a>
                    <%} else { %>
                        <a onclick="checkFinalSub(this,'<%=strDeleteForm%>');" style="cursor:pointer;">Delete</a>
                    <% }%>                    
                    <%if (encrypt.equals("yes")) {
                               if (encryptStatus.isEmpty()) {%>
                    &nbsp;|&nbsp; <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>&bidId=<%=bidForm.getFieldName1()%>&action=Encrypt">Encrypt</a>
                    <%
                      }else{
                          out.print(" &nbsp;|&nbsp; <b>Encrypted</b>");
                          if("yes".equalsIgnoreCase(chkMandatory)||"imported".equalsIgnoreCase(formname.getFieldName12())||"manufactured".equalsIgnoreCase(formname.getFieldName12()))
                            encryptedFormCount=encryptedFormCount+1;
                          if("imported".equalsIgnoreCase(formname.getFieldName12()))
                                isImported=true;
                          else if("manufactured".equalsIgnoreCase(formname.getFieldName12()))
                                isManufactured=true;
                      }
                                                }
                                                cnt = cnt + 1;
                                            }
                                            if (multiFill.equals("yes")) {%>
                    <br/><br/><%if(!"c".equalsIgnoreCase( formname.getFieldName4())){%><a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill Again </a><%}else{out.print("-");}%>
                    <%}%>
                    <%} else {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4()))

                    {

                    if(isICT==true){
                        if(isImported==true)
                            countImportedOrManufactured=importedCount;
                        else if (isManufactured=true)
                            countImportedOrManufactured=manufacturedCount;
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && (encryptedFormCount != isMandatoryCount + countImportedOrManufactured ))
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();" >Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    else if(isICT==false){
                    if("Discount Form".equalsIgnoreCase(formname.getFieldName1())  && encryptedFormCount != isMandatoryCount )
                    {
                        
                    %>
                    <!-- other form = priceFormList.size-1-cancelform -->
                    <a onclick="ShowStatus();">Fill</a>
                    <% }else { %>

                    <a href="BidForm.jsp?tenderId=<%=request.getParameter("tenderId")%>&formId=<%=formname.getFieldName5()%>">Fill</a>
                                    <% }}
                    }
                    else{out.print("-");}%><%}%>
                    <%}%>
                </td>

                <td class="t-align-center">
                    <%if((isSubDt) || (!iswid )|| (!isfinalSub )) {%>
                    <%if(!"c".equalsIgnoreCase( formname.getFieldName4())){
                        int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formname.getFieldName5(), "" + userId);
                        if(doc_pending==0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadDone" src="../resources/images/done.ico" border="0" alt="All required documents uploaded" style="vertical-align:middle;" />
                            <span class="tooltiptext">All required documents uploaded</span>
                        </div>
                        <% }
                        else if(doc_pending > 0)
                        { %>
                        <div class="tooltipDiv">
                            <img id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" alt="One or more documents are not uploaded yet" style="vertical-align:middle;" />
                            <span class="tooltiptext">One or more documents are not uploaded yet</span>
                        </div>
                        <% }
                    %>

                    <a href="TendererFormDocMap.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Map</a><%}else{out.print("-");}%>


                    <%if(isDocumentMapped){%>
                    &nbsp; | &nbsp;
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    
                    %>
                    <%} else {%>
                    <%if(isDocumentMapped){%>
                    <a href="ViewMappedDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" target="_blank">View</a>
                    <% }else {
                        //out.print(" - ");
                    }
                    %>
                    <%}%>
                </td>
                <%}%>
            </tr>
                                         <%
                }
                                    //End ICT Tenderer
                formNo ++;
                } 
              }
                }
              //Taher
                 if (!isNoForm) {
                if(frmCount==0) {
            %>
            <tr>
                <td class="t-align-left" colspan="3"><B>No form name found!</B></td>
            </tr>
            <% }}%>
        </table>
        <%}%>


        <% } %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        
            function ShowStatus() {
                jAlert ("Please fill first the other mandatory financial form");
            }
            
            function ShowMessage() {
                jAlert ("Please delete first 'Discount form'");
            }

    </script>
    <script type="text/javascript">
//<!--Dohatec Start-->
   $(document).ready(function(){
//       $('.DocumentUploadIconTooltip').poshytip({
//                className: 'tip-yellowsimple',
//                showOn: 'focus',
//                alignTo: 'target',
//                alignX: 'left',
//                alignY: 'center',
//                offsetX: 5,
//                showTimeout: 100
//        }); 
       // $('tr.imported', tblMain).hide();
       
        var lot = "<%=lotNo%>";
        var lotNo = parseInt(lot);
        var i;
        //alert(lotNo);
       
       for( i = 1; i<=lotNo; i++){
           
           var importedtr = "tr.imported"+i;
           var attr = '#cmbTndrgroup'+i;
          $(importedtr).css("display", "none");
          
          $('#tblFinalSubMsg').hide();

          if($('#hdnICTorNCT').val() == 'ICT')
          {
                getEncryptedForm(i);
                getSelectedValue($(attr).val(),i);
          }   
      }
    });
    
    function getEncryptedForm(lotNo)
    {
        var value = '';
        var attr = '#cmbTndrgroup'+lotNo;
        // Which form is Encryped
        if(checkImported('Fill',lotNo))
        {
            value = 'Imported';
            $(attr).val(value);
        }
        else if(checkManufactured('Fill',lotNo))
        {
            value = 'Manufactured';
            $(attr).val(value);
        }
        else
        {
            value = 'Select';
            $(attr).val(value);
        }
    }

    function getSelectedValue(value,lotNo) {
    //var index = document.getElementById('cmbTndrgroup').selectedIndex;
    //document.getElementById('cmbTndrgroup').Value=value;

    // Form Show Hidden
    var imported = "tr.imported"+lotNo;
    var manufactured = "tr.manufactured"+lotNo;
    
    if(value == 'Imported'){
       //  $('tr.manufactured', tblMain).hide();
          $(manufactured).css("display", "none");
          $(imported).css("display", "table-row");
         //$('tr.imported', tblMain).show();

    }
    else if(value == 'Manufactured'){

        // $('tr.manufactured', tblMain).show();
        // $('tr.imported', tblMain).hide();
          $(manufactured).css("display", "table-row");
          $(imported).css("display", "none");
    }
    else // Value = 'Select'
    {
        $(imported).css("display", "none");
        $(manufactured).css("display", "none");
    }

    finalSubmissionCheck(value,lotNo);
    //alert("text="+document.getElementById('cmbTndrgroup').options[index].text);
   }

   //<!--Dohatec End-->

   // alert("value="+document.getElementById('cmbTndrgroup').text);
    </script>

    <script language="javascript" type="text/javascript">
        function checkFinalSub(currObj,checkCase){
            $.ajax({
                url: "<%=request.getContextPath()%>/CreateTenderFormSrvt?action=checkFinalSub&tenderId=<%=tenderId%>&userId=<%=userIdBidPrep%>",
                method: 'POST',
                async: false,
                success: function(j) {
                    if(j!=""){
                        return false;
                    }else{
                        var con = window.confirm('Do you want to delete this form?');
                        if(con){
                            currObj.href= checkCase;
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            });
        }
    </script>
</html>
