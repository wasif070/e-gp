<%--
    Document   : BidFormTable
    Created on : Nov 18, 2010, 3:08:48 PM
    Author     : Sanjay
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalCommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderBidDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.SignerImpl" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<div style="overflow: auto; min-width: 964px;">
    <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
    <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

<%
    int userId = 0;
    if(session.getAttribute("userId") != null) {
        if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
        }
    }
    int userTypeId = 0;
    if(session.getAttribute("userTypeId") != null) {
        if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }
    }
%>
<%
    String decryptedValue = "";
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;
    int tenderId = 0;
    int cellId = 0;
    int formId = 0;
    String hashkey = "";

    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";

%>
<%

    if(request.getParameter("hashKey")!=null && !"".equalsIgnoreCase(request.getParameter("hashKey"))){
        hashkey = request.getParameter("hashKey");
    }
    if("null".equals(hashkey)){
        System.out.println("tata");
    }else{
        System.out.println("dfsdf");
        }

    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
     if(request.getParameter("formId")!=null && !"".equalsIgnoreCase(request.getParameter("formId"))){
        formId = Integer.parseInt(request.getParameter("formId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    if(request.getParameter("tenderId")!=null && !"".equalsIgnoreCase(request.getParameter("tenderId"))){
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }

    if(request.getParameter("userId")!=null && !"".equalsIgnoreCase(request.getParameter("userId"))){
        userId = Integer.parseInt(request.getParameter("userId"));
    }

    String action = "";
    if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
         action = request.getParameter("action");
    }

    boolean isEdit = false;
    if("Edit".equalsIgnoreCase(action)){
        isEdit = true;
    }

    boolean isView = false;
    if("View".equalsIgnoreCase(action)){
        isView = true;
    }

    boolean isEncrypt = false;
    if("Encrypt".equalsIgnoreCase(action)){
        isEncrypt = true;
    }

    boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }
    String tableName = "";
    if(request.getParameter("tableName")!=null && !"".equalsIgnoreCase(request.getParameter("tableName"))){
        tableName = request.getParameter("tableName");
    }
    
%>
<tbody id="MainTable<%=tableId%>">
            <input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
            <input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">
<script>
        	var lbl_ids=new Array();
                var lbl_countid=0;
</script>
<%
        short fillBy[] = new short[cols];
        int arrColId[] = new int[cols];
        int arrForShowHide[] = new int[cols];
        String arrColType[] = new String[cols];

        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }

        ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getTableColumns(tableId, true).listIterator();
        ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();
        int grandTotalCell = tenderBidSrBean.getCellForTotalWordsCaption(formId, tableId);
        int grandTotalValueCell = tenderBidSrBean.getCellForTotalWordsCaption(formId, tableId) +1;
        ListIterator<CommonFormData> tblBidTableId = null;
        ListIterator<CommonFormData> tblBidData = null;
        ListIterator<CommonFormData> tblBidCellData = null;

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();

        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
        listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId);

         while(tblFormulaDtl.hasNext()){
            CommonFormData formulaData = tblFormulaDtl.next();
            if(formulaData.getFormula().contains("TOTAL")){
                isTotal = 1;
            }
        }
        while(tblFormulaDtl.hasPrevious()){
            tblFormulaDtl.previous();
        }
        if(isEdit || isView || isEncrypt){
            tblBidTableId = tenderBidSrBean.getBidTableId(bidId, tableId,userId).listIterator();
            tblBidData = tenderBidSrBean.getBidData(bidId,userId).listIterator();

            if(isMultiTable){
                //tblCellsDtl = tenderBidSrBean.getBidCellData(tableId, userId, bidId).listIterator();
            }

            while(tblBidTableId.hasNext()){
                //tblBidTableId.next();
                CommonFormData bidTableIdDt = tblBidTableId.next();
                bidTableIds.append(bidTableIdDt.getBidTableId() + " , ");
                cntRow++;
            }

            if(isEncrypt){
%>
            <input type="hidden" id="hdnBidTableIds" name="hdnBidTableIds" value="<%=bidTableIds%>">
<%
            }
%>
            <script>
                arrBidCount[gblCnt++]='<%=cntRow%>';
            </script>
<%

            if(tblBidTableId.hasPrevious())
                tblBidTableId.previous();

                if(tblBidTableId.hasNext()){
                    tblBidTableId.next();
                    if(rows > 1 && isTotal == 1){
                        //if(isMultiTable){
                            editedRowCount = (cntRow * (rows-1)) + 1;
                        //}else{
                        //    editedRowCount = (cntRow * (rows-1)) + 1;
                            //editedRowCount = (rows*cntRow) - cntRow;
                        //}
                    }else{
                        editedRowCount = (rows*cntRow);
                    }
                }
                TotalBidCount= cntRow;
         }else{
            cntRow = 1;
         }

         int noOfRowsWOGT = 0;

         if(rows > 1 && isTotal == 1){
             noOfRowsWOGT = rows -1;
         }else{
             noOfRowsWOGT = rows;
         }
%>
            <script>
		var rowcount = parseInt("<%=rows%>")*parseInt("<%=cntRow%>");
		arrCompType[<%=tableIndex%>] = new Array(rowcount);
		arrCellData[<%=tableIndex%>] = new Array(rowcount);
		arrDataTypesforCell[<%=tableIndex%>] = new Array(rowcount);
                <%if(isEdit || isView || isEncrypt){%>
                arrTableAddedKey[<%=tableId%>] = parseInt(<%=cntRow%>);
                arrTableAdded[<%=tableIndex%>] = parseInt(<%=cntRow%>);
                totalBidTable[<%=tableIndex%>] = '<%=cntRow%>';
                <%}%>
		for (i=1;i<=rowcount;i++)
		{
			arrCompType[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrCellData[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrDataTypesforCell[<%=tableIndex%>][i]=new Array(<%=cols%>);
		}

		if(rowcount == 1)
		{
			i = <%=cols%>;
		}
                 arrColFillBy[<%=tableIndex%>] = new Array(<%=cols%>);
            </script>
        <%if(isEdit || isView || isEncrypt){%>
            <script>
		document.getElementById("tableCount<%=tableId%>").value = "<%=cntRow%>";
		document.getElementById("originalTableCount<%=tableId%>").value = "<%=cntRow%>";
                //arrBidCount.push(document.getElementById("tableCount"+<%//=tableId%>).value);
            </script>
            <input type="hidden" value="<%=editedRowCount%>" name="eRowCount<%=tableId%>" id="editRowCount">
<%
                while(tblBidTableId.hasPrevious()){
                    tblBidTableId.previous();
                }
        }
        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
%>
                    <script>
                        //alert(<%=formulaData.getColumnId()%>);
                        //alert(arrColTotalIds);
                        isColTotalforTable[<%=tableIndex%>] = 1;
                        for(var i=1;i<=<%=cols%>;i++){
                            if(i == <%=formulaData.getColumnId()%>){
                                arrColTotalIds[<%=tableIndex%>][i-1] = '<%=formulaData.getColumnId()%>';
                            }
                        }
                    </script>
<%
                }
                cnt++;
                FormulaCount = cnt;
            }
%>
            <script>
                    var totalWordArr = new Array();
                    var q = 0;
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
%>
                    <script>
                        var TotalWordColId = "<%=formulaData.getColumnId()%>";
                        totalWordArr[q] = "<%=formulaData.getColumnId()%>";
                        q++;
                        arrColTotalWordsIds[<%=tableIndex%>][<%=x%>] = '<%=formulaData.getColumnId()%>';
                    </script>
<%
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
%>
            <script>
                arrTableFormula[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores Formula
                arrFormulaFor[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores where to Apply Formula
                arrIds[<%=tableIndex%>] = new Array(<%=FormulaCount%>);
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
%>
		<script>
                    arrTableFormula[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getFormula()%>';
                    arrFormulaFor[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getColumnId()%>';
		</script>
<%
                counter++;
            }
%>
            <script language="JavaScript" >
                arrColIds[<%=tableIndex%>] =new Array();
                arrStaticColIds[<%=tableIndex%>] =new Array();
                arrForLabelDisp[<%=tableIndex%>] = new Array();
            </script>
<%
        }else{
            /*
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                }
            }
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
            */
        }
        if(isEdit || isView || isEncrypt){
            if(isMultiTable){
                //rows = editedRowCount;
                while(tblBidTableId.hasPrevious())
                {
                    tblBidTableId.previous();
                }
            }
        }
        if(!(isEdit || isView || isEncrypt)) // Form Add Start
        {
            int colId = 0;
            for (short i = -1; i <= rows; i++) {
                if(i == 0){
%>
             <tr id="ColumnRow">
                 <% if(isMultiTable && (!"List of Drawings".equalsIgnoreCase(tableName))){ %>
                 <th>Select</th>
                 <% } %>
<%
                    for(short j=0;j<cols;j++){
                        if(tblColumnsDtl.hasNext()){
                            CommonFormData colData = tblColumnsDtl.next();
                            //BTN -> Nu. by Emtaz on 24/April/2016
                            colHeader = colData.getColumnHeader().replaceAll("BTN", "Nu.");
                            //colHeader = colData.getColumnHeader();
                            colType = colData.getColumnType();
                            filledBy = colData.getFillBy();
                            showOrHide = Integer.parseInt(colData.getShowOrHide());
                            dataType = colData.getDataType();
                            colId = colData.getColumnId();
                            colData = null;
                        }
                        arrColType[j] = colType;
                        fillBy[j] = filledBy;
                        arrColId[j] = colId;
                        arrForShowHide[j] = showOrHide;
%>
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %>
                    <%if(showOrHide==2){
                        //Dohatec Start
                    %>
                    <script>
                        //document.getElementById("addTD<%= j + 1 %>").style.visibility = "collapse";
                        document.getElementById("addTD<%= j + 1 %>").style.display = "none";
                    </script>
                    <%}%>
                     <script>
                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                    </script>
                </th>
<%
                    }
%>
             </tr>
<%
                }
                if(i > 0){
%>
             <tr id="row<%=tableId%>_<%=i%>">
                 <% if(isMultiTable && (i==1) && (!"List of Drawings".equalsIgnoreCase(tableName))){ %>
                 <td class="t-align-center"><input type="checkbox" id="chk<%=tableId%>_<%=i%>" /></td>
                 <% }else if(isMultiTable && (!"List of Drawings".equalsIgnoreCase(tableName))){ %>
                 <td class="t-align-center">&nbsp;</td>
                 <% } %>
<%
                    int cnt = 0;
                    for(int j=0; j<cols; j++){
                        String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=i%>_<%=columnId%>" align="center">
                <%
                //if(showOrHide==2){
                if(arrForShowHide[j]==2){%>
                    <script>
                      //  document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").style.visibility = "collapse";
                        document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").style.display = "none";
                    </script>
<%              }

                        if(tblCellsDtl.hasNext()){
                            cnt++;
                            CommonFormData cellData = tblCellsDtl.next();
                            dataType = cellData.getCellDataType();
                            //filledBy = cellData.getCellDataType();
                            //filledBy = cellData.getFillBy();
                            cellId = cellData.getCellId();
                            cellValue = cellData.getCellValue();
                            columnId = cellData.getColumnId();
                            // Dohatec Encrypted data save  Prevent start
                            %>
                                      <input type="hidden" name="row_DataType<%=tableId%>_<%=i%>_<%=columnId%>" id="row_DataType<%=tableId%>_<%=i%>_<%=columnId%>" value="<%=dataType%>" />
                            <%
                             // Dohatec Encrypted data save  Prevent end
                            if(true){
                                String cValue = "";
                                cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '';
                             </script>
<%
                            }
                            /* Dohatec Start*/
                            TenderCommonService tenderCommonServiceICT1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                List<SPTenderCommonData> listDP1 = tenderCommonServiceICT1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                                boolean isIctTender1 = false;
                                                                if(!listDP1.isEmpty() && listDP1.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                                    isIctTender1 = true;
                                                                }
                            if(grandTotalCell==cellId && !isIctTender1){
                                out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                            }
                            /* Dohatec End */                            
                            if(fillBy[j] == 1){

%>
                                    <label  name="lbl@<%=tableId%>_<%=i%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=i%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType==11){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                        <%=cellValue.replaceAll("\n", "<br />") %>
                                        <%-- =cellValue --%>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=i%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                               }
                            } else if(fillBy[j] == 2 || fillBy[j] == 3){
                                if(dataType != 2){
                                    if(i == (rows)){
                                        if(FormulaCount>0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
%>                                                        <textarea class="formTxtArea_1"
                                                                readonly
                                                                rows="4"
								tabindex="-1"
                                                                name="row<%=tableId%>_<%=i%>_<%=columnId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>"
							><%=textAreaValue%></textarea>
<%
                                                        } else if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){

                                                            if(listCurrencyObj.size() > 0){
                                                            %>
                                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                                <% } %>
                                                            </select>
                                                            <%
                                                          }
                                                        }else if(dataType==9 || dataType==10){
                                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                                listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                                if(listCellDetail.size() > 0){
                                                                TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                                List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                                /* Dohatec: ICT-Goods Start */
                                                                //Get Currency List to be shown at 'Currency' Combo box
                                                                List<Object[]> currencyObjectList = null;
                                                                if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                                    currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                                }

                                                                //Procurement Type (i.e. NCT/ICT) retrieval
                                                                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                                boolean isIctTender = false;
                                                                if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                                    isIctTender = true;
                                                                }

                                                                //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                                String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                                /* Dohatec: ICT-Goods End */
                                                                 %>
                                                                 <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onclick="changeTextVal(this,'<%=tableId%>');">
                                                                    <%  /* Dohatec: ICT-Goods Start */
                                                                    if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                        for(Object[] obj :currencyObjectList){
                                                                            String selected = "";
                                                                            if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                            out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                        }
                                                                    }   /* Dohatec: ICT-Goods End */
                                                                    else {
                                                                        for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                            String selectedOption = "";
                                                                            if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                                selectedOption = "selected='selected'";
                                                                            }
                                                                            out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                        }
                                                                    }
                                                                    %>
                                                                </select>
                                                                <script language="javascript">
                                                                    //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                                    //combo_val_tableid.push("<%=tableId%>");
                                                                    changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                                </script>
                    <%                                          }
                                                        }else {
%>
                                                        <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                 }else{
                                                    inputtype = "text";
                                                 }
                                            }
                                        }
                                    }

                                    if(i == (rows-1) && isTotal == 1){
                                        //out.print("<b>GenerateViewFormula <b>");
                                    }

                                    //out.print("FillBy : "+fillBy[j]);
                                    //out.print("  Datatype : "+dataType);
                                   if(i != (rows))
                                   {
                                       if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                       {
                                            if(listCurrencyObj.size() > 0){
                                        %>
                                        <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                            <% for(Object[] obj :listCurrencyObj){ %>
                                            <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                            <% } %>
                                        </select>
                                        <%
                                            }
                                       }
%>
                                       <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                        if(fillBy[j] != 3){
                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this, '<%=tableIndex%>');"
<%
                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');"
<%
                                            } else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                            } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==12){
%>
                                          readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                        }
                                             if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none;"
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
                                          if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                        />
<%
                                        if(dataType==12 && fillBy[j]==2){
%>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','img_<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="img_<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align: middle;"
                                             />
<%
                                        }
                                        if(dataType==9 || dataType==10){

                                            List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                            TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                            listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                            if(listCellDetail.size() > 0){
                                            TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                            List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                            /* Dohatec: ICT-Goods Start */
                                            //Get Currency List to be shown at 'Currency' Combo box
                                            List<Object[]> currencyObjectList = null;
                                            if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                            }
                                            String conversaionRate = "";
                                            if(currencyObjectList!=null)
                                            {
                                                JSONObject currency = new JSONObject();
                                                for(Object[] obj :currencyObjectList){
                                                    currency.put(obj[2].toString(), obj[1]);
                                                }
                                                conversaionRate= currency.toString();
                                                conversaionRate = conversaionRate.replace("\"", "'");
                                            }
                                            
                                            //Procurement Type (i.e. NCT/ICT) retrieval
                                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                            boolean isIctTender = false;
                                            if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                isIctTender = true;
                                            }
                                            //Procurement Nature (i.e. Goods/Works/Service) retrival
                                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                            String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                           
                                            /* Dohatec: ICT-Goods End */
                                             %>
                                             <!-- Dohatec :: A function is added onchange of dropdown :: txtReadOnly(this); -->
                                             <%if(tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){ %>
                                             <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" <%if(currencyObjectList!=null){%>onchange="changeTextVal(this,'<%=tableId%>',1,<%=conversaionRate%>); txtReadOnlySBDCurrency(this);"<%} else if(currencyObjectList==null) {%>onchange="changeTextVal(this,'<%=tableId%>'); txtReadOnlySBDCurrency(this);"  <%}else{%>onchange="changeTextVal(this,'<%=tableId%>');"<%}%> >
                                             <%}else{%>
                                             <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" <%if(isIctTender){%>onchange="changeTextVal(this,'<%=tableId%>'); txtReadOnly(this);"<%} else{%>onchange="changeTextVal(this,'<%=tableId%>');"<%}%> >
                                             <%}%>
                                                 
                                              <%  /* Dohatec: ICT-Goods Start */
                                                  //need to add here
                                                if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                    for(Object[] obj :currencyObjectList){
                                                        String selected = "";
                                                        if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                        out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                    }
                                                }   /* Dohatec: ICT-Goods End */
                                                else {
                                                    for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                        String selectedOption = "";
                                                        if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                            selectedOption = "selected='selected'";
                                                        }
                                                        out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                    }
                                                }
                                                %>
                                            </select>
                                                <script language="javascript">
                                                    
                                                   <%if(isIctTender && (currencyObjectList!=null)){%>
                                                        changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>',1,<%=conversaionRate%>); //
                                                    <%}
                                                   else if(isIctTender && (currencyObjectList==null)){%>
                                                        changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>'); //
                                                    <% } 
                                                    else  {%>
                                                        changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                     <% } %>   
                                                </script>
<%                                             }
                                   }
                                   }
                                   else if(i == (rows))
                                   {
                                       if(FormulaCount > 0)
                                        {
                                            if(isTotal == 1)
                                            {
                                                /* Dohatec Start*/
                                            TenderCommonService tenderCommonService6 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<SPTenderCommonData> listDP = tenderCommonService6.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                            boolean isIctTender = false;
                                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                isIctTender = true;
                                            }
                                            /* Dohatec End*/
                                                if(arrFormulaColid[columnId-1] == columnId && !isIctTender)
                                                {

%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(fillBy[j] != 3){
                                                        if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        }else if(dataType == 1 || dataType == 2)
                                                        {
%>

<%
                                                        }
                                                         else if(dataType==12){
%>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                        } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                        } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"

                                        <%
                                                        }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                                    }
                                        if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>

                                        />
<%
                                                }
                                            }else{
                                               if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                    } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                    } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType == 1 || dataType == 2){
%>

<%
                                                    }else if(dataType==12){
%>
                                                        readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                                }
                                               if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }

                                        if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none;"
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
%>
                                        />

<%
                                        if(dataType==12 && fillBy[j]==2){
%>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','img_<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="img_<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align: middle;"
                                            />
<%
                                        }
                                                 if(dataType==9 || dataType==10){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                        /* Dohatec: ICT-Goods Start */
                                                        //Get Currency List to be shown at 'Currency' Combo box
                                                        List<Object[]> currencyObjectList = null;
                                                        if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                            currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                        }
                                                        //JSONObject conversionRate = new JSONObject();
                                                        //for(Object[] obj :currencyObjectList){
                                                        //    conversionRate.put(obj[2].toString(), obj[1]);
                                                       // }

                                                        //Procurement Type (i.e. NCT/ICT) retrieval
                                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                        List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                        boolean isIctTender = false;
                                                        if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                                            isIctTender = true;
                                                        }

                                                        //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                        String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                        /* Dohatec: ICT-Goods End */
                                                         %>
                                                         <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                            <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    String selected = "";
                                                                    if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                    out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                    
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    String selectedOption = "";
                                                                    if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                        selectedOption = "selected='selected'";
                                                                    }
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                            %>
                                                        </select>
                                                        <script language="javascript">
                                                            //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                            //combo_val_tableid.push("<%=tableId%>");
                                                            changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                        </script>
            <%                                             }

                                            }
                                            }
                                        }else{
                                           if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                           {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                           }
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                } else if(dataType == 1 || dataType == 2){
%>

<%
                                                } else if(dataType==12){
%>
                                                     readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                            }
                                            if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none; "
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
                                           if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
                                         %>
                                        />
                                        <%
                                        if(dataType==12 && fillBy[j]==2){
%>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','img_<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="img_<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align: middle;"
                                             />
<%
                                        }
                                        if(dataType==9 || dataType==10){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                        /* Dohatec: ICT-Goods Start */
                                                        //Get Currency List to be shown at 'Currency' Combo box
                                                        List<Object[]> currencyObjectList = null;
                                                        if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                            currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                        }
                                                       // JSONObject conversionRate = new JSONObject();
                                                       // for(Object[] obj :currencyObjectList){
                                                      //      conversionRate.put(obj[2].toString(), obj[1]);
                                                      //  }
                                                        //Procurement Type (i.e. NCT/ICT) retrieval
                                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                        List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                        boolean isIctTender = false;
                                                        if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))) {
                                                            isIctTender = true;
                                                        }

                                                        //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                        String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                        /* Dohatec: ICT-Goods End */
                                                         %>
                                                         <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                            <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    String selected = "";
                                                                    if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                    out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    String selectedOption = "";
                                                                    if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                        selectedOption = "selected='selected'";
                                                                    }
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                            %>
                                                        </select>
                                                        <script language="javascript" defer >
                                                            //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                            //combo_val_tableid.push("<%=tableId%>");
                                                            changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                        </script>
 <%                                             }

                                            }

                                        }
                                   }
                           }

                                if(dataType == 2){
                                    if(i == (rows)){
                                        if(FormulaCount > 0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                          %>
                                                      <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
                                                          <%
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                    inputtype = "text";
                                                }
                                            }
                                        }
                                    }

                                //out.print("cellId : "+cellData.getCellId());
                                    if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea rows="4" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtArea_1"></textarea>
<%
                                        if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                        }
                                    }else{
%>
                                        <textarea name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1"  <%if(isEdit || isView || isEncrypt){out.print(" readOnly ");}%> rows="4"></textarea>
<%

                                    }
                                }
                                if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "<%=dataType%>";
/* dipal: for bug id:5680*/
                                        if(arrColIds[<%=tableIndex%>].length != (parseInt('<%=cols%>')+1)){
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');
                                        }
				      /* */

                                    </script>
<%
                                }
                            }
/*Rokib Bidder Quoted Start*/
else if(fillBy[j] == 4 ){ if(i==1){
EvalCommonSearchService evalCommonSearchData = (EvalCommonSearchService) AppContext.getSpringBean("EvalCommonSearchService");
String GrandSumValue = evalCommonSearchData.GrandSumDiscountValue(request.getParameter("tenderId"), session.getAttribute("userId").toString(),request.getParameter("formId"));
%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" readonly class="formTxtBox_2" value="<%=GrandSumValue%>" />
<%}}
else if(fillBy[j] == 5 ){ if(i==1){%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" class="formTxtBox_2" value="" />
                                         <%}
                                                          
                            }
/*Rokib Bidder Quoted End*/

                        } // celldtl
%>
                </td>
<%
                    }
%>
            </tr>
<%
                } // if(i>0)
            } //for(row)
        }
        else // Start Edit here
        {
            int colId = 0;
            int cntBidTable = 0;
            int tmpRowId = 0;
            String PercentageValue = "0";
            String GrandSumValue = "0";
            String PercantageColumn = "0";
            EvalCommonSearchService evalCommonSearchData = (EvalCommonSearchService) AppContext.getSpringBean("EvalCommonSearchService");

            while(tblBidTableId.hasNext())
            {
                CommonFormData bidTableIdDt = tblBidTableId.next();
                cntBidTable++;
                while(tblCellsDtl.hasPrevious()){
                        tblCellsDtl.previous();
                }
                for (short i = -1; i <= rows; i++) {
                    if((i == 0) && (cntBidTable==1)){
%>
             <tr id="ColumnRow">
                 <% if(isMultiTable){ %>
                 <th>Select</th>
                 <% } %>
<%
                        for(short j=0;j<cols;j++){
                            if(tblColumnsDtl.hasNext()){
                                CommonFormData colData = tblColumnsDtl.next();

                                colHeader = colData.getColumnHeader();
                                colType = colData.getColumnType();
                                filledBy = colData.getFillBy();
                                showOrHide = Integer.parseInt(colData.getShowOrHide());
                                dataType = colData.getDataType();
                                colId = colData.getColumnId();
                                colData = null;
                            }
                            arrColType[j] = colType;
                            fillBy[j] = filledBy;
                            arrColId[j] = colId;
                            arrForShowHide[j] = showOrHide;
%>
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %>
                    <%if(showOrHide==2){%>
                    <script>
                        //document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                        document.getElementById("addTD<%= j + 1 %>").style.display = "none";
                    </script>
                    <%}%>
                    <script>
                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                    </script>
                </th>
<%
                        }
%>
             </tr>
<%
                    }
                    if(i > 0){
                        if(isTotal==1 && isMultiTable){
                            if(cntBidTable != cntRow && i==rows){
                                continue;
                            }else{
                                tmpRowId++;
                            }
                        }else{
                            tmpRowId++;
                        }
%>
             <tr id="row<%=tableId%>_<%=tmpRowId%>">
                 <% if(isMultiTable && i==1){ %>
                 <td class="t-align-center"><input type="checkbox" id="chk<%=tableId%>_<%=i%>" /></td>
                 <% }else if(isMultiTable){ %>
                 <td class="t-align-center">&nbsp;</td>
                 <% } %>
<%
                        int cnt = 0;
                        for(int j=0; j<cols; j++){
                            String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" align="center">
                    <%
                   // if(showOrHide==2){
                    /* Dohatec Start */
                    if(arrForShowHide[j]==2){
                    %>
                    <script>
                        //document.getElementById("td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>").visibility = "collapse";
                        document.getElementById("td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>").style.display = "none";
                    </script>

                <%  /* Dohatec End*/
                    } %>
<%
                            if(tblCellsDtl.hasNext()){
                                cnt++;
                                CommonFormData cellData = tblCellsDtl.next();
                                dataType = cellData.getCellDataType();
                                cellValue = cellData.getCellValue();
                                columnId = cellData.getColumnId();
                                cellId = cellData.getCellId();
                                if(true){   // Dohatec Encrypted data save  Prevent start
                                    %>
                                      <input type="hidden" name="row_DataType<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row_DataType<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" value="<%=dataType%>" />
                            <%              // Dohatec Encrypted data save  Prevent end
                                    String cValue = "";
                                    cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '';
                             </script>
<%
                                /* Dohatec Start*/
                                TenderCommonService tenderCommonServiceICT = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<SPTenderCommonData> listDP2 = tenderCommonServiceICT.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                boolean isIctTender2 = false;
                                if(!listDP2.isEmpty() && listDP2.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                                    isIctTender2 = true;
                                                                }
                                    if(grandTotalCell==cellId && !isIctTender2){
                                        out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                                    }
                                }
                                /* Dohatec End*/
                                if(fillBy[j] == 1){

%>
                                    <label  name="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType==11){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                        <%=cellValue.replaceAll("\n", "<br />") %>
                                                <%-- =cellValue --%>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                    if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                                    }
                                } else if(fillBy[j] == 2 || fillBy[j] == 3){
                                    if(dataType != 2){
                                        if(tmpRowId == (editedRowCount))
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(cntRow < TotalBidCount)
                                                    if(i%(rows - 1)==0)
                                                        break;

                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
                                                            %>
                                                        <textarea class="formTxtArea_1"
                                                                  rows="4"
                                                                readonly
								tabindex="-1"
								name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
								id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"><%
                                                                while(tblBidData.hasPrevious()){
                                                                    tblBidData.previous();
                                                                }
                                                                while(tblBidData.hasNext()){
                                                                    CommonFormData bidData = tblBidData.next();
                                                                    if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                        if(!"".equals(bidData.getCellValue())){
                                                                            if(bidData.getCellValue()!=null)
                                                                            {

                                                                                if ("null".equals(hashkey)) {
                                                                                    decryptedValue = bidData.getCellValue();
                                                                                }else{
                                                                                    SignerImpl obj = new SignerImpl();
                                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                                }

                                                                            }else
                                                                                decryptedValue = bidData.getCellValue();
                                                                            
                                                                            out.print(decryptedValue);
                                                                        }
                                                                     }
                                                                }
                                                            %></textarea>
<%
                                                        } else if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){
                                                            if(listCurrencyObj.size() > 0){
                                                            %>
                                                            <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                                <% } %>
                                                            </select>
                                                            <%
                                                          }
                                                        }else if((dataType==9 || dataType==10)){
                                                            List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                            TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                            listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                            if(listCellDetail.size() > 0){
                                                            TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                            List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                            /* Dohatec: ICT-Goods Start */
                                                            //Get Currency List to be shown at 'Currency' Combo box
                                                            List<Object[]> currencyObjectList = null;
                                                            if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                                currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                            }
                                                            
                                                            //Procurement Type (i.e. NCT/ICT) retrieval
                                                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                            boolean isIctTender = false;
                                                            if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                                isIctTender = true;
                                                            }

                                                            //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                            String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                            /* Dohatec: ICT-Goods End */
                                                             %>
                                                             <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');" style="display: none;">
                                                                <%  /* Dohatec: ICT-Goods Start */
                                                                if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                    for(Object[] obj :currencyObjectList){
                                                                        out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                                    }
                                                                }   /* Dohatec: ICT-Goods End */
                                                                else {
                                                                    for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                        String selectedOption = "";
                                                                        if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                            selectedOption = "selected='selected'";
                                                                        }
                                                                        out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                    }
                                                                }
                                                                %>
                                                            </select>
                <%                                          }
                                                        }else{
%>
                                                        <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                        inputtype = "text";
                                                }
                                            }
                                        }

                                       //out.print(" FillBy : "+fillBy[j]);
                                       //out.print(" Datatype : "+dataType);
                                       //out.print("tmpRowId : "+tmpRowId);
                                       //out.print("rows : "+rows);
                                       if(tmpRowId != (rows))
                                       {

                                           if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                           {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                           }
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                        if(isEdit)
                                        {
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                                }else if(dataType == 1 || dataType == 2){
%>

<%
                                                } else if(dataType==12){
%>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                               }  else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this);" readOnly
<%
                                               }
                                            }
                                        }
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){

                                                    if(bidData.getCellValue()!=null)
                                                    {

                                                        if ("null".equals(hashkey)) {
                                                            decryptedValue = bidData.getCellValue();
                                                        }else{
                                                            SignerImpl obj = new SignerImpl();
                                                            obj.setEncrypt(bidData.getCellValue());
                                                            decryptedValue = obj.getSymDecrypt(hashkey);
                                                        }

                                                    }else
                                                        decryptedValue = bidData.getCellValue();

%>
                                         value="<%=decryptedValue%>"
<%
                                                }
                                            }
                                            if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                            %>
                                                style="text-align: right;"
                                            <%
                                            }
                                           if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                          />
<%
                                            if(dataType==12 && fillBy[j]==2){
%>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align: middle;"
                                             />
<%
                                            }
                                            if((dataType==9 || dataType==10)){

                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                if(listCellDetail.size() > 0){
                                                TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                /* Dohatec: ICT-Goods Start */
                                                //Get Currency List to be shown at 'Currency' Combo box
                                                List<Object[]> currencyObjectList = null;
                                                if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                    currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                }
                                                String conversaionRate = "";
                                                if(currencyObjectList!=null)
                                                {
                                                    JSONObject currency = new JSONObject();
                                                    for(Object[] obj :currencyObjectList){
                                                        currency.put(obj[2].toString(), obj[1]);
                                                    }
                                                    conversaionRate= currency.toString();
                                                    conversaionRate = conversaionRate.replace("\"", "'");
                                                }

                                                //Procurement Type (i.e. NCT/ICT) retrieval
                                                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                boolean isIctTender = false;
                                                if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                    isIctTender = true;
                                                }

                                                //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                /* Dohatec: ICT-Goods End */
                                                 %>
                                                 <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>',1,<%=conversaionRate%>); txtReadOnlySBDCurrency(this);" style="display: none;">
                                                    <%  /* Dohatec: ICT-Goods Start */
                                                    if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                        for(Object[] obj :currencyObjectList){
                                                            out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                        }
                                                    }   /* Dohatec: ICT-Goods End */
                                                    else {
                                                        for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                            String selectedOption = "";
                                                            if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                selectedOption = "selected='selected'";
                                                            }
                                                            out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                        }
                                                    }
                                                    %>
                                                </select>
<%                                             }
                                       }
                                       }
                                       else if(tmpRowId == (rows))
                                       {
                                           //out.print("FormulaCount : "+FormulaCount);
                                           //out.print("isTotal : "+isTotal);
                                            if(FormulaCount > 0)
                                            {
                                                if(isTotal == 1)
                                                {
                                                    if(isMultiTable){
                                                     %>
                                                    <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                    <%if(fillBy[j] == 3){%>
                                                    readOnly
                                                    <%}%>
                                                    <%if(isView || isEncrypt){%>
                                                    readOnly
                                                    <%}%>
            <%
                                                                if(isEdit)
                                                                {
                                                                    if(fillBy[j] != 3){
                                                                        if(dataType==3){
            %>
                                                    onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
            <%
                                                                        }else if(dataType==4){
            %>
                                                    onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
            <%
                                                                        } else if(dataType==8){
            %>
                                                    onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
            <%
                                                                        }else if(dataType == 1 || dataType == 2){
            %>

            <%
                                                                        } else if(dataType==12){
            %>
                                                     readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
            <%
                                                                        } else if(dataType==11){
            %>
                                                    onBlur="chkFloatMinus5Plus5('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
            <%

                                                                        }else if(dataType==13){
            %>
                                                    onBlur="CheckNumeric3Decimal('<%=tableId%>',this);" readOnly
            <%
                                                           }
                                                                    }
                                                                }

                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){

                                                                            if(bidData.getCellValue()!=null)
                                                                            {

                                                                                if ("null".equals(hashkey)) {
                                                                                    decryptedValue = bidData.getCellValue();
                                                                                }else{
                                                                                    SignerImpl obj = new SignerImpl();
                                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                                }

                                                                            }else
                                                                                decryptedValue = bidData.getCellValue();
            %>
                                                     value="<%=decryptedValue%>"
            <%
                                                                        }
                                                                    }

                                                        if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                                            %>
                                                                style=" text-align: right;"
                                                            <%
                                                            }
                                                    if(dataType == 12){ %> class="formTxtDate_1" <% }
                                                    else { %> class="formTxtBox_2" <% }
            %>
                                                    />

            <%
                                                            if(dataType==12 && fillBy[j]==2){
%>
                                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                                 onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                                 id="img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                                 style="vertical-align: middle;"
                                                             />
<%
                                                            }
                                                            if((dataType==9 || dataType==10)){
                                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                                    if(listCellDetail.size() > 0){
                                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                                    /* Dohatec: ICT-Goods Start */
                                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                                    List<Object[]> currencyObjectList = null;
                                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                                    }

                                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                    List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                                    boolean isIctTender = false;
                                                                    if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                                        isIctTender = true;
                                                                    }

                                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                                    /* Dohatec: ICT-Goods End */
                                                                     %>
                                                                     <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');" style="display: none;">
                                                                        <%  /* Dohatec: ICT-Goods Start */
                                                                        if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                            for(Object[] obj :currencyObjectList){
                                                                                out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                                            }
                                                                        }   /* Dohatec: ICT-Goods End */
                                                                        else {
                                                                            for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                                String selectedOption = "";
                                                                                if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                                    selectedOption = "selected='selected'";
                                                                                }
                                                                                out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                            }
                                                                        }
                                                                        %>
                                                                    </select>
                        <%                                             }

                                                            }
                                                    }else{
                                                        if(arrFormulaColid[columnId-1] == columnId){
                                                            /*Dohatec Start*/
                                                            TenderCommonService tenderCommonServiceICT3 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                List<SPTenderCommonData> listDP1 = tenderCommonServiceICT3.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                                boolean isIctTender3 = false;
                                                                if(!listDP1.isEmpty() && listDP1.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                                    isIctTender3= true;
                                                                }
                                                                if(!isIctTender3){
                                                                    /*Dohatec End*/
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                                            }else if(dataType == 1 || dataType == 2){
%>

<%
                                                            } else if(dataType==12){
%>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%

                                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this);" readOnly
<%
                                               }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId) && (!isIctTender3 /*&& ((int)cellData.getCellId() == (grandTotalValueCell))*/) ) //need clarification
                                                            {

                                                                if(bidData.getCellValue()!=null)
                                                                {

                                                                    if ("null".equals(hashkey)) {
                                                                        decryptedValue = bidData.getCellValue();
                                                                    }else{
                                                                        SignerImpl obj = new SignerImpl();
                                                                        obj.setEncrypt(bidData.getCellValue());
                                                                        decryptedValue = obj.getSymDecrypt(hashkey);
                                                                    }

                                                                }else
                                                                    decryptedValue = bidData.getCellValue();
                                                                %>
                                         value="<%=decryptedValue%>"
<%
                                                            }
                                                        }

                                            if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                                %>
                                                    style="text-align: right;"
                                                <%
                                                }
                                          if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                        />

<%
        }
                                                if(dataType==12 && fillBy[j]==2){
%>
                                                <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                     onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                     id="img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                     style="vertical-align: middle;"
                                                 />
    <%
                                                }
                                                if((dataType==9 || dataType==10) && !isView){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                        /* Dohatec: ICT-Goods Start */
                                                        //Get Currency List to be shown at 'Currency' Combo box
                                                        List<Object[]> currencyObjectList = null;
                                                        if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                            currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                        }

                                                        //Procurement Type (i.e. NCT/ICT) retrieval
                                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                        List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                        boolean isIctTender = false;
                                                        if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                            isIctTender = true;
                                                        }

                                                        //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                        String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                        /* Dohatec: ICT-Goods End */
                                                         %>
                                                         <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');" style="display: none;">
                                                            <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    String selectedOption = "";
                                                                    if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                        selectedOption = "selected='selected'";
                                                                    }
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                            %>
                                                        </select>
            <%                                             }

                                                            }
                                                        }
                                                    }
                                                }else{
                                            if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                           {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                           }
                                            /*Dohatec Start*/
                                         /*   TenderCommonService tenderCommonService4 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                             List<SPTenderCommonData> listDP4 = tenderCommonService4.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                             boolean isIctTender4 = false;
                                                        if(!listDP4.isEmpty() && listDP4.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                            isIctTender4 = true;
                                                        }*/
                                             /*Dohatec End */
                                            // commented to solve the issue : encrypt the form e-5A8 Staffing Schedule by Rokibul
                                          //  if(!isIctTender4 && tmpRowId!= rows){
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                                            }else if(dataType == 1 || dataType == 2){
%>

<%
                                                            } else if(dataType==12){
%>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%

                                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this);" readOnly
<%
                                               }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
                                                            {

                                                                if(bidData.getCellValue()!=null)
                                                                {

                                                                    if ("null".equals(hashkey)) {
                                                                        decryptedValue = bidData.getCellValue();
                                                                    }else{
                                                                        SignerImpl obj = new SignerImpl();
                                                                        obj.setEncrypt(bidData.getCellValue());
                                                                        decryptedValue = obj.getSymDecrypt(hashkey);
                                                                    }

                                                                }else
                                                                    decryptedValue = bidData.getCellValue();
%>
                                         value="<%=decryptedValue%>"
<%
                                                            }
                                                        }
                                            if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                            %>
                                                style=" text-align: right;"
                                            <%
                                            }
                                            if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                        />
<%//}
                                                    if(dataType==12 && fillBy[j]==2){
%>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align: middle;"
                                                     />
<%
                                                    }
                                                     if((dataType==9 || dataType==10)){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                        /* Dohatec: ICT-Goods Start */
                                                        //Get Currency List to be shown at 'Currency' Combo box
                                                        List<Object[]> currencyObjectList = null;
                                                        if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                            currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                        }

                                                        //Procurement Type (i.e. NCT/ICT) retrieval
                                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                        List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                        boolean isIctTender = false;
                                                        if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                            isIctTender = true;
                                                        }

                                                        //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                        String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                        /* Dohatec: ICT-Goods End */
                                                         %>
                                                         <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');" style="display: none;">
                                                            <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    String selectedOption = "";
                                                                    if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                        selectedOption = "selected='selected'";
                                                                    }
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                            %>
                                                        </select>
            <%                                             }

                                                }
                                                }
                                            }else{
                                                if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]) && !isView)
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                            if(isEdit)
                                            {
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" readOnly
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                                    }else if(dataType == 1 || dataType == 2){
%>

<%
                                                } else if(dataType==12){
%>
                                                    readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this, '<%=tableIndex%>');" readOnly
<%
                                                   }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this);" readOnly
<%
                                               }
                                                }
                                            }

                                                while(tblBidData.hasPrevious()){
                                                    tblBidData.previous();
                                                }
                                                while(tblBidData.hasNext()){
                                                    CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){

                                                            if(bidData.getCellValue()!=null){

                                                                if ("null".equals(hashkey)) {
                                                                    decryptedValue = bidData.getCellValue();
                                                                }else{
                                                                    SignerImpl obj = new SignerImpl();
                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                }

                                                            }else
                                                                    decryptedValue = bidData.getCellValue();
%>
                                         value="<%=decryptedValue%>"
<%
                                                        }
                                                    }
                                                if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                            %>
                                                style=" text-align: right;"
                                            <%
                                            }
                                                if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                         />
<%
                                                if(dataType==12 && fillBy[j]==2){
%>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="img_<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align: middle;"
                                                     />
<%
                                                }
                                                if((dataType==9 || dataType==10)){
                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0){
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                        isIctTender = true;
                                                    }

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
                                                     %>
                                                     <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');" style="display: none;">
                                                        <%  /* Dohatec: ICT-Goods Start */
                                                        if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                            for(Object[] obj :currencyObjectList){
                                                                out.print("<option value='" + obj[2] + "'>" + obj[3] + "</option>");
                                                            }
                                                        }   /* Dohatec: ICT-Goods End */
                                                        else {
                                                            for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                String selectedOption = "";
                                                                if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                    selectedOption = "selected='selected'";
                                                                }
                                                                out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                            }
                                                        }
                                                        %>
                                                    </select>
        <%                                             }

                                            }
                                        }
                                    }
                                    }

                                    if(dataType == 2){
                                        if(tmpRowId == (rows)){
                                            if(FormulaCount > 0){
                                                if(isTotal == 1){
                                                    if(!isMultiTable){
                                                           if(arrFormulaColid[columnId-1] != columnId){
                                                                if(arrWordFormulaColid[columnId-1] == columnId){
                                                                  %>
                                                              <textarea class="formTxtArea_1"
                                                                    readonly
                                                                    tabindex="-1"
                                                                    rows="4"
                                                                    name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                    id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                ><%
                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){

                                                                            if(bidData.getCellValue()!=null){

                                                                                if ("null".equals(hashkey)) {
                                                                                    decryptedValue = bidData.getCellValue();
                                                                                }else{
                                                                                    SignerImpl obj = new SignerImpl();
                                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                                }

                                                                            }else
                                                                                    decryptedValue = bidData.getCellValue();

                                                                            
                                                                            
                                                                            out.print(decryptedValue);
                                                                        }
                                                                    }
                                                                    %></textarea>
                                                                  <%
                                                            }
                                                            out.print("</td>");
                                                            continue;
                                                        }else{
                                                           inputtype = "text";
                                                        }
                                                    }else{
                                                          if(arrWordFormulaColid[columnId-1] == columnId){
                                                                  %>
                                                              <textarea class="formTxtArea_1"
                                                                    readonly
                                                                    tabindex="-1"
                                                                    rows="4"
                                                                    name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                    id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                ><%
                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                            
                                                                            if(bidData.getCellValue()!=null){

                                                                                if ("null".equals(hashkey)) {
                                                                                    decryptedValue = bidData.getCellValue();
                                                                                }else{
                                                                                    SignerImpl obj = new SignerImpl();
                                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                                }

                                                                            }else
                                                                                    decryptedValue = bidData.getCellValue();
                                                                            
                                                                            out.print(decryptedValue);
                                                                        }
                                                                    }
                                                                    %></textarea>
                                                                  <%
                                                            out.print("</td>");
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                          if(fillBy[j] == 3){
                                              if(tmpRowId!=rows){
                                                  //11thJan2012 Yagnesh: Added below condition because it was showing Null with textarea in last row for column in which we are displaying "Grand Total" Caption (isMultipleTable is true)
                                                   if(grandTotalCell != (int)cellData.getCellId()){
%>
                                        <label name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);"  readOnly rows="4" class="formTxtArea_1"><%
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                    
                                                    if(bidData.getCellValue()!=null){

                                                        if ("null".equals(hashkey)) {
                                                            decryptedValue = bidData.getCellValue();
                                                        }else{
                                                            SignerImpl obj = new SignerImpl();
                                                            obj.setEncrypt(bidData.getCellValue());
                                                            decryptedValue = obj.getSymDecrypt(hashkey);
                                                        }

                                                    }else
                                                            decryptedValue = bidData.getCellValue();
                                                    
                                                    out.print(decryptedValue);
                                                }
                                            }
%></textarea>
<%
                                            if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                            }
                                            }
                                           }else{
                                                  /*Change on 17-Dec-2011 for eventum issue id #4949*/
                                                             if(grandTotalCell != (int)cellData.getCellId()){
                                                    %>
                                        <label name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);"  readOnly class="formTxtArea_1" rows="4"><%
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                    
                                                    if(bidData.getCellValue()!=null){

                                                        if ("null".equals(hashkey)) {
                                                            decryptedValue = bidData.getCellValue();
                                                        }else{
                                                            SignerImpl obj = new SignerImpl();
                                                            obj.setEncrypt(bidData.getCellValue());
                                                            decryptedValue = obj.getSymDecrypt(hashkey);
                                                        }

                                                    }else
                                                            decryptedValue = bidData.getCellValue();
                                                    
                                                    out.print(decryptedValue);
                                                }
                                            }
%></textarea>
<%
                                            if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                                }
                                            }
                                            /*Changes Complited*/
                                           }
                                        }else{
                                            if(isMultiTable && isTotal==1){
                                                if(tmpRowId!=(editedRowCount)){
%>
<textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" rows="4"
                                                onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1" <%if(isEdit || isView || isEncrypt){out.print("readOnly ");}%>><%
                                                    while(tblBidData.hasPrevious()){
                                                        tblBidData.previous();
                                                    }
                                                    while(tblBidData.hasNext()){
                                                        CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                            
                                                            if(bidData.getCellValue()!=null){

                                                                if ("null".equals(hashkey)) {
                                                                    decryptedValue = bidData.getCellValue();
                                                                }else{
                                                                    SignerImpl obj = new SignerImpl();
                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                }

                                                            }else
                                                                    decryptedValue = bidData.getCellValue();
                                                            
                                                            out.print(decryptedValue);
                                                        }
                                                    }
        %></textarea>
        <%
                                                   }
                                            }else{
%>
                                                <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" rows="4"
                                                onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1"  <%if(isEdit || isView || isEncrypt){out.print("readOnly");}%>><%
                                                    while(tblBidData.hasPrevious()){
                                                        tblBidData.previous();
                                                    }
                                                    while(tblBidData.hasNext()){
                                                        CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                            
                                                            if(bidData.getCellValue()!=null){

                                                                if ("null".equals(hashkey)) {
                                                                    decryptedValue = bidData.getCellValue();
                                                                }else{
                                                                    SignerImpl obj = new SignerImpl();
                                                                    obj.setEncrypt(bidData.getCellValue());
                                                                    decryptedValue = obj.getSymDecrypt(hashkey);
                                                                }

                                                            }else
                                                                    decryptedValue = bidData.getCellValue();
                                                            
                                                            out.print(decryptedValue);
                                                        }
                                                    }
        %></textarea>
        <%
                                            }

                                        }

                                    }
                                    if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "<%=dataType%>";
/* dipal: for bug id:5680*/
                                        if(arrColIds[<%=tableIndex%>].length != (parseInt('<%=cols%>')+1)){
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');
                                        }
/**/
                                    </script>
<%
                                    }
                                }
/*Rokib Bidder Quoted Start*/
else if(fillBy[j] == 4 ){ if(i==1){
GrandSumValue = evalCommonSearchData.GrandSumDiscountValue(request.getParameter("tenderId"), session.getAttribute("userId").toString(),request.getParameter("formId"));
if(request.getParameter("hashKey")!=null && !("null".equals(hashkey))) {
%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"  readonly class="formTxtBox_2" value="<%=GrandSumValue%>" /> 

<%} else {
%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"  readonly class="formTxtBox_2" value="0" /> 
<%}%>
<input type="hidden" name="row_FillBy<%=tableId%>_<%=i%>_<%=columnId%>" id="row_FillBy<%=tableId%>_<%=i%>_<%=columnId%>" value="<%=fillBy[j]%>" />
<%
}}

else if(fillBy[j] == 5 ){ if(i==1){
PercentageValue = evalCommonSearchData.PercantageValue(request.getParameter("tenderId"), session.getAttribute("userId").toString(),request.getParameter("formId"),request.getParameter("bidId"),String.valueOf(i));
PercentageValue = String.valueOf(Double.valueOf(PercentageValue)*100/Double.valueOf(GrandSumValue));
if(!PercentageValue.equalsIgnoreCase("0")){
PercentageValue=String.format("%.3f", Double.valueOf(PercentageValue));
}
if(request.getParameter("hashKey")!=null && !("null".equals(hashkey))) {
%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" class="formTxtBox_2" value="<%=PercentageValue%>" />
<%} else {
%>
<input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" readonly class="formTxtBox_2" value="0" /> 
<%}%>
<input type="hidden" name="row_FillBy<%=tableId%>_<%=i%>_<%=columnId%>" id="row_FillBy<%=tableId%>_<%=i%>_<%=columnId%>" value="<%=fillBy[j]%>" />
<%
}}
/*Rokib Bidder Quoted End*/
                            } // celldtl
%>
                </td>
<%
                        }
%>
            </tr>
<%
                    }
                } // edn of for loop for row
            }
        }
%>
        </tbody>
        <div>
        <tr>
            <td colspan="<%=cols%>" style="display:none">
                <label class="formBtn_1" id="lblDeleteTable">
                    <input type="button" name="btnDel<%=tableId%>" id="bttnDel<%=request.getParameter("tableIndex")%>" value="Delete Record" onClick="DelTable(this.form,<%=tableId%>,this,'<%=isTotal%>');"/>
                </label>
                <label class="formBtn_1" id="lblAddTable">
                    <input type="button" name="btn<%=tableId%>" id="bttn<%=request.getParameter("tableIndex")%>" value="Add Record" onClick="AddBidTable(this.form,<%=tableId%>,this)"/>
                </label>
            </td>
        </tr>
        </div>
    </table>
                <input type="hidden" value="<%=tableId%>" id="hdnTblNum" name="hdnTblNum">
                </div>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>