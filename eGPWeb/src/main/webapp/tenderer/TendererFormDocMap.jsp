<%-- 
    Document   : TendererFormDocMap
    Created on : Dec 15, 2010, 5:51:45 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Map from Document Library</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <style>
            .ui-jqgrid .ui-jqgrid-htable th div {
                height:auto;
                overflow:hidden;
                padding-right:0px;
                padding-top:0px;
                position:relative;
                vertical-align:text-top;
                white-space:normal !important;
            }
        </style>
       
    </head>

    <body onunload="removeMsg();">        
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>



                <%
                    int userId = 0;
                    int tendererId = 0;
                    int tenderId = 0;
                    int formId = 0;
                    String type = "";
                    String title = "";
                    String strMessage="";
                    if (session.getAttribute("userId") != null)
                    {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                    if (request.getParameter("tenderId") != null)
                    {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("formId") != null)
                    {
                        formId = Integer.parseInt(request.getParameter("formId"));
                    }
                    String lotid = "";
                    if (request.getParameter("lotId") != null)
                    {
                        lotid = request.getParameter("lotId");
                    }

                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");


                    if ("Map".equalsIgnoreCase(request.getParameter("btnMap")))
                    {

                        String maplotid = "";
                        if (request.getParameter("maplotid") != null)
                        {
                            maplotid = request.getParameter("maplotid");
                        }
                        String docIds = "";
                        String[] dIds = null;
                        if (request.getParameterValues("docIds") != null)
                        {
                            dIds = request.getParameterValues("docIds");
                        }
                        //dIds = docIds.split(",");

                        // Coad added by Dipal for Audit Trail Log.
                        String idType = "tenderId";
                        int auditId = tenderId;
                        String auditAction = "Tender has maped required document to Forms";
                        String moduleName = EgpModule.Bid_Submission.getName();
                        String remarks = "Tenderer(User id): " + session.getAttribute("userId") + " has maped required document to Form id:" + formId;
                        String docMsg = "";
                        String docId = "";
                        try
                        {
                            for (int i = 0; i < dIds.length; i++)
                            {
                                if(briefcaseDoc.isTendererDocumentExistsOrNot(String.valueOf(dIds[i]), String.valueOf(userId))){
                               docMsg = briefcaseDoc.formMapDocData(tenderId, formId, userId, Integer.parseInt(dIds[i]), request.getParameter("manDocId"));
                               }
                                else {
                                docMsg = "Exception. File not found. Please upload another File";
                                }
                               if(!docMsg.contains("mapped") && !docMsg.contains("Exception"))
                               {
                                    strMessage = docMsg+", "+strMessage;
                               }
                               else
                               {
                                    docId = dIds[i]+", "+docId;
                               }
                            }
                            if(!docMsg.contains("mapped") && !docMsg.contains("Exception"))
                            {
                                    strMessage = strMessage.substring(0, strMessage.length()-2);
                                    strMessage = strMessage + " document(s) are already mapped.";
                            }
                            else
                            {
                                strMessage = docMsg;
                            }
                        }
                        catch (Exception ex)
                        {
                            auditAction = "Error in Tender has maped required document to Forms " + ex.getMessage();
                        }
                        finally
                        {
                            docId = (docId.length() > 3 && !docId.equals("")? docId.substring(0, docId.length()-2):"");
                            remarks = remarks + " docsID: "+docId;
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        }

                %>
                <script>
                    jAlert("Document mapped successfully.","Map Document", function(RetVal) {
                    });
                </script>
                <%
                        response.sendRedirect("TendererFormDocMap.jsp?tenderId=" + tenderId + "&formId=" + formId + "&msg=1&lotId=" + maplotid + "&val="+strMessage);
                    }
                    else if ("Remove".equalsIgnoreCase(request.getParameter("btnMap")))
                    {
                        // Coad added by Dipal for Audit Trail Log.
                        String idType = "tenderId";
                        int auditId = tenderId;
                        String auditAction = "Tender has removed required document from Forms";
                        String moduleName = EgpModule.Bid_Submission.getName();
                        String remarks = "Tenderer(User id): " + session.getAttribute("userId") + " has removed required document from Form id:" + formId;
                        String docId = "";
                        String removelotid = "";
                        if (request.getParameter("removelotid") != null)
                        {
                            removelotid = request.getParameter("removelotid");
                        }
                        String docIds = "";
                        String[] dIds = null;
                        if (request.getParameterValues("docIds") != null)
                        {
                            dIds = request.getParameterValues("docIds");
                        }
                        //dIds = docIds.split(",");
                        try
                        {

                            for (int i = 0; i < dIds.length; i++)
                            {
                                briefcaseDoc.removeMapDocData(formId, userId, Integer.parseInt(dIds[i]));
                                docId = dIds[i]+", "+docId;
                            }
                        }
                        catch (Exception ex)
                        {
                            auditAction = "Error in Tender has removed required document from Forms " + ex.getMessage();
                        }
                        finally
                        {
                            docId = (docId.length() > 3 && !docId.equals("")? docId.substring(0, docId.length()-2):"");
                            remarks = remarks + " docsID: "+docId;
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        }

                        response.sendRedirect("TendererFormDocMap.jsp?tenderId=" + tenderId + "&formId=" + formId + "&msg=2&lotId=" + removelotid + "");
                    }


                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null)
                    {
                        if (!"".equalsIgnoreCase(request.getParameter("userType")))
                        {
                            userType.append(request.getParameter("userType"));
                        }
                        else
                        {
                            userType.append("org");
                        }
                    }
                    else
                    {
                        userType.append("org");
                    }



                    tendererId = briefcaseDoc.getTendererIdFromUserId(userId);


                    TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");
                    TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
                    List<Object[]> mandDocList = templateForm.getTendMandDocs(request.getParameter("tenderId"), request.getParameter("formId"));



                %>


                <div class="dashboard_div">
                    <!--Dashboard Header Start-->
                    <div class="topHeader">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr valign="top">
                                <%--<jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                                    <jsp:param name="userType" value="<%=userType.toString()%>"/>
                                </jsp:include>--%>
                                <td class="contentArea_1">
                                    <!--Page Content Start-->
                                    <div class="pageHead_1">
                                        Map from Document Library
                                        <%
                                            //if("1".equalsIgnoreCase(strpg))
                                            //{
                                            if ("null".equalsIgnoreCase(request.getParameter("lotId")))
                                            {
                                        %>
                                        <span class="c-alignment-right">  <a class="action-button-goback" href="LotTendPrep.jsp?tab=4&tenderId=<%=tenderId%>"> Go Back to Dashboard</a></span>
                                        <%}
                                        else
                                        {%>
                                        <span class="c-alignment-right">  <a class="action-button-goback" href="BidPreperation.jsp?tab=4&lotId=<%=request.getParameter("lotId")%>&tenderId=<%=tenderId%>"> Go Back to Dashboard</a></span>
                                        <%
                                            }
                                        %>
                                    </div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr valign="top">
                                            <td>

                                                <!--Dashboard Header End-->

                                                <!--Dashboard Content Part Start-->
                                                <div>&nbsp;</div>
                                                <%
                                                    if (request.getParameter("flag") != null)
                                                    {
                                                        if ("true".equalsIgnoreCase(request.getParameter("flag")))
                                                        {
                                                %>
                                                <div id="docMsg" class="responseMsg successMsg">Document Uploaded and Mapped Successfully.</div>
                                                <%                    }
                                                    }
                                                    if (request.getParameter("msg") != null)
                                                    {
                                                        if ("2".equalsIgnoreCase(request.getParameter("msg")))
                                                        {
                                                %>
                                                <div id="docMsg" class="responseMsg successMsg">Document Unmapped.</div>
                                                <%                    }
                                                    }
                                                    if (request.getParameter("msg") != null)
                                                    {
                                                        if ("1".equalsIgnoreCase(request.getParameter("msg")))
                                                        {
                                                            if(request.getParameter("val").equalsIgnoreCase("mapped")){
                                                %>
                                                <div id="docMsg" class="responseMsg successMsg">Document Mapped Successfully.</div>
                                                <%                    }else{%>
                                                     <div id="docMsg" class="responseMsg errorMsg"><%=request.getParameter("val")%></div>
                                                <%}
                                                        }
                                                    }
                                                    if (request.getParameter("fq") != null)
                                                    {
                                                %>
                                                <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                                                <%
                                                    }
                                                    if (request.getParameter("fs") != null)
                                                    {
                                                %>
                                                <div class="responseMsg errorMsg">
                                                    Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed,Please try again.
                                                </div>
                                                <%
                                                    }
                                                    if (request.getParameter("sf") != null)
                                                    {
                                                %>
                                                <div class="responseMsg errorMsg">
                                                    File already exist.
                                                </div>
                                                <%                }
                                                    if (request.getParameter("fup") != null)
                                                    {
                                                %>
                                                <div class="responseMsg errorMsg">
                                                    Problem has been occurred during file upload.
                                                </div>
                                                <%
                                                }
                                                    if (request.getParameter("msg") != null && "hashfail".equalsIgnoreCase(request.getParameter("msg")))
                                                    {
                                                %>
                                                <div class="responseMsg errorMsg">
                                                    Hash/e-Signature of this document could not be generated. Please try again.
                                                </div>
                                                <%}%>
                                                
                                                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                                <%
                                                     SpgetCommonSearchDataMore tenderCommonService = (SpgetCommonSearchDataMore) AppContext.getSpringBean("SpgetCommonSearchDataMore");
                                                     List<SPCommonSearchDataMore> mapDocList = tenderCommonService.executeProcedure("GetBidderDocuments", "" + userId, "" + formId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                %>
                                                <form id="form1" method="post">
                                                    <tr>
                                                        <td colspan="4">
                                                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                                <tr>
                                                                    <th  class="t-align-center">Instructions</th>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">Upload the documents in black/white resolution  with 75-100 DPI only, unless higher DPI is required.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">Advised to extract and verify the contents of the zipped files to avoid disqualifications.</td>
                                                                </tr>
                                                                <tr>
                                                                    <!--Edited
                                                                    <td class="t-align-left">Authenticity and validity of the uploaded documents and Content of the uploaded documents remains with the bidder/consultant. Failure to upload authentic document may result in the violation of PPA-2006 and PPR-2008 and the bidder/consultant will be responsible for all consequences</td>
                                                                    -->
                                                                    <td class="t-align-left">Authenticity and validity of the uploaded documents and Content of the uploaded documents remains with the bidder/consultant. Failure to upload authentic document may result in the violation of the Procurement Rules and the bidder/consultant will be responsible for all consequences</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">Acceptable File Types <span class="mandatory">(<%=tblConfigurationMaster.getAllowedExtension()%>)</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="t-align-left">Click on <span class="mandatory">MAP</span> link available in front of uploaded document to map it with a <span class="mandatory">Folder</span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <ul class="tabPanel_1">
                                                                <li id="fetchunmapped" ><a href="#label" class="sMenu" id="0" onclick="checkSatus('unmap');" >View Unmapped Files</a></li>
                                                                <li id="fetchdata"><a href="#label" id="1" onclick="checkSatus('folderwise');">View Folder wise Files</a></li>
                                                                <li id="fetchalldata"><a href="#label" id="2" onclick="checkSatus('allfile');">View Files</a></li>
                                                                <!--                                <li id="fetchmapfilewithfolder"><a href="#label" id="4" onclick="checkSatus('mapfile');">Mapped Files</a></li>-->
                                                                <li id="fetcharchivefile"><a href="#label" id="3" onclick="checkSatus('archivefile');">View Archive Files</a></li>
                                                            </ul>
                                                            <%--<div class="tabPanelArea_1">--%>
                                                            <div class="tabPanelArea_1" >
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                                                    <tr id="tr1" style="display:none">
                                                                        <%--<td id="td1" class="t-align-left ff"></td>--%>
                                                                        <td id="td2" class="t-align-left ff">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" cellspacing="0" cellpadding="0" class="t_space">
                                                                    <tr>
                                                                        <td>
                                                                            <div id="jqGrid" class="" >
                                                                            </div>
                                                                        </td>                                   
                                                                    </tr>
                                                                </table>                                                                    
                                                            </div>


                                                        </td>

                                                    </tr>
                                                </form>
                                                </table>
                                </td>

                            </tr>
                        </table>
                        <!--Page Content End-->
                        </td>
                        </tr>
                        </table>               
                        <form id="frmMap" name="frmMap" method="post"  >
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                <input type="hidden" name="maplotid" id="maplotid" value="<%=lotid%>" />
                                <td class="t-align-center ff" id="trMapBtn">
                                    <%if (mandDocList.isEmpty())
                                        {
                                            out.print("<input type='hidden' name='manDocId' id='manDocId' value='0'/>");
                                        }
                                        else
                                        {%>
                                    <select name="manDocId" class="formTxtBox_1" id="manDocId">
                                        <%
                                            for (Object[] dataObj : mandDocList)
                                            {
                                                boolean ToADD = true;
                                                for(SPCommonSearchDataMore bidderDocToBeRemoved : mapDocList)
                                                {
                                                    if(bidderDocToBeRemoved.getFieldName12().equalsIgnoreCase(dataObj[1].toString()))
                                                    {
                                                        ToADD = false; 
                                                    }
                                                }
                                                if(ToADD)
                                                {
                                                    //Emtaz on 24/April/2016
                                                    String NIDtoCID = dataObj[1].toString().replaceAll("National ID", "CID");
                                                    NIDtoCID = NIDtoCID.replaceAll("NID", "CID");
                                                    out.print("<option value='" + dataObj[0] + "'>" + NIDtoCID + "</option>");
                                                }
                                            }
                                            out.print("<option value='0'>Other</option>");
//                                            for (Object[] dataObj : mandDocList)
//                                            {
//                                                out.print("<option value='" + dataObj[0] + "'>" + dataObj[1] + "</option>");
//                                            }
//                                            out.print("<option value='0'>Other</option>");
                                        %>
                                    </select>                                
                                    <%}%>
                                    <label class="formBtn_1">
                                        <input type="button" name="btnMap" id="btnMap" value="Map" onclick="return mapDocumentData();" />
                                    </label>
                                </td>
                                </tr>
                                <tr>
                                <input type="hidden" name="formId" id="formId" value="<%=formId%>" />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                                </tr>
                            </table>
                        </form>
                        <div style="width: 1100px; height: 250px; margin-top: 50px" class="">
                        <%
                            if (!mandDocList.isEmpty())
                            {
                                int doc_total = mandDocList.size();
                                int doc_pending = (int) briefcaseDoc.getRemainingDocCount("" + tenderId, "" + formId, "" + userId);
                                int doc_uploaded = doc_total - doc_pending;
                        %>
                        
                            <div style="float: left; width: 350px; height: 250px" class="">
                                <table class="tableList_1 t_space" style="border-width:0;">
                                    <tr><td style="border-width:0;">&nbsp;</td>
                                        <td style="border-width:0;padding-left: 8px;" >
                                            <table class="tableList_1 t_space">
                                                <tr>
                                                    <th colspan="2" >Uploaded Documents</th>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Pending Required Documents</td>
                                                    <td class="t-align-center ff" style="color: red;"><%=doc_pending%></td>
                                                </tr>
                                                <tr>
                                                    <th>Total Required Documents</th>
                                                    <th>Mapped Documents</th>
                                                <tr>
                                                    <td class="t-align-center ff"><%=doc_total%></td>
                                                    <td class="t-align-center ff"><%=doc_uploaded%></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                             <%}%>
                            <div style="height: 250px; width: 650px; float: right" class="">
                                <form id="frmFileUp" method="post" enctype="multipart/form-data"  action="<%=request.getContextPath()%>/TendererFileUploadComDoc?funName=mapDocForm">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                        <tr>
                                            <input type="hidden"  name="funName"  id="funName"  value="mapDocForm" />
                                            <input type="hidden"  name="tenderId"  id="tenderId"  value="<%=tenderId%>" />
                                            <input type="hidden"  name="formId"  id="formId"  value="<%=formId%>" />
                                            <input type="hidden"  name="userId"  id="userId"  value="<%=userId%>" />
                                            <input type="hidden"  name="lotId"  id="lotId"  value="<%=lotid%>" />                        

                                            <td colspan="4" align="left">Field marked (<span class="mandatory">*</span>) are mandatory.</td>
                                        </tr>
                                        <tr>
                                            <td width="30%" class="ff">Select a file to upload : <span>*</span></td>
                                            <td width="70%"><input name="fileUpload" id="fileUpload" onblur="//checkfile();" type="file" class="formTxtBox_1" style="width:400px; background:none;"  class="formTxtBox_1"/>
                                                <br/><span id="fileerror"></span></td>
                                            <!--<td width="15%"></td>-->
                                            <!--<td width="35%"></td>-->
                                        </tr>

                                        <%if (mandDocList.isEmpty())
                                            {
                                                out.print("<input type='hidden' name='manDocId' id='manDocId' value='0'/>");
                                            }
                                            else
                                            {%>
                                        <tr>
                                            <td width="30%" class="ff">Document Type :<span>*</span></td>
                                            <td width="70%" class="txt_2">
                                                <div id="something" style="">
                                                <select name="manDocId" class="formTxtBox_1" id="manDocId" style="width: 415px">

                                                    <%
                                                        for (Object[] dataObj : mandDocList)
                                                        {
                                                            boolean ToADD = true;
                                                            for(SPCommonSearchDataMore bidderDocToBeRemoved : mapDocList)
                                                            {
                                                                if(bidderDocToBeRemoved.getFieldName12().equalsIgnoreCase(dataObj[1].toString()))
                                                                {
                                                                    ToADD = false; 
                                                                }
                                                            }
                                                            if(ToADD)
                                                            {
                                                                //Emtaz on 24/April/2016
                                                                String NIDtoCID = dataObj[1].toString().replaceAll("National ID", "CID");
                                                                NIDtoCID = NIDtoCID.replaceAll("NID", "CID");
                                                                out.print("<option value='" + dataObj[0] + "'>" + NIDtoCID + "</option>");
                                                            }
                                                        }
                                                        out.print("<option value='0'>Other</option>");
                                                    %>                                
                                                </select>
                                                </div>
                                            </td>
                                            <!--<td width="15%"></td>-->
                                            <!--<td width="35%"></td>-->
                                        </tr>
                                        <%}%>
                                        <tr>
                                            <td width="30%" class="ff">Description :<span>*</span></td>
                                            <td width="70%" class="txt_2"><input name="documentBrief" id="txtDescription" type="text" class="formTxtBox_1" id="textfield62" size="50" onkeyup="//checkfile();" style="width:200px;" />
                                                <span id="descriptionerror"></span></td>
<!--                                            <td width="15%"></td>
                                            <td width="35%"></td>-->
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="t-align-center">
                                                <!--                            <label class="formBtn_1"><input type="submit" name="button" id="button" value="Upload" onclick="return checkfile()"/></label></td>-->
                                                <label class="formBtn_1"><input type="submit" name="button" id="button" value="Upload" onclick="return validate()"/></label></td>

                                            <!--<td width="15%">-->

                                                <input type="hidden"  name="tendererId"  id="hidtendererId"  value="<%=tendererId%>" />
                                            <!--</td>-->
                                            <!--<td width="15%"></td>-->
                                            <!--<td width="35%"></td>-->
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                                    
                        <div class="tabPanelArea_1 t_space" style="margin-top: 50px;">
                            <form method="post">
                                <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1 t_space">
                                    <tr>                               
                                    <input type="hidden" name="removelotid" id="removelotid" value="<%=lotid%>" />
                                    <th colspan="9">Mapped Files</th>
                                    </tr>
                                    <tr>
                                        <th class="ff">&nbsp;</th>
                                        <th class="ff" style="width: 4%">Sl. No.</th>
                                        <th class="ff">File Name</th>
                                        <th class="ff">Required Document</th>
                                        <th class="ff">File Description</th>
                                        <th class="ff">e-Signature / Hash</th>
                                        <th class="ff" width="60">File Size (in KB)</th>
                                        <th class="ff">Date and Time</th>
                                        <th class="ff">Action</th>
                                    </tr>
                                    <%
                                        //SpgetCommonSearchDataMore tenderCommonService = (SpgetCommonSearchDataMore) AppContext.getSpringBean("SpgetCommonSearchDataMore");
                                        int g_cnt = 1;
                                        //List<SPCommonSearchDataMore> mapDocList = tenderCommonService.executeProcedure("GetBidderDocuments", "" + userId, "" + formId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                        for (SPCommonSearchDataMore bidderDocDetail : mapDocList)
                                        {
                                    %>
                                    <tr>
                                        <td><input type="checkbox" value="<%=bidderDocDetail.getFieldName1()%>" name="docIds" id="mapchk_<%=g_cnt%>"/></td>
                                        <td class="t-align-center"><%=g_cnt%></td>
                                        <td><%=bidderDocDetail.getFieldName2()%></td>
                                        <td class="t-align-center">
                                            <%
                                                //Emtaz on 24/April/2016
                                                String NIDtoCIDForTable = bidderDocDetail.getFieldName12().replaceAll("National ID", "CID");
                                                NIDtoCIDForTable = NIDtoCIDForTable.replaceAll("NID","CID");
                                                out.print(NIDtoCIDForTable);
                                            %>
                                        </td>
                                        <td><%=bidderDocDetail.getFieldName4()%></td>
                                        <td><%=bidderDocDetail.getFieldName13()%></td>
                                            <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");
                                            out.print(twoDForm.format(Double.parseDouble((bidderDocDetail.getFieldName3())) / 1024));%></td>
                                        <td class="t-align-center"><%=bidderDocDetail.getFieldName5()%></td>
                                        <td class="t-align-center">
                                            <a onclick="downloadFile('<%=bidderDocDetail.getFieldName2()%>','<%=bidderDocDetail.getFieldName3()%>')" href="javascript:void();">Download</a>
                                        </td>
                                    </tr>
                                    <%g_cnt++;
                                        }%>
                                    <%if (mapDocList.isEmpty())
                                        {%>
                                    <tr><td colspan="9" class="ff t-align-center" style="color: red;">No Records Found.</td></tr>
                                    <%}%>
                                    <input type="hidden" id="g_cnt" value="<%=g_cnt%>"/>
                                </table>
                            
                            <%if (!mapDocList.isEmpty())
                                {%>
                            <div align="center" class="t_space">
                                <label class="formBtn_1">                                
                                    <input type="submit" value="Remove" name="btnMap" onclick="return validateMap()"/>
                                </label>
                            </div>
                            <%}%>
                        </form>
                        </div>
                        <div>&nbsp;</div>
                        <!--Dashboard Content Part End-->
                        <%@include  file="../resources/common/Bottom.jsp"%>
                    </div>

                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDocLib");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        
        
        var optionText, array = [], newString, maxChar = 70;
        $('select').each(function(){
            $(this).find('option').each(function(i,e) {
                $(e).attr('title',$(e).text());
                optionText = $(e).text();
                newString = '';
                if (optionText.length > maxChar) {
                    array = optionText.split(' ');
                    $.each(array,function(ind,ele) { 
                        newString += ele+' ';
                        if (ind > 0 && newString.length > maxChar) {
                            $(e).text(newString);
                            return false;
                        }
                    });

                }
            });
        });
    </script>

</html>
<form id="form2" method="post">
</form>
<script type="text/javascript">
    

      
    var arrayChk = new Array();
    function checkBoxClick(chkDocId){
                
        var flag=true;
        for(var i=0;i < arrayChk.length;i++){            
            if(arrayChk[i] == chkDocId ){               
                arrayChk.splice(i,1);
                flag=false;
                break;
            }
        }
        if(flag){
            arrayChk.push(chkDocId);
        }        
    }

    function mapDocumentData(){

        var mapOrRemove = document.getElementById("btnMap").value;        
        if(arrayChk.length == 0){
            jAlert("Please select atleast one document.","Map Document", function(RetVal) {
                return false;
            });
        }
        else{
            var temp="";
            for(var i=0;i < arrayChk.length;i++){
                temp+="docIds="+arrayChk[i]+"&"
            }
            document.getElementById("frmMap").action="TendererFormDocMap.jsp?btnMap="+mapOrRemove+"&tenderId=<%=tenderId%>&formId=<%=formId%>&"+temp;
            document.getElementById("frmMap").submit();
            return true;
        }
    }

    function downloadFile(fileName,fileLen){
        document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName="+fileName+"&fileLen="+fileLen+"&fromMapDoc=true&tenderId=<%=tenderId%>&formId=<%=formId%>";
        document.getElementById("form2").submit();
    }
    function forUnMappedFile(){
        document.getElementById("0").className="sMenu";
        document.getElementById("1").className="";
        document.getElementById("2").className="";
        document.getElementById("3").className="";
        $('#tr1').hide();
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapDocForm&action=GetDocumentsWithPaging&type=unmap&formId=<%=formId%>&tenderId=<%=tenderId%>",
            datatype: "xml",
            //height: 690,
            height: 'auto',
            colNames:['','Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
            colModel:[
                {name:'checkBox',index:'checkBox', width:10,sortable:false,search : false},
                {name:'srno',index:'srno', width:15,sortable:false,search : false,align:'center'},
                {name:'fileName',index:'documentName', width:90,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'fileDescription',index:'documentBrief', width:70,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'docHash',index:'docHash', width:150,search: false,sortable: false,align:'center'},
                {name:'fileSize',index:'documentSize', width:30,sortable: true,search : false,align:'center'},
                //                {name:'maptofolder',index:'maptofolder', width:50,sortable:false,search : false},
                {name:'uploaddate',index:'uploadedDate', width:60,sortable:true,search : false,align:'center'},
                {name:'action',index:'action', width:70,sortable:false,search : false},
            ],
            async: false,
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:15,
            rowList:[15,30,45],
            pager: $("#page"),            
            caption: "<%=title%> ",
            id: 'Id',
            gridComplete: function(){
                $("#list tr:nth-child(even)").css("background-color", "#fff");
            }
        }).navGrid('#page',{edit:false,add:false,del:false});
        $('#btnMap').val('Map');
        $('#trMapBtn').show();
    }

    function forFolderWiseFile(){
        document.getElementById("1").className="sMenu";
        document.getElementById("0").className="";
        document.getElementById("2").className="";
        document.getElementById("3").className="";
        //document.getElementById("4").className="";
        $('#tr1').hide();
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=getfoldersWithPaging&type=folderwise&formId=<%=formId%>&tenderId=<%=tenderId%>",
            datatype: "xml",
            height: 'auto',
            colNames:['Sl. <br/> No.','Folder Name','Action'],
            colModel:[
                {name:'srno',index:'srno', width:10,sortable:false,search : false,align:'center'},
                {name:'folderName',index:'folderName', width:90,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'action',index:'action', width:100,sortable:false,search : false,align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:15,
            rowList:[15,30,45],
            pager: $("#page"),
            caption: "<%=title%> ",
            gridComplete: function(){
                $("#list tr:nth-child(even)").css("background-color", "#fff");
            }

        }).navGrid('#page',{edit:false,add:false,del:false});
        $('#btnMap').val('Map');
        $('#trMapBtn').show();
    }

    function forAllFile(){
        document.getElementById("2").className="sMenu";
        document.getElementById("1").className="";
        document.getElementById("0").className="";
        document.getElementById("3").className="";
        //document.getElementById("4").className="";
        $('#tr1').hide();
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapDocForm&action=GetAllDocumentsWithPaging&type=unmap&formId=<%=formId%>&tenderId=<%=tenderId%>",
            datatype: "xml",
            height: 'auto',
            colNames:['','Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Folder Name','Date and Time','Action'],
            colModel:[
                {name:'checkBox',index:'checkBox', width:10,sortable:false,search : false},
                {name:'srno',index:'srno', width:15,sortable:false,search : false,align:'center'},
                {name:'fileName',index:'documentName', width:80,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'fileDescription',index:'documentBrief', width:70,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'docHash',index:'docHash', width:170,search: false,sortable: false,align:'center'},
                {name:'fileSize',index:'documentSize', width:35,search : false,sortable:false,align:'center'},
                {name:'foldername',index:'folderName', width:35,search : true,sortable: true,align:'center', searchoptions: { sopt: ['eq', 'cn'] },search : false},
                //                {name:'maptofolder',index:'maptofolder',search : false, width:50,sortable:true,align:'center'},
                {name:'uploaddate',index:'uploadedDate', width:70,sortable: true,search : false,align:'center'},
                {name:'action',index:'action', width:90,sortable:false,search : false},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:15,
            rowList:[15,30,45],
            pager: $("#page"),
            caption: "<%=title%> ",
            id: 'Id',
            gridComplete: function(){
                $("#list tr:nth-child(even)").css("background-color", "#fff");
            }

        }).navGrid('#page',{edit:false,add:false,del:false});
        $('#btnMap').val('Map');
        $('#trMapBtn').show();
    }

    function forArchive(){
        document.getElementById("3").className="sMenu";
        document.getElementById("1").className="";
        document.getElementById("2").className="";
        document.getElementById("0").className="";
        //document.getElementById("4").className="";
        $('#tr1').hide();
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapDocForm&action=GetArchiveDocumentsWithPaging&type=archivefile&formId=<%=formId%>&tenderId=<%=tenderId%>",
            datatype: "xml",            
            height: 'auto',
            colNames:['','Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
            colModel:[
                {name:'checkBox',index:'checkBox', width:10,sortable:false,search: false},
                {name:'srno',index:'srno', width:15,search: true,search: false,align:'center'},
                {name:'fileName',index:'documentName', width:90,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'fileDescription',index:'documentBrief', width:90,search : true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'docHash',index:'docHash', width:150,search: false,sortable: false,align:'center'},
                {name:'fileSize',index:'documentSize', width:30,sortable: true,search: false,align:'center'},
                {name:'uploaddate',index:'uploadedDate', width:60,sortable: true,search: false,align:'center'},
                {name:'action',index:'action', width:50,sortable:false,search: false,align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:15,
            rowList:[15,30,45],
            pager: $("#page"),
            caption: "<%=title%> ",
            gridComplete: function(){
                $("#list tr:nth-child(even)").css("background-color", "#fff");
            }
        }).navGrid('#page',{edit:false,add:false,del:false});
        $('#btnMap').val('Map');
        $('#trMapBtn').hide();
    }

    function checkSatus(type){
    if("unmap" == type ){
        document.getElementById("0").className = "sMenu";
        document.getElementById("1").className="";
        document.getElementById("2").className="";
        document.getElementById("3").className="";
        //document.getElementById("4").className="";
        forUnMappedFile();
    }
    else if("folderwise" == type ) {
        document.getElementById("0").className = "";
        document.getElementById("1").className="sMenu";
        document.getElementById("2").className="";
        document.getElementById("3").className="";
        //document.getElementById("4").className="";
        forFolderWiseFile();
    }else if("allfile" == type){
        document.getElementById("0").className = "";
        document.getElementById("1").className="";
        document.getElementById("2").className="sMenu";
        document.getElementById("3").className="";
        //document.getElementById("4").className="";
        forAllFile();
    }
    else if("archivefile" == type){
        document.getElementById("0").className = "";
        document.getElementById("1").className="";
        document.getElementById("2").className="";
        document.getElementById("3").className="sMenu";
        //document.getElementById("4").className="";
            forArchive();
    }else{
        document.getElementById("0").className = "";
        document.getElementById("1").className="";
        document.getElementById("2").className="";
        document.getElementById("3").className="";
        //document.getElementById("4").className="sMenu";
        viewMapFileWithFolder('<%=formId%>');
    }
}

    jQuery().ready(function (){
        $('#tr1').hide();
        //viewMapFileWithFolder(<%=formId%>)//request.getParameter("formId")
        forUnMappedFile();
    });

    function deleteFile(docId,fileName,redirectaction){

    <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'delete',docId:docId,fileName:fileName}, function(j){
          // alert(j);
    });--%>
            $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                if (r == true){
                    $.ajax({
                        url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=delete&fileName="+fileName,
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            jAlert("Document deleted successfully.","Document Deleted.",function(RetVal) {
                            });
                        }
                    });
                    if(redirectaction == "getfolders"){
                        forFolderWiseFile();
                    }
                    else if(redirectaction == "GetArchiveDocuments"){
                        forArchive();
                    }
                    else if(redirectaction == "GetAllDocuments"){
                        forAllFile();
                    }else{
                        forUnMappedFile();
                    }
                }
            });

        }
        function cancelFile(docId){
            //alert(docId);
            $.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'cancel',docId:docId}, function(j){
                //  alert(j);
            });
        }
        function archiveFile(docId,redirectaction){
    <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'archive',docId:docId}, function(j){
        jAlert("Document Archived."," Archive Document ", function(RetVal) {
        });
         //  alert(j);
    });--%>

            $.alerts._show("Delete Document", 'Do you really want to archive this file', null, 'confirm', function(r) {
                if(r == true){
                    $.ajax({
                        url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=archive&",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            jAlert("Document Archived."," Archive Document ", function(RetVal) {
                            });
                        }
                    });
                    if(redirectaction == "getfolders"){
                        forFolderWiseFile();
                    }
                    else if(redirectaction == "GetArchiveDocuments"){
                        forArchive();
                    }
                    else if(redirectaction == "GetAllDocuments"){
                        forAllFile();
                    }else{
                        forUnMappedFile();
                    }
                }
            });

        }

        function viewFile(folderId,folderName){
            // var goBack="<a href='#' onclock='forFolderWiseFile();'  ></a>";
            //$('#jqGridHeader').html("<table id='table1'><tr><td>"+goBack+"</td></tr></table>");
            $('#tr1').show();
            var td2 = document.getElementById("td2");
            td2.innerHTML = "<a href='#' onclick='forFolderWiseFile();' class='action-button-goback' >Go back</a>";
            var title = "Folder Name : &nbsp;"+folderName;

            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&msg=chkallowed&action=GetDocumentsWithPaging&type=unmap&folderId="+folderId,
                //url: "<a%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapDocForm&action=getdocuments&type=unmap&folderId="+folderId+"&formId=<a%=formId%>&tenderId=<a%=tenderId%>",
                datatype: "xml",
                height: 'auto',
                colNames:['','Sl. No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
                colModel:[
                    {name:'checkBox',index:'checkBox', width:10,sortable:false},
                    {name:'srno',index:'srno', width:15,sortable:false,align:'center'},
                    {name:'fileName',index:'fileName', width:90,sortable:false},
                    {name:'fileDescription',index:'fileDescription', width:70,sortable:false},
                    {name:'docHash',index:'docHash', width:150,search: false,sortable: false,align:'center'},
                    {name:'fileSize',index:'fileSize', width:30,sortable:false,align:'center'},
                    //                {name:'maptofolder',index:'maptofolder', width:50,sortable:false,align:'center'},
                    {name:'uploaddate',index:'uploaddate', width:60,sortable:false,align:'center'},
                    {name:'action',index:'action', width:90,sortable:false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false});
            $('#btnMap').val('Map');
            $('#trMapBtn').show();
        }


        function viewMapFileWithFolder(formId){
            //        document.getElementById("3").className="";
            //        document.getElementById("1").className="";
            //        document.getElementById("2").className="";
            //        document.getElementById("0").className="";
            //document.getElementById("4").className="sMenu";
            $('#tr1').hide();
            $("#jqGrid2").html("<table id=\"list\"></table><div id=\"page\"></div>");

            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapDocForm&action=GetBidderDocuments&type=mappedWithForm&formId="+formId+"&tenderId=<%=tenderId%>",
                datatype: "xml",
                height:'auto',
                colNames:['','Sl. No.','File Name','Document Type','File Description','File Size','Date','Action'],
                colModel:[
                    {name:'checkBox',index:'checkBox', width:10,sortable:false},
                    {name:'srno',index:'srno', width:15,sortable:false},
                    {name:'fileName',index:'fileName', width:90,sortable:false},
                    {name:'docType',index:'docType', width:90,sortable:false},
                    {name:'fileDescription',index:'fileDescription', width:90,sortable:false},
                    {name:'fileSize',index:'fileSize', width:50,sortable:false},                
                    {name:'uploaddate',index:'uploaddate', width:50,sortable:false},
                    {name:'action',index:'action', width:100,sortable:false},
                ],
                async: false,
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false});

            $('#btnMap').val('Remove');
            $('#trMapBtn').show();
        }

        function removeMsg()
        {
            if(document.getElementById("docMsg")!= null)
            {
                if(document.getElementById("docMsg").innerHTML != "")
                {
                    document.getElementById("docMsg").innerHTML = "";
                }
            }
        }

</script>
<script type="text/javascript">
    //            function checkfile()
    //            {
    //
    //                var fvalue=document.getElementById('fileUpload').value;
    //                var param="document";
    //                var ext = fvalue.split(".");
    //                var fileType = ext[ext.length-1];
    //                fileType=fileType.toLowerCase();
    //
    //
    //                if (param=="document" && ( fileType =='pdf'|| fileType =='zip'|| fileType =='jpeg' || fileType =='doc' || fileType =='docx' || fileType =='xls' || fileType =='xlsx' ||
    //                    fileType =='bmp' || fileType =='rar' || fileType =='png'))
    //                {
    //                    var toCheck=fvalue;
    //                    var index1=toCheck.indexOf("\\");
    //                    var onlyFilePath=toCheck.substring(index1+1,toCheck.length);
    //                    var reExp1=/^[A-Za-z0-9\s\.\-\_\\]+$/
    //                     if (!reExp1.test(onlyFilePath)){
    //                            document.getElementById('fileerror').innerHTML ="<div class='reqF_1'>A file path may contain only Space, - , _ , \\ special characters !</div>";
    //                            return false;
    //                    }
    //
    //                    document.getElementById('fileerror').innerHTML='';
    //                    if (document.getElementById('txtDescription').value=="")
    //                    {
    //                       document.getElementById('descriptionerror').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
    //                       return false;
    //                    }else
    //                    {
    //                        if(document.getElementById('txtDescription').value.length > 50){
    //                       document.getElementById('descriptionerror').innerHTML="<div class='reqF_1'>Maximum 50 characters are allowed.</div>";
    //                       return false;
    //                        }else{
    //                           document.getElementById('descriptionerror').innerHTML='';
    //                           return true;
    //                        }
    //
    //                      }
    //                }
    //                else
    //                {
    //                    if(fvalue == ""){
    //                        document.getElementById('fileerror').innerHTML="<div class='reqF_1'>Please select file.</div>";
    //                        return false;
    //                    }else{
    //                       document.getElementById('fileerror').innerHTML="<div class='reqF_1'>File type is not allowed.</div>";
    //                        return false;
    //                    }
    //                }
    //            }
    
    function mapDocForm(){
        var panel = document.getElementById("innerPanel");
        panel.innerHTML = "Performing requested Operation. Please wait.<img src = 'images/loading.gif' />"
        $.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean", {work:'mapDocForm'}, function(j){
            $("fieldset#innerPanel").html(j);
        });
    }
    function validate(){
        $(".err").remove();
        var valid = true;
        if($("#fileUpload").val() == "") {
            $("#fileUpload").parent().append("<div class='err' style='color:red;'>Please select a file to upload</div>");
            valid = false;
        }
        var fvalue=$("#fileUpload").val();
        if(fvalue.indexOf("&", "0")!=-1 || fvalue.indexOf("%", "0")!=-1 || fvalue.indexOf("+", "0")!=-1 || fvalue.indexOf("#", "0")!=-1){
           $("#fileUpload").parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&,+,#).</div>");
             valid = false;
        }
        if($("#txtDescription").val() == "") {
            $("#txtDescription").parent().append("<div class='err' style='color:red;'>Please enter Description</div>");
            valid = false;
        }
        return valid;
    }
    function validateMap(){
        var cnt = $('#g_cnt').val();
        var cnt2=0;
        for(var j=1;j<cnt;j++){
            if(document.getElementById('mapchk_'+j).checked){
                cnt2++;
            }
        }
        if(cnt2==0){
            jAlert("Please select Document to be removed.","Map Document", function(RetVal) {
            });
            return false;
        }else{
            return true;//true
        }
    }
    $(function() {
        $('#frmFileUp').submit(function() {
            if(validate()){
                $('.err').remove();
                var count = 0;
                var browserName=""
                var maxSize = parseInt($('#fileSize').val())*1024*1024;
                var actSize = 0;
                var fileName = "";
                jQuery.each(jQuery.browser, function(i, val) {
                    browserName+=i;
                });
                $(":input[type='file']").each(function(){
                    if(browserName.indexOf("mozilla", 0)!=-1){
                        actSize = this.files[0].size;
                        fileName = this.files[0].name;
                    }else{
                        var file = this;
                        var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                        var filepath = file.value;
                        var thefile = myFSO.getFile(filepath);
                        actSize = thefile.size;
                        fileName = thefile.name;
                    }
                    if(parseInt(actSize)==0){
                        $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                        count++;
                    }
                    if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1 || fileName.indexOf("+", "0")!=-1){
                        $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                        count++;
                    }
                    if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                        $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                        count++;
                    }
                });
                if(count==0){
                    $('#button').attr("disabled", "disabled");
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
    });
    jQuery("#grid").jqGrid('setGridHeight', Math.min(300,parseInt(jQuery(".ui-jqgrid-btable").css('height'))));

    function adjustHeight(archiveFile, maxHeight){
        var height = grid.height();
        if (height>maxHeight)height = maxHeight;
        grid.setGridHeight(height);
    }

</script>
<%
    briefcaseDoc = null;
    checkExtension = null;
%>
