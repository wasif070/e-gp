<%-- 
    Document   : LotPckDocs
    Created on : Dec 10, 2010, 3:42:54 PM
    Author     : Karan
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.web.databean.TendererTabDtBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Tender Documents</title>
       <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%

          SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
          if(session.getAttribute("payment")!=null){
              session.removeAttribute("payment");
          }
          boolean isEligible = false, isIntegrityPackAgreed=false;
          boolean isPackage = false, isLot = false; // for docAvlMethod
          String procMethod="";
                    boolean isPaid = false, isFree = false, isDocFeesPaid=false;
          boolean showDeclaration = true, showMessage = false, isError = false, isDeclarationDone = false, isOnline = false;

                    String tenderId = "", docAvlMethod = "", docFeesMode = "", userId = "", msgTxt = "";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                }

                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                }
                String auditAction = null;
                try{
                    if (request.getParameter("btnAgree") != null) {
            String dtXml = "";
            auditAction = "Bidder Agreed to Declartion";
            dtXml = "<tbl_BidConfirmation tenderId=\"" + tenderId + "\" maskName=\"\" confirmationDt=\"" + format.format(new Date()) + "\" eSignature=\"\" digitalSignature=\"\" userId=\"" + userId + "\" confirmationStatus=\"accepted\" />";
            dtXml = "<root>" + dtXml + "</root>";

            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_BidConfirmation", dtXml, "").get(0);
            //out.println(commonMsgChk.getMsg());

            if (commonMsgChk.getFlag().equals(true)) {
                showDeclaration = false;
                showMessage = true;
                            msgTxt = "Declaration completed successfully";
            } else {
                showMessage = true;
                isError = true;
                            msgTxt = "There was some error";
            }
                    } else if (request.getParameter("btnDisagree") != null) {
             String dtXml = "";
             auditAction = "Bidder Disagreed to Declartion";
            dtXml = "<tbl_BidConfirmation tenderId=\"" + tenderId + "\" maskName=\"\" confirmationDt=\"" + format.format(new Date()) + "\" eSignature=\"\" digitalSignature=\"\" userId=\"" + userId + "\" confirmationStatus=\"rejected\" />";
            dtXml = "<root>" + dtXml + "</root>";

            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_BidConfirmation", dtXml, "").get(0);
            //out.println(commonMsgChk.getMsg());
            
            if (commonMsgChk.getFlag().equals(true)) {
                showDeclaration = false;
                showMessage = true;
                            isError = true;
                            msgTxt = "You cannot proceed further";
            } else {
                showMessage = true;
                isError = true;
                            msgTxt = "There was some error";
            }
        }

        }catch(Exception e){
            auditAction = "Error in "+auditAction+" "+e.getMessage();    
        }finally{
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(userId), "userId", EgpModule.DELCLARTION.getName(), auditAction, "Tender ID  = "+tenderId);
                auditAction = null;
        }
 
        %>

        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
              <div class="pageHead_1">Tender Documents</div>
              <div>&nbsp;</div>
                 <%
                      // Variable tenderId is defined by u on ur current page.
                      pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>

                <table width="100%" cellspacing="0">
                    <tr><td>
                            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        </td>
                    </tr>
                    <tr><td>
                            <%pageContext.setAttribute("tab", "1");%>
                            <%@include file="TendererTabPanel.jsp" %>
                        </td></tr>
                </table>
                <%if (!is_debared) {%>
                   <div class="tabPanelArea_1">

                   <%
                        TendererTabDtBean tabDtBean = new TendererTabDtBean();
                        boolean isTenClosed = tabDtBean.isTenderClosed(tenderId);
                        int subContract_cnt = (int)tabDtBean.subContractCount(tenderId, userId);
                        if(subContract_cnt==0){
                   %>
                <%if (showMessage) {%>
                            <%if (isError) {%>
                    <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                            <%} else {%>
                    <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                            <%}%>
                            <%}%>


                   <%

         /* Start: Get basic Tender Information */
        tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                       // List<SPTenderCommonData> lstTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tenderId, userId);
         List<SPTenderCommonData> lstTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tenderId, userId);
                        if (!lstTenderInfo.isEmpty() && lstTenderInfo.size() > 0) {

                            procMethod = lstTenderInfo.get(0).getFieldName10(); //added by me.. :v
                            docFeesMode = lstTenderInfo.get(0).getFieldName2();

                            if ("Free".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())) {
                                isFree = true;
                            } else if ("Paid".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())) {
                    isPaid = true;
                }

                            docAvlMethod = lstTenderInfo.get(0).getFieldName3();
                            if (docAvlMethod.equalsIgnoreCase("package")) {
                     isPackage = true;
                            } else if (docAvlMethod.equalsIgnoreCase("lot")) {
                     isLot = true;
                 }
                            
                            if(isPaid && "Yes".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName8())){                                
                                isDocFeesPaid = true;
         }

                        }
                        
          /* End: Get basic Tender Information */

         List<SPTenderCommonData> lstDeclarationInfo = tenderCommonService.returndata("getDeclarationDoneStatus", tenderId, userId);
                        if (!lstDeclarationInfo.isEmpty() && lstDeclarationInfo.size() > 0) {
                            if ("yes".equalsIgnoreCase(lstDeclarationInfo.get(0).getFieldName1())) {
                isDeclarationDone = true;
                showDeclaration = false;
             }
         } 
                 lstDeclarationInfo = tenderCommonService.returndata("isIntegrityPackAgreed", tenderId, userId);
                        if (!lstDeclarationInfo.isEmpty() && lstDeclarationInfo.size() > 0) {
                            if ("accepted".equalsIgnoreCase(lstDeclarationInfo.get(0).getFieldName1())) {
                                    isIntegrityPackAgreed = true;
                            }
                        }

                        returndata = tenderCommonService.getCompnyIDAndCompanyAdmin(Integer.parseInt(userId));
                        count = tenderCommonService.getAllDetailsOfAssignedUser(Integer.parseInt(userId), Integer.parseInt(tenderId));
                        boolean flagfordisp = tenderCommonService.CheckForExist(Integer.parseInt(tenderId), Integer.parseInt(userId));
                        for (Object[] obj : returndata) {
                            companyId = obj[0].toString();
                            isAdmin = obj[1].toString();
                        }
                        boolean flagg = false;
                        if (companyId != "1") {
                            if (isAdmin.equalsIgnoreCase("yes") || count > 0 || flagfordisp) {
                                flagg = true;
                                }
                            }
                        
                        if(isPaid && !isDocFeesPaid){
                            showDeclaration = false; // Document Fees Payment is Pending
                        }
                        
                        
                        
                        List<SPTenderCommonData> lstUserBidPermision = tenderCommonService.returndata("getUserBidPermissionDetails", tenderId, userId);
                        List<SPTenderCommonData> lstAppPermision = tenderCommonService.returndata("getAppPermissionDetails", tenderId, userId);
                        List<SPTenderCommonData> ServicesType = tenderCommonService.returndata("getServicesType", tenderId, userId);
                        
                        if((!lstAppPermision.isEmpty() && lstAppPermision.size()>0) && (!lstUserBidPermision.isEmpty() && lstUserBidPermision.size()>0) && !procMethod.equalsIgnoreCase("Goods")){
                            for(SPTenderCommonData appPermission : lstAppPermision){
                                String workType = appPermission.getFieldName1();
                                String workCategory = appPermission.getFieldName2();
                                for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                    if(procMethod.equalsIgnoreCase(bidPermission.getFieldName1()) && workType.equalsIgnoreCase(bidPermission.getFieldName2()) && workCategory.equalsIgnoreCase(bidPermission.getFieldName3())){
                                           isEligible = true;
                                           break;
                                    
                                    }
                                }
                                if(isEligible) break;
                            }
                        }else if(procMethod.equalsIgnoreCase("Goods")){
                            for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                    if(procMethod.equalsIgnoreCase(bidPermission.getFieldName1())){
                                           isEligible = true;
                                           break;
                                    
                                    }
                                }
                        }else if(procMethod.equalsIgnoreCase("Services")){
                            for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                    if(procMethod.equalsIgnoreCase(bidPermission.getFieldName1()) && ServicesType.get(0).getFieldName1().equalsIgnoreCase(bidPermission.getFieldName2())){
                                           isEligible = true;
                                           break;
                                    }
                                }
                        }
                        

                        
        %>

        <form id="frmLotPckDocs" action="LotPckDocs.jsp?tenderId=<%=request.getParameter("tenderId")%>" method="POST">

                        <%if (isFree) {%>

 <% if (isPackage) {
    List<SPTenderCommonData> PackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, null);
    %>
                        <%if(!flagg){ %>
                            <div id="succMsg" class="responseMsg errorMsg" style="margin-bottom: 12px;">Declaration for a tender can be given only by a user who has got tender submission right.</div>
                            <%}%>
                            <%if(!isThisTenderApproved){ %>
                            <div id="succMsg" class="responseMsg errorMsg" style="margin-bottom: 12px;">Tender has been cancelled.</div>
                            <%}%>

                        <table width="100%" cellspacing="0" class="tableList_1">                            
      <tr>
         <th width="15%" class="t-align-center">Package. No.</th>
         <th width="70%" class="t-align-center">Package Description</th>
         <th class="t-align-center" width="15%">Action</th>
     </tr>
                            <%if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
        <tr>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
            <td class="t-align-center">
                                    <%if (PackageList.get(0).getFieldName3().equalsIgnoreCase("N.A.")) {%>
                 <a href="TenderDocView.jsp?tenderId=<%=tenderId%>">Documents</a>
                 <%}%>
            </td>
        </tr>
                               <%}%>
    </table>
                     
                        <%} else if (isLot) {%>
                        <% List<SPTenderCommonData> PackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, null);%>
                         <%if(!flagg){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Declaration for a tender can be given only by a user who has got tender submission right.</div>
    <%}%>
                             <%if(!isThisTenderApproved){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Tender has been cancelled.</div>
                            <%}%>
                        <table width="100%" cellspacing="0" class="tableList_1">                           
      <tr>
         <th width="15%" class="t-align-center">Package. No.</th>
         <th width="70%" class="t-align-center">Package Description</th>
     </tr>
                            <%if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
        <tr>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
        </tr>
                              <%}%>
    </table>
        <div>&nbsp;</div>

                     <%--    <%if(!flagg){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Declaration for a tender can be given only by a user who has got tender submission right.</div>
    <%}%>
                             <%if(!isThisTenderApproved){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Tender has been cancelled.</div>
                            <%}%> --%>
                        <table width="100%" cellspacing="0" class="tableList_1">                           
     <tr>
         <th width="15%" class="t-align-center">Lot. No.</th>
         <th width="70%" class="t-align-center">Lot Description</th>
         <th class="t-align-center" width="15%">Action</th>
     </tr>
     <% int j = 0;
         tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

         //for (SPTenderCommonData sptcd : tenderCommonService.returndata("getLotDescriptonForDocView", tenderId, null)) {
         for (SPTenderCommonData sptcd : tenderCommonService.returndata("getLotDescriptonForDocViewMore", tenderId, userId)) {
            j++;
     %>
     <tr
                                <%if (Math.IEEEremainder(j, 2) == 0) {%>
            class="bgColor-Green"
        <%} else {%>
            class="bgColor-white"
        <%}%>
         >
         <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
         <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
         <td class="t-align-center">
              <%if (sptcd.getFieldName4().equalsIgnoreCase("N.A.")) {%>
             <a href="TenderDocView.jsp?tenderId=<%=tenderId%>&porlId=<%=sptcd.getFieldName3()%>">Documents</a>
             <%}%>
         </td>
     </tr>
     <%}%>
 </table>
 <%}%>

<!--                </div>-->
                <%} else if (isPaid) {%>
<!--               <div class="tabPanelArea_1">-->
            
                    <%//if(!isDocFeesPaid) {%>
<!--                        <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Document fees is pending. Please pay the document fees to participate in this tender.</div>-->
                    <%//}%>

 <% if (isPackage) {
    List<SPTenderCommonData> PackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, userId);
    %>
                     <%if(!flagg){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Declaration for a tender can be given only by a user who has got tender submission right.</div>
                            <%}%>
                             <%if(!isThisTenderApproved){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Tender has been cancelled.</div>
                            <%}%>
                            <table width="100%" cellspacing="0" class="tableList_1">                                
      <tr>
         <th width="15%" class="t-align-center">Package. No.</th>
         <th width="50%" class="t-align-center">Package Description</th>
         <th class="t-align-center" width="35%">Action</th>
     </tr>
                                <%if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
        <tr>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
            <td class="t-align-center">
                <%if (!PackageList.get(0).getFieldName3().equalsIgnoreCase("0") && !PackageList.get(0).getFieldName3().equalsIgnoreCase("N.A.")) {%>
                    <%--Payment Done--%>
                    <a href="TenderDocView.jsp?tenderId=<%=tenderId%>">Documents</a>
                 <%} else {%>
                   <% if(!isEligible) { %>
                    <span style="color:brown;">You do not have permission to apply for this tender!!!</span>
                <% } else { %>
                    <%--Payment Pending--%>
                    Payment Pending ||
                   <%--Dohatec Start Tenderer can view document before payment--%>
                    <a href="TenderDocView.jsp?tenderId=<%=tenderId%>">View Document </a>
                   <%--Dohatec End--%>
                 <%isOnline = true; } } %>
            </td>
        </tr>
                                <%}%>
    </table>
                  
                    <%} else if (isLot) {%>
                    <% List<SPTenderCommonData> PackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, null);%>
                    <%if(!flagg){ %>

                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Declaration for a tender can be given only by a user who has got tender submission right.</div>
    <%}%>
                             <%if(!isThisTenderApproved){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Tender has been cancelled.</div>
                            <%}%>
                    <table width="100%" cellspacing="0" class="tableList_1">                        
      <tr>
         <th width="15%" class="t-align-center">Package. No.</th>
         <th width="70%" class="t-align-center">Package Description</th>
     </tr>
                        <%if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
        <tr>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
        </tr>
                         <%}%>
    </table>
        <div>&nbsp;</div>
                   
                    <%-- <%if(!flagg){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Declaration for a tender can be given only by a user who has got tender submission right.</div>
    <%}%>
                             <%if(!isThisTenderApproved){ %>
                            <div style="margin-bottom: 12px;" id="succMsg" class="responseMsg errorMsg">Tender has been cancelled.</div>
                            <%}%>--%>
                    <table width="100%" cellspacing="0" class="tableList_1">                       
     <tr>
         <th width="15%" class="t-align-center">Lot. No.</th>
         <th width="70%" class="t-align-center">Lot Description</th>
         <th class="t-align-center" width="15%">Action</th>
     </tr>
     <% int j = 0;
         tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

         //for (SPTenderCommonData sptcd : tenderCommonService.returndata("getLotDescriptonForDocView", tenderId, userId)) {
         for (SPTenderCommonData sptcd : tenderCommonService.returndata("getLotDescriptonForDocViewMore", tenderId, userId)) {
            j++;
     %>
     <tr
                            <%if (Math.IEEEremainder(j, 2) == 0) {%>
            class="bgColor-Green"
        <%} else {%>
            class="bgColor-white"
        <%}%>
         >
         <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
         <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
         <td class="t-align-center">
              <%if (!sptcd.getFieldName4().equalsIgnoreCase("0") && !sptcd.getFieldName4().equalsIgnoreCase("N.A.")) {%>
                <%--Payment Done--%>
                <a href="TenderDocView.jsp?tenderId=<%=tenderId%>&porlId=<%=sptcd.getFieldName3()%>">Documents</a>
             <%} else {%>
                <%--Payment Pending--%>
                    Payment Pending
             <%isOnline = true;}%>
         </td>
     </tr>
     <%}%>
 </table>
 <%}%>

                            
                <%}%>


  <div>&nbsp;</div>
   <%if (isOnline){
        boolean isDocSellingDateClosed = false;
        List<SPTenderCommonData> DocEndDateList = tenderCommonService.returndata("getTenderSellingDateStatus", tenderId, null);
        if (!DocEndDateList.isEmpty() && DocEndDateList.size() > 0) {
            if("YES".equalsIgnoreCase(DocEndDateList.get(0).getFieldName1())){
                isDocSellingDateClosed = true;
            }
        }
    %>
       <%if (isDocSellingDateClosed) {%>
           <div align="center" style="display: none;">
               <a class="anchorLink" href="<%=request.getContextPath()%>/OnlinePmtForDocFees.jsp?tenderId=<%=tenderId%>">Click Here</a> to pay document fees Online.
           </div>
       <%}else{%>
            <div class="responseMsg noticeMsg t_space"><span>Document Fees Submission Date has been lapsed</span></div>
       <%}%>
   <%}%>

                 <%if(flagg && isThisTenderApproved && showDeclaration && isTenClosed){%>
                 <div class="tabPanelArea_1"><%if(isEligible){%>
                     <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                         <tr>
                                <td class="ff">Terms and Conditions : <span>*</span></td>
                                <td style="width: 80%;">
                                    <div style=" padding: 5px; border-top: 1px #ededed solid; border-left: 1px #ededed solid; width:100%; height: 200px; text-align: justify;overflow: scroll">
                                                                        <b style="color: green;">Integrity Pact</b>
                                        <br/><br/><b> 1. General:</b>
                                        <br/>Whereas the Head of the Procuring Agency of the Royal Government of Bhutan, hereinafter referred to as the <b>“Employer”</b> on one part, and <b>Business</b> registered with the authority concerned, hereinafter referred to as the <b>“Bidder”</b> on the other part hereby shall execute this pact as follows:
                                        <br/><br/>Whereas, the Employer and the Bidder agree to abide by the terms and conditions stated in this document, hereinafter referred to as ‘IP’.
                                        <br/><br/>This IP is applicable to all <b>contracts</b>  related to works, goods and services.
                                        <br/><br/><b> 2. Objectives:</b>
                                        <br/>This IP aims to prevent all forms of corruption or deceptive practice by following a system that is fair, transparent and free from any influence/unprejudiced dealings in the <b>bidding process</b>  and <b>contract administration</b> , with a view to:
                                        <br/><br/>2.1 Enabling the Employer to obtain the desired contract at a reasonable and competitive  price in conformity to the defined specifications of  the works  or goods or services; and 
                                        <br/><br/>2.2 Enabling bidders to abstain from bribing or any corrupt practice in order to secure the contract by providing assurance to them that their competitors will also refrain from bribing and other corrupt practices.
                                        <br/><br/><b>Business</b>, means any business, trade, occupation, profession, calling, industry or undertaking of any kind, or any other activity carried on for gain or profit by any person within Bhutan or elsewhere, and includes all property derived from or used in or for the purpose of carrying on such other activity, and all the rights and liabilities arising from such other activity.
                                        <br/><b>Contract</b>, means a formal agreement in writing entered into between the procuring agency and the supplier, service provider or the contractor on acceptable terms and conditions and which are in compliance with all the relevant provisions of the laws of the Kingdom. The term “contract” will also include “framework contract”.
                                        <br/><b>Bidding process</b>, for the purpose of this IP, shall mean the procedures covering tendering process starting from bid preparation, bid submission, bid processing, and bid evaluation. 
                                        <br/><b>Contract administration</b>, for the purpose of this IP, shall mean contract award, contract implementation, un-authorized sub-contracting and contract handing/taking over.
                                        <br/><br/><b> 3. Scope:</b>
                                        <br/>The validity of this IP shall cover the bidding process and contract administration period.
                                        <br/><br/><b> 4. Commitments of the Employer :</b>
                                        <br/>The Employer Commits itself to the following:
                                        <br/><br/>4.1 The  Employer  hereby  undertakes  that  no  officials of  the  Employer,  connected directly or indirectly with the contract, will demand, take a promise for or accept, directly or through intermediaries, any bribe, consideration, gift, reward, favor or any material or immaterial benefit or any other advantage from the Bidder, either for themselves or for any person, organization or third party related to the contract in exchange for an advantage in the bidding process and contract administration.
                                        <br/><br/>4.2 The Employer hereby confirms that its officials shall declare conflict of interest and if any official(s) or his or her relative or associate has a private or personal interest in a decision to be taken by the Employer, those officials shall not vote or take part in a proceeding or process of the Employer relating to such decision.
                                        <br/><br/>4.3 Officials of the Employer, who may have observed or noticed or have reasonable suspicion of person(s) who breaches or attempts to breach the conditions under clauses 4.1 and 4.2 shall report it to the Employer or the authority concerned.
                                        <br/><br/>4.4 Following report on breach of conditions under clauses 4.1 and 4.2 by official (s), through any source, necessary disciplinary proceedings or any other action as deemed fit, shall be initiated by the Employer including criminal proceedings and such a person shall be debarred from further dealings related to the bidding process and contract administration.
                                        <br/><br/><b> 5. Commitments of Bidders :</b>
                                        <br/>The Bidder commits himself/herself to take all measures necessary to prevent corrupt practices, unfair means and illegal activities during any stage of the bidding process and contract administration in order to secure the contract or in furtherance to secure it and in particular commits himself/herself to the following:
                                        <br/><br/>5.1 The Bidder shall not offer, directly or through intermediaries, any bribe, gift, consideration, reward, favor, any material or immaterial benefit or other advantage, commission, fees, brokerage or inducement to any official of the Employer, connected  directly  or  indirectly  with  the bidding  process  and contract administration, or to any person, organization or third party related to the contract in exchange for any advantage in the bidding process and contract administration.
                                        <br/><br/>5.2 The Bidder shall not collude with other parties interested in the contract to manipulate in whatsoever form or manner, the bidding process and contract administration.
                                        <br/><br/>5.3 If the bidder(s) have observed or noticed or have reasonable suspicion that the provisions of the IP have been breached by the Employer or other bidders, the bidder shall report such breach to the Employer or authority concerned.
                                        <br/><br/><b> 6. Sanctions :</b>
                                        <br/>For the breach of any of the aforementioned conditions, the bidder/employer shall also be liable for offences under the Chapter 4 of the Anti-Corruption Act 2011 and other relevant rules and laws.
                                        <br/><br/><b> 7. Monitoring and Administration :</b>
                                        <br/><br/>7.1 The respective Employer shall be responsible for administration and monitoring of the IP as per the relevant laws. 
                                        <br/><br/>7.2 The bidder shall have the right to appeal as per the arbitration mechanism contained in the relevant rules.
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input type="checkbox" value="IntegrityPack" name="IntegrityPack" id="integrityPackcnk" />
                                    <span id="tnc">I, hereby declare that I have read and understood the clauses of this agreement and I hereby affirm that I shall stand by the above conditions. In the event that I default, I understand that I shall be dealt with as per the Anti-Corruption Act of Bhutan 2011 and/or any other Rules and Laws of the Kingdom of Bhutan.</span>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <span class="reqF_1" id="spanchkIntegrityPack"></span>
                                </td>
                            </tr>
                     </table>
<!--   <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       We, hereby declare that, we have read, examined and understood the tender/proposal document pertaining to this tender/proposal notice.</p><br>-->
<%}%>
   <div class="t-align-center">
       <div>&nbsp;</div>
       <%
           if(isEligible){
       
       %>
       <label class="formBtn_1">
           <input type="submit" name="btnAgree" id="btnAgree" value="I Agree"  onClick="return Validation();"/>
       </label>
       <!--&nbsp;-->
        <!--<label class="formBtn_1">-->
           <!--<input onclick="return GetConfirmation();" type="submit" name="btnDisagree" id="btnDisagree" value="I Disagree" />-->
       <!--</label>-->

        <script>
            function GetConfirmation(){
               var val=confirm("Unless you agree, you will not be allowed to proceed further");
               if(val==true){
                    return true;
                }else {
                    return false;
                }
            }
            function Validation()
                {
                    if(!document.getElementById('integrityPackcnk').checked)
                        {
                            document.getElementById('spanchkIntegrityPack').innerHTML = '<div class="reqF_1">Please accept Terms and Conditions of Integrity Pact</div>';
                            return false;
                        }
                }
        </script>
       
       <% } else{ %>
       
       <h2><span style="color:brown;">You do not have permission to apply for this tender!!!</span></h2>
       
       <%}%>

   </div>
                </div>
                <div>&nbsp;</div>

                

                <% } else if (isIntegrityPackAgreed){%>
                    <h4><span style="color:green;">You have agreed with Integrity Pact</span></h4>
                 </form>
                <%}}else{out.print("<div align='center' style='color: red;'>You are already participating in this tender as a sub contractor/consultant. You can't participate in this tender on your own capacity.</div>");}%>
                 </div>
                <%}%>
            <!--Dashboard Content Part End-->
               
            </div>
        </div>
            <!--Dashboard Footer Start-->
            <div align="center"
                 <%@include file="../resources/common/Bottom.jsp" %>
        </div>
        <!--Dashboard Footer End-->


</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
