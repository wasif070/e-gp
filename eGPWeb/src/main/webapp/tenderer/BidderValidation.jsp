<%-- 
    Document   : BidderValidation
    Created on : Dec 29, 2011, 12:14:07 PM
    Author     : shreyansh.shah
--%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="com.cptu.egp.eps.web.utility.SignerAPI"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.BidderValidation"%>
<%@page import="java.util.List"%>
<%@page import="java.io.IOException"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="java.security.spec.InvalidKeySpecException"%>
<%@page import="java.security.InvalidAlgorithmParameterException"%>
<%@page import="javax.crypto.NoSuchPaddingException"%>
<%@page import="java.security.InvalidKeyException"%>
<%@page import="javax.crypto.BadPaddingException"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.IllegalBlockSizeException"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="javax.crypto.spec.PBEParameterSpec"%>
<%@page import="javax.crypto.SecretKey"%>
<%@page import="javax.crypto.SecretKeyFactory"%>
<%@page import="javax.crypto.spec.PBEKeySpec"%>
<%@page import="sun.misc.BASE64Decoder"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%  BidderValidation bidval = (BidderValidation) AppContext.getSpringBean("BidderValidation");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
        <%!
            public Collection<String> compareData(List<String> Origin, List<String> Diff) {
                Collection<String> similar = new HashSet<String>(Origin);
                Collection<String> different = new HashSet<String>();
                different.addAll(Origin);
                different.addAll(Diff);
                similar.retainAll(Diff);
                different.removeAll(similar);
                return different;
            }

            public List<String> concateData(List<String> Origin1, List<String> Origin2) {
                List<String> concateList = new ArrayList<String>();
                for (int i = 0; i < Origin1.size(); i++) {
                    concateList.add(Origin1.get(i).toString() + Origin2.get(i).toString());
                }
                return concateList;
            }

        %>
        <% List<Object[]> list = bidval.getBidderFormId(3133, 2871);
                    out.println("list bidder size :" + list.size());
                    out.println("list details" + list.get(0)[1]);
                    int counttc = 0;
                    int countss = 0;
                    int countsr = 0;

                    for (int ct = 0; ct < list.size(); ct++) {
                        // check for Team Composition count
                        if (list.get(ct)[1].toString().equals("13")) {
                            counttc++;
                        } // Check For Staffing schedule count
                        else if (list.get(ct)[1].toString().equals("14")) {
                            countss++;
                        } // Check for Salary Reimbursable count
                        else if (list.get(ct)[1].toString().equals("15")) {
                            countsr++;
                        }
                    }
                    if (counttc == 0) {
                        out.println("list not contain teamcomposition data");
                    }
                    if (countss == 0) {
                        out.println("list not contain staff schedule");
                    }
                    if (countsr == 0) {
                        out.println("list not contain Salary Reimbursable data");
                    }
                    // Check if there is entry for Team composition, staffing schedule and salary reimbursable
                    if (countsr > 0 && counttc > 0 && countss > 0) {
                        List<Object[]> listbiddet = null;
                        List<String> arrTC = new ArrayList<String>();
                        List<String> arrSS = new ArrayList<String>();
                        List<String> arrSSHomeField = new ArrayList<String>();
                        List<String> arrSR = new ArrayList<String>();
                        List<String> arrSRHomeField = new ArrayList<String>();
                        Set set = new HashSet();
                        for (int i = 0; i < list.size(); i++) {
                            System.out.println("list :" + list.get(i)[0]);
                            if (list.get(i)[1].toString().equals("13")) {
                                listbiddet = bidval.getBidDetails(3133, "2", Integer.parseInt(list.get(i)[0].toString()), 2871);
                            } else {
                                listbiddet = bidval.getBidDetails(3133, "2,4", Integer.parseInt(list.get(i)[0].toString()), 2871);
                            }
                            for (int j = 0; j < listbiddet.size(); j++) {
                                String encrypt = "";
                                // Check for last row of salary reimbursable is not nulll
                                if (!listbiddet.get(j)[3].toString().isEmpty() && listbiddet.get(j)[3] != null && !listbiddet.get(j)[3].equals("null")) {
                                    encrypt = listbiddet.get(j)[3].toString();
                                    List<TblLoginMaster> listpass = bidval.getPasswordByUserId(2871);
                                    String password = listpass.get(0).getPassword();
                                    try {
                                        String salt = encrypt.substring(0, 12);
                                        String ciphertext = encrypt.substring(12, encrypt.length());
                                        BASE64Decoder decoder = new BASE64Decoder();
                                        byte[] saltArray = decoder.decodeBuffer(salt);
                                        byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
                                        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
                                        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
                                        SecretKey key = keyFactory.generateSecret(keySpec);
                                        PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
                                        Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                                        cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
                                        byte[] plaintextArray = cipher.doFinal(ciphertextArray);
                                        if (list.get(i)[1].toString().equals("13")) {
                                            String staffname = new String(plaintextArray);
                                            arrTC.add(staffname);
                                            //  arrTC[j] = new String(plaintextArray).toString();
                                        } // Check For Staffing schedule count
                                        else if (list.get(i)[1].toString().equals("14")) {
                                            if (j % 2 == 0) {
                                                arrSS.add(new String(plaintextArray));
                                            } else {
                                                arrSSHomeField.add(new String(plaintextArray));
                                            }
                                        } // Check for Salary Reimbursable count
                                        else if (list.get(i)[1].toString().equals("15")) {
                                            if (j % 2 == 0) {
                                                arrSR.add(new String(plaintextArray));
                                            } else {
                                                arrSRHomeField.add(new String(plaintextArray));
                                            }
                                            System.out.println("SR :" + new String(plaintextArray));
                                        }
                                        out.println(new String(plaintextArray));
                                    } catch (IllegalBlockSizeException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    } catch (BadPaddingException ex) {
                                        //   LOGGER.error("Error API : " + ex);
                                    } catch (InvalidKeyException ex) {
                                        //   LOGGER.error("Error API : " + ex);
                                    } catch (InvalidAlgorithmParameterException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    } catch (NoSuchPaddingException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    } catch (InvalidKeySpecException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    } catch (NoSuchAlgorithmException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    } catch (IOException ex) {
                                        //  LOGGER.error("Error API : " + ex);
                                    }
                                }
                                //  out.println("Bid Details :" + listbiddet.get(j)[3]);
                            }
                        }
                        set.addAll(arrTC);
                        if (arrTC.size() != set.size()) {
                            out.println("There is duplicate entry in Team composition");
                        } else {
                            out.println("There is no duplicate entry in team composition");
                        }
                        Collection<String> diff = new HashSet<String>();
                        diff = compareData(arrTC, arrSS);

                        if (diff.isEmpty()) {
                            System.out.println("TC & SS Matched Successfully");
                        } else {
                            System.out.println("TC & SS not Matched Successfully");
                        }
                        diff = compareData(arrTC, arrSR);
                        if (diff.isEmpty()) {
                            System.out.println("TC & SR Matched Successfully");
                        } else {
                            System.out.println("TC & SR not Matched Successfully");
                        }

                        // Concatenate data for Staff Schedule staffname,homefield
                        List<String> SSconList = new ArrayList<String>();
                        SSconList = concateData(arrSS, arrSSHomeField);

                        // Create unique Staff Schedule list
                        Set uniqueSSConList = new HashSet();
                        uniqueSSConList.addAll(SSconList);
                        SSconList.removeAll(SSconList);
                        SSconList.addAll(uniqueSSConList);

                        // Concatedata data of Salary Reimbursable staffname,homefield
                        List<String> SRconList = new ArrayList<String>();
                        SRconList = concateData(arrSR, arrSRHomeField);

                        // Create set to check duplicate entry in Salary Reimbursable
                        Set SetSrConList = new HashSet();
                        SetSrConList.addAll(SRconList);

                        // CompareData of after concatenating home/field of staff and salary reimbrusable
                        diff = compareData(SSconList, SRconList);
                        if (diff.isEmpty()) {
                            System.out.println("SS & SR Matched Successfully");
                        } else {
                            System.out.println("SS & SR not Matched Successfully");
                        }

                        // Check Duplicate entry of Salary Reimbursable
                        if (SetSrConList.size() != SRconList.size()) {
                            System.out.println("Duplicate entry in Salary Reimbursable");
                        } else {
                            System.out.println("There is not duplicated entry in Salary Reimbursable");
                        }
                        // System.out.printf("One:%s%nTwo:%s%nSimilar:%s%nDifferent:%s%n", arrTC, arrSS, similar, different);
                    }


        %>
        <form name="biddet" action="">

            <input type="button" name="Submit" value="Save" onclick="decryptData();">
        </form>
    </body>
</html>
<script>
    

</script>
