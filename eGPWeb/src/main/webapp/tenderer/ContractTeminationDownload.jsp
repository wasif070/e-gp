<%--
    Document   : ContractTeminationUpload
    Created on : Aug 18, 2011, 5:24:23 PM
    Author     : Sreenu.Durga
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCtcertDoc"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCtCertDocServiceBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload Documents to certificate</title>
        <%
                    int contractTerminationId = 0;
                    if (request.getParameter("contractTerminationId") != null) {
                        contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
                    }
                    int tenderId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    int contractUserId = 0;
                    if (request.getParameter("contractUserId") != null) {
                        contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                    }
                    int contractSignId = 0;
                    if (request.getParameter("contractSignId") != null) {
                        contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                    }
                    String functionName = "";
                    String message = "";
                    if (request.getParameter("functionName") != null) {
                        functionName = request.getParameter("functionName");
                    }
                    String action = "";
                    if (request.getParameter("action") != null) {
                        action = request.getParameter("action");
                    }
                    String title = "Upload a Document";
                    if ("upload".equalsIgnoreCase(action)) {
                        title = "Upload a Document";
                    } else if ("download".equalsIgnoreCase(action)) {
                        title = "Download a Document";
                    }
                    boolean check = false;
                    if (functionName.length() > 0) {
                        if (request.getParameter("check") != null) {
                            check = Boolean.parseBoolean(request.getParameter("check"));
                        }
                        if ("upload".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Uploaded Succesfully!";
                            } else {
                                message = "Error encountered while Uploading the file. </br> Please Check the <b>Name</b> or <b>Size</b> or <b>Extension</b> of the file!";
                            }
                        } else if ("download".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Downloaded Succesfully!";
                            } else {
                                message = "Error encountered while downloading the file. Please try again!";
                            }
                        } else if ("remove".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Removed Succesfully!";
                            } else {
                                message = "Error encountered while removing the file. Please try again!";
                            }
                        }
                    }
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                    String strProcNature = "";
                    if("Services".equalsIgnoreCase(procnature))
                    {
                        strProcNature = "Consultant";
                    }else if("goods".equalsIgnoreCase(procnature)){
                        strProcNature = "Supplier";
                    }else{
                        strProcNature = "Contractor";
                    }
        %>
    </head>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#frmCTUpload").validate({
                rules: {
                    uploadDocFile: {required: true},
                    documentBrief: {required: true,maxlength:100}
                },
                messages: {
                    uploadDocFile: { required: "<div class='reqF_1'>Please select Document.</div>"},
                    documentBrief: { required: "<div class='reqF_1'>Please enter Description.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                }
            });
            $(function() {
                $('#frmCTUpload').submit(function() {
                    if($('#frmCTUpload').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                            browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                alert(fileName);
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        });
    </script>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1"> <%=title%>
                                <span style="float: right; text-align: right;">
                                    
                                    <a class="action-button-goback" href="<%=request.getContextPath()%>/tenderer/TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    
                                </span>
                            </div>
                            <%
                                        if (message.length() != 0) {
                                            if (!check) {
                                                out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");
                                            } else {
                                                out.print("<div id='errorMsg' class='responseMsg errorMsg' style='display:block; margin-top: 10px;'>");
                                            }
                                            out.print(message);
                                            out.print("</div>");
                                        }
                            %>

                            <%
                                        if ("upload".equalsIgnoreCase(action)) {
                            %>
                            <form method="POST" id="frmCTUpload"  enctype="multipart/form-data" action="<%=request.getContextPath() %>/CTUploadServlet?functionName=upload">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space ">
                                    <tr>
                                        <td colspan="2" class="ff" align="left">
<!--                                            Fields marked with (<span class="mandatory">*</span>) are mandatory.-->
                                            <input type="hidden" name="contractTerminationId" value="<%=contractTerminationId%>" id="contractTerminationId">
                                            <input type="hidden" name="tenderId" value="<%=tenderId%>" id="tenderId">
                                            <input type="hidden" name="contractUserId" value="<%=contractUserId%>" id="contractUserId">
                                            <input type="hidden" name="contractSignId" value="<%=contractSignId%>" id="contractSignId">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%" class="ff t-align-left">Document   : </td>
                                        <td width="85%" class="t-align-left">
                                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Description : </td>
                                        <td>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                            <div id="dvDescpErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="100%"  class="t-align-left">Instructions</th>
                                    </tr>
                                    <tr>
                                        <%
                                                                                    CheckExtension checkExtension = new CheckExtension();
                                                                                    TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");
                                        %>
                                        <td class="t-align-left">
                                            Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                            <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left">Acceptable File Types
                                            <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left">A file path may contain any below given special characters:
                                            <span class="mandatory">(Space, -, _, \)</span>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <%
                                        }
                            %>

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
                                    <th class="t-align-center" width="18%">Uploaded By</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                </tr>
                                <%
                                            CmsCtCertDocServiceBean cmsCtCertDocServiceBean = new CmsCtCertDocServiceBean();
                                            cmsCtCertDocServiceBean.setLogUserId(logUserId);
                                            int docCnt = 0;
                                            List<TblCmsCtcertDoc> tblCmsCtCertDocList = cmsCtCertDocServiceBean.getAllCmsCtcertDocForCtId(contractTerminationId);
                                            if (tblCmsCtCertDocList != null && !tblCmsCtCertDocList.isEmpty()) {
                                                for (TblCmsCtcertDoc tblCmsCtcertDoc : tblCmsCtCertDocList) {
                                                    docCnt++;
                                                    out.print("<tr>");
                                                    out.print("<td class='t-align-center'> " + docCnt + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsCtcertDoc.getDocumentName() + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsCtcertDoc.getDocDescription() + "</td>");
                                                    out.print("<td class='t-align-center'> " + (Long.parseLong(tblCmsCtcertDoc.getDocSize()) / 1024) + "</td>");
                                                    if(tblCmsCtcertDoc.getUserTypeId()==2){
                                                        out.print("<td class='t-align-center'> " + strProcNature + "</td>");
                                                    }else{
                                                        out.print("<td class='t-align-center'> " + "PE" + "</td>");
                                                    }
                                                    out.print("<td class='t-align-center'> "
                                                            + "<a href =" + request.getContextPath() + "/CTUploadServlet?functionName=download&ctCertDocId="
                                                            + tblCmsCtcertDoc.getCtCertDocId() + "&contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                            + "><img src='/resources/images/Dashboard/Download.png' alt='Download' /></a>");
                                                    if(tblCmsCtcertDoc.getUserTypeId()==2){
                                                    if ("upload".equalsIgnoreCase(action)) {
                                                        out.print("&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;"
                                                                + "<a href =" + request.getContextPath() + "/CTUploadServlet?functionName=remove&ctCertDocId="
                                                                + tblCmsCtcertDoc.getCtCertDocId() + "&contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                                + "><img src='/resources/images/Dashboard/Delete.png' alt='Remove' /></a>");
                                                    }
                                                    }
                                                    out.print("</td>");
                                                    out.print("</tr>");
                                                }
                                            } else {
                                                out.print("<tr>");
                                                out.print(" <td colspan='5' class='t-align-center'>No records found.</td>");
                                                out.print("</tr>");
                                            }
                                %>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>
</html>
