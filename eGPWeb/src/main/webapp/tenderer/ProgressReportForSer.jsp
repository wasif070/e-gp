<%-- 
    Document   : ProgressReportForSer
    Created on : Dec 9, 2011, 3:32:03 PM
    Author     : shreyansh Jogi
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        List<Object> formmapId = cmss.getSrvFormMapId(Integer.parseInt(request.getParameter("tenderId")), 15);
        List<Object[]> list = null;
        int j=0;
        if(formmapId!=null && !formmapId.isEmpty()){
           list = cmss.getListForPRForTB((Integer)formmapId.get(0),30);
            }
 
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script src="../resources/js/monthDaysCalculation.js" type="text/javascript"></script>
        <script type="text/javascript">
             var daysArray1 = new Array();
             var daysArray2 = new Array();
             var daysArray3 = new Array();
        </script>
          <%
                                            if(list!=null && !list.isEmpty()){
                                                int i = 0;
                                                for(Object obj[] : list){%>
                                                <script type="text/javascript">
                                                        daysArray1[<%=i%>] = '<%=obj[5]%>';
                                                        daysArray2[<%=i%>] = '<%=obj[6]%>';
                                                        daysArray3[<%=i%>] = '<%=obj[3]%>';
                                                </script>
                                                <% i++;}
                                                }%>
        <script>
            function ViewPRR(wpId,tenderId,lotId,cntId){
                dynamicFromSubmit("viewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
            }
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function monthset(id){
                var count = $('#listcount').val();
                var monthname = new Array("January","February","March","April","May","June", "July","August","September","October","November","December");
                for(var i=1;i<count;i++){
                    document.getElementById("month_"+i).innerHTML=monthname[$('#month').val()-1];
                }
                 
            }
            function checkForAttSheet(){
                var count = $('#listcount').val();
                var tbol=true;

                for(var i=0;i<count;i++){
                    var tcId = document.getElementById("tcId_"+i).value;
                    if(document.getElementById("nod_"+i+"_"+tcId).value=="")
                        {
                            tbol=false;
                            jAlert("Please enter all the data","Attendance Sheet", function(RetVal) {
                                });
                            break;
                        }
                }
                if(tbol){
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CMSSerCaseServlet",
                        data:"tenderId="+$('#tenderId').val()+"&monthId="+$('#month').val()+"&"+"yearId="+$('#year').val()+"&"+"action=checkAttExist",
                        async: false,
                        success: function(j){
                            if(j.toString()!="0"){
                                tbol=false;
                                jAlert("Attendance Sheet is already prepared for this month and year","Attendance Sheet", function(RetVal) {
                                });
                            }
                        }
                    });
                }
                if(tbol){
                    document.getElementById("action").value = "creatAttSheet";
                    document.forms["prfrm"].submit();
                }
            }
            function yearset(id){
            var count = $('#listcount').val();
                for(var i=1;i<count;i++){
                    document.getElementById("year_"+i).innerHTML=$('#year').val();
                }
            }
            function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function checkNod(id,tcId){
                var nod = $('#nod_'+id+'_'+tcId).val();

                var WorkFrom = daysArray3[id];
                var remOffcnt = daysArray1[id];
                var remOncnt = daysArray2[id];
                var monthDays = getMonthDays($('#month').val(),$('#year').val());
                var check = true;
                 $('.err').remove();
                if(!regForNumber(nod)){
                    $("#nod_"+id+'_'+tcId).parent().append("<div class='err' style='color:red;'>Days must be numeric value </div>");
                    check = false;
                    document.getElementById('nod_'+id+'_'+tcId).value="0";
                }else if(nod.indexOf("-")!=-1){
                    $("#nod_"+id+'_'+tcId).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
                     check = false;
                    document.getElementById('nod_'+id+'_'+tcId).value="0";
                }else if(!positive(nod)){
                    $("#nod_"+id+'_'+tcId).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                     check = false;
                    document.getElementById('nod_'+id+'_'+tcId).value="0";
                }else if(nod.length > 5){
                    $("#nod_"+id+'_'+tcId).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
                     check = false;
                    document.getElementById('nod_'+id+'_'+tcId).value="0";
                }else{
                    check = true;
                }
                if(check){                    
                    if(WorkFrom.toUpperCase() =="HOME"){

                    if(parseInt(nod) > parseInt(monthDays)){
                         jAlert('Input days can not exceed '+monthDays,'Progress Report','Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id+'_'+tcId).val('0');
                    }else{
                        if(parseInt(nod) > parseInt(remOffcnt)){
                               jAlert('No of days can not be greater than total Home count '+remOffcnt,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id+'_'+tcId).val('0');
                        }

                    }

                }else{

                     if(parseInt(nod) > parseInt(monthDays)){
                         jAlert('Input days can not exceed '+monthDays,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id+'_'+tcId).val('0');
                    }else{
                       if(parseInt(nod) > parseInt(remOncnt)){
                        jAlert('No of days can not be greater than total Field count '+remOncnt,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id+'_'+tcId).val('0');

                    }
                }
                }
                var days=0;
                var count = 0;
                $("input[type='text']").each(function(){
                    
                    var temp1 = $(this).attr('id').split("_");
                     var tcid1 = parseInt(temp1[2]);
                     
                        if(tcId==tcid1 && count!=id && $('#nod_'+count+'_'+tcid1).val()!='0'){
                           days = parseInt(nod)+ parseInt($('#nod_'+count+'_'+tcid1).val());
                        }
                        count++;
                    });
                    if(parseInt(days) > parseInt(monthDays)){
                        jAlert('No of days can not be greater than total count '+monthDays,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id+'_'+tcId).val('0');

                    }
                
                
            }
            }
        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Progress Report
            <span class="c-alignment-right">
                <%
                    CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if("Time based".equalsIgnoreCase(serviceType.toString()))
                    {    
                %>
                        <a href="ProgressReportMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}else{%>
                        <a href="SrvLumpSumPr.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}%>
            </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");

            %>
                <div class="tabPanelArea_1">
                    <div align="left">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                                    
                                %>
                                <form name="prfrm" id="prfrm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post">
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                 <th width="3%" class="t-align-left">Sr.No</th>
                                                <th width="20%" class="t-align-left">Name of Employees</th>
                                                <th width="15%" class="t-align-left">Position Assigned
                                                </th>
                                                <th width="9%" class="t-align-left">Home / Field
                                                </th>
                                                <th width="5%" class="t-align-left">No of Days
                                                </th>
                                                <th width="18%" class="t-align-left">Month
                                                </th>
                                                <th width="18%" class="t-align-left">Year
                                                </th>
                                            </tr>
                                            <tr>
                                            <%
                                            
                                            if(list!=null && !list.isEmpty()){
                                                
                                                for(Object obj[] : list){%>
                                            <input type="hidden" name="rate_<%=j%>" id="rate_<%=j%>" value="<%=obj[4].toString() %>" />
                                            <input type="hidden" name="tcId_<%=j%>" id="tcId_<%=j%>" value="<%=obj[7].toString() %>" />
                                            <input type="hidden" name="WF_<%=j%>" id="WF_<%=j%>" value="<%=obj[3].toString() %>" />
                                                
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%>
                                                </td>
                                                <td width="9%" class="t-align-left"><%=obj[3]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><Input type="text" name="nod_<%=j%>" value="0" onchange="checkNod(<%=j%>,<%=obj[7]%>);" id="nod_<%=j%>_<%=obj[7]%>" class="formTxtBox_1" />
                                                </td>
                                                <td width="5%" class="t-align-left">
                                                    <%if (j==0){
        %>
        <select class="formTxtBox_1" id="month" name="month" onchange="monthset();">
                                                
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                                
                                            </select>

                                                    <%}else{%>
                                                    <label id="month_<%=j%>"></label>
                                                    <%}%>
                                                </td>
                                                <td width="5%" class="t-align-left">
                                                    <%if (j==0){
        %>
        <select  class="formTxtBox_1" id="year" name="year" onchange="yearset();">
                                                <% for(int k=2011;k<=2031;k++){%>
                                                <option value="<%=k%>"><%=k%></option>
                                                <%}%>
                                            </select>

                                                     <%}else{%>
                                                    <label id="year_<%=j%>"></label>
                                                    <%}%>
                                                </td>
                                                </tr>
                                                    <% j++;
                                                }
                                                }
%>
<input type="hidden" name="listcount" id="listcount" value="<%=j%>" />
<input type="hidden" name="action" id="action" value="" />
<input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
<input type="hidden" name="lotId" id="tenderId" value="<%=request.getParameter("lotId")%>" />
<tr><td colspan="7"><center>
                <label class="formBtn_1">
                    <input type="button" name="button" onclick="checkForAttSheet();" value="Submit" />
                </label>
        </center> </td></tr>
                                 </table>
                                </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        yearset();
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        monthset();
       
        
    </script>
</html>
