<%-- 
    Document   : LOI
    Created on : Feb 27, 2017, 4:38:26 PM
    Author     : Nishith
--%>

<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
                    String userId = "";
                    String tenderId = "";
                    String procNature = "";
                    String procType = "";
                    String procNatureId = "";
                    String procTypeId = "";
                    String procMethodId = "";
                    String daysToNOA = "";
                    
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    }
                    
                    if(request.getParameter("tenderid") != null){
                        tenderId = request.getParameter("tenderid");
                    }
                    
                    EvaluationService evalService1 = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                    int eCount = 0;
                    eCount = evalService1.getEvaluationNo(Integer.parseInt(tenderId));
                    
                    List<CommonTenderDetails> list = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
                    if (!list.isEmpty()){
                        procMethodId = list.get(0).getProcurementMethodId().toString();
                        
                        procNature = list.get(0).getProcurementNature();
                        procType = list.get(0).getProcurementType();
                    }
                    procTypeId = procType.equalsIgnoreCase("NTC")? "1" : "2" ;
                    procNatureId = procNature.equalsIgnoreCase("Goods")?"1":procNature.equalsIgnoreCase("Works")?"2":"3";
                    
                    
                    
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> getNOAConfig = commonSearchDataMoreService.geteGPData("GetNOAConfig", tenderId, procMethodId, procNatureId, procTypeId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    
                    
                    daysToNOA = getNOAConfig.isEmpty()?"0":getNOAConfig.get(0).getFieldName2();
                    
                    
        
                    TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
                    boolean flagEstCost = false;
                    flagEstCost = estcost.checkForLink(Integer.parseInt(request.getParameter("tenderid")));
                    
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String eventType = commonService.getEventType(request.getParameter("tenderid")).toString();
                    
                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> getTenderCommPubDate
                = tenderCommonService1.returndata("getTenderCommPubDate", tenderId, null);
                    

               
                    

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter Of Intent (LOI)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        
        <div class="contentArea_1">
        <div class="pageHead_1">Letter Of Intent (LOI) </div>

            <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", tenderId);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>
            
            <% 
                pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                pageContext.setAttribute("tab","24"); 
            %>
            
            
            <div>
            
            <%@include file="TendererTabPanel.jsp"%>
            
            </div>
            <div>&nbsp;</div>
            <div class="tabPanelArea_1"> 
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td colspan="3" class="t-align-left ff">Letter Of Intent(LOI)</td>
                    </tr>
                    <%
                        List<SPTenderCommonData> lstLots = tenderCommonService1.returndata("getLoIdLotDesc_ForOpening", tenderid, null);
                        List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                        List<SPCommonSearchDataMore> listReportID = null;
                        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        
                        
                        String loiDate = "";
                        String tenderLot = "0";
                        String roundID = "0";
                        String reportID = "0";
                        int lotCounter = 1;
                        for (SPTenderCommonData objLot : lstLots) {
                        if(!tenderProcureNature.equalsIgnoreCase("Services"))
                        {
                            tenderLot =  objLot.getFieldName1();
                        } 
                        
                        listReportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid,tenderLot,"0");
                        if(listReportID!=null && (!listReportID.isEmpty())){
                            //if(listReportID.get(0).getFieldName2().equals("1"))
                                    reportID = listReportID.get(0).getFieldName1();
                        }

                        bidderRounds = dataMore.getEvalProcessInclude("getRoundsforEvaluation",tenderid,reportID,"L1",String.valueOf(eCount),tenderLot);
                        if(bidderRounds!=null && !bidderRounds.isEmpty())
                            roundID = bidderRounds.get(0).getFieldName1();
                    

                        loiDate = tenderCommonService1.GetLOIIssueDate(tenderId,tenderLot,roundID);
                        
                        String convDate1;
                        Date date;
                        String noaDate="";
                        
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        Calendar cal = Calendar.getInstance();
                        try{
                            date = formatter.parse(loiDate);
                            
                            cal.setTime(date);
                            cal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(daysToNOA));
                            date = cal.getTime();
                            noaDate = formatter.format(date);
                            
                        }catch(Exception ex){
                            System.out.println(ex);
                        }




                              %>

                                            <tr>
                        <td colspan="3" class="t-align-left ff">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="30%" class="t-align-left ff">Lot No: <%=objLot.getFieldName3()%></td>
                        <td width="80%" class="t-align-left">
                            <%if("0".equalsIgnoreCase(roundID)){%>
                                <h5>Winner not yet selected.</h5>
                            <%}else{%>
                                <a href="ViewLOI.jsp?tLotId=<%=tenderLot%>&tenderid=<%=tenderId%>&lotNo=<%=lotCounter%>&roundId=<%=roundID%>">View</a>
                            <%}%> 
                        </td>
                    </tr>
                        
                    <tr>
                        <td width="30%" class="t-align-left ff">LOA Issue Date</td>
                        <td width="80%" class="t-align-left">
                            <%if("".equalsIgnoreCase(noaDate)){%>
                                <h5>Letter Of Intent not yet issued.</h5>
                            <%}else{%>
                                <span><b><%=noaDate%></b></span>
                            <%}%>
                        </td>
                    </tr>
                    <% lotCounter++; }%>
    
                </table>
            </div>
        </div>

            
        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>
    
</html>


