<%-- 
    Document   : ViewNOA
    Created on : Dec 24, 2010, 7:41:43 PM
    Author     : rajesh,rishita
--%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaAcceptance"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
      <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
       <script type="text/javascript" src="../resources/js/jQuery/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        

        <script type="text/javascript">

             $(function() {
                    $(document).ready(function() {
                        $("#frmnoa").validate({
                            rules: {
                                txtcomments:{required:true,maxlength: 1000},
                                actionselect:{required:true}
                            },
                            messages: {

                                txtcomments:{required:"<div class='reqF_1'>Please Enter your Comments.</div>",
                                    maxlength:"<div class='reqF_1'>Maximum 1000 Characters are allowed.</div>"},
                                actionselect:{required:"<div class='reqF_1'>Please select action.</div>"}
                            }
                        });
                    });
            });
            function showHideForm(){
                if($('#actionselect').val()=='approved'){
                        $('#formTbl').show();
                }else{
                            $('#formTbl').hide();
                       
                }
            }
            function showForm(){
                var flag = true;
                var spaceTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                //var errMsg = 'Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only';
                $(".err").remove();
                if($("#frmnoa").valid()){
                    if($('#actionselect').val()=='approved'){
                        if($.trim($("#txtTitleAccount").val()).length==0) {
                            $("#txtTitleAccount").parent().append("<div class='err' style='color:red;'>Please Enter Account Title</div>");
                            flag = false;
                        }
                        
                        if($.trim($("#txtBank").val()).length==0) {
                            $("#txtBank").parent().append("<div class='err' style='color:red;'>Please Enter Financial Institute Name</div>");
                            flag = false;
                        }else{
                            spaceTest = /^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/;
                            if(!spaceTest.test($('#txtBank').val())){
                                $("#txtBank").parent().append("<div class='err' style='color:red;'>Please Enter a valid Financial Institute Name</div>");
                                flag = false;
                            }
                        }       
                        if($.trim($("#txtBranch").val()).length==0) {
                            $("#txtBranch").parent().append("<div class='err' style='color:red;'>Please Enter Branch Name</div>");
                            flag = false;
                        }
//                        else{
//                            spaceTest = /^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/;
//                            if(!spaceTest.test($('#txtBranch').val())){
//                                $("#txtBranch").parent().append("<div class='err' style='color:red;'>Please Enter a valid Branch Name</div>");
//                                flag = false;
//                            }
//                        }    
                        if($.trim($("#txtAccNumber").val()).length==0) {
                            $("#txtAccNumber").parent().append("<div class='err' style='color:red;'>Please Enter Account Number</div>");
                            flag = false;
                        }       
                        if($.trim($("#txtAddress").val()).length==0) {
                            $("#txtAddress").parent().append("<div class='err' style='color:red;'>Please Enter Branch Address</div>");
                            flag = false;
                        }       
                        if($.trim($("#txtTel").val()).length==0) {
                            $("#txtTel").parent().append("<div class='err' style='color:red;'>Please Enter Telephone Number</div>");
                            flag = false;
                        }else{
                            spaceTest =  /^[0-9\-]+$/;
                            if(!spaceTest.test($('#txtTel').val())){
                                $("#txtTel").parent().append("<div class='err' style='color:red;'>Allows numbers (0-9) and hyphen (-) only</div>");
                                flag = false;
                            }
                        }       
                        if($.trim($("#txtFax").val()).length!=0) {
                            spaceTest =  /^[0-9\-]+$/;
                            if(!spaceTest.test($('#txtFax').val())){
                                $("#txtFax").parent().append("<div class='err' style='color:red;'>Allows numbers (0-9) and hyphen (-) only</div>");
                                flag = false;
                            }
                        }       
                        if($.trim($("#txtemailId").val()).length!=0) {
                        
                            spaceTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                                if(!spaceTest.test($('#txtemailId').val())){
                                    $("#txtemailId").parent().append("<div class='err' style='color:red;'>Please Enter valid e-mail ID</div>");
                                    flag = false;
                                }
                        }       
                        if($.trim($("#txtSwiftCode").val()).length==0) {
                            $("#txtSwiftCode").parent().append("<div class='err' style='color:red;'>Please Enter Swift Code</div>");
                            flag = false;
                        }       
                    }else{    
                       var r = window.confirm("If you decline, it will lead to forfeiture of your bid security. Are you sure to decline?");
                       if(r){
                            flag = true;
                       } else {
                            flag = false;;
                        }
                  }
                }else{
                    flag =false;
                }
                if(flag == true){
                    document.getElementById("submit").style.display = 'none';
                }
                return flag;
            }
        </script>
    </head>
    <body>
        <%
                        String userid = "";
                //swati--to generate pdf
                String tenderId = "",NOAID="",app="",pcklotid="";
                String userNameNOA = null;
                if (request.getParameter("tenderid") != null) {
                    tenderId = request.getParameter("tenderid");
                }
                if (request.getParameter("pcklotid") != null) {
                    pcklotid = request.getParameter("pcklotid");
                }
                if (request.getParameter("NOAID") != null){
                    NOAID = request.getParameter("NOAID");
                }
                if (request.getParameter("app") != null){
                    app = request.getParameter("app");
                }
                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }
                String folderName = folderName = pdfConstant.NOA;
                String genId = tenderId+"_"+NOAID;

                if(isPDF.equalsIgnoreCase("true")){
                    userid = request.getParameter("userid");
                    userNameNOA= request.getParameter("userNameNOA");
                 }
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService"); 
                if (!(isPDF.equalsIgnoreCase("true")) && false) {
                    try{
                     String reqURL = request.getRequestURL().toString();
                     //tenderid=1518&NOAID=46&app=no
                     String reqQuery = "tenderid="+tenderId+"&NOAID="+NOAID+"&app="+app+"&userid="+userid;
                     pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                     }
                    catch(Exception e){
                         e.printStackTrace();
                     }
                 }
                //end

       %>

        
                <!--Dashboard Header Start-->

                <%
                            String noaIssueId = request.getParameter("NOAID");
                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> pckLotId = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getLotId",noaIssueId , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                            if (request.getParameter("btnnoa") != null) {

                                java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                String table = "", updateString = "", whereCondition = "";
                                table = "tbl_NoaAcceptance";
                                if("approved".equalsIgnoreCase(request.getParameter("actionselect"))){
                                    updateString = " comments='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments"))+
                                            "', acceptRejStatus='" + request.getParameter("actionselect")+ 
                                            "',acceptRejDt=getdate()" +
                                            " ,accountTitle = '"+ request.getParameter("txtTitleAccount")+
                                            "' ,bankName='"+request.getParameter("txtBank")+
                                            "' ,branchName='"+request.getParameter("txtBranch")+
                                            "' ,accountNo = '"+request.getParameter("txtAccNumber")+
                                            "' ,address= '"+request.getParameter("txtAddress")+
                                            "' ,telephone= '"+request.getParameter("txtTel")+
                                            "' ,fax = '"+request.getParameter("txtFax")+
                                            "' ,email='"+handleSpecialChar.handleSpecialChar(request.getParameter("txtemailId"))+
                                            "' ,swiftCode = '"+request.getParameter("txtSwiftCode")+"'" ;
                                }else{
                                    updateString = " comments='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments")) + "', acceptRejStatus='" + request.getParameter("actionselect") + "',acceptRejDt=getdate()";
                                }
                                
                                whereCondition = "noaIssueId=" + request.getParameter("NOAID");
                                System.out.println("updateString------>\n"+updateString);
                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userid = hs.getAttribute("userId").toString();
                                    commonXMLSPService.setLogUserId(session.getAttribute("userId").toString());
                                    tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                                    commonSearchDataMoreService.setLogUserId(session.getAttribute("userId").toString());
                                }
                                String noaStatus = "", auditStatus="accepted";
                                if("approved".equalsIgnoreCase(request.getParameter("actionselect"))){
                                    noaStatus = "accepted";
                                    auditStatus = "Accepted";
                                }else if("decline".equalsIgnoreCase(request.getParameter("actionselect"))){
                                    noaStatus = "rejected";
                                    auditStatus="Declined";
                                }
                                
                                // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                String idType="tenderId";
                                int auditId=Integer.parseInt(tenderId);
                                String auditAction="Bidder has given acceptance Status as "+auditStatus;
                                String moduleName=EgpModule.Noa.getName();
                                String remarks=request.getParameter("txtcomments");
                                
        
        
                                String updateStringPQ = "noaStatus='" + noaStatus +"'";
                                String whereConditionPQ = "tenderId=" + tenderId + "and pkgLotId=" + pckLotId.get(0).getFieldName1() + " and userId = " + userid;
//                                CommonMsgChk msgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_PostQualification", updateStringPQ, whereConditionPQ).get(0);
//                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_NoaAcceptance", updateString, whereCondition).get(0);
                                String comments=handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments")), email = handleSpecialChar.handleSpecialChar(request.getParameter("txtemailId"));
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertNOAAcceptance("NOAAcceptanceAndContractSign", tenderId, userid, pckLotId.get(0).getFieldName1(), noaIssueId, comments, noaStatus, request.getParameter("actionselect").toString(), request.getParameter("txtTitleAccount").toString(), request.getParameter("txtBank").toString(), request.getParameter("txtBranch").toString(), request.getParameter("txtAccNumber").toString(), request.getParameter("txtAddress").toString(), request.getParameter("txtTel").toString(), request.getParameter("txtFax").toString(), email, request.getParameter("txtSwiftCode").toString(), "", "", "").get(0);
                                //if (commonMsgChk.getFlag() == true && msgChk.getFlag() == true) {
                                if (commonMsgChk.getFlag() == true) {
                                    //Sending mail to user,
                                    List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetPackagerow", request.getParameter("NOAID"), null);
                                    if (!tenderLotList.isEmpty()) {
                                        String status = "";
                                        if(tenderLotList.get(0).getFieldName5().equalsIgnoreCase("approved")){
                                            status = "Accepted";
                                        }else if(tenderLotList.get(0).getFieldName5().equalsIgnoreCase("decline")){
                                            status = "Denied";
                                        }
                                        List<SPTenderCommonData> emails = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                                        //List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId", request.getParameter("hdextReqBy"), null);
                                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                                        String[] mail = {emails.get(0).getFieldName5()};
                                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                        MailContentUtility mailContentUtility = new MailContentUtility();
                                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                        String mailText = mailContentUtility.contApproval(tenderLotList.get(0).getFieldName1(), tenderLotList.get(0).getFieldName2(), tenderLotList.get(0).getFieldName3(), tenderLotList.get(0).getFieldName4(), status,tenderLotList.get(0).getFieldName6(),tenderLotList.get(0).getFieldName7(),tenderLotList.get(0).getFieldName8());
                                        sendMessageUtil.setEmailTo(mail);
                                        sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar("e-GP - LOA Status"));
                                        sendMessageUtil.setEmailMessage(mailText);
                                        userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar("e-GP - LOA Status"), msgBoxContentUtility.contApproval(tenderLotList.get(0).getFieldName1(), tenderLotList.get(0).getFieldName2(), tenderLotList.get(0).getFieldName3(), tenderLotList.get(0).getFieldName4(), status,tenderLotList.get(0).getFieldName6(),tenderLotList.get(0).getFieldName7(),tenderLotList.get(0).getFieldName8()));
                                        sendMessageUtil.sendEmail();
                                        sendMessageUtil = null;
                                        mailContentUtility = null;
                                        mail = null;
                                        msgBoxContentUtility = null;

                                    }
                                    if("decline".equalsIgnoreCase(request.getParameter("actionselect")))
                                    {
                                    ConsolodateService service = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                                    service.deleteIfNOARejects(pcklotid);
                                    }
                                    
                                    // Added by Dipal for audit trail
                                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                    
                                    response.sendRedirect("NOAListing.jsp?tenderid=" + request.getParameter("tenderid") + "&flag=true");
                                }
                                else 
                                {
                                    // Added by Dipal for audit trail
                                    auditAction="Error in "+auditAction;
                                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                    
                                    response.sendRedirect("ViewNOA.jsp?tenderid=" + request.getParameter("tenderid") + "&NOAID=" + request.getParameter("NOAID") + "&app=" + request.getParameter("app") + "&flag=false");
                                }
                            }
                            else // Action = View NOA
                            {
                                // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                String idType="tenderId";
                                int auditId=Integer.parseInt(tenderId);
                                String auditAction="View NOA";
                                String moduleName=EgpModule.Noa.getName();
                                String remarks="User Id: "+session.getAttribute("userId")+" has viewed NOA Id: "+noaIssueId;
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            }
                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> awarddetails = tenderCommonService.returndata("GetAwardDetails", request.getParameter("tenderid"), request.getParameter("NOAID"));
                            List<SPCommonSearchDataMore> detailsOfTender = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getPEOfficeName", tenderId);
                            List<SPCommonSearchDataMore> detailsOfTender1 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getPEName", tenderId,noaIssueId);
                            List<SPCommonSearchDataMore> procurenature = commonSearchDataMoreService.geteGPData("GetDetailsForNOA", request.getParameter("tenderid"), pckLotId.get(0).getFieldName2());
                            List<SPTenderCommonData> awarddetails2 = tenderCommonService.returndata("GetAwardDetails2", request.getParameter("NOAID"), request.getParameter("tenderid"));
                            List<SPCommonSearchDataMore> tenderdetails = commonSearchDataMoreService.geteGPData("getTendererDetails", noaIssueId);
                            List<SPTenderCommonData> companyList = tenderCommonService.returndata("GetEvaluatedBidders", tenderId, pckLotId.get(0).getFieldName1());
                        String Procurementnature = "";
                        String status ="";
                        String procMethod = "";
                        if (!procurenature.isEmpty()) {
                                Procurementnature = procurenature.get(0).getFieldName1();
                           }
                        if (!awarddetails2.isEmpty()) {
                                status = awarddetails2.get(0).getFieldName6();
                           }
                        if(tenderdetails!=null && !tenderdetails.isEmpty()){
                            procMethod = tenderdetails.get(0).getFieldName5();
                        }
                        String eveType = commonService.getEventType(tenderId).toString();
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userid = hs.getAttribute("userId").toString();
                            }
                            
                %>
                <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                    <%@include  file="../resources/common/AfterLoginTop.jsp"%>

                <%
                    userNameNOA = objUserName.toString();
                } %>
            
            <div class="dashboard_div">
                <!--Dashboard Header End-->
                <div  id="print_area">
                <div class="contentArea_1">
                     <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                    <div class="pageHead_1"><%if (request.getParameter("app") != null && request.getParameter("app").equalsIgnoreCase("yes")) {%>Accept / Decline Letter of Acceptance<% } else {%>Letter of Acceptance (LOA)<% }%>
                        <span class="c-alignment-right">
                            <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                                  <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;&nbsp;
                                 <a id="hdnpdf" class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=genId%>&tId=<%=tenderId%>&NOAID=<%=NOAID%>&userNameNOA=<%=userNameNOA%>">Save As PDF </a>&nbsp;&nbsp;
                            <% } %>
                            <a id="hdnLink" href="NOAListing.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a>
                        </span>

                    </div>
                    <% } %>
               
                    <%if (request.getParameter("flag") != null && request.getParameter("flag").equals("false")) {%>
                    <div id="errordiv" class="responseMsg errorMsg">System facing problem..Please try again later.</div>
                    <%}%>
                    <form id="frmnoa" name="frmnoa" action="" method="post">
                        <input type="hidden" name="hdextReqBy" id="hdextReqBy" value="87"/>
                    <table width="100%" border="0" cellpadding="0" cellspacing="5" class="t_space">
                        <%if(isPDF.equalsIgnoreCase("true")){%>
                        <tr>
                            <td colspan="2">
                                <div style="font-size: 15px;font-weight: bold;">Letter of Acceptances</div>
                            </td>
                        </tr>
                        <%}%>
                        <%
                                    if (!awarddetails.isEmpty()) {
                        %>
                        <tr>
                            <td width="40%" class="t-align-left ff">
                                Contract No:  <%=awarddetails.get(0).getFieldName1()%><label></label>
                            </td>
                            <td width="60%" class="t-align-right ff">Date: <%=awarddetails.get(0).getFieldName3()%><label></label></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">To : 

                            </td>
                            <td class="t-align-right ff">&nbsp;</td>
                        </tr>
                        <%
                            if(tenderdetails!=null && !tenderdetails.isEmpty()){
                            %>
                            <tr>
                                <td class="t-align-left ff">Name: <%=commonService.getConsultantName(pckLotId.get(0).getFieldName2())%>
                                </td>
                                <td class="t-align-right ff"></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Address: <%=tenderdetails.get(0).getFieldName1() %>
                                </td>
                                <td class="t-align-right ff"></td>
                            </tr>
                        <%--<tr>
                            <td colspan="2" class="t-align-left ff"><br/><%=tenderdetails.get(0).getFieldName3()%>
                            </td>
                        </tr>--%>
                       
<!--                        <tr>
                            <td colspan="2" class="t-align-left ff">PE office name : <-%=awarddetails.get(0).getFieldName5()%>
                            </td>
                        </tr>-->
                        <tr>
                            <td class="t-align-left ff">&nbsp;<label></label></td>
                            <td class="t-align-right ff">&nbsp;</td>
                        </tr>
                        <%
                         String Amount ="";
                         String AmountPer ="";
                        // int j=awarddetails.size();
                         for (int i = 0; i <awarddetails.size() ; i++)
                             {
                         Amount=Amount+"   "+awarddetails.get(i).getFieldName4();

                             }
                         for (int j = 0; j <awarddetails2.size() ; j++)
                             {

                         AmountPer=AmountPer+ "   " +awarddetails2.get(j).getFieldName2();
                             }
                                         if("Services".equalsIgnoreCase(Procurementnature)){
                                     %>
                                     <tr><td colspan="2">
                                     <jsp:include page="../resources/common/NOAServiceLetter.jsp" >
                                        <jsp:param name="noaId" value="<%=noaIssueId%>" />
                                        <jsp:param name="dop" value="<%=procurenature.get(0).getFieldName2()%>" />
                                        <jsp:param name="brief" value="<%=tenderdetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="amt" value="<%=awarddetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="contractSignDay" value="<%=awarddetails2.get(0).getFieldName1()%>" />
                                        <jsp:param name="perSecAmt" value="<%=awarddetails2.get(0).getFieldName2()%>" />
                                        <jsp:param name="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>" />
                                        <jsp:param name="perSecDt" value="<%=awarddetails2.get(0).getFieldName10()%>" />
                                    </jsp:include>
                                         </td></tr>
                                     <%}else{
                                        if("RFQ".equalsIgnoreCase(eveType) || "RFQL".equalsIgnoreCase(eveType) || "RFQU".equalsIgnoreCase(eveType)){
                                     %>
                                     <tr><td colspan="2">
                                     <jsp:include page="../resources/common/NOAGoodsWorkLetter.jsp" >
                                        <jsp:param name="dop" value="<%=procurenature.get(0).getFieldName2()%>" />
                                        <jsp:param name="brief" value="<%=tenderdetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="amt" value="<%=awarddetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="contractSignDay" value="<%=awarddetails2.get(0).getFieldName1()%>" />
                                        <jsp:param name="perSecAmt" value="<%=awarddetails2.get(0).getFieldName2()%>" />
                                        <jsp:param name="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>" />
                                        <jsp:param name="perSecDt" value="<%=awarddetails2.get(0).getFieldName5()%>" />
                                        <jsp:param name="nature" value="<%=Procurementnature%>" />
                                    </jsp:include>
                                             </td></tr>
                                     <%}else{
                                    if (!procurenature.isEmpty()) {
                            %>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	This is to notify you that your bid dated <em><strong><%=procurenature.get(0).getFieldName2()%></strong></em>
                                    <%
                                            if (Procurementnature.equalsIgnoreCase("Works")) {
                                    %>
                                for the execution of the <em><strong>Works</strong></em> for
                                <%     } else if (Procurementnature.equalsIgnoreCase("Goods")) {%>
                                <u>for the supply of <em><strong>Goods and related Services </em></strong> for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Services")) {%>
                                as per new format coming in e-LoI : <u>the following consulting services for</u>
                                <%     } 
                                    Amount = Amount.replace("Tk.","Nu.");
                                    Amount = Amount.replace("Taka","Ngultrum");
                                %>
                                    <em><strong><%=awarddetails.get(0).getFieldName2()%></strong></em> for the Contract Price of the equivalent of <em><strong><%=Amount%></strong></em> in BTN, as corrected and modified  in accordance with the Instructions to Bidders is hereby accepted by our Agency.<%-- <em><strong><%=awarddetails.get(0).getFieldName5()%></strong></em>.--%>
                                </td>
                            </tr>
                            <%}%>
                            <tr>
                                <td colspan="2" class="t-align-left">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	The Contract in duplicate is attached hereto. You are hereby instructed to: </br> </br>
                                </td>
                            </tr>
                            <%
                                        if (!awarddetails2.isEmpty()) {
                            %>
                            <tr>
                                <td colspan="2" class="t-align-left">
                                    <div class="listBox_emailFormatNOA">
                                        <ol>
                                            <%--<li>--%>
                	<%--accept in writing the Notification of Award within <input type="hidden" id="noaIssueDays" value="<%=awarddetails2.get(0).getFieldName1()%>"/><b><label id="noaIssueDaysWords"></label></b> <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> working days of its issuance pursuant to ITB Clause --%>
                        (a)	confirm your acceptance of this Letter of Acceptance by signing and dating both copies of it, and returning one copy to us no later than <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> days from the date hereof; and </br> </br>                   
                        <%-- </li> --%>
                                            <%
                                                                                        //List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                                                                        if (!pckLotId.isEmpty() && !"".equalsIgnoreCase(pckLotId.get(0).getFieldName3()) && !"ZERO".equalsIgnoreCase(pckLotId.get(0).getFieldName3())) {
                                                                                            AmountPer = AmountPer.replace("Tk.","Nu.");
                                                                                            AmountPer = AmountPer.replace("Taka","Ngultrum");
                                            %>
                                            <br/>
                                            <%--<li>--%>
                	<%--furnish a Performance Security in the specified format  and in the amount of <em><strong><%=AmountPer%></strong></em>, within <input type="hidden" id="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>"/><label id="perSecDaysWords"></label><em><strong> (<%=awarddetails2.get(0).getFieldName3()%>)</strong></em> days of acceptance of this Notification of Award but not later than <em><strong><u><%=awarddetails2.get(0).getFieldName4()%></u></strong></em>, in accordance with ITB Clause--%>
                        (b)	forward the Performance Security pursuant to ITB Sub-Clause 47.1, i.e., within <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> days after receipt of this Letter of Acceptance, and pursuant to GCC Sub-Clause 19.1                    
                        <%--</li>--%>
                                            <br/>
                                            <% }%>
                                            <%--<li>
                	sign the Contract within <input type="hidden" id="contractSignDays" value="<%=awarddetails2.get(0).getFieldName9()%>"/><b><label id="contractSignDaysWords"></label></b> <em><strong>(<%=awarddetails2.get(0).getFieldName9()%>) </strong></em>days of issuance of this Notification of Award but not later than <em><strong><%=awarddetails2.get(0).getFieldName5()%></strong></em>, in accordance with ITB Clause
                                            </li>--%>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	<%--You may proceed with the execution
                                    <%
                                            if (!procurenature.isEmpty()) {
                                               if (Procurementnature.equalsIgnoreCase("Works")) {
                                    %>
                                   <u>for the execution of the Works for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Goods")) {%>
                                   <u>for the supply of Goods and related Services for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Services")) {%>
                                <u>as per new format coming in e-LoI : the following consulting services for</u>
                                <%     }
                                                                                }%>
                                    only upon completion of the above tasks. You may also please note that this Notification of Award shall constitute the formation of this Contract, which shall become binding upon you.
        --%>
                                </td>
                            </tr>
                            <tr>
                                <td height="5px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	<%--We attach the draft Contract and all other documents for your perusal and signature.--%>
                                </td>
                            </tr>
                            <%}}}%>
                        
                        <tr>
                        <%
                            String hopaName = "";
                            String hopaOffice = "";
                            List<SPTenderCommonData> tenderinfo = tenderCommonService.returndata("getTenderInfoAfterTCApprove", tenderId, null);
                            List<SPTenderCommonData> hopaInfo3 = tenderCommonService.returndata("getHopaName", tenderId, null);
                            hopaName = hopaInfo3.get(0).getFieldName1();
                            
                            List<SPTenderCommonData> hopaInfo2 = tenderCommonService.returndata("getHopaOffice", tenderId, null);
                            hopaOffice = hopaInfo2.get(0).getFieldName1();
                            
                        %> 
                        
                        <td class="t-align-left" style="float: left;">
                                    Authorized Signature: 
                                    <br/>
                                    <br/>
                                    Name: <em><strong>[<%=hopaName%>]</em></strong>
                                    <br/>
                                    <br/>
                                    Title of Signatory:  HOPA
                                    <br/>
                                    <br/>
                                    Name of Agency:<em><strong>[
                                    <%=hopaOffice%>
                                    ]</em></strong></br></br></br>
                                </td>
                        
                        </tr>
                            <tr><td></td><td></td></tr>
                            <%
                        TblNoaAcceptance tna = tenderSrBean.getTblNoaAcceptance(Integer.parseInt(NOAID));
                        if(tna!=null && "approved".equalsIgnoreCase(tna.getAcceptRejStatus())){
                    %>
                    <div class="t-align-left t_space" style="background-color: #f6f6f6; padding: 5px;">
                        <%--<table width="100%" cellpadding="0" cellspacing="5">
                        <tr>
                            <th colspan="4" align="center" class="ff">Account Information</th>
                        </tr>
                        <tr>
                            <td width="15%" class="ff">Title of the Account :  </td>
                            <td width="35%"><%=tna.getAccountTitle()!=null?tna.getAccountTitle():"-"%></td>
                            <td width="15%" class="ff">Name of Financial Institute :  </td>
                            <td width="35%"><%=tna.getBankName()!=null?tna.getBankName():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Branch :  </td>
                            <td><%=tna.getBranchName()!=null?tna.getBranchName():"-"%></td>
                            <td class="ff">Account Number :  </td>
                            <td><%=tna.getAccountNo()!=null?tna.getAccountNo():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Telephone :  </td>
                            <td><%=tna.getTelephone()!=null?tna.getTelephone():"-"%></td>
                            <td class="ff">Fax No :  </td>
                            <td><%=tna.getFax()!=null?tna.getFax():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch e-mail ID :  </td>
                            <td><%=tna.getEmail()!=null?tna.getEmail():"-"%></td>
                            <td class="ff">SWIFT Code :  </td>
                            <td><%=tna.getSwiftCode()!=null?tna.getSwiftCode():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Address : </td>
                            <td><%=tna.getAddress()!=null?tna.getAddress():"-"%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>--%>
                    </div>
                    <%}%>
                    <tr>
                        <td colspan="2">
                        <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=pcklotid%>" />
                            <jsp:param name="comuserId" value="<%=userid%>" />
                            <jsp:param name="isPDF" value="<%=isPDF%>" />
                        </jsp:include>
                            </td>
                        </tr>
                        <tr>
                            <td height="5px" colspan="2"></td>
                        </tr>
                        <%
                                                                if ((!status.equals("pending") || "yes".equalsIgnoreCase(request.getParameter("app"))) && !("true".equalsIgnoreCase(isPDF))) {
                        %>
                        <tr>
                            <td colspan="2" class="t-align-left" id="tdCommentsAction">
                                <table width="100%" cellpadding="0" cellspacing="5">
                                    <tr>
                                        <td width="8%" class="ff" align="left" valign="top">Comment : <%if (request.getParameter("app").equals("yes")) {%><span class="mandatory">*</span><% } %></td>
                                        <td width="92%" align="left" valign="top">
                                            <%
                                                                                                                if (request.getParameter("app").equals("yes")) {
                                            %>
                                            <textarea rows="5" id="txtcomments" name="txtcomments" class="formTxtBox_1" style="width:100%;"></textarea>
                                            <%} else {%>
                                            <%=awarddetails2.get(0).getFieldName7()%>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"><%if (request.getParameter("app").equals("yes")) {%>Action : <span class="mandatory">*</span><%}%></td>
                                        <td>
                                            <%
                                                                                                                if (request.getParameter("app").equals("yes")) {
                                                                                                                    if(!(Procurementnature.equalsIgnoreCase("Services") && "DPM".equalsIgnoreCase(procMethod))){
                                            %>
                                            <select id="actionselect" name="actionselect" class="formTxtBox_1" onchange="showHideForm();">
                                                <option value="">Select</option>
                                                <%if(!Procurementnature.equalsIgnoreCase("Services")){%>
                                                <option value="approved">Accept</option>
                                                <!--<option value="decline">Decline</option>-->
                                                <%}else{%>
                                                 <option value="approved">Accept</option>
                                                <%}%>
                                            </select>
                                            <%}else{%>
                                            <label>Accept</label>
                                            <input type="hidden" id="actionselect" name="actionselect" value="approved" />
                                            <%}%>
                                        </td>
                                    </tr>
                                    
                                    <tr><td colspan="2">
                                            <table id="formTbl" width="100%" cellpadding="0" cellspacing="5" <%if(!(Procurementnature.equalsIgnoreCase("Services") && "DPM".equalsIgnoreCase(procMethod))){%>style="display: none"<%}%>>
                                                    <tr>
                                                        <td class="ff" width="15%"> 
                                                            Title of the Account :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td width="35%">
                                                            <input type="text" id="txtTitleAccount" name="txtTitleAccount" class="formTxtBox_1" maxlength="100"/>
                                                        </td>
                                                        <td class="ff" width="15%">
                                                            Name of Financial Institute :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td width="35%">
                                                            <input type="text" id="txtBank" name="txtBank" class="formTxtBox_1"  />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff" >
                                                            Name of Branch :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtBranch" name="txtBranch" class="formTxtBox_1"  />
                                                        </td>
                                                    
                                                        <td class="ff" >
                                                            Account Number :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtAccNumber" name="txtAccNumber" class="formTxtBox_1" />
                                                        </td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td class="ff">
                                                            Telephone :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtTel" name="txtTel" class="formTxtBox_1" />
                                                        </td>
                                                    
                                                        <td class="ff">
                                                            Fax No :  
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtFax" name="txtFax" class="formTxtBox_1" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">
                                                            Branch e-mail ID : 
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtemailId" name="txtemailId" class="formTxtBox_1" />
                                                        </td>
                                                        <td class="ff">
                                                            SWIFT Code :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtSwiftCode" name="txtSwiftCode" class="formTxtBox_1" />
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td class="ff">
                                                           Branch Address :  <span class="mandatory">*</span>
                                                        </td>
                                                        <td>
                                                            <textarea rows="5" id="txtAddress" name="txtAddress" class="formTxtBox_1" ></textarea>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            
                                        
                                    <%}%> <%--else {
                                            if("approved".equalsIgnoreCase(awarddetails2.get(0).getFieldName8().trim())){%>
                                         Accepted
                                            <%}else{
                                            %>
                                         Declined
                                            <% } } --%>
                                    </td></tr>
                                </table>

                            </td>
                        </tr>
                        <%
                                                              }                                                     if (request.getParameter("app").equals("yes")) {
                        %>
                        <tr>
                            <td colspan="2" class="t-align-center noprint" id="tdSubmit">
                                <label class="formBtn_1 t_space">
                                    <input name="btnnoa" id="btnnoa" type="submit" value="Submit" onclick="return showForm();"/>
                                </label>
                            </td>
                        </tr>
                        <%}
                                                                }%>
                    </table>
                    </form>
                    <%}%>
                   <div>&nbsp;</div>
                   
               </div>
                   </div>
                  

                   

                 <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
                <% } %>
            </div>
                <!--<div id="dialog-confirm" title="Empty the recycle bin?">
                    <p><span style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>-->
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%--<script type="text/javascript">
                    $(document).ready(function() {
                                $("#print").click(function() {
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $(this).parent().append("<span class='reqF_1'>-</span>");
                                            $(this).hide();
                                            $('.reqF_1').remove();
                                    })
                                    $("th[view='print']").each(function(){
                                            $(this).hide();
                                    })
                                    $("td[view='print']").each(function(){
                                            $(this).hide();
                                    })

                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                        if(temp=="-"){
                                            $(this).show();
                                        }
                                    })
                                    $("th[view='print']").each(function(){
                                            $(this).show();
                                    })
                                    $("td[view='print']").each(function(){
                                            $(this).show();
                                    })
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }
                            
        </script>--%>
    <script type="text/javascript">
             $(document).ready(function() {
                                $("#print").click(function() {
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $(this).parent().append("<span class='disp_link'>-</span>");
                                            $(this).hide();
                                            
                                            //$('.reqF_1').remove();
                                    })
                                    $('#hdnLink').hide();
                                    $('#hdnpdf').hide();
                                    $('#print').hide();
                                    <%if (!status.equals("pending") || "yes".equalsIgnoreCase(request.getParameter("app"))) {%>
                                        $('#tdCommentsAction').hide();
                                    <%}%>
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $('.disp_link').remove();
                                            //$('.reqF_1').remove();
                                            $(this).show();
                                    })
                                    $('#hdnLink').show();
                                    $('#hdnpdf').show();
                                    $('#print').show();
                                    <%if (!status.equals("pending") || "yes".equalsIgnoreCase(request.getParameter("app"))) {%>
                                        $('#tdCommentsAction').show();
                                    <%}%>
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }
        if(document.getElementById('noaIssueDays'))
        {
           document.getElementById('noaIssueDaysWords').innerHTML =WORD(document.getElementById('noaIssueDays').value);
        }
        //document.getElementById('perSecDaysWords').innerHTML =WORD(document.getElementById('perSecDays').value);
        document.getElementById('contractSignDaysWords').innerHTML =WORD(document.getElementById('contractSignDays').value);

        </script>

</html>
<%
    tenderSrBean = null;
    pdfCmd = null;
    pdfConstant = null;
%>