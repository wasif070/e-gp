<%--
    Document   : GetBidData
    Created on : Nov 27, 2010, 4:17:03 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="com.cptu.egp.eps.web.utility.HashUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.SignerImpl" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
<jsp:useBean id="tenderTablesSrBean" class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<%

    // Coad added by Dipal for Audit Trail Log.
    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

    String hashKey = request.getParameter("hdnPwd");
    String buyerHashKey = request.getParameter("hdnBuyerPwd");
    String idType="tenderId";
    int auditId=0;
    String auditAction="Bidder has Filled Forms";
    String moduleName=EgpModule.Bid_Submission.getName();
    String remarks="Bidder(User id): "+session.getAttribute("userId")+" has Filled Form id:"+request.getParameter("hdnFormId");
               
    Logger LOGGER = Logger.getLogger("GetBidData.jsp");
    String action = "Save";
    if(request.getParameter("save")!=null && !"".equalsIgnoreCase(request.getParameter("save"))){
        action = request.getParameter("save");
    }else{
        if(request.getParameter("encrypt")!=null && !"".equalsIgnoreCase(request.getParameter("encrypt"))){
            action = request.getParameter("encrypt");
        }else if(request.getParameter("action")!=null && !"".equalsIgnoreCase(request.getParameter("action"))){
            action = request.getParameter("action");
        }
    }

    //out.println(" action " + action);
    int bidId = 0;
    if(request.getParameter("hdnBidId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnBidId"))){
        bidId = Integer.parseInt(request.getParameter("hdnBidId"));
    }

    int lotId = 0;
    if(request.getParameter("hdnLotId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnLotId"))){
        lotId = Integer.parseInt(request.getParameter("hdnLotId"));
    }


    long msg=0;
    int tableCount = 0;
    int tableId = 0;
    int columnId = 0;

    int tableIds[] = new int[1000];
    for(int i=0;i<1000;i++){
        tableIds[i] = 0;
    }

    if("Encrypt And Save".equalsIgnoreCase(action))
    {
        // General Declaration
        ListIterator<CommonFormData> tblBidCellsDtl = null;
        ArrayList arr = new ArrayList();
        int count = 0;
        TenderOpening tenderOpen = new TenderOpening();
        // Bid Form
        int tenderId = 0;
        if(request.getParameter("hdnTenderId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnTenderId"))){
            tenderId = Integer.parseInt(request.getParameter("hdnTenderId"));
        }

        int tenderFormId = 0;
        if(request.getParameter("hdnFormId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnFormId"))){
            tenderFormId = Integer.parseInt(request.getParameter("hdnFormId"));
        }

        String bidTableIds[] = null;
        if(request.getParameter("hdnBidTableIds")!=null && !"".equalsIgnoreCase(request.getParameter("hdnBidTableIds"))){
            bidTableIds = request.getParameterValues("hdnBidTableIds");
        }

        int userId = 0;
        if(session.getAttribute("userId") != null) {
            if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
        }

        
        /* validation for submit form more than once if form is single time fill*/
        msg = 0;
        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(tenderFormId);
        String isMultipleFormFeeling=null;
        for(CommonFormData formData : formDetails)
        {
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
        }

        if(isMultipleFormFeeling!= null && isMultipleFormFeeling.equalsIgnoreCase("no"))
        {
            msg = tenderBidSrBean.checkBidFormEncriptedAndSaved(tenderId,tenderFormId,userId);
            if(msg > 0)
            {
               response.sendRedirect("BidPreperation.jsp?tab=4&lotId="+lotId+"&tenderId="+tenderId+"&msg=existEncrypt");
            }
        }   

        if(msg == 0)
        {

            List<String> newbidTableIds = new ArrayList<String>();

            for(int i=0;i<(bidTableIds.length);i++){

                bidTableIds[i] = bidTableIds[i].substring(0, bidTableIds[i].length()-2);
                String splitbidTableId[] = bidTableIds[i].split(" , \\s*");
                for(String bidtblId:splitbidTableId){
                    newbidTableIds.add(bidtblId);
                    //bidTableIds[i] = bidTableIds[i].replaceAll(" , ", "");
                }
                splitbidTableId = null;
            }
            if(newbidTableIds!=null){
                 bidTableIds = newbidTableIds.toArray(new String[newbidTableIds.size()]);
            }

            int rowId = 0;
            int tmpRowId = 0;
            int cellId = 0;
            String cellValue = "";
            tableId = 0;
            int cellCnt = 0;
            int bidTableCnt = 0;
            int clCnt = 0;
            boolean bidFlag = false;
            boolean rFlag = true;
            int noOfRows = 0;
            int rows = 0;
            int totalTableCnt = 0;

            List<TblTenderTables> listTenderTables = new ArrayList<TblTenderTables>();
            String isMultipleFilling = "";
            boolean isTableWithGrandTotal = false;

            String xmlBidEncrypt = "";
            xmlBidEncrypt += "<root>";

            int identkey = 0;
            List<CommonFormData> formTables = tenderBidSrBean.getFormTables(tenderFormId);
            int cntTableId = 0;
            for(CommonFormData formData : formTables){
                tableId = formData.getTableId();

                if(request.getParameter("tableCount"+tableId) != null){
                    tableCount = Integer.parseInt(request.getParameter("tableCount"+tableId));
                }
                for(int i=1;i<=tableCount;i++){
                    tableIds[cntTableId++] = tableId;
                    arr.add(tableId);
                }
            }
            tableId = 0;
            List<String> listCellValue = new ArrayList<String>();
            for(int i=0;i<tableIds.length;i++)
            {
                clCnt=0;
                if(tableIds[i]!=0)
                {
                        //identkey++;
                        if(tableId != tableIds[i])
                        {
                            tableId = tableIds[i];
                            tblBidCellsDtl = tenderBidSrBean.getTableBidCells(tableId).listIterator();
                            isTableWithGrandTotal = tenderTablesSrBean.isTotalFormulaCreated(tableId);
                            listTenderTables = tenderTablesSrBean.getTenderTablesDetail(tableId);

                            if(!listTenderTables.isEmpty() && isTableWithGrandTotal){
                                rows = listTenderTables.get(0).getNoOfRows();
                                isMultipleFilling = listTenderTables.get(0).getIsMultipleFilling();
                            }
                            tmpRowId = 0;
                            bidFlag = true;
                        }
                        else
                        {

                            while(tblBidCellsDtl.hasPrevious())
                            {
                                tblBidCellsDtl.previous();
                            }
                            bidFlag = false;
                            cellCnt = tenderBidSrBean.getBidCellCnt(tableId);
                            //tmpRowId++;
                            bidTableCnt++;
                            noOfRows = rowId;
                            rFlag = true;
                        }

                        while(tblBidCellsDtl.hasNext())
                        {
                            CommonFormData bidCelData = tblBidCellsDtl.next();

                            columnId = bidCelData.getColumnId();
                            if(isTableWithGrandTotal && "Yes".equalsIgnoreCase(isMultipleFilling)){
                                if(bidCelData.getRowId() > (rows-1) && tableCount!=(i+1)){
                                    continue;
                                }
                            }

                            if(bidFlag)
                            {
                                rowId = bidCelData.getRowId();
                                tmpRowId = rowId;
                                bidTableCnt=0;
                                noOfRows = 0;
                            }
                            else
                            {
                                /*
                                if(rFlag)
                                {
                                    clCnt++;
                                }
                                if((cellCnt==1) && (!(rFlag)))
                                {
                                    tmpRowId++;
                                    rFlag = false;
                                }
                                else
                                {
                                    if(clCnt==(cellCnt+1))
                                    {
                                        tmpRowId++;
                                        clCnt=0;
                                        rFlag = false;
                                    }
                                }
                                */
                                rowId = bidCelData.getRowId();
                                tmpRowId = (noOfRows*bidTableCnt)+rowId;
                            }

                            cellValue = request.getParameter("row"+tableId+"_"+tmpRowId+"_"+columnId);
                            if(cellValue!=null){
                                SignerImpl obj = new SignerImpl();
                                obj.setData(cellValue);
                                cellValue = obj.getSymEncrypt(buyerHashKey);

                                listCellValue.add(cellValue);
                                cellValue = cellValue.replace(" ","");
                            }

                            cellId = bidCelData.getCellId();

                            xmlBidEncrypt += "<tbl_TenderBidEncrypt ";
                            xmlBidEncrypt += "bidTableId=\""+bidTableIds[identkey]+"\" ";
                            xmlBidEncrypt += "tenderColumnId=\""+ columnId +"\" ";
                            xmlBidEncrypt += "tenderTableId=\""+ tableId +"\" ";
                            xmlBidEncrypt += "cellValue=\""+ cellValue +"\" ";
                            xmlBidEncrypt += "rowId=\""+ (rowId) +"\" ";
                            xmlBidEncrypt += "cellId=\""+ cellId +"\" ";
                            xmlBidEncrypt += "formId=\""+ tenderFormId +"\" ";
                            xmlBidEncrypt += "/>";
                            //bidCelData = null;
                        }
                }
                identkey++;
            }
            xmlBidEncrypt += "</root>";
            count = tenderOpen.checkCellValue(listCellValue, String.valueOf(tenderId));
            // Filling value for Audit Log
            idType="tenderId";
            auditId=tenderId;
            auditAction="Tenderer has Encrypted Forms";
            moduleName=EgpModule.Bid_Submission.getName();
            remarks="Tenderer(User id): "+session.getAttribute("userId")+" has Encrypted Form id:"+request.getParameter("hdnFormId")+" and Bid id:"+bidId;
            String pageName = "BidForm.jsp?tenderId="+tenderId+"&formId="+tenderFormId+"&bidId="+bidId+"&action=Encrypt&eMessage=err";
            try{

                //tableName, xmlBidEncrypt, whereCond, isInEditMode(bool)
                if(count==0){
                    if(tenderBidSrBean.insertBidEncrypt("tbl_TenderBidEncrypt", xmlBidEncrypt, "", false)){
                        int uId = 0;
                        HttpSession hs = request.getSession();
                        if(hs.getAttribute("userId")!=null){
                            uId = Integer.parseInt(hs.getAttribute("userId").toString());
                        }
                        int fsId = tenderBidSrBean.getFinalSubId(tenderId, uId); // Is entry present
                        String ipAddress = request.getHeader("X-FORWARDED-FOR");

                        if(ipAddress == null || "null".equals(ipAddress)){
                            ipAddress = request.getRemoteAddr();
                            if(ipAddress == null || "null".equals(ipAddress)){
                                ipAddress = "-1";
                            }
                        }
                        if(fsId != -1){
                            tenderBidSrBean.insertIntoFSDtl(fsId, tenderFormId, bidId, ipAddress);
                        }else{
                            int finalSubId = tenderBidSrBean.insertIntoFS(tenderId, uId);
                            if(finalSubId != -1){
                                tenderBidSrBean.insertIntoFSDtl(finalSubId, tenderFormId, bidId, ipAddress);
                            }
                        }

                    }else{

                    }
                }
            }catch(Exception e){
                auditAction="Error in Bidder has Encrypted Forms "+e.getMessage();
                LOGGER.error("Error in Insert form data : "+e.toString());
                e.printStackTrace();
            }
            finally
            {
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
            }
            if(count==0){
                if(lotId==0){
                    pageName = "LotTendPrep.jsp?tab=4&tenderId="+tenderId;
                }else{
                        pageName = "BidPreperation.jsp?tab=4&lotId="+lotId+"&tenderId="+tenderId;
                }
            }
            response.sendRedirect(pageName);
        }
    }

    if("Save".equalsIgnoreCase(action) || "Update".equalsIgnoreCase(action))
    {
        // General Declaration
        ListIterator<CommonFormData> tblBidCellsDtl = null;

        // Bid Form
        int tenderId = 0;
        if(request.getParameter("hdnTenderId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnTenderId"))){
            tenderId = Integer.parseInt(request.getParameter("hdnTenderId"));
        }

        int tenderFormId = 0;
        if(request.getParameter("hdnFormId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnFormId"))){
            tenderFormId = Integer.parseInt(request.getParameter("hdnFormId"));
        }

        int userId = 0;
        if(session.getAttribute("userId") != null) {
            if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
        }

        int submittedTime = 1;
       
        /* validation for submit form more than once if form is single time fill */
        if("Save".equalsIgnoreCase(action))
        {
            msg = 0;
            List<CommonFormData> formDetails = tenderBidSrBean.getFormData(tenderFormId);
            String isMultipleFormFeeling=null;
            for(CommonFormData formData : formDetails)
            {
                isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            }
           
                if(isMultipleFormFeeling!= null && isMultipleFormFeeling.equalsIgnoreCase("no"))
                {
                    
                    msg = tenderBidSrBean.checkBidFormSubmited(tenderId,tenderFormId,userId);
                   
                    if(msg > 0)
                    {
                       response.sendRedirect("BidPreperation.jsp?tab=4&lotId="+lotId+"&tenderId="+tenderId+"&msg=exist");
                    }
                }
        }
        if(msg == 0)
        {
            String submissionDt =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date().getTime());


            String tblXML_TenderBidForm = "";
            String tblXML_TenderBidTable = "";
            String tblXML_TenderBidDetail = "";
            String tblXML_TenderBidSign = "";

            tblXML_TenderBidForm = "<root>"
                + "<tbl_TenderBidForm "
                + "tenderId=\""+ tenderId +"\" "
                + "userId=\""+ userId +"\" "
                + "tenderFormId=\""+ tenderFormId +"\" "
                + "submittedTime=\""+ submittedTime +"\" "
                + "submissionDt=\""+ submissionDt +"\" "
                + "pkgLotId=\""+ lotId +"\" "
                + "/>"
                + "</root>";


            // Bid Table
                List<CommonFormData> formTables = tenderBidSrBean.getFormTables(tenderFormId);

                int cntTableId = 0;
                int submittedTimes = 1;
                tblXML_TenderBidTable = "<root>";
                for(CommonFormData formData : formTables){
                    tableId = formData.getTableId();

                    if(request.getParameter("tableCount"+tableId) != null){
                        tableCount = Integer.parseInt(request.getParameter("tableCount"+tableId));
                    }

                    for(int i=1;i<=tableCount;i++)
                    {
                        tblXML_TenderBidTable += "<tbl_TenderBidTable "
                        + "bidId=\"IdentKey\" "
                        + "tenderFormId=\""+ tenderFormId +"\" "
                        + "tenderTableId=\""+ tableId +"\" "
                        + "submittedTimes=\""+ (submittedTimes) +"\" "
                        + "submissionDt=\""+ submissionDt +"\" "
                        + "/>";
                        tableIds[cntTableId++] = tableId;
                    }
                    //submittedTimes++;
                }
                tblXML_TenderBidTable += "</root>";

                // Bid Details - cellvalue
                int rowId = 0;
                int tmpRowId = 0;
                int cellId = 0;
                int cellCnt = 0;
                int clCnt = 0;
                boolean rFlag = true;
                int bidTableCnt = 0;
                String cellValue = "";
                tableId = 0;
                boolean bidFlag = false;
                int noOfRows = 0;
                int rows = 0;
                int totalTableCnt = 0;
                List<TblTenderTables> listTenderTables = new ArrayList<TblTenderTables>();
                String isMultipleFilling = "";
                boolean isTableWithGrandTotal = false;

                tblXML_TenderBidDetail = "<root>";

                int identkey = 0;
                for(int i=0;i<tableIds.length;i++)
                {
                    clCnt=0;
                    if(tableIds[i]!=0)
                    {
                        identkey++;
                        if(tableId != tableIds[i])
                        {
                            tableId = tableIds[i];
                            tblBidCellsDtl = tenderBidSrBean.getTableBidCells(tableId).listIterator();
                            isTableWithGrandTotal = tenderTablesSrBean.isTotalFormulaCreated(tableId);
                            listTenderTables = tenderTablesSrBean.getTenderTablesDetail(tableId);

                            if(!listTenderTables.isEmpty() && isTableWithGrandTotal){
                                rows = listTenderTables.get(0).getNoOfRows();
                                isMultipleFilling = listTenderTables.get(0).getIsMultipleFilling();
                            }
                            tmpRowId = 0;
                            bidFlag = true;

                        }
                        else
                        {
                            while(tblBidCellsDtl.hasPrevious())
                            {
                                tblBidCellsDtl.previous();
                            }
                            bidFlag = false;
                            cellCnt = tenderBidSrBean.getBidCellCnt(tableId);
                            //tmpRowId++;
                            bidTableCnt++;
                            noOfRows = rowId;
                            rFlag = true;
                        }

                        //rowId++;

                        while(tblBidCellsDtl.hasNext())
                        {
                            CommonFormData bidCelData = tblBidCellsDtl.next();

                            columnId = bidCelData.getColumnId();
                            if(isTableWithGrandTotal && "Yes".equalsIgnoreCase(isMultipleFilling)){
                                if(bidCelData.getRowId() > (rows-1) && tableCount!=(i+1)){
                                    continue;
                                }
                            }

                            if(bidFlag)
                            {
                                rowId = bidCelData.getRowId();
                                tmpRowId = rowId;
                                bidTableCnt=0;
                                noOfRows = 0;
                            }
                            else
                            {
                                /*
                                if(rFlag)
                                {
                                    clCnt++;
                                }
                                if((cellCnt==1) && (!(rFlag)))
                                {
                                    tmpRowId++;
                                    rFlag = false;
                                }
                                else
                                {
                                    if(clCnt==(cellCnt+1))
                                    {
                                        tmpRowId++;
                                        clCnt=0;
                                        rFlag = false;
                                    }
                                }
                                */

                                rowId = bidCelData.getRowId();
                                tmpRowId = (noOfRows*bidTableCnt)+rowId;
                            }

                            cellValue = request.getParameter("row"+tableId+"_"+tmpRowId+"_"+columnId);


                            

                            if(cellValue!=null){

                                SignerImpl obj = new SignerImpl();
                                obj.setData(cellValue);
                                cellValue = obj.getSymEncrypt(hashKey);

                                cellValue = cellValue.replace(" ","");
                            }
                            cellId = bidCelData.getCellId();

                            tblXML_TenderBidDetail += "<tbl_TenderBidDetail "
                            + "bidTableId=\"IdentKey"+identkey+"\" "
                            + "columnId=\""+ columnId +"\" "
                            + "tenderTableId=\""+ tableId +"\" "
                            + "cellValue=\""+ cellValue +"\" "
                            + "rowId=\""+ (rowId) +"\" "
                            + "cellId=\""+ cellId +"\" "
                            + "/>";

                            //bidCelData = null;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                tblXML_TenderBidDetail += "</root>";

            // Bid Sign

                String signText = request.getParameter("signedData");

                tblXML_TenderBidSign = "<root>"
                + "<tbl_TenderBidSign "
                + "signText=\""+ signText +"\" "
                + "signDt=\""+ submissionDt +"\" "
                + "bidId=\"IdentKey\" "
                + "/>"
                + "</root>";



                if("Save".equalsIgnoreCase(action))
                {
                    // Filling value for Audit Log
                    idType="tenderId";
                    auditId=tenderId;
                    auditAction="Bidder has Filled Forms";
                    moduleName=EgpModule.Bid_Submission.getName();
                    remarks="Tenderer(User id): "+session.getAttribute("userId")+" has Filled Form id:"+request.getParameter("hdnFormId");


                    try{
                        if(tenderBidSrBean.insertFormData(tblXML_TenderBidForm,tblXML_TenderBidTable,tblXML_TenderBidDetail,tblXML_TenderBidSign)){

                            if(lotId==0){
                                response.sendRedirect("LotTendPrep.jsp?tab=4&tenderId="+tenderId+"&msg=add");
                            }else{
                                response.sendRedirect("BidPreperation.jsp?tab=4&lotId="+lotId+"&tenderId="+tenderId+"&msg=add");
                            }
                        }else{

                        }
                    }
                    catch(Exception e)
                    {
                        LOGGER.error("Error in Insert form data : "+e.toString());
                        auditAction="Error in Bidder has Filled Forms "+e.toString();
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                }
                else if("Update".equalsIgnoreCase(action))
                {
                    // Filling value for Audit Log
                    idType="tenderId";
                    auditId=tenderId;
                    auditAction="Bidder has Edited Forms";
                    moduleName=EgpModule.Bid_Submission.getName();
                    remarks="Tenderer(User id): "+session.getAttribute("userId")+" has Edited Form id:"+request.getParameter("hdnFormId")+" and Bid id:"+bidId;

                    try{
                        if(tenderBidSrBean.updateFormData(tblXML_TenderBidForm,tblXML_TenderBidTable,tblXML_TenderBidDetail,tblXML_TenderBidSign,bidId)){

                            if(lotId==0){
                                response.sendRedirect("LotTendPrep.jsp?tab=4&tenderId="+tenderId+"&msg=edit");
                            }else{
                                response.sendRedirect("BidPreperation.jsp?tab=4&lotId="+lotId+"&tenderId="+tenderId+"&msg=edit");
                            }
                        }else{
                            LOGGER.debug("Form Data updation unsuccessfully");
                        }
                    }catch(Exception e){
                        LOGGER.error("Error in Insert form data : "+e.toString());
                         auditAction="Error in Bidder has Edited Forms "+e.toString();
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                }
            }// end of (msg == 0)
    }
%>
