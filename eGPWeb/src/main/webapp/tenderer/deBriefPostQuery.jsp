<%-- 
    Document   : deBriefPostQuery
    Created on : Jun 15, 2011, 6:25:51 PM
    Author     : dixit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Seek Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>        
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>        

        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmPreTendQuery").validate({
                    rules:{
                        txtQuery:{required:true,maxlength:1000},
                        uploadDocFile:{required:function(){var val = $('#documentBrief').val().length;if(val>0){document.getElementById("mentoryup").innerHTML='*';return true;}else{return false;}}},
                        documentBrief:{required:function(){var val = $('#uploadDocFile').val().length;if(val>0){ document.getElementById("mentory").innerHTML='*';  return true;}else{return false;}},maxlength:100}
                    },
                    messages:{
                        txtQuery:{required:"<div class='reqF_1'>Please enter query</div>",maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"},
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Document Description.</div>",maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmPreTendQuery').submit(function() {
                    if($('#frmPreTendQuery').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
            
            function mendatoryhide(){
                document.getElementById("mentory").innerHTML='';
                document.getElementById("mentoryup").innerHTML='';
            }
            
        </script>

    </head>
    <body onload="mendatoryhide();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <%
                    String TenderID = "";
                    if(request.getParameter("tenderId")!=null)
                    {
                        TenderID =request.getParameter("tenderId");
                    }
                %>

                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1 ">Seek Clarification
                            <span class="c-alignment-right">
                                <a class="action-button-goback " href="deBriefQuery.jsp?tenderId=<%=TenderID%>" title="Go Back">Go Back</a>
                            </span>
                        </div>   
                        <%                            
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        %>
                         
                         <%@include file="../resources/common/TenderInfoBar.jsp" %>
                         <%pageContext.setAttribute("tab", "6");%>
                         <%@include file="../tenderer/TendererTabPanel.jsp" %>
                        <div>&nbsp;</div>
                        
                        <%--<%@include file="TendererTabPanel.jsp" %>--%>
                    <div class="tabPanelArea_1">
                        <jsp:include page="EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="5" />
                            <jsp:param name="tenderId" value="<%=TenderID%>" />
                      </jsp:include>
                        <form method="post" enctype="multipart/form-data" id="frmPreTendQuery" action="<%=request.getContextPath()%>/deBriefQueryServlet?tenderId=<%=TenderID%>&action=addQuery">
                            <input type="hidden" name="refNo" value="<%=toextTenderRefNo%>"/>
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <%
                                        if (request.getParameter("fq") != null)
                                        {
                                        %>

                                        <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                                        <%
                                        }
                                        if (request.getParameter("fs") != null)
                                        {
                                        %>

                                        <div class="responseMsg errorMsg"  style="margin-top: 10px;">
                                            Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                                        </div>
                                        <%
                                        }

                                        if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("true"))
                                        {
                                        %>
                                            <div id="succMsg" class="responseMsg successMsg">Topic Posted Successfully.</div>

                                        <%
                                        }
                                        if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("false"))
                                        {
                                        %>
                                                 <div id="errMsg" class="responseMsg errorMsg">Some Problem occur during Post Topic.</div>

                                        <%
                                        }
                                        %>
                                        <div style="font-style: italic" class="formStyle_1 ff t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
<!--                                    <tr>
                                        <td class="t-align-left ff" colspan="2">Post Query </td>
                                    </tr>-->
                                    <tr>
                                        <td width="17%" class="t-align-left ff">Seek Clarification  :<span class="mandatory">*</span></td>
                                        <td width="83%" class="t-align-left">
                                            <label>
                                                <textarea name="txtQuery" rows="3" class="formTxtBox_1" id="txtQuery" style="width:650px;"></textarea>
                                            </label>          </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" nowrap> Upload Document :  <span class="mandatory" id="mentoryup" >*</span> </td>
                                        <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" nowrap>Document Description : <span class="mandatory" id="mentory" >*</span></td>
                                        <td>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                            <div id="dvDescpErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                </table>
                                <div>&nbsp;</div>
                            <div class="t-align-center">
                                <label class="formBtn_1">
                                    <input type="submit" name="button3" id="button3" value="Submit" />
                                </label>
                            </div>

                            </div>
<!--                            <div>&nbsp;</div>
                            <div class="t-align-center">
                                <label class="formBtn_1">
                                    <input type="submit" name="button3" id="button3" value="Submit" />
                                </label>
                            </div>-->
                        </form>
                        <div>&nbsp;</div>
                    </div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    <!--Dashboard Footer End-->
                </div>
            </div>
            </div></div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>