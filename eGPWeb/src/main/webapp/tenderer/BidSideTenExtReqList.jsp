<%-- 
    Document   : BidSideTenExtReqList
    Created on : Dec 2, 2010, 2:07:28 PM
    Author     : Rajesh Singh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Validity Extension Request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        
        <script type="text/javascript">

            $(document).ready(function() {

                //Default Action
                $(".tabPanelArea_3").hide(); //Hide all content
               // $("ul.tabPanel_1 li:first").addClass("sMenu").show(); //Activate first tab
                $(".tabPanelArea_3:first").show(); //Show first tab content

                $('#tab1').show();

                //On Click Event
                $("ul.tabPanel_1 li").click(function() {
                    //alert("1");
                    $("ul.tabPanel_1 li").removeClass("sMenu"); //Remove any "active" class
                    $(this).addClass("sMenu"); //Add "active" class to selected tab
                    $(".tabPanelArea_3").hide(); //Hide all tab content
                    var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
                    if(activeTab=='#tab1')
                    {
                        //alert("2");
                        document.getElementById("tab2").style.display="none";
                        document.getElementById("tab1").style.display="";
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is first Tab';
                    }
                    else if (activeTab=='#tab2')
                    {
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is second Tab';
                       document.getElementById("tab1").style.display="none";
                        document.getElementById("tab2").style.display="";
                   }

                    //$(activeTab).fadeIn(); //Fade in the active content
                    
                });

            });
        </script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="contentArea_1">
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">Validity Extension Request</div>
                <%
                            String msg = "";
                            msg = request.getParameter("msg");
                %>
                <div>&nbsp;</div>
                <% if (msg != null && msg.contains("true")) {%>
                <div class="responseMsg successMsg t_space">Validity Extension Request Done Succussfully.</div>
                <%  }%>
                <div class="mainDiv">
                    <%
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                </div>
                <%
                            int tenderId = 0;

                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                %>
                <% pageContext.setAttribute("tab", "6"); %>
                <%@include file="../tenderer/TendererTabPanel.jsp" %>
                <%if(!is_debared){%>
                <div class="tabPanelArea_1">
                    <jsp:include page="../tenderer/EvalInnerTendererTab.jsp" >
                        <jsp:param name="tenderId" value="<%=tenderId%>" />
                        <jsp:param name="EvalInnerTab" value="2"/>
                    </jsp:include>
                    <div class="tabPanelArea_1">
                        <div style="width: 100%; float: left;">
                            <ul class="tabPanel_1">
                                <li class="sMenu"><a href="#tab1">Pending</a></li>
                                <li class=""><a href="#tab2">Accepted / Rejected</a></li>
                            </ul>
                        </div>
                        <div class="tabPanelArea_1" id="tab1" >
                            <table width="100%" cellspacing="0" class="tableList_1" style="margin-top: 0px;">
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No. </th>
                                    <th width="17%" class="t-align-left">Current Tender Validity date</th>
                                    <th width="17%" class="t-align-left">New Tender Validity date</th>
                                    <th width="20%" class="t-align-left">Last date for request acceptance</th>
                                    <th width="10%" class="t-align-left">Acceptance Status</th>
                                    <th width="11%" class="t-align-left">Action</th>
                                </tr>
                                <%
                                            String userId = "";
                                            HttpSession hs = request.getSession();
                                            if (hs.getAttribute("userId") != null) {
                                                userId = hs.getAttribute("userId").toString();
                                            }

                                            int i = 1;

                                            List<SPTenderCommonData> listas = tenderCommonService.returndata("BidderSideTendervalidity", request.getParameter("tenderId"), userId);
                                            if (!listas.isEmpty()) {
                                                for (SPTenderCommonData sptcd : listas) {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=i%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName2()%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                                    <td class="t-align-center">
                                        <%if (sptcd.getFieldName6() != null) {%>
                                <% if(sptcd.getFieldName6().contains("Approv")){ %>
                                Accepted
                                <% } else if(sptcd.getFieldName6().contains("Reject")) {%>
                                Rejected
                                <%}else{ %>
                                <%=sptcd.getFieldName6() %>
                                <%} %>
                            <%}%>

                                        </td>
                                    <td class="t-align-center"><a href="BidSideTenExtReqProcessed.jsp?tenderId=<%=sptcd.getFieldName5()%>&ExtId=<%=sptcd.getFieldName1()%>">Process</a>
                                        | <a href="BidSideTenExtReqView.jsp?tenderId=<%=sptcd.getFieldName5()%>&ExtId=<%=sptcd.getFieldName1()%>">View</a></td>
                                </tr>
                                <%

                                                                                    i++;
                                                                                }
                                                                            } else {%>
                                <tr>
                                    <td class="t-align-left" colspan="6">No Record Found</td>
                                </tr>
                                <%}
                                %>
                            </table>
                        </div>
                        <div class="tabPanelArea_1" id="tab2" style="display: none;">
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No. </th>
                                    <th width="21%" class="t-align-left">Current Tender Validity date</th>
                                    <th width="13%" class="t-align-left">New Tender Validity date</th>
                                    <th width="20%" class="t-align-left">Last date for request acceptance</th>
                                    <th width="10%" class="t-align-left">Acceptance Status</th>
                                    <th width="11%" class="t-align-left">Action</th>
                                </tr>
                                <%
                                            int j = 1;
                                            List<SPTenderCommonData> list = tenderCommonService.returndata("BidderSideTendervalidityProcessed", request.getParameter("tenderId"), userId);
                                            if (!list.isEmpty()) {
                                                for (SPTenderCommonData sptcd : list) {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=j%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName2()%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                                    <td class="t-align-center">
                                       <%if (sptcd.getFieldName6() != null) {%>
                                <% if(sptcd.getFieldName6().contains("Approv")){ %>
                                Accepted
                                <% } else if(sptcd.getFieldName6().contains("Reject")) {%>
                                Rejected
                                <%}else{ %>
                                <%=sptcd.getFieldName6() %>
                                <%} %>
                            <%}%>

                                    </td>
                                    <td class="t-align-center"><a href="BidSideTenExtReqView.jsp?tenderId=<%=sptcd.getFieldName5()%>&ExtId=<%=sptcd.getFieldName1()%>">View</a>
                                    </td>
                                </tr>
                                <%

                                                                                    j++;
                                                                                }
                                                                            } else {%>
                                <tr>
                                    <td class="t-align-left" colspan="6">No Record Found</td>
                                </tr>
                                <%}
                                %>
                            </table>
                        </div>
                        <%-- </div>--%>
                    </div>

                </div>
                    
                <%}%>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

