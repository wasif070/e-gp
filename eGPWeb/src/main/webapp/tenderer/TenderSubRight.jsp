<%-- 
    Document   : TenderSubRight
    Created on : Apr 28, 2011, 12:06:40 PM
    Author     : KETAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Submission Right to the User</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">

     /*       function validateSearch(){
                document.getElementById("msgTenderId").innerHTML = '';
                var vTenderId = document.getElementById('tenderId').value;
                
                // Create regular expression for comma separated integers.
                var regExPr = /^[\d,]*$/;

                if(regExPr.test(vTenderId))
                {
                    return true;
                }
                else
                {
                    document.getElementById("msgTenderId").innerHTML = "Tender ID should be numeric";
                    return false;
                }
            }
       */

            function validateSearch(){
                document.getElementById("msgTenderId").innerHTML = '';
                if(document.getElementById('tenderId').value!=""){
                    if(isNaN(document.getElementById('tenderId').value)){
                        document.getElementById("msgTenderId").innerHTML = "Tender ID should be numeric";
                        return false;
                    }
                }
                return true;
            }

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    if(!validateSearch()){
                        return false;
                    }
                    $("#mode").val("Search");
                    $("#pageNo").val("1");
                    var deptId=$('#txtdepartmentid').val();
                    //alert($("#refNo").val());
                    $.post("<%=request.getContextPath()%>/TenderSubRightServlet", {funName: "AllTenders",viewType:$("#viewType").val(),TenderType:$("#TenderType").val(),departmentId: deptId,office: $("#cmbOffice").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),closeDtFrm: $("#closeDtFrm").val(),closeDtTo: $("#closeDtTo").val(),cpvCategory: $("#txtaCPV").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        var noRecordFound;
                        $('#resultTable').find("tr:gt(0)").remove();

                        $('#resultTable tr:last').after(j);
                        sortTable();
                        $('#resultTable tr').each(function() {
                            noRecordFound = $(this).find('td#noRecordFound');
                        });
                        if(noRecordFound.length == 1){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                    
                });
            });
        </script>
        <!-- reset -->
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    document.getElementById("msgTenderId").innerHTML = '';
                    $("#mode").val("Search");
                    $("#pageNo").val("1");
                    var deptId='0';
                    $.post("<%=request.getContextPath()%>/TenderSubRightServlet", {funName: "AllTenders",departmentId: deptId,viewType:$("#viewType").val(),TenderType:$("#TenderType").val(),office: '',procNature: '',procType:'',procMethod: '0',tenderId: '0',refNo: '',pubDtFrm: '',pubDtTo: '',closeDtFrm: '',closeDtTo: '',cpvCategory: '',pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        var noRecordFound;
                        $('#resultTable').find("tr:gt(0)").remove();

                        $('#resultTable tr:last').after(j);
                        sortTable();
                        $('#resultTable tr').each(function() {
                            noRecordFound = $(this).find('td#noRecordFound');
                        });
                        if(noRecordFound.length == 1){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                });
            });
        </script>


        <script type="text/javascript">
            function loadTable()
            {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/TenderSubRightServlet", {funName: "AllTenders",departmentId: deptId,viewType:$("#viewType").val(),TenderType:$("#TenderType").val(),office: $("#cmbOffice").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),closeDtFrm: $("#closeDtFrm").val(),closeDtTo: $("#closeDtTo").val(),cpvCategory: $("#txtaCPV").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    var noRecordFound;
                    $('#resultTable').find("tr:gt(0)").remove();

                    $('#resultTable tr:last').after(j);
                    sortTable();
                    $('#resultTable tr').each(function() {
                        noRecordFound = $(this).find('td#noRecordFound');
                    });
                    if(noRecordFound.length == 1){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                        //chkdisble($('#pageNo').val());

                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                            //chkdisble($('#pageNo').val());
                            /*if(parseInt($('#pageNo').val(), 10) != 1)
                                $('#btnFirst').removeAttr("disabled");
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnFirst').attr("disabled", "true")

                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnLast').attr("disabled", "true");
                            else
                                $('#btnLast').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnNext').attr("disabled", "true")
                            else
                                $('#btnNext').removeAttr("disabled");*/
                        }
                    }
                });
            });
        </script>

    </head>
    <body  <%if (session.getAttribute("userId") != null) {%>onload="loadTable();hide();" <%}%> >
        <div class="mainDiv">

            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea">
                            <!--Page Content Start-->

                            <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                            <div class="t_space">
                                <%
                                            Object objUsrTypeId = session.getAttribute("userTypeId");
                                            StringBuilder title = new StringBuilder("Advanced Search for Tenders");
                                            if (objUsrTypeId != null) {
                                                title.delete(0, title.length());
                                                title.append("All Tenders/Proposals");
                                            }
                                %>
                                <div class="pageHead_1">Tender Submission Right to the User
                                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('7');">Save as PDF</a></span>
                                </div>
                                <%if ("y".equals(request.getParameter("succFlag"))) {%>
                                <div id="succMsg" class="responseMsg successMsg">Tender Submission Right assigned successfully.</div>
                                <%}%>
                                <div class="formBg_1 t_space">
                                    <form id="alltenderFrm" action=""  method="post">
                                        <input type="hidden" name="viewType" id="viewType" value="Live"/>
                                        <input type="hidden" name="TenderType" id="TenderType" value="All"/>
                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                                        <table id="tblSearchBox" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                            <!--    <tr>
                                                    <td class="ff">Select Hierarchy Node :</td>
                                                    <td colspan="3">
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="ff">Procuring Entity :</td>
                                                    <td colspan="3">
                                                        <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:305px;">
                                                            <option value="">-- Select Office --</option>

                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="22%" class="ff">Procurement Category : </td>
                                                    <td width="38%">
                                                        <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                                            <option value="" selected="selected">-- Select Category --</option>
                                                            <option value="1">Goods</option>
                                                            <option value="2">Works</option>
                                                            <option value="3">Service</option>
                                                        </select>
                                                    </td>
                                                    <td width="15%" class="ff"></td>
                                                    <td width="25%">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procurement Type :</td>
                                                    <td>
                                                        <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                                            <option value="">-- Select Type --</option>
                                                            <option value="NCT">NCT</option>
                                                            <option value="ICT">ICT</option>
                                                        </select>
                                                    </td>
                                                    <td class="ff">Procurement Method :</td>
                                                    <td><select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                                            <option value="0" selected="selected">- Select Procurement Method -</option>
                                            <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                                <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr> -->
                                            <tr>
                                                <td class="ff">Tender ID :</td>
                                                <td>
                                                    <input name="tenderId" id="tenderId" type="tenderId" class="formTxtBox_1" id="textfield5" style="width:202px;" />
                                                    <span class="reqF_1" id="msgTenderId"></span>
                                                </td>
                                                <td class="ff">Reference No :</td>
                                                <td><input name="refNo" id="refNo" type="text" class="formTxtBox_1" style="width:202px;" /></td>
                                            </tr>
                                            <!--        <tr>
                                                        <td class="ff">From Publishing Date :</td>
                                                        <td><input name="pubDtFrm" id="pubDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('pubDtFrm','pubDtFrmImg');"/>&nbsp;
                                                            <a  href="javascript:void(0);" title="Calender"><img id="pubDtFrmImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtFrm','pubDtFrmImg');"/></a>
                                                            <span id="dtErrPub" class="reqF_1"><br /></span>
                                                        </td>
                                                        <td class="ff">To Publishing Date :</td>
                                                        <td><input name="pubDtTo" id="pubDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                            <a  href="javascript:void(0);" title="Calender"><img id="pubDtToImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtTo','pubDtToImg');"/></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">From Closing Date :</td>
                                                        <td><input name="closeDtFrm" id="closeDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                            <a  href="javascript:void(0);" title="Calender"><img id="closeDtFrmImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtFrm','closeDtFrmImg');"/></a>
                                                            <span id="dtErrClose" class="reqF_1"><br /></span>
                                                        </td>
                                                        <td class="ff">To Closing Date :</td>
                                                        <td><input name="closeDtTo" id="closeDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                                            <a  href="javascript:void(0);" title="Calender"><img id="closeDtToImg" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('closeDtTo','closeDtToImg');"/></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Category :</td>
                                                        <td colspan="3">
                                                            <input name="cpvCat" id="txtaCPV" type="text" class="formTxtBox_1" style="width:202px;" />
                                                            <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()">Select Category</a>
                                                        </td>
                                                    </tr> -->
                                            <tr>

                                                <td colspan="4" class="t-align-center">

                                                    <label class="formBtn_1 t_space"><input type="button" name="search" id="btnSearch" value="Search" /></label>&nbsp;&nbsp;
                                                    <label class="formBtn_1 t_space"><input type="reset" name="btnReset" id="btnReset" value="Clear" /></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>

                                <div id="resultDiv" style="display: none">
                                    <div class="tableHead_1 t_space">Tender Search Results</div>
                                    <ul class="tabPanel_1 t_space">
                                        <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Live Tenders</a></li>
                                        <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);">Limited Tenders</a></li>
                                        <!--<li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">All</a></li>-->
                                    </ul>
                                    <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space" cols="@0,7">
                                        <tr>
                                            <th width="4%" class="t-align-center">Sl.<br/>No.</th>
                                            <th width="15%" class="t-align-center">Tender ID,<br />
                                                Reference No</th>
                                            <th width="22%" class="t-align-center">Procurement Category,
                                                <br/>Title</th>
                                            <th width="30%" class="t-align-center">Hierarchy Node</th>
                                            <th width="10%" class="t-align-center">Type, <br />
                                                Method</th>
                                            <th width="19%" class="t-align-center">
                                                Closing <br />Date and<br /> Time</th>
                                            <th width="10%" class="t-align-center">Right <br />
                                                Assigned To</th>
                                            <th width="10%" class="t-align-center">Action</th>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td class="prevNext-container"><ul>
                                                    <li>&laquo; <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                    <li>&#8249; <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <input type="hidden" name="size" id="size" value="10"/>
                                    </div>
                                    <form id="formstyle" action="" method="post" name="formstyle">

                                       <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                                       <%
                                         SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                         String appenddate = dateFormat1.format(new Date());
                                       %>
                                       <input type="hidden" name="fileName" id="fileName" value="TenderSubmissionRight_<%=appenddate%>" />
                                        <input type="hidden" name="id" id="id" value="TenderSubmissionRight" />
                                    </form>
                                    <div>&nbsp;</div>
                                </div>
                            </div>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>

                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" language="Javascript">
    var headSel_Obj = document.getElementById("headTabCompVerify");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
    	function showHide()
	{
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                    document.getElementById('tblSearchBox').style.display = 'table';
                    document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                    document.getElementById('tblSearchBox').style.display = 'none';
                    document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
	}
	function hide()
	{
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
</script>
<script>
    function test(type){
        if(type==1){
            document.getElementById("linkPending").className = "sMenu";
            document.getElementById("linkApproved").className = "";
            //document.getElementById("linkRejected").className = "";
            //document.getElementById("viewType").value = "Live";
            document.getElementById("TenderType").value = "All";
            //fillGridOnEvent('pending');
            $("#pageNo").val("1");
            loadTable();
        }else if(type==2){
            document.getElementById("linkPending").className = "";
            document.getElementById("linkApproved").className = "sMenu";
            //document.getElementById("linkRejected").className = "";
            //document.getElementById("viewType").value = "Live";
            document.getElementById("TenderType").value = "Limited";
            $("#pageNo").val("1");
            loadTable();
        }
//        else{
//            document.getElementById("linkPending").className = "";
//            document.getElementById("linkApproved").className = "";
//            document.getElementById("linkRejected").className = "sMenu";
//            document.getElementById("viewType").value = "AllTenders";
//            //fillGridOnEvent('rejected');
//            $("#pageNo").val("1");
//            loadTable();
//        }
    }
    
    var Interval;
    clearInterval(Interval);
    Interval = setInterval(function(){
        var PageNo = document.getElementById('pageNo').value;
        var Size = document.getElementById('size').value;
        CorrectSerialNumber(PageNo-1,Size); 
    }, 100);
    
</script>
</html>

