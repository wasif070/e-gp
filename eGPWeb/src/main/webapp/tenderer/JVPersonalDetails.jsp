<%-- 
    Document   : JVPersonalDetails
    Created on : Mar 3, 2011, 12:35:23 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page buffer="15kb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tendererMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>JVCA : Personal Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
       
        <script type="text/javascript">
            $(document).ready(function() {
                $("#signupForm").validate({
                    rules: {
                        title: { required: true },
                        country:{required:true},
                        state:{required:true},
                        tinDocName:{required:true},
                        tinNo: { maxlength: 100 },
                        firstName: { required: true,FirstLastName:true,maxlength: 50 },
                        middleName: {maxlength: 50,FirstLastName:true},
                        lasatName: { /*required: true,*/ FirstLastName:true, maxlength: 50},
                        nationalIdNo: { /*required: true,*/ maxlength: 25, alphaNationalId:true},
                        address1: { required: true,maxlength: 250 },
                        address2: {/*required: true,*/ maxlength: 250 },
                        city: { /*required: true,*/ maxlength: 50,DigAndNum:true },
//                        upJilla: { maxlength: 50,alphaNationalId:true},
                        postcode: { /*required: true,*/minlength: 4,number:true},
                        phoneNo: { number: true,maxlength: 20 },
                        phoneSTD: { number: true,maxlength: 10 },
                        faxNo: { number: true,maxlength: 20 },
                        faxSTD: { number: true,maxlength: 10 },
                        mobileNo: {required: true,number: true },
                        emailAddress: {required: true, email: true},
                        designation: { required: true,alphaNationalId:true,maxlength: 30 },
                        department: { /*required: true,*/ maxlength: 30 },
//                        specialization: { required: true},
                        comments: { required: true, maxlength: 100 }
                    },
                    messages: {
                        title: { required: "<div class='reqF_1'>Please select Title</div>" },
                        country: { required: "<div class='reqF_1'>Please select Country</div>" },
                        state: { required: "<div class='reqF_1'>Please enter Dzongkhag / District Name</div>" },
                        tinDocName: { required: "<div class='reqF_1'>Please select Document type</div>" },
                        tinNo: {
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        },
                        firstName: {
                            required: "<div class='reqF_1'>Please enter  First Name</div>",
                            FirstLastName:"<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        middleName: {
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            FirstLastName:"<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>"
                        },
                        lasatName: {
                            required: "<div class='reqF_1'>Please enter Last Name</div>",
                            FirstLastName:"<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        nationalIdNo: {
                            required: "<div class='reqF_1'>Please enter National ID / Passport No. / Driving License No</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        address1: {
                            required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        address2: {
                            //required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        city: {
                            required: "<div class='reqF_1'>Please enter City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only</div>"
                        },
//                        upJilla: {
//                            maxlength: "<br/>Maximum 50 characters are allowed",
//                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
//                        },
                        postcode: {
                            /*required: "<div class='reqF_1'>Please enter Postcode / Zip Code</div>",*/
                            number:"<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 characters are required.</div>"
                        },
                        phoneNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed</div>"
                        },
                        phoneSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed</div>"
                        },
                        faxNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed"
                        },
                        faxSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed"
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>"
                        },
                        emailAddress: {
                            required: "<div class='reqF_1'>Please enter  Email Address</div>",
                            email: "<div class='reqF_1'>Please enter valid e-mail ID</div>"
                        },
                        designation: {
                            required: "<div class='reqF_1'>Please enter Designation</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Max 30 characters are allowed</div>"
                        },
                        department: {
                            required: "<div class='reqF_1'>Please enter Department</div>",
                            maxlength: "<div class='reqF_1'>Max 30 Characters are allowed</div>"
                        },
//                        specialization: {
//                            required: "<div class='reqF_1'>Please select Specialization </div>"
//                        },
                        comments: {
                            required: "<div class='reqF_1'>Please enter  Comment</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "faxCode2"){
                            error.insertAfter("#txtFax");
                        }
                        else if (element.attr("name") == "phoneCode2"){
                            error.insertAfter("#txtPhone");
                        }
                        else{
                            error.insertAfter(element);
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            
            
            
            function numbersOnly(value) {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            }
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCountry').val(),funName:'countryCode'},  function(j){
                        $('#cntCode').val(j.toString());
                    });
                });
            });
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateComboValue'},  function(j){
                        $("select#cmbState").html(j);
                        //Change 136 to 150,Proshanto
                        if($('#cmbCountry').val()=="150"){

                            $('#trBangla').css("display","table-row")
                            $('#trRthana').css("display","table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#phoneCode2').val(j);
                                $('#faxCode2').val(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbSubdistrict').html(j);
                            });
                        }else{
                            $('#trBangla').css("display","none")
                            $('#trRthana').css("display","none")
                            $('#trRSubdistrict').css("display", "none")
                        }
                        $('#txtCity').val(null);
                        $('#txtPost').val(null);
                        $('#txtMob').val(null);
                        $('#txtPhone').val(null);
                        $('#txtFax').val(null);
                        $('#phoneCode2').val(null);
                        $('#faxCode2').val(null);
                    });
                });
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbSubdistrict').html(j);
                    });
                    
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#phoneCode2').val(j);
                        $('#faxCode2').val(j);
                    });
                });
                
                $('#cmbSubdistrict').change(function () {
                    //alert($('#cmbCorpSubdistrict').val());
                    if($('#cmbSubdistrict').val()=='Phuentsholing')
                        {$('#phoneCode2').val('05');
                        $('#faxCode2').val('05');}
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                            $('#phoneCode2').val(j);
                            $('#faxCode2').val(j);
                        });
                    }
                    if ($('#cmbSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    } else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    }
                });
            });

            /*$(function() {
                $('#txtNationalId').blur(function() {
                    if($.trim(('#txtNationalId').val()!="")){
                        if(alphaForPassword($('#txtNationalId').val())){
                        $('span.#natMsg').css("color","red");
                        $('span.#natMsg').html("Checking for Unique National Id...");
                        $.post("/CommonServlet", {nationalId:$('#txtNationalId').val(),funName:'verifyNationId'},
                        function(j){
                            if(j.toString().indexOf("N", 0)!=-1){
                                $('span.#natMsg').css("color","red");
                            }
                            $('span.#natMsg').html(j);
                        });
                        }
                    }
                });
            });*/

            function alphaForPassword(value) {
                return /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value);

            }

            /*$(function() {
                $('#txtMob').blur(function() {
                    if($.trim($('#txtMob').val())!=""){
                        if($.trim($('#txtMob').val()).length>=10 && $.trim($('#txtMob').val()).length<=15){
                        if(numbersOnly($('#txtMob').val())){
                        $('span.#mobMsg').css("color","red");
                        $('span.#mobMsg').html("Checking for Unique Mobile Number...");
                        $.post("<-%//=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobile'},
                        function(j){
                            if(j.toString().indexOf("M", 0)!=-1){
                                $('span.#mobMsg').css("color","red");
                                $('span.#mobMsg').html(j);
                            }else{
                                $('span.#mobMsg').css("color","green");
                                $('span.#mobMsg').html("Ok");
                            }

                        });
                    }
                    }
                    else{
                       $('span.#mobMsg').html(null);
                    }
                   }
                });
            });*/

            $(function() {
                $('#txtThana').blur(function() {
//                    if($.trim($('#txtThana').val())==""){
//                            $('#upErr').html("Please enter Gewog");
//                            return false;
//                        }else{
//                            $('#upErr').html(null);
//                   }
                });
            });

            $(function() {
                $('#signupForm').submit(function() {
                    //Change 136 to 150,Proshanto
                    if($('#cmbCountry').val()=="150"){
//                        if($.trim($('#txtThana').val())==""){
//                            $('#upErr').html("Please enter Gewog");
//                            return false;
//                        }else{
//                            $('#upErr').html(null);
//                        }
                    }
                    //if(($('span.#mobMsg').html()=="Mobile no. already exist")||($('span.#natMsg').html()=="National Id./Passport No./Driving License No. already exist")){
                    if($('span.#mobMsg').html()=="Mobile no. already exist"){
                        return false;
                    }
                    if($('#signupForm').valid()){
                        ChangeTitleValue();
                        if($('#btnSave')!=null){
                            $('#btnSave').attr("disabled","true");
                            $('#hdnbutton').val("Save");
                        }
                        if($('#btnUpdate')!=null){
                            $('#btnUpdate').attr("disabled","true");
                            $('#hdnedit').val("Update");
                        }
                    }
                });
            });
            <%--For emptying thana msg on blur--%>
                $(function() {
                    $('#txtThana').blur(function() {
//                        if($.trim($('#txtThana').val())!=""){
//                            if($('#upErr').html()=="Please enter Gewog"){
//                                $('#upErr').html(null);
//                            }
//                        }
                    });
                });
                $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCountry').val(),funName:'countryCode'},  function(j){
                        $('#phoneCode').val(j.toString());
                        $('#faxCode').val(j.toString());
                    });
                });
            });
            
            function SubDistrictANDAreaCodeLoad()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue'}, function (j) {
                    $('#cmbSubDistrict').html(j);
                });
                if($('#cmbSubDistrict').val()=="Phuentsholing")
                {
                    $('#phoneCode2').val('05');
                    $('#faxCode2').val('05');
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#phoneCode2').val(j);
                        $('#faxCode2').val(j);
                    });
                }
            }
            <%--end here--%>
        </script>
    </head>
    
        <%
                    tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
                    boolean ifEdit = false;
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    List<TblTendererMaster> list = contentAdminService.findTblTendererMasters("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId"))));
                    TblTendererMaster dtBean=null;                    
                    if (!list.isEmpty()) {
                        dtBean = list.get(0);
                        ifEdit = true;
                    }
                    String msg=null;
                    if ("Save".equals(request.getParameter("hdnbutton"))) {
                            if(tendererMasterSrBean.regJVPersonalDetail(tendererMasterDtBean, Integer.parseInt(request.getParameter("uId")))){
                                response.sendRedirect("JVPersonalDetails.jsp?uId="+request.getParameter("uId"));
                            }else{
                                out.print("<h1>Error in Contact Person Details Registration</h1>");
                            }
                    }
                    else if ("Update".equals(request.getParameter("hdnedit"))) {
                            tendererMasterDtBean.setTendererId(dtBean.getTendererId());
                            tendererMasterDtBean.setCmpId(dtBean.getTblCompanyMaster().getCompanyId());
                            tendererMasterDtBean.setUserId(dtBean.getTblLoginMaster().getUserId());
                            tendererMasterDtBean.setIsAdmin(dtBean.getIsAdmin());
                            if(tendererMasterSrBean.updateJVPersonalDetail(tendererMasterDtBean)){
                                response.sendRedirect("JVPersonalDetails.jsp?msg=s&uId="+request.getParameter("uId"));
                            }else{
                                out.print("<h1>Error in Contact Person Details Registration</h1>");
                            }
                    }else if("Complete JVCA Registration Process".equalsIgnoreCase(request.getParameter("submit"))){
                        tendererMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        if(tendererMasterSrBean.finalSubJVCompany(request.getParameter("uId"))){
                            response.sendRedirect("JvcaList.jsp");
                        }else{
                            out.print("<h1>Error in Complete JVCA Registration Process</h1>");
                        }
                    }else{
        %>
        <body onload="SubDistrictANDAreaCodeLoad();<%if (ifEdit) {%>TitleChange('<%=dtBean.getTitle()%>') <% } else { %>TitleChange('') <%}%>">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">                        
                            <td class="contentArea_1">
                                <!--Page Content Start-->
                                <div class="pageHead_1">JVCA - Contact Person Details</div>
                                <!--Navigation Panel starts-->
                                 <%
                                    String pageName=request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().indexOf(".jsp"));                                    
                                 %>

                                <div class="stepWiz_1 t_space" style="width: 100%; height: 18px;">
                                    <div style="width:80%; float: left;">
                                        <ul>
                                            <li <%if(pageName.equals("JVCompanyDetails")){%>class="sMenu"<%}%>><%if(pageName.equals("JVCompanyDetails")){out.print("Company Details");}else{%><a href="JVCompanyDetails.jsp?uId=<%=request.getParameter("uId")%>">Company Details</a><%}%></li>
                                            <li>>></li>
                                            <li <%if(pageName.equals("JVPersonalDetails")){%>class="sMenu"<%}%>><%if(pageName.equals("JVPersonalDetails")){out.print("Company Contact Person Details");}else{%><a href="JVPersonalDetails.jsp?uId=<%=request.getParameter("uId")%>">Company Contact Person Details</a><%}%></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Navigation Panel ends-->
                                <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                                <%if (msg!=null) {%><br/><div class="responseMsg errorMsg"><%=msg%>.</div><%}%>
                                <form id="signupForm" name="signupForm" method="post" action="JVPersonalDetails.jsp?uId=<%=request.getParameter("uId")%>">
                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                     </tr>
                                    <tr>
                                        <td class="ff" width="22%">Title : <span>*</span></td>
                                        <td width="78%"><select name="title" class="formTxtBox_1" id="cmbTitle" style="width: 215px;" <%if (ifEdit) {%>onchange="TitleChange('<%=dtBean.getTitle()%>')" <% } else { %>onchange="TitleChange('')" <%}%> >
                                                <%if (!ifEdit) {%><option value="">Select</option><%}%>
                                                <option value="Mr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                    }%>>Mr.</option>
                                                <option value="Ms." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Ms.")) {%>selected<%}
                                                                    }%>>Ms.</option>
                                                <option value="Mrs." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mrs.")) {%>selected<%}
                                                                    }%>>Mrs.</option>
                                                <option value="Dr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dr.")) {%>selected<%}
                                                                    }%>>Dr.</option>
                                                <option id="OtherTitle" value="Other" <%if (ifEdit) {
                                                                if (!dtBean.getTitle().equals("Dr.")&&!dtBean.getTitle().equals("Mrs.")&&!dtBean.getTitle().equals("Ms.")&&!dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                    }%>>Other</option>
                                            </select></td>
                                    </tr>
                                            
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                            
                                    <tr>
                                        <td class="ff"><label id="OtherTitleName"></label></td>
                                        <td>
                                            <label id="OtherTitleValue"></label>
                                            <span id="OtherTitleMsg" style="color: red;"></span>
                                        </td>
                                        
                                    </tr>
                                        
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                    </tr>
                                        
                                    <tr>
                                        <td class="ff">First Name : <span>*</span></td>
                                        <td><input name="firstName" type="text" class="formTxtBox_1" maxlength="50" id="txtFirstName" style="width:200px;" value="<%if (ifEdit) {
                                                        out.print(dtBean.getFirstName());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Middle Name : <span>&nbsp;</span></td>
                                        <td><input name="middleName" type="text" class="formTxtBox_1" maxlength="50" id="txtMiddleName" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getMiddleName());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Last Name : <span>&nbsp;</span></td>
                                        <td><input name="lasatName" type="text" class="formTxtBox_1" maxlength="50" id="txtLastName" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getLasatName());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
<!--                                    <tr id="trBangla">
                                        <td class="ff">Name in Dzongkha :</td>
                                        <td><input name="banglaName" type="text" class="formTxtBox_1" id="txtNameBangla" maxlength="300" style="width:200px;"  value="<%--<%if (ifEdit) {
                                                        if (dtBean.getFullNameInBangla() != null) {
                                                            out.print(BanglaNameUtils.getUTFString(dtBean.getFullNameInBangla()));
                                                        }
                                                    }%>--%>"/>
                                            <div class="formNoteTxt">(If Bhutanese)</div></td>
                                    </tr>
                                            <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">CID No. / Passport No. / <br />
                                            Driving License No. : <span>*</span></td>
                                        <td><input name="nationalIdNo" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;"  maxlength="25" value="<%--<%if (ifEdit) {
                                                        out.print(dtBean.getNationalIdNo());
                                                    }%>--%>"/>
                                            <span id="natMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                            <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>-->
                                    <tr>
                                        <td class="ff">Designation : <span>*</span></td>
                                        <td><input name="designation" type="text" class="formTxtBox_1" maxlength="30" id="txtDesig" style="width:200px;"   value="<%if (ifEdit) {
                                                        out.print(dtBean.getDesignation());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Department : <span>&nbsp;</span></td>
                                        <td><input name="department" type="text" class="formTxtBox_1" maxlength="30" id="txtDept" style="width:200px;"   value="<%if (ifEdit) {
                                                        out.print(dtBean.getDepartment());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Address : <span>*</span></td>
                                        <td><textarea name="address1" rows="4" class="formTxtBox_1" id="txtaAddr1" maxlength="250" style="width:400px;"><%if (ifEdit) {
                                                        out.print(dtBean.getAddress1());
                                                    }%></textarea></td>
                                    </tr>
<!--                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Address Line 2 : </td>
                                        <td><textarea name="address2" rows="2" class="formTxtBox_1" id="txtaAddr2" maxlength="250" style="width:400px;"><%--<%if (ifEdit) {
                                                        out.print(dtBean.getAddress2());
                                                    }%>--%></textarea></td>
                                    </tr>-->
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="country" class="formTxtBox_1" id="cmbCountry">
                                                <%
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            String countryName = "Bhutan";
                                                            if (ifEdit) {
                                                                countryName = dtBean.getCountry();
                                                            }
                                                            for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryName)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                            <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td><select name="state" class="formTxtBox_1" id="cmbState" style="width:218px;">
                                                <%String cntName = "";
                                                            if (ifEdit) {
                                                                cntName = dtBean.getCountry();
                                                            } else {
                                                                //Change Bangladesh to Bhutan,Proshanto
                                                                cntName = "Bhutan";
                                                            }
                                                            for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                                <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                                if (state.getObjectValue().equals(dtBean.getState())) {%>selected<%}
                                                                } else {
                                                                     //Change Dhaka to Thimphu,Proshanto
                                                                    if (state.getObjectValue().equals("Thimphu")) {%>selected<%}
                                                                                }%>><%=state.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr id="trRSubdistrict">
                                        <td class="ff">Dungkhag / Sub-district : <span>&nbsp;</span></td>
                                        <td><select name="subDistrict" class="formTxtBox_1" id="cmbSubdistrict" style="width:218px;">

                                                <%String regdistrictName = "";
                                                    if (ifEdit) {
                                                        regdistrictName = dtBean.getState();
                                                    } else {
                                                        regdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                         %>
                                                        <%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr id="trRthana">
                                        <td class="ff" height="30">Gewog : </td>
                                        <td><select name="upJilla" class="formTxtBox_1" id="txtThana" style="width:218px;">
                                                <%String state = "Thimphu";
                                                    String subdistrict = "";
                                                    if (ifEdit) {
                                                        state = dtBean.getState();
                                                        subdistrict = dtBean.getSubDistrict();
                                                    }
                                                    if (!state.isEmpty() && !subdistrict.isEmpty()) {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(subdistrict)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                            }
                                                        } else if (gewoglist.getObjectValue().equals("")) {%>
                                                        selected<%
                                                            }%>><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                } else {
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(state)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                                }
                                                            }%>

                                                        ><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                    }
                                                %>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town : <span>&nbsp;</span></td>
                                        <td><input name="city" type="text" class="formTxtBox_1" maxlength="50" id="txtCity" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getCity());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <%--<tr id="trRthana">
                                        <td class="ff">Gewog : <span>&nbsp;</span></td>
                                        <td><input name="upJilla" type="text" class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getUpJilla());
                                                    }%>"/><div id="upErr" style="color: red;"></div></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>--%>
                                    <%if (ifEdit) {
                                                    //Change Bangladesh to Bhutan,Proshanto
                                                    if (!dtBean.getCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trBangla').css("display","none");
                                        $('#trRthana').css("display","none");
                                        $('#trRSubdistrict').css("display", "none");
                                    </script>
                                    <%}
                                                }%>
                                    <tr>
                                        <td class="ff">Post Code : <span>&nbsp;</span></td>
                                        <td><input name="postcode" type="text" class="formTxtBox_1" maxlength="10" id="txtPost" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getPostcode());
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Email Address : <span>*</span></td>
                                        <td><input name="emailAddress" type="text" class="formTxtBox_1" maxlength="30" id="txtEmail" style="width:200px;"   value="<%if (ifEdit) {
                                                out.print(dtBean.getEmailAddress());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                            <%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");%>
                                    <tr>
                                        <td class="ff">Mobile No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" style="width: 30px" id="cntCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                        }%>" readonly/>-<input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMob" style="width:160px;"  value="<%if (ifEdit) {
                                                                        out.print(dtBean.getMobileNo());
                                                                    }%>"/>
                                            <span id="mobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Phone No. : <span>&nbsp;</span></td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="phoneCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input id="phoneCode2" type="text" value="<%if(ifEdit){
if(!"".equals(dtBean.getPhoneNo()))
out.print(dtBean.getPhoneNo().split("-")[0]);

}%>" name="phoneSTD" class="formTxtBox_1" style="width:50px;" />-<input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhone" style="width:100px;"  value="<%if (ifEdit) {
 if(!"".equals(dtBean.getPhoneNo()))
                                      out.print(dtBean.getPhoneNo().split("-")[1]);
                                                    }%>"/></td>
                                    </tr>
                                   <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td class="ff">Fax No. : <span>&nbsp;</span></td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="faxCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                        }%>" readonly/>-<input id="faxCode2" type="text" value="<%if(ifEdit){
if(!"".equals(dtBean.getFaxNo()))
out.print(dtBean.getFaxNo().split("-")[0]);}%>" name="faxSTD" class="formTxtBox_1" style="width:50px;"/>-<input name="faxNo" type="text" class="formTxtBox_1" maxlength="20" id="txtFax" style="width:100px;"  value="<%if (ifEdit) {
 if(!"".equals(dtBean.getFaxNo()))                                                       out.print(dtBean.getFaxNo().split("-")[1]);
                                                    }%>"/></td>
                                    </tr>
                                    
                                    <%--<tr>
                                        <td class="ff">Website : </td>
                                        <td><input name="website" type="text" class="formTxtBox_1" maxlength="100" id="txtWebsite" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getWebsite());
                                                    }%>"/></td>
                                    </tr> --%>
                                    <tr>
                                                <td colspan="2" height="7"></td>
                                            </tr>
                                    <tr>
                                        <td>&nbsp;<input name="comments" type="hidden" class="formTxtBox_1" maxlength="100" id="txtComment" style="width:200px;"  value="user"/></td>
                                        <td>
                                            <%if (ifEdit) {%>
                                            <label class="formBtn_1">
                                                <input type="submit" name="edit" id="btnUpdate" value="Update" />
                                                <input type="hidden" name="hdnedit" id="hdnedit" value=""/>
                                            </label>
                                            <%} else {%>
                                            <label class="formBtn_1">
                                                <input type="submit" name="button" id="btnSave" value="Save"/>
                                                <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                            </label>
                                            <%}%>
                                        </td>
                                    </tr>
                                        <input type="hidden" id="ValidityCheck" value="1" />
                                </table>
                          </form>
                          <%if(ifEdit){%>
                          <form method="post" action="JVPersonalDetails.jsp?uId=<%=request.getParameter("uId")%>">
                              <div align="center">
                                  <br /><br />
                            <label class="formBtn_1">
                                    <input type="submit" name="submit" value="Complete JVCA Registration Process" />
                            </label>
                              </div>
                          </form>
                          <%}%>
                                <!--Page Content End-->
                            </td>
                        </tr>
                    </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%
                tendererMasterDtBean=null;
                tendererMasterSrBean=null;
                dtBean=null;

          }
        %>
    </body>
    
    <script>
        function TitleChange(TitleValue)
        {
            if(document.getElementById("cmbTitle").value == 'Other')
            {
                document.getElementById('OtherTitleName').innerHTML = 'Other Title :<span>*</span>';
                document.getElementById('OtherTitleValue').innerHTML = '<input class="formTxtBox_1" maxlength="50" type="text" id="OtherTitleValueInput" name="OtherTitleValueInput" onblur="TitleValidity()" value="'+TitleValue+'" />';
            }
            else
            {
                document.getElementById('OtherTitleName').innerHTML = '';
                document.getElementById('OtherTitleValue').innerHTML = '';
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
        function ChangeTitleValue()
        {
            if(document.getElementById("cmbTitle").value == 'Other')
            {
                var OtherTitleValue = document.getElementById("OtherTitleValueInput").value;
                document.getElementById("OtherTitle").value = OtherTitleValue;
            }
        }
        function TitleValidity()
        {
            if($("#OtherTitleValueInput").val().length>5)
            {
                $('#OtherTitleMsg').html("Maximum 5 characters are allowed.");
                document.getElementById("ValidityCheck").value = "0";
            }
            else if($("#OtherTitleValueInput").val()=="")
            {
                $('#OtherTitleMsg').html("Enter Title.");
                document.getElementById("ValidityCheck").value = "0";
            }
            else
            {
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
    </script>
</html>