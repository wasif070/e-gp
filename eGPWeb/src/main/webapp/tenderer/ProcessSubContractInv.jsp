<%--
    Document   : ProcessSubContractInv
    Created on : Dec 27, 2010, 2:14:51 PM
    Author     : Administrator

--%>


<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<jsp:useBean id="subContractingSrBean" class="com.cptu.egp.eps.web.servicebean.SubContractingSrbean" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sub Contracting / Consultancy</title>
        
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
        <%
                    String tenderId = "";
                    String s_lorpkgId = "0";
                    String s_package = "package";
                    List<Object[]> list_lorPkg = new ArrayList<Object[]>();
                    if (request.getParameter("tenderid") != null) {
                        tenderId = request.getParameter("tenderid");
                    }
                    if (request.getParameter("lorpkgId") != null) {
                        s_lorpkgId = request.getParameter("lorpkgId");
                        
                        if(!"0".equalsIgnoreCase(s_lorpkgId)){
                            s_package = "lot";
                            CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                            list_lorPkg = commonService.getLotDetailsByPkgLotId(s_lorpkgId,tenderId);
                       }
                    }
                    TenderSrBean tenderSrBean  = new TenderSrBean();
                    boolean b_bidWithDraw = true;
                             String s_bidStatus = subContractingSrBean.getBidWidrawStatus(Integer.parseInt(tenderId), Integer.parseInt(session.getAttribute("userId").toString()));
                             if("withdrawal".equalsIgnoreCase(s_bidStatus) || "finalsubmission".equalsIgnoreCase(s_bidStatus)){
                                 b_bidWithDraw = false;
                             }
                    boolean bol_isLive = tenderSrBean.chkTenderStatus(tenderId) ;
                    String str_comment_lbl = "";
                    pageContext.setAttribute("tenderId", tenderId);
                    String user = session.getAttribute("userId") + "";
                     List<TblTenderDetails> listTender = new ArrayList<TblTenderDetails>();
                                
                                listTender = tenderSrBean.getTenderDetails(Integer.parseInt(tenderId));
                                        if("Works".equalsIgnoreCase(listTender.get(0).getProcurementNature())){
                                            str_comment_lbl = "Elements of Activity and Brief description of Activity";
                                        }else if("goods".equalsIgnoreCase(listTender.get(0).getProcurementNature())){
                                            str_comment_lbl = "Nature of the Supply and related Services";
                                        }else{
                                            str_comment_lbl = "Elements of Activity and Brief description of Activity";
                                        }

        %>

    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="topHeader">
                    <div class="pageHead_1">Accept or Reject Sub Contracting / Consultancy request
                        <span style="float:right;"><a href="SubContracting.jsp?tenderid=<%=request.getParameter("tenderid")%>&lorpkgId=<%=request.getParameter("lorpkgId")%>" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%
                                String errMsg = request.getParameter("errMsg");
                                if (errMsg != null) {
                                    if (errMsg.contains("accepted") || errMsg.contains("rejected")) {
                    %>                <div class="responseMsg successMsg" style="margin-top: 10px;">Invitation <%=errMsg%> Successfully</div>
                    <%} else {%>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=errMsg%></div>
                    <%}
                                }%>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                
                <% pageContext.setAttribute("tab", "3");%>
                <%@include file="TendererTabPanel.jsp"%>
                <%
                            String tenderRefNo = pageContext.getAttribute("tenderRefNo") + "";
                %>
                     <%if(!is_debared){%>
                <div class="tabPanelArea_1 ">
                    <ul class="tabPanel_1">
                        <li><a href="SubContracting.jsp?tenderid=<%=tenderId%>&lorpkgId=<%=s_lorpkgId%>" >Send Invitation</a></li>
                        <li><a href="ProcessSubContractInv.jsp?tenderid=<%=tenderId%>&lorpkgId=<%=s_lorpkgId%>" class="sMenu">Received Invitation.</a></li>
                    </ul>
                    <div class="tabPanelArea_1 ">
                        <%
                                    int cnt = 0;
                                    String status = "";
                                    boolean bole_flag = false;
                                    CommonSearchService tenderCS = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    int int_lpCnt = 0;
                                    for (SPCommonSearchData sptcd : tenderCS.searchData("SubContractApprovedReq", tenderId, user,s_lorpkgId,s_package,null,null,null,null,null)) {
                                            
                                        if (sptcd.getFieldName7().equals("Approved")||sptcd.getFieldName7().equals("Rejected")) {
                                            int_lpCnt =1;
                                            bole_flag = true;
                                            
                                        } else {
                                            int_lpCnt =1;
                                            bole_flag = false;
                                            status = sptcd.getFieldName7();
                                            break;
                                            
                                        }

                                        if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }
                                    if(int_lpCnt<1){
                                            bole_flag = true;
                                        }
                                    if("lot".equalsIgnoreCase(s_package)){
                        %>
                        
                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="tableList_1 t_space">
                      <tr>
                          <td width="15%" class="ff">Lot No.</td>
                          <td><%=list_lorPkg.get(0)[0]%></td>
                      </tr>
                      <tr>
                          <td class="ff">Lot Description</td>
                          <td><%=list_lorPkg.get(0)[1]%></td>
                      </tr>

                    </table>
                        <%}/*Condtion of lot ends here*/if (!bole_flag && bol_isLive && b_bidWithDraw) {%>
                        <div <%if (bole_flag) {%>style="display: none;"<%} else {%>style="display: block;"<%}%>>
                                <div class="formStyle_1 t-align-left ff">Field marked <span class="mandatory">*</span> are mandatory.</div>
                                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="tableList_1 t_space">
                                    <form id="frmprocessSubContracting_<%=cnt%>" name="frmprocessSubContracting_<%=cnt%>" method="POST" action="<%=request.getContextPath()%>/ServletSubContracting">
                                        <tr>
                                            <th width="10%" class="t-align-center">Invitation From</th>
                                            <th width="30%" class="t-align-center"><%=str_comment_lbl%></th>
                                            <th width="5%" class="t-align-center">Last date for accepting</th>
                                            <%--  <%if (status.equals("Reject")) {%>--%>
                                            <%--  <th class="t-align-left">Status</th>--%>
                                            <%--  <%} else {%>--%>
                                            <th width="15%" class="t-align-center">Status</th>
                                            <th width="25%" class="t-align-center">Comments <span class="mandatory">*</span></th>
                                            <th width="10%" class="t-align-center">Action</th>
                                            <%--  <%}%>--%>
                                        </tr>
                                        <%
                                        /*For listing of request*/
                                            cnt = 0;
                                            for (SPCommonSearchData sptcd : tenderCS.searchData("SubContractApprovedReq", tenderId, user,s_lorpkgId,s_package,null,null,null,null,null)) {

                                                if (sptcd.getFieldName7().equals("Pending")) {
                                                    
                                                    cnt++;


                                        %>

                                        <tr>
                                            <td class="t-align-left"><%=sptcd.getFieldName6()%></td>
                                            <td class="t-align-left"><%=sptcd.getFieldName4().replaceAll("quot;", "'")%></td>
                                            <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                                            <%-- <%if(status.equals("Reject")){%>--%>
                                            <%--  <%}else{%>--%>
                                            <td class="t-align-left"><input type="radio" name="status_<%=cnt%>" id="status_<%=cnt%>" value="Approved" checked="checked" />&nbsp;Accept&nbsp;&nbsp;&nbsp;<input type="radio" name="status_<%=cnt%>" id="status_<%=cnt%>" value="Rejected" />&nbsp;Reject</td>
                                            <td class="t-align-left"><textarea name="txtComments_<%=cnt%>" rows="5" class="formTxtBox_1" id="txtComments_<%=cnt%>" style="width:97%;"></textarea>
                                                <span class="reqF_1" id="txtErr_<%=cnt%>"></span>
                                            </td>
                                            <td valign="middle" class="t-align-left" style="vertical-align: middle;"><label class="formBtn_1">
                                                    <input type="submit"  id="add_<%=cnt%>" name="add_<%=cnt%>" value="Submit" onclick="return validate(this);" />
                                                </label>
                                                <input type="hidden" name="counter" value="<%=cnt%>" />
                                                <input type="hidden" name="comName_<%=cnt%>" value="<%=sptcd.getFieldName2()%>" />
                                                <input type="hidden" name="contractId_<%=cnt%>" value="<%=sptcd.getFieldName8()%>" />
                                                <input type="hidden" name="emalVal_<%=cnt%>" value="<%=sptcd.getFieldName10()%>" />
                                                <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                                                <input type="hidden" name="tenderRefNo" value="<%=tenderRefNo%>" />
                                                <input type="hidden" name="lorpkgId" value="<%=s_lorpkgId%>" />
                                            </td>
                                        </tr>
                                    </form>
                                    <%}
                                            if (sptcd != null) {
                                                sptcd = null;
                                            }/*For loop ends here*/
                                        }%>
                                    <% if (cnt == 0) {%>
                                    <tr>
                                        <td colspan="6" class="t-align-center" style="color: red;font-weight: bold">No records found</td>
                                    </tr>
                                    <%}%>

                                </table>
                            <%--</div>--%>
                            <%}%>
                            
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="22%" class="t-align-center">Invitation From</th>
                                    <th width="33%" class="t-align-center"><%=str_comment_lbl%></th>
                                    <th width="12%" class="t-align-center">Last date for accepting invitation</th>
                                    <th width="7%" class="t-align-center">Status</th>
                                    <th width="26%" class="t-align-center">Comments</th>
                                </tr>
                                    
                                <%
                                            cnt = 0;
                                if(bole_flag){
                                            boolean bol_flag = true;
                                            /*for diplaying data*/
                                            String s_status = "";
                                            for (SPCommonSearchData sptcd : tenderCS.searchData("SubContractApprovedReq", tenderId, user,s_lorpkgId,s_package,null,null,null,null,null)) {
                                                if("pending".equalsIgnoreCase(sptcd.getFieldName7())){
                                                        bol_flag = false;
                                                        s_status = "Pending";
                                                    }else{
                                                        if("Approved".equalsIgnoreCase(sptcd.getFieldName7())){
                                                        s_status = "Accepted";
                                                        }else{
                                                            s_status = "Rejected";
                                                        }
                                                    }
                                                if(bol_flag){
                                                cnt++;
                                %>
                                <tr>
                                    <td class="t-align-left"><%=sptcd.getFieldName6()%></td>
                                    <td class="t-align-left"><%=sptcd.getFieldName4().replaceAll("quot;", "'")%></td>
                                    <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                                    <td class="t-align-center"><%=s_status%></td>
                                    <td class="t-align-left"><%=sptcd.getFieldName9().replaceAll("quot;", "'")%></td>
                                </tr>
                                <%
                                                if (sptcd != null) {
                                                    sptcd = null;
                                                }
                                            }/*If Status is pending record not displayed*/}/*Loop ends here*/%>
                                             <%}%>
                                <% if (cnt < 1) {%>
                                <tr>
                                    <td colspan="5" class="t-align-center" style="color: red;font-weight: bold">No Invitations are Accepted or Rejected</td>
                                </tr>
                                <%}%>
                               
                            </table>
                            
                            

                        </div>
                    </div>
                </div><%}%>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

    <script type="text/javascript">
        function validate(obj){
            var btnName = obj.name;
            var count= btnName.substring(btnName.indexOf("_", 0)+1, btnName.length);
            var txtComments = "txtComments_"+count;
            var message = "txtErr_"+count;
            var isError = true;
            if(document.getElementById(txtComments).value!=""){
                if(document.getElementById(txtComments).value.length>1000){
                   
                    document.getElementById(message).innerHTML="<br/>Maximum 1000 charcters are allowed";
                    isError = false;
                }
            }else{
                document.getElementById(message).innerHTML="<br/>Please Enter Comments";
                isError = false;
            }
            if(isError){
                confirm('Are You Sure You Want to Proceed');
                return true;
            }else{
                return false;
            }
        }
    </script>
</html>
