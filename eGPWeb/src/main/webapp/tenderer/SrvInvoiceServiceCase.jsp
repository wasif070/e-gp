<%-- 
    Document   : SrvInvoiceServiceCase
    Created on : Dec 13, 2011, 6:02:55 PM
    Author     : shreyansh Jogi
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Invoice</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");

            %>
            <%@include  file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "3");
                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                                String msg = "";

                                if (request.getParameter("msgg") != null && !"".equalsIgnoreCase(request.getParameter("msgg"))) {
                                    msg = request.getParameter("msgg");
                                }
                                if ("PSRrequest".equalsIgnoreCase(msg)) {
                    %>
                    <div class="responseMsg successMsg t_space"><span>Request for Release Performance Security has Been Sent To PE</span></div>
                    <%}%>
                    <% if (request.getParameter("msg") != null) {
                                    if ("crinv".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.Generated.Msg")%></div>
                    <%}
                                }

                    %>
                    <div align="center">

                        <%
                                    boolean isAdvInvFlag = false;
                                    List<Object[]> invIdData = null;
                                    String tenderId = request.getParameter("tenderId");
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    String userId = "";
                                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                        userId = session.getAttribute("userId").toString();
                                    }
                                    List<Object[]> list = null;

                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                                    int i = 0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {

                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        String wpId = service.getWpIdForService(Integer.parseInt(lotList.getFieldName5()));
                                        boolean isCntTer = service.isContractTerminatedOrNot(lotList.getFieldName5());
                        %>

                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                <tr><td colspan="2">
                                        
                                        <%
                                            NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                            List<Object[]> noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotList.getFieldName5()));
                                            Object[] noaObj  = null;
                                            if(!noalist.isEmpty())
                                            {
                                                noaObj = noalist.get(0);
                                                if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                {
                                        %>

                                        <%}}%>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-center">
                                                    Form Name
                                                </th>
                                                <th width="60%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                            <%
                                            int countt=1;
                                            
                                                List<Object[]> invoicId = service.getInvoiceIdList(wpId);
                                                List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(wpId));

                                            %>
                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td>
                                                  Reimbursable Expenses
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                        <tr>
                                                            <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                            <th width="50%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                            <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                            </th>
                                                        </tr>
                                                        <%
                                                            boolean shoflag = false;
                                                            int j=1;
                                                            int prId = 0;
                                                            if (!Prlist.isEmpty() && Prlist != null) {
                                                                j = 1;
                                                                for (int ii = 0; ii <Prlist.size(); ii++) {
                                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                                    String temp1[] = temp[4].split("-");
                                                                    String finalNo = temp[0] + " " + temp[1] + " " + temp[2] + " " + temp[3] + " " + DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(), "yyyy-MM-dd"));

                                                        %>
                                                        <tr>
                                                            <td class="t-align-center"><%=j + ii%></td>
                                                            <td class="t-align-left"><%=finalNo%></td>
                                                            <td class="t-align-left"><a href="SrvViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=wpId%>&tenderId=<%=tenderId%>&flag=inv&lotId=<%= lotList.getFieldName5()%>">View</a></td>
                                                            <%
                                                                        prId = Prlist.get(ii).getProgressRepId();
                                                                    }
                                                                            list = service.getListForAnyItemAnyPercent(0, 100, Prlist.get(Prlist.size() - 1).getProgressRepId());

                                                                        if(list!=null && !list.isEmpty()){
                                                                        for (Object[] oob : list) {
                                                                            double qty = 0;
                                                                                qty = Double.parseDouble(oob[5].toString()) - Double.parseDouble(oob[6].toString());
                                                                                if (qty > 0) {
                                                                                    shoflag = true;
                                                                                } else {
                                                                                    shoflag = false;
                                                                                }
                                                                            
                                                                        }
                                                                        }

                                                                }%>
                                                    </table>
                                                </td>

                                                <td class="t-align-left">
                                                    <%if (isCntTer) {%>
                                                    <a href="#" onclick="jAlert('You cannot Generate Invoice as Contract has been Terminated by Procuring Entity','Invoice', function(RetVal) {
                                                    });">Generate Invoice</a>
                                                    <%} else {
                                                        if (shoflag) {
                                                            if(noaObj[12]==null || "0.000".equalsIgnoreCase(noaObj[12].toString())){
                                                    %>
                                                    <a href="SrvGenerateInvoice.jsp?prId=<%=prId%>&tenderId=<%=request.getParameter("tenderId")%>&contractId=<%=lotList.getFieldName6()%>&wpId=<%=wpId%>&lotId=<%=lotList.getFieldName5()%>&srvFrm=true">Generate Invoice</a>
                                                    <%}else{
                                                         //boolean bflag = service.getAcceptedFlagforBoq(Integer.parseInt(wpid.get(0).toString()));
                                                         //if(bflag){
                                                    %>
                                                            <a href="SrvGenerateInvoice.jsp?prId=<%=prId%>&tenderId=<%=request.getParameter("tenderId")%>&wpId=<%=wpId%>&lotId=<%=lotList.getFieldName5()%>&srvFrm=true">Generate Invoice</a>
                                                    <%
                                                         //}
                                                    }
                                                    %>
                                                    <%}}%>
                                                    <%if (invoicId != null && !invoicId.isEmpty()) {
                                                    %>
                                                    <div>
                                                        <table class="tableList_1 t_space">
                                                            <tr>
                                                                <th width="3%" class="t-align-center">Invoices</th>
                                                                <th width="3%" class="t-align-center">Status</th>
                                                            </tr>
                                                            <%
                                                                int ii = 1;
                                                                for ( j = 0; j < invoicId.size(); j++) {
                                                                    Object[] invobj = invoicId.get(j);
                                                            %>
                                                            <tr>
                                                                <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&invoiceId=<%=invobj[0]%>&wpId=<%=wpId%>"><%=invobj[2].toString()%></a></td>
                                                                <td class="t-align-left">
        <%
                                                                            String status = "";
                                                                            if (invobj[1] == null) {
                                                                                status = "-";
                                                                            } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                                                if (procnature.equalsIgnoreCase("works")) {
                                                                                    status = "Invoice Generated by Contractor";
                                                                                } else {
                                                                                    status = "Invoice Generated by Supplier";
                                                                                }
                                                                            } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Invoice Accepted by PE";
                                                                            } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "In Process";
                                                                            } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Processed by Accounts Officer";
                                                                            } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Processed by PE";
                                                                            } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Invoice Rejected by PE";
                                                                            }
                                                                            out.print(status);
        %>
                                                                </td>
                                                            </tr>
                                                            <%
                                                                    ii++;
                                                                }%>

                                                        </table>
                                                    </div>
                                                    <%}%></td>
                                            </tr>


                                            <%
                                                                        countt++;
                                            %>
                                        </table>
                                    </td></tr>
                                    <%
                                                                            }
                                    %>
                            </table>
                        </form>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
    function Edit(wpId,tenderId,lotId){


        dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

    }
    function view(wpId,tenderId,lotId){
        dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
    }
    </script>
    <script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
    </script>

</html>

