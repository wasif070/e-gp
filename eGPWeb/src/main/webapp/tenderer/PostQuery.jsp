<%-- 
    Document   : PostQuery
    Created on : Nov 28, 2010, 2:54:13 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post Query</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        

        <script type="text/javascript">
            $(document).ready(function(){

                $("#frmPreTendQuery").validate({
                    rules:{
                        txtQuery:{required:true,maxlength:1000}
                    },
                    messages:
                        {
                        txtQuery:{ required:"<div id='divQtxt' class='reqF_1'>Please enter query.</div>",
                            maxlength:"<div id='divMaxLen' class='reqF_1'>Maximum 1000 characters are allowed.</div>"
                        }
                    }
                });
            });
              
        </script>

    </head>
    <body onload="getDocData();" >
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            
                            preTendDtBean.setUserId(userId);
                            preTendDtBean.setTenderId(tenderId);
                            List<SPTenderCommonData> getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate", tenderId, 0);

                            String preBidStartDate = "";
                            String preBidEndDate = "";

                            if (getPreBidDates.size() > 0) {
                                if (getPreBidDates.get(0).getFieldName1() != null) {
                                    preBidStartDate = getPreBidDates.get(0).getFieldName1();
                                }
                                if (getPreBidDates.get(0).getFieldName2() != null) {
                                    preBidEndDate = getPreBidDates.get(0).getFieldName2();
                                }
                            }
                %>

                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Pre – Tender Meeting Queries<span style="float: right;"><a class="action-button-goback" href="PreTenTenderer.jsp?tenderId=<%= tenderId %>" >Go Back</a></span></div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", 2);%>
                         <%@include file="TendererTabPanel.jsp" %>
                     <%if(!is_debared){%>
                        
                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=postQuery">
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Meeting Start Date &amp; Time :</td>
                                        <td width="23%" class="t-align-left"> <%=preBidStartDate%> </td>
                                        <td width="22%" class="t-align-left ff">Meeting End Date &amp; Time :</td>
                                        <td width="23%" class="t-align-left"> <%=preBidEndDate%> </td>
                                    </tr>
                                </table>

                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="t-align-left ff" colspan="2">Post Query </td>
                                    </tr>
                                    <tr>
                                        <td width="17%" class="t-align-left ff">Query :&nbsp;<span class="mandatory">*</span></td>
                                        <td width="83%" class="t-align-left">
                                            <label>
                                                <textarea name="txtQuery" rows="3" class="formTxtBox_1" id="txtQuery" style="width:650px;"></textarea>
                                            </label>          </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Reference Document :</td>
                                        <td class="t-align-left"><a href="#" onClick="window.open('PreTenderQueryDocUpload.jsp?tenderId=<%=tenderId%>&docType=Prebid','window','width=900,height=500','scrollbars=auto');">Upload</a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label> Please don't upload the document containing query for the tender document</label>
                                        </td>
                                    </tr>
                                    <!--tr>
                                        <td class="t-align-left ff">Comments :</td>
                                        <td class="t-align-left">
                                            <label>
                                                <textarea name="txtComment" rows="3" class="formTxtBox_1" id="txtComment" style="width:650px;"></textarea>
                                            </label>
                                        </td>
                                    </tr-->
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td>
                                            <div id="docData">

                                            </div>
                                        </td></tr>
                                </table>
                            </div>
                            <div>&nbsp;</div>
                            <div class="t-align-center">
                                <label class="formBtn_1" id="lblPostQuery" >
                                    <input type="submit" name="button3" id="button3" value="Post Query" onclick="hideButton();" />
                                 </label>
                                    <input type="hidden" name="hidTenderId" id="hidTenderId"  value="<%=tenderId%>"/>
                                    <input type="hidden" name="tenderId" id="tenderId"  value="<%=tenderId%>"/>
                               
                            </div>
                        </form><%}%>
                        <div>&nbsp;</div>
                    </div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    <!--Dashboard Footer End-->
                </div>
            </div>
        </div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<form id="form2" method="post">
</form>
<script type="text/javascript">
    function hideButton(){
        if($.trim(document.getElementById('txtQuery').value) != ""){
            $('#button3').hide();
            $('#lblPostQuery').hide();
        }
    }
    function getDocData(){
    <%--$.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=docData&docAction=PreTenderQueDocs",
     method: 'POST',
     async: true,
     success: function(j) {
         document.getElementById("docData").innerHTML = j;
     }
 });--%>
         $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'PreTenderQueDocs'}, function(j){
             document.getElementById("docData").innerHTML = j;
         });
     }
    <%-- $(document).ready(function() {
     $('#docData').html(' ');
        $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'PreTenderQueDocs'}, function(j){
                document.getElementById("docData").innerHTML = j;
         });
     });--%>
         getDocData();

         function downloadFile(docName,docSize,tender){
    <%-- $.ajax({
              url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
              method: 'POST',
              async: false,
              success: function(j) {
              }
      });--%>
              document.getElementById("form2").action="<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
              document.getElementById("form2").submit();
          }

          function deleteFile(docName,docId,tender){
    <%-- $.ajax({
            url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docId="+docId+"&tender="+tender+"&funName=remove",
            method: 'POST',
            async: false,
            success: function(j) {
                jAlert("Document Deleted."," Delete Document ", function(RetVal) {
                });
            }
    });--%>
            $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                if(r == true){
                    $.post("<%=request.getContextPath()%>/PreTenderQueryDocServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                        jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                            getDocData();
                        });
                    });
                }else{
                    getDocData();
                }
            });
       

        }

    <%-- $.ajax({
          url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=docData&docAction=PreTenderQueDocs",
          method: 'POST',
          async: false,
          success: function(j) {
              document.getElementById("docData").innerHTML = j;
          }
      });--%>

</script>
