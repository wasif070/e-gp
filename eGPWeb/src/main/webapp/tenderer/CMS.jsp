<%--
    Document   : CMS
    Created on : Jul 25, 2011, 12:46:45 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CMS</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script>
           
            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
            function viewR(wpId,tenderId,lotId,cntId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
            }
            function Viewworks(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDatesForWorks.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
            function vari(wpId,tenderId,lotId){
                dynamicFromSubmit("VariationOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
            function Edit(wpId,tenderId,lotId){
                dynamicFromSubmit("DateEditTenSide.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");
            }
        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">CMS</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
                        String procType = commonServiceee.getProcurementType(request.getParameter("tenderId")).toString();
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
                CommonService commonService1 = (CommonService) AppContext.getSpringBean("CommonService");
                List<Object[]> lotObj = commonService1.getLotDetails(request.getParameter("tenderId"));
                Object[] objj = null;
                if(lotObj!=null && !lotObj.isEmpty())
                {
                    objj = lotObj.get(0);
                    pageContext.setAttribute("lotId", objj[0].toString());
                }
                if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
            %>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <%}%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");


            %>
            <%@include file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                                if (request.getParameter("msg") != null) {
                                    if (request.getParameter("msg").equals("edit")) {
                    %>

                    <div class='responseMsg successMsg '><%=bdl.getString("CMS.dates.edit")%></div>

                    <%}
                                                        if (request.getParameter("msg").equals("accvari")) {
                    %>

                    <div class='responseMsg successMsg '><%=bdl.getString("CMS.var.accVariTenside")%></div>

                    <%}

                                                        if (request.getParameter("msg").equals("sent")) {
                    %>

                    <div class='responseMsg successMsg '><%=bdl.getString("CMS.works.sentToPE")%> </div>

                    <%}
                                }%>
                    <div align="center">
                        <%

                                    String tenderId = request.getParameter("tenderId");
                                    boolean flagg = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    int i = 0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        //List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        /* Dohatec ICT Start*/
                                        List<Object> wpid;
                                        if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                        {
                                            wpid = service.getWpIdForTenderForms(Integer.parseInt(lotList.getFieldName5()),tenderId);
                                        }
                                        else
                                        {
                                            wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        }
                                         /* Dohatec ICT End */

                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        boolean isDateEdited = service.checkForDateEditedOrNot(Integer.parseInt(lotList.getFieldName5()));
                                        // boolean chkContract = service.checkContractSigning(tenderId,Integer.parseInt(lotList.getFieldName5()));
%>
                        <script type="text/javascript">
                        document.getElementById('pckNo').innerHTML = '<%=lotList.getFieldName1()%>';
                        document.getElementById('pckDesc').innerHTML = '<%=lotList.getFieldName2()%>';
                        </script>
                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>

                                <%
                                                                int countt = 1;
                                                                if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="1%" class="t-align-center">S.No</th>

                                                <th width="17%" class="t-align-center">
                                                    <%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <%=bdl.getString("CMS.Goods.Forms")%>
                                                    <%} else {%>
                                                    <%=bdl.getString("CMS.works.Forms")%>
                                                    <%}%>
                                                </th>
                                               <%if ( procnature.equalsIgnoreCase("works")) {%> <th width="12%" class="t-align-center">
                                                    Work Program submission date
                                                   </th> <%}%>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                                <%if (procnature.equalsIgnoreCase("works")) {%>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.works.variation")%></th>
                                                <%}%>
                                            </tr>
                                            <%for (Object obj : wpid) {
                                                                                                                                    String toDisplay = service.getConsolidate(obj.toString());
                                                                        Object date = service.getCreatedDate(obj.toString());                                                             boolean isallowvariation = service.allowVariationOrderOrNotForTenderer(Integer.parseInt(obj.toString()));
                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td><%
                                                    boolean flagfordateedit = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                                    boolean forWorksdateEdit = service.isDateEditedInWorksTenSide(obj.toString(), "sendtope");
                                                    if (procnature.equalsIgnoreCase("goods")) {
                                                        out.print(toDisplay.replace("Consolidate", "Delivery Schedule"));
                                                } else {
                                                    out.print(toDisplay.replace("Consolidate", "BoQ"));
                                                }%></td>
                                                <%if (procnature.equalsIgnoreCase("works")) {%><td>
                                                   <% if(forWorksdateEdit){
                                                        %>
                                                        <%= DateUtils.gridDateToStrWithoutSec((Date) date)%>
                                                    <%} else {%>
                                                     <%=bdl.getString("CMS.works.edidatesByTenderer")%>
                                                    <%}}%>
                                                </td>
                                                <td>
                                                    <%if (procnature.equalsIgnoreCase("works")) {
                                                            if (flagfordateedit) {
                                                                if (forWorksdateEdit) {
                                                    %>
                                                    <a href="#" onclick="Viewworks(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">View</a>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinksTenSide.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="ConfiDatesUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Workprogram&module=wp">Upload / Download Document</a>
                                                    <%} else {%>
                                                    <a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Configure Dates</a>
                                                    <%}
                                                                                                    } else {%>
                                                       <a href="#" onclick="jAlert('<%=bdl.getString("CMS.works.dateconfig.pending")%>','Work Program', function(RetVal) {
                                                });">Configure Dates</a>
                                                    <%}                                                    
                                                    } else {%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinksTenSide.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="ConfiDatesUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Delivery&module=ds">Upload / Download Document</a>
                                                    <%}%>                                                    
                                                </td>
                                                <%if (procnature.equalsIgnoreCase("works")) {%>
                                                <td>
                                                    <%                                                        
                                                        if (!isallowvariation) {
                                                    %>
                                                        <a href="#" onclick="vari(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Variation Order</a>&nbsp;|&nbsp;                                                       
                                                    <%        
                                                        }
                                                    %>   
                                                    <a href="#" onclick="history(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Variation Order history </a>&nbsp;|&nbsp;
                                                    <a href="variOrderUploadDocTenside.jsp?keyId=<%=obj.toString()%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">Upload / Download Document</a>    
                                                    
                                                </td>    
                                                <%}%>
                                                    



                                            </tr>


                                            <%
                                                                                                                                countt++;
                                                                                                                            }%>

                                        </table>
                                    </td></tr>
                                    <%
                                                                        }
                                    %>

                                 <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(lotList.getFieldName5()));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));

                        %>

                              <tr>
                                    <td colspan="3">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                                <%
                                                                int countR = 1;
                                                                if (!wpids.isEmpty() && wpids != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="1%" class="t-align-center">S.No</th>

                                                <th width="17%" class="t-align-center">
                                                   
                                                    <%=bdl.getString("CMS.Goods.Forms")%>
                                                   
                                                </th>
                                              <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                            </tr>
                                            <%
                                            int showR=0;
                                            for (Object obj : wpids) {
                                            List<Object[]> formsR = dDocSrBean.getAllBoQFormsForRO(Integer.parseInt(lotList.getFieldName5()));
                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countR%></td>
                                                <td><%
                                                   out.print("Delivery Schedule of "+formsR.get(showR)[1]);
                                               %></td>

                                                <td>

                                                    <a href="#" onclick="viewR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">View</a>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinksTenSide.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&cntId=<%=objd[11]%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="ConfiDatesUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Delivery&module=ds">Upload / Download Document</a>

                                                </td>



                                            </tr>


                                            <%
                                                                                                                                countR++;
                                                                                                                                showR++;
                                                                                                                            }%>

                                        </table>
                                    </td></tr>
                                    <%
                                                                        }
                                    %>
                                    




<%icount++;}}
                                                }%>


                                <%
            }%>


                            </table>
                            <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                        </form>

                    </div>
                </div></div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include></div>
    </div>
    <script>
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
    function history(wpId,tenderId,lotId){
        dynamicFromSubmit("VariationOrderHistory.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
    }
    </script>
</html>
