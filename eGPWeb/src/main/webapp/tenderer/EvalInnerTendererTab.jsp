<%-- 
    Document   : EvalInnerTendererTab
    Created on : Dec 25, 2010, 3:13:30 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TendererTabServiceImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%       
    String tenderId="";
    String EvalInnerTab="";
    int suserTypeId =0;
     if (session.getAttribute("userTypeId") != null) {
            suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
     }
     if(request.getParameter("EvalInnerTab") != null && !"null".equalsIgnoreCase(request.getParameter("EvalInnerTab")) ){
          EvalInnerTab =   request.getParameter("EvalInnerTab");
     }

     if (pageContext.getAttribute("tab") != null && !"null".equalsIgnoreCase((String) pageContext.getAttribute("tab"))) {
         EvalInnerTab = (String) pageContext.getAttribute("tab");
     }

    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
         tenderId = request.getParameter("tenderid");
    }else  if(request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId")) ){
         tenderId =   request.getParameter("tenderId");
    }
    
    CommonService procNatureService = (CommonService)AppContext.getSpringBean("CommonService");
    Object evalTabprocNature = procNatureService.getProcNature(tenderId);
    Object afterLoginTenderType = procNatureService.getEventType(tenderId);
    Object evalTabprocMethod = procNatureService.getProcMethod(tenderId);
    
    /*TendererTabServiceImpl innerTab_tabService  = (TendererTabServiceImpl)AppContext.getSpringBean("TendererTabServiceImpl");
    boolean b_evalITopenDate = innerTab_tabService.isOpeningDateTime(Integer.parseInt(tenderId));
    if(!b_evalITopenDate){*/
  %>
 
  <ul class="tabPanel_1">
    <li><a href="<%=request.getContextPath()%>/tenderer/EvalBidderClari.jsp?tenderId=<%=tenderId%>" <% if("1".equals(EvalInnerTab)){ %> class="sMenu" <% } %>>Clarification</a></li>
    <li><a href="../tenderer/BidSideTenExtReqList.jsp?tenderId=<%=tenderId%>" <%if ("2".equals(EvalInnerTab)) {%>class="sMenu"<%} %>>Validity / Security Extension</a></li>
    <%
        if (!"Services".equalsIgnoreCase(evalTabprocNature.toString()) && (!(evalTabprocMethod.toString().equals("DPM"))) && (!(afterLoginTenderType.toString().equalsIgnoreCase("PQ") || afterLoginTenderType.toString().equalsIgnoreCase("REOI") || afterLoginTenderType.toString().equalsIgnoreCase("1 stage-TSTM")))){
    %>
    <li><a href="<%=request.getContextPath()%>/tenderer/ViewPostQualiDetail.jsp?tenderId=<%=tenderId%>"<% if("3".equals(EvalInnerTab)){ %> class="sMenu" <% } %> >Post Qualification</a></li>
    <% } 
        TendererTabServiceImpl tendererTabServiceImpl = (TendererTabServiceImpl) AppContext.getSpringBean("TendererTabServiceImpl");
        boolean evalInnerTendererTabflag = true;
                evalInnerTendererTabflag = tendererTabServiceImpl.isOpeningDateTime(Integer.parseInt(tenderId));                
                if(!evalInnerTendererTabflag && suserTypeId==2)
                {
    %>
                <%--  <tr>  <!-- Comment: Jira Issue "EBDP-10" disable debriefing under evaluation tab for now -->
                <li><a href="<%=request.getContextPath()%>/tenderer/deBriefQuery.jsp?tenderId=<%=tenderId%>" <% if("5".equals(EvalInnerTab)){ %> class="sMenu" <% }%>>
                   <%
                    if("Services".equalsIgnoreCase(evalTabprocNature.toString()))
                    {
                   %>
                        Debriefing on Proposal
                   <%
                    }
                    else
                    {
                    %>
                        Debriefing on Tender
                   <%
                    }
                   %>
                    </a></li> --%>
    <%}%>
        <%if(("Services".equalsIgnoreCase(evalTabprocNature.toString()) && (!(afterLoginTenderType.toString().equalsIgnoreCase("PQ") || afterLoginTenderType.toString().equalsIgnoreCase("REOI") || afterLoginTenderType.toString().equalsIgnoreCase("1 stage-TSTM")))) || (!"Services".equalsIgnoreCase(evalTabprocNature.toString()) && (evalTabprocMethod.toString().equalsIgnoreCase("DPM")))){%>
    <li>
        <a href="<%=request.getContextPath()%>/tenderer/NegoTendererProcess.jsp?tenderId=<%=tenderId%>" <% if("4".equals(EvalInnerTab)){ %> class="sMenu" <% } %>>Negotiation</a>
    </li>
    <% } %>
    <%if(evalTabprocMethod.toString().equalsIgnoreCase("LTM")){%>
    <li>
        <a href="<%=request.getContextPath()%>/tenderer/LotteryResult.jsp?tenderId=<%=tenderId%>" <% if("6".equals(EvalInnerTab)){ %> class="sMenu" <% } %>>Lottery Result</a>
    </li>
    <% } %>
  </ul>
  <%--<%}%>--%>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>