<%--
    Document   : JVCompanyDetails
    Created on : Mar 3, 2011, 11:21:57 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="javax.print.DocFlavor.STRING"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>JVCA : Company Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
       <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
             $(document).ready(function() {
                $("#frmComp").validate({
                    rules: {
                        //companyName:{maxlength: 200,alphaCompanyName:true},
//                        companyRegNumber:{maxlength: 30},
//                        specialization: {required: true},
//                        legalStatus: {required: true},
                        regOffAddress: {required: true, maxlength: 250},
                        regOffCity: {/*required: true,*/ maxlength: 50,DigAndNum:true},
//                        regOffUpjilla: { maxlength: 50,alphaNationalId:true},
                        regOffPostcode: {/*required: true,*/number:true,minlength:4},
                        regPhoneSTD: {required: true, maxlength:10 , digits:true},
                        regOffPhoneNo: { maxlength:20 , digits:true},
                        regOffFaxNo: {maxlength:20 , digits:true},
                        regFaxSTD: {maxlength:10 , digits:true},
                        //tinNo: {required: true },
//                        establishmentYear:{digits:true,max4Digitsmust:"#A"},
                        website: {url:true},
                        regOffMobileNo: {required: true, number: true}
                    },
                    messages: {

                        //companyName:{
                           // maxlength:"<div class='reqF_1'>Maximum 200 characters are allowed</div>",
                           // alphaCompanyName:"<div class='reqF_1'>Allows alpha numerics and the following special characters (&/-.)</div>"},
//                        companyRegNumber:{maxlength:"<div class='reqF_1'>Maximum 30 characters are allowed</div>"},
//                        specialization: { required: "<div class='reqF_1'>Please select Nature of Business</div>"},
//                        legalStatus: { required: "<div class='reqF_1'>Please select Company's Legal Status</div>"},
                        regOffAddress: { required: "<div class='reqF_1'>Please enter Registered office Address</div>",
                            maxlength:"<div class='reqF_1'>Maximum 150 characters are allowed</div>"},
                        regOffCity: {
                            maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only</div>"},
/*                        regOffUpjilla: {
                            maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"},*/
                        regOffPostcode: {/* required: "<div class='reqF_1'>Please enter Postcode / Zip Code</div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 Characters are required.</div>"},
                        regPhoneSTD: { required: "<div class='reqF_1'>Please enter Area Code.</div>",
                            digits: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 10 Digits are allowed</div>"},
                        regOffPhoneNo: { 
                            digits: "<div class='reqF_1'>Please enter Numbers Only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 Digits are allowed</div>"},
                        regOffFaxNo: { digits: "<div class='reqF_1'>Please enter Numbers Only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 Digits are allowed</div>"},
                        regFaxSTD: { digits: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 10 Digits are allowed</div>"},
                        //tinNo: { required: "<div class='reqF_1'>Please enter Tax Payment Number/Other Identification Number</div>"},
//                        establishmentYear:{
//                            digits:"<div class='reqF_1'>Please enter numeric values only</div>",
//                            max4Digitsmust:"<div class='reqF_1'>Please enter 4 digits only. Only 0's not allowed</div>"},
                        website: {url: "<div class='reqF_1'>Please enter valid Website Name</div>"},
                        regOffMobileNo: {required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                        },
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "licExpiryDateStr"){
                            error.insertAfter("#txttenderLastSellDateimg1");
                            }
                         /*else
                            error.insertAfter(element);*/


                         else if (element.attr("name") == "licIssueDateStr"){
                            error.insertAfter("#txttenderLastSellDateimg");
                            }
                            /*else
                            error.insertAfter(element);*/

                        else if (element.attr("name") == "specialization"){
                            error.insertAfter("#cpvTip");
                            }
                            /*else
                            error.insertAfter(element);*/

                        else if (element.attr("name") == "establishmentYear"){
                            error.insertAfter("#cey");
                            }
                        else if (element.attr("name") == "regPhoneSTD"){
                            error.insertAfter("#txtRegPhone");
                            }

                        else if (element.attr("name") == "regOffPhoneNo"){
                            error.insertAfter("#hdnvalue");
                            }
                            /*else
                            error.insertAfter(element);*/

                        else if (element.attr("name") == "regFaxSTD"){
                            error.insertAfter("#txtRegFax");
                            }
                        else if (element.attr("name") == "website"){
                            error.insertAfter("#webnote");
                            }
                            else{
                            error.insertAfter(element);
                        }

                    }

                });
        });

        /*function ValidateUpdate(){
                    if($('#cmbTin').val()=="tin"){
                        var check=''
                        if($('#tinNoNo').val()=='')
                        {
                            document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number/Other Identification Number';
                            check='true'
                        }
                        else
                        {
                            if($('#tinNoNo').val().length >=30)
                            {
                                document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number less than 30 characters';
                                check='true'
                            }
                            else
                            {
                                document.getElementById('tinNoNospan').innerHTML='';
                            }
                        }
                    }
                    else
                    {
                        if($('#tinNoNo').val()=='')
                        {
                            document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number/Other Identification Number';
                            check='true'
                        }
                        else
                        {
                            if($('#tinNoNo').val().length >=30)
                            {
                                document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number less than 30 characters';
                                check='true'
                            }
                            else
                            {
                                document.getElementById('tinNoNospan').innerHTML='';
                            }
                        }


                        if($('#txtaODoc').val()=='')
                        {
                            document.getElementById('txtaODocspan').innerHTML='Please enter Description';
                            check='true'
                        }
                        else
                        {
                            if($('#txtaODoc').val().length >=100)
                            {
                                document.getElementById('txtaODocspan').innerHTML='Please enter Description less than 100';
                                check='true'
                            }
                            else
                            {
                                document.getElementById('txtaODocspan').innerHTML='';
                            }
                        }

                    }
                    if(check=="true")
                    {
                        return false;
                    }
                }

         function cleartinDocName(){
             var check1=''
                        if($('#tinNoNo').val()=='')
                        {
                            document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number/Other Identification Number';
                            check1='true'
                        }
                        else
                        {
                            if($('#tinNoNo').val().length >=30)
                            {
                                document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number less than 30 characters';
                                check1='true'
                            }
                            else
                            {
                                document.getElementById('tinNoNospan').innerHTML='';
                            }
                        }

                        if(check1=="true")
                    {
                        return false;
                    }
         }

         function cleartxtaODoc(){
              var check2=''

            if($('#tinNoNo').val()=='')
                        {
                            document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number/Other Identification Number';
                            check2='true'
                        }
                        else
                        {
                            if($('#tinNoNo').val().length >=30)
                            {
                                document.getElementById('tinNoNospan').innerHTML='Please enter Tax Payment Number less than 30 characters';
                                check2='true'
                            }
                            else
                            {
                                document.getElementById('tinNoNospan').innerHTML='';
                            }
                        }


                        if($('#txtaODoc').val()=='')
                        {
                            document.getElementById('txtaODocspan').innerHTML='Please enter Description';
                            check='true'
                        }
                        else
                        {
                            if($('#txtaODoc').val().length >=100)
                            {
                                document.getElementById('txtaODocspan').innerHTML='Please enter Description less than 100';
                                check2='true'
                            }
                            else
                            {
                                document.getElementById('txtaODocspan').innerHTML='';
                            }
                        }
                        if(check2=="true")
                    {
                        return false;
                    }
         }*/

         function ValidateissueDate()
            {    var chck="true";
                if($('#frmComp').validate())
                 {
                     //Change code 136 to 150,Proshanto
                    if($('#cmbRegCountry').val()=="150")
                    {
                        if(document.getElementById("txtIssueDate").value=='')
                        {
                            document.getElementById("issueDate").innerHTML="<br/>Please enter Trade License Issue Date";
//                             chck="false";
                        }
                     }
                  }
                 if(chck=="false"){
                      return false;
                  }
            }
            function ValidateexpiryDate()
            {   var chck="false";
                if($('#frmComp').validate())
                 {
                     //Change code 136 to 150,Proshanto
                    if($('#cmbRegCountry').val()=="150")
                    {
                        if(document.getElementById("txtExpiryDate").value=='')
                        {
                            document.getElementById("expiryDate").innerHTML="<br/>Please enter Trade License Expiry Date";
//                            chck="false";
                        }
                     }
                  }
                  if(chck=='false'){
                      return false;
                  }
            }
            function clearIssueDate(){
                        if($('#txtIssueDate').val()!=""){
                            if(!CompareToForGreater1($('#hdnCurrDate').val(),$('#txtIssueDate').val())){
                                $('#issueDate').html('');
                                $('#issueDatestart').html("Trade License Issue Date has to be prior than the current date.");
                            }else{
                                $('#issueDate').html('');
                                $('#issueDatestart').html("");
                            }
                        }
            }
            function clearExpiryDate(){
                        if($('#txtIssueDate').val()!=""  && $('#txtExpiryDate').val()!=""){
                            if(!CompareToForGreater1($('#txtExpiryDate').val(),$('#txtIssueDate').val())){
                                $('#expiryDate').html('');
                              $('#cmpDate').html("Trade license expiry date must be greater than issue date.");
                            }else{
                                $('#expiryDate').html('');
                              $('#cmpDate').html(null);
                            }
                        }
            }
        </script>
                <script type="text/javascript">
             /*$(function() { //Taher
                $('#cmbTin').change(function() {
                    if($('#cmbTin').val()=="tin"){
                        document.getElementById('divdescription').style.display="None";
                        //$('#divdescription').attr("display", "none");
                        //$('#txtaODoc').css("visibility", "collapse");
                        //$('#2n').css("visibility", "collapse");
                    }else{
                        document.getElementById('divdescription').style.display="inline";
                        //$('#divdescription').attr("display", "inline");
                        //$('#2n').css("visibility", "visible");
                        //$('#txtaODoc').css("visibility", "visible");
                    }
                });
            });*/
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function onTickCountry(data){
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:data,funName:'stateComboValue'},  function(j){
                        $("select#cmbCorpState").html(j);
                        //Change code 136 to 150,Proshanto
                        if($('#cmbCorpCountry').val()=="150"){
                            $('#trCthana').css("display","table-row");
                        }else{
                            $('#trCthana').css("display","none");
                        }
               });
               $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCorpCountry').val(),funName:'countryCode'},  function(j){
                   $('#corpPhoneCode').val(j.toString());
                   $('#corpFaxCode').val(j.toString());
               });
            }
            $(function() { //Taher
                $('#cmbRegCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbRegState").html(j);
                        //Code by Proshanto
                        if ($('#cmbRegCountry').val() == "150") {//136
                            $('#trRthana').css("display", "table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                            $('#trtlid').css("display", "inline")
                            $('#trtled').css("display", "inline")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#regPhoneSTD').val(j);
                                $('#regFaxSTD').val(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtRegThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbRegSubdistrict').html(j);
                            });
                        } else {
                            $('#trRthana').css("display", "none")
                            $('#trRSubdistrict').css("display", "none")
                            //rajesh
                            $('#trtlid').css("display", "none")
                            $('#trtled').css("display", "none")
                            
                            $('#issueDate').html(null);
                            $('#expiryDate').html(null);
                            $('#issueDatestart').html(null);
                            $('#cmpDate').html(null);
                        }
                    });
                    $('#txtRegCity').val(null);
                    $('#txtRegPost').val(null);
                    $('#txtregOffMobileNo').val(null);
                    $('#txtRegPhone').val(null);
                    $('#txtRegFax').val(null);
                    $('#regPhoneSTD').val(null);
                    $('#regFaxSTD').val(null);
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbRegCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#regPhoneCode').val(j.toString());
                        $('#regFaxCode').val(j.toString());
                        $('#regMobCode').val(j.toString());
                        
                    });
                    
                });
                
                $('#cmbRegSubdistrict').change(function () {
                        //alert($('#cmbRegSubdistrict').val());
                        if($('#cmbRegSubdistrict').val()=='Phuentsholing')
                            {$('#regPhoneSTD').val('05');
                            $('#regFaxSTD').val('05');}
                        else
                        {
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#regPhoneSTD').val(j);
                                $('#regFaxSTD').val(j);
                            });
                        }
                });
                    
                $('#cmbRegState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbRegSubdistrict').html(j);
                    });
                    
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#regPhoneSTD').val(j);
                        $('#regFaxSTD').val(j);
                    });
                });
                
                $('#cmbRegSubdistrict').change(function () {
                    if($('#cmbRegSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                });
            });
            $(function() {
                $('#cmbCorpCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCorpCountry').val(),funName:'stateComboValue'},  function(j){
                        $("select#cmbCorpState").html(j);
                        //Change code 136 to 150,Proshanto
                        if($('#cmbCorpCountry').val()=="150"){
                            $('#trCthana').css("display","table-row")
                        }else{
                            $('#trCthana').css("display","none")
                        }
                    });
                });
            });
            $(function() {
                $('#frmComp').submit(function() {
                    //Added

$('#txtaCorpAddr').val($('#txtaRegAddr').val());
                        $('#txtCorpCity').val($('#txtRegCity').val());
                        $('#txtCorpThana').val($('#txtRegThana').val());
                        $('#txtCorpPost').val($('#txtRegPost').val());
                        $('#corpPhoneSTD').val($('#regPhoneSTD').val());
                        $('#txtCorpPhone').val($('#txtRegPhone').val());
                        $('#corpFaxSTD').val($('#regFaxSTD').val());
                        $('#txtCorpFax').val($('#txtRegFax').val());
                        $('#cmbCorpCountry').val($('#cmbRegCountry').val());
                        $('#corpPhoneCode').val($('#regPhoneCode').val());
                        $('#corpFaxCode').val($('#regFaxCode').val());
                        $('#txtaCorpAddr').attr("readonly", "true");
                        $('#corpPhoneSTD').attr("readonly", "true");
                        $('#corpFaxSTD').attr("readonly", "true");
                        $('#txtCorpCity').attr("readonly", "true");
                         $('#txtCorpThana').attr("readonly", "true");
                         $('#txtCorpPost').attr("readonly", "true");
                         $('#corpPhoneCode').attr("readonly", "true");
                        $('#corpFaxCode').attr("readonly", "true");
                         $('#txtCorpPhone').attr("readonly", "true");
                         $('#txtCorpFax').attr("readonly", "true");
                        //Change code 136 to 150,Proshanto
                        if($('#cmbRegCountry').val()!="150"){
                            $('#trCthana').css("display","none")
                        }
                        $('#cmbCorpState').html($('#cmbRegState').html());
                        for(var i=0;i<document.getElementById('cmbRegState').options.length;i++){
                            if(document.getElementById('cmbRegState').options[i].selected){
                                document.getElementById('cmbCorpState').options[i].selected="selected";
                            }else{
                                document.getElementById('cmbCorpState').options[i].disabled="true";
                            }
                        }
                        for(var i=0;i<document.getElementById('cmbRegCountry').options.length;i++){
                            if(document.getElementById('cmbRegCountry').options[i].selected){
                                document.getElementById('cmbCorpCountry').options[i].selected="selected";
                            }else{
                                document.getElementById('cmbCorpCountry').options[i].disabled="true";
                            }
                        }

                        if($.trim($('#txtaRegAddr').val())==""){
                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
                        bol= false;
                    }else
                        {
                            if(document.getElementById("txtaRegAddr").value.length >150){
                                $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                 $('#stxtaCorpAddr').html('');
                            }
                        }


                     if($.trim($('#txtRegCity').val())==""){
//                        $('#stxtCorpCity').html('Please enter City / Town');
//                        bol= false;
                     }else
                        {
                            if($.trim(document.getElementById("txtRegCity").value.length) >50){
                                $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                if(!DigAndNum(document.getElementById("txtRegCity").value)){
                                    $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                                }else{
                                 $('#stxtCorpCity').html('');
                                 }
                            }
                        }

                        if($.trim($('#regPhoneSTD').val())==""){
                        $('#scorpPhoneSTD').html('Please enter Area Code.');
                        bol= false;
                         }else
                            {
                                if($.trim(document.getElementById("regPhoneSTD").value.length) >10){
                                    $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                                    bol= false;
                                }
                                else
                                {

                                    if(!digits(document.getElementById("regPhoneSTD").value)){
                                        $('#scorpPhoneSTD').html("Please enter Numbers only");
                                    }else{
                                     $('#scorpPhoneSTD').html('');
                                     }
                                }
                            }

                            if($.trim($('#txtRegPhone').val())==""){
                                $('#stxtCorpPhone').html('Please enter Phone No.');
//                                bol= false;
                             }else
                                {
                                    if($.trim(document.getElementById("txtRegPhone").value.length) >20){
                                        $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                                        bol= false;
                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("txtRegPhone").value)){
                                            $('#stxtCorpPhone').html("Please enter Numbers Only");
                                        }else{
                                         $('#stxtCorpPhone').html('');
                                         }
                                    }
                                }

                                if($.trim($('#txtRegThana').val())==""){
//                                        $('#spCthana').html("Please enter Dungkhag / Sub-district");
                                    }
                                    else
                                        {
                                            $('#spCthana').html(null);
                                        }


                    //Ended
                    var bol=true;
                    //Change code 136 to 150,Proshanto
                    if($('#cmbRegCountry').val()=="150"){
                        if($('#txtIssueDate').val()!="" && $('#txtExpiryDate').val()!=""){
                            if(!CompareToForGreater1($('#hdnCurrDate').val(),$('#txtIssueDate').val())){
                                $('#issueDatestart').html("Trade License Issue Date has to be prior than the current date.");
                                bol= false;
                            }else{
                                if(!CompareToForGreater1($('#txtExpiryDate').val(),$('#txtIssueDate').val())){
                                    $('#cmpDate').html("Trade license expiry date must be greater than issue date.");
                                    bol= false;
                                }else{
                                    $('#cmpDate').html(null);
                                    $('#issueDatestart').html(null);
                                }
                            }
                        }
                         if($.trim($('#txtRegThana').val())==""){
//                            $('#spRthana').html("Please enter Thana / UpaZilla");
//                            $('#txtRegThana').focus();
//                             bol= false;
                         }else{
                             $('#spRthana').html(null);
                         }
                     }
                     else
                     {
                        if($('#txtIssueDate').val()!=""){
                            if(!CompareToForGreater1($('#hdnCurrDate').val(),$('#txtIssueDate').val())){
                                $('#issueDatestart').html("Trade License Issue Date has to be prior than the current date.");
                                bol= false;
                            }else{
                                if($('#txtExpiryDate').val()!=""){
                                    if(!CompareToForGreater1($('#txtExpiryDate').val(),$('#txtIssueDate').val())){
                                    $('#cmpDate').html("Trade license expiry date must be greater than issue date.");
                                    bol= false;
                                }else{
                                    $('#issueDate').html(null);
                                    $('#expiryDate').html(null);
                                    $('#cmpDate').html(null);
                                    $('#issueDatestart').html(null);
                                }
                                }else
                                    {
                                        $('#cmpDate').html("Please enter expiry date.");
                                    }
                            }
                        }

                     }

                     //Change code 136 to 150,Proshanto
                     if($('#cmbCorpCountry').val()=="150"){
                         if($.trim($('#txtCorpThana').val())==""){
//                             $('#spCthana').html("Please enter Gewog");
//                             $('#txtCorpThana').focus();
//                             bol= false;
                         }else{
                             if($.trim($('#txtCorpThana').val()).lenght >50){
                                 $('#spCthana').html("Maximum 50 Characters are allowed.");
                             bol= false;
                             }
                             else
                             {
                                 if(!alphaNationalId($.trim($('#txtCorpThana').val()))){
                                     $('#spCthana').html("Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.");
                                     bol= false;
                             }else
                                     {
                                         $('#spCthana').html(null);
                                     }

                             }
                         }
                     }
                    /*if($('#cmbTin').val()=="tin"){
                        $('#txtaODoc').val(null);
                        if($('#tinNoNo').val()==""){
                            $('#tinNoNospan').html("Please enter Tax Payment Number/Other Identification Number");
                            $('#tinNoNo').focus();
                            bol= false;
                        }
                    }if($('#cmbTin').val()=="other"){
                        if($('#tinNoNo').val()==""){
                            $('#tinNoNospan').html("Please enter Tax Payment Number/Other Identification Number");
                            $('#tinNoNo').focus();
                            bol= false;
                        }
                        if($('#txtaODoc').val()==""){
                            $('#txtaODocspan').html("Please enter Description");
                            $('#txtaODoc').focus();
                            bol= false;
                        }
                    }*/
                    /*if($('#ifedit').html()==null){
                        if($.trim($('#txtCompNo').val())==""){
                            $('span.#comMsg').css("color","red");
                            $('span.#comMsg').html('Please enter Company Registration Number');
                            bol= false;
                        }
                    }*/

                    if($.trim($('#txtaCorpAddr').val())==""){
                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
                        bol= false;
                    }else
                        {
                            if(document.getElementById("txtaCorpAddr").value.length >150){
                                $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                 $('#stxtaCorpAddr').html('');
                            }
                        }


                     if($.trim($('#txtCorpCity').val())==""){
//                        $('#stxtCorpCity').html('Please enter City / Town');
//                        bol= false;
                     }else
                        {
                            if($.trim(document.getElementById("txtCorpCity").value.length) >50){
                                $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                if(!DigAndNum(document.getElementById("txtCorpCity").value)){
                                    $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                                }else{
                                 $('#stxtCorpCity').html('');
                                 }
                            }
                        }

                        if($.trim($('#corpPhoneSTD').val())==""){
                        $('#scorpPhoneSTD').html('Please enter Area Code.');
                        bol= false;
                         }else
                            {
                                if($.trim(document.getElementById("corpPhoneSTD").value.length) >10){
                                    $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                                    bol= false;
                                }
                                else
                                {

                                    if(!digits(document.getElementById("corpPhoneSTD").value)){
                                        $('#scorpPhoneSTD').html("Please enter Numbers only");
                                    }else{
                                     $('#scorpPhoneSTD').html('');
                                     }
                                }
                            }

                            if($.trim($('#txtCorpPhone').val())==""){
                                $('#stxtCorpPhone').html('Please enter Phone No.');
//                                bol= false;
                             }else
                                {
                                    if($.trim(document.getElementById("txtCorpPhone").value.length) >20){
                                        $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                                        bol= false;
                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("txtCorpPhone").value)){
                                            $('#stxtCorpPhone').html("Please enter Numbers Only");
                                        }else{
                                         $('#stxtCorpPhone').html('');
                                         }
                                    }
                                }


                    if(!bol){
                        return false;
                    }else{

                        if(($('#cmpSubmit').valid()) && (document.getElementById("hdnerrorcount").value==0)){
                                if($('#btnSave').val()!=null){
                                    //$('#btnSave').attr("disabled","true");
                                    $('#hdnsaveNEdit').val("Save");
                                 }
                                 if($('#btnUpdate').val()!=null){
                                    //$('#btnUpdate').attr("disabled","true");
                                    $('#hdnsaveNEdit').val("Update");
                                 }
                        }
                    }
                });
            });

            function DigAndNum(value) {
                return /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-.&,\(\)\']+)?)+$/.test(value);
            }
            function alphaNationalId(value) {
                return /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\!\(\)\-\.\&\*\#\@/\\(/)/)])+$)/.test(value);
            }

            function alphanumeric(value) {
                return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
            }

            function digits(value) {
                return /^\d+$/.test(value);
            }

            function CompareToForGreater1(value,params)
            {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                         var date =  new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                         var datep =  new Date(mdyphr[0], mdyp[1]-1, mdyp[0]);
                    }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                         //alert('Both DateTime');
                         var mdyhrsec=mdyhr[1].split(':');
                         var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                         var mdyphrsec=mdyphr[1].split(':');
                         var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                else
                    {
                            //alert('one Date and One DateTime');
                            var a = mdyhr[1];  //time
                            var b = mdyphr[1]; // time

                            if(a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                    {
                                       //alert('Second Date');
                                       var mdyhrsec=mdyhr[1].split(':');
                                       var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                       var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                    }

                return Date.parse(date) > Date.parse(datep);
            }
        </script>
        <script type="text/javascript">
            $(function() {
                
                
                
                
                
                $('#txtCompNo').blur(function() {
                    if($.trim($('#txtCompNo').val())==""){
                        $('span.#comMsg').css("color","red");
//                        $('span.#comMsg').html('Please enter Company Registration Number');
                        $('#tbodyHide').show();
                        $('span.#redirect').css("display","none");
                    }else{
                        $('span.#comMsg').css("color","red");
                        $('span.#comMsg').html("Checking for Unique Registration No...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {compRegNo:$('#txtCompNo').val(),funName:'compNoChk'},
                        function(j){
                            if(j.toString()!=""){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('span.#comMsg').css("color","green");
                                    $('#tbodyHide').show();
                                    $('span.#redirect').css("display","none");
                                }
                                else {
                                    /*
                                     *Below block of code is commented for change that
                                     *already exist company dump is
                                     *no more required.
                                    */
                                    $('span.#comMsg').css("color","red");
                                    /*$('#tbodyHide').hide();
                                    $('span.#redirect').css("display","block");
                                    var temp = "<=request.getContextPath()%>/PersonalDetails.jsp?cmpId="+j.toString().substring(j.toString().indexOf(",", 0)+1, j.toString().length);
                                    $('#hdnCmp').html("<=request.getContextPath()%>/PersonalDetails.jsp?cmpId="+j.toString().substring(j.toString().indexOf(",", 0)+1, j.toString().indexOf("@", 0)));
                                    $('#txtCmpName').val(j.toString().substring(j.toString().indexOf("@", 0)+1, j.toString().length));*/
                                    j = j.toString().substring(0,j.toString().indexOf(",", 0));
                                }
                                $('span.#comMsg').html(j);
                            }else{
                                $('span.#comMsg').html(null);
                                $('#tbodyHide').show();
                                $('span.#redirect').css("display","none");
                            }
                        });
                       }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {objectId:$('#txtCompNo').val(),funName:'tmpCmpInsert'},  function(j){
                        document.getElementById("cmpSubmit").action="<%=request.getContextPath()%>/PersonalDetails.jsp";
                        document.getElementById("cmpSubmit").submit();
            <%--?cmpId="+j.toString();-- if replicate entry is removed
            than uncomment this and append it above (j) contains cmpId--%>
                        });
                    });
                });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#chkRC').click(function() {
                    if($('#chkRC').attr("checked")){
                        $('#txtaCorpAddr').val($('#txtaRegAddr').val());
                        $('#txtCorpCity').val($('#txtRegCity').val());
                        $('#txtCorpThana').val($('#txtRegThana').val());
                        $('#txtCorpPost').val($('#txtRegPost').val());
                        $('#corpPhoneSTD').val($('#regPhoneSTD').val());
                        $('#txtCorpPhone').val($('#txtRegPhone').val());
                        $('#corpFaxSTD').val($('#regFaxSTD').val());
                        $('#txtCorpFax').val($('#txtRegFax').val());
                        $('#cmbCorpCountry').val($('#cmbRegCountry').val());
                        $('#corpPhoneCode').val($('#regPhoneCode').val());
                        $('#corpFaxCode').val($('#regFaxCode').val());
                        $('#txtaCorpAddr').attr("readonly", "true");
                        $('#corpPhoneSTD').attr("readonly", "true");
                        $('#corpFaxSTD').attr("readonly", "true");
                        $('#txtCorpCity').attr("readonly", "true");
                         $('#txtCorpThana').attr("readonly", "true");
                         $('#txtCorpPost').attr("readonly", "true");
                         $('#corpPhoneCode').attr("readonly", "true");
                        $('#corpFaxCode').attr("readonly", "true");
                         $('#txtCorpPhone').attr("readonly", "true");
                         $('#txtCorpFax').attr("readonly", "true");
                        //Change code 136 to 150,Proshanto
                        if($('#cmbRegCountry').val()!="150"){
                            $('#trCthana').css("display","none")
                        }
                        $('#cmbCorpState').html($('#cmbRegState').html());
                        for(var i=0;i<document.getElementById('cmbRegState').options.length;i++){
                            if(document.getElementById('cmbRegState').options[i].selected){
                                document.getElementById('cmbCorpState').options[i].selected="selected";
                            }else{
                                document.getElementById('cmbCorpState').options[i].disabled="true";
                            }
                        }
                        for(var i=0;i<document.getElementById('cmbRegCountry').options.length;i++){
                            if(document.getElementById('cmbRegCountry').options[i].selected){
                                document.getElementById('cmbCorpCountry').options[i].selected="selected";
                            }else{
                                document.getElementById('cmbCorpCountry').options[i].disabled="true";
                            }
                        }

                        if($.trim($('#txtaRegAddr').val())==""){
//                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
//                        bol= false;
                    }else
                        {
                            if(document.getElementById("txtaRegAddr").value.length >150){
                                $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                 $('#stxtaCorpAddr').html('');
                            }
                        }


                     if($.trim($('#txtRegCity').val())==""){
//                        $('#stxtCorpCity').html('Please enter City / Town');
//                        bol= false;
                     }else
                        {
                            if($.trim(document.getElementById("txtRegCity").value.length) >50){
                                $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                                bol= false;
                            }
                            else
                            {
                                if(!DigAndNum(document.getElementById("txtRegCity").value)){
                                    $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                                }else{
                                 $('#stxtCorpCity').html('');
                                 }
                            }
                        }

                        if($.trim($('#regPhoneSTD').val())==""){
//                        $('#scorpPhoneSTD').html('Please enter Area Code.');
//                        bol= false;
                         }else
                            {
                                if($.trim(document.getElementById("regPhoneSTD").value.length) >10){
                                    $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                                    bol= false;
                                }
                                else
                                {

                                    if(!digits(document.getElementById("regPhoneSTD").value)){
                                        $('#scorpPhoneSTD').html("Please enter Numbers only");
                                    }else{
                                     $('#scorpPhoneSTD').html('');
                                     }
                                }
                            }

                            if($.trim($('#txtRegPhone').val())==""){
//                                $('#stxtCorpPhone').html('Please enter Phone No.');
//                                bol= false;
                             }else
                                {
                                    if($.trim(document.getElementById("txtRegPhone").value.length) >20){
                                        $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                                        bol= false;
                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("txtRegPhone").value)){
                                            $('#stxtCorpPhone').html("Please enter Numbers Only");
                                        }else{
                                         $('#stxtCorpPhone').html('');
                                         }
                                    }
                                }

                                if($.trim($('#txtRegThana').val())==""){
//                                        $('#spCthana').html("Please enter Gewog");
                                    }
                                    else
                                        {
                                            $('#spCthana').html(null);
                                        }



                    }else{
                        for(var i=0;i<document.getElementById('cmbCorpCountry').options.length;i++){
                            document.getElementById('cmbCorpCountry').options[i].disabled=false;
                            //Change code 136 to 150,Proshanto
                            if(document.getElementById('cmbCorpCountry').options[i].value=="150"){
                                document.getElementById('cmbCorpCountry').options[i].selected="selected";
                            }
                        }
                        //Change code 136 to 150,Proshanto
                        onTickCountry("150");
                        $('#txtaCorpAddr').val(null);
                        $('#txtCorpCity').val(null);
                        $('#txtCorpThana').val(null);
                        $('#txtCorpPost').val(null);
                        $('#corpPhoneSTD').val(null);
                        $('#txtCorpPhone').val(null);
                        $('#corpFaxSTD').val(null);
                        $('#txtCorpFax').val(null);
                        $('#txtaCorpAddr').removeAttr("readonly");
                        $('#corpPhoneSTD').removeAttr("readonly");
                        $('#corpFaxSTD').removeAttr("readonly");
                        $('#txtCorpCity').removeAttr("readonly");
                         $('#txtCorpThana').removeAttr("readonly");
                         $('#txtCorpPost').removeAttr("readonly");
                         $('#corpPhoneCode').removeAttr("readonly");
                        $('#corpFaxCode').removeAttr("readonly");
                         $('#txtCorpPhone').removeAttr("readonly");
                         $('#txtCorpFax').removeAttr("readonly");
                    }


                });
            });
            <%--For emptying thana msg on blur--%>
             $(function() {
                $('#txtRegThana').blur(function() {
                    if($.trim($('#txtRegThana').val())==""){
//                        $('#spRthana').html("Please enter Gewog");
                    }
                    else
                        {
                            $('#spRthana').html(null);
                        }
                });

                $('#txtaCorpAddr').blur(function() {
                    if($.trim($('#txtaCorpAddr').val())==""){
//                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');

                    }else
                        {
                            if(document.getElementById("txtaCorpAddr").value.length >150){
                                $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');

                            }
                            else
                            {
                                 $('#stxtaCorpAddr').html('');
                            }
                        }
                });

                $('#txtCorpCity').blur(function() {
                    if($.trim($('#txtCorpCity').val())==""){
                        //$('#stxtCorpCity').html('Please enter City / Town');

                     }else
                        {
                            if($.trim(document.getElementById("txtCorpCity").value.length) >50){
                                $('#stxtCorpCity').html('Maximum 50 Characters are allowed');

                            }
                            else
                            {
                                if(!DigAndNum(document.getElementById("txtCorpCity").value)){
                                    $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                                }else{
                                 $('#stxtCorpCity').html('');
                                 }
                            }
                        }
                });

                $('#corpPhoneSTD').blur(function() {
                    if($.trim($('#corpPhoneSTD').val())==""){
//                        $('#scorpPhoneSTD').html('Please enter Area Code.');

                         }else
                            {
                                if($.trim(document.getElementById("corpPhoneSTD").value.length) >10){
                                    $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');

                                }
                                else
                                {

                                    if(!digits(document.getElementById("corpPhoneSTD").value)){
//                                        $('#scorpPhoneSTD').html("Please enter Numbers only");
                                    }else{
                                     $('#scorpPhoneSTD').html('');
                                     }
                                }
                            }
                });

                $('#txtCorpPhone').blur(function() {
                     if($.trim($('#txtCorpPhone').val())==""){
                                $('#stxtCorpPhone').html('Please enter Phone No.');

                             }else
                                {
                                    if($.trim(document.getElementById("txtCorpPhone").value.length) >20){
                                        $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');

                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("txtCorpPhone").value)){
                                            $('#stxtCorpPhone').html("Please enter Numbers Only");
                                        }else{
                                         $('#stxtCorpPhone').html('');
                                         }
                                    }
                                }
                });


                $('#corpFaxSTD').blur(function() {
                    if($.trim(document.getElementById("corpFaxSTD").value).length!=0){
                    if($.trim(document.getElementById("corpFaxSTD").value.length) >10){
                                        $('#scorpFaxSTD').html('Maximum 10 Digits are allowed.');

                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("corpFaxSTD").value)){
                                            $('#scorpFaxSTD').html("Please enter Numbers Only");

                                        }else{
                                         $('#scorpFaxSTD').html('');
                                         }
                                    }
                   }
                });

                $('#txtCorpFax').blur(function() {
                    if($.trim(document.getElementById("txtCorpFax").value).length!=0){
                    if($.trim(document.getElementById("txtCorpFax").value.length) >20){
                                        $('#scorpFaxSTD').html('Maximum 20 Digits are allowed.');

                                    }
                                    else
                                    {

                                        if(!digits(document.getElementById("txtCorpFax").value)){
                                            $('#scorpFaxSTD').html("Please enter Numbers Only");

                                        }else{
                                         $('#scorpFaxSTD').html('');
                                         }
                                    }
                    }
                });
            });
            $(function() {
                $('#txtCorpThana').blur(function() {
                    if($.trim($('#txtCorpThana').val())==""){
                        //$('#spCthana').html("Please enter Gewog");
                    }else
                        {
                            $('#spCthana').html(null);
                        }
                });
            });
            <%--that msg ends--%>
             $(function() {
                $('#cmbCorpCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbCorpCountry').val(),funName:'countryCode'},  function(j){
                        $('#corpPhoneCode').val(j.toString());
                        $('#corpFaxCode').val(j.toString());
                    });
                });
            });
            $(function() {
                $('#cmbRegCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId:$('#cmbRegCountry').val(),funName:'countryCode'},  function(j){
                        $('#regPhoneCode').val(j.toString());
                        $('#regFaxCode').val(j.toString());
                    });
                });
            });
             function loadCPVTree()
            {
                window.open('../resources/common/CPVTree.jsp','CPVTree','menubar=0,scrollbars=1,width=700px');
            }
            function SubDistrictANDAreaCodeLoadOffice()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbRegSubdistrict').val()}, function (j) {
                    $('#cmbRegSubdistrict').html(j);
                });
                if($('#cmbRegSubdistrict').val()=="Phuentsholing")
                {
                    $('#regPhoneSTD').val('05');
                    $('#regFaxSTD').val('05');
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#regPhoneSTD').val(j);
                        $('#regFaxSTD').val(j);
                    });
                }
            }
        </script>
    </head>
    <body onload="SubDistrictANDAreaCodeLoadOffice();">
         <%
                tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
                boolean ifEdit = false;
                boolean ifCmpRegExist = false;
                String cmpName = null;
                ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                TblCompanyMaster dtBean = null;
                List<TblCompanyMaster> list = contentAdminService.findCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId"))));
                String jvCmpName = contentAdminService.jvcaName(request.getParameter("uId"));
                //String jvcaId = contentAdminService.jvcaID(request.getParameter("uId"));
                if(!list.isEmpty()){
                    dtBean = list.get(0);
                    ifEdit = true;
                }


        %>
        <form name="cmpSubmit" id="cmpSubmit" method="post"></form>
        <div class="mainDiv">
            <input type="hidden" id="hdnerrorcount" name="hdnerrorcount" value="0"/>
            <div class="fixDiv">
                <jsp:include page="../resources/common/AfterLoginTop.jsp"/>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea-Blogin"><div class="pageHead_1">JVCA - Company Details</div>
                            <!--Page Content Start-->
                            <!--Navigation Panel starts-->
                                 <%
                                    String pageName=request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1, request.getRequestURI().indexOf(".jsp"));
                                    List<TblTendererMaster> navDisplay = contentAdminService.findTblTendererMasters("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId"))));
                                    boolean isPAvail=false;
                                    if (!list.isEmpty()) {
                                        isPAvail=true;
                                    }
                                 %>

                                <div class="stepWiz_1 t_space" style="width: 100%; height: 18px;">
                                    <div style="width:80%; float: left;">
                                        <ul>
                                            <li <%if(pageName.equals("JVCompanyDetails")){%>class="sMenu"<%}%>><%if(pageName.equals("JVCompanyDetails")){out.print("Company Details");}else{%><a href="JVCompanyDetails.jsp?uId=<%=request.getParameter("uId")%>">Company Details</a><%}%></li>
                                            <li>>></li>
                                            <li <%if(!isPAvail){%>class="sMenu"<%}%>><%if(!isPAvail){out.print("Company Contact Person Details");}else{%><a href="JVPersonalDetails.jsp?uId=<%=request.getParameter("uId")%>">Company Contact Person Details</a><%}%></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--Navigation Panel ends-->

                                <form id="frmComp" method="post" action="InsertJVCompanyDetails.jsp?uId=<%=request.getParameter("uId")%>">
                                <%if (!ifCmpRegExist) {%>
                                <%if("s".equals(request.getParameter("msg"))){%><br/><div id="succMsg" class="responseMsg successMsg"  >Information successfully updated.</div><%}%>
                                <%if("y".equals(request.getParameter("cpr"))){%><br/><div class="responseMsg errorMsg">Company Registration No. already exist</div><%}%>
                                <%if (ifEdit) {%><input type="hidden" value="y" id="ifedit"/><%}%>
                                <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" id="tb2" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" height="30" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr  style="display: none;">
                                        <td class="ff" width="30%" height="30">Company Registration No. : <span>*</span></td>
                                        <td width="70%"><%if (ifEdit) {out.print(dtBean.getCompanyRegNumber()+"<input name='companyRegNumber' type='hidden' value='"+dtBean.getCompanyRegNumber()+"'/>");}else{%><input name="companyRegNumber" type="text" class="formTxtBox_1" id="txtCompNo" maxlength="30" style="width:200px;" value=" "/><%}%>
                                            <br/><span id="comMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                </table>
                                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1"  id="tbodyHide"  width="100%">
                                            <%--<tr>
                                                <td colspan="2">                                            --%>
                                                <%--<tbody>--%>
                                        <tr>
                                            <td class="ff" height="30">JVCA Name : <span>&nbsp;</span> </td> 
                                            <td width="70%"> <strong> <%=jvCmpName%> </strong> <input name="companyName" type="hidden" class="formTxtBox_1" id="txtCompName" maxlength="200" style="width:200px;" value="<%if (true) {
                                                    out.print(jvCmpName);
                                                }%>"/></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Company Name in Bangla : </td>
                                            <td><input name="banglaName" type="text" class="formTxtBox_1" id="txtCompBang" maxlength="300" style="width:200px;" value="<%if(ifEdit){if(dtBean.getCompanyNameInBangla()!=null){out.print(BanglaNameUtils.getUTFString(dtBean.getCompanyNameInBangla()));}}else{out.print(" ");}%>"/></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Company's Legal Status : <span>*</span></td>
                                            <td><select name="legalStatus" class="formTxtBox_1" id="cmbLegal">
                                                    <option  value="public" <%if (ifEdit) {if (dtBean.getLegalStatus().equals("public")) {%>selected<%}}%>>Public Ltd</option>
                                                    <option value="private" <%if (ifEdit) {if (dtBean.getLegalStatus().equals("private")) {%>selected<%}}%>>Private Ltd</option>
                                                    <option value="partnership" <%if (ifEdit) {if (dtBean.getLegalStatus().equals("partnership")) {%>selected<%}}%>>Partnership</option>
                                                    <option value="proprietor" <%if (ifEdit) {if (dtBean.getLegalStatus().equals("proprietor")) {%>selected<%}}%>>Proprietor</option>
                                                    <option value="government" <%if (ifEdit) {if (dtBean.getLegalStatus().equals("government")) {%>selected<%}}%>>Government Undertaking</option>
                                                </select></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Company's<br />
                                                Establishment Year : <span>*</span></td>
                                            <td><input name="establishmentYear" type="text" class="formTxtBox_1" id="txtComYear" style="width:200px;" value="<%if (ifEdit) {
                                                    out.print(dtBean.getEstablishmentYear());
                                                }else{out.print(" ");}%>"/> <span class="formNoteTxt" id="cey">(YYYY e.g. 2000)</span></td>
                                        </tr>
                                        <%--<tr style="display:none;">
                                            <td class="ff" height="30">Tax Payment Number(TIN)/<br/>Other Identification Number : <span>*</span></td>
                                            <td>
                                            <select name="tinNo" class="formTxtBox_1" id="cmbTin">
                                                <option <%if(ifEdit){if(dtBean.getTinNo().equals("tin")){%>selected<%}}%> value="tin">TIN</option>
                                                <option value="other" <%if(ifEdit){if(dtBean.getTinNo().equals("other")){%>selected<%}}%>>Other Similar Document</option>
                                            </select>

                                            <div class="t_space">
                                                <%
                                                    if(ifEdit){
                                                        if(dtBean.getTinDocName()!=null && dtBean.getTinDocName().contains("$")){
                                                %>
                                                <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%=dtBean.getTinDocName().substring(0, dtBean.getTinDocName().indexOf("$"))%>"  class="formTxtBox_1" onblur="cleartinDocName()"/><br/>
                                                <div class="formNoteTxt" id="1n">(Tax Payment Number or Other Similar No.)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                                <div id="divdescription">
                                                    <textarea name="otherDoc" cols="10" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;" onblur="cleartxtaODoc()"><%=dtBean.getTinDocName().substring(dtBean.getTinDocName().indexOf("$")+1,dtBean.getTinDocName().length())%></textarea>
                                                <div class="formNoteTxt" id="2n">(Document Description)<br/><span id="txtaODocspan"></span></div>
                                                </div>
                                                <%
                                                        }else{
                                                            %>
                                                 <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%if(ifEdit){out.print(dtBean.getTinDocName());}%>"  class="formTxtBox_1" onblur="cleartinDocName()"/>
                                                 <div class="formNoteTxt" id="1n">(Tax Payment Number or Other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                                 <div id="divdescription" style="display: none;">
                                                     <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;" onblur="cleartxtaODoc()"></textarea>
                                                    <div class="formNoteTxt" id="2n" >(Document Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                                 </div>
                                                    <%
                                                        }
                                                     }else{
                                            %>
                                            <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" class="formTxtBox_1" onblur="cleartinDocName()"/><br/>
                                            <div class="formNoteTxt" id="1n">(Tax Payment Number or Other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                             <div id="divdescription" style="display: none;">
                                                 <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;" onblur="cleartxtaODoc()"></textarea>
                                            <div class="formNoteTxt" id="2n">(Document Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                           </div>
                                            <%
                                                     }
                                            %>
                                            </div>
                                        </td>
                                        </tr>--%>
                                            <tr>
                                                <td colspan="2" height="5"></td>
                                            </tr>
                                        <%--<tr style="display: none">
                                            <td class="ff" height="30">Nature of Business : <span>*</span></td>
                                            <td><textarea cols="10"  name="specialization" rows="3" class="formTxtBox_1" id="txtaCPV" style="width:350px;" readonly><%if(ifEdit){out.print(dtBean.getSpecialization());}%></textarea> <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()" id="aTree">Select Categories</a>
                                                <div id="cpvTip">(Please select the relevant category in which you wish to get enlisted for)</div>
                                            </td>
                                        </tr>--%>
<!--                                        <tr>
                                                <td colspan="2" height="5"></td>
                                        </tr>-->
                                        <tr>
                                            <td class="ff">Office Address : <span>*</span></td>
                                            <td><textarea cols="10" name="regOffAddress" rows="3" class="formTxtBox_1" id="txtaRegAddr" style="width:350px;"><%if (ifEdit) {
                                                    out.print(dtBean.getRegOffAddress());
                                                }%></textarea></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="5"></td>
                                            </tr>
                                        <tr>
                                            <td class="ff" height="30">Country : <span>*</span></td>
                                            <td><select name="regOffCountry" class="formTxtBox_1" id="cmbRegCountry">
                                                    <%
                                                        //Change Bangladesh to Bhutan,Proshanto
                                                        String countryReg = "Bhutan";
                                                        if (ifEdit) {
                                                            countryReg = dtBean.getRegOffCountry();
                                                        }
                                                        for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                    %>
                                                    <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryReg)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="5"></td>
                                            </tr>
                                        <tr>
                                            <td class="ff" height="30">Dzongkhag / District : <span>*</span></td>
                                            <td><select name="regOffState" class="formTxtBox_1" id="cmbRegState">
                                                    <!--Change Bangladesh to Bhutan,Proshanto-->
                                                    <%String regCntName="";if(ifEdit){regCntName=dtBean.getRegOffCountry();}else{regCntName="Bhutan";}for (SelectItem state : tendererMasterSrBean.getStateList(regCntName)) {%>
                                                    <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                            if (state.getObjectValue().equals(dtBean.getRegOffState())) {%>selected<%}
                                                               //Change Dhaka to Thimphu,Proshanto
                                                                }else{if(state.getObjectValue().equals("Thimphu")){%>selected<%}}%>><%=state.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="5"></td>
                                        </tr>
                                        <%--<tr  style="display: none;">
                                            <td class="ff" height="30">Trade License Issue Date : <span id="trtlid">*</span></td>
                                            <td><input name="licIssueDateStr" class="formTxtBox_1" id="txtIssueDate" type="text" style="width:100px;"  value="<%if (ifEdit) {
                                                    if(dtBean.getLicIssueDate()!=null){out.print(DateUtils.formatStdDate(dtBean.getLicIssueDate()));}
                                            }%>" readonly="true" onblur="clearIssueDate();"  onfocus="GetCal('txtIssueDate','txtIssueDate');"/>

                                                <img id="txttenderLastSellDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtIssueDate','txttenderLastSellDateimg');"/>
                                                <span id="issueDate" style="color: red; "></span>
                                                <div id="issueDatestart" style="color: red; "></div>
                                            </td>
                                        </tr>
                                        <tr  style="display: none;">
                                            <td class="ff" height="30">Trade License Expiry Date : <span id="trtled">*</span></td>
                                            <td><input name="licExpiryDateStr" type="text" class="formTxtBox_1" type="text" id="txtExpiryDate" style="width:100px;"  value="<%if (ifEdit) {
                                                    if(dtBean.getLicExpiryDate()!=null){out.print(DateUtils.formatStdDate(dtBean.getLicExpiryDate()));
                                           }}%>"  readonly="true" onblur="clearExpiryDate();" onfocus="GetCal('txtExpiryDate','txtExpiryDate');"/>

                                                <img id="txttenderLastSellDateimg1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtExpiryDate','txttenderLastSellDateimg1');"/>
                                            <span id="expiryDate" style="color: red; "></span>
                                            <div id="cmpDate" style="color: red; "></div>
                                            </td>
                                        </tr>--%>
                                        
                                        <tr id="trRSubdistrict">
                                            <td class="ff" height="30">Dungkhag / Sub-district : </td>
                                            <td>
                                                <select name="regOffSubDistrict" class="formTxtBox_1" id="cmbRegSubdistrict"  style="width:218px;">
                                                    <%String regdistrictName = "";
                                                        if (ifEdit) {
                                                            regdistrictName = dtBean.getRegOffState();
                                                        } else {
                                                            regdistrictName = "Thimphu";
                                                        }
                                                        for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regdistrictName)) {%>
                                                    <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                            if (subdistrict.getObjectValue().equals(dtBean.getRegOffSubDistrict())) {
                                                            %>selected<%
                                                                }
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                    <%}%>
                                                </select>
                                                
                                                
                                            
                                            </td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        <tr id="trRthana">
                                            <td class="ff" height="30">Gewog : </td>
                                            <td><select name="regOffUpjilla" class="formTxtBox_1" id="txtRegThana" style="width:218px;">
                                                    <%String regState = "Thimphu";
                                                    String regSubdistrict = "";
                                                        if (ifEdit) {
                                                            regState = dtBean.getRegOffState();
                                                            regSubdistrict = dtBean.getRegOffSubDistrict();
                                                        }
                                                        if(!regState.isEmpty() && !regSubdistrict.isEmpty()){
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(regSubdistrict)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } else if (gewoglist.getObjectValue().equals("")) {%>
                                                            selected<%
                                                                }%>><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    else {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(regState)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } %>
                                                            
                                                                ><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    %>
                                                </select></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">City / Town : <span>&nbsp;</span></td>
                                            <td><input name="regOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtRegCity" style="width:200px;" value="<%if (ifEdit) {
                                                    if(dtBean.getRegOffCity()!=null)out.print(dtBean.getRegOffCity());
                                                }%>"/></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        <%--<tr id="trRthana">
                                            <td class="ff">Gewog: <span>&nbsp;</span></td>
                                            <td><input name="regOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtRegThana" style="width:200px;"  value="<%if (ifEdit) {
                                                    out.print(dtBean.getRegOffUpjilla());
                                                }%>"/><div id="spRthana" style="color: red;"></div></td>
                                        </tr>--%>
                                        <tr id="trThanaHgt">
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                          <!--Change Bangladesh to Bhutan-->
                                          <%if (ifEdit) {if(!dtBean.getRegOffCountry().equalsIgnoreCase("Bhutan")){%>
                                          <script type="text/javascript">
                                              $('#trtlid').css("display","none");
                                              $('#trtled').css("display","none");
                                              $('#trRthana').css("display","none");
                                              $('#trThanaHgt').css("display","none");
                                          </script>
                                          <%}}%>
                                        <tr>
                                            <td class="ff" height="30">Postcode : <span>&nbsp;</span></td>
                                            <td><input name="regOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtRegPost" style="width:200px;"  value="<%if (ifEdit) {
                                                    out.print(dtBean.getRegOffPostcode());
                                                }%>"/></td>
                                        </tr>
                                        <tr id="trThanaHgt">
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="ff">Mobile No. : <span>*</span></td>
                                            <td>
                                                <input class="formTxtBox_1" style="width: 30px" id="regMobCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                    } else {
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input name="regOffMobileNo" type="text" class="formTxtBox_1" maxlength="20" id="txtregOffMobileNo" style="width:150px;"  value="<%if (ifEdit) {
                                                            out.print(dtBean.getRegOffMobileNo());
                                                        }%>"/>
                                                <span id="regmobMsg" style="color: red;">&nbsp;</span>
                                            </td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="ff" height="30">Phone No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="regPhoneCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if(ifEdit){
                                                    if(!dtBean.getRegOffPhoneNo().isEmpty()){
                                                    out.print(dtBean.getRegOffPhoneNo().split("-")[0]);}}
                                                %>" id="regPhoneSTD" name="regPhoneSTD" class="formTxtBox_1" style="width:50px;" />-<input name="regOffPhoneNo" type="text" maxlength="20" class="formTxtBox_1" id="txtRegPhone" style="width:100px;"  
                                                value="<%if (ifEdit) {
                                                    if(!dtBean.getRegOffPhoneNo().isEmpty()){
                                                    out.print(dtBean.getRegOffPhoneNo().split("-")[1]);
                                                }}%>"/>
                                                <input type="hidden" id="hdnvalue" name="hdnvalue" />
                                                <span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Fax No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="regFaxCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                        }%>" readonly/>-<input type="text" value="<%if(ifEdit){
                                                            if(!dtBean.getRegOffFaxNo().isEmpty()){
                                                            if(dtBean.getRegOffFaxNo().contains("-")){out.print(dtBean.getRegOffFaxNo().split("-")[0]);}}}%>" id="regFaxSTD" name="regFaxSTD" class="formTxtBox_1" style="width:50px;"/>-<input name="regOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtRegFax"  style="width:100px;"  value="<%if(ifEdit){
                                                                if(!dtBean.getRegOffFaxNo().isEmpty()){
                                                                if(dtBean.getRegOffFaxNo().contains("-")){out.print(dtBean.getRegOffFaxNo().split("-")[1]);
                                                }}}%>"/></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" height="7"></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">&nbsp;</td>
                                            <td>
                                                <div class="formNoteTxt"><input name="chkRC" type="checkbox" id="chkRC" /> (Check if Registered and Corporate office details are same)</div>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Corporate / Head office<br />
                                                Address : <span>*</span></td>
                                            <td><textarea cols="10" name="corpOffAddress" rows="3" class="formTxtBox_1" id="txtaCorpAddr" style="width:350px;"><%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffAddress());
                                                }else{out.print(" ");}%></textarea>
                                            <span id="stxtaCorpAddr" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                                <td colspan="2" height="5"></td>
                                            </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Country : <span>*</span></td>
                                            <td><select name="corpOffCountry" class="formTxtBox_1" id="cmbCorpCountry">
                                                    <%
                                                        //Change Bangladesh to Bhutan,Proshanto
                                                        String countryCorp = "Bhutan";
                                                        if (ifEdit) {
                                                            countryCorp = dtBean.getCorpOffCountry();
                                                        }
                                                        for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                    %>
                                                    <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryCorp)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Dzongkhag / District : <span>*</span></td>
                                            <td><select name="corpOffState" class="formTxtBox_1" id="cmbCorpState">
                                                    <!--Change Bangladesh to Bhutan,Proshanto-->
                                                    <%String corpCntName="";if(ifEdit){corpCntName=dtBean.getCorpOffCountry();}else{corpCntName="Bhutan";}for (SelectItem state : tendererMasterSrBean.getStateList(corpCntName)) {%>
                                                    <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                            if (state.getObjectValue().equals(dtBean.getCorpOffState())) {%>selected<%}
                                                               //Change Dhaka to Thimphu,Proshanto
                                                                }else{if(state.getObjectValue().equals("Thimphu")){%>selected<%}}%>><%=state.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">City / Town : <span>*</span></td>
                                            <td><input name="corpOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpCity" style="width:200px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffCity());
                                                }else{out.print(" ");}%>"/>
                                            <span id="stxtCorpCity" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <tr id="trCthana" height="30" style="display: none;">
                                            <td class="ff">Gewog : <span>*</span></td>
                                            <td><input name="corpOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpThana" style="width:200px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffUpjilla());
                                                                                                   }else{out.print(" ");}%>"/><div id="spCthana" style="color: red;"></div>
                                            <span id="stxtCorpThana" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <!--Change Bangladesh to Bhutan,Proshanto-->
                                        <%if (ifEdit) {if(!dtBean.getCorpOffCountry().equalsIgnoreCase("Bhutan")){%>
                                          <script type="text/javascript">
                                              $('#trCthana').css("display","none");
                                          </script>
                                          <%}}%>
                                        <%--<tr style="display: none;">
                                            <td class="ff" height="30">Postcode / Zip Code : <!--span>*</span--></td>
                                            <td><input name="corpOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtCorpPost" style="width:200px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffPostcode());
                                                }else{out.print(" ");}%>"/></td>
                                        </tr>
                                        
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Phone No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="corpPhoneCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if(ifEdit){out.print(dtBean.getCorpOffPhoneno().split("-")[0]);}%>" id="corpPhoneSTD" name="corpPhoneSTD" class="formTxtBox_1" style="width:50px;"/>-<input name="corpOffPhoneno" type="text" maxlength="20" class="formTxtBox_1" id="txtCorpPhone"  style="width:100px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffPhoneno().split("-")[1]);
                                                }else{out.print("123-456");}%>"/>
                                                <input type="hidden" id="hdnvaluec" name="hdnvaluec" />
                                                <span id="scorpPhoneSTD" class='reqF_1'></span>
                                                <span id="stxtCorpPhone" class='reqF_1'></span>
                                                <span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span> </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="ff" height="30">Fax No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="corpFaxCode" value="<%
                                                        if (ifEdit) {
                                                            out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                                        } else {
                                                            //Change Bangladesh to Bhutan,Proshanto
                                                            out.print(commonService.countryCode("Bhutan", false));
                                                                }%>" readonly/>-<input type="text" value="<%if(ifEdit){if(dtBean.getCorpOffFaxNo().contains("-")){out.print(dtBean.getCorpOffFaxNo().split("-")[0]);}}%>" id="corpFaxSTD" name="corpFaxSTD" class="formTxtBox_1" style="width:50px;"/>-<input name="corpOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtCorpFax"  style="width:100px;"  value="<%if (ifEdit) {
                                                    if(dtBean.getCorpOffFaxNo().contains("-")){out.print(dtBean.getCorpOffFaxNo().split("-")[1]);
                                                }}else{out.print(" ");}%>"/>
                                            <span id="scorpFaxSTD" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <tr  style="display: none;">
                                            <td class="ff" height="30">Company's Website : </td>
                                            <td><input name="website" type="text" maxlength="100" class="formTxtBox_1" id="txtWeb" style="width:200px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getWebsite());
                                                }%>"/>
                                                <br/><span class="formNoteTxt" id="webnote">(Enter website name without 'http://' e.g. pppd.gov.bt)</span></td>
                                        </tr>--%>
                                        <tr>
                                            <td height="30">&nbsp;</td>
                                            <td><%--<label class="formBtn_1">
                                                    <input type="submit" name="previous" id="btnPrev" value="Previous" />
                                                </label>
                                                &nbsp;--%>
                                                <%if (ifEdit) {%>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="saveNEdit" id="btnUpdate" value="Update" />
                                                </label>
                                                <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value="Update"/>
                                                <%} else {%>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="saveNEdit" id="btnSave" value="Save"  />
                                                </label>
                                                <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value="Save"/>
                                                <%}%>
                                            </td>
                                        </tr>
                                    <%--</tbody>
                                    </td>
                                            </tr>--%>

                                </table>
                                <%} else {%>
                                <br/>
                                <div class="responseMsg successMsg">Company with the name "<%=cmpName%>" is already approved by e-GP Admin so it can't be modified.</div>
                                <%}%>
                                <span id="redirect" style="display: none;">
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">
                                        <tr>
                                            <td class="ff" width="20%">Company Name : </td>
                                            <td align="80%"><input name="cmpName" type="text" class="formTxtBox_1" id="txtCmpName" maxlength="30" style="width:200px;" readonly/>
                                        </td>
                                    </tr>
                                        <tr>
                                     <td>&nbsp;</td>
                                     <td>
                                    <label class="formBtn_1">
                                        <%--<input type="hidden" id="hdnCmp"/>--%>
                                        <span id="hdnCmp" style="display: none"></span>
                                        <input type="button" name="next" id="btnNext" value="Next"/>

                                    </label>
                                     </td>
                                        </tr>
                                  </table>
                                </span>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp"/>
            </div>
        </div>
    </body>
</html>
