<%--
    Document   : CompanyVerification
    Created on : Dec 31, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

    </head>       
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    java.util.List<Object[]> tblCompanyJointVenture = contentAdminService.findJvCompanyDetail(Integer.parseInt(request.getParameter("cId")));
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">JVCA Details</div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
                <br/><table width="100%" align="center" cellspacing="0" class="tableList_1">
                    <tr>
                        <th style="width:4%;">Sl.  No.</th>
                        <th>Company Name</th>
                        <th>Company Registration No.</th>
                        <th>Jvca Role</th>
                    </tr>
                    <%
                                int count = 1;
                                for (Object[] jvcaCompListDtBean : tblCompanyJointVenture) {
                    %>
                    <tr>
                        <td><%=count%></td>

                        <td><%=jvcaCompListDtBean[0]%></td>
                        <td><%=jvcaCompListDtBean[1]%></td>
                        <td><%if (jvcaCompListDtBean[2].equals("lead")) {%>Lead<%} else {%>Secondary<%}%></td>
                    </tr>
                    <%
                                    count++;
                                }
                    %>
                </table>
                <br/>
                <div align="center">
                <a href="TendererDocs.jsp?tId=<%=request.getParameter("tId")%>&payId=<%=request.getParameter("payId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&jv=<%=request.getParameter("jv")%>&s=<%=request.getParameter("s")%>"  class="anchorLink">Next</a>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
