<%--
    Document   : CompanyVerification
    Created on : Dec 31, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

    </head>       
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    TblCompanyMaster tblCompanyMaster = contentAdminService.findCompanyMaster("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId"))).get(0);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">Bidder/Consultant Company Details</div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">
                    <tr>
                        <td width="18%%" class="ff">Company Registration No. : </td>
                        <td width="82%%"><%=tblCompanyMaster.getCompanyRegNumber()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Company Name : </td>
                        <td><%=tblCompanyMaster.getCompanyName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Company Name in Bangla : </td>
                        <td><%if(tblCompanyMaster.getCompanyNameInBangla()!=null){out.print(BanglaNameUtils.getUTFString(tblCompanyMaster.getCompanyNameInBangla()));}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Company's Legal Status : </td>
                        <td>
                            <%if(tblCompanyMaster.getLegalStatus().equals("public")){%>Public Ltd.<%}%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("private")){%>Private Ltd.<%}%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("partnership")){%>Partnership.<%}%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("proprietor")){%>Proprietor.<%}%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("government")){%>Government Undertaking.<%}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Company's<br />
                            Establishment Year : </td>
                        <td><%=tblCompanyMaster.getEstablishmentYear()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Trade License Issue Date : </td>
                        <td><%if(tblCompanyMaster.getLicIssueDate()!=null){out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicIssueDate()));}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Trade License Expiry Date : </td>
                        <td><%if(tblCompanyMaster.getLicExpiryDate()!=null){out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicExpiryDate()));}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Tax Payment Number : </td>
                        <td>                            
                            <%if(tblCompanyMaster.getTinNo().equalsIgnoreCase("other")){out.print(tblCompanyMaster.getTinDocName().substring(0, tblCompanyMaster.getTinDocName().indexOf("$"))+"<div class=\"formNoteTxt\">(Other No)</div>"+tblCompanyMaster.getTinDocName().substring(tblCompanyMaster.getTinDocName().indexOf("$")+1,tblCompanyMaster.getTinDocName().length())+"<div class=\"formNoteTxt\">(Description)</div>");}
                            else{
                                if(tblCompanyMaster.getTinDocName()!= null)
                                out.print(tblCompanyMaster.getTinDocName());}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Nature of Business : </td>
                        <td><%=tblCompanyMaster.getSpecialization()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registered Address : </td>
                        <td><%=tblCompanyMaster.getRegOffAddress()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getRegOffCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblCompanyMaster.getRegOffState()%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%=tblCompanyMaster.getRegOffCity()%></td>
                    </tr>
                    <tr id="trRthana">
                        <td class="ff">Gewog : </td>
                        <td><%=tblCompanyMaster.getRegOffUpjilla()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code / Zip Code : </td>
                        <td><%=tblCompanyMaster.getRegOffPostcode()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No. : </td>
                        <td><%if(!tblCompanyMaster.getRegOffPhoneNo().equals("")){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getRegOffPhoneNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Fax No. : </td>
                        <td><%if(!tblCompanyMaster.getRegOffFaxNo().equals("")){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getRegOffFaxNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Corporate / Head office<br />
                            Address : </td>
                        <td><%=tblCompanyMaster.getCorpOffAddress()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getCorpOffCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblCompanyMaster.getCorpOffState()%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%=tblCompanyMaster.getCorpOffCity()%></td>
                    </tr>
                    <tr id="trCthana">
                        <td class="ff">Gewog : </td>
                        <td><%=tblCompanyMaster.getCorpOffUpjilla()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code / Zip Code : </td>
                        <td><%=tblCompanyMaster.getCorpOffPostcode()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No : </td>
                        <td><%if(!tblCompanyMaster.getCorpOffPhoneno().equals("")){out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(),false)+"-"+tblCompanyMaster.getCorpOffPhoneno());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Fax No : </td>
                        <td><%if(!tblCompanyMaster.getCorpOffFaxNo().equals("")){out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(),false)+"-"+tblCompanyMaster.getCorpOffFaxNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Company's Website : </td>
                        <td><%=tblCompanyMaster.getWebsite()%></td>
                    </tr>                    
                </table>
                <table border="0" cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td width="18%">&nbsp;</td>
                            <td width="82%" align="left">
                                <a href="TendererDetails.jsp?s=<%=request.getParameter("s")%>&payId=<%=request.getParameter("payId")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&jv=<%=request.getParameter("jv")%>"  class="anchorLink">Next</a>
                            </td>
                        </tr>
                    </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
