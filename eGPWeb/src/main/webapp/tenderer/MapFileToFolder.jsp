<%-- 
    Document   : MapFileToFolder
    Created on : Nov 24, 2010, 11:46:38 AM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
  <%
        int userId=0;
        int tendererId=0;
        int comDocId=0;
        if(session.getAttribute("userId") != null){
            userId=Integer.parseInt(session.getAttribute("userId").toString());
        }
        if(request.getParameter("docId") != null){
            comDocId = Integer.parseInt(request.getParameter("docId"));
        }
        List<SPCommonSearchDataMore> folderDetails = briefcaseDoc.getFolderDetails(userId);
       
       // tendererId = briefcaseDoc.getTendererIdFromUserId(userId);
       
%>
        <form  method="post" id="mapFileToFolder" action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean">
            <input type="hidden" name="work" id="work" value="mapFolder" />
            <input type="hidden" name="comDocId" id="comDocId" value="<%=comDocId%>" />
            
                <div class="pageHead_1">Folder Management</div>
                <div>&nbsp;</div>
                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                    <tr>
                        <td>&nbsp</td>
                        <td colspan="4" align="right"></td>
                    </tr>
                    <tr>
                        <td width="30%">Select A folder</td>
    	 		<td width="70%">
                            <select name = "folderId" id = "folderId" style="width:150px" >
                            <%for(int i=0;i<folderDetails.size();i++){%>
                            <option value="<%=folderDetails.get(i).getFieldName1()%>" ><%=folderDetails.get(i).getFieldName2()%> </option>
                            <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp
                            <input type="hidden" id="hidComDocID" name="hidComDocID" value="<%=comDocId%>" />
                        </td>
                        <td width="15%" align="left"><label class="formBtn_1"><input type="button" name="button" id="button" value="Map" onclick="mapFolder();" /></label></td>
                    </tr>
                </table>
        </form>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
<form id="form2" method="post">
</form>
<script language="javascript">
    
    function mapFolder(){
        var folderId = 0;
        folderId = document.getElementById("folderId").value;
        
        $.ajax({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=mapFolder&comDocId=<%=comDocId%>&folderId="+folderId,
                method: 'POST',
                async: false,
                success: function(j) {
                    jAlert("Document Mapped."," Map Document ", function(RetVal) {
                    });
                }
        });
        //document.getElementById("form2").submit();
        window.opener.forFolderWiseFile();
        window.close();
    }
</script>
