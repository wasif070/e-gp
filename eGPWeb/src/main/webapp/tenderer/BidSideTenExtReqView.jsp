<%-- 
    Document   : BidSideTenExtReqView
    Created on : Dec 2, 2010, 3:29:54 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender / Proposal validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

         
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Validity Extension Request
            <span class="c-alignment-right"><a href="BidSideTenExtReqList.jsp?tenderId=<%=request.getParameter("tenderId") %>"
                                                   class="action-button-goback">Go Back to Dashboard</a></span>
            </div>
            <div class="mainDiv">
                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>
            <div>&nbsp;</div>
            <form id="frmhope" action="TenExtLastDateAccep.jsp?tenderId=<%=request.getParameter("tenderId") %>&ExtId=<%=request.getParameter("ExtId") %>" method="post">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), null);
                            if (!list.isEmpty()) {
                %>
                    <tr>
                        <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : </td>
                        <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                    </tr>

                    <tr>
                        <td class="t-align-left ff">Last Date  of Tender Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New  Date of Tender Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName3()%></td>
                    </tr>
                    <% TenderSrBean tenderSrBeanForValSecTen = new TenderSrBean();
                    List<Object[]> objForChkingValSec = tenderSrBeanForValSecTen.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[2].toString().equalsIgnoreCase("yes")){%>
                    <tr>
                        <td class="t-align-left ff">Last Date of Tender Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName4()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New Date of Tender Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName5()%></td>
                    </tr>
                    <% } %>
                   <%-- <tr>
                        <td class="t-align-left ff">Last Date for Acceptance of Tender Validity Extension Request : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName9()%>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="t-align-left ff">Extension Reason :  </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName8()%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Status :  </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName6()%>
                        </td>
                    </tr>
                    <%}%>
                </table>
              
            </form>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
