<%-- 
    Document   : EvalBidderClari
    Created on : Dec 31, 2010, 11:38:21 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TendererTabServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clarification sought by TEC/PEC</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
<body>



      <%
      BidderClarificationSrBean srBean = new BidderClarificationSrBean();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      String tenderId="", userId="";
      String strComments="", lastDt="";
        if (request.getParameter("tenderId")!=null){
            tenderId=request.getParameter("tenderId");
        }

       if (session.getAttribute("userId") != null) {
            userId = session.getAttribute("userId").toString();
      }

      boolean isReplied=false, canReply=false, isClarificationCompleted=false;
      boolean chkSuccess = false;
      
      List<SPTenderCommonData> lstCondition = srBean.getBidderClarificationInfo(tenderId, userId);

      if (!lstCondition.isEmpty() && lstCondition.size()>0) {
          if (!"null".equalsIgnoreCase(lstCondition.get(0).getFieldName1())){
              if (Integer.parseInt(lstCondition.get(0).getFieldName1())>0){
                canReply=true;
              }
          }

          if ("yes".equalsIgnoreCase(lstCondition.get(0).getFieldName2())){
              isClarificationCompleted=true;
          }

           if ("yes".equalsIgnoreCase(lstCondition.get(0).getFieldName3())){
              isReplied=true;
          }

          strComments=lstCondition.get(0).getFieldName4();
          lastDt=lstCondition.get(0).getFieldName5();
      }

      if (request.getParameter("btnSubmit") != null){
          String strRefNo = request.getParameter("hdnTenderRefNo");
          srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        chkSuccess = srBean.updConfirmation(tenderId, userId, strRefNo);
         if (chkSuccess){
            response.sendRedirect("EvalBidderClari.jsp?tenderId="+ tenderId + "&msgId=submitted");
         } else {
            response.sendRedirect("EvalBidderClari.jsp?tenderId="+ tenderId + "&msgId=error");
         }
      }

     EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
     int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
      
     %>

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%
                String referer = "";
                    if (request.getHeader("referer") != null) {
                        referer = request.getHeader("referer");
                    }
                %>
  <div class="contentArea_1">
      <div class="pageHead_1">Clarification sought by TEC/PEC
<!--          <span style="float:right;">
              <a href="<%//=referer%>" class="action-button-goback">Go back</a>
          </span>-->
      </div>

      <%
           // Variable tenderId is defined by u on ur current page.
           pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
      %>
      <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../tenderer/TendererTabPanel.jsp" %>
      
      <%if(!is_debared){%>
      

  <div class="tabPanelArea_1">
   <%
    
    if (request.getParameter("msgId")!=null){
        String msgId="", msgTxt="";
        boolean isError=false;
        msgId=request.getParameter("msgId");
        if (!msgId.equalsIgnoreCase("")){
           if(msgId.equalsIgnoreCase("success")){
               msgTxt="Clarification provided successfully.";
           }else if(msgId.equalsIgnoreCase("submitted")){
               msgTxt="Clarification completed successfully.";
           } else if(msgId.equalsIgnoreCase("error")){
               isError=true; msgTxt="There was some error.";
           }  else {
               msgTxt="";
           }
        %>
       <%if (isError){%>
            <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
       <%} else {%>
            <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
       <%}%>
    <%}}%>
<%
            TendererTabServiceImpl tenTabSer = (TendererTabServiceImpl)AppContext.getSpringBean("TendererTabServiceImpl");
            boolean b_isOpeningDate = tenTabSer.isOpeningDateTime(Integer.parseInt(tenderId));
            if(b_isOpeningDate){

%>
<div class="responseMsg noticeMsg" style="margin-bottom: 12px;">Opening date and time not yet lapsed</div>
<%}else{%>
   <%-- Start: Sub Panel --%>
    <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="1" />
        <jsp:param name="tenderId" value="<%=tenderId%>" />
     </jsp:include>
   <%-- End: Sub Panel --%>
   
      <table width="100%" cellspacing="0" class="tableList_1">
          <tr>
              <th colspan="2" class="t-align-left">Tender/Proposal Details</th>
          </tr>
          <%
            List<SPTenderCommonData> PackageList = srBean.getLotOrPackageData(tenderId, "Package,1");

            if (!PackageList.isEmpty() && PackageList.size()>0) {
          %>
          <tr>
              <td width="16%" class="t-align-left ff">Package  No. :</td>
              <td width="84%" class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
          </tr>
          <tr>
              <td class="t-align-left ff">Package Description :</td>
              <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
          </tr>
          <%}%>
      </table>

       <table width="100%" cellspacing="0" class="tableList_1">
<!--          <tr>
              <th colspan="2" class="t-align-left">Tender Details</th>
          </tr>-->
          <tr>
              <td width="16%" class="t-align-left ff"> Last Date and Time for Response :</td>
              <td width="84%" class="t-align-left"><%=lastDt%></td>
          </tr>
          <tr>
              <td width="16%" class="t-align-left ff">Remarks :</td>
              <td width="84%" class="t-align-left"><%=strComments%></td>
          </tr>
      </table>

      <% 
        if (isTenPackageWise){           
        // SHOW FORMS PACKAGE WISE
      %>
      <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
          <tr>
              <th width="4%" class="ff">Sl. No.</th>
              <th width="76%" class="ff">Form Name</th>
              <th width="20%">Action</th>
             <%-- <%if(canReply && !isClarificationCompleted){%><th width="15%">Documents</th><%}--%>
          </tr>
          <%
            int frmCnt=0;
           //List<SPTenderCommonData> formList = tenderCommonServiceFrm.returndata("getformnamebytenderidandlotid",thisTenderId,"0");
            List<SPTenderCommonData> formList = srBean.getLotOrPackageFormsForQuestions(tenderId, "0", userId, evalCount);
                for (SPTenderCommonData formname : formList) {
                    frmCnt++;
                %>
                <tr>
                    <td class="t-align-center"><%=frmCnt%></td>
                    <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
                    </td>
                    <td class="t-align-center">
                        <a href="EvalFormQuestions.jsp?tenderId=<%=tenderId%>&lotId=0&formId=<%=formname.getFieldName2()%>">
                           <%if(canReply && !isClarificationCompleted){%>
                                       <%if("Yes".equalsIgnoreCase(formname.getFieldName3())) { %>
                                        Clarifications prepared
                                    <%} else {%>
                                        Prepare Clarification
                                    <%}%>
                                    <%} else {%>
                                        View
                                    <%}%>
                        </a>
                        <input type="hidden" name="hdnFormId_<%=frmCnt%>" value="<%=formname.getFieldName2()%>"></input>
                    </td>
                    <%--<%if(canReply && !isClarificationCompleted){%>
                    <td class="t-align-center"><a href="../resources/common/evalClariDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName2()%>&pId=bid">Upload Document</a></td>
                    <%}%>--%>
                </tr>
                <%}%>
            <%if (frmCnt==0) {%>
                 <tr>
                    <td class="t-align-left" <%if(canReply && !isClarificationCompleted){%> colspan="4"<%}else{%>colspan="3"<%}%>>No forms found!</td>
                </tr>
            <%}%>
    </table>
    <% } else {
      // SHOW FORMS LOT WISE                
      %>
      <div>&nbsp;</div>
       <table width="100%" cellspacing="0" class="tableList_1">          
          <%
            List<SPTenderCommonData> LotList = srBean.getTenderLots(tenderId, userId);

            if (!LotList.isEmpty() && LotList.size()>0) {
                for (SPTenderCommonData lotname : LotList) {
          %>
          <tr>
              <td width="12%" class="t-align-left ff">Lot No. :</td>
              <td width="88%" class="t-align-left"><%=lotname.getFieldName1()%></td>
          </tr>
          <tr>
              <td class="t-align-left ff">Lot Description :</td>
              <td class="t-align-left"><%=lotname.getFieldName2()%></td>
          </tr>
          <tr>
              <td colspan="2">
                  <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                      <tr>
                          <th width="5%" class="ff">Sl. No.</th>
                          <th width="50%" class="ff">Form Name</th>
                          <th width="15%">Action</th>
                          <%-- <%if(canReply && !isClarificationCompleted){%><th width="15%">Documents</th><%}%>--%>
                      </tr>
                <%
                    int frmCnt=0;
                    List<SPTenderCommonData> formList = srBean.getLotOrPackageFormsForQuestions(tenderId, lotname.getFieldName3(), userId,evalCount);
                    if (!formList.isEmpty()) {%>
                         <%for (SPTenderCommonData formname : formList) {
                            frmCnt++;%>
                            <tr>
                                <td class="t-align-center"><%=frmCnt%></td>
                                <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
                                </td>
                                <td class="t-align-center">
                                    <a href="EvalFormQuestions.jsp?tenderId=<%=tenderId%>&lotId=<%=lotname.getFieldName3()%>&formId=<%=formname.getFieldName2()%>">
                                    <%if(canReply && !isClarificationCompleted){%>
                                    <%if("Yes".equalsIgnoreCase(formname.getFieldName3())) { %>
                                        Clarifications prepared
                                    <%} else {%>
                                        Prepare Clarification
                                    <%}%>

                                    <%} else {%>
                                        Clarifications prepared
                                    <%}%>

                                    </a>
                                    <input type="hidden" name="hdnFormId_<%=frmCnt%>" value="<%=formname.getFieldName2()%>"></input>
                                </td>
                               <%-- <%if(canReply && !isClarificationCompleted){%>
                                <td><a href="../resources/common/evalClariDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName2()%>&pId=bid">Upload Document</a></td>
                                <%}%>--%>
                            </tr>    
                        <%}%>                         
                    <%}%>
                    <%if (frmCnt==0) {%>
                             <tr>
                                <td class="t-align-left" <%if(canReply && !isClarificationCompleted){%> colspan="4"<%}else{%>colspan="3"<%}%>>No forms found!</td>
                            </tr>
                        <%}%>
                 </table>
                 <div>&nbsp;</div>
              </td>
          </tr>
          <%}%>
          <%}%>
      </table>
            
      <%}%>

      <%if (isReplied && canReply && !isClarificationCompleted) {%>
      <form id="frmBidderClarification" action="EvalBidderClari.jsp?tenderId=<%=tenderId%>" method="POST">
          <table width= "100%" class="t_space">
              <tr>
                  <td class="t-align-center">
                      <span class="reqF_1 b_space">Please click on Notify TEC Chairperson button once all the clarifications are prepared</span>
                      <label class="formBtn_1">
                          <input name="btnSubmit" id="btnSubmit" type="submit" value="Notify TEC Chairperson"  />
                          </label>
                  </td>
              </tr>
          </table>
          <input type="hidden" id="hdnTenderRefNo" name="hdnTenderRefNo" value="<%=toextTenderRefNo%>"></input>
      </form>
      <%}%>
  </div>
  <%}/*Condtion for is Time is not lapsed*/%>
  <%}/*Not Debared Condtion ends here*/%>
    <div>&nbsp;</div>
    </div>
    <!--Dashboard Content Part End-->

    <!--Dashboard Footer Start-->
   <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    <!--Dashboard Footer End-->
</div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
