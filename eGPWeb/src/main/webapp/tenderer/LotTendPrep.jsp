<%--
    Document   : LotTendPrep
    Created on : Nov 20, 2010, 12:16:06 PM
    Author     : Administrator
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblBidderLots"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%--<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<jsp:useBean id="tenderSrBean1" class="com.cptu.egp.eps.web.servicebean.TenderSrBean" scope="page" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender preparation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/Tooltip.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script>
            function checkValidate(){
                var check = false;
                if((document.getElementById("chkEmailOTP").checked || document.getElementById("chkSMSOTP").checked) && document.getElementById("chkValidate").checked)
                {
                    check = true;
                    if(document.getElementById("chkEmailOTP").checked)
                    {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {jspOtpOption: 'email',funName: 'otpOptionSet'},
                        function (j)
                        {});
                    }
                    else if(document.getElementById("chkSMSOTP").checked)
                    {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {jspOtpOption: 'sms',funName: 'otpOptionSet'},
                        function (j)
                        {});
                    }
                    
                    
                } 
                else 
                {
                    if(!document.getElementById("chkValidate").checked)
                    {
                        jAlert("Please Agree on Terms and Conditions. Select 'I Agree'.", "Confirmation Alert");
                    }
                    else
                    {
                        jAlert("Please Select OTP option.", "Confirmation Alert");
                    }
                    check = false;
                }
             return check;
            }
           // Dohatec Start
           function finalSubmissionCheck(value,lotNo)
           {
               var importLink = "td.imported"+lotNo+" a";
               var manufactureLink = "td.manufactured"+lotNo+" a";
               
               if(value == 'Imported'){

                    if(checkManufactured('Fill',lotNo) && !checkImported('Fill',lotNo))
                    {
                        $('#tblFinalSub').hide();
                        //$('#dvFinalSubMsg').text("You have already filled '" + $("td.manufacturedFormName").text().replace("*","").replace("  "," ").trim() + "' form. Please delete it first");
                        $('#dvFinalSubMsg').text("You have already filled '" + "Price and Delivery Schedule for Goods (Form e-LG-4b)" + "' form. Please delete it first");
                        $('#tblFinalSubMsg').show();
                        //jAlert("You have already filled '" + $("td.manufacturedFormName").text().replace("*","").replace("  "," ").trim() + "' form. Please delete it first");
                        jAlert("You have already filled '" + "Price and Delivery Schedule for Goods (e-LG-4b)" + "' form. Please delete it first");
                        $(importLink).css("display","none");

                    }
                    else
                    {
                        if(checkImported('Final',lotNo))
                        {
                            $('#tblFinalSub').show();
                            $('#tblFinalSubMsg').hide();
                        }
                        else
                        {
                            $('#tblFinalSub').hide();
                            $('#dvFinalSubMsg').text('Please fill Mandatory forms and Map Supporting / Reference Documents as requested');
                            $('#tblFinalSubMsg').show();
                        }
                    }
               }
               else if(value == 'Manufactured'){

                    if(checkImported('Fill',lotNo) && !checkManufactured('Fill',lotNo))
                    {

                        $('#tblFinalSub').hide();
                        //$('#dvFinalSubMsg').text("You have already filled '" + $("td.importedFormName").text().replace("*","").replace("  "," ").trim() + "' form. Please delete it first");
                        $('#dvFinalSubMsg').text("You have already filled '" + "Price and Delivery Schedule for Goods (Form e-LG-4a)" + "' form. Please delete it first");
                        $('#tblFinalSubMsg').show();
                        //jAlert("You have already filled '" + $("td.importedFormName").text().replace("*","").replace("  "," ").trim() + "' form. Please delete it first");
                        jAlert("You have already filled '" + "Price and Delivery Schedule for Goods (Form e-LG-4a)" + "' form. Please delete it first");
                        $(manufactureLink).css("display","none");
                    }
                    else
                    {
                        if(checkManufactured('Final',lotNo))
                        {
                            $('#tblFinalSub').show();
                            $('#tblFinalSubMsg').hide();
                        }
                        else
                        {
                            $('#tblFinalSub').hide();
                            $('#dvFinalSubMsg').text('Please fill Mandatory forms and Map Supporting / Reference Documents as requested');
                            $('#tblFinalSubMsg').show();
                        }
                    }
               }
               else { // Value = 'Select'
                    if($('#isFinalSubValue').val() == '1')
                    {
                        $('#tblFinalSub').hide();
                        $('#dvFinalSubMsg').text('Please select an option from Tender Group');
                        $('#tblFinalSubMsg').show();
                    }
               }
           }

           /*function checkManufactured()
           {
                var splitText = $("td.manufactured").text().replace(' ','').split('|');
                if(((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1)) && ($('#isFinalSubValue').val() == '1'|| (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1) || ((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1))))
                {
                    return true;
                }
                return false;
           }

           function checkImported()
           {
                var splitText = $("td.imported").text().replace(' ','').split('|');
                if(((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1)) && ($('#isFinalSubValue').val() == '1'|| (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1) || ((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1))))
                {
                    return true;
                }
                return false;
           }*/
           
           function checkManufactured(flag,lotNo)
           {
                var count = 0;
                var attr = "td.manufactured" + lotNo;
                $(attr).each(function(){
                    var splitText = $(this).text().replace(' ','').split('|');
                    if(((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1)) && ($('#isFinalSubValue').val() == '1'|| (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1) || ((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1))))
                    {
                        count++;
                    }
                });
                //return flag;
                
                if(flag == 'Fill' && count > 0)
                    return true;
                else if(flag == 'Fill' && count == 0)
                    return false;
                else if(flag == 'Final' && $(attr).length == count)
                    return true;
                else if(flag == 'Final' && $(attr).length > count)
                    return false;
           }

           function checkImported(flag, lotNo)
           {
                var count = 0;
                var attr = "td.imported"+lotNo;
                $(attr).each(function(){
                    var splitText = $(this).text().replace(' ','').split('|');
                    if(((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1)) && ($('#isFinalSubValue').val() == '1'|| (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1) || ((splitText[2] != undefined && splitText[2].replace(' ','').indexOf('Encrypted') != -1) || (splitText[3] != undefined && splitText[3].replace(' ','').indexOf('Encrypt') != -1))))
                    {
                        count++;
                    }
                });
                //return flag;

                if(flag == 'Fill' && count > 0)
                    return true;
                else if(flag == 'Fill' && count == 0)
                    return false;
                else if(flag == 'Final' && $(attr).length == count)
                    return true;
                else if(flag == 'Final' && $(attr).length > count)
                    return false;
           }
            
           //Dohatec End
        </script>
    </head>
    <body>
       <%
       if (request.getParameter("btnFinalSub") != null)
       {
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            String idType="tenderId";
            int auditId=Integer.parseInt(request.getParameter("tenderId"));
            String auditAction="Bidder has Agreed and go to final submission";
            String moduleName=EgpModule.Bid_Submission.getName();
            String remarks="Bidder(User Id): "+session.getAttribute("userId") +" has  Agreed and go to final submission of Tender id: "+auditId;
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
            response.sendRedirect("FinalSubmission.jsp?tenderId="+request.getParameter("tenderId"));
       }else{
           String pkgLotId = "";
       %>
       <form id="mainForm" name="mainForm" method="post">
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                <div class="pageHead_1">Lot Wise Tender Preparation</div>

                    <div class="mainDiv">
                <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                <div>&nbsp;</div>

                <%
                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    boolean isSubDt = false;
                    String tId = null;
                    if (request.getParameter("tenderId") != null){
                        tId = request.getParameter("tenderId");
                    }

                    List<SPCommonSearchData> submitDateSts= commonSearchService.searchData("chkSubmissionDt", tId, null, null, null, null, null, null, null, null);
                    if(submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true")){
                        isSubDt = true;
                    }else{
                        isSubDt=false;
                         String msgbidmod = "";
                            if("with".equals(request.getParameter("msg"))) {
                                    msgbidmod="&msg=with";
                            }
                            if("mod".equals(request.getParameter("msg"))) {
                                    msgbidmod="&msg=mod";
                            }
                            //response.sendRedirect("SubmissionReceipt.jsp?tenderId="+request.getParameter("tenderId")+msgbidmod);
                                %>
                                            <script>
                                                document.getElementById("mainForm").setAttribute("action","SubmissionReceipt.jsp?tenderId=<%=request.getParameter("tenderId")%><%=msgbidmod%>")
                                                document.getElementById("mainForm").submit();
                                             </script>
                               <%
                    }

                    String tenDocFeesMode="";
                     boolean isTenDocPaid = true;
                    List<SPTenderCommonData> lstCurrTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tId, null);
                    if (!lstCurrTenderInfo.isEmpty() && lstCurrTenderInfo.size()>0){
                        tenDocFeesMode=lstCurrTenderInfo.get(0).getFieldName2();

                        /*
                        if (tenDocFeesMode.equalsIgnoreCase("offline")){
                            isTenDocPaid= true;
                        } else if (tenDocFeesMode.equalsIgnoreCase("bank")){
                            isTenDocPaid = true;
                        } else if (tenDocFeesMode.equalsIgnoreCase("offline/bank")) {
                            isTenDocPaid = true;
                        } else {
                            isTenDocPaid = false;
                        }
                         */

                        if ("Free".equalsIgnoreCase(lstCurrTenderInfo.get(0).getFieldName7())) {
                            isTenDocPaid = false;
                        } else if ("Paid".equalsIgnoreCase(lstCurrTenderInfo.get(0).getFieldName7())) {
                            isTenDocPaid = true;
                    }
                    }
                %>


                <% pageContext.setAttribute("tab", "4");%>
                        <%@include file="TendererTabPanel.jsp" %>
                     <%if(!is_debared){%>
                    <div class="tabPanelArea_1 ">
                        <%
                            //Message Added by TaherT
                            if("with".equals(request.getParameter("msg"))){
                                out.print("<br/><div class='responseMsg successMsg'>You have withdrawn your submission successfully</div>");
                            }
                            if("mod".equals(request.getParameter("msg"))){
                                out.print("<br/><div class='responseMsg successMsg'>Reason entered successfully. Please proceed for modification</div>");
                            }
                        %>
                        <%
                        List<TblBidderLots> listbidder  = new ArrayList<TblBidderLots>();
                                List<SPTenderCommonData> listView = tenderCommonService.returndata("getLotDescriptonForDocViewMore", tId, session.getAttribute("userId").toString());
                                listbidder = tenderSrBean1.viewBidderLots(Integer.parseInt(tId), Integer.parseInt(session.getAttribute("userId").toString()));
                                if(listView.size()>1){
                                if(!listbidder.isEmpty()){
                                List<SPTenderCommonData> setLink =  tenderCommonService.returndata("getBidderLotSelectionLinkStatus", tId, session.getAttribute("userId").toString());
                                if(setLink.get(0).getFieldName1().equalsIgnoreCase("1")){
                        %>
                       <table width="100%" cellspacing="0" class="tableList_1 t_space b_space">
                        <tr>
                            <td width="15%" valign="middle" class="t-align-left ff">Lot Selection : </td>
                            <td width="85%" valign="middle" class="t-align-left"><a href="LotSelection.jsp?tab=4&tenderId=<%=tId%>">Edit Lot Selection</a></td>
                        </tr>
                       </table>
                        <%}/*Codition If tender is not archived and finalsubmission is not over and bid widrwal is not over*/}/*If iist is not null*/}/*ListView Size is Greater then 1*/%>
                <%
                        String tenderId = null;
                        String loggedUserId="";
                        if (session.getAttribute("userId") != null) {
                            loggedUserId = session.getAttribute("userId").toString();
                        }

                        int paidCnt=0;
                        tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        if (request.getParameter("tenderId") != null){
                            tenderId = request.getParameter("tenderId");
                            //List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("gettenderlotbytenderid", tenderId,null);

                            // IF SUBMISSION DATE OVER REDIRECT TO RECEIPT PAGE

                          if (isSubDt) {

                        if (isTenPackageWise) {
                            //paidCnt=1;
                            List<SPTenderCommonData> paidPackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, loggedUserId);
                            if (!paidPackageList.isEmpty()) {
                                 if (!paidPackageList.get(0).getFieldName3().equalsIgnoreCase("0") && !paidPackageList.get(0).getFieldName3().equalsIgnoreCase("N.A.")){
                                    paidCnt++;
                                 }
                            }

                            List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, loggedUserId, null, null, null, null, null, null, null);
                            if (!btnFinallst.isEmpty()) {
                                if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("modify") || btnFinallst.get(0).getFieldName1().equalsIgnoreCase("Pending")  || btnFinallst.get(0).getFieldName1()== null){
                    %>
                                    <jsp:include page="BidPreperation.jsp" ></jsp:include>
                    <%
                                }else{

                                    if(request.getParameter("msg")!=null && "delerror".equalsIgnoreCase(request.getParameter("msg"))){
                                        //response.sendRedirect("SubmissionReceipt.jsp?tenderId="+tenderId+"&msg=delerror");
                                        %>
                                            <script>
                                                document.getElementById("mainForm").setAttribute("action","SubmissionReceipt.jsp?tenderId=<%=tenderId%>&msg=delerror")
                                                document.getElementById("mainForm").submit();
                                             </script>
                                       <%
                                    } else{
                                        %>
                                            <script>
                                                document.getElementById("mainForm").setAttribute("action","SubmissionReceipt.jsp?tenderId=<%=tenderId%>")
                                                document.getElementById("mainForm").submit();
                                             </script>
                                       <%
                                        //response.sendRedirect("SubmissionReceipt.jsp?tenderId="+tenderId);
                                    }
                                    // IN CASE OF BID WIDTHDrawal%>

                               <% }
                            }else{
                    %>
                                    <jsp:include page="BidPreperation.jsp" ></jsp:include>
                    <%
                            }
                        } else {
                            boolean dispLotprep = false;
                            List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, loggedUserId, null, null, null, null, null, null, null);
                            if (!btnFinallst.isEmpty()) {
                                if ("finalsubmission".equalsIgnoreCase(btnFinallst.get(0).getFieldName1())){
                                    if(request.getParameter("msg")!=null && "delerror".equalsIgnoreCase(request.getParameter("msg"))){
                                        //response.sendRedirect("SubmissionReceipt.jsp?tenderId="+tenderId+"&msg=delerror");
                                        %>
                                            <script>
                                                document.getElementById("mainForm").setAttribute("action","SubmissionReceipt.jsp?tenderId=<%=tenderId%>&msg=delerror")
                                                document.getElementById("mainForm").submit();
                                             </script>
                                       <%
                                    } else{
                                        //response.sendRedirect("SubmissionReceipt.jsp?tenderId="+tenderId);
                                        %>
                                            <script>
                                                document.getElementById("mainForm").setAttribute("action","SubmissionReceipt.jsp?tenderId=<%=tenderId%>")
                                                document.getElementById("mainForm").submit();
                                             </script>
                                       <%
                                    }
                                }else{
                                    dispLotprep=true;
                                }
                             }else{
                                dispLotprep=true;
                             }
                             if(dispLotprep){
                            // List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getLotDescriptonForDocView", tenderId,loggedUserId);
                                 List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getLotDescriptonForDocViewMore", tenderId,loggedUserId);
                            if (!tenderLotList.isEmpty()) {
                                    //if (tenderLotList.get(0).getFieldName1().equalsIgnoreCase("Package")) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="6%" valign="middle" class="t-align-center">Lot. No.</th>
                            <th width="65%" valign="middle" class="t-align-center">Lot Description</th>
                            <th width="14%" valign="middle" class="t-align-center">Tender Preparation</th>
<!--                            <th width="15%" class="t-align-left">Submission Detail</th>-->
                        </tr>
                        <%
                            int i = 1;
                            for (SPTenderCommonData tenderLot : tenderLotList) {
                                if(Math.IEEEremainder(i,2)==0) {
                        %>
                        <tr style="background-color: #e9f9db;">
                        <% } else { %>
                        <tr>
                        <% } %>
                            <td class="t-align-center"><%=tenderLot.getFieldName1()%></td>
                            <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
                            <td class="t-align-center">

                        <%
                                if (!isTenDocPaid ||(!tenderLot.getFieldName4().equalsIgnoreCase("0") && !tenderLot.getFieldName4().equalsIgnoreCase("N.A."))){
                                    if(listbidder!=null && !listbidder.isEmpty()){
                                        String s_selected = "Lot not Selected";
                                        for(TblBidderLots tblBidderLots : listbidder){
                                            if(tenderLot.getFieldName3().equalsIgnoreCase(tblBidderLots.getPkgLotId()+"")){
                                                s_selected = "<a href='BidPreperation.jsp?tab="+request.getParameter("tab")+"&lotId="+tenderLot.getFieldName3()+"&tenderId="+tenderId+"'>Prepare</a>";
                                                paidCnt++;
                                                                                    }/*If Selected Lot*/
                                                                              }/*For loop for tenderLLotList ends here*/
                                        out.print(s_selected);
                                    }/*TenderList is not null and not empty*/
                                } else {
                        %>
                                    Payment Pending
                        <%
                                }
                        %>
                            </td>
                            <%--<td class="t-align-left">
                            <%if (!tenderLot.getFieldName4().equalsIgnoreCase("0") && !tenderLot.getFieldName4().equalsIgnoreCase("N.A.")){%>
                                <a href="#">View</a>
                                <%} else {%>
                                   //Payment Pending
                                    N.A.
                                 <%}%>
                            </td>--%>
                        </tr>
                        <%
                                i++;
                            } // END FOR LOOP
                        %>
                        </table>
                        <%
                        } else {
                        %>
                        <tr>
                            <td class="t-align-left" colspan="4"><B>No lots available for this tender!</B></td>
                        </tr>
                    <%
                        }
                    }}
                            }
                }
                if (paidCnt>0 || !isTenDocPaid) {
                    if (isSubDt) {
                        List<SPCommonSearchData> finalSubCond = commonSearchService.searchData("ChkTodoFinalSubmission", tenderId, session.getAttribute("userId").toString() , null, null, null, null, null, null, null);
                            if(!finalSubCond.isEmpty()){
                               if("1".equals(finalSubCond.get(0).getFieldName1())){
                    %>
                    <!--Dohatec Start-->
                        <input type="hidden" id="isFinalSubValue" value="<%=finalSubCond.get(0).getFieldName1()%>"/>
                    <!--Dohatec End-->
                    <form action="" id="frmTenderLotPrep" method="POST">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblFinalSub">
                            <tr>
                                <td class="t-align-center">
                                    <br/>
                                    <b>Send OTP via</b>
                                    <input type="checkbox" name="chkSMSOTP" id ="chkSMSOTP" value="ON" onclick="uncheckEmail();"/>
                                    <b>SMS or </b>
                                    <input type="checkbox" name="chkEmailOTP" id ="chkEmailOTP" value="ON" onclick="uncheckSMS();"/>
                                    <b>Email</b>
                                    <br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-center">
                                    I hereby declare that I have read and understood all the tender documents, pre tender meeting (if applicable), amendment/ corrigendum.
                                    <br /><br />
                                    <input type="checkbox" name="chkValidate" id ="chkValidate" value="ON" />
                                    I Agree
                                </td>
                            </tr>
                            <tr>
                                <td  class="t-align-center" >
                                    <label class="formBtn_1">
                                        <input name="btnFinalSub" id="btnFinalSub" type="Submit" value="Go To Final Submission" onclick="return checkValidate();"/></label>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblFinalSubMsg">
                            <td class="t-align-center">
                                   <!--
                                   <div class="responseMsg msgInRed t-align-center" id="dvMap">Please fill Mandatory forms and Map Supporting / Reference Documents as requested</div>
                                   <div class="responseMsg msgInRed t-align-center" id="dvEncrypt">Filled BOQ/Price Bid Form Not Encrypted Using Buyer's Hash.</div>
                                   <div class="responseMsg msgInRed t-align-center" id="dvTenderGroup">Please select an option from Tender Group</div>
                                   -->
                                   <div class="responseMsg msgInRed t-align-center" id="dvFinalSubMsg"></div>
                            </td>
                        </table>

                    </form>
                    <%}else{
                            if(finalSubCond.get(0).getFieldName2().equalsIgnoreCase("Tender Security Payment is Pending"))
                            {   
                                short lotCount = 1;
                                String lotPayment = "";
                                String lotDeclaration = "";
                                List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getLotDescriptonForDocViewMore", tenderId,loggedUserId);
                                for (SPTenderCommonData tenderLot : tenderLotList) {
                                
                                    if(listbidder!=null && !listbidder.isEmpty()){
                                        for(TblBidderLots tblBidderLots : listbidder){
                                            if(tenderLot.getFieldName3().equalsIgnoreCase(tblBidderLots.getPkgLotId()+"")){
                                               pkgLotId = String.valueOf(tblBidderLots.getPkgLotId());
                                                List<SPCommonSearchData> secType = commonSearchService.searchData("getSecurityPaymentType", tenderId,pkgLotId, session.getAttribute("userId").toString() , null, null, null, null, null, null);
                                               //List<SPTenderCommonData> secType = tenderCommonService.returndata("getSecurityPaymentType", tenderId,pkgLotId);
                                               if(secType!=null && !secType.isEmpty()){
                                                    if(tenderLotList.size()==1)
                                                     {
                                                          if(secType.get(0).getFieldName1().equals("1"))
                                                             out.print("<br/><div class='responseMsg msgInRed t-align-center'>Your Financial Institution Payment is Pending</div>");
                                                         else if (secType.get(0).getFieldName1().equals("2"))
                                                             out.print("<br/><div class='responseMsg msgInRed t-align-center'>Your Bid Security Decleration is Pending</div>");
                                                     }
                                                     else
                                                     {
                                                         if(secType.get(0).getFieldName1().equals("1"))
                                                            // lotPayment = lotPayment + String.valueOf(lotCount)+","; \
                                                            lotPayment = lotPayment + tenderLot.getFieldName1()+" ,";
                                                         else if (secType.get(0).getFieldName1().equals("2"))
                                                             //lotDeclaration = lotDeclaration + String.valueOf(lotCount)+",";
                                                             lotDeclaration = lotDeclaration + tenderLot.getFieldName1()+" ,";
                                                         lotCount++;
                                                     }
                                                }
                                            }
                                        }
                                    }
                                }
                                if(!lotDeclaration.equals(""))
                                {
                                    lotDeclaration = lotDeclaration.substring(0, lotDeclaration.length()-1);
                                    out.print("<br/><div class='responseMsg msgInRed t-align-center'>Your Bid Security Decleration is Pending for Lot "+lotDeclaration+"</div>");
                                }   
                                if(!lotPayment.equals("")){
                                    lotPayment = lotPayment.substring(0, lotPayment.length()-1);
                                    out.print("<br/><div class='responseMsg msgInRed t-align-center'>Your Financial Institution Payment is Pending for Lot "+lotPayment+"</div>");
                                    
                                }
                            }
                            else
                                out.print("<br/><div class='responseMsg msgInRed t-align-center'>"+finalSubCond.get(0).getFieldName2()+"</div>");
                        }
                    }
                    %>
                <%
                    }
                }
                %>
                </div><%}%>
                <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
       </form>
       <%
        }
       %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        
        function uncheckEmail()
        {
          if (document.getElementById('chkSMSOTP').checked) 
          {
              document.getElementById("chkEmailOTP").checked = false;
          }
          
        }
        
        function uncheckSMS()
        {
          if (document.getElementById('chkEmailOTP').checked) 
          {
              document.getElementById("chkSMSOTP").checked = false;
          }
        }

    </script>

</html>
