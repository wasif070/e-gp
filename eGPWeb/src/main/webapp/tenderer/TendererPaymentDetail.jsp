<%--
    Document   : TendererPaymentDetail
    Created on : Jan 23, 2011, 5:47:44 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
               <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    boolean isBankUser=false;
                    String userId="", payUserId="", regPaymentId="";
                %>
                <%

                 HttpSession hs = request.getSession();
             if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
              }
             else {//response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
             }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                    
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                    
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                    
                }
                
                
                %>
                <div class="contentArea_1">
                    <br/>
                    <div class="pageHead_1">Registration Payment Details</div>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully";
                        } else  if(msgId.equalsIgnoreCase("canceled")){
                            msgTxt="Payment canceled successfully";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                         <%

                         
                            int recordsCnt=0;
                            boolean isEmailVerifed=false, isPaymentDone=false;
                            String bidderUserId="0", bidderEmail="", bidderCompany="", payStatus="", registeredDt="";

                            String cmdName="", emailId="", bankUserId="";

                                List<SPTenderCommonData> lstTendererEml =
                                        tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
                                         
                                emailId=lstTendererEml.get(0).getFieldName1();

                                cmdName = "SearchEmailForRegFee";
                                bankUserId = userId;
                                for (SPTenderCommonData sptcd : tenderCommonService.returndata(cmdName, emailId, null)) {
                                    recordsCnt++;
                                    bidderUserId=sptcd.getFieldName1();
                                    bidderEmail=sptcd.getFieldName2();
                                    bidderCompany=sptcd.getFieldName4();
                                    if("yes".equalsIgnoreCase(sptcd.getFieldName5())){
                                        isEmailVerifed=true;
                                    }
                                   // regPaymentId=sptcd.getFieldName6();
                                    //sptcd.getFieldName1();

                                    if(!"null".equalsIgnoreCase(sptcd.getFieldName7())){
                                        payStatus=sptcd.getFieldName7();
                                    } else {
                                        payStatus="Pending";
                                    }
                                    registeredDt=sptcd.getFieldName8();
                               }



                        %>

                            <%
                            
                          //  List<SPTenderCommonData> lstPaymentDetail = tenderCommonService.returndata("getRegistrationFeePaymentDetail", regPaymentId, null);

                                            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> lstPaymentDetail =null;
                lstPaymentDetail = dataMore.geteGPData("getRegistrationFeePaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
 

                                if (!lstPaymentDetail.isEmpty()) {
                    %>

                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="18%" class="ff">e-mail ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                           <tr>
                            <td class="ff">Financial Institute Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName12()%></td>
                        </tr>
                        <%if(!lstPaymentDetail.get(0).getFieldName5().equalsIgnoreCase("Online")){%>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Checker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <%} else {%>
                          <tr>
                            <td class="ff">Transaction Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName15()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Card Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName14()%></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%if(lstPaymentDetail.get(0).getFieldName3().equalsIgnoreCase("BTN")){
                                    out.print("Nu.");
                                }else{
                                    out.print(lstPaymentDetail.get(0).getFieldName3());
                                    }
                                %></td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                            </td>
                        </tr>
                        <%if(lstPaymentDetail.get(0).getFieldName5().equalsIgnoreCase("Online")){%>
                            <tr>
                                <td class="ff" >Service Charge :</td>
                                <td><label>Tk.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName16() %></td>
                            </tr>
                            <tr>
                                <td class="ff" >Total Amount :</td>
                                <td><label>Tk.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName17() %></td>
                            </tr>
                        <%}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institute :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institute Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName7()%></td>
                            </tr>

                    </table>


                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                </div>
                     <table border="0" cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td width="18%">&nbsp;</td>
                            <td width="82%" align="left">
                                <a href="TendererRegDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&s=<%=request.getParameter("s")%>&payId=<%=request.getParameter("payId")%>" class="anchorLink">Next</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <!--Dashboard Content Part End-->
                <!--Taken from RegistrationFeePaymentDetails.jsp-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
            </div>
        </div>
    </body>
</html>
