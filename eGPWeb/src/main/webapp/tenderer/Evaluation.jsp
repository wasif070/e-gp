<%-- 
    Document   : Evaluation
    Created on : Dec 25, 2010, 2:40:36 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Evaluation</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <%@include file="../resources/common/AfterLoginTop.jsp"%>
<%
    int tenderId = 0;

    if (request.getParameter("tenderId") != null) {
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }
%>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Evaluation</div>
<%
    // Variable tenderId is defined by u on ur current page.
    pageContext.setAttribute("tenderId", tenderId);
%>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <div>&nbsp;</div>
  <%@include file="TendererTabPanel.jsp" %>
  <%if(!is_debared){%>
 <div class="tabPanelArea_1">
     <jsp:include page="EvalInnerTendererTab.jsp" >
         <jsp:param name="tenderId" value="<%=tenderId%>" />
     </jsp:include>

  <div class="tabPanelArea_1">
  <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
    <tr>
      <td class="t-align-left ff">Clarification Details</td>
      <td class="t-align-left">
      	<textarea rows="5" id="textarea4" name="textarea4" class="formTxtBox_1" style="width:90%;"></textarea>
      </td>
    </tr>
    <tr>
      <td width="18%" class="t-align-left ff">Upload reference document :</td>
      <td width="82%" class="t-align-left"><a href="#">Upload</a></td>
    </tr>
    <tr>
      <td class="t-align-left ff">Uploaded documents  :</td>
      <td class="t-align-left">
      	<table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                  <th width="4%" class="t-align-left">Sl.  No.</th>
                  <th class="t-align-left" width="27%">File Name</th>
                  <th class="t-align-left" width="32%">File Description</th>
                  <th class="t-align-left" width="8%">File Size<br /> (in Kb)</th>
                  <th class="t-align-left" width="10%">Status</th>
                  <th class="t-align-left" width="19%">Action</th>
                </tr>
                <tr>
                  <td class="t-align-left">1</td>
                  <td class="t-align-left">New Microsoft Word Document3.doc</td>
                  <td class="t-align-left">test1</td>
                  <td class="t-align-left">100</td>
                  <td class="t-align-left">Approved</td>
                  <td class="t-align-left">
					<a href="#" title="Delete">Remove</a>&nbsp;|&nbsp;
                  	<a href="#" title="Cancel">Cancel</a>&nbsp;|&nbsp;
                  	<a href="#" title="Archive">Archive</a>&nbsp;|&nbsp;
					<a href="#" title="Download">Download</a></td>
                </tr>
              </table>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="t-align-center">
        <label class="formBtn_1 t_space">
          <input name="submit" type="button" value="Submit" />
          </label>
        </td>
    </tr>
    </table>
  </div>
 </div>
 <%}%>
  </div>
    <div>&nbsp;</div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
    <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
    </table>
    <!--Dashboard Footer End-->
</body>
 <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>

