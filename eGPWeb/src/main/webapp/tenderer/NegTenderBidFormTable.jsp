<%--
    Document   : BidFormTable
    Created on : Nov 18, 2010, 3:08:48 PM
    Author     : Sanjay,Dipal
    Modify on  : Dec 14 2011
    Bug id     : 4963
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderBidDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationProcssSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<div style="overflow: auto; min-width: 964px;">
    <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
    <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

<%

    ResourceBundle bdl = null;
    bdl = ResourceBundle.getBundle("properties.negotiation");
    int userId = 0;
    if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
        userId = Integer.parseInt(request.getParameter("uId"));
    }
    //out.println("<BR>user id="+userId+"<BR>");
    int userTypeId = 0;
    if(session.getAttribute("userTypeId") != null)
    {
        if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString()))
        {
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }
    }
%>
<%
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;
    int cellId = 0;
    int formId = 0;
    int negBidformId=0;
    int tenderId = 0;

    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";
    int boqId=0;
    String ContractType="";
    String columnlist="";

%>
<%
    if(request.getParameter("ContractType")!=null && !"".equalsIgnoreCase(request.getParameter("ContractType")))
    {
        ContractType = request.getParameter("ContractType");
    }
    if(request.getParameter("boqId")!=null && !"".equalsIgnoreCase(request.getParameter("boqId"))){
        boqId = Integer.parseInt(request.getParameter("boqId"));
    }
    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
     if(request.getParameter("formId")!=null && !"".equalsIgnoreCase(request.getParameter("formId"))){
        formId = Integer.parseInt(request.getParameter("formId"));
    }
    if(request.getParameter("negBidformId")!=null && !"".equalsIgnoreCase(request.getParameter("negBidformId"))){
        negBidformId = Integer.parseInt(request.getParameter("negBidformId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    String action = "";
    if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
         action = request.getParameter("action");
    }

    boolean isEdit = false;
    if("Edit".equalsIgnoreCase(action)){
        isEdit = true;
    }

    boolean isView = false;
    if("View".equalsIgnoreCase(action)){
        isView = true;
    }

      boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }
      String propKey="srvBoqId_"+boqId;
      if(boqId == 18 || boqId == 12)
      {
          if(ContractType!= null && ContractType.equalsIgnoreCase("Time based"))
          {
              propKey += "_timebase";
          }
          else
          {
              propKey += "_lumsum";
          }
      }
List lstAllocolumn = new ArrayList();
  if(boqId != 20 && boqId != 17)
{
   columnlist=bdl.getString(propKey);
   //out.println("propKey="+propKey+"   columnlist="+columnlist);
   String colunArr[]=columnlist.split(",");

     for(int i=0;i<colunArr.length;i++)
     {
        lstAllocolumn.add(colunArr[i]);
     }
}
else
{
    columnlist=null;
    String colunArr[]=null;
}

%>
<tbody id="MainTable<%=tableId%>">
    <input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
    <input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">

<script>
        var verified = true;
        var lbl_ids=new Array();
        var lbl_countid=0;
</script>
<%
        short fillBy[] = new short[cols];
        int arrColId[] = new int[cols];
        String arrColType[] = new String[cols];

        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }
       // out.println("bidid="+bidId+" tableid="+tableId +" boqid="+boqId+" contract type="+ContractType);

        ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getNegTableColumns(tableId,bidId, true).listIterator();
        ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();
        int grandTotalCell = tenderBidSrBean.getCellForTotalWordsCaption(formId, tableId);

        ListIterator<CommonFormData> tblBidTableId = null;
        ListIterator<CommonFormData> tblBidData = null;
        ListIterator<CommonFormData> tblBidCellData = null;

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();

        String type = "";
        if(request.getParameter("type")!= null && !"".equalsIgnoreCase(request.getParameter("type"))){
               type = request.getParameter("type");
        }

        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
        listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId);

         while(tblFormulaDtl.hasNext()){
            CommonFormData formulaData = tblFormulaDtl.next();
            if(formulaData.getFormula().contains("TOTAL")){
                isTotal = 1;
            }
        }
        while(tblFormulaDtl.hasPrevious()){
            tblFormulaDtl.previous();
        }
        if(isEdit || isView){
           // out.println("----------------------------"+tableId);

            if("negbiddata".equalsIgnoreCase(type))
            {
                tblBidTableId = tenderBidSrBean.GetNegBidTableId(negBidformId, tableId).listIterator();
                tblBidData = tenderBidSrBean.getNegBidSrcData(negBidformId).listIterator();
               // tblBidData = tenderBidSrBean.getNegBidPlainData(bidId,userId).listIterator();
            }else if("biddata".equalsIgnoreCase(type)){
                tblBidTableId = tenderBidSrBean.getBidTableId(bidId, tableId,userId).listIterator();
                tblBidData = tenderBidSrBean.getNegBidPlainData(bidId,userId).listIterator();
            }
           // out.println(lstAllocolumn.size());
           // out.println("nego bid data size ="+tenderBidSrBean.getNegBidSrcData(negBidformId).size());
          // out.println("bid data size ="+tenderBidSrBean.getNegBidPlainData(bidId, userId).size());
            if(isMultiTable){
                //tblCellsDtl = tenderBidSrBean.getBidCellData(tableId, userId, bidId).listIterator();
            }


            while(tblBidTableId.hasNext()){
                //tblBidTableId.next();
                CommonFormData bidTableIdDt = tblBidTableId.next();
                bidTableIds.append(bidTableIdDt.getBidTableId() + " , ");
                cntRow++;
            }


%>
            <script>
                arrBidCount[gblCnt++]='<%=cntRow%>';
            </script>
<%

            if(tblBidTableId.hasPrevious())
                tblBidTableId.previous();

                if(tblBidTableId.hasNext()){
                    tblBidTableId.next();
                    if(rows > 1 && isTotal == 1){
                        editedRowCount = (cntRow * (rows-1)) + 1;
                        //out.println("<BR>cntrow="+cntRow+" rows="+rows);
                    }else{
                        editedRowCount = (rows*cntRow);
                    }
                }
                TotalBidCount= cntRow;
         }else{
            cntRow = 1;
         }

         int noOfRowsWOGT = 0;

         if(rows > 1 && isTotal == 1){
             noOfRowsWOGT = rows -1;
         }else{
             noOfRowsWOGT = rows;
         }
%>
            <script>
		var rowcount = parseInt("<%=rows%>")*parseInt("<%=cntRow%>");
		arrCompType[<%=tableIndex%>] = new Array(rowcount);
		arrCellData[<%=tableIndex%>] = new Array(rowcount);
		arrDataTypesforCell[<%=tableIndex%>] = new Array(rowcount);
                <%if(isEdit || isView ){%>
                arrTableAddedKey[<%=tableId%>] = parseInt(<%=cntRow%>);
                arrTableAdded[<%=tableIndex%>] = parseInt(<%=cntRow%>);
                totalBidTable[<%=tableIndex%>] = '<%=cntRow%>';
                <%}%>
		for (i=1;i<=rowcount;i++)
		{
			arrCompType[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrCellData[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrDataTypesforCell[<%=tableIndex%>][i]=new Array(<%=cols%>);
		}

		if(rowcount == 1)
		{
			i = <%=cols%>;
		}
               arrColFillBy[<%=tableIndex%>] = new Array(<%=cols%>);
            </script>
        <%if(isEdit || isView ){%>
            <script>
		document.getElementById("tableCount<%=tableId%>").value = "<%=cntRow%>";
		document.getElementById("originalTableCount<%=tableId%>").value = "<%=cntRow%>";
                //arrBidCount.push(document.getElementById("tableCount"+<%//=tableId%>).value);
            </script>
            <input type="hidden" value="<%=editedRowCount%>" name="eRowCount<%=tableId%>" id="editRowCount">
<%
                while(tblBidTableId.hasPrevious()){
                    tblBidTableId.previous();
                }
        }
        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
%>
                    <script>
                        //alert(<%=formulaData.getColumnId()%>);
                        //alert(arrColTotalIds);
                        isColTotalforTable[<%=tableIndex%>] = 1;
                        for(var i=1;i<=<%=cols%>;i++){
                            if(i == <%=formulaData.getColumnId()%>){
                                arrColTotalIds[<%=tableIndex%>][i-1] = '<%=formulaData.getColumnId()%>';
                            }
                        }
                    </script>
<%
                }
                cnt++;
                FormulaCount = cnt;
            }
%>
            <script>
                    var totalWordArr = new Array();
                    var q = 0;
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
%>
                    <script>
                        var TotalWordColId = "<%=formulaData.getColumnId()%>";
                        totalWordArr[q] = "<%=formulaData.getColumnId()%>";
                        q++;
                        arrColTotalWordsIds[<%=tableIndex%>][<%=x%>] = '<%=formulaData.getColumnId()%>';
                    </script>
<%
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
%>
            <script>
                arrTableFormula[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores Formula
                arrFormulaFor[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores where to Apply Formula
                arrIds[<%=tableIndex%>] = new Array(<%=FormulaCount%>);
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
%>
		<script>
                    arrTableFormula[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getFormula()%>';
                    arrFormulaFor[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getColumnId()%>';
		</script>
<%
                counter++;
            }
%>
            <script language="JavaScript" >
                arrColIds[<%=tableIndex%>] =new Array();
                arrStaticColIds[<%=tableIndex%>] =new Array();
                arrForLabelDisp[<%=tableIndex%>] = new Array();
            </script>
<%
        }
        if(isEdit || isView ){
            if(isMultiTable){
                //rows = editedRowCount;
                while(tblBidTableId.hasPrevious())
                {
                    tblBidTableId.previous();
                }
            }
        }



        if(!(isEdit || isView)) // Form Add Start
        {
            int colId = 0;

            for (short i = -1; i <= rows; i++) {
                if(i == 0){
%>
             <tr id="ColumnRow">
                 <% if(isMultiTable){ %>
                 <th>Select</th>
                 <% } %>
<%
                    for(short j=0;j<cols;j++){
                        if(tblColumnsDtl.hasNext()){
                            CommonFormData colData = tblColumnsDtl.next();

                            colHeader = colData.getColumnHeader();
                            colType = colData.getColumnType();
                            filledBy = colData.getFillBy();
                            showOrHide = Integer.parseInt(colData.getShowOrHide());
                            dataType = colData.getDataType();
                            colId = colData.getColumnId();
                            colData = null;
                        }
                        arrColType[j] = colType;
                        fillBy[j] = filledBy;
                        arrColId[j] = colId;
%>
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %> 
                    <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                    </script>
                    <%}%>
                    <script>
                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                    </script>
                </th>
<%
                    }
%>
             </tr>
<%
                }
                if(i > 0){
%>
             <tr id="row<%=tableId%>_<%=i%>">
                 <% if(isMultiTable && i==1 && (!isView) ){ %>
                 <td class="t-align-center">
                     <input type="checkbox" id="chk<%=tableId%>_<%=i%>" />
                 </td>
                 <% }else if(isMultiTable){ %>
                 <td class="t-align-center">&nbsp;</td>
                 <% } %>
<%
                    int cnt = 0;
                    for(int j=0; j<cols; j++){
                        String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=i%>_<%=columnId%>" align="center">
                <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").visibility = "collapse";
                    </script>
                <%}%>
<%
                        if(tblCellsDtl.hasNext()){
                            cnt++;
                            CommonFormData cellData = tblCellsDtl.next();
                            dataType = cellData.getCellDataType();
                            //filledBy = cellData.getCellDataType();
                            //filledBy = cellData.getFillBy();
                            cellId = cellData.getCellId();
                            cellValue = cellData.getCellValue();
                            columnId = cellData.getColumnId();
                            if(true){
                                String cValue = "";
                                cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '';
                             </script>
<%
                            }
                            if(grandTotalCell==cellId){
                                out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                            }
                            if(fillBy[j] == 1){

%>
                                    <label  name="lbl@<%=tableId%>_<%=i%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=i%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType==11){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue.replaceAll("\n", "<br />") %>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=i%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                               }
                            } else if(fillBy[j] == 2 || fillBy[j] == 3){
                                if(dataType != 2){
                                    if(i == (rows)){
                                        if(FormulaCount>0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
%>                                                        <textarea class="formTxtArea_1"
                                                                readonly
								tabindex="-1"
                                                                rows="4"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>"
							><%=textAreaValue%></textarea>
<%
                                                        } else if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){

                                                            if(listCurrencyObj.size() > 0){
                                                            %>
                                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                                <% } %>
                                                            </select>
                                                            <%
                                                          }
                                                        }else if(dataType==9 || dataType==10){
                                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                                listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                                if(listCellDetail.size() > 0){
                                                                TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                                List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                                 %>
                                                                 <select  id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                                    <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                        <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                                <% if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){ %>
                                                                                selected="selected"
                                                                                <% } %>
                                                                                >
                                                                            <%=tblTenderListDetail.getItemText()%>
                                                                        </option>
                                                                    <% } %>
                                                                </select>
                                                                <script language="javascript">
                                                                    //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                                    //combo_val_tableid.push("<%=tableId%>");
                                                                    changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                                </script>
                    <%                                          }
                                                        }else {
%>
                                                        <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                 }else{
                                                    inputtype = "text";
                                                 }
                                            }
                                        }
                                    }

                                    if(i == (rows-1) && isTotal == 1){
                                        //out.print("<b>GenerateViewFormula <b>");
                                    }

                                    //out.print("FillBy : "+fillBy[j]);
                                    //out.print("  Datatype : "+dataType);
                                   if(i != (rows))// Current row is not last row
                                   {
                                       if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                       {
                                            if(listCurrencyObj.size() > 0){
                                        %>
                                        <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                            <% for(Object[] obj :listCurrencyObj){ %>
                                            <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                            <% } %>
                                        </select>
                                        <%
                                            }
                                       }
%>
                                       <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" 
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                        if(fillBy[j] != 3){
                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this, '<%=tableIndex%>');"
<%
                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this, '<%=tableIndex%>');"
<%
                                            } else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                            } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==12){
%>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                        }
                                             if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none;"
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
                                       if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                        />
<%
                                        if(dataType == 12 && fillBy[j] == 2)
                                       {
                                        %>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="imgrow<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align:middle;">
                                        <%
                                        }
                                        if(dataType==9 || dataType==10){

                                            List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                            TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                            listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                            if(listCellDetail.size() > 0){
                                            TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                            List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                             %>
                                             <select  id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                    <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                            <% if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){ %>
                                                            selected="selected"
                                                            <% } %>
                                                            >
                                                        <%=tblTenderListDetail.getItemText()%>
                                                    </option>
                                                <% } %>
                                            </select>
                                                <script language="javascript">
                                                    //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                   // combo_val_tableid.push("<%=tableId%>");
                                                    changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                </script>
<%                                             }
                                            }
                                   }
                                   else if(i == (rows))
                                   {
                                       if(FormulaCount > 0)
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(arrFormulaColid[columnId-1] == columnId)
                                                {

%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(fillBy[j] != 3){
                                                        if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                        }else if(dataType == 1 || dataType == 2)
                                                        {
%>
                                                          
<%
                                                        }
                                                         else if(dataType==12){
%>
                                       readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                        } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                        } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"

                                        <%
                                                        }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                                    }
                                        if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                                
                                        />
<%
                                                }
                                            }else{
                                               if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" 
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                    } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                    } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType == 1 || dataType == 2){
%>
                                                      
<%
                                                    }else if(dataType==12){
%>
                                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                                }

                                        if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none; "
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
                                               if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
%>
                                        />

<%
                                    if(dataType == 12 && fillBy[j] == 2)
                                       {
                                        %>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="imgrow<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align:middle;">
                                        <%
                                        }
                                                 if(dataType==9 || dataType==10){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                         %>
                                                         <select  id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                            <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                    <% if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){ %>
                                                                    selected="selected"
                                                                    <% } %>
                                                                >
                                                                <%=tblTenderListDetail.getItemText()%>
                                                                </option>
                                                            <% } %>
                                                        </select>
                                                        <script language="javascript">
                                                            //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                            //combo_val_tableid.push("<%=tableId%>");
                                                            changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                        </script>
            <%                                             }

                                            }
                                            }
                                        }else{
                                           if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                           {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                           }
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" 
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==1){
%>
                                        onBlur="chkSizeTextBox('<%=tableId%>',this);"
<%
                                                } else if(dataType==2){
%>
                                        onBlur="chkSizeTextArea('<%=tableId%>',this);"
<%
                                                } else if(dataType == 1 || dataType == 2){
%>
                                                    
<%
                                                } else if(dataType==12){
%>
                                                   readonly="true" onfocus="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                            }
                                            if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none; "
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
                                           if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
                                         %>
                                        />
                                        <%
                                        if(dataType == 12 && fillBy[j] == 2)
                                       {
                                        %>
                                            <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                 onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                 id="imgrow<%=tableId%>_<%=i%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                 style="vertical-align:middle;">
                                        <%
                                        }
                                                 if(dataType==9 || dataType==10){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                         %>
                                                         <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');">
                                                            <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                        <% if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){ %>
                                                                        selected="selected"
                                                                        <% } %>
                                                                        >
                                                                    <%=tblTenderListDetail.getItemText()%>
                                                                </option>
                                                            <% } %>
                                                        </select>
                                                        <script language="javascript" defer >
                                                            //combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                            //combo_val_tableid.push("<%=tableId%>");
                                                            changeTextVal(document.getElementById("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>"),'<%=tableId%>');
                                                        </script>
 <%                                             }

                                            }

                                        }
                                   }
                           }

                                if(dataType == 2){
                                    if(i == (rows)){
                                        if(FormulaCount > 0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                          %>
                                                      <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
                                                          <%
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                    inputtype = "text";
                                                }
                                            }
                                        }
                                    }

                                //out.print("cellId : "+cellData.getCellId());
                                    if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea rows="4" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtArea_1"></textarea>
<%
                                        if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                        }
                                    }else{
%>
                                        <textarea name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1" rows="4" <%if(isEdit || isView ){out.print("readOnly");}%> ></textarea>
<%

                                    }
                                }
                                if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                }
                            }
                        } // celldtl
%>
                </td>
<%
                    }
%>
            </tr>
<%
                } // if(i>0)
            } //for(row)
        } // Add From Ends





















        else // ----------------------------------------Start Edit here--------------------------------------------
        {
            int colId = 0;
            int cntBidTable = 0;
            int tmpRowId = 0;
            String currentCellValue="";
            while(tblBidTableId.hasNext())// Loop for list of table added multiple time
            {
                CommonFormData bidTableIdDt = tblBidTableId.next();
                cntBidTable++;
                while(tblCellsDtl.hasPrevious())
                {
                        tblCellsDtl.previous();
                }


                for (short i = -1; i <= rows; i++)  // Loop for row dispaly including Headers
                {

                        if((i == 0) && (cntBidTable==1))
                        { // Displaying Header row
        %>
                        <tr id="ColumnRow">
                         <% if(isMultiTable && !isView){ %>
                         <th>Select</th>
                         <% } %>
        <%              // Loop for column Header
                        for(short j=0;j<cols;j++)
                        {


                                if(tblColumnsDtl.hasNext())
                                {
                                    CommonFormData colData = tblColumnsDtl.next();

                                    colHeader = colData.getColumnHeader();
                                    colType = colData.getColumnType();
                                    filledBy = colData.getFillBy();
                                    showOrHide = Integer.parseInt(colData.getShowOrHide());
                                    dataType = colData.getDataType();
                                    colId = colData.getColumnId();
                                    colData = null;
                                }
                                arrColType[j] = colType;
                                fillBy[j] = filledBy;
                                arrColId[j] = colId;

                               String bgcolor="";
                                if( fillBy[j] == 2 &&( lstAllocolumn.size()==0 || lstAllocolumn.contains((j+1)+"")))
                                    {
                                        bgcolor="#e8aaad";
                                    }
				    else
				    {
					    bgcolor="";
				    }
        %>
        <th id="addTD<%= j + 1 %>" style="background-color:<%= bgcolor%>">
                                    <%= colHeader %>
                                    <%if(showOrHide==2){%>
                                    <script>
                                        document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                                    </script>
                                    <%}%>
                                    <script>
                                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                                    </script>
                                </th>
        <%
                        }
%>
                    </tr>
<%                  } // End Displaying Header row


              if(i > 0) // Dispalying Rows start.....
              {
                  
                        if(isTotal==1 && isMultiTable){
                            if(cntBidTable != cntRow && i==rows){
                                continue;
                            }else{
                                tmpRowId++;
                            }
                        }else{
                            tmpRowId++;
                        }
                %>
                        <tr id="row<%=tableId%>_<%=tmpRowId%>">
                <%
                      //  out.println("===================================tmpRowId="+tmpRowId);
%>

                 <% if(isMultiTable && i==1 && !isView){ %>
                 <td class="t-align-center"><input type="checkbox" id="chk<%=tableId%>_<%=i%>" /></td>
                 <% }else if(isMultiTable && !isView){ %>
                 <td class="t-align-center">&nbsp;</td>
                 <% } %>
<%
            int cnt = 0;
            for(int j=0; j<cols; j++) // Loop For Column Loop
            {
                String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" align="center"
                <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>").visibility = "collapse";
                    </script>
                <%} %>
                >
<%


                            if(tblCellsDtl.hasNext())  // Extractiong List of table cell Data
                            {
                                cnt++;
                                CommonFormData cellData = tblCellsDtl.next();
                                dataType = cellData.getCellDataType();
                                cellValue = cellData.getCellValue();
                                columnId = cellData.getColumnId();
                                cellId = cellData.getCellId();
                                if(true){
                                    String cValue = "";
                                    cValue = cellValue.replaceAll("\r\n", "");



%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '';
                             </script>
<%
                                    if(grandTotalCell==cellId){
                                        out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                                    }
                                }
                                if(fillBy[j] == 1)
                                { // If Fill by PE

%>
                                    <label  name="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType==11){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue.replaceAll("\n", "<br />") %> 
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                    if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                                    }
                                }
                                else if(fillBy[j] == 2 || fillBy[j] == 3) // If Fill by Tenderer or Auto
                                {
                                    if(dataType != 2) // Data type is Not Long Text
                                    {
                                        if(tmpRowId == (editedRowCount)) // if current row is last row?
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(cntRow < TotalBidCount)
                                                    if(i%(rows - 1)==0)
                                                        break;

                                                if(arrFormulaColid[columnId-1] != columnId) // if column is not formula column
                                                {
                                                    if(arrWordFormulaColid[columnId-1] == columnId) // If column is word formula
                                                    {
                                                        if(fillBy[j] == 3 && dataType == 3) // Auto column
                                                        {
                                                            %>
                                                        <textarea class="formTxtArea_1"
                                                                readonly
								tabindex="-1"
								rows="4"
								name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
								id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"><%
                                                                while(tblBidData.hasPrevious()){
                                                                    tblBidData.previous();
                                                                }
                                                                while(tblBidData.hasNext()){
                                                                    CommonFormData bidData = tblBidData.next();
                                                                    if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                        if(!"".equals(bidData.getCellValue())){
                                                                            out.print(bidData.getCellValue());
                                                                        }
                                                                     }
                                                                }
                                                            %></textarea>
<%
                                                        }
                                                        else if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])) // FIll by Tenderer and data type is Money Posiitive
                                                        {
                                                            if(listCurrencyObj.size() > 0){
                                                            %>
                                                            <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                                <% } %>
                                                            </select>
                                                            <%
                                                          }
                                                        }
                                                        else if((dataType==9 || dataType==10)) // Data type is Combo box
                                                        {

                                                            List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                            TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                            listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                            if(listCellDetail.size() > 0){
                                                            TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                            List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                             %>
                                                             <select  id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                                    <%
                                                                        if((lstAllocolumn.size() > 0 && !lstAllocolumn.contains((j+1)+"") )|| isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                                     >
                                                                <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                    <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                    <% if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){ %>
                                                                    selected="selected"
                                                                    <% } %>
                                                                            >
                                                                    <%=tblTenderListDetail.getItemText()%>
                                                                    </option>
                                                                <% } %>
                                                            </select>
                <%                                          }
                                                        }
                                                       else // Data type is any thing but not combo box
                                                        {
%>
                                                        <textarea class="formTxtArea_1"
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                            rows="4"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    } // End of word formula
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                        inputtype = "text";
                                                }
                                            }
                                        } // end of if(tmpRowId == (editedRowCount)) ////end of  if current row is last row?

                                       //out.print(" FillBy : "+fillBy[j]);
                                       //out.print(" Datatype : "+dataType);
                                       //out.print("tmpRowId : "+tmpRowId);
                                       //out.print("rows : "+rows);

                                       if(tmpRowId != (rows))  // current row is not last row of current table
                                       {
                                               if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])) // Fill by is Tenderer
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }
    %>
                                            <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" 
                                            <%if(fillBy[j] == 3)
                                            {%>
                                            readOnly
                                            <%}%>
                                            <%if(isView)
                                            {
                                            %>
                                            readOnly
                                            <%
                                            }
                                            if(lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+""))
                                            {
                                            %>
                                            readOnly
                                            <%
                                            }
                                           if((dataType==9 || dataType==10))
                                            {
                                            %>
                                                style="display: none"
                                            <%
                                            }
                                            if(isEdit)
                                            {
                                                if(fillBy[j] != 3) // If fill by tenderer
                                                {
                                                    if(dataType==3){
    %>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
    <%
                                                    }else if(dataType==4){
    %>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                    } else if(dataType==8){
    %>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
    <%
                                                    }else if(dataType == 1 || dataType == 2){
    %>
                                          
    <%
                                                    } else if(dataType==12){
    %>
                                         readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                    } else if(dataType==11){
    %>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this);"
    <%
                                                   }  else if(dataType==13){
    %>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                   }
                                                }  // End of If fill by tenderer
                                            }
                                                while(tblBidData.hasPrevious()){
                                                    tblBidData.previous();
                                                }
                                                while(tblBidData.hasNext()){
                                                    CommonFormData bidData = tblBidData.next();
                                                    if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
                                                    {
                                                        currentCellValue=bidData.getCellValue();
    %>
                                             value="<%=bidData.getCellValue()%>"
    <%
                                                    }
                                                }
                                                if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                                %>
                                                    style=" text-align: right;"
                                                <%
                                                }
                                               if(dataType == 12){ %> class="formTxtDate_1" <% }
                                          else { %> class="formTxtBox_2" <% }
    %>
                                              />


    <%
                                            
                                            if(dataType == 12 && fillBy[j] == 2 && ((lstAllocolumn.size()>0 && lstAllocolumn.contains((j+1)+"")) || lstAllocolumn.size() == 0 ))
                                               {
                                                %>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="imgrow<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align:middle;">
                                                <%
                                                }
                                            
                                                if((dataType==9 || dataType==10))
                                                { // if data type is combo and fill by tenderer

                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0)
                                                    {

                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                     %>
                                                     <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                             <%
                                                                        if((lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+"")) || isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                             >
                                                        <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                            <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                            <% //if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault()))
                                                               if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue()))
                                                               { %>
                                                                selected="selected"
                                                            <% } %>
                                                            >
                                                            <%=tblTenderListDetail.getItemText()%>
                                                            </option>
                                                        <% } %>
                                                    </select>
    <%                                             }
                                           }
                                       }
                                       else if(tmpRowId == (rows)) // current row is  last row
                                       {
                                           //out.print("FormulaCount : "+FormulaCount);
                                           //out.print("isTotal : "+isTotal);
                                            if(FormulaCount > 0) // Table contains formula column
                                            {
                                                if(isTotal == 1)
                                                {
                                                    if(isMultiTable){
                                                     %>
                                                    <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" 
                                                    <%if(fillBy[j] == 3){%>
                                                    readOnly
                                                    <%}%>
                                                    <%if(isView){%>
							readOnly
							<%}
						    if(lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+""))
							    {
							    %>
							    readOnly
							    <%
							    }
                                                    if((dataType==9 || dataType==10))
                                                        {
                                                        %>
                                                            style="display: none"
                                                        <%
                                                        }
                                                            %>
            <%
                                                                if(isEdit)
                                                                {
                                                                    if(fillBy[j] != 3){
                                                                        if(dataType==3){
            %>
                                                    onBlur="CheckFloat1('<%=tableId%>',this);"
            <%
                                                                        }else if(dataType==4){
            %>
                                                    onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
            <%
                                                                        } else if(dataType==8){
            %>
                                                    onBlur="moneywithminus('<%=tableId%>',this);"
            <%
                                                                        }else if(dataType == 1 || dataType == 2){
            %>
                                                    
            <%
                                                                        } else if(dataType==12){
            %>
                                                    readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
            <%
                                                                        } else if(dataType==11){
            %>
                                                    onBlur="chkFloatMinus5Plus5('<%=tableId%>',this);"
            <%

                                                                        }else if(dataType==13){
            %>
                                                    onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
            <%
                                                           }
                                                                    }
                                                                }

                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
									{
										currentCellValue=bidData.getCellValue();
            %>
                                                     value="<%=bidData.getCellValue()%>"
            <%
                                                                        }
                                                                    }

                                                        if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                                            %>
                                                                style=" text-align: right;"
                                                            <%
                                                            }
                                                    if(dataType == 12){ %> class="formTxtDate_1" <% }
                                                    else { %> class="formTxtBox_2" <% }
            %>
                                                    />

            <%
                                                        /*
                                                        if(dataType == 12 && fillBy[j] == 2 && lstAllocolumn.contains((j+1)+""))
                                                           {
                                                            %>
                                                                <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                                     onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                                     id="imgrow<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                                     style="vertical-align:middle;">
                                                            <%
                                                            }*/
                                                            if((dataType==9 || dataType==10)){
                                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                                    if(listCellDetail.size() > 0){
                                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                                     %>
                                                                     <select id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                                     <%
                                                                        if((lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+"")) || isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                                             >
                                                                        <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                            <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                            <% if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue())){ %>
                                                                            selected="selected"
                                                                            <% } %>
                                                                             >
                                                                            <%=tblTenderListDetail.getItemText()%>
                                                                            </option>
                                                                        <% } %>
                                                                    </select>
                        <%                                             }

                                                            }
                                                    }
                                                    else
                                                    {
                                                        if(arrFormulaColid[columnId-1] == columnId){
%>
                                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" 
                                                        <%if(fillBy[j] == 3){%>
                                                        readOnly
                                                        <%}%>
                                                        <%if(isView){%>
                                                        readOnly
                                                        <%}
					    if(lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+""))
						    {
						    %>
						    readOnly
						    <%
						    }
                                                     if((dataType==9 || dataType==10))
                                                    {
                                                    %>
                                                        style="display: none"
                                                    <%
                                                    }
						%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                            }else if(dataType == 1 || dataType == 2){
%>
                                       
<%
                                                            } else if(dataType==12){
%>
                                        readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this);"
<%

                                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                               }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
							    {
							    currentCellValue=bidData.getCellValue();
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                            }
                                                        }

                                            if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                                %>
                                                    style="text-align: right;"
                                                <%
                                                }
                                            if(dataType == 12){ %> class="formTxtDate_1" <% }
                                            else { %> class="formTxtBox_2" <% }
%>
                                        />

<%
                                              if(dataType == 12 && fillBy[j] == 2 && ((lstAllocolumn.size()>0 && lstAllocolumn.contains((j+1)+"")) || lstAllocolumn.size() == 0 ))
                                               {
                                                %>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="imgrow<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align:middle;">
                                                <%
                                                }

                                                if((dataType==9 || dataType==10)){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                         %>
                                                         <select  id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                                 <%
                                                                        if((lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+"")) || isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                                 >
                                                            <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                <% if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue())){ %>
                                                                selected="selected"
                                                                <% } %>
                                                                 >
                                                                <%=tblTenderListDetail.getItemText()%>
                                                                </option>
                                                            <% } %>
                                                        </select>
            <%                                             }

                                                            }
                                                        }
                                                    }
                                        } //End of if isTotal == 1
                                        else
                                        {
                                            if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                           {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                           }
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" 
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView){%>
                                        readOnly
                                        <%}
                                        if(lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+""))
                                            {
                                            %>
                                            readOnly
                                            <%
                                            }
                                            if((dataType==9 || dataType==10))
                                            {
                                            %>
                                                style="display: none"
                                            <%
                                            }
					%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                            }else if(dataType == 1 || dataType == 2){
%>
                                      
<%
                                                            } else if(dataType==12){
%>
                                        readonly="true" onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this);"
<%

                                                            }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                               }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
							    {
								currentCellValue=bidData.getCellValue();
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                            }
                                                        }
                                            if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                            %>
                                                style=" text-align: right;"
                                            <%
                                            }
                                            if(dataType == 12){ %> class="formTxtDate_1" <% }
                                            else { %> class="formTxtBox_2" <% }
%>
                                        />
<%
                                             if(dataType == 12 && fillBy[j] == 2 && ((lstAllocolumn.size()>0 && lstAllocolumn.contains((j+1)+"")) || lstAllocolumn.size() == 0 ))
                                               {
                                                %>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="imgrow<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align:middle;">
                                                <%
                                                }

                                                     if((dataType==9 || dataType==10)){
                                                        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                        TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                        listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                        if(listCellDetail.size() > 0){
                                                        TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                        List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                         %>
                                                         <select  id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                                 <%
                                                                        if((lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+"")) || isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                                 >
                                                            <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                                <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                                <% if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue())){ %>
                                                                selected="selected"
                                                                <% } %>
                                                                 >
                                                                <%=tblTenderListDetail.getItemText()%>
                                                                </option>
                                                            <% } %>
                                                        </select>
            <%                                             }

                                                }
                                                }
                                            } // End of if last column is formula column
                                            else
                                            { // Column is last column and table not contain any formula column
                                                if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]) && !isView)
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="Currency<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this.value,'<%=tableId%>');" style="display: none;">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" 
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView){%>
                                        readOnly
                                        <%}
                                        if(lstAllocolumn.size()>0 && !lstAllocolumn.contains((j+1)+""))
                                        {
                                        %>
                                            readOnly
                                        <%
                                        }
                                        if((dataType==9 || dataType==10))
                                            {
                                            %>
                                                style="display: none"
                                            <%
                                            }
                                        %>
<%
                                            if(isEdit)
                                            {
                                                if(fillBy[j] != 3)
                                                {
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                    }else if(dataType == 1 || dataType == 2){
%>
                                                  
<%
                                                } else if(dataType==12){
%>
                                                    readonly="true"  onfocus="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this);"
<%
                                                   }else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                               }
                                                }
                                            }

                                                while(tblBidData.hasPrevious()){
                                                    tblBidData.previous();
                                                }
                                                while(tblBidData.hasNext()){
                                                    CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId))
							{
							currentCellValue=bidData.getCellValue();
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                        }
                                                    }
                                                if((dataType==3 || dataType==4 || dataType==8 || dataType==11) && fillBy[j]==2 && isView){
                                            %>
                                                style=" text-align: right;"
                                            <%
                                            }
                                            if(dataType == 12){ %> class="formTxtDate_1" <% }
                                            else { %> class="formTxtBox_2" <% }
%>
                                         />
<%
                                     if(dataType == 12 && fillBy[j] == 2 && ((lstAllocolumn.size()>0 && lstAllocolumn.contains((j+1)+"")) || lstAllocolumn.size() == 0 ))
                                               {
                                                %>
                                                    <img border="0" src="../resources/images/Dashboard/calendarIcn.png"
                                                         onclick="GetCal('row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
                                                         id="imgrow<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="calendarIcn" alt="Select a Date"
                                                         style="vertical-align:middle;">
                                                <%
                                                }

                                                if((dataType==9 || dataType==10)){ // Fill by tenderere and data type is combo
                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = tenderBidSrBean.getTenderListCellDetail(formId,tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0){
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                     %>
                                                     <select  id="idcombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formSelect_1" onchange="changeTextVal(this,'<%=tableId%>');"
                                                             <%
                                                                        if((lstAllocolumn.size()> 0 &&  !lstAllocolumn.contains((j+1)+"")) || isView)
                                                                            out.println(" disabled=true");
                                                                    %>
                                                             >
                                                        <% for(TblTenderListDetail tblTenderListDetail:listBoxDetail){ %>
                                                            <option value="<%=tblTenderListDetail.getItemValue()%>"
                                                            <%  if(currentCellValue.equalsIgnoreCase(tblTenderListDetail.getItemValue())){ %>
                                                                selected="selected"
                                                            <% } %>
                                                            >
                                                            <%=tblTenderListDetail.getItemText()%>
                                                            </option>
                                                        <% } %>
                                                    </select>
        <%                                             }

                                            }
                                        }
                                    } // // End of if current row is last row
                                } // if data type is not log text

                                if(dataType == 2)
                                { // If fill by is tenderere or auto and data type is long text

                                        //For Dispaly total in words
                                        if(tmpRowId == (rows)) // row is last row
                                        {        if(FormulaCount > 0){
                                                if(isTotal == 1){
                                                    if(!isMultiTable){
                                                           if(arrFormulaColid[columnId-1] != columnId){
                                                                if(arrWordFormulaColid[columnId-1] == columnId){
                                                                  %>
                                                              <textarea class="formTxtArea_1"
                                                                    readonly
                                                                    tabindex="-1"
                                                                    rows="4"
                                                                    name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                    id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                ><%
                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                            out.print(bidData.getCellValue());
                                                                        }
                                                                    }
                                                                    %></textarea>
                                                                  <%
                                                            }
                                                            out.print("</td>");
                                                            continue;
                                                        }else{
                                                           inputtype = "text";
                                                        }
                                                    }else{
                                                        /* Negotiation last row showing unwanted textarea. so added below if condition so that it will load text area where needed */
                                                        if(arrWordFormulaColid[columnId-1] == columnId){
                                                                  %>
                                                              <textarea class="formTxtArea_1"
                                                                    readonly
                                                                    tabindex="-1"
                                                                    rows="4"
                                                                    name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                    id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                                ><%
                                                                    while(tblBidData.hasPrevious()){
                                                                        tblBidData.previous();
                                                                    }
                                                                    while(tblBidData.hasNext()){
                                                                        CommonFormData bidData = tblBidData.next();
                                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                                            out.print(bidData.getCellValue());
                                                                        }
                                                                    }
                                                                    %></textarea>
                                                                  <%
                                                            out.print("</td>");
                                                            continue;
                                                            }
                                                         }
                                                        }
                                                    }
                                                }


                                      if(fillBy[j] == 3) // if fill by auto
                                      {

%>
                                        <label name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" rows="4" readOnly class="formTxtArea_1"><%
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                    out.print(bidData.getCellValue());
                                                }
                                            }
%></textarea>
<%
                                            if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                            }
                                        }
                                       else  // if fill by Tenderer
                                        {
                                            if(isMultiTable && isTotal==1)
                                            {
                                                if(tmpRowId!=(editedRowCount)){
%>
                                                <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1" rows="4" <%if(isEdit || isView){out.print("readOnly");}%>><%
                                                    while(tblBidData.hasPrevious()){
                                                        tblBidData.previous();
                                                    }
                                                    while(tblBidData.hasNext()){
                                                        CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                            out.print(bidData.getCellValue());
                                                        }
                                                    }
                                                %>
                                                </textarea>
                                                <%
                                                   }
                                            }else{
%>
                                                <textarea name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtArea_1" rows="4" <%if((lstAllocolumn.size()> 0 &&  !lstAllocolumn.contains((j+1)+"")) || isView){out.print("readOnly");}%>><%
                                                    while(tblBidData.hasPrevious()){
                                                        tblBidData.previous();
                                                    }
                                                    while(tblBidData.hasNext()){
                                                        CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                            out.print(bidData.getCellValue());
                                                        }
                                                    }
        %></textarea>
        <%
                                            }

                                        }

                                    } // End of If fill by is tenderere or auto and data type is long text


                                    if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                    }

                                } // End of If Fill by Tenderer or Auto
                            } // celldtl
%>
                </td>
<%
                        } // End Loop For Column Loop
%>
            </tr>
<%
                    }  // End Loop for row dispaly including Headers

                }  // End Loop for list of table added multiple time
            }
        }
%>
        </tbody>

        <div>
        <tr>
            <td colspan="<%=cols%>" style="display:none">
                <label class="formBtn_1" id="lblDeleteTable">
                    <input type="button" name="btnDel<%=tableId%>" id="bttnDel<%=request.getParameter("tableIndex")%>" value="Delete Record" onClick="DelTable(this.form,<%=tableId%>,this);"/>
                </label>
                <label class="formBtn_1" id="lblAddTable">
                    <input type="button" name="btn<%=tableId%>" id="bttn<%=request.getParameter("tableIndex")%>" value="Add Record" onClick="AddTable(this.form,<%=tableId%>,this)"/>
                </label>
            </td>
        </tr>
        </div>

    </table>
                </div>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>