<%-- 
    Document   : ContractSigningListing
    Created on : Jan 11, 2011, 3:00:02 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    pageContext.setAttribute("tab", "9");
        %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Signing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Tender Dashboard</div>
                <%
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                tenderId = request.getParameter("tenderId");
                            }
                            String userId = "";
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = session.getAttribute("userId").toString();
                            }
                            SimpleDateFormat dateformate = new SimpleDateFormat("yyyy/MM/dd hh:mm");
                            List<Object[]> listDetial =  issueNOASrBean.getCSListingBidder(Integer.parseInt(tenderId), Integer.parseInt(userId));
                            int pckLotId = 0;
                            int roundId = 0;
                            if(listDetial!=null && !listDetial.isEmpty()){
                                pckLotId = Integer.parseInt(listDetial.get(0)[9].toString());
                                roundId = Integer.parseInt(listDetial.get(0)[10].toString());
                            }
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include file="TendererTabPanel.jsp"%>
                <%if(!is_debared){%>
                <div class="tabPanelArea_1">
                    <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=pckLotId %>" />
                            <jsp:param name="comuserId" value="<%=userId%>" />
                            <jsp:param name="comroundId" value="<%=roundId%>" />
                        </jsp:include>
                    <div align="center">
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="30%" class="t-align-left">Contract Signing Location</th>
                                <th width="30%" class="t-align-left">Contract Signing Date and Time</th>
                                <th width="40%" class="t-align-left">Action</th>
                            </tr>
                                <%
                                    for(Object[] obj : listDetial){
                                        StringBuffer wCCHtml = new StringBuffer("");
                                        int contractSignId = (Integer)obj[0];
                                        CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                        cmsWcCertificateServiceBean.setLogUserId(userId);
                                        TblCmsWcCertificate tblCmsWcCertificate = cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
                                        if (tblCmsWcCertificate != null) {
                                            wCCHtml.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                            /*wCCHtml.append("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId() + "&tenderId=" + tenderId + "'>ViewWCCertificate</a>");*/
                                        }
                                        out.print("<tr><td class=\"t-align-center\">"+ obj[3] +"</td>");
                                        out.print("<td class=\"t-align-center\">"+ DateUtils.gridDateToStrWithoutSec((Date)obj[2]) +"</td>");
                                        out.print("<td class=\"t-align-center\">"
                                                + "<a onclick=\"javascript:window.open('ViewContractSigning.jsp?noaIssueId="+obj[8]+"&contractSignId="+obj[0] + "&tenderId="+ tenderId +"', '', 'resizable=yes,scrollbars=1','');\" href=\"javascript:void(0);\">View</a>"
                                                + wCCHtml.toString()
                                                + "</td></tr>");
                                    }
                                %>
                        </table>
                    </div>
                </div>
               <%}%>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<%
    issueNOASrBean = null;
%>
