<%-- 
    Document   : deBriefQuery
    Created on : Jun 15, 2011, 4:44:38 PM
    Author     : dixit
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debriefing  on Tender</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
    <body onload="getQueryData('postQuery')">

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
<div class="contentArea_1">
      <div class="pageHead_1">Debriefing  on Tender
          <span style="float:right;">
<!--              <a href="<a%=referer%>" class="action-button-goback">Go back</a>-->
          </span>
      </div>
      <%
            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
            String message ="";
            if(request.getParameter("message")!=null)
            {
                message = request.getParameter("message");
            }
            String strTenderId = "";
            if(request.getParameter("tenderId")!=null);
            {
                strTenderId = request.getParameter("tenderId");
            }
      %>
      <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../tenderer/TendererTabPanel.jsp" %>
      <div class="tabPanelArea_1">
      <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="5" />
        <jsp:param name="tenderId" value="<%=strTenderId%>" />
      </jsp:include>
      <table width="100%" cellspacing="0" class="tableList_1">
      <tr>
          <td colspan="3" width="85%">
              <%
                    if("success".equalsIgnoreCase(message))
                    {
              %>
                        <div class="responseMsg successMsg t_space"><span>Your Query Posted Successfully</span></div>
              <%
                    }
                    if("fail".equalsIgnoreCase(message))
                    {
              %>
                        <div class="responseMsg errorMsg t_space"><span>Your Query is not Posted Successfully</span></div>
              <%
                    }
              %>
          </td>
          <td class="t-align-center" ><a href="<%=request.getContextPath()%>/tenderer/deBriefPostQuery.jsp?tenderId=<%=request.getParameter("tenderId")%>">Seek Clarification</a></td>
<!--          class="anchorLink" style="text-decoration: none;margin-top: 10px;color: #fff;"-->
      </tr></table>

          <div id ="queryData"></div>
           
      </div>
      </div>
      </div>
      <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
</body>
<script type="text/javascript">
function getQueryData(val){
       $.ajax({
        url: "<%=request.getContextPath()%>/deBriefQueryServlet?tenderId=<%=request.getParameter("tenderId")%>&refNo=<%=toextTenderRefNo%>&action=getQuestionData&queryAction="+val,
        method: 'POST',
        async: false,
        cache : false,
        success: function(j) {
            document.getElementById("queryData").innerHTML = j;
        }
     });
}
</script>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
