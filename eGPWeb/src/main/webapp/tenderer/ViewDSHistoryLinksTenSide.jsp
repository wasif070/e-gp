<%--
    Document   : ViewDSHistoryLinks
    Created on : Sep 15, 2011, 6:32:05 PM
    Author     : dixit
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetailHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View History Links</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String lotId = "";
            if (request.getParameter("lotId") != null) {
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
                lotId = request.getParameter("lotId");
            }
            String wpId = "";
            if (request.getParameter("wpId") != null) {
                wpId = request.getParameter("wpId");
            }
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();
                service.setLogUserId(userId);
                cs.setUserId(userId);
            }
            List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId,tenderId);
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View History
                    <span style="float: right; text-align: right;">
                    <a class="action-button-goback" href="CMS.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    </span>
                </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp"%>
                <div>&nbsp;</div>
                <div class="tabPanelArea_1">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
               <%for(Object[] obj : list){%>
                    <tr>
                        <td width="20%">Lot No.</td>
                        <td width="80%"><%=obj[0] %></td>
                    </tr>
                    <tr>
                        <td>Lot Description</td>
                        <td class="t-align-left"><%=obj[1]%></td>
                    </tr>
                    <%}%>
                </table>
            <br />
            <div class="tableHead_1 t_space" align="left">View History</div>
            <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                <tr>
                    <th>No of History</th>
                    <th>Creation Date</th>
                    <th>Record</th>
                </tr>
                <%
                     List<Object[]> getDistinctCount = service.getHistoryDistinctCount(Integer.parseInt(wpId));
                     Map<Integer,Object[]> mapDistinctCount = new HashMap<Integer, Object[]>();
                     if(!getDistinctCount.isEmpty())
                     {
                        Object[] obj = null;
                        for(int i=0; i<getDistinctCount.size(); i++)
                        {
                            obj = getDistinctCount.get(i);
                            mapDistinctCount.put((Integer)obj[0], obj);
                        }
                        getDistinctCount.clear();
                        for(Integer keygetDate : mapDistinctCount.keySet()){
                            getDistinctCount.add(mapDistinctCount.get(keygetDate));
                        }
                        for(int i=getDistinctCount.size()-1; i>=0; i--){
                            obj = getDistinctCount.get(i);
                %>
                <tr>
                    <td class="t-align-center">
                    <a href="ViewDSHistoryTenSide.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=wpId%>&historyCount=<%=obj[0]%>">View History <%=(i+1)%></a>
                    </td>
                    <td class="t-align-center"><%if(obj[1]!=null){out.print(DateUtils.gridDateToStr((Date)obj[1]));}else{out.print("-");}%></td>
                    <td class="t-align-center"><%if(getDistinctCount.size()==1){out.print("First Time Insertion");}else if(i==0){out.print("First Time Insertion");}else if(i==getDistinctCount.size()-1){out.print("Latest Edited");}else{out.print("edited");}%></td>
                </tr>
                <%}}
                //ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId",EgpModule.Delivery_Schedule.getName(), "View Delivery Schedule History", "");
     %>
            </table>
            </div>
        </div></div>

    <div>&nbsp;</div>
    <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

