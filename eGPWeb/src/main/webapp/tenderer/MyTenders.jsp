<%-- 
    Document   : MyTenders
    Created on : Nov 22, 2010, 3:56:07 PM
    Author     : Malhar,rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>My Tenders/Proposals</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <!-- AJAX Grid Functions Start -->
    </head>
    <body onload="hide();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">My Tenders/Proposals
                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                </div>
                <div>&nbsp;</div>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                    <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                        <tr>
                            <td width="16%" class="ff">Procurement Category :</td>
                            <td width="34%">
                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                    <option value="" selected="selected">-- Select Category --</option>
                                    <option value="1">Goods</option>
                                    <option value="2">Works</option>
                                    <option value="3">Service</option>
                                </select>
                            </td>
                            <td width="16%" class="ff">
                                <input type="hidden" id="status" value="Pending"/>
                                <input type="hidden" id="statusTab" value="Live"/>

                            </td>
                            <td width="34%"></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Type : </td>
                            <td>
                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                    <option value="">-- Select Type --</option>
                                    <option value="NCT">NCB</option>    
                                    <option value="ICT">ICB</option>
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Method :</td>
                            <td>
                                <select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                    <option value="0" selected="selected">- Select Procurement Method -</option>
                                    <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                        <c:choose>
                                            <c:when test = "${procMethod.objectValue=='RFQ'}">
                                                <option value="${procMethod.objectId}">LEM</option>
                                            </c:when>
                                            <c:when test = "${procMethod.objectValue=='DPM'}">
                                                <option value="${procMethod.objectId}">DCM</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">Tender ID : </td>
                            <td><input type="text" class="formTxtBox_1" id="tenderId" onkeypress="checkKey(event);" style="width:194px;" /></td>
                            <td class="ff">Reference No :</td>
                            <td><input type="text" onkeypress="checkKey(event);" class="formTxtBox_1" id="refNo" style="width:194px;" /></td>
                        </tr>
                        <tr>
                            <td class="ff">Publishing Date From :</td>
                            <td><input name="pubDtFrm" id="pubDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('pubDtFrm','pubDtFrmImg');"/>&nbsp;
                                <a  href="javascript:void(0);" title="Calender"><img id="pubDtFrmImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtFrm','pubDtFrmImg');"/></a></td>
                            <td class="ff">Publishing Date To :</td>
                            <td><input name="pubDtTo" id="pubDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                <a  href="javascript:void(0);" title="Calender"><img id="pubDtToImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtTo','pubDtToImg');"/></a></td>
                        </tr>
                        <%--<tr>
                            <td width="16%" class="ff">Status :</td>
                            <td width="32%">
                                <select name="select2" class="formTxtBox_1" id="cmbStatus" >
                                    <option value="Pending">Pending</option>
                                    <option value="Approved">Approved</option>
                                </select>
                            </td>
                            <td width="15%" class="ff"></td>
                            <td width="38%"></td>
                        </tr>--%>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="submit" name="button" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                </label></td>
                        </tr>
                    </table>

                </div>
                <input type="hidden" id="CurrentTime" value="" >
                <div class="tableHead_1 t_space">Tender Search Result</div>
                <%-- <ul class="tabPanel_1">
                     <li><a href="javascript:void(0);" id="pendingTab" onclick="chngTab(1)" class="sMenu">Pending</a></li>
                     <li><a href="javascript:void(0);" id="approvedTab" onclick="chngTab(2)">Approved</a></li>
                 </ul>--%>
                <ul class="tabPanel_1 t_space">
                    <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Live</a></li>
                    <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);">Archived</a></li>
                    <li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">Cancelled</a></li>
                </ul>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,6">
                        <tr>
                            <th width="5%" class="t-align-center"><p>Sl.</p>
                                <p> No.</p></th>
                            <th width="10%" class="t-align-center"><div align="center">Tender ID, <br />
                                    Reference No., <br />
                                Tender/Proposal Status</div>
                            </th>
                            <th width="25%" class="t-align-center"><div align="center">Procurement Category, <br />
                                    Title</div></th>
                            <th width="15%" class="t-align-center"><div align="center">Hierarchy Node</div></th>
                            <th width="10%" class="t-align-center"><div align="center">Type, <br />
                                    Method</div></th>
                            <th width="20%" class="t-align-center"><div align="center">Publishing Date | <br />
                                    Closing Date</div></th>
                            <th width="15%" class="t-align-center"><div align="center">Dashboard</div></th>
                        </tr>
                    </table>
                    <table width="100%" id="pagination" border="0" cellspacing="0" cellpadding="0" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span></td>
                            <td align="center"><input name="textfield3" type="text" onkeypress="checkKeyGoTo(event);" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                </label></td>
                            <td class="prevNext-container"><ul>
                                    <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                    <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                </ul></td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="10"/>
                    </div>
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                            String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="TenderListing_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="mytender" />
            </form>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>
    <script type="text/javascript">
        function chkdisble(pageNo){
            //alert(pageNo);
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnSearch').click(function() {
                $("#pageNo").val("1");
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",statusTab:$("#statusTab").val(),action: "get tenderermytenders",status: "Approved",tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());

                    $("#pageTot").html($("#totalPages").val());

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        try
                        {
                            var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                            var temp1 = $('#tenderBrief_'+i).html();
                            if(temp.length > 250){
                                temp = temp1.substr(0, 250);
                                $('#tenderBrief_'+i).html(temp+'...');
                            }
                        }
                        catch(e){}
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {

            $('#btnReset').click(function() {
                $("#pageNo").val("1");
                $("#refNo").val('');
                $("#tenderId").val('');
                $("#procNature").val('');
                $("#cmbType").val('');
                $("#cmbProcMethod").val('');
                $("#pubDtFrm").val('');
                $("#pubDtTo").val('');
                loadTenderTable();
                /*$.post("<-%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "get tenderermytenders",status:'Approved',tenderId: '0',refNo: '',procNature:'',procType:'',procMethod: '0',pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());

                    $("#pageTot").html($("#totalPages").val());

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                });*/
            });
        });
    </script>
    <script type="text/javascript">
        function loadTenderTable()
        {
            $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",statusTab:$("#statusTab").val(),action: "get tenderermytenders",status: "Approved",tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                if($('#noRecordFound').attr("value") == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }

                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());

                $("#pageTot").html($("#totalPages").val());

                var counter = $('#cntTenBrief').val();
                for(var i=0;i<counter;i++){
                    try
                    {
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                    catch(e){}
                }
            });
        }
        function loadTenderTableOne()
        {
            $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {statusTab:$("#statusTab").val(),funName: "MyTenders",action: "get tenderermytenders",status: $("#status").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                if($('#noRecordFound').attr("value") == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }

                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());

                $("#pageTot").html($("#totalPages").val());

                var counter = $('#cntTenBrief').val();
                for(var i=0;i<counter;i++){
                    try
                    {
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                    catch(e){}
                }
            });
        }
    </script>
    <%--<script type="text/javascript">
    function loadListingTable()
    {
        $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "defualt",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
            $('#resultTable').find("tr:gt(0)").remove();
            $('#resultTable tr:last').after(j);
        });
    }
    </script>--%>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo != totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                /*var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }*/
                $('#btnGoto').click();
            }
        }
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
    </script>
    <script>
        function test(type){
            if(type==1){
                document.getElementById("linkPending").className = "sMenu";
                document.getElementById("linkApproved").className = "";
                document.getElementById("linkRejected").className = "";
                document.getElementById("statusTab").value = "Live";
                document.getElementById("status").value = "Approved";

            }else if(type==2){
                document.getElementById("linkPending").className = "";
                document.getElementById("linkApproved").className = "sMenu";
                document.getElementById("linkRejected").className = "";
                document.getElementById("statusTab").value = "Archive";
                document.getElementById("status").value = "Approved";

            }else{
                document.getElementById("linkPending").className = "";
                document.getElementById("linkApproved").className = "";
                document.getElementById("linkRejected").className = "sMenu";
                document.getElementById("statusTab").value = "Canceled";
                document.getElementById("status").value = "Canceled";
            }
            loadTenderTableOne();
        }
    </script>
    <script type="text/javascript">
        loadTenderTable();
        function checkKey(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $('#btnSearch').click();
                /*$.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "get tenderermytenders",status: "Approved",tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());

                    $("#pageTot").html($("#totalPages").val());

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                });*/
            }
        }
        function SetCurrentDate(CurrentTime)
        {
            document.getElementById("CurrentTime").value = CurrentTime;
        }
        function UpdateCurrentTimeByOneSec()
        {
            var Current = document.getElementById("CurrentTime").value ;
            AddOneSecond = parseInt(Current) + parseInt(1000);
            document.getElementById("CurrentTime").value = AddOneSecond;
        }
        function TimelineShow(obj,Submitdate)
        {
            var today = document.getElementById("CurrentTime").value ;
            var Difference =  Submitdate - today;  
            var Day = 0, Hour = 0, Minute = 0, Second = 0;
            if(Difference > 0)
            {
                var DifferenceInSeconds = Difference/1000;
                Day = DifferenceInSeconds/(60*60*24);
                DifferenceInSeconds = DifferenceInSeconds % (60*60*24);
                Hour = DifferenceInSeconds/(60*60);
                DifferenceInSeconds = DifferenceInSeconds % (60*60);
                Minute = DifferenceInSeconds/(60);
                Second = DifferenceInSeconds % (60);
                obj.innerHTML = "<b>" + Math.floor(Day) + "D " + Math.floor(Hour) + "H " + Math.floor(Minute) + "M "+ Math.floor(Second) + "S </b>";
            }
            else
            {
                obj.innerHTML = "<b>Time Elapsed</b>";

            }
        }
        
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        
    </script>
</html>
