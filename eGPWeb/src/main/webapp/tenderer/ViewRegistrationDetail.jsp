<%-- 
    Document   : ViewRegistrationDetail
    Created on : Jan 5, 2011, 11:08:08 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblUserRegInfo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="java.util.List"%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <%
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    boolean is_viewReg = false;
                    if(session.getAttribute("userId")!=null && session.getAttribute("userId").toString().equals(request.getParameter("uId"))){
                        userRegisterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    List<TblLoginMaster> tblLoginMasters = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId")));
                    TblLoginMaster tblLoginMaster = null;
                    Date approvalDate=null;
                    if(tblLoginMasters!=null && (!tblLoginMasters.isEmpty())){
                        tblLoginMaster = tblLoginMasters.get(0);
                        approvalDate = userRegisterService.getUserRegInfo(""+tblLoginMaster.getUserId());
                        is_viewReg=true;
                    }
                    String reg_qString = "";
                    if("no".equals(request.getParameter("top"))){
                        reg_qString = "&top=no";
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if("".equals(reg_qString)){%>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%}else{if(session.getAttribute("userId")==null){response.sendRedirect(request.getContextPath()+"/SessionTimedOut.jsp");}}%>
                <div class="tableHead_1 t_space">Registration Details</div>
                <div class="stepWiz_1 t_space">
                <ul>
                <%                            
                            List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(request.getParameter("uId")));//
                            String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];
                            if (pageList.size() == 3) {
                %>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Personal Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 4) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 5) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%}%>
            </ul>
        </div>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <%if(is_viewReg){%>
                    <tr>
                        <td width="18%" class="ff">e-mail ID : </td>
                        <td width="82%"><%=tblLoginMaster.getEmailId()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Nationality : </td>
                        <td><%=tblLoginMaster.getNationality()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country of Business : </td>
                        <td><%=tblLoginMaster.getBusinessCountryName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registration date & time : </td>
                        <td><%if(tblLoginMaster.getRegisteredDate()!=null){out.print(DateUtils.convertStrToDate(tblLoginMaster.getRegisteredDate()));}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registration Type : </td>
                        <td>
                            <%if (tblLoginMaster.getRegistrationType().equals("contractor")) {%>Bidder / Consultant<%}%>
                            <%if (tblLoginMaster.getRegistrationType().equals("individualconsultant")) {%>Individual Consultant<%}%>
                            <%if (tblLoginMaster.getRegistrationType().equals("govtundertaking")) {%> Government owned Enterprise<%}%>
                            <%if (tblLoginMaster.getRegistrationType().equals("media")) {%>Media<%}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Registration Approval Date : </td>
                        <td><%if(approvalDate!=null){out.print(DateUtils.convertStrToDate(approvalDate));}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registration Expiry Date : </td>
                        <td><%if(tblLoginMaster.getValidUpTo()!=null){out.print(DateUtils.convertStrToDate(tblLoginMaster.getValidUpTo()));}%></td>
                    </tr>

                    <!--tr id="trJvca">
                        <td class="ff">Is JVCA : </td>
                        <td><!%if(tblLoginMaster.getIsJvca().equals("yes")){%>Yes<!%}else{%>No<!%}%></td>
                    </tr-->
                    <%}else{out.print("<tr><td style='color:red' class='t-align-center'>No Records Found<td></tr>");}%>
                </table>
                    <table border="0" cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td width="18%">&nbsp;</td>
                            <td width="82%" align="left">
                                <a href="<%if(request.getParameter("cId").equals("1")){%>ViewPersonalDetail.jsp?tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%><%}else{%>ViewCompanyDetail.jsp?cId=<%=request.getParameter("cId")%>&uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%><%=reg_qString%><%}%>"  class="anchorLink">Next</a>
                            </td>
                        </tr>
                    </table>
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
