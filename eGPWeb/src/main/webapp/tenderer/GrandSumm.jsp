<%-- 
    Document   : GrandSumm
    Created on : Dec 18, 2010, 6:20:42 PM
    Author     : TaherT
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.SignerImpl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<%
    String hashKey = null;
    double sum = 0.00;
    if(request.getParameter("hdnPwd")!=null)
        hashKey = request.getParameter("hdnPwd");

    boolean isSigned = false;
    if(hashKey != null){
        isSigned = true;
    }
    String encryptedData[] = new String[1000];
    Arrays.fill(encryptedData,"");
    
    int cnt = 0;
    if(request.getParameter("cnt")!=null){
        cnt = Integer.parseInt(request.getParameter("cnt"));
    }
    if(cnt > 0 && isSigned){
        for(int i = 1; i<=cnt; i++){
            String name = "bidval"+(i);
            SignerImpl obj = new SignerImpl();
            obj.setEncrypt(request.getParameter(name));
            encryptedData[i] = obj.getSymDecrypt(hashKey);
            String FormType =  "formType"+(i);  
            String FormTypeStr = "Normal";
            if(request.getParameter(FormType)!=null){
                FormTypeStr = request.getParameter(FormType);
            }
            if(FormTypeStr.equalsIgnoreCase("BOQsalvage")){
                sum = sum-Double.parseDouble(encryptedData[i]);
            } else{
            sum += Double.parseDouble(encryptedData[i]);
            }
        }
        //sum = Double.parseDouble(new DecimalFormat("##.###").format(sum));
    }

    


%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grand Summary</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />        
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>               
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>        
        <script type="text/javascript">
//            var SignerAPI;
//            $(function() {
//                SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
//            });
            //alert(SignerAPI);
        </script>
        <script type="text/javascript">
            function decryptNVerify(){
                $("#dialog-form").dialog("open");
            }
            

            $(function() {
            <%--tt--%>
                    var password = $( "#password" ),
                    allFields = $( [] ).add( password ),
                    tips = $( ".validateTips" );

                    var check = false;
                    function updateTips( t ) {
                        tips
                        .text( t )
                        .addClass( "ui-state-highlight" );
                        setTimeout(function() {
                            tips.removeClass( "ui-state-highlight", 1500 );
                        }, 500 );
                    }

                    function checkLength( o, n, min, max ) {
                        if(o.val()==""){
                            o.addClass( "ui-state-error" );
                            updateTips("Please enter password");
                        }else{
                            if ( o.val().length > max || o.val().length < min ) {
                                o.addClass( "ui-state-error" );
                                updateTips( "Length of " + n + " must be between " +
                                    min + " and " + max + "." );
                                return false;
                            } else {
                                return true;
                            }
                        }
                    }
            <%--ttend--%>

                    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $("#dialog-form").dialog({
                        autoOpen: false,
                        height: 180,
                        width: 280,
                        modal: true,
                        resizable: false,
                        position: 'center',
                        buttons: {
                            "Verify Password": function() {
                                 
                                var bValid = true;
                                allFields.removeClass( "ui-state-error" );
                                bValid = bValid && checkLength( password, "password", 5, 16 );
                                if (bValid) {
                                    if(document.getElementById("password").value!=""){
                                        $.post("<%=request.getContextPath()%>/LoginSrBean", {param1:$('#password').val(),param2:'<%=session.getAttribute("userId").toString()%>',funName:'verifyPWD'},  function(j){
                                            if(j.charAt(0)=="1"){
                                              document.getElementById("hdnPwd").value = j.toString().substring(2, j.toString().length);
                                               var sum=0.0;
//                                                for(var k=1;k<=$("#cnt").val();k++){
//                                                    SignerAPI.setEncrypt($("#bidval"+k).html());
//                                                    var num=SignerAPI.getSymDecrypt(j.toString().substring(2, j.toString().length))+"";
//                                                    $("#bidval"+k).html((parseFloat(num)).toFixed(3));
//                                                //if form type is salvage need to subtract from grandsum
//                                                    if($('#formType'+k).val()=='BOQsalvage')
//                                                        {
//                                                          sum = sum -parseFloat(num);
//                                                          continue;
//
//                                                        }
//                                                 //if form type normal need to add to grandsum
//                                                    sum = sum +parseFloat(num);
//                                                }
//
//                                                $("#sum").html(parseFloat(sum).toFixed(3));
//                                                updateTips("Done");
//                                                $("#dialog-form").dialog( "close" );
//                                                $("#decrypt").attr("disabled","true");
                                                document.getElementById('frmBidSubmit').submit();
                                            }else{ 
                                                updateTips("Please enter valid password"); 
                                                $('#password').val() = ""; 
                                            } 
                                        });
                                    } 
                                } 
                            }
                        },
                        close: function() {
                            allFields.val("").removeClass("ui-state-error");
                        }
                    });
                });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                        String userId = "";
                        String actionName = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            //userId = Integer.parseInt(session.getAttribute("userId").toString());
                            userId = session.getAttribute("userId").toString();
                        }
                        String tenderId = "";
                        if (request.getParameter("tenderId") != null) {
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            tenderId = request.getParameter("tenderId");
                        }
                        String lotId = "";
                        if(request.getParameter("lotId")!=null){
                            lotId = request.getParameter("lotId").toString();
                        }

                        actionName = "GrandSumm.jsp?tenderId="+tenderId+"&lotId="+lotId;

                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(tenderId);
                        String auditAction="View Grand Summary";
                        String moduleName=EgpModule.Bid_Submission.getName();
                        String remarks="Bidder (User Id): "+session.getAttribute("userId") +" has viewed Grand Summary for Tender id: "+tenderId;
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                       
                                                                
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">Grand Summary<span style="float:right;"><a class="action-button-goback" href="LotTendPrep.jsp?tab=4&tenderId=<%=tenderId%>">Go back to Dashboard</a></span></div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div class="t_space" >&nbsp;</div>                    
                    <div class="tabPanelArea_1">
                        <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="<%=actionName%>">
                            <script type="text/javascript">
//                            deployJava.runApplet(
//                            {
//                                codebase:"/",
//                                archive:"Signer.jar",
//                                code:"com.cptu.egp.eps.SignerAPI.class",
//                                width:"0",
//                                Height:"0",
//                                ID: "SignerAPI",
//                                classloader_cache: "false"
//                            },
//                            null,
//                            "1.6"
//                        );
                            </script>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td colspan="4" class="t-align-left ff">Grand Summary</td>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-left ff">Sl. No.</th>
                                    <th width="53%" class="t-align-left">Form Name</th>
                                    <th width="21%" class="t-align-left">Form Type</th>
                                    <th width="22%" class="t-align-left ff">Amount in Figure (IN Nu.)</th>
                                </tr>
                                <%      int srno = 1;
                                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                            for (SPCommonSearchData details : commonSearchService.searchData("getGrandSum", tenderId, userId, lotId, null, null, null, null, null, null)) {
                                %>
                                <tr class="<%if(srno%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                    <td class="t-align-center"><%=srno%></td>
                                    <td class="t-align-left"><%=details.getFieldName1()%></td>
                                     <td class="t-align-center"><span name="formType_<%=srno%>" id="formType_<%=srno%>"><% if("BOQsalvage".equalsIgnoreCase(details.getFieldName3())){ %><input type="hidden" name="formType<%=srno%>" id="formType<%=srno%>" value="BOQsalvage" />Discount form<% }else{ %><input type="hidden" name="formType<%=srno%>" id="formType<%=srno%>" value="Normal" />-<%  } %></span></td>
                                    <td style="text-align: right;">
                                        <span id="bidval<%=srno%>"><%=details.getFieldName2()%></span>
                                        <input id="bidval<%=srno%>" name="bidval<%=srno%>" type="hidden" value="<%=details.getFieldName2()%>" />
                                    </td>
                                 </tr>
                                <% srno++;
                                        }%>
                                <input type="hidden" value="<%=srno - 1%>" id="cnt" name="cnt"/>
                                <tr>
                                    <td colspan="3" class="t-align-right ff">Grand Total : </td>
                                    <td style="text-align: right"><span id="sum"></span></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="t-align-left ff">
                                        <label class="bidFormBtn_1" id="lblDecrypt">
                                            <input type="button" name="decrypt" id="decrypt" value="Decrypt" onClick="return decryptNVerify()"/>
                                        </label>
                                    </td>
                                </tr>
                            </table>

                                <input type="hidden" name="hdnPwd" id="hdnPwd" value="" />
                        </form>
                    </div>                    
                </div>
                <div id="dialog-form" title="Verify Password">
                    <label for="password">Password : </label>
                    <input type="password" name="password" id="password" value="" class="formTxtBox_1" autocomplete="off" />
                </div>                
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        $(document).ready(function() {

             if(<%=isSigned%>){
//                 for(var i=1; i<=<%=cnt%>;i++){
//                    $("#bidval"+i).html((parseFloat(<%=encryptedData[0]%>)).toFixed(3));
//                 }
            var jsArray = new Array();
            <% for(int i=1; i<=cnt; i++){%>
                $("#bidval"+<%=i%>).html(<%=encryptedData[i] %>);
            <%}%>

             $("#sum").html(parseFloat(<%=sum%>).toFixed(3));
             if(document.getElementById("decrypt")!=null)
                document.getElementById("decrypt").style.display ="none";
             if(document.getElementById("lblDecrypt")!=null)
                document.getElementById("lblDecrypt").style.display ="none";
             updateTips("Done");


            }
        });

    </script>

</html>
