<%--
    Document   : WorkScheduleUploadDoc
    Created on : Jan 9, 2012, 11:07:54 AM
    Author     : dixit
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvWrkSchDoc"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetailDocs"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ProgressReportUploadDocService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Download Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        
    </head>
    <body onload="getQueryData();">
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    String tenderId = "";
                    String keyId = "";
                    String wpId = "";
                    String lotId = "";
                    int varOrdId = 0;
                    String isedit = "";
                    if (session.getAttribute("userId") == null) {
                        response.sendRedirect("SessionTimedOut.jsp");
                    }
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    if (request.getParameter("keyId") != null) {
                        keyId = request.getParameter("keyId");
                    }
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }
                    if (request.getParameter("lotId") != null) {
                        lotId = request.getParameter("lotId");
                    }
                    if (request.getParameter("varOrdId") != null) {
                        varOrdId = Integer.parseInt(request.getParameter("varOrdId"));
                    }
                    if (request.getParameter("isedit") != null) {
                        isedit = request.getParameter("isedit");
                    } 
                    /*ProgressReportUploadDocService prudS = (ProgressReportUploadDocService) AppContext.getSpringBean("ProgressReportUploadDocService");                    
                    List<Object[]> prViewList = null;*/
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                    ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="dashboard_div">
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            Downlaod Document
                            <span style="float: right; text-align: right;">         
                                <a class="action-button-goback" href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=varOrdId%>&flag=<%=request.getParameter("flag")%>" title="Go back">Go Back</a>
                            </span>
                        </div>
                        <div class="t_space"></div>
                        <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/WorkSchUploadDocServlet?funName=Upload" enctype="multipart/form-data" name="frmUploadDoc">
                            <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                            <input type="hidden" name="keyId" value="<%=keyId%>" />
                            <input type="hidden" name="wpId" value="<%=wpId%>" />
                            <input type="hidden" name="lotId" value="<%=lotId%>" />
                            <input type="hidden" name="varOrdId" value="<%=varOrdId%>"/>
                            <input type="hidden" name="isedit" value="<%=isedit%>" />
                            <input type="hidden" name="docx" value="<%=request.getParameter("docx")%>" />
                            <input type="hidden" name="module" value="<%=request.getParameter("module")%>" />
                        </form>    
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center">Sl.  No.</th>
                                <th class="t-align-center" width="23%">File Name</th>
                                <th class="t-align-center" width="32%">File Description</th>
                                <th class="t-align-center" width="7%">File Size <br />
                                    (in KB)</th>
                                <th class="t-align-center" width="">Uploaded By</th>
                                <th class="t-align-center" width="18%">Action</th>
                            </tr>
                            <%
                                List<TblCmsSrvWrkSchDoc> getSrvWrkSchDoc = cmss.getWorkScheduleDocsDetails(Integer.parseInt(keyId),varOrdId);
                                if (!getSrvWrkSchDoc.isEmpty()) {
                                    for (int i = 0; i < getSrvWrkSchDoc.size(); i++) {
                            %>
                            <tr>
                                <td class="t-align-center"><%=(i + 1)%></td>
                                <td class="t-align-left"><%=getSrvWrkSchDoc.get(i).getDocumentName()%></td>
                                <td class="t-align-left"><%=getSrvWrkSchDoc.get(i).getDocDescription()%></td>
                                <td class="t-align-center"><%=(Long.parseLong(getSrvWrkSchDoc.get(i).getDocSize()) / 1024)%></td>
                                <td class="t-align-center">
                                        <%
                                        if (2 == getSrvWrkSchDoc.get(i).getUserTypeId()) {
                                            out.print("Supplier");
                                        }else{
                                            out.print("PE Officer");
                                        }%>
                                </td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/WorkSchUploadDocServlet?docName=<%=getSrvWrkSchDoc.get(i).getDocumentName()%>&docSize=<%=getSrvWrkSchDoc.get(i).getDocSize()%>&tenderId=<%=tenderId%>&WsDocId=<%=getSrvWrkSchDoc.get(i).getWsDocId()%>&keyId=<%=keyId%>&prRepId=<%=getSrvWrkSchDoc.get(i).getSrvFormMapId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&varOrdId=<%=varOrdId%>&docx=<%=request.getParameter("docx")%>&module=<%=request.getParameter("module")%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                    &nbsp;
                                    <%if (2 == getSrvWrkSchDoc.get(i).getUserTypeId()) {%>
<!--                                    <a href="<%=request.getContextPath()%>/WorkSchUploadDocServlet?&docName=<%=getSrvWrkSchDoc.get(i).getDocumentName()%>&docSize=<%=getSrvWrkSchDoc.get(i).getDocSize()%>&tenderId=<%=tenderId%>&WsDocId=<%=getSrvWrkSchDoc.get(i).getWsDocId()%>&keyId=<%=keyId%>&prRepId=<%=getSrvWrkSchDoc.get(i).getSrvFormMapId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&varOrdId=<%=varOrdId%>&docx=<%=request.getParameter("docx")%>&module=<%=request.getParameter("module")%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>-->
                                    <%}%>
                                </td>
                            </tr>
                            <%
                                                                                                                   }} else {%>
                            <tr>
                                <td colspan="6" class="t-align-center">No records found.</td>
                            </tr>
                            <%}%>
                        </table>

                        <div>&nbsp;</div>
                    </div>
                </div></div></div>
                <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

