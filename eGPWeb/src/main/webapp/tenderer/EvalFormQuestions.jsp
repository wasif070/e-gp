<%-- 
    Document   : EvalFormQuestions
    Created on : Dec 31, 2010, 4:04:03 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
<body>
<div class="dashboard_div">

          <%
      BidderClarificationSrBean srBean = new BidderClarificationSrBean();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
      
      String tenderId="", pkgLotId="0", formId="", userId="", frmNm="";
      String strCondition="";
      String strComments="", lastDt="";
      boolean chkSuccess = false;
      boolean isReplied=false, canReply=false, isClarificationCompleted=false;
      
      if (session.getAttribute("userId") != null) {
            userId = session.getAttribute("userId").toString();
      }

      if (request.getParameter("tenderId")!=null){
            tenderId=request.getParameter("tenderId");
        }

      if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
            pkgLotId=request.getParameter("lotId");
      }

      if (request.getParameter("formId")!="" && !request.getParameter("formId").equalsIgnoreCase("null")){
            formId=request.getParameter("formId");
      }

      EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
      int evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tenderId").toString()));
      List<SPTenderCommonData> lstCondition = srBean.getBidderClarificationInfo(tenderId, userId);

      if (!lstCondition.isEmpty() && lstCondition.size()>0) {
          if (!"null".equalsIgnoreCase(lstCondition.get(0).getFieldName1())){
              if (Integer.parseInt(lstCondition.get(0).getFieldName1())>0){
                canReply=true;
              }
          }

          if ("yes".equalsIgnoreCase(lstCondition.get(0).getFieldName2())){
              isClarificationCompleted=true;
          }

           if ("yes".equalsIgnoreCase(lstCondition.get(0).getFieldName3())){
              isReplied=true;
          }
          strComments=lstCondition.get(0).getFieldName4();
          lastDt=lstCondition.get(0).getFieldName5();
          
          
      }

      if (request.getParameter("btnSubmit") != null){
         
          String ctrlHidden="hdnQuestionId_", ctrlTxtArea="txtAnswer_";
          String QuesId="", strAns="";
          int intCounter=0; 
          int QuesCount=0;

        QuesCount= Integer.parseInt(request.getParameter("hdnQuestionCnt"));                 
        for(intCounter=1;intCounter<=QuesCount;intCounter++){
                QuesId = request.getParameter(ctrlHidden+intCounter);
                strAns = request.getParameter(ctrlTxtArea+intCounter);
                String s_tendRefNo = request.getParameter("tenRefNo");
                String s_userName = request.getParameter("userName");
                 tenderId = request.getParameter("tenderId");
                
                if (strAns!=null && QuesId!=null){
                    if(!"".equalsIgnoreCase(strAns.trim()) && !"null".equalsIgnoreCase(strAns.trim())){                        
                        srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        chkSuccess = srBean.updAnswers(handleSpecialChar.handleSpecialChar(strAns.trim()), QuesId,tenderId,s_tendRefNo,s_userName);

                    }
                }
        }

         if (chkSuccess){
            response.sendRedirect("EvalBidderClari.jsp?tenderId="+ tenderId + "&msgId=success");
         } else {
            response.sendRedirect("EvalFormQuestions.jsp?tenderId="+ tenderId + "&lotId=" + pkgLotId + "&formId=" + formId + "&msgId=error");
         }
      }

     %>

  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
      <%
                String referer = "";
                    if (request.getHeader("referer") != null) {
                        referer = "EvalBidderClari.jsp?tenderId=" + tenderId; //request.getHeader("referer");
                    }
                %>
      <div class="pageHead_1">Evaluation Clarification
          <span style="float:right;">
              <a href="<%=referer%>" class="action-button-goback">Go back</a>
          </span>
      </div>

      <%
           // Variable tenderId is defined by u on ur current page.
           pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
      %>
      <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <%pageContext.setAttribute("tab", "6");%>
      <%@include file="../tenderer/TendererTabPanel.jsp" %>
   <%if(!is_debared){%>
  <div class="tabPanelArea_1">
       <%if (request.getParameter("msgId")!=null){
        String msgId="", msgTxt="";
        boolean isError=false;
        msgId=request.getParameter("msgId");
        if (!msgId.equalsIgnoreCase("")){
            if(msgId.equalsIgnoreCase("success")){
               msgTxt="Answers updated successfully.";
           } else if(msgId.equalsIgnoreCase("error")){
               isError=true; msgTxt="There was some error.";
           }  else {
               msgTxt="";
           }
        %>
       <%if (isError){%>
            <div class="responseMsg errorMsg"><%=msgTxt%></div>
       <%} else {%>
            <div class="responseMsg successMsg"><%=msgTxt%></div>
       <%}%>
    <%}}%>

  <%-- Start: Sub Panel --%>
    <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="1" />
        <jsp:param name="tenderId" value="<%=tenderId%>" />
     </jsp:include>
   <%-- End: Sub Panel --%>

   


      <table width="100%" cellspacing="0" class="tableList_1">
          <tr>
              <th colspan="2" class="t-align-left">Tender Details</th>
          </tr>
          <%
            List<SPTenderCommonData> PackageList = srBean.getLotOrPackageData(tenderId, "Package,1");

            if (!PackageList.isEmpty() && PackageList.size()>0) {
          %>
          <tr>
              <td width="16%" class="t-align-left ff">Package  No. :</td>
              <td width="84%" class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
          </tr>
          <tr>
              <td class="t-align-left ff">Package Description :</td>
              <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
          </tr>
          <%}%>
      </table>

      <table width="100%" cellspacing="0" class="tableList_1">
          <tr>
              <td width="16%" class="t-align-left ff">Last Date of Response :</td>
              <td width="84%" class="t-align-left"><%=lastDt%></td>
          </tr>
          <tr>
              <td width="16%" class="t-align-left ff">Remarks :</td>
              <td width="84%" class="t-align-left"><%=strComments%></td>
          </tr>
      </table>

     


          <%if (!"0".equalsIgnoreCase(pkgLotId)){%>
               <div>&nbsp;</div>
               <table width="100%" cellspacing="0" class="tableList_1">
                  <%
                    List<SPTenderCommonData> LotList = srBean.getLotOrPackageData(tenderId, "Lot," + pkgLotId);
                    if (!LotList.isEmpty() && LotList.size()>0) {
                  %>
                  <tr>
                      <td width="12%" class="t-align-left ff">Lot No. :</td>
                      <td width="88%" class="t-align-left"><%=LotList.get(0).getFieldName1()%></td>
                  </tr>
                  <tr>
                      <td class="t-align-left ff">Lot Description :</td>
                      <td class="t-align-left"><%=LotList.get(0).getFieldName2()%></td>
                  </tr>
                 <%}%>
              </table>
          <%}%>

          <form id="frmEvalFormQuestions" action="EvalFormQuestions.jsp?tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&formId=<%=formId%>" method="POST">
              <input type="hidden" name="tenRefNo" value="<%=toextTenderRefNo%>" />
              <input type="hidden" name="userName" value="<%=objUserName%>" />
              <input type="hidden" name="tenderId" value="<%=tenderId%>" />
             
                  <table width="100%" class=" t_space b_space">
                      <tr>
                          <td align="left;">
                              <div class="table-section-header_title t_space b_space">
                                  <p><strong>Form Name :  <%=srBean.getFormName(formId)%></strong></p>
                              </div>
                          </td>
                          <td align="right">
                          <%if(canReply && !isClarificationCompleted){%>
                                 <a class="action-button-upload" href="../resources/common/evalClariDoc.jsp?tenderId=<%=tenderId%>&formId=<%=formId%>&pId=bid">Upload Document</a>
                            <%}%>
                              <%--<a class="action-button-download l_space" onclick="FocusDocs()">Download Docs.</a>--%>
                          </td>
                      </tr>
                  </table>
              <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
              <tr>
                  <th width="5%" class="ff">Sl. No.</th>
                  <th width="40%" class="ff">Query</th>
                  <th width="45%">Clarification</th>
              </tr>
          <%
                if("0".equalsIgnoreCase(pkgLotId)){
                    strCondition="userId=" + userId + " And tenderId=" + tenderId + " And pkgLotId=" + pkgLotId + " And tenderFormId=" + formId + " And queSentByTec = 1 and evalCount=" + String.valueOf(evalCount);
                } else {
                    strCondition="userId=" + userId + " And tenderId=" + tenderId + " And tenderFormId=" + formId + " And queSentByTec = 1 and evalCount=" + String.valueOf(evalCount);
                }

            int QusCnt=0;
            List<SPTenderCommonData> QuestionList = srBean.getFormQuestions(strCondition);
            if (!QuestionList.isEmpty() && QuestionList.size()>0){

                for (SPTenderCommonData Question : QuestionList){
                    QusCnt++;
          %>
          <tr>
              <td><%=QusCnt%></td>
              <td><%=URLDecoder.decode(Question.getFieldName6(),"UTF-8")%></td>
              <td>
                  <textarea <%if (!canReply || isClarificationCompleted) {%>style="width: 500px; display: none;"<%} else {%>style="width: 500px;"<%}%> onfocus="javascript:return removeSpace(this)" id="txtAnswer_<%=QusCnt%>" name="txtAnswer_<%=QusCnt%>" onload="javascript:return removeSpaceOnLoad(this)" maxlength="500" class="formTxtBox_1" ><%=Question.getFieldName7()%></textarea>
                  <%if (!canReply || isClarificationCompleted) {%>
                  <label><%=URLDecoder.decode(Question.getFieldName7(),"UTF-8")%></label>
                  <%}%>
                  <span id="errMsg_<%=QusCnt%>" class='reqF_1'></span>
                  <input type="hidden" id="hdnQuestionId_<%=QusCnt%>" name="hdnQuestionId_<%=QusCnt%>" value="<%=Question.getFieldName1()%>"></input>
              </td>
          </tr>
          <%} if (QusCnt==0){%>
                <tr>
                    <td colspan="3">No Queries Found</td>
                </tr>
              <%} else {%>
                <tr <%if (!canReply || isClarificationCompleted) {%>style="display: none;"<%}%>>
                <td class="t-align-center" colspan="3">
                    <label class="formBtn_1">
                        <input <%if (!canReply || isClarificationCompleted) {%>style="display: none;"<%}%> name="btnSubmit" id="btnSubmit" type="submit" value="Save" onclick="javascript: return validateSubmit(this)" />
                        <input type="hidden" id="hdnQuestionCnt" name="hdnQuestionCnt" value="<%=QusCnt%>"></input>
                    </label>
                    <span id="errMsg" class='reqF_1'></span>
                </td>
            </tr>
              <%}
            } else {%>
            <tr>
                <td colspan="3">No Queries Found</td>
            </tr>
          <%}%>
          </table>
          </form>
          
<!--          <div style="height:  500px;">Hello</div>          -->

          <div id="Docs" class="table-section-header_title t_space b_space">
              <p><strong>Document List</strong></p>
          </div>

        
<table width="100%" cellspacing="0" class="tableList_1 t_space">
    <tr>
        <th width="8%" class="t-align-left" width="4%">Sl.  No.</th>
        <th class="t-align-left" width="35%">File Name</th>
        <th class="t-align-left" width="41%">File Description</th>
        <th class="t-align-left" width="10%">File Size <br />
            (in KB)</th>
        <th class="t-align-left" width="10%">Action</th>
    </tr>
    <%

                int docCnt = 0;
                CommonSearchService CommonSD = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                String userid = session.getAttribute("userId").toString();
               // boolean bole_flag = false;
                for (SPCommonSearchData spcsd : CommonSD.searchData("EvalClariDocs", tenderId, formId, userid, null, String.valueOf(evalCount), null, null, null, null)) {
                    docCnt++;

    %>
    <tr>
        <td class="t-align-center"><%=docCnt%></td>
        <td class="t-align-left"><%=spcsd.getFieldName1()%></td>
        <td class="t-align-left"><%=spcsd.getFieldName2()%></td>
        <td class="t-align-center"><%=(Long.parseLong(spcsd.getFieldName3()) / 1024)%></td>
        <td class="t-align-center">
            <a href="<%=request.getContextPath()%>/ServletEvalClariDocs?docName=<%=spcsd.getFieldName1()%>&docSize=<%=spcsd.getFieldName3()%>&tenderId=<%=tenderId%>&formId=<%=formId%>&funName=download&fs=<%=spcsd.getFieldName7()%>&docType=<%=spcsd.getFieldName5()%>&evalCount=<%=evalCount%>&action=removeDoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
            <%
                    if(userid.equalsIgnoreCase(spcsd.getFieldName6())){
            %>
            | <a onclick="return window.confirm('Do you really want to delete this file?');" href="<%=request.getContextPath()%>/ServletEvalClariDocs?&docName=<%=spcsd.getFieldName1()%>&docId=<%=spcsd.getFieldName4()%>&tenderId=<%=tenderId%>&formId=<%=formId%>&pId=ef&funName=remove&fs=<%=spcsd.getFieldName7()%>&docType=<%=spcsd.getFieldName5()%>&evalCount=<%=evalCount%>&action=tenderer"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
            <%}%>
        </td>
    </tr>
    <%   if (spcsd != null) {
             spcsd = null;
         }
    }//Officer Loop Ends here
     %>
    <% if (docCnt == 0) {%>
    <tr>
        <td colspan="5" class="t-align-center">No records found.</td>
    </tr>
    <%}%>
</table>

          <input id="txtDocFocus" type="text" style="height: 0px; width: 0px; border: 0px;"></input>
  </div><%}%>
    <div>&nbsp;</div>

    

    </div>
    <!--Dashboard Content Part End-->

    <!--Dashboard Footer Start-->
   <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    <!--Dashboard Footer End-->


</div>

    <script type="text/javascript" language="javascript">

        function FocusDocs(){
            //alert('Hi');
            document.getElementById("txtDocFocus").style.display="block";
            document.getElementById("txtDocFocus").focus();
            document.getElementById("txtDocFocus").style.display="none";
        }

          function trimString(str) {
         str = this != window ? this : str;
         return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
     }


        function removeSpace(obj){
            //alert(obj.type)
            String.prototype.trim = trimString;

            var errId=document.getElementById(obj).id.replace("txtAnswer", "errMsg");
            document.getElementById(errId).innerHTML="";

            if (document.getElementById(obj.id).innerHTML.trim()==''){
                document.getElementById(obj.id).innerHTML='';
                //document.getElementById(obj.id).focus();
            } else {
               document.getElementById(obj.id).value=document.getElementById(obj.id).value.trim();
            }
        }

        function removeSpaceOnLoad(obj){
            //alert(obj.type)
            String.prototype.trim = trimString;
            if (document.getElementById(obj.id).innerHTML.trim()==''){
                document.getElementById(obj.id).innerHTML='';
                //document.getElementById(obj.id).focus();
            } else {
               document.getElementById(curId).value=document.getElementById(curId).value.trim();
            }
        }
    
     
      function validateSubmit(obj){
          String.prototype.trim = trimString;
          document.getElementById("errMsg").innerHTML="";

          var elm = obj.form.elements;
          var ansCount=0;          
          var errFlag=0;
            for (i = 0; i < elm.length; i++) {
                if (elm[i].type=="textarea"){
                    var curId = elm[i].id
                    //alert(curId);
                    //alert(document.getElementById(curId).value);
                    if (document.getElementById(curId).value.trim()!=''){
                        ansCount=ansCount+1;
                        //alert(document.getElementById(curId).value.length);
                        if (document.getElementById(curId).value.length>500){
                            //alert(document.getElementById(curId).id)
                            errFlag=errFlag+1;
                            var errId=document.getElementById(curId).id.replace("txtAnswer", "errMsg")                            
                            document.getElementById(errId).innerHTML="Maximum 500 characters are allowed."
                        }                        
                    }
                }
            }
            //alert(ansCount);
            if (ansCount==0){
              document.getElementById("errMsg").innerHTML="<br/>Enter answer for atleast one question.";
              return false;
            } else if (errFlag>0){
                return false;
            }
      }
  </script>
</body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
