<%-- 
    Document   : BidWithdrawal
    Created on : Dec 13, 2010, 6:35:24 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.FinalSubmissionService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page  import="com.cptu.egp.eps.service.serviceinterface.TenderBidService"%>
<%@page  import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService "%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%if ("w".equals(request.getParameter("bid")))
            {
                out.print("Bid Withdrawal");
            }
            else
            {
                out.print("Bid Substitution");
            }%></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" /> 
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmBid").validate({
                    rules: {
                        bidReason:{required:true,maxlength: 300},
                        txtOTP:{required:true}
                    },
                    messages: {

                        bidReason:{required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 300 Characters are allowed.</div>"},
                        txtOTP:{required: function(){ document.getElementById("msgtxtOTP").innerHTML = "Please Enter OTP" ;}}
                    }

                });
            });
            function showConfirmMsg(){
                if($("#frmBid").valid()){
                    if($('#bidMod').val()=="Withdrawal"){
                        if(confirm("You will not be able to resubmit your Tender again. Still want to withdraw?")){
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return true;
                    }
                }            
            }  
        </script>
    </head>
    <body onload="UpdateOTP();">
        <%
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "tenderId";
            int auditId = Integer.parseInt(request.getParameter("tenderid"));
            String auditAction = null;
            String moduleName = EgpModule.Bid_Submission.getName();
            String remarks = null;

            Logger LOGGER = Logger.getLogger("BidWithdrawal.jsp");
            String tenderid = request.getParameter("tenderid");
             String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
            String OTP = request.getParameter("txtOTP") ;
            if(OTP != null && request.getParameter("bidMod") != null){
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> list = null;
                        list = commonSearchService.searchData("findOTP", null, tenderid, userId, null, null, null, null, null, null);
                        if(Integer.parseInt(list.get(0).getFieldName2()) > 5) {
                            response.sendRedirect("BidWithdrawal.jsp?tenderid="+tenderid+"&timeLimit=exceed");
                            return;
                        }
                       else if(! list.get(0).getFieldName1().equals(OTP)){
                            response.sendRedirect("BidWithdrawal.jsp?tenderid="+tenderid+"&wrongAttempt=true");
                            return;
                        } 
                        else {
                        list = commonSearchService.searchData("deleteOTP", null, tenderid, userId, null, null, null, null, null, null);
                       }
                }        
            TenderBidService tenderBidService = (TenderBidService) AppContext.getSpringBean("TenderBidService");
            FinalSubmissionService finalSubmissionService = (FinalSubmissionService) AppContext.getSpringBean("FinalSubmissionService");
            pageContext.setAttribute("tenderId", tenderid);
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null)
            {
                ipAddress = request.getRemoteAddr();
            }
            String msg = null;
            if ("Withdrawal".equalsIgnoreCase(request.getParameter("bidMod")))
            {
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                long archiveCnt = tenderCommonService1.tenderArchiveCnt(tenderid);
                if (archiveCnt != 0)
                {//archiveCnt != 0
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    String data[] = null;
                    SPTenderCommonData sptcd = null;

                    // Coad added by Dipal for Audit Trail Log.
                    auditAction = "Bidder has taken withdrowal";
                    remarks = request.getParameter("bidReason");
                    try
                    {
                        tenderBidService.updateFinalSubStatus(Integer.parseInt(tenderid), Integer.parseInt(session.getAttribute("userId").toString()), request.getParameter("bidReason"), false, ipAddress);
                        sptcd = tenderCommonService1.returndata("tenderinfobar", tenderid, null).get(0);

                        data = tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());
                        try
                        {
                            List<CommonSPReturn> cmr = finalSubmissionService.generateMegaHashWithdrwal(Integer.parseInt(tenderid), Integer.parseInt(session.getAttribute("userId").toString()));
                            if (cmr.get(0).getFlag())
                            {
                                LOGGER.debug("Success : " + cmr.get(0).getMsg());
                            }
                        }
                        catch (Exception e)
                        {
                            LOGGER.error("Error in Generate Mega Hash : " + e.toString());
                        }

                        String mails[] =
                        {
                            data[0]
                        };
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        userRegisterService.contentAdmMsgBox(data[0], XMLReader.getMessage("emailIdNoReply"), "e-GP Tender has been withdrawn successfully", msgBoxContentUtility.bidTenderSubMsg(tenderid, session.getAttribute("userId").toString(), "withdraw"));
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        String mailText = mailContentUtility.bidTenderSubMail(tenderid, session.getAttribute("userId").toString(), "withdraw");
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP Tender has been withdrawn successfully");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                        String tempMobNo = data[1] + data[2];
                        if (data[2].contains(data[1]))
                        {
                            tempMobNo = data[2];
                        }
                        sendMessageUtil.setSmsNo(tempMobNo);
                        //sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully withdrawn your bid for the below mentioned Tender%0ATender Id:"+tenderid+"%0ARef. No:"+sptcd.getFieldName2()+"%0Ae-GP System");
                        sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully withdrawn your bid for%0ATender Id:" + tenderid + "%0ARef. No:" + sptcd.getFieldName2() + "%0Ae-GP System");
                        sendMessageUtil.sendSMS();
                        data = null;
                        mails = null;
                        sendMessageUtil = null;
                        msgBoxContentUtility = null;

                    }
                    catch (Exception ex)
                    {
                        auditAction = "Error in Bidder has taken withdrowal " + ex.getMessage();
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    }
                    response.sendRedirect("LotTendPrep.jsp?tenderId=" + tenderid + "&msg=with");
                }
                else
                {
                    msg = "Closing date & time has been lapsed. You can't withdraw your tender now.";
                }
            }
            if ("Substitute / Modification".equalsIgnoreCase(request.getParameter("bidMod")))
            {
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                long archiveCnt = tenderCommonService1.tenderArchiveCnt(tenderid);
                if (archiveCnt != 0)
                {
                    // Coad added by Dipal for Audit Trail Log.
                    auditAction = "Bidder has done Substitution/Modification ";
                    remarks = request.getParameter("bidReason");
                    
                    try
                    {
                        tenderBidService.updateFinalSubStatus(Integer.parseInt(tenderid), Integer.parseInt(session.getAttribute("userId").toString()), request.getParameter("bidReason"), true, ipAddress);
                        SPTenderCommonData sptcd = tenderCommonService1.returndata("tenderinfobar", tenderid, null).get(0);
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        String data[] = tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());
                        try
                        {
                            List<CommonSPReturn> cmr = finalSubmissionService.generateMegaHashWithdrwal(Integer.parseInt(tenderid), Integer.parseInt(session.getAttribute("userId").toString()));
                            if (cmr.get(0).getFlag())
                            {
                                LOGGER.debug("Success : " + cmr.get(0).getMsg());
                            }
                        }
                        catch (Exception e)
                        {
                            LOGGER.error("Error in Generate Mega Hash : " + e.toString());
                            
                        }
                        String mails[] =
                        {
                            data[0]
                        };
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        userRegisterService.contentAdmMsgBox(data[0], XMLReader.getMessage("emailIdNoReply"), "e-GP Tender has been modified successfully", msgBoxContentUtility.bidTenderSubMsg(tenderid, session.getAttribute("userId").toString(), "modify"));
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        String mailText = mailContentUtility.bidTenderSubMail(tenderid, session.getAttribute("userId").toString(), "modify");
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP Tender has been modified successfully");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                        String tempMobNo = data[1] + data[2];
                        if (data[2].contains(data[1]))
                        {
                            tempMobNo = data[2];
                        }
                        sendMessageUtil.setSmsNo(tempMobNo);
                        //sendMessageUtil.setSmsBody("Dear User,%0AYou have decided to modify your tender/bid for the below mentioned Tender%0ATender Id:"+tenderid+"%0ARef. No:"+sptcd.getFieldName2()+"%0Ae-GP System");
                        sendMessageUtil.setSmsBody("Dear User,%0AYou have decided to modify following%0ATender Id:" + tenderid + "%0ARef. No:" + sptcd.getFieldName2() + "%0Ae-GP System");
                        sendMessageUtil.sendSMS();
                        data = null;
                        mails = null;
                        sendMessageUtil = null;
                        msgBoxContentUtility = null;
                    }
                    catch (Exception ex)
                    {
                        auditAction = "Error in Bidder has done Substitution/Modification "+ex.getMessage();
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    }
                    response.sendRedirect("LotTendPrep.jsp?tenderId=" + tenderid + "&msg=mod");
                }
                else
                {
                    msg = "Closing date & time has been lapsed. You can't modify your tender now.";
                }
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Tender Submission</div>
            <div class="mainDiv">               
                <div class="t-align-right"><a class="action-button-goback" href="LotTendPrep.jsp?tenderId=<%=tenderid%>">Go back to Dashboard</a></div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>                               
            </div>                        
            <div class="tabPanelArea_1">
                <%if (msg != null)
                    {%>
                <div class="responseMsg errorMsg"><%=msg%></div>
                <%}%>
                <form action="BidWithdrawal.jsp?tenderid=<%=tenderid%>&bid=<%=request.getParameter("bid")%>" method="post" id="frmBid" name="frmBid">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td>
                                Reason <span class="mandatory">*</span>:
                            </td>
                            <td>
                                <textarea cols="100" rows="5" id="txtaReason" name="bidReason" class="formTxtBox_1"></textarea>
                            </td>
                        </tr>
                        <tr>                     
                            <td colspan="2" class="ff t-align-center" width="23%"> OTP : <span class="mandatory">*</span> &nbsp;    
                                <input name="txtOTP" type="text" class="formTxtBox_1" id="txtOTP" onblur="checkOTP();"  style="width:150px;" /> &nbsp;
                                <span style = "color : red;" id="msgtxtOTP">
                                    <%  if(request.getParameter("timeLimit") != null && request.getParameter("timeLimit").equals("exceed")){
                                          out.print("OTP Time Limit Exceed");
                                          %>
                                          <a href="#" style="color:green; padding-left: 10px; text-decoration: none;" onclick="reUpdateOTP();">Resend OTP</a>
                                        <% }
                                        else if(request.getParameter("wrongAttempt") != null && request.getParameter("wrongAttempt").equals("true")){
                                            out.print("Please Enter Valid OTP");
                                        }
                                    %>
                                </span>
                            </td> 
                        </tr>
                        <tr>
                            <td colspan="2" class="t-align-center ">
                                <label class="formBtn_1"><input name="bidMod" id="bidMod" type="submit" value="<%if ("w".equals(request.getParameter("bid")))
                                {
                                    out.print("Withdrawal");
                                }
                                else
                                {
                                    out.print("Substitute / Modification");
                                }%>" onclick="return showConfirmMsg();"/></label>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>            
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>            
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function checkOTP(){
            if (document.getElementById("txtOTP").value == '' ){
                document.getElementById("msgtxtOTP").innerHTML = "Please Enter OTP" ;
            }
            else{
                document.getElementById("msgtxtOTP").innerHTML = "" ;  
            } 
        }
            
        function UpdateOTP(){ 
            var update = <%=request.getParameter("wrongAttempt")%>;
            var timelimit = '<%=request.getParameter("timeLimit")%>';

            if (timelimit != 'null' && timelimit == 'exceed' ){
               jAlert("OTP Time Limit Exceed","Time Limit","");
            }
            else if (update != null && update == true ){
               jAlert("Please Enter Valid OTP","Wrong Attempt","");
            }
            else{
                $.post("<%=request.getContextPath()%>/CommonServlet", {event:'withdrawal',tenderid:<%=tenderid%>, userid:<%=userId%>,funName: 'updateOTP'}, function (j) {
                    var data = j.split("#") ;
                    if(data[0] == 'Success'){            
                      // $("#txtOTP").val(data[1]);
                       jAlert("OTP sent to your mobile no / email address for substitute / modification / withdrawal","Input OTP","");
                     }
                     else{
                         //$("#txtOTP").val(data[1]);
                     }
                });
            }
        }
        
        function reUpdateOTP(){
            $.post("<%=request.getContextPath()%>/CommonServlet", {event:'withdrawal',tenderid:<%=tenderid%>, userid:<%=userId%>,funName: 'updateOTP'}, function (j) {
                    var data = j.split("#") ;
                    if(data[0] == 'Success'){            
                       //$("#txtOTP").val(data[1]);
                        document.getElementById("msgtxtOTP").innerHTML = "" ;  
                        jAlert("OTP sent to your mobile no / email address for substitute / modification / withdrawal","Input OTP","");
                     }
                     else{
                         //$("#txtOTP").val(data[1]);
                     }
                });
        }
    </script>
</html>
