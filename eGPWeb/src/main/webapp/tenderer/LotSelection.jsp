<%-- 
    Document   : LotSelection
    Created on : Mar 22, 2011, 11:27:50 AM
    Author     : Administrator
This page is for updating and inserting of LotSection....
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblBidderLots"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean" scope="page" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int userId = Integer.parseInt(session.getAttribute("userId").toString());
                    List<TblBidderLots> forUpdateList = new ArrayList<TblBidderLots>();
                    String lot_tab = "";
                    String action = "add";
                    if(request.getParameter("tab")!=null){
                                lot_tab = request.getParameter("tab");
                       }
                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData>  list_lot = tenderCS.returndata("getLotDescriptonForDocViewMore", tenderId+"", userId+"");/*For populate info from tbl_tenderlotSecurity*/
                    if("4".equalsIgnoreCase(lot_tab)){
                     forUpdateList = tenderSrBean.viewBidderLots(tenderId, userId);/*Data from tbl_bidderLots...For updating purpose*/
                         if(forUpdateList!=null && !forUpdateList.isEmpty()){
                                action = "update";
                        }
                    }
                    
                    
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <% if("4".equalsIgnoreCase(lot_tab)){%>
            Lot Section
            <%}else{%>
            Select Lots for Sub Contracting
            <%}%>
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
         <div class="dashboard_div">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <div class="contentArea_1">
        <div class="pageHead_1">
            <% if("4".equalsIgnoreCase(lot_tab)){%>
            Lot Section
            <%}else{%>
            Select Lots for Sub Contracting
            <%}%>
            <span style="float:right;"><a href="TendererDashboard.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back to Dashboard</a></span>
        </div>
                <%
                        pageContext.setAttribute("tenderId", tenderId);
                %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        
        <% pageContext.setAttribute("tab", lot_tab);%>
        
        <%@include file="TendererTabPanel.jsp"%>
        
        <div class="tabPanelArea_1">
            <%if("4".equalsIgnoreCase(lot_tab)){%>
            <form name="lotSectionDetail" action="<%=request.getContextPath()%>/LotSelectionServlet" method="post">
                <input type="hidden" value="<%=action%>" name="action" id="hdnAction"/>
                <%}else{%>
                <form name="lotSectionDetail" action="<%=request.getContextPath()%>/ServletSubContracting" method="post">
                    <input type="hidden" value="lotWise" name="funName" />
                <%}%>
                <input type="hidden" value="<%=tenderId%>" name="tenderId" />
                <table class="tableList_1" cellspacing="0" width="100%">
                    <tr>
                        <!--                        <th class="t-align-center">Select</th>-->
                        <th class="t-align-center">Sl. No.</th>
                        <th class="t-align-center">Lot No.</th>
                        <th class="t-align-center">Lot Description</th>
                        <th class="t-align-center">Action</th>
                    </tr>
                    <%int i = 0;/*Loops for itreating the record*/
                        for(SPTenderCommonData tblTenderLotSecurity: list_lot){
                            i++;
                            /*Itreate list_lot*/
                    %>
                    <tr>
                        <td width="5%" class="t-align-center"><%=i%></td>
                        <td width="15%" class="t-align-center"><%=tblTenderLotSecurity.getFieldName1()%></td>
                        <td width="65%" class="t-align-center"><%=tblTenderLotSecurity.getFieldName2()%></td>
                        <td width="15%" class="t-align-center">
                            <%if("4".equalsIgnoreCase(lot_tab)){%>
                            <%if(!tblTenderLotSecurity.getFieldName4().equalsIgnoreCase("0")){%>
                        <%if(action.equalsIgnoreCase("add")){%>
                        <input type="checkbox" name="lotchkBox_<%=i%>" id="ckBox_lot_<%=i%>" value="<%=tblTenderLotSecurity.getFieldName3()%>"/>
                            <%}else{/*Loop for intreating the update record*/%>
                                
                            <input type="checkbox" <%for(TblBidderLots tblBidderLots : forUpdateList ){if(tblBidderLots.getPkgLotId()==Integer.parseInt(tblTenderLotSecurity.getFieldName3())){%>checked="checked"<%}}%> name="lotchkBox_<%=i%>" id="ckBox_lot_<%=i%>" value="<%=tblTenderLotSecurity.getFieldName3()%>"/>
                                   
                           <% }/*Ends for loop of TblBidderLots*/%>
                           <%}/*If Paymnet is not pending ends here */else{out.print("Payment Pending");}%>
                           <%}/*If Lot Selection for tenderPrer*/else{/*Lot Selection for Subcontracting*/%>
                           <a href="SubContracting.jsp?tenderid=<%=tenderId%>&lorpkgId=<%=tblTenderLotSecurity.getFieldName3()%>">Sub Contracting</a>
                           <%}%>
                        </td>
                        
                    <tr>
                            <%}/*Loop Ends of TblTenderLotSecurity*/%>
                    <input type="hidden" value="<%=i%>" id="counter" name="counter">
                    <%if("4".equalsIgnoreCase(lot_tab)){%>
                    <tr>
                        <td colspan="4" class="t-align-center">
                             <label class="formBtn_1">
                                 
                                 <input type="submit" Value="Submit" onclick="return validateFrm();">
                             </label>
                        </td>
                    </tr>
                    <%}%>
                </table>
            </form>
        </div>
        
<script type="text/javascript">
    /*Check atleast one lot is selected*/
    function validateFrm(){
        var cnt = document.getElementById("counter").value;
        //alert('Cnt '+cnt);
        var flag =false;
        for(i=1;i<=cnt;i++){
            var ckBox ="ckBox_lot_"+i;
          //  alert(ckBox+' CkBox');
            //alert('Conditiin '+document.getElementById(ckBox).checked);
            if(document.getElementById(ckBox).checked){
                flag = true;
                break;
            }
        }
        if(flag==false){
            jAlert("Select atleast One Record","Lot Section alert", function(RetVal) {
            });
            return false;            
        }        
        if(flag && $('#hdnAction').val()=="update"){
            return confirm('Do you really want to change the Lot selection?\nChanging the Lots which was selected earlier would delete Forms data.');
        }        
    }
</script>
<%--<script type="text/javascript">
    function hideRow(){
        document.getElementById("errMsg").innerHTML = '';
        document.getElementById("errMsg_Tr").style.display = 'none';
    }
</script>--%>
<div>&nbsp;</div>
</div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
         </div>
    </body>
</html>
<%
/*For null all the List*/
    if(list_lot!=null){
            list_lot.clear();
            list_lot = null;
        }
    if(forUpdateList!=null){
            forUpdateList.clear();
            forUpdateList = null;
        }
%>