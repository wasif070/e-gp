<%-- 
    Document   : NegoGrandSumm
    Created on : Dec 21, 2011, 11:28:08 AM
    Author     : dipal.shah
--%>

<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grand Summary</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />        
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>               
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>        
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                        String userId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            //userId = Integer.parseInt(session.getAttribute("userId").toString());
                            userId = session.getAttribute("userId").toString();
                        }
                        String tenderId = "";
                        if (request.getParameter("tenderId") != null) {
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            tenderId = request.getParameter("tenderId");
                        }
                        String negId = "";
                        if(request.getParameter("negId")!=null){
                            negId = request.getParameter("negId").toString();
                        }      
                        
                       BigDecimal sum= new BigDecimal("0");

                                                // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(tenderId);
                        String auditAction="View Grand Summary";
                        String moduleName=EgpModule.Negotiation.getName();
                        String remarks="Bidder (User Id): "+session.getAttribute("userId") +" has viewed Grand Summary for Tender id: "+tenderId+" for Negotiation Id: "+negId;
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                       
                                                                
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">Grand Summary<span style="float:right;"><a class="action-button-goback" href="NegTendererForm.jsp?tenderId=<%=tenderId%>&negId=<%=request.getParameter("negId")%>&uId=<%=userId%>" title="Bid Dashboard">Go Back To Dashboard</a></span></div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div class="t_space" >&nbsp;</div>                    
                    <div class="tabPanelArea_1">
                        <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="GetBidData.jsp">
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td colspan="3" class="t-align-left ff">Grand Summary</td>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-left ff">Sl. No.</th>
                                    <th width="74%" class="t-align-left">Form Name</th>
                                    <th width="22%" class="t-align-left ff">Amount in Figure (IN Nu.)</th>
                                </tr>
                                <%      int srno = 1;
                                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                            for (SPCommonSearchData details : commonSearchService.searchData("getGrandSumNego", tenderId, userId, "0", null, null, null, null, null, null)) 
                                            {
                                %>
                                <tr class="<%if(srno%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                    <td class="t-align-center"><%=srno%></td>
                                    <td class="t-align-left"><%=details.getFieldName1()%></td>
                                    <td style="text-align: right;"><span id="bidval<%=srno%>">
                                            <%
                                             if(details.getFieldName2() != null && !details.getFieldName2().equalsIgnoreCase("") && !details.getFieldName2().equalsIgnoreCase("null"))
                                                {
                                                 sum = sum.add(new BigDecimal(details.getFieldName2()).setScale(3,0));
                                                 out.println(new BigDecimal(details.getFieldName2()).setScale(3,0));
                                                }
                                            %>
                                        </span></td>
                                </tr>
                                <% srno++;
                                        }%>
                                <input type="hidden" value="<%=srno - 1%>" id="cnt"/>
                                <tr>
                                    <td colspan="2" class="t-align-right ff">Grand Total : </td>
                                    <td style="text-align: right"><span id="sum"><%=sum.setScale(3,0)%></span></td>
                                </tr>
                            </table>
                        </form>
                    </div>                    
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
