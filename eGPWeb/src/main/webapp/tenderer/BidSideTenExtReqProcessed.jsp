<%-- 
    Document   : BidSideTenExtReqProcessed
    Created on : Dec 2, 2010, 3:57:04 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

         
        
    </head>
    <body>
        <%
                    if (request.getParameter("btnhope") != null) {

                        String dtXml = "";
                        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


                        //String dt = request.getParameter("hdnlastaccept");
                        
                        //String[] dtArr = dt.split("/");
                        //dt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];

                        String userId = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userId = hs.getAttribute("userId").toString();
                                }

                        String table="", updateString = "", whereCondition="";
                        table="tbl_TenderValAcceptance";
                        updateString=" status='"+request.getParameter("select2")+"',tenderValAcceptDt='" + format.format(new Date())+"'";
                        whereCondition="valExtDtId=" + request.getParameter("ExtId") + " And userID="+ userId+ " And tenderId="+ request.getParameter("tenderId");

                        //out.println("update tbl_TenderValAcceptance set "+updateString+ " where "+whereCondition);
                        
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderValAcceptance", updateString, whereCondition).get(0);

                        if(commonMsgChk.getFlag()==true)
                        {
                            String refNo = request.getParameter("hdnRefNo");
                            TenderCommonService tcsForTenValSec = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> listUserId = tcsForTenValSec.returndata("CheckPE",request.getParameter("tenderId"),null);
                            String peUserId = "";
                            if(!listUserId.isEmpty()){
                                peUserId = listUserId.get(0).getFieldName1();
                            }
                            List<SPTenderCommonData> emails = tcsForTenValSec.returndata("getEmailIdfromUserId",peUserId,null);
                            String [] mail={emails.get(0).getFieldName1()};
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            String mailText = mailContentUtility.contentTenProSec(request.getParameter("tenderId"),refNo);
                            sendMessageUtil.setEmailTo(mail);
                            sendMessageUtil.setEmailSub("e-GP: Pretender meeting MOM published");
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();
                            sendMessageUtil=null;
                            mailContentUtility=null;
                            response.sendRedirect("BidSideTenExtReqList.jsp?tenderId="+request.getParameter("tenderId")+"&msg=true");
                        }else
                        {
                            response.sendRedirect("BidSideTenExtReqProcessed.jsp?tenderId="+request.getParameter("tenderId")+"&ExtId="+request.getParameter("ExtId")+"msg=false");
                        }
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Validity Extension Request
            <span class="c-alignment-right"><a href="BidSideTenExtReqList.jsp?tenderId=<%=request.getParameter("tenderId") %>"
                                                   class="action-button-goback">Go Back to Dashboard</a></span>
            </div>
            <%
                        String msg ="";
                        msg = request.getParameter("msg");
            %>
            <% if (msg != null && msg.contains("false")) {%>
            <div class="responseMsg errorMsg t_space">Validity Extension Request operation failed, Please try again later.</div>
            <%  }%>
            <div class="mainDiv">
                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>
            <div>&nbsp;</div>
           
            <form id="frmhope" action="BidSideTenExtReqProcessed.jsp?tenderId=<%=request.getParameter("tenderId") %>&ExtId=<%=request.getParameter("ExtId") %>" method="post">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <input type="hidden" value="<%=toextTenderRefNo%>" name="hdnRefNo"/>
                <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), null);
                            if (!list.isEmpty()) {
                %>
                    <tr>
                        <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : </td>
                        <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                    </tr>

                    <tr>
                        <td class="t-align-left ff">Last Date  of Tender Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New  Date of Tender Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName3()%></td>
                    </tr>
                    <% TenderSrBean tenderSrBeanForValSecTen = new TenderSrBean();
                    List<Object[]> objForChkingValSec = tenderSrBeanForValSecTen.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[2].toString().equalsIgnoreCase("yes")){%>
                    <tr>
                        <td class="t-align-left ff">Last Date of Tender Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName4()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New Date of Tender Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName5()%></td>
                    </tr>
                    <% } %>
                    <%--<tr>
                        <td class="t-align-left ff">Last Date for Acceptance of Tender Validity Extension Request : </td>
                        <td class="t-align-left">
                        <%=list.get(0).getFieldName9()%>
                        <input type="hidden" name="hdnlastaccept" value="<%=list.get(0).getFieldName9()%>"/>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="t-align-left ff">Extension Reason :  </td>
                        <td class="t-align-left">
                           <%=list.get(0).getFieldName8()%>
                           <input type="hidden" name="hdncomments" value="<%=list.get(0).getFieldName8()%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Action : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">
                            <select name="select2" class="formTxtBox_1" id="select2" style="width:100px;">
                                <option value="Approved">Accept</option>
                                <option value="Reject">Reject</option>
                            </select>
                        </td>
                    </tr>
                    <%}%>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnhope" id="btnhope" type="submit" value="Submit"/>
                    </label>
                </div>
            </form>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
