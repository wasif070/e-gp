<%--
    Document   : CompanyVerification
    Created on : Dec 31, 2010, 11:40 PM
    Author     : taher
--%>

<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.model.table.TblMandatoryDoc"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyDocuments"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--        <script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmComp").validate({
                    rules: {
                        comments:{required:true,maxlength: 100}
                    },
                    messages: {
                        comments:{required:"<div class='reqF_1'>Please Enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 100 Characters are allowed.</div>"}
                    }
                });
                 });
        </script>
    </head>       
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                    if ("Approve".equals(request.getParameter("btnApprove"))) {
                        String emailMobile = contentAdminService.changeUserStatus(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "approved", request.getParameter("comments"),request.getParameter("cId"),false);
                        String mails[]={emailMobile.substring(0,emailMobile.lastIndexOf("$"))};
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        service.contentAdmMsgBox(mails[0], XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP System.", msgBoxContentUtility.contentAdminApprove());
                        String url=request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                        String mailText = mailContentUtility.contAdmAppRej("APPROVED",request.getParameter("comments"),url.substring(0, url.lastIndexOf("/")));
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP System:  Bidder Registration - APPROVED");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                        sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$")+1,emailMobile.length()));
                        sendMessageUtil.setSmsBody("Dear Bidder,%0AYour e-GP profile has been Approved by e-GP Admin.");
                        sendMessageUtil.sendSMS();
                        mails=null;
                        sendMessageUtil=null;
                        mailContentUtility=null;
                        msgBoxContentUtility=null;
                        response.sendRedirect("CompanyVerification.jsp?cId="+request.getParameter("cId")+"&succ=a");
                    } else if ("Reject".equals(request.getParameter("btnReject"))) {
                        String emailMobile = contentAdminService.changeUserStatus(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "rejected", request.getParameter("comments"),request.getParameter("cId"),false);
                        String mails[]={emailMobile.substring(0,emailMobile.lastIndexOf("$"))};
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();                        
                        String url=request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                        String mailText = mailContentUtility.contAdmAppRej("REJECTED",request.getParameter("comments"),url.substring(0, url.lastIndexOf("/")));
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP System:  Bidder Registration - REJECTED");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                        sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$")+1,emailMobile.length()));
                        sendMessageUtil.setSmsBody("Dear Bidder,%0AYour e-GP profile has been Rejected by e-GP Admin.");
                        sendMessageUtil.sendSMS();
                        mails=null;
                        sendMessageUtil=null;
                        mailContentUtility=null;                        
                        response.sendRedirect("CompanyVerification.jsp?cId="+request.getParameter("cId")+"&succ=r");
                    }

                    java.util.List<TblCompanyDocuments> tblCompanyDocuments = contentAdminService.findCompanyDocumentses("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(Integer.parseInt(request.getParameter("tId"))),"documentTypeId",Operation_enum.NE,"briefcase");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">Bidder/Consultant Documents</div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include><br/>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th style="width:10%;" class="t-align-left">Sl.  No.</th>
                        <th class="t-align-left">Document Name</th>
                        <th class="t-align-left">Document Description</th>
                        <th class="t-align-left">File Size (In KB)</th>
                        <th style="width:10%;"  class="t-align-left">Action</th>
                    </tr>
                    <%
                                int count = 1;                                
                                List<TblMandatoryDoc> list = service.getMandatoryDocs(request.getParameter("uId"),false, false);
                                for (TblCompanyDocuments ttcd : tblCompanyDocuments) {
                    %>
                    <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-center"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-center"><%for(TblMandatoryDoc doc : list){if(doc.getDocTypeValue().equals(ttcd.getDocumentTypeId())){out.print(doc.getDocType());}}%></td>
                        <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocumentSize()))/1024));%></td>
                        <td  class="t-align-center">
                            <a href="<%=request.getContextPath()%>/SupportDocServlet?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocumentSize()%>&funName=download&iscontent=y&uId=<%=request.getParameter("uId")%>" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                        </td>
                    </tr>
                    <%
                                    count++;
                                }
                    %>
                </table>
                <br/>
                <div align="center" style="display: <%if("a".equals(request.getParameter("s"))){out.print("none;");}else{out.print("block;");}%>">
                    <form action="TendererDocs.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>" method="post" name="frmComp" id="frmComp">
                        <table width="100%" cellspacing="0" class="tableList_2">
                            <tr>
                                <td width="18%" class="ff">Comments : <span style="color: red;">*</span></td>
                                <td width="82%">
                                    <textarea name="comments" class="formTxtBox_1" style="width:400px;" rows="3" cols="5"></textarea>
                                </td>
                            </tr>                        
                            <tr>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <label class="formBtn_1" style="visibility: <%if ("r".equals(request.getParameter("s")) || "p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                        <input type="submit" name="btnApprove" id="btnApprove" value="Approve" />
                                    </label>
                                    &nbsp;
<!--                                    To give Reject Link in approved tab keep this "a".equals(request.getParameter("s")) || -->
                                    <label class="formBtn_1" style="visibility: <%if ("p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                        <input type="submit" name="btnReject" id="btnReject" value="Reject" />
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
