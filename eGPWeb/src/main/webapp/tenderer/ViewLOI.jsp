<%-- 
    Document   : ViewLOI
    Created on : Mar 1, 2017, 12:38:59 PM
    Author     : Nishith
--%>


<%@page import="com.cptu.egp.eps.model.table.TblNoaAcceptance"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.web.utility.NumberToWord"%>

<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter Of Intent (LOI)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        

    </head>
    <body>
        <%
                    boolean bCommon = true;
                    if (request.getParameter("action") != null && "common".equals(request.getParameter("action"))) {
                        bCommon = false;
                    }
                    String userid = "";
                    //swati--to generate pdf
                    String tenderId = "";
                    String tLotId = "";
                    String lotNo = "";
                    String roundId = "";
                    
                    if (request.getParameter("tenderid") != null) {
                        tenderId = request.getParameter("tenderid");
                    }
                    
                    if (request.getParameter("tLotId") != null) {
                        tLotId = request.getParameter("tLotId");
                    }
                    
                    if (request.getParameter("lotNo") != null) {
                        lotNo = request.getParameter("lotNo");
                    }
                    
                    if(request.getParameter("roundId") != null){
                        roundId = request.getParameter("roundId");
                    }
                    

                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
                    String folderName = folderName = pdfConstant.LOI;
                    String genId = tenderId+"_Lot"+lotNo;// + "_" + LOIID;

                    if (isPDF.equalsIgnoreCase("true")) {
                        userid = request.getParameter("userid");
                    }
                    if (!(isPDF.equalsIgnoreCase("true"))) {
                        try {
                            String reqURL = request.getRequestURL().toString();
                            //tenderid=1518&NOAID=46&app=no
                            String reqQuery = "tLotId="+ tLotId + "&tenderid=" + tenderId + "&lotNo=" + lotNo + "&roundId=" + roundId;
                            //String reqQuery = "tenderid=" + tenderId +  "&userid=" + userid;
                            pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    
                    TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonSearchService commonSearchData = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    
                    
                    //String userNameNOA = null;
                    //end

        %>

        <% if (!(isPDF.equalsIgnoreCase("true"))) {
                        if (bCommon) {
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <%
                            //userNameNOA = objUserName.toString();
                        }
                    }%>
        
                    <div class="dashboard_div">
                    <!--LOI DASHBOARD START-->
                        <%
                            if (bCommon) {
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userid = hs.getAttribute("userId").toString();
                            }
                        }
                            
                        
                        String bidderName; String bidderAddress; String packageDes; String IdenRef; String bidAmount; String paName; String paDesignation; String paAgency;
                        List<SPTenderCommonData> bidderInfo = objTenderCommonService.returndata("getBidderInfoAfterTCApprove", tenderId, roundId);
                        if(bidderInfo.get(0).getFieldName1().equalsIgnoreCase("company")){
                            bidderName = bidderInfo.get(0).getFieldName2();
                            bidderAddress = bidderInfo.get(0).getFieldName3() 
                                    + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName4())
                                    + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName5())
                                    + (bidderInfo.get(0).getFieldName6().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName6())
                                    + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName7());
                        }else{
                            bidderName = bidderInfo.get(0).getFieldName2() 
                                    + (bidderInfo.get(0).getFieldName3().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName3())
                                    + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName4())
                                    + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName5());
                            bidderAddress = bidderInfo.get(0).getFieldName6() 
                                    + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName7())
                                    + (bidderInfo.get(0).getFieldName8().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName8())
                                    + (bidderInfo.get(0).getFieldName9().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName9())
                                    + (bidderInfo.get(0).getFieldName10().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName10())
                                    + (bidderInfo.get(0).getFieldName11().equalsIgnoreCase("") ? "" : ", "+bidderInfo.get(0).getFieldName11());
                        }
                        List<SPTenderCommonData> tenderinfo = objTenderCommonService.returndata("getTenderInfoAfterTCApprove", tenderId, null);
                        IdenRef = tenderinfo.get(0).getFieldName1();
                        packageDes = tenderinfo.get(0).getFieldName2();
                        paName = tenderinfo.get(0).getFieldName3();
                        paDesignation = tenderinfo.get(0).getFieldName4();
                        paAgency = tenderinfo.get(0).getFieldName5();

                        List<SPCommonSearchData> amountInfo = commonSearchData.searchData("GetBidderAmountRank1", tenderId, tLotId, roundId, null, null, null, null, null, null);
                        bidAmount = amountInfo.get(0).getFieldName1();
                        String wordAmount = NumberToWord.Convert(bidAmount);
                        bidAmount = " Nu. "+ bidAmount + " ("+wordAmount+" Nu.)";

                        
                        %>
                        <!--LOI DASHBOARD ENDS-->
                        
                        <div class="contentArea_1">
                            <div id="print_area">
                                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                                <div class="pageHead_1">
                                    Letter Of Intent (LOI) of Tender ID: <%=tenderId%>
                                    <% if (bCommon) {%>
                                    <span id="hdnLink" class="c-alignment-right">
                                        <a class="action-button-savepdf" id="saveASPDFBtn" href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=genId%>&tId=<%=tenderId%>" >Save As PDF </a>&nbsp;
                                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                        <a href="LOI.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a>
                                    </span>
                                    <% }%>
                                </div>
                                <%}%>
                                
                                <div style="width: 70%; margin-left: 5%;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="5" class="t_space">
                                        <tr>
                                            <td><span>To,</span></td>
                                        </tr>
                                        <tr>
                                            <td><span><b><%=bidderName%></b><br /><%=bidderAddress%><br/><br/><br/></span></td>
                                        </tr>
                                        <tr>
                                            <td><p>This is to notify you that, it is our intention to award the contract for your bid dated <b><%=DateUtils.formatStdDate(new Date())%></b> for execution of the <b>"<%=packageDes%>"</b>, Identification Reference No. <b>"<%=IdenRef%>"</b> for the Contract Price of <b><%=bidAmount%></b> as corrected and modified in accordance with the Instructions to Bidders.<br/><br/></p></td>
                                        </tr>
                                        <tr>
                                            <td>Thanks,</td>
                                        </tr>
                                        <tr>
                                            <td><b><%=paName%></b></td>
                                        </tr>
                                        <tr>
                                            <td><%=paDesignation%></td>
                                        </tr>
                                        <tr>
                                            <td><%=paAgency%></td>
                                        </tr>
                                    </table>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
        
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
             $(document).ready(function() {
                                $("#print").click(function() {
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $(this).parent().append("<span class='disp_link'>-</span>");
                                            $(this).hide();
                                            //$('.reqF_1').remove();
                                    })
                                    $('#hdnLink').hide();
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $('.disp_link').remove();
                                            //$('.reqF_1').remove();
                                            $(this).show();
                                    })
                                    $('#hdnLink').show();
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }       

        </script>
    <script type="text/javascript">
        
        document.getElementById('noaIssueDaysWords').innerHTML =WORD(document.getElementById('noaIssueDays').value);
        document.getElementById('perSecDaysWords').innerHTML =WORD(document.getElementById('perSecDays').value);
        document.getElementById('contractSignDaysWords').innerHTML =WORD(document.getElementById('contractSignDays').value);
    </script>
</html>
<%
            tenderSrBean = null;
            pdfCmd = null;
            pdfConstant = null;
%>
