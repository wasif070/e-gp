<%-- 
    Document   : BidderFrom
    Created on : Nov 22, 2010, 12:00:22 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bidder Form</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        
    </head>
    <jsp:useBean id="signerBean"  class="com.cptu.egp.eps.web.utility.SignerAPI" />
    <body>
         <script type="text/javascript">
            var SignerAPI;
            $(function() {
                SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
            });
            //alert(SignerAPI);
        </script>
    <div id="appcon">
        <script type="text/javascript">
            
           // alert(SignerAPI);
           alert(deployJava);
            deployJava.runApplet(
                {
                    codebase:"/",
                    archive:"Signer.jar",
                    code:"com.cptu.egp.eps.SignerAPI.class",
                    width:"0",
                    Height:"0",
                    ID: "SignerAPI",
                    classloader_cache: "false"
                },
                null,
                "1.6"
            );
            alert(SignerAPI);
        </script>
       
    </div>
    <div class="mainDiv">
        <div class="dashboard_div">
            <jsp:include page="../resources/common/AfterLoginTop.jsp"></jsp:include>
            <div class="fixDiv">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td>
                            <div class="t_space">
                            <div class="pageHead_1">Bid Form</div>
                            <div>&nbsp;</div>
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td valign="middle">Actual data : </td>
                                                <td>
                                                    <textarea id="aData" cols="60" rows="10" class="formTxtBox_1"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Password : </td>
                                                <td>
                                                    <input type="password" id="pwd1" name="pwd1" class="formTxtBox_1" autoautocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <label class="formBtn_1">
                                                        <input type="button" name="encrypt" id="encrypt" value="Encrypt">
                                                    </label>
                                                </td>
                                            </tr>
                                            <script type="text/javascript">
                                                $('#encrypt').click(function() {
                                                    alert('sanjay');
                                                     alert(SignerAPI);
                                                    alert($('#aData').val());
                                                    SignerAPI.setData($('#aData').val());
                                                    
                                                    enc = SignerAPI.getSymEncrypt($('#pwd1').val());
                                                    $('#eData').val(enc);
                                                    //alert("done"+enc);
                                                });
                                            </script>
                                            <tr></tr>
                                            <tr>
                                                <td valign="middle">Encrypted data : </td>
                                                <td>
                                                    <textarea id="eData" cols="60" rows="10" class="formTxtBox_1"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Password : </td>
                                                <td>
                                                    <input type="password" id="pwd2" name="pwd2" class="formTxtBox_1" autocomplete="off" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                   <label class="formBtn_1">
                                                       <input type="button" name="decrypt" id="decrypt" value="Decrypt"/>
                                                   </label>
                                                </td>
                                            </tr>
                                            <tr></tr>
                                            <SCRIPT type="text/javascript">
                                                $('#decrypt').click(function() {
                                                    SignerAPI.setEncrypt($('#eData').val());
                                                    enc = SignerAPI.getSymDecrypt($('#pwd2').val());
                                                    $('#dData').val(enc);
                                                });
                                            </SCRIPT>
                                            <tr>
                                                <td valign="middle">Decrypted data : </td>
                                                <td>
                                                    <textarea id="dData" cols="60" rows="10" class="formTxtBox_1"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </head>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>