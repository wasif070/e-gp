<%-- 
    Document   : Reregistration
    Created on : May 15, 2016, 1:54:44 PM
    Author     : SRISTY
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.TempBiddingPermission"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.model.table.TblBiddingPermission,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.model.table.TblTempBiddingPermission"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <%
        tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        boolean ifEdit = true;
        int chkValue=0;
        ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
        TblCompanyMaster dtBean = contentAdminService.findCompanyMaster("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId"))).get(0);
        List<TblBiddingPermission> biddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId")), "isReapplied", Operation_enum.EQ, Integer.parseInt("0"));//false
        List<TblBiddingPermission> pendingBiddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId")), "isReapplied", Operation_enum.EQ, Integer.parseInt("1"));//false
        if("next".equalsIgnoreCase(request.getParameter("next")))
        {
            response.sendRedirect("NewSupportingDocuments.jsp?cId=" + request.getParameter("cId") + "&tId=" + request.getParameter("tId"));
        }
        if ("Submit".equals(request.getParameter("saveNEdit"))) {
            tempCompanyMasterDtBean.setCompanyId(dtBean.getCompanyId());
            tempCompanyMasterDtBean.setUserId(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
//            tempCompanyMasterDtBean.setCompanyRegNumber(dtBean.getCompanyRegNumber());
//            tempCompanyMasterDtBean.setTradeLicenseNumber(dtBean.getTradeLicenseNumber());
//            tempCompanyMasterDtBean.setCompanyName(dtBean.getCompanyName());
//            tempCompanyMasterDtBean.setCompanyNameInBangla(dtBean.getCompanyNameInBangla());
//            tempCompanyMasterDtBean.setLegalStatus(dtBean.getLegalStatus());
//            tempCompanyMasterDtBean.setEstablishmentYear(dtBean.getEstablishmentYear());
//            tempCompanyMasterDtBean.setTinNo(dtBean.getTinNo());
//            tempCompanyMasterDtBean.setTinDocName(dtBean.getTinDocName());
//            tempCompanyMasterDtBean.setLicIssueDate(dtBean.getLicIssueDate());
//            tempCompanyMasterDtBean.setLicExpiryDate(dtBean.getLicExpiryDate());
            //tempCompanyMasterDtBean.setcom(dtBean.getWorkCategory());
            //tempCompanyMasterDtBean.setStatutoryCertificateNo(dtBean.getStatutoryCertificateNo());
            //tempCompanyMasterDtBean.setOriginCountry(((String)request.getParameter("originCountry")));

            String qString = null;
            tendererMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            List<TempBiddingPermission> tempBiddingPermissionList = new ArrayList<TempBiddingPermission>();

            //// set permission like company detail
            if (request.getParameter("ProcurementType1") != null) {
                TempBiddingPermission bidpermission = new TempBiddingPermission();
                bidpermission.setProcurementCategory("Goods");
                bidpermission.setWorkType("");
                bidpermission.setWorkCategroy("");
                tempBiddingPermissionList.add(bidpermission);
            }
            for (int i = 0; i <= 3; i++) {
                if (request.getParameter("large" + i) != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();

                    bidpermission.setWorkCategroy("W" + (i + 1));
                    bidpermission.setWorkType("Large");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);
                }

                if (request.getParameter("medium" + i) != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();

                    bidpermission.setWorkCategroy("W" + (i + 1));
                    bidpermission.setWorkType("Medium");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);
                }

                if (request.getParameter("small" + i) != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setWorkCategroy("W" + (i + 1));
                    bidpermission.setWorkType("Small");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);
                }
            }

            //registered
            if (request.getParameter("Registered") != null) {
                boolean isLarge = false, isMedium = false, isSmall = false;
                for (int i = 0; i < tempBiddingPermissionList.size(); i++) {
                    if (!"Goods".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString()) && !"Services".equalsIgnoreCase(tempBiddingPermissionList.get(i).getProcurementCategory().toString())) {
                        if ("Large".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                            isLarge = true;
                        }
                        if ("Medium".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                            isMedium = true;
                        }
                        if ("Small".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkType().toString())
                                && "W2".equalsIgnoreCase(tempBiddingPermissionList.get(i).getWorkCategroy().toString())) {
                            isSmall = true;
                        }
                    }
                }

                if (!isLarge) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setWorkCategroy("W2");
                    bidpermission.setWorkType("Large");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);

                }
                if (!isMedium) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setWorkCategroy("W2");
                    bidpermission.setWorkType("Medium");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);
                }
                if (!isSmall) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();
                    bidpermission.setWorkCategroy("W2");
                    bidpermission.setWorkType("Small");
                    bidpermission.setProcurementCategory("Works");
                    tempBiddingPermissionList.add(bidpermission);
                }
            }

            for (int i = 0; i <= 1; i++) {
                if (request.getParameter("servicetype" + i) != null) {
                    TempBiddingPermission bidpermission = new TempBiddingPermission();

                    bidpermission.setWorkType(request.getParameter("servicetype" + i));
                    bidpermission.setProcurementCategory("Services");
                    tempBiddingPermissionList.add(bidpermission);
                }
            }
            //biddingPermisionList-previous, tempBiddingPermissionList-reapply
            //check same permission cannot be reapply again
            if(biddingPermisionList.size() == tempBiddingPermissionList.size())
            {
                for(int i = 0; i < tempBiddingPermissionList.size(); i++)
                {
                    for(int j = 0; j < biddingPermisionList.size(); j++)
                    {
                        if(tempBiddingPermissionList.get(i).getProcurementCategory().toString().equalsIgnoreCase(biddingPermisionList.get(j).getProcurementCategory().toString()))
                        {
                            if(tempBiddingPermissionList.get(i).getWorkType().toString().equalsIgnoreCase(biddingPermisionList.get(j).getWorkType().toString()))
                            {
                                if(tempBiddingPermissionList.get(i).getWorkCategroy().toString().equalsIgnoreCase(biddingPermisionList.get(j).getWorkCategroy().toString()))
                                    chkValue++;
                            }
                        }
                    }
                }
            }
            if(chkValue == biddingPermisionList.size())
            {
                qString = "&msg=per";
                response.sendRedirect("Reregistration.jsp?cId=" + request.getParameter("cId") + "&tId=" + request.getParameter("tId") + qString);
            }
            else
            {
                if (tendererMasterSrBean.reapplyTblBiddingPermission(tempBiddingPermissionList, dtBean.getCompanyId(), Integer.parseInt(session.getAttribute("userId").toString()))) {
                    qString = "&msg=s";
                } else {
                    qString = "&msg=fail";
                }
                if ("&msg=s".equalsIgnoreCase(qString)) {
                    response.sendRedirect("NewSupportingDocuments.jsp?cId=" + request.getParameter("cId") + "&tId=" + request.getParameter("tId"));
                } else {
                    response.sendRedirect("Reregistration.jsp?cId=" + request.getParameter("cId") + "&tId=" + request.getParameter("tId") + qString);
                }
            }
        }
    %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Apply Procurement Category</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>       

        <script type="text/javascript">
            function setRegisteredCategory() {
                if (document.getElementById('registeredchk').checked) {
                    document.getElementById('w2largechk').checked = true;
                    document.getElementById('w2mediumchk').checked = true;
                    document.getElementById('w2smallchk').checked = true;
                    document.getElementById('largechkbox').checked = true;
                    document.getElementById('mediumchkbox').checked = true;
                    document.getElementById('smallchkbox').checked = true;

                } else if (!document.getElementById('registeredchk').checked) {

                    document.getElementById('w2largechk').checked = false;
                    document.getElementById('w2mediumchk').checked = false;
                    document.getElementById('w2smallchk').checked = false;
//                    document.getElementById('largechkbox').checked = false;
//                    document.getElementById('mediumchkbox').checked = false;
//                    document.getElementById('smallchkbox').checked = false;


                }
            }

            function CheckedCategory()
            {
                if (document.getElementById('nonconsultingchk').checked)
                    document.getElementById('serviceschk').checked = true;
                else
                    document.getElementById('serviceschk').checked = false;

                if (document.getElementById('consultingchk').checked)
                    document.getElementById('serviceschk').checked = true;
                else
                    document.getElementById('serviceschk').checked = false;

            }

            $(function () {
                $('#frmComp').submit(function () {
                    
                    if (!document.getElementById('goodschk').checked && !document.getElementById('workschk').checked && !document.getElementById('serviceschk').checked && !document.getElementById('registeredchk').checked) {
                        $('#msgprocurementtype').html('Please Select a Procurement Type');
                        return false;
                    }
                    if (document.getElementById('workschk').checked)
                    {
                        if (!document.getElementById('largechkbox').checked && !document.getElementById('mediumchkbox').checked && !document.getElementById('smallchkbox').checked && !document.getElementById('registeredchk').checked) {
                            $('#msgprocurementtype').html('Please Select Classification of Contractor');
                            return false;
                        }

                        if (document.getElementById('largechkbox').checked)
                        {
                            if (!document.getElementById('w1largechk').checked && !document.getElementById('w2largechk').checked && !document.getElementById('w3largechk').checked && !document.getElementById('w4largechk').checked) {
                                $('#msgprocurementtype').html('Please Select Work Type');
                                return false;
                            }
                        }
                        if (document.getElementById('mediumchkbox').checked)
                        {
                            if (!document.getElementById('w1mediumchk').checked && !document.getElementById('w2mediumchk').checked && !document.getElementById('w3mediumchk').checked && !document.getElementById('w4mediumchk').checked) {
                                $('#msgprocurementtype').html('Please Select Work Type');
                                return false;
                            }
                        }
                        if (document.getElementById('smallchkbox').checked)
                        {
                            if (!document.getElementById('w1smallchk').checked && !document.getElementById('w2smallchk').checked && !document.getElementById('w3smallchk').checked && !document.getElementById('w4smallchk').checked) {
                                $('#msgprocurementtype').html('Please Select Work Type');
                                return false;
                            }
                        }
                    }
                    if (document.getElementById('serviceschk').checked)
                    {
                        if (!document.getElementById('consultingchk').checked && !document.getElementById('nonconsultingchk').checked) {
                            $('#msgprocurementtype').html('Please Select Consulting or Non-Consulting');
                            return false;
                        }
                    }
                })
            });
        </script>
    </head>
    <body>
        <form name="cmpSubmit" id="cmpSubmit" method="post"></form>        
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->                
                <div class="pageHead_1">Request for New Procurement Category</div>
                <div class="stepWiz_1 t_space">
                    <ul>
                        <% String pageName = request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1];
                        %>
                        <%--<li <%if (pageName.equals("EditCompanyDetail.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditCompanyDetail.jsp")) {%><a href="EditCompanyDetail.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Details<%if (!pageName.equals("EditCompanyDetail.jsp")) {%></a><%}%></li>
                        <li <%if (pageName.equals("EditPersonalDetails.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditPersonalDetails.jsp")) {%><a href="EditPersonalDetails.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Contact Person Details<%if (!pageName.equals("EditPersonalDetails.jsp")) {%></a><%}%></li>
                        --%>                        
                        <li <%if (pageName.equals("Reregistration.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("Reregistration.jsp")) {%><a href="Reregistration.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Request for New Procurement Category<%if (!pageName.equals("Reregistration.jsp")) {%></a><%}%></li>
                        <li <%if (pageName.equals("NewSupportingDocuments.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("NewSupportingDocuments.jsp")) {%><a href="NewSupportingDocuments.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Upload Supporting Documents<%if (!pageName.equals("NewSupportingDocuments.jsp")) {%></a><%}%></li>
                    </ul>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <form id="frmComp" method="post" action="Reregistration.jsp?cId=<%=request.getParameter("cId")%>&tId=<%=request.getParameter("tId")%>">
                                <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                                <%if ("fail".equals(request.getParameter("msg"))) {%><br/><div class="responseMsg errorMsg"  >Problem in Add New Procurement Category.</div><%}%>
                                <%if ("per".equals(request.getParameter("msg"))) {%><br/><div class="responseMsg errorMsg"  >You already permitted for these selected category.</div><%}%>
                                <%--<%if ("y".equals(request.getParameter("cpr"))) {%><br/><div class="responseMsg errorMsg">Company Registration No. already exist</div><%}%>--%>
                                <%if (ifEdit) {%><input type="hidden" value="y" id="ifedit"/><%}%>                                
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1"  id="tbodyHide"  width="100%">                                            


                                    <tr>
                                        <td class="ff" height="30">Company Name : </td>
                                        <td style="color: black"><%=dtBean.getCompanyName()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Company's Legal Status  : </td>
                                        <td style="color: black">
                                            <%if (dtBean.getLegalStatus().equals("public")) {%>Public Limited Company<%}%>
                                            <%if (dtBean.getLegalStatus().equals("private")) {%>Private Limited Company<%}%>
                                            <%--<%if (dtBean.getLegalStatus().equals("partnership")) {%>Partnership<%}%>--%>
                                            <%if (dtBean.getLegalStatus().equals("proprietor")) {%>Sole proprietorship<%}%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Company's Establishment Year : </td>
                                        <td style="color: black"><%=dtBean.getEstablishmentYear()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Origin of Country : </td>
                                        <td style="color: black"><%if(dtBean.getOriginCountry()!=null)
                                            out.print(dtBean.getOriginCountry());%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Country : </td>
                                        <td style="color: black"><%=dtBean.getRegOffCountry()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Dzongkhag / District : </td>
                                        <td style="color: black"><%=dtBean.getRegOffState()%></td>
                                    </tr>
                                    <% if (!pendingBiddingPermisionList.isEmpty()) { %>
                                    <tr>
                                        <td class="ff" height="30"> Procurement Category : </td>
                                        <td style="color: black">
                                            <% if (pendingBiddingPermisionList.size() != 0) 
                                {
                                    for (int i = 0; i < pendingBiddingPermisionList.size(); i++) {                                        
                                        out.println(pendingBiddingPermisionList.get(i).getProcurementCategory().toString());
                                        if (pendingBiddingPermisionList.get(i).getWorkType() != null && !pendingBiddingPermisionList.get(i).getWorkType().isEmpty()) {
                                            out.print(" : " + pendingBiddingPermisionList.get(i).getWorkType().toString());
                                        }
                                        if (pendingBiddingPermisionList.get(i).getWorkCategroy() != null && !pendingBiddingPermisionList.get(i).getWorkCategroy().isEmpty()) {
                                            out.print(" - " + pendingBiddingPermisionList.get(i).getWorkCategroy().toString());
                                        }
                                        out.print("<br/>");
                                    }
                                }
                            %>
                                        </td>    
                                    </tr>
                                    <% } else { %>
                                    <tr>
                                        <td class="ff" height="30"> Procurement Category : <span>*</span></td>
                                        <td style="color: black">
                                            <input name="ProcurementType1" value="Goods" type="checkbox" id="goodschk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {

                                                               if ("Goods".equalsIgnoreCase(biddingPermisionList.get(i).getProcurementCategory())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%> 
                                                   />&nbsp;Goods&nbsp;&nbsp;
                                        </td>    
                                    </tr>

                                    <tr >
                                        <td class="ff" height="30"> </td>
                                        <td style="color: black"> 
                                            <input name="ProcurementType2" value="Works" type="checkbox" readonly="true" id="workschk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Works".equalsIgnoreCase(biddingPermisionList.get(i).getProcurementCategory())) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%>
                                                   />&nbsp;Works&nbsp;&nbsp;
                                        </td>
                                    </tr>

                                    <tr id="largecategorytr">
                                        <td class="ff" height="30"> </td>
                                        <td style="color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="worktype1" value="Large" type="checkbox" readonly="true" id="largechkbox"
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%>/>
                                            &nbsp;Large : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="large0" value="W1" type="checkbox" id="w1largechk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W1".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W1
                                            <input name="large1" value="W2" type="checkbox" id="w2largechk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W2
                                            <input name="large2" value="W3" type="checkbox" id="w3largechk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W3".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W3
                                            <input name="large3" value="W4" type="checkbox" id="w4largechk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W4".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W4
                                        </td> 
                                    </tr>
                                    <tr id="mediumcategorytr">
                                        <td class="ff" height="30"> </td>
                                        <td style="color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="worktype2" value="Medium"  type="checkbox" readonly="true" id="mediumchkbox" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%>/>&nbsp;Medium  : &nbsp;&nbsp;
                                            <input name="medium0" value="W1" type="checkbox" id="w1mediumchk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W1".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>
                                                   /> W1
                                            <input name="medium1" value="W2" type="checkbox" id="w2mediumchk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W2
                                            <input name="medium2" value="W3" type="checkbox" id="w3mediumchk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W3".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W3
                                            <input name="medium3" value="W4" type="checkbox" id="w4mediumchk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("W4".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");

                                                               }
                                                           }
                                                       }%>/> W4
                                        </td> 
                                    </tr>
                                    <tr id="smallcategorytr">
                                        <td class="ff" height="30"> </td>
                                        <td style="color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="worktype3" value="Small" type="checkbox" id="smallchkbox"
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%>>&nbsp;Small : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input name="small0" value="W1" type="checkbox" id="w1smallchk" 
                                                       <% if (ifEdit) {
                                                               for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                                   if ("W1".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                       out.print("checked");

                                                                   }
                                                               }
                                                           }%>/> W1
                                                <input name="small1" value="W2" type="checkbox" id="w2smallchk" 
                                                       <% if (ifEdit) {
                                                               for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                                   if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                       out.print("checked");

                                                                   }
                                                               }
                                                           }%>/> W2
                                                <input name="small2" value="W3" type="checkbox" id="w3smallchk" 
                                                       <% if (ifEdit) {
                                                               for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                                   if ("W3".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                       out.print("checked");

                                                                   }
                                                               }
                                                           }%>/> W3
                                                <input name="small3" value="W4" type="checkbox" id="w4smallchk" 
                                                       <% if (ifEdit) {
                                                               for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                                   if ("W4".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                       out.print("checked");

                                                                   }
                                                               }
                                                           }%>/> W4
                                        </td> 
                                    </tr>
                                    <tr id="registeredcategorytr">
                                        <td class="ff" height="30"> </td>
                                        <td style="color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="Registered" value="Registered" type="checkbox" id="registeredchk"  onchange="setRegisteredCategory();"
                                                   <% if (ifEdit) {
                                                           int countOfW2 = 0;
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {

                                                               if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   countOfW2++;
                                                               }
                                                               if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   countOfW2++;
                                                               }
                                                               if ("W2".equalsIgnoreCase(biddingPermisionList.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   countOfW2++;
                                                               }
                                                               if (countOfW2 == 3) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%>/> 
                                            Registered

                                        </td> 
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">
                                        </td>
                                        <td style="color: black">
                                            <input name="ProcurementType3" value="Services" type="checkbox" id="serviceschk" 
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Services".equalsIgnoreCase(biddingPermisionList.get(i).getProcurementCategory())) {
                                                                   out.print("checked");
                                                                   break;
                                                               }
                                                           }
                                                       }%> 
                                                   />&nbsp;Services&nbsp;&nbsp;

                                        </td>
                                    </tr>
                                    <tr id="servicecategorytr">
                                        <td class="ff" height="30">  </td>
                                        <td style="color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="servicetype0" value="Consulting" type="checkbox" id="consultingchk" onclick="CheckedCategory();"
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Consulting".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");
                                                               }
                                                           }
                                                       }%>/> Consulting &nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="servicetype1" value="Non-Consulting" type="checkbox" id="nonconsultingchk"onclick="CheckedCategory();"
                                                   <% if (ifEdit) {
                                                           for (int i = 0; i < biddingPermisionList.size(); i++) {
                                                               if ("Non-Consulting".equalsIgnoreCase(biddingPermisionList.get(i).getWorkType())) {
                                                                   out.print("checked");
                                                               }
                                                           }
                                                       }%>
                                                   /> Non-Consulting
                                            <br/>
                                            <span id="msgprocurementtype" style="color: red"></span>
                                        </td> 
                                    </tr>
                                    <%}%>

                                    <tr>
                                        <td align="center" colspan="2">   
                                            <% if (pendingBiddingPermisionList.isEmpty()) {%>
                                            <label class="formBtn_1">
                                                <input type="submit" name="saveNEdit" id="btnUpdate" value="Submit"/>
                                            </label>
                                            <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value=""/> 
                                            <% } else { %>
                                            <label style="color: red"> Bidding Permission is Pending</label>
                                            <br/><br/>
                                            <label class="formBtn_1">
                                            <input type="submit" name="next" id="btnNext" value="Next"/>
                                            </label>
                                            <% }%>
                                        </td>
                                    </tr>                                    
                                </table>                                                            
                            </form>                            
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp"/>
            </div>
        </div>
    </body>


    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

