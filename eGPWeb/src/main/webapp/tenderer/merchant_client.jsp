<%-- 
    Document   : merchant_client
    Created on : Feb 1, 2012, 1:46:19 PM
    Author     : darshan
--%>

<%@page import="java.net.URLEncoder"%>
<%@page import="java.io.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

            String errmsg = ""; // error message
            String sname = ""; // sender's name
            String email = ""; // sender's email addres
            String subject = ""; // message subject
            String message = ""; // the message itself
            String strAmount = "";
            String strDesc = "";
            String strTranID = "";
            String stripAddress = request.getHeader("X-FORWARDED-FOR");
            boolean onlinePaymentStatusSuccess = false;

            if (stripAddress == null) {
                stripAddress = request.getRemoteAddr();
            }
            if (request.getParameter("btnSubmit") != null && request.getParameter("btnSubmit").equalsIgnoreCase("submit")) {

                strAmount = request.getParameter("Amount");
                strDesc = request.getParameter("Desc");
               
                if (strAmount != null && !strAmount.trim().equals("")) {
                    if(strAmount.indexOf(".") == -1)
                    {
                        strAmount = ""+Integer.parseInt(strAmount)*100;
                    }
                    else if((strAmount.indexOf(".") == strAmount.lastIndexOf(".")) && strAmount.substring((strAmount.indexOf(".")+1), strAmount.length()).length() == 2)
                    {
                        strAmount = strAmount.replace(".","");
                    }
                    else
                    {
                        errmsg = "Please enter valid amount (max 2 digit after decimal)";
                    }
                }
                else
                {
                        errmsg = "Please enter valid amount";
                }

                if (strDesc.trim() == "") {
                    errmsg = "Please enter description";
                }

                try {
                    String strExecCMD = "java -jar C:/paymentGatewayTest/ecomm_merchant.jar  C:/paymentGatewayTest/merchant.properties -v " + strAmount + " 050 " + stripAddress + "  amt_" + strAmount + "_desc_" + strDesc;// + " > C:/test/result.trns";

                    Writer output = null;
                    File file = new File("C:/paymentGatewayTest/paymetTest.cmd");
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    output = new BufferedWriter(new FileWriter(file));
                    output.write(strExecCMD);
                    output.close();

                    Process p = Runtime.getRuntime().exec("C:/paymentGatewayTest/paymetTest.cmd");
                    BufferedReader brSucc = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    BufferedReader brFail = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                    /*Success reponse from Process*/
                    try {
                        while ((strTranID = brSucc.readLine()) != null) {
                            // System.out.println("Sucess strTranID = " + strTranID+" strTranID.indexOf(TRANSACTION_ID)  "+strTranID.startsWith("TRANSACTION_ID") );
                            if (strTranID.indexOf("TRANSACTION_ID") >= 0) {
                                onlinePaymentStatusSuccess = true;
                                break;
                            }
                        }
                        System.out.println("Org strTranID = " + strTranID);
                        strTranID = URLEncoder.encode(strTranID.substring(16), "UTF-8");
                    } catch (IOException e) {
                        onlinePaymentStatusSuccess = false;
                        e.printStackTrace();
                    }
                    brSucc.close();
                    System.out.println("After success and encode strTranID = " + strTranID);

                    /*Fail reponse from Process*/
                    if(!onlinePaymentStatusSuccess){
                        try {
                            while ((strTranID = brFail.readLine()) != null) {
                                    onlinePaymentStatusSuccess = false;
                            }
                        } catch (IOException e) {
                            onlinePaymentStatusSuccess = false;
                            e.printStackTrace();
                        }
                        brFail.close();
                    }
                } catch (IOException e1) {
                    onlinePaymentStatusSuccess = false;
                    e1.printStackTrace();
                }

                /*Final response recirect page on base of "onlinePaymentStatusSuccess"*/
                if (onlinePaymentStatusSuccess) {
                    System.out.println("strTranID = " + strTranID);
                    response.sendRedirect("https://ecomtest.dutchbanglabank.com/ecomm2/ClientHandler?trans_id=" + strTranID);
                } else {
                    response.sendRedirect(request.getContextPath()+"/tenderer/merchant_client.jsp?er=1");
                }
            } else {
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <title>Online registration fees payment </title>
        <script language='JavaScript' type='text/JavaScript'>
            <!--
            function validate() {
                if(document.form1.textinput.value=='')
                {
                    alert('Fill the Input box before submitting');
                    return false;
                }

                else	{
                    return true;
                }
            }
            //-->

            function validate_required(field,alerttxt)
            {
                with (field)
                {
                    if (value==null||value=="")
                    {
                        alert(alerttxt);return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            function validate_form(thisform)
            {
                with (thisform)
                {
                    if (validate_required(email,"Email must be filled out!")==false)
                    {email.focus();return false;}
                }
            }

            function checkForm()
            {
                var cAmount, cDesc;
                with(window.document.fr_merchant)
                {
                    cAmount   = Amount;
                    cDesc   = Desc;
                }

                if(trim(cAmount.value) == '')
                {
                    alert('Please enter Amount');
                    cAmount.focus();
                    return false;
                }
                else if(trim(cDesc.value) == '')
                {
                    alert('Please enter your Deascription');
                    cDesc.focus();
                    return false;
                }
                else
                {
                    cAmount.value    = trim(cAmount.value);
                    cDesc.value   = trim(cDesc.value);
                    return true;
                }
            }

            function trim(str)
            {
                return str.replace(/^\s+|\s+$/g,'');
            }

            function isEmail(str)
            {
                var regex = /^[-_.a-z0-9]+@(([-_a-z0-9]+\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i;

                return regex.test(str);
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="250"  valign="top">
                            <jsp:include page="../resources/common/Left.jsp" ></jsp:include>
                        </td>
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                             <div class="t_space">
<%                          if (request.getParameter("er") != null) {
%>
                                    <div class="responseMsg errorMsg " >Unable to proceed with online payment. Please try again.</div>
<%                          }
%>
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="pageHead_1">
                                <tr>
                                    <td align="left">Online registration fee payment - Customer Information</td>
                                    <td align="right" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" ><img src="<%=request.getContextPath()%>/resources/images/Dashboard/helpIcon.png" /></a></td>
                                    <td width="40" align="left" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" style="color: #333;" > &nbsp;Help</a></td>
                                </tr>
                            </table>
                            <form name="fr_merchant" action="merchant_client.jsp"  method="post" onSubmit="return checkForm();"><br>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Amount:(In Nu.) <span>*</span></td>
                                        <td width="80%"><input type="text" maxlength="12" name="Amount" class="formTxtBox_1"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Description: <span>*</span></td>
                                        <td width="80%"><input type="text" maxlength="125" name="Desc" class="formTxtBox_1"></td>
                                    </tr>

                                    <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                               <label class="formBtn_1">
                                                    <input type="submit"  value="Submit"  onClick="return checkForm();" name="btnSubmit" id="btnSubmit"/>
                                               </label>
                                           </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
<%}%>