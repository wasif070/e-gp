<%-- 
    Document   : CompanyUser
    Created on : Feb 28, 2011, 12:38:31 PM
    Author     : rishita,TaherT
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@ page buffer="15kb" %>                 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Company User Registration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                var vboolean;
                $('#frmCompanyUser').submit(function () {
                    if ($('#Submit').html() != null) {
                        vboolean = true;
                        if ($('#txtMail').val() == "") {
                            $('#mailMsg').css("color", "red");
                            $('#mailMsg').html("Please enter e-mail ID");
                            vboolean = false;
                        }
                        if ($('#mailMsg').html() != "OK") {
                            vboolean = false;
                        }
                        
                        if(document.getElementById("ValidityCheck").value == '0')
                        {
                            return false;
                        }
                        //Code by Proshanto
                        if ($('#cmbCountry').val() == "150") {//136
                            if ($.trim($('#txtRegThana').val()) == "") {
                                //$('#upErr').html("Please enter Gewog");
                                //vboolean = false;
                            } else {
                                $('#upErr').html(null);
                            }
                        }
                        /*if(vboolean == 'false'){
                         return false;
                         }*/

                        if ($('#mailMsg').html().toUpperCase() == 'OK') {
                            if ($('#frmCompanyUser').valid() && vboolean) {
                                if ($('#Submit') != null) {
                                    ChangeTitleValue();
                                    $('#Submit').attr("disabled", "true");
                                    $('#hdnbutton').val("Submit");
                                }
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else if ($('#Update').html() != null) {
                        if ($('#frmCompanyUser').valid()) {
                            ChangeTitleValue();
                            $('#Update').attr("disabled", "true");
                            $('#hdnbutton').val("Update");
                        }
                    } else if ($('#Suspend').html() != null) {
                        if ($('#frmCompanyUser').valid()) {
                            $('#Suspend').attr("disabled", "true");
                            $('#hdnbutton').val("Suspend");
                        }
                    } else if ($('#Resume').html() != null) {
                        if ($('#frmCompanyUser').valid()) {
                            $('#Resume').attr("disabled", "true");
                            $('#hdnbutton').val("Resume");
                        }
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function () {
                $('#txtMail').blur(function () {
                    if ($('#txtMail').val() == "") {
                        $('#mailMsg').css("color", "red");
                        $('#mailMsg').html("Please enter e-mail ID");
                    } else {
                        $('#mailMsg').css("color", "red");
                        $('#mailMsg').html("Checking for unique e-mail ID...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId: $('#txtMail').val(), funName: 'verifyMail'},
                                function (j) {
                                    if (j.toString().indexOf("OK", 0) != -1) {
                                        $('#mailMsg').css("color", "green");
                                    } else {
                                        $('#mailMsg').css("color", "red");
                                    }
                                    $('#mailMsg').html(j);
                                });
                    }
                });
            });

            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbState").html(j);
                        //Code by Proshanto
                        if ($('#cmbCountry').val() == "150") {//136

                            $('#trBangla').css("display", "table-row")
                            $('#trRthana').css("display", "table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#phoneCode2').val(j);
                                $('#faxCode2').val(j);
                            });
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtRegThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbSubDistrict').html(j);
                            });
                        } else {
                            $('#trBangla').css("display", "none")
                            $('#trRthana').css("display", "none")
                            $('#trRSubdistrict').css("display", "none")
                        }
                    });
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#phoneCode').val(j.toString());
                        $('#faxCode').val(j.toString());
                        $('#cntCode').val(j.toString());
                    });
                    $('#txtRegThana').val(null);
                    $('#txtCity').val(null);
                    $('#cmbSubDistrict').val(null);
                    $('#txtPost').val(null);
                    $('#txtMob').val(null);
                    $('#txtPhone').val(null);
                    $('#txtFax').val(null);
                    $('#phoneCode2').val(null);
                    $('#faxCode2').val(null);
                    $('.reqF_1').css("display", "none");
                });
            });
            $('#cmbCountry').change(function () {
                $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                    //alert('rishita');
                    $('#phoneCode').val(j.toString());
                    $('#faxCode').val(j.toString());
                });
            });

            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                    });
                });
                
                $('#cmbSubDistrict').change(function () {
                        //alert($('#cmbCorpSubdistrict').val());
                    if($('#cmbSubDistrict').val()=='Phuentsholing')
                        {$('#phoneCode2').val('05');
                        $('#faxCode2').val('05');}
                    else
                        {
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#phoneCode2').val(j);
                                $('#faxCode2').val(j);
                            });
                        }
                    });
            });
//            $(function () {
//                $('#cmbSubDistrict').change(function () {
//                    $.post("<%--<%=request.getContextPath()%>--%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
//                        $('#txtRegThana').html(j);
//                    });
//                });
//            });
            
            $(function () {
                $('#cmbSubDistrict').change(function () {
                    if($('#cmbSubDistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                });
            });
            

            function SubDistrictANDAreaCodeLoad()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbSubDistrict').val()}, function (j) {
                    $('#cmbSubDistrict').html(j);
                });
                if($('#cmbSubDistrict').val()=='Phuentsholing')
                        {$('#phoneCode2').val('05');
                        $('#faxCode2').val('05');}
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#phoneCode2').val(j);
                        $('#faxCode2').val(j);
                    });
                }
            }

            function TitleChange(TitleValue)
            {
                if (document.getElementById("cmbTitle").value == 'Other')
                {
                    document.getElementById('OtherTitleName').innerHTML = 'Other Title :<span>*</span>';
                    document.getElementById('OtherTitleValue').innerHTML = '<input class="formTxtBox_1" maxlength="50" type="text" id="OtherTitleValueInput" name="OtherTitleValueInput" onblur="TitleValidity()" value="' + TitleValue + '" />';
                } else
                {
                    document.getElementById('OtherTitleName').innerHTML = '';
                    document.getElementById('OtherTitleValue').innerHTML = '';
                    $('#OtherTitleMsg').html("");
                    document.getElementById("ValidityCheck").value = "1";
                }
            }
            function ChangeTitleValue()
            {
                if (document.getElementById("cmbTitle").value == 'Other')
                {
                    var OtherTitleValue = document.getElementById("OtherTitleValueInput").value;
                    document.getElementById("OtherTitle").value = OtherTitleValue;
                }
            }
            function TitleValidity()
            {
                if ($("#OtherTitleValueInput").val().length > 5)
                {
                    $('#OtherTitleMsg').html("Maximum 5 characters are allowed.");
                    document.getElementById("ValidityCheck").value = "0";
                } else if ($("#OtherTitleValueInput").val() == "")
                {
                    $('#OtherTitleMsg').html("Enter Title.");
                    document.getElementById("ValidityCheck").value = "0";
                } else
                {
                    $('#OtherTitleMsg').html("");
                    document.getElementById("ValidityCheck").value = "1";
                }
            }


        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#frmCompanyUser").validate({
                    rules: {
                        password: {required: true, minlength: 8, maxlength: 25, alphaForPassword: true},
                        confirmPassWord: {required: true, equalTo: "#txtPass"},
                        title: {required: true},
                        country: {required: true},
                        state: {required: true},
                        tinDocName: {required: true},
                        tinNo: {maxlength: 100},
                        firstName: {required: true, FirstLastName: true, maxlength: 50},
                        middleName: {maxlength: 50, FirstLastName: true},
                        lasatName: {//required: true, 
                            FirstLastName: true, maxlength: 50},
                        nationalIdNo: {required: true, maxlength: 11, minlength: 11, number: true}, //alphaNationalId: true},
                        address1: {required: true, maxlength: 250},
                        address2: {/*required: true,*/ maxlength: 250},
                        city: {//required: true, 
                            maxlength: 50, DigAndNum: true},
                        //upJilla: { required: true,maxlength: 50,DigAndNum:true},
                        //upJilla: {maxlength: 50},//, DigAndNum: true},
                        postcode: { /*required: true,*/minlength: 4, number: true},
                        phoneNo: {number: true, maxlength: 20},
                        phoneSTD: {number: true, maxlength: 10},
                        faxNo: {number: true, maxlength: 20},
                        faxSTD: {number: true, maxlength: 10},
                        mobileNo: {required: true, number: true},
                        designation: {required: true, alpha1: true, maxlength: 30},
                        department: {//required: true,
                            maxlength: 30},
                        comments: {required: true, maxlength: 120},
                        nationality: {required: true}
                    },
                    messages: {
                        password: {required: "<div class='reqF_1'>Please enter Password</div>",
                            minlength: "<div class='reqF_1'>Please enter at least 8 characters and password must contain both alphabets and numbers</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter alphanumeric only</div>"},
                        //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>"},

                        confirmPassWord: {required: "<div class='reqF_1'>Please retype Password.</div>",
                            //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>",
                            equalTo: "<div class='reqF_1' id='msgPwdNotMatch'>Password does not match. Please try again</div>"},
                        //NotequalTo: "<div style='color:green'><b>Password Matches</b></div>",

                        title: {required: "<div class='reqF_1'>Please select Title</div>"},
                        country: {required: "<div class='reqF_1'>Please select Country</div>"},
                        state: {required: "<div class='reqF_1'>Please enter Dzongkhag / District Name</div>"},
                        tinDocName: {required: "<div class='reqF_1'>Please select Document type</div>"},
                        tinNo: {
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        },
                        firstName: {
                            required: "<div class='reqF_1'>Please enter  First Name</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        middleName: {
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>"
                        },
                        lasatName: {
                            //required: "<div class='reqF_1'>Please enter Last Name</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        nationalIdNo: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed</div>",
                            //alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 11 digits are allowed</div>",
                            minlength: "<div class='reqF_1'>Minimum 11 digits are required</div>"
                        },
                        address1: {
                            required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        address2: {
                            //required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        city: {
                            //required: "<div class='reqF_1'>Please enter City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only</div>"
                        },
                        upJilla: {
                            //maxlength: "<br/>Maximum 50 characters are allowed",
                            //DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only</div>"
                        },
                        postcode: {
                            /*required: "<div class='reqF_1'>Please enter Postcode / Zip Code</div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 characters are required.</div>"
                        },
                        phoneNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed</div>"
                        },
                        phoneSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed</div>"
                        },
                        faxNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed"
                        },
                        faxSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed"
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>"
                        },
                        designation: {
                            required: "<div class='reqF_1'>Please enter Designation</div>",
                            alpha1: "<div class='reqF_1'>Please enter Alphabets only</div>",
                            maxlength: "<div class='reqF_1'>Max 30 characters are allowed</div>"
                        },
                        department: {
                            //required: "<div class='reqF_1'>Please enter Department</div>",
                            maxlength: "<div class='reqF_1'>Max 30 Characters are allowed</div>"
                        },
                        comments: {
                            required: "<div class='reqF_1'>Please enter Comments</div>",
                            maxlength: "<div class='reqF_1'>Maximum 120 characters are allowed</div>"
                        },
                        nationality: {required: "<div class='reqF_1'>Please enter Nationality </div>"}

                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "faxCode2") {
                            error.insertAfter("#txtFax");
                        } else if (element.attr("name") == "phoneCode2") {
                            error.insertAfter("#txtPhone");
                        } else if (element.attr("name") == "password") {
                            error.insertAfter("#passnote");
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

            });
        </script>
    </head>
    <jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tendererMasterDtBean"/>   
    <%
        boolean isEdit = false;
        boolean isRole = false;
        boolean isView = false;
        boolean isSuspend = false;
        boolean isResume = false;
    %>

    <%
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType = "userId";
        int auditId = Integer.parseInt(session.getAttribute("userId").toString());
        String auditAction = null;
        String moduleName = EgpModule.My_Account.getName();
        String remarks = null;

        String userId = "0";
        if (session.getAttribute("userId") != null) {
            userId = session.getAttribute("userId").toString();
            tendererMasterSrBean.setLogUserId(userId);
            commonService.setUserId(userId);
        }
        LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(userId);
        //tendererMasterSrBean.setLogUserId(userId);
        userId = null;
        String msg = null;
        if ("Submit".equalsIgnoreCase(request.getParameter("hdnbutton"))) {
            if (tendererMasterDtBean.getEmailId() != null) {
                if (!tendererMasterDtBean.getEmailId().trim().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    msg = "Please enter Valid e-mail ID";
                } else {

                    msg = commonService.verifyMail(tendererMasterDtBean.getEmailId());
                }
            } else {
                msg = "Please enter e-mail ID";
            }
            if (msg.equalsIgnoreCase("ok")) {
                remarks = "Tenderer[User id]: " + auditId + " has registered Company User";
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                    tendererMasterDtBean.setSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                    tendererMasterDtBean.setUpJilla("");
                }
                int ids[] = tendererMasterSrBean.createCompanyUser(tendererMasterDtBean, Integer.parseInt(session.getAttribute("userId").toString()), request.getParameter("password"));
                if (ids != null) {

                    auditAction = "Register Company User";
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                    response.sendRedirect("CompanyUser.jsp?action=view&uId=" + ids[0] + "&tId=" + ids[1] + "&msg=create&cId=" + request.getParameter("cId"));
                } else {
                    auditAction = "Error in Register Company User";
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                    response.sendRedirect("CompanyUser.jsp?msg=failure&cId=" + request.getParameter("cId"));
                }
            }
        } else if ("Update".equalsIgnoreCase(request.getParameter("hdnbutton"))) {
            remarks = "Tenderer[User id]: " + auditId + " has edited Company User Id:" + request.getParameter("uId") + " of Company Id:" + request.getParameter("cId");
            if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                tendererMasterDtBean.setSubDistrict("");
            }
            if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                tendererMasterDtBean.setUpJilla("");
            }
            if (tendererMasterSrBean.updateCompanyUser(tendererMasterDtBean, Integer.parseInt(request.getParameter("uId")), Integer.parseInt(request.getParameter("tId")))) {
                auditAction = "Edit Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyUser.jsp?action=view&uId=" + request.getParameter("uId") + "&tId=" + request.getParameter("tId") + "&msg=update&cId=" + request.getParameter("cId"));
            } else {
                auditAction = "Error in Edit Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyUser.jsp?action=edit&uId=" + request.getParameter("uId") + "&tId=" + request.getParameter("tId") + "&msg=failureUpdate&cId=" + request.getParameter("cId"));
            }
        } else if ("Suspend".equalsIgnoreCase(request.getParameter("hdnbutton"))) {
            remarks = request.getParameter("comments");

            if (tendererMasterSrBean.suspendUser(Integer.parseInt(request.getParameter("uId")), request.getParameter("comments"), session.getAttribute("userId").toString(), "suspended")) {
                auditAction = "Suspend Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyAdminListing.jsp?cId=" + request.getParameter("cId"));
            } else {
                auditAction = "Error in Suspend Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyUser.jsp?action=view&uId=" + request.getParameter("uId") + "&tId=" + request.getParameter("tId") + "&msg=failureSuspend&stat=s&cId=" + request.getParameter("cId"));
            }
        } else if ("Resume".equalsIgnoreCase(request.getParameter("hdnbutton"))) {
            remarks = request.getParameter("comments");

            if (tendererMasterSrBean.suspendUser(Integer.parseInt(request.getParameter("uId")), request.getParameter("comments"), session.getAttribute("userId").toString(), "approved")) {
                auditAction = "Resume Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyAdminListing.jsp?cId=" + request.getParameter("cId"));
            } else {
                auditAction = "Error in Resume Company User";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                response.sendRedirect("CompanyUser.jsp?action=view&uId=" + request.getParameter("uId") + "&tId=" + request.getParameter("tId") + "&msg=failureResume&stat=a&cId=" + request.getParameter("cId"));
            }
        }

        if ("Edit".equalsIgnoreCase(request.getParameter("edit"))) {
            response.sendRedirect("CompanyUser.jsp?action=edit&uId=" + request.getParameter("uId") + "&tId=" + request.getParameter("tId") + "&cId=" + request.getParameter("cId"));
        }
        if ("Ok".equalsIgnoreCase(request.getParameter("ok"))) {
            response.sendRedirect("CompanyAdminListing.jsp?cId=" + request.getParameter("cId"));
        }

        Object[] loginDetails = null;
        List<TblTendererMaster> ttm = null;
        if ("view".equals(request.getParameter("action"))) {
            isView = true;
        } else if ("edit".equals(request.getParameter("action"))) {
            isEdit = true;
        } else if ("arole".equals(request.getParameter("action"))) {
            isRole = true;
            isView = true;
        }
        if ("s".equals(request.getParameter("stat"))) {
            isSuspend = true;
        } else if ("a".equals(request.getParameter("stat"))) {
            isResume = true;
        }
        if (isEdit || isView || isRole) {
            loginDetails = tendererMasterSrBean.getDetailsLogin(Integer.parseInt(request.getParameter("uId")));
            ttm = tendererMasterSrBean.getDetailsMultipleUsers(Integer.parseInt(request.getParameter("tId")));
        }

        if (isView) // Adding Audit Trail Entry for Action = view
        {
            auditAction = "View Company User";
            remarks = "Tenderer[User Id]: " + auditId + " has viewed Company User Id:" + request.getParameter("uId") + " of Company Id:" + request.getParameter("cId");
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
        }

    %>
    <body onload="SubDistrictANDAreaCodeLoad(); <%if (isEdit) {%>TitleChange('<%=ttm.get(0).getTitle()%>') <% } else { %>TitleChange('') <%}%>" >
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea">
                    <div class="pageHead_1"><% if (isView && !isResume && !isSuspend) {%>View <% }
                            if (isSuspend) {%>Suspend <% }
                                if (isResume) {%>Resume <% }
                                        if (isEdit) {%>Edit <% }
                                        if (!isEdit && !isView && !isResume && !isSuspend) {%>Register <%}%>Company User <%if (isEdit) {%>Details <% }%>
                        <span style="float:right;"> <a class="action-button-goback" href="CompanyAdminListing.jsp?cId=<%=request.getParameter("cId")%>">Go back</a> </span>
                    </div>
                    <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failure")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg errorMsg ">Company User not created successfully</div>
                    <% } else if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failureSuspend")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg errorMsg ">Company User not Suspended successfully</div>
                    <% } else if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failureResume")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg errorMsg ">Company User not Resume successfully</div>
                    <% } else if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failureUpdate")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg errorMsg ">Company User not updated successfully</div>
                    <% } else if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("create")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg successMsg ">Company User created successfully</div>
                    <% } else if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("update")) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg successMsg ">Company User updated successfully</div>
                    <% } else if ("fail".equals(request.getParameter("msg"))) {%>
                    <div align="left" id="sucMsg" class="t_space responseMsg errorMsg">Problem in assigning Role</div>
                    <% }%>
                    <form id="frmCompanyUser" name="frmCompanyUser" method="post" action="">
                        <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                            <% if (!isView) {%>
                            <tr align="left">
                                <td style="font-style: italic" colspan="2" class="t-align-right">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <% }
                                if (isSuspend || isResume) {%>
                            <tr align="left">
                                <td style="font-style: italic" colspan="2" class="t-align-right">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <%}%>
                            <tr>
                                    <td class="ff" width="20%">e-mail ID : <% if (isView || isEdit) {
                                    } else {%><span>*</span><% }%></td>
                                <td width="80%">
                                    <%if (isView || isEdit) {%>
                                    <label><%=loginDetails[0]%></label>
                                    <% } else {%>
                                    <input name="emailId" type="text" class="formTxtBox_1" id="txtMail" style="width:200px;" /> <div class="passnote">(e-mail ID should be valid. Example: xyz@gmail.com)</div>
                                    <div id="mailMsg" style="color: red; "></div>
                                    <% }%>
                                </td>
                            </tr>
                            <%if (isView || isEdit) {
                                } else {%>
                            <tr>
                                <td class="ff">Password : <span>*</span></td>
                                <td><input name="password" type="password" class="formTxtBox_1" id="txtPass" style="width:200px;" onblur="chkPasswordMatches();" autocomplete="off" />
                                    <div id="passnote">(Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                        <br/>Special characters may be added)</div></td>
                            </tr>
                            <tr>
                                <td class="ff">Confirm Password : <span>*</span></td>
                                <td><input name="confirmPassWord" type="password" class="formTxtBox_1" id="txtConPass" style="width:200px;" onblur="chkPasswordMatches();" onpaste="return copyPaste();" autocomplete="off" />
                                    <div style="color: green" id="pwdMatchMsg"></div>
                                </td>
                            </tr>
                            <% }%>
                            <tr>
                                <td class="ff">Nationality : <% if (!isView) {%> <span>*</span> <% }%></td>
                                <td><% if (isView) {
                                        out.print(loginDetails[1]);
                                    } else {%>
                                    <select name="nationality" class="formTxtBox_1" id="cmbNational" style="width:auto;">
                                        <% for (SelectItem nList : loginMasterSrBean.getNationalityList()) {%>
                                        <option value="<%=nList.getObjectId()%>" <%
                                            String selectedValue = "";
                                            if (isEdit) {
                                                selectedValue = loginDetails[1].toString();
                                            } else {
                                                //Change Bangladesh to Bhutan,Proshanto
                                                selectedValue = "Bhutanese";
                                            }
                                            if (nList.getObjectValue().equalsIgnoreCase(selectedValue)) {%>selected<%}%>><%=nList.getObjectValue()%></option>
                                        <% }%>
                                    </select>
                                    <!--                                    <input name="nationality" type="text" class="formTxtBox_1" value="<% //if (isEdit) {
                                        // out.print(loginDetails[1]);
                                        //}%>" id="txtNationality" style="width:200px;"/>-->
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="22%">Title : <% if (!isView) {%><span>*</span><% }%></td>
                                <td width="78%">
                                    <% if (isView) {
                                            out.print(ttm.get(0).getTitle());
                                        }else {%>
                                    <select name="title" class="formTxtBox_1" id="cmbTitle" style="width: 100px;" <%if (isEdit) {%>onchange="TitleChange('<%=ttm.get(0).getTitle()%>')" <% } else { %>onchange="TitleChange('')" <%}%>>
                                        <option value="">Select</option>
                                        <option <%if (isEdit) {
                                                 if (ttm.get(0).getTitle().equals("Mr.")) {%>selected<%}
                                                      }%> value="Mr.">Mr.</option>
                                        <option <%if (isEdit) {
                                                 if (ttm.get(0).getTitle().equals("Ms.")) {%>selected<%}
                                                      }%> value="Ms.">Ms.</option>
                                        <option <%if (isEdit) {
                                                 if (ttm.get(0).getTitle().equals("Mrs.")) {%>selected<%}
                                                      }%> value="Mrs.">Mrs.</option>
                                        <option value="Dasho" <%if (isEdit) {
                                                                if (ttm.get(0).getTitle().equals("Dasho")) {%>selected<%}
                                                                    }%>>Dasho</option>
                                        <option value="Dr." <%if (isEdit) {
                                                                if (ttm.get(0).getTitle().equals("Dr.")) {%>selected<%}
                                                                    }%>>Dr.</option>
                                        <option id="OtherTitle" value="Other" <%if (isEdit) {
                                                                if (!ttm.get(0).getTitle().equals("Dasho") && !ttm.get(0).getTitle().equals("Dr.")&&!ttm.get(0).getTitle().equals("Mrs.")&&!ttm.get(0).getTitle().equals("Ms.")&&!ttm.get(0).getTitle().equals("Mr.")) {%>selected<%}
                                                                    }%>>Other</option>
                                    </select><% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><label id="OtherTitleName"></label></td>
                                <td>
                                    <label id="OtherTitleValue"></label>
                                    <span id="OtherTitleMsg" style="color: red;">&nbsp;</span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">First Name : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getFirstName());
                                        } else{%>
                                    <input name="firstName" type="text" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getFirstName());
                                        }%>" class="formTxtBox_1" maxlength="50" id="txtFirstName" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Middle Name : </td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getMiddleName());
                                        } else{%>
                                    <input name="middleName" type="text" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getMiddleName());
                                        }%>" class="formTxtBox_1" maxlength="50" id="txtMiddleName" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Last Name : <% if (!isView) {%><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getLastName());
                                        } else{%>
                                    <input name="lasatName" type="text" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getLastName());
                                        }%>" class="formTxtBox_1" maxlength="50" id="txtLastName" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <%--<tr id="trBangla" style="display: none">
                                <td class="ff">Name in Dzongkha :</td>
                                <td>
                                    <%if (isView) {
                                            if (ttm.get(0).getFullNameInBangla() != null) {
                                                out.print(BanglaNameUtils.getUTFString(ttm.get(0).getFullNameInBangla()));
                                            }
                                        } else{%>
                                    <input name="banglaName" type="text" value="<%if (isEdit) {
                                            if (ttm.get(0).getFullNameInBangla() != null) {
                                                out.print(BanglaNameUtils.getUTFString(ttm.get(0).getFullNameInBangla()));
                                            }
                                        }%>" class="formTxtBox_1" id="txtNameBangla" maxlength="300" style="width:200px;"/>
                                    <div class="passnote">(If Bhutanese)</div>
                                    <% }%>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="ff">CID No. : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getNationalIdNo());
                                        } else{%>
                                    <input name="nationalIdNo" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getNationalIdNo());
                                        }%>" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;"  maxlength="25"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Designation : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                       <% if (isView) {
                                            out.print(ttm.get(0).getDesignation());
                                        } else{%>
                                       <input name="designation" type="text" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getDesignation());
                                        }%>" class="formTxtBox_1" maxlength="30" id="txtDesig" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Department : <% if (!isView) {%><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getDepartment());
                                        } else{%>
                                    <input name="department" type="text" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getDepartment());
                                        }%>" class="formTxtBox_1" maxlength="30" id="txtDept" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Address : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getAddress1());
                                        } else{%>
                                    <textarea name="address1" rows="2" class="formTxtBox_1" id="txtaAddr1" maxlength="250" style="width:400px;"><%if (isEdit) {
                                            out.print(ttm.get(0).getAddress1());
                                        }%></textarea>
                                        <% }%>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="ff">Address Line 2 : </td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getAddress2());
                                        } else{%>
                                    <textarea name="address2" rows="2" class="formTxtBox_1" id="txtaAddr2" maxlength="250" style="width:400px;"><%if (isEdit) {
                                            out.print(ttm.get(0).getAddress2());
                                        }%></textarea>
                                        <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Country : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getCountry());
                                        } else {%>
                                    <select name="country" class="formTxtBox_1" id="cmbCountry">
                                        <%          String countryName = "Bhutan";
                                            if (isEdit) {
                                                countryName = ttm.get(0).getCountry();
                                            }
                                            for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                        %>
                                        <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryName)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                        <%}%>
                                    </select>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Dzongkhag / District : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getState());
                                        } else {%>
                                    <select name="state" class="formTxtBox_1" id="cmbState" onchange="SubDistrictANDAreaCodeLoad()" style="width:218px;">
                                            <%String cntName = "";
                                                if (isEdit) {
                                                    cntName = ttm.get(0).getCountry();
                                                } else {
                                                    //Change Bangladesh to Bhutan,Proshanto
                                                    cntName = "Bhutan";
                                                }

                                                for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                            <option value="<%=state.getObjectValue()%>" <%if (isEdit) {
                                                    if (state.getObjectValue().equals(ttm.get(0).getState())) {%>selected<%}
                                                        } else //Change Dhaka to Thimphu,Proshanto
                                                            if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                        <%}%>
                                    </select>
                                    <% }%>
                                </td>
                            </tr>
                            <tr id="trRSubdistrict">
                                <td class="ff">Dungkhag / Sub-district : </td>
                                <td><% if (isView) {
                                        out.print(ttm.get(0).getSubDistrict());
                                    } else {%><select name="subDistrict" class="formTxtBox_1" id="cmbSubDistrict" style="width:218px;">

                                        <%String regsubdistrictName = "";
                                            if (isEdit) {
                                                regsubdistrictName = ttm.get(0).getState();
                                            } else {
                                                regsubdistrictName = "Thimphu";
                                            }
                                            for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regsubdistrictName)) {%>
                                        <option value="<%=subdistrict.getObjectValue()%>" <%if (isEdit) {
                                                if (subdistrict.getObjectValue().equals(ttm.get(0).getSubDistrict())) {
                                                %>selected<%
                                                    }
                                                } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                selected<%
                                                    }%>><%=subdistrict.getObjectValue()%></option>
                                        <%}
                                            }%>
                                    </select></td>

                            </tr>

                            <tr id="trRthana">
                                <td class="ff" height="30">Gewog : </td>
                                <td><% if (isView) {
                                            out.print(ttm.get(0).getUpJilla());
                                        } else {%><select name="upJilla" class="formTxtBox_1" id="txtRegThana" style="width:218px;">
                                        <%String regState = "Thimphu";
                                            String regSubdistrict = "";
                                            if (isEdit) {
                                                regState = ttm.get(0).getState();
                                                regSubdistrict = ttm.get(0).getSubDistrict();
                                            }
                                            if (!regState.isEmpty() && !regSubdistrict.isEmpty()) {
                                                for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(regSubdistrict)) {%>
                                        <option value="<%=gewoglist.getObjectValue()%>" <%if (isEdit) {
                                                if (gewoglist.getObjectValue().equals(ttm.get(0).getUpJilla())) {
                                                %>selected<%
                                                    }
                                                } else if (gewoglist.getObjectValue().equals("")) {%>
                                                selected<%
                                                    }%>><%=gewoglist.getObjectValue()%></option>
                                        <%}
                                        } else {
                                            for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(regState)) {%>
                                        <option value="<%=gewoglist.getObjectValue()%>" <%if (isEdit) {
                                                if (gewoglist.getObjectValue().equals(ttm.get(0).getUpJilla())) {
                                                %>selected<%
                                                        }
                                                    }%>

                                                ><%=gewoglist.getObjectValue()%></option>
                                        <%}
                                                }
                                            }
                                        %>
                                    </select></td>
                            </tr>

                            <tr>
                                <td class="ff">City / Town : <% if (!isView) {%><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getCity());
                                        } else{%>
                                    <input name="city" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getCity());
                                        }%>" type="text" class="formTxtBox_1" maxlength="50" id="txtCity" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <%--<tr id="trRthana">
                                <td class="ff">Gewog : <% if (!isView) {%> <% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getUpJilla());
                                        } else{%>
                                    <input name="upJilla" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getUpJilla());
                                        }%>" type="text" class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;"/><div id="upErr" style="color: red;"></div>
                                    <% }%>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="ff">Post Code : <!--span>*</span--></td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getPostcode());
                                        } else{%>
                                    <input name="postcode" value="<%if (isEdit) {
                                            out.print(ttm.get(0).getPostcode());
                                        }%>" type="text" class="formTxtBox_1" maxlength="10" id="txtPost" style="width:200px;"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Phone No. : </td>
                                <td>
                                    <% if (isView) {
                                            out.print(ttm.get(0).getPhoneNo());
                                        } else {%>
                                    <input class="formTxtBox_1" name="phoneCode" style="width: 30px" id="phoneCode" value="<%//CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                        if (isEdit) {
                                            if (ttm.get(0).getPhoneNo() != null && !ttm.get(0).getPhoneNo().equals("")) {
                                                String[] spliltDash = ttm.get(0).getPhoneNo().split("-");
                                                out.print(spliltDash[0]);
                                            } else {
                                                out.print(commonService.countryCode(ttm.get(0).getCountry(), false));
                                            }
                                        } else {
                                            //Change Bangladesh to Bhutan,Proshanto
                                            out.print(commonService.countryCode("Bhutan", false));
                                        }%>" readonly/>-<input id="phoneCode2" type="text" value="<%if (isEdit) {
                                                if (!"".equals(ttm.get(0).getPhoneNo())) {
                                                    out.print(ttm.get(0).getPhoneNo().split("-")[1]);
                                                }

                                            }%>" name="phoneSTD" class="formTxtBox_1" style="width:50px;" />-<input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhone" style="width:100px;"  value="<%if (isEdit) {
                                                    if (!"".equals(ttm.get(0).getPhoneNo())) {
                                                        out.print(ttm.get(0).getPhoneNo().split("-")[2]);
                                                    }
                                                }%>"/>
                                    <% }%>
                                </td>
                            </tr>
                            <tr>
                                <%if (isEdit || isView) {
                                        //Change Bangladesh to Bhutan,Proshanto
                                        if (!ttm.get(0).getCountry().equalsIgnoreCase("Bhutan")) {%>
                            <script type="text/javascript">
                                $('#trBangla').css("display", "none");
                                $('#trRthana').css("display", "none");
                                $('#trRSubdistrict').css("display", "none");
                            </script>
                            <%}
                                }%>
                            <td class="ff">Fax No. : </td>
                            <td>
                                <% if (isView) {
                                        out.print(ttm.get(0).getFaxNo());
                                    } else {%>
                                <input class="formTxtBox_1" name="faxCode" style="width: 30px" id="faxCode" value="<%//CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                    if (isEdit) {
                                        if (ttm.get(0).getFaxNo() != null && !ttm.get(0).getFaxNo().equals("")) {
                                            String[] spliltDash = ttm.get(0).getFaxNo().split("-");
                                            out.print(spliltDash[0]);
                                        } else {
                                            out.print(commonService.countryCode(ttm.get(0).getCountry(), false));
                                        }
                                    } else {
                                        //Change Bangladesh to Bhutan,Proshanto
                                        out.print(commonService.countryCode("Bhutan", false));
                                    }%>" readonly/>-<input id="faxCode2" type="text" value="<%if (isEdit) {
                                            if (!"".equals(ttm.get(0).getFaxNo())) {
                                                out.print(ttm.get(0).getFaxNo().split("-")[1]);
                                            }
                                        }%>" name="faxSTD" class="formTxtBox_1" style="width:50px;"/>-<input name="faxNo" type="text" class="formTxtBox_1" maxlength="20" id="txtFax" style="width:100px;"  value="<%if (isEdit) {
                                                if (!"".equals(ttm.get(0).getFaxNo())) {
                                                    out.print(ttm.get(0).getFaxNo().split("-")[2]);
                                                }
                                            }%>"/>
                                <% }%>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff">Mobile No. : <% if (!isView) {%><span>*</span><% }%></td>
                                <td>
                                    <% if (isView) {
                                            out.print(commonService.countryCode(ttm.get(0).getCountry(), false)+"-"+ttm.get(0).getMobileNo());
                                        } else {%>
                                    <input name="mobileCode" class="formTxtBox_1" style="width: 30px" id="cntCode" value="<%//CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                        if (isEdit) {
                                            out.print(commonService.countryCode(ttm.get(0).getCountry(), false));
                                        } else {
                                            //Change Bangladesh to Bhutan,Proshanto
                                            out.print(commonService.countryCode("Bhutan", false));
                                        }%>" readonly/>-<input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMob" style="width:160px;"  value="<%if (isEdit) {
                                                //String splitDash[] = ttm.get(0).getMobileNo().split("-");
                                                out.print(ttm.get(0).getMobileNo());
                                            }%>"/>
                                    <span id="mobMsg" style="color: red;">&nbsp;</span>
                                    <% }%>
                                </td>
                            </tr>
                            <% if (isSuspend || isResume) {%>
                            <tr>
                                <td class="ff">Comments : <span>*</span></td>
                                <td><textarea name="comments" id="comments" rows="3" class="formTxtBox_1" style="width:400px;"></textarea></td>
                            </tr>
                            <% }%>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <% if (isView) {%>
                                    <input type="hidden" value="<%=ttm.get(0).getTblCompanyMaster().getCompanyId()%>" id="companyId" name="companyId"/>
                                    <%if (isSuspend) {%>
                                    <label class="formBtn_1">
                                        <input type="Submit" name="button" id="Suspend" value="Suspend"/>
                                        <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                    </label>
                                    <% } else if (isResume) {%>
                                    <label class="formBtn_1">
                                        <input type="Submit" name="button" id="Resume" value="Resume"/>
                                        <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                    </label>
                                    <%
                                    } else if (!isRole) {
                                    %>
                                    <label class="formBtn_1"><input id="ok" name="ok" value="Ok" type="submit"/></label>&nbsp;<label class="formBtn_1"><input id="edit" name="edit" value="Edit" type="submit"/></label>
                                        <% }
                                        } else if (isEdit) {%>
                                    <input type="hidden" value="<%=ttm.get(0).getTblCompanyMaster().getCompanyId()%>" id="companyId" name="companyId"/>
                                    <label class="formBtn_1">
                                        <input type="Submit" name="button" id="Update" value="Update"/>
                                        <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                    </label>
                                    <%} else {%>
                                    <label class="formBtn_1">
                                        <input type="Submit" name="button" id="Submit" value="Submit"/>
                                        <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                    </label>
                                    <% }%>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="ValidityCheck" value="1" />
                    </form>
                    <%--<%if (isRole) {%>
                    <div class="t-align-center">

                        <a href="<%=request.getContextPath()%>/CompanyAdminServlet?action=assignRole&cId=<%=request.getParameter("cId")%>&tId=<%=request.getParameter("tId")%>" class="anchorLink" onclick="return confirm('Do you really want to assign Admin Role to this user?');">Assign Role</a>
                    </div>
                    <%}%>--%>
                </div>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script type="text/javascript">
                function chkPasswordMatches() {
                    var objPwd = document.getElementById("txtPass");
                    var objConfirmPwd = document.getElementById("txtConPass");
                    if (objPwd != null && objConfirmPwd != null) {
                        if ($.trim(objPwd.value) == "" || $.trim(objConfirmPwd.value) == "") {
                        } else {
                            var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
                            if (objPwd.value == objConfirmPwd.value) {
                                var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
                                if (msgPwdNotMatch != null) {
                                    if (msgPwdNotMatch.innerHTML == "Password does not match. Please try again") {
                                        msgPwdNotMatch.style.diplay = "none";
                                        msgPwdNotMatch.innerHTML = "";
                                    }
                                }
                                msgPwdMatchObjDiv.innerHTML = "Password Matches";
                            } else {
                                msgPwdMatchObjDiv.innerHTML = "";
                            }
                        }
                    }
                }
                function copyPaste() {
                    jAlert("Copy and Paste not allowed.", "Company User Registration", function (RetVal) {
                    });
                    return false;
                }
                $(function () {
                    $('#txtRegThana').blur(function () {
                        if ($.trim($('#txtRegThana').val()) == "") {
                            //$('#upErr').html("Please enter Gewog");
                            //return false;
                        } else {
                            $('#upErr').html(null);
                        }
                    });
                });
            </script>
        <%
            tendererMasterDtBean = null;
            tendererMasterSrBean = null;
            loginMasterSrBean = null;
        %>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
