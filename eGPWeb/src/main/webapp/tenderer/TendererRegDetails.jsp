<%--
    Document   : TendererRegDetails
    Created on : Dec 31, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

    </head>       
    <body>
        <%
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    TblLoginMaster tblLoginMaster = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId"))).get(0);
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">View Registration Details</div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <tr>
                        <td width="18%" class="ff">e-mail ID : </td>
                        <td width="82%"><%=tblLoginMaster.getEmailId()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Nationality : </td>
                        <td><%=tblLoginMaster.getNationality()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country of Business : </td>
                        <td><%=tblLoginMaster.getBusinessCountryName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registration Date & Time : </td>
                        <td><%if(tblLoginMaster.getRegisteredDate()!=null){String date = DateUtils.gridDateToStr(tblLoginMaster.getRegisteredDate());out.print(date.substring(0, date.lastIndexOf(":")));}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registration Type : </td>
                        <td>
                            <%if (tblLoginMaster.getRegistrationType().equals("contractor")) {%>Bidder / Consultant<%}%>                            
                            <%if (tblLoginMaster.getRegistrationType().equals("individualconsultant")) {%>Individual Consultant<%}%>
                            <%if (tblLoginMaster.getRegistrationType().equals("govtundertaking")) {%> Government owned Enterprise<%}%>
                            <%if (tblLoginMaster.getRegistrationType().equals("media")) {%>Media<%}%>                            
                        </td>
                    </tr>
                </table>
                    <table border="0" cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td width="18%">&nbsp;</td>
                            <td width="82%" align="left">
                                <a href="<%if(request.getParameter("cId").equals("1")){%>TendererDetails.jsp?jv=no&payId=<%=request.getParameter("payId")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&s=<%=request.getParameter("s")%><%}else{%>TendererCompDetails.jsp?cId=<%=request.getParameter("cId")%>&payId=<%=request.getParameter("payId")%>&uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&s=<%=request.getParameter("s")%>&jv=<%if(tblLoginMaster.getIsJvca().equals("yes")){%>y<%}else{%>n<%}%><%}%>"  class="anchorLink">Next</a>
                            </td>
                        </tr>
                    </table>                    
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
