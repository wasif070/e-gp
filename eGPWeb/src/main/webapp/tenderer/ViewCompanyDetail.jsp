<%--
    Document   : ViewCompanyDetail
    Created on : Jan 05, 2011, 11:40 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblBiddingPermission"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
    </head>
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                     if(session.getAttribute("userId")!=null && session.getAttribute("userId").toString().equals(request.getParameter("uId"))){
                        contentAdminService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    TblCompanyMaster tblCompanyMaster = contentAdminService.findCompanyMaster("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId"))).get(0);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    List<TblBiddingPermission> biddingPermisionList = contentAdminService.findBiddingpermission("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId").toString()), "isReapplied", Operation_enum.EQ, 0);
                    String reg_qString = "";
                    if("no".equals(request.getParameter("top"))){
                        reg_qString = "&top=no";
                    }
                    boolean isJVCA = commonService.isUserJVCA(request.getParameter("uId"));
                    String displayTag = "";
                    if(isJVCA){
                        displayTag = " style='display: none;' ";
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if("".equals(reg_qString)){%>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%}else{if(session.getAttribute("userId")==null){response.sendRedirect(request.getContextPath()+"/SessionTimedOut.jsp");}}%>
                <div class="tableHead_1 t_space">Company Details</div>
                <div class="stepWiz_1 t_space">
                <ul>
                <%                            
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(request.getParameter("uId")));//
                            String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];
                            if (pageList.size() == 3) {
                %>
               <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 4) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 5) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%}%>
            </ul>
            </div>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">
                    <tr<%=displayTag%> style="display: none">
                        <td width="20%" class="ff">Trade License Number : </td>
                        <td width="80%"><%if(tblCompanyMaster.getTradeLicenseNumber()!=null)
                            {out.print(tblCompanyMaster.getTradeLicenseNumber());}%></td>
                    </tr>
                    <tr<%=displayTag%> style="display: none">
                        <td width="20%" class="ff">Company Registration No. : </td>
                        <td width="80%"><%if(tblCompanyMaster.getCompanyRegNumber()!=null)
                            {out.print(tblCompanyMaster.getCompanyRegNumber());}%></td>
                    </tr>
                    <tr>
                        <td width="20%" class="ff">Company Name : </td>
                        <td width="80%"><%=tblCompanyMaster.getCompanyName()%></td>
                    </tr>
                    <tr<%=displayTag%> style="display: none">
                        <td class="ff">Company Name in Dzongkha : </td>
                        <td><%if(tblCompanyMaster.getCompanyNameInBangla()!=null){out.print(BanglaNameUtils.getUTFString(tblCompanyMaster.getCompanyNameInBangla()));}%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Company's Legal Status : </td>
                        <td>
                            <%if(tblCompanyMaster.getLegalStatus().equals("public")){%>Public Limited Company<%}%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("private")){%>Private Limited Company<%}%>
                            <%--<%if(tblCompanyMaster.getLegalStatus().equals("partnership")){%>Partnership<%}%>--%>
                            <%if(tblCompanyMaster.getLegalStatus().equals("proprietor")){%>Sole Proprietorship<%}%>
                            <%--<%if(tblCompanyMaster.getLegalStatus().equals("government")){%>Government Undertaking.<%}%>--%>
                        </td>
                    </tr>
                    <tr<%=displayTag%>> 
                        <td class="ff">Procurement Category :</td>
                        <td>
                            <% if (biddingPermisionList.size() != 0) 
                                {
                                    for (int i = 0; i < biddingPermisionList.size(); i++) {                                        
                                        out.println(biddingPermisionList.get(i).getProcurementCategory().toString());
                                        if (biddingPermisionList.get(i).getWorkType() != null && !biddingPermisionList.get(i).getWorkType().isEmpty()) {
                                            out.print(", " + biddingPermisionList.get(i).getWorkType().toString());
                                        }
                                        if (biddingPermisionList.get(i).getWorkCategroy() != null && !biddingPermisionList.get(i).getWorkCategroy().isEmpty()) {
                                            out.print(", " + biddingPermisionList.get(i).getWorkCategroy().toString());
                                        }
                                        out.print("<br/>");
                                    }
                                }
                            %>
                        </td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Company's Establishment Year : </td>
                        <td><%=tblCompanyMaster.getEstablishmentYear()%></td>
                    </tr>
<!--                    <tr<%--<%=displayTag%> style="display: none">
                        <td class="ff">Work Category : </td>
                        <td>
                            <%--   <%if(tblCompanyMaster.getWorkCategory().equals("-1")){%> <%}%> 
                            <%if(tblCompanyMaster.getWorkCategory()!=null){%>     Edit by aprojit
                            <%if(tblCompanyMaster.getWorkCategory().equals("W1")){%>W1<%}%>
                            <%if(tblCompanyMaster.getWorkCategory().equals("W2")){%>W2<%}%>
                            <%if(tblCompanyMaster.getWorkCategory().equals("W3")){%>W3<%}%>
                            <%if(tblCompanyMaster.getWorkCategory().equals("W4")){%>W4<%} }%>--%>
                        </td>
                    </tr>-->
                    <tr<%=displayTag%> style="display: none">
                        <td class="ff">Trade License Issue Date : </td>
                        <td><%if(tblCompanyMaster.getLicIssueDate()!=null){out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicIssueDate()));}%>
                        </td>
                    </tr>
                    <tr<%=displayTag%> style="display: none">
                        <td class="ff">Trade License Expiry Date : </td>
                        <td><%if(tblCompanyMaster.getLicExpiryDate()!=null){out.print(DateUtils.formatStdDate(tblCompanyMaster.getLicExpiryDate()));}%>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Tax Payment Number : </td>
                        <td>
                            <%if(tblCompanyMaster.getTinDocName() != null){if(tblCompanyMaster.getTinNo().equalsIgnoreCase("other")){
                                out.print(tblCompanyMaster.getTinDocName().substring(0, tblCompanyMaster.getTinDocName().indexOf("$"))+"<div class=\"formNoteTxt\">(Other No)</div>"+tblCompanyMaster.getTinDocName().substring(tblCompanyMaster.getTinDocName().indexOf("$")+1,tblCompanyMaster.getTinDocName().length())+"<div class=\"formNoteTxt\">(Description)</div>");}
                            }else{
                                if (tblCompanyMaster.getTinDocName() != null )
                                  out.print(tblCompanyMaster.getTinDocName());}%>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Statutory Certificate No. : </td>
                        <td><%if(tblCompanyMaster.getStatutoryCertificateNo()!=null){out.print(tblCompanyMaster.getStatutoryCertificateNo());}%></td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Nature of Business : </td>
                        <td><%=tblCompanyMaster.getSpecialization()%></td>
                    </tr>
                    <tr>
                        <td class="ff"><%if(!isJVCA){%>Registered <%}%>Address : </td>
                        <td><%=tblCompanyMaster.getRegOffAddress()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Origin of Country : </td>
                        <td><%if(tblCompanyMaster.getOriginCountry()!=null){out.print(tblCompanyMaster.getOriginCountry());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getRegOffCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblCompanyMaster.getRegOffState()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dungkhag / Sub-district  : </td>
                        <td><%if(tblCompanyMaster.getRegOffSubDistrict()!=null)
                            out.print(tblCompanyMaster.getRegOffSubDistrict());%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%if(tblCompanyMaster.getRegOffCity()!=null)
                            out.print(tblCompanyMaster.getRegOffCity());%></td>
                    </tr>
                    <tr id="trRthana">
                        <td class="ff">Gewog : </td>
                        <td><%if(tblCompanyMaster.getRegOffUpjilla()!=null)
                            out.print(tblCompanyMaster.getRegOffUpjilla());%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code  : </td>
                        <td><%if(tblCompanyMaster.getRegOffPostcode()!=null)
                            out.print(tblCompanyMaster.getRegOffPostcode());%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No : </td>
                        <td><%if(tblCompanyMaster.getRegOffMobileNo()!=null){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getRegOffMobileNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No : </td>
                        <td><%if(tblCompanyMaster.getRegOffPhoneNo()!=null){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getRegOffPhoneNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Fax No : </td>
                        <td><%if(tblCompanyMaster.getRegOffFaxNo()!=null){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getRegOffFaxNo());}%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Corporate / Head office<br />
                            Address : </td>
                        <td><%=tblCompanyMaster.getCorpOffAddress()%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getCorpOffCountry()%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblCompanyMaster.getCorpOffState()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dungkhag / Sub-district  : </td>
                        <td><%if(tblCompanyMaster.getCorpOffSubDistrict()!=null)
                            out.print(tblCompanyMaster.getCorpOffSubDistrict());%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">City / Town : </td>
                        <td><%if(tblCompanyMaster.getCorpOffCity()!=null)
                            out.print(tblCompanyMaster.getCorpOffCity());%></td>
                    </tr>
                    <tr id="trCthana"<%=displayTag%>>
                        <td class="ff">Gewog : </td>
                        <td><%if(tblCompanyMaster.getCorpOffUpjilla()!=null)
                            out.print(tblCompanyMaster.getCorpOffUpjilla());%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Post Code : </td>
                        <td><%if(tblCompanyMaster.getCorpOffPostcode()!=null)
                            out.print(tblCompanyMaster.getCorpOffPostcode());%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No : </td>
                        <td><%if(tblCompanyMaster.getCorpOffMobMobileNo()!=null){out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(),false)+"-"+tblCompanyMaster.getCorpOffMobMobileNo());}%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Phone No : </td>
                        <td><%if(tblCompanyMaster.getCorpOffPhoneno()!=null){out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(),false)+"-"+tblCompanyMaster.getCorpOffPhoneno());}%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Fax No : </td>
                        <td><%if(tblCompanyMaster.getCorpOffFaxNo()!=null){out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(),false)+"-"+tblCompanyMaster.getCorpOffFaxNo());}%></td>
                    </tr>
                    <tr<%=displayTag%>>
                        <td class="ff">Company's website : </td>
                        <td><%=tblCompanyMaster.getWebsite()%></td>
                    </tr>
                </table>
                <table border="0" cellspacing="10" cellpadding="0" width="100%">
                        <tr>
                            <td width="18%">&nbsp;</td>
                            <td width="82%" align="left">
                                <a href="ViewPersonalDetail.jsp?tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"  class="anchorLink">Next</a>
                            </td>
                        </tr>
                    </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <%if(!"0".equalsIgnoreCase(request.getParameter("jvca"))){%>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%}%>
    <%--<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%}%>--%>
</html>
