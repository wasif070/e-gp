<%-- 
    Document   : BidForm
    Created on : Nov 16, 2010, 5:18:59 PM
    Author     : Sanjay,rishita,Dipal
    Modify on  : Dec 14 2011 
    Bug id     : 4963
--%>

<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/border-radius.css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/lang/en.js"></script>
        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
        <body>
          
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">
                <script type="text/javascript">
                    

                function chkValidate(obj){
                    if($('#frmBidSubmit').valid()){
                        //obj.disabled = true;
                        $('#save').hide();
                       return true;
                    }
                }

                function validate()
                {
                    var finalRows;
                    for(var totalTables=0;totalTables<arr.length;totalTables++)
                    {
                        if(chkEdit == true)
                            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
                        else
                            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

                        for(var rowCount=0;rowCount<=finalRows;rowCount++)
                        {
                            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
                            {
                                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                                {
                                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value=="")
                                    {
                                        jAlert("One of the form fields is empty","Title", function(retVal) {});
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    return true;
                }
                    var verified = true;
                    function GetCal(txtname,controlname,tableId,obj,tableIndex)
                    {
                        new Calendar({
                            inputField: txtname,
                            trigger: controlname,
                            showTime: 24,
                            dateFormat:"%d-%b-%Y",
                            onSelect: function() {
                                var date = Calendar.intToDate(this.selection.get());
                                LEFT_CAL.args.min = date;
                                LEFT_CAL.redraw();
                                this.hide();
                                document.getElementById(txtname).focus();
                                CheckDate(tableId,obj,txtname,txtname,tableIndex);
                            }
                        });

                        var LEFT_CAL = Calendar.setup({
                            weekNumbers: false
                        })

                    }
                    //alert(SignerAPI);
                </script>
                    <!--Middle Content Table Start-->
<%
                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }        
                int formId = 0;
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }

                int negId = 0;
                if (request.getParameter("negId") != null) {
                    negId = Integer.parseInt(request.getParameter("negId"));
                }

                 int negBidformId = 0;
                if (request.getParameter("negBidformId") != null) {
                    negBidformId = Integer.parseInt(request.getParameter("negBidformId"));
                }

                int uId = 0;
                if (request.getParameter("uId") != null) {
                    uId = Integer.parseInt(request.getParameter("uId"));
                }
                
                int bidId = 0;
                if (request.getParameter("bidId") != null) {
                    bidId = Integer.parseInt(request.getParameter("bidId"));
                }

                int lotId = 0;
                if (request.getParameter("lotId") != null) {
                    lotId = Integer.parseInt(request.getParameter("lotId"));
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
                     action = request.getParameter("action");
                }

                int formBoqId=0;
                if(request.getParameter("boqId")!=null && !"".equalsIgnoreCase(request.getParameter("boqId")))
                {
                         formBoqId = Integer.parseInt(request.getParameter("boqId"));
                }

                boolean isEdit = false;
                if("Edit".equalsIgnoreCase(action)){
                    isEdit = true;
                }

                boolean isView = false;
                if("View".equalsIgnoreCase(action)){
                    isView = true;
                    
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

                    String idType="tenderId";
                    int auditId=tenderId;
                    String auditAction="Bidder has view Forms";
                    String moduleName=EgpModule.Negotiation.getName();
                    String remarks="Tenderer(User id): "+session.getAttribute("userId")+" has View Form id:"+formId+" for Negotiation";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);     
                }

                int tableCount = 0;
                tableCount = tenderBidSrBean.getNoOfTable(formId);

                if(action != null && "".equalsIgnoreCase(action) || "Fill".equalsIgnoreCase(action)){
%>
<script>
        var gblCnt =0;
        var isMultiTable = false;
        var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
        var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
        var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
        var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
        var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
        var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
        var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
        var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
        var brokenFormulaIds = new Array();
        var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
        var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
        var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
        var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
        var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
        var arrDataTypesforCell = new Array(<%=tableCount%>);
        var arrColTotalIds = new Array(<%=tableCount%>);
        var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
        var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
        var isColTotalforTable = new Array(<%=tableCount%>);
        for(var i=0;i<isColTotalforTable.length;i++)
                isColTotalforTable[i]=0;
        var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
        var arrColFillBy=new Array(<%=tableCount%>); // Stores the Array of Column having fill by Tenderer/PE/Auto
        
</script>
<%
                    }
                    if(isEdit || isView ){
                        
                        tableCount = tenderBidSrBean.getNoOfTable(formId);
%>
<script>
		verified = false;
                var gblCnt =0;
                chkEdit = true;
                var isMultiTable = false;
                var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
                var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
                var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
                var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
                var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
                var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
                var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
                var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
                var brokenFormulaIds = new Array();
                var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
                var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
                var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
                var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
                var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
                var arrDataTypesforCell = new Array(<%=tableCount%>);
                var arrColTotalIds = new Array(<%=tableCount%>);
                var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
                var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
                var isColTotalforTable = new Array(<%=tableCount%>);
                var totalBidTable = new Array(<%=tableCount%>);
                for(var i=0;i<isColTotalforTable.length;i++)
                        isColTotalforTable[i]=0;
                var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
                var arrColFillBy=new Array(<%=tableCount%>); // Stores the Array of Column having fill by Tenderer/PE/Auto

</script>              
<%
                    }
%>
                <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="NegTenderGetBidData.jsp">
                    <%if(isEdit || isView ){%>
                    <input type="hidden" name="hdnBidId" id="hdnBidId" value="<%=bidId%>">
                    <%}%>
                    <input type="hidden" name="hidnegId" id="hidnegId" value="<%=negId%>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
<%
        String formName = "";
        String formHeader = "";
        String formFooter = "";
        String isMultipleFormFeeling = "";
        String isEncryption = "";
        String isPriceBid = "";
        String isMandatory = "";

        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(formId);
        for(CommonFormData formData : formDetails){
            formName = formData.getFormName();
            formHeader = formData.getFormHeader();
            formFooter = formData.getFormFooter();
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            isEncryption = formData.getIsEncryption();
            isPriceBid = formData.getIsPriceBid();
            isMandatory = formData.getIsMandatory();
        }
%>
                            <div class="t_space">
                                <table width="100%"   id="divFormName">
                                    <tr>
                                    <td width="80%" class="pageHead_1">
                                         <%=formName%>
                                   </td>
                                    <td width="20%" align="right" class="pageHead_1">
                                            <a class="action-button-goback" href="NegTendererForm.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>&uId=<%=uId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                                    </td>
                                        </tr>
                                </table>
                                <BR>
                                <table width='100%' cellspacing='0' class='tableList_1'>
                                <tr><td><div class="responseMsg noticeMsg">Column(s) marked with pink color are editable.</div></td></tr>
                                <% if(formBoqId == 20)
                                    {
                                %>
                                <tr><td>
                                    <div class='responseMsg noticeMsg t_space'>
                                    Instructions for replacement of CV:
                                     <ol type="1"  class="t_space b_space" style="margin-left: 20px; line-height: 15px;" >
                                         <li>If any Staff to be replaced or removed or information is to be updated, CV of the new staff should be sent to evaluation committee along with your response to negotiation questions. </li>
                                         <li>You must upload a new CV document for the approval from Committee.</li>
                                         <li>Replacing staff must be equivalent or higher in qualifying criteria than the originally proposed staff.</li>
                                     </ol>
                                    </div>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                </table>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td>
                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>"/>
                                            <input type="hidden" name="hdnFormId" id="hdnFormId" value="<%=formId%>"/>
                                            <input type="hidden" name="hdnLotId" id="hdnLotId" value="<%=lotId%>"/>
                                            <input type="hidden" name="hdnNegId" id="hdnNegId" value="<%=negId%>"/>
                                            <input type="hidden" name="negBidformId" id="negBidformId" value="<%=negBidformId%>"/>
<%
        int tableId = 0;
        int tblCnt1 = 0;
        short cols = 0;
        short rows = 0;
        String tableName = "";
        String tableHeader = "";
        String tableFooter = "";
        
        String isMultiTable = "";

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();
            tableHeader = formData.getTableHeader();
            tableFooter = formData.getTableFooter();
            List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
            if (tableInfo != null) {
                if (tableInfo.size() >= 0) {
                    tableName = tableInfo.get(0).getTableName();
                    cols = tableInfo.get(0).getNoOfCols();
                    rows = tableInfo.get(0).getNoOfRows();
                    isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                }
                tableInfo = null;
            }
            
            cols = (tenderBidSrBean.getNoOfColsInTable(tableId)).shortValue();
            rows = (tenderBidSrBean.getNoOfRowsInTable(tableId, (short) 1)).shortValue();
            
            
%>
<script>
        chkBidTableId.push(<%=tableId%>);
        arrBidCount.push(1);
        // Setting TableId in Array
        arr[<%=tblCnt1%>]=<%=tableId%>;
        // Setting TableIdwise No of Rows in Array
        arrRow[<%=tblCnt1%>]=<%=rows%>;
        //alert('tablecnt : <%=tblCnt1%>')
        //alert('<%=rows%>');
        arrRowsKey[<%=tableId%>]=<%=rows%>;
        // Setting TableIdwise No of Cols in Array
        arrCol[<%=tblCnt1%>]=<%=cols%>;
        arrColsKey[<%=tableId%>]=<%=cols%>;
        // Setting TableIdwise No of Tables in Added
        arrTableAdded[<%=tblCnt1%>]=<%=1%>;
        arrTableAddedKey[<%=tableId%>]=<%=1%>;
        arrColTotalIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColTotalWordsIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColOriValIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        for(var i=0;i<arrColTotalIds[<%=tblCnt1%>].length;i++){
                arrColTotalIds[<%=tblCnt1%>][i] = 0;
                arrColTotalWordsIds[<%=tblCnt1%>][i] = 0;
                arrColOriValIds[<%=tblCnt1%>][i] = 0;
        }
</script>
                                                <div id="divMsg" class="responseMsg successMsg" style="display:none">&nbsp;</div>
                                                <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                                    <tr>
                                                        <td width="100" class="ff" colspan="2"><%=tableName%></td>
                                                        <td class="t-align-right">
                                            <%
                                                    if("yes".equalsIgnoreCase(isMultiTable) && !isView){
                                            %>
                                                            <script>
                                                                isMultiTable = true;
                                                            </script>
                                                            <label class="formBtn_1" style="float: right" id="lblDelTable<%=tableId%>">
                                                                <input type="button" name="btnDel<%=tableId%>" id="bttnDel<%=tableId%>" value="Delete Record" onClick="DelTable(this.form,<%=tableId%>,this)"/>
                                                            </label>
                                                        <label style="float: right" >&nbsp;&nbsp;&nbsp;
                                                         </label>
                                                            <label class="formBtn_1" style="float: right" id="lblAddTable<%=tableId%>">
                                                                <input type="button" name="btn<%=tableId%>" id="bttn<%=tableId%>" value="Add Record" onClick="AddBidTable(this.form,<%=tableId%>,this)"/>
                                                            </label>
                                                            <input type="hidden" name="delTableIds<%=tableId%>" id="delTableIds<%=tableId%>" value="" />
                                                        <%
                                                                }
                                                        %>
                                                        </td>
                                                    </tr>
                                                    <% if(!"".equals(tableHeader)){ %>
                                                    <tr>
                                                        <td width="100" class="ff" colspan="3"><%=tableHeader%></td>
                                                    </tr>
                                                    <% }  %>
                                                </table>
                                                
                                                <jsp:include page="NegTenderBidFormTable.jsp" flush="true" >
                                                    <jsp:param name="tableId" value="<%=tableId%>" />
                                                    <jsp:param name="formId" value="<%=formId %>" />
                                                    <jsp:param name="cols" value="<%=cols%>" />
                                                    <jsp:param name="rows" value="<%=rows%>" />
                                                    <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                    <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                     <jsp:param name="negBidformId" value="<%=negBidformId%>" />
                                                </jsp:include>
                                                    

                                                <% if(!"".equals(tableFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">
                                                         <tr>
                                                             <td width="100%" colspan="3" class="ff"><%=tableFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  %>
                                                   <% if(!"".equals(formFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">        
                                                         <tr>
                                                             <td width="100%" class="ff" colspan="3"><%=formFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  
                                                 
                                            %>
<script>
        var totalInWordAt = -1;
        try{
            totalInWordAt = TotalWordColId
            if(totalInWordAt < 0){
                totalInWordAt = -1;
            }
        }catch(err){
            totalInWordAt = -1;
        }
</script>
<%if(tblCnt1 >= 0){%>
<script>
        if(arrStaticColIds[<%=tblCnt1%>].length > 0)
	{
            for(var j=0;j<arr.length;j++)
            {
                LoopCounter++;
            }
	}
        breakFormulas(arrDataTypesforCell);
        checkForFunctions();
</script>
<%}%>
<%
            tblCnt1++;
            formData = null;
        }
        if(isEdit)
        {
            %>

         <table width="100%" cellspacing="0" cellpadding="0" class="formStyle_1 t_space">
            <tr>
                <td align="center">
                    <div  class="btnContainer t_space">
                        <label class="formBtn_1" id="lblSave">
                            <input type="submit" name="save" id="save" value="Update" onclick="return validate();">
                        </label>
                    </div>
                </td>
            </tr>
         </table>
    <%
       }
       else if(!(isEdit || isView))
        {
             %>

         <table width="100%" cellspacing="0" cellpadding="0" class="formStyle_1 t_space">
            <tr>
                <td align="center">
                    <div  class="btnContainer t_space">
                        <label class="formBtn_1" id="lblSave">
                            <input type="submit" name="save" id="save" value="Save" onclick="return validate();">
                        </label>
                    </div>
                </td>
            </tr>
         </table>
    <%
}
%>
<script>
    breakFormulas(arrDataTypesforCell);
    checkForFunctions();
</script>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                </div>
                <div>
                    <input type="hidden" name="hdnPwd" id="hdnPwd"/>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
