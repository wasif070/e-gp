<%-- 
    Document   : ViewSsHistoryDetails
    Created on : Dec 19, 2011, 6:05:43 PM
    Author     : shreyansh.shah
--%>


<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Staffing Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int histCnt = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("histCnt") != null) {
                        histCnt = Integer.parseInt(request.getParameter("histCnt"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%       out.print(srbd.getString("CMS.service.staffSchedule.title"));
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr>
                                        <th width="10%"><%=srbd.getString("CMS.service.workSchedule.srNo")%></th>
                                        <th width="40%"><%=srbd.getString("CMS.service.workSchedule.empName")%></th>
                                        <th width="10%"><%=srbd.getString("CMS.service.workSchedule.workFrom")%></th>
                                        <th width="10%"><%=srbd.getString("CMS.sdate")%></th>
                                        <th width="10%"><%=srbd.getString("CMS.service.workSchedule.noOfDays")%></th>
                                        <th width="10%"><%=srbd.getString("CMS.edate")%></th>
                                        <th width="10%">Creation Date</th>
                                    </tr>
                                    <%

                                         List<Object[]> listt = cmss.getStaffScheduleHistory(formMapId,histCnt);
                                         try {

                                             if (listt != null && !listt.isEmpty()) {
                                                 int i = 0;
                                                 for (i = 0; i < listt.size(); i++) {
                                                     if (i % 2 == 0) {
                                                         styleClass = "bgColor-white";
                                                     } else {
                                                         styleClass = "bgColor-Green";
                                                     }
                                                     out.print("<tr class='" + styleClass + "'>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[8] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[9] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[1] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[2]).split(" ")[0] + "</td>");
                                                     out.print("<td  class=\"t-align-left\" style=\"text-align :right;\">" + listt.get(i)[3] + "</td>");
                                                     out.print("<td >" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[4]).split(" ")[0] + "</td>");
                                                     if(listt.get(i)[10]!=null){
                                                        out.print("<td >"+DateUtils.gridDateToStr((Date)listt.get(i)[10])+"</td>");
                                                     }else{out.print("<td>"+"-"+"</td>");}
                                                     out.print("<input type=hidden name=srvwplanid_" + i + " id=srvwplanid_" + i + " value=" + listt.get(i)[7] + " />");
                                                     out.print("</tr>");
                                                 }
                                                 out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                                             } else {
                                                 out.print("<tr>");
                                                 out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                                 out.print("</tr>");
                                             }

                                         } catch (Exception e) {
                                             e.printStackTrace();
                                         }

                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Staffing Schedule history details", "");
                                         }

                    %>

                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>



                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>

