<%--
    Document   : NegTendOfflineProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>
<%@page import="com.cptu.egp.eps.model.table.TblNegReply"%>
<%@page import="com.cptu.egp.eps.model.table.TblNegQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation Questions</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <div class="dashboard_div">
 
      <div class="contentArea">
  <!--Dashboard Header Start-->
   
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%
        int ttenderId =0;
        int negId = 0;
        boolean flagNag = false;
        String action = "";
        String msg = "";
        String type = "";
        
        if(request.getParameter("tenderId") != null){
            ttenderId= Integer.parseInt(request.getParameter("tenderId"));
        }
        if(request.getParameter("negId") != null){
            negId= Integer.parseInt(request.getParameter("negId").toString());
        }
        if(request.getParameter("type") != null){
            type= request.getParameter("type").toString();
        }
        if (request.getParameter("flag") != null) {
            if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                flagNag = true;
            }
        }
        if(request.getParameter("action")!=null){
            action = request.getParameter("action");
        }
        if(flagNag){
           if("replyQuestion".equalsIgnoreCase(action)){
                msg = "Question Replied Successfully";
           }
        }
        pageContext.setAttribute("tenderId", ttenderId);
   %>
  <div class="pageHead_1">
      Negotiation Questions
      <span class="c-alignment-right"><a href="NegoTendererProcess.jsp?tenderId=<%=ttenderId %>" class="action-button-goback" title="Tender/Proposal Document">Go back To Dashboard</a></span>
  </div>
  
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
   
  <%pageContext.setAttribute("tab", "6");%>
  <%@include file="../tenderer/TendererTabPanel.jsp" %>
 
<div class="tabPanelArea_1">

  <%  pageContext.setAttribute("tab", "4"); %>
   <%@include file="EvalInnerTendererTab.jsp" %>
  <div class="tabPanelArea_1">
            <div class="b_space">
                <% if(flagNag){ %>
                <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                <% }else{ %>
                    <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                 <% } %>
            </div>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="10%" class="t-align-left">S.No.</th>
                    <th width="70%" class="t-align-left">Questions</th>
                    <th width="20%" class="t-align-left">Action</th>
                </tr>
                <%
                    int i=1;
                    List<TblNegQuery> listQuery = negotiationdtbean.viewQueries(ttenderId,negId);
                    if(!listQuery.isEmpty()){
                       for(TblNegQuery Query:listQuery){
                           List<TblNegReply> listReply = negotiationdtbean.viewReply(Query.getNegQueryId());
                %>
                <tr>
                    <td class="t-align-left"><%=i %></td>
                    <td class="t-align-left"><%=Query.getQueryText()%></td>
                    <td class="t-align-center">
                        <a href="../resources/common/ViewQuery.jsp?tenderId=<%=ttenderId%>&negId=<%=negId%>&queryId=<%=Query.getNegQueryId()%>">View</a>
                        <% if(listReply.isEmpty()){  if(!"View".equalsIgnoreCase(type)){ %>
                            &nbsp;|&nbsp;<a href="<%=request.getContextPath()%>/tenderer/ReplyNegQueries.jsp?tenderId=<%=ttenderId%>&negId=<%=negId%>&negQueryId=<%=Query.getNegQueryId()%>">Reply</a>
                        <% } } %>
                    </td>
                </tr>
                <% i++; } }else{ %>
                <tr>
                    <td colspan="5" class="t-align-center">No records found.</td>
                </tr>
                <% } %>
            </table>
        </div>
   <div>&nbsp;</div>
   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center"
         <%@include file="../resources/common/Bottom.jsp" %>
   </div>
   <!--Dashboard Footer End-->
</div>
  </div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>

