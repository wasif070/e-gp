<%-- 
    Document   : SrvViewInvoice
    Created on : Dec 14, 2011, 10:35:02 AM
    Author     : shreyansh Jogi
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceMaster"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvRemarks"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>

        <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
    </head>
    <div class="dashboard_div">
        <%  CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
            String procCase = commService.getProcNature(request.getParameter("tenderId")).toString();
            int strVatPlusAit = 0;
            int strAdvVatPlusAdvAit = 0;
            int netVatandAitStr = 0;
            String wpId = "";
            if(request.getParameter("wpId")!=null)
            {
                wpId = request.getParameter("wpId");
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }
            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            accPaymentService.setLogUserId(session.getAttribute("userId").toString());
            List<TblCmsInvoiceAccDetails> invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(request.getParameter("invoiceId")));
            boolean isEdit = false;
            boolean HideActionScenario = false;
            boolean createdtenflag = false;
            if(!invoicedetails.isEmpty())
            {
                isEdit = true;
            }
            List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(request.getParameter("invoiceId")));
            if(!invstatusObject.isEmpty())
            {
                if("sendtope".equalsIgnoreCase(invstatusObject.get(0).toString()) || "sendtotenderer".equalsIgnoreCase(invstatusObject.get(0).toString()))
                {
                    HideActionScenario = true;
                }
                if("createdbyten".equalsIgnoreCase(invstatusObject.get(0).toString()))
                {
                    createdtenflag = true;
                }
            }
        %>
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div  id="print_area">
        <div class="contentArea_1">
            <div class="pageHead_1"><%=bdl.getString("CMS.Inv.View.Tenderer.Title")%>
                <span style="float: right; text-align: right;" class="noprint">
                <%if(HideActionScenario){%>
                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                <%}%>
                <%
                if("services".equalsIgnoreCase(procCase)){%>
                <a class="action-button-goback" href="InvoiceServiceCase.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                <%}else{%>
                <a class="action-button-goback" href="SrvInvoiceServiceCase.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                <%}%>
            </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");

            %>
           <%@include file="TendererTabPanel.jsp"%>
            <%
                                    ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                                    cs.setLogUserId(session.getAttribute("userId").toString());
                                    String tenderId = request.getParameter("tenderId");
                                    boolean isInvGeneratedOrNot = cs.checkInvGeneratedOrNot(request.getParameter("invoiceId"));


                        %>
            <div class="tabPanelArea_1">

                <%
                             pageContext.setAttribute("TSCtab", "3");
                             pageContext.setAttribute("invoiceId", request.getParameter("invoiceId"));
                             pageContext.setAttribute("lotId", request.getParameter("lotId"));
                             pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                             String conType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));;

                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1 t_space">
                <div class="t_space">
                <%@include file="../resources/common/ContractInfoBar.jsp"%>
                </div>
                    <%if(!isInvGeneratedOrNot){%>
                            <div>&nbsp;</div><div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.Inv.Generated")%></div>
                                <%}%>
                     <% if(request.getParameter("msg") != null){
                     if("gnratinv".equalsIgnoreCase(request.getParameter("msg"))){

    %>
                            <div>&nbsp;</div><div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.Generated.Msg")%></div>
                   <%}
                     if ("crinv".equalsIgnoreCase(request.getParameter("msg")))
                     {
    %>
                            <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.Generated.Msg")%></div>
    <%
                     }
                     if ("InvGenerated".equalsIgnoreCase(request.getParameter("msg"))) {
    %>
                         <div class='responseMsg successMsg'>Selected MileStone Invoice Generated SuccessFully</div>
    <%
                     }
                     }

 %>
                    <div align="center">


                        <form name="frmcons" action="<%=request.getContextPath()%>/InvoiceGenerationServlet"  method="post">
                            <%
                              List<TblCmsInvoiceMaster> InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(request.getParameter("invoiceId")));
                              if(!"services".equalsIgnoreCase(procnature)){
                               if(!InvMasdata.isEmpty())
                               {
                                   if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                   {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td class="ff" width="88%">Advance Amount</td>
                                    <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                </tr>
                            </table>
                            <%}else{%>
                            <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                            <%}}}else{
                               if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                               {
                            %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="ff" width="88%">Advance Amount</td>
                                        <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                    </tr>
                                </table>
                             <%}else{
                                   if("time based".equalsIgnoreCase(contType)){
                                   %>
                              <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                              <%}else{%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>
                                <th>Milestone Name </th>
                                <th>Description</th>
                                <th>Payment as % of Contract Value</th>
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>
                                <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                <%
                                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                    List<TblCmsSrvPaymentSch> listPaymentData = cmss.getPaymentScheduleDatabySrvPSId(InvMasdata.get(0).getTillPrid());
                                    if(!listPaymentData.isEmpty()){
                                %>
                                <tr>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getSrNo()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getMilestone()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getDescription()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getPercentOfCtrVal()%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getEndDate())%></td>
                                    <td><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*listPaymentData.get(0).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                </tr>
                            </table>
                            <%}}}
                               if(!InvMasdata.isEmpty())
                               {
                                   if(InvMasdata.get(0).getConRemarks()!=null)
                                   {

                            %>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr><td width="20%" class="ff">Remarks by Consultant</td>
                                    <td>
                                        <%
                                            out.print(InvMasdata.get(0).getConRemarks());
                                        %>
                                    </td>
                                </tr>
                            </table>
                            <%}}}%>
                             <%if("rejected".equalsIgnoreCase(invstatusObject.get(0).toString()) || "acceptedbype".equalsIgnoreCase(invstatusObject.get(0).toString())){
                                       List<Object> inv = accPaymentService.getInvoiceRemarks(Integer.parseInt(request.getParameter("invoiceId")));
    %>                                  <br />
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr><td class="t_align_left ff" width="15%">
                                              Remarks By PE
                                            </td>
                                            <td><%=inv.get(0)%></td>
                                        </tr>
                                    </table>
                                    <%}%>
                            <%if(HideActionScenario){
                        %>
                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff" width="25%" class="t-align-left"><%=bdl.getString("CMS.Inv.invamount")%></td>
                                <td   class="t-align-left"> <%if(isEdit){%><%=invoicedetails.get(0).getInvoiceAmt()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.deduction")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advAmount")%></td>
                                <td   class="t-align-left">
                                <%
                                    double strAdvanceAmount = 0;
                                    String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(request.getParameter("lotId")));
                                    if(!"".equalsIgnoreCase(AdvanceAmount))
                                    {   strAdvanceAmount = new BigDecimal(AdvanceAmount.toString()).doubleValue();
                                    }else{strAdvanceAmount = new BigDecimal(0).doubleValue();}
                                %>
                                    <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvanceAmount).setScale(3,0)%></td>
                                            <td style="text-align: right;border:none;padding:0px;"><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAmount")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvAdjAmt()%><%}%></td>
                                            <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VAT")%>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getVatAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getVatAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjVAT")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvVatAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvVatAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.AIT")%>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAIT")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                 <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VATAIT")%></td>
                                <%
                                    if(isEdit){
                                        if(invoicedetails.get(0).getVatAmt()!=null && invoicedetails.get(0).getAitAmt()!=null)
                                        {
                                            strVatPlusAit = (invoicedetails.get(0).getVatAmt().intValue()+invoicedetails.get(0).getAitAmt().intValue());
                                        }
                                        else if(invoicedetails.get(0).getVatAmt()!=null && invoicedetails.get(0).getAitAmt()==null)
                                        {
                                            strVatPlusAit = invoicedetails.get(0).getVatAmt().intValue();
                                        }
                                        else if(invoicedetails.get(0).getVatAmt()==null && invoicedetails.get(0).getAitAmt()!=null)
                                        {
                                            strVatPlusAit = invoicedetails.get(0).getAitAmt().intValue();
                                        }
                                    }
                                %>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strVatPlusAit).setScale(3)%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*strVatPlusAit)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.advVATAIT")%>
                                </td>
                                <%
                                    if(isEdit){
                                        if(invoicedetails.get(0).getAdvVatAmt()!=null && invoicedetails.get(0).getAdvAitAmt()!=null)
                                        {
                                            strAdvVatPlusAdvAit = (invoicedetails.get(0).getAdvVatAmt().intValue()+invoicedetails.get(0).getAdvAitAmt().intValue());
                                        }
                                        else if(invoicedetails.get(0).getAdvVatAmt()!=null && invoicedetails.get(0).getAdvAitAmt()==null)
                                        {
                                            strAdvVatPlusAdvAit = invoicedetails.get(0).getAdvVatAmt().intValue();
                                        }
                                        else if(invoicedetails.get(0).getAdvVatAmt()==null && invoicedetails.get(0).getAitAmt()!=null)
                                        {
                                            strAdvVatPlusAdvAit = invoicedetails.get(0).getAdvAitAmt().intValue();
                                        }
                                    }
                                %>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvVatPlusAdvAit).setScale(3)%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*strAdvVatPlusAdvAit)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><%=bdl.getString("CMS.Inv.netVATAIT")%></td>
                                <%
                                    if(isEdit){
                                      netVatandAitStr = strVatPlusAit - strAdvVatPlusAdvAit;
                                    }
                                %>
                                <td class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(netVatandAitStr).setScale(3)%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*netVatandAitStr)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff"></td>
                                <td class="ff"></td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.liquditydamage")%></td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getldAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.penalty")%>
                                </td>
                                <td   class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getPenaltyAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.addition")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left">Bonus :</td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getBonusAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getBonusAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    </tr>
                                    </table>
                                </td>
                                <td class="ff" width="26%"  class="t-align-left"><%=bdl.getString("CMS.Inv.InterestOnDP")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%=invoicedetails.get(0).getInterestonDP()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}}%></td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.retention")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoney")%></td>
                                <td   class="t-align-left">
                                        <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){%><%=invoicedetails.get(0).getRetentionMadd()%>&nbsp;<span style="color: #f00;">(Added)</span><%}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){%><%=invoicedetails.get(0).getRetentionMdeduct()%>&nbsp;<span style="color: #f00;">(Deducted)</span><%}}%></td>
                                            <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getRetentionMadd().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT"));}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getRetentionMdeduct().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT"));}}%></td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr> <td colspan="2"><ol style="padding-right: 5px;margin-left: 20px;"><li>Retention money at a percentage as specified in Schedule II will be
                                         deductable from each bill due to a Contractor until completion of the whole Works or delivery.</li>
                                     <li>On completion of the whole Works, half the total amount retained shall be repaid to
                                         the Contractor and the remaining amount may also be paid to the Contractor if an unconditional
                                         Financial Institute guarantee is furnished for that remaining amount.</li>
                                     <li >The remaining amount or the Financial Institute guarantee, under Sub-Rule (2), shall be returned,
                                         within the period specified in schedule II , after issuance of all Defects Correction Certificate
                                         under Rule 39(29) by the Project Manager or any other appropriate Authority ..</li>
                                     <li >Deduction of retention money shall not be applied to small Works Contracts if no
                                         advance payment has been made to the Contractor and in such case the provisions of Sub-Rule
                                         (7) and (8) of Rule 27 shall be applied.</li></ol>
                                 </td><td></td>
                            </tr>
                        </table>
                                <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getModeOfPayment()%><%}%>
                                <span id="modeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getDateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getBankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getBranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getInstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%
                            double NetVat = 0;
                            if(invoicedetails.get(0).getVatAmt()!=null)
                            {
                                if(invoicedetails.get(0).getAdvVatAmt()!=null)
                                {
                                    NetVat = (((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getVatAmt().intValue())/100) - ((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvVatAmt().intValue())/100)) ;
                                }
                            }
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.vat")%>&nbsp;&nbsp;<%=new BigDecimal(NetVat).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%></div>
                        <%if(invoicedetails.get(0).getVatmodeOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatmodeOfPayment()%><%}%>
                                <span id="vatmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getVatdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <%
                            double NetAit = 0;
                            if(invoicedetails.get(0).getVatAmt()!=null)
                            {
                                if(invoicedetails.get(0).getAdvVatAmt()!=null)
                                {
                                    NetAit = (((invoicedetails.get(0).getInvoiceAmt().intValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100) - ((invoicedetails.get(0).getInvoiceAmt().intValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100)) ;
                                }
                            }
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.ait")%>&nbsp;&nbsp;<%=new BigDecimal(NetAit).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%></div>
                        <%if(invoicedetails.get(0).getAitmodeOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitmodeOfPayment()%><%}%>
                                <span id="aitmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getAitdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.gross")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                        <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.grossamt")%></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getGrossAmt()%><%}%>
                                <input type="hidden" id="grosshiddenamt" <%if(isEdit) {%>value ="<%=invoicedetails.get(0).getGrossAmt()%>"<%}%>>
                                &nbsp;<span id="grossinWords" class="ff"></span>
                            </td>
                        </tr>
                        </table>

                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.Remarks.ViewCaps")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                        <%
                             List<TblCmsInvRemarks> getRemarks = accPaymentService.getRemarksDetails(Integer.parseInt(request.getParameter("invoiceId")));
                             if(!getRemarks.isEmpty())
                             {
                                 for(int i=0; i<getRemarks.size(); i++)
                                 {
                        %>
                                <tr>
                                <td class="t-align-center" width="15%">
                                    <%=(i+1)%>
                                </td>
                                <td>
                                    <%=getRemarks.get(i).getRemarks()%>
                                </td>
                                </tr>
                           <%}}%>
                        </table>
                        <%}%>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td class="t-align-left ff" width="15%">
                                    <%if(createdtenflag){%>
                                    <%=bdl.getString("CMS.Inv.Upload")%>
                                    <%}else{%>
                                    <%=bdl.getString("CMS.Inv.Download")%>
                                    <%}%>
                                </td>
                                <td>
                                    <%if(createdtenflag){%>
                                    <a href="TendererPaymentDocUpload.jsp?tenderId=<%=request.getParameter("tenderId")%>&invoiceId=<%=request.getParameter("invoiceId")%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Upload</a>
                                    <%}%>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                        <th width="8%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                        <th class="t-align-center" width="23%"><%=bdl.getString("CMS.Inv.FileName")%></th>
                                        <th class="t-align-center" width="28%"><%=bdl.getString("CMS.Inv.FileDescription")%></th>
                                        <th class="t-align-center" width="9%"><%=bdl.getString("CMS.Inv.FileSize")%><br />
                                            <%=bdl.getString("CMS.Inv.inKB")%></th>
                                        <th class="t-align-center" width="">Uploaded By</th>
                                        <th class="t-align-center" width="18%"><%=bdl.getString("CMS.action")%></th>
                                        </tr>
                                        <%
                                            String userId = "1";
                                            if (session.getAttribute("userId") != null) {
                                                userId = session.getAttribute("userId").toString();//Integer.parseInt(InvoiceId)
                                            }
                                            List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetails(Integer.parseInt(request.getParameter("invoiceId")));
                                            if(!getInvoiceDocData.isEmpty())
                                            {
                                                for(int i =0; i<getInvoiceDocData.size(); i++)
                                                {
                                        %>
                                            <tr>
                                                <td class="t-align-center"><%=(i+1)%></td>
                                                <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocumentName()%></td>
                                                <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocDescription()%></td>
                                                <td class="t-align-center"><%=(Long.parseLong(getInvoiceDocData.get(i).getDocSize())/1024)%></td>
                                                <td class="t-align-center">
                                                    <%String str = "";
                                                    if (2 == getInvoiceDocData.get(i).getUserTypeId()) {
                                                        str = "supplier";
                                                        out.print("Supplier");
                                                    } else {
                                                        if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(Integer.toString(getInvoiceDocData.get(i).getUploadedBy()))))
                                                        {
                                                            str = "accountant";
                                                            out.print("Account Officer");
                                                        }else{
                                                            str = "PE";
                                                            out.print("PE Officer");
                                                        }

                                                    }%>
                                                </td>
                                                <td class="t-align-center">
                                                    <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&accountant=<%if(3==getInvoiceDocData.get(i).getUserTypeId()){%>yes<%}%>&str=<%=str%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                    &nbsp;
                                                    <%--<%if(isInvGeneratedOrNot){%>
                                                    <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?&docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&str=<%=str%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                    <%}%>--%>
                                                </td>
                                            </tr>
                                             <%}
                                            }else{%>
                                            <tr>
                                                <td colspan="6" class="t-align-center"><%=bdl.getString("CMS.Inv.NoRecord")%></td>
                                            </tr>
                                            <%}%>
                                    </table>
                                </td>
                            </tr>
                        </table>

                                <br /><input type="hidden" name="action" id="action" value="generate"  />
                                <input type="hidden" name="lotId" id="action" value="<%=request.getParameter("lotId")%>"  />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>"  />
                                <input type="hidden" name="invId" id="invId" value="<%=request.getParameter("invoiceId")%>"  />
                                  <%if(isInvGeneratedOrNot){%>
                        <label class="formBtn_1">

                                <input type="submit" name="Boqbutton" id="Boqbutton" value="Generate Invoice" />
                                 </label>
                                <%}%>


                        </form>

                    </div></div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        //document.getElementById("grossinWords").innerHTML = DoIt(document.getElementById("grosshiddenamt").value);
        //Numeric to word currency conversion by Emtaz on 20/April/2016
        document.getElementById("grossinWords").innerHTML = CurrencyConverter(document.getElementById("grosshiddenamt").value);
    </script>

</html>

