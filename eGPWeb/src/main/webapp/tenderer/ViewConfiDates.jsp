<%-- 
    Document   : ViewConfiDates
    Created on : Nov 22, 2011, 6:11:03 PM
    Author     : dixit
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfigHistory"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>

    </head>
    <body>
            <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String lotId = "";
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = request.getParameter("lotId");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        issueNOASrBean.setLogUserId(userId);
                    }
                    pageContext.setAttribute("tab", "10");

            %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div  id="print_area">
            <div class="contentArea_1">
                <div class="pageHead_1">View Configure Dates History
                    <span style="float: right; text-align: right;" class="noprint">
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <a class="action-button-goback" href="CMSMain.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    </span>
                </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp"%>
                <div>&nbsp;</div>

                <%@include file="TendererTabPanel.jsp"%>
                    <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "5");
                        CommonService commonSer = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnaturee = commonSer.getProcNature(request.getParameter("tenderId")).toString();
                        String strLotNo = "Lot No.";
                        String strLotDes = "Lot Description";
                        if ("goods".equalsIgnoreCase(procnaturee)) {
                        } else if ("works".equalsIgnoreCase(procnaturee)) {
                        }else if("services".equalsIgnoreCase(procnaturee))
                        {
                            strLotNo = "Package No.";
                            strLotDes = "Package Description";
                        }
                    %>
                    <%@include  file="../tenderer/cmsTab.jsp"%>
                    <div class="tabPanelArea_1">
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <div>&nbsp;</div>
                    <div class="tabPanelArea_1">
                    <div align="center">
                        <%              List<Object[]> list = issueNOASrBean.getNOAListing(Integer.parseInt(tenderId));
                                        Object[] obj = list.get(0);
                                        List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                        %>

                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff" width="12%" class="t-align-left"><%=strLotNo%></td>
                                <td   class="t-align-left"><%=obj[6]%></td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=strLotDes%></td>
                                <td  class="t-align-left"><%=obj[7]%></td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                            <tr>
                                <th width="5%" class="t-align-center">Sl. No.</th>
                                <th width="12%" class="t-align-center">Actual Contract Start Date</th>
                                <th width="12%" class="t-align-center">Actual Contract End Date</th>
                                <th width="12%" class="t-align-center">Modified Date and Time</th>
                                <th width="12%" class="t-align-center">Record</th>
                            </tr>
                            <%
                                 CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                 List<TblCmsDateConfig> dateConfigdata = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(lotId),Integer.parseInt(request.getParameter("contractSignId")));
                                 if(!dateConfigdata.isEmpty())
                                 {
                                    List<TblCmsDateConfigHistory> historydata = cmsConfigDateService.getCmsDateConfigHistorydata(dateConfigdata.get(0).getActContractDtId());
                                    if(!historydata.isEmpty())
                                    {
                                        for(int i=0; i<historydata.size(); i++)
                                        {
                            %>
                                        <tr>
                                            <td width="5%" class="t-align-center"><%=(historydata.size()-(i))%></td>
                                            <td width="12%" class="t-align-center"><%=DateUtils.customDateFormate(historydata.get(i).getActualStartDate())%></td>
                                            <td width="12%" class="t-align-center"><%=DateUtils.customDateFormate(historydata.get(i).getActualEndDate())%></td>
                                            <td width="12%" class="t-align-center"><%=DateUtils.gridDateToStr(historydata.get(i).getCreatedDate())%></td>
                                            <%if(i==(historydata.size()-1)){%>
                                            <td width="12%" class="t-align-center">First Time Insertion</td>
                                            <%}else{%>
                                            <td width="12%" class="t-align-center">Extended</td>
                                            <%}%>
                                           
                                        </tr>
                            <%
                                        }
                                    }
                                 }
                                          MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                          makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(c_obj[7].toString()), "ContractId", EgpModule.Commencement_Date.getName(), "View Configure Date/Days", "");
                            %>
                        </table>

                    </div>
                </div></div></div>
            </div></div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            issueNOASrBean = null;
%>
