<%-- 
    Document   : ViewMappedDoc
    Created on : Dec 17, 2010, 5:43:38 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Map Document</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>

        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href=".<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%-- <jsp:include page="AfterLoginTop.jsp" ></jsp:include>--%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 0;
                            int formId = 0;
                            String viewType = "";
                            

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))){
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("formId") != null && !"null".equalsIgnoreCase(request.getParameter("formId"))) {
                                formId = Integer.parseInt(request.getParameter("formId"));
                            }
                            if (request.getParameter("viewType") != null && !"null".equalsIgnoreCase(request.getParameter("viewType"))) {
                                viewType = request.getParameter("viewType");
                            }                            

                            // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            String idType="userId";
                            int auditId=userId;
                            String auditAction="Bidder has viewed mapped Documents of Form";
                            String moduleName=EgpModule.Bid_Submission.getName();
                            String remarks="Bidder(User Id): "+auditId+" has viewed mapped documents of Form Id: "+formId;
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Map Document</div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>
                       
<!--                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td>
                                    <div id="docData">
                                    </div>
                                </td>
                            </tr>
                        </table>-->
                        <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1 t_space">
                               <tr>
                                   <th colspan="8">Mapped Files</th>
                               </tr>
                                <tr>
                                    <th class="ff">Sl. No.</th>
                                    <th class="ff">File Name</th>
                                    <th class="ff">Document Type</th>
                                    <th class="ff">File Description</th>
                                    <th class="ff">File Size (In KB)</th>
<!--                                    <th class="ff">Date</th>-->
                                    <th class="ff">Action</th>
                                </tr>
                                <%
                                    CommonSearchService tenderCommonService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    int g_cnt=1;
                                    for(SPCommonSearchData bidderDocDetail : tenderCommonService.searchData("GetBidderDocuments",""+userId, ""+formId, null, null, null, null, null, null, null)){
                                %>
                                <tr>
                                    <td class="t-align-center"><%=g_cnt%></td>
                                    <td><%=bidderDocDetail.getFieldName2()%></td>
                                    <td><%=bidderDocDetail.getFieldName12()%></td>
                                    <td><%=bidderDocDetail.getFieldName4()%></td>
                                    <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((bidderDocDetail.getFieldName3()))/1024));%></td>
<!--                                    <td class="t-align-center"><%=bidderDocDetail.getFieldName5()%></td>-->
                                    <td class="t-align-center">
                                        <a onclick="downloadFile('<%=bidderDocDetail.getFieldName2()%>','<%=bidderDocDetail.getFieldName3()%>')" href="javascript:void();">Download</a>
                                    </td>
                                </tr>
                                <%
                                    g_cnt++;
                                    }
                                    if(g_cnt <= 1)
                                    {
                                 %>
                                 <tr>
                                    <td colspan="6" class="t-align-center">No records found.</td>
                                </tr>
                                 <%
                                    }
                                %>

                                <input type="hidden" id="g_cnt" value="<%=g_cnt%>"/>
                        </table>
                        <div>&nbsp;</div>
                        <!--Dashboard Content Part End-->
                        <!--Dashboard Footer Start-->
                        <%-- <%@include file="Bottom.jsp" %>--%>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
                        <%@include file="../resources/common/Bottom.jsp"%>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

<form id="form2" method="post">
</form>
<script>
    /*function getDocData(){
            $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {userId:<%=userId%>,formId:<%=formId%>,action:'docData',docAction:'GetBidderDocuments'}, function(j){
                document.getElementById("docData").innerHTML = j;
            });
    }*/

  function downloadFile(fileName,fileLen){
      document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName="+fileName+"&fileLen="+fileLen+"&fromViewMapDoc=true&tenderId=<%=tenderId%>&formId=<%=formId%>";
      document.getElementById("form2").submit();
  }
  
  getDocData();
</script>
