<%-- 
    Document   : ViewTenderSubContractor
    Created on : June 9, 2011, 12:08:08 AM
    Author     : TaherT
--%>
<%@page import="javax.print.DocFlavor.STRING"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>        
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                    String tenderId = request.getParameter("tenderId");
                    String invFromUserId = request.getParameter("uId");
                    ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                    List<Object[]> subList  = creationService.getCompanyName(invFromUserId, tenderId);                    
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">Sub Contractor details</div>
                <table  cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                    <tr>
                        <th>Sl. No</th>
                        <th>Company Name</th>
                        <th>Status</th>
                    </tr>
                    <%
                         int cnt_contract=1;
                         String tId=request.getParameter("tId");
                         String cId=request.getParameter("cId");
                         if(tId!=null && cId!=null){
                            CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                            out.print("<tr>");
                            out.print("<td width='15%' class='t-align-center'>"+cnt_contract+"</td>");
                            out.print("<td width='50%'><a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+invFromUserId+"&tId="+tId+"&cId="+cId+"&top=no' target='_blank'>"+commonService.getConsultantName(invFromUserId)+"</a></td>");
                            out.print("<td width='35%' class='t-align-center'>Lead</td>");
                            out.print("</tr>");
                             cnt_contract++;
                         }
                    %>
                    <%
                        
                        for(Object[] users : subList){
                    %>                    
                    <tr>
                        <td width="15%" class='t-align-center'><%=cnt_contract%></td>
                        <td width="50%">
                            <%
                                //0. ttm.userId,1. ttm.tendererId,2. tcm.companyId,3. tcm.companyName,4. ttm.firstName,5. ttm.lastName
                                StringBuilder cmpName = new StringBuilder();
                                if(users[2].toString().equals("1")){
                                    cmpName.append(users[4].toString()+" "+users[5].toString());
                                }else{
                                    cmpName.append(users[3].toString());
                                }
                                out.print("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+users[0]+"&tId="+users[1]+"&cId="+users[2]+"&top=no' target='_blank'>"+cmpName+"</a>");
                            %>

                        </td>
                        <td width="35%" class='t-align-center'>
                            Sub Contractor
                        </td>
                    </tr>
                    <%cnt_contract++;}%>
                </table>                    
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>    
</html>
