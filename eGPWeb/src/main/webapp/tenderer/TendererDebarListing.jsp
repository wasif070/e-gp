<%-- 
    Document   : TendererDebarListing
    Created on : Feb 15, 2011, 7:08:57 PM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Process Debarment Clarification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script>
                function fillGridOnEvent(type){
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:'<%=request.getContextPath()%>/InitDebarment?p=5&q=1&action=fetchData&status='+type,
                        datatype: "xml",
                        height: 250,
                        colNames:['Sl. No.','Company Name','Procuring Entity','Date and Time of Request',"Status","Action"],
                        colModel:[
                           {name:'srNo',index:'srNo', width:50,sortable:false,align:'center'},
                            {name:'procureEntity',index:'procureEntity', width:320,sortable:false},
                            {name:'dateRequest',index:'tdr.clarificationReqDt', width:160,align:'center'},
                            {name:'companyName',index:'(CASE WHEN tcm.companyId = 1 THEN concat(tm.firstName,\' \',tm.lastName)  ELSE tcm.companyName END)', width:200},
                            {name:'status',index:'status', width:170,sortable:false,align:'center'},
                            {name:'userid',index:'userid', width:50,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});
                }
                jQuery().ready(function (){
                    //fillGrid();
                });
        </script>
       <script>
                function test(id){
                    if(id==1){
                        document.getElementById("linkPending").className = "sMenu";
                        document.getElementById("linkProccessed").className = "";
                        fillGridOnEvent('not in (\'withpe\') and tdr.userId=<%=session.getAttribute("userId")%> and tdr.debarmentId not in (select tdre.tblDebarmentReq.debarmentId from TblDebarmentResp tdre where tdre.tblDebarmentReq.debarmentId=tdr.debarmentId and tdre.userId=<%=session.getAttribute("userId")%>)');
                    }else if(id==2){
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkProccessed").className = "sMenu";
                        fillGridOnEvent('not in (\'withpe\') and tdr.userId=<%=session.getAttribute("userId")%> and tdr.debarmentId in (select tdre.tblDebarmentReq.debarmentId from TblDebarmentResp tdre where tdre.tblDebarmentReq.debarmentId=tdr.debarmentId and tdre.userId=<%=session.getAttribute("userId")%>)');
                    }
                }
      </script>
    </head>
    <body onload="test(<%if("y".equals(request.getParameter("isprocess"))){out.print("2");}else{out.print("1");}%>);">
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="pageHead_1">Process Debarment Clarification
            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a></span>
            </div><br/>
            <%if("success".equals(request.getParameter("flag"))){out.print("<div class=\"responseMsg successMsg\">Response posted successfully</div>");}%>
            <ul class="tabPanel_1 t_space">
                <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);">Pending</a></li>
                <li><a href="javascript:void(0);" id="linkProccessed" onclick="test(2);">Processed</a></li>
            </ul>
            <div class="tabPanelArea_1">
                    <div id="jQGrid" align="center">
                        <table id="list"></table>
                        <div id="page"></div>
                    </div>
                </div>
             <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                            String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="Debarment_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Debarment" />
            </form>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabDebar");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
