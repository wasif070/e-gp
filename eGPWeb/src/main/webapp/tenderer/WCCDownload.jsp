<%-- 
    Document   : WCCDownload
    Created on : Aug 29, 2011, 6:14:58 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertDoc"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertDocServiceBean"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Download Documents </title>
        <%
                    int wcCertId = 0;
                    if (request.getParameter("wcCertId") != null) {
                        wcCertId = Integer.parseInt(request.getParameter("wcCertId"));
                    }
                    int tenderId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    }
                    int contractUserId = 0;
                    if (request.getParameter("contractUserId") != null) {
                        contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                    }
                    int contractSignId = 0;
                    if (request.getParameter("contractSignId") != null) {
                        contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                    }
                    String functionName = "";
                    String message = "";
                    if (request.getParameter("functionName") != null) {
                        functionName = request.getParameter("functionName");
                    }
                    boolean check = false;
                    if (functionName.length() > 0) {
                        if (request.getParameter("check") != null) {
                            check = Boolean.parseBoolean(request.getParameter("check"));
                        }
                        if ("download".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Downloaded Succesfully!";
                            } else {
                                message = "Error encountered while downloading the file. Please try again!";
                            }
                        }
                    }
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
        %>
    </head>
    
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <div class="pageHead_1"> Download a Document
                                <span style="float: right; text-align: right;">
                                    <%
                                    CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                                    String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
                                    String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                                    if("services".equalsIgnoreCase(procnature))
                                    {
                                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                                        {
                                    %>
                                            <a class="action-button-goback" href="ProgressReportMain.jsp?tenderId=<%=tenderId%>">Go back</a>
                                        <%}else{%>
                                            <a class="action-button-goback" href="SrvLumpSumPr.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}}else{%>
                                    <a class="action-button-goback" href="progressReport.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                </span>
                            </div>
                            <%
                                        if (message.length() != 0) {
                                            if (!check) {
                                                out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");

                                            } else {
                                                out.print("<div id='errorMsg' class='responseMsg errorMsg' style='display:block; margin-top: 10px;'>");
                                            }
                                            out.print(message);
                                            out.print("</div>");
                                        }
                            %>
                            <input type="hidden" name="wcCertId" value="<%=wcCertId%>" id="wcCertId">
                            <input type="hidden" name="tenderId" value="<%=tenderId%>" id="tenderId">
                            <input type="hidden" name="contractUserId" value="<%=contractUserId%>" id="contractUserId">
                            <input type="hidden" name="contractSignId" value="<%=contractSignId%>" id="contractSignId">

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                </tr>
                                <%
                                            CmsWcCertDocServiceBean cmsWcCertDocServiceBean = new CmsWcCertDocServiceBean();
                                            cmsWcCertDocServiceBean.setLogUserId(logUserId);
                                            int docCnt = 0;
                                            List<TblCmsWcCertDoc> tblCmsWcCertDocList = cmsWcCertDocServiceBean.getAllCmsWcCertDocForWccId(wcCertId);
                                            if (tblCmsWcCertDocList != null && !tblCmsWcCertDocList.isEmpty()) {
                                                for (TblCmsWcCertDoc tblCmsWcCertDoc : tblCmsWcCertDocList) {
                                                    docCnt++;
                                                    out.print("<tr>");
                                                    out.print("<td class='t-align-center'> " + docCnt + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocumentName() + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocDescription() + "</td>");
                                                    out.print("<td class='t-align-center'> " + (Long.parseLong(tblCmsWcCertDoc.getDocSize()) / 1024) + "</td>");
                                                    out.print("<td class='t-align-center'> "
                                                            + "<a href =" + request.getContextPath() + "/WCCUploadServlet?functionName=download&wcCertDocId="
                                                            + tblCmsWcCertDoc.getWcCertDocId() + "&wcCertId=" + wcCertId + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                            + "><img src='../resources/images/Dashboard/Download.png' alt='Download' /></a>");
                                                    out.print("</td>");
                                                    out.print("</tr>");
                                                }
                                            } else {
                                                out.print("<tr>");
                                                out.print(" <td colspan='5' class='t-align-center'>No records found.</td>");
                                                out.print("</tr>");
                                            }
                                %>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>
