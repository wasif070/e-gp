<%--
    Document   : NegTenderGetBidData
    Created on : Nov 27, 2010, 4:17:03 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables"%>
<%@page import="com.cptu.egp.eps.model.table.TblNegBidTable"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="com.cptu.egp.eps.web.utility.HashUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
<jsp:useBean id="tenderTablesSrBean" class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="negotiationFormsSrBean"  class="com.cptu.egp.eps.web.servicebean.NegotiationFormsSrBean" />
<jsp:useBean id="handleSpecialChar"  class="com.cptu.egp.eps.web.utility.HandleSpecialChar" />
<%
    Logger LOGGER = Logger.getLogger("NegTenderGetBidData.jsp");
    String action = "Save";
    if(request.getParameter("save")!=null && !"".equalsIgnoreCase(request.getParameter("save"))){
        action = request.getParameter("save");
    }

    int negBidformId=0;
    if(request.getParameter("negBidformId")!=null && !"".equalsIgnoreCase(request.getParameter("negBidformId"))){
        negBidformId = Integer.parseInt(request.getParameter("negBidformId"));
    }

    int bidId = 0;
    if(request.getParameter("hdnBidId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnBidId"))){
        bidId = Integer.parseInt(request.getParameter("hdnBidId"));
    }

    int lotId = 0;
    if(request.getParameter("hdnLotId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnLotId"))){
        lotId = Integer.parseInt(request.getParameter("hdnLotId"));
    }
    int negId = 0;
    if(request.getParameter("hidnegId")!=null && !"".equalsIgnoreCase(request.getParameter("hidnegId"))){
        negId = Integer.parseInt(request.getParameter("hidnegId"));
    }

    int uId = 0;
    if(request.getParameter("hiduId")!=null && !"".equalsIgnoreCase(request.getParameter("hiduId"))){
        uId = Integer.parseInt(request.getParameter("hiduId"));
    }

    String formType = "";
    if(request.getParameter("formType")!=null){
        formType = request.getParameter("formType");
    }

    
    int tableCount = 0;
    int tableId = 0;
    int columnId = 0;

    int tableIds[] = new int[1000];
    for(int i=0;i<1000;i++){
        tableIds[i] = 0;
    }

    int tenderId = 0;
        if(request.getParameter("hdnTenderId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnTenderId"))){
            tenderId = Integer.parseInt(request.getParameter("hdnTenderId"));
        }

    // Coad added by Dipal for Audit Trail Log.
    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

    String idType="tenderId";
    int auditId=tenderId;
    String auditAction="";
    String moduleName=EgpModule.Negotiation.getName();
    String remarks="Tenderer(User id): "+session.getAttribute("userId")+" has Filled Form id:"+request.getParameter("hdnFormId")+" for Negotiation";

    
    if("Save".equalsIgnoreCase(action) || "Update".equalsIgnoreCase(action)){
        // General Declaration
        ListIterator<CommonFormData> tblBidCellsDtl = null;

        // Bid Form
        

        int tenderFormId = 0;
        if(request.getParameter("hdnFormId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnFormId"))){
            tenderFormId = Integer.parseInt(request.getParameter("hdnFormId"));
        }

        int userId = 0;
        if(session.getAttribute("userId") != null) {
            if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
        }

        String submissionDt =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date().getTime());
        
         if("Save".equalsIgnoreCase(action))
           {
             auditAction="Bidder has Filled Forms";
             remarks="Bidder(User id): "+session.getAttribute("userId")+" has Filled Form id:"+request.getParameter("hdnFormId")+" for Negotiation";
           }
        else
           {
            auditAction="Bidder has updated Forms";
            remarks="Bidder(User id): "+session.getAttribute("userId")+" has updated Form id:"+request.getParameter("hdnFormId")+" for Negotiation";
            }

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(tenderFormId);

        int cntTableId = 0;
        
      String tblXML_TenderNegoBidTableDetail="";
        String tblXML_TenderNegoBidDetail = "";
        String tblXML_TenderNegoBidForm ="";
        
        

        // Bid Form
            tblXML_TenderNegoBidForm = "<root> "
                                 + "<tbl_NegBidForm "
                                 + "negId=\""+ negId +"\" "
                                 + "bidId=\""+ bidId +"\" "
                                 + "tenderFormId=\""+ tenderFormId +"\" "
                                 + "userId=\""+ userId +"\" "
                                 + "submissionDt=\""+ submissionDt +"\" "
                                 + "/>";
            tblXML_TenderNegoBidForm += "</root>";
        
        // Bid Table
            tblXML_TenderNegoBidTableDetail = "<root>";
             
             
            for(CommonFormData formData : formTables){
                tableId = formData.getTableId();

                if(request.getParameter("tableCount"+tableId) != null){
                    tableCount = Integer.parseInt(request.getParameter("tableCount"+tableId));
                }
                for(int i=1;i<=tableCount;i++)
                {
                    
                    tblXML_TenderNegoBidTableDetail += "<tbl_NegBidTable "
                    + "negBidFormId=\"IdentKey\" "
                    + "bidId=\""+ bidId +"\" "
                    + "tenderFormId=\""+ tenderFormId +"\" "
                    + "tenderTableId=\""+ tableId +"\" "
                    //+ "submittedTimes=\""+ (submittedTimes) +"\" "
                    + "submissionDt=\""+ submissionDt +"\" "
                    + "/>";
                    tableIds[cntTableId++] = tableId;
                    
                }
                //submittedTimes++;
            }
            tblXML_TenderNegoBidTableDetail += "</root>";
            
         
            // Bid Details - cellvalue
            int rowId = 0;
            int tmpRowId = 0;
            int cellId = 0;
            int cellCnt = 0;
            int clCnt = 0;
            boolean rFlag = true;
            int bidTableCnt = 0;
            String cellValue = "";
            tableId = 0;
            boolean bidFlag = false;
            int noOfRows = 0;
            int rows = 0;
            int totalTableCnt = 0;
            List<TblTenderTables> listTenderTables = new ArrayList<TblTenderTables>();
            String isMultipleFilling = "";
            boolean isTableWithGrandTotal = false;

            tblXML_TenderNegoBidDetail = "<root>";

            int identkey = 0;
            for(int i=0;i<tableIds.length;i++) // Table loop
            {
                clCnt=0;
                if(tableIds[i]!=0)
                {
                    identkey++;
                    if(tableId != tableIds[i])
                    {
                        tableId = tableIds[i];
                        tblBidCellsDtl = tenderBidSrBean.getTableBidCells(tableId).listIterator();
                        isTableWithGrandTotal = tenderTablesSrBean.isTotalFormulaCreated(tableId);
                        listTenderTables = tenderTablesSrBean.getTenderTablesDetail(tableId);

                        if(!listTenderTables.isEmpty() && isTableWithGrandTotal){
                            rows = listTenderTables.get(0).getNoOfRows();
                            isMultipleFilling = listTenderTables.get(0).getIsMultipleFilling();
                        }
                        tmpRowId = 0;
                        bidFlag = true;
                    }
                    else
                    {
                        while(tblBidCellsDtl.hasPrevious())
                        {
                            tblBidCellsDtl.previous();
                        }
                        bidFlag = false;
                        cellCnt = tenderBidSrBean.getBidCellCnt(tableId);
                        //tmpRowId++;
                        bidTableCnt++;
                        noOfRows = rowId;
                        rFlag = true;
                    }

                    //rowId++;

                    while(tblBidCellsDtl.hasNext())
                    {
                        CommonFormData bidCelData = tblBidCellsDtl.next();

                        columnId = bidCelData.getColumnId();
                        if(isTableWithGrandTotal && "Yes".equalsIgnoreCase(isMultipleFilling)){
                            if(bidCelData.getRowId() > (rows-1) && tableCount!=(i+1)){
                                continue;
                            }
                        }

                        if(bidFlag)
                        {
                            rowId = bidCelData.getRowId();
                            tmpRowId = rowId;
                            bidTableCnt=0;
                            noOfRows = 0;
                        }
                        else
                        {
                            rowId = bidCelData.getRowId();
                            tmpRowId = (noOfRows*bidTableCnt)+rowId;
                        }
                        cellValue = request.getParameter("row"+tableId+"_"+tmpRowId+"_"+columnId);
                       
                        cellValue = HandleSpecialChar.handleSpecialChar(cellValue);
                        cellId = bidCelData.getCellId();

                        tblXML_TenderNegoBidDetail += "<tbl_NegBidDtlSrv "
                        + "negBidTableId=\"IdentKey"+identkey+"\" "
                        + "tenderColumnId=\""+ columnId +"\" "
                        + "tenderTableId=\""+ tableId +"\" "
                        + "cellValue=\""+ cellValue +"\" "
                        + "rowId=\""+ (rowId) +"\" "
                        + "cellId=\""+ cellId +"\" "
                        + "formId=\""+ tenderFormId  +"\" "
                        + "negId=\""+negId +"\" "
                        + "/>";

                        //bidCelData = null;
                    }
                }
                else
                {
                    break;
                }
            }

            tblXML_TenderNegoBidDetail += "</root>";
            
            // System.out.println(tblXML_TenderNegoBidForm);
          //  System.out.println(tblXML_TenderNegoBidTableDetail);
            //out.println("<BR><BR>")
           // System.out.println(tblXML_TenderNegoBidDetail);
           
            
            
            
            
           if("Update".equalsIgnoreCase(action)){
                try{
                     //(String xmlStr1, String xmlStr2, String xmlStr3, int bidId,int negId,int userId, int formId) 
                    if(tenderBidSrBean.updateBidderNegFormDataSrv(tblXML_TenderNegoBidForm,tblXML_TenderNegoBidTableDetail,tblXML_TenderNegoBidDetail,bidId,negId,userId,tenderFormId)){
                       
                        response.sendRedirect("NegTendererForm.jsp?tenderId="+tenderId+"&uId="+uId+"&negId="+negId+"&msg=edit");
                    }else{
                        remarks="Error in "+remarks;
                        LOGGER.debug("Form Data updation unsuccessfully");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                     remarks="Error in "+remarks;
                    LOGGER.debug("Error in Insert form data : "+e.toString());
                }
                finally
                {
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);     
                }
            }
            if("Save".equalsIgnoreCase(action)){
                try{
                     //(String xmlStr1, String xmlStr2, String xmlStr3, int bidId,int negId,int userId, int formId)
                    if(tenderBidSrBean.insertBidderNegFormDataSrv(tblXML_TenderNegoBidForm,tblXML_TenderNegoBidTableDetail,tblXML_TenderNegoBidDetail,bidId,negId,userId,tenderFormId)){

                        response.sendRedirect("NegTendererForm.jsp?tenderId="+tenderId+"&uId="+uId+"&negId="+negId+"&msg=add");
                    }else{
                        remarks="Error in "+remarks;
                        LOGGER.debug("Form Data inserted unsuccessfully");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                     remarks="Error in "+remarks;
                    LOGGER.debug("Error in Insert form data : "+e.toString());
                }
                finally
                {
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                }
            }
        }
            if("Delete".equalsIgnoreCase(action))
            {
                 auditAction="Bidder has Deleted Forms";
                 remarks="";
                
                try
                {
                    if (tenderBidSrBean.deleteBidderNegFormDataSrv(negBidformId))
                    {
                        response.sendRedirect("NegTendererForm.jsp?tenderId=" + tenderId + "&uId=" + uId + "&negId=" + negId + "&msg=delete");
                    } else {
                        remarks = "Error in " + remarks;
                        LOGGER.debug("Form Data deletion unsuccessfully");
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                     remarks="Error in "+remarks;
                    LOGGER.debug("Error in Delete form data : "+e.toString());
                }
                finally
                {
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                }
            }
 
    
%>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
