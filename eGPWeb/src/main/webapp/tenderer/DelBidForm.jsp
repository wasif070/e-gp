<%-- 
    Document   : delBidForm
    Created on : Dec 21, 2010, 6:27:54 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
<%
    int formId = 0;
    if (request.getParameter("formId") != null)
    {
        formId = Integer.parseInt(request.getParameter("formId"));
    }
    int userId = 0;
    if (session.getAttribute("userId") != null)
    {
        userId = Integer.parseInt(session.getAttribute("userId").toString());
    }
    int bidId = 0;
    if (request.getParameter("bidId") != null)
    {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    int lotId = 0;
    if (request.getParameter("lotId") != null && !"".equalsIgnoreCase(request.getParameter("lotId")))
    {
        lotId = Integer.parseInt(request.getParameter("lotId"));
    }

    int tenderId = 0;
    if (request.getParameter("tenderId") != null && !"".equalsIgnoreCase(request.getParameter("tenderId")))
    {
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }

       if ("Delete".equalsIgnoreCase(request.getParameter("action")))
    {

        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType = "tenderId";
        int auditId = tenderId;
        String auditAction = "Bidder has Deleted Forms";
        String moduleName = EgpModule.Bid_Submission.getName();
        String remarks = "Bidder(User id): " + session.getAttribute("userId") + " has Deleted Form id:" + formId + " & Bid Id: " + bidId;
    
        
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        boolean isFinalSub = tenderCommonService.isFinalSubDone("" + tenderId, "" + userId);
        if (isFinalSub)
        {
            if (lotId == 0)
            {
                response.sendRedirect("LotTendPrep.jsp?tab=4&tenderId=" + request.getParameter("tenderId") + "&msg=delerror");
            }
            else
            {
                response.sendRedirect("BidPreperation.jsp?tab=4&lotId=" + lotId + "&tenderId=" + tenderId + "&msg=delerror");
            }
        }
        else
        {
            if (tenderBidSrBean.delBidForm(formId, userId, bidId)) // If deletion Operation Successed?
            {
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                if (lotId == 0)
                {
                    response.sendRedirect("LotTendPrep.jsp?tab=4&tenderId=" + request.getParameter("tenderId") + "&msg=del");
                }
                else
                {
                    response.sendRedirect("BidPreperation.jsp?tab=4&lotId=" + lotId + "&tenderId=" + tenderId + "&msg=del");
                }
            }
            else // If deletion Operation Failed
            {
                auditAction = "Error in Tenderer has Deleted Forms";
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                if (lotId == 0)
                {
                    response.sendRedirect("LotTendPrep.jsp?tab=4&tenderId=" + request.getParameter("tenderId") + "&msg=del");
                }
                else
                {
                    response.sendRedirect("BidPreperation.jsp?tab=4&lotId=" + lotId + "&tenderId=" + tenderId + "&msg=del");
                }
            }
        }


    }
%>
<!--    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>-->