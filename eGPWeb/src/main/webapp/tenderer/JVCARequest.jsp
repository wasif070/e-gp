<%-- 
    Document   : JVCARequest
    Created on : Mar 1, 2011, 2:46:09 PM
    Author     : Swati
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
    %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Acceptance of JVCA Partnership</title>
<link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
                $("#jvcaRequest").validate({
                    rules: {
                        radiobtn:{required:true}
                    },
                    messages: {
                        chkQuestion:{required:"<div class='reqF_1'>Please select your choice</div>"}
                    }
                });
            });
</script>
<script type="text/javascript">
            function chkConfirm()
            {
                var value='Disagree';
                if($('#radiobtn').attr("checked")==true){
                    value = 'Agree';
                }
               var retVal = confirm("Are you sure you want to "+value+" to Proposed JVCA?");
               //alert(retVal);
               if( !retVal){
                      return false;
               }
                 
            }
</script>
</head>
<body>
    <jsp:useBean id="jvcaSrBean" class="com.cptu.egp.eps.web.servicebean.JvcaSrBean" scope="request"/>

    <%
    int JVId = 0;
    String cuserId = "";
    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
             cuserId = session.getAttribute("userId").toString();
         }
    jvcaSrBean.setLogUserId(cuserId);
    jvcaSrBean.setAuditTrail(null);
    if(request.getParameter("JVCId")!= null)
            JVId = Integer.parseInt(request.getParameter("JVCId"));

    if ("Submit".equals(request.getParameter("submitbtn"))) {
        jvcaSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        String status = request.getParameter("radiobtn");
        //to update JVCA partner status
        int success = jvcaSrBean.updateJvcaPartner(JVId,status,Integer.parseInt(cuserId));
        if(success > 0){
            //response.sendRedirectFilter(request.getContextPath()+"/tenderer/JvcaList.jsp?msg=req");
            response.sendRedirect("JvcaList.jsp?msg=req");
        }
    }
    %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
   <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
<div class="contentArea_1">
  <div class="pageHead_1">Acceptance of JVCA Partnership
  <span style="float: right;">
        <a class="action-button-goback" href="JvcaList.jsp">Go Back</a>
  </span>
  </div>
  <%
      List<Object []> listPartner = null;
      //to get NAme of current JVCA User by JVCID
      jvcaSrBean.getJvcaNamefromId(JVId);

      //to get JVCA partner List(JVCID vise)
      listPartner = jvcaSrBean.getJvcaPartnerDetailList(JVId);
  %>
  <div class="inner-tableHead t_space"><b>Name Of JVCA : </b> <%= jvcaSrBean.getJVCAName() %>
        
  </div>
  <div>&nbsp;</div>
    <table width="970" cellspacing="0" class="tableList_1">
     
      <tr>
        <th width="45">Sl. No.</th>
        <th width="212" class="table-fileName">Company Name</th>
        <th width="254" class="t-align-left">e-mail ID</th>
        <th width="145" class="t-align-left">JVCA Role</th>
        <%--<th width="161" class="t-align-left">Nominated Partner</th>--%>
        <th width="139" class="t-align-left">JVCA Request Status</th>
      </tr>
        <%for (int i = 0; i < listPartner.size(); i++) { %>
      <tr>
          <td class="t-align-center"><%=(i+1)%></td>
          <td class="t-align-left"><%=listPartner.get(i)[0]%></td>
          <td class="t-align-left"><%=listPartner.get(i)[4]%></td>
          <td class="t-align-center"><%=listPartner.get(i)[1]%></td>
          <%--<td class="t-align-center"><%=listPartner.get(i)[2]%></td>--%>
          <td class="t-align-center"><%=listPartner.get(i)[3]%></td>
      </tr>
      <%}%>
      
    </table>
  <div>&nbsp;</div>
  <form id="jvcaRequest" action="JVCARequest.jsp?JVCId=<%=JVId%>" method="post" >
	<table width="970" cellspacing="10" cellpadding="0" border="0">
    	<tr>
            <td width="185">
            <label id="agree">
                <input name="radiobtn" id="radiobtn" type="radio" value="Agreed" checked/>
                <b> I Agree to proposed JVCA </b>
                </label>
            </td>
            <td width="779">
            <label id="disagree">
                <input name="radiobtn" id="radiobtn" type="radio" value="Disagreed" />
                <b> I Disagree to proposed JVCA </b>
                </label>
            </td>
        </tr>
    	<tr>
    	  <td colspan="2" class="t-align-center">
          	<label class="formBtn_1">
                    <input name="submitbtn" id="submitbtn" type="submit" value="Submit"  onclick="return chkConfirm();"/>
            </label>
          </td>
   	  </tr>
    </table>
 </form>
</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
   <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->
</div>
</body>
</html>

