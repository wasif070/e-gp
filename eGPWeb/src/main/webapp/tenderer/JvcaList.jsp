<%-- 
    Document   : JvcaList
    Created on : Mar 1, 2011, 2:25:58 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblJvcapartners"%>
<%@page import="com.cptu.egp.eps.model.table.TblJointVenture"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List of JVCA</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<%--<jsp:useBean id="jvcaSrBean" class="com.cptu.egp.eps.web.servicebean.JvcaSrBean" scope="request"/>--%>
<script type="text/javascript">
function forPartner(){
         $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/JVCAServlet?funName=viewpartner&status=partner",
            datatype: "xml",
            height: 250,
            colNames:['Sl. <br/> No.','Name of JVCA','Status','Action'],
            colModel:[
                {name:'srno',index:'srno', width:5,sortable:false,search: false,align:'center'},
                {name:'jvname',index:'jvname', width:65,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'jvstatus',index:'jvstatus', width:10,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }, align:'center'},
                {name:'action',index:'action', width:30,sortable:false,search: false, align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            caption: "",
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});
    }

    function forJvca(){
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/JVCAServlet?funName=viewjvca&status=jvca",
            datatype: "xml",
            height: 250,
            colNames:['Sl. <br/> No.','Name of JVCA','Status','Action'],
            colModel:[
                {name:'srno',index:'srno', width:8, sortable:false, search: false,align:'center'},
                {name:'jvname',index:'jvname', width:80,sortable: true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'jvstatus',index:'jvstatus', width:10,sortable:true,searchoptions: { sopt: ['eq', 'cn'] },align:'center'},
                {name:'action',index:'action', width:22,sortable:false,align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            caption: "",
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});
    }
</script>
<%if(request.getParameter("msg")!=null && "req".equalsIgnoreCase(request.getParameter("msg"))){%>
<script type="text/javascript">
jQuery().ready(function (){
         forPartner(); 
        });
</script>        
<%}else{%>
<script type="text/javascript">
jQuery().ready(function (){
         forJvca();
          });

</script>        
<%}%>

<script type="text/javascript">
    function checkSatus(status){
        if("jvca" == status ){
            document.getElementById("hrefJvca").className = "sMenu";
            document.getElementById("hrefPartner").className="";
            forJvca();
        }
        else{
            document.getElementById("hrefJvca").className = "";
            document.getElementById("hrefPartner").className = "sMenu";
            forPartner();
        }
    }
</script>
</head>
<body>
    <%
    String uId="0";
    if(session.getAttribute("userId")!=null){
        uId=session.getAttribute("userId").toString();
    }
    LoginMasterSrBean userLoginBean = new LoginMasterSrBean(uId);
    uId=null;
    %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%--<%
                List<TblJointVenture> listJvca = null;
                List<Object []> listPartner = null;
                String type = "";
                if(request.getParameter("action")!=null){
                type = request.getParameter("action");
                }
                int userId = Integer.parseInt(session.getAttribute("userId").toString());
                if(type.equalsIgnoreCase("jvca")){
                    listPartner = jvcaSrBean.partnerRequestJvcaList(userId);
                }else{
                    listJvca = jvcaSrBean.preposedJvca(userId);
                        
                }

  %>--%>
  <%
    HttpSession hs = request.getSession();
    int userId = Integer.parseInt(hs.getAttribute("userId").toString());
    String strRegType = "";
    
    strRegType = userLoginBean.getUserRegType(userId);
  %>
  <div class="contentArea_1">
      <div class="pageHead_1">List of JVCA
          <span style="float:right;">
              <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('3');">Save as PDF</a></span>
              <% if(!"individualconsultant".equalsIgnoreCase(strRegType)){ %><a href="CreateJVCA.jsp" class="action-button-add">Add JVCA</a><% } %>
          </span>
      </div>
              <%if(request.getParameter("msg")!=null && "complete".equalsIgnoreCase(request.getParameter("msg"))){%>
              <div class="responseMsg successMsg" style="margin-top: 10px;" >JVCA invitation sent successfully</div>
          <%}%>
          <%if(request.getParameter("msg")!=null && "req".equalsIgnoreCase(request.getParameter("msg"))){%>
              <div class="responseMsg successMsg" style="margin-top: 10px;" >JVCA invitation processed successfully</div>
          <%}%>
          
      <ul class="tabPanel_1 t_space">
     <%if(request.getParameter("msg")!=null && "req".equalsIgnoreCase(request.getParameter("msg"))){%>
     <li><a href="javascript:void(0);" id="hrefJvca" onclick="checkSatus('jvca');"  >Proposed JVCA</a></li>
     <li><a href="javascript:void(0);" id="hrefPartner" onclick="checkSatus('partner');" class="sMenu">JVCA Partner Requests</a></li>
     <%}else{%>
     <li><a href="javascript:void(0);" id="hrefJvca" onclick="checkSatus('jvca');" class="sMenu" >Proposed JVCA</a></li>
     <li><a href="javascript:void(0);" id="hrefPartner" onclick="checkSatus('partner');">JVCA Partner Requests</a></li>
     <%}%>
      </ul>
  <div class="tabPanelArea_1 t_space" >
                                            <table width="100%" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <%--<div >
                                                            <%if (!" ".equalsIgnoreCase(msg) || flag) {%>
                                                            <div id="successMsg" class="responseMsg successMsg" style="display:none"><%=msg%></div>
                                                            <%} else if (!" ".equalsIgnoreCase(msg)) {%>
                                                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                                                            <%}%>
                                                        </div>--%>

                                                        <div align="center" id="jqGrid">
                                                        </div>
                                                        <%--<div align="center" id="divApprove" style="display:none">
                                                            <table id="lista"></table>
                                                            <div id="page">
                                                            </div>
                                                        </div>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        
    <%--<table width="100%" cellspacing="0" class="tableList_1">
      <tr>
        <th width="6%">S. No.</th>
        <th width="80%" class="t-align-left">Name Of JVCA</th>
        <th width="9%" class="t-align-left">Status</th>
        <%if (type.equalsIgnoreCase("jvca")) {%>
        <th width="5%" class="t-align-left">Process</th>
        <%}%>
      </tr>
      <%int cnt = 0;
                  if (type.equalsIgnoreCase("jvca")) {
                      if (listPartner != null && listPartner.size() > 0) {
                          for (int i = 0; i < listPartner.size(); i++) {
                              cnt++;
      %>
      <tr>
          <td class="t-align-center"><%=cnt%></td>
          <td class="t-align-left"><%=listPartner.get(i)[0]%></td>
          <td class="t-align-left"><%=listPartner.get(i)[2]%></td>
          <%if(listPartner.get(i)[2].toString().trim().equalsIgnoreCase("pending")){%>
          <td class="t-align-center"><a href="JVCARequest.jsp?JVCId=<%=listPartner.get(i)[1]%>"  >Edit</a></td>
              <%}else{%>
          <td class="t-align-center">-</td>
              <%}%>

      </tr>
      <%}//For Loop Ends here
                }//Size and null check of list

            }//If type = jvca Condition
            else {
                cnt = 0;
                if (listJvca != null && listJvca.size() > 0) {
                    for (int i = 0; i < listJvca.size(); i++) {
                        cnt++;
      %>
      <tr>
          <td><%=cnt%></td>
          <td><%=listJvca.get(i).getJvname()%></td>
          <td><%=listJvca.get(i).getJvstatus()%></td>
      </tr>
      <%}//For Loop Ends here
                      }//if List is not null and size is > 0
                  }//Else Condition ends here


      %>
    </table>--%>
  </div>
  </div>
  <div>&nbsp;</div>
  <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="JvcaList_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="JvcaList" />
            </form>
            <!--For Generate PDF  Ends-->
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
</html>
<%--<%
    if(jvcaSrBean!=null){
            jvcaSrBean = null;
        }
      if(listJvca!=null){
            listJvca.clear();
            listJvca = null;
          }
      if(listPartner!=null){
            listPartner.clear();
            listPartner = null;
          }

%>--%>

