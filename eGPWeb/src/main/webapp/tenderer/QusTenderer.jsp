<%-- 
    Document   : qusTenderer
    Created on : Dec 8, 2010, 6:37:09 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Post Query</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
<body>
    <div class="mainDiv">
            <div class="fixDiv">
               <%@include  file="../resources/common/AfterLoginTop.jsp"%>
<%
    boolean bolflag=false;
    String action="";
    String msg="";

    if (request.getParameter("flag") != null) {
        if ("true".equalsIgnoreCase(request.getParameter("bolflag"))) {
            bolflag = true;
        }
    }

    if (request.getParameter("action") != null) {
        action = request.getParameter("action");
    }

    int suserTypeId=0;
    int userId=0;
    int tenderId=0;
    if( session.getAttribute("userTypeId") != null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
        suserTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
    }
    if(session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())){
        userId = Integer.parseInt(session.getAttribute("userId").toString());
    }
    if(request.getParameter("tenderId") != null){
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }

    
    preTendDtBean.setUserId(userId);
    preTendDtBean.setTenderId(tenderId);
    List<SPTenderCommonData> postQueryData = preTendDtBean.getDataFromSP("GetMyPreBidQuestion",tenderId,userId);
    List<SPTenderCommonData>  getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate",tenderId,0);
    
    java.util.Calendar currentDate = java.util.Calendar.getInstance();    
    java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("dd-MMM-yyyy hh:mm");
    String preBidEndDate="";
    java.util.Date edate = null;

    boolean isQuestionStart=false;
    //String  queryDate= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date());

    if(getPreBidDates != null){
        if(getPreBidDates.size() > 0){
             if(getPreBidDates.get(0).getFieldName2() != null){
                edate = sd.parse(getPreBidDates.get(0).getFieldName2());
                        
                if(currentDate.getTime().after(edate)){
                    isQuestionStart = true;
                }
                else{
                    isQuestionStart = false;
                }
             }else{
                 isQuestionStart = true;
             }
        }
    }

     List<SPTenderCommonData> getConfigQus = preTendDtBean.getDataFromSP("GetConfigPrebid",tenderId,0);
    List<SPTenderCommonData> getClosingDt = preTendDtBean.getDataFromSP("GetClosingDt",tenderId,0);


    java.util.Calendar calendar = java.util.Calendar.getInstance();
    int questionEndDate = 0;
    java.util.Date tenderClosingDate = null;

    if(getClosingDt.size() > 0){
           if(getClosingDt != null){
              tenderClosingDate = sd.parse(getClosingDt.get(0).getFieldName2());
           }
    }

    if (getConfigQus.size() > 0) {
        if (getConfigQus != null) {
            if(getClosingDt.size() > 0){
                if(getClosingDt != null){
                    if (getClosingDt.get(0).getFieldName2() != null) {
                        calendar.setTime(sd.parse(getClosingDt.get(0).getFieldName2()));
                        if (getConfigQus.get(0).getFieldName2() != null) {
                            questionEndDate = Integer.parseInt(getConfigQus.get(0).getFieldName2());
                            calendar.add(Calendar.DATE, -questionEndDate);
                        }
                    }
                }
            }
        }
    }

    

    if(bolflag){
       msg = " Query Posted Successfully.";
    }

    java.util.Date d = new java.util.Date();
    

    /*java.util.Date today=new java.util.Date();
    Calendar calendar;

    calendar = Calendar.getInstance();
    calendar.setTime(sd.parse(getPreBidDates.get(0).getFieldName2()));
    calendar.add(Calendar.DATE, +2);*/
    


%>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Post Query</div>
   <%
              // Variable tenderId is defined by u on ur current page.
              pageContext.setAttribute("tenderId",tenderId);
  %>

    <%@include file="../resources/common/TenderInfoBar.jsp" %>
    
    <div>&nbsp;</div>
    <%@include file="../tenderer/TendererTabPanel.jsp" %>
      
 <%--<%@include file="TendererTabPanel.jsp" %>--%>

  <table width="100%" cellspacing="0" class="tableList_1">
  	<tr>
    	<td colspan="4">
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td colspan="3" >
                        <div >
                        <%if(bolflag){ %>
                        <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                        <%}else{%>
                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                         <%}%>
                         </div>
                    </td>
                </tr>
			  <tr>
                              <%--<td width="15%" class="t-align-left"><strong>Meeting Start Date &amp; Time :</strong></td>
				<td width="30%" class="t-align-left"><%=preBidStartDate %> </td>
				<td width="15%" class="t-align-left"><strong>Meeting End Date &amp; Time :</strong></td>
				<td width="30%" class="t-align-left"><%=preBidEndDate%></td>--%>
			    <td width="10%" class="t-align-left">&nbsp;
                                <%--<%
                                    if(isQuestionStart)
                                    {
                                        if(questionEndDate > 0){                                           
                                            if(calendar.getTime().before(currentDate.getTime())){
                                %>
                                                 <a href="#" onclick="ruleAlert();" >Post Question</a>
                                <%
                                            }
                                            else{
                                %>
                                                <a href="PostQuestion.jsp?tenderId=<%=tenderId%>">Post Question </a>
                                <%
                                            }
                                        }
                                        else if(tenderClosingDate.before(currentDate.getTime())){
                                %>
                                            <a href="PostQuestion.jsp?tenderId=<%=tenderId%>">Post Question </a>
                                <%      }else{
                                            out.print("Post Question");
                                        }
                                    }
                                    else{
                                           out.print("Post Question");
                                    }
                                %>--%>
                                <%
                                    TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
                                    if (session.getAttribute("userId") != null) {
                                        tenderPostQueConfigService.setLogUserId(session.getAttribute("userId").toString());
                                    }                                      
                                    List<TblTenderPostQueConfig> listt = tenderPostQueConfigService.getTenderPostQueConfig(tenderId);
                                    if(!listt.isEmpty())
                                    {
                                        Date lastdate = listt.get(0).getPostQueLastDt();
                                        Date todaysdate = new Date();                                                                                
                                        if(lastdate.after(todaysdate))
                                        {
                                %>
                                              <a href="PostQuestion.jsp?tenderId=<%=tenderId%>">Post Query</a>
                                <%
                                        }else{
                                            out.print("Post Question");
                                        }
                                    }
                                %>
                            </td>
                            <td width="15%" class="t-align-left ff">View Queries & Replies  :</td>
                              <td width="10%" class="t-align-left">
                                  <a href="<%=request.getContextPath()%>/resources/common/QuestionAnswer.jsp?tenderId=<%=tenderId%>&viewType=Question" target="_blank"  >View</a>
                              </td>
			  </tr>
			</table>

                <table width="100%" cellspacing="0" class="tableList_1">
<!--                    <tr><td>
                            <ul class="tabPanel_1 t_space">
                                <li><a href="javascript:void(0);" class="sMenu" id="myquery" onclick="getQueryData('GetMyPreBidQuestion');" >My Questions</a></li>
                                <li><a href="#" id="allquery"  onclick="getQueryData('GetAllPreBidQuery')" >All Queries</a></li>
                            </ul>
                        </td> </tr>-->
<!--                    <tr><td class="ff">Questions</td></tr>-->
                    <tr><td>
                            <%
                               Date dtLastDate = null;
                                if(!listt.isEmpty())
                                {
                                    dtLastDate = listt.get(0).getPostQueLastDt();
                                }
                            %>
                            <div class="responseMsg noticeMsg"><span>Query can be posted till <%=DateUtils.gridDateToStrWithoutSec(dtLastDate)%></span></div>
                            <div id="queryData">
                            </div>
                        </td> </tr>
                    </table>

      </td>
    </tr>
  </table>
       <%if (isQuestionStart) {
                    if(questionEndDate > 0){
       %>
       <table width="100%" cellspacing="0" class="t_space">
           <tr><td>
                   <div id="noticeMsg" class="responseMsg noticeMsg" style="display:block">
                       Query can be posted till <%=new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.getTime())%>
                   </div>
               </td>
           </tr>
       </table>
       <%      
               }
           }
       %>

<div>&nbsp;</div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    <!--Dashboard Footer End-->
</div>
            </div>
            </div>
    </div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<script type="text/javascript">

//    function forTabDisplay(str)
//    {
//        if(str == "myqury"){
//            document.getElementById("myquery").className ="sMenu" ;
//             document.getElementById("allquery").className="";
//            $('#tr0').show();
//             $('#tr1').hide();
//        }
//    }

     function getQueryData(queryData){
       $.ajax({
        url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=getQuestionData&queryAction="+queryData,
        method: 'POST',
        async: false,
        success: function(j) {
            document.getElementById("queryData").innerHTML = j;
        }
     });}
//             $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'getQuestionData',queryAction:queryData}, function(j){
//                 document.getElementById("queryData").innerHTML = j;
//             });
//             if(queryData == "GetMyPreBidQuestion"){
//                 document.getElementById("myquery").className ="sMenu" ;
//                 document.getElementById("allquery").className="";
//             }else{
//                  document.getElementById("myquery").className ="" ;
//                 document.getElementById("allquery").className="sMenu";
//             }
   

    getQueryData('GetMyPreBidQuestion');
    
    function ruleAlert(){
        var dateValue = '<%=new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.getTime())%>';
         jAlert('Query can be posted till '+dateValue,'Query - Answer', function(RetVal) {
          });
    }
</script>
<%
    postQueryData = null;
    preTendDtBean = null;
%>


