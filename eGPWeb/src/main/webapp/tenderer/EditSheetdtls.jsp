<%--
    Document   : EditSheetdtls
    Created on : Dec 12, 2011, 4:35:09 PM
    Author     : shreyansh Jogi
--%>

 <%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        List<Object> formmapId = cmss.getSrvFormMapId(Integer.parseInt(request.getParameter("tenderId")), 15);
        List<Object[]> list = null;
        int j=0;
        if(formmapId!=null && !formmapId.isEmpty()){
           list = cmss.getListForPRForTB((Integer)formmapId.get(0),30);
            }

%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script src="../resources/js/monthDaysCalculation.js" type="text/javascript"></script>
        <script type="text/javascript">
             var daysArray1 = new Array();
             var daysArray2 = new Array();
             var daysArray3 = new Array();
        </script>
          <%
                                            if(list!=null && !list.isEmpty()){
                                                int i = 0;
                                                for(Object obj[] : list){%>
                                                <script type="text/javascript">
                                                        daysArray1[<%=i%>] = '<%=obj[5]%>';
                                                        daysArray2[<%=i%>] = '<%=obj[6]%>';
                                                        daysArray3[<%=i%>] = '<%=obj[3]%>';
                                                </script>
                                                <% i++;}
                                                }%>
        <script>
            function ViewPRR(wpId,tenderId,lotId,cntId){
                dynamicFromSubmit("viewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
            }
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
           function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function checkForAttSheet1(){
                    document.getElementById("action").value = "editsheettenside";
                    document.forms["prfrm"].submit();
            }
 function checkNod(id){
                var nod = $('#nod_'+id).val();

                var WorkFrom = daysArray3[id];
                var remOffcnt = daysArray1[id];
                var remOncnt = daysArray2[id];
                var monthDays = getMonthDays($('#month').val(),$('#year').val());
                var check = true;
                 $('.err').remove();
                if(!regForNumber(nod)){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Days must be numeric value </div>");
                    check = false;
                    document.getElementById('nod_'+id).value="0";
                }else if(nod.indexOf("-")!=-1){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
                     check = false;
                    document.getElementById('nod_'+id).value="0";
                }else if(!positive(nod)){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                     check = false;
                    document.getElementById('nod_'+id).value="0";
                }else if(nod.length > 5){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
                     check = false;
                    document.getElementById('nod_'+id).value="0";
                }else{
                    check = true;
                }
                if(check){
                    if(WorkFrom.toUpperCase() =="HOME"){

                    if(parseInt(nod) > parseInt(monthDays)){
                         jAlert('please enter valid days','Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id).val('0');
                    }else{
                        if(parseInt(nod) > parseInt(remOffcnt)){
                               jAlert('No of days can not be greater than total Home count '+remOffcnt,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id).val('0');
                        }

                    }

                }else{

                     if(parseInt(nod) > parseInt(monthDays)){
                         jAlert('please enter valid days','Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id).val('0');
                    }else{
                       if(parseInt(nod) > parseInt(remOncnt)){
                        jAlert('No of days can not be greater than total Field count '+remOncnt,'Progress Report', function(RetVal) {
                                        });
                                        $('#nod_'+id).val('0');

                    }
                }
                }

            }
            }


        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Edit Attendance Sheet
            <span class="c-alignment-right">
                <%
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");            
                    String serviceType = commonService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if("Time based".equalsIgnoreCase(serviceType.toString()))
                    {    
                %>
                        <a href="ProgressReportMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}else{%>
                        <a href="SrvLumpSumPr.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}%>
            </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");

            %>
                <div class="tabPanelArea_1">
                    <div align="center">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

                                %>
                                <form name="prfrm" id="prfrm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post">
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                 <th width="3%" class="t-align-center">Sr.No.</th>
                                                <th width="20%" class="t-align-center">Name of Employees</th>
                                                <th width="15%" class="t-align-center">Position Assigned
                                                </th>
                                                <th width="9%" class="t-align-center">Home / Field
                                                </th>
                                                <th width="5%" class="t-align-center">No. of Days
                                                </th>
                                                <th width="18%" class="t-align-center">Month
                                                </th>
                                                <th width="18%" class="t-align-center">Year
                                                </th>
                                                 <th width="10%" class="t-align-center">Remarks
                                                </th>
                                            </tr>

                                                <%
                                                List<Object[]> listt = cmss.ViewAttSheetDtls(Integer.parseInt(request.getParameter("sheetId")));
                                                 MonthName monthName = new MonthName();
                                                 int jj = 0;
                                            if(listt!=null && !listt.isEmpty()){

                                                for(Object obj[] : listt){%>
                                                <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%>
                                                </td>

                                                <td width="5%" class="t-align-left"><%=obj[7]%>
                                                </td>
                                                <td width="9%" style="text-align: right" >
                                                    <Input type="text" name="nod_<%=jj%>" value="<%=obj[3]%>" onchange="checkNod(<%=jj%>);" id="nod_<%=jj%>" class="formTxtBox_1" />

                                                </td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                </td>
                                                <td width="5%" style="text-align: right"><%=obj[6]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><input type="text" class="formTxtBox_1" readonly name="remarks_<%=jj%>" id="remarks_<%=jj%>" value="<%=obj[11]%>" />
                                                </td>
                                                </tr>
                                                <input type="hidden" name="attdtlsheetId_<%=jj%>" value="<%=obj[8]%>" />
                                                <input type="hidden" name="WF_<%=jj%>" value="<%=obj[7]%>" />
                                                <input type="hidden" name="tcId_<%=jj%>" value="<%=obj[12]%>" />


                                                   <% jj++;}}%>
                                                   <input type="hidden" name="attsheetId" value="<%=listt.get(0)[9]%>" />
                                                   <input type="hidden" name="listcount" value=<%=jj%> />
                                                   <input type="hidden" name="action" id="action" value="" />
                                                   <input type="hidden" name="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                                   <input type="hidden" name="status" id="status" value="" />
                                                   <input type="hidden" name="month" id="month" value="<%=listt.get(0)[5]%>" />
                                                    <input type="hidden" name="year" id="year" value="<%=listt.get(0)[6]%>" />
                                                   </table>
                                                    </div>
                                                   </div>
                                                   <br />
                                                            <div class="responseMsg noticeMsg t_align_left">
                                                                Once Attendance Sheet send to PE, It can not be edited unless PE rejects the same.
                                                            </div>
                                                   <br />

        <center>
               <label class="formBtn_1">
                    <input type="button" name="button" onclick="checkForAttSheet1();" id="statusbtnrjct"  value="Send to PE" />
                </label>
        </center>
                                 
                                </form>
                   
                
            </div>
        </div>
        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>


