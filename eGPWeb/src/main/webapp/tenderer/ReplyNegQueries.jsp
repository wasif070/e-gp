<%--
    Document   : NegTendOfflineProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>

<%@page import="com.cptu.egp.eps.model.table.TblNegQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
          <script type="text/javascript">

     $(document).ready(function() {
        $("#idfrmreplyNegQueries").validate({
            rules: {
                txtReply: { required: true,maxlength: 1000}

            },
            messages: {
                txtReply: { required: "<div class='reqF_1'>Please enter Reply</div>",
                maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}
            }
        }
    );
    });


 </script>
</head>
<body>
<%@include  file="../resources/common/AfterLoginTop.jsp" %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="contentArea">
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%
        int tenderId =0;
        int negId = 0;
        int negQueryId = 0;

        if(request.getParameter("tenderId") != null){
            tenderId= Integer.parseInt(request.getParameter("tenderId"));
        }
        if(request.getParameter("negId") != null){
            negId= Integer.parseInt(request.getParameter("negId").toString());
        }
        if(request.getParameter("negQueryId") != null){
            negQueryId= Integer.parseInt(request.getParameter("negQueryId"));
        }

        pageContext.setAttribute("tenderId", tenderId);

        String queryText = "";

        List<TblNegQuery> queryDetails = negotiationdtbean.getQueryById(tenderId, negId, negQueryId);

        if(!queryDetails.isEmpty()){
           queryText =  queryDetails.get(0).getQueryText();

        }
   %>
  <div class="pageHead_1">
      Negotiation
      <span class="c-alignment-right">
        <a href="viewNegQuestions.jsp?tenderId=<%=tenderId %>&negId=<%=negId%>" class="action-button-goback" title="Go Back To Dashboard">Go Back To Dashboard</a>
      </span>
  </div>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <%pageContext.setAttribute("tab", "6");%>
  <%@include file="TendererTabPanel.jsp" %>
  
  <%if(!is_debared){%>
  <div class="tabPanelArea_1">
       <jsp:include page="EvalInnerTendererTab.jsp" >
        <jsp:param name="EvalInnerTab" value="4" />
        <jsp:param name="tenderId" value="<%=tenderId%>" />
     </jsp:include>
      <div class="tabPanelArea_1">
      <form name="frmreplyNegQueries" id="idfrmreplyNegQueries" action="<%=request.getContextPath()%>/NegotiationSrBean" method="post">
      <input type="hidden" name="action" id="idaction" value="replyQuestion" />
      <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
      <input type="hidden" name="negIdName" id="idnegIdName" value="<%=negId%>" />
      <input type="hidden" name="negQueryId" id="idnegQueryId" value="<%=negQueryId%>" />
      <div style="font-style: italic" class="ff" align="left">
          Fields marked with (<span class="mandatory">*</span>) are mandatory.
      </div>
        <table width="100%" cellspacing="0" class="tableList_1">
            <tr>
                <td width="10%" class="t-align-left ff">Question :</td>
                <td width="90%" class="t-align-left"><%=queryText%></td>
            </tr>
            <tr>
                <td width="10%" class="t-align-left ff">Document :</td>
                <td width="90%" class="t-align-left">
                    <a href="../resources/common/ViewNegQueryDocs.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>&queryId=<%=negQueryId%>" target="_blank">Download</a>
                </td>
            </tr>
            <tr>
                <td width="10%"  class="t-align-left ff">Reply : <span class="mandatory">*</span></td>
                <td>
                    <label>
                        <textarea name="txtReply" rows="7" class="formTxtBox_1" id="txtReply" style="width:675px;"></textarea>
                        <span id="SPReply" class="reqF_1"></span>
                    </label>
                </td>
            </tr>
            <tr>
                <td width="10%" class="t-align-left ff">Document :</td>
                <td width="90%" class="t-align-left">
                    <a href="javascript:void(0)" onclick="window.open('NegReplyDocs.jsp?tenderId=<%=tenderId%>&negQueryId=<%=negQueryId%>','window','width=900,height=500');">Upload</a>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <div id="queryData"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                                <input name="btnSubmit" type="submit" value="Submit" />
                        </label>
                    </div>
                </td>
            </tr>
        </table>
      </form>
      </div>
  </div><%}%>
  <div>&nbsp;</div>
   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center"
         <%@include file="../resources/common/Bottom.jsp" %>
   </div>
   <!--Dashboard Footer End-->
</div>
</div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<form id="form2" method="post">
</form>
<script language="javascript" type="text/javascript">
    function getDocData(){
        $.post("<%=request.getContextPath()%>/NegotiationSrBean", {tenderId:<%=tenderId%>,negQueryId:<%=negQueryId%>,docAction:'NegReplyDocs',action:'negDocs'}, function(j){
            document.getElementById("queryData").innerHTML = j;
        });
        document.getElementById("myquery").className ="" ;
        document.getElementById("allquery").className="sMenu";
    }
    function deleteFile(docName,docId,tender,negId,negQueryId){
            $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if(r == true){
                $.post("<%=request.getContextPath()%>/ServletNegReplyDocs", {docName:docName,docId:docId,tenderId:tender,negId:negId,negQueryId:negQueryId,funName:'remove'}, function(j){
                    jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                    getDocData();
                });
            });
            }else{
                getDocData();
            }
        });
    }
    function downloadFile(docName,docSize,tender,negId,negQueryId){
        document.getElementById("form2").action= "<%=request.getContextPath()%>/ServletNegReplyDocs?docName="+docName+"&negId="+negId+"&docSize="+docSize+"&negQueryId="+negQueryId+"&tenderId="+tender+"&funName=download";
        document.getElementById("form2").submit();
    }
getDocData();
</script>