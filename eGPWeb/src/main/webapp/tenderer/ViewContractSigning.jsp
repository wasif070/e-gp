<%-- 
    Document   : ViewContractSigning
    Created on : Jan 11, 2011, 3:38:31 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>        
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                int contractSignId = 0;
                if (request.getParameter("contractSignId") != null) {
                    contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                }

                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                int noaIssueId = 0;
                if (request.getParameter("noaIssueId") != null) {
                    noaIssueId = Integer.parseInt(request.getParameter("noaIssueId"));
                }
                
                //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                int counter;
                
                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=Integer.parseInt(tenderId);
                String auditAction="View Contract";
                String moduleName=EgpModule.Contract_Signing.getName();
                String remarks="User Id: "+session.getAttribute("userId")+" has viewed ContractId: "+contractSignId+" and NOAId: "+noaIssueId;
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> contractList = dataMoreService.geteGPDataMore("ContractDetailsofUser",String.valueOf(contractSignId));
    %>
    <body>
        <div class="contentArea_1">
            <div class="pageHead_1">Contract  Agreement
<!--                <span class="c-alignment-right">
                    <a href="ContractSigningListing.jsp?tenderId=<%//tenderId %>" class="action-button-goback">Go Back to Dashboard</a>
                </span>-->
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <form id="frmContractAgreement" name="frmContractAgreement" action="" method="post">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <%
                        for(SPCommonSearchDataMore details : contractList){

                    %>
                    <tr>
                        <td align="left" valign="middle" class="ff">Bidder's/Consultant's Name : </td>
                        <td align="left" valign="middle">
                            <%=details.getFieldName1()%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Procuring Agency : </td>
                        <td align="left" valign="middle">
                            <%out.print(details.getFieldName2()+" / "+details.getFieldName3().split("/")[1]);%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Contract Amount(In Nu.) : </td>
                        <td align="left" valign="middle">
                            <%=details.getFieldName4()%>
                        </td>
                    </tr>
                    <!--tr>
                        <td width="22%" align="left" valign="middle" class="ff">Last Date of Signing of Agreement :</td>
                        <td width="78%" align="left" valign="middle">< %=DateUtils.customDateFormate(details.getLastDtContractDt()) %>
                        </td>
                    </tr>-->
                    <tr>
                        <td align="left" valign="middle" class="ff">Date  of Signing of Agreement : </td>
                        <td align="left" valign="middle">
                            <%=details.getFieldName5()%>
                        </td>
                    </tr>                    
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name &amp; Address From PA:</td>
                        <td align="left" valign="middle">
                            <table width="70%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <% String nameAddPE = details.getFieldName6();
                                                        String[] splitnameAddPE = nameAddPE.split("@");
                                                        for (counter = 0; counter < splitnameAddPE.length; counter++) {
                                %>
                                <tr>
                                    <%if (counter == 0 || counter == 1) {
                                    %>
                                    <td class="t-align-left" width="510px">
                                        <%=splitnameAddPE[counter]%>
                                    </td>
                                </tr>
                                <% } }%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name &amp; Address From Bidder/Consultant:</td>
                        <td align="left" valign="middle">
                            <table width="70%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <% String nameAddTenderer = details.getFieldName6();
                                                        String[] splitnameAddTenderer = nameAddTenderer.split("@");
                                                        for (counter = 0; counter < splitnameAddTenderer.length; counter++) {
                                                            if (counter == 0 || counter == 1) {
                                %>
                                <tr>
                                <% } else {%>
                                <td class="t-align-left" width="510px">
                                    <%=splitnameAddTenderer[counter]%>
                                </td>
                                </tr>
                                <% } }%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Place of Signing Agreement : </td>
                        <td align="left" valign="middle"><%=details.getFieldName7() %></td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Publish Agreement on website :</td>
                        <td align="left" valign="middle"><% if(details.getFieldName8().equalsIgnoreCase("Yes")){%>Yes<% } else{%>No<%} %>
                            <label for="checkbox"></label>
                        </td>
                    </tr>
                    <% } %>
                </table>
                <div id="dataDoc">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="33%">File Name</th>
                            <th class="t-align-center" width="43%">File Description</th>
                            <th class="t-align-center" width="10%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="10%">Action</th>
                            <!--                            <th class="t-align-center" width="18%">Action</th>-->
                        </tr>
                        <%

                                    int docCnt = 0;
                                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    /*Listing of data*/
                                    for (SPTenderCommonData sptcd : tenderCS.returndata("contractSignDoc", String.valueOf(noaIssueId), null)) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-left"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-left">
                                <a href="<%=request.getContextPath()%>/ServletContractSignDoc?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&noaIssueId=<%=noaIssueId%>&tenderId=<%=tenderId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
<!--                                &nbsp;
                                <a href="<-%=request.getContextPath()%>/ServletContractSignDoc?&docName=<-%=sptcd.getFieldName1()%>&docId=<-%=sptcd.getFieldName4()%>&noaIssueId=<-%=noaIssueId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>-->
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                    </table>
                </div>
            </form>
        </div>
        <!--            </div>-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--        </div>-->
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>