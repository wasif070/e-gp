<%-- 
    Document   : CommonDocLib
    Created on : Dec 27, 2010, 2:14:22 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Common Documents Library</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>        
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            function checkfile()
            {                
                var vbool=true;
                var fvalue=document.getElementById('fileUpload').value;var dvalue=document.getElementById('txtDescription').value;
                var param="document";
                var ext = fvalue.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();

                if(fvalue== "")
                {
                /*if (param=="document" && ( fileType =='pdf'|| fileType =='zip'|| fileType =='jpeg' || fileType =='doc' || fileType =='docx' || fileType =='xls' || fileType =='xlsx' ||
                    fileType =='bmp' || fileType =='rar' || fileType =='png' || fileType == 'txt')||fileType =='jpg')

                {
                    var toCheck=fvalue;
                    var index1=toCheck.indexOf("\\");
                    var onlyFilePath=toCheck.substring(index1+1,toCheck.length);
                    var reExp1=/^[A-Za-z0-9\s\.\-\_\\]+$/
                     if (!reExp1.test(onlyFilePath)){
                            document.getElementById('fileerror').innerHTML ="<div class='reqF_1'>A file path may contain only Space, - , _ , \\ special characters !</div>";
                            vbool= false;
                    }
                    
                    document.getElementById('fileerror').innerHTML='';}else{
                    document.getElementById('fileerror').innerHTML="<div class='reqF_1'>File type is not allowed.</div>";
                        vbool= false;}
                }
                else
                {*/     
                        document.getElementById('fileerror').innerHTML="<div class='reqF_1'>Please select a file to upload.</div>";
                        vbool= false;
                }
                if(fvalue.indexOf("&", "0")!=-1 || fvalue.indexOf("%", "0")!=-1 || fvalue.indexOf("+", "0")!=-1 || fvalue.indexOf("#", "0")!=-1){
                                document.getElementById('fileerror').innerHTML="<div class='reqF_1'>File name should not contain special characters(%,&,+,#).</div>";
                        vbool= false;
                            }
                    if (document.getElementById('txtDescription').value=="")
                    {
                       document.getElementById('descriptionerror').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                       vbool= false;
                    }else
                    {
                        if(document.getElementById('txtDescription').value.length > 50){
                       document.getElementById('descriptionerror').innerHTML="<div class='reqF_1'>Maximum 50 characters are allowed.</div>";
                       vbool= false;
                        }else{
                           document.getElementById('descriptionerror').innerHTML='';
                           
                        }

                      }
                return vbool;
            }

            $(function() {
                $('#frmFileUp').submit(function() {
                    if($('#frmFileUp').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1 || fileName.indexOf("+", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+maxSize+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        function fetchunmapped(){		
            var panel = document.getElementById("innerPanel");
            panel.innerHTML = "Performing requested Operation. Please wait.<img src = 'images/loading.gif' />"
            $.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean", {work:'fetchunmapped'}, function(j){
                 $("fieldset#innerPanel").html(j);
             });
	}

        </script>
    </head>
    <style>
   .ui-jqgrid .ui-jqgrid-htable th div {
        height:auto;
        overflow:hidden;
        padding-right:0px;
        padding-top:0px;
        position:relative;
        vertical-align:text-top;
        white-space:normal !important;
    }
    </style>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
<%
        StringBuilder userType = new StringBuilder();
        if (request.getParameter("userType") != null) {
            if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                userType.append(request.getParameter("userType"));
            } else {
                userType.append("org");
            }
        } else {
            userType.append("org");
        }
        int userId=0;
        int tendererId=0;
        String type="";
        String title="";
        if(session.getAttribute("userId") != null){
            userId=Integer.parseInt(session.getAttribute("userId").toString());
        }
       
        tendererId = briefcaseDoc.getTendererIdFromUserId(userId);
        String logUserId = "0";
        if (session.getAttribute("userId") != null) {
        logUserId = session.getAttribute("userId").toString();
        }
        briefcaseDoc.setLogUserId(logUserId);
       TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");

       
%>

        
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%--<jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>--%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">
                                            Common Documents Library
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('6');">Save as PDF</a></span>
                                        </div>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->                
                <div>&nbsp;</div>
            <%
            if(request.getParameter("fq")!=null){
            %>
            <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
            <%
            }if(request.getParameter("fs")!=null){
            %>
            <div class="responseMsg errorMsg">
                Max FileSize <%=request.getParameter("fs")%>MB allowed,Please try again.
            </div>
            <%
            }if(request.getParameter("sf")!=null){
            %>
            <div class="responseMsg errorMsg">
                File already exist.
            </div>
            <%
            }
            if(request.getParameter("fup")!=null){
            %>
            <div class="responseMsg errorMsg">
                Problem has been occurred during file upload.
            </div>
            <% }
            if(request.getParameter("ft")!=null){
            %>
            <div class="responseMsg errorMsg">
                FileType <%=request.getParameter("ft")%> allowed,Please try again.
            </div>
            <%}%>
            <%
            if(request.getParameter("msg")!=null && "hashfail".equalsIgnoreCase(request.getParameter("msg"))){
            %>
            <div class="responseMsg errorMsg">
                Hash/e-Signature of this document could not be generated. Please try again.
            </div>
            <%}%>
            
                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                    <form id="frmFileUp" method="post" enctype="multipart/form-data"  action="<%=request.getContextPath()%>/FileUploadServlet?funName=brifcasedoc">
                    <tr>
                        <input type="hidden"  name="funName"  id="funName"  value="brifcasedoc" />
                        <td colspan="2" align="right" class="">Field marked (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">Select a file to upload : <span>*</span></td>
                        <td width="85%"><input name="fileUpload" id="fileUpload" onchange="checkfile();" type="file" class="formTxtBox_1" style="width:250px; background-color: white; " />
                            <br/><span id="fileerror"></span></td>
                    </tr>
                    <tr>
                        <td class="ff">Description :<span>*</span></td>
                        <td class="txt_2"><input name="documentBrief" id="txtDescription" type="text" class="formTxtBox_1" id="textfield62" size="50" onkeyup="checkfile();" style="width:200px;" />
                            <span id="descriptionerror"></span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td  class="t-align-left">
                            <label class="formBtn_1"><input type="submit" name="button" id="button" value="Upload" onclick="return checkfile()"/></label>
                            <input type="hidden"  name="tendererId"  id="hidtendererId"  value="<%=tendererId%>" />
                        </td>
                    </tr>
                           </form>
                </table>
                        <form id="form1" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th  class="t-align-center">Instructions</th>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Upload the documents in black/white resolution  with 75-100 DPI only, unless higher DPI is required.</td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Advised to extract and verify the contents of the zipped files to avoid disqualifications.</td>
                                </tr>
                                <tr>
                                    <!-- Edited 
                                    <td class="t-align-left">Authenticity and validity of the uploaded documents and Content of the uploaded documents remains with the tenderer. Failure to upload authentic document may result in the violation of PPA-2006 and PPR-2008 and the tenderer will be responsible for all consequences</td>
                                    -->
                                    <td class="t-align-left">Authenticity and validity of the uploaded documents and Content of the uploaded documents remains with the bidder. Failure to upload authentic document may result in the violation of the Procurement Rules and the bidder will be responsible for all consequences</td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">
                                        Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory">(<%=tblConfigurationMaster.getAllowedExtension().replace(",", ",  ") %>)</span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Click on <span class="mandatory">MAP</span> link available in front of uploaded document to map it with a <span class="mandatory">Folder</span></td>
                                </tr>
                            </table>
                        
                    <table width="100%" cellspacing="0" class="t_space">
                    <tr>
                        <td colspan="4">
                            <ul class="tabPanel_2">
                                <li id="fetchunmapped" ><a href="#label" class="sMenu" id="0" onclick="checkSatus('unmap');" >View Unmapped Files</a></li>
                                <li id="fetchdata"><a href="#label" id="1" onclick="checkSatus('folderwise');">View Folderwise Files</a></li>
                                <li id="fetchalldata"><a href="#label" id="2" onclick="checkSatus('allfile');">View Files</a></li>
                                <li id="fetcharchivefile"><a href="#label" id="3" onclick="checkSatus('archivefile');">View Archive Files</a></li>
                            </ul>
                            <%--<div class="tabPanelArea_1">--%>
                            <div class="tabPanelArea_1" >
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                    <tr id="tr1" style="display:none">
                                        <%--<td id="td1" class="t-align-left ff"></td>--%>
                                        <td id="td2" class="t-align-right ff">
                                        </td>
                                    </tr>
                                </table>
                                        <table width="100%" cellspacing="0" cellpadding="0" class="t_space">
                                    <tr>
                                        <td>
                                            <div id="jqGrid" class="" >
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </table>
                    </form>
                <!--Page Content End-->
                </table>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <form id="formstyle" action="" method="post" name="formstyle">

                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="UploadDocs_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="UploadDocs" />
                </form>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
            </div>
            </div>
        </div>
        
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDocLib");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    </html>
<form id="form2" method="post"></form>
    <script type="text/javascript">

        function downloadFile(fileName,fileLen){
          document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName="+fileName+"&fileLen="+fileLen;
          document.getElementById("form2").submit();
        }
        function forUnMappedFile(){
            document.getElementById("0").className="sMenu";
            document.getElementById("1").className="";
            document.getElementById("2").className="";
            document.getElementById("3").className="";
            $('#tr1').hide();
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=GetDocumentsWithPaging&type=unmap",
                datatype: "xml",
                //height: 710,
                height: 'auto',
                colNames:['Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:10,sortable:false,align:'center',search: false},
                    {name:'fileName',index:'documentName', width:70,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'fileDescription',index:'documentBrief', width:60,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'docHash',index:'docHash', width:130,search: false,sortable: true,align:'center'},
                    {name:'fileSize',index:'documentSize', width:30,align:'center',search: false,sortable: true},
    //                {name:'maptofolder',index:'maptofolder', width:50,align:'center',search: false,sortable: true},
                    {name:'uploaddate',index:'uploadedDate', width:60,align:'center',search: false,sortable: true},
                    {name:'action',index:'action', width:70,sortable:false,align:'center',search: false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
            }).navGrid('#page',{edit:false,add:false,del:false});
           // alert($("#jqGrid").html());
        }

        function forFolderWiseFile(){
            document.getElementById("1").className="sMenu";
            document.getElementById("0").className="";
            document.getElementById("2").className="";
            document.getElementById("3").className="";
            $('#tr1').hide();
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=getfoldersWithPaging&type=folderwise",
                datatype: "xml",
                height: 'auto',
                colNames:['Sl. <br/> No.','Folder Name','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:10,sortable:false,align:'center',search: false},
                    {name:'folderName',index:'folderName', width:90,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'action',index:'action', width:100,sortable:false,align:'center',search: false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
            }).navGrid('#page',{edit:false,add:false,del:false});

        }

        function forAllFile(){
        //alert('allFiles');
            document.getElementById("2").className="sMenu";
            document.getElementById("1").className="";
            document.getElementById("0").className="";
            document.getElementById("3").className="";
             $('#tr1').hide();
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=GetAllDocumentsWithPaging&type=unmap",
                datatype: "xml",
                height: 'auto',
                colNames:['Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Folder Name','Date and Time','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:10,sortable:false,align:'center',search: false},
                    {name:'fileName',index:'documentName', width:80,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'fileDescription',index:'documentBrief', width:60,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'docHash',index:'docHash', width:170,search: false,sortable: true,align:'center'},
                    {name:'fileSize',index:'documentSize', width:35,align:'center',search: false,sortable: true},
    //                {name:'maptofolder',index:'maptofolder', width:50,search: false,sortable:false,align:'center'},
                    {name:'foldername',index:'folderName', width:50,align:'center',search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'uploaddate',index:'uploadedDate', width:70,align:'center',search: false},
                    {name:'action',index:'action', width:80,align:'center',search: false,sortable:false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
            }).navGrid('#page',{edit:false,add:false,del:false});

        }

        function forArchive(){
            document.getElementById("3").className="sMenu";
            document.getElementById("1").className="";
            document.getElementById("2").className="";
            document.getElementById("0").className="";
            $('#tr1').hide();
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=GetArchiveDocumentsWithPaging&type=archivefile",
                datatype: "xml",
                height: 'auto',
                colNames:['Sl. <br/> No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:10,sortable:false,align:'center',search: false},
                    {name:'fileName',index:'documentName', width:100,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'fileDescription',index:'documentBrief', width:70,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'docHash',index:'docHash', width:150,search: false,sortable: true,align:'center'},
                    {name:'fileSize',index:'documentSize', width:35,align:'center',search: false,sortable: true},
                    {name:'uploaddate',index:'uploadedDate', width:60,align:'center',search: false,sortable: true},
                    {name:'action',index:'action', width:40,sortable:false,align:'center',search: false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
            }).navGrid('#page',{edit:false,add:false,del:false});

        }

        function checkSatus(type){
            if("unmap" == type ){
                document.getElementById("0").className = "sMenu";
                document.getElementById("1").className="";
                document.getElementById("2").className="";
                document.getElementById("3").className="";
                forUnMappedFile();
            }
            else if("folderwise" == type ) {
                document.getElementById("0").className = "";
                document.getElementById("1").className="sMenu";
                document.getElementById("2").className="";
                document.getElementById("3").className="";
                forFolderWiseFile();
            }else if("allfile" == type){
                document.getElementById("0").className = "";
                document.getElementById("1").className="";
                document.getElementById("2").className="sMenu";
                document.getElementById("3").className="";
                forAllFile();
            }
            else{
                document.getElementById("0").className = "";
                document.getElementById("1").className="";
                document.getElementById("2").className="";
                document.getElementById("3").className="sMenu";
                forArchive();
            }
        }

        jQuery().ready(function (){
            $('#tr1').hide();
            forUnMappedFile();
        });

        function deleteFile(docId,fileName,redirectaction){


            <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'delete',docId:docId,fileName:fileName}, function(j){
                  // alert(j);
            });--%>
            $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                if (r == true){
                    $.ajax({
                        url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=delete&fileName="+fileName,
                        method: 'POST',
                        async: false,
                        success: function(j) {

                        }
                    });
                    jAlert("Document deleted successfully.","Document Deleted", function(RetVal) {
                            });

                    if(redirectaction == "getfoldersWithPaging"){
                       forFolderWiseFile();
                    }
                    else if(redirectaction == "GetArchiveDocumentsWithPaging"){
                        forArchive();
                    }
                    else if(redirectaction == "GetAllDocumentsWithPaging"){
                        forAllFile();
                    }else{
                        forUnMappedFile();
                    }
                }
            });

        }
         function cancelFile(docId){
             //alert(docId);
            $.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'cancel',docId:docId}, function(j){
                 //  alert(j);
            });
        }
         function archiveFile(docId,redirectaction){
             redirectaction = "GetArchiveDocumentsWithPaging";
            <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'archive',docId:docId}, function(j){
                jAlert("Document Archived."," Archive Document ", function(RetVal) {
                });
                 //  alert(j);
            });--%>

         $.alerts._show("Archive Document", 'Do you really want to archive this file', null, 'confirm', function(r) {
             if(r == true){
                 $.ajax({
                        url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=archive&",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            jAlert("Document Archived."," Archive Document ", function(RetVal) {
                            });
                        }
                    });
                    if(redirectaction == "getfoldersWithPaging"){
                        forFolderWiseFile();
                    }
                    else if(redirectaction == "GetArchiveDocumentsWithPaging"){
                        forArchive();
                    }
                    else if(redirectaction == "GetAllDocumentsWithPaging"){
                        forAllFile();
                    }else{
                        forUnMappedFile();
                    }
                }
            });

        }

        function viewFile(folderId,folderName){
           // var goBack="<a href='#' onclock='forFolderWiseFile();'  ></a>";
            //$('#jqGridHeader').html("<table id='table1'><tr><td>"+goBack+"</td></tr></table>");
            $('#tr1').show();
            var td2 = document.getElementById("td2");
            td2.innerHTML = "<a href='#' onclick='forFolderWiseFile();' class='action-button-goback' >Go back</a>";
            var title = "Folder Name : &nbsp;"+folderName;

            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery("#list").jqGrid({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=GetDocumentsWithPaging&type=unmap&folderId="+folderId,
                datatype: "xml",
                height: 'auto',
                colNames:['Sl. No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and Time','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:15,sortable:false,align:'center',search: false},
                    {name:'fileName',index:'documentName', width:80,sortable:false,search: true,searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'fileDescription',index:'documentBrief', width:60,sortable:false,search: true,searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'docHash',index:'docHash', width:150,search: false,sortable: false,align:'center'},
                    {name:'fileSize',index:'fileSize', width:30,sortable:false,align:'center',search: false},
    //                {name:'maptofolder',index:'maptofolder', width:50,sortable:false,search: false},
                    {name:'uploaddate',index:'uploaddate', width:65,sortable:false,align:'center',search: false},
                    {name:'action',index:'action', width:100,sortable:false,search: false},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:15,
                rowList:[15,30,45],
                pager: $("#page"),
                caption: "<%=title%> ",
                gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
            }).navGrid('#page',{edit:false,add:false,del:false});
        }

    </script>
