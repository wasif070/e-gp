<%--
    Document   : NegTendOfflineProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>

<%@page import="com.cptu.egp.eps.model.table.TblNegQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation Documents </title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
          <script type="text/javascript">

     $(document).ready(function() {
        $("#idfrmfinalapproveNegtender").validate({
            rules: {
                nameRemarks: { required: true,maxlength: 1000}

            },
            messages: {
                nameRemarks: { required: "<div class='reqF_1'>Please Enter Comments</div>",
                maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}
            }
        }
    );
    });


 </script>
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
   <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
   <div class="contentArea">
  <!--Dashboard Content Part Start-->
  <%
        int tenderId =0;
        int negId = 0;
        int negQueryId = 0;
        boolean flag1 = false;
        String action = "";
        String msg = "";

        if(request.getParameter("tenderId") != null){
            tenderId= Integer.parseInt(request.getParameter("tenderId"));
        }
        if(request.getParameter("negId") != null){
            negId= Integer.parseInt(request.getParameter("negId").toString());
        }

        pageContext.setAttribute("tenderId", tenderId);
        String queryText = "";

        List<TblNegQuery> queryDetails = negotiationdtbean.getQueryById(tenderId, negId, negQueryId);

        if(!queryDetails.isEmpty()){
           queryText =  queryDetails.get(0).getQueryText();

        }

        if (request.getParameter("flag") != null) {
            if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                flag1 = true;
            }
        }
        if(request.getParameter("action")!=null){
            action = request.getParameter("action");
        }
         if(flag1){
           if("approveNegotiation".equalsIgnoreCase(action)){
                msg = " Negotiation Approved Successfully";
           }
        }
   %>
  <div class="pageHead_1">
       Negotiation Documents 
       <span style="float:right;">
        <a href="<%=request.getContextPath()%>/tenderer/NegoTendererProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
       </span>
  </div>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <%pageContext.setAttribute("tab", "6");%>
  <%@include file="TendererTabPanel.jsp" %>
  <div class="tabPanelArea_1">
   <jsp:include page="EvalInnerTendererTab.jsp" >
    <jsp:param name="EvalInnerTab" value="4" />
    <jsp:param name="tenderId" value="<%=tenderId%>" />
 </jsp:include>     
  <%if(!is_debared){%>
  <div class="tabPanelArea_1">
      <form name="frmfinalapproveNegtender" id="idfrmfinalapproveNegtender" action="<%=request.getContextPath()%>/NegotiationSrBean" method="post">
      <input type="hidden" name="action" id="idaction" value="approveNegotiation" />
      <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
      <input type="hidden" name="negIdName" id="idnegIdName" value="<%=negId%>" />
      <div style="font-style: italic" class="ff" align="left">
          Fields marked with (<span class="mandatory">*</span>) are mandatory.
      </div>
        <table width="100%" cellspacing="0" class="tableList_1">
            <tr>
                <td colspan="2">
                  <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td colspan="5" >
                            <div>
                            <% if(flag1){ %>
                            <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                            <% }else{ %>
                                <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                             <% } %>
                             </div>
                        </td>
                    </tr>
                    <tr>
                        <th width="4%" class="t-align-left">Sl.  No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%

                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                for (SPTenderCommonData sptcd : tenderCS.returndata("NegotiationDocInfo", ""+negId, "negotiation")) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                  </table>
                </td>
            </tr>
            <% if (docCnt != 0){ %>
            <tr>
                <td width="10%" class="t-align-left ff">Action :</td>
                <td width="90%" class="t-align-left">
                    <input type="radio" name="nameNegDocs" id="idNegDocs" value="Accept" checked="checked">&nbsp;Accept&nbsp;&nbsp;
                    <input type="radio" name="nameNegDocs" id="idNegDocs" value="Reject">&nbsp;Reject
                </td>
            </tr>
            <tr>
                <td width="10%" class="t-align-left ff">Comments : <span class="mandatory">*</span></td>
                <td width="90%" class="t-align-left">
                    <textarea name="nameRemarks" id="idRemarks" rows="5" cols="20" class="formTxtBox_1" style="width:675px;"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                                <input name="btnSubmit" type="submit" value="Submit" />
                        </label>
                    </div>
                </td>
            </tr>
            <% } %>
        </table>
      </form>
  </div>
  <%}%>
  <div>&nbsp;</div>
  </div>
   </div>
   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center"
         <%@include file="../resources/common/Bottom.jsp" %>
   </div>
   <!--Dashboard Footer End-->
</div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>