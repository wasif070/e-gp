<%-- 
    Document   : NewSupportingDocuments
    Created on : May 21, 2016, 4:48:22 PM
    Author     : SRISTY
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblMandatoryDoc"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyDocuments"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.TempBiddingPermission"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblBiddingPermission,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%--<%@page import="com.cptu.egp.eps.model.table.TblTempBiddingPermission"%>--%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <%--<jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>--%>
    <%--<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>--%>
    <jsp:useBean id="tempCompanyDocumentsSrBean" class="com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean"/> 
    <%--<jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>--%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <%

    %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Upload Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>   

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('#btnUpload').click(function () {

                    //alert($('#uploadDocFile').val());
                    var errVal = 0;

                    $('#dvUploadFileErMsg').html('');
                    $('#dvDescpErMsg').html('');

                    if ($('#uploadDocFile').val() == "") {
                        $('#dvUploadFileErMsg').html('Please select Document')
                        errVal = 1;
                    }

                    if ($('#cmbDocType').val() == "other" && $.trim($('#docName').val()) == "") {
                        $('#dvFileNameErMsg').html('Please enter Description of Document');
                        errVal = 1;
                    }


                    if (errVal == 1)
                        return false;
                });

                $("#cmbDocType").change(function () {
                    $(this).find("option:selected").each(function () {
                        if ($(this).attr("value") === "other") {
                            $("#docNameRow").show();
                        } else {
                            $("#docName").val("");
                            $('#dvFileNameErMsg').html("");
                            $("#docNameRow").hide();
                        }
                    });
                }).change();

            });

            $(function () {
                $('#cmbDocType').change(function () {
                    var cmb = document.getElementById('cmbDocType');
                    $('#hdndocBrief').val(cmb.options[cmb.selectedIndex].text);
                });
            });

            $(function () {
                $('#frmUploadDoc').submit(function () {
                    if ($('#frmUploadDoc').valid()) {
                        $('.err').remove();
                        var count = 0;
                        var browserName = ""
                        var maxSize = parseInt($('#fileSize').val()) * 1024 * 1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function (i, val) {
                            browserName += i;
                        });
                        $(":input[type='file']").each(function () {
                            if (browserName.indexOf("mozilla", 0) != -1) {
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            } else {
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if (parseInt(actSize) == 0) {
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if (fileName.indexOf("&", "0") != -1 || fileName.indexOf("%", "0") != -1) {
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if (parseInt(parseInt(maxSize) - parseInt(actSize)) < 0) {
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed " + $('#fileSize').val() + " MB. </div>");
                                count++;
                            }
                        });
                        if (count == 0) {
                            $('#btnUpload').attr("disabled", "disabled");
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
            });


        </script>
    </head>
    <body>
        <%            tempCompanyDocumentsSrBean.setLogUserId(session.getAttribute("userId").toString());
        %>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->                
                <div class="pageHead_1">Request for New Procurement Category - Upload Document</div>
                <div class="stepWiz_1 t_space">
                    <ul>
                        <% String pageName = request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1];
                        %>
                        <%--<li <%if (pageName.equals("EditCompanyDetail.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditCompanyDetail.jsp")) {%><a href="EditCompanyDetail.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Details<%if (!pageName.equals("EditCompanyDetail.jsp")) {%></a><%}%></li>
                        <li <%if (pageName.equals("EditPersonalDetails.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditPersonalDetails.jsp")) {%><a href="EditPersonalDetails.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Contact Person Details<%if (!pageName.equals("EditPersonalDetails.jsp")) {%></a><%}%></li>
                        --%>                        
                        <li <%if (pageName.equals("Reregistration.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("Reregistration.jsp")) {%><a href="Reregistration.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Request for New Procurement Category<%if (!pageName.equals("Reregistration.jsp")) {%></a><%}%></li>
                        <li <%if (pageName.equals("NewSupportingDocuments.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("NewSupportingDocuments.jsp")) {%><a href="NewSupportingDocuments.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Upload Supporting Documents<%if (!pageName.equals("NewSupportingDocuments.jsp")) {%></a><%}%></li>
                    </ul>
                </div>


                <%
                    if (request.getParameter("fq") != null) {
                %>
                <br/><div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                <%
                    }
                    if (request.getParameter("fs") != null) {
                %>
                <br/><div class="responseMsg errorMsg">
                    Max file size of a single file must not exceed <%=request.getParameter("fs")%> MB.
                </div>
                <%
                    }
                    if (request.getParameter("ft") != null) {
                %>
                <br/><div class="responseMsg errorMsg">
                    Acceptable file types <%=request.getParameter("ft")%>.
                </div>                                            
                <%
                    }
                    if (request.getParameter("sf") != null) {
                %>
                <br/><div class="responseMsg errorMsg">
                    File already exist. Please rename the New File Name.
                </div>
                <%
                    }
                    if ("fail".equals(request.getParameter("msg"))) {
                %>
                <br/><div class="responseMsg errorMsg">
                    Problem in uploading Document
                </div>
                <%
                    }
                    String style = "";
                    int btnShow = 0;
                    String msg = "";
                    StringBuffer sb = new StringBuffer();
                    //int tendererId = Integer.parseInt(tempCompanyDocumentsSrBean.checkJVCA(session.getAttribute("userId").toString()));
                    int tendererId = Integer.parseInt(request.getParameter("tId").toString());
                    int companyId = Integer.parseInt(request.getParameter("cId").toString());
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    List<TblBiddingPermission> pendingBiddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId")), "isReapplied", Operation_enum.EQ, Integer.parseInt("1"));//false
                    java.util.List<TblCompanyDocuments> documentsList = new java.util.ArrayList<TblCompanyDocuments>();
                    documentsList = tempCompanyDocumentsSrBean.getTblCompanyDocumentses(tendererId);
                    int cnt = documentsList.size();
                    boolean FileExists = true;
                    String FileNotExistsString = "";
                    for (TblCompanyDocuments documents : documentsList) {
                        if (!briefcaseDoc.isRegisDocumentExistsOrNot(documents.getDocumentName(), session.getAttribute("userId").toString())) {
                            FileNotExistsString = FileNotExistsString + " " + documents.getDocumentName() + ",";
                            if (FileExists) {
                                FileExists = false;
                            }
                        }
                    }
                    UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
//                    List<String> pageList = service.pageNavigationList(Integer.parseInt(request.getSession().getAttribute("userId").toString()));//
                    boolean showListofDoc = false;
//                    if (pageList.size() == 3) {
//                        for (String pages : pageList) {
//                            if (pages.trim().startsWith("I")) {
//                                showListofDoc = true;
//                            }
//                        }
//                    }
                    List<TblCompanyDocuments> reapplyDocumentList=service.findBidderDocuments("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(tendererId), "isReapplied", Operation_enum.EQ, 1);
                    List<TblMandatoryDoc> list = service.getMandatoryDocs(session.getAttribute("userId").toString(), false, true);
                    int cnt1 = 0;
                    int totalCnt = 0;
                    int mandDocs = 0;
                    for (TblMandatoryDoc doc : list) {
                        totalCnt++;
                        boolean bool = true;
                        if (doc.getIsMandatory().equals("yes")) {
                            cnt1++;
                        }
                        for (TblCompanyDocuments documents : documentsList) {
                            if (documents.getDocumentTypeId().equals(doc.getDocTypeValue())) {
                                bool = false;
                            }
                            if (documents.getDocumentTypeId().equals(doc.getDocTypeValue()) && doc.getIsMandatory().equals("yes")) {
                                mandDocs++;
                            }
                        }
                        if (bool) {
                            sb.append("<option value='" + doc.getDocTypeValue() + "'>" + doc.getDocType() + "</option>");
                            btnShow++;
                        }
                    }

                    CheckExtension ext = new CheckExtension();
                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("tenderer");
                    byte fileSize = configurationMaster.getFileSize();
                %>

                <%if (btnShow != 0) {%> 
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/FileUploadServlet" enctype="multipart/form-data" name="frmUploadDoc"><%-- onsubmit="startProgress()"--%>
                    <%
                        if ("Click here for Re-apply".equals(request.getParameter("btnReapply"))) {
                            String update = service.updateCompanyDocumentsStatus(request.getParameter("tId").toString());
                            if(update.equalsIgnoreCase("OK")){
                                style = "none";
                    %>
                        <br/><div id="succMsg" class="responseMsg successMsg"  >Bidder's Re-apply Process Submitted Successfully.</div>
                    <%} else if (update.equalsIgnoreCase("NOT OK")) {%>
                        <br/><div class="responseMsg errorMsg">Error Occurred. Please try again later.</div>
                    <%}} %>
                    <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                        <tr>
                            <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>

                        <% if (!showListofDoc) {%>
                        <tr>
                            <td colspan="2">
                                <div>

                                    <h2><span style="color:#DAA520;">List of Documents</span></h2>
                                    <hr/>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th class="t-align-center" width="50%">Mandatory Documents</th>
                                            <th class="t-align-center" width="50%">Optional Documents</th>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Self Declaration </b><span style="color:red">*</span><i> (For Company Owner)</i></td>
                                            <td class="t-align-left"><b>Company Registration No</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Citizenship Identity Card</b> <span style="color:red">*</span><i> (For Company Contact Person)</i></td>
                                            <td class="t-align-left"><b>Tax Payment No. (TPN)</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Power Of Attorney</b><span style="color:red">*</span><i> (For Company's Authorized User)</i></td>
                                            <td class="t-align-left"><b>Statutory Certificate No</b></td>
                                        </tr>
                                         <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Trade License</b><span style="color:red">*</span><i> (For Goods, Works and Service)</i></td>
                                            <td class="t-align-left"><b>Others/More</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Construction Development Board (CDB) Registration Certificate</b><span style="color:red">*</span><i> (For Works Only)</i></td>
                                            <td class="t-align-left"><b></b></td>
                                        </tr>

                                    </table>

                                    
                                </div>

                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <% if (pendingBiddingPermisionList.size() != 0 && reapplyDocumentList.size() == 0) {%>
                        <tr>
                            <td class="ff" width="16%">Document Type : <span>*</span></td>
                            <td width="84%">
                                <select name="documentType" class="formTxtBox_1" id="cmbDocType">                                                
                                    <%=sb.toString()%>
                                </select> <br/>All documents listed above are mandatory except ones labeled as "(If applicable)". "If applicable" indicates optional document.<!--Document listed in combo are mandatory, except ones labeled (if applicable).-->                                            
                                <input type="hidden" value="<%=tendererId%>" name="tendererId" id="hdnTendererId"/>                                            
                                <input type="hidden" name="documentBrief" id="hdndocBrief"/>
                                <input type="hidden" name="funName" value="supportdocreapply" id="hdnFunction"/>


                                <script type="text/javascript">
                                    var cmb = document.getElementById('cmbDocType');
                                    $('#hdndocBrief').val(cmb.options[cmb.selectedIndex].text);
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>

                        <tr id="docNameRow" style="display:none;">
                            <td class="ff" width="16%">Document Name : <span>*</span></td>
                            <td width="84%">
                                <input type="text" name="docName" id="docName" class="formTxtBox_1" />
                                <div id="dvFileNameErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>

                        <tr>
                            <td class="ff" width="16%">Document Ref No :</td>
                            <td width="84%">
                                <input type="text" name="docRefNo" id="docRefNo" class="formTxtBox_1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>




                        <tr>
                            <td class="ff" width="11%"> Select Document   : <span>*</span></td>
                            <td width="89%"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/><br/>
                                Acceptable File Types <span class="formNoteTxt">(<%out.print(configurationMaster.getAllowedExtension().replace(",", ", "));%>)</span>
                                <br/>Maximum file size of single file should not exceed <%out.print(fileSize);%>MB.
                                <input type="hidden" value="<%=fileSize%>" id="fileSize"/>
                                <div id="dvUploadFileErMsg" class='reqF_1'></div>


                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>

                        <tr>
                            <td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<%
                                            //java.util.List<TblCompanyDocuments> documentsList = new java.util.ArrayList<TblCompanyDocuments>();
                                            
                                            //Below one is not working for redirection
                                            if ("Next".equals(request.getParameter("btnNext"))) {
                                                if (documentsList.isEmpty()) {
                                                    msg = "Please upload atleast one document.";
                                                    response.sendRedirect("SupportingDocuments.jsp");
                                                } else {
                                                    response.sendRedirect("Authenticate.jsp");
                                                }
                                            }
                                            //Ends
                                %>   --%>                                         
                                <label class="formBtn_1" style="visibility:visible;" id="lbUpload">
                                    <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                                </label>                                            
                            </td>
                        </tr>
                        <%}%>
                    </table>
                </form>  



                <%}%> 
                <%
                    if (!msg.equals("")) {
                %>
                <div class="responseMsg errorMsg"><%=msg%></div>
                <%
                    }
                    if (pendingBiddingPermisionList.size() != 0 && reapplyDocumentList.size() == 0) {
                %>
                <div class="pageHead_1"> Uploaded Documents</div>
                <div class="t_space">
                    <b><font color="Black">Total Documents Uploaded:</font> <font color="Green"><%= cnt%></font></b><br/><br/>
                    <b>Mandatory Document Upload Status: <font color="Black">Total: <%= cnt1%> </font> | <font color="Green">Uploaded: <%= mandDocs%></font> | <font color="Red">Pending: <%=cnt1 - mandDocs%></font></b>
                </div><br/>
                <div class="t_space" style="padding-left: 10px">
                    <font color="red">Submit button for Re-apply will appear automatically once all the mandatory documents are uploaded.</font>
                    <br/><br/>
                    <b><font color="red" >If require to upload latest documents then at first delete existing document from below table and upload latest document.</font></b>
                </div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th class="t-align-center" width="7%">Sl. No.</th>
                        <th class="t-align-center" width="29%">Document Name</th>
                        <th class="t-align-center" width="39%">Document Description</th>
                        <th class="t-align-center" width="10%">File Size<br/> (In KB)</th>
                        <th class="t-align-center" width="15%">Action</th>
                    </tr>
                    <%
                        int count = 1;
                        for (TblCompanyDocuments ttcd : documentsList) {
                    %>
                    <tr class="<%if (count % 2 == 0) {
                            out.print("bgColor-Green");
                        } else {
                            out.print("bgColor-white");
                        }%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-left"><%for (TblMandatoryDoc doc : list) {
                                if (doc.getDocTypeValue().equals(ttcd.getDocumentTypeId())) {
                                    out.print(doc.getDocType());
                                }
                            }%></td>
                            <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");
                            out.print(twoDForm.format(Double.parseDouble((ttcd.getDocumentSize())) / 1024));%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/SupportDocServlet?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocumentSize()%>&funName=reapplydownload" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>                                        
                            <a href="<%=request.getContextPath()%>/SupportDocServlet?docId=<%=ttcd.getCompanyDocId()%>&docName=<%=ttcd.getDocumentName()%>&funName=reapplyremove&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                    </tr>
                    <%count++;
                        }%>
                </table>                            

                <%
                    if (FileExists) {
                        if (cnt >= cnt1) {

                            {
                                //    if(true){
%><form id="frmComp" method="post" action="NewSupportingDocuments.jsp?tId=<%= tendererId%>&cId=<%=companyId%>">
                    <div class="t-align-center t_space" style="margin-top:20px;" >
                        <label class="formBtn_1" style="display: <%= style%>"> <input type="submit" name="btnReapply" id="btnReapply" value="Click here for Re-apply" /></label>
                    </div>
                </form>
                <%}
                    }%>
                <%} else {
                    FileNotExistsString = FileNotExistsString.substring(0, FileNotExistsString.length() - 1);
                %>
                <table border="0" cellspacing="10" cellpadding="0" width="100%" class="t_space">
                    <tr>
                        <td align="center">
                            <span style="color: red; font-size: large"><b>Problem occurred during file upload.</b></span> </br> <b>Try to remove and again upload these files: </b> </br> <b><%=FileNotExistsString%></b>
                        </td>
                    </tr>
                </table>
                <%
                    }
                } else if(reapplyDocumentList.size() > 0){ %>
                <label style="color: red; text-align: center; padding-left: 50px; font-size: medium">Your new procurement category request is pending.</label>
                <%}else {%>
                <label style="color: red; text-align: center; padding-left: 50px; font-size: medium">Go to "My Account" menu and click on "Request for New Procurement Category" sub-menu, for Reapplying.</label>
                <% }
                %>
                <!--Page Content End-->
                </td>










                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp"/>
            </div>
        </div>
    </body>


    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

