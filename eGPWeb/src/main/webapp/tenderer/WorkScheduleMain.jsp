<%-- 
    Document   : WorkScheduleMain
    Created on : Sep 13, 2011, 4:31:58 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Work Schedule</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
            CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
            String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
            List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
            Object[] objj = null;
            if(lotObj!=null && !lotObj.isEmpty())
            {
                objj = lotObj.get(0);
                pageContext.setAttribute("lotId", objj[0].toString());
            }%>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");

            %>
            <%@include  file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                                    if ("edit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%> </div>
                    <%}
                         if ("sent".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.works.sentToTen")%></div>
                    <%}

                         if ("noedit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.var.success")%></div>
                    <%}
                         if ("changed".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg errorMsg'><%=bdl.getString("CMS.var.Nochange")%></div>
                    <%}

                                }
                    %>
                    <div>

                        <%
                                    String tenderId = request.getParameter("tenderId");
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    int i = 0;
                                    int lotid = 0;

                                    boolean isTimeBased = false;
                                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                                    if("Time based".equalsIgnoreCase(serviceType.toString())){
                                        isTimeBased = true;
                                    }
                                    boolean allew = true;
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        if(flags){
                        %>

                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%">Package No.</td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>Package Description</td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                            </table>

                                

                                <%
                                                lotid = Integer.parseInt(lotList.getFieldName5().toString());
                                            %>
                                
                                
                                <%
                                            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                            List<Object[]> serviceForms = cmss.getAllFormsForService(Integer.parseInt(tenderId));
                                %>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td colspan="4">
                                            <span style="float: left; text-align: left;font-weight:bold;"><b><a href="StaffInputTotal.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>">Total Staff Input Report</a></b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="3%" class="t_align_left">Sr.No</th>
                                        <th  width="67%" class="t_align_left">Forms</th>
                                        <th  width="20%" class="t_align_left">Action</th>
                                        
                                    </tr>

                                    <% int serivceSrNO = 1;
                                                List<Object> data = null;
                                                for (Object[] obj : serviceForms) {%>
                                    <tr>
                                        <td class="t_align_left"><%=serivceSrNO%></td>
                                        <td class="t_align_left"><%=obj[0]%></td>
                                        <%
                                                                                        data = cmss.dataSucssDump(Integer.parseInt(tenderId), (Integer) obj[1]);
                                                                                        if (data != null && !data.isEmpty()) {
                                                                                            int srvBoqId = Integer.parseInt(obj[2].toString());
                                                                                            if (srvBoqId == 19) {
                                         %>
                                        <td style=""> <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewWorkServiceHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&srvBoqId=<%=obj[2]%>&Type=Edit">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 12) {%>
                                        <td style="">
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                             <% if(!isTimeBased){ %>
                                            | <a href="ViewPSHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                            <% } %>
                                            &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 18) {%>
                                        <td style=""><a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewHistoryForCC.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 13) {%>
                                        <td style=""> <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewTCHistoryTenSide.jsp?tenderId=<%=tenderId%>&lotId=<%= lotid%>&formMapId=<%=data.get(0)%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 14) {%>
                                        <td style=""> <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewSsHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&srvBoqId=<%=obj[2]%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 15) {%>
                                        <td style=""> <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 16) {%>
                                        <td style=""><a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewHistoryForRE.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% } else if (srvBoqId == 20) {
                                            SpGeteGPCmsDataMore spcsdm = (SpGeteGPCmsDataMore)AppContext.getSpringBean("SpGeteGPCmsDataMore");
                                            List<SPCommonSearchDataMore> listBidderId = spcsdm.executeProcedure("getNOADetailWithTenderId", tenderId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
                                            String bidderId = "";
                                            if(listBidderId!=null && !listBidderId.isEmpty()){
                                                bidderId = listBidderId.get(0).getFieldName1();
                                            }
                                        %>
                                        <td><a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        | 
                                            <a view='link' onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/ViewBOQForms.jsp?formId=<%=obj[1] %>&userId=<%=bidderId%>&nture=Services&tenId=<%=tenderId%>&ws=t', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>
                                        </td>
                                        <% } else { %>
                                        <td style="">-</td>
                                        <%}%>
                                        
                                    </tr>
                                    <%serivceSrNO++;

                                                    }
                                                }
                                    %>
                                </table>

                                <%
                                 List<TblCmsVariationOrder> ifVari =  cmss.getListOfVariationOrderForSrv(Integer.parseInt(tenderId));
                                int ii =1;
                                int ij =0;
                                if(!ifVari.isEmpty()){

    %>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <th class="t_align_center">Sl. No.</th>
                                    <th class="t_align_center">Variation Order</th>
                                    <th class="t_align_center">Creation Date</th>
                                    <th class="t_align_center">Status</th>
                                    <th class="t_align_center">Action</th>

<%for(TblCmsVariationOrder order : ifVari){%>
<tr>
<td class="t_align_left"><%=ii%></td>
<td class="t_align_left"><%=order.getVariationNo()%></td>
<td class="t_align_left"><%=DateUtils.gridDateToStrWithoutSec(order.getVariOrdCreationDt())%></td>
<td class="t_align_left"><%if(order.getVariOrdStatus().equalsIgnoreCase("sendtoten")){out.print("Sent to Consultant");}else if(order.getVariOrdStatus().equalsIgnoreCase("finalise")){out.print("Finalized");}else{out.print((String.valueOf(Character.toUpperCase(order.getVariOrdStatus().charAt(0))).concat(order.getVariOrdStatus().substring(1))).replace('s', 'z')) ;} %></td>
<td class="t_align_left">
    <%if(ifVari.get(ij).getVariOrdStatus().equalsIgnoreCase("accepted")){%>
    <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ij).getVariOrdId()%>">View</a>
    <%}else if(ifVari.get(ij).getVariOrdStatus().equalsIgnoreCase("sendtoten")){%>
    <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ij).getVariOrdId()%>&flag=sendtoten">View and Accept Variation order</a>
    <%}%>

</td>
</tr>
<%ii++;ij++;
}%></table><%}%>

                        </form>
                        <%
                                }else{
                                    allew = false;
                                }
                            }
                            if(!allew){
                        %>
                            <div class="responseMsg noticeMsg">Date configuration is pending</div>
                        <%
                            }
                        %>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function Edit(wpId,tenderId,lotId){

       
            dynamicFromSubmit("ViewDatesForServices.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");
      
        }
        function view(wpId,tenderId,lotId){
        
            dynamicFromSubmit("ViewDatesForServices.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
       
        }
        function vari(wpId,tenderId,lotId){
            dynamicFromSubmit("VariationOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
