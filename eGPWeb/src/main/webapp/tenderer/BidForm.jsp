<%--
    Document   : BidForm
    Created on : Nov 16, 2010, 5:18:59 PM
    Author     : Sanjay,rishita
--%>

<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderFormService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.SignerImpl" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/border-radius.css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/lang/en.js"></script>
        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

    <body>
         <div id="encrypt-dialog-message" title="Instruction" style="display:none;">
             <p style="color: red;">
                Please note, when you click the "Encrypt" button below, the form will be encrypted with e-GP Tender Opening Committee Security Password &amp;
                hence you are requested to be sure that there will not be any further corrections in this form.
                Click the "Encrypt" button only if you have completed filling the form, and you are sure that the information entered
                is correct
            </p>
            </div>
            <style type="text/css">
                .formStyle_1 .ff {text-align:left}
            </style>
            <script type="text/javascript">
//            var combo_val = [];
//            var combo_val_tableid = [];
            $(function() {
                $( "#encrypt-dialog-message" ).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 500,
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            var btnencrypt = document.getElementById("encrypt");
                            if(true){ //encryptBData(btnencrypt)
                                document.getElementById("action").value = "Encrypt And Save";
                                document.getElementById("frmBidSubmit").submit();
                            }else{
                                jAlert("Encryption not successful. Please try again.", "Data Encryption", function(retVal){
                                    if(retVal){
                                        btnencrypt.style.display = "block";
// Added By Dohatec to resolve online tracking id 2012 Start
                                        btnencrypt.disabled = true;
                                        document.getElementById("lblEncrypt").className = "formBtn_disabled";
// Added By Dohatec to resolve online tracking id 2012 End
                                        return false;
                                    }else{
                                        return false;
                                    }
                                });
                            }
                        },
                        Cancel: function() {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        </script>


        <!--Dohatec Start -->
        <script type="text/javascript">

            $(document).ready(function(){

                var totalColumn = $('#FormMatrix th').length;
                var totalRow = $('#FormMatrix tr').length;  // Row Number = totalRow - 3;
                if($('#ProcurementTypeValue').val() == 'ICT' && ($('#hdnEncrypt').val() != 'Encrypt' && $('#hdnEncrypt').val() != 'View') && $('#hdnEncrypt').val() != 'Edit')
                {
                    findSelectedValue(totalRow - 3, $('#hdnTblNum').val());
                }
                else if ($('#hdnSBDCurrency').val()=='Yes' && $('#hdnEncrypt').val() != 'Encrypt' && $('#hdnEncrypt').val() != 'View' && $('#hdnEncrypt').val() != 'Edit')
                {
                    findSelectedValueSBDCurrency(totalRow - 3, $('#hdnTblNum').val());
                }
            });

            function txtReadOnly(obj)
            {
                var ddlId = obj.id;
                var splitId = ddlId.split('_');
                var rowNum = splitId[1].replace(' ', '');
                var colNum = splitId[2].replace(' ', '');
                var num = splitId[0].replace("idcombodetail", "").replace(' ', '');

                if(colNum == '7')
                {
                    if($("#"+ddlId).val() == '1')    //  value = 1 = Group C
                    {
                        jAlert('You have selected Group C. So fields of Group B will be unavailable.');

                        //  Group B Start
                        $('#row'+num+'_'+ rowNum +'_9').val('0');
                        $('#row'+num+'_'+ rowNum +'_9').attr('readonly', 'readonly');

                        //  If Group C is selected, BTN will be selected
                        var valBDT = $('#idcombodetail'+num+'_'+ rowNum +'_10 option:contains(' + "'BTN'" + ')').val(); //  Find the value of dropdown where text is BTN
                        
                        $('#idcombodetail'+num+'_'+ rowNum +'_10').val(valBDT);
                        $('#idcombodetail'+num+'_'+ rowNum +'_10').attr('disabled', 'disabled');

                        $('#row'+num+'_'+ rowNum +'_11').val('0');
                        $('#row'+num+'_'+ rowNum +'_11').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_14').val('0');
                        $('#row'+num+'_'+ rowNum +'_14').attr('readonly', 'readonly');
                        //  Group B End

                        //  Group C Start
                        $('#row'+num+'_'+ rowNum +'_12').val('');
                        $('#row'+num+'_'+ rowNum +'_12').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_13').val('');
                        $('#row'+num+'_'+ rowNum +'_13').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_15').val('');
                        $('#row'+num+'_'+ rowNum +'_15').removeAttr('readonly');
                        //  Group C End
                    }
                    else if($("#"+ddlId).val() == '2')   //  value = 2 = Group B
                    {
                        jAlert('You have selected Group B. So fields of Group C will be unavailable.');

                        //  Group C Start
                        $('#row'+num+'_'+ rowNum +'_12').val('0');
                        $('#row'+num+'_'+ rowNum +'_12').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_13').val('0');
                        $('#row'+num+'_'+ rowNum +'_13').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_15').val('0');
                        $('#row'+num+'_'+ rowNum +'_15').attr('readonly', 'readonly');
                        //  Group C End

                        //  Group B Start
                        $('#row'+num+'_'+ rowNum +'_9').val('');
                        $('#row'+num+'_'+ rowNum +'_9').removeAttr('readonly');

                        $('#idcombodetail'+num+'_'+ rowNum +'_10').removeAttr('disabled');

                        $('#row'+num+'_'+ rowNum +'_11').val('');
                        $('#row'+num+'_'+ rowNum +'_11').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_14').val('');
                        $('#row'+num+'_'+ rowNum +'_14').removeAttr('readonly');
                        //  Group B End
                    }
                }
            }
            function txtReadOnlySBDCurrency(obj)
            {
                var ddlId = obj.id;
                var splitId = ddlId.split('_');
                var rowNum = splitId[1].replace(' ', '');
                var colNum = splitId[2].replace(' ', '');
                var num = splitId[0].replace("idcombodetail", "").replace(' ', '');

                if(colNum == '7')
                {
                    if($("#"+ddlId).val() == '1')    //  value = 1 = Group C
                    {
                        jAlert('You have selected Group C. So fields of Group B will be unavailable.');

                        //  Group B Start
                        $('#row'+num+'_'+ rowNum +'_9').val('0');
                        $('#row'+num+'_'+ rowNum +'_9').attr('readonly', 'readonly');

                        //  If Group C is selected, BTN will be selected
                        var valBDT = $('#idcombodetail'+num+'_'+ rowNum +'_10 option:contains(' + "'BTN'" + ')').val(); //  Find the value of dropdown where text is BTN
                        
                        $('#idcombodetail'+num+'_'+ rowNum +'_10').val(valBDT);
                        $('#idcombodetail'+num+'_'+ rowNum +'_10').attr('disabled', 'disabled');
                        $('#row'+num+'_'+ rowNum +'_10').val(valBDT);

                        $('#row'+num+'_'+ rowNum +'_12').val('0');
                        $('#row'+num+'_'+ rowNum +'_12').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_15').val('0');
                        $('#row'+num+'_'+ rowNum +'_15').attr('readonly', 'readonly');
                        //  Group B End

                        //  Group C Start
                        $('#row'+num+'_'+ rowNum +'_13').val('');
                        $('#row'+num+'_'+ rowNum +'_13').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_14').val('');
                        $('#row'+num+'_'+ rowNum +'_14').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_16').val('');
                        $('#row'+num+'_'+ rowNum +'_16').removeAttr('readonly');
                        //  Group C End
                    }
                    else if($("#"+ddlId).val() == '2')   //  value = 2 = Group B
                    {
                        jAlert('You have selected Group B. So fields of Group C will be unavailable.');

                        //  Group C Start
                        $('#row'+num+'_'+ rowNum +'_13').val('0');
                        $('#row'+num+'_'+ rowNum +'_13').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_14').val('0');
                        $('#row'+num+'_'+ rowNum +'_14').attr('readonly', 'readonly');

                        $('#row'+num+'_'+ rowNum +'_16').val('0');
                        $('#row'+num+'_'+ rowNum +'_16').attr('readonly', 'readonly');
                        //  Group C End

                        //  Group B Start
                        $('#row'+num+'_'+ rowNum +'_9').val('');
                        $('#row'+num+'_'+ rowNum +'_9').removeAttr('readonly');

                        $('#idcombodetail'+num+'_'+ rowNum +'_10').removeAttr('disabled');

                        $('#row'+num+'_'+ rowNum +'_12').val('');
                        $('#row'+num+'_'+ rowNum +'_12').removeAttr('readonly');

                        $('#row'+num+'_'+ rowNum +'_15').val('');
                        $('#row'+num+'_'+ rowNum +'_15').removeAttr('readonly');
                        //  Group B End
                    }
                    for(var j =1;j<18;j++)
                    {
                          var currency = $('#idcombodetail'+num+'_'+ rowNum +'_'+j+' option:contains(' + "'BTN'" + ')').val();

                     //   alert(currency);
                        if (currency == '2'){
                             $('#row'+num+'_'+rowNum+'_'+(j+1)).val('1');
                             $('#row'+num+'_'+rowNum+'_'+(j+1)).attr('readonly', true);
                        }
                    }
                }
                
            }

            function findSelectedValue(rowNumber, tblNum)
            {
               
                for(var i = 1; i<=rowNumber; i++)
                {
                   
                    if($('#idcombodetail'+tblNum+'_'+i+'_7').val() == '1')    //  value = 1 = Group C
                    {

                        $('#row'+tblNum+'_'+i+'_9').val('0');
                        $('#row'+tblNum+'_'+i+'_9').attr('readonly', 'readonly');

                        $('#idcombodetail'+tblNum+'_'+i+'_10').attr('disabled', 'disabled');

                        $('#row'+tblNum+'_'+i+'_11').val('0');
                        $('#row'+tblNum+'_'+i+'_11').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_14').val('0');
                        $('#row'+tblNum+'_'+i+'_14').attr('readonly', 'readonly');
                    }
                    else if($('#idcombodetail'+tblNum+'_'+i+'_7').val() == '2')    //  value = 2 = Group B
                    {
                        $('#row'+tblNum+'_'+i+'_12').val('0');
                        $('#row'+tblNum+'_'+i+'_12').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_13').val('0');
                        $('#row'+tblNum+'_'+i+'_13').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_15').val('0');
                        $('#row'+tblNum+'_'+i+'_15').attr('readonly', 'readonly');
                    }
                }
            }
            function findSelectedValueSBDCurrency(rowNumber, tblNum)
            {
               
                for(var i = 1; i<=rowNumber; i++)
                {
                   
                    for(var j =1;j<18;j++)
                    {
                        /*
                         * Bug fixed by MD. Emtazul Haque on 16/April/2018 
                         * Problem: If any field contains value 2 then next field automatically becomes 1.
                         * Checking for text as well as value added and that solves the problem.
                        */
                        if ( $('#row'+tblNum+'_'+i+'_'+j).val()==2 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").val()==2 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").text()=='BTN' ){
                             $('#row'+tblNum+'_'+i+'_'+(j+1)).val('1');
                             $('#row'+tblNum+'_'+i+'_'+(j+1)).attr('readonly', true);
                         }
                    }
                    if($('#idcombodetail'+tblNum+'_'+i+'_7').val() == '1')    //  value = 1 = Group C
                    {

                        $('#row'+tblNum+'_'+i+'_9').val('0');
                        $('#row'+tblNum+'_'+i+'_9').attr('readonly', 'readonly');

                        $('#idcombodetail'+tblNum+'_'+i+'_10').attr('disabled', 'disabled');

                        $('#row'+tblNum+'_'+i+'_12').val('0');
                        $('#row'+tblNum+'_'+i+'_12').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_15').val('0');
                        $('#row'+tblNum+'_'+i+'_15').attr('readonly', 'readonly');
                    }
                    else if($('#idcombodetail'+tblNum+'_'+i+'_7').val() == '2')    //  value = 2 = Group B
                    {
                        $('#row'+tblNum+'_'+i+'_13').val('0');
                        $('#row'+tblNum+'_'+i+'_13').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_14').val('0');
                        $('#row'+tblNum+'_'+i+'_14').attr('readonly', 'readonly');

                        $('#row'+tblNum+'_'+i+'_16').val('0');
                        $('#row'+tblNum+'_'+i+'_16').attr('readonly', 'readonly');
                    }
                }
            }


        </script>
        <!--Dohatec End -->
        <script language="javascript" type="text/javascript">
            function openDialog(){
                 $("#encrypt-dialog-message").dialog( "open");
                 return false;
            }
        </script>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">
                <script type="text/javascript">
                    var verified = true;
                    //var SignerAPI;
//                    $(function() {
//                        SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
//                    });
                    function GetCal(txtname,controlname,tableId,obj,tableIndex)
                    {
                        new Calendar({
                            inputField: txtname,
                            trigger: controlname,
                            showTime: 24,
                            dateFormat:"%d-%b-%Y",
                            onSelect: function() {
                                var date = Calendar.intToDate(this.selection.get());
                                LEFT_CAL.args.min = date;
                                LEFT_CAL.redraw();
                                this.hide();
                                document.getElementById(txtname).focus();
                                CheckDate(tableId,obj,txtname,txtname,tableIndex);
                            }
                        });

                        var LEFT_CAL = Calendar.setup({
                            weekNumbers: false
                        })

                    }
                    //alert(SignerAPI);
                </script>
                    <!--Middle Content Table Start-->
<%
                String actionName = null;
                String hashKey = null;
                if(request.getParameter("hdnPwd")!=null)
                    hashKey = request.getParameter("hdnPwd");


                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }
                /*int userTypeId = 0;
                if(session.getAttribute("userTypeId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                        userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                }*/
                int formId = 0;
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }

                int bidId = 0;
                if (request.getParameter("bidId") != null) {
                    bidId = Integer.parseInt(request.getParameter("bidId"));
                }

                int lotId = 0;
                if (request.getParameter("lotId") != null) {
                    lotId = Integer.parseInt(request.getParameter("lotId"));
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
                     action = request.getParameter("action");
                }

                int parentLink = 0;
                if(request.getParameter("parentLink")!= null){
                    parentLink = Integer.parseInt(request.getParameter("parentLink"));
                }

                boolean isEdit = false;
                if("Edit".equalsIgnoreCase(action)){
                    isEdit = true;
                }

                boolean isView = false;
                if("View".equalsIgnoreCase(action)){
                    isView = true;
                }

                boolean isEncrypt = false;
                if("Encrypt".equalsIgnoreCase(action)){
                    isEncrypt = true;
                }

                boolean isSigned = false;
                if(hashKey != null){
                    isSigned = true;
                }

                /* Code added by rokibul for ICT vat message start */
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                TenderCommonService tendercommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                commonService.setUserId(session.getAttribute("userId").toString());
                %>
                <input type="hidden" name="ProcurementTypeValue" id="ProcurementTypeValue" value="<%=commonService.getProcurementType(request.getParameter("tenderId")).toString().toUpperCase()%>">
                <input type="hidden" name="ProcurementNatureValue" id="ProcurementNatureValue" value="<%=commonService.getProcNature(request.getParameter("tenderId")).toString().toUpperCase()%>">
                <input type="hidden" id="hdnSBDCurrency" name="hdnSBDCurrency" value="<%=tendercommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId")))%>">
                <%
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getLotDescriptonForDocViewMore",String.valueOf(tenderId), String.valueOf(userId));
                if(!lotList.isEmpty()) {
                    TenderFormService tenderFormService = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
                   if (!lotList.get(0).getFieldName3().equalsIgnoreCase("0") && !lotList.get(0).getFieldName3().equalsIgnoreCase("N.A."))
                        {%>
                        <input type="hidden" name="ValidFormValue" id="ValidFormValue" value="<%=tenderFormService.getPriceBidFormsICT(Integer.parseInt(lotList.get(0).getFieldName3()), formId)%>">
                        <%}
                       }
                /* Code added by rokibul for ICT vat message end*/

                int tableCount = 0;
                tableCount = tenderBidSrBean.getNoOfTable(formId);

                if(isView) // Making Audit Trail Entry for Operation is View
                {
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType="tenderId";
                    int auditId=tenderId;
                    String auditAction="Tenderer has View Forms";
                    String moduleName=EgpModule.Bid_Submission.getName();
                    String remarks="Tenderer(User id): "+session.getAttribute("userId")+" has View Form id:"+request.getParameter("formId")+" & Bid Id: "+request.getParameter("bidId");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                }


                if("".equalsIgnoreCase(action)){
%>
<script>
        var gblCnt =0;
        var isMultiTable = false;
        var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
        var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
        var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
        var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
        var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
        var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
        var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
        var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
        var brokenFormulaIds = new Array();
        var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
        var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
        var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
        var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
        var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
        var arrDataTypesforCell = new Array(<%=tableCount%>);
        var arrColTotalIds = new Array(<%=tableCount%>);
        var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
        var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
        var isColTotalforTable = new Array(<%=tableCount%>);
        for(var i=0;i<isColTotalforTable.length;i++)
                isColTotalforTable[i]=0;
        var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table

        var arrColFillBy=new Array(<%=tableCount%>); // Stores the Array of Column having fill by Tenderer/PE/Auto
</script>
<%
                    }
                    if(isEdit || isView || isEncrypt){

                        tableCount = tenderBidSrBean.getNoOfTable(formId);
%>
<script>
		verified = false;
                var gblCnt =0;
                chkEdit = true;
                var isMultiTable = false;
                var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
                var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
                var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
                var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
                var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
                var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
                var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
                var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
                var brokenFormulaIds = new Array();
                var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
                var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
                var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
                var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
                var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
                var arrDataTypesforCell = new Array(<%=tableCount%>);
                var arrColTotalIds = new Array(<%=tableCount%>);
                var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
                var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
                var isColTotalforTable = new Array(<%=tableCount%>);
                var totalBidTable = new Array(<%=tableCount%>);
                for(var i=0;i<isColTotalforTable.length;i++)
                        isColTotalforTable[i]=0;
                var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
                var arrColFillBy=new Array(<%=tableCount%>); // Stores the Array of Column having fill by Tenderer/PE/Auto

</script>
<%
                    }
                
                if((!isView && !isEdit && !isEncrypt)|| isSigned){
                    actionName = "GetBidData.jsp";
                }else{
                    actionName = "BidForm.jsp?tenderId="+tenderId+"&formId="+formId+"&bidId="+bidId+"&action="+action+"&parentLink="+parentLink;
                    }



%>
                <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="<%=actionName%>">


                    <script type="text/javascript">
//                        deployJava.runApplet(
//                            {
//                                codebase:"<%=request.getContextPath()%>/",
//                                archive:"Signer.jar",
//                                code:"com.cptu.egp.eps.SignerAPI.class",
//                                width:"0",
//                                Height:"0",
//                                ID: "SignerAPI",
//                                classloader_cache: "false"
//                            },
//                            null,
//                            "1.6"
//                        );
                    </script>


                    <%if(isEdit || isView || isEncrypt){%>
                    <input type="hidden" name="hdnBidId" id="hdnBidId" value="<%=bidId%>">
                    <%}%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
<%
        String formName = "";
        String formHeader = "";
        String formFooter = "";
        String isMultipleFormFeeling = "";
        String isEncryption = "";
        String isPriceBid = "";
        String isMandatory = "";

        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(formId);
        for(CommonFormData formData : formDetails){
            formName = formData.getFormName();
            formHeader = formData.getFormHeader();
            formFooter = formData.getFormFooter();
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            isEncryption = formData.getIsEncryption();
            isPriceBid = formData.getIsPriceBid();
            isMandatory = formData.getIsMandatory();
        }
%>
                                <div class="t_space">
                                <div class="pageHead_1" id="divFormName">
                                    <%=formName%>
                                    <span style="float: right; text-align: right;">
<%
        if(lotId==0){
%>
                                        <a class="action-button-goback" href="LotTendPrep.jsp?tab=4&tenderId=<%= tenderId %>" title="Bid Dashboard">Go Back To Dashboard</a>
<%
        }else{
%>
                                        <a class="action-button-goback" href="BidPreperation.jsp?tab=4&lotId=<%=lotId%>&tenderId=<%= tenderId %>" title="Bid Dashboard">Go Back To Dashboard</a>
<%
        }
%>
                                    </span>
                                </div>

                            <!--<div align="right"><strong>In Text box - 1000 Characters are allowed.</strong></div>
                                <div align="right"><strong>In Text area - 5000 Characters are allowed.</strong></div>-->
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <%if("err".equalsIgnoreCase(request.getParameter("eMessage"))){%>
                                    <tr>
                                        <td>
                                            <div class="responseMsg errorMsg">
                                                There is a Problem in Form Submission. Please delete this form and fill once again.
                                            </div>
                                        </td>
                                    </tr>
                                <%}%>
                                    <tr>
                                        <td>
<%
        if(isEncrypt)
        {
%>
                                            <input type="hidden" name="hdnBuyerPwd" id="hdnBuyerPwd" value="<%=tenderBidSrBean.getBuyerPwdHash(tenderId)%>">
<%
        }
%>
                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="hdnFormId" id="hdnFormId" value="<%=formId%>">
                                            <input type="hidden" name="hdnLotId" id="hdnLotId" value="<%=lotId%>">
                                        <!-- Dohatec Start -->
                                            <input type="hidden" id="hdnEncrypt" name="hdnEncrypt" value="<%=request.getParameter("action")%>">
                                            <input type="hidden" id="hdnSBDCurrency" name="hdnSBDCurrency" value="<%=tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId")))%>">
                                        <!-- Dohatec End -->
<%
        int tableId = 0;
        int tblCnt1 = 0;
        short cols = 0;
        short rows = 0;
        String tableName = "";
        String tableHeader = "";
        String tableFooter = "";

        String isMultiTable = "";

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();
            tableHeader = formData.getTableHeader();
            tableFooter = formData.getTableFooter();
            List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
            if (tableInfo != null) {
                if (tableInfo.size() >= 0) {
                    tableName = tableInfo.get(0).getTableName();

                    cols = tableInfo.get(0).getNoOfCols();
                    rows = tableInfo.get(0).getNoOfRows();
                    isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                }
                tableInfo = null;
            }

            cols = (tenderBidSrBean.getNoOfColsInTable(tableId)).shortValue();
            rows = (tenderBidSrBean.getNoOfRowsInTable(tableId, (short) 1)).shortValue();

%>
<script>
        chkBidTableId.push(<%=tableId%>);
        arrBidCount.push(1);
        // Setting TableId in Array
        arr[<%=tblCnt1%>]=<%=tableId%>;
        // Setting TableIdwise No of Rows in Array
        arrRow[<%=tblCnt1%>]=<%=rows%>;
        //alert('tablecnt : <%=tblCnt1%>')
        //alert('<%=rows%>');
        arrRowsKey[<%=tableId%>]=<%=rows%>;
        // Setting TableIdwise No of Cols in Array
        arrCol[<%=tblCnt1%>]=<%=cols%>;
        arrColsKey[<%=tableId%>]=<%=cols%>;
        // Setting TableIdwise No of Tables in Added
        arrTableAdded[<%=tblCnt1%>]=<%=1%>;
        arrTableAddedKey[<%=tableId%>]=<%=1%>;
        arrColTotalIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColTotalWordsIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColOriValIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        for(var i=0;i<arrColTotalIds[<%=tblCnt1%>].length;i++){
                arrColTotalIds[<%=tblCnt1%>][i] = 0;
                arrColTotalWordsIds[<%=tblCnt1%>][i] = 0;
                arrColOriValIds[<%=tblCnt1%>][i] = 0;
        }
</script>
                                                <div id="divMsg" class="responseMsg successMsg" style="display:none">&nbsp;</div>
                                                <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                            <%
                                                    if("yes".equalsIgnoreCase(isMultiTable) || !"".equalsIgnoreCase(tableName)){
                                            %>
                                                    <tr>
                                                        <td width="50%" class="ff" colspan="2"><%=tableName%></td>
                                                        <td class="t-align-right" >
                                            <%
                                                    if(("yes".equalsIgnoreCase(isMultiTable)) && (!"List of Drawings".equalsIgnoreCase(tableName))){
                                            %>
                                                            <script>
                                                                isMultiTable = true;
                                                            </script>
                                                            <label <%if(isEdit){%>class="formBtn_disabled"<%}else if(!(isEdit || isView || isEncrypt)){%>class="bidFormBtn_1"<%}%> style="float: right" id="lblDelTable<%=tableId%>"
                                                            <%if(isView){%>style="display:none" <%}%>>
                                                                <input type="button" name="btnDel<%=tableId%>" id="bttnDel<%=tableId%>" value="Delete Record" onClick="DelTable(this.form,<%=tableId%>,this)"
                                                                       <%if(isEdit){%>
                                                                       disabled="true"
                                                                       <%}%>
                                                                       <%if(isView || isEncrypt){%>style="display:none" <%}%>
                                                                       />
                                                            </label>
                                                            <label <%if(isEdit){%>class="formBtn_disabled"<%}else if(!(isEdit || isView || isEncrypt)){%>class="bidFormBtn_1"<%}%> style="float: right" id="lblAddTable<%=tableId%>"
                                                            <%if(isView){%>style="display:none" <%}%>>
                                                                <input type="button" name="btn<%=tableId%>" id="bttn<%=tableId%>" value="Add Record" onClick="AddBidTable(this.form,<%=tableId%>,this)"
                                                                       <%if(isEdit){%>
                                                                       disabled="true"
                                                                       <%}%>
                                                                       <%if(isView || isEncrypt){%>style="display:none" <%}%>
                                                                       />
                                                            </label>
                                                            <input type="hidden" name="delTableIds<%=tableId%>" id="delTableIds<%=tableId%>" value="" />
                                                        <%
                                                                }
                                                        %>
                                                        </td>
                                                    </tr>
                                                        <%
                                                                }
                                                        %>
                                                    <% if(!"".equals(tableHeader)){ %>
                                                    <tr>
                                                        <td width="100%" class="ff" colspan="3" style="line-height: 1.75"><%=tableHeader%></td>
                                                    </tr>
                                                    <% }  %>
                                                </table>
                                                <jsp:include page="BidFormTable.jsp" flush="true">
                                                    <jsp:param name="tableId" value="<%=tableId%>" />
                                                    <jsp:param name="formId" value="<%=formId %>" />
                                                    <jsp:param name="cols" value="<%=cols%>" />
                                                    <jsp:param name="rows" value="<%=rows%>" />
                                                    <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                    <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                    <jsp:param name="tableName" value="<%=tableName%>" />
                                                     <jsp:param name="hashKey" value="<%=hashKey%>" />
                                                </jsp:include>
                                                <% if(!"".equals(tableFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">
                                                         <tr>
                                                             <td width="100%" colspan="3" class="ff" style="line-height: 1.75"><%=tableFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  %>
                                                   <% if(!"".equals(formFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">
                                                         <tr>
                                                             <td width="100%" class="ff" colspan="3" style="line-height: 1.75"><%=formFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  %>

<script>
        var totalInWordAt = -1;
        try{
            totalInWordAt = TotalWordColId
            if(totalInWordAt < 0){
                totalInWordAt = -1;
            }
        }catch(err){
            totalInWordAt = -1;
        }
</script>
<%if(tblCnt1 >= 0){%>
<script>
        if(arrStaticColIds[<%=tblCnt1%>].length > 0)
	{
            for(var j=0;j<arr.length;j++)
            {
                LoopCounter++;
            }
	}
        breakFormulas(arrDataTypesforCell);
        checkForFunctions();
</script>
<%}%>
<%
            tblCnt1++;
            formData = null;
        }
%>
<script>
    breakFormulas(arrDataTypesforCell);
    checkForFunctions();
</script>

                                            <% if("Encrypt".equals(action)){ %>
                                            <table width="100%" cellspacing="0" cellpadding="0" class="formStyle_1 t_space">
                                                <tr>
                                                    <td colspan="2" style="color: red;">
                                                        Please note, when you click the "Encrypt" button below, the form will be encrypted with e-GP Tender Opening Committee Security Password &amp; hence you are requested to be sure that there will not be any further correction in this form. Click the "Encrypt" button only if you have completed filling the form, and you are sure that the information entered is correct.
                                                    </td>
                                                </tr>
                                            </table>
                                            <% } %>
                                            <table width="100%" cellspacing="0" cellpadding="0" class="formStyle_1 t_space" >
                                                <tr style="color: red;">
                                                    <td>&nbsp;</td>
                                                    <%if(isEdit){
                                                        if("yes".equalsIgnoreCase(isEncryption)){%>
                                                      
                                                        <td align="left">
                                                         <span id="msgId">Please click on 'Decrypt' button to decrypt the form</span>
                                                        </td>
                                                        <% }else { %>
                                                    <td align="left">
                                                         <span id="msgId">Please click on 'Verify' button to verify the form</span>
                                                    </td>
                                                    <%} } else if(isView){
                                                        if("yes".equalsIgnoreCase(isEncryption)){
                                                            if(!isSigned){%>
                                                        <td align="left">
                                                            <span id="msgId">Please click on 'Decrypt' button to decrypt the form</span>
                                                        </td>
                                                        <%}}
                                                    }else if(isEncrypt){%>
                                                    <td align="left">
                                                         <span id="msgId">Please click on 'Decrypt' button to decrypt the form</span>
                                                    </td>
                                                    <% } else {%>
                                                    <td align="left">
                                                         <span id="msgId">Please click on 'Sign' button to sign the form</span>
                                                    </td>
                                                    <% } %>
                                                </tr>
                                                <tr >
                                                    <td>&nbsp;</td>
                                                    <td align="left" width="90%" >
<%
    if("yes".equalsIgnoreCase(isEncryption)){ // Price Bid Form
        if(!(isEdit || isView || isEncrypt)){ // Fill Form
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
                                                        <label class="bidFormBtn_1" id="lblSign">
                                                            <input type="button" name="sign" id="sign" value="Sign"  onclick="return SignNVerify();">
                                                        </label>
<!--                                                        <label class="bidFormBtn_1" id="lblSignWithPk">
                                                            <input type="button" name="signWithPk" id="signWithPk" value="Sign with PKI"  onclick="alert('This functionality is under Development','Bid Submission');">
                                                        </label>-->
                                                            <!--bhutan-n disabled-->
<!--                                                        <label class="formBtn_disabled" id="lblEncrypt">
                                                            <input disabled="true" type="button" name="encrypt" id="encrypt" value="Encrypt" onclick="return encryptData(1);">
                                                        </label>-->
<!--bhutan-n disabled!-->
                                                        <label class="formBtn_disabled" id="lblSave">
                                                            <input disabled="true" type="submit" name="save" id="save" value="Save" onclick="return chkValidate(this);">
                                                        </label>
                                                        </div>
<%
        }else if(isEdit){
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
                                                        <label class="bidFormBtn_1" id="lblSign" style="display:none;">
                                                            <input type="button" name="sign" id="sign" value="Sign"  onclick="return SignNVerify();" style="display:none;">
                                                        </label>
<!--					      <label class="bidFormBtn_1" id="lblSignWithPk">
                                                            <input type="button" name="signWithPk" id="signWithPk" value="Sign with PKI"  onclick="alert('This functionality is under Development','Bid Submission');return false;">
                                                        </label>-->
                                                        <label class="formBtn_disabled" id="lblVerify">
                                                            <input type="button" name="verify" id="verify" value="Verify" disabled="true" onClick="return verifyData();"/>
                                                        </label>
                                                        <label class="bidFormBtn_1" id="lblDecrypt">
                                                            <input type="button" name="decrypt" id="decrypt" value="Decrypt" onClick="return decryptNVerify()"/>
                                                        </label>
                                                        <label class="formBtn_disabled" id="lblEncrypt" style="display:none;">
                                                            <input disabled="true" type="button" name="encrypt" id="encrypt" value="Encrypt"  onclick="return encryptData(2);" style="display:none;">
                                                        </label>
                                                        <label class="formBtn_disabled" id="lblSave">
                                                            <input disabled="true" type="submit" name="save" id="save" value="Update" onclick="return chkValidate(this);">
                                                        </label>
                                                        </div>
<%
        } else if(isView && !isSigned){
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
<!--                                                            <label class="bidFormBtn_1" id="lblSign">
                                                                <input type="button" name="sign" id="sign" value="Sign"  onclick="return decryptNView();">
                                                            </label>-->

                                                        <label class="bidFormBtn_1" id="lblDecrypt">
                                                            <input  type="button" name="decrypt" id="decrypt" value="Decrypt" onClick="return decryptNView();"/> <!--return chkValidate(this) -->
                                                        </label>
                                                        </div>
<%
        }else if(isEncrypt){
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
                                                        <label class="bidFormBtn_1" id="lblDecrypt">
                                                            <input type="button" name="decrypt" id="decrypt" value="Decrypt" onClick="return decryptNEncrypt()"/>
                                                        </label>
<!--                                                        <label class="bidFormBtn_1" id="lblSignWithPk">
                                                            <input type="button" name="signWithPk" id="signWithPk" value="Sign with PKI"  onclick="alert('This functionality is under Development','Bid Submission');">
                                                        </label>-->
                                                        <label class="formBtn_disabled" id="lblEncrypt" style="display:none;">
                                                            <input type="hidden" name="action" id="action" value="" />
                                                            <input type="submit" name="encrypt" id="encrypt" value="Encrypt And Save"  onclick="return openDialog();" style="display:none;">
                                                        </label>
                                                        </div>
<%
        }

        %>
                                                <div>
                                                    <input type="hidden" name="isEncryption" id="isEncryption" value="<%=isEncryption%>"/>
                                                    <input type="hidden" name="isEdit" id="isEdit" value="<%=isEdit%>"/>
                                                    <input type="hidden" name="isView" id="isView" value="<%=isView%>"/>
                                                    <input type="hidden" name="isEncrypt" id="isEncrypt" value="<%=isEncrypt%>"/>
                                                    <input type="hidden" name="isSigned" id="isSigned" value="<%=isSigned%>"/>
                                                </div>

        <%



    }else{ // Technical Form
         if(!(isEdit || isView || isEncrypt)){
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
                                                        <label class="bidFormBtn_1" id="lblSign">
                                                            <input type="button" name="sign" id="sign" value="Sign"  onclick="return SignVerify(1);">
                                                        </label>
<!--					      <label class="bidFormBtn_1" id="lblSignWithPk">
                                                            <input type="button" name="signWithPk" id="signWithPk" value="Sign with PKI"  onclick="alert('This functionality is under Development','Bid Submission');">
                                                        </label>-->
                                                        <label class="formBtn_disabled" id="lblSave">
                                                            <input disabled="true" type="submit" name="save" id="save" value="Save" onclick="return chkValidate(this);">
                                                        </label>
                                                        </div>
<%
        }else if(isEdit){
%>
                                                        <div  class="btnContainer t_space" style="width: 90%">
                                                        <label class="bidFormBtn_1" id="lblVerify">
                                                            <input type="button" name="verify" id="verify" value="Verify" onClick="return verifyFormData();"/>
                                                        </label>
<!--					      <label class="bidFormBtn_1" id="lblSignWithPk">
                                                            <input type="button" name="signWithPk" id="signWithPk" value="Sign with PKI"  onclick="jAlert('This functionality is under Development','Bid Submission');">
                                                        </label>-->
                                                        <label class="formBtn_disabled" id="lblSign">
                                                            <input type="button" name="sign" id="sign" value="Sign"  onclick="return SignVerify(2);" disabled="true">
                                                        </label>
                                                        <label class="formBtn_disabled" id="lblSave">
                                                            <input disabled="true" type="submit" name="save" id="save" value="Update" onclick="return chkValidate(this);">
                                                        </label>
                                                        </div>
<%
        }
    }
%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="10%" class="ff">e-Signature / Hash of Document : </td>
                                                    <td align="left" width="90%">
                                                        <textarea  name="signedData" cols="90" readonly="readOnly" id="signedData" width="100%" class="formTxtBox_1"><%
                                                        if(isEdit || isView || isEncrypt)
                                                        {
                                                            out.print(tenderBidSrBean.getBidSign(bidId, formId, userId));
                                                        }
%></textarea>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                 <div>
                    <input type="hidden" name="hdnPwd" id="hdnPwd" value=""/>
                </div>
                </form>
                </div>
                <div id="dialog-form" title="Enter Password">
<!--                    <form>-->
                            <label for="password">Password : </label>
                            <input type="password" name="password" id="password" value="" class="formTxtBox_1" autocomplete="off" />
                            <br/>
                            <p align="center" class="validateTips"></p>
<!--                    </form>-->
                </div>
                
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>

    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<script>
$( "#dialog:ui-dialog" ).dialog( "destroy" );

var password = $( "#password" ),
    allFields = $( [] ).add( password ),
    tips = $( ".validateTips" );

var check = false;
var encryptFlag = false;
function updateTips( t ) {
 if(t.length > 0 && t != 'Done' )
 {
    tips
            .text( t )
            .addClass( "ui-state-highlight" );
    setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
 }
}

function chkValidate(obj){
    if($('#frmBidSubmit').valid()){
        //obj.disabled = true;
        $('#save').hide();
        return true;
    }
}

function checkLength( o, n, min, max ) {
        if(o.val()==""){
            o.addClass( "ui-state-error" );
            updateTips("Please enter password");
        }else{
            if ( o.val().length > max || o.val().length < min ) {
                    o.addClass( "ui-state-error" );
                    updateTips( "Length of " + n + " must be between " +
                            min + " and " + max + "." );
                    return false;
            } else {
                    return true;
            }
        }
}

function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
        } else {
                return true;
        }
}

$("#dialog-form").dialog({
        autoOpen: false,
        height: 180,
        width: 280,
        modal: true,
        resizable: false,
        position: 'center',
        buttons: {

                "Verify Password": function() {

                        var bValid = true;
                        var isEncryption = document.getElementById("isEncryption").value;
                        var isEdit = document.getElementById("isEdit").value.toLowerCase().trim() == "true"?true:false;
                        var isView = document.getElementById("isView").value.toLowerCase().trim() == "true"?true:false;
                        var isEncrypt = document.getElementById("isEncrypt").value.toLowerCase().trim() == "true"?true:false;
                        var isSigned = document.getElementById("isSigned").value.toLowerCase().trim() == "true"?true:false;
                        
                        allFields.removeClass( "ui-state-error" );
                        bValid = bValid && checkLength( password, "password", 8, 25 );

                        if (bValid) {
                            if(document.getElementById("password").value!=""){
                                $.post("<%=request.getContextPath()%>/LoginSrBean", {param1:$('#password').val(),param2:'<%=userId%>',funName:'verifyPWD'},  function(j){
                                    if(j.charAt(0)=="1"){
                                        //document.getElementById("hdnEncrypData").value = j.toString().substring(2, j.toString().length);
                                        document.getElementById("hdnPwd").value = j.toString().substring(2, j.toString().length);
                                        if(check){
                                            generateString();
                                            if(document.getElementById("signedData")!=null)
                                                document.getElementById("signedData").value = sha1Hash(document.getElementById("signedData").value);
                                            if(isEdit){
                                                if(document.getElementById("encrypt")!=null)
                                                    document.getElementById("encrypt").style.display = "none";
                                                if(document.getElementById("lblEncrypt")!=null){
                                                    document.getElementById("lblEncrypt").style.display = "none";
                                                }
                                            }else{
                                                if(document.getElementById("encrypt")!=null)
                                                    document.getElementById("encrypt").disabled = false;
                                                if(document.getElementById("lblEncrypt")!=null){
                                                    document.getElementById("lblEncrypt").className = "bidFormBtn_1";
                                                    document.getElementById("msgId").innerHTML = "Please click on 'Encrypt' button to encrypt the form";
                                                }
                                            }

                                            if(document.getElementById("sign")!=null)
                                                document.getElementById("sign").disabled = true;
                                            if(document.getElementById("lblSign")!=null)
                                                document.getElementById("lblSign").className = "formBtn_disabled";

                                            verified = false;
                                            if(document.getElementById("divMsg")!=null){
                                                document.getElementById("divMsg").innerHTML = "Formed signed successfully";
                                            }

                                            //bhutan-new
                                            
                                            if(isEncryption.toLowerCase() == "yes")
                                            { // Price Bid Form
                                                if(!(isEdit || isView || isEncrypt))
                                                { // Fill Form
                                                        
                                                        readyData(1);
                                                }
                                                if(isEdit)
                                                {
                                                    
                                                        readyData(2);
                                                }

                                            }

                                            //bhutan-new


                                        }
                                        else
                                        {
                                            //alert(isSigned);
                                            if(isView){
                                                //decryptData();
                                                document.getElementById("decrypt").disabled = false;
                                                document.getElementById("lblDecrypt").className = "bidFormBtn_1";
                                                document.getElementById("msgId").innerHTML="Please click on 'Decrypt' button to decrypt the form"
                                                if(document.getElementById("sign")!=null)
                                                    document.getElementById("sign").disabled = true;
                                                if(document.getElementById("lblSign")!=null)
                                                    document.getElementById("lblSign").className = "formBtn_disabled";

                                                

                                             }else if(isEdit){
                                                 if(document.getElementById("decrypt")!=null)
                                                        document.getElementById("decrypt").style.display ="none";
                                                    if(document.getElementById("msgId").innerHTML=="Please click on 'Decrypt' button to decrypt the form"){
                                                        document.getElementById("msgId").innerHTML = "";
                                                    }
                                                    if(document.getElementById("lblDecrypt")!=null)
                                                        document.getElementById("lblDecrypt").style.display ="none";
                                                    

                                                    if(document.getElementById("encrypt")!=null)
                                                        document.getElementById("encrypt").style.display ="block";
                                                    if(document.getElementById("lblEncrypt")!=null)
                                                        document.getElementById("lblEncrypt").style.display ="block";
                                                    if(document.getElementById("verify")!=null)
                                                        document.getElementById("verify").disabled = false;
                                                    if(document.getElementById("lblVerify")!=null){
                                                        document.getElementById("lblVerify").className = "bidFormBtn_1";
                                                        document.getElementById("msgId").innerHTML = "Please click on 'Verify' button to verify the form";
                                                    }

                                                    if(document.getElementById("sign")!=null)
                                                        document.getElementById("sign").disabled = true;
                                                    if(document.getElementById("lblSign")!=null)
                                                        document.getElementById("lblSign").className = "formBtn_disabled";
                                                    if(document.getElementById("divMsg")!=null){
                                                        document.getElementById("divMsg").innerHTML = "Form Decrypted successfully.";
                                                        document.getElementById("divMsg").style.display = "block";
                                                    }
                                             }else if(isEncrypt){
                                                 document.getElementById("decrypt").disabled = false;
                                                document.getElementById("lblDecrypt").className = "bidFormBtn_1";

                                                if(document.getElementById("decrypt")!=null)
                                                    document.getElementById("decrypt").style.display ="none";
                                                if(document.getElementById("lblDecrypt")!=null)
                                                    document.getElementById("lblDecrypt").style.display ="none";

                                                if(document.getElementById("encrypt")!=null)
                                                    document.getElementById("encrypt").style.display ="block";
                                                if(document.getElementById("lblEncrypt")!=null)
                                                    document.getElementById("lblEncrypt").style.display ="block";
                                                if(document.getElementById("lblEncrypt")!=null){
                                                    document.getElementById("lblEncrypt").className = "bidFormBtn_1";
                                                    if(document.getElementById("msgId").innerHTML!="Please click on 'Encrypt And Save' button to save the form"){
                                                        document.getElementById("msgId").innerHTML = "Please click on 'Encrypt' button to encrypt the form";
                                                    }
                                                }
                                             }
                                             else{

                                                var decCount = decryptData();
                                                if(decCount==0){
                                                if(!encryptFlag)
                                                {
                                                   
                                                    document.getElementById("decrypt").disabled = false;
                                                    document.getElementById("lblDecrypt").className = "bidFormBtn_1";



                                                    if(document.getElementById("decrypt")!=null)
                                                        document.getElementById("decrypt").style.display ="none";
                                                    if(document.getElementById("msgId").innerHTML=="Please click on 'Decrypt' button to decrypt the form"){
                                                        document.getElementById("msgId").innerHTML = "";
                                                    }
                                                    if(document.getElementById("lblDecrypt")!=null)
                                                        document.getElementById("lblDecrypt").style.display ="none";
                                                    

                                                    if(document.getElementById("encrypt")!=null)
                                                        document.getElementById("encrypt").style.display ="block";
                                                    if(document.getElementById("lblEncrypt")!=null)
                                                        document.getElementById("lblEncrypt").style.display ="block";
                                                    if(document.getElementById("verify")!=null)
                                                        document.getElementById("verify").disabled = false;
                                                    if(document.getElementById("lblVerify")!=null){
                                                        document.getElementById("lblVerify").className = "bidFormBtn_1";
                                                        document.getElementById("msgId").innerHTML = "Please click on 'Verify' button to verify the form";
                                                    }

                                                    if(document.getElementById("sign")!=null)
                                                        document.getElementById("sign").disabled = true;
                                                    if(document.getElementById("lblSign")!=null)
                                                        document.getElementById("lblSign").className = "formBtn_disabled";
                                                    if(document.getElementById("divMsg")!=null){
                                                        document.getElementById("divMsg").innerHTML = "Form Decrypted successfully.";
                                                        document.getElementById("divMsg").style.display = "block";
                                                    }
                                                }
                                                else
                                                {
                                                    document.getElementById("decrypt").disabled = false;
                                                    document.getElementById("lblDecrypt").className = "bidFormBtn_1";

                                                    if(document.getElementById("decrypt")!=null)
                                                        document.getElementById("decrypt").style.display ="none";
                                                    if(document.getElementById("lblDecrypt")!=null)
                                                        document.getElementById("lblDecrypt").style.display ="none";
                                                    
                                                    if(document.getElementById("encrypt")!=null)
                                                        document.getElementById("encrypt").style.display ="block";
                                                    if(document.getElementById("lblEncrypt")!=null)
                                                        document.getElementById("lblEncrypt").style.display ="block";
                                                    if(document.getElementById("lblEncrypt")!=null){
                                                        document.getElementById("lblEncrypt").className = "bidFormBtn_1";
                                                        if(document.getElementById("msgId").innerHTML!="Please click on 'Encrypt And Save' button to save the form"){
                                                            document.getElementById("msgId").innerHTML = "Please click on 'Encrypt' button to encrypt the form";
                                                        }
                                                    }
                                                }
                                                updateTips("Done");
                                                }
                                            }
                                        }

                                        $("#dialog-form").dialog("close");
                                        if(!<%=isSigned%> && (isView || isEdit || isEncrypt))
                                            document.getElementById('frmBidSubmit').submit();

                                        
                                    }else{
                                        updateTips("Please enter valid password");
                                        $('#password').val() = "";
                                    }
                                });
                            }
                        }
                }
        },
        close: function() {
                allFields.val("").removeClass("ui-state-error");
        }
});


function validateVat()
{
  // Dohatec :: Condition is added for checking valid form and Procurement Type and Procurement Nature
  if((document.getElementById("ProcurementTypeValue").value=="ICT" )  && (document.getElementById("ValidFormValue").value=="Valid") && (document.getElementById("ProcurementNatureValue").value == "GOODS" ))
     {
         var alertMessage="Sales Tax amount must be calculated on the existing rate as permissible by RGOB rules.If the RGOB increases the Sales Tax rate, then the increased amount will also be calculated and deducted at source.";
         return confirm(alertMessage);
    }
    else
        { return true; }
}

function validate()
{
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    // Dohatec :: Condition is added for checking readonly fields
//                    if(document.getElementById("ProcurementNatureValue").value == "WORKS" || document.getElementById("ProcurementNatureValue").value == "GOODS" && document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value!=2)
//                    {
//                        jAlert("You have to agree to proceed further.","Title", function(retVal) {});
//                        return false;
//                    }
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value=="" && !(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly))
                    {
                        jAlert("One of the form fields is empty","Title", function(retVal) {});
                        return false;
                    }
                    
                }
            }
        }
    }
    return true;
}
function SignNVerify(){
      if(validate()){
          if(validateVat()){
        check = true;
        $("#dialog-form").dialog("open");
        $( ".validateTips" ).text("");
       // document.getElementById("msgId").innerHTML = "Please click on 'Encrypt' button to encrypt the form";
        }
    }
}

function SignVerify(obj){
    if(validate()){
        check = true;
        if(check){
            generateString();
            document.getElementById("signedData").value = sha1Hash(document.getElementById("signedData").value);
            document.getElementById("sign").disabled = true;
            document.getElementById("lblSign").className = "formBtn_disabled";
            document.getElementById("save").disabled = false;
            document.getElementById("lblSave").className = "bidFormBtn_1";
            if(obj == "1"){
                document.getElementById("msgId").innerHTML = "Please click on 'Save' button to save the form";
            }else if(obj == "2"){
                document.getElementById("msgId").innerHTML = "Please click on 'Update' button to update the form";
            }

        }else{
        }
    }
}

function decryptNVerify(){
    $("#dialog-form").dialog("open");
    //document.getElementById("msgId").innerHTML = "Please click on 'Verify' button to verify the form";
}

function decryptNEncrypt(){
    encryptFlag = true;
    $("#dialog-form").dialog("open");
    document.getElementById("msgId").innerHTML = "Please click on 'Encrypt And Save' button to save the form";
}

function decryptNView(){
    $("#dialog-form").dialog("open");
    //alert($("#dialog-form").dialog("open"));
    //document.getElementById("msgId").innerHTML = "";
}

function verifyFormData(){

    var finalRows;
    var signData = "";

    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true){
            //alert('arrBidCount : '+parseInt(arrBidCount[totalTables]));
            //alert('arrRow : '+parseInt(arrRow[totalTables]));
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
            //alert('finalRows : '+finalRows);
        }else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        signData +=  " value for New Table ";

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = false;
                    signData = trim(signData) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                }
            }
        }
    }
    signData = sha1Hash(signData);

    if(document.getElementById("signedData").value == signData){
        //alert('sanjay');
        document.getElementById("lblVerify").className = "formBtn_disabled";
        document.getElementById("verify").disabled = true;
        document.getElementById("lblSign").className = "bidFormBtn_1";
        document.getElementById("sign").disabled = false;
        <%if(isEdit){%>
            for(var totalTables=0;totalTables<arr.length;totalTables++)
            {
                    if(isMultiTable){
                        if(document.getElementById("bttn"+arr[totalTables])!=null && document.getElementById("lblAddTable"+arr[totalTables])!=null){
                            document.getElementById("bttn"+arr[totalTables]).disabled = false;
                            document.getElementById("bttnDel"+arr[totalTables]).disabled = false;
                            document.getElementById("lblAddTable"+arr[totalTables]).className = "bidFormBtn_1";
                            document.getElementById("lblDelTable"+arr[totalTables]).className = "bidFormBtn_1";
                        }
                    }
            }
        <%}%>
        jAlert("e-Signature Verified Successfully","Success", function(retVal) {});
        document.getElementById("msgId").innerHTML = "Please click on 'Sign' button to sign the form";
        //document.getElementById("msgId").innerHTML = "Please click on 'Verify' button to verify the form";
        return true;
   } else {
        jAlert("e-Signature Not Verified","Failure", function(retVal) {});
        return false;
    }
}

function verifyData(){
    var finalRows;
    var signData = "";
    var formType="Normal";

    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        var tblNum = arr[totalTables];
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        signData +=  " value for New Table ";

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            var i = rowCount;
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                var j = colCount;
                if(document.getElementById("row_FillBy"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!= null)
                {
                    formType="Discount";
                    if(document.getElementById("row_FillBy"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value="5"){
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = false;
                    }
                }
                else
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                    {
                        if ( $('#row'+tblNum+'_'+i+'_'+j).val()==2 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").val()==2 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").text()=='BTN' )
                        {
                            $('#row'+tblNum+'_'+i+'_'+(j+1)).val('1');
                            $('#row'+tblNum+'_'+i+'_'+(j+1)).attr('readonly', true);
                        }
                        else if ( $('#row'+tblNum+'_'+i+'_'+j).val()==12 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").val()==12 && $("#idcombodetail"+tblNum+"_"+i+"_"+j+" option:selected").text()=='USD' )
                        {
                            $('#row'+tblNum+'_'+i+'_'+(j+1)).attr('readonly', true);
                        }
                        else
                        {
                            document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = false;
                        }
                        signData = trim(signData) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;

                        if(document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null){
                            document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).disabled = false;
                        }
                    }
                }
            }
            
            if($('#row'+tblNum+'_'+i+'_7').val() == '1' && $("#idcombodetail"+tblNum+"_"+i+"_7 option:selected").val()==1 && $("#idcombodetail"+tblNum+"_"+i+"_7 option:selected").text()=='Goods already importated (Group C)')    //  value = 1 = Group C
            {
                $('#row'+tblNum+'_'+i+'_9').attr('readonly', 'readonly');
                $('#idcombodetail'+tblNum+'_'+i+'_10').attr('disabled', 'disabled');
                $('#row'+tblNum+'_'+i+'_12').attr('readonly', 'readonly');
                $('#row'+tblNum+'_'+i+'_15').attr('readonly', 'readonly');
            }
            else if($('#row'+tblNum+'_'+i+'_7').val() == '2' && $("#idcombodetail"+tblNum+"_"+i+"_7 option:selected").val()==2 && $("#idcombodetail"+tblNum+"_"+i+"_7 option:selected").text()=='Goods to be importated (Group B)')    //  value = 2 = Group B
            {
                $('#row'+tblNum+'_'+i+'_13').attr('readonly', 'readonly');
                $('#row'+tblNum+'_'+i+'_14').attr('readonly', 'readonly');
                $('#row'+tblNum+'_'+i+'_16').attr('readonly', 'readonly');
            }
        }
    }

    signData = sha1Hash(signData);

    if(document.getElementById("signedData").value == signData || formType=="Discount"){
        document.getElementById("verify").style.display ="none";
        document.getElementById("lblVerify").style.display ="none";
        document.getElementById("sign").style.display = "block";
        document.getElementById("lblSign").style.display = "block";
        document.getElementById("sign").disabled = false;
        document.getElementById("lblSign").className = "bidFormBtn_1";
        for(var totalTables=0;totalTables<arr.length;totalTables++)
        {
            if(isMultiTable){
                if(document.getElementById("bttn"+arr[totalTables])!=null && document.getElementById("lblAddTable"+arr[totalTables])!=null){
                    document.getElementById("bttn"+arr[totalTables]).disabled = false;
                    document.getElementById("bttnDel"+arr[totalTables]).disabled = false;
                    document.getElementById("lblAddTable"+arr[totalTables]).className = "bidFormBtn_1";
                    document.getElementById("lblDelTable"+arr[totalTables]).className = "bidFormBtn_1";
                }
            }
        }
        updateTips("");
        verified = true;
        if(document.getElementById("divMsg")!=null){
            document.getElementById("divMsg").innerHTML = "e-Signature verified successfully";
            //document.getElementById("divMsg").style.display = "block";
        }
        jAlert("e-Signature verified successfully","Success", function(retVal) {});
        document.getElementById("msgId").innerHTML = "Please click on 'Sign' button to sign the form";
        return true;
    } else {
        jAlert("e-Signature Not Verified","Failure", function(retVal) {});
        return false;
    }
}

function generateString()
{
    document.getElementById("signedData").value = "";
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(parseInt(arrRow[totalTables])!=0)
        {
            if(chkEdit == true){
                finalRows = eval(parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]));
            }else{
                finalRows = eval(parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]));
            }

            document.getElementById("signedData").value +=  " value for New Table ";

            for(var rowCount=0;rowCount<=finalRows;rowCount++)
            {
                for(var colCount=0;colCount<arrCol[totalTables];colCount++)
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                    {
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                        if(document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null){
                            document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).disabled = true;
                        }
                        document.getElementById("signedData").value = trim(document.getElementById("signedData").value) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                    }
                }
            }
        }
    }
    //alert(document.getElementById("signedData").value);
    return true;
}


//new function to eliminate client side encryption
function readyData(obj){
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);
        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    

                    //SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                    
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display == "none"){

                        if(document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null){
                            document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                        }
                        else if(document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null)
                        {
                            document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                        }

                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "block";
                    }
// Added By Dohatec to resolve online tracking id 2012 Start
                   if((document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==3)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==4)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==8)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==11)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==13))
                        {
                            if(CheckFloat1Value(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value)) {
                                //document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnPwd').val());
                                document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                            }
                            else{document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = "";
                                document.getElementById("encrypt").disabled = true;
                                document.getElementById("lblEncrypt").className = "formBtn_disabled";
                                jAlert("Encryption Error. Please fill up this form again","Faliure", function(retVal) {});
                             return false;
                             }

                        }
                        else
                            {
                            //document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnPwd').val());
                            document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;}

                }
// Added By Dohatec to resolve online tracking id 2012 End
            }
        }
    }

    //document.getElementById("encrypt").disabled = true;
    //document.getElementById("lblEncrypt").className = "formBtn_disabled";
    document.getElementById("save").disabled = false;
    document.getElementById("lblSave").className = "bidFormBtn_1";
    if(obj == "1"){
        document.getElementById("msgId").innerHTML = "Please click on 'Save' button to encrypt & save the form";
    }else if(obj == "2"){
        document.getElementById("msgId").innerHTML = "Please click on 'Update' button to encrypt & update the form";
    }
}


function encryptData(obj){
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);
        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {

                    SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display == "none"){

                        if(document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null){
                            document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                        }
                        else if(document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null)
                        {
                            document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                        }

                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "block";
                    }
// Added By Dohatec to resolve online tracking id 2012 Start
                   if((document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==3)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==4)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==8)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==11)
                       ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==13))
                        {
                            if(CheckFloat1Value(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value)) {
                                document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnPwd').val());
                                document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                            }
                            else{document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = "";
                                document.getElementById("encrypt").disabled = true;
                                document.getElementById("lblEncrypt").className = "formBtn_disabled";
                                jAlert("Encryption Error. Please fill up this form again","Faliure", function(retVal) {});
                             return false;
                             }
                            
                        }
                        else
                            { document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnPwd').val());
                            document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;}
                    
                }
// Added By Dohatec to resolve online tracking id 2012 End
            }
        }
    }

    //document.getElementById("encrypt").disabled = true;
    //document.getElementById("lblEncrypt").className = "formBtn_disabled";
    document.getElementById("save").disabled = false;
    document.getElementById("lblSave").className = "bidFormBtn_1";
    if(obj == "1"){
        document.getElementById("msgId").innerHTML = "Please click on 'Save' button to save the form";
    }else if(obj == "2"){
        document.getElementById("msgId").innerHTML = "Please click on 'Update' button to update the form";
    }

}

function encryptBData(objEncBtn){
    try{
        objEncBtn.style.display = "none";
        var finalRows;
        for(var totalTables=0;totalTables<arr.length;totalTables++)
        {
            if(chkEdit == true)
                finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
            else
                finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

            for(var rowCount=0;rowCount<=finalRows;rowCount++)
            {
                for(var colCount=0;colCount<arrCol[totalTables];colCount++)
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                    {// Added By Dohatec to resolve online tracking id 2012 Start                        
                        if((document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==3)
                        ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==4)
                        ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==8)
                        ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==11)
                        ||(document.getElementById("row_DataType"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value==13)) {
                       if(CheckFloat1Value(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value)) {
                                SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                            }
                            else { alert("Encryption Error . Please Delete and fill up this form again");
                              return false; }
                        }
                        else {
                            SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                        }
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnBuyerPwd').val());
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                     // Added By Dohatec to resolve online tracking id 2012 End
                    }
                }
            }
        }
        return true;
    }catch(e){
        return false;
    }
}

function decryptData(){
    var finalRows;
    var counter = 0;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true){
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        }else{
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);
        }

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null)
                {
                

//                    SignerAPI.setEncrypt(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
//                    var value = SignerAPI.getSymDecrypt($('#hdnPwd').val());
//                    if(value==null || value == '' || value == 'null'){
//                        counter++;
//                        break;
//                    }
//                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = value;

                    var value = document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;

                    var combo = document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1));

                    for(var i=0;i<combo.options.length;i++){
                        if(value==combo.options[i].value){
                            combo.options[i].selected = true;
                        }
                    }

                    document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "block";
                    document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).disabled = true;
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                }
                else if(document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1))!=null)
                {
//                    SignerAPI.setEncrypt(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
//                    var value = SignerAPI.getSymDecrypt($('#hdnPwd').val());
//                    if(value==null || value == '' || value == 'null'){
//                        counter++;
//                        break;
//                    }
//                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = value;

                    var value = document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                    var combo = document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1));

                    for(var i=0;i<combo.options.length;i++){
                        if(value==combo.options[i].value){
                            combo.options[i].selected = true;
                        }
                    }

                    document.getElementById("Currency"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "block";
                    document.getElementById("idcombodetail"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).disabled = true;
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).style.display = "none";
                }
                else
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                    {
                     
//                        SignerAPI.setEncrypt(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
//                        var value = SignerAPI.getSymDecrypt($('#hdnPwd').val());
//                        if(value==null || value == '' || value == 'null'){
//                            counter++;
//                            break;
//                        }
//                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = value;
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                    }
                }
            }
            if(counter!=0){
                break;
            }
        }
        if(counter!=0){
             break;
         }
    }
    if(counter!=0){
        jAlert('Data couldnot be decrypted. This could have happened because of Change of the Password. You can delete this form and can fill and Encrypt with the latest password to proceed further', 'Bid Forms',function(retVal) {});
    }
    return counter;

}
 // Added By Dohatec to resolve online tracking id 2012  start
function CheckFloat1Value(val)
{
        return !isNaN(parseFloat(val)) && isFinite(val);
}
 // Added By Dohatec to resolve online tracking id 2012  End
//for(var cnt=0;cnt<combo_val.length;cnt++)
//{
//    changeTextVal(document.getElementById(combo_val[cnt]),combo_val_tableid[cnt]);
//}



            $(document).ready(function(){

                if(<%=isSigned%> && (<%=isView%> || <%=isEdit%> || <%=isEncrypt%>) ){
                    decryptData();
                    if(document.getElementById("divMsg")!=null){
                        document.getElementById("divMsg").innerHTML = "Form decrypted successfully";
                        document.getElementById("divMsg").style.display = "block";
                    }

                    if(<%=isEdit%>){
                        if(document.getElementById("decrypt")!=null)
                             document.getElementById("decrypt").style.display ="none";
                            if(document.getElementById("msgId").innerHTML=="Please click on 'Decrypt' button to decrypt the form"){
                                document.getElementById("msgId").innerHTML = "";
                            }
                            if(document.getElementById("lblDecrypt")!=null)
                                document.getElementById("lblDecrypt").style.display ="none";


                            if(document.getElementById("encrypt")!=null)
                                document.getElementById("encrypt").style.display ="block";
                            if(document.getElementById("lblEncrypt")!=null)
                                document.getElementById("lblEncrypt").style.display ="block";
                            if(document.getElementById("verify")!=null)
                                document.getElementById("verify").disabled = false;
                            if(document.getElementById("lblVerify")!=null){
                                document.getElementById("lblVerify").className = "bidFormBtn_1";
                                document.getElementById("msgId").innerHTML = "Please click on 'Verify' button to verify the form";
                            }

                            if(document.getElementById("sign")!=null)
                                document.getElementById("sign").disabled = true;
                            if(document.getElementById("lblSign")!=null)
                                document.getElementById("lblSign").className = "formBtn_disabled";
                            if(document.getElementById("divMsg")!=null){
                                document.getElementById("divMsg").innerHTML = "Form Decrypted successfully.";
                                document.getElementById("divMsg").style.display = "block";
                            }
                    }else if(<%=isEncrypt%>){
                        
                        document.getElementById("decrypt").disabled = false;
                        document.getElementById("lblDecrypt").className = "bidFormBtn_1";

                        if(document.getElementById("decrypt")!=null)
                            document.getElementById("decrypt").style.display ="none";
                        if(document.getElementById("lblDecrypt")!=null)
                            document.getElementById("lblDecrypt").style.display ="none";

                        if(document.getElementById("encrypt")!=null)
                            document.getElementById("encrypt").style.display ="block";
                        if(document.getElementById("lblEncrypt")!=null)
                            document.getElementById("lblEncrypt").style.display ="block";
                        if(document.getElementById("lblEncrypt")!=null){
                            document.getElementById("lblEncrypt").className = "bidFormBtn_1";
                            if(document.getElementById("msgId").innerHTML!="Please click on 'Encrypt And Save' button to save the form"){
                                document.getElementById("msgId").innerHTML = "Please click on 'Encrypt And Save' button to save the form";
                            }
                        }
                       
                    }
                }

            });
</script>