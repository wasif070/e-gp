<%-- 
    Document   : InsertJVCompanyDetails
    Created on : Mar 3, 2011, 4:18:46 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
<jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head></head>
    <body>
        <%
            TblCompanyMaster dtBean = null;
            ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
             List<TblCompanyMaster> list = contentAdminService.findCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(request.getParameter("uId"))));
            if(!list.isEmpty()){
                dtBean = list.get(0);
            }
            tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
            if ("Save".equals(request.getParameter("hdnsaveNEdit"))) {
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("regOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setRegOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("regOffUpjilla"))) {
                    tempCompanyMasterDtBean.setRegOffUpjilla("");
                }
                String jvcaId = contentAdminService.jvcaID(request.getParameter("uId"));
                String jvcaName = contentAdminService.jvcaName(request.getParameter("uId"));
                tempCompanyMasterDtBean.setCompanyName(jvcaName);
//                    if (tempCompanyMasterDtBean.getCompanyRegNumber() != null) {
                        //UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        //if (userRegisterService.companyRegNoCheck(tempCompanyMasterDtBean.getCompanyRegNumber(), 0).isEmpty()) {
                         if(true) {
                            if(tendererMasterSrBean.regJVCompanyDetail(tempCompanyMasterDtBean, Integer.parseInt(request.getParameter("uId")), Integer.parseInt(jvcaId))){
                                response.sendRedirect("JVPersonalDetails.jsp?uId="+request.getParameter("uId"));
                            }else{
                                out.print("<h1>Error in Company Registration</h1>");
                            }
//                        } else {
//                            response.sendRedirect("JVCompanyDetails.jsp?cpr=y&uId="+request.getParameter("uId"));
//                        }
                    } else {
                        response.sendRedirect("JVCompanyDetails.jsp?uId="+request.getParameter("uId"));
                    }
                } else if ("Update".equals(request.getParameter("hdnsaveNEdit"))) {
                    if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("regOffSubDistrict"))) {
                        tempCompanyMasterDtBean.setRegOffSubDistrict("");
                    }
                    if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("regOffUpjilla"))) {
                        tempCompanyMasterDtBean.setRegOffUpjilla("");
                    }
                    tempCompanyMasterDtBean.setCompanyId(dtBean.getCompanyId());
                    tempCompanyMasterDtBean.setUserId(Integer.parseInt(request.getParameter("uId")));
                    if(tendererMasterSrBean.updateJVCompanyDetails(tempCompanyMasterDtBean)){
                        response.sendRedirect("JVCompanyDetails.jsp?msg=s&uId="+request.getParameter("uId"));
                    }else{
                        out.print("<h1>Error in Update</h1>");
                    }
               }
            if(dtBean!=null){
                dtBean = null;
            }
            if(tempCompanyMasterDtBean!=null){
                tempCompanyMasterDtBean = null;
            }
            if(tendererMasterSrBean!=null){
                tendererMasterSrBean = null;
            }
        %>
    </body>
</html>
