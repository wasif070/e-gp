<%--
    Document   : TenderDocPrep
    Created on : 20-Nov-2010, 12:37:32 PM
    Author     : Yagnesh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDocStatistics"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.List, com.cptu.egp.eps.model.table.TblTenderSection"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  />
<jsp:useBean id="grandSummary" class="com.cptu.egp.eps.web.servicebean.GrandSummarySrBean"  />
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Document View</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <%
            int tenderId = 0;
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }

            int tenderStdId = 0;

            int packageOrLotId = -1;
            if (request.getParameter("porlId") != null) {
                packageOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }
            String stdName = "";
            //boolean isPackage = false;
            //isPackage = true;

            // workflow Integration by Chalapathi

             int uid = 0;
             if(session.getAttribute("userId") != null){
               Integer ob1 = (Integer)session.getAttribute("userId");
               uid = ob1.intValue();
             }

             if(dDocSrBean.tendDocStatCount(String.valueOf(tenderId), String.valueOf(packageOrLotId), String.valueOf(uid))==0){
                    TblTenderDocStatistics tblTenderDocStatistics = new TblTenderDocStatistics();
                    tblTenderDocStatistics.setTenderId(tenderId);
                    tblTenderDocStatistics.setPkgLotId(packageOrLotId);
                    tblTenderDocStatistics.setUserId(uid);
                    tblTenderDocStatistics.setDownloadDt(new java.util.Date());

                    dDocSrBean.addTenderDocStatistics(tblTenderDocStatistics);
             }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <!--Page Content Start-->
            <div class="contentArea_1">
            <div class="pageHead_1">
                Tender/Proposal Document View
                <span style="float: right;" ><a class="action-button-goback" href="LotPckDocs.jsp?tenderId=<%= tenderId %>" title="Go Back">Go Back</a></span>

            </div>
                
            <%if(request.getParameter("downForm")!=null){%>
            <div id="errMsg"  class="responseMsg errorMsg" style="margin-top: 10px;">The document was not uploaded properly.</div>
            <%}%>



            <%
                pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
                if(isTenPackageWise){
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
                }else{
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId, packageOrLotId);
                }
                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
             String lotDesc = "-";
             String st_forDownloadLink = "Package";
             String lotNo="-";
              List<TblTenderLotSecurity> lots= null;
             if(isTenPackageWise == false){
                  lots =  tenderDocumentSrBean.getLotDetailsByLotId(tenderId,packageOrLotId);
                  if(lots.size()>0){
                  TblTenderLotSecurity tenderLotSecurity =  lots.get(0);
                  lotDesc = tenderLotSecurity.getLotDesc();
                  lotNo = tenderLotSecurity.getLotNo();
                  st_forDownloadLink = "Lot_"+tenderLotSecurity.getLotNo();
                  }
                 }
            %>
             <div class="t_space">
             <span class="c-alignment-right b_space">
                 <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=tenderId%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Tender/Proposal Dcoument">Download Tender/Proposal Document</a>
                </span>
             </div>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <%--<tr>
                    <td colspan="3" class="t-align-left ff">Standard Bidding Document : <%= stdName %> </td>
                </tr>--%>
                <tr>
                    <td class="t-align-left ff" style="width: 12%">
                        <%if(isTenPackageWise){ %>
                            Package No. :
                        <%}else{%>
                            Lot No. :
                        <%}%>
                    </td>
                    <td colspan="2" class="t-align-left">
                        <%
                            if(isTenPackageWise){
                                out.print(tenderDocumentSrBean.getPkgNo(String.valueOf(tenderId)));
                            }else{
                                out.print(lotNo);
                            }
                        %>
                    </td>
                </tr>
                <tr>
                    <td class="t-align-left ff">
                        <%if(isTenPackageWise){%>
                            Package Description :
                        <%}else{%>
                            Lot Description :
                        <%}%>
                    </td>
                    <td colspan="2" class="t-align-left">
                         <%if(isTenPackageWise){
                            if(packageDescription == null)
                                out.print("--");
                            else out.print(packageDescription);
                        }else {
                            out.print(lotDesc);
                           }%>
                    </td>
                </tr>
                <tr>
                    <th width="10%" class="t-align-left">Section No.</th>
                    <th width="80%" class="t-align-left">Section Name</th>
                    <th width="10%" class="t-align-left">Action</th>
                </tr>
                <%
                    int tenderittSectionId = 0;
                    List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
                    if(tSections != null){
                        if(tSections.size() > 0){
                            short j = 0;
                            for(TblTenderSection tts : tSections){
                                j++;
                                String str_FolderName = tts.getSectionName().replace("&", "^");
                                str_FolderName = "Section"+j+"_"+str_FolderName;
                                if(tts.getContentType().equals("Form") || tts.getContentType().equals("TOR")){
                                    if (packageOrLotId != -1) {%>
                <tr>
                                    <td class="t-align-center"><%= j %></td>
                                    <td colspan="2" class="t-align-left">
                                         <%= tts.getSectionName() %>
                                         <%
                                              String folderName = pdfConstant.TENDERDOC;
                                              String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                         %>
                                         <a class="action-button-savepdf"
                                             href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=j%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true">Save As PDF </a>
                                             <table width="100%" cellspacing="5" cellpadding="0" border="0" class="tableList_1">
                                            <tr>
                                                <td colspan="2" class="ff">Tender/Proposal forms</td>
                                            </tr>
                                            <tr>
                                                <td class="ff" width="15%">Grand summary : </td>
                                                <td class="t-align-left">
                                                    <%
                                                    int isGSCreatedId = -1;
                                                    isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,packageOrLotId);
                                                    if(isGSCreatedId != -1){
                                                        %>
                                                        <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId %>&sectionId=<%= tts.getTenderSectionId() %>&gsId=<%= isGSCreatedId %>&porlId=<%=packageOrLotId%>">View</a>
                                                        <%
                                                    }else{
                                                        out.println("Not Prepared");
                                                    }
                                                    %>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <th width="4%" class="t-align-left">Sl.  No.</th>
                    <th class="t-align-left" width="30%">File Name</th>
                    <th class="t-align-left" width="38%">File Description</th>
                    <th class="t-align-left" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-left" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                    String sectionId = tts.getTenderSectionId()+"";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                              <%--<%if(sptcd.getFieldName5().contains("0")){ %>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}else{%>

                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>

                                            </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="4%" style="text-align:center;">Sl. <br/> No.</th>
                                                <th width="56%">Form Name</th>
                                                <th width="40%">Actions</th>
                                            </tr>
                                            <%
                                             java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
                                            if(forms != null){
                                                if(forms.size() != 0){
                                                    for(int jj=0;jj<forms.size();jj++){
                                            %>
                                                    <tr>
                                                        <td width="4%" style="text-align:center;"><%=(jj+1)%></td>
                                                        <td><%= forms.get(jj).getFormName() %></td>
                                                        <td>
                                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId %>&sectionId=<%= tts.getTenderSectionId() %>&formId=<%= forms.get(jj).getTenderFormId() %>&porlId=<%=packageOrLotId%>">View Form</a>
                                                        </td>
                                                    </tr>
                                            <%
                                                    }
                                                }
                                              }
                                                forms = null;
                                            %>
                                        </table>
                                    </td>
                </tr>
                <%
                                    }
                                    else
                                        { %>

                                                <tr>
                                    <td class="t-align-center"><%= j %></td>
                                    <td colspan="2" class="t-align-left">
                                         <%= tts.getSectionName() %>
                                         <%
                                              String folderName = pdfConstant.TENDERDOC;
                                              String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                         %>
                                         <div align="right">
                                         <a class="action-button-savepdf"
                                             href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=j%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true">Save As PDF </a>
                                         </div>    
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <th width="4%" class="t-align-left">Sl.  No.</th>
                    <th class="t-align-left" width="30%">File Name</th>
                    <th class="t-align-left" width="38%">File Description</th>
                    <th class="t-align-left" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-left" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                    String sectionId = tts.getTenderSectionId()+"";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                              <%--<%if(sptcd.getFieldName5().contains("0")){ %>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}else{%>

                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>

                                            </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="4%" style="text-align:center;">Sl. <br/> No.</th>
                                                <th width="56%">Form Name</th>
                                                <th width="40%">Actions</th>
                                            </tr>
                                            <%
                                            if(isTenPackageWise){
                                                List<Object[]> techForms = dDocSrBean.getForms(tts.getTenderSectionId(),"no",0);
                                                int cnt=1;
                                                for(Object[] techForm : techForms){
                                                    boolean isFormCanceled = false;
                                                    if(techForm[2]!=null && "c".equalsIgnoreCase(techForm[2].toString())){
                                                        isFormCanceled = true;
                                                    }
                                                     //formstatus in DB - {a,createp,NULL,c,p,cp}
                                                    if(techForm[2]==null || (techForm[2]!=null && !"createp".equalsIgnoreCase(techForm[2].toString()))){
                                             %>
                                             
                                             <tr>
                                                        <td width="4%" style="text-align:center;"><%=cnt%></td>
                                                        <td>
                                                            <%
                                                                out.print(techForm[1]); 
                                                                if(isFormCanceled){
                                                                    out.print("  <font color='red'>(Form Canceled)</font>");
                                                                }
                                                            %>
                                                        </td>
                                                        <td>
                                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId %>&sectionId=<%= tts.getTenderSectionId() %>&formId=<%= techForm[0]%>">View Form</a>
                                                        </td>
                                             </tr>
                                             <%cnt++;
                                             
                                                    }
                                                }
                                            }
                                            %>
                                        </table>
                                        <%
                                                                                        for (TblTenderLotSecurity ttls : tenderDocumentSrBean.getLotList(tenderId)) {
                                %>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0" class="tableList_1 t_space">
                                    <tr>
                                        <td width="15%" class="ff">Lot No.</td>
                                        <td width="85%"><%=ttls.getLotNo()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Lot Description</td>
                                        <td ><%=ttls.getLotDesc()%></td>
                                    </tr>
                                    <tr>
                                                <td class="ff" width="15%">Grand summary : </td>
                                                <td class="t-align-left">
                                                    <%
                                                    int isGSCreatedId = -1;
                                                    isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,ttls.getAppPkgLotId());
                                                                                                                        //out.println(" isGSCreatedId " + isGSCreatedId + " ispublish " + ispublish);
                                                                                                                        if (isGSCreatedId != -1) {
                                                        %>
                                            <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&porlId=<%=ttls.getAppPkgLotId()%>">View</a>
                                                        <%
                                                                                                                                        } else {
                                                                                                                                out.print("Not Prepared");
                                                                                                                                }
                                                    %>
                                                </td>
                                            </tr>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th style="text-align:center; width:4%;">Sl. <br/> No.</th>
                                        <th width="56%">Form Name</th>
                                        <th width="40%">Actions</th>
                                    </tr>
                                    <% int jj = 0;
                                                                                                                for (TblTenderForms tblTenderForms : tenderDocumentSrBean.getTenderFormLot(tts.getTenderSectionId(), ttls.getAppPkgLotId())) {%>
                                    <%
                                                                                                                                    boolean isFrmCanceled = false;
                                                                                                                                    isFrmCanceled = false;
                                                                                                                                    if(tblTenderForms.getFormStatus()==null || (tblTenderForms.getFormStatus()!=null && !"createp".equalsIgnoreCase(tblTenderForms.getFormStatus().toString()))){
                                    %>
                                    <tr>
                                        <td style="text-align:center; width:4%;"><%=(jj += 1)%></td>
                                        <td width="56%">
                                            <%
                                                                                                                                            out.print(tblTenderForms.getFormName().replace("?s", "'s"));
                                                                                                                                            if (tblTenderForms.getFormStatus() != null) {
                                                                                                                                                if ("c".equals(tblTenderForms.getFormStatus().toString())) {
                                                                                                                                                    isFrmCanceled = true;
                                                                                                                                                    out.print("  <font color='red'>(Form Canceled)</font>");
                                                                                                                                                }
                                                                                                                                            }
                                            %>
                                        </td>
                                        <td width="40%">
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%=tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                        </td>
                                    </tr>
                                    <%}/*Condition for not showing forms created by pe in corrigendum not published*/ }%>
                                </table>
                                <% }%>
                                    </td>
                </tr>
                <%
                                        }
                                }else if(tts.getContentType().equals("Document")){
                %>
                                    <tr>
                                        <td class="t-align-center"><%= j %></td>
                                        <td colspan="2" class="t-align-left">

                                            <table width="100%" cellspacing="0" class="tableList_1">
                                                <tr>
                                                    <td colspan="5" class="t-align-left ff"><%= tts.getSectionName() %></td>
                                                </tr>


                                                <th width="4%" class="t-align-left">Sl.  No.</th>
                    <th class="t-align-left" width="30%">File Name</th>
                    <th class="t-align-left" width="38%">File Description</th>
                    <th class="t-align-left" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-left" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                    String sectionId = tts.getTenderSectionId()+"";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                              <%--<%if(sptcd.getFieldName5().contains("0")){ %>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}else{%>

                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>

                                            </table>
                                        </td>
                                    </tr>
                <%
                                }else if(tts.getContentType().equals("Document By PE")){
                %>
                                    <tr>
                                        <td class="t-align-center"><%= j %></td>
                                        <td colspan="2" class="t-align-left">
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <td colspan="5" class="t-align-left ff"><%= tts.getSectionName() %></td>
                                                </tr>
                                                <th width="4%" class="t-align-left">Sl. <br/>  No.</th>
                    <th class="t-align-left" width="30%">File Name</th>
                    <th class="t-align-left" width="38%">File Description</th>
                    <th class="t-align-left" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-left" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                    String sectionId = tts.getTenderSectionId()+"";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&tenderId=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>      </td>
                                    </tr>
                <%
                                }else{
                %>
                                    <tr>
                                        <td class="t-align-center"><%= j %>
                                        <td class="t-align-left"><%= tts.getSectionName() %>
                                            <div align="right">
      <%        if((tts.getContentType()).contains("ITT") || (tts.getContentType()).contains("GCC")){
                        String reqURL = request.getRequestURL().toString() ;
                        String tempID =dDocSrBean.findStdTemplateId(String.valueOf(request.getParameter("tenderId")));
                        String reqQuery = "sectionId="+tts.getTenderSectionId()+"&templateId="+tempID;
                        String folderName = pdfConstant.STDPDF;
                        String genId = tempID+"_"+tts.getTenderSectionId();
            %>

            <a class="action-button-savepdf"
                <%--href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>--%>

                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=j%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true">Save As PDF </a>
           <% } %>


            <%        if((tts.getContentType()).contains("TDS") || (tts.getContentType()).contains("PCC")){
                        String reqURL = request.getRequestURL().toString() ;
                        String reqQuery = "tenderStdId="+tenderStdId+"&tenderId="+tenderId+"&sectionId="+tenderittSectionId;
                        String folderName = pdfConstant.TENDERDOC;
                        String pdfId = tenderId+"_"+tenderittSectionId+"_"+tenderStdId;
            %>

            <a class="action-button-savepdf"
                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=j%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true">Save As PDF </a>
           <% } %>

                                            </div>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                                <th width="4%" class="t-align-left">Sl.  No.</th>
                    <th class="t-align-left" width="30%">File Name</th>
                    <th class="t-align-left" width="38%">File Description</th>
                    <th class="t-align-left" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-left" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                    String sectionId = tts.getTenderSectionId()+"";


                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                             <%--<%if(sptcd.getFieldName5().contains("0")){ %>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}else{%>

                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                         <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&bidderPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>

                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>
                                        </td>
                                        <%
                                        if(tts.getContentType().equals("TDS")
                                        || tts.getContentType().equals("PCC")){
                                           // tenderittSectionId = tts.getTenderSectionId();
                                        %>
                                            <td class="t-align-left"><a href="TenderTDSDashBoard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%=packageOrLotId%>">View</a></td>
                                        <%
                                        }else{
                                            tenderittSectionId = tts.getTenderSectionId();
                                            %>
                                            <td class="t-align-left"><a href="TenderITTDashboard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&porlId=<%=packageOrLotId%>">View</a></td>
                                        <%
                                        }
                                        %>
                                    </tr>
                <%
                                }
                                tSections = null;
                            }
                        }else{
                %>
                            <tr>
                                <td colspan="3">
                                    No Records Found
                                </td>
                            </tr>
                <%
                        }
                        tSections = null;
                    }else{
                %>
                        <tr>
                            <td colspan="3">
                                No Records Found
                            </td>
                        </tr>
                <%
                    }
                %>
                <tr>
                    <td colspan="3">
                        <div>
                            <span class="c-alignment-right">
                             <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=tenderId%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Tender/Proposal Dcoument">Download Tender/Proposal Document</a>
                            </span>
                         </div>
                    </td>
                </tr>
            </table>
            <jsp:include page="../resources/common/ViewTenderDoc.jsp" flush="true">
                <jsp:param name="tenderId" value="<%=tenderId%>"/>
            </jsp:include>
            </div>
        </div>

                         <jsp:include page="../resources/common/PreTenderQueRep.jsp" flush="true">
                <jsp:param name="tenderId" value="<%=tenderId%>"/>
                <jsp:param name="isPDF" value="true" />
                <jsp:param name="hideDiv" value="hideDiv" />
                <jsp:param name="viewType" value="viewType" />
            </jsp:include>



            <%--<div class="t-align-center t_space"><label class="formBtn_1"><input name="DocPreparation" type="button" value="Complete Document Preparation" /></label></div>--%>
            <%--<div>&nbsp;</div>--%>

            <!--Page Content End-->
            <!--Middle Content Table End-->

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<%
    if(grandSummary != null){
        grandSummary = null;
    }
    if(dDocSrBean != null){
        dDocSrBean = null;
    }
%>
