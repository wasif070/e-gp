<%--
    Document   : ViewPersonalDetail
    Created on : Jan 05, 2011, 11:40 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Personal Detail</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
    </head>
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    if(session.getAttribute("userId")!=null && session.getAttribute("userId").toString().equals(request.getParameter("uId"))){
                        contentAdminService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    TblTendererMaster tblTendererMaster = contentAdminService.findTblTendererMasters("tendererId", Operation_enum.EQ, Integer.parseInt(request.getParameter("tId"))).get(0);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String reg_qString = "";
                    if("no".equals(request.getParameter("top"))){
                        reg_qString = "&top=no";
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if("".equals(reg_qString)){%>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%}else{if(session.getAttribute("userId")==null){response.sendRedirect(request.getContextPath()+"/SessionTimedOut.jsp");}}%>
                <div class="tableHead_1 t_space"><% if(tblTendererMaster.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%> Personal Details <% } else { %>Company Contact Person Details<% } %></div>
                <div class="stepWiz_1 t_space">
                <ul>
                <%                            
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(request.getParameter("uId")));//
                            String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];
                            if (pageList.size() == 3) {
                %>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Personal Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 4) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 5) {%>
                <li <%if(pageName.equals("ViewRegistrationDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewRegistrationDetail.jsp")){%><a href="ViewRegistrationDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Registration Details<%if(!pageName.equals("ViewRegistrationDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewCompanyDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewCompanyDetail.jsp")){%><a href="ViewCompanyDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Details<%if(!pageName.equals("ViewCompanyDetail.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("ViewPersonalDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("ViewPersonalDetail.jsp")){%><a href="ViewPersonalDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%><%=reg_qString%>"><%}%>Company Contact Person Details<%if(!pageName.equals("ViewPersonalDetail.jsp")){%></a><%}%></li>
                <%}%>
            </ul>
            </div>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <tr>
                        <td width="18%" class="ff">Title : </td>
                        <td width="82%"><%=tblTendererMaster.getTitle()%></td>
                    </tr>
                    <tr>
                        <td class="ff">First Name : </td>
                        <td><%=tblTendererMaster.getFirstName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Middle Name : </td>
                        <td><%=tblTendererMaster.getMiddleName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Last Name : </td>
                        <td><%=tblTendererMaster.getLastName()%></td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Name in Dzongkha :</td>
                        <td><%if(tblTendererMaster.getFullNameInBangla()!=null){out.print(BanglaNameUtils.getUTFString(tblTendererMaster.getFullNameInBangla()));}%></td>
                    </tr>
<!--                    <tr>
                        <td class="ff">CID No. : </td>
                        <td><%//=tblTendererMaster.getNationalIdNo()%>
                        </td>
                    </tr>-->
                    <%if(!tblTendererMaster.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%>
                    <tr>
                        <td class="ff">Designation : </td>
                        <td><%=tblTendererMaster.getDesignation()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Department : </td>
                        <td><%=tblTendererMaster.getDepartment()%></td>
                    </tr>
                    <%}%>
                    <tr>
                        <td class="ff">Address : </td>
                        <td><%=tblTendererMaster.getAddress1()%></td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Address 2 : </td>
                        <td><%=tblTendererMaster.getAddress2()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblTendererMaster.getCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblTendererMaster.getState()%></td>
                    </tr>
                    
                    <tr>
                        <td class="ff">Dungkhag / Sub-district : </td>
                        <td><%if(tblTendererMaster.getSubDistrict()!=null){out.print(tblTendererMaster.getSubDistrict());}%></td>
                    </tr>
                    <tr id="trthana">
                        <td class="ff">Gewog : </td>
                        <td><%=tblTendererMaster.getUpJilla()%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%=tblTendererMaster.getCity()%></td>
                    </tr>
                    
                    <tr>
                        <td class="ff">Post Code : </td>
                        <td><%=tblTendererMaster.getPostcode()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No : </td>
                        <td><%if(!tblTendererMaster.getPhoneNo().equals("")){out.print(commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getPhoneNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Fax No : </td>
                        <td><%if(!tblTendererMaster.getFaxNo().equals("")){out.print(commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getFaxNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No : </td>
                        <td><%=commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getMobileNo()%>
                        </td>
                    </tr>
                    <%if(tblTendererMaster.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%>
                    <tr>
                        <td class="ff">Tax Payment Number : </td>
                        <td>
                             <%if(tblTendererMaster.getTinNo().equalsIgnoreCase("other")){out.print(tblTendererMaster.getTinDocName().substring(0, tblTendererMaster.getTinDocName().indexOf("$"))+"<div class=\"formNoteTxt\">(Other No)</div>"+tblTendererMaster.getTinDocName().substring(tblTendererMaster.getTinDocName().indexOf("$")+1,tblTendererMaster.getTinDocName().length())+"<div class=\"formNoteTxt\">(Description)</div>");}else{out.print(tblTendererMaster.getTinDocName());}%>
                        </td>
                    </tr>
<!--                    <tr >
                        <td class="ff">Specialization : </td>
                        <td>
                             <%//=tblTendererMaster.getSpecialization()%>
                        </td>
                    </tr>-->
                     <tr>
                        <td class="ff">Website : </td>
                        <td><%=tblTendererMaster.getWebsite()%></td>
                    </tr>
                    <%}%>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
