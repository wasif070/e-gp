<%-- 
    Document   : RegisterJVCA
    Created on : Mar 3, 2011, 10:39:00 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>
<%@ page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@ page import="com.cptu.egp.eps.web.utility.AppContext"%>
<jsp:useBean id="loginMasterDtBean" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
<jsp:setProperty property="*" name="loginMasterDtBean"/>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>New User Registration - Login Account Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#frmUserReg").validate({
                    rules: {
                        //emailId:{email:true},
                        //,spacevalidate:true
                        password: {required: true, minlength: 8, maxlength: 25, alphaForPassword: true},
                        confirmPassWord: {required: true, equalTo: "#txtPass"},
                        hintQuestion: {required: true},
                        hintAnswer: {required: true, maxlength: 100},
                        businessCountryName1: {required: true},
                        checkbox: {required: true}
                        //isJvca: { required: true }
                    },
                    messages: {
                        //emailId:{email:"<div class='reqF_1'>e-mail ID must be in xyz@abc.com format</div>" },
                        businessCountryName1: {required: "<div class='reqF_1'>Please select Country of Business</div>"},
                        password: {required: "<div class='reqF_1'>Please enter Password</div>",
                            minlength: "<div class='reqF_1'>Please enter at least 8 characters and password must contain both alphabets and numbers</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter alphanumeric only</div>"},
                        //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>"},

                        confirmPassWord: {required: "<div class='reqF_1'>Please retype the Password.</div>",
                            //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>",
                            equalTo: "<div class='reqF_1' id='msgPwdNotMatch'>Password does not match. Please try again</div>"},
                        //NotequalTo: "<div style='color:green'><b>Password Matches</b></div>",
                        hintQuestion: {required: "<div class='reqF_1'>Please select Hint Question</div>"},
                        hintAnswer: {required: "<div class='reqF_1'>Please enter your Hint Answer</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"},
                        checkbox: {required: "<div class='reqF_1'>Please accept Terms and Conditions</div>"}
                        //isJvca: {required: "<div class='reqF_1'>Please Select JVCA.</div>"}
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "checkbox") {
                            error.insertAfter("#tnc");
                        } else if (element.attr("name") == "password") {
                            error.insertAfter("#passnote");
                        } else if (element.attr("name") == "hintAnswer") {
                            error.insertAfter("#hintanssp");
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(function () {
                $('#cmbHintQue').change(function () {
                    if ($('#cmbHintQue').val() == 'other') {
                        $('#hintQueTr').show();
                    } else {
                        $('#hintQueTr').hide();
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#txtMail').blur(function () {
                    if ($('#txtMail').val() == "") {
                        $('span.#mailMsg').css("color", "red");
                        $('span.#mailMsg').html("Please enter e-mail ID");
                    } else {
                        $('span.#mailMsg').css("color", "red");
                        $('#mailMsg').html("Checking for unique e-mail ID...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId: $('#txtMail').val(), funName: 'verifyMail'},
                                function (j) {
                                    if (j.toString().indexOf("OK", 0) != -1) {
                                        $('span.#mailMsg').css("color", "green");
                                    } else {
                                        $('span.#mailMsg').css("color", "red");
                                    }
                                    $('span.#mailMsg').html(j);
                                });
                    }
                });
            });
            $(function () {
                $('#recaptcha_response_field').blur(function () {
                    if ($('#recaptcha_response_field').val() != "") {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {challenge: $('#recaptcha_challenge_field').val(), userInput: $('#recaptcha_response_field').val(), funName: 'captchaValid'},
                                function (j) {
                                    if (j.toString() == "OK") {
                                        $('span.#vericode').css("color", "green");
                                    } else {
                                        javascript:Recaptcha.reload();
                                        $('span.#vericode').css("color", "red");
                                    }
                                    $('span.#vericode').html(j);
                                });
                    } else {
                        $('span.#vericode').css("color", "red");
                        $('#vericode').html("Please enter Verification Code");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#frmUserReg').submit(function () {
                    if ($('#txtMail').val() == "") {
                        $('span.#mailMsg').css("color", "red");
                        $('span.#mailMsg').html("Please enter e-mail ID");
                        return false;
                    }
                    if ($('span.#mailMsg').html() != "OK") {
                        return false;
                    }
                });
            });
        </script>
        <script type="text/javascript">

            function Validate()
            {
                if (document.getElementById("cmbHintQue").value == "other")
                {
                    if ($.trim(document.getElementById("txtaQuestion").value) == '')
                    {
                        document.getElementById("hintquestionerror").innerHTML = "Please enter Hint Question.";
                        return false;
                    }
                }
            }

            function chkPasswordMatches() {
                var objPwd = document.getElementById("txtPass");
                var objConfirmPwd = document.getElementById("txtConPass");
                if (objPwd != null && objConfirmPwd != null) {
                    if ($.trim(objPwd.value) == "" || $.trim(objConfirmPwd.value) == "") {
                    } else {
                        var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
                        if (objPwd.value == objConfirmPwd.value) {
                            var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
                            if (msgPwdNotMatch != null) {
                                if (msgPwdNotMatch.innerHTML == "Password does not match. Please try again") {
                                    msgPwdNotMatch.style.diplay = "none";
                                    msgPwdNotMatch.innerHTML = "";
                                }
                            }
                            msgPwdMatchObjDiv.innerHTML = "Password Matches";
                        } else {
                            msgPwdMatchObjDiv.innerHTML = "";
                        }
                    }
                }
            }

            function ClearMessage() {
                if (document.getElementById("cmbHintQue").value == 'other')
                {
                    if ($.trim(document.getElementById("txtaQuestion").value) != '')
                    {
                        if ($.trim(document.getElementById("txtaQuestion").value).length <= 50)
                        {
                            document.getElementById("hintquestionerror").innerHTML = "";
                        } else
                        {
                            document.getElementById("hintquestionerror").innerHTML = "Maximum 50 characters are allowed.";
                            return false;
                        }
                    } else
                    {
                        document.getElementById("hintquestionerror").innerHTML = "Please enter Hint Question.";
                        return false;
                    }
                }
            }
            $(function () {
                $('#frmUserReg').submit(function () {
                    if ($('#recaptcha_response_field').val() == "") {
                        $('span.#vericode').css("color", "red");
                        $('#vericode').html("Please enter Verification Code");
                        return false;
                    }
                    if ($('#vericode').html() != "OK") {
                        javascript:Recaptcha.reload();
                        return false;
                    }
                });
            });
            $(function () {
                $('#btnReset').click(function () {
                    $('#vericode').html(null);
                    $('#mailMsg').html(null);
                    var validator = $("#frmUserReg").validate();
                    validator.resetForm();
                });
            });
            /*function myMethod(e){
             if(e.keyCode==17 || e.keyCode==93){
             alert('Copy Paste not allowed.');
             return false;
             }
             }
             function rajesh(e){
             $(e).bind("contextmenu",function(e){
             return false;
             });
             }*/
            function taher() {
                jAlert("Copy and Paste not allowed.", "New User Registration", function (RetVal) {
                });
                return false;
            }
        </script>
    </head>
    <body>
        <%

            String uId = "0";
            if (session.getAttribute("userId") != null) {
                uId = session.getAttribute("userId").toString();
            }
            String jvcaName = "";
            int jvcId = Integer.parseInt(request.getParameter("jvcId"));
            LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(uId);
            jvcaName = loginMasterSrBean.getJVCACompanyName(jvcId);
            uId = null;
            String msg = null;
            if ("Submit".equals(request.getParameter("button"))) {
                if (loginMasterDtBean.getEmailId() != null) {
                    if (!loginMasterDtBean.getEmailId().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                        msg = "Please enter Valid e-mail ID";
                    } else {
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        msg = commonService.verifyMail(loginMasterDtBean.getEmailId());
                    }
                } else {
                    msg = "Please enter e-mail ID";
                }
                if (msg.equalsIgnoreCase("ok")) {
                    String tmp = "";
                    for (String str : request.getParameterValues("businessCountryName1")) {
                        tmp += str + ",";
                    }
                    loginMasterDtBean.setBusinessCountryName(tmp.substring(0, tmp.length() - 1));
                    int stat = loginMasterSrBean.jvcaRegister(loginMasterDtBean, request.getParameter("jvcId"));
                    if (stat == 0) {
                        out.print("<h1>Error in registration of JVCA</h1>");
                    } else {
                        response.sendRedirect("JVCompanyDetails.jsp?uId=" + stat);
                    }
                }
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                    <!--Middle Content Table Start-->
                    <div class="pageHead_1">Register JVCA<span style="float: right;"><a class="action-button-goback" href="JvcaList.jsp" >Go Back</a></span></div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">                                    
                            <td class="contentArea-Blogin">                                                        
                                <form id="frmUserReg" method="post" action="RegisterJVCA.jsp?jvcId=<%=request.getParameter("jvcId")%>">
                                    <%if (msg != null) {%><br/><div class="responseMsg errorMsg"><%=msg%></div><%}%>
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                        <tr>
                                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="20%">JVCA Name : </td>
                                            <td> <strong> <%=jvcaName%> </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" >e-mail ID : <span>*</span></td>
                                            <td ><input name="emailId" type="text" class="formTxtBox_1" id="txtMail" style="width:200px;" /> <span id="emailnote" class="formNoteTxt"><br/>(e-mail ID should be valid. Example: xyz@example.com)</span>
                                                <br /><span id="mailMsg" style="color: red; ">&nbsp;</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Password : <span>*</span></td>
                                            <td><input name="password" type="password" class="formTxtBox_1" id="txtPass" style="width:200px;" onblur="chkPasswordMatches();" autocomplete="off" /><span id="passnote" class="formNoteTxt">
                                                    <br/>(Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added)</span></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Confirm Password : <span>*</span></td>
                                            <td><input name="confirmPassWord" type="password" class="formTxtBox_1" id="txtConPass" style="width:200px;" onblur="chkPasswordMatches();" onpaste="return taher();" autocomplete="off" />
                                                <div style="color: green" id="pwdMatchMsg"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Hint Question : <span>*</span></td>
                                            <td><select name="hintQuestion" class="formTxtBox_1" id="cmbHintQue" style="width:auto;min-width: 200px;" onchange="changeHint()">
                                                    <option value="">Select Hint Question</option>
                                                    <%for (SelectItem hintQus : loginMasterSrBean.getHintQueList()) {%>
                                                    <option value="<%=hintQus.getObjectId()%>"><%=hintQus.getObjectValue()%></option>
                                                    <%}%>
                                                    <option value="other">Create Your own Question</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr style="display: none;" id="hintQueTr">
                                            <td class="ff">Create your Own Hint Question :<span>*</span></td>
                                            <td><textarea name="hintQuestionOwn" rows="3" class="formTxtBox_1" id="txtaQuestion" style="width:400px;" onblur="ClearMessage()"></textarea>
                                                <span id="hintquestionerror" class="reqF_1"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Hint Answer : <span>*</span></td>
                                            <td><input name="hintAnswer" type="text" maxlength="50" class="formTxtBox_1" id="txtHintAns" style="width:200px;" />
                                                <span class="formNoteTxt" id="hintanssp"><br/>(Please remember the answer. You need this answer to retrieve the password in case you forgot)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Nationality : <span>*</span></td>
                                            <td><select name="nationality" class="formTxtBox_1" id="cmbNational" style="width:auto;">
                                                    <%for (SelectItem nationality : loginMasterSrBean.getNationalityList()) {%>
                                                    <option value="<%=nationality.getObjectId()%>" <%if (nationality.getObjectValue().equals("Bhutanese")) {%>selected<%}%> ><%=nationality.getObjectValue()%></option>
                                                    <%}%>                                                
                                                </select>
                                            </td>
                                        </tr>                                    
                                        <input type="hidden" value="yes" name="isJvca" id="cmbJvca"/>
                                        <input type="hidden" value="contractor" name="registrationType"/>
                                        <tr>
                                            <td class="ff">Country of Business : <span>*</span></td>
                                            <td><select name="businessCountryName1" class="formTxtBox_1" id="cmbCountryBus" multiple size="5">
                                                    <%for (SelectItem country : loginMasterSrBean.getCountryList()) {%>
                                                    <option value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals("Bhutan")) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                    <%}%>                                                
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnSubmit" value="Submit" onclick="return Validate();"/>
                                                </label>
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                                </label></td>
                                        </tr>

                                    </table>

                                </form>
                                <!--Page Content End-->
                            </td>
                        </tr>
                    </table>
                    <!--Middle Content Table End-->
                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </div>
    </body>
</html>