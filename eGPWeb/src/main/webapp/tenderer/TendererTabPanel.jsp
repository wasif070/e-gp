<%--
    Document   : tendererTAbs
    Created on : Nov 22, 2010, 11:46:23 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="tenderTabId" class="com.cptu.egp.eps.web.databean.TendererTabDtBean" />
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%--<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>--%>

<%
            int maxEvalCount = 0;
            boolean isLOIAccessible = false;
            
            String tab = "";
            String tenderid = "";
            int tendererId = 0;
            String isTenderApproved = "";
            TenderCommonService tenderCS1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if(pageContext.getAttribute("isThisTenderApproved")!=null){
            isTenderApproved = pageContext.getAttribute("isThisTenderApproved").toString();}
            if (request.getParameter("tab") != null && !"null".equalsIgnoreCase(request.getParameter("tab"))) {
                  tab =   request.getParameter("tab");
            }

            if (pageContext.getAttribute("tab") != null && !"null".equalsIgnoreCase(pageContext.getAttribute("tab").toString())) {
                 tab = pageContext.getAttribute("tab").toString();
            }

            if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                 tenderid = request.getParameter("tenderid");
                 tenderTabId.setTenderId(Integer.parseInt(tenderid));
                    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                 tenderid =   request.getParameter("tenderId");
                 tenderTabId.setTenderId(Integer.parseInt(tenderid));
            }

            List<SPTenderCommonData> sptcdMore = tenderCS1.returndata("tenderinfobarMore", tenderid, null);
            String tenderProcureNature1="";String tabLabel ="";
            if(sptcdMore!= null && sptcdMore.size() > 0 && sptcdMore.get(0)!= null)
            {
                tenderProcureNature1=sptcdMore.get(0).getFieldName2();
            }
            Boolean isPackageWise = false;
                    if (pageContext.getAttribute("isTenPackageWise") != null) {
                isPackageWise = (Boolean) pageContext.getAttribute("isTenPackageWise");
            }

            String ispdf2 = "";
            if(pageContext.getAttribute("isPDF")!=null && !"".equals(pageContext.getAttribute("isPDF"))){
                ispdf2 = pageContext.getAttribute("isPDF").toString();
            }

            String userid2 = "";
            if("true".equalsIgnoreCase(ispdf2)){
                if(pageContext.getAttribute("userId")!=null && !"".equals(pageContext.getAttribute("userId"))){
                    userid2 = pageContext.getAttribute("userId").toString();
                }
            }else{
                userid2 = session.getAttribute("userId").toString();
            }

            if("true".equalsIgnoreCase(ispdf2)){
                tendererId = Integer.parseInt(userid2);
                tenderTabId.setTendererId(Integer.parseInt(userid2));
            }else{
                if (session.getAttribute("userId") != null && !"null".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                   tendererId = Integer.parseInt(session.getAttribute("userId").toString());
                   tenderTabId.setTendererId(tendererId);
                }
            }
            
            //For LOI
            maxEvalCount = tenderCS1.GetEvalCount(Integer.parseInt(tenderid));
            isLOIAccessible = tenderCS1.IsLOIAccessible(Integer.parseInt(tenderid), Integer.parseInt(userid2)) && tenderCS1.LOIVisibilityStatus(Integer.parseInt(tenderid));
            

            /*If PQ dont show NOA, Contract Sgin  and CMS START bug ID: 4365 */
            boolean officeTabIs2Env = false;
            boolean officeTabIsCase1 = false;//PQ, 1st stage of TSTM and OSTETM

            CommonService cmnSrv = (CommonService) AppContext.getSpringBean("CommonService");
            CommonSearchDataMoreService commonSearchDataMoreServiceForTTP = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            //used here so commented below on line no 209

            List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreServiceForTTP.geteGPData("GetTenderEnvCount", tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            String officeTabPMethod = cmnSrv.getProcMethod(tenderid).toString();
            String eventTypeCase = cmnSrv.getEventType(tenderid).toString();

            if(envDataMores!=null && (!envDataMores.isEmpty()) && envDataMores.get(0).getFieldName1().equals("2")){
                officeTabIs2Env = true;
                envDataMores = null;
            }
            if (eventTypeCase.equals("1 stage-TSTM") || eventTypeCase.equals("PQ") || (officeTabPMethod.equals("OSTETM") && (!officeTabIs2Env))) {
                officeTabIsCase1 = true;
            }
            /*If PQ dont show NOA, Contract Sgin  and CMS END bug ID: 4365 */
            String isTendererDeclStepOver = tenderTabId.isTendererDeclarationStepOver();

            if("true".equalsIgnoreCase(ispdf2)){
                tenderCS1.setLogUserId(userid2);
            }else{
                if (session.getAttribute("userId") != null) {
                    tenderCS1.setLogUserId(session.getAttribute("userId").toString());
                }
            }

            List<Object[]> returndata = null;
            long count = 0;
            returndata = tenderCS1.getCompnyIDAndCompanyAdmin(tendererId);
            count = tenderCS1.getAllDetailsOfAssignedUser(tendererId, Integer.parseInt(tenderid));
            String companyId = "";
            String isAdmin = "";
            for (Object[] obj : returndata) {
                companyId = obj[0].toString();
                isAdmin = obj[1].toString();
            }
            boolean flag = false;
            if (companyId != "1") {
                if (isAdmin.equalsIgnoreCase("yes") || count > 0) {
                    flag = true;
                }
            }
            boolean is_debared = false;
            String debar_start = null;
            String debar_end = null;
            String debar_egpcomments = null;

            /* Start: CODE TO GET TENDER OPEN STATUS */
              boolean isCurTenOpened=false;
              List<SPTenderCommonData> listTenderOpenStatus  = tenderCS1.returndata("getTenderOpenStatus", tenderid, null);
               if (!listTenderOpenStatus.isEmpty()) {
                  if("Yes".equalsIgnoreCase(listTenderOpenStatus.get(0).getFieldName1())){
                    isCurTenOpened = true;
                  }
              }
              listTenderOpenStatus = null;
              /* End: CODE TO GET TENDER OPEN STATUS */
              /*This method is for get tenderer details*/
             ContentAdminService cas1 = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                                                Object[] objTender = new Object[3];
                                                objTender = cas1.getCompanyTendererId(userid2);

%>
<html>
    <body>
        <div>&nbsp;</div>
        <%
            /*
            *By TaherT for debarment
            */
            List<SPCommonSearchData> debar_egp_list = tenderTabId.getDebarmentDetails(String.valueOf(tendererId), tenderid);
                    if (debar_egp_list!=null && !debar_egp_list.isEmpty()) {
                        if (debar_egp_list.get(0).getFieldName1().equalsIgnoreCase("yes")) {
                            is_debared = true;
                    debar_start = debar_egp_list.get(0).getFieldName4();
                    debar_end = debar_egp_list.get(0).getFieldName5();
                    debar_egpcomments = debar_egp_list.get(0).getFieldName6();
                }
            }
        %>
        <%                    
                    if (is_debared) {%>
                    <div class='tabPanelArea_1'>
                        <div class='bigTxt_1' align='left'>
                        Dear User,<br/><br/>

                        You have been debarred by the Procuring Entity. Debarment detail is as mentioned below:<br/><br/>

                        Debarment Reason: <%=debar_egpcomments%><br/>
                        Debarred From: '<%=debar_start%>' to Debarred To: '<%=debar_end%>'
                        </div>
                        </div>
                    <%} else {
        %>
        <ul class="tabPanel_1 noprint">
            <li><%--<a href="../tenderer/Declaration.jsp?tab=1&tenderId=<%=tenderid%>" id="hrfDeclaration"
                   <%if("1".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Declaration</a>--%>
                   <a href="../tenderer/LotPckDocs.jsp?tab=1&tenderId=<%=tenderid%>" id="hrfDeclaration"
                   <%if ("1".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>>Docs.</a>
            </li>
             <%--  Q & A  --%>

            <%
            TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
            List<TblTenderPostQueConfig> listForClarificationTab = tenderPostQueConfigService.getTenderPostQueConfig(Integer.parseInt(tenderid));
            if(!listForClarificationTab.isEmpty() && listForClarificationTab.get(0).getIsQueAnsConfig().equalsIgnoreCase("yes")){
            if(isTendererDeclStepOver==null){
            %>
                            <li><a href="#" onclick="jAlert('Please complete the Declaration step first,click on Docs..',' Tender Declaration ', function(RetVal){});"  >Clarification</a></li>
            <%
                }else{

                       //TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
                       if (session.getAttribute("userId") != null) {
                            tenderPostQueConfigService.setLogUserId(session.getAttribute("userId").toString());
                       }
                       int tenderID = Integer.parseInt(tenderid);
                       List<TblTenderPostQueConfig> listt = tenderPostQueConfigService.getTenderPostQueConfig(tenderID);
                       if(!listt.isEmpty())
                       {
                            String isQueAnsConfig = listt.get(0).getIsQueAnsConfig();
                            if("Yes".equalsIgnoreCase(isQueAnsConfig))
                            {
            %>
                                <li><a href="<%=request.getContextPath()%>/tenderer/QusTenderer.jsp?tenderId=<%=tenderid%>" id="hrfQusAns" <%if("11".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>  title="Clarification on Tender/Proposal">Clarification</a></li>
            <%
                            }
                       }
                } }
            %>

            <%--  Pre Tend. Meeting   --%>
            <%
                                //CommonSearchDataMoreService commonSearchDataMoreServiceForTTP = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            tabLabel="Pre Tend. Meeting";
            if(tenderProcureNature1!= null && tenderProcureNature1.equalsIgnoreCase("Services"))
            {
                tabLabel="Pre Prop. Meeting";
            }
            
                                List<SPCommonSearchDataMore> detailsForTender = commonSearchDataMoreServiceForTTP.geteGPData("getDataForTenderer", tenderid);
                                if(!detailsForTender.isEmpty() && detailsForTender.get(0).getFieldName1().equalsIgnoreCase("no")){
                                    if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  ><%=tabLabel%></a></li>
            <%                            }
                   else if ("rejected".equals(isTendererDeclStepOver)) {
                        out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">"+tabLabel+"</a></li>");
                   }else {
            %>
            <li><a href="<%=request.getContextPath()%>/tenderer/PreTenTenderer.jsp?tenderId=<%=tenderid%>" id="hrfPreTendMeet" <%if ("2".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>  ><%= tabLabel%></a></li>
            <%
                }
                                }
            %>

            <%--  Sub Contracting   --%>

            <%

            tabLabel="Sub Contracting";
            if(tenderProcureNature1!= null && tenderProcureNature1.equalsIgnoreCase("Services"))
            {
                tabLabel="Sub Consulting";
            }
            
            /*if jvca then dont need to show this is for jvca check*/
            if(!"yes".equalsIgnoreCase(objTender[2].toString())){
                TenderSrBean tab_tenderSrBean = new TenderSrBean();
                 if("true".equalsIgnoreCase(ispdf2)){
                     tab_tenderSrBean.setLogUserId(userid2);
                 }else{
                      tab_tenderSrBean.setLogUserId(session.getAttribute("userId").toString());
                 }


            %>
            <%--<li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Sub Contracting</a></li>
            <%                            } else {
            %>
            <%if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot do Sub Contracting.',' Tender Declaration ', function(RetVal){});\">Sub Contracting</a></li>");
                                    }else{%>--%>
            <li><a href="<%=request.getContextPath()%>/ServletSubContracting?tenderid=<%=tenderid%>&isPackage=<%=isPackageWise%>&funName=link" id="hrfsubcontract" <%if ("3".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>><%= tabLabel%></a></li>
            <%
                           //}
                          }
                //}
            %>

              <%--  Payment   --%>

            <%
                                    if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Payment</a></li>
            <%                            } else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">Payment</a></li>");
                                    }else {
            %>
                <li>
<!--                <a style="display:none;" href="../partner/ForTenderPayment.jsp?tab=7&tenderId=<%=tenderid%>" <%if ("7".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>>Payment</a>-->
                    <a href="../partner/ForViewTenderPayment.jsp?tab=7&tenderId=<%=tenderid%>" <%if ("7".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>>Payment</a>
                </li>
            <%
                }
            %>

            <%--  Tend. preparation   --%>

            <%
            tabLabel="Tend. Preparation";
            if(tenderProcureNature1!= null && tenderProcureNature1.equalsIgnoreCase("Services"))
            {
                tabLabel="Prop. Preparation";
            }
            
                         boolean b_isSubContract = tenderTabId.subContractCondition(tenderid, userid2);

                                    if(!b_isSubContract){%>
                                    <li><a href="#" onclick="jAlert('You have already accepted a request of Sub Contracting / Consultancy. You cannot do final submission now.',' Sub Contracting / Consultancy ', function(RetVal){});"  ><%=tabLabel%></a></li>
                                                  <%}else if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  ><%= tabLabel%></a></li>
            <%                            }else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender/Proposal you cannot submit the Tender/Proposal.',' Tender/Proposal Declaration ', function(RetVal){});\">"+tabLabel+"</a></li>");
                                    } else if (!flag) {
            %>
            <li><a href="#" onclick="jAlert('Declaration for a tender can be given only by a user who has got tender submission right.',' Tender Declaration ', function(RetVal){});"  ><%= tabLabel %></a></li>

            <%} else if (isTenderApproved.equalsIgnoreCase("false")) {%>

            <li><a href="#" onclick="jAlert('Tender has been cancelled.',' Tender Declaration ', function(RetVal){});"  ><%= tabLabel%></a></li>


            <%} else {%>

            <li><a href="<%=request.getContextPath()%>/LotSelectionServlet?tenderId=<%=tenderid%>&action=linkSet"
                   id="hrfTendPrep" <%if ("4".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>><%= tabLabel%></a></li>
            <%
                }
            %>

            <%-- Opening   --%>
            <%if (!isCurTenOpened) {%>
                <li><a href="#" onclick="jAlert('Opening Date and Time not yet lapsed.',' Opening ');">Opening</a></li>
            <%
                                   } else if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});">Opening</a></li>
            <%                            }
                else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">Opening</a></li>");
                                    }
                else {
            %>
            <li><a href="<%=request.getContextPath()%>/tenderer/ResultSharingTenderer.jsp?tenderId=<%=tenderid%>" <%if ("5".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%> >Opening</a></li>
            <%
                }
            %>

            <%--  Evaluation   --%>

           <%if (!isCurTenOpened) {%>
                <li><a href="#" onclick="jAlert('Opening Date and Time not yet lapsed.',' Opening ');">Evaluation</a></li>
            <%
                                   } else if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Evaluation</a></li>
            <%                            }else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">Evaluation</a></li>");
                                    }
                else {
            %>
            <li><a href="<%=request.getContextPath()%>/tenderer/EvalBidderClari.jsp?tenderId=<%=tenderid%>" id="hrfEval" <%if ("6".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>  >Evaluation</a></li>
            <%
                }
            %>

            <!--LOI-->
            <%
                if(isLOIAccessible){
            %>
            <li><a href="<%=request.getContextPath()%>/tenderer/LOI.jsp?tenderid=<%=tenderid%>" id="loi" <%if ("24".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>  >Letter Of Intent</a></li>

            <%--  NOA   --%>

            <% }
            /*IF not PQ pre qualified event type then only show NOA, Contract Sign and CMS bug ID: 4365 Condition Start */
            if(!officeTabIsCase1){ //NOA, Cont. Signing and CMS tabs disable for 1st phase UAT test
                                    if (isTendererDeclStepOver==null) {
            %>
            <li><a href="#" <%if ("8".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%> onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Letter of Acceptance</a></li>
            <%
                                        } else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">Letter of Acceptance</a></li>");
                                    }else {
                    List<TblTenderDetails> tblTenderDetails = tenderTabId.getProcurementNature(Integer.parseInt(tenderid));
                    String procurementNature = tblTenderDetails.get(0).getProcurementNature();
                                        //    if (!procurementNature.equalsIgnoreCase("Services")) {
            %>
            <li><a href="<%=request.getContextPath()%>/tenderer/NOAListing.jsp?tenderid=<%=tenderid%>" <%if ("8".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%>>Letter of Acceptance</a></li>
            <%    //  } else if (procurementNature.equalsIgnoreCase("Services")) {%>
          <!--  <li><a href="< %=request.getContextPath()%>/tenderer/NOAListing.jsp?tenderid=< %=tenderid%>" < % if ("8".equalsIgnoreCase(tab)) {%>class="sMenu"< %}%>>Draft Agreement</a></li> -->
            <% //}
                }%>
            <%--  Disc. Forum  --%>
            <%
                boolean noaIssuedStatus = tenderCS1.getCMSTab(tendererId, Integer.parseInt(tenderid));
                if(noaIssuedStatus){
            %>
            <li class=""><a title="Private Discussion Forum" href="<%=request.getContextPath()%>/resources/common/PriDissForumTopicList.jsp?tenderId=<%=tenderid %>" id="cms" <%if("17".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >Private Forum</a></li>
            <%}%>
            <%--  Contract Signing tab hide for 1st phase --%>

            <%--<%
                                    if (isTendererDeclStepOver==null) {
            %>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Cont. Signing</a></li>
            <%}else if ("rejected".equals(isTendererDeclStepOver)) {
                                out.print("<li><a href=\"#\" onclick=\"jAlert('Declaration completed successfully. As you have disagreed to the Terms and Conditions of the Tender you cannot submit the Tender.',' Tender Declaration ', function(RetVal){});\">Cont. Signing</a></li>");
                                    }
                else{%>
            <li><a href="<%=request.getContextPath()%>/tenderer/ContractSigningListing.jsp?tenderId=<%=tenderid%>" id="conSign" <%if ("9".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%> >Cont. Signing</a></li>
            <%}%>--%>

            <%
                if(noaIssuedStatus){
            %>
            <li> <a href="<%=request.getContextPath()%>/tenderer/CMSMain.jsp?tenderId=<%=tenderid%>" id="conSign" <%if ("10".equalsIgnoreCase(tab)) {%>class="sMenu"<%}%> >CMS</a></li>
            <%
                }
            } /*Conditon Ends*/
            %>
            <%-- complaint management system  start --%>
           <% if (false) { //Complaint Mgmt tab disable for 1st phase UAT test
            if(isTendererDeclStepOver==null){%>
                <li><a href="#" onclick="jAlert('Please complete the Declaration step first, click on Docs.',' Tender Declaration ', function(RetVal){});"  >Complaint Mgmt</a></li>
            <%}else{%>
                <li><a href="<%=request.getContextPath()%>/tenderer/complaint.htm?tenderId=<%=tenderid%>" id="conSign" <%if("11".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Complaint Mgmt</a></li>
            <%} } %>
            <%-- complaint management system  end --%>
        </ul>
        <%}%>
    </body>
    <script type="text/javascript">
        //var headSel_Obj = document.getElementById("headTabCompVerify");
        //if(headSel_Obj != null){
            //headSel_Obj.setAttribute("class", "selected");
        //}
    </script>

</html>
<%
   // if(tenderCS1!=null){
       //     tenderCS1=null;
      //  }
%>