<%--
    Document   : BidPreperation
    Created on : Nov 20, 2010, 5:52:22 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document read confirmation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if (request.getParameter("lotId") != null) {
        %>
        
            <div class="mainDiv">
                <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
            </div>
            <div class="tabPanelArea_1">
                <%
                    String tenderId = request.getParameter("tenderId");

                    List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getlotorpackagebytenderid",
                                                                                    tenderId,
                                                                                    "Lot," + request.getParameter("lotId"));
                    if (!lotList.isEmpty()) {
                %>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="15%" class="t-align-left ff">Lot No. :</td>
                        <td width="85%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Lot Description :</td>
                        <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
                    </tr>
                </table>
                <%
                    }

                    List<SPTenderCommonData> formList = tenderCommonService1.returndata("getfrmNamebytenderidandlotid",
                                                                                        tenderId,
                                                                                        request.getParameter("lotId"));
                %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="61%" class="t-align-left ff">Form Name</th>
                        <th width="21%" class="t-align-left ff">Submitted1</th>
                        <th width="18%" class="t-align-left ff">Attached Document</th>
                    </tr>
                    <%
                        if (!formList.isEmpty()) {
                            for (SPTenderCommonData formname : formList) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=formname.getFieldName3()%></td>
                        <td class="t-align-left"><%=formname.getFieldName5()%></td>
                        <td class="t-align-left"><a href="#">Map</a></td>
                    </tr>
                    <%
                        }
                            } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><B>No form name found!</B></td>
                    </tr>
                    <% } %>
                </table>
            </div>
        
        <%
            } else {
                String tenderId = request.getParameter("tenderId");
                List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getlotorpackagebytenderid",
                                                                                    tenderId,
                                                                                    "Package,1");
                    if (!lotList.isEmpty()) {
        %>
        <table width="100%" cellspacing="0" class="tableList_1">

            <tr>
                <td width="15%" class="t-align-left ff">Package No. :</td>
                <td width="85%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
            </tr>
            <tr>
                <td class="t-align-left ff">Package Description :</td>
                <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
            </tr>
        </table>
        <%
            }

            List<SPTenderCommonData> formList = tenderCommonService1.returndata("getfrmNamebytenderidandlotid",
                                                                                tenderId,
                                                                                "0");
        %>
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <tr>
                <th width="61%" class="t-align-left ff">Form Name</th>
                <th width="21%" class="t-align-left ff">Submitted2</th>
                        <th width="18%" class="t-align-left ff">Attached Document</th>
            </tr>
            <%
                if (!formList.isEmpty()) {
                    for (SPTenderCommonData formname : formList) {
            %>
            <tr>
                <td class="t-align-left"><%=formname.getFieldName3() %></td>
                <td class="t-align-left"><%=formname.getFieldName5()%></td>
                <td class="t-align-left"><a href="#">Map</a></td>
            </tr>
            <%      }
                 } else {
            %>
            <tr>
                <td class="t-align-left" colspan="3"><B>No form name found!</B></td>
            </tr>
            <% }%>
        </table>
        <% } %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
