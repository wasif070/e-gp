<%-- 
    Document   : ViewTenderPaymentDetails
    Created on : Jun 11, 2011, 11:17:30 AM
    Author     : Karan
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

<!--         <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });

                function printElem(options){
                $('#header').show();
                if (document.getElementById("print_area")!=null){
                    $('#print_area').printElement(options);
                }
                $('#header').hide();
            }
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                //  Dohatec Start
                //TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = objTSC.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true"))
                {
                    isIctTender = true;
                }
                //  Dohatec End

                if (session.getAttribute("userId") != null) {
                        objTSC.setLogUserId(session.getAttribute("userId").toString());
                    }

                    boolean isPEUser = false;
                    boolean isTenderer=false;
                    boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false, isRedirect=false;
                    String userBranchId="";
                    String userId="", payUserId="", regPaymentId="", redirectPath="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="";

                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  } else {
                    // response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                     response.sendRedirect("SessionTimedOut.jsp");
                  }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("2".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isTenderer=true; // userType is Tenderer";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }

                if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }

                if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }
                int auditId=0;
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                String idType="Contractid";
                if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Bid Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             }else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                             }
                        }
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()),conId, idType, EgpModule.Payment.getName(), "View Payment Details for "+paymentTxt, "");
                    }
                %>
                <div  id="print_area">
                <div class="contentArea_1">
                    <div class="pageHead_1">Payment Details
                        <span style="float:right;" class="noprint">
                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                        <a href="Invoice.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        </span>
                    </div>

               <div id="resultDiv">
                        
                        <% pageContext.setAttribute("tenderId", tenderId); %>

                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
<%
/* Start: CODE TO CHECK PE USER */
List<SPTenderCommonData> listCurUserPE  =
      objTSC.returndata("getPEOfficerUserIdfromTenderId", tenderId, userId);

if (!listCurUserPE.isEmpty()) {
  if(userId.equalsIgnoreCase(listCurUserPE.get(0).getFieldName1())){
    isPEUser = true;
  }
}
listCurUserPE = null;
/* End: CODE TO CHECK PE USER */
%>
                    <%
                if (isTenderer) {
                    if(request.getParameter("view")!=null && "CMS".equalsIgnoreCase(request.getParameter("view"))){
                        pageContext.setAttribute("tab", "14");
                    }else{
                        pageContext.setAttribute("tab", "7");
                    }
                %>
                    <%@include file="../tenderer/TendererTabPanel.jsp" %>
               <%} else if (isPEUser) {
                        if(request.getParameter("view")!=null && "CMS".equalsIgnoreCase(request.getParameter("view"))){
                            pageContext.setAttribute("tab", "14");
                        }else{
                            pageContext.setAttribute("tab", "8");
                        }
                %>
                    <%@include file="../officer/officerTabPanel.jsp" %>
                <% } %>
    
    <%
                pageContext.setAttribute("TSCtab", "3");
                if(request.getParameter("view")!=null && "CMS".equalsIgnoreCase(request.getParameter("view"))){
    %>
        <div class="tabPanelArea_1">
        <%@include  file="../tenderer/cmsTab.jsp"%>
    <%
        }
    %>
                        <div id="print_area">
                            <div style="display: none" id="header" class="pageHead_1">Payment details</div>
         <div class="tabPanelArea_1 t_space">
             
                         <%
                            boolean isPaymentVerified=false;
                            String bidderEmail="";
                            String emailId="";

                          


                          List<SPTenderCommonData> lstTendererEml =
                                  objTSC.returndata("getEmailIdfromUserId",payUserId,null);

                                if(!lstTendererEml.isEmpty()){
                                    emailId=lstTendererEml.get(0).getFieldName1();
                                    bidderEmail=emailId;
                                }

                          List<SPCommonSearchDataMore> lstPaymentDetail =
                                  //objTSC.returndata("getTenderPaymentDetail", regPaymentId, null);
                                  objPayment.geteGPData("getTenderPaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                          if (!lstPaymentDetail.isEmpty()) {
                              if ("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())) {
                                   isPaymentVerified = true;
                              }
                        %>
                        
                        <div>&nbsp;</div>
                        <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                        <div >
                            <%@include file="../partner/CommonPackageLotDescription.jsp" %>
                        </div>
                        <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->

                        
                        <!-- START: COMMON PAYMENT RELATED DOCS -->
                        <div class="noprint">
                            <%@include file="../partner/CommonPaymentDocsList.jsp" %>
                        </div>

                        <!-- END COMMON PAYMENT RELATED DOCS -->

                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="18%" class="ff">Payment Status :</td>
                            <td>

                                  <% if (!"Pending".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()) && !isPaymentVerified) { %>
                                    Verification is Pending
                                                <%} else {%>
                                                <% 
                                                    if("Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10())){
                                                        out.print("Compensated");
                                                       }else{
                                                            out.print(lstPaymentDetail.get(0).getFieldName10());
                                                       }
                                                 } %>
                            </td>
                        </tr>
                        <tr>
                            <td width="18%" class="ff">Email ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                        <tr>
                            <td class="ff">Financial Institute Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <%if(!"Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                    <%}else{%>
                        <tr>
                            <td class="ff">Transaction Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName13()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Card Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName14()%></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=paymentTxt%></td>
                        </tr>

                        <!-- Dohatec Start -->
                        <%if(isIctTender && "ps".equals(request.getParameter("payTyp"))){%>
                        <%
                            //for currency symbol
                            List<SPCommonSearchDataMore> lstPerSecPayDetail = objPayment.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());

                            if(!lstPerSecPayDetail.isEmpty())
                            {
                                String tID = request.getParameter("tenderId");
                                List<SPCommonSearchDataMore> lstPaymentDetail1 =
                                  objPayment.geteGPData("getTenderPaymentDetail", regPaymentId, "Performance Security", tID, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                                for(int i=0;i<lstPaymentDetail1.size();i++){
                        %>
                            <tr>
                                <td class="ff"><%out.println("Amount ( in "+ lstPaymentDetail1.get(i).getFieldName3() + ") :");%></td>
                                <td><%out.println(""+lstPerSecPayDetail.get(i).getFieldName13()+" " +lstPaymentDetail1.get(i).getFieldName4()); %></td>
                            </tr>
                        <%}}}else if(isIctTender && "bg".equalsIgnoreCase(request.getParameter("payTyp"))){
                            String txtCurrencySymbol = "";
                            String currencyType = "";
                            String perSecAmount = "";

                            CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> lstNwPerSec = csdms.geteGPData("getNewPerSecDetail", tenderId.toString(), pkgLotId.toString(), payUserId.toString());

                            if(!lstNwPerSec.isEmpty())
                            {
                                for(int i = 0; i<lstNwPerSec.size(); i++)
                                {
                                    perSecAmount = lstNwPerSec.get(i).getFieldName1();
                                    currencyType = lstNwPerSec.get(i).getFieldName2();
                                    txtCurrencySymbol = lstNwPerSec.get(i).getFieldName3();
                        %>
                             <tr>
                                <td class="ff">Amount (in <%=currencyType%>) :</td>
                                <td><label id="lblCurrencySymbol_<%=i%>"><%=txtCurrencySymbol%></label>&nbsp;
                                    <label id="lblAmount_<%=i%>"><%=perSecAmount%></label>
                                </td>
                            </tr>
                        <%}}}else{%>  <!-- Old Version -->
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                            </td>
                        </tr>
                        <%}%>

                        <%if("Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                            <tr>
                                <td class="ff" >Service Charge :</td>
                                <td><label>Taka</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName15() %></td>
                            </tr>
                            <tr>
                                <td class="ff" >Total Amount :</td>
                                <td><label>Taka</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName16() %></td>
                            </tr>
                        <%}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                 <%if(!"Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                                <tr>
                                    <td class="ff">Issuing Financial Institute :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institute Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }}
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName7()%></td>
                            </tr>
                    </table>
 </div>
                              </div>

                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>

                 <!--Dashboard Content Part End-->
                 </div>
        </div>
        </div></div>
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
        </div>
        </body>
        <script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>

</html>

