<%-- 
    Document   : EditCompanyDetail
    Created on : Jan 5, 2011, 7:38:36 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <%
        tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        boolean ifEdit = true;
        ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
        TblCompanyMaster dtBean = contentAdminService.findCompanyMaster("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId"))).get(0);
        if ("Update".equals(request.getParameter("hdnsaveNEdit"))) {
            tempCompanyMasterDtBean.setCompanyId(dtBean.getCompanyId());
            tempCompanyMasterDtBean.setUserId(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
            tempCompanyMasterDtBean.setCompanyRegNumber(dtBean.getCompanyRegNumber());
            tempCompanyMasterDtBean.setTradeLicenseNumber(dtBean.getTradeLicenseNumber());
            tempCompanyMasterDtBean.setCompanyName(dtBean.getCompanyName());
            tempCompanyMasterDtBean.setCompanyNameInBangla(dtBean.getCompanyNameInBangla());
            tempCompanyMasterDtBean.setLegalStatus(dtBean.getLegalStatus());
            tempCompanyMasterDtBean.setEstablishmentYear(dtBean.getEstablishmentYear());
            //tempCompanyMasterDtBean.setTinNo(dtBean.getTinNo());
            //tempCompanyMasterDtBean.setTinDocName(dtBean.getTinDocName());
            //tempCompanyMasterDtBean.setLicIssueDate(dtBean.getLicIssueDate());
            //tempCompanyMasterDtBean.setLicExpiryDate(dtBean.getLicExpiryDate());
            //tempCompanyMasterDtBean.setcom(dtBean.getWorkCategory());
            //tempCompanyMasterDtBean.setStatutoryCertificateNo(dtBean.getStatutoryCertificateNo());
            //tempCompanyMasterDtBean.setOriginCountry(((String)request.getParameter("originCountry")));
            if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("regOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setRegOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("regOffUpjilla"))) {
                    tempCompanyMasterDtBean.setRegOffUpjilla("");
                }
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("corpOffSubDistrict"))) {
                    tempCompanyMasterDtBean.setCorpOffSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("corpOffUpjilla"))) {
                    tempCompanyMasterDtBean.setCorpOffUpjilla("");
                }
            String qString = null;
            tendererMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            if (tendererMasterSrBean.updateTblCompanyMaster(tempCompanyMasterDtBean)) {
                qString = "&msg=s";
            } else {
                qString = "&msg=fail";
            }
            response.sendRedirect("EditCompanyDetail.jsp?cId=" + request.getParameter("cId") + "&tId=" + request.getParameter("tId") + qString);
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Company Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>       

    </head>
    <body onload="SubDistrictANDAreaCodeLoadOffice();SubDistrictANDAreaCodeLoadCorporation();">
        <form name="cmpSubmit" id="cmpSubmit" method="post"></form>        
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->                
                <div class="pageHead_1">Edit Company Details</div>
                <div class="stepWiz_1 t_space">
                    <ul>
                        <%                        String pageName = request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length - 1];
                        %>
                        <li <%if (pageName.equals("EditCompanyDetail.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditCompanyDetail.jsp")) {%><a href="EditCompanyDetail.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Details<%if (!pageName.equals("EditCompanyDetail.jsp")) {%></a><%}%></li>
                        <li <%if (pageName.equals("EditPersonalDetails.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("EditPersonalDetails.jsp")) {%><a href="EditPersonalDetails.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Company Contact Person Details<%if (!pageName.equals("EditPersonalDetails.jsp")) {%></a><%}%></li>
                        <%--<li <%if (pageName.equals("Reregistration.jsp")) {%>class="sMenu"<%}%>><%if (!pageName.equals("Reregistration.jsp")) {%><a href="Reregistration.jsp?tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>"><%}%>Apply for New Procurement Category<%if (!pageName.equals("Reregistration.jsp")) {%></a><%}%></li>--%>
                    </ul>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <form id="frmComp" method="post" action="EditCompanyDetail.jsp?cId=<%=request.getParameter("cId")%>&tId=<%=request.getParameter("tId")%>">
                                <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                                <%if ("fail".equals(request.getParameter("msg"))) {%><br/><div class="responseMsg errorMsg"  >Problem in updating Company Details.</div><%}%>
                                <%if ("y".equals(request.getParameter("cpr"))) {%><br/><div class="responseMsg errorMsg">Company Registration No. already exist</div><%}%>
                                <%if (ifEdit) {%><input type="hidden" value="y" id="ifedit"/><%}%>                                
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1"  id="tbodyHide"  width="100%">                                            

                                    <!--                                         <tr>
                                                                                <td class="ff">Work Category : </td>
                                                                                <td>
                                                                                    <select name="workCategory" class="formTxtBox_1" id="cmbCategory">
                                    <%--<%if(!ifEdit){%><option  value="">Select Work Category</option><%}%>
                                    <option value="-1" <%if (ifEdit) {if (dtBean.getWorkCategory().equals("-1")) {%>selected<%}}%>>Select</option>
                                    <option value="W1" <%if (ifEdit) {if (dtBean.getWorkCategory().equals("W1")) {%>selected<%}}%>>W1</option>
                                    <option value="W2" <%if (ifEdit) {if (dtBean.getWorkCategory().equals("W2")) {%>selected<%}}%>>W2</option>
                                    <option value="W3" <%if (ifEdit) {if (dtBean.getWorkCategory().equals("W3")) {%>selected<%}}%>>W3</option>
                                    <option value="W4" <%if (ifEdit) {if (dtBean.getWorkCategory().equals("W4")) {%>selected<%}}%>>W4</option>--%>
                                </select>
                            </td>
                        </tr>-->
                                    <%--<tr>
                                        <td class="ff" style="display: none">Nature of Business : <span>*</span></td>
                                        <td><textarea cols="10"  name="specialization" rows="3" class="formTxtBox_1" id="txtaCPV" style="width:400px;" readonly><%if (ifEdit) {
                                                out.print(dtBean.getSpecialization());
                                            }%></textarea> <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()" id="aTree">Select Category</a></td>
                                    </tr>--%>
                                    <tr>
                                        <td class="ff">Registered Office Address : <span>*</span></td>
                                        <td><textarea cols="10" name="regOffAddress" rows="3" class="formTxtBox_1" id="txtaRegAddr" style="width:400px;"><%if (ifEdit) {
                                                out.print(dtBean.getRegOffAddress());
                                            }%></textarea></td>
                                    </tr>


                                    <!--                                        <tr>
                                                                                <td class="ff"> Statutory Certificate No. : <span>*</span></td>
                                                                                <td><input name="statutoryCertificateNo" type="text" maxlength="50" class="formTxtBox_1" id="txtStatutoryCert" style="width:200px;" 
                                                                                           value="<%--<%if (ifEdit) {
                                                                                               if(dtBean.getStatutoryCertificateNo()!=null)
                                                                                        out.print(dtBean.getStatutoryCertificateNo());
                                                                                    }%>--%>"/></td>
                                                                            </tr>-->


                                    <tr>
                                        <td class="ff">Origin of Country : <span>*</span></td>
                                        <td><select name="originCountry" class="formTxtBox_1" id="cmbOrgCountry">
                                                <%
                                                    String originCountry = "Bhutan";
                                                    if (ifEdit) {
                                                        originCountry = dtBean.getOriginCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(originCountry)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="regOffCountry" class="formTxtBox_1" id="cmbRegCountry">
                                                <%
                                                    String countryReg = "Bhutan";
                                                    if (ifEdit) {
                                                        countryReg = dtBean.getRegOffCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryReg)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td><select name="regOffState" class="formTxtBox_1" id="cmbRegState" style="width:218px;"  >
                                                <!--Change Bangladesh to Bhutan,Proshanto-->
                                                <%String regCntName = "";
                                                    if (ifEdit) {
                                                        regCntName = dtBean.getRegOffCountry();
                                                    } else {
                                                        regCntName = "Bhutan";
                                                    }
                                                    for (SelectItem state : tendererMasterSrBean.getStateList(regCntName)) {%>
                                                        <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                                if (state.getObjectValue().equals(dtBean.getRegOffState())) {%>selected<%}
                                                                            //Change Dhaka to Thimphu,proshanto
                                                                        } else if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>

                                    <tr id="trRSubdistrict">
                                        <td class="ff" height="30">Dungkhag / Sub-district : </td>
                                        <td><select name="regOffSubDistrict" class="formTxtBox_1" id="cmbRegSubdistrict" style="width:218px;">
                                                <%String regsubdistrictName = "";
                                                    if (ifEdit) {
                                                        regsubdistrictName = dtBean.getRegOffState();
                                                    } else {
                                                        regsubdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regsubdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getRegOffSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                        } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                        selected<%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr id="trRthana">
                                        <td class="ff" height="30">Gewog : </td>
                                        <td><select name="regOffUpjilla" class="formTxtBox_1" id="txtRegThana" style="width:218px;">
                                                <%String regState = "Thimphu";
                                                String regSubdistrict = "";
                                                    if (ifEdit) {
                                                        regState = dtBean.getRegOffState();
                                                        regSubdistrict = dtBean.getRegOffSubDistrict();
                                                    }
                                                    if(!regState.isEmpty() && !regSubdistrict.isEmpty()){
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(regSubdistrict)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                        %>selected<%
                                                            }
                                                        } else if (gewoglist.getObjectValue().equals("")) {%>
                                                        selected<%
                                                            }%>><%=gewoglist.getObjectValue()%></option>
                                                <%}}
                                                else {
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(regState)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                        %>selected<%
                                                            }
                                                        } %>

                                                            ><%=gewoglist.getObjectValue()%></option>
                                                <%}}
                                                %>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town : </td>
                                        <td><input name="regOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtRegCity" style="width:200px;" value="<%if (ifEdit) {
                                                if (dtBean.getRegOffCity() != null) {
                                                    out.print(dtBean.getRegOffCity());
                                                }
                                            }%>"/></td>
                                    </tr>
                                            <%--<tr id="trRthana">
                                        <td class="ff">Gewog : </td>
                                        <td><input name="regOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtRegThana" style="width:200px;"  value="<%if (ifEdit) {
                                                out.print(dtBean.getRegOffUpjilla());
                                            }%>"/><div id="spRthana" style="color: red;"></div></td>
                                    </tr>--%>
                                    <!--Change Bangladesh to Bhutan,Proshanto-->
                                    <%if (ifEdit) {
                                            if (!dtBean.getRegOffCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trRthana').css("display", "none");
                                        $('#trRSubdistrict').css("display", "none");
                                    </script>
                                    <%}
                                        }%>
                                    <tr>
                                        <td class="ff">Post Code: <!--span>*</span--></td>
                                        <td><input name="regOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtRegPost" style="width:200px;"  value="<%if (ifEdit) {
                                                out.print(dtBean.getRegOffPostcode());
                                            }%>"/></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Mobile No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" style="width: 30px" id="regMobCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                if (ifEdit) {
                                                    out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input name="regOffMobileNo" type="text" class="formTxtBox_1" maxlength="20" id="txtregOffMobileNo" style="width:160px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getRegOffMobileNo() != null) {
                                                            out.print(dtBean.getRegOffMobileNo());
                                                        }
                                                    }%>"/>
                                            <span id="regmobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Phone No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="regPhoneCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                            } else {
                                                //Change Bangladesh to Bhutan,Proshanto
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                    if (dtBean.getRegOffPhoneNo() != null) {
                                                        out.print(dtBean.getRegOffPhoneNo().split("-")[0]);
                                                    }
                                                }%>" id="regPhoneSTD" name="regPhoneSTD" class="formTxtBox_1" style="width:50px;" readonly />-<input name="regOffPhoneNo" type="text" maxlength="20" class="formTxtBox_1" id="txtRegPhone" style="width:100px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getRegOffPhoneNo() != null && !dtBean.getRegOffPhoneNo().isEmpty()) {
                                                            out.print(dtBean.getRegOffPhoneNo().split("-")[1]);
                                                        }
                                                    }%>"/><input type="hidden" id="hdnvalue" name="hdnvalue" /><span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="regFaxCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                            } else {
                                                //Change Bangladesh to Bhutan,Proshanto
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                    if (dtBean.getRegOffFaxNo() != null) {
                                                        if (dtBean.getRegOffFaxNo().contains("-")) {
                                                            out.print(dtBean.getRegOffFaxNo().split("-")[0]);
                                                        }
                                                    }
                                                }%>" id="regFaxSTD" name="regFaxSTD" class="formTxtBox_1" style="width:50px;" readonly/>-<input name="regOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtRegFax"  style="width:100px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getRegOffFaxNo() != null && !dtBean.getRegOffFaxNo().isEmpty()) {
                                                            if (dtBean.getRegOffFaxNo().contains("-")) {
                                                                out.print(dtBean.getRegOffFaxNo().split("-")[1]);
                                                            }
                                                        }
                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td>
                                            <input name="chkRC" type="checkbox" id="chkRC" style="width:200px;"/>
                                            <div class="formNoteTxt">(Tick if Registered and Corporate office details are same)</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Corporate / Head office<br />
                                            Address : <span>*</span></td>
                                        <td><textarea cols="10" name="corpOffAddress" rows="3" class="formTxtBox_1" id="txtaCorpAddr" style="width:400px;"><%if (ifEdit) {
                                                out.print(dtBean.getCorpOffAddress());
                                            }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="corpOffCountry" class="formTxtBox_1" id="cmbCorpCountry">
                                                <%
                                                    //Code by Proshanto
                                                    String countryCorp = "Bhutan";//Bangladesh
                                                    if (ifEdit) {
                                                        countryCorp = dtBean.getCorpOffCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryCorp)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td><select name="corpOffState" class="formTxtBox_1" id="cmbCorpState" style="width:218px;" >
                                                <!--Code by Proshanto,Change Bangladesh to Bhutan-->
                                                <%String corpCntName = "";
                                                    if (ifEdit) {
                                                        corpCntName = dtBean.getCorpOffCountry();
                                                    } else {
                                                        corpCntName = "Bhutan";
                                                    }
                                                    for (SelectItem state : tendererMasterSrBean.getStateList(corpCntName)) {%>
                                                        <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                                if (state.getObjectValue().equals(dtBean.getCorpOffState())) {%>selected<%}
                                                            //Change Dhaka to Thimphu,proshanto
                                                        } else if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>

                                    <tr id="trCSubdistrict">
                                        <td class="ff" height="30">Dungkhag / Sub-district : </td>
                                        <td><select name="corpOffSubDistrict" class="formTxtBox_1" id="cmbCorpSubdistrict" style="width:218px;">
                                                <% String corpsubdistrictName = "";
                                                    if (ifEdit) {
                                                        corpsubdistrictName = dtBean.getCorpOffState();
                                                    } else {
                                                        corpsubdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(corpsubdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getCorpOffSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                        } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                        selected<%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr id="trCthana">
                                        <td class="ff" height="30">Gewog : </td>
                                        <td><select name="corpOffUpjilla" class="formTxtBox_1" id="txtCorpThana" style="width:218px;">
                                                <%String corpState = "Thimphu";
                                                String corpSubdistrict = "";
                                                    if (ifEdit) {
                                                        corpState = dtBean.getCorpOffState();
                                                        corpSubdistrict = dtBean.getCorpOffSubDistrict();
                                                    }
                                                    if(!corpState.isEmpty() && !corpSubdistrict.isEmpty()){
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(corpSubdistrict)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getCorpOffUpjilla())) {
                                                        %>selected<%
                                                            }
                                                        } else if (gewoglist.getObjectValue().equals("")) {%>
                                                        selected<%
                                                            }%>><%=gewoglist.getObjectValue()%></option>
                                                <%}}
                                                else {
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(corpState)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getCorpOffUpjilla())) {
                                                        %>selected<%
                                                            }
                                                        } %>

                                                            ><%=gewoglist.getObjectValue()%></option>
                                                <%}}
                                                %>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town : </td>
                                        <td><input name="corpOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpCity" style="width:200px;"   value="<%if (ifEdit) {
                                                if (dtBean.getCorpOffCity() != null) {
                                                    out.print(dtBean.getCorpOffCity());
                                                }
                                            }%>"/></td>
                                    </tr>
                                    <%--<tr id="trCthana">
                                        <td class="ff">Gewog  : </td>
                                        <td><input name="corpOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpThana" style="width:200px;"   value="<%if (ifEdit) {
                                                if (dtBean.getCorpOffUpjilla() != null) {
                                                    out.print(dtBean.getCorpOffUpjilla());
                                                }
                                            }%>"/><div id="spCthana" style="color: red;"></div></td>
                                    </tr>--%>
                                    <!--Change Bangladesh to Bhutan,Proshanto-->
                                    <%if (ifEdit) {
                                            if (!dtBean.getCorpOffCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trCthana').css("display", "none");
                                        $('#trCSubdistrict').css("display", "none");
                                    </script>
                                    <%}
                                        }%>
                                    <tr>
                                        <td class="ff">Post Code : <!--span>*</span--></td>
                                        <td><input name="corpOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtCorpPost" style="width:200px;"   value="<%if (ifEdit) {
                                                out.print(dtBean.getCorpOffPostcode());
                                            }%>"/></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Mobile No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" style="width: 30px" id="corpMobCode" value="<%
                                                if (ifEdit) {
                                                    out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input name="corpOffMobMobileNo" type="text" class="formTxtBox_1" maxlength="20" id="txtcorpOffMobileNo" style="width:160px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getCorpOffMobMobileNo() != null) {
                                                            out.print(dtBean.getCorpOffMobMobileNo());
                                                        }
                                                    }%>"/>
                                            <span id="corpmobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Phone No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="corpPhoneCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                            } else {
                                                //Change Bangladesh to Bhutan,Proshanto
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                    if (dtBean.getCorpOffPhoneno() != null) {
                                                        out.print(dtBean.getCorpOffPhoneno().split("-")[0]);
                                                    }
                                                }%>" id="corpPhoneSTD" name="corpPhoneSTD" class="formTxtBox_1" style="width:50px;" readonly />-<input name="corpOffPhoneno" type="text" maxlength="20" class="formTxtBox_1" id="txtCorpPhone"  style="width:100px;"   value="<%if (ifEdit) {
                                                        if (dtBean.getCorpOffPhoneno() != null && !dtBean.getCorpOffPhoneno().isEmpty()) {
                                                            out.print(dtBean.getCorpOffPhoneno().split("-")[1]);
                                                        }
                                                    }%>"/>
                                            <input type="hidden" id="hdnvaluec" name="hdnvaluec" />
                                            <span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span>
                                            <span id="scorpPhoneSTD" class='reqF_1'></span>
                                            <span id="stxtCorpPhone" class='reqF_1'></span>                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="corpFaxCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                            } else {
                                                //Change Bangladesh to Bhutan,Proshanto
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                    if (dtBean.getCorpOffFaxNo() != null && !dtBean.getCorpOffFaxNo().isEmpty()) {
                                                        if (dtBean.getCorpOffFaxNo().contains("-")) {
//                                                            if (dtBean.getCorpOffPhoneno() != null ) {
                                                                out.print(dtBean.getCorpOffFaxNo().split("-")[0]);
//                                                            }
                                                        }
                                                    }
                                                }%>" id="corpFaxSTD" name="corpFaxSTD" class="formTxtBox_1" style="width:50px;" readonly />-<input name="corpOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtCorpFax"  style="width:100px;"  value="<%if (ifEdit) {
                                                                        if (dtBean.getCorpOffFaxNo() != null && !dtBean.getCorpOffFaxNo().isEmpty()) {
                                                                            if (dtBean.getCorpOffFaxNo().contains("-")) {
                                                                                out.print(dtBean.getCorpOffFaxNo().split("-")[1]);
                                                                            }
                                                                        }
                                                                    }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Company's website : </td>
                                        <td><input name="website" type="text" maxlength="100" class="formTxtBox_1" id="txtWeb" style="width:200px;"   value="<%if (ifEdit) {
                                            if(dtBean.getWebsite()!=null)
                                            {
                                            out.print(dtBean.getWebsite());}
                                            }%>"/></td>
                                    </tr>
                                    <tr><td>&nbsp;</td>
                                        <td>                                                
                                            <label class="formBtn_1">
                                                <input type="submit" name="saveNEdit" id="btnUpdate" value="Update"/>
                                            </label>
                                            <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value=""/>                                                
                                        </td>
                                    </tr>                                    
                                </table>                                                            
                            </form>                            
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp"/>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#frmComp").validate({
                rules: {
                    specialization: {required: true},
                    regOffAddress: {required: true, maxlength: 250},
                    regOffCity: {maxlength: 50, DigAndNum: true},
                    //regOffUpjilla: {maxlength: 50, alphaNationalId: true},
                    regOffPostcode: {/*required: true,*/number: true, minlength: 4},
                    regOffPhoneNo: {maxlength: 20, digits: true},
                    regOffMobileNo: {required: true, digits: true, minlength: 8, maxlength: 8},
                    regPhoneSTD: {maxlength: 10, digits: true},
                    regOffFaxNo: {maxlength: 20, digits: true},
                    regFaxSTD: {maxlength: 10, digits: true},
                    corpOffAddress: {required: true, maxlength: 250},
                    corpOffCity: {maxlength: 50, DigAndNum: true},
                    //corpOffUpjilla: {maxlength: 50, alphaNationalId: true},
                    corpOffPostcode: {/*required: true,*/number: true, minlength: 4},
                    corpOffMobMobileNo: {required: true, digits: true, minlength: 8, maxlength: 8},
                    corpOffPhoneno: {maxlength: 20, digits: true},
                    corpPhoneSTD: {maxlength: 10, digits: true},
                    corpOffFaxNo: {maxlength: 20, digits: true},
                    corpFaxSTD: {maxlength: 10, digits: true},
                    website: {url: true}
                },
                messages: {
                    specialization: {required: "<div class='reqF_1'>Please Enter Nature of Business.</div>"
                    },
                    regOffAddress: {required: "<div class='reqF_1'>Please Enter Registered office Address.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 150 Characters are allowed.</div>"
                    },
                    regOffCity: {//required: "<div class='reqF_1'>Please Enter City / Town.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 50 Characters are allowed.</div>",
                        DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only.</div>"
                    },
                    regOffUpjilla: {
                        //maxlength: "<div class='reqF_1'>Maximum 50 Characters are allowed.</div>",
                        //alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
                    },
                    regOffPostcode: {/*required: "<div class='reqF_1'>Please Enter Post Code / Zip Code.</div>",*/
                        number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                        maxlength: "<div class='reqF_1'>Minimum 4 Digits are required.</div>"
                    },
                    regOffMobileNo: {required: "<div class='reqF_1'>Please Enter Mobile No.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        minlength: "<div class='reqF_1'>Exactly 8 digits required</div>",
                        maxlength: "<div class='reqF_1'>Exactly 8 digits required</div>"
                    },
                    regOffPhoneNo: {//required: "<div class='reqF_1'>Please Enter Phone Number.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed.</div>"
                    },
                    regPhoneSTD: {//required: "<div class='reqF_1'>Please Enter STD code.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed.</div>"
                    },
                    regOffFaxNo: {digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed.</div>"
                    },
                    regFaxSTD: {digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed.</div>"
                    },
                    corpOffAddress: {required: "<div class='reqF_1'>Please Enter Corporate office Address.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 150 Characters are allowed.</div>"
                    },
                    corpOffCity: {//required: "<div class='reqF_1'>Please Enter City / Town.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 50 Characters are allowed.</div>",
                        DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only.</div>"
                    },
                    corpOffUpjilla: {
                        //maxlength: "<div class='reqF_1'>Maximum 50 Characters are allowed.</div>",
                        //alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
                    },
                    corpOffPostcode: {/*required: "<div class='reqF_1'>Please Enter Post Code / Zip Code.</div>",*/
                        number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                        minlength: "<div class='reqF_1'>Minimum 4 Digits are required.</div>"
                    },
                    corpOffMobMobileNo: {required: "<div class='reqF_1'>Please Enter Mobile No.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        minlength: "<div class='reqF_1'>Exactly 8 digits required</div>",
                        maxlength: "<div class='reqF_1'>Exactly 8 digits required</div>"
                        
                    },
                    corpOffPhoneno: {//required: "<div class='reqF_1'>Please Enter Phone Number.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed.</div>"
                    },
                    corpPhoneSTD: {//required: "<div class='reqF_1'>Please Enter STD code.</div>",
                        digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed.</div>"
                    },
                    corpOffFaxNo: {digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed.</div>"
                    },
                    corpFaxSTD: {digits: "<div class='reqF_1'>Please Enter Numbers Only</div>",
                        maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed.</div>"
                    },
                    website: {url: "<div class='reqF_1'>Please Enter Valid Website Name</div>"
                    }
                },
                errorPlacement: function (error, element) {
                    if (element.attr("name") == "specialization") {
                        error.insertAfter("#aTree");
                    } else {
                        error.insertAfter(element);
                    }
                    if (element.attr("name") == "regPhoneSTD") {
                        error.insertAfter("#txtRegPhone");
                    }
                    if (element.attr("name") == "regFaxSTD") {
                        error.insertAfter("#txtRegFax");
                    }
                    if (element.attr("name") == "corpPhoneSTD") {
                        error.insertAfter("#txtCorpPhone");
                    }
                    if (element.attr("name") == "corpFaxSTD") {
                        error.insertAfter("#txtCorpFax");
                    }
                }

            });
        });
    </script>
    <script type="text/javascript">
//        $(function () {
//            $('#cmbRegState').change(function () {
//                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue'}, function (j) {
//                    $('#cmbRegSubDistrict').html(j);
//                });
//            });
//        });
        function SubDistrictANDAreaCodeLoadOffice()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                //alert($('#cmbRegSubdistrict').val());
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbRegSubdistrict').val()}, function (j) {
                    $('#cmbRegSubdistrict').html(j);
                });
            if($('#cmbRegSubdistrict').val()=="Phuentsholing")
            {
                $('#regPhoneSTD').val('05');
                $('#regFaxSTD').val('05');
            }
            else{
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                    $('#regPhoneSTD').val(j);
                    $('#regFaxSTD').val(j);
                });}
            }
        function SubDistrictANDAreaCodeLoadCorporation()
        {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
            //alert($('#cmbCorpSubdistrict').val());
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbCorpSubdistrict').val()}, function (j) {
                $('#cmbCorpSubdistrict').html(j);
            });
            if($('#cmbCorpSubdistrict').val()=="Phuentsholing")
            {
                $('#corpPhoneSTD').val('05');
                $('#corpFaxSTD').val('05');
            }
            else
            {
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                $('#corpPhoneSTD').val(j);
                $('#corpFaxSTD').val(j);
            });
        }
        }
       
//        $(function () {
//            $('#cmbCorpState').change(function () {
//                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue'}, function (j) {
//                    $('#cmbCorpSubDistrict').html(j);
//                });
//            });
//        });
        $(function () {
            $('#cmbRegCountry').change(function () {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegCountry').val(), funName: 'stateComboValue'}, function (j) {
                    $("select#cmbRegState").html(j);
                    //Code by Proshanto
                    if ($('#cmbRegCountry').val() == "150") {//136
                        $('#trRthana').css("display", "table-row")
                        $('#trRSubdistrict').css("display", "table-row")
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#regPhoneSTD').val(j);
                                $('#regFaxSTD').val(j);
                            });
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtRegThana').html(j);
                        });
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                            $('#cmbRegSubdistrict').html(j);
                        });
                    } else {
                        $('#trRthana').css("display", "none")
                        $('#trRSubdistrict').css("display", "none")
                    }
                    
                    $('#txtRegCity').val(null);
                    $('#txtRegPost').val(null);
                    $('#txtregOffMobileNo').val(null);
                    $('#txtRegPhone').val(null);
                    $('#txtRegFax').val(null);
                    $('#regPhoneSTD').val(null);
                    $('#regFaxSTD').val(null);
                    $('#txtRegThana').val(null);
                });
            });
        });
        $(function () {
            $('#cmbCorpCountry').change(function () {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpCountry').val(), funName: 'stateComboValue'}, function (j) {
                    $("select#cmbCorpState").html(j);
                    //Code by Proshanto
                    if ($('#cmbCorpCountry').val() == "150") {//136
                        $('#trCthana').css("display", "table-row")
                        $('#trCSubdistrict').css("display", "table-row")
                        
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#corpPhoneSTD').val(j);
                                $('#corpFaxSTD').val(j);
                            });
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtCorpThana').html(j);
                        });
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                            $('#cmbCorpSubdistrict').html(j);
                        });
                    } else {
                        $('#trCthana').css("display", "none")
                        $('#trCSubdistrict').css("display", "none")
                    }
                    
                    $('#txtCorpCity').val(null);
                    $('#txtCorpPost').val(null);
                    $('#txtcorpOffMobileNo').val(null);
                    $('#txtCorpPhone').val(null);
                    $('#txtCorpFax').val(null);
                    $('#corpPhoneSTD').val(null);
                    $('#corpFaxSTD').val(null);
                    $('#txtCorpThana').val(null);
                });
            });
        });
        
        $(function () {
                $('#cmbRegState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbRegSubdistrict').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#regPhoneSTD').val(j);
                        $('#regFaxSTD').val(j);
                    });
                });
                
                $('#cmbCorpSubdistrict').change(function () {
                        //alert($('#cmbCorpSubdistrict').val());
                        if($('#cmbCorpSubdistrict').val()=='Phuentsholing')
                        {$('#corpPhoneSTD').val('05');
                        $('#corpFaxSTD').val('05');}
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                            $('#corpPhoneSTD').val(j);
                            $('#corpFaxSTD').val(j);
                        });
                    }
                    });
                    
                    $('#cmbRegSubdistrict').change(function () {
                        //alert($('#cmbRegSubdistrict').val());
                        if($('#cmbRegSubdistrict').val()=='Phuentsholing')
                        {$('#regPhoneSTD').val('05');
                        $('#regFaxSTD').val('05');}
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                            $('#regPhoneSTD').val(j);
                            $('#regFaxSTD').val(j);
                        });
                    }
                    });
            });
            $(function () {
                $('#cmbRegSubdistrict').change(function () {
                    if($('#cmbRegSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtRegThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtRegThana').html(j);
                        });
                    }
                });
            });
            
            $(function () {
                $('#cmbCorpState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtCorpThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                    $('#cmbCorpSubdistrict').html(j);
                });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#corpPhoneSTD').val(j);
                        $('#corpFaxSTD').val(j);
                    });
                });
            });
            $(function () {
                $('#cmbCorpSubdistrict').change(function () {
                    if($('#cmbCorpSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtCorpThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtCorpThana').html(j);
                        });
                    }
                });
            });
        $(function () {
            $('#frmComp').submit(function () {
                //Code by Proshanto
                if ($('#cmbRegCountry').val() == "150") {//136
                    if ($.trim($('#txtRegThana').val()) == "") {
//                            $('#spRthana').html("Please Enter Gewog ");
//                            $('#txtRegThana').focus();
//                             return false;
                        //return true;
                    } else {
                        $('#spRthana').html(null);
                    }
                }
                //Code by Proshanto
                if ($('#cmbCorpCountry').val() == "150") {//136
                    if ($.trim($('#txtCorpThana').val()) == "") {
//                        $('#spCthana').html("Please Enter Gewog ");
//                        $('#txtCorpThana').focus();
//                        return false;
                        //return true;
                    } else {
                        $('#spCthana').html(null);
                    }
                }
                if ($('#frmComp').valid()) {
                    $('#btnUpdate').attr("disabled", "true");
                    $('#hdnsaveNEdit').val("Update");
                }
            });
        });

        /*$(function() {
         $('#chkRC').click(function() {
         if($('#chkRC').attr("checked")){
         $('#txtaCorpAddr').val($('#txtaRegAddr').val());
         $('#txtCorpCity').val($('#txtRegCity').val());
         $('#txtCorpThana').val($('#txtRegThana').val());
         $('#txtCorpPost').val($('#txtRegPost').val());
         $('#corpPhoneSTD').val($('#regPhoneSTD').val());
         $('#txtCorpPhone').val($('#txtRegPhone').val());
         $('#corpFaxSTD').val($('#regFaxSTD').val());
         $('#txtCorpFax').val($('#txtRegFax').val());
         $('#cmbCorpCountry').val($('#cmbRegCountry').val());
         if($('#cmbRegCountry').val()!="136"){
         $('#trCthana').css("display","none")
         }
         $('#cmbCorpState').html($('#cmbRegState').html());
         }else{
         $('#txtaCorpAddr').val(null);
         $('#txtCorpCity').val(null);
         $('#txtCorpThana').val(null);
         $('#txtCorpPost').val(null);
         $('#corpPhoneSTD').val(null);
         $('#txtCorpPhone').val(null);
         $('#corpFaxSTD').val(null);
         $('#txtCorpFax').val(null);
         }
         });
         });*/
        $(function () {
            $('#chkRC').click(function () {
                if ($('#chkRC').attr("checked")) {
                    $('#txtaCorpAddr').val($('#txtaRegAddr').val());
                    $('#txtCorpCity').val($('#txtRegCity').val());
                    $('#txtCorpThana').val($('#txtRegThana').val());
                    $('#txtCorpPost').val($('#txtRegPost').val());
                    $('#corpPhoneSTD').val($('#regPhoneSTD').val());
                    $('#txtCorpPhone').val($('#txtRegPhone').val());
                    $('#corpFaxSTD').val($('#regFaxSTD').val());
                    $('#txtCorpFax').val($('#txtRegFax').val());
                    $('#cmbCorpCountry').val($('#cmbRegCountry').val());
                    $('#cmbCorpSubdistrict').html($('#cmbRegSubdistrict').html());
                    $('#corpPhoneCode').val($('#regPhoneCode').val());
                    $('#corpFaxCode').val($('#regFaxCode').val());
                    $('#txtaCorpAddr').attr("readonly", "true");
                    $('#corpPhoneSTD').attr("readonly", "true");
                    $('#corpFaxSTD').attr("readonly", "true");
                    $('#txtCorpCity').attr("readonly", "true");
                    $('#txtCorpThana').attr("readonly", "true");
                    $('#txtCorpPost').attr("readonly", "true");
                    $('#corpPhoneCode').attr("readonly", "true");
                    $('#corpFaxCode').attr("readonly", "true");
                    $('#txtCorpPhone').attr("readonly", "true");
                    $('#txtCorpFax').attr("readonly", "true");
                    $('#txtcorpOffMobileNo').attr("readonly", "true");
                    $('#txtcorpOffMobileNo').val($('#txtregOffMobileNo').val());
                    $('#cmbCorpSubDistrict').val($('#cmbRegSubDistrict').val());
                    //cmbCorpSubDistrict
                    //Code by Proshanto
                    if ($('#cmbRegCountry').val() != "150") {//136
                        $('#trCthana').css("display", "none")
                        $('#trCSubdistrict').css("display", "none")        
                
                    }
                    $('#cmbCorpState').html($('#cmbRegState').html());
                    for (var i = 0; i < document.getElementById('cmbRegState').options.length; i++) {
                        if (document.getElementById('cmbRegState').options[i].selected) {
                            document.getElementById('cmbCorpState').options[i].selected = "selected";
                        } else {
                            document.getElementById('cmbCorpState').options[i].disabled = "true";
                        }
                    }
                    
                    for (var i = 0; i < document.getElementById('cmbRegSubdistrict').options.length; i++) {
                        if (document.getElementById('cmbRegSubdistrict').options[i].selected) {
                            document.getElementById('cmbCorpSubdistrict').options[i].selected = "selected";
                        } else {
                            document.getElementById('cmbCorpSubdistrict').options[i].disabled = "true";
                        }
                    }
                    $('#txtCorpThana').html($('#txtRegThana').html());
                    for (var i = 0; i < document.getElementById('txtRegThana').options.length; i++) {
                        if (document.getElementById('txtRegThana').options[i].selected) {
                            document.getElementById('txtCorpThana').options[i].selected = "selected";
                        } else {
                            document.getElementById('txtCorpThana').options[i].disabled = "true";
                        }
                    }
                    
                    for (var i = 0; i < document.getElementById('cmbRegCountry').options.length; i++) {
                        if (document.getElementById('cmbRegCountry').options[i].selected) {
                            document.getElementById('cmbCorpCountry').options[i].selected = "selected";
                        } else {
                            document.getElementById('cmbCorpCountry').options[i].disabled = "true";
                        }
                    }

                    if ($.trim($('#txtaRegAddr').val()) == "") {
                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
                        bol = false;
                    } else
                    {
                        if (document.getElementById("txtaRegAddr").value.length > 150) {
                            $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                            bol = false;
                        } else
                        {
                            $('#stxtaCorpAddr').html('');
                        }
                    }


                    if ($.trim($('#txtRegCity').val()) == "") {
                        $('#stxtCorpCity').html('Please enter City / Town');
                        bol = false;
                    } else
                    {
                        if ($.trim(document.getElementById("txtRegCity").value.length) > 50) {
                            $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                            bol = false;
                        } else
                        {
                            if (!DigAndNum(document.getElementById("txtRegCity").value)) {
                                $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                            } else {
                                $('#stxtCorpCity').html('');
                            }
                        }
                    }

                    if ($.trim($('#regPhoneSTD').val()) == "") {
//                        $('#scorpPhoneSTD').html('Please enter Area Code.');
//                        bol = false;
                        bol = true;
                    } else
                    {
                        if ($.trim(document.getElementById("regPhoneSTD").value.length) > 10) {
                            $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                            bol = false;
                        } else
                        {

                            if (!digits(document.getElementById("regPhoneSTD").value)) {
                                $('#scorpPhoneSTD').html("Please enter Numbers only");
                            } else {
                                $('#scorpPhoneSTD').html('');
                            }
                        }
                    }

                    if ($.trim($('#txtRegPhone').val()) == "") {
//                        $('#stxtCorpPhone').html('Please enter Phone No.');
//                        bol = false;
                        bol = true;
                    } else
                    {
                        if ($.trim(document.getElementById("txtRegPhone").value.length) > 8) {
                            $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                            bol = false;
                        } else
                        {

                            if (!digits(document.getElementById("txtRegPhone").value)) {
                                $('#stxtCorpPhone').html("Please enter Numbers Only");
                            } else {
                                $('#stxtCorpPhone').html('');
                            }
                        }
                    }

                    if ($.trim($('#txtRegThana').val()) == "") {
                        //$('#spCthana').html("Please enter Gewog ");
                        //return true;
                    } else
                    {
                        $('#spCthana').html(null);
                    }



                } else {
                    for (var i = 0; i < document.getElementById('cmbCorpCountry').options.length; i++) {
                        document.getElementById('cmbCorpCountry').options[i].disabled = false;
                        //Code by Proshanto
                        if (document.getElementById('cmbCorpCountry').options[i].value == "150") {//136
                            document.getElementById('cmbCorpCountry').options[i].selected = "selected";
                        }
                    }
                    //Code by Proshanto
                    onTickCountry("150");//136
                    $('#txtaCorpAddr').val(null);
                    $('#txtCorpCity').val(null);
                    $('#txtCorpThana').val(null);
                    $('#txtCorpPost').val(null);
                    $('#corpPhoneSTD').val(null);
                    $('#txtCorpPhone').val(null);
                    $('#corpFaxSTD').val(null);
                    $('#txtCorpFax').val(null);
                    $('#cmbCorpSubdistrict').val(null);
                    $('#txtcorpOffMobileNo').val(null);
                    $('#txtcorpOffMobileNo').removeAttr("readonly");
                    $('#cmbCorpSubdistrict').removeAttr("readonly");
                    $('#txtaCorpAddr').removeAttr("readonly");
                    $('#corpPhoneSTD').removeAttr("readonly");
                    $('#corpFaxSTD').removeAttr("readonly");
                    $('#txtCorpCity').removeAttr("readonly");
                    $('#txtCorpThana').removeAttr("readonly");
                    $('#txtCorpPost').removeAttr("readonly");
                    $('#corpPhoneCode').removeAttr("readonly");
                    $('#corpFaxCode').removeAttr("readonly");
                    $('#txtCorpPhone').removeAttr("readonly");
                    $('#txtCorpFax').removeAttr("readonly");
                }


            });
        });
        function onTickCountry(data) {
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: data, funName: 'stateComboValue'}, function (j) {
                $("select#cmbCorpState").html(j);
                //Code by Proshanto
                if ($('#cmbCorpCountry').val() == "150") {//136
                    $('#trCthana').css("display", "table-row");
                    $('#trCSubdistrict').css("display", "table-row");
                } else {
                    $('#trCthana').css("display", "none");
                    $('#trCSubdistrict').css("display", "none");
                }
            });
            $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCorpCountry').val(), funName: 'countryCode'}, function (j) {
                $('#corpPhoneCode').val(j.toString());
                $('#corpFaxCode').val(j.toString());
                $('#corpMobCode').val(j.toString());
            });
            
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                $('#cmbCorpSubdistrict').html(j);
            });
                
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                $('#txtCorpThana').html(j);
            });
            
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: 'Bumthang', funName: 'AreaCodeSelection'}, function (j) {
                $('#corpPhoneSTD').val(j);
                $('#corpFaxSTD').val(j);
            });
        }
        function DigAndNum(value) {
            return /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-.&,\(\)\']+)?)+$/.test(value);
        }

        function alphanumeric(value) {
            return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
        }

        function digits(value) {
            return /^\d+$/.test(value);
        }

        <%--For emptying thana msg on blur--%>
//        $(function () {
//            $('#txtRegThana').blur(function () {
//                if ($.trim($('#txtRegThana').val()) != "") {
//                    if ($('#spRthana').html() == "Please Enter Gewog.") {
//                        $('#spRthana').html(null);
//                    }
//                }
//            });
//        });
//        $(function () {
//            $('#txtCorpThana').blur(function () {
//                if ($.trim($('#txtCorpThana').val()) != "") {
//                    if ($('#spCthana').html() == "Please Enter Gewog") {
//                        $('#spCthana').html(null);
//                    }
//                }
//            });
//        });
        <%--that msg ends--%>
        $(function () {
            $('#cmbCorpCountry').change(function () {
                $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCorpCountry').val(), funName: 'countryCode'}, function (j) {
                    $('#corpPhoneCode').val(j.toString());
                    $('#corpFaxCode').val(j.toString());
                    $('#corpMobCode').val(j.toString());
                });
            });
        });
        $(function () {
            $('#cmbRegCountry').change(function () {
                $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbRegCountry').val(), funName: 'countryCode'}, function (j) {
                    $('#regPhoneCode').val(j.toString());
                    $('#regFaxCode').val(j.toString());
                    $('#regMobCode').val(j.toString());
                });
            });
        });
        function loadCPVTree()
        {
            window.open('../resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
        }
    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
