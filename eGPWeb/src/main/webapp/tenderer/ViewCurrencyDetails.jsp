<%--
    Document   : ViewCurrencyDetails
    Created on : Sep 2, 2012, 12:35:19 PM
    Author     : khaled
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
            // Variable tenderId is defined by u on ur current page.
            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Currency Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">Currency Details <span style="float:right;"><a href="ResultSharingTenderer.jsp?tenderId=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                int iTenderId = Integer.parseInt(request.getParameter("tenderid"));

                Date lastUpdateDate = tenderCurrencyService.getLastDateOfCurrencyExchangeRates(iTenderId);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, d MMM, yyyy HH:mm:ss");

            %>
            <div class="tableHead_1 t_space" style="margin-top: 3px;">Currency conversion rate as of <%= dateFormatter.format(lastUpdateDate) %></div>
            <div class="tabPanelArea_1 ">
                <table id="tblCurrency" width="100%" cellspacing="0" class="tableList_1">
                    <thead>
                        <tr>
                            <th width="15%" class="t-align-center">No.</th>
                            <th width="43%" class="t-align-center">Currency Name</th>
                            <th width="42%" class="t-align-center">Conversion Rate in Nu.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            String currencyRows = ""; //will hold the result

                            List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                            listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(iTenderId);

                            if(listCurrencyObj.size() > 0){
                                int rowNo = 0;
                                for(Object[] obj :listCurrencyObj){
                                    rowNo +=1;
                                    currencyRows+= "<tr>";
                                    currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                                    currencyRows+= "<td width='43%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                    String str = obj[1].toString();
                                    if(Double.valueOf(str).doubleValue() == 0){
                                        currencyRows+= "<td width='42%' class='t-align-center'></td>";
                                    }
                                    else {
                                        currencyRows+= "<td width='42%' class='t-align-center'>" + obj[1] + "</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                    }
                                }

                                    currencyRows+= "</tr>";
                                }
                            out.print(currencyRows);
                        %>
                    </tbody>
                </table>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
</html>