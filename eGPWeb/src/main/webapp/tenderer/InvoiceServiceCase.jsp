<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <%
     String tenderId = "";
     if(request.getParameter("tenderId")!=null)
                        {
                            tenderId = request.getParameter("tenderId");
                            pageContext.setAttribute("tenderId", tenderId);
                        }  
    %>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Progress Report</div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>            
            <%
                        pageContext.setAttribute("tab", "15");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                       
                                             
                        String lotId = "";
                        String wpId = "";
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                        TblTenderMaster tblTenderMaster = tenderService.getTenderMasterDetails(Integer.parseInt(tenderId));
                        TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");                                    
                        List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",""+tblTenderMaster.getCreatedBy(),request.getParameter("tenderId"));                        
                        List<Object[]> objBusinessRule = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                        CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                        CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                        List<Object[]> lotObj = cs.getLotDetails(tenderId);
                        Object[] objlt = null;
                        if(lotObj!=null && !lotObj.isEmpty())
                        {
                            objlt = lotObj.get(0);
                            lotId = objlt[0].toString();
                        }
                        List<Object[]> WpIdObj = cmss.getWpId(Integer.parseInt(tenderId));
                        Object[] objwp = null;
                        if(WpIdObj!=null && !WpIdObj.isEmpty())
                        {
                            objwp = WpIdObj.get(0);
                            wpId = objwp[3].toString();
                        }
                        List<Object[]> listSrvFormMapId = cmss.getSrvFormMapId(Integer.parseInt(tenderId));
                        Object[] srvFormMapobj = null;
                        if(!listSrvFormMapId.isEmpty())
                        {
                            srvFormMapobj = listSrvFormMapId.get(0);
                            //lotId = srvFormMapobj[1].toString();
                            //wpId = srvFormMapobj[3].toString();
                        }                                                                                                    
                        pageContext.setAttribute("lotId", lotId); 
                        boolean flagg = true;
                        CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                        boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotId));
                        CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                        String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                        {
                            flagg = false;
                        }
            %>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%@include  file="../tenderer/TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">
                <%if(flags){
                            pageContext.setAttribute("TSCtab", "3");
                            
                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                        String msg = "";
                        if (request.getParameter("msgg") != null && !"".equalsIgnoreCase(request.getParameter("msgg"))) {
                            msg = request.getParameter("msgg");
                        }
                        if ("PS".equalsIgnoreCase(msg)) {
                    %>
                        <div class="responseMsg successMsg t_space"><span>Request for Release Performance Security has been sent To PE</span></div>
                        <%}else if("NEWPS".equalsIgnoreCase(msg)){%>
                        <div class="responseMsg successMsg t_space"><span>Request for Release New Performance Security has been sent To PE</span></div>
                        <%}%>

                    <% if (request.getParameter("msg") != null) {
                         if ("InvGenerated".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Selected MileStone Invoice Generated SuccessFully</div>
                    <%}else if("fail".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class='responseMsg errorMsg'>Selected MileStone Invoice Generation Failed</div>
                    <%}}%>
                    <div align="center">                        
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <%
                         
                         int jj = 0;

                         String paymentToId = "";
                         List<SPCommonSearchData> lstPaymentss= commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Performance Security", ""+tblTenderMaster.getCreatedBy(), null, null, null, null, null);
                         List<SPCommonSearchData> lstBankGuaranteee = commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Bank Guarantee", ""+tblTenderMaster.getCreatedBy(), null, null, null, null, null);
                         CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                         int icountcon=0;
                         if(lstPaymentss!=null && !lstPaymentss.isEmpty()){
                         out.print(" <tr>");
                         out.print("<td class='ff' width='20%'  class='t-align-left'>Performance Security</td>");
                         out.print("<td><table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                         out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                         out.print("<th width='15%' class='t-align-center'>Contract No.</th>");
                         out.print("<th width='38%' class='t-align-center'><span id='cmpname'>Company Name</span></th>");
                         out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                         out.print("<th class='t-align-center' width='12%'>Payment Amount</th>");
                         out.print("<th class='t-align-center' width='12%'>Payment Date</th>");
                         out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                         for (SPCommonSearchData sptcd : lstPaymentss)
                         {
                             List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                             jj++;
                             out.print("<tr");
                             if(Math.IEEEremainder(jj,2)==0){
                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                             out.print("<td class='t-align-center'>"+jj+"</td>");
                             out.print("<td class='t-align-center'>");
                             if(cntDetails!=null && !cntDetails.isEmpty() ){
                             if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                 out.print(cntDetails.get(0)[0]+" (For Repeat Order)");
                                 }else{
                                 out.print(cntDetails.get(0)[0]);
                                 }
                             }
                             out.print("</td>");
                             if("individualconsultant".equalsIgnoreCase(sptcd.getFieldName10()))
                             {   %>
                                     <script type="text/javascript">
                                         document.getElementById("cmpname").innerHTML='Individual Consultant Name';
                                     </script>
                                 <% 
                             }   
                             out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                             if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                 out.print("<td class='t-align-center'>Compensated</td>");
                             }else{
                                 out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                             }
                             List<SPCommonSearchDataMore> lstPaymentDetail = objPayment.geteGPData("getTenderPaymentDetail", sptcd.getFieldName1(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);


                              out.print("<td class='t-align-center'>");
                              if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){ %>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }
                             out.print("</td>");
                             out.print("<td class='t-align-center'>"+lstPaymentDetail.get(0).getFieldName6()+"</td>");
                             out.print("<td class='t-align-center'>");
                             if(!checkuserRights.isEmpty()){
                                 paymentToId = sptcd.getFieldName3();
                                 out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                 if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    List<SPCommonSearchData> lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Performance Security", ""+tblTenderMaster.getCreatedBy(), null, null, null, null, null);
                                    if(!lstPaymentss.isEmpty()){
                                        if("Paid".equalsIgnoreCase(sptcd.getFieldName8())){
                                            out.print("&nbsp;|&nbsp;<a href="+request.getContextPath()+"/InvoiceGenerationServlet?action=PSRrequestAction&tenderId="+tenderId+"&lotId="+lotId+"&status=PS>Release Request</a>");
                                        }
                                    }}
                              }
                              out.print("</td></tr>");
                              icountcon++;
                             }
                             if(jj==0){
                                out.print("<tr><td colspan='7'>No records available</td></tr>");
                             }
                             out.print(" </table></td></tr>");
                             }
                        %>    
<!--                            <tr>
                                <td class="ff">New Performance Security</td>
                                <td>-->
                                      <%
                                         CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService"); 
                                         List<Object[]> listBg = cmsNewBankGuarnateeService.fetchBankGuaranteeList(Integer.parseInt(tenderId),tblTenderMaster.getCreatedBy());
                                         List<Object[]> listBgMaker = cmsNewBankGuarnateeService.fetchBankGuaranteeListMaker(Integer.parseInt(tenderId),tblTenderMaster.getCreatedBy());
                                         List<SPCommonSearchData> lstBankGuarantee = commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Bank Guarantee",""+ tblTenderMaster.getCreatedBy(), null, null, null, null, null);
                                         int m = 0;
                                         if((listBg!=null && !listBg.isEmpty()) || (listBgMaker!=null && !listBgMaker.isEmpty()) || (lstBankGuarantee!=null && !lstBankGuarantee.isEmpty())){
                                         out.print("<tr><td class='ff'>New Performance Security</td><td>");
                                         out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                         out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                                         out.print("<th width='7%' class='t-align-left'>Contract  No.</th>");
                                         out.print("<th width='38%' class='t-align-center'>Company Name</th>");
                                         out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                         out.print("<th class='t-align-center' width='30%'>Action</th></tr>");    
                                         
                                         for(Object[] obj1: listBg){
                                             m++;
                                             out.print("<tr");
                                             if(Math.IEEEremainder(m,2)==0){
                                                 out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                                 out.print("<td class='t-align-center'>"+m+"</td>");
                                                 out.print("<td class='t-align-left'>");
                                                 if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                    out.print(""+obj1[3]+" (For Repeat Order)");
                                                 }else{
                                                    out.print(""+obj1[3]+"");
                                                 }
                                                 out.print("</td>");
                                                 String strCompname = "";
                                                 if("1".equals(obj1[6].toString())){
                                                     strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                                 }else{
                                                     strCompname = obj1[7].toString();
                                                 }
                                                 out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                                 out.print("<td class='t-align-center'>Pending</td>");
                                                 out.print("<td class='t-align-center'>");
                                                 out.print("<a href='"+request.getContextPath()+"/tenderer/ViewBankGuarantee.jsp?uId="+tblTenderMaster.getCreatedBy()+"&tenderId="+tenderId+"&lotId="+lotId+"&bgId="+obj1[2]+"'>View</a>");
                                                 //out.print("&nbsp;|&nbsp;<a href="+request.getContextPath()+"/InvoiceGenerationServlet?action=PSRrequestAction&tenderId="+tenderId+"&lotId="+lotId+"&status=NEWPS>Release Request</a>");
                                                 out.print("</td>");
                                                 out.print("</tr>");
                                         }
                                         for(Object[] obj1: listBgMaker){
                                             m++;
                                             out.print("<tr");
                                             if(Math.IEEEremainder(m,2)==0){
                                                 out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                                 out.print("<td class='t-align-center'>"+m+"</td>");
                                                 out.print("<td class='t-align-left'>");
                                                 if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                    out.print(""+obj1[3]+" (For Repeat Order)");
                                                 }else{
                                                    out.print(""+obj1[3]+"");
                                                 }
                                                 out.print("</td>");
                                                 String strCompname = "";
                                                 if("1".equals(obj1[6].toString())){
                                                     strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                                 }else{
                                                     strCompname = obj1[7].toString();
                                                 }
                                                 out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                                 out.print("<td class='t-align-center'>Pending</td>");
                                                 out.print("<td class='t-align-center'>");
                                                 out.print("<a href='"+request.getContextPath()+"/tenderer/ViewBankGuarantee.jsp?uId="+tblTenderMaster.getCreatedBy()+"&tenderId="+tenderId+"&lotId="+lotId+"&bgId="+obj1[2]+"'>View</a>");
                                                 //out.print("&nbsp;|&nbsp;<a href="+request.getContextPath()+"/InvoiceGenerationServlet?action=PSRrequestAction&tenderId="+tenderId+"&lotId="+lotId+"&status=NEWPS>Release Request</a>");
                                                 out.print("</td>");
                                                 out.print("</tr>");
                                         }                                         
                                         for (SPCommonSearchData sptcd : lstBankGuarantee)
                                         {
                                             List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                             m++;
                                             out.print("<tr");
                                             if(Math.IEEEremainder(m,2)==0){
                                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                             out.print("<td class='t-align-center'>"+m+"</td>");
                                             out.print("<td class='t-align-left'>");
                                             if(cntDetails!=null && !cntDetails.isEmpty() ){
                                             if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                             out.print(""+cntDetails.get(0)[0]+" (For Repeat Order)");
                                             }else{
                                             out.print(""+cntDetails.get(0)[0]+"");
                                             }
                                             }
                                             out.print("</td>");
                                             out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                             if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                                 out.print("<td class='t-align-center'>Compensated</td>");
                                             }else{
                                                 out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                             }
                                             out.print("<td class='t-align-center'>");
                                             out.print("<a href='"+request.getContextPath()+"/tenderer/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                             if("Paid".equalsIgnoreCase(sptcd.getFieldName8()))
                                             {    
                                                out.print("&nbsp;|&nbsp;<a href="+request.getContextPath()+"/InvoiceGenerationServlet?action=PSRrequestAction&tenderId="+tenderId+"&lotId="+lotId+"&status=NEWPS>Release Request</a>");
                                             }   
                                             out.print("</td></tr>");
                                         }
                                         if(m==0){
                                                    out.print("<tr><td colspan='5'>No records available</td></tr>");
                                                 }
                                                 out.print("</table>");
                                         }
                                    %>
                                    </table>
                                </td>
                            </tr>
                    </table>    
                            <%
                                NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                List<Object[]> noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                Object[] noaObj  = null;
                                List<Object[]> invIdData = null;
                                boolean isAdvInvFlag = false;
                                if(!noalist.isEmpty())
                                {
                                    noaObj = noalist.get(0);
                                    if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                    {BigDecimal big = new BigDecimal(c_obj[2].toString()).multiply(new BigDecimal(noaObj[12].toString()));
                            %>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td width="34%">Advance Amount</td>
                                        <td width="1%">
                                            <%
                                                if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                {out.print(noaObj[12].toString()+"<br/>(In %)");}
                                            %>
                                        </td>
                                        <td width="1%"><%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,0)%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                        <%
                                            isAdvInvFlag = service.getAcceptedFlag(Integer.parseInt(wpId));
                                            if(isAdvInvFlag)
                                            {
                                        %>
                                        <td width="9%" class="t_align_center" style="text-align: center"><a href="#" onclick="validateAdvanceAmt('<%=tenderId%>','<%=lotId%>','<%=wpId%>','<%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,0)%>')" >Generate Invoice</a></td>
                                        <%  }
                                            invIdData = service.getInvoiceMasterDatawithInvAmt(wpId);
                                            if (!invIdData.isEmpty() && invIdData != null) {
                                                for(int k=0;k<invIdData.size(); k++)
                                                {
                                                  Object[] objInvdata = invIdData.get(k);
                                           %>
                                        <td width="9%" class="t_align_center" style="text-align: center">   
                                        <table class="tableList_1 t_space">
                                        <tr>
                                            <th width="1%" class="t-align-center">Invoices</th>
                                            <th width="1%" class="t-align-center">Status</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=objInvdata[0]%>&wpId=<%=wpId%>"><%=objInvdata[2].toString()%></a></td>                                            
                                            <td class="t-align-left">
                                                <%
                                                    String status = "";
                                                    if (objInvdata[1] == null) {
                                                        status = "-";
                                                    } else if ("createdbyten".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Generated by Consultant";
                                                    } else if ("acceptedbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Accepted by PE";
                                                    } else if ("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "In Process";
                                                    } else if ("sendtotenderer".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Processed by Accounts Officer";
                                                    } else if ("remarksbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Processed by PE";
                                                    } else if ("rejected".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Rejected by PE";
                                                    }
                                                    out.print(status);
                                                %>
                                            </td>
                                        </tr>
                                        </table></td>
                                        <%}}%>                                                                                                                                                                                                               
                                    </tr>
                                    </table>
                            <%            
                                    }
                                }    
                            %>
                            <%if(flagg){%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>                                    
                                <th>Milestone Name </th>                                
                                <th>Description</th>                                
                                <th>Payment as % of Contract Value</th>                                
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>
                                <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                <th>Remarks<br/>By Consultant</th>
                                <th>Action</th>                                
                                <%                                                                        
                                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(Integer.parseInt(srvFormMapobj[0].toString()));
                                    if(!list.isEmpty()){
                                    for(int i=0; i<list.size(); i++)
                                    {    
                                %>
                                <tr>
                                    <td class="t-align-center"><%=list.get(i).getSrNo()%></td>                                    
                                    <td class="t-align-center"><%=list.get(i).getMilestone()%></td>                                    
                                    <td class="t-align-center"><%=list.get(i).getDescription()%></td>                                    
                                    <td class="t-align-center"><%=list.get(i).getPercentOfCtrVal()%></td>                                    
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td> 
                                    <td style="text-align: right"><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*list.get(i).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                    <%BigDecimal totalAmount =  new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*list.get(i).getPercentOfCtrVal().doubleValue())/100).setScale(3,0);%>
                                    </td>                 
                                    <form id="frmid<%=list.get(i).getSrvPsid()%>" name="frmname<%=list.get(i).getSrvPsid()%>" method="post" action="<%=request.getContextPath()%>/CMSSerCaseServlet?action=GenerateInvoiceTenSide"> 
                                        <td class="t-align-center">
                                            <%
                                                List<Object[]> invData = cmss.getInvoiceData(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));                                                
                                                if(invData.isEmpty()){
                                            %>
                                                <label><textarea name="textarea<%=list.get(i).getSrvPsid()%>" rows="2" class="formTxtBox_1" id="textareaid<%=list.get(i).getSrvPsid()%>" style="width:100px;"></textarea></label>
                                            <%}else{
                                                    isAdvInvFlag = cmss.getAcceptedFlaginLumpsum(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));
                                                    if(isAdvInvFlag)
                                                    {
                                                        %>
                                                            <label><textarea name="textarea<%=list.get(i).getSrvPsid()%>" rows="2" class="formTxtBox_1" id="textareaid<%=list.get(i).getSrvPsid()%>" style="width:100px;"></textarea></label>
                                                        <%
                                                        
                                                    }else{
                                                        List<Object[]> listlumpsum = cmss.getInvoiceDatainlumpsum(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));
                                                        if(!listlumpsum.isEmpty() && listlumpsum!=null)
                                                        {
                                                            Object[] InvlumpObj = listlumpsum.get(0);
                                                            if(InvlumpObj[6]!=null){
                                                                out.print(InvlumpObj[6].toString());
                                                            }
                                                        }                                                                                                                                                                                                                                        
                                                    }                                                    
                                            }%>
                                        </td>                                    
                                    <td class="t-align-center">
                                    <%if("Completed".equalsIgnoreCase(list.get(i).getStatus())){%>
                                    <%
                                        isAdvInvFlag = cmss.getAcceptedFlaginLumpsum(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));
                                        if(isAdvInvFlag)
                                        {
                                    %>
                                            <label class="formBtn_1"><input type="button" name="submitbtn" value="Generate Invoice" onclick="return validate(<%=list.get(i).getSrvPsid()%>)"/></label>
                                    <%  }%>
                                    <% 
                                        Object[] InvObj = null;
                                        if(!invData.isEmpty() && invData!=null){
                                            for(int k=0;k<invData.size(); k++)
                                            {
                                            InvObj = invData.get(k);
                                            if(InvObj!=null)
                                            {
                                                    %>
                                                <table class="tableList_1 t_space">
                                                <tr>
                                                    <th width="3%" class="t-align-center">Invoices</th>
                                                    <th width="3%" class="t-align-center">Status</th>
                                                </tr>
                                                <tr>                                                                
                                                    <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=InvObj[0]%>&wpId=<%=wpId%>&srvPsId=<%=list.get(i).getSrvPsid()%>"><%=InvObj[4].toString()%></a></td>
                                                    <td class="t-align-left">
                                                        <%
                                                            String status = "";
                                                            if (InvObj[2] == null) {
                                                                status = "-";
                                                            } else if ("createdbyten".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Generated by Consultant";
                                                            } else if ("acceptedbype".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Accepted by PE";
                                                            } else if ("sendtope".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "In Process";
                                                            } else if ("sendtotenderer".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Processed by Accounts Officer";
                                                            } else if ("remarksbype".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Processed by PE";
                                                            } else if ("rejected".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Rejected by PE";
                                                            }
                                                            out.print(status);
                                                        %>
                                                    </td>
                                                </tr>
                                                </table>
                                    <%            
                                            }    
                                        }}else{
                                    %>
<!--                                    <label class="formBtn_1"><input type="button" name="submitbtn" value="Generate Invoice" onclick="return validate(<%=list.get(i).getSrvPsid()%>)"/></label>                                        -->
<!--                                    <a class="anchorLink" onclick="validate('<%=tenderId%>','<%=list.get(i).getSrvPsid()%>','<%=lotId%>','<%=totalAmount%>','<%=wpId%>');"style="text-decoration: none; color: #fff; ">Generate Invoice</a>                                    -->
                                    <%}}else{%>
                                    MileStone has Not been Completed Yet.
                                    <%}%>
                                    </td>
                                    <input type="hidden" value="<%=list.get(i).getSrvPsid()%>" name="SrvPsid"/>
                                    <input type="hidden" value="<%=tenderId%>" name="tenderId"/>
                                    <input type="hidden" value="<%=wpId%>" name="wpId"/>
                                    <input type="hidden" value="<%=lotId%>" name="lotId"/>
                                    <input type="hidden" value="<%=totalAmount%>" name="totalamount"/>
                                    </form>
                                </tr>                                
                                <%}}%>                                 
                            </table>
                            <%}else{%>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <th width="12%" class="t-align-left">Sr.No</th>
                                <th class="t-align-left">Attendance Sheet</th>
                                <th class="t-align-left">Month-Year</th>
                                <th class="t-align-left">Created Date</th>
                                <th class="t-align-left">Action</th>
                                <%
                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                    MonthName monthName = new MonthName();                                    
                                    if(os!=null && !os.isEmpty()){
                                        int cc=1;
                                        for(TblCmsSrvAttSheetMaster master : os){
                                %>
                                        <tr>
                                        <td width="12%" class="t-align-left"><%=cc %></td>
                                        <td width="15%" class="t-align-left">Attendance Sheet - <%=cc%></td>
                                        <td class="t-align-left"><%=monthName.getMonth(master.getMonthId())+" - "+master.getYear() %></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(master.getCreatedDt()) %></td>
                                        <td class="t-align-left">
                                        <%
                                            List<Object[]> invDataforAttaSheet = cmss.getInvoiceData(master.getAttSheetId(),Integer.parseInt(tenderId));                                                
                                            if(invDataforAttaSheet.isEmpty()){
                                                if("accept".equalsIgnoreCase(cmss.checkAttStatus(master.getAttSheetId()+""))){
                                        %>
                                        <a href="GenerateInvSheetdtls.jsp?sheetId=<%=master.getAttSheetId()%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Generate Invoice</a>
                                        <%}else{%>
                                        PE has not edit attendance sheet yet
                                        <%}}else{
                                            Object[] InvObjforAttSheets = null;
                                            InvObjforAttSheets = invDataforAttaSheet.get(0);
                                            if(InvObjforAttSheets!=null)
                                            {    
                                        %>
                                        <table class="tableList_1 t_space">
                                        <tr>
                                            <th width="1%" class="t-align-center">Invoices</th>
                                            <th width="1%" class="t-align-center">Status</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=InvObjforAttSheets[0]%>&wpId=<%=wpId%>&Sheetflag=true"><%=InvObjforAttSheets[4].toString()%></a></td>                                            
                                            <td class="t-align-left">
                                                <%
                                                    String status = "";
                                                    if (InvObjforAttSheets[2] == null) {
                                                        status = "-";
                                                    } else if ("createdbyten".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Generated by Consultant";
                                                    } else if ("acceptedbype".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Accepted by PE";
                                                    } else if ("sendtope".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "In Process";
                                                    } else if ("sendtotenderer".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Processed by Accounts Officer";
                                                    } else if ("remarksbype".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Processed by PE";
                                                    } else if ("rejected".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Rejected by PE";
                                                    }
                                                    out.print(status);
                                                %>
                                            </td>
                                        </tr>
                                        </table>
                                        <%}}%>
                                        </td>
                                        </tr>
                                    <%cc++;}}else{%>
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                    <%}%>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-center">
                                                    Form Name
                                                </th>
                                                <th width="60%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                            <%
                                            int countt=1;
 String wpIdd = service.getWpIdForService(Integer.parseInt(lotId));
  boolean isCntTer = service.isContractTerminatedOrNot(lotId);
                                                List<Object[]> invoicId = service.getInvoiceIdList(wpIdd);
                                                List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(wpIdd));

                                            %>
                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td>
                                                  Reimbursable Expenses
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                        <tr>
                                                            <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                            <th width="50%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                            <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                            </th>
                                                        </tr>
                                                        <%
                                                            boolean shoflag = false;
                                                            int j=1;
                                                            int prId = 0;
                                                            if (!Prlist.isEmpty() && Prlist != null) {
                                                                j = 1;
                                                                for (int ii = 0; ii <Prlist.size(); ii++) {
                                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                                    String temp1[] = temp[4].split("-");
                                                                    String finalNo = temp[0] + " " + temp[1] + " " + temp[2] + " " + temp[3] + " " + DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(), "yyyy-MM-dd"));

                                                        %>
                                                        <tr>
                                                            <td class="t-align-center"><%=j + ii%></td>
                                                            <td class="t-align-left"><%=finalNo%></td>
                                                            <td class="t-align-left"><a href="SrvViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=wpId%>&tenderId=<%=tenderId%>&flag=inv&lotId=<%= lotId%>">View</a></td>
                                                            <%
                                                                        prId = Prlist.get(ii).getProgressRepId();
                                                                    }
                                                                           List<Object[]> listt = service.getListForAnyItemAnyPercent(0, 100, Prlist.get(Prlist.size() - 1).getProgressRepId());
                                                                        double qty = 0;
                                                                        if(listt!=null && !listt.isEmpty()){
                                                                        for (Object[] oob : listt) {
                                                                            
                                                                                qty = Double.parseDouble(oob[5].toString()) - Double.parseDouble(oob[6].toString());
                                                                                

                                                                        }
                                                                        }

                                                                }%>
                                                    </table>
                                                </td>

                                                <td class="t-align-left">
                                                    <%if (isCntTer) {%>
                                                    <a href="#" onclick="jAlert('You cannot Generate Invoice as Contract has been Terminated by Procuring Entity','Invoice', function(RetVal) {
                                                    });">Generate Invoice</a>
                                                    <%} else {
                                                            if(noaObj[12]==null || "0.000".equalsIgnoreCase(noaObj[12].toString())){
                                                    %>
                                                    <a href="SrvGenerateInvoice.jsp?prId=<%=prId%>&tenderId=<%=request.getParameter("tenderId")%>&wpId=<%=wpIdd%>&lotId=<%=lotId%>&srvFrm=true">Generate Invoice</a>
                                                    <%}else{
                                                         //boolean bflag = service.getAcceptedFlagforBoq(Integer.parseInt(wpid.get(0).toString()));
                                                         //if(bflag){
                                                    %>
                                                            <a href="SrvGenerateInvoice.jsp?prId=<%=prId%>&tenderId=<%=request.getParameter("tenderId")%>&wpId=<%=wpIdd%>&lotId=<%=lotId%>&srvFrm=true">Generate Invoice</a>
                                                    <%
                                                         //}
                                                    }
                                                    %>
                                                    <%}%>
                                                    <%if (invoicId != null && !invoicId.isEmpty()) {
                                                    %>
                                                    <div>
                                                        <table class="tableList_1 t_space">
                                                            <tr>
                                                                <th width="3%" class="t-align-center">Invoices</th>
                                                                <th width="3%" class="t-align-center">Status</th>
                                                            </tr>
                                                            <%
                                                                int ii = 1;
                                                                for ( j = 0; j < invoicId.size(); j++) {
                                                                    Object[] invobj = invoicId.get(j);
                                                            %>
                                                            <tr>
                                                                <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=invobj[0]%>&wpId=<%=wpId%>"><%=invobj[2].toString()%></a></td>
                                                                <td class="t-align-left">
        <%
                                                                            String status = "";
                                                                            if (invobj[1] == null) {
                                                                                status = "-";
                                                                            } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Invoice Generated by Consultant";
                                                                            } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Invoice Accepted by PE";
                                                                            } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "In Process";
                                                                            } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Processed by Accounts Officer";
                                                                            } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Processed by PE";
                                                                            } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                                                status = "Invoice Rejected by PE";
                                                                            }
                                                                            out.print(status);
        %>
                                                                </td>
                                                            </tr>
                                                            <%
                                                                    ii++;
                                                                }%>

                                                        </table>
                                                    </div>
                                                    <%}%></td>
                                            </tr>


                                            <%
                                                                        countt++;
                                            %>
                                        </table>
                            <%}%>
                            <%}else{%>
                            <div class="responseMsg noticeMsg">date Configuration is pending</div>
                            <%}%>
<!--                        </form>-->
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
<script>
function validate(SrvPsid)
{    
    jConfirm("Are you sure you want to generate invoice", "Generate Invoice", function(bool){
       if(bool){
           var frmObj = document.getElementById("frmid"+SrvPsid);
           frmObj.submit();
           return true;
       }else{
           return false;
       }
    });
    /*if(confirm("Are you sure MileStone has been Completed?")){
        return true;
    }else{
        return false;
    }*/
}
function validateAdvanceAmt(tenderId,lotId,wpId,totalamount)
{
    jConfirm("Are you sure you want to generate invoice", "Generate Invoice", function(bool){
       if(bool){
           dynamicFromSubmit("<%=request.getContextPath()%>/CMSSerCaseServlet?action=GenerateInvoiceTenSide&tenderId="+tenderId+"&lotId="+lotId+"&wpId="+wpId+"&totalamount="+totalamount+"&isAdvInv=yes");                      
       }
    });
}
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
</script>
</html>

