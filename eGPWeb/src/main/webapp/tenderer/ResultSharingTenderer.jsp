<%-- 
    Document   : ResultSharingTenderer
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : parag
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Result Sharing</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
   <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Result Sharing</div>
    <%
        String tenderId = request.getParameter("tenderId");
        pageContext.setAttribute("tenderId", tenderId);
    %>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <%pageContext.setAttribute("tab", "5");%>
  <%@include file="TendererTabPanel.jsp" %>
  <%if(!is_debared){%>
  <div class="tabPanelArea_1">
  <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
        <%
            //tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

            // CHECK docAvlMethod COLUMN WHETHER THERE IS "Lot" OR "Package"
        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
        String repLabel=null;

        boolean isForeignCurrencyExchangeRateAdded = false; //dohatec: ICT
        boolean  isProcurementTypeIct = false; //dohatec: ICT
          
        if(request.getParameter("tenderId")!=null){
                  tenderId = request.getParameter("tenderId");
              }

        String procNature = commonService.getProcNature(request.getParameter("tenderId")).toString();
        if(procNature.equals("Services")){
                        repLabel = "Proposal";
                    }else{
                        repLabel = "Tender";
                    }

        /* Dohatec: ICT Start */
                            // 	Procurement Type (i.e. NCT/ICT) retrieving
                            List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                isProcurementTypeIct = true;
                            }
         //Dohatec: ICT End

                    //getProcurementNatureForEval if to be checked on procurement Category
                    // IF PACKAGE FOUND THEN DISPLAY PACKAGE INFORMATION
                    if(procNature.equals("Services")){

                        // FETCH PACKAGE INFORMATION
                        List<SPTenderCommonData> packageList = tenderCommonService.returndata("getlotorpackagebytenderid",
                                                                            tenderId,
                                                                            "Package,1");
                            if (!packageList.isEmpty()) {
                 List<SPTenderCommonData> TOR_tenderer_condition = tenderCommonService.returndata("TOR_tenderer_condition",tenderId,"0");

                 /* Dohatec: ICT Start */

                            //check at least one foreign currency exchange rate (except BTN) added or not
                            TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                            if(isProcurementTypeIct){
                                isForeignCurrencyExchangeRateAdded = tenderCurrencyService.isAtleastOneCurrencyExchangeRateAdded(Integer.parseInt(request.getParameter("tenderid")));
                            }
                        /* Dohatec: ICT End */
       %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="15%" class="t-align-left ff">Package No :</td>
                        <td width="85%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Package Description :</td>
                        <td class="t-align-left"><%=packageList.get(0).getFieldName2()%></td>
                    </tr>
                    <%if((!TOR_tenderer_condition.isEmpty()) && TOR_tenderer_condition.get(0).getFieldName1().equals("1")){%>
                    <tr>
                        <td class="t-align-left ff">BOR</td>
                            <td>
                                <a href="<%=request.getContextPath()%>/report/TORCommon.jsp?isT=y&isPDF=true&tenderid=<%=tenderId%>&lotId=0" target="_blank">View</a>
                            </td>
                        </tr>
                        <%}else{out.print("<tr><td colspan='2'><div class='responseMsg noticeMsg'>Opening Reports are yet not shared by the Opening Committee.</div></td></tr>");}%>
                </table>
                <% }  %>
                <!--  Tender Form Listing
                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="48%" class="t-align-left">Form Name</th>
                        <th width="27%" class="t-align-left">Individual chart</th>
                        <th width="25%" class="t-align-left">Comparative chart</th>
                    </tr>
                    < %
                        List<SPTenderCommonData> formList = tenderCommonService.returndata("GetFormNameByTenderIdForResultShare",
                                                                                            tenderId,
                                                                                            "0");
                        if (!formList.isEmpty()) {
                            for (SPTenderCommonData formname : formList) {
                    %>
                    <tr>
                        <td class="t-align-left">< %=formname.getFieldName1()%></td>
                        < % if (formname.getFieldName7().equalsIgnoreCase("Individual")) { %>
                        <td class="t-align-left"><a href="< %=request.getContextPath()%>/officer/IndReport.jsp?tenderId=< %=tenderId%>&formId=< %=formname.getFieldName5()%>">View</a></td>
                        < % } else { %>
                        <td class="t-align-left">-</td>
                        < % } if (formname.getFieldName8().equalsIgnoreCase("Comparative")) { %>
                        <td class="t-align-left"><a href="< %=request.getContextPath()%>/officer/ComReport.jsp?tenderId=< %=tenderId%>&formId=< %=formname.getFieldName5()%>">View</a></td>
                        < % } else { %>
                        <td class="t-align-left">-</td>
                        < % } %>
                    </tr>
                    < %
                            } // END FOR LOOP
                         } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><b>No form name found! </b></td>
                    </tr>
                    < % } %>
                </table>-->
                <%
                     } else {
                        List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getProcurementNatureForEval",tenderId,null);
                                for (SPTenderCommonData tenderLot : tenderLotList) {                                    
                    List<SPTenderCommonData> TOR_tenderer_condition = tenderCommonService.returndata("TOR_tenderer_condition",tenderId,tenderLot.getFieldName3());
                %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="13%" class="t-align-left ff">Lot No:</td>
                        <td width="87%" class="t-align-left"><%=tenderLot.getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Lot Description:</td>
                        <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
                    </tr>
                    <%if((!TOR_tenderer_condition.isEmpty()) && TOR_tenderer_condition.get(0).getFieldName1().equals("1")){
                     if(isProcurementTypeIct){%>
                      <tr>
                        <td width="18%" class="t-align-left ff">Currency Rate</td>
                        <td width="82%" class="t-align-left">
                        <a href="ViewCurrencyDetails.jsp?tenderid=<%=tenderId%>">View</a>
                        </td>
                    </tr>
                        <%}%>
                        <tr>
                            <td class="t-align-left ff"><%if(repLabel.equals("Tender")){%>B<%}else{%>P<%}%>OR</td>
                            <td>                                
                                <a href="<%=request.getContextPath()%>/report/TORCommon.jsp?isT=y&isPDF=true&tenderid=<%=tenderId%>&lotId=<%=tenderLot.getFieldName3()%>" target="_blank">View</a>
                            </td>
                        </tr>
                      <%}else{out.print("<tr><td colspan='2'><div class='responseMsg noticeMsg'>Opening Reports are yet not shared by the Opening Committee.</div></td></tr>");}%>
                </table>                    
            <!--  Tender Form Listing
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="48%" class="t-align-left">Form Name</th>
                        <th width="27%" class="t-align-left">Individual chart</th>
                        <th width="25%" class="t-align-left">Comparative chart</th>
                    </tr>
                    <%/*
                        List<SPTenderCommonData> formList = tenderCommonService.returndata("GetFormNameByTenderIdForResultShare",
                                                                                            tenderId,
                                                                                            tenderLot.getFieldName3().toString());
                        if (!formList.isEmpty()) {
                            for (SPTenderCommonData formname : formList) {*/
                    %>
                    <tr>
                        <td class="t-align-left">< %=formname.getFieldName1()%></td>
                        < %  if (formname.getFieldName7().equalsIgnoreCase("Individual")) { %>
                        <td class="t-align-left"><a href="< %=request.getContextPath()%>/officer/IndReport.jsp?tenderId=< %=tenderId%>&formId=< %=formname.getFieldName5()%>">View</a></td>
                        < % } else { %>
                        <td class="t-align-left">-</td>
                        < % } if (formname.getFieldName8().equalsIgnoreCase("Comparative")) { %>
                        <td class="t-align-left"><a href="< %=request.getContextPath()%>/officer/ComReport.jsp?tenderId=< %=tenderId%>&formId=< %=formname.getFieldName5()%>">View</a></td>
                        < % } else { %>
                        <td class="t-align-left">-</td>
                        < % } %>
                    </tr>
                    < %
                            } // END FOR LOOP
                         } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><b>No form name found! </b></td>
                    </tr>
                    < % } %>
                </table>-->
                <%
                                } // END LOT FOR LOOP
                           } // END IF                      
                %>
<!--	<tr>
		<th width="76%" class="ff">Form Name</th>
		<th width="12%">Individual</th>
		<th width="12%" class="tableList_1 t_space">Comparative</th>
      </tr>
	<tr>
		<td>A</td>
		<td><a href="#">Result</a></td>
		<td><a href="#">Result</a></td>
	  </tr>
	<tr>
	  <td>B</td>
	  <td><a href="#">Result</a></td>
	  <td><a href="#">Result</a></td>
	  </tr>-->
	</table>
<%
    if (userTypeId != 2) {
%>
<div class="t-align-center t_space">
   	  <label class="formBtn_1">
        	<input name="" type="button" value="Result Sharing" />
        </label>
    </div>
<% } %>
</div><%}%>
    <div>&nbsp;</div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
   <div align="center"
                 <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    <!--Dashboard Footer End-->
</div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>

