<%-- 
    Document   : PreTenderQueryDocUpload
    Created on : Nov 29, 2010, 12:21:20 PM
    Author     : Administrator
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Reference Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                                        maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
</script>
<script type="text/javascript">
    function closeWondow(){
                window.opener.getDocData();
                window.close();
            }
</script>
        

    </head>
    <body onunload="doUnload()">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
<%--<%@include file="../resources/common/AfterLoginTop.jsp" %>--%>
            <%
                        int tenderId = 1;
                        String preDocType="";
                        boolean showOnlyDoc=false;
                        int queryId=0;
                        if(session.getAttribute("userId")==null){
                                response.sendRedirect("SessionTimedOut.jsp");
                            }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        }
                        if (request.getParameter("docType") != null) {
                            preDocType = request.getParameter("docType");
                        }
                        if (request.getParameter("actionType") != null) {
                           if("download".equalsIgnoreCase(request.getParameter("actionType"))){
                               showOnlyDoc = true;
                           }
                        }
                        if(request.getParameter("tender") != null){
                            tenderId = Integer.parseInt(request.getParameter("tender"));
                        }
            %>
            <div class="mainDiv">
            <div class="fixDiv">
                 <div class="dashboard_div">
            <div class="contentArea_1">
            <div class="pageHead_1">Reference Document</div>
             <%if(!showOnlyDoc){%>
            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/PreTenderQueryDocServlet" enctype="multipart/form-data" name="frmUploadDoc">
                <input type="hidden" name="tenderId" value="<%= tenderId%>" />
                <input type="hidden" name="preDocType" value="<%=preDocType%>" />

               <%-- <%   pageContext.setAttribute("tenderId", tenderId);%>--%>
               <%-- <%@include file="../resources/common/TenderInfoBar.jsp" %>--%>
               

                <%
                                if (request.getParameter("fq") != null) {
                    %>
                     <div> &nbsp;</div>
                    <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>
                     <div> &nbsp;</div>
                    <div class="responseMsg errorMsg">
                       
                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }if (request.getParameter("ud") != null) {
                    %>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">

                        File uploaded sucessfully
                    </div>
                    <%
                                }if(request.getParameter("rv") != null){

                    %>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">
                        File Removed sucessfully
                    </div>
                    <%}%>
                    <table width="90%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    
                    <tr>

                        <td style="font-style: italic" colspan="2" class="ff t-align-left" >Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                    <tr>
                        <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                        <td width="80%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="ff">Description : <span>*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>

                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="btnUpld" value="Upload" /></label>
                            <label class="formBtn_1"><input type="button" name="btnClose" id="btnClose" value="Close" onclick="return closeWondow()" /></label>
                        </td>
                    </tr>

                </table>
                    <table width="90%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="100%"  class="t-align-left">Instructions</th>
                                </tr>
                                <tr>
                                    <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");%>
                                    <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                            </table>
            </form>
            <%}%>
            <table width="90%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="4%" class="t-align-center">Sl.  No.</th>
                    <th class="t-align-center" width="23%">File Name</th>
                    <th class="t-align-center" width="32%">File Description</th>
                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                </tr>
                <%
                            String userId = "1";
                            if (session.getAttribute("userId") != null) {
                                userId = session.getAttribute("userId").toString();
                            }
                            if(showOnlyDoc){
                                if(request.getParameter("userId") != null){
                                    userId = request.getParameter("userId");
                                 }
                                if(request.getParameter("queryId") != null){
                                    queryId = Integer.parseInt(request.getParameter("queryId"));
                                 }
                            }
                            //String tender = request.getParameter("tender");
                            
                            

                            int docCnt = 0;
                            TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            java.util.List<SPTenderCommonData> docData=null;
                            if("Prebid".equalsIgnoreCase(preDocType)){
                                 if(showOnlyDoc){
                                     if(queryId > 0){
                                        docData =  tenderCS.returndata("PreTenderQueDocuments", ""+tenderId,""+queryId);
                                     }else{
                                       docData =  tenderCS.returndata("PreTenderQueDocs", ""+tenderId,userId);
                                     }
                                 }else{
                                   docData =  tenderCS.returndata("PreTenderQueDocs", ""+tenderId,userId);
                                 }
                            }else{
                               // docData =  tenderCS.returndata("QuestionDocsBy", ""+tenderId,userId);
                                    if(showOnlyDoc){
                                        if(queryId > 0){
                                            docData =  tenderCS.returndata("QuestionDocumentsBy", ""+tenderId,""+queryId);
                                        }else{
                                            docData =  tenderCS.returndata("QuestionDocsBy", ""+tenderId,userId);
                                        }
                                     }else{
                                         docData =  tenderCS.returndata("QuestionDocsBy", ""+tenderId,userId);
                                    }
                            }

                            for (SPTenderCommonData sptcd : docData) {
                                docCnt++;
                %>
                <tr>
                    <td class="t-align-center"><%=docCnt%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                    <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                    <td class="t-align-center">
                        <a href="<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tenderId%>&queryId=<%=queryId%>&docType=<%=preDocType%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        &nbsp;
                        <%if(!showOnlyDoc){%>
                            <a href="<%=request.getContextPath()%>/PreTenderQueryDocServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tender=<%=tenderId%>&queryId=<%=queryId%>&docType=<%=preDocType%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        <%}%>
                    </td>
                </tr>

                      <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                <% if (docCnt == 0) {%>
                <tr>
                    <td colspan="5" class="t-align-center">No records found.</td>
                </tr>
                <%}%>
            </table>

            <div>&nbsp;</div>
            </div>
                 </div></div></div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
           <%-- <table width="100%" cellspacing="0" class="footerCss">
                <tr>
                    <td align="left">e-GP &copy; All Rights Reserved
                        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                    <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a>


                    </td>
                </tr>
            </table>--%>
           <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        <%--</div>--%>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<script>
function doUnload()
{
   window.opener.getDocData();
  
}

    </script>
