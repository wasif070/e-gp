<%-- 
    Document   : NegotiationTendererForms
    Created on : Jan 11, 2011, 12:28:23 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="neg" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean" />
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%
                    String tenderId;
                    int isBidModify = 0;
                    tenderId = request.getParameter("tenderId");

                    String usrId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        usrId = session.getAttribute("userId").toString();
                    }

                    int negId = 0;
                    if(request.getParameter("negId")!=null){
                        negId = Integer.parseInt(request.getParameter("negId"));
                    }
            %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea">
                        <div class="pageHead_1">Negotiation</div>
                        <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <jsp:include page="EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="4" />
                        </jsp:include>
                        <%
                             boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                        %>
                        <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            if (isTenPackageWis) {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <th width="78%" class="t-align-left">Action</th>
                        </tr>
                        <%
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, null, null, null, null)) {
                                if(formdetails.getFieldName6()!=null){
                                    isBidModify++;
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            <td class="t-align-left">
                                <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&negId=<%=negId%>&lotId=0&type=negbiddata&action=Edit">View</a>
                            </td>
                        </tr>
                        <%    }  %>
                        <%
                                }
                            out.println("</table>");
                              }
                            }else {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                 for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, lotList.getFieldName3(), usrId, null, null, null, null, null)) {
                                        if(formdetails.getFieldName6()!=null){
                                            isBidModify++;
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left ff">
                                    <a href="../resources/common/ViewNegBidform.jspViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&lotId=0&action=Edit">View</a>
                                </td>
                            </tr>
                             <%         }
                                }
                            %>
                        </table>
                        <%     }
                            }
                        %>
                        <% if(isBidModify!=0){ %>
                        <form name="frmapproveNegBid" id="idfrmapproveNegBid" method="post" action="<%=request.getContextPath()%>/NegotiationSrBean">
                            <input type="hidden" name="negId" id="idnegId" value="<%=negId%>"/>
                            <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=tenderId %>"/>
                            <input type="hidden" name="action" id="action" value="approveBid" />
                            <table class="tableList_1" width="100%">
                                <tr>
                                    <td width="22%">&nbsp;</td>
                                    <td>
                                        <input type="radio" name="FinalStatus" id="idradiofinalStatus1" value="Accept" />&nbsp;Accept
                                        <input type="radio" name="FinalStatus" id="idradiofinalStatus2" value="Reject" />&nbsp;Reject
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="t-align-center">
                                        <label class="formBtn_1 t_space">
                                               <input type="submit" name="btnsubmit" id="idbtnsubmit" value="Submit" />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>
                       <% } %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
              <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>