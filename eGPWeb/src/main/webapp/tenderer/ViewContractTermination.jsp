<%-- 
    Document   : ViewContractTermination
    Created on : Aug 29, 2011, 7:21:06 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <title> Contact Termination View Page</title>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int contractTerminationId = 0;
                if (request.getParameter("contractTerminationId") != null) {
                    contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
                }
                String message = "";
                boolean isError = false;
                if (request.getParameter("msg") != null) {
                    message = request.getParameter("msg");
                    if (message.equalsIgnoreCase("approve")) {
                        message = "Contract Temination status is approved";
                    } else if (message.equalsIgnoreCase("update")) {
                        message = "Contract Temination is updated";
                    } else if (message.equalsIgnoreCase("error")) {
                        isError = true;
                        message = "Please try again!";
                    }
                }//request checking null
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                }
                CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
                cmsCTReasonTypeBean.setLogUserId(userId);
                List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();

                CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                cmsContractTerminationBean.setLogUserId(userId);
                TblCmsContractTermination tblCmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
    %>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <div  id="print_area">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Contract Termination Page
                                <span style="float: right; text-align: right;"class="noprint">
                                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                </span>
                            </div>
                            <%
                                        if (message.length() > 0) {
                                            if (isError) {
                                                out.print("<div id='errMsg' class='responseMsg errorMsg' style='display:block; margin-top: 10px;'>");
                                            } else {
                                                out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");
                                            }
                                            out.print(message);
                                            out.print("</div>");
                                        }
                            %>
                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                <tr id="tblRow_dateOfTermination">
                                    <td class="ff" width="15%">Date of Termination : </td>
                                    <td><%=DateUtils.gridDateToStrWithoutSec(tblCmsContractTermination.getDateOfTermination())%> </td>
                                </tr>
                                <tr id="tblRow_typeOfReason">
                                    <td class="ff">Type of Reason : </td>
                                    <td>
                                        <%
                                                    String[] reasonTypes = tblCmsContractTermination.getReasonType().split("\\^");
                                                    for (String reasonTypeId : reasonTypes) {
                                                        int reasonId = Integer.parseInt(reasonTypeId);
                                                        for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                                            if (cmsCTReasonType.getCtReasonTypeId() == reasonId) {
                                                                out.print(cmsCTReasonType.getCtReason() + "<br>");
                                                                break;
                                                            }
                                                        }//inner for
                                                    }// outer for
                                        %>
                                    </td>
                                </tr>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Status : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getStatus()%>
                                    </td>
                                </tr>
                                <%
                                    if(userTypeId==3)
                                    {
                                %>
                                <%if(tblCmsContractTermination.getPaySetAmt()!=null && !"0.000".equalsIgnoreCase(tblCmsContractTermination.getPaySetAmt().toString())){%>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Payment Settlement Amount (In Nu.) :</td>
                                    <td>
                                        <%=tblCmsContractTermination.getPaySetAmt().setScale(3,0)%>
                                    </td>
                                </tr>
                                <%}%>
                                <%if(tblCmsContractTermination.getPaySetAmtRemarks()!=null && !"".equalsIgnoreCase(tblCmsContractTermination.getPaySetAmtRemarks())){%>
                                <tr id="tblRow_workFlowStatus">
                                    <td class="ff">Remarks for Payment Settlement Amount :</td>
                                    <td>
                                        <%= tblCmsContractTermination.getPaySetAmtRemarks()%>
                                    </td>
                                </tr>
                                <%}}%>
<!--                                <tr id="tblRow_releasePerformanceGuarantee" >
                                    <td class="ff">Release Performance Security : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getReleasePg()%>
                                    </td>
                                </tr>--> 
                                <tr id="tblRow_reason">
                                    <td  class="ff">Reason For Termination : </td>
                                    <td>
                                        <%= tblCmsContractTermination.getReason()%>
                                    </td>
                                </tr>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End--></div>
                <!--Dashboard Footer Start-->
                                 <%     MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(c_obj[7].toString()), "ContractId",EgpModule.Contract_Termination.getName(), "View Terminated Contract", ""); %>
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
       
</html>

