<%--
    Document   : CreateJVCA
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : Rikin
--%>

<%@page import="com.cptu.egp.eps.model.table.TblJointVenture"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="jvcSrBean" class="com.cptu.egp.eps.web.servicebean.JvcaSrBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            if(session.getAttribute("userId")!=null){
             jvcSrBean.setLogUserId(session.getAttribute("userId").toString());
           }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure JVCA (Joint Venture Consortium Association)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>Configure JVCA (Joint Venture Consortium Association)</title>
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    var validateFlag = true;
                     $(".errValidate").remove();
                     //if($('#txtEmailId').val()==''&&$('#txtCompanyName').val()==''){
                     if($.trim($('#txtEmailId').val())==''){
                         $('#txtEmailId').parent().append("<div class='errValidate' style='color:red;'>Please enter email-ID</div>");
                         validateFlag = false;
                     }
                    if(validateFlag==false){
                        return false;
                    }
                    //$.post("<%=request.getContextPath()%>/JVCAServlet", {companyName:$('#txtCompanyName').val(),emailId:$('#txtEmailId').val(),funName:'partnerSearch'}, function(j){
                    $.post("<%=request.getContextPath()%>/JVCAServlet", {emailId:$('#txtEmailId').val(),funName:'partnerSearch'}, function(j){
                        var i = j.toString();
                        $("#tbodype").html(j);
                    });
                });
            });
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:false,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            $(function() {
                                    var len=$('#members').children()[1].children.length;
                                    var i =len;
                                    $(":checkbox[checked='true']").each(function(){
                                        var a = $(this).attr("id");
                                        var b = a.substring(3, a.length);
                                        var addUser=true;
                                        for(var j=0;j<len;j++){
                                            if($('#companyid'+j).val()==$("#chk"+b).val()){
                                                addUser=false;
                                                break;
                                            }
                                        }
                                        if(addUser){
                                            $( "#members tbody" ).append("<tr>"+
                                                "<td class='t-align-left'><input id='companyid"+i+"' type='hidden' name='companyIds' value='"+$("#chk"+b).val()+"'/>"+$("#companyName"+b).val()+"</td>"+
                                                "<td class='t-align-left'><input id='userid"+i+"' type='hidden' name='userIds' value='"+$("#userId"+b).val()+"'/>"+$("#emailId"+b).val()+"</td>"+
                                                "<td class='t-align-left'>"+
                                                "<select name='cmbPartnerType' id='cmbPartnerType"+i+"' class='formTxtBox_1' style='width:150px;'><option value=''>--Select--</option><option value='Lead'>Lead</option><option value='Secondary'>Secondary</option></select></td>"+
                                                "<td class='t-align-center'><input type='radio' class='radioList_1'  name='radioisNominated' id='radioisNominated"+i+"' value='"+$("#userId"+b).val()+"' /></td>"+
                                                "<td class='t-align-center'><a onclick='delRow(this)' class='action-button-delete' id='delRow"+i+"'>Remove</a></td>"+
                                                "</tr>");
                                            i++;
                                        }else{
                                            jAlert("Company already added ","Same Company alert", function(RetVal) {
                                            });
                                        }
                                        });
                                    });
                                    $( this ).dialog( "close" );
                                },
                                Cancel: function() {
                                    $( this ).dialog( "close" );
                                }
                            },
                            close: function() {
                            }
                        });

                    $("#addmem" ).click(function(){
                        $("#dialog-form").dialog("open");
                        $("#txtEmailId").val('');
                        $("#tbodype").html('');
                        //$("input[type='checkbox']").removeAttr("checked");
                    });
                });

                function validate(){
                    $(".err").remove();
                    var valid = true;
                    if($.trim($("#txtJVCAName").val()) == "") {
                        $("#txtJVCAName").parent().append("<div class='err' style='color:red;'>Please enter JVCA Name</div>");
                        valid = false;
                        return valid;
                    }
                    var cmbPartner = document.getElementsByTagName('select');

                    if(cmbPartner.length<2){
                        jAlert("Please Add Partner ","Add Partner alert", function(RetVal) {
                               });
                        valid = false;
                        return valid;
                    }
                    
                    var isLead = false;
                    var isNominate = false;
                    for(var i=0;i<cmbPartner.length;i++)
                    {
                       var inputname =  cmbPartner[i].id;
                       
                       if($("#"+inputname).val()==""){
                            jAlert("Please Select JV Role for all field","Lead Partner alert", function(RetVal) {
                            });
                            valid =false;
                            return valid;
                       }
                       
                       if($("#"+inputname).val()=="Lead"){
                           if(isLead==false){
                               isLead = true;
                           }else{
                               jAlert("Please Select only one Lead Partner ","Lead Partner alert", function(RetVal) {
                               });
                               valid = false;
                               return valid;
                           }
                       }
                       if($("#radioisNominated"+i).attr("checked")){
                            isNominate = true;
                       }
                    }
                    if(isLead==false){
                        jAlert("Please Select at least one Lead Partner ","Lead Partner alert", function(RetVal) {
                               });
                        valid = false;
                        return valid;
                    }
                    if(isNominate==false){
                        jAlert("Please select Nominated Partner ","Nominate Partner alert", function(RetVal) {
                               });
                        valid = false;
                        return valid;
                    }
                    
                    if(!valid){
                        return false;
                    }
                }
                function checkCompanyName(){
                       $(".err").remove();
                    if($("#txtJVCAName").val() == "") {
                        $("#txtJVCAName").parent().append("<div class='err' style='color:red;'>Please enter JVCA Name</div>");
                    }
                }
                
                function delRow(row){
                    var curRow = $(row).parents('tr');
                    var len=$('#members').children()[1].children.length;
                    var id=$(row).attr("id").substring(6,$(row).attr("id").length);
                    for(var i=id;i<len;i++){
                        $('#companyid'+eval(eval(i)+eval(1))).attr("id","companyid"+(i));
                        $('#userid'+eval(eval(i)+eval(1))).attr("id","userid"+(i));
                        $('#cmbPartnerType'+eval(eval(i)+eval(1))).attr("id","cmbPartnerType"+(i));
                        $('#radioisNominated'+eval(eval(i)+eval(1))).attr("id","radioisNominated"+(i));
                        $('#delRow'+eval(eval(i)+eval(1))).attr("id","delRow"+(i));
                    }
                    curRow.remove();
                }
        </script>
    </head>
    <body>
        <%
            String contextPath = request.getContextPath();
            String jvcId = "";
            String JVName ="";

            HttpSession hs = request.getSession();
            int userId = Integer.parseInt(hs.getAttribute("userId").toString());

            if(request.getParameter("jvcId")!=null && !"".equalsIgnoreCase(request.getParameter("jvcId"))){
                jvcId = request.getParameter("jvcId");
                
                List<TblJointVenture> jvcDetails = jvcSrBean.getJVCADetails(Integer.parseInt(jvcId));
                JVName = jvcDetails.get(0).getJvname();
            }
        %>
        <div id="dialog-form" title="Add Partner">
            <fieldset>
                <table width="100%" cellspacing="0">
                    <tr>
                        <td colspan="4">
                        <%--Other PE Search--%>
                        <div class="formBg_1"  id="pe2">
                            <input type="hidden" id="pe1count" value="">
                            <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                               <%-- <tr>
                                    <td width="22%" class="ff">Company Name :</td>
                                    <td width="78%">
                                        <input type="text" id="txtCompanyName" name="txtCompanyName" style="width: 200px;">
                                    </td>
                                </tr>--%>
                                <tr>
                                
                                    <td class="ff" width="22%">e-mail ID : <span class="mandatory">*</span></td>
                                    <td width="78%">
                                        <input type="text" id="txtEmailId" name="txtEmailId" style="width: 200px;" >
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <label class="formBtn_1">
                                            <input type="button" id="btnSearch" value="Search" />
                                        </label>
                                    </td>
                                   
                                </tr>
                            </table>
                            </div>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <%--<th width="10%" class="t-align-center">Select</th>--%>
                                    <th class="t-align-center" width="45%">Company Name</th>
                                    <th class="t-align-center" width="45%">e-mail ID</th>
                                </tr>
                                <tbody id="tbodype">
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">JVCA Partner
            <span style="float: right;">
                <a class="action-button-goback" href="JvcaList.jsp">Go Back to Dashboard</a>
            </span>
        </div>
        <div class="contentArea_1">
            <form action="<%=contextPath%>/JVCAServlet" method="post" id="frmComm">
                <% if(jvcId!=null && !"".equalsIgnoreCase(jvcId)){ %>
                    <input type="hidden" name="funName" id="funName" value="updateJVCA" />
                    <input type="hidden" name="jvcId" id="jvcId" value="<%=jvcId%>" />
                <% }else{ %>
                    <input type="hidden" name="funName" id="funName" value="createJVCA" />
                <% } %>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="formStyle_1 t_space">
                        <tr>
                            <td width="15%" class="t-align-left ff">JVCA Name : <span class="mandatory">*</span></td>
                            <td width="85%" class="t-align-left">
                                <input type="text" name="txtJVCAName" id="txtJVCAName" value="<%=JVName%>" class="formTxtBox_1" onblur="checkCompanyName();" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="t-align-right"><a id="addmem" class="action-button-add">Add Partner</a></div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                        <thead>
                            <tr>
                                <th width="28%" class="t-align-left ff">Company Name</th>
                                <th width="27%" class="t-align-left ff">e-mail ID </th>
                                <th width="20%" class="t-align-left ff">Lead or Secondary Partner</th>
                                <th width="25%" class="t-align-left ff">Nominated Partner</th>
                                <th width="10%" class="t-align-left ff">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <%
                                if(jvcId!=null && !"".equalsIgnoreCase(jvcId)){
                                    List<Object[]> listPartner = jvcSrBean.getJvcaPartnerDetailList(Integer.parseInt(jvcId));
                                    if(listPartner.size() > 0){
                                        int i=0;
                                        for(Object[] Partner:listPartner){
                                        %>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="companyIds" value="<%=Partner[5]%>" id="companyid<%=i%>" />
                                                <%=Partner[0]%>
                                            </td>
                                            <td>
                                                <input type="hidden" name="userIds" value="<%=Partner[6]%>" id="userid<%=i%>" />
                                                <%=Partner[4]%>
                                            </td>
                                            <td>
                                                <select name="cmbPartnerType" id="cmbPartnerType<%=i%>" class="formTxtBox_1" style="width: 150px;">
                                                    <option value="">--Select--</option>
                                                    <option value="Lead" <% if("Lead".equalsIgnoreCase(Partner[1].toString())){%>selected="selected"<% } %>>Lead</option>
                                                    <option value="Secondary" <% if("Secondary".equalsIgnoreCase(Partner[1].toString())){%>selected="selected"<% } %>>Secondary</option>
                                                </select>
                                            </td>
                                            <td class="t-align-center">
                                                <input type="radio" name="radioisNominated" id="radioisNominated<%=i%>" value="<%=Partner[6]%>" <% if("Yes".equalsIgnoreCase(Partner[2].toString())){ %>checked="checked"<% } %> />
                                            </td>
                                            <td class="t-align-center">
                                                <% if(!(userId==Integer.parseInt(Partner[6].toString()))){ %>
                                                <a id="delRow<%=i%>" class="action-button-delete" onclick="delRow(this)">Remove</a>
                                                <% }else{ %>
                                                &nbsp;
                                                <% } %>
                                            </td>
                                        </tr>
                                       <% i++; }
                                    }
                                 }else{
                                    List<Object[]> listPartner = jvcSrBean.getOwnCompanyDetails(userId);
                               %>
                                  <tr>
                                    <td>
                                        <input type="hidden" name="companyIds" 
                                               <%if(!listPartner.isEmpty()){%>
                                               value="<%=listPartner.get(0)[0]%>" 
                                               <%}%>
                                               id="companyid0" />
                                        <%if(!listPartner.isEmpty()){%>
                                        <%=listPartner.get(0)[1]%>
                                        <%}%>
                                        
                                    </td>
                                    <td>
                                        <input type="hidden" name="userIds" 
                                               <%if(!listPartner.isEmpty()){%>
                                               value="<%=listPartner.get(0)[3]%>" 
                                               <%}%>
                                               id="userid0" />
                                        <%if(!listPartner.isEmpty()){%>
                                        <%=listPartner.get(0)[2]%>
                                        <%}%>
                                    </td>
                                    <td>
                                        <select name="cmbPartnerType" id="cmbPartnerType0" class="formTxtBox_1" style="width: 150px;">
                                            <option value="">--Select--</option>
                                            <option value="Lead" selected="selected">Lead</option>
                                            <option value="Secondary">Secondary</option>
                                        </select>
                                    </td>
                                    <td class="t-align-center">
                                        <input type="radio" name="radioisNominated" id="radioisNominated0" 
                                               <%if(!listPartner.isEmpty()){%>
                                               value="<%=listPartner.get(0)[3]%>" 
                                               <%}else{%>
                                               value="" 
                                               <% } %>
                                               checked="checked" />
                                    </td>
                                    <td class="t-align-center">
                                        &nbsp;
                                    </td>
                                </tr>
                               <% } %>
                        </tbody>
                    </table>
                    <div>&nbsp;</div>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="submit" type="submit" value="Submit" id="btnSubmit" onclick="return validate();"/>
                        </label>
                    </div>
                </div>
            </form>
        </div>
        <div>&nbsp;</div>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
