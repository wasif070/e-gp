<%-- 
    Document   : TabContractTermination
    Created on : Aug 29, 2011, 7:02:34 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Termination</title>
    </head>
    <%
                pageContext.setAttribute("TSCtab", "4");
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                String msg = null;
                if (request.getParameter("msg") != null) {
                    msg = request.getParameter("msg");
                }
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Contract Termination
                            </div>
                            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
                            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                            <%
                                CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
                                String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
                                List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
                                Object[] objj = null;
                                if(lotObj!=null && !lotObj.isEmpty())
                                {
                                    objj = lotObj.get(0);
                                    pageContext.setAttribute("lotId", objj[0].toString());
                                }
                                if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
                                %>
                                <%@include file="../resources/common/ContractInfoBar.jsp" %>
                                <%}%>
                            <%
                                        pageContext.setAttribute("tab", "14");
                            %>
                            <%@include file="TendererTabPanel.jsp"%>
                            <div class="tabPanelArea_1">
                                <%@include  file="../tenderer/cmsTab.jsp"%>
                                <div class="tabPanelArea_1">
                                    <%if (msg != null && msg.contains("Duplicate")) {%>
                                    <div id="successMsg" class="responseMsg errorMsg t_space" ><span>Procuring Entity has already Terminated the Contract</span></div>
                                    <%}%>
                                    <%
                                                pageContext.setAttribute("TSCtab", "4");
                                    %>
                                    <div align="center">
                                        <%
                                                    final int ZERO = 0;
                                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                                     List<SPCommonSearchDataMore> packageLotList = null;
                                                     String conType = commonService.getServiceTypeForTender(Integer.parseInt(tenderId));
                                                        if("services".equalsIgnoreCase(procnature)){
                                                            packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                        }else{
                                                            packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                        }
                                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        %>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                            <tr>
                                                <td width="20%">Lot No.</td>
                                                <td width="80%"><%=lotList.getFieldName3()%></td>
                                            </tr>
                                            <tr>
                                                <td>Lot Description</td>
                                                <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                            </tr>
                                            <tr>
                                                <td >Contract Termination</td>
                                                <td>
                                                    <%

                                                    Object[] object = issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5())).get(ZERO);
                                                    if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                                        int contractSignId = 0;
                                                        contractSignId = issueNOASrBean.getContractSignId();
                                                        CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                                        cmsContractTerminationBean.setLogUserId(userId);
                                                        TblCmsContractTermination tblCmsContractTermination =
                                                                cmsContractTerminationBean.getCmsContractTerminationForContractSignId(contractSignId);
                                                        if (tblCmsContractTermination == null) {
                                                            out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                        }
                                                        else{
                                                            if("rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                            {
                                                                out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + " '>Terminate</a> ");

                                                            }else{
                                                            out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                    + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Contract Termination</a>");
                                                           // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                            out.print("&nbsp|&nbsp<a href ='ContractTeminationDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                    + "&lotId=" + lotList.getFieldName5() + "&action=upload'>Upload/Download Files</a>");
                                                            }
                                                        }//end else
                                                        out.print("&nbsp|&nbsp<a href='"+request.getContextPath()+"/resources/common/CTHistory.jsp?tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Contract Termination History</a>");
                                                    }//end main if

                                                    %>
                                                </td>
                                            </tr>
                                       
                                        <br>
                                         <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(lotList.getFieldName5()));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));

                        %>

                              <tr>
                                    <td colspan="2">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                                    </tr>
                                               <tr>
                                                <td>Contract Termination</td>
                                                <td>
                                         <%

                                                    Object[] objects = issueNOASrBean.getDetailsForRO(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(objs[0].toString())).get(ZERO);
                                                    if (issueNOASrBean.isAvlTCS((Integer) objects[1])) {
                                                        int contractSignId = 0;
                                                        contractSignId = issueNOASrBean.getContractSignId();
                                                        CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                                        cmsContractTerminationBean.setLogUserId(userId);
                                                        TblCmsContractTermination tblCmsContractTermination =
                                                                cmsContractTerminationBean.getCmsContractTerminationForContractSignId(contractSignId);
                                                        if (tblCmsContractTermination == null) {
                                                            out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                        }
                                                        else{
                                                            if("rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                            {
                                                                out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + " '>Terminate</a> ");

                                                            }else{
                                                            out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                    + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Contract Termination</a>");
                                                           // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                            out.print("&nbsp|&nbsp<a href ='ContractTeminationDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) objects[8]
                                                                    + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                    + "&lotId=" + lotList.getFieldName5() + "'>Download Files</a>");
                                                            }
                                                        }//end else
                                                        out.print("&nbsp|&nbsp<a href='"+request.getContextPath()+"/resources/common/CTHistory.jsp?tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Contract Termination History</a>");
                                                    }//end main if

                                                    %>
                                                </td>
                                            </tr>





                                        <%
                                                  }}}  }
                                        %>
                                     </table>
                                        </div>
                                </div>
                            </div>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
      <script type="text/javascript">
          function checkDupli(ContractId)
          {
              var flag = true;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/ContractTerminationServlet",
                        data:"contractId="+ContractId+"&"+"funName=checkDuplicateRecord",
                        async: false,
                        success: function(j){                           
                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Procuring Entity has already Terminated the Contract","Contract Termination", function(RetVal) {
                                });
                            }
                        }
                    });
                    return flag;
          }
      </script>
    </body>
</html>

