<%-- 
    Document   : Generate Invoice
    Created on : Dec 12, 2011, 2:08:23 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="java.lang.reflect.Array"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.math.BigDecimal"%>


<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generate Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Generate Attendance Sheet Invoice
            <span class="c-alignment-right"><a href="InvoiceServiceCase.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
            pageContext.setAttribute("tab", "10");
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String sheetId = "";
            if(request.getParameter("sheetId")!=null)
            {
                sheetId = request.getParameter("sheetId");
            }
            String wpId = "";
            if(request.getParameter("wpId")!=null)
            {
                wpId = request.getParameter("wpId");
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }

            %>
                <div class="tabPanelArea_1">
                    <div align="center">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

                                %>
                                <form name="prfrm" id="prfrm" action="<%=request.getContextPath()%>/CMSSerCaseServlet?action=GenerateInvoiceTenSide" method="post">
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>

                                                <th width="3%" class="t-align-center">Sr.No.</th>
                                                <th width="20%" class="t-align-center">Name of Employees</th>
                                                <th width="15%" class="t-align-center">Position Assigned
                                                </th>
                                                <th width="9%" class="t-align-left">Home / Field
                                                </th>
                                                <th width="5%" class="t-align-left">No of Days
                                                </th>

<!--                                                <th width="5%" class="t-align-center">No of Days by PE
                                                </th>-->

                                                <th width="5%" class="t-align-left">Rate
                                                </th>
                                                <th width="18%" class="t-align-left">Month
                                                </th>
                                                <th width="18%" class="t-align-left">Year
                                                </th>
                                                <th width="18%" class="t-align-left">Invoice Amount
                                                </th>
                                            </tr>
                                            
                                                <%
                                                CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                                List<Object[]> list = cmss.ViewAttSheetDtls(Integer.parseInt(request.getParameter("sheetId")));
                                                String weeks_days[] = new String[7];
                                                MonthName monthName = new MonthName();
                                                BigDecimal TotalAmount = new BigDecimal(0);
                                                if(list!=null && !list.isEmpty()){
                                                for(Object obj[] : list){
                                                TotalAmount = TotalAmount.add((BigDecimal)obj[10]) ;
                                                %>
                                                <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%>
                                                </td>
                                                <td width="9%" class="t-align-left"><%=obj[7]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=obj[3]%>
                                                </td>
<!--                                                <td width="5%" class="t-align-center"><%=obj[11]%>
                                                </td>-->
                                                <td width="5%" class="t-align-left"><%=obj[4]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=obj[6]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=obj[10]%>
                                                </td>
                                                </tr>
                                               <% }}%>

                                               <tr><td class="t-align-center" colspan=7></td>
                                                   <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                                   <td class="t-align-right ff" style="text-align: right;"><%=TotalAmount%></td>
                                               </tr>
                                               <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                               <tr>
                                                   <td class="t-align-left ff" colspan=8 >Remarks : </td>
                                                   <td width="%" class="t-align-left">
                                                   <label>
                                                       <textarea name="textarea<%=sheetId%>" rows="3" class="formTxtBox_1" id="txtReply" style="width:400px;"></textarea>
                                                   </label>
                                                   </td>
<!--                                                   <td class="t-align-left" colspan="7"></td>-->
                                               </tr>   
                                               </table>
                                 </table>
                                               <table width="100%" cellspacing="0" class=" t_space" id="resultTable">
                                                   <tr>
                                                       <td align="left"><center>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="submitbtn" id="subpref" value="Generate Invoice"/>
                                                            <input type="hidden" name="tenderId" value="<%=tenderId%>">
                                                            <input type="hidden" name="lotId" value="<%=lotId%>">
                                                            <input type="hidden" name="wpId" value="<%=wpId%>">
                                                            <input type="hidden" name="sheetId" value="<%=sheetId%>">
                                                            <input type="hidden" name="totalamount" value="<%=TotalAmount%>">                                                            
                                                            <input type="hidden" name="Sheetflag" value="true">
                                                        </label></center>
                                                    </td>
                                                    </tr>
                                               </table>    
                                </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        yearset();
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        monthset();


    </script>
</html>

