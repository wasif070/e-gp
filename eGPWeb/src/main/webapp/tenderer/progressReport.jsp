<%--
    Document   : ProgressReport
    Created on : Jul 25, 2011, 12:46:45 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script>
           
            function ViewPR(wpId,tenderId,lotId){
                dynamicFromSubmit("viewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
            function ViewPRR(wpId,tenderId,lotId,cntId){
                dynamicFromSubmit("viewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
            }
        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>        
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Progress Report</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
            CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
            String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
            String procType = commonServiceee.getProcurementType(request.getParameter("tenderId")).toString();
            List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
            Object[] objj = null;
            if(lotObj!=null && !lotObj.isEmpty())
            {
                objj = lotObj.get(0);
                pageContext.setAttribute("lotId", objj[0].toString());
            }
            if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
            %>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <%}%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");

            %>
            <%@include file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "2");

                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">                    
                    <div align="center">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                                    String tenderId = request.getParameter("tenderId");
                                    boolean flagg = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    int i = 0;
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        List<Object[]> forms = dDocSrBean.getAllBoQForms(Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(tenderId));
                                       // List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));

                                        /* Dohatec Start*/
                                              List<Object> wpid;
                                                if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                {
                                                     wpid = service.getWpIdForTenderForms(Integer.parseInt(lotList.getFieldName5()),tenderId);
                                                }
                                                else
                                                {
                                                    wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                                }
                                        /* Dohatec End*/
                                        boolean isDateEdited = service.checkForDateEditedOrNot(Integer.parseInt(lotList.getFieldName5()));
                                        boolean chkContract = service.checkContractSigning(tenderId, Integer.parseInt(lotList.getFieldName5()));
                        %>
                        <script type="text/javascript">
                            document.getElementById('pckNo').innerHTML = '<%=lotList.getFieldName1()%>';
                            document.getElementById('pckDesc').innerHTML = '<%=lotList.getFieldName2()%>';
                        </script>

                          <%    if (request.getParameter("msg") != null) {
                                                    if ("reqested".equals(request.getParameter("msg"))) {%>

                                                    <div  class='responseMsg successMsg t-align-left'>
                                                       Work Completion Certificate request posted successfully
                                                    </div>


                                        <%


                                                    }
                                                }
                                        %>

                                        <form name="frmcons" method="post">


                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                <%
                                                                        int countt = 1;
                                                                        if (!wpid.isEmpty() && wpid != null) {
                                %>                                
                                <tr><td colspan="2">
                                        <span style="float: left; text-align: left;font-weight:bold;">
                                            <a href="../resources/common/ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>" class="">Financial Progress Report</a>
                                        </span>                                        
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="17%" class="t-align-center"><%=bdl.getString("CMS.PR.report")%></th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                            </tr>
                                            <%
                                                                                                                        for (Object obj : wpid) {
                                                                                                                            String toDisplay = service.getConsolidate(obj.toString());

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td><%=toDisplay.replace("Consolidate", "Progress report")%></td>
                                                <td>
                                                    <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View History</a>
                                                </td>
                                            </tr>
                                            <%
                                                                                                                            countt++;
                                                                                                                        }
                                            %>

                                        </table>
                                        <%
                                        String userId = "";
                                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                            userId = session.getAttribute("userId").toString();
                                            issueNOASrBean.setLogUserId(userId);
                                        }
                                        Object[] object = issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5())).get(0);
                                        if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                            int contractSignId = 0;
                                            contractSignId = issueNOASrBean.getContractSignId();
                                            CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                            cmsWcCertificateServiceBean.setLogUserId(userId);
                                            TblCmsWcCertificate tblCmsWcCertificate =
                                                    cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);

                                            if (tblCmsWcCertificate != null) {
                                                out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                                out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                                out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                        + "&lotId=" + lotList.getFieldName5() + "&action=Request'>Request for Work Completion Certificate </a>");
                                                }
                                               // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                out.print("&nbsp|&nbsp<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                        + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Work Completion Certificate </a>");
                                                //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("&nbsp|&nbsp<a href ='WCCDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                            + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                            +"&lotId=" + lotList.getFieldName5() + "'>Download Files</a>");
                                                }
                                                out.print(" </td>");
                                                out.print(" </tr>");
                                                out.print(" <tr>");
                                                out.print("</table>");
                                            }else{
                                                out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                                out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                                out.print("<td  width='80%'  class='t-align-left'>");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),0)){
                                                    out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "&action=Request&cntId=0'>Request for Work Completion Certificate</a>");
                                                }else{
                                                    out.print("Already requested for Work Completion Certificate");
                                                }
                                                out.print(" </td>");
                                                out.print(" </tr>");
                                                out.print(" <tr>");
                                                out.print("</table>");
                                            }
                                        }//if


                                        %>
                                    </td></tr>
                                    <%
                                                                            }
                                    %>



                                 <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(lotList.getFieldName5()));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));

                        %>

                              <tr>
                                    <td colspan="3">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                                                               <%
                                                                        int countR = 1;
                                                                        if (!wpids.isEmpty() && wpids != null) {
                                %>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="17%" class="t-align-center"><%=bdl.getString("CMS.PR.report")%></th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                            </tr>
                                            <%
                                            int showR=0;
                                                    for (Object obj : wpids) {
                                                        List<Object[]> formsR = dDocSrBean.getAllBoQFormsForRO(Integer.parseInt(lotList.getFieldName5()));


                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countR%></td>
                                                <td><%= "Progress report "+formsR.get(showR)[1] %></td>
                                                <td>
                                                    <a href="#" onclick="ViewPRR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">View History</a>
                                                </td>
                                            </tr>
                                            <%
                                                                                                                            countR++;
                                                                                                                            showR++;
                                                                                                                        }
                                            %>

                                        </table>
                                        <%
                                        String userId = "";
                                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                            userId = session.getAttribute("userId").toString();
                                            issueNOASrBean.setLogUserId(userId);
                                        }
                                        Object[] object = issueNOASrBean.getDetailsForRO(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()),(Integer)objs[0]).get(0);
                                        if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                            int contractSignId = 0;
                                            contractSignId = issueNOASrBean.getContractSignId();
                                            CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                            cmsWcCertificateServiceBean.setLogUserId(userId);
                                            TblCmsWcCertificate tblCmsWcCertificate =
                                                    cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId((Integer)objd[11]);

                                            if (tblCmsWcCertificate != null) {
                                                out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                                out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                                out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11]) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                        + "&lotId=" + lotList.getFieldName5() + "&action=Request'>Request for Work Completion Certificate </a>");
                                                }
                                               // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                out.print("&nbsp|&nbsp<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                        + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Work Completion Certificate </a>");
                                                //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11]) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("&nbsp|&nbsp<a href ='WCCDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                            + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                            +"&lotId=" + lotList.getFieldName5() + "'>Download Files</a>");
                                                }
                                                out.print(" </td>");
                                                out.print(" </tr>");
                                                out.print(" <tr>");
                                                out.print("</table>");
                                            }else{
                                                out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                                out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                                out.print("<td  width='80%'  class='t-align-left'>");
                                                if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11])){
                                                    out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "&action=Request&cntId="+contractSignId+"'>Request for Work Completion Certificate</a>");
                                                }else{
                                                    out.print("Already requested for Work Completion Certificate");
                                                }
                                                out.print(" </td>");
                                                out.print(" </tr>");
                                                out.print(" <tr>");
                                                out.print("</table>");
                                            }
                                        }//if


                                        %>
                                    </td></tr>
                                    <%
                                                                            }
                                    %>






<%}icount++;}
                                                }%>




                                <%
                                            }
                                %>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>