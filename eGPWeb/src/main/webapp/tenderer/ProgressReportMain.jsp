<%-- 
    Document   : ProgressReportMain
    Created on : Dec 10, 2011, 10:53:04 AM
    Author     : shreyansh Jogi
--%>



<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <script>
            function ViewPR(wpId,tenderId,lotId){
                dynamicFromSubmit("SrvViewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
        </script>
        <%
            ResourceBundle bdl = null;
            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
            bdl = ResourceBundle.getBundle("properties.cmsproperty");
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Progress Report</div>
                <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                    }
                    pageContext.setAttribute("tab", "10");

                    CommonService commonServicee = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnaturee = commonServicee.getProcNature(request.getParameter("tenderId")).toString();
                    String strProcNaturee = "";
                    String strLotNo = "Lot No.";
                    String strLotDes = "Lot Description";
                    if("Services".equalsIgnoreCase(procnaturee))
                    {
                        strLotNo = "Package No.";
                        strLotDes = "Package Description";
                        strProcNaturee = "Consultant";
                    }else if("goods".equalsIgnoreCase(procnaturee)){
                        strProcNaturee = "Supplier";
                    }else{
                        strProcNaturee = "Contractor";
                    }

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
                String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
                List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
                Object[] objj = null;
                if(lotObj!=null && !lotObj.isEmpty())
                {
                    objj = lotObj.get(0);
                    pageContext.setAttribute("lotId", objj[0].toString());
                }%>
                <%@include file="../resources/common/ContractInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include file="TendererTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "2");

                    %>
                    <%@include  file="../tenderer/cmsTab.jsp"%>
                    <div class="tabPanelArea_1">
                       
                            <%    if (request.getParameter("msg") != null) {
                                        if ("reqested".equals(request.getParameter("msg"))) {%>
                                        <div  class='responseMsg successMsg t-align-left'>
                                           Work Completion Certificate request posted successfully
                                        </div>
                            <%
                                        }
                                        if ("fin".equals(request.getParameter("msg"))) {%>
                                        <div  class='responseMsg successMsg t-align-left'>
                                           Attendance Sheet created successfully
                                        </div>
                            <%
                                        }if ("updated".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Attendance Sheet edited successfully</div>
                    <%}
                                    }
                            %>
                            <%
                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                for (Object[] obj : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                            %>
 <div align="center">
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left"><%=strLotNo%></td>
                                    <td   class="t-align-left"><%=obj[6]%></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"><%=strLotDes%></td>
                                    <td  class="t-align-left"><%=obj[7]%></td>
                                </tr>
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr><td>
                                <span style="float: left; text-align: left;font-weight:bold;">
                                    <a href="../resources/common/ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=obj[0].toString()%>" class="">Financial Progress Report</a>
                                </span>
                                </td>
                                </tr>    
                            </table>    
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left">Attendance and Progress Report</td>
                                    <td class="t-align-left"><a href="ProgressReportForSer.jsp?tenderId=<%=tenderId%>&lotId=<%=obj[0].toString()%>">Create</a></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"></td>
                                    <td  class="t-align-left">
                                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                            <tr>
                                                <th width="12%" class="t-align-left">Sr.No.</th>
                                                <th width="20%" class="t-align-left">Attendance and Progress Report</th>
                                                <th class="t-align-left">Month-Year</th>
                                                <th class="t-align-left">Created Date</th>
                                                <th class="t-align-left">Action</th>
                                                <%
                                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                                    MonthName monthName = new MonthName();
                                                    String wpId = service.getWpIdForService(Integer.parseInt(obj[0].toString()));
                                                    if (os != null && !os.isEmpty()) {
                                                        int cc = os.size();
                                                        int ccc = 1;
                                                        for (int ik=(os.size()-1);ik>=0;ik--){
                                                %>
                                            <tr>
                                                <td width="12%" ><%=ccc%></td>
                                                <td width="15%" >Attendance and Progress Report - <%=cc%></td>
                                                <td class="t-align-left"><%=monthName.getMonth(os.get(ik).getMonthId()) + " - " + os.get(ik).getYear()%></td>
                                                <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(os.get(ik).getCreatedDt())%></td>

                                                <td class="t-align-left">
                                                    <a href="ViewSheetdtls.jsp?sheetId=<%=os.get(ik).getAttSheetId()%>&tenderId=<%=tenderId%>">View</a>
                                                    <% if ("reject".equalsIgnoreCase(cmss.checkAttStatus(os.get(ik).getAttSheetId() + ""))) {%>&nbsp;|&nbsp;
                                                    <a href="EditSheetdtls.jsp?sheetId=<%=os.get(ik).getAttSheetId()%>&tenderId=<%=tenderId%>">Edit</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="AttanshtUploadDoc.jsp?tenderId=<%=tenderId%>&wpId=0&keyId=<%=os.get(ik).getAttSheetId()%>&docx=AttSheet&module=PR">Upload / Download Document</a>
                                                </td>
                                            </tr>

                                            <%cc--;
                                            ccc++;
    }
} else {%>
                                            <tr>
                                                <td colspan="5">No records found</td>
                                            </tr>

                                            <%}%>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space t_space">
                                <tr><td width="15%" class="t_align_left ff">
                                        Reimbursable Expenses 
                                    </td>
                                    <td>
                                        <a href="#" onclick="ViewPR(<%=wpId%>,<%=tenderId%>,<%=obj[0]%>);">View History</a>
                                    </td>
                                </tr>
                            </table>
                            <%
                                Object[] object = issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(obj[0].toString())).get(0);
                                if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                    int contractSignId = 0;
                                    contractSignId = issueNOASrBean.getContractSignId();
                                    CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                    cmsWcCertificateServiceBean.setLogUserId(userId);
                                    TblCmsWcCertificate tblCmsWcCertificate =
                                            cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);

                                    if (tblCmsWcCertificate != null) {
                                        out.print("<table cellspacing='0' class='tableList_1 t_space' width='100%'>");
                                        out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                        out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(obj[0].toString()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                            out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                + "&lotId=" + obj[0].toString() + "&action=Request&type=tb'>Request for Work Completion Certificate </a>");
                                        }
                                       // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                        out.print("&nbsp|&nbsp<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                + "&tenderId=" + tenderId + "&lotId=" + obj[0].toString() + "'>View Work Completion Certificate </a>");
                                        //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(obj[0].toString()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                            out.print("&nbsp|&nbsp<a href ='WCCDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                    + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                    +"&lotId=" + obj[0].toString() + "'>Download Files</a>");
                                        }
                                        out.print(" </td>");
                                        out.print(" </tr>");
                                        out.print(" <tr>");
                                        out.print("</table>");
                                    }else{
                                        out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                        out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                        out.print("<td  width='80%'  class='t-align-left'>");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(obj[0].toString()),0)){
                                            out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&lotId=" + obj[0].toString() + "&action=Request&cntId=0&type=tb&type=tb'>Request for Work Completion Certificate</a>");
                                        }else{
                                            out.print("Already requested for Work Completion Certificate");
                                        }
                                        out.print(" </td>");
                                        out.print(" </tr>");
                                        out.print(" <tr>");
                                        out.print("</table>");
                                    }
                                }
                            %>        

                            <%}%>

                        </div>
                    </div></div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
    issueNOASrBean = null;
%>