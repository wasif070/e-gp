<%--
    Document   : CompanyVerification
    Created on : Dec 31, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />               
        <script>
                function test(type){
                    if(type==1){
                        document.getElementById("linkPending").className = "sMenu";
                        document.getElementById("linkApproved").className = "";
                        document.getElementById("linkRejected").className = "";
                        fillGridOnEvent('pending');
                    }else if(type==2){
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkApproved").className = "sMenu";
                        document.getElementById("linkRejected").className = "";
                        fillGridOnEvent('approved')
                    }else{
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkApproved").className = "";
                        document.getElementById("linkRejected").className = "sMenu";
                        fillGridOnEvent('rejected');
                    }
                }
        </script>
        <script>
                function fillGridOnEvent(type){
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:'<%=request.getContextPath()%>/ContentAdminServlet?q=1&action=fetchData&status='+type+'&cId=<%=request.getParameter("cId")%>',
                        datatype: "xml",
                        height: 250,
                        colNames:['Sl. No.','Registration Type','Company Name','e-mail ID',"Country","State","City","Registration Date and Time","Action"],
                        colModel:[
                            {name:'srNo',index:'srNo', width:35,sortable:false,align:'center'},
                            {name:'registrationType',index:'registrationType', width:130,sortable:false},
                            {name:'companyName',index:'companyName', width:180},
                            {name:'emailId',index:'emailId', width:150},
                            {name:'country',index:'tm.country', width:80},
                            {name:'state',index:'tm.state', width:80},
                            {name:'city',index:'city', width:80,sortable:false},
                            {name:'registeredDate',index:'registeredDate', width:90},
                            {name:'userid',index:'userid', width:130,sortable:false}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});
                }
                jQuery().ready(function (){
                    //fillGrid();
                });
        </script>
    </head>       
    <body onload="fillGridOnEvent('pending')">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%if("a".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">User profile approved successfully.</div>");}else if("r".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">User profile Rejected successfully.</div>");}%>
                <div class="contentArea_1">                
                <ul class="tabPanel_1">
                    <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Pending</a></li>
                    <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);">Approved</a></li>
                    <li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">Rejected</a></li>
                </ul>                          
                <div class="tabPanelArea_1">
                    <div id="jQGrid" align="left">
                        <table id="list"></table>
                        <div id="page"></div>
                    </div>
                </div>                
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
