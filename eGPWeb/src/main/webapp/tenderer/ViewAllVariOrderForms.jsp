<%--
    Document   : ViewAllVariOrderForms
    Created on : Dec 23, 2011, 12:37:25 PM
    Author     : shreyansh
--%>


<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSrvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvRevari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvWpvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Variation Order</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int varOrdId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("varId") != null) {
                        varOrdId = Integer.parseInt(request.getParameter("varId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%
                                    out.print("Variation Order View");
                                    if (request.getParameter("tenderId") != null) {
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                    }


                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <input type="hidden" name="action" id="action" value="acceptVariOrder" />
                        <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                        <input type="hidden" name="varId" id="varId" value="<%=request.getParameter("varId")%>" />
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <tr>
                                   

                                    <%

                                                 List<TblCmsSrvWpvari> listvarwp = cmss.getSrvWpVariationOrder(varOrdId);

                                                try {

                                                    if (listvarwp != null && !listvarwp.isEmpty()) {
                                    %>
                                     <td colspan="6">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Work Schedule</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th width="3%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.srNo")%></th>
                                        <th width="40%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.activity")%></th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.sdate")%></th>
                                        <th width="5%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.noOfDays")%>
                                        </th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.edate")%>
                                        </th>
                                        <th width="15%" class="t-align-center"><%=srbd.getString("CMS.PR.remarks")%>
                                        </th>
                                    </tr>
                                    <%
                                                        int i = 0;
                                                        for (i = 0; i < listvarwp.size(); i++) {
                                                            if (i % 2 == 0) {
                                                                styleClass = "bgColor-white";
                                                            } else {
                                                                styleClass = "bgColor-Green";
                                                            }

                                                            out.print("<tr class='" + styleClass + "'>");
                                                            out.print("<td  class=\"t-align-left\">" + listvarwp.get(i).getSrNo() + "</td>");
                                                            out.print("<td  class=\"t-align-left\">" + listvarwp.get(i).getActivity() + "</td>");
                                                            out.print("<td  class=\"t-align-left\">" + DateUtils.gridDateToStrWithoutSec((Date) listvarwp.get(i).getStartDt()).split(" ")[0] + "</td>");
                                                            out.print("<td style=\"text-align :right;\">" + listvarwp.get(i).getNoOfDays() + "</td>");
                                                            out.print("<td  class=\"t-align-left\">" + DateUtils.gridDateToStrWithoutSec((Date) listvarwp.get(i).getEndDt()).split(" ")[0] + "</td>");
                                                            out.print("<td  class=\"t-align-left\">" + listvarwp.get(i).getRemarks() + "</td>");
                                                            out.print("<input type=hidden name=srvwplanid_" + i + " id=srvwplanid_" + i + " value=" + listvarwp.get(i).getSrNo() + " />");
                                                            out.print("</tr>");
                                                        }
                                                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                                                    } 

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }



                                    %>
                                    <%if (listvarwp != null && !listvarwp.isEmpty()) {%>
                                        <tr>
                                        <td colspan="6">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=listvarwp.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>
                                </table>

                                    <%
                                                List<TblCmsSrvRevari> list = cmss.getSrvReVariationOrder(varOrdId);
                                                if (!list.isEmpty()) {%>
                                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                             <tr>
                                    <td colspan="7">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Reimbursable Expenses</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th>S No</th>
                                        <th>Description Category</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Total Amount</th>
                                    </tr>

                                                <%    for (int i = 0; i < list.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getCatagoryDesc()%></td>
                                        <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                        <td class="t-align-left"><%=list.get(i).getUnit()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getQty()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getUnitCost()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getTotalAmt()%></td>
                                    </tr>
                                    <%}%>
                                    <%if (list != null && !list.isEmpty()) {%>
                                        <tr>
                                        <td colspan="7">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=list.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>
                                    </table>
                                                <%}%>



                                    <%
                                                List<TblCmsSrvCcvari> ccvaris = cmss.getSrvCcVariationOrder(varOrdId);
                                                if (!ccvaris.isEmpty()) {%>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                     <tr>
                                    <td colspan="9">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Consultant Composition</li>
                                </ul>

                                    </td>
                                     <tr>
                                        <th>S No</th>
                                        <th>Consultant Category</th>
                                        <%if (flag) {%>
                                        <th>Indicated No Of Key Professional Staff</th>
                                        <th>Input Staff Months</th>
                                        <%}%>
                                        <th>Indicated No. of Professional Staff</th>
                                        <th>Indicated No. of Support Staff</th>
                                        <th>Consultant Name</th>
                                        <th>No. of Key Professional Staff</th>
                                        <th>No. of Support Professional Staff</th>
                                    </tr>
                                                    <%for (int i = 0; i < ccvaris.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=ccvaris.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=ccvaris.get(i).getConsultantCtg()%></td>
                                        <%if (flag) {%>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getTotalNosPe()%></td>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getMonths()%></td>
                                        <% }%>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getKeyNosPe()%></td>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getSupportNosPe()%></td>
                                        <td class="t-align-left"><%=ccvaris.get(i).getConsultantName()%></td>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getKeyNos()%></td>
                                        <td style="text-align :right;"><%=ccvaris.get(i).getSupportNos()%>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <%if (ccvaris != null && !ccvaris.isEmpty()) {%>
                                        <tr>
                                        <td colspan="9">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=ccvaris.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>
                                    </table>
                                               <% }%>


                                    <%
                                                List<TblCmsSrvPsvari> psvaris = cmss.getSrvPsVariationOrder(varOrdId);
                                                if (!psvaris.isEmpty()) {%>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                     <tr>
                                    <td colspan="6">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Payment Schedule</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th>S No</th>
                                        <%if (!flag) {%>
                                        <th>Milestone Name </th>
                                        <%}%>
                                        <th>Description</th>
                                        <%if (!flag) {%>
                                        <th>Payment as % of Contract Value</th>
                                        <%}%>
                                        <th>Mile Stone Date proposed by PE </th>
                                        <th>Mile Stone Date proposed by Consultant</th>
                                    </tr>

                                                <%    for (int i = 0; i < psvaris.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=psvaris.get(i).getSrNo()%></td>
                                        <%if (!flag) {%>
                                        <td class="t-align-left"><%=psvaris.get(i).getMilestone()%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=psvaris.get(i).getDescription()%></td>
                                        <%if (!flag) {%>
                                        <td style="text-align :right;"><%=psvaris.get(i).getPercentOfCtrVal()%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(psvaris.get(i).getPeenddate())%></td>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(psvaris.get(i).getEndDate())%></td>
                                    </tr>
                                    <%}%>
                                    <%if (psvaris != null && !psvaris.isEmpty()) {%>
                                        <tr>
                                        <td colspan="9">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=psvaris.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>            
                                    </table>
                                         <%       }%>


                                    <%
                                                List<TblCmsSrvTcvari> tcvaris = cmss.getDetailOfTCForVari(varOrdId);
                                                if (!tcvaris.isEmpty()) {%>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                     <tr>
                                    <td colspan="8">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Team Composition</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th>S No</th>
                                        <th>Name of  Staff </th>
                                        <th>Firm/Organization</th>
                                        <th>Staff Category    (Key / Support)</th>
                                        <th>Position Defined by Consultant </th>
                                        <th>Category of Consultant proposed Consultant Category</th>
                                        <th>Area of Expertise</th>
                                        <th>Task Assigned</th>
                                    </tr>

                                                <%    for (int i = 0; i < tcvaris.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=tcvaris.get(i).getSrno()%></td>
                                        <td class="t-align-left"><%=cmss.getEmpName(tcvaris.get(i).getSrvTcvariId()) %></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getOrganization()%></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getStaffCat()%></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getPositionDefined()%></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getConsultPropCat()%></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getAreaOfExpertise() %></td>
                                        <td class="t-align-left"><%=tcvaris.get(i).getTaskAssigned()%></td>
                                    </tr>
                                    <%}%>
                                    <%if (tcvaris != null && !tcvaris.isEmpty()) {%>
                                        <tr>
                                        <td colspan="8">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=tcvaris.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>                        
                                    </table>
                                         <%       }%>



                                    <%
                                                List<TblCmsSrvSsvari> ssvaris = cmss.getDetailOfSSForVari(varOrdId);
                                                if (!ssvaris.isEmpty()) {%>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                     <tr>
                                    <td colspan="6">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Staffing Schedule</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th>S No</th>
                                        <th>Name of Employees </th>
                                        <th>Home / Field</th>
                                        <th>Start Date </th>
                                        <th>No of Days</th>
                                        <th>End Date</th>
                                    </tr>

                                                <%    for (int i = 0; i < ssvaris.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=ssvaris.get(i).getSrno()%></td>
                                        <td class="t-align-left"><%=cmss.getEmpName(ssvaris.get(i).getSrvTcvariId()) %></td>
                                        <td class="t-align-left"><%=ssvaris.get(i).getWorkFrom()%></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) ssvaris.get(i).getStartDt()).split(" ")[0]%></td>
                                        <td style="text-align :right;"><%=ssvaris.get(i).getNoOfDays()%></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) ssvaris.get(i).getEndDt()).split(" ")[0]%></td>

                                    </tr>
                                    <%}%>
                                    <%if (ssvaris != null && !ssvaris.isEmpty()) {%>
                                        <tr>
                                        <td colspan="6">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=ssvaris.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>      
                                    </table>
                                         <%}%>


                                    <%
                                                List<TblCmsSrvSrvari> srvaris = cmss.getDetailOfSRForVari(varOrdId);
                                                if (!srvaris.isEmpty()) {%>

                                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                     <tr>
                                    <td colspan="5">
                                <ul class="tabPanel_1 noprint">
                                    <li class="sMenu button_padding">Salary Reimbursable</li>
                                </ul>

                                    </td>
                                    <tr>
                                        <th width="5%">S No</th>
                                        <th width="60%">Name of Employees </th>
                                        <th width="15%">Home / Field</th>
                                        <th width="10%">Staff Months</th>
                                        <th width="10%">Staff Month Rate</th>
                                    </tr>
                                                <%    for (int i = 0; i < srvaris.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=srvaris.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=cmss.getEmpName(srvaris.get(i).getSrvTcvariId()) %></td>
                                        <td class="t-align-left"><%=srvaris.get(i).getWorkFrom()%></td>
                                        <td style="text-align :right;"><%=srvaris.get(i).getWorkMonths()%></td>
                                        <td style="text-align :right;"><%=srvaris.get(i).getEmpMonthSalary()%></td>

                                    </tr>
                                    <%}%>
                                    <%if (srvaris != null && !srvaris.isEmpty()) {%>
                                        <tr>
                                        <td colspan="5">
                                        <a href="DownloadVariDocs.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=0&keyId=<%=srvaris.get(0).getSrvFormMapId()%>&varOrdId=<%=varOrdId%>&docx=VariOrder&module=WS&flag=<%=request.getParameter("flag")%>">Download Document</a>
                                        </td>
                                        </tr>
                                    <%}%>            
                                    </table>
                                         <%       }%>


                                    <%makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Variation Order", "");
                                    %>

                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <input type="hidden" id="lotId" name="lotId" value=<%=lotId%> />
                        <br />
                        <center>
                            <%if(request.getParameter("flag")!=null){
                                if(request.getParameter("flag").equalsIgnoreCase("sendtoten")){
                            %>
                                <label class="formBtn_1">
                                    <input type="submit" name="Boqbutton" id="Boqbutton" value="Accept"  />
                                     </label>
                                <%}}%>



                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>

