<%-- 
    Document   : AssignTenderSubRight
    Created on : Apr 28, 2011, 2:00:25 PM
    Author     : KETAN
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderSubRightServiceImpl"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.UserRegisterServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Collections"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }

                    String strTenderId = "";
                    String strRefNo = "";
                    String strPEOfficeName = "";
                    strTenderId = request.getParameter("tenderId");
                    strRefNo = request.getParameter("referenceNo");
                    strPEOfficeName = "";

                    TenderSubRightServiceImpl tenderRightsServiceImpl = (TenderSubRightServiceImpl) AppContext.getSpringBean("TenderSubRightService");
                    List<Object[]> listTendererMaster = tenderRightsServiceImpl.getActiveUserOfCmpnyByUserId(logUserId);

                    List<TblTenderDetails> listTenderDetails = tenderRightsServiceImpl.getTenderDetails(strTenderId);
                    strPEOfficeName = listTenderDetails.get(0).getPeOfficeName();

                    String strTRAssigneeName = "";
                    String strTRAssigneeId = "";
                    strTRAssigneeId = request.getParameter("assigneeId");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Assign Tender Submission Right to the User</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    </head>

    <script type="text/javascript">

        function validate()
        {
            if($.trim($("#sel_assingTo").val())=="" ){
                $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please select user.</div>");
                return false;
            }
            else if(document.getElementById("<%=strTRAssigneeId%>") != null)
            {
                if(document.getElementById("sel_assingTo").value == document.getElementById("<%=strTRAssigneeId%>").value){
                    jAlert("Tender rights already assigned to the user, you selected. <br />Please select another user.","Assign Tender Submission Right to the User",function(RetVal) {
                    });
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        function defaultAssignee()
        {
            if(document.getElementById("<%=strTRAssigneeId%>") != null){
                document.getElementById("<%=strTRAssigneeId%>").selected = true;
            }
        }
    </script>


    <body onload="defaultAssignee();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->
                                    <td>
                                        <div class="pageHead_1">
                                            Assign Tender Submission Right to the User
                                            <span class="c-alignment-right"><a href="TenderSubRight.jsp" class="action-button-goback">Go Back</a></span>
                                        </div>
                                        <div class="t_space">
                                        </div>
                                        <form id="frmTenderRights" name="frmTenderRights" method="post" action="<%=request.getContextPath()%>/TenderSubRightServlet" >
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff" width="15%">
                                                        Tender ID :
                                                    </td>
                                                    <td><%=strTenderId%>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td width="15%" class="ff">Reference No. : </td>
                                                    <td width="85%"><%=strRefNo%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="ff">Title / Brief:</td>
                                                    <td width="85%">
                                                        <%=listTenderDetails.get(0).getTenderBrief()%>
                                                    </td>
                                                </tr>
                                                <% 
                                                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                                List<SPCommonSearchData> finalSubcheck = commonSearchService.searchData("ChkCompanyFinalSubmission", strTenderId, session.getAttribute("userId").toString() , null, null, null, null, null, null, null);
                                                if (finalSubcheck.get(0).getFieldName1().equals("true")) {
                                                %>
                                                <tr>       
                                                    <td width="15%" class="ff"></td>
                                                    <td width="85%" style="color:green; font-size: 20px;"> Final Submission already done for this Company </td>
                                                </tr>
                                                <% } else { %>
                                                <tr>
                                                    <td width="15%" class="ff">Assign To:</td>
                                                    <td width="85%">
                                                        <select name="sel_assingTo" id="sel_assingTo" class="formTxtBox_1" style="width:200px;" >
                                                            <%
                                                                        for (Object[] obj : listTendererMaster) {
                                                                            out.println("<option id=" + obj[1] + " value=" + obj[1] + ">" + obj[3] + " " + obj[4] + " " + obj[5] + " " + obj[6] + "</option>");
                                                                        }
                                                            %>

                                                        </select>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td  width="15%" class="ff">&nbsp;</td>
                                                    <td width="85%"><label class="formBtn_1"><input name="button" type="submit" id="button" value="Submit" onclick="return validate();" /></label></td>
                                                </tr>
                                                <% } %>
                                            </table>
                                            <input type="hidden" name="hdn_action" id="hdn_action" value="assignTenderRights" />
                                            <input type="hidden" name="hdn_tenderId" id="hdn_tenderId" value="<%=strTenderId%>" />
                                            <input type="hidden" name="hdn_refNo" id="hdn_refNo" value="<%=strRefNo%>" />
                                            <input type="hidden" name="hdn_peOfficeName" id="hdn_peOfficeName" value="<%=strPEOfficeName%>" />
                                        </form>
                                        <!--Page Content End-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>