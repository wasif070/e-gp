<%--
    Document   : repeatOrderMain
    Created on : Jul 30, 2011, 11:22:36 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            CommonService commonService1 = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature1 = commonService1.getProcNature(request.getParameter("tenderId")).toString();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Repeat Order</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">

            <div class="pageHead_1">
                Repeat Order
            </div>
            <%
                        String tenderId = request.getParameter("tenderId");
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", request.getParameter("tenderId"), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);%>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
            <% pageContext.setAttribute("lotId", packageLotList.get(0).getFieldName5());%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String userId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                            appSrBean.setLogUserId(userId);
                        }
            %>
           <%@include file="TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

               <%
                            pageContext.setAttribute("TSCtab", "6");

                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("flag") != null) {
                                    if ("true".equalsIgnoreCase(request.getParameter("flag"))) {

                    %>
                    <div class='responseMsg successMsg'>Letter of Acceptance (LOA) action taken successfully</div>
                    <%}
                                 
                    

                                }
                    %>
                    <div align="center">

                        <%


                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                    int i = 0;
                                    int lotId = 0;
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        lotId = Integer.parseInt(lotList.getFieldName5());
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(lotList.getFieldName5()));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    if(objs[2].toString().equalsIgnoreCase("finalise")){
                                                        List<Object> wpid = ros.getWpIdForRO(Integer.parseInt(objs[1].toString()));

                        %>
                        <form action="<%=request.getContextPath()%>/ConsolidateServlet" method="post">
                            <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>" />
                            <input type="hidden" name="cntrctvalue" id="cntrctvalue" value="<%=c_obj[2].toString()%>" />
                            <input type="hidden" name="action"  value="finaliseRO" />
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">

                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>
                                    
                            
                                <%
                                                                        int countt = 1;
                                                                        if (!wpid.isEmpty() && wpid != null) {%>
                               
                                        <table width="100%" cellspacing="0" class="tableList_1 ">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="14%" class="t-align-center">
                                                    <%=bdl.getString("CMS.Goods.Forms")%>

                                                </th>
                                                <th width="6%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>

                                            </tr>
                                            <%

                                                        for (Object obj : wpid) {
                                                            String toDisplay = service.getConsolidate(obj.toString());

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td>
                                                    <%=toDisplay%>

                                                </td>
                                                <td>
                                                   
                                                    <a href="#" onclick="ROview(<%=obj.toString()%>,<%=tenderId%>,<%=lotId%>,<%=objs[1].toString()%>) ">View Repeat Order </a>

                                            </tr>

                                            <%    countt++;
                                                                                                                    }    } %>
                                        </table>
                                                                                                                    
                                        <ul class="tabPanel_1 noprint t_space ">
                                    <li class="sMenu button_padding">Letter of Acceptance(LOA)</li>
                                </ul>
                                     <table width="100%" cellspacing="0" class="tableList_1 ">
                                    <tr>
                                        <th width="5%" class="t-align-left">Contract  No.</th>
                                        <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                                        <th width="11%" class="t-align-left">Date of issue of Letter of Acceptance (LOA)</th>
                                        <th width="11%" class="t-align-left">Deadline of Acceptance of Letter of Acceptance (LOA)</th>
                                        <th width="18%" class="t-align-left">Letter of Acceptance (LOA) Acceptance Status</th>
                                        <th width="11%" class="t-align-left">Accept / Decline Date & Time</th>
                                        <th width="11%" class="t-align-left">Action</th>
                                    </tr><%
                                                                                            List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("ContractListingTenderSideForRO", c_obj[14].toString(), request.getParameter("tenderId"), lotId+"", objs[0].toString() , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                            //List<SPTenderCommonData> contractlist = tenderCommonService.returndata("ContractListing", userid, tenderid);
                                                                                            String temp="";
                                                                                            for (SPCommonSearchDataMore contractlists : sPCommonSearchDataMores) {
                                                                                                temp = contractlists.getFieldName6();
                            %>
                            <tr>
                                <td class="t-align-center"><%=contractlists.getFieldName1()%></td>
                                <td style="text-align: right;"><%=contractlists.getFieldName2()%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName3()%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName9()%></td>
                                <td class="t-align-center"><% if (contractlists.getFieldName6().equalsIgnoreCase("pending")) {%>Pending<% } else if (contractlists.getFieldName6().equalsIgnoreCase("approved")) {%>Accepted<%} else {%>Declined<%}%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName4()%></td>
                                <td class="t-align-center">
                                    <a href="ViewNoaForRO.jsp?tenderid=<%=contractlists.getFieldName7()%>&pcklotid=<%=contractlists.getFieldName8()%>&NOAID=<%=contractlists.getFieldName5()%>&app=no">View</a>
                                    <%if (!temp.equals("pending") || temp.equals("decline")) {%>

                                    <%} else {%>
                                    &nbsp;|&nbsp;
                                    <a href="ViewNoaForRO.jsp?tenderid=<%=contractlists.getFieldName7()%>&pcklotid=<%=contractlists.getFieldName8()%>&NOAID=<%=contractlists.getFieldName5()%>&app=yes">Accept / Decline</a>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>
                                    </table>
                                     <%if (!temp.equals("pending") || temp.equals("decline")) {%>
                                        <ul class="tabPanel_1 noprint t_space ">
                                    <li class="sMenu button_padding">Contract Sign</li>
                                </ul>
                                     <table width="100%" cellspacing="0" class="tableList_1 ">
                                    <tr>
                                <th width="30%" class="t-align-left">Contract Signing Location</th>
                                <th width="30%" class="t-align-left">Contract Signing Date and Time</th>
                                <th width="40%" class="t-align-left">Action</th>
                            </tr> <%
                                    for(Object[] obj : issueNOASrBean.getCSListingBidderForRO(Integer.parseInt(tenderId), Integer.parseInt(userId),(Integer)objs[0])){
                                        StringBuffer wCCHtml = new StringBuffer("");
                                        int contractSignId = (Integer)obj[0];
                                        CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                        cmsWcCertificateServiceBean.setLogUserId(userId);
                                        TblCmsWcCertificate tblCmsWcCertificate = cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
                                        if (tblCmsWcCertificate != null) {
                                            wCCHtml.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                            /*wCCHtml.append("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId() + "&tenderId=" + tenderId + "'>ViewWCCertificate</a>");*/
                                        }
                                        out.print("<tr><td class=\"t-align-center\">"+ obj[3] +"</td>");
                                        out.print("<td class=\"t-align-center\">"+ DateUtils.gridDateToStrWithoutSec((Date)obj[2]) +"</td>");
                                        out.print("<td class=\"t-align-center\">"
                                                + "<a onclick=\"javascript:window.open('ViewContractSigning.jsp?noaIssueId="+obj[8]+"&contractSignId="+obj[0] + "&tenderId="+ tenderId +"', '', 'resizable=yes,scrollbars=1','');\" href=\"javascript:void(0);\">View</a>"
                                                + wCCHtml.toString()
                                                + "</td></tr>");
                                    }
                                %>

                                    <%}%>
                                     </table>
                                     
                                    </td>
                                </tr>


                                    <%
                                        icount++;
                                            }}
                                                                              }
                                
                                            }%>


                            </table>
                            
                            

                        </form>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function editRO(romId,wpId,tenderId,lotId){

            dynamicFromSubmit("EditRepeatOrder.jsp?romId="+romId+"&wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&pe=yes");

        }
        function view(wpId,tenderId,lotId){
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&RO=ro");
        }
        function ROview(wpId,tenderId,lotId,romId){
            dynamicFromSubmit("ROView.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&romId="+romId);
        }
        function repeatorder(wpId,tenderId,lotId){
            dynamicFromSubmit("PlaceRepeatOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
        function finish(tenderId,lotId){
            document.frmcons.method="post";
            document.frmcons.action="CosolidateServlet?lotId="+lotId+"&action=finaliseRO&tenderId="+tenderId;
            document.frmcons.submit();
        }
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
