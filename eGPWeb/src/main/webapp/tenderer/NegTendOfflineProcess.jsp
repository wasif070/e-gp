<%--
    Document   : NegTendOfflineProcess
    Created on : Dec 22, 2010, 3:38:38 PM
    Author     : Rikin
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Negotiation </title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
          <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
          <script type="text/javascript">

     $(document).ready(function() {
        $("#idfrmtenderOfflineProcess").validate({
            rules: {
                remarksName: { required: true,maxlength: 1000}

            },
            messages: {
                remarksName: { required: "<div class='reqF_1'>Please enter Comments</div>",
                maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}
            }
        }
    );
    });


 </script>
</head>
<body>
<div class="mainDiv">
<div class="fixDiv">

    <%@include  file="../resources/common/AfterLoginTop.jsp" %>
    <div class="dashboard_div">
    <div class="contentArea">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <%
        int tenderId =0;
        if(request.getParameter("tenderId") != null){
            tenderId= Integer.parseInt(request.getParameter("tenderId"));
        }
        pageContext.setAttribute("tenderId", tenderId);
   %>
  <div class="pageHead_1">
       Negotiation
      <span style="float:right;">
            <a href="<%=request.getContextPath()%>/tenderer/NegoTendererProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
       </span>
  </div>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  
  <%pageContext.setAttribute("tab", "6");%>
  <%@include file="../tenderer/TendererTabPanel.jsp" %>
 
<div class="tabPanelArea_1">

  <jsp:include page="EvalInnerTendererTab.jsp" >
    <jsp:param name="EvalInnerTab" value="4" />
     <jsp:param name="tenderId" value="<%=tenderId%>" />
  </jsp:include> 

             <%
                int negId = 0;
                String companyName = "";
                String negMode = "";
                String startDate = "";
                String endDate = "";
                String details = "";
                if(request.getParameter("negId") != null)
                    negId= Integer.parseInt(request.getParameter("negId"));
                
                List<SPTenderCommonData> listBidderNegDetails = negotiationdtbean.getBidderNegDetailsById(tenderId, negId);

                if(!listBidderNegDetails.isEmpty()){
                    startDate = listBidderNegDetails.get(0).getFieldName2();
                    endDate = listBidderNegDetails.get(0).getFieldName3();
                    companyName = listBidderNegDetails.get(0).getFieldName4();
                    details = listBidderNegDetails.get(0).getFieldName5();
                    negMode = listBidderNegDetails.get(0).getFieldName7();
                }
             %>
             <div class="tabPanelArea_1">
             
            <form name="frmtenderOfflineProcess" id="idfrmtenderOfflineProcess" action="<%=request.getContextPath()%>/NegotiationSrBean" method="post">
            <input type="hidden" name="action" id="idaction" value="agree" />
            <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
            <input type="hidden" name="negIdName" id="idnegIdName" value="<%=negId%>" />
            <input type="hidden" name="tenderRefNo" id="tenderRefNo" value="<%=toextTenderRefNo%>" />
            <table width="100%" cellspacing="0" class="tableList_1">                
                <tr>
                    <td  width="20%" class="t-align-left ff">Mode of Negotiation : </td>
                    <td width="80%" class="t-align-left"><%=negMode%></td>
                </tr>
                <tr>
                     <td  width="20%" class="t-align-left ff"> Negotiation Start Date and Time  : </td>
                     <td width="80%" class="t-align-left"><%=startDate%></td>
                </tr>
                <tr>
                     <td  width="20%" class="t-align-left ff"> Negotiation End Date and Time : </td>
                     <td width="80%" class="t-align-left"><%=endDate%></td>
                </tr>
                <tr>
                    <td width="20%" class="t-align-left ff"> Invitation details : </td>
                    <td width="80%" class="t-align-left"><%=details%></td>
                </tr>
                <tr>
                    <td width="20%" class="t-align-left ff"> Action  : <span class="mandatory">*</span></td>
                    <td width="80%" class="t-align-left">
                        <label><input type="radio" name="statusName" id="idaction" value="Accept" checked="checked">&nbsp;Accept&nbsp;&nbsp;</label>
                        <label><input type="radio" name="statusName" id="idaction" value="Reject">&nbsp;Reject</label>
                    </td>
                </tr>
                <tr>
                    <td width="20%" class="t-align-left ff"> Comments : <span class="mandatory">*</span></td>
                    <td width="80%" class="t-align-left">
                        <textarea name="remarksName" class="formTxtBox_1" id="idremarks" style="width: 40%;" rows="5"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                 <input name="btnSubmit" type="submit" value="Submit" />
                            </label>&nbsp;
                        </div>
                    </td>
                </tr>
            </table>
            </form>
             </div>
        </div>

   <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
   <div align="center">
         <%@include file="../resources/common/Bottom.jsp" %>
   </div>
   </div>
   <!--Dashboard Footer End-->
</div>
   
</div></div> 
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>

