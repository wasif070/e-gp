<%-- 
    Document   : BidWithdrawal
    Created on : Dec 18, 2010, 2:35:24 PM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page  import="com.cptu.egp.eps.service.serviceinterface.TenderBidService"%>
<%@page  import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page  import="com.cptu.egp.eps.model.table.TblBidModification"%>
<%@page  import="java.util.List"%>
<%@page  import="com.cptu.egp.eps.web.utility.DateUtils"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bid Modification History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                    String tenderid=request.getParameter("tenderid");
                    TenderBidService tenderBidService = (TenderBidService)AppContext.getSpringBean("TenderBidService");
                    pageContext.setAttribute("tenderId", tenderid);
               %>
            <div class="pageHead_1">Bid Modification History<span style="float: right;"><a class="action-button-goback" href="LotTendPrep.jsp?tenderId=<%=tenderid%>">Go back to Dashboard</a></span></div>
            <div class="mainDiv">
                <div class="contentArea_1">               
                <%@include file="../resources/common/TenderInfoBar.jsp" %>                
            </div>                        
            <div class="tabPanelArea_1">                
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <th width="10%">Sr No.</th>
                    <th width="70%">Reason</th>
                    <th width="20%">Date & Time</th>
                    <%
                        List<TblBidModification> tbms = tenderBidService.findBidModifyHistory(session.getAttribute("userId").toString(), tenderid);
                        int i=1;
                        for(TblBidModification tbm : tbms){

                    %>
                    <tr>
                        <td><%=i%></td>
                        <td><%=tbm.getComments()%></td>
                        <td><%=DateUtils.convertStrToDate(tbm.getBidModDt())%></td>
                    </tr>
                    <%
                                i++;}
                    %>                   
                </table>              
            </div>
            <div>&nbsp;</div>            
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
