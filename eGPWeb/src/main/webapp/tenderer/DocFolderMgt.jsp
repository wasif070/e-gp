<%-- 
    Document   : DocFolderMgt
    Created on : Jan 6, 2011, 6:54:17 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Folder Management</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />


        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
         <script src="../resources/js/jQuery/jquery.validate.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">

             $(document).ready(function(){
                $("#frmDocFolderMgt").validate({
                    rules:{
                        txtFolderName:{required:true,maxlength:25}
                    },

                    messages:{
                        txtFolderName:{required:"<div class='reqF_1'>Please enter Folder Name.</div>",
                            maxlength:"<div class='reqF_1'> Maximum 25 characters are allowed.</div>"}
                    }
                });
            });
        </script>
    <style>
   .ui-jqgrid .ui-jqgrid-htable th div {
        height:auto;
        overflow:hidden;
        padding-right:0px;
        padding-top:0px;
        position:relative;
        vertical-align:text-top;
        white-space:normal !important;
    }
    </style>
    </head>
    
    <body>
<form id="form2" name="form2" method="post">
</form>
<%
            String logUserId = "0";
            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
            }
            briefcaseDoc.setLogUserId(logUserId);
            boolean flag=false;
            String action="";
            String msg="";

            if(request.getParameter("flag") != null){
                 if("true".equalsIgnoreCase(request.getParameter("flag"))){
                     flag=true;
                 }
            }
            if(request.getParameter("action") != null){
                action = request.getParameter("action");
            }
            if(flag){
                 msg = "Folder created successfully";
            }
%>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    int userId = 0;
                    int tendererId = 0;
                    String type = "";
                    String title = "Folder Information";
                    if (session.getAttribute("userId") != null) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }

                    tendererId = briefcaseDoc.getTendererIdFromUserId(userId);
                %>

                <div class="dashboard_div">
                    <!--Dashboard Header Start-->
                    <div class="topHeader">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">
                                <%--<jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                                    <jsp:param name="userType" value="<%=userType.toString()%>"/>
                                </jsp:include>--%>
                                <td class="contentArea_1">
                                    
                                    <!--Page Content Start-->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr valign="top">
                                            <td class="contentArea_1"><div class="pageHead_1">Folder Management
                                                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('2');">Save as PDF</a></span>
                                                </div>
                                                
                                                <!--Dashboard Header End-->
                                                <!--Dashboard Content Part Start-->
                                                <div >
                                                    <%if (flag) {%>
                                                    <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                                                    <%} else {%>
                                                    <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                                                    <%}%>
                                                </div>
                                                <div>&nbsp;</div>
                                                <form  method="post" id="frmDocFolderMgt" action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=createFolder">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formStyle_1">
                                                        <tr >
                                                            <td width="10%" class="t-align-left ff">Folder Name : <span>*</span></td>
                                                            <td width="35%" class="t-align-left"><input name="txtFolderName" type="text" class="formTxtBox_1" id="txtFolderName" onblur="return checkFolderName();" style="width:100%;" />
                                                                <span id="folderMsg"  style="color: red; font-weight: bold">&nbsp;</span>
                                                            </td>
                                                            <td width="1%"></td>
                                                            <td width="15%" align="">
                                                                <label class="formBtn_1">
                                                                    <input type="submit" name="button" id="button" value="Create Folder" onclick="return checkFolderName();" /></label></td>
                                                            <td width="5%">
                                                                <input type="hidden" name="hidTendererId" id="hidTendererId" value="<%=tendererId%>" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="formStyle_1 t_space">
                                                        <tr>
                                                            <td class="t-align-left ff tableHead_1" colspan="3"  >Folder Information :</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="" >
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="t_space" >
                                                                    <tr id="tr1" style="display:none">
                                                                        <%--<td id="td1" class="t-align-left ff"></td>--%>
                                                                        <td id="td2" class="t-align-right ff">
                                                                        </td>
                                                                    </tr>
                                                                    </table>
                                                                    <table width="100%" cellspacing="0" class="t_space">
                                                                        <tr>
                                                                            <td>
                                                                                <div id="jqGrid" class="" >
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <th width="15%" class="t-align-center">Sr.  No.</th>
                                                            <th class="t-align-center" width="64%">Folder Name</th>
                                                            <th class="t-align-center" width="21%">Action</th>
                                                        </tr>--%>
                                                        <%--<tr>
                                                            <td class="t-align-center">1</td>
                                                            <td class="t-align-center">Text</td>
                                                            <td class="t-align-center"><a href="#">View</a></td>
                                                        </tr>--%>
                                                    </table>
                                                </form>
                                                    <div>&nbsp;</div>
                                                    <!--Dashboard Content Part End-->
                                             
                                            </td>

                                        </tr>
                                    </table>
                                    <!--Page Content End-->
                                </td>
                            </tr>
                        </table>
                        <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="DocFolder_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="DocFolder" />
            </form>
            <!--For Generate PDF  Ends-->
                        <!--Dashboard Footer Start-->
                        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDocLib");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script>
    var hiddenforlderid ="";
    var hiddenfoldername ="";
    function downloadFile(fileName,fileLen){
      document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName="+fileName+"&fileLen="+fileLen+"&fromDocFolder=true";
      document.getElementById("form2").submit();
    }

    function forFolderWiseFile(){
        $('#tr1').hide();
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=getfoldersWithPaging&type=folderwise",
            datatype: "xml",
            height: 250,
            colNames:['Sl. <br/> No.','Folder Name','Action'],
            colModel:[
                {name:'srno',index:'srno', width:10,sortable:false,align:'center',search : false},
                {name:'folderName',index:'folderName', width:90,search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'action',index:'action', width:100,sortable:false,align:'center',search : false},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            caption: "<%=title%> ",
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});

    }

    function viewFile(folderId,folderName){
        // var goBack="<a href='#' onclock='forFolderWiseFile();'  ></a>";
        //$('#jqGridHeader').html("<table id='table1'><tr><td>"+goBack+"</td></tr></table>");
        hiddenforlderid = folderId;
        hiddenfoldername = folderName;
        $('#tr1').show();
       <%-- var td1 = document.getElementById("td1");
        td1.innerHTML = "Folder Name : &nbsp;"+folderName;--%>
        var td2 = document.getElementById("td2");
        td2.innerHTML = "<a href='#' onclick='forFolderWiseFile();' class='action-button-goback' >Go back</a>";
        var title = "Folder Name : &nbsp;"+folderName;
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=fetchunmapped&action=getdocumentsWithPaging&type=unmap&folderId="+folderId,
            datatype: "xml",
            height: 250,
            colNames:['Sl. No.','File Name','File Description','e-Signature / Hash','File Size (in KB)','Date and time','Action'],
            colModel:[
                {name:'srno',index:'srno', width:20,sortable:false,align:'center'},
                {name:'fileName',index:'fileName', width:90,sortable:false,align:'center'},
                {name:'fileDescription',index:'fileDescription', width:90,sortable:false,align:'center'},
                {name:'docHash',index:'docHash', width:150,search: false,sortable: false,align:'center'},
                {name:'fileSize',index:'fileSize', width:30,sortable:false,align:'center'},
//                {name:'maptofolder',index:'maptofolder', width:50,sortable:false,align:'center'},
                {name:'uploaddate',index:'uploaddate', width:60,sortable:false,align:'center'},
                {name:'action',index:'action', width:80,sortable:false,align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            loadonce : true,
            caption: title,
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});
    }


    jQuery().ready(function (){
        $('#tr1').hide();
        forFolderWiseFile();
    });


    function checkFolderName(){
        var flag=false;
        var iChars = "!@#$%^&*()+=-[]\\\';,/{}|\":<>?";
        for (var j = 0; j < document.getElementById("txtFolderName").value.length; j++) {
            if (iChars.indexOf(document.getElementById("txtFolderName").value.charAt(j)) != -1) {
                 $('span.#folderMsg').css("color","red");
                document.getElementById("folderMsg").innerHTML = "Special Characters are not allowed";
                return false;
            }
        }
        if($.trim(document.getElementById("txtFolderName").value) != "" && $.trim($('#txtFolderName').val().length)<26)
        {
            $.ajax({
                url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?txtFolderName="+$.trim($('#txtFolderName').val())+"&work=verifyFolderName&tenderid="+<%=tendererId%>,
                method: 'POST',
                async: false,
                success: function(j) {
                    if(j == "OK"){
                        $('span.#folderMsg').css("color","green");
                    }else{
                         $('span.#folderMsg').css("color","red");
                    }
                    $('span.#folderMsg').html(j);
                    if(j == "OK"){
                        flag = true;
                    }
                }
            });
           // alert("flag:" + flag);
        }
        else{
            document.getElementById("folderMsg").innerHTML="";
            flag=true;
        }
        return flag;
    }

    function deleteFile(docId,fileName,redirectaction){

        <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'delete',docId:docId,fileName:fileName}, function(j){
              // alert(j);
        });--%>
        $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if (r == true){
                $.ajax({
                    url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=delete&fileName="+fileName,
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        jAlert("Document deleted successfully.","Document Deleted", function(RetVal) {
                        });
                    }
                });

                jQuery().ready(function (){
                viewFile(hiddenforlderid, hiddenfoldername);
                });

                if(redirectaction == "getfoldersWithPaging"){
                    forFolderWiseFile();
                }
                else if(redirectaction == "GetArchiveDocumentsWithPaging"){
                    forArchive();
                }
                else if(redirectaction == "GetAllDocumentsWithPaging"){
                    forAllFile();
                }else{
                    //forUnMappedFile();
                }
            }
        });
    }

    function archiveFile(docId,redirectaction){
        <%--$.post("<%=request.getContextPath()%>/DocumentBriefcaseSrBean",{work:'archive',docId:docId}, function(j){
            jAlert("Document Archived."," Archive Document ", function(RetVal) {
            });
             //  alert(j);
        });--%>

     $.alerts._show("Delete Document", 'Do you really want to archive this file', null, 'confirm', function(r) {
         if(r == true){
             $.ajax({
                    url: "<%=request.getContextPath()%>/DocumentBriefcaseSrBean?docId="+docId+"&work=archive&",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        jAlert("Document Archived."," Archive Document ", function(RetVal) {
                        });
                    }
                });
                if(redirectaction == "getfoldersWithPaging"){
                    forFolderWiseFile();
                }
                else if(redirectaction == "GetArchiveDocumentsWithPaging"){
                    forArchive();
                }
                else if(redirectaction == "GetAllDocumentsWithPaging"){
                    forAllFile();
                }else{
                    //forUnMappedFile();
                }
            }
        });
    }

</script>
</html>