<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Schedule(Lump Sum - Progress Report)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Payment Schedule(Lump Sum - Progress Report)</div>
            <%
                pageContext.setAttribute("tab", "14");
                ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                String tenderId = "";
                if(request.getParameter("tenderId")!=null){
                    tenderId = request.getParameter("tenderId");
                }

                String lotId = "";
                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                for (SPCommonSearchDataMore lotList : packageLotList) {
                    lotId = lotList.getFieldName5();
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                System.out.println("lotId" +lotId);
                int srvFormMapId = 0;
                CMSService cmsSrv = (CMSService) AppContext.getSpringBean("CMSService");
                List<Object> lstGetSrvFormMapId = cmsSrv.getSrvFormMapId(Integer.parseInt(tenderId), 12);
                if(lstGetSrvFormMapId!=null && !lstGetSrvFormMapId.isEmpty()){
                    srvFormMapId = (Integer) lstGetSrvFormMapId.get(0);
                }
                
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                pageContext.setAttribute("lotId", lotId);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("TSCtab", "3");
            %>
            <%@include  file="../tenderer/TendererTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "2");

                %>
                <%@include  file="../tenderer/cmsTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {

                          if ("reqested".equals(request.getParameter("msg"))) {%>
                                        <div  class='responseMsg successMsg t-align-left'>
                                           Work Completion Certificate request posted successfully
                                        </div>
                            <%
                                        }
                         if ("Updated".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Selected MileStone Completed SuccessFully</div>
                    <%}else if("fail".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class='responseMsg errorMsg'>Selected MileStone Completion Failed</div>
                    <%}}%>
                    <div align="center">                        
                        <form id="frmReExpenses" name="frmReExpenses" method="post" action='<%=request.getContextPath()%>/CMSSerCaseServlet?srvFormMapId=<%=srvFormMapId%>&action=SrvPaymentSchedule&tenderId=<%=tenderId%>&lotId=<%=lotId%>'>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>                                    
                                <th>Milestone Name </th>                                
                                <th>Description</th>                                
                                <th>Payment as % of Contract Value</th>                                
                                <th>Milestone Date proposed by PE </th>
                                <th>Milestone Date proposed by Consultant</th>
                                <th>Milestone Status</th>
                                <%                                    
                                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(srvFormMapId);
                                    if(!list.isEmpty()){
                                    for(int i=0; i<list.size(); i++)
                                    {    
                                %>
                                <tr>
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getSrNo()%></td>                                    
                                    <td class="t-align-left"><%=list.get(i).getMilestone()%></td>                                    
                                    <td class="t-align-left"><%=list.get(i).getDescription()%></td>                                    
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getPercentOfCtrVal()%></td>                                    
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td> 
                                    <td class="t-align-center">
                                    <%if("Completed".equalsIgnoreCase(list.get(i).getStatus()))
                                    {
                                        out.print("Completed");
                                        out.print("&nbsp|&nbsp<a href ='SrvLumpSumUploadDoc.jsp?tenderId="+tenderId+"&wpId=0&keyId="+list.get(i).getSrvPsid()+"&docx=LumpSumPR&module=PR'>Upload / Download Document</a>");
                                    }else{out.print("Not Completed yet");}
                                    %>
                                    </td>
                                    <input type="hidden" value="<%=list.get(i).getSrvPsid()%>" name="SrvPsid"/>                                                                         
                                </tr>                                
                                <%}}%>                                
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr><td>
                                <span style="float: left; text-align: left;font-weight:bold;">
                                    <a href="../resources/common/ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>" class="">Financial Progress Report</a>
                                </span>
                                </td>
                                </tr>    
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left">Attendance sheet</td>
                                    <td class="t-align-left"><a href="ProgressReportForSer.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>">Create</a></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"></td>
                                    <td  class="t-align-left">
                                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                            <tr>
                                                <th width="12%" class="t-align-left">Sr.No.</th>
                                                <th width="20%" class="t-align-left">Attendance Sheet</th>
                                                <th class="t-align-left">Month-Year</th>
                                                <th class="t-align-left">Created Date</th>
                                                <th class="t-align-left">Action</th>
                                                <%
                                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                                    MonthName monthName = new MonthName();
                                                    String wpId = service.getWpIdForService(Integer.parseInt(lotId));
                                                    if (os != null && !os.isEmpty()) {
                                                        int cc = os.size();
                                                        int ccc = 1;
                                                        for (int ik=(os.size()-1);ik>=0;ik--){
                                                %>
                                            <tr>
                                                <td width="12%" ><%=ccc%></td>
                                                <td width="15%" >Attendance Sheet - <%=cc%></td>
                                                <td class="t-align-left"><%=monthName.getMonth(os.get(ik).getMonthId()) + " - " + os.get(ik).getYear()%></td>
                                                <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(os.get(ik).getCreatedDt())%></td>

                                                <td class="t-align-left">
                                                    <a href="ViewSheetdtls.jsp?sheetId=<%=os.get(ik).getAttSheetId()%>&tenderId=<%=tenderId%>">View</a>
                                                    <% if ("reject".equalsIgnoreCase(cmss.checkAttStatus(os.get(ik).getAttSheetId() + ""))) {%>&nbsp;|&nbsp;
                                                    <a href="EditSheetdtls.jsp?sheetId=<%=os.get(ik).getAttSheetId()%>&tenderId=<%=tenderId%>">Edit</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="AttanshtUploadDoc.jsp?tenderId=<%=tenderId%>&wpId=0&keyId=<%=os.get(ik).getAttSheetId()%>&docx=AttSheet&module=PR">Upload / Download Document</a>
                                                </td>
                                            </tr>

                                            <%cc--;
                                            ccc++;
    }
} else {%>
                                            <tr>
                                                <td colspan="5">No records found</td>
                                            </tr>

                                            <%}%>
                                        </table>

                                    </td>
                                </tr>
                            </table>    
                            <%
                                Object[] object = issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotId)).get(0);
                                if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                    int contractSignId = 0;
                                    contractSignId = issueNOASrBean.getContractSignId();
                                    CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                    cmsWcCertificateServiceBean.setLogUserId(userId);
                                    TblCmsWcCertificate tblCmsWcCertificate =
                                            cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);

                                    if (tblCmsWcCertificate != null) {
                                        out.print("<table cellspacing='0' class='tableList_1 t_space' width='100%'>");
                                        out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                        out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotId),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                            out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                + "&lotId=" + lotId + "&action=Request'>Request for Work Completion Certificate </a>");
                                        }
                                       // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                        out.print("&nbsp|&nbsp<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                + "&tenderId=" + tenderId + "&lotId=" + lotId + "'>View Work Completion Certificate </a>");
                                        //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotId),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                            out.print("&nbsp|&nbsp<a href ='WCCDownload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                    + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                    +"&lotId=" + lotId + "'>Download Files</a>");
                                        }
                                        out.print(" </td>");
                                        out.print(" </tr>");
                                        out.print(" <tr>");
                                        out.print("</table>");
                                    }else{
                                        out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                        out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                        out.print("<td  width='80%'  class='t-align-left'>");
                                        if(!cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotId),0)){
                                            out.print("<a href ='"+request.getContextPath()+"/CmsVendorReqWccServlet?tenderId=" + tenderId + "&lotId=" + lotId + "&action=Request&cntId=0'>Request for Work Completion Certificate</a>");
                                        }else{
                                            out.print("Already requested for Work Completion Certificate");
                                        }
                                        out.print(" </td>");
                                        out.print(" </tr>");
                                        out.print(" <tr>");
                                        out.print("</table>");
                                    }
                                }
                            %>        
                        </form>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
<script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
</script>
</html>

