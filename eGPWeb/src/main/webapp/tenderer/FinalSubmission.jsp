<%--
    Document   : FinalSubmission
    Created on : Nov 23, 2010, 4:33:27 PM
    Author     : Kinjal Shah
--%>

<%@page import="com.cptu.egp.eps.dao.daoimpl.HibernateQueryImpl"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.model.table.TblBidderLots"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page  import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService "%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.TenderBidService"%>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.FinalSubmissionService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="java.io.File"%>
<%@page import="com.cptu.egp.eps.web.utility.FilePathUtility"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document read confirmation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
          <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
		<link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script> 
    </head>
    <%
                Logger LOGGER = Logger.getLogger("FinalSubmission.jsp");
                CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                FinalSubmissionService finalSubmissionService = (FinalSubmissionService) AppContext.getSpringBean("FinalSubmissionService");
                String megaHashMessage = "";
                String userId = "";
                String DESTINATION_DIR_PATH_1 = FilePathUtility.getFilePath().get("FileUploadServlet");
                File destinationDir_1;
                boolean FileExists = false,RestrictFinalSubmission = false;
                String realPath = DESTINATION_DIR_PATH_1 + request.getSession().getAttribute("userId");
                destinationDir_1 = new File(realPath);
                if (!destinationDir_1.isDirectory()){
                    FileExists = false;
                    RestrictFinalSubmission=true;
                }
                 boolean isSubDt = false;
                 boolean OperationSuccess = true;
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                }
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                 List<SPCommonSearchData> submitDateSts= commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
             if(submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true"))
                 {isSubDt = true;}
             else{isSubDt=false;}
            if (request.getParameter("btnFinalSub") != null) {
                
                    if(request.getParameter("txtOTP") != null){
                            List<SPCommonSearchData> list = null;
                            list = commonSearchService.searchData("findOTP", null, tenderId, userId, null, null, null, null, null, null);
                            if(Integer.parseInt(list.get(0).getFieldName2()) > 5) {
                                response.sendRedirect("FinalSubmission.jsp?tenderId="+tenderId+"&timeLimit=exceed");
                                return;
                            }
                            else if(! list.get(0).getFieldName1().equals(request.getParameter("txtOTP"))){
                                response.sendRedirect("FinalSubmission.jsp?tenderId="+tenderId+"&wrongAttempt=true");
                                return;
                            }  
                            else {
                                    list = commonSearchService.searchData("deleteOTP", null, tenderId, userId, null, null, null, null, null, null);                               
                            }
                        }
                         OperationSuccess = false;
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        //out.println("kinj");
                        //out.println(tenderId);
                        //out.println(userId);

                         String ipAddress = request.getHeader("X-FORWARDED-FOR");

                         if (ipAddress == null || "null".equals(ipAddress) || ipAddress == "-1" || "-1".equals(ipAddress)) {
                             ipAddress = request.getRemoteAddr(); }
                        List<SPCommonSearchData> lotList = commonSearchService.searchData("doFinalSubmission", tenderId, userId, ipAddress, null, null, null, null, null, null);
                        try{
                            List<CommonSPReturn> cmr = finalSubmissionService.generateMegaHash(Integer.parseInt(tenderId), Integer.parseInt(userId));
                            if(cmr.get(0).getFlag()){
                                LOGGER.debug("Success : "+cmr.get(0).getMsg());
                                OperationSuccess = true;
                            }
                            else{
                                megaHashMessage = "Mega Hash Generation Failed";
                            }                                
                        }catch(Exception e){
                            LOGGER.error("Error in generate mega hash : "+e.toString());
                        }

                        
                        
                        if (!lotList.isEmpty() && OperationSuccess) {


                            if (lotList.get(0).getFieldName1().equalsIgnoreCase("1")) {
                                
                                 // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                String idType="tenderId";
                                int auditId=Integer.parseInt(request.getParameter("tenderId"));
                                String auditAction="Tenderer has done final submission";
                                String moduleName=EgpModule.Bid_Submission.getName();
                                String remarks="Tenderer(User Id): "+session.getAttribute("userId") +" has  done final submission of Tender id: "+auditId;
                                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                

                                List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                    if (!btnFinallst.isEmpty())
                                    {
                                         
                                        if (btnFinallst.get(0).getFieldName1() != null && btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission"))
                                        {

                               //For mail, sms and msgBox(TaherT)
                              try{
                                TenderBidService tenderBidService = (TenderBidService)AppContext.getSpringBean("TenderBidService");
                               SPTenderCommonData sptcd = tenderCommonService1.returndata("tenderinfobar",tenderId,null).get(0);
                                UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                String data[]= tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());
                                String mails[]={data[0]};
                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                String msgBox = msgBoxContentUtility.bidTenderSubMsg(tenderId, userId,"final");
                                userRegisterService.contentAdmMsgBox(data[0], XMLReader.getMessage("msgboxfrom"), "e-GP: Final submission of an e-Tender", msgBox);
                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                MailContentUtility mailContentUtility = new MailContentUtility();
                                String mailText = mailContentUtility.bidTenderSubMail(tenderId, userId,"final");
                                sendMessageUtil.setEmailTo(mails);
                                sendMessageUtil.setEmailSub("e-GP: Final submission of an e-Tender");
                                sendMessageUtil.setEmailMessage(mailText);
                                sendMessageUtil.sendEmail();
                                sendMessageUtil.setSmsNo(data[1]+data[2]);
                                //sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully completed final submission for the below mentioned Tender%0ATender Id:"+tenderid+"%0ARef. No:"+sptcd.getFieldName2()+"%0Ae-GP System");
                                sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully completed final submission of%0ATender Id:"+tenderId+"%0ARef. No:"+sptcd.getFieldName2()+"%0Ae-GP System");
                                sendMessageUtil.sendSMS();
                                data=null;
                                mails=null;
                                sendMessageUtil=null;
                                msgBoxContentUtility=null;
                                //msg sending ended
                              }
                              catch(Exception e){
                                  auditAction="Error in Bidder has done final submission "+e.getMessage();
                                LOGGER.error("Error in FinalSubmission.jsp",e);
                              }
                              finally
                              {
                                  makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                              }

                                response.sendRedirect("SubmissionReceipt.jsp?tenderId=" + tenderId);
                              }else{
                                 auditAction="Error in Bidder has done final submission ";
                                 makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                              }
                           }else{
                                 auditAction="Error in Bidder has done final submission ";
                                 makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                              }
                            } else if ("0".equalsIgnoreCase(lotList.get(0).getFieldName1())) {
                                out.print("<div width='100%' align='center'><font style='color:red;'>" + lotList.get(0).getFieldName2() + "</font></div>");

                            }
                        }
            }
    %>
    <body>
		<%

            TenderCommonService tenderCommonServiceCommonBid = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<TblBidderLots> addList = new ArrayList<TblBidderLots>();
            int counter = 1;
            String strUserLot = session.getAttribute("userId").toString();
            String tenderIdUserLot = request.getParameter("tenderId");
            int userIdBid = Integer.parseInt(session.getAttribute("userId").toString());
            int tenderIdlot = Integer.parseInt(request.getParameter("tenderId"));
            String lotID = "0";

            CommonSearchDataMoreService dataMoreHope = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            //List<SPCommonSearchDataMore> tenderList = dataMoreHope.getEvalProcessInclude("getSendToHope", tenderIdUserLot);
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            List<Object[]> list_lorPkg = new ArrayList<Object[]>();
            list_lorPkg = commonService.getLotDetailsByTenderIdforEval(tenderIdUserLot);
                    for(Object[] list_detial : list_lorPkg){
                            lotID = list_detial[2].toString();
                         }

            List<SPCommonSearchDataMore> ChkBidderLot = dataMoreHope.getEvalProcessInclude("checkBidderLot", tenderIdUserLot,strUserLot);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            if ( ChkBidderLot.get(0).getFieldName1().equalsIgnoreCase("0") )
            {
                String dtXml = "";
                dtXml = "<tbl_bidderLots insdate=\"" + format.format(new Date()) + "\" pkgLotId=\""+Integer.parseInt(lotID)+"\" tenderId=\"" + tenderIdlot + "\" userId=\""+userIdBid+"\"  deleteDate=\"\" deleteStatus=\"\" />";
                dtXml = "<root>" + dtXml + "</root>";
                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_bidderLots", dtXml, "").get(0);
            }




        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->

            <div class="contentArea_1">
                <div class="pageHead_1">Document Read Confirmation</div>
                <div class="mainDiv">
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <% boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");%>
                </div>
                <div>&nbsp;</div>
               <% pageContext.setAttribute("tab", "4");%>
                         <%@include file="TendererTabPanel.jsp" %>
                     <%if(!is_debared ){%>


                    
                <div class="tabPanelArea_1">
                    <%
                    if (!OperationSuccess){
                            out.print("<div width='100%' align='center' class='t-align-center ff'><font style='color:red;'>"+megaHashMessage+"<br/></font></div>");
                        }
                                if (isTenPackageWis) {                                
                                    for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                    {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <th width="22%" class="t-align-left ff">Filled (Yes/No)</th>
                            <th width="22%" class="t-align-left ff">Mandatory (Yes/No)</th>
                            <th width="22%" class="t-align-left ff">Encrypted with Buyer Hash</th>

                        </tr>
                        <%
                            //Taher Starts
                            List<SPCommonSearchData> techFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Technical", session.getAttribute("userId").toString(), null, null, null, null, null);
                            for (SPCommonSearchData formname : techFormList) {
                         %>
                         <tr>
                            <td class="t-align-left ff"><%out.print(formname.getFieldName1());if("c".equalsIgnoreCase(formname.getFieldName6())){out.print("<font color='red'>(cancelled)</font>");}%></td>
                            <td class="t-align-center ff"><%if(formname.getFieldName2().equalsIgnoreCase("yes")){out.print("Yes");}else{out.print("No");}%></td>
                            <td class="t-align-center ff"><%if(formname.getFieldName4().equalsIgnoreCase("yes")){out.print("Yes");}else{out.print("No");}%></td>
                            <td class="t-align-center ff"><%=formname.getFieldName7()%></td>

                        </tr>
                         <%
                            }
                        %>
                        <%
                        List<SPCommonSearchData> lotIdDesc  = commonSearchService.searchData("getlotid_lotdesc", tenderId, session.getAttribute("userId").toString(), null, null, null, null, null, null, null);
                        Object procnaturee = cs.getProcNature(tenderId);
                        for(SPCommonSearchData lotdsc : lotIdDesc) {
                            if(!"Services".equalsIgnoreCase(procnaturee.toString())){
                                out.print("<tr><td colspan='4'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Lot No.</td><td width='85%'>"+lotdsc.getFieldName4()+"</td></tr><tr><td class='t-align-left ff'>Lot Description</td><td>"+lotdsc.getFieldName2()+"</td></tr></table></td></tr>");
                            }
                             List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Pricebid", session.getAttribute("userId").toString(), lotdsc.getFieldName1(), null, null, null, null);
                             for (SPCommonSearchData formdetails : priceFormList)
                             {
                        //for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, "0", null, null, null, null, null, null)) {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%out.print(formdetails.getFieldName1());if("c".equalsIgnoreCase(formdetails.getFieldName6())){out.print("<font color='red'>(cancelled)</font>");}%></td>
                            <td class="t-align-center ff"><%if(formdetails.getFieldName2().equalsIgnoreCase("yes")){out.print("Yes");}else{out.print("No");}%></td>
                            <td class="t-align-center ff"><%if(formdetails.getFieldName4().equalsIgnoreCase("yes")){out.print("Yes");}else{out.print("No");}%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName7()%></td>

                        </tr>
                        <% }}//Taher Ends%>
                    </table>
                    <% }
                                               } else {

                                                   for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                                   {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                        </tr>
                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <%  }
                       for (SPCommonSearchData lotList : commonSearchService.searchData("getlotid_lotdesc", tenderId, userId, "", null, null, null, null, null, null))
                        {
                    %>
                    <%                        
                        Object procnature = cs.getProcNature(tenderId);
                        if(!"Services".equalsIgnoreCase(procnature.toString())){
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">

                        <tr>
                            <td width="22%" class="t-align-left ff">Lot No. :</td>
                            <td width="78%" class="t-align-left"><%=lotList.getFieldName4()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Lot Description :</td>
                            <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                        </tr>
                    </table>
                        <%}%>
                        <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <!--                                <th width="23%" class="t-align-left">Mapped Documents List</th>-->
                            <th width="22%" class="t-align-left ff">Filled (Yes/No)</th>
                            <th width="22%" class="t-align-left ff">Mandatory (Yes/No)</th>
                            <th width="22%" class="t-align-left ff">Encrypted with Buyer Hash</th>

                        </tr>
                        <%
                          for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, lotList.getFieldName5(), null, null, null, null, null, null))
                          {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            <td class="t-align-center ff"><%if(formdetails.getFieldName3().equalsIgnoreCase("yes")){out.print("Yes");}else{out.print("No");}%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName6()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName4()%></td>
                        </tr>
                        <% }%>
                    </table>
                    <%     }
                                    }%>
                    <%                         
                    List<SPCommonSearchData> formList = commonSearchService.searchData("getFormNameByTenderId", tenderId, "", "", "", "", "", "", "", "");
                        boolean showflag = false;

                        for (SPCommonSearchData formname : formList) {
                            List<SPCommonSearchData> docList = commonSearchService.searchData("getDocRelatedInfo", tenderId, formname.getFieldName2(), ""+userId, "", "", "", "", "", "");
                    %>
                    <% if(!docList.isEmpty()){ %>

                    <% if(showflag==false){ %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="20%" class="t-align-center ff">Form Name</th>
                        <td width="80%" class="t-align-center ff">
                            <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1">
                                <tr>
                                    <th width="110px" class="t-align-left ff">Mapped Document’s Name</th>
                                    <th width="110px" class="t-align-left ff">File Name </th>
                                    <th width="150px" class="t-align-left ff">Document Status</th>
<!--                                    <th width="150px" class="t-align-left ff">e-Signature / Hash</th>-->
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% 
                            showflag = true;
                        }
                    %>
                    <tr>
                        <td width="20%" class="t-align-left ff"><%=formname.getFieldName1()%></td>
                        <td colspan="3">
                            <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1" style="table-layout: fixed;">
                            <%
                                for (SPCommonSearchData docname : docList) {
                                   if("".equals(docname.getFieldName1())){
                                       FileExists = true;
                                       }
                                       else {
                                           File file = new File(destinationDir_1, docname.getFieldName1());                                           
                                           FileExists = false;
                                     if (file.exists()) { FileExists = true;  }
                                       else {if(!RestrictFinalSubmission){RestrictFinalSubmission = true;}}}
                            %>
                             <tr>  
                                 <td width="110px" class="t-align-center" style="word-wrap: break-word;"><% if("".equals(docname.getFieldName2())){  out.print("-"); }else{ out.print(docname.getFieldName2()); }  %>&nbsp;</td>
                                <td width="110px" class="t-align-center" style="word-wrap: break-word;"><% if("".equals(docname.getFieldName1())){  out.print("-"); }else{ out.print(docname.getFieldName1()); }  %>&nbsp;</td>
                                <%if(!"".equals(docname.getFieldName1())){ if(FileExists){%><td width="150px" class="t-align-center" style="word-wrap: break-word;color: green;"><%%>Ok </td><%} else{%><td width="150px" class="t-align-center" style="word-wrap: break-word;color: red;">Pending<%}%></td>
                                <%} else {%><td width="150px" class="t-align-center" style="word-wrap: break-word;">-</td>><%}%>
                                <%--<td width="150px" class="t-align-center" style="word-wrap: break-word;">
                                    <% if(docname.getFieldName3()==null || "null".equalsIgnoreCase(docname.getFieldName3())){ out.print("-"); }else{ out.print(docname.getFieldName3()); } %>&nbsp;
                                </td>--%>
                             </tr>
                            <% }  %>
                            </table>
                        </td>
                    </tr>
                     <%  } }
                    %>
                </table>
                     
                    <%
                            List<SPCommonSearchData> finalsubchk = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                              if (!finalsubchk.isEmpty()) {
                         if (finalsubchk.get(0).getFieldName1().equalsIgnoreCase("finalsubmission"))
                         {

                        %>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="22%" class="t-align-left ff">Your Mega Hash :</td>
                                <td width="78%" class="t-align-left">
                                    <%
                                                for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                    out.println(megaHash.getFieldName1());
                                                }
                                    %>

                                </td>
                            </tr>
                            <%if (!isSubDt){%>
                            <tr>
                                <td class="t-align-left ff">Mega Mega Hash</td>
                                <td class="t-align-left">
                                <%
                                            for (SPCommonSearchData megaMegaHash : commonSearchService.searchData("getTenderMegaMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                out.println(megaMegaHash.getFieldName1());
                                                                     }
                                %>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td class="t-align-left">
                                    <%
                                    for (SPCommonSearchData msg : commonSearchService.searchData("FinalSubDtIP", tenderId, userId, "0", null, null, null, null, null, null)) {
                                    %>
                                        <div class="mandatory">Final Submission Completed Successfully at - <%=msg.getFieldName1() %>. IP Address : <%=msg.getFieldName2() %></div>
                                    <% } %>

                                </td>
                            </tr>
                            <tr style="display: none;" id="withmodifyDisp">
                                <td class="t-align-left">
                                        <div class="mandatory">If you want to Substitute or Withdraw the submitted tender, click the relevant button below.</div>
                                </td>
                            </tr>
                        </table>
                                    <%}}%>
<form action="" id="frmDocReadConf" method="POST">


    <%//if (paidCnt>0) {%>
                    <%if (!isSubDt){ /// IF SUBMISSION DATE GETS OVER%>
                    <div class="t-align-center t_space">
                        <label class='reqF_1'>Submission Date is Over </label>
                    </div>
					
                     <%} else if (isSubDt && RestrictFinalSubmission){ /// IF DOCUMENT UPLOAD IS NOT SUCCESSFULL %>
                    <div class="t-align-center t_space">
                        <label class='reqF_1'> Mapped Document is missing. Remove the mapped document from 'Mapped Files' list (Tender >> MyTender >> MyTenderDashboard >> TenderPreparation >> Map ) and re-upload and map it.</label>
                    </div>
                     <%} else { /// IF SUBMISSION DATE LIVE %>
                                
			    <div class="t-align-center t_space">
                                <%if(OperationSuccess){%>
                                <script type="text/javascript">
                                    $(function() {
                                     UpdateOTP();
                                     });
                                </script>
                                <% } else { %>
                                    <script type="text/javascript">
                                    $(function() {
                                      $("#txtOTP" ).hide();
                                      $("#btnFinalSubmission" ).hide();   
                                      $("#otp" ).hide(); 
                                      });
                                    </script>
                                <% } %>
                                <div class="ff" width="23%"> <span id="otp" >OTP :<span class="mandatory">*</span></span>&nbsp;    
                                  <input name="txtOTP" type="text" class="formTxtBox_1" id="txtOTP" onblur="checkOTP();"  style="width:150px;" /> &nbsp;
                                  <span style = "color : red;" id="msgtxtOTP">
                                      <%  if(!OperationSuccess){ %>
                                           <a href="#" style="color:green; padding-left: 10px;" onclick="reUpdateOTP(<%=OperationSuccess%>);">Resend OTP</a> 
                                      <%}
                                      else{
                                          if(request.getParameter("timeLimit") != null && request.getParameter("timeLimit").equals("exceed")){
                                            out.print("OTP Time Limit Exceed");
                                            %>
                                            <a href="#" style="color:green; padding-left: 10px;" onclick="reUpdateOTP(true);">Resend OTP</a>
                                          <% }
                                          else if(request.getParameter("wrongAttempt") != null && request.getParameter("wrongAttempt").equals("true")){
                                              out.print("Please Enter Valid OTP");
                                          }
                                        }    
                                      %>
                                  </span>
                              </div> 
                        </div>
                    <div class="t-align-center t_space" id ="btnFinalSubmission">
                        <%
                            List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                              if (!btnFinallst.isEmpty()) {
                         if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("modify") || btnFinallst.get(0).getFieldName1().equalsIgnoreCase("Pending")  || btnFinallst.get(0).getFieldName1()== null)
                        {
                            List<SPCommonSearchData> finalSubCond = commonSearchService.searchData("ChkTodoFinalSubmission", tenderId, userId, null, null, null, null, null, null, null);
                            if(!finalSubCond.isEmpty()){
                               if("1".equals(finalSubCond.get(0).getFieldName1())){
                        %>
                                <label class="formBtn_1"><input name="btnFinalSub" type="Submit" value="Final Submission" onclick="return Validate();"/></label>
                        <%
                            }else{out.print("<div class='responseMsg errorMsg'>"+finalSubCond.get(0).getFieldName2()+"</div>");}}
                          }else if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission")) {
                         %>
                                <script type="text/javascript">
                                        $('#withmodifyDisp').show();
                                </script>
                                <label class="l_space"><a href="BidWithdrawal.jsp?bid=w&tenderid=<%=tenderId%>" class="anchorLink">Bid Withdrawl</a></label>
                                <label class="l_space"><a href="BidWithdrawal.jsp?tenderid=<%=tenderId%>" class="anchorLink">Substitution</a></label>
                        <%}} else {
                                //boolean b_isSubContract = true;
                                //List<SPCommonSearchData> subContractCond = commonSearchService.searchData("isSubContractingAccepted", tenderId, userId, null, null, null, null, null, null, null);
                                  // if(subContractCond!=null && !subContractCond.isEmpty()){
                                    //    b_isSubContract = false;
                                     //}if(b_isSubContract){
                                List<SPCommonSearchData> finalSubCond = commonSearchService.searchData("ChkTodoFinalSubmission", tenderId, userId, null, null, null, null, null, null, null);
                            if(!finalSubCond.isEmpty()){
                               if("1".equals(finalSubCond.get(0).getFieldName1())){
                                   
                                %>
                                <label class="formBtn_1">
                                    <input name="btnFinalSub" type="Submit" value="Final Submission"/></label>
                                <%
                                                          
                                   }else{out.print("<div class='responseMsg errorMsg'>"+finalSubCond.get(0).getFieldName2()+"</div>");}}
                                        //}else{
                                          //        out.print("<div class='responseMsg errorMsg'> You have already accepted a request of Sub Contracting / Consultancy. You can’t do final submission now.</div>");
                                            //      }
                                    }%>

                    </div>
                    <%}//}%>
                     </form>
                </div> <%}%> </div>
        </div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
	<script> 
        
        function UpdateOTP(){ 
            var update = <%=request.getParameter("wrongAttempt")%>;
            var timelimit = '<%=request.getParameter("timeLimit")%>';
            
            if (timelimit != 'null' && timelimit == 'exceed' ){
               jAlert("OTP Time Limit Exceed","Time Limit","");
            }
            else if (update != null && update == true ){
               jAlert("Please Enter Valid OTP","Wrong Attempt","");
            }
            else{
                $.post("<%=request.getContextPath()%>/CommonServlet", {event:'submission',tenderid:<%=tenderId%>, userid:<%=userId%>,funName: 'updateOTP'}, function (j) {
                    if( j == 'Success'){            
                       jAlert("OTP sent to your mobile no / email address for final submission","Input OTP","");
                     }
                });
            }
        }
        
        function reUpdateOTP(OperationSuccess){
        
            if(!OperationSuccess){
                $("#txtOTP" ).show();
                $("#btnFinalSubmission" ).show();   
                $("#otp" ).show(); 
            }
            
            $.post("<%=request.getContextPath()%>/CommonServlet", {event:'submission',tenderid:<%=tenderId%>, userid:<%=userId%>,funName: 'updateOTP'}, function (j) {
                    if( j == 'Success'){            
                        document.getElementById("msgtxtOTP").innerHTML = "" ;  
                        jAlert("OTP sent to your mobile no / email address for final submission","Input OTP","");
                     }
                });
        }
     
        function checkOTP(){
           if (document.getElementById("txtOTP").value == '' ){
                document.getElementById("msgtxtOTP").innerHTML = "Please Enter OTP" ;
            }
            else{
                document.getElementById("msgtxtOTP").innerHTML = "" ;  
            } 
        }
     
        function Validate(){
                     
            var check = false ;
            
            if (document.getElementById("txtOTP").value == '' ){
                document.getElementById("msgtxtOTP").innerHTML = "Please Enter OTP" ;
                check = false;  
            }
            else{
                check = true; 
            }
            
            return check ;
        }
        
    </script>  

</html>
