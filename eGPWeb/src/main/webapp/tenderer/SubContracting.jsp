<%--
    Document   : SubContracting
    Created on : Dec 27, 2010, 2:14:51 PM
    Author     : Administrator

--%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sub Contracting / Consulting</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean" %>
        <jsp:useBean id="subContractingSrBean" class="com.cptu.egp.eps.web.servicebean.SubContractingSrbean" scope="request"/>
        <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
        <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            /*For validating form filed*/
            $(document).ready(function() {
                $("#frmSubContracting").validate({
                    rules: {
                        emailVal:{required: true,email:true}
                        
                    },
                    messages: {
                        emailVal:{ required: "<div class='reqF_1'> Please enter e-mail ID.</div>",
                            email:"<div class='reqF_1'>Please enter valid e-mail ID.</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "emailVal"){
                            error.insertAfter("#btnSearchlbl");
                        }
                        else{
                            error.insertAfter(element);
                        }
                    }

                }
            );
            });
        </script>
<script type="text/javascript">
function isEmailCheck(){
    $(".reqF_1").remove();
    var tbol=true;
   var spaceTest =  /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i
   if(spaceTest.test($('#emailVal').val())){
       $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/ServletSubContracting",
                        data:"emailId="+$('#emailVal').val()+"&"+"tenderId=<%=request.getParameter("tenderid")%>"+"&"+"funName=chkMail&lorpkgId=<%=request.getParameter("lorpkgId")%>",
                        async: false,
                        success: function(j){
                           if(j.toString().length>2){
                            tbol=false;
                            $("#btnSearchlbl").parent().append("<div class='reqF_1'><br />"+j.toString()+"</div>");
                            return false;
                            }
                        }
       });
   }//If valid email
   if(tbol==false){
       return false;
   }
}
</script>
        <script type="text/javascript">
            /*For jquery calander*/
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
         <%
          String tenderId = request.getParameter("tenderid");
          String s_lorPkgId = "0";
          String s_package = "package";
          List<Object[]> list_lorPkg = new ArrayList<Object[]>();
            if(request.getParameter("lorpkgId")!=null && !"0".equalsIgnoreCase(request.getParameter("lorpkgId"))){
                s_lorPkgId = request.getParameter("lorpkgId");
                if(!"0".equalsIgnoreCase(s_lorPkgId)){
                            s_package = "lot";
                            CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                            list_lorPkg = commonService.getLotDetailsByPkgLotId(s_lorPkgId,tenderId);
               }
            }
                                String str_comment_lbl = "";
                                List<TblTenderDetails> listTender = new ArrayList<TblTenderDetails>();
                                TenderSrBean tenderSrBean  = new TenderSrBean();
                                listTender = tenderSrBean.getTenderDetails(Integer.parseInt(tenderId));
                                        if("Works".equalsIgnoreCase(listTender.get(0).getProcurementNature())){
                                            str_comment_lbl = "Elements of Activity and Brief description of Activity";
                                        }else if("goods".equalsIgnoreCase(listTender.get(0).getProcurementNature())){
                                            str_comment_lbl = "Nature of the Supply and related Services";
                                        }else{
                                            str_comment_lbl = "Elements of Activity and Brief description of Activity";
                                        }
        %>
        <script type ="text/javascript">
            /*Comparison of date filed*/
            var falg = false;
            function CompareToForToday(value)
            {
                document.getElementById('remarksErr').innerHTML='';
                document.getElementById('dtErr').innerHTML='';
                if(value!=null && value!=''){
                    flag = true;
                var mdy = value.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split

                if(mdyhr[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }
                var d = new Date();
                if(mdyhr[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                if(Date.parse(valuedate) > Date.parse(todaydate)){
                        flag =  true;
                }else{
                        document.getElementById('dtErr').innerHTML='<br/>Last date for accepting invitation should be greater than current date';
                                flag =  false;
                        }
                }else{
                    document.getElementById('dtErr').innerHTML='<br/>Please select last date for Accepting invitation';
                    flag =  false;
                }
                if(CKEDITOR.instances.txtremarks.getData() == 0){
                    document.getElementById("remarksErr").innerHTML="Please enter <%=str_comment_lbl%>";
                    flag=false;
                }
                else
                    {
                         if(CKEDITOR.instances.txtremarks.getData().length <= 1000)
                        {
                            document.getElementById("remarksErr").innerHTML="";
                        }
                        else
                        {
                           document.getElementById("remarksErr").innerHTML="Maximum 1000 characters are allowed.";
                           flag=false;
                        }
                    }
                /*if($("#txtremarks")!=null && $("#txtremarks").val()!=''){
                    if($("#txtremarks").val().length>1000){
                        document.getElementById('remarksErr').innerHTML="<br />Maximum 1000 character is allowed";
                        flag =  false;
                    }
                }else{
                    document.getElementById('remarksErr').innerHTML="<br />Please enter Comments.";
                    flag =  false;
                }*/
                if(flag==false){
                            return false;
                }else{
                    if(confirm('Do you really want to invite this Bidder / Consultant')){
                    return true;
                    }else{
                        return false;
                    }
                }
                                            
            }
        </script>
        <script type ="text/javascript">
             $(function() {
                $('#frmsubContract').submit(function() {
                   // if($("#remarksErr").val().length==0 &&$("#dtErr").val().length==0){
                   if(flag){
                    $('#btnInvite').css("visibility", "collapse");
                    }
                });
            });
        </script>




    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <%
                            String usertype = "";
                            boolean b_bidWithDraw = true;
                             String s_bidStatus = subContractingSrBean.getBidWidrawStatus(Integer.parseInt(tenderId), Integer.parseInt(session.getAttribute("userId").toString()));
                             if("withdrawal".equalsIgnoreCase(s_bidStatus) || "finalsubmission".equalsIgnoreCase(s_bidStatus)){
                                 b_bidWithDraw = false;
                             }
                            pageContext.setAttribute("tenderId", tenderId);
                            String user = session.getAttribute("userId") + "";
                            String emalVal1 = "";
                            String comName = "";
                            boolean bol_isLive = tenderSrBean.chkTenderStatus(tenderId) ; 
                //if(archiveCnt!=0){
                %>




                <!--Dashboard Content Part Start-->
                <div class="pageHead_1 t_space">Invite Sub Contractor/ Consultant
                <span style="float:right;"><a href="TendererDashboard.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back to Dashboard</a></span>
                </div>
                <%
                            String errMsg = request.getParameter("errMsg");
                            if (errMsg != null && errMsg.equals("Ok")) {%>
                <div class="responseMsg successMsg" style="margin-top: 10px;">Invitation Sent Successfully</div>
                <%} else if (errMsg != null) {%>
                <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=errMsg%></div>
                <%}%>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%pageContext.setAttribute("tab", "3");%>
                <%@include file="TendererTabPanel.jsp"%>
                <%
                            String tenderRefNo = pageContext.getAttribute("tenderRefNo") + "";
                %>
                <%if(!is_debared){%>
                <div class="tabPanelArea_1 ">
                    <%
                                String userId = session.getAttribute("userId").toString();
                                
                               
                                /* TenderCommonService tenderCS1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                long archiveCnt = tenderCS1.tenderArchiveCnt(tenderid);
                                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                flag = btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission");*/
                                java.util.List<SPCommonSearchData> list1 = new java.util.ArrayList<SPCommonSearchData>();
                                java.util.List<SPCommonSearchData> list2 = new java.util.ArrayList<SPCommonSearchData>();
                                CommonSearchService tenderCS = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                TenderCommonService tenderCS2 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                int docCnt = 0;
                                boolean bole_flag = false;
                                for (SPCommonSearchData sptcd : tenderCS.searchData("SubContractInvAppReq",tenderId, user,s_lorPkgId,s_package,null,null,null,null,null)) {
                                    
                                    docCnt++;
                                    if (sptcd.getFieldName7().equals("Approved")&&!user.equalsIgnoreCase(sptcd.getFieldName10())) {
                                        bole_flag = true;
                                        break;
                                    }
                                    if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }
                                /*For searching of email address form tendrer*/
                                if ("Submit".equals(request.getParameter("btnSearch"))) {
                                    if ((request.getParameter("emailVal")) != null) {
                                        String emalVal = request.getParameter("emailVal");
                                        if(request.getParameter("lorpkgId")!=null){
                                        s_lorPkgId = request.getParameter("lorpkgId");
                                       }
                                        emalVal = "emailId='" + emalVal + "'";
                                        CommonSearchService CommonSD = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        list1 = CommonSD.searchData("searchSubContractBidder", tenderId, emalVal, user, s_lorPkgId, null, null, null, null, null);
                                    }
                                }
                                boolean isFinalSubmission = false;
                                long archiveCnttemp = tenderCS2.tenderArchiveCnt(tenderid);
                               // CommonSearchService CommonSD = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                list2 = tenderCS.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                if (list2.size() > 0 && list2 != null) {
                                    isFinalSubmission = list2.get(0).getFieldName1().equalsIgnoreCase("finalsubmission");
                                    list2.clear();
                                    list2 = null;
                                }
                                if (archiveCnttemp != 0 || !isFinalSubmission) {
                    %>
                    <ul class="tabPanel_1">
                        <li><a href="#" class="sMenu">Send Invitation</a></li>
                        <%if(!"withdrawal".equalsIgnoreCase(s_bidStatus)){%>
                        <li><a id="hrfDeclaration" href="ProcessSubContractInv.jsp?tenderid=<%=tenderId%>&lorpkgId=<%=s_lorPkgId%>">Received Invitation.</a></li>
                        <%}%>

                    </ul>
                    <div class="tabPanelArea_1">
                    <%if("lot".equalsIgnoreCase(s_package)){%>
                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="tableList_1 t_space">
                      <tr>
                          <td width="15%" class="ff">Lot No.</td>
                          <td><%=list_lorPkg.get(0)[0]%></td>
                      </tr>
                      <tr>
                          <td class="ff">Lot Description</td>
                          <td><%=list_lorPkg.get(0)[1]%></td>
                      </tr>

                    </table>
                      <%}%>
                        <%if (!bole_flag) {
                                if(b_bidWithDraw){
                                    if(bol_isLive){
                        %>
                        <div >
                            <div class="t-align-left ff formStyle_1">Field marked (<span class="mandatory">*</span>) are mandatory</div>
                            <form id="frmSubContracting" name="frmSubContracting" method="POST"  action="SubContracting.jsp?&tenderid=<%=request.getParameter("tenderid")%>&lorpkgId=<%=s_lorPkgId%>">
                                <div class="formBg_1 t_space">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                        <tr>
                                            <td width="10%" class="ff">e-mail ID : <span>*</span></td>
                                            <td width="40%"><span class="txt_2"><input name="emailVal" type="text" class="formTxtBox_1" id="emailVal" style="width:200px;" /></span>
                                                &nbsp;
                                                <label id="btnSearchlbl" class="formBtn_1">
                                                    <input type="submit" name="btnSearch" id="btnSearch" value="Submit" onclick="return isEmailCheck();"/>
                                                </label>
                                            </td>
                                            <td width="15%"></td>
                                            <td width="35%"></td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                            <div <%if (list1.size() > 0) {%>style="display: block;"<%} else {%>style="display: none;"<%}%>>
                                <%
                                    for (SPCommonSearchData sptcd : list1) {
                                        usertype = sptcd.getFieldName8();
                                        comName = sptcd.getFieldName1();
                                        emalVal1 = sptcd.getFieldName3();
                                        
                                %>


                                <div class="tableHead_1 t_space">Company Details:</div>

                                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1" id="table-row" >
                                    <tr>
                                        <td width="19%" class="ff">Company Registration No. :</td>
                                        <td width="31%"><%if(sptcd.getFieldName2()!=null)
                                            out.print(sptcd.getFieldName2());%></td>
                                        <td width="11%" class="ff"></td>
                                        <td width="39%"></td>
                                    </tr>
                                    <tr>
                                        <td width="19%" class="ff">Company Name :</td>
                                        <td width="31%"><%=sptcd.getFieldName1()%></td>
                                        <td width="11%" class="ff">e-mail ID :</td>
                                        <td width="39%"><%=sptcd.getFieldName3()%></td>

                                    </tr>
                                    <tr>
                                        <td width="19%" class="ff">Country :</td>
                                        <td width="31%"><%=sptcd.getFieldName4()%></td>
                                        <td width="11%" class="ff">Dzongkhag / District  :</td>
                                        <td width="39%"><%=sptcd.getFieldName5()%></td>
                                        <%--<td width="31%">${subContractingDtBean.regOffState}</td>--%>
                                    </tr>
                                    <tr>
                                        <td width="19%" class="ff">City / Town :</td>
                                        <td width="31%"><%if(sptcd.getFieldName9()!=null)
                                            out.print(sptcd.getFieldName9());%></td>
                                        <td width="11%" class="ff"></td>
                                        <td width="39%"></td>
                                    </tr>
                                    <tr>
                                        <td width="19%" class="ff">Company's Legal <br/>Status :</td>
                                        <td width="31%"><%--<%=sptcd.getFieldName7()%>--%>
                                        <%if (sptcd.getFieldName7().equals("public")) {%>Public Limited Company.<%}%>
                                            <%if (sptcd.getFieldName7().equals("private")) {%>Private Limited Company<%}%>
                                            <%if (sptcd.getFieldName7().equals("partnership")) {%>Partnership.<%}%>
                                            <%if (sptcd.getFieldName7().equals("proprietor")) {%>Sole Proprietorship.<%}%>
                                        </td>
                                        <td width="11%" class="ff"></td>
                                        <td width="39%"></td>
                                    </tr>
                                </table>
                                <%
                                        if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }
                                %>
                            </div>

                            <div <%if (list1.size() > 0) {%>style="display: block;"<%} else {%>style="display: none;"<%}%>>
                                <div class="tableHead_1 t_space">Send Invitation:</div>
                                <form  id="frmsubContract" method="post" action="<%=request.getContextPath()%>/ServletSubContracting"   name="frmsubContract">
                                    <input type="hidden" name="emalVal" value="<%=emalVal1%>"/>
                                    <input type="hidden" name="comName" value="<%=comName%>"/>
                                    <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                                    <input type="hidden" name="usertype" value="<%=usertype%>"/>
                                    <input type="hidden" name="tenderRefNo" value="<%=tenderRefNo%>"/>
                                    <input type="hidden" name="lorpkgId" value="<%=s_lorPkgId%>"/>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                        <tr>
                                            <td width="24%" class="ff">Last date for accepting invitation : <span>*</span></td>
                                            <td width="76%"><input class="formTxtBox_1" name="lastAcceptDt" type="text" style="width: 100px;" id="lastAcceptDt" onfocus="GetCal('lastAcceptDt','lastAcceptDt');"  readonly="readonly" />
                                                <img  onclick="GetCal('lastAcceptDt','calendarIcn');" alt="calendarIcn" style="vertical-align: bottom; cursor: pointer; border: 0px;" id="calendarIcn" name="calendarIcn" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" />
                                                <span class="reqF_1" id="dtErr"></span>
                                            </td>
                                            <%--<td width="76%"><input name="lastAcceptDt" type="text" class="formTxtBox_1" id="lastAcceptDt" style="width:100px;" readonly="true"/>
                                                <img src="../resources/images/Dashboard/calendarIcn.png" name="calendarIcn" alt="Calendar" border="0" style="vertical-align:middle;" style="vertical-align:middle;" onclick="GetCal('lastAcceptDt','lastAcceptDt');" /></td>--%>
                                        </tr>
                                        <tr>
                                            <td width="24%" class="ff"><%=str_comment_lbl%><span>*</span></td>
                                            <td width="76%"><textarea name="remarks" rows="5" class="formTxtBox_1" id="txtremarks" style="width:600px;"></textarea>
                                                <script type="text/javascript">
                                                  CKEDITOR.replace( 'remarks',
                                                  {
                                                       toolbar : "egpToolbar"

                                                  });
                                                 </script>
                                            <span class="reqF_1" id="remarksErr"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="t-align-center" id="btnInvite">
                                                <label class="formBtn_1"><input type="submit" name="button" id="button" value="Invite" onclick="return CompareToForToday(document.getElementById('lastAcceptDt').value)" /></label>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <%}/*IF tender Is live condition ends here*/%>
                        <%}/*Bid withDraw condition ends here*/else{
                                    if("withdrawal".equalsIgnoreCase(s_bidStatus)){%>
                        <div class="responseMsg noticeMsg" style="margin-bottom: 12px;">You have already withdrawn this tender. You cannot do Sub-Contracting.</div>
                        <%}/*Withdrawl Condition*/}%>
                        <%}//Condition for request is approved or not...
                                    }//end for finalSubmission and archive tender condition.
%>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                            <tr>
                                <th width="25%" class="t-align-center">Invitation sent to</th>
                                <th width="25%" class="t-align-center"><%=str_comment_lbl%></th>
                                <th width="25%" class="t-align-center">Last date for accepting invitation</th>
                                <th width="25%" class="t-align-center">Status</th>
                            </tr>
                            <%
                                        docCnt = 0;
                                        /*List of data*/
                                        String s_status = "Pending";
                                        for (SPCommonSearchData sptcd : tenderCS.searchData("SubContract", tenderId, user,s_lorPkgId,s_package,null,null,null,null,null)) {
                                            docCnt++;
                                            if(sptcd.getFieldName7().equalsIgnoreCase("Approved")){
                                               s_status = "Accepted"; 
                                            }else if(sptcd.getFieldName7().equalsIgnoreCase("Rejected")){
                                                s_status = "Rejected"; 
                                            }else{
                                                s_status = "Pending";
                                            }
                            %>
                            <tr>
                                <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                <td class="t-align-left"><%=sptcd.getFieldName4().replaceAll("quot;", "'") %></td>
                                <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                                <td class="t-align-center"><%=s_status%></td>
                            </tr>
                            <%
                                            if (sptcd != null) {
                                                sptcd = null;
                                            }
                                        }%>
                            <% if (docCnt == 0) {%>
                            <tr>
                                <td colspan="5" class="t-align-center" style="color: red;font-weight: bold">No records found</td>
                            </tr>
                            <%}%>
                        </table>
                    </div>
                </div>
                <%
            if (!list1.isEmpty()) {
                list1.clear();
                list1 = null;
            }

                %>
                <%}%>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
