<%-- 
    Document   : NegoTendererProcess
    Created on : Dec 24, 2010, 10:39:35 AM
    Author     : parag
--%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NegotiationProcessImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="neg" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean" />
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 1;
                            boolean flag1 = false;
                            String action = "";
                            String msg = "";
                            
                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("flag") != null) {
                                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                                    flag1 = true;
                                }
                            }
                            if(request.getParameter("action")!=null){
                                action = request.getParameter("action");
                            }
                             if(flag1){
                               if("accpet".equalsIgnoreCase(action)){
                                    msg = " Negotiation accepted successfully";
                               }
                               if("reject".equalsIgnoreCase(action)){
                                    msg = " Negotiation rejected successfully";
                               }
                               if("docsaccept".equalsIgnoreCase(action)){
                                    msg = " Negotiation Documents Accepted Successfully";
                               }
                               if("docsreject".equalsIgnoreCase(action)){
                                    msg = " Negotiation Documents Rejected Successfully";
                               }
                            }

                            List<SPTenderCommonData> tenderCommonDatas=neg.getBidderNegotiationDetails(tenderId,userId);

                            Object[] objects = neg.getTenderDetails(tenderId);
                            String procurementType = "";
                            String estCost = "";

                            procurementType = objects[0].toString();
                %>
                <div class="dashboard_div">                    
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea">
                        <div class="pageHead_1">Negotiation</div>
                        <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", tenderId);
                        %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        
                          <%pageContext.setAttribute("tab", "6");%>
                          <%@include file="../tenderer/TendererTabPanel.jsp" %>

                        <div class="tabPanelArea_1">
                            
                       
                            <div class="b_space">
                        <% if(flag1){ %>
                            <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                        <% }else{ %>
                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                         <% } %>
                         </div>
                         
                         <jsp:include page="EvalInnerTendererTab.jsp" >
                            <jsp:param name="EvalInnerTab" value="4" />
                            <jsp:param name="tenderId" value="<%=tenderId%>" />
                         </jsp:include>
                          <div class="tabPanelArea_1">
                         <table width="100%" cellspacing="0" class="tableList_1">  
                                <tr>
                                    <th width="5%">S.No.</th>
                                    <th width="10%">Start Date and Time</th>
                                    <th width="10%">End Date and Time</th>
                                    <th width="10%">Invitation <br/>Status</th>
                                    <th width="15%">Invitation <br/>Details</th>
                                    <%--<th width="10%">Negotiation <br/>Documents</th>--%>
                                    <th>Form Revision Status</th>
                                    <%--<th width="15%">Your Negotiation <br/>Acceptance Status</th>--%>
                                    <th width="15%">TEC Negotiation <br/>Acceptance Status</th>
                                    <th width="25%">Action</th>
                                </tr>
                                <%
                                   int count1=0;
                                   int negId = 0;
                                   TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                   if(!tenderCommonDatas.isEmpty()){
                                   for (SPTenderCommonData negDetails : tenderCommonDatas) {
                                        negId = Integer.parseInt(negDetails.getFieldName1());
                                        List<SPTenderCommonData> tenderAllCommonDatas = neg.getAllNegotiationDetails(negId);
                                %>
                                <tr>
                                    <td  style="text-align: center"><%=++count1%></td>
                                    <td style="text-align: center"><% if(!"".equals(negDetails.getFieldName2())){ out.print(negDetails.getFieldName2()); }else{ out.print("-"); } %></td>
                                    <td style="text-align: center"><% if(!"".equals(negDetails.getFieldName3())){ out.print(negDetails.getFieldName3()); }else{ out.print("-"); } %></td>
                                    <td style="text-align: center"><% if("Accept".equalsIgnoreCase(negDetails.getFieldName4())){ %>Accepted<% }else if("Reject".equalsIgnoreCase(negDetails.getFieldName4())){ %>Rejected<% }else{ out.println(negDetails.getFieldName4()); } %></td>
                                    <td style="text-align: center">
                                        <% if("pending".equalsIgnoreCase(negDetails.getFieldName8())) { %>
                                            <a href="<%=request.getContextPath()%>/tenderer/NegTendOfflineProcess.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>" >Accept / Reject</a>
                                        <%}else if("Accept".equalsIgnoreCase(negDetails.getFieldName8()) || "Reject".equalsIgnoreCase(negDetails.getFieldName8())){ %>
                                            <a href="../resources/common/ViewNegTendProcess.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>" >View </a>
                                        <% }else{ %>
                                            -
                                        <% } %>
                                    </td>
                                    <%--<td style="text-align: center">
                                        <% if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8())) { %>
                                            <a href="../resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
                                        <%
                                            if((!"Accept".equalsIgnoreCase(negDetails.getFieldName10().trim())) && (!"Reject".equalsIgnoreCase(negDetails.getFieldName10().trim()))){
                                               List<SPTenderCommonData> list = tenderCS.returndata("NegotiationDocInfo", ""+negDetails.getFieldName1(), "negotiation");
                                               if(!list.isEmpty()){
                                            %>
                                            &nbsp;|&nbsp;
                                            <a href="FinalApproveNegTender.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >Accept / Reject</a>
                                         <% } } }else{  out.println("-"); } %>
                                    </td>--%>
                                    <td style="text-align: center"> 
                                    <%
                                    if("Accept".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                        { %>Accepted<% }
                                    else if("Reject".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                        { %>Rejected<% }
                                    else if("yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                                        { %>Revised<% }
                                    else if("resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                            { %>Resend<% }
                                    else { %>-<%}
                                    %>
                                    </td>
                                    <%--<td style="text-align: center"><% if("Accept".equalsIgnoreCase(negDetails.getFieldName10())){ %>Accepted<% }else if("Reject".equalsIgnoreCase(negDetails.getFieldName10())){ %>Rejected<% }else{ out.println(negDetails.getFieldName10()); } %></td>--%>
                                    <td style="text-align: center"><%=negDetails.getFieldName9()%></td>
                                    <td style="text-align: center">
                                        <% if ("Online".equalsIgnoreCase(negDetails.getFieldName7()))
                                        {
                                           if ("Pending".equalsIgnoreCase(negDetails.getFieldName9()))
                                           {
                                               if ("Services".equalsIgnoreCase(procurementType))
                                               {
                                                   if ("Accept".equalsIgnoreCase(negDetails.getFieldName8()))
                                                   {
                                                       if ("".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()) || "resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                                       {
                                                            %>
                                                            <span style="color: red;">*</span> <a href="<%=request.getContextPath()%>/tenderer/viewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">Online Negotiation</a>
                                                            <%
                                                        }
                                                       else
                                                       {
                                                            %>
                                                            <a href="<%=request.getContextPath()%>/tenderer/viewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">View</a>
                                                            <%
                                                       }

                                                        if ("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName2()) && "".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                                                        {
                                                            %>
                                                            | <br /><a href="NegTendererForm.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">Process <br /></a>
                                                            <%
                                                        }
                                                       else if(negDetails.getFieldName5()!=null && negDetails.getFieldName5().equalsIgnoreCase("yes"))
                                                       {
                                                           %>
                                                            | <br /><a href="NegTendererForm.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form <br /></a>
                                                            <%

                                                        }
                                                   }
                                                }
                                               
                                               else
                                               {
                                                    if ("Accept".equalsIgnoreCase(negDetails.getFieldName8()) && "".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                                                    {
                                                        %>
                                                        <span style="color: red;">*</span> <a href="<%=request.getContextPath()%>/tenderer/viewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">Online Negotiation</a>
                                                        <%
                                                        if ("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName2()) && "".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                                                        {
                                                            if ("Services".equalsIgnoreCase(procurementType))
                                                            {%>
                                                                | <br /><a href="NegTendererForm.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">Process <br /></a>
                                                    <%
                                                            }
                                                            else
                                                            {
                                                    %>
                                                                | <br /><a href="NegTenderBidPrepare.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>">Process <br /></a>
                                                                <%
                                                            }
                                                       }
                                                   }
                                               }
                                               
                                               
                                           }
                                           else
                                           {
                                               out.println("-");
                                           }
                                    }
                                        %>
                                        
                                    </td>
                                </tr>
                                <% }
                                   }else{ %>
                                   <tr>
                                       <td colspan="10">No Records Found</td>
                                   </tr>
                                <% } %>
                            </table></div>
                        </div>
                        <%@include file="../resources/common/Bottom.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>

