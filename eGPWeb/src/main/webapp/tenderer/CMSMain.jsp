<%-- 
    Document   : CMSMain
    Created on : Nov 22, 2011, 5:37:05 PM
    Author     : dixit
--%>


<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>CMS</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">CMS</div>
                <%
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                tenderId = request.getParameter("tenderId");
                            }
                            String userId = "";
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = session.getAttribute("userId").toString();
                                issueNOASrBean.setLogUserId(userId);
                            }
                            pageContext.setAttribute("tab", "10");
                            List<Object[]> objBusinessRule = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                                        
                            //String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include file="TendererTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "5");

                    %>
                    <%@include  file="../tenderer/cmsTab.jsp"%>
                    <div class="tabPanelArea_1">
                        <div align="center">
                            <%
                                        String capNameOfVendor = "";
                                        String strLotNo = "Lot No.";
                                        String strLotDes = "Lot Description";
                                        if ("goods".equalsIgnoreCase(procnature)) {
                                            capNameOfVendor = "Name of Supplier";
                                        } else if ("works".equalsIgnoreCase(procnature)) {
                                            capNameOfVendor = "Name of Contractor";
                                        }else if("services".equalsIgnoreCase(procnature))
                                        {
                                            strLotNo = "Package No.";
                                            strLotDes = "Package Description";
                                            capNameOfVendor = "Name of Consultant";
                                        }
                                        for (Object[] obj : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                                            List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                            SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                                            if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                                CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                                List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId), Integer.parseInt(obj[0].toString()));
                                                boolean editFlag = false;
                                                if (!configdatalist.isEmpty()) {
                                                    editFlag = true;
                                                }
                                                boolean flagg = service.checkContractSigning(tenderId, Integer.parseInt(obj[0].toString()));
                            %>

                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="12%" class="t-align-left"><%=strLotNo%></td>
                                    <td   class="t-align-left"><%=obj[6]%></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"><%=strLotDes%></td>
                                    <td  class="t-align-left"><%=obj[7]%></td>
                                </tr>
                            </table>
                            <br />
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="15%" class="t-align-left"><%= capNameOfVendor%></th>
                                    <th width="10%" class="t-align-left">Contract No.</th>
                                    <th width="10%" class="t-align-left">Contract Name</th>
                                    <!--                                <th width="15%" class="t-align-left">Date of Issue</th>
                                                                    <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                                                    <th width="15%" class="t-align-left">NOA Acceptance Status</th>-->
                                    <%
                                         if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    %>
                                    <th width="15%" class="t-align-left">Performance Security </th>
                                    <%}%>
                                    <th width="10%" class="t-align-left">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><%=obj[9]%></td>
                                    <td class="t-align-center" > <%=obj[2]%></td>
                                    <td class="t-align-center" > <%=obj[7]%></td>
                                    <%--<td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[3])%></td>
                                    <%if (obj[5] != null) {%>
                                    <td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[5])%></td>
                                    <%}
                                        if (obj[4].toString().equalsIgnoreCase("approved")) {
                                    %>
                                    <td class="t-align-center">Accepted</td>
                                    <%} else if (obj[4].toString().equalsIgnoreCase("declined")) {%>
                                    <td class="t-align-center">Declined</td>
                                    <%}--%><%--<%
                                                                            if (detailsPayment.isEmpty()) {%>
                                    <td class="t-align-center" >Pending</td>
                                    <%} else {
                                                                        isPerformanceSecurityPaid = true;
                                    %>
                                    <td class="t-align-center" >Received (<%=detailsPayment.get(0).getStatus()%>)</td>

                                    <%}%>--%>
                                    <%
                                        if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                            out.print("<td class=\"t-align-center\">");
                                            if (detailsPayment.isEmpty()) {
                                                out.print("Pending");
                                            } else {
                                                if (detailsPayment.get(0).getIsVerified().equalsIgnoreCase("yes")) {
                                                    out.print("Received");
                                                } else {
                                                    out.print("Pending");
                                                }
                                            }
                                        }
                                    %>
                                    <td class="t-align-center">
                                        
                                        <% List<TblCmsDateConfig> tblcmsdatedata = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(obj[0].toString()));
                                        if(!tblcmsdatedata.isEmpty()){
%>

                                        <a href="ConfiDatesUploadDoc.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&keyId=<%=tblcmsdatedata.get(0).getActContractDtId()%>&docx=Download">Download Files</a>&nbsp;|&nbsp;
                                        <a href="ViewConfiDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=view">View Date History</a>
                                        <%}else{out.print("Commencement Date is not configured by PE");} }%>
                                    </td>
                                </tr>
                            </table>
                                                                      <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide((Integer) obj[0]);
                                        if(!mainForRO.isEmpty()){
                                            int icount=1;
                                                for(Object[] objs : mainForRO){

                                        for (Object[] objR : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                            List<TblTenderPayment> detailsPaymentR = issueNOASrBean.getPaymentDetailsForRO(Integer.parseInt(tenderId), (Integer) objR[0], (Integer) objR[8],(Integer) objR[1]);
                                            if (issueNOASrBean.isAvlTCS((Integer) objR[1])) {
                                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                                CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                                List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(objR[0].toString()),(Integer) objR[11]);
                                                boolean editFlag = false;
                                                if (!configdatalist.isEmpty()) {
                                                    editFlag = true;
                                                }
                                                boolean flagg = service.checkContractSigning(tenderId, Integer.parseInt(objR[0].toString()));
                            %>

                            <br />
                            <%if (!editFlag) {
                            %>
                            <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.goods.configdates")%></div>
                                                                  <%  }%>

                            <br />
                            <td colspan="5">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="15%" class="t-align-left"><%= capNameOfVendor%></th>
                                    <th width="10%" class="t-align-left">Contract No.</th>
                                    <th width="10%" class="t-align-left">Contract Name</th>
                                    <!--                                <th width="15%" class="t-align-left">Date of Issue</th>
                                                                    <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                                                    <th width="15%" class="t-align-left">NOA Acceptance Status</th>-->
                                    <%
                                         if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    %>
                                    <th width="15%" class="t-align-left">Performance Security </th>
                                    <%}%>
                                    <th width="10%" class="t-align-left">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><%=objR[9]%></td>
                                    <td class="t-align-center" > <%=objR[2]%></td>
                                    <td class="t-align-center" > <%=objR[7]%></td>

                                   
                                    <%
                                        if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                            out.print("<td class=\"t-align-center\">");
                                            if (detailsPaymentR.isEmpty()) {
                                                out.print("Pending");
                                            } else {
                                                if (detailsPaymentR.get(0).getIsVerified().equalsIgnoreCase("yes")) {
                                                    out.print("Received");
                                                } else {
                                                    out.print("Pending");
                                                }
                                            }
                                        }
                                    %>
                                    <td class="t-align-center">

                                        <% List<TblCmsDateConfig> tblcmsdatedata = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(objR[0].toString()));
                                        if(!configdatalist.isEmpty()){
%>

                                        <a href="ConfiDatesUploadDoc.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&keyId=<%=tblcmsdatedata.get(0).getActContractDtId()%>&docx=Download">Download Files</a>&nbsp;|&nbsp;
                                        <a href="ViewConfiDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=view">View Date History</a>
                                        <%}else{out.print("Commencement Date is not configured by PE");} }%>
                                    </td>
                                </tr>
                            </table>


<%}%>
<%icount++;}%>
<%}%>




                            <%}%>
                        </div>
                    </div></div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }        
    </script>
</html>
<%
    issueNOASrBean = null;
%>