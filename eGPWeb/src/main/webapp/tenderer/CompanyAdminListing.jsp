<%-- 
    Document   : CompanyAdminListing
    Created on : Mar 2, 2011, 10:33:44 AM
    Author     : TaherT
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Users</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script>
                function fillGridOnEvent(type){
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:'<%=request.getContextPath()%>/CompanyAdminServlet?cId=<%=request.getParameter("cId")%>&action=getData&status='+type,
                        datatype: "xml",
                        height: 250,
                        colNames:['Sl. <br/> No.','e-mail ID','User\'s Name','Country',"State","Department","Status","Action"],
                        colModel:[
                           {name:'srNo',index:'srNo', width:50,sortable:false,align:'center'},
                            {name:'emailId',index:'tlm.emailId', width:180,sortable:true},
                            {name:'userName',index:'ttm.firstName+\' \'+ttm.lastName', width:150,sortable:true},
                            {name:'country',index:'ttm.country', width:90,sortable:true,align:'center'},
                            {name:'state',index:'ttm.state', width:90,sortable:true,align:'center'},
                            {name:'department',index:'ttm.department', width:120,sortable:true},
                            {name:'status',index:'tlm.status', width:80,sortable:true,align:'center'},
                            {name:'action',index:'action', width:200,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});
                }
                jQuery().ready(function (){
                    //fillGrid();
                });
        </script>
       <script>
                function test(id){
                    if(id==1){
                        document.getElementById("linkPending").className = "sMenu";
                        document.getElementById("linkProccessed").className = "";
                        fillGridOnEvent('approved');
                    }else if(id==2){
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkProccessed").className = "sMenu";
                        fillGridOnEvent('suspended');
                    }
                }
      </script>
    </head>
    <body onload="test(1);">
        <div class="mainDiv">
            <div class="fixDiv">
            <div class="contentArea_1">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="pageHead_1">Manage Users
            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('7');">Save as PDF</a></span>
            </div><br/>
            <%if("success".equals(request.getParameter("flag"))){out.print("<div class=\"responseMsg successMsg\">Response posted successfully</div>");}%>
            <div align="right">
            <a href="CompanyUser.jsp?cId=<%=request.getParameter("cId")%>" class="action-button-add">Register User</a></div>
            <ul class="tabPanel_1 t_space">
                <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);">Approved</a></li>
                <li><a href="javascript:void(0);" id="linkProccessed" onclick="test(2);">Suspended</a></li>
            </ul>            
            <div class="tabPanelArea_1">
                    <div id="jQGrid" align="center">
                        <table id="list"></table>
                        <div id="page"></div>
                    </div>
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">

                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="ManageUsers_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="ManageUsers" />
                </form>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
        </div>
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabCompVerify");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
