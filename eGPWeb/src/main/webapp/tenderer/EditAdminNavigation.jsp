<%-- 
    Document   : EditNavigation
    Created on : Jan 4, 2010, 06:32:18 PM
    Author     : Taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.*"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
    <body>
        <div class="stepWiz_1 t_space">
            <ul>
                <%
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(request.getParameter("uId")));//
                            String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];
                            String payId="0";
                            TenderCommonService service = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> peBUEmail = service.returndata("getEmailIdfromUserId",request.getParameter("uId"),null);
                            List<SPTenderCommonData> list = service.returndata("SearchEmailForContentAdmin",peBUEmail.get(0).getFieldName1(), null);
                            if(!list.isEmpty()){
                                if(!list.get(0).getFieldName1().equals("0")){
                                     payId=list.get(0).getFieldName1();                                     
                                 %>
                                    <li <%if(pageName.equals("TendererPaymentDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererPaymentDetail.jsp")){%><a href="TendererPaymentDetail.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Payment Details<%if(!pageName.equals("TendererPaymentDetail.jsp")){%></a><%}%></li>
                                 <%
                                }
                            }
                            if (pageList.size() == 3) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 4) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererCompDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererCompDetails.jsp")){%><a href="TendererCompDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Company Details<%if(!pageName.equals("TendererCompDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 5) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererCompDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererCompDetails.jsp")){%><a href="TendererCompDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Company Details<%if(!pageName.equals("TendererCompDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererJVDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererJVDetails.jsp")){%><a href="TendererJVDetails.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant JVCA Details<%if(!pageName.equals("TendererJVDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=request.getParameter("s")%>"><%}%>Bidder/Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%}
                %>
            </ul>
        </div>   
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabCompVerify");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
