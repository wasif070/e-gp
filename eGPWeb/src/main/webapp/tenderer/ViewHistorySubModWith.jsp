<%-- 
    Document   : ViewHistorySubModWith
    Created on : Jun 9, 2011, 11:08:59 PM
    Author     : rikin.p
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Substitution / Modification History or Withdrawal History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <%
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();
            }
            
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            commonSearchService.setLogUserId(userId);
            
             // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            String idType="tenderId";
            int auditId=Integer.parseInt(request.getParameter("tenderId"));
            String auditAction="Bidder has viewed substitution/modification/Withdrawal history";
            String moduleName=EgpModule.Bid_Submission.getName();
            String remarks="Bidder(User Id): "+session.getAttribute("userId") +" has viewed substitution/modification/Withdrawal history of Tender id: "+auditId;
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

        %>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">View Substitution / Modification History or Withdrawal History
                         <span style="float:right;">
                            <a class="action-button-goback" href="<%=request.getContextPath()%>/tenderer/SubmissionReceipt.jsp?tenderId=<%=tenderId%>">Go back to dashboard </a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div class="t_space">
                        <%@include file="TendererTabPanel.jsp" %>
                    </div>
                    <div class="tabPanelArea_1">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="20%" class="t-align-center ff">Withdrawal</th>
                                <th width="80%" class="t-align-center ff">&nbsp;</th>
                            </tr>
                            <tr>
                                <td width="20%" class="t-align-center ff">Date and Time</td>
                                <td width="80%" class="t-align-center ff">Reason for Withdrawal</td>
                            </tr>
                            <%
                                List<SPCommonSearchData> withdrawList = commonSearchService.searchData("getBidModificationInfo", "withdrawal", ""+tenderId, ""+userId, "", "", "", "", "", "");
                            %>
                            <% if(withdrawList.isEmpty()){ %>
                            <tr>
                                <td class="t-align-center ff" style="color: red;" colspan="2">No Records Found</td>
                            </tr>
                            <% }
                               else
                               { 
                                    for (SPCommonSearchData withdrawItem : withdrawList) { %>
                                    <tr>
                                        <td class="t-align-center"><%=withdrawItem.getFieldName4()%></td>
                                        <td class="t-align-left"><%=withdrawItem.getFieldName3()%></td>
                                    </tr> 
                                <%  }
                               }
                            %>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="20%" class="t-align-center ff">Substitution / Modification</th>
                                <th width="80%" class="t-align-center ff">&nbsp;</th>
                            </tr>
                            <tr>
                                <td width="20%" class="t-align-center ff">Date and Time</td>
                                <td width="80%" class="t-align-center ff">Reason for Substitution / Modification</td>
                            </tr>
                            <%
                                List<SPCommonSearchData> modifyList = commonSearchService.searchData("getBidModificationInfo","modify", ""+tenderId, ""+userId, "", "", "", "", "", "");
                                
                            %>
                            <% if(modifyList.isEmpty()){ %>
                            <tr>
                                <td class="t-align-center ff" style="color: red;" colspan="2">No Records Found</td>
                            </tr>
                            <% }
                               else
                               { 
                                    for (SPCommonSearchData modifyItem : modifyList) { %>
                                    <tr>
                                        <td class="t-align-center"><%=modifyItem.getFieldName4()%></td>
                                        <td class="t-align-left"><%=modifyItem.getFieldName3()%></td>
                                    </tr> 
                                <%  }
                               }
                            %>
                        </table>
                    </div>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
