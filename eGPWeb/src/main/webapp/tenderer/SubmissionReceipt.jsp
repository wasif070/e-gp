<%--
    Document   : SubmissionReceipt
    Created on : Dec 16, 2010, 4:28:26 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.FinalSubmissionService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderBidService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Submission Receipt</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" /> 
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#print_area').printElement(options);
            }
        </script>
    </head>
    <style>
    </style>
    <%
        CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
        Object procnature = cs.getProcNature(request.getParameter("tenderId"));
        String userId = "";

        String isPDF = "abc";
        if (request.getParameter("isPDF") != null)
        {
            isPDF = request.getParameter("isPDF");
        }
        pageContext.setAttribute("isPDF", isPDF);

        if ("true".equalsIgnoreCase(isPDF))
        {
            if (request.getParameter("userId") != null && !"".equalsIgnoreCase(request.getParameter("userId").toString()))
            {
                userId = request.getParameter("userId").toString();
            }
        } else
        {
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString()))
            {
                userId = session.getAttribute("userId").toString();
            }
        }
        pageContext.setAttribute("userId", userId);

        boolean isSubDt = false;
        String tenderId = "";
        if (request.getParameter("tenderId") != null)
        {
            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
            tenderId = request.getParameter("tenderId");
        }

        String folderName = folderName = pdfConstant.SUBMISSIONRECEIPT;
        String genId = tenderId;

        //swati--to generate pdf
        if (!(isPDF.equalsIgnoreCase("true")))
        {
            try
            {
                String reqURL = request.getRequestURL().toString();
                String reqQuery = "tenderId=" + tenderId + "&userId=" + userId;
                pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        //end

        //List<CommonTenderDetails> commonTenderDetails = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
        //String docAvlMethod = commonTenderDetails.get(0).getDocAvlMethod();
        //String docAvlMethod = commonTenderDetails.get(0).getTenderLotSecId();

    %>
    <body>
        <%
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

            if (request.getParameter("btnFinalSub") != null)
            {
                
                
                FinalSubmissionService finalSubmissionService = (FinalSubmissionService) AppContext.getSpringBean("FinalSubmissionService");
                //out.println("kinj");
                //out.println(tenderId);
                //out.println(userId);

                String ipAddress = request.getHeader("X-FORWARDED-FOR");

                if (ipAddress == null || "null".equals(ipAddress) || ipAddress == "-1" || "-1".equals(ipAddress))
                {
                    ipAddress = request.getRemoteAddr();
                }
                List<SPCommonSearchData> lotList = commonSearchService.searchData("doFinalSubmission", tenderId, userId, ipAddress, null, null, null, null, null, null);

                try
                {
                    List<CommonSPReturn> cmr = finalSubmissionService.generateMegaHash(Integer.parseInt(tenderId), Integer.parseInt(userId));
                    if (cmr.get(0).getFlag())
                    {
                        System.out.println("Success : " + cmr.get(0).getMsg());
                    }
                } catch (Exception e)
                {
                    System.out.println("Error in generate mega hash : " + e.toString());
                }
                if (!lotList.isEmpty())
                {


                    if (lotList.get(0).getFieldName1().equalsIgnoreCase("1"))
                    {


                        //For mail, sms and msgBox(TaherT)
                        try
                        {
                            TenderBidService tenderBidService = (TenderBidService) AppContext.getSpringBean("TenderBidService");
                            SPTenderCommonData sptcd = tenderCommonService1.returndata("tenderinfobar", tenderId, null).get(0);
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            String data[] = tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());
                            String mails[] =
                            {
                                data[0]
                            };
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            String msgBox = msgBoxContentUtility.bidTenderSubMsg(tenderId, userId, "final");
                            userRegisterService.contentAdmMsgBox(data[0], XMLReader.getMessage("msgboxfrom"), "e-GP: Final submission of an e-Tender", msgBox);
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            String mailText = mailContentUtility.bidTenderSubMail(tenderId, userId, "final");
                            sendMessageUtil.setEmailTo(mails);
                            sendMessageUtil.setEmailSub("e-GP: Final submission of an e-Tender");
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();
                            sendMessageUtil.setSmsNo(data[1] + data[2]);
                            //sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully completed final submission for the below mentioned Tender%0ATender Id:"+tenderid+"%0ARef. No:"+sptcd.getFieldName2()+"%0Ae-GP System");
                            sendMessageUtil.setSmsBody("Dear User,%0AYou have successfully completed final submission of%0ATender Id:" + tenderId + "%0ARef. No:" + sptcd.getFieldName2() + "%0Ae-GP System");
                            sendMessageUtil.sendSMS();
                            data = null;
                            mails = null;
                            sendMessageUtil = null;
                            msgBoxContentUtility = null;
                            //msg sending ended
                        } catch (Exception e)
                        {
                        }

                        response.sendRedirect("SubmissionReceipt.jsp?tenderId=" + tenderId);
                    } else if ("0".equalsIgnoreCase(lotList.get(0).getFieldName1()))
                    {
                        out.print("<div width='100%' align='center'><font style='color:red;'>" + lotList.get(0).getFieldName2() + "</font></div>");

                    }
                }
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <% if (!(isPDF.equalsIgnoreCase("true")))
                    {%>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <% }%>
                <div class="contentArea_1">
                    <% if (!(isPDF.equalsIgnoreCase("true")))
                        {%>
                    <div class="pageHead_1">Tender Dashboard</div>
                    <% }%>
                    <div  id="print_area">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>                    

                        <%
                            boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                        %>
                        <div class="t_space" >&nbsp;</div>
                        <% if (!(isPDF.equalsIgnoreCase("true")))
                        {%>
                        <span style="float:right;" class="noprint">
                            <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>&nbsp;
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                        </span>

                        <% }%>

                        <%
                            pageContext.setAttribute("tab", "4");
                        %>

                        <% if (!(isPDF.equalsIgnoreCase("true")))
                        {%>
                        <div class="noprint">
                            <% } else
                    {%>
                            <div style="display: none;">
                                <% }%>              
                                <%@include file="TendererTabPanel.jsp" %>
                            </div>  
                            <%if (!is_debared)
                         {%>
                            <div class="tabPanelArea_1">
                                <%
                                    //Message Added by TaherT
                                    if ("with".equals(request.getParameter("msg")))
                                    {
                                        out.print("<br/><div class='responseMsg successMsg'>You have withdrawn your submission successfully</div>");
                                    }
                                    if ("mod".equals(request.getParameter("msg")))
                                    {
                                        out.print("<br/><div class='responseMsg successMsg'>Reason entered successfully. Please proceed for modification</div>");
                                    }
                                    if ("delerror".equals(request.getParameter("msg")))
                                    {
                                        out.print("<br/><div class='responseMsg errorMsg'>You have already completed Final Submission. You can't delete this form now</div><br />");
                                    }
                                %>
                                <%
                                    List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                                    if (submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true"))
                                    {
                                        isSubDt = true;
                                    } else
                                    {
                                        isSubDt = false;
                                    }
                                    if (isTenPackageWis)
                                    {
                                        //taher
                                        //List<SPCommonSearchData> packageList = ;
                                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                        {
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                                    </tr>

                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="50%" class="t-align-left ff">Form Name</th>
                                        <!--<th width="25%" class="t-align-left">Mapped Documents List</th>-->
                                        <th width="10%" class="t-align-center ff">Filled (Yes/No)</th>
                                        <!--<th width="22%" class="t-align-center ff">Encrypted with Buyer Hash</th>-->
                                        <th width="40%" class="t-align-left ff">e-Signature / Hash</th>
                                    </tr>
                                    <%
                                        //Taher Starts
                                        List<SPCommonSearchData> techFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Technical", userId, null, null, null, null, null);
                                        for (SPCommonSearchData formname : techFormList)
                                        {
                                            if (formname.getFieldName6() == null || (formname.getFieldName6() != null && !"createp".equalsIgnoreCase(formname.getFieldName6())))
                                            {
                                    %>
                                    <tr>
                                            <td class="t-align-left ff"><%out.print(formname.getFieldName1());
                                if ("c".equalsIgnoreCase(formname.getFieldName6()))
                                {
                                    out.print("<font color='red'>(cancelled)</font>");
                                }%></td>
                                            <%--<td class="t-align-center ff">
                                                        <% List<SPCommonSearchData> mapedDLL = commonSearchService.searchData("getTenderDoc", tenderId, formname.getFieldName5(), userId, null, null, null, null, null, null);
                                                            if(!mapedDLL.isEmpty()){
                                                                                                                                    for (SPCommonSearchData mapedDL : mapedDLL) {

                                    %>
                                    <%=mapedDL.getFieldName2()%><br/>
                                    <% } }else{out.print("-");}%></td>--%>
                                        <td class="t-align-center ff"><%=formname.getFieldName11()%></td>
                                        <!--<td class="t-align-center ff"><%=formname.getFieldName7()%></td>-->
                                        <td class="t-align-center ff"><%if (formname.getFieldName10() == null || "null".equalsIgnoreCase(formname.getFieldName10()))
                                {
                                    out.print("-");
                                } else
                                {
                                    out.print(formname.getFieldName10());
                                }%></td>

                                    </tr>
                                    <%
                                            }/*For Forms which are corrigendum is not published*/
                                        }
                                    %>
                                    <%
                                        List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("getlotid_lotdesc", tenderId, userId, null, null, null, null, null, null, null);
                                        for (SPCommonSearchData lotdsc : lotIdDesc)
                                        {
                                            if(!"services".equalsIgnoreCase(procnature.toString())){
                                                out.print("<tr><td colspan='5'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Lot No.</td><td width='85%'>" + lotdsc.getFieldName4() + "</td></tr><tr><td class='t-align-left ff'>Lot Description</td><td>" + lotdsc.getFieldName2() + "</td></tr></table></td></tr>");
                                            }else{
                                                out.print("<tr><td colspan='5'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Package No.</td><td width='85%'>" + lotdsc.getFieldName4() + "</td></tr><tr><td class='t-align-left ff'>Package Description</td><td>" + lotdsc.getFieldName2() + "</td></tr></table></td></tr>");
                                            }
                                            out.print("<tr>");
                                            out.print("<th width='50%' class='t-align-left ff'>Form Name</th>");
                                            //out.print("<th width='25%' class='t-align-left'>Mapped Documents List</th>");
                                            out.print("<th width='10%' class='t-align-center ff'>Filled (Yes/No)</th>");
                                            //out.print("<th width='22%' class='t-align-center ff'>Encrypted with Buyer Hash</th>");
                                            out.print("<th width='40%' class='t-align-left ff'>e-Signature / Hash</th>");
                                            out.print("</tr>");
                                            List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Pricebid", userId, lotdsc.getFieldName1(), null, null, null, null);
                                            for (SPCommonSearchData formdetails : priceFormList)
                                            {
                                                if (formdetails.getFieldName6() == null || (formdetails.getFieldName6() != null && !"createp".equalsIgnoreCase(formdetails.getFieldName6())))
                                                {
                                                    //for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, "0", null, null, null, null, null, null)) {
%>
                                    <tr>
                                        <td class="t-align-left ff"><%out.print(formdetails.getFieldName1());
                                if ("c".equalsIgnoreCase(formdetails.getFieldName6()))
                                {
                                    out.print("<font color='red'>(cancelled)</font>");
                                }%></td>
                                        <%--<td class="t-align-center ff"><% List<SPCommonSearchData> mapedDLL = commonSearchService.searchData("getTenderDoc", tenderId, formdetails.getFieldName5(), userId, null, null, null, null, null, null);
                                                       if(!mapedDLL.isEmpty()){
                                                                                                                               for (SPCommonSearchData mapedDL : mapedDLL) {

                                    %>
                                    <%=mapedDL.getFieldName2()%><br/>
                                    <% } }else{out.print("-");}%></td>--%>
                                        <td class="t-align-center ff"><%=formdetails.getFieldName11()%></td>
                                        <%--<td class="t-align-center ff"><%=formdetails.getFieldName7()%></td>--%>
                                        <td class="t-align-center ff"><%if (formdetails.getFieldName10() == null || "null".equalsIgnoreCase(formdetails.getFieldName10()))
                                {
                                    out.print("-");
                                } else
                                {
                                    out.print(formdetails.getFieldName10());
                                }%></td>
                                    </tr>
                                    <%}/*Condtion for corrigendum is not published then dont need to show forms*/ }
                            }//Taher Ends%>                           
                                </table>
                                <% }
                                } else
                                {

                                    for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                    {
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                                    </tr>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                    </tr>
                                </table>
                                <%  }
                                    for (SPCommonSearchData lotList : commonSearchService.searchData("getlotid_lotdesc", tenderId, "" + userId, "", null, null, null, null, null, null))
                                    {
                                %>
                                <%                                    
                                    if(!"services".equalsIgnoreCase(procnature.toString())){
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                    <tr>
                                        <td width="22%" class="t-align-left ff">Lot No. :</td>
                                        <td width="78%" class="t-align-left"><%=lotList.getFieldName4()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Lot Description :</td>
                                        <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                                    </tr>
                                </table>
                                <%}%>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th width="22%" class="t-align-left ff">Form Name</th>
                                        <!--                                <th width="23%" class="t-align-left">Mapped Documents List</th>-->
                                        <th width="22%" class="t-align-center ff">Filled (Yes/No)</th>
                                        <!--                                <th width="22%" class="t-align-center ff">Encrypted with Buyer Hash</th>-->
                                        <th width="22%" class="t-align-left ff">e-Signature / Hash</th>
                                    </tr>
                                    <%
                                        for (SPCommonSearchDataMore formdetails : commonSearchDataMoreService.geteGPData("getformDetails", tenderId, userId, lotList.getFieldName5()))
                                        {
                                            if (formdetails.getFieldName6() == null || (formdetails.getFieldName6() != null && !"createp".equalsIgnoreCase(formdetails.getFieldName6())))
                                            {

                                    %>
                                    <tr>
                                        <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                        <%-- <td class="t-align-left">
                                             <%
                                                                                                                         for (SPCommonSearchData mapedDL : commonSearchService.searchData("getTenderDoc", tenderId, formdetails.getFieldName1(), userId, null, null, null, null, null, null)) {
                                                             if(!"".equalsIgnoreCase(mapedDL.getFieldName2())){
                                             %>
                                             <%=mapedDL.getFieldName2()%><br/>
                                             <% }else{out.print("-");}}%>
                                         </td>--%>
                                        <td class="t-align-center ff"><%=formdetails.getFieldName3()%></td>
                                        <%--<td class="t-align-center ff"><%=formdetails.getFieldName4()%></td>--%>
                                        <td class="t-align-left ff"><% if (formdetails.getFieldName5() == null || "null".equalsIgnoreCase(formdetails.getFieldName5()))
                                    {
                                        out.print("-");
                                    } else
                                    {
                                        out.print(formdetails.getFieldName5());
                                    }%></td>
                                    </tr>
                                    <%}
                                }%>
                                </table>
                                <%     }/*If corigendum is not published then dont need to show from created*/
                                    }
                                %>

                                <%
                                    List<SPCommonSearchData> formList = commonSearchService.searchData("getFormNameByTenderId", tenderId, "", "", "", "", "", "", "", "");
                                    boolean showflag = false;

                                    for (SPCommonSearchData formname : formList)
                                    {
                                        List<SPCommonSearchData> docList = commonSearchService.searchData("getDocRelatedInfo", tenderId, formname.getFieldName2(), "" + userId, "", "", "", "", "", "");
                                %>
                                <% if (!docList.isEmpty())
                                {%>

                                <% if (showflag == false)
                                {%>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="20%" class="t-align-center ff">Form Name</th>
                                        <td width="80%" class="t-align-center ff">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1">
                                                <tr>
                                                    <th width="110px" class="t-align-left ff">Mapped Document’s Name</th>
                                                    <th width="110px" class="t-align-left ff">File Name </th>
                                                    <th width="150px" class="t-align-left ff">e-Signature / Hash</th>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%
                                            showflag = true;
                                        }
                                    %>
                                    <tr>
                                        <td width="20%" class="t-align-left ff"><%=formname.getFieldName1()%></td>
                                        <td colspan="3">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1" style="table-layout: fixed;">
                                                <%
                                                    for (SPCommonSearchData docname : docList)
                                                    {
                                                %>
                                                <tr>  
                                                    <td width="110px" class="t-align-center" style="word-wrap: break-word;"><% if ("".equals(docname.getFieldName2()))
                                             {
                                                 out.print("-");
                                             } else
                                             {
                                                 out.print(docname.getFieldName2());
                                             }%>&nbsp;</td>
                                                    <td width="110px" class="t-align-center" style="word-wrap: break-word;"><% if ("".equals(docname.getFieldName1()))
                                            {
                                                out.print("-");
                                            } else
                                            {
                                                out.print(docname.getFieldName1());
                                            }%>&nbsp;</td>
                                                    <td width="150px" class="t-align-center" style="word-wrap: break-word;">
                                                        <% if (docname.getFieldName3() == null || "null".equalsIgnoreCase(docname.getFieldName3()))
                                                {
                                                    out.print("-");
                                                } else
                                                {
                                                    out.print(docname.getFieldName3());
                                                }%>&nbsp;
                                                    </td>
                                                </tr>
                                                <% }%>
                                            </table>
                                        </td>
                                    </tr>
                                    <%  }
                                        }
                                    %>
                                </table>
                                <%
                                    List<SPCommonSearchData> withdrawList = commonSearchService.searchData("getBidModificationInfo", "withdrawal", "" + tenderId, "" + userId, "", "", "", "", "", "");
                                    List<SPCommonSearchData> modifyList = commonSearchService.searchData("getBidModificationInfo", "modify", "" + tenderId, "" + userId, "", "", "", "", "", "");

                                    if (!withdrawList.isEmpty() || !modifyList.isEmpty())
                                    {
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                                    <tr>
                                        <td width="40%" class="t-align-left ff">Tender Substitution / Modification History, Withdrawal History :</td>
                                        <td width="60%" class="t-align-left"><a href="<%=request.getContextPath()%>/tenderer/ViewHistorySubModWith.jsp?tenderId=<%=tenderId%>" class="noprint">View</a></td>
                                    </tr>
                                </table>
                                <% }%>
                                
                                 <%
                                    List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                                    if (!btnFinallst.isEmpty())
                                    {
                                        // out.print("<h1>"+btnFinallst.get(0).getFieldName1()+"</h1>");
                                        if (btnFinallst.get(0).getFieldName1() != null && btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission"))
                                        {
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td width="40%" class="t-align-left ff">Your Mega Hash :</td>
                                        <td width="60%" class="t-align-left">
                                            <%
                                                for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaHash", tenderId, userId, "0", null, null, null, null, null, null))
                                                {
                                                    out.println(megaHash.getFieldName1());
                                                }
                                            %>

                                        </td>
                                    </tr>
                                    <%if (!isSubDt)
                                 {%>
                                    <tr>
                                        <td class="t-align-left ff">Mega Mega Hash</td>

                                        <td class="t-align-left">
                                            <%
                                                for (SPCommonSearchData megaMegaHash : commonSearchService.searchData("getTenderMegaMegaHash", tenderId, userId, "0", null, null, null, null, null, null))
                                                {

                                                    out.println(megaMegaHash.getFieldName1());
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                               
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="t-align-left">
                                            <%
                                                for (SPCommonSearchData msg : commonSearchService.searchData("FinalSubDtIP", tenderId, userId, "0", null, null, null, null, null, null))
                                                {
                                            %>
                                            <div class="mandatory">Final Submission Completed Successfully at - <%=msg.getFieldName1()%>. IP Address : <%=msg.getFieldName2()%></div>
                                            <% }%>

                                        </td>
                                    </tr>
                                    <tr style="display: none;" class="noprint" id="withmodifyDisp">
                                        <td class="t-align-left">
                                            <div class="mandatory">If you want to Substitute or Withdraw the submitted tender, click the relevant button below.</div>
                                        </td>
                                    </tr>
                                </table>
                                <%
                                        }
                                        else
                                        {
                                    %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <td class="t-align-left">
                                                        <div class="responseMsg noticeMsg" style="margin-top: 10px;">You have not submitted your Bid.</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        <%
                                        }
                            }
                            else
                            {
                        %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="t-align-left">
                                            <div class="responseMsg noticeMsg" style="margin-top: 10px;">You have not submitted your Bid.</div>
                                        </td>
                                    </tr>
                                </table>
                            <%
                            }
                            %>
                            </div>
                            <% if (!(isPDF.equalsIgnoreCase("true")))
                       {%>
                            <div class="t-align-center t_space">
                                <form action="" id="frmDocReadConf" method="POST">
                                    <%//if (paidCnt>0) {%>
                                    <%if (!isSubDt)
                                    { /// IF SUBMISSION DATE GETS OVER%>
                                    <div class="t-align-center t_space">
                                        <label class='reqF_1'>Tender Closing Date and Time has been Lapsed </label>
                                    </div>


                                    <%} else
                                { /// IF SUBMISSION DATE LIVE %>
                                
                                    <div class="t-align-center t_space noprint">
                                        <%
                                            if (!btnFinallst.isEmpty())
                                            {
                                                if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("modify") || btnFinallst.get(0).getFieldName1().equalsIgnoreCase("Pending") || btnFinallst.get(0).getFieldName1() == null)
                                                {

                                        %>
                                        <label class="formBtn_1"><input name="btnFinalSub" type="Submit" value="Final Submission"/></label>
                                            <%                                                 } else if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission"))
                                            {
                                            %>
                                        <script type="text/javascript">
                                            $('#withmodifyDisp').show();
                                        </script>
                                        <br/>
                                        <b>Send OTP via</b>
                                        <input type="checkbox" name="chkSMSOTP" id ="chkSMSOTP" value="ON" onclick="uncheckEmail();"/>
                                        <b>SMS or </b>
                                        <input type="checkbox" name="chkEmailOTP" id ="chkEmailOTP" value="ON" onclick="uncheckSMS();"/>
                                        <b>Email</b>
                                        <br/><br/>
                                        <a href="BidWithdrawal.jsp?tenderid=<%=tenderId%>" class="anchorLink" onclick="return checkValidate()">Substitute / Modification</a>
                                        <a href="BidWithdrawal.jsp?bid=w&tenderid=<%=tenderId%>" class="anchorLink" onclick="return checkValidate()">Tender Withdrawal</a>
                                        <%}
                                        } else
                                        {%>
                                        <label class="formBtn_1 noprint">
                                            <input name="btnFinalSub" type="Submit" value="Final Submission"/></label>
                                            <%}%>

                                    </div>
                                    <% }//}%>

                                </form>
                            </div>
                            <% } //end isPDF %>
                        </div><%}%>

                    </div>
                    <% if (!(isPDF.equalsIgnoreCase("true")))
                    {%>
                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    <% }%>
                </div>
            </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        
        function uncheckEmail()
        {
          if (document.getElementById('chkSMSOTP').checked) 
          {
              document.getElementById("chkEmailOTP").checked = false;
          }
          
        }
        
        function uncheckSMS()
        {
          if (document.getElementById('chkEmailOTP').checked) 
          {
              document.getElementById("chkSMSOTP").checked = false;
          }
        }
        function checkValidate(){
                var check = false;
                if(document.getElementById("chkEmailOTP").checked || document.getElementById("chkSMSOTP").checked)
                {
                    check = true;
                    if(document.getElementById("chkEmailOTP").checked)
                    {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {jspOtpOption: 'email',funName: 'otpOptionSet'},
                        function (j)
                        {});
                    }
                    else if(document.getElementById("chkSMSOTP").checked)
                    {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {jspOtpOption: 'sms',funName: 'otpOptionSet'},
                        function (j)
                        {});
                    }
                    
                    
                } 
                else 
                {
                    jAlert("Please Select OTP option.", "Confirmation Alert");
                    check = false;
                }
             return check;
            }

    </script>  
</html>
