<%--
    Document   : CMSTab
    Created on : Jul 25, 2011, 3:00:46 PM
    Author     : shreyanshjogi
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"er
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Technical Sub Committee Formation Request</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<!--        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery.alerts.js" type="text/javascript"></script>-->
    </head>
    <body>

            <%
            int uTypeID = 0;
            if(session.getAttribute("userTypeId") != null){
                uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
            }
            String tabRecCla = "";

            String tsctab="";
            String tsctenderid="";
            String comType="";
            String ispost="";
            String tabComType=request.getParameter("comType");
            TenderCommonService tenderEvalCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            //out.println(pageContext.getAttribute("TSCtab"));
            if(pageContext.getAttribute("TSCtab") != null && !"null".equalsIgnoreCase((String)pageContext.getAttribute("TSCtab")) ){
                tsctab =   (String)pageContext.getAttribute("TSCtab");
            }

            if(request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid")) ){
                tsctenderid =   request.getParameter("tenderid");
            } else  if(request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId")) ){
                tsctenderid =   request.getParameter("tenderId");
            }
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
         List<Object[]> c_lists = commonService.getLotDetails(request.getParameter("tenderId"));
        ConsolodateService c_css = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        boolean C_flag = c_css.checkForItemFullyReceivedOrNot((Integer)c_lists.get(0)[0]);
        String contType = commonService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));



        %>
        <input type="hidden" value="<%=tabComType%>" id="tabComType"/>

<!--        END : SCRIPT TO HIDE OFFICER TAB PANEL-->

                <ul class="tabPanel_1 noprint">
                     <%if(procnature.equalsIgnoreCase("goods")){
                         if(C_flag){%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/repeatOrderMain.jsp?tenderId=<%=tsctenderid%>" <%if("6".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Repeat Order</a></li>
                        <%}}%>
                     <li class=""><a href="<%=request.getContextPath()%>/tenderer/CMSMain.jsp?tenderId=<%=tsctenderid%>" <%if("5".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Commencement Date</a></li>
                     <%if(procnature.equalsIgnoreCase("goods")){%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/CMS.jsp?tenderId=<%=tsctenderid%>" <%if("1".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Delivery Schedule</a></li>
                        <%}else if(procnature.equalsIgnoreCase("works")){%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/CMS.jsp?tenderId=<%=tsctenderid%>" <%if("1".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Work Program</a></li>
                        <%}else{%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/WorkScheduleMain.jsp?tenderId=<%=tsctenderid%>" <%if("1".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Work Schedule</a></li>
                        <%}%>
                         <%if(procnature.equalsIgnoreCase("services")){
                         if("Time based".equalsIgnoreCase(contType)){
                         %>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/ProgressReportMain.jsp?tenderId=<%=tsctenderid%>" <%if("2".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Progress Report</a></li>
                        <%}else{%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/SrvLumpSumPr.jsp?tenderId=<%=tsctenderid%>" <%if("2".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Progress Report</a></li>
                        <%}}else{%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/progressReport.jsp?tenderId=<%=tsctenderid%>" <%if("2".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Progress Report</a></li>
                        <%}%>
                        <%if(procnature.equalsIgnoreCase("services")){
     if("Time based".equalsIgnoreCase(contType)){
    %>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/InvoiceServiceCase.jsp?tenderId=<%=tsctenderid%>" <%if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Payment</a></li>
                        <%}else{%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/InvoiceServiceCase.jsp?tenderId=<%=tsctenderid%>" <%if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Payment</a></li>
<%}}else{%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/Invoice.jsp?tenderId=<%=tsctenderid%>" <%if("3".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Payment</a></li>
                        <%}%>
                        <li class=""><a href="<%=request.getContextPath()%>/tenderer/TabContractTermination.jsp?tenderId=<%=tsctenderid%>" <%if("4".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Contract Termination</a></li>
<!--                        <li class=""><a href="<%=request.getContextPath()%>/resources/common/PriDissForumTopicList.jsp?tenderId=<%=tsctenderid%>" <%if("7".equalsIgnoreCase(tsctab)){%>class="sMenu"<%}%>>Private Discussion Forum</a></li>-->
                </ul>


    </body>
</html>
