<%--
    Document   : TenderITTDashBoard
    Created on : 20-Nov-2010, 12:37:32 PM
    Author     : Yagnesh
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <%
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int tenderId = Integer.parseInt(request.getParameter("tenderId"));
        int tenderStdId= Integer.parseInt(request.getParameter("tenderStdId"));
        
        List<com.cptu.egp.eps.model.table.TblTenderIttHeader> subSectionDetail = createSubSectionSrBean.getSubSectionDetail(sectionId);
        String contentType = createSubSectionSrBean.getContectType(sectionId);
        
        int packageOrLotId = -1;
        if (request.getParameter("porlId") != null) {
            packageOrLotId = Integer.parseInt(request.getParameter("porlId"));
        }
        
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ITB Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
         <div class="dashboard_div">
             <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%}%>
            <div class="contentArea_1">
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                            <div class="t_space">
                                <div class="pageHead_1"><%if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Dashboard<span style="float:right;"><a href="TenderDocView.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%=packageOrLotId%>" class="action-button-goback">Go Back to Tender Document</a></span></div>
                            </div>
                            <%}%>

                            <%--<table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td>
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>" class="action-button-goback">Go Back to Dashboard</a>
                                    </td>
                                </tr>
                            </table>--%>
                <%   pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                            <form action="DefineSTDInDtl.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                for(int i=0;i<subSectionDetail.size();i++){
                                %>
                                    <tr>
                                        <td width="30%" align="center"><b>Sub Section No.</b></td>
                                        <td width="70%"><%=(i+1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name Of Sub Section</b></td>
                                        <td><%=subSectionDetail.get(i).getIttHeaderName() %></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td>
                                            <%
                                            if(!createSubSectionSrBean.clauseCreated(subSectionDetail.get(i).getTenderIttHeaderId())){
                                            %>
                                                <i>No Details Found</i>
                                            <%
                                            }else{
                                            %>
                                                <a href="<%=request.getContextPath()%>/tenderer/ViewTenderSectionClause.jsp?ittHeaderId=<%=subSectionDetail.get(i).getTenderIttHeaderId()%>&sectionId=<%=request.getParameter("sectionId")%>&tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%=packageOrLotId%>&noa=<%=request.getParameter("noa")%>">View</a>
                                            <%
                                            }
                                            %>


                                        </td>
                                    </tr>
                                <%
                                }
                                subSectionDetail=null;
                                createSubSectionSrBean=null;
                                %>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                </div>
                <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%}%>
            </div>
         </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>