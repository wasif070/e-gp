<%-- 
    Document   : PreTenTenderer
    Created on : Nov 27, 2010, 6:34:20 PM
    Author     : parag
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pre - Tender/Proposal Meeting</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>   
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
<body onload="getQueryData('getmyprebidquery');">
    <div class="mainDiv">
            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp"%>
<%
    boolean Result=false;
    String action="";
    String msg="";

    if (request.getParameter("flag") != null) {
        if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
            Result = true;
        }
    }
    
    if (request.getParameter("action") != null) {
        action = request.getParameter("action");
    }
    
    int userId=0;
    String preTenderId="";
    
    if(session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())){
        userId = Integer.parseInt(session.getAttribute("userId").toString());
    }
    if(request.getParameter("tenderId") != null){
        preTenderId = request.getParameter("tenderId");
    }
    
    
    preTendDtBean.setUserId(userId);
    preTendDtBean.setTenderId(Integer.parseInt(preTenderId));
    List<SPTenderCommonData>  getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate",Integer.parseInt(preTenderId),0);
    //List<SPTenderCommonData> postQueryData = preTendDtBean.getDataFromSP("getmyprebidquery",Integer.parseInt(preTenderId),userId);
    //List<SPTenderCommonData> postAllQueryData = preTendDtBean.getDataFromSP("GetAllPreBidQuery",Integer.parseInt(preTenderId),userId);

    String preBidStartDate="";
    String preBidEndDate="";
    java.util.Calendar currentDate = java.util.Calendar.getInstance();
    java.util.Date cdate = null;
    java.util.Date sdate = null;
    java.util.Date edate = null;
   // java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
    //String  queryDate= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date());
    
    SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
    if(getPreBidDates != null){
        if(getPreBidDates.size() > 0){
            if(getPreBidDates.get(0).getFieldName1() != null){
                preBidStartDate = getPreBidDates.get(0).getFieldName1();
                sdate = sd.parse(getPreBidDates.get(0).getFieldName1());
            }
            if(getPreBidDates.get(0).getFieldName2() != null){
                preBidEndDate = getPreBidDates.get(0).getFieldName2();
                edate = sd.parse(getPreBidDates.get(0).getFieldName2());
    
            }
            //cdate = sd.parse(currentDate.getTime().toString());
        }
    }
    

    boolean isPreBidPublished = preTendDtBean.isPrebidPublished(Integer.parseInt(preTenderId));

    if(Result){
       msg = " Query posted successfully.";
    }

   // java.util.Date d = new java.util.Date();
    

    /*java.util.Date today=new java.util.Date();
    Calendar calendar;

    calendar = Calendar.getInstance();
    calendar.setTime(sd.parse(getPreBidDates.get(0).getFieldName2()));
    calendar.add(Calendar.DATE, +2);
    */


%>
<%if(isPreBidPublished){%>
<script type="text/javascript">
function gettDocData(){
        $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=preTenderId%>,action:'docData',docAction:'ViewPreTenderMetsDocs'}, function(j){
            if(document.getElementById("doccData")){
                document.getElementById("doccData").innerHTML = j;
            }
        });
    }
    function downloadFile(docName,docSize){
        document.getElementById("downloadForm").action= "<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?docName="+docName+"&docSize="+docSize+"&tenderId="+<%=preTenderId%>+"&funName=download";
        document.getElementById("downloadForm").submit();
    }
</script>
    <%}%>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Pre - Tender/Proposal Meeting </div>
  
   <%
              // Variable tenderId is defined by u on ur current page.
             pageContext.setAttribute("tenderId",preTenderId);
  %>

    <%@include file="../resources/common/TenderInfoBar.jsp" %>
    <div>&nbsp;</div>
    <% pageContext.setAttribute("tab", 2);%>
   <%@include file="TendererTabPanel.jsp" %>
   <%if(!is_debared){%>
 <%--<%@include file="TendererTabPanel.jsp" %>--%>

  <table width="100%" cellspacing="0" class="tableList_1">
  	<tr>
    	<td colspan="4">
            <div class="t_space b_space">
                <%if (Result) {%>
                <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                <%} else {%>
                <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                <%}%>
            </div>
            <table width="100%" cellspacing="0" class="tableList_1">                
			  <tr>
                              <td width="20%" class="t-align-left"><strong>Meeting Start Date and Time :</strong></td>
				<td width="15%" class="t-align-left"><%=preBidStartDate %> </td>
				<td width="20%" class="t-align-left"><strong>Meeting End Date and Time :</strong></td>
				<td width="15%" class="t-align-left"><%=preBidEndDate%></td>
                               <%if(isPreBidPublished){%>
                                <td width="15%" class="t-align-left ff"><strong>View All Queries :</strong></td>
                              <td width="15%" class="t-align-left">
                                   
                                  <a href="<%=request.getContextPath()%>/resources/common/PreTenderQueRep.jsp?tenderId=<%=preTenderId%>&viewType=Prebid" target="_blank"  >View</a>
                              </td>
                              <%}else{%>
                              <td width="15%" class="t-align-left ff"><strong>Action :</strong></td>
                              <td width="15%"  align="right">
<%
                                    if(sdate != null)
                                    {
                                        if(sdate.before(currentDate.getTime()) && edate.after(currentDate.getTime()))
                                        {                                        
%>
                                            <a href="PostQuery.jsp?tenderId=<%=preTenderId%>">Post Query </a>
<%
                                        }
                                    }
%>
                            </td>
                            <%}%>
			  </tr>
			</table>

                <ul class="tabPanel_1 t_space">
                    <li><a href="javascript:void(0);" class="sMenu" id="myquery" onclick="getQueryData('getmyprebidquery');" >My Queries</a></li>
                    <li><a href="#" id="allquery"  onclick="getQueryData('GetAllPreBidQuery')" >All Queries</a></li>
                </ul>
                <table width="100%" cellspacing="0">
                    <tr><td>
                            <div id="queryData">

                            </div>
                        </td> </tr>
                    
               </table>
                          <%if(isPreBidPublished){%>
                          
                            <table width="100%" cellspacting ="0" class="t_space">
                            <tr>
                                <td align="left" class="ff">
                                    Pre Tender/Proposal Meeting Document :
                                </td>
                            </tr>   
                            <tr class="t_space">
                                <td>
                                    <div id="doccData">
                                        </div>
                                </td>
                            </tr>
                            </table>
    <script type="text/javascript">
        gettDocData();
    </script>
<form id="downloadForm" method="post">
</form>
<%}%>

      </td>
    </tr>
  </table>
  <%}%>
<div>&nbsp;</div>
</div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    <!--Dashboard Footer End-->
</div>
            </div>
    </div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>

</html>
<script type="text/javascript">

    function forTabDisplay(str)
    {
        if(str == "myqury"){
            document.getElementById("myquery").className ="sMenu" ;
             document.getElementById("allquery").className="";
            $('#tr0').show();
             $('#tr1').hide();
        }else{
            document.getElementById("allquery").className ="sMenu" ;
            document.getElementById("myquery").className ="" ;
            $('#tr1').show();
             $('#tr0').hide();
        }
    }

     function getQueryData(queryData){
          $('#queryData').html(' ');
          $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=preTenderId%>,action:'getQueryData',queryAction:queryData}, function(j){
              document.getElementById("queryData").innerHTML = j;
          });
          if(queryData == "getmyprebidquery"){
              document.getElementById("myquery").className ="sMenu" ;
              document.getElementById("allquery").className="";
          }else{
              document.getElementById("myquery").className ="" ;
              document.getElementById("allquery").className="sMenu";
          }
    }

        getQueryData('getmyprebidquery');

   <%-- $(document).ready(function() {
    $('#queryData').html(' ');
     $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=preTenderId%>,action:'getQueryData',queryAction:'getmyprebidquery'}, function(j){
        // alert(j);
             document.getElementById("queryData").innerHTML = j;
      });
  });--%>

      <%--$.ajax({
        url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=preTenderId%>&action=getQueryData&queryAction="+queryData,
        method: 'POST',
        async: true,
        success: function(j) {
            document.getElementById("queryData").innerHTML = j;
        }
     });--%>

</script>
<%   
    preTendDtBean = null;
%>

