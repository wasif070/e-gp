<%--
    Document   : DebarmentClarification
    Created on : Jan 10, 2010, 10:04:21 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDocs"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentResp"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentReq"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Debarment Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script>
            function validate(){
                $(".err").remove();
                if($.trim($("#response").val()) == ""){
                    $("#response").parent().append("<div class='err' style='color:red;'>Please enter your Response</div>");
                    return false;
                }                                
                if(!CompareToForGreater($('#accDate').val(),$('#currDate').val())){
                    $("#currDate").parent().append("<div class='err' style='color:red;'>Last response date is over</div>");
                    return false;
                }                
            }
            function validateFile(){
               var bool=true;
               $(".err").remove();
               if($("#uploadDocFile").val()=="") {
                    $("#uploadDocFile").parent().append("<div class='err' style='color:red;'>Please select File.</div>");
                        bool=false;
                }
                if($.trim($("#txtDescription").val())=="") {
                    $("#txtDescription").parent().append("<div class='err' style='color:red;'>Please enter Description.</div>");
                        bool=false;
                }
                var count = 0;
                var browserName=""
                var maxSize = parseInt($('#fileSize').val())*1024*1024;
                var actSize = 0;
                var fileName = "";
                jQuery.each(jQuery.browser, function(i, val) {
                     browserName+=i;
                });
                $(":input[type='file']").each(function(){
                    if(browserName.indexOf("mozilla", 0)!=-1){
                        actSize = this.files[0].size;
                        fileName = this.files[0].name;
                    }else{
                        var file = this;
                        var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                        var filepath = file.value;
                        var thefile = myFSO.getFile(filepath);
                        actSize = thefile.size;
                        fileName = thefile.name;
                    }
                    if(parseInt(actSize)==0){
                        $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                        count++;
                    }
                    if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                        $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                        count++;
                    }
                    if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                        $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                        count++;
                    }
                });
                if(count!=0){
                   bool = false;
                } 
                if(bool){
                  $('#btnUpload').attr("disabled", "disabled");
                }
                return bool;
            }
            function CompareToForGreater(value,params)
                {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) >= Date.parse(datep);
            }
        </script>
    </head>
    <body>
        <%
                    String debarId= request.getParameter("debId");
                    String status= request.getParameter("stat");
                    InitDebarmentSrBean srBean = new InitDebarmentSrBean();
                    List<Object[]> list = srBean.findDebarmentReqs(session.getAttribute("userId").toString(),debarId);
                    List<TblDebarmentResp> userResponse = srBean.findDebarResponse("userId",Operation_enum.EQ,Integer.parseInt(session.getAttribute("userId").toString()),"tblDebarmentReq",Operation_enum.EQ,new TblDebarmentReq(Integer.parseInt(debarId)));
                    if(!userResponse.isEmpty()){
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "Debarment Clarification Viewed by Tenderer", "");
                    }
                    List<TblDebarmentTypes> debarTypes = srBean.getAllDebars();
                    List<Object[]> hopeAns = null;
                    String data1 = null;
                    String debarTypeId=null;
                    Object[] allIds =  srBean.findPETenderHopeId(debarId);
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Content Part Start-->
        <div class="pageHead_1">Debarment Clarification<span style="float: right;"><a class="action-button-goback" href="TendererDebarListing.jsp<%if("y".equals(request.getParameter("isprocess"))){out.print("?isprocess="+request.getParameter("isprocess"));}%>">Go Back</a></span></div>
         <%
          if(request.getParameter("fq")!=null){out.print("<br/><div class='responseMsg errorMsg'>"+request.getParameter("fq")+"</div>");}
          if(request.getParameter("fs")!=null){out.print("<br/><div class='responseMsg errorMsg'>Max file size of a single file must not exceed "+request.getParameter("fs")+" MB.</div>");}
          if(request.getParameter("ft")!=null){out.print("<br/><div class='responseMsg errorMsg'>Acceptable file types "+request.getParameter("ft")+".</div>");}
          if(request.getParameter("sf")!=null){out.print("<br/><div class='responseMsg errorMsg'>File already exist</div>");}
        %>
        <%for (Object[] data : list) {%>
        <form name ="frm" action="<%=request.getContextPath()%>/InitDebarment" method="post">
            <input type="hidden" value="bidderResponse" name="action" id="action" />
            <input type="hidden" value="<%=data[0]%>" name="debarmentId" id="debarmentId" />
            <input type="hidden" value="<%if(data[4].toString().equals("-")){out.print(data[5]+" "+data[6]);}else{out.print(data[4]);}%>" name="cmpName"/>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td width="16%" class="t-align-left ff">Company Name :</td>
                    <td width="84%" class="t-align-left"><%if(data[4].toString().equals("-")){out.print(data[5]+" "+data[6]);}else{out.print(data[4]);}%></td>
                </tr>
                <tr>
                    <td valign="top" class="t-align-left ff">Clarification :</td>
                    <td width="84%" class="t-align-left"><%=data[1]%></td>
                </tr>
                <tr>
                    <td class="t-align-left ff">Last  Date for Response :</td>
                    <td class="t-align-left">
                        <%=DateUtils.formatStdDate((Date)data[2])%>
                        <input type="hidden" value="<%=DateUtils.formatStdDate((Date)data[2])%>" id="accDate"/>
                        <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" id="currDate"/>
                    </td>
                </tr>
                <tr id="trVal">
                    <td class="t-align-left ff">Debarment Type :</td>
                    <td class="t-align-left">
                    <%
                        if(!allIds[2].toString().equals("0")){
                            if("pesatisfy".equalsIgnoreCase(status)){
                               hopeAns =  srBean.findPeSatisfy(debarId);
                            }else if("sendtohope".equalsIgnoreCase(status)){
                               hopeAns =  srBean.findSendHopeAns(debarId);
                            }else{
                                hopeAns = srBean.findHopeAns(debarId);
                           }
                        }else{
                           data1 = allIds[3].toString();
                        }
                        for (TblDebarmentTypes types : debarTypes) {
                            if(hopeAns==null || hopeAns.isEmpty()){
                                 if(Integer.parseInt(data1)==types.getDebarTypeId()){
                                     debarTypeId = data1;
                                     out.print(types.getDebarType());
                                 }
                            }else{
                                if(Integer.parseInt(hopeAns.get(0)[4].toString())==types.getDebarTypeId()){
                                 debarTypeId = hopeAns.get(0)[4].toString();
                                 out.print(types.getDebarType());
                                }
                            }

                         }
                    %>
                    </td>
                </tr>
                <%if(!"6".equals(debarTypeId)){%>
                <tr id="trVal">
                <td class="t-align-left ff">&nbsp;</td>
                <td class="t-align-left">
                            <%
                                String hopePEId = null;
                                String condText=null;
                                if(allIds[2].toString().equals("0")){
                                    condText="bype";
                                    hopePEId = allIds[1].toString();
                                }else{
                                    if("pesatisfy".equalsIgnoreCase(status) || "sendtohope".equalsIgnoreCase(status)){
                                         condText="bype";
                                        hopePEId = allIds[1].toString();
                                    }else{
                                        condText="byhope";
                                        hopePEId = allIds[2].toString();
                                   }
                                }
                                List<TblDebarmentDetails> dataList = srBean.getDebarDetailsForHope("tblDebarmentReq",Operation_enum.EQ,new TblDebarmentReq(Integer.parseInt(debarId)),"debarStatus",Operation_enum.EQ,condText);
                                String[] debarIds = dataList.get(0).getDebarIds().split(",");
                                String pageHead1=null;
                                String pageHead2=null;
                                boolean isTenderDeb = false;
                                if(!allIds[2].toString().equals("0")){
                                    if(hopeAns.get(0)[4].toString().equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(hopeAns.get(0)[4].toString().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(hopeAns.get(0)[4].toString().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }else{
                                    if(data1.equals("1")){
                                    pageHead1="Ref No.";
                                    pageHead2="Tender/Proposal Brief";
                                    isTenderDeb = true;
                                    }else if(data1.equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(data1.equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(data1.equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(data1.equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }
                            %>
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <%if(isTenderDeb){%>
                                    <th class='t-align-center'>Tender ID</th>
                                    <%}%>
                                    <th class='t-align-center' width='30%'><label id='dlbHead1'><%=pageHead1%></label></th>
                                    <th class='t-align-center' width='50%'><label id='dlbHead2'><%=pageHead2%></label></th>
                                </tr>
                                <tbody id="tbodyVal">
                                    <%
                                        List<SPTenderCommonData> listChk = srBean.searchDataForDebarType(debarTypeId,hopePEId);
                                        for(int j=0; j<debarIds.length;j++){
                                            for(SPTenderCommonData sptcd : listChk){
                                                if(sptcd.getFieldName1().equals(debarIds[j])){
                                                    out.print("<tr>");
                                                    String viewLink = null;
                                                    if(isTenderDeb){
                                                        out.print("<td>"+sptcd.getFieldName1()+"</td>");
                                                        viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + sptcd.getFieldName1() +"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + sptcd.getFieldName3() + "</a>";
                                                    }else{
                                                        viewLink = sptcd.getFieldName3();
                                                    }
                                                    out.print("<td><input type='hidden' value='"+sptcd.getFieldName1()+"' id='debId"+j+"'/>"+sptcd.getFieldName2()+"</td><td>"+viewLink+"</td>");
                                                    out.print("</tr>");
                                                }
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>
                        </td>
                 </tr>
                 <%}%>
                 <%                        
                        List<TblDebarmentDocs>  debarmentDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(data[7].toString()));
                        if(!debarmentDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>                 
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by PA</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : debarmentDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=data[7]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                <tr>
                    <td valign="top" class="t-align-left ff">Response : <%if(userResponse.isEmpty()){out.print("<span class='mandatory'>*</span>");}%></td>
                    <td class="t-align-left">
                        <%if(userResponse.isEmpty()){%>
                        <textarea rows="5" id="response" name="response" class="formTxtBox_1" style="width:99%;"></textarea>
                        <%}else{out.print(userResponse.get(0).getResponseTxt());}%>
                    </td>
                </tr>                
            </table>
            <%if(userResponse.isEmpty()){%>
            <div class="t-align-center t_space">
                <label class="formBtn_1"><input type="submit" name="button3" id="button3" value="Submit" onclick="return validate();" /></label>
            </div>
            <%}%>
        </form>
            <%
                if(userResponse.isEmpty()){
                out.print("<div style='color: red;font-weight: bold;'>Note : Please ensure uploading reference documents if any before clicking on Submit to send your clarification to PA.</div>");
                CheckExtension ext = new CheckExtension();
                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                byte fileSize = configurationMaster.getFileSize();
            %>
            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/InitDebarment?action=debarDocUpload&debId=<%=debarId%>&user=tend&stat=<%=status%>" enctype="multipart/form-data" name="frmUploadDoc" onsubmit="return validateFile();">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td class="ff" width="15%"> Select Document   : <span class="mandatory">*</span></td>
                        <td width="85%">
                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/><br/>
                            Acceptable File Types <span class="formNoteTxt">(<%out.print(configurationMaster.getAllowedExtension().replace(",", ", "));%>)</span>
                            <br/>Maximum file size of single file should not exceed <%out.print(fileSize);%>MB.
                            <input type="hidden" value="<%=fileSize%>" id="fileSize"/>
                            <div id="dvUploadFileErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Description : <span class="mandatory">*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="txtDescription" style="width:200px;"/>
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="formBtn_1" style="visibility:visible;" id="lbUpload">
                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </form>
            <%   
                }
                List<TblDebarmentDocs>  tenderDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(session.getAttribute("userId").toString()));
                if(!tenderDocses.isEmpty()){
            %>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="5" class="t-align-left">Reference documents by Bidder/Consultant</th>
                    </tr>
                    <tr>
                        <th class="t-align-center">Sl. No.</th>
                        <th class="t-align-center">Document Name</th>
                        <th class="t-align-center">Document Description</th>
                        <th class="t-align-center">File Size (In KB)</th>
                        <th class="t-align-center">Action</th>
                    </tr>
                    <%
                                int count = 1;
                                for (TblDebarmentDocs ttcd : tenderDocses) {
                    %>
                    <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                        <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>&uTid=2" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                            <%if(userResponse.isEmpty()){%>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/InitDebarment?docId=<%=ttcd.getDebarDocId()%>&docName=<%=ttcd.getDocumentName()%>&action=debarDocDelete&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>
                    <%count++;}%>
           </table>
        <%}}%>
        <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
