<%@page import="com.lowagie.text.Document"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Enable Java Script in your Browser</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        
        <%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
%>

<%
String currValue = (String) request.getHeader("User-Agent")+ " ";
//out.println(" Browser Info " + currValue +" " + "<br/>");
            String browser = new String("");
            String version = new String("");
            if (currValue != null) {

                if ((currValue.indexOf("Chrome") != -1)) {
                    browser = "Google";
                    String tempStr = currValue.substring(currValue.indexOf("Chrome"), currValue.length());
                    version = tempStr.substring(0, tempStr.indexOf("Safari"));

                } else if ((currValue.indexOf("Firefox") != -1)) {
                    browser = "Mozilla";
                    String tempStr = currValue.substring(currValue.indexOf("Firefox"), currValue.length());
                    version = tempStr.substring(0, tempStr.indexOf(" "));

                } else {
                    browser = "IE";
                    String tempStr = currValue.substring(currValue.indexOf("MSIE"), currValue.length());
                    version = tempStr.substring(4, tempStr.indexOf(";"));
}

}
//out.println(" now browser type is " + browser +" " + version);
%>

<!--        <script type="text/javascript">
            $(document).ready(function(){                 
                jQuery.each(jQuery.browser, function(i, val) {
                    browserVer = jQuery.browser.version;                   
                    
                    if(i == "mozilla" && browserVer.substr(0,3) == "1.9"){
                        document.getElementById("browserversion").innerHTML= i +" "+ nomVersionNavig();
                        document.getElementById("browserversion1").innerHTML= i +" "+ nomVersionNavig();
                        $('#IE8').hide();
                        $('#IE9').hide();

                    }
                    if(i == "msie" && (browserVer.substr(0,3) == "8.0" || browserVer.substr(0,3) == "9.0")){
                        document.getElementById("browserversion").innerHTML= i +" "+ browserVer.substr(0,3);
                        document.getElementById("browserversion1").innerHTML= i +" "+ browserVer.substr(0,3);
                        $('#mozilla').hide();
                        if(browserVer.substr(0,3) == "8.0"){
                            $('#IE9').hide();
                        }else
                        {
                            $('#IE8').hide();
                        }
                    }
                });
            });
        </script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->               
                <!--Middle Content Table-->
                <div  class="contentArea_1" align="center">
                    <table border=0 cellSpacing=7 cellPadding=0
                           width="90%" class="instructionTable" >

                        <tr>
                            <td align="center">
                                <div>    <b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan</b></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="Index.jsp"><img src="<%=request.getContextPath()%>/resources/images/cptuEgpLogo.gif" alt="" width="160" height="71"></a>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>   <strong> Government Procurement and Property Management Division (GPPMD)</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>    Department of National Properties, Ministry of Finance, Royal Government of Bhutan</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div style="color:#FF9326;"><br/>  <strong>  You are using <%= browser +" " + version %> browser.</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div style="color:#FF9326;font-size: medium;">    <strong>Enable JavaScript in Your Browser</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    Dear User,<br/><br/>
                                    Safety and security of your information and transactions, integrity of your data, and availability of complete e-GP System functionality is of prime importance for carrying out procurement processes through e-GP System and one of the requirement for this is JavaScript enabled browser.  Currently JavaScript is not enabled in your browser and request you to enable the same.
                                    <br/><br/>
                                    To enable JavaScript in your browser please follow below mentioned instructions.
                                    <br/><br/>You are using <%= browser +" " + version %> browser.

                                </div><br />
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1" vAlign=top>

                                <%                                    
                                   if (browser.equalsIgnoreCase("IE")) {
                                   if (version.contains("8.0")) {                                      
    %>
                                <div class="listBox_aboutUs" id="IE8">
                                    <div class="instructionTitle">
                                        Internet Explorer 8.x<br/><br/>
                                    </div>
                                    <div class="instructionTitle">
                                        <i>Follow instructions below OR <a target="blank" href="<%= request.getContextPath()%>/help/jsenable/eGP_JSEnable_IE8.pdf">Click here</a> to download instructions with screenshots (Requires PDF Viewer)</i><br/><br/>
                                    </div>
                                    <ul style="margin-top:2px;margin-left: 30px;" class="atxt_1">
                                        <b>Step 1:</b> Open Internet Explorer 8.x<br/>
                                        <b>Step 2:</b> Go to "<b>Tools</b>" menu<br/>
                                        <b>Step 3:</b> Select "<b>Internet Options</b>"<br/>
                                        <b>Step 4:</b> "<b>Internet Options</b>" screen appears<br/>
                                        <b>Step 5:</b> Select the "<b>Security</b>" tab<br/>
                                        <b>Step 6:</b> Click "<b>Custom level</b>" button at bottom<br/>
                                        <b>Step 7:</b> Then "<b>Security Settings</b>" dialog box will pop up<br/>
                                        <b>Step 8:</b> Scroll down to "<b>Scripting</b>" Category<br/>
                                        <b>Step 9:</b> Click on "<b>Enable</b>" under "<b>Active scripting</b>"<br/>
                                        <b>Step 10:</b> Click "<b>Ok</b>"<br/>
                                        <b>Step 11:</b> Click "<b>Yes</b>"<br/>
                                        <b>Step 12:</b> Click "<b>Ok</b>"<br/>
                                        <b>Step 13:</b> <a href="<%= request.getContextPath()%>/Index.jsp"><b><i>Click here</i></b></a> to go to Home page.<br/>

                                    </ul>

                                </div>
                                    <%}}%>
                                <%--<div align=right>
                                    <a href="#">Back
                                        To Top</a>
                                </div>

                                --%>
                                  <%  if (browser.equalsIgnoreCase("IE")) {
                                   if (version.contains("9.0")) {
    %>
                                <div class="listBox_aboutUs" id="IE9">
                                    <div class="instructionTitle">
                                        Internet Explorer 9.x<br/><br/>
                                    </div>
                                    <div class="instructionTitle">
                                        <i>Follow instructions below OR <a target="blank" href="<%= request.getContextPath()%>/help/jsenable/eGP_JSEnable_IE9.pdf">Click here</a> to download instructions with screenshots (Requires PDF Viewer)</i><br/><br/>
                                    </div>
                                    <ul style="margin-top:2px;margin-left: 30px;" class="atxt_1">
                                        <b>Step 1:</b> Open Internet Explorer 9.x<br/>
                                        <b>Step 2:</b> Go to "<b>Tools</b>" menu<br/>
                                        <b>Step 3:</b> Select "<b>Internet Options</b>"<br/>
                                        <b>Step 4:</b> "<b>Internet Options</b>" screen appears<br/>
                                        <b>Step 5:</b> Select the "<b>Security</b>" tab<br/>
                                        <b>Step 6:</b> Click "<b>Custom level</b>" button at bottom<br/>
                                        <b>Step 7:</b> Then "<b>Security Settings</b>" dialog box will pop up<br/>
                                        <b>Step 8:</b> Scroll down to "<b>Scripting</b>" Category<br/>
                                        <b>Step 9:</b> Click on "<b>Enable</b>" under "<b>Active scripting</b>"<br/>
                                        <b>Step 10:</b> Click "<b>Ok</b>"<br/>
                                        <b>Step 11:</b> Click "<b>Yes</b>"<br/>
                                        <b>Step 12:</b> Click "<b>Ok</b>"<br/>
                                        <b>Step 13:</b> <a href="<%= request.getContextPath()%>/Index.jsp"><b><i>Click here</i></b></a> to go to Home page.<br/>
                                    </ul>

                                </div>
                                    <%}}%>
                                <%--<div align=right>
                                    <a href="#">Back
                                        To Top</a>
                                </div>

                                --%>
                                <% if (browser =="Mozilla"){
                                    if (version.contains("3.6")) {
    %>
                                <div class="listBox_aboutUs" id="mozilla" >
                                    <div class="instructionTitle">
                                        Mozilla Firefox<br/><br/>
                                    </div>
                                    <div class="instructionTitle">
                                        <i>Follow instructions below OR <a target="blank" href="<%= request.getContextPath()%>/help/jsenable/eGP_JSEnable_MF 3.6.pdf">Click here</a> to download instructions with screenshots (Requires PDF Viewer)</i><br/><br/>
                                    </div>
                                    <ul style="margin-top:2px;margin-left: 30px;" class="atxt_1">
                                        <b>Step 1:</b> Open Mozilla Firefox 3.6x<br/>
                                        <b>Step 2:</b> Click on "<b>Tools</b>" <br/>
                                        <b>Step 3:</b> Click on "<b>Options</b>" from the drop-down menu<br/>
                                        <b>Step 4:</b> Click on "<b>Content</b>" tab<br/>
                                        <b>Step 5:</b> Click on "<b>Enable JavaScript</b>"<br/>
                                        <b>Step 6:</b> Click "<b>Ok</b>"<br/>
                                        <b>Step 7:</b> <a href="<%= request.getContextPath()%>/Index.jsp"><b><i>Click here</i></b></a> to go to Home page.<br/>

                                    </ul>

                                </div>
                                    <%}}%>
                                <%--<div align=right>
                                    <a href="#">Back
                                        To Top</a>
                                </div>

                                --%>

                                
                                Start using e-GP System.<br/><br />
                                Please note that GPPMD is not responsible for any issues or problems resulting from use of browsers where JavaScript is not enabled.
                            </td></tr>
                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>Thank You for your support.</strong><br/><br/></td></tr>
                        <tr><td align="center"><img alt="" src="<%=request.getContextPath()%>/resources/images/cptuLogo.jpg" width="435" height="37"></td>
                        </tr>
                        <tr><td align="center">Box 116, Thimphu, Bhutan<br/><br/>
                                <b>Phone</b>: +975-02-336962 |   <b>Fax</b>: +975-02-336961 | <b>Email</b>: <a href="mailto:info.bhutan@dohatec.com.bd" >info.bhutan@dohatec.com.bd</a>  | <b>Web</b>: <a href="http://www.pppd.gov.bt/" >www.pppd.gov.bt</a>
                            </td></tr>
                        <tr>
                            <td class="headerCss"></td>
                        </tr>
                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>e-GP Support Contact Details </strong><br/></td></tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1" style="text-align: center;">
                                    <b>Phone</b>: +880-2-9144 252/53 | <b>Email</b>: <a href="mailto:helpdesk@eprocure.gov.bd" >helpdesk@eprocure.gov.bd</a>
                                </div><br />
                            </td>
                        </tr>

                    </table>
                </div>
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
