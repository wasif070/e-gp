<%-- 
    Document   : OnlinePmtForDocFees
    Created on : Feb 16, 2012, 11:54:32 AM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <title>Online Tender Document Fee - Pay Online Tender Document Fee</title>
    </head>
    <%
                UserRegisterService userRegisterServiceNavigate = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                if (session.getAttribute("payment") == null) {
                    TenderCommonService tenderCommonServicePmt = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    // List<SPTenderCommonData> lstTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tenderId, userId);
                    List<SPTenderCommonData> lstTenderInfo = tenderCommonServicePmt.returndata("getTenderInfoForDocView", request.getParameter("tenderId"), session.getAttribute("userId").toString());
                    String amt = "";
                    if (!lstTenderInfo.isEmpty() && lstTenderInfo.size() > 0) {
                        //docFeesMode = lstTenderInfo.get(0).getFieldName2();
                        if ("Paid".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())) {
                            if (lstTenderInfo.get(0).getFieldName3().equalsIgnoreCase("package")) {
                                amt = lstTenderInfo.get(0).getFieldName6();
                            } else if (lstTenderInfo.get(0).getFieldName3().equalsIgnoreCase("lot")) {
                                amt = lstTenderInfo.get(0).getFieldName9();
                            }
                        }
                    }
                    Object[] paymentNavigateObj = userRegisterServiceNavigate.getLoginDetails(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                    if (paymentNavigateObj != null) {
                        OnlinePaymentDtBean onpdb = new OnlinePaymentDtBean(String.valueOf(paymentNavigateObj[0]), amt, "docFees", request.getParameter("tenderId"), XMLReader.getMessage("ServiceCharge"));
                        session.setAttribute("payment", onpdb);
                        onpdb = null;
                        paymentNavigateObj = null;
                    }
                }
    %>
    <body>
        <div class="dashboard_div">
            <div class="mainDiv">
                <div class="fixDiv">
                    <%@include  file="resources/common/AfterLoginTop.jsp" %>

                    <div class="pageHead_1">Online Tender Document Fee - Pay Online Tender Document Fee
                        <span style="float:right;"><a class="action-button-goback" href="/tenderer/LotPckDocs.jsp?tab=1&tenderId=<%=request.getParameter("tenderId")%>">Go Back</a></span>
                    </div>
                    <div>&nbsp;</div>
                    <div class="contentArea_1">
                        <%
                        
                        OnlinePaymentDtBean opdb = (OnlinePaymentDtBean) session.getAttribute("payment");
                        if(request.getParameter("tenderId") != null)
                            {
                         pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                         } else{
                         pageContext.setAttribute("tenderId", opdb.getTenderId());
                    }
                         %>
                        <%@include file="resources/common/TenderInfoBar.jsp" %>
                          <%          
                                    if (opdb != null) {
                                        System.out.println(opdb.getCardNumber());
                                        if (opdb.getResponse() != null && ((opdb.getResponse().indexOf("000") > 0 && opdb.getResponse().indexOf("OK") > 0) || opdb.getResponse().indexOf("<txn_status>ACCEPTED</txn_status>") > 0)) {
                        %>
                        <jsp:include page="resources/common/OnlinePaymentSuc.jsp" ></jsp:include>
                        <%} else {%>
                        <jsp:include page="resources/common/OnlinePayment.jsp" ></jsp:include>
                        <%}
                                    }%>
                    </div>
                </div>
            </div>
        </div>
        <div align="center">
            <%@include file="resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
