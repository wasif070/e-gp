<%--
    Document   : IndividualConsultant
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tendererMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Bidder Registration : Individual Consultant Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" /> 
        <link href="resources/js/poshytipLib/tip-yellowsimple/tip-yellowsimple.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>  
        <script src="resources/js/poshytipLib/jquery.poshytip.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.formTxtBox_1').poshytip({
                    className: 'tip-yellowsimple',
                    showOn: 'focus',
                    alignTo: 'target',
                    alignX: 'right',
                    alignY: 'center',
                    offsetX: 5,
                    showTimeout: 100
                });
                $("#frmSignup").validate({
                    rules: {
                        title: {required: false},
                        country: {required: true},
                        state: {required: true},
                        firstName: {required: true, FirstLastName: true, maxlength: 50},
                        middleName: {maxlength: 50, FirstLastName: true},
                        lasatName: { /*required: true,*/ FirstLastName: true, maxlength: 50},
                        nationalIdNo: {required: true, alphaNationalId: true, maxlength: 25},
                        address1: {required: true, maxlength: 250},
                        address2: {/* required: true,*/maxlength: 250},
                        city: { /*required: true,*/maxlength: 50, DigAndNum: true},
//                        upJilla: { maxlength: 50,alphaNationalId:true},
                        postcode: { /*required: true,*/number: true, minlength: 4},
                        phoneNo: {/*required: true,*/ number: true, maxlength: 20},
                        phoneSTD: {/*required: true,*/number: true, maxlength: 10},
                        faxNo: {number: true, maxlength: 20},
                        faxSTD: {number: true, maxlength: 10},
                        mobileNo: {required: true, number: true},
                        specialization: {required: true},
                        website: {url: true}
                    },
                    messages: {
//                        title: { required: "<div class='reqF_1'>Please select Title</div>" },
                        country: {required: "<div class='reqF_1'>Please select Country</div>"},
                        state: {required: "<div class='reqF_1'>Please enter Dzongkhag / District Name</div>"},
                        firstName: {
                            required: "<div class='reqF_1'>Please enter  First Name</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        middleName: {
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>"
                        },
                        lasatName: {
                            required: "<div class='reqF_1'>Please enter Last Name</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        nationalIdNo: {
                            required: "<div class='reqF_1'>Please enter National ID / Passport No. / Driving License No.</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        address1: {
                            required: "<div class='reqF_1'>Please enter Address Line 1</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        address2: {
                            //required: "<div class='reqF_1'>Please enter Address Line 2</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        city: {
                            required: "<div class='reqF_1'>Please select City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only.</div>"
                        },
//                        upJilla: {
//                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
//                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
//                        },
                        postcode: {
                            /*required: "<div class='reqF_1'>Please enter Postcode /Zip Code</div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            maxlength: "<div class='reqF_1'>Minimum 4 characters are required</div>"
                        },
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>"
                        },
                        phoneSTD: {
//                            required: "<div class='reqF_1'>Please enter Area Code</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 digits are allowed</div>"
                        },
                        faxNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>"
                        },
                        faxSTD: {
                            number: "<div class='reqF_1'>Please enter numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 digits are allowed"
                        },
                        mobileNo: {required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>"
                        },
                        specialization: {
                            required: "<div class='reqF_1'>Please select Specialization</div>"
                        },
                        website: {
                            url: "<div class='reqF_1'>Please enter valid Website Name</div>"
                        }
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "specialization") {
                            error.insertAfter("#aTree");
                        } else if (element.attr("name") == "faxSTD") {
                            error.insertAfter("#txtFax");
                        } else if (element.attr("name") == "phoneSTD") {
                            error.insertAfter("#txtPhone");
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            });

            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbSubDistrict').change(function () {
                    if ($('#cmbSubDistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    } else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    }
                });
            });
        </script>
    </head>
    <%
        tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        boolean ifEdit = false;
        TblTempTendererMaster dtBean = tendererMasterSrBean.getTendererDetails(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
        if (dtBean != null) {
            ifEdit = true;
        }
        String msg = null;
    %>
    <body onload="SubDistrictANDAreaCodeLoad();<%if (ifEdit) {%>TitleChange('<%=dtBean.getTitle()%>') <% } else { %>TitleChange('') <%}%>">
        <%

            if ("Save".equals(request.getParameter("hdnbutton"))) {
                /*if (tendererMasterDtBean.getMobileNo() != null && tendererMasterDtBean.getNationalIdNo() != null) {
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='" + tendererMasterDtBean.getMobileNo() + "'") != 0) {
                                msg = "Mobile no. already exist";
                            } else {
                                if(false){
                                //if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='" + tendererMasterDtBean.getNationalIdNo() + "'") != 0) {
                                    msg = "National ID./Passport No./Driving License No. already exist";
                                } else {*/
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                    tendererMasterDtBean.setSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                    tendererMasterDtBean.setUpJilla("");
                }
                if (tendererMasterSrBean.registerIndividualClient(tendererMasterDtBean, Integer.parseInt(session.getAttribute("userId").toString()))) {
                    response.sendRedirect("SupportingDocuments.jsp");
                } else {
                    response.sendRedirect("IndividualConsultant.jsp?msg=error");
                }

                /*}
                            }
                        }*/
            }
            if ("Update".equals(request.getParameter("hdnedit"))) {
                /*if (tendererMasterDtBean.getMobileNo() != null && tendererMasterDtBean.getNationalIdNo() != null) {
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='" + tendererMasterDtBean.getMobileNo() + "'") != 0) {
                                msg = "Mobile no. already exist";
                            } else {
                                if(false){
                                //if (userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='" + tendererMasterDtBean.getNationalIdNo() + "'") != 0) {
                                    msg = "National ID./Passport No./Driving License No. already exist";
                                } else {*/
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                    tendererMasterDtBean.setSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                    tendererMasterDtBean.setUpJilla("");
                }
                tendererMasterDtBean.setTendererId(dtBean.getTendererId());
                tendererMasterDtBean.setCmpId(dtBean.getTblTempCompanyMaster().getCompanyId());
                tendererMasterDtBean.setUserId(dtBean.getTblLoginMaster().getUserId());
                tendererMasterDtBean.setIsAdmin(dtBean.getIsAdmin());
                if (tendererMasterSrBean.updateIndividualClient(tendererMasterDtBean, true)) {
                    response.sendRedirect("IndividualConsultant.jsp?msg=s");
                } else {
                    msg = "Error in Updating IndividualsDetail";
                }
                /*}
                            }
                        }*/
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->                
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">

                            <td class="contentArea_1">
                                <!--Page Content Start-->
                                <div class="pageHead_1">Bidder Registration - Individual Consultant</div>
                            <jsp:include page="EditNavigation.jsp" ></jsp:include>
                            <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                            <%if (msg != null) {%><br/><div class="responseMsg errorMsg"><%=msg%>.</div><%}%><br/>
                            <%if ("error".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg errorMsg"  >Problem in inserting Individual Consultant's Information.</div><%}%>
                            <form id="frmSignup" name="signupForm" method="post">

                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td style="font-style: italic;" class="ff" align="left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="ff">Title : <span>*</span></td>
                                        <td width="75%"><select name="title" class="formTxtBox_1" id="cmbTitle" style="width: 100px;" <%if (ifEdit) {%>onchange="TitleChange('<%=dtBean.getTitle()%>')" <% } else { %>onchange="TitleChange('')" <%}%> >
                                                <%if (!ifEdit) {%><option value="">Select</option><%}%>
                                                        <option value="Mr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                            }%>>Mr.</option>
                                                        <option value="Ms." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Ms.")) {%>selected<%}
                                                            }%>>Ms.</option>
                                                        <option value="Mrs." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mrs.")) {%>selected<%}
                                                            }%>>Mrs.</option>
                                                <option value="Dasho" <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dasho")) {%>selected<%}
                                                                    }%>>Dasho</option>
                                                <option value="Dr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dr.")) {%>selected<%}
                                                                    }%>>Dr.</option>
                                                <option id="OtherTitle" value="Other" <%if (ifEdit) {
                                                                if (!dtBean.getTitle().equals("Dasho") && !dtBean.getTitle().equals("Dr.") && !dtBean.getTitle().equals("Mrs.") && !dtBean.getTitle().equals("Ms.") && !dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                    }%>>Other</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--By Emtaz for showing Other Titles on 22/May/2016-->
                                    <tr>
                                        <td class="ff"><label id="OtherTitleName"></label></td>
                                        <td>
                                            <label id="OtherTitleValue"></label>
                                            <span id="OtherTitleMsg" style="color: red;">&nbsp;</span>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">First Name : <span>*</span></td>
                                                   <td><input name="firstName" type="text" class="formTxtBox_1" maxlength="50" id="txtFirstName" style="width:200px;" onblur="ToUpperCase(this)" title="First Name (English - No Abbreviations- All Block Letters)" value="<%if (ifEdit) {
                                                           out.print(dtBean.getFirstName());
                                                       }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Middle Name : </td>
                                                   <td><input name="middleName" type="text" class="formTxtBox_1" maxlength="50" id="txtMiddleName" style="width:200px;" onblur="ToUpperCase(this)"  value="<%if (ifEdit) {
                                                           out.print(dtBean.getMiddleName());
                                                       }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Last Name : </td>
                                                   <td><input name="lasatName" type="text" class="formTxtBox_1" maxlength="50" id="txtLastName" style="width:200px;" onblur="ToUpperCase(this)" value="<%if (ifEdit) {
                                                           out.print(dtBean.getLasatName());
                                                       }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--<tr id="trBangla">-->
                                    <!--<td class="ff">Name in Dzongkha :</td>-->
                                    <input name="banglaName" type="hidden" class="formTxtBox_1" maxlength="300" id="txtNameBangla" style="width:200px;" onblur="ToUpperCase(this)" value="<%if (ifEdit) {
                                            if (dtBean.getFullNameInBangla() != null) {
                                                out.print(BanglaNameUtils.getUTFString(dtBean.getFullNameInBangla()));
                                            }
                                        }%>"/>
                                    <!--<div class="formNoteTxt">(If Bhutanese)</div></td>-->
                                    <!--</tr>-->
                                    <tr id="trBngHght">
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--                                  <tr>
                                                                            <td class="ff">National ID / Passport No. / <br />
                                                                                Driving License No : <span>*</span></td>
                                                                            <td><input name="nationalIdNo" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;"  value="<%if (ifEdit) {
                                                                                    out.print(dtBean.getNationalIdNo());
                                                                                }%>"/>
                                                                                <br/><span id="natMsg" style="color: red;">&nbsp;</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" height="7"></td>
                                                                         </tr>-->
                                    <tr>
                                        <td class="ff">Address : <span>*</span></td>
                                                <td><textarea name="address1" rows="3" class="formTxtBox_1" maxlength="250" id="txtaAddr1" style="width:350px;"><%if (ifEdit) {
                                                        out.print(dtBean.getAddress1());
                                                    }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--                                    <tr>
                                                                            <td class="ff">Address Line 2 : </td>
                                                                            <td><textarea name="address2" rows="3" class="formTxtBox_1" maxlength="250" id="txtaAddr2" style="width:350px;"><%if (ifEdit) {
                                                                                    out.print(dtBean.getAddress2());
                                                                                }%></textarea></td>
                                                                        </tr>
                                                                         <tr>
                                                                                    <td colspan="2" height="7"></td>
                                                                         </tr>-->
                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="country" class="formTxtBox_1" id="cmbCountry">
                                                <%          //Code by Proshanto
                                                    String countryName = "Bhutan";//Bangladesh
                                                    if (ifEdit) {
                                                        countryName = dtBean.getCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryName)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td><select name="state" class="formTxtBox_1" id="cmbState" onchange="SubDistrictANDAreaCodeLoad()">
                                                <%String cntName = "";
                                                    if (ifEdit) {
                                                        cntName = dtBean.getCountry();
                                                    } else {
                                                        //Code by Proshanto
                                                        cntName = "Bhutan";//Bangladesh
                                                    }
                                                    for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                                <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                        if (state.getObjectValue().equals(dtBean.getState())) {%>selected<%}
                                                        } else //Change Dhaka to Thimphu
                                                            if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>

                                    <tr id="trRSubdistrict">
                                        <td class="ff">Dungkhag / Sub-district : </td>
                                        <td><select name="subDistrict" class="formTxtBox_1" id="cmbSubDistrict">

                                                <%String regsubdistrictName = "";
                                                    if (ifEdit) {
                                                        regsubdistrictName = dtBean.getState();
                                                    } else {
                                                        regsubdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regsubdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                        } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                        selected<%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>

                                    <tr id="trRthana">
                                        <td class="ff" height="30">Gewog : </td>
                                        <td><select name="upJilla" class="formTxtBox_1" id="txtThana" style="width:218px;">
                                                <%String state = "Thimphu";
                                                    String subdistrict = "";
                                                    if (ifEdit) {
                                                        state = dtBean.getState();
                                                        subdistrict = dtBean.getSubDistrict();
                                                    }
                                                    if (!state.isEmpty() && !subdistrict.isEmpty()) {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(subdistrict)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                            }
                                                        } else if (gewoglist.getObjectValue().equals("")) {%>
                                                        selected<%
                                                            }%>><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                } else {
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(state)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                                }
                                                            }%>

                                                        ><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                    }
                                                %>
                                            </select></td>
                                    </tr>        

                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">City / Town : </td>
                                                   <td><input name="city" type="text" class="formTxtBox_1" maxlength="50" id="txtCity" style="width:200px;"  value="<%if (ifEdit) {
                                                           out.print(dtBean.getCity());
                                                       }%>"/></td>

                                        <!--                                                 <td>   <select name="subDistrict" class="formTxtBox_1" id="cmbSubDistrict">
                                        
                                        <%/*String areaCode = "";
                                            if (ifEdit) { 
                                                regsubdistrictName = dtBean.getState();
                                            } else {
                                                regsubdistrictName = "Thimphu";
                                            }
                                            for (SelectItem areacode : tendererMasterSrBean.getareaCode(regsubdistrictName)) {*/%>
                                        <option value="<%/*=areacode.getObjectValue()*/%>" ><%/*=areacode.getObjectValue()*/%></option>
                                        <%//}%>
                                    </select> </td>-->
                                    </tr>

                                    <%--<tr id="trRthana">
                                        <td class="ff">Gewog : </td>
                                        <td><input name="upJilla" type="text" class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getUpJilla());
                                                    }%>"/>
                                            <div id="upErr" style="color: red;"></div></td>
                                    </tr>--%>
                                    <tr id="trHght">
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <%if (ifEdit) {
                                        //Code by Proshanto,Bangladesh to Bhutan
                                        if (!dtBean.getCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trBangla').css("display", "none");
                                        $('#trRthana').css("display", "none");
                                        $('#trRSubdistrict').css("display", "none");
                                        /*$('#trHght').css("display","none");
                                         $('#trBngHght').css("display","none");*/
                                    </script>
                                    <%}
                                        }%>                                     
                                    <tr>
                                        <td class="ff">Postcode : <!--span>*</span--></td>
                                        <td><input name="postcode" type="text" class="formTxtBox_1" maxlength="10" id="txtPost" style="width:200px;"  value="<%if (ifEdit

                                            
                                                ) {
                                                        out.print(dtBean.getPostcode());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="phoneCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                            if (ifEdit

                                            
                                                ) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                            }

                                            
                                                else {
                                                        //Code by Proshanto,Bangladesh to Bhutan
                                                        out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit

                                                
                                                    ) {
                                                                    if (dtBean.getPhoneNo().contains("-")) {
                                                        out.print(dtBean.getPhoneNo().split("-")[0]);
                                                    }
                                                }%>" name="phoneSTD" id="phoneSTD" class="formTxtBox_1" style="width:50px;" readonly/>-<input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhone" style="width:100px;"  value="<%if (ifEdit

                                                            
                                                                ) {
                                                                                if (dtBean.getPhoneNo().contains("-")) {
                                                                    out.print(dtBean.getPhoneNo().split("-")[1]);
                                                                }
                                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="faxCode" value="<%
                                            if (ifEdit

                                            
                                                ) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                            }

                                            
                                                else {
                                                        //Code by Proshanto,Bangladesh to Bhutan
                                                        out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" value="<%if (ifEdit

                                                
                                                    ) {
                                                        if (dtBean.getFaxNo().contains("-")) {
                                                        out.print(dtBean.getFaxNo().split("-")[0]);
                                                    }
                                                }%>" name="faxSTD" id="faxSTD" class="formTxtBox_1" style="width:50px;" readonly/>-<input name="faxNo" type="text" class="formTxtBox_1" maxlength="20" id="txtFax" style="width:100px;"  value="<%if (ifEdit

                                                            
                                                                ) {
                                                                                if (dtBean.getFaxNo().contains("-")) {
                                                                    out.print(dtBean.getFaxNo().split("-")[1]);
                                                                }
                                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile No : <span>*</span></td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="cntCode" value="<%if (ifEdit

                                            
                                                ) {
                                                        out.print(commonService.countryCode(dtBean.getCountry(), false));
                                            }

                                            
                                                else {
                                                        //Code by Proshanto,Bangladesh to Bhutan
                                                        out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMob" style="width:160px;"  value="<%if (ifEdit

                                                
                                                    ) {
                                                                    out.print(dtBean.getMobileNo());
                                                }%>"/>
                                            <span id="mobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Tax Payment Number : <span>*</span></td>
                                        <td>
                                            <!--                                            <select name="tinNo" class="formTxtBox_1" id="cmbTin">
                                                                                            <option <%/*if (ifEdit) {
                                                                if (dtBean.getTinNo().equals("tin")) {*/%>selected<%/*}
                                                                            }*/%> value="tin">TIN</option>
                                                                                            <option value="other" <%/*if (ifEdit) {
                                                                if (dtBean.getTinNo().equals("other")) {*/%>selected<%/*}
                                                                            }*/%>>Other Similar Document</option>
                                                                                        </select>-->
                                            <input type="hidden" name="tinNo" id="cmbTin" value="tin" />


                                            <%
                                                if (ifEdit

                                                
                                                    ) {
                                                            if (dtBean.getTinDocName().contains("$")) {
                                            %>
                                            <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%=dtBean.getTinDocName().substring(0, dtBean.getTinDocName().indexOf("$"))%>"  class="formTxtBox_1"/><br/>
                                            <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                            <div id="divdescription">
                                                <textarea name="otherDoc" cols="10" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"><%=dtBean.getTinDocName().substring(dtBean.getTinDocName().indexOf("$") + 1, dtBean.getTinDocName().length())%></textarea>
                                                <div class="formNoteTxt" id="2n">(Description)<br/><span id="txtaODocspan"></span></div>
                                            </div>
                                            <%
                                            } else
                                            %>
                                            <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" value="<%if (ifEdit) {
                                                        out.print(dtBean.getTinDocName());
                                                    }%>"  class="formTxtBox_1"/>
                                            <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                            <div id="divdescription" style="display: none;">
                                                <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"></textarea>
                                                <div class="formNoteTxt" id="2n" >(Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                            </div>
                                            <%
                                                }

                                                
                                                
                                                else {
                                            %>
                                            <input name="tinDocName" type="text" id="tinNoNo" style="width: 200px;" class="formTxtBox_1"/><br/>
                                            <div class="formNoteTxt" id="1n">(Tax Payment Number or other similar document number)<br/><span id="tinNoNospan" class="reqF_1"></span></div>
                                            <div id="divdescription" style="display: none;">
                                                <textarea cols="10" name="otherDoc" rows="3" class="formTxtBox_1" id="txtaODoc" style="width:350px;"></textarea>
                                                <div class="formNoteTxt" id="2n">(Document Description)<br/><span id="txtaODocspan" class="reqF_1"></span></div>
                                            </div>
                                            <%                                                        }
                                            %>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--                                    <tr>
                                                                            <td class="ff">Specialization : <span>*</span></td>
                                                                            <td><textarea name="specialization" cols="10" rows="3" class="formTxtBox_1" id="txtaCPV" style="width:350px;" readonly><%/*if (ifEdit) {
                                                                                            out.print(dtBean.getSpecialization());
                                                                                        }*/%></textarea> <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()" id="aTree">Select Categories</a></td>
                                                                        </tr>-->
                                    <input type="hidden" name="specialization" class="formTxtBox_1" id="txtaCPV" value="disabled"/>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Website : </td>
                                        <td><input name="website" type="text" class="formTxtBox_1" maxlength="100" id="txtWebsite" style="width:200px;"  value="<%if (ifEdit

                                            
                                                ) {
                                                        out.print(dtBean.getWebsite());
                                            }%>"/><br/>(Enter website name without 'http://' e.g. eprocure.gov.bd)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <%if (ifEdit

                                        
                                        
                                        ) {%>
                                    <tr>
                                        <td>&nbsp;<input name="comments" type="hidden" class="formTxtBox_1" maxlength="100" id="txtComment" style="width:200px;"  value="user"/></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            &nbsp;
                                            <%if (ifEdit

                                                
                                                

                                                
                                                
                                                ) {%>
                                            <label class="formBtn_1">
                                                <input type="submit" name="edit" id="btnUpdate" value="Update" />
                                                <input type="hidden" name="hdnedit" id="hdnedit" value=""  />
                                            </label>
                                            <%} else {%>
                                            <label class="formBtn_1">
                                                <input type="submit" name="button" id="btnSave" value="Save"  />
                                                <input type="hidden" name="hdnbutton" id="hdnbutton" value="" />
                                            </label>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                        <!--<td width="266" class="td-background">--> 
                            <%--<jsp:include page="resources/common/Left.jsp" ></jsp:include>--%>
                            <!--</td>-->
                        </tr>
                        <input type="hidden" id="ValidityCheck" value="1" />
                    </table>                
                    <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </body>
        <script type="text/javascript">
            //    $(function () {// Automatic SubDistrict & Area code selection by Emtaz 21/May/2016
            //                $('#cmbState').change(function () {
            //                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue'}, function (j) {
            //                        $('#cmbSubDistrict').html(j);
            //                    });
            //                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
            //                        $('#phoneSTD').val(j);
            //                        $('#faxSTD').val(j);
            //                    });
            //                });
            //            });
            function SubDistrictANDAreaCodeLoad()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue'}, function (j) {
                    $('#cmbSubDistrict').html(j);
                });
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                    $('#phoneSTD').val(j);
                    $('#faxSTD').val(j);
                });
            }
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#cntCode').val(j.toString());
                    });
                });
            });
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbState").html(j);
                        //Code by Proshanto
                        if ($('#cmbCountry').val() == "150") {//136
                            $('#trBangla').css("display", "table-row")
                            $('#trRthana').css("display", "table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                        } else {
                            $('#trBangla').css("display", "none")
                            $('#trRthana').css("display", "none")
                            $('#trRSubdistrict').css("display", "none")
                        }
                    });
                });
            });

            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbSubDistrict').change(function () {
                    if ($('#cmbSubDistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    } else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    }
                });
            });

            /*$(function() {
             $('#txtNationalId').blur(function() {
             if($('#txtNationalId').val()!=""){
             //$('span.#natMsg').css("color","red");
             //$('span.#natMsg').html("Checking for Unique National ID...");
             $.post("<//=request.getContextPath()%>/CommonServlet", {nationalId:$('#txtNationalId').val(),funName:'verifyNationId'},
             function(j){
             if(j.toString()==""){
             $('span.#natMsg').html(null);
             }else{
             $('span.#natMsg').css("color","red");
             $('span.#natMsg').html(j);
             }
             });
             }else{
             $('span.#natMsg').html(null);
             }
             });
             });*/

            /*$(function() {
             $('#txtMob').blur(function() {                    
             if($.trim($('#txtMob').val())!=""){
             if($.trim($('#txtMob').val()).length>=10 && $.trim($('#txtMob').val()).length<=15){
             if(numbersOnly($('#txtMob').val())){
             $('span.#mobMsg').css("color","red");
             $('span.#mobMsg').html("Checking for Unique Mobile Number...");
             $.post("<-%=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobile'},
             function(j){                            
             if(j.toString().indexOf("M", 0)!=-1){
             $('span.#mobMsg').css("color","red");
             $('span.#mobMsg').html(j);
             }else{
             $('span.#mobMsg').css("color","green");
             $('span.#mobMsg').html("Ok");
             }
             });
             }
             }
             else{
             $('span.#mobMsg').html(null);
             }
             }
             });
             });*/
            $(function () {
                $('#cmbTin').change(function () {
                    if ($('#cmbTin').val() == "tin") {
                        document.getElementById('divdescription').style.display = "None";
                        $('#txtaODoc').val(null);
                    } else {
                        document.getElementById('divdescription').style.display = "inline";
                    }
                });
            });

            //    $(function() {
            //        $('#txtThana').blur(function() {
            //            if($.trim($('#txtThana').val())==""){
            //                    $('#upErr').html("Please enter Thana / UpaZilla");
            //                    return false;
            //                }else{
            //                    $('#upErr').html(null);
            //           }
            //        });
            //    });

            $(function () {
                $('#frmSignup').submit(function () {
                    if ($("#cmbTin").val() == "tin") {
                        if ($.trim($("#tinNoNo").val()) == "") {
                            $("#tinNoNospan").html("Please Enter Tax Payment Number");
                            $("#tinNoNo").focus();
                            return false;
                        } else {
                            if ($("#tinNoNo").val().length > 30) {
                                $("#tinNoNospan").html("Please Enter Tax Payment Number less than 30");
                                $("#tinNoNo").focus();
                                return false;
                            } else {
                                $("#tinNoNospan").html(null);
                            }
                        }
                    } else if ($("#cmbTin").val() == "other") {
                        if ($.trim($("#tinNoNo").val()) == "") {
                            $("#tinNoNospan").html("Please Enter Tax Payment Number");
                            $("#tinNoNo").focus();
                            return false;
                        } else {
                            if ($("#tinNoNo").val().length > 30) {
                                $("#tinNoNospan").html("Please Enter Tax Payment Number less than 30");
                                $("#tinNoNo").focus();
                                return false;
                            } else {
                                $("#tinNoNospan").html(null);
                            }
                        }
                        if ($.trim($("#txtaODoc").val()) == "") {
                            $("#txtaODocspan").html("Please Enter Description");
                            $("#txtaODoc").focus();
                            return false;
                        } else {
                            if ($("#txtaODoc").val().length > 100) {
                                $("#txtaODocspan").html("Please Enter Description less than 100");
                                $("#txtaODoc").focus();
                                return false;
                            } else {
                                $("#txtaODocspan").html(null);
                            }
                        }
                    }
                    //Code by Proshanto
                    if ($('#cmbCountry').val() == "150") {//136
                        //                if($.trim($('#txtThana').val())==""){
                        //                    $('#upErr').html("Please Enter Thana/UpaZilla");
                        //                    return false;
                        //                }else{
                        //                    $('#upErr').html(null);
                        //                }
                    }
                    if ($('span.#mobMsg').html() == "Mobile no. already exist") {//||($('span.#natMsg').html()=="National ID./Passport No./Driving License No. already exist")
                        return false;
                    }
                    if (document.getElementById("ValidityCheck").value == '0')
                    {
                        return false;
                    }

                    if ($('#frmSignup').valid()) {
                        ChangeTitleValue();
                        if ($('#btnSave') != null) {
                            $('#btnSave').attr("disabled", "true");
                            $('#hdnbutton').val("Save");
                        }
                        if ($('#btnUpdate') != null) {
                            $('#btnUpdate').attr("disabled", "true");
                            $('#hdnedit').val("Update");
                        }

                    }
                });
            });
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#phoneCode').val(j.toString());
                        $('#faxCode').val(j.toString());
                    });
                });
            });
            function loadCPVTree()
            {
                window.open('resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
            }
    </script>
    <script>
        //Emtaz 22/May/2016.
        function ToUpperCase(obj)
        {
            obj.value = obj.value.toUpperCase();
        }
        function TitleChange(TitleValue)
        {
            if (document.getElementById("cmbTitle").value == 'Other')
            {
                document.getElementById('OtherTitleName').innerHTML = 'Other Title :<span>*</span>';
                document.getElementById('OtherTitleValue').innerHTML = '<input class="formTxtBox_1" maxlength="50" type="text" id="OtherTitleValueInput" name="OtherTitleValueInput" onblur="TitleValidity()" value="' + TitleValue + '" />';
            } else
            {
                document.getElementById('OtherTitleName').innerHTML = '';
                document.getElementById('OtherTitleValue').innerHTML = '';
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
        function ChangeTitleValue()
        {
            if (document.getElementById("cmbTitle").value == 'Other')
            {
                var OtherTitleValue = document.getElementById("OtherTitleValueInput").value;
                document.getElementById("OtherTitle").value = OtherTitleValue;
            }
        }
        function TitleValidity()
        {
            if ($("#OtherTitleValueInput").val().length > 5)
            {
                $('#OtherTitleMsg').html("Maximum 5 characters are allowed.");
                document.getElementById("ValidityCheck").value = "0";
            } else if ($("#OtherTitleValueInput").val() == "")
            {
                $('#OtherTitleMsg').html("Enter Title.");
                document.getElementById("ValidityCheck").value = "0";
            } else
            {
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
        //Emtaz
    </script>
</html>
