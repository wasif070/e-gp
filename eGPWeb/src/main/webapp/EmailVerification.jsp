<%--
    Document   : EmailVerification
    Created on : Oct 29, 2010, 12:29:27 PM
    Author     : Administrator
--%>

<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP: Email Verification</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>

        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmReset").validate({
                    rules: {
                        emailId:{required: true,email:true},
                        password: {spacevalidate: true, requiredWithoutSpace: true , maxlength: 25 , minlength: 8, alphaForPassword : true},
                        verificationCode:{required:true}
                    },
                    messages: {
                        emailId:{ required: "<div class='reqF_1'> Please enter e-mail ID</div>",
                            email:"<div class='reqF_1'>Please enter valid e-mail ID</div>"},
                        password: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>" ,
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"
                        },
                        verificationCode:{required:"<div class='reqF_1'>Please enter Verification Code</div>"}
                    }
                });
            });

            function chekValidation(){
                if(document.getElementById("serverMsg")!=null){
                   // document.getElementById("serverMsg").innerHTML=null;
                    $(".responseMsg").remove();
                }
            }
        </script>
    </head>
    <body>
<%
    if ("No Thanks. I will register later on".equalsIgnoreCase(request.getParameter("submit"))) {
        session.invalidate();
        response.sendRedirect("Index.jsp");
    } else {
            String msg = request.getParameter("msg");
                    //  if (msg != null && !(msg.startsWith("Error"))){
%>

        <!--        <form name="frmMailVerification" id="frmMailVerification" method="post"></form>
                <script type="text/javascript">
                    jAlert('Account verified successfully','Email Verification', function(r) {
                        if (r) {
                            document.getElementById("frmMailVerification").action = "<%= msg%>";
                            document.getElementById("frmMailVerification").submit();
                        }
                    });
                </script>-->
<%/*
        }else{*/
        if ("Proceed To Profile Submission".equalsIgnoreCase(request.getParameter("submit"))) {
            String userId = msg.substring(msg.indexOf("-") + 1, msg.length());
            String message = msg.substring(0, msg.indexOf("-")) + ".jsp";
            session = request.getSession(true);

            response.sendRedirect(message);

        }/*if ("No Thanks. I will register later on".equalsIgnoreCase(request.getParameter("submit"))) {
        response.sendRedirect("Index.jsp");
        }*/
%>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <div class="pageHead_1 t_space">Email Verification</div>
<%
                                        boolean flag = true;
                                        if (msg != null) {
                                            if (msg.startsWith("Error")) {

                                                msg = msg.substring(msg.indexOf(":") + 1, msg.length());
                                                flag = true;
%>
                                        <div id="serverMsg" class="responseMsg errorMsg" style="margin-top: 10px;">
                                            <%= msg%>
                                        </div>
<%
                                            } else {
                                                flag = false;
%>
                                        <table width="100%" cellpadding="0" cellspacing="0" class="t_space">
                                            <tr>
                                                <td colspan="2">
                                                    Dear User,<br/><br/>
                                                    Your e-mail ID has been successfully verified.
                                                </td>
                                            </tr>
                                          
                                            
                                            <tr style="display: none">
                                                <td colspan="2">
                                                    <div class="atxt_1 c_t_space">
                                                        Be informed that second stage of the registration process is your Profile and mandatory documents submission including payment slip for registration. You must complete your Profile, upload mandatory documents and submit all within <strong><%=XMLReader.getMessage("FinalSubComplete")%> from the date of email verification on e-GP System</strong>.  If you fail to submit your Profile and documents within <strong><%=XMLReader.getMessage("FinalSubComplete")%></strong>, your complete records will be removed from e-GP System and you will need to re-register.<br/><br/>
                                                        For completing your Profile submission and document submission, click <strong>"Proceed To Profile Submission"</strong> button below now or you can come back later to log in to the
                                                        e-GP System using your login information [i.e. email address (ID) and password you provided during registration request] and complete the Profile and document submission.<br/><br/>

                                                        Before starting your profile and document submission, please read the following information and make sure the required authentic MANDATORY documents are ready.

                                                        <br /><br />Please <a href="#" onclick="window.open('MandRegDoc.jsp', '', 'resizable=yes,scrollbars=1','')" title="View"><font color="green">click here for list of mandatory credential documents.</font></a>
                                                        <div class="listBox_abt">
                                                            <ol>
                                                                <li class="t-align-justify b_space">
                                                                    Scan those mandatory documents you have been asked for and convert into <b>PDF</b> or <b>Zip file format</b>. Size of a single file must not exceed <strong><%CheckExtension ext = new CheckExtension();out.print(ext.getConfigurationMaster("tenderer").getFileSize()); ext=null;%> MB</strong>. Scan the files in such a way that it can be easily readable. And also ensure that the documents are virus Free.  GPPMD as a system provider does not take any responsibility of any consequences to your future transactions in case ineligible scanned document, file containing virus or misrepresentation of the information in the uploaded documents.</li>
                                                                <li class="t-align-justify b_space">
                                                                    Upload the mandatory credential documents for the registration. Failure to upload any one of the documents, e-GP system does not allow you to proceed further.
                                                                </li>
                                                                <!-- Hide 
                                                                <li class="t-align-justify b_space">
                                                                    If the uploaded documents are found forged or false, your case will be treated under section 64 (Professional misconduct) of the Public Procurement Rules and Regulations 2009.
                                                                </li> -->
                                                                <li class="t-align-justify">
                                                                    In case of physical submission of mandatory document, advice for physical submission of attested (by the government class-1 official) copies of scanned and already uploaded mandatory documents through Courier or Registered mail.
                                                                </li>
                                                            </ol>
                                                            <br/>
                                                            <div class="aboutUs_title" style="padding-left: 2px; margin-bottom: 5px;">How to make payment for registration?</div>
                                                            <br/>
                                                            You should make payment by using credit card online instantly after clicking on the “Proceed to Profile Submission” link below or by visiting one of <a href="#" onclick="window.open('MemScheduleBank.jsp', '', 'resizable=yes,scrollbars=1','')" title="View"><font color="green">the scheduled member banks of e-GP e-Payment Network</font></a> and make payment through Cash or Account to Account Transfer. In case of Account to Account Transfer, the bank will require to debit from your bank account on that bank for the User Registration in e-GP System.<br /><br/>
                                                            <%--Hide 
                                                            For Bangladeshi Tenderers and Consultants, registration fee is Nu. <%=XMLReader.getMessage("localTenderRegFee")%> (<%=XMLReader.getMessage("localTenderRegFeeWrd")%>) and annual renewal fee is Nu. <%=XMLReader.getMessage("localTenderRenewFee")%> (<%=XMLReader.getMessage("localTenderRenewFeeWrd")%>). You must renew your user account in e-GP portal each year before the expiry date.<br /><br/>--%>
                                                            For international Bidders and Consultants, registration fee is USD $<%=XMLReader.getMessage("intTednerRegFee")%> (<%=XMLReader.getMessage("intTednerRegFeeWrd")%>) and annual renewal fee is USD $ <%=XMLReader.getMessage("intTednerRenewFee")%> (<%=XMLReader.getMessage("intTednerRenewFeeWrd")%>).<br /><br /><br/>
                                                            Thank you for using e-GP System.<br />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%"  class="t-align-right" >
                                                    <form name="mailVerifactionSucess" action="<%=request.getContextPath()%>/LoginSrBean?funName=cmpltVeriPrcs" method="post">
                                                        <label class="formBtn_1 t_space">
                                                            <input type="submit" name="submit" id="btnSubmit" value="Proceed To Profile Submission"/>
                                                        </label>
                                                    </form>
                                                </td>
                                                <td width="50%" class="t-aling-left">
                                                    <form name="frmNoThanks" action="EmailVerification.jsp" method="post">
                                                        <label class="formBtn_1 t_space l_space">
                                                            <input type="submit" name="submit" id="btnSubmit" value="No Thanks. I will register later on"/>
                                                        </label>
                                                    </form>
                                                </td>
                                            </tr>
                                        </table>
<%                                          }
                                        }
                                        if (flag) {
%>
                                        <form id="frmReset" name="frmReset" method="POST" action="<%=request.getContextPath()%>/LoginSrBean">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtEmailId" name="emailId" style="width: 200px"/>
                                                        <span id="errorMsg" style="color: red;font-weight: bold">&nbsp;</span>
                                                        <input type="hidden" name="funName" value="verifyMail"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="password" id="txtPassword" name="password" style="width: 200px;" autocomplete="off"  />
                                                        <span id="errorMsg" style="color: red;font-weight: bold">&nbsp;</span>
                                                        <input type="hidden" name="action" value="verifyMail"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Verification Code : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtVerificationCode" name="verificationCode" style="width: 200px" maxlength="40"/>
                                                    </td>
                                                </tr>
                                                <tr>&nbsp;
                                                    <td>
                                                        <!--                                                        <label class="formBtn_1">
                                                                                                                    <input type="submit" name="submit" id="btnSubmit" value="Continue registration process" />
                                                                                                                </label>-->
                                                    </td>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="submit" id="btnSubmit" value="Submit" onclick="chekValidation();"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
<%
                                        }
%>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                                     <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
<%
    }
%>
</html>
