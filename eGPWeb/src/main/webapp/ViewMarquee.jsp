 <%-- 
    Document   : ViewMarquee
    Created on : Jan 18, 2011, 5:18:43 PM
    Author     : Rajesh Singh,rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View All Notifications</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body onload="loadTable();">
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();

                            if(parseInt($('#pageNo').val(), 10) != 1)
                                $('#btnFirst').removeAttr("disabled");
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnFirst').attr("disabled", "true")

                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnLast').attr("disabled", "true");
                            else
                                $('#btnLast').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnNext').attr("disabled", "true")
                            else
                                $('#btnNext').removeAttr("disabled");
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable(){
                $(function() {
                    $.post("<%=request.getContextPath()%>/MarqueeServlet", {funName: "viewMarquee",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);

                        if($('#noRecordFound').val() == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }

                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                    
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                });
            }
        </script>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="250">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                        <td class="contentArea" align="left">
                            <!--Page Content Start-->
                            <div class="pageHead_1">
                               View All Notifications</div>
                            <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th width="34%" class="t-align-left">Message</th>
                                    <th width="21%" class="t-align-center">Publication Date & Time</th>
                                    <th width="21%" class="t-align-center">Start Date & Time</th>
                                    <th width="20%" class="t-align-center">End Date & Time</th>
                                </tr>
                            </table>
                            <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                <tr>
                                    <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                    <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                    <td class="prevNext-container"><ul>
                                            <li>&laquo; <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                            <li>&#8249; <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                            <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                        </ul></td>
                                </tr>
                            </table>
                            <div align="center">
                                <input type="hidden" id="pageNo" value="1"/>
                                <input type="hidden" name="size" id="size" value="10"/>
                            </div>
                            <div>&nbsp;</div>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>


    </body>
</html>


