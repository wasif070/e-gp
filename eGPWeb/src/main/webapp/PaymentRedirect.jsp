<%-- 
    Document   : PaymentRedirect
    Created on : Jun 18, 2012, 5:46:26 PM
    Author     : shreyansh.shah
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%
    String strPayGateway =  XMLReader.getBankMessage(request.getParameter("pgw"));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment in process....</title>
        <script type="text/javascript" language="javascript">
 function submitform(){
    document.getElementById('payredirect').submit();
 }

 </script>
    </head>
    <body onload="submitform()">
        <h3>Please wait...Do not press back button or refesh. We will take you on payment page.</h3>
        <form action="<%=strPayGateway%>" method="post" name="payredirect" id="payredirect">
            <% String sdata = request.getParameter("encryptedInvoicePay");
            System.out.println("payment jsp data :"+sdata);
            %>
            <input type="hidden" value="<%=sdata%>" name="encryptedInvoicePay">
        </form>
    </body>
</html>
