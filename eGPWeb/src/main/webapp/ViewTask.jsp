<%-- 
    Document   : ViewTask
    Created on : Oct 27, 2010, 12:20:11 PM
    Author     : Naresh.Akurathi
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Task</title>
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <table width="100%" cellspacing="0">
      <tr valign="top">
        <td><div class="dash_menu_1">
            <ul>
              <li><a href="#"><img src="resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/appIcn.png" />APP</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/tenderIcn.png" />Tender</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/committeeIcn.png" />Evaluation</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/reportIcn.png" />Report</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/myAccountIcn.png" />My Account</a></li>
              <li><a href="#"><img src="resources/images/Dashboard/helpIcn.png" />Help</a></li>
            </ul>
          </div>
          <table width="100%" cellspacing="6" class="loginInfoBar">
            <tr>
              <td align="left"><b>Friday 27/08/2010 21:45</b></td>
              <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
              <td align="right"><img src="resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="Images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
            </tr>
          </table></td>
        <td width="141"><img src="resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
      </tr>
    </table>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav"><ul class="lftLinkBtn_1">  </ul>
       <!-- Left content of the page -->
      <jsp:include page="MsgBoxLeft.jsp" ></jsp:include>
        </td>
      <td class="contentArea"><div class="pageHead_1">View Task</div>
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
          <tr>
            <th width="1%" style="text-align:center; width:4%;">Sl.  No.</th>
            <th style="text-align:center; width:13%;">Task Brief</th>
            <th style="text-align:center; width:13%;">Priority</th>
            <th style="text-align:center; width:13%;">Start Date </th>
            <th style="text-align:center; width:13%;">End Date</th>
          </tr>
          <tr>
            <td style="text-align:center;">1</td>
            <td style="text-align:center;"><a href="#">Tender</a></td>
            <td style="text-align:center;">Notice &amp; Document</td>
            <td style="text-align:center;">Invitation for TOC member</td>
            <td style="text-align:center;">Urmil Mehta</td>
          </tr>

        </table>

        <div>&nbsp;</div></td>
    </tr>
  </table>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
  </table>
  <!--Dashboard Footer End-->
</div>
</body>
</html>
