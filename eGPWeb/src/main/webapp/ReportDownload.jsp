<%--
    Document   : Tenders
    Created on : Mar 21, 2011, 5:30:28 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reports</title>
<script type="text/javascript" src="resources/js/ddlevelsmenu.js"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="mainDiv">
    <div class="fixDiv">
         <jsp:include page="resources/common/Top.jsp" ></jsp:include>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td width="266">
                    <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                </td>
                <td class="contentArea-Blogin">
                      <div class="DashboardContainer">
                    <div class="pageHead_1">Procurement Performance Indicators Report </div>
                        <ul class="tabPanel_1 t_space">
                           <li><a href="RegistrationDetails.jsp">Registration Details</a></li>
                           <li><a href="AnnualProcurementPlans.jsp">Annual Procurement Plans </a></li>
                           <li><a href="Tenders.jsp">Tenders/Proposals</a></li>
                           <li><a href="ReportDownload.jsp" class="sMenu">Performance Indicators Report </a></li>
                        </ul>
                        <div class="tabPanelArea_1">
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="28%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");                           
                                for (SPTenderCommonData sptcd : tenderCS.returndata("DownloadDocs", "", "")) {
                                    docCnt++;

                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ReportDownloadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                          
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                        </div>
                      </div>
                </td>
            </tr>
         </table>
         <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
    </div>
</div>
</body>
</html>
