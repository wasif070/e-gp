<%--

--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Disclaimer and Privacy Policy</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
         <%
            String lang = null, contentprivacypolicy = null;

            if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
                lang = request.getParameter("lang");
            }else{
                lang = "en_US";
            }

            MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
            List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"privacypolicy");

            if(!langContentList.isEmpty())
            {
                for(TblMultiLangContent tblMultiLangContent:langContentList)
                {
                    if(tblMultiLangContent.getSubTitle().equals("content_privacypolicy")){
                        if("bn_IN".equals(lang)){
                           contentprivacypolicy = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                        }else{
                           contentprivacypolicy = new String(tblMultiLangContent.getValue());
                        }
                    }
                }
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start -->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <% if (session.getAttribute("userId") == null) { %>
                                    <td width="250">
                                        <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                                    </td>
                         <%}%>
                        <td class="contentArea_1"><!--Page Content Start-->
                              <%=contentprivacypolicy%>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End -->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
