<%-- 
    Document   : eSBDs
    Created on : Jan 3, 2018, 11:58:22 AM
    Author     : Feroz
--%>

<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PRR and Standard Bidding Documents</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/simpletreemenu.js"></script>
        <style>
        div.gallery {
            margin: 5px;
            /*border: 1px solid #ccc;*/
            float: left;
            width: 180px;
        }

        div.gallery:hover {
            /*border: 1px solid #777;
            background-color: lightyellow;*/
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }
        
        div.desc2 {
            border-style: ridge;
            padding: 15px;
            text-align: center;
        }
        div.desc2:hover {
            background-color: lightyellow;
        }

	#homePage{
		text-decoration:none;
		color:#FFF;
		padding-bottom:10px;
	}
</style>
    </head>
    <body>
        <div class="mainDiv" style="display:block;" id="listDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->  
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         
                            <br><br>
                            <div class="pageHead_1" style="padding-left:0%;">PRR and Standard Bidding Documents
                            </div>
                            <br>
                            <span id="lTable">
                            <table   style="padding-left: 15%;width: 100%;">
                                <tr>
                                   <td align="center" style="">
                                    <div class="gallery" style="#">
                                        <div class="desc"><b>Procurement of Goods- Up to Nu. 0.250 Million</div>
                                        <img src="/SBD/poster/Goods Up to .25.png" alt="Procurement of Goods" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('SBD Small Goods 30-06-14.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SBD Goods(Small) 30-06-14.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <div class="desc"><b>Procurement of Goods- Above Nu. 0.250 Million</div>
                                        <img src="/SBD/poster/Goods Above .25.png" alt="Procurement of Goods" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Procurement of Goods-Above Nu. 0.250 Million.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SBD  Goods(Large) 01-07-15.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <div class="desc"><b>Procurement of Works- Above Nu. 4 Million</div>
                                        <img src="/SBD/poster/Works Above 4.png" alt="Procurement of Works" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Procurement of Works-Above Nu. 4Million.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SBD Works (Large) 01-07-15.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                   </td>
                                   
                                </tr>
                                <tr>
                                    <td align="center" style="">
                                    <div class="gallery">
                                        <div class="desc"><b>Procurement of Works- Up to Nu. 4 Million</div>
                                        <img src="/SBD/poster/Works up to 4.png" alt="Procurement of Works" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('SBD Small Works 01-07-15.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SBD Works(Small) 01-07-15.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                   </td>
                                    <td align="center" style="">
                                      <div class="gallery">
                                          <div class="desc"><b>Procurement of Consulting Service- Above Nu.1 Million</div>
                                          <img src="/SBD/poster/SRFP Large.png" alt="Procurement of Consulting Service" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Procurement of Consulting Service- Above Nu.1 Million.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SRFP (large)01-07-15.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                    </td>
                                    <td align="center" style="">
                                      <div class="gallery">
                                          <div class="desc"><b>Procurement of Consulting Service- Up to Nu.1 Million</div>
                                          <img src="/SBD/poster/SRFP small.png" alt="Procurement of Consulting Service" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('SRFP Small 30-06-14.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        <a href="javascript:void();" onclick="downloadFile('SRFP (Small) 30-06-14.doc')">
                                          <div class="desc2"><b>Download DOC File</div>
                                        </a>
                                      </div>
                                    </td>
                                    
                                </tr>
                                <tr>
                                   <td align="center" style="">
                                    <div class="gallery" style="#">
                                        <div class="desc"><b>Procurement Rules and Regulations</div>
                                        <img src="/PRR/poster/prr.png" alt="Procurement Rules and Regulations" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Procurement Rules and Regulations.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <div class="desc"><b>Procurement Guidelines</div>
                                        <img src="/PRR/poster/procurementGuidelines.png" alt="Procurement Guidelines" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Procurement Guidelines.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <div class="desc"><b>Evaluation Guideline for Procurement of Works</div>
                                        <img src="/PRR/poster/evaluationGuidelines.png" alt="Evaluation Guideline for Procurement of Works" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('Evaluation Guideline for procurement of Works.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                      </div>
                                   </td>
                                   
                                </tr>
                                <tr>
                                   <td align="center" style="">
                                    <div class="gallery" style="#">
                                        <div class="desc"><b>Sample Evaluation Report</div>
                                        <img src="/PRR/poster/evaluationReport.png" alt="Sample Evaluation Report" width="50" height="70" style="border:1px solid #021a40;">
                                          
                                        <a href="javascript:void();" onclick="downloadFile('SampleEvaluationReport.pdf')">
                                          <div class="desc2"><b>Download PDF</div>
                                        </a>
                                        </div>
                                   </td>
                                   
                                </tr>
                                
                            </table>
                            </span>
                                
                                
                        </td>
                    </tr>
                </table>
                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
           
        
            


    </body>
</html>
            
            
<form id="form2" method="post">
</form>
<script type="text/javascript">

function downloadFile(fileName){
        document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=downloadPrr&fileName="+fileName;
        document.getElementById("form2").submit();
    }

</script>

