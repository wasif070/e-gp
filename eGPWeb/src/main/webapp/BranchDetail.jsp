<%--
    Document   : BranchDetail
    Created on : Feb 5, 2011, 6:32:04 PM
    Author     : dixit,rishita
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService"%>
<%@page import="com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
<%
        String sBankDevelopid = "";
        int sfBankDevelopid =0;
        //int sBankDevelopid ;
        if (!"".equalsIgnoreCase(request.getParameter("sbankdevelopid"))) {
            sBankDevelopid = request.getParameter("sbankdevelopid");
        }
        if(request.getHeader("referer") == null){
         response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp?rs=t");
        }
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Branch Detail</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 20) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 20) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">

            //add by dohatec
            function resetTable()
            {
                document.getElementById("branch").value="";
                document.getElementById("thana").value="";
                document.getElementById("city").value="";
                document.getElementById("cmbState").selectedIndex = 0;
                //Code Start By Proshanto Kumar Saha,Dohatec
                document.getElementById("cmbBank").selectedIndex = 0;
                //Code Start By Proshanto Kumar Saha,Dohatec
                //$("#pageNo").val("1");
                //loadTable();
               firstLoadTable();
              
            }
            //end
            //Code Start by Proshanto Kumar Saha
            function firstLoadTable() {
                //$('#resultTable').hide();
                //$('#pagination').hide();
                $.post("<%=request.getContextPath()%>/ScBankDevPartnerJqGridSrBean", {sbankdevelopId :$("#cmbBank").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),branch:$("#branch").val(),thana:$("#thana").val(),city:$("#city").val(),district:$("#cmbState").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
           //Code End by Proshanto Kumar Saha
           
            function loadTable()
            {
                 //Code Start by Proshanto Kumar Saha
                if($.trim($('#cmbBank').val()) ==-1 || $.trim($('#cmbState').val())==-1){
                    jAlert("Please Select Financial Institute and Dzongkhag / District.","Search Criteria", function(RetVal) {});
                    return false;
                }else{
                //Code End by Proshanto Kumar Saha
                     //$('#resultTable').show();
                    $.post("<%=request.getContextPath()%>/ScBankDevPartnerJqGridSrBean", {sbankdevelopId :$("#cmbBank").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),branch:$("#branch").val(),thana:$("#thana").val(),city:$("#city").val(),district:$("#cmbState").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
                }
            }
           
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 20) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
    </head>
    <!--Code Start by Proshanto Kumar Saha-->
    <body onload="firstLoadTable();">
        
        <div class="mainDiv">
            
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="266"><jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                        <td class="contentArea">
                            <div class="pageHead_1">Registered Financial Institute and its Branch Details
                            </div>
                            <table width="100%" cellspacing="0" class="t_space">
                                <tr valign="top">
                                    <td>
                                        <div id="resultDiv" style="display: block;">
                                            <div style="font-weight: bold;">
                                            </div>
                                            <div class="formBg_1 t_space">
                                              <!-- add by dohatec -->
                                             <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                                 <!--Code Start By Proshanto Kumar Saha,Dohatec-->
                                                 <tr>
                                                    <td class="ff">Financial Institute Name :</td>
                                                    <td><select class="formTxtBox_1" id="cmbBank" name="bank" style="width: 208px">
                                                     <option value=-1 selected ="selected">Select</option>
                                                     <option value="" >All</option>
                                                  <%
                                                     ScBankDevpartnerService scbankdevpartner = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
                                                    List<TblScBankDevPartnerMaster> bankname = scbankdevpartner.getBankName();
                                                     if (!bankname.isEmpty()) {
                                                     for (int i = 0; i < bankname.size(); i++) {
                                                  %>
                                                <option  value="<%=bankname.get(i).getSbankDevelopId()%>"><%=bankname.get(i).getSbDevelopName()%></option>
                                                <%}}%>
                                                 </select>
                                              </td>
                                                </tr>
                                                <!--Code End By Proshanto Kumar Saha,Dohatec-->
                                                 <tr>
                                                    <td class="ff">Branch Name :</td>
                                                    <td>
                                                        <input name="branch" id="branch" type="text" class="formTxtBox_1" style="width:202px;" />
                                                        <span class="reqF_1" id="msgTenderId"></span>
                                                    </td>
                                                    <td class="ff">Gewog :</td>
                                                    <td><input name="thana" id="thana" type="text" class="formTxtBox_1" style="width:202px;" /></td>
                                                </tr>
                                                 
                                                 <tr>
                                                    <td class="ff">Town/City :</td>
                                                    <td>
                                                        <input name="city" id="city" type="text" class="formTxtBox_1" style="width:202px;" />
                                                        <span class="reqF_1" id="msgTenderId"></span>
                                                    </td>
                                                    <td class="ff">Dzongkhag / District :</td>
                                                    <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                     <option value=-1 selected ="selected">Select</option>
                                                     <option value="" >All</option>
                                                <%
                                                     //150 instead of 136 by Proshanto
                                                     for (SelectItem stateItem : scBankCreationSrBean.getStateList((short) 150)) {
                                                %>
                                                <option  value="<%=stateItem.getObjectValue()%>"><%=stateItem.getObjectValue()%></option>
                                                <%}%>
                                                
                                            </select>
                                            </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="t-align-center">
                                                        <label class="formBtn_1 t_space"><input type="button" name="search" id="btnSearch" value="Search" onclick="loadTable();" /></label>&nbsp;&nbsp;
                                                        <label class="formBtn_1 t_space"><input type="reset" name="btnReset" id="btnReset" value="Reset" onclick="resetTable();" /></label>
                                                    </td>
                                                </tr>
                                               
                                             </table>
                                                <!--end dohatec-->
                                            </div>                                                
                                            <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space">
                                                <tr>
                                                    <th width="5%" class="t-align-center">Sl. No.</th>
                                                    <!--Code Start By Proshanto Kumar Saha,Dohatec-->
                                                    <th width="25%" class="t-align-center">Financial Institute Name</th>
                                                    <!--Code End By Proshanto Kumar Saha,Dohatec-->
                                                    <th width="25%" class="t-align-center">Branch Name</th>
                                                    <th width="40%" class="t-align-center">Address</th>
                                                    <th width="15%" class="t-align-center">Phone No.</th>
                                                    <th width="15%" class="t-align-center">Fax No.</th>
                                                </tr>
                                            </table>                               
                                            <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                                 <tr>
                                                    <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
                                                    <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="button" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                        </label></td>
                                                      <td align="right" class="prevNext-container"><ul>
                                                            <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                            <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                            <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                        </ul></td>
                                                   </tr>
                                               </table>                                                                                          
                                            <input type="hidden" id="pageNo" value="1"/>
                                            <input type="hidden" id="size" value="20"/>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
