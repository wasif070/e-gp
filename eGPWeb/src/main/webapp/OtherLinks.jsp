<%-- 
    Document   : OtherLinks
    Created on : Jan 28, 2011, 12:23:19 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; " />
        <title>Other Links</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        </script>
        </head>
        <body>
        <%
            String lang=null;
            String otherlinks=null;

            if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
                lang = request.getParameter("lang");
            }else{
                lang = "en_US";
            }

            MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
            List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"otherlinks");

            if(!langContentList.isEmpty())
            {
                for(TblMultiLangContent tblMultiLangContent:langContentList)
                {
                    if(tblMultiLangContent.getSubTitle().equals("other_links")){
                        if("bn_IN".equals(lang)){
                            otherlinks = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                        }else{
                            otherlinks = new String(tblMultiLangContent.getValue());
                        }
                     }
                }
             }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                        
                       <td class="contentArea-Blogin">
                             <div class="pageHead_1">Other Links</div>
                             <div class="atxt_1 c_t_space">
                             <%=otherlinks%>
                            </div>
                        </td>
                      </tr>
                   </table>
                <!--Middle Content Table Start-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
