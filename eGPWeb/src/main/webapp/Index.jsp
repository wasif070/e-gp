<%--
    Document   : Index
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : yanki,Rajesh for (Announcement and News scroller)
--%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="com.lowagie.text.pdf.codec.Base64.OutputStream"%>
<%@page import="com.cptu.egp.eps.web.utility.FilePathUtility"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvertisementService"%>
<%
    String uri = new String(request.getRequestURL());
    if (uri.contains("http://eprocure.gov.bd/")) {
        response.sendRedirect("http://www.eprocure.gov.bd/Index.jsp");
    } else if (uri.contains("http://staging.eprocure.gov.bd/")) {
        response.sendRedirect("http://www.staging.eprocure.gov.bd/Index.jsp");
    } else if (uri.contains("http://training.eprocure.gov.bd/")) {
        response.sendRedirect("http://www.training.eprocure.gov.bd/Index.jsp");
    } else if (uri.contains("https://inhouse.eprocure.gov.bd/")) {
        response.sendRedirect("https://www.inhouse.eprocure.gov.bd/Index.jsp");
    } else if (uri.contains("https://www.development.eprocure.gov.bd/") || uri.contains("https://www.development.eprocure.gov.bd/Index.jsp")) {
        response.sendRedirect("http://development.eprocure.gov.bd/Index.jsp");
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; " />
        <title>Welcome to e-GP</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                var pauseA = 2500;
                var pause = 3500;
                var browserName = "";
                var browserVer = "";

                function newstickerA()
                {
                    var temp1 = document.getElementById("listtickerA");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#listtickerA li:last').hide().remove();
                            $('ul#listtickerA').prepend(last);
                            $('ul#listtickerA li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(newstickerA, pauseA);
                
                var pauseB = 2500;
                function newstickerB()
                {
                    var temp1 = document.getElementById("listtickerB");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#listtickerB li:last').hide().remove();
                            $('ul#listtickerB').prepend(last);
                            $('ul#listtickerB li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(newstickerB, pauseB);
                
                var pauseC = 2500;
                function newstickerC()
                {
                    var temp1 = document.getElementById("listtickerC");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#listtickerC li:last').hide().remove();
                            $('ul#listtickerC').prepend(last);
                            $('ul#listtickerC li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(newstickerC, pauseC);
                
                var pauseD = 2500;
                function newstickerD()
                {
                    var temp1 = document.getElementById("listtickerD");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#listtickerD li:last').hide().remove();
                            $('ul#listtickerD').prepend(last);
                            $('ul#listtickerD li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(newstickerD, pauseD);

//                function newsticker()
//                {
//                    var temp1=document.getElementById("listticker");
//                    if(temp1!=null){
//                        if(temp1.getElementsByTagName("li").length >10){
//                            last = $('ul#listticker li:last').hide().remove();
//                            $('ul#listticker').prepend(last);
//                            $('ul#listticker li:first').slideDown("slow");
//                        }
//                    }
//                }
                //interval = setInterval(newsticker, pause);

                /*var allowBrowser = false;
                 jQuery.each(jQuery.browser, function(i, val) {
                 browserVer = jQuery.browser.version;
                 if(i == "mozilla" && browserVer.substr(0,3) == "1.9"){
                 allowBrowser = true;
                 }
                 if(i == "msie" && (browserVer.substr(0,3) == "8.0" || browserVer.substr(0,3) == "7.0")){
                 allowBrowser = true;
                 }
                 });
                 
                 if(!allowBrowser){
                 window.location.href = "BrowserInst.jsp";
                 //document.getElementById("browserDiv").style.display = "block";
                 }*/
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                var pauseA = 5000;
                var pause = 3500;
                var browserName = "";
                var browserVer = "";

                function listtickerA()
                {
                    var temp1 = document.getElementById("newsTickerA");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#newsTickerA li:last').hide().remove();
                            $('ul#newsTickerA').prepend(last);
                            $('ul#newsTickerA li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(listtickerA, pauseA);
                
                var pauseB = 5000;
                function listtickerB()
                {
                    var temp1 = document.getElementById("newsTickerB");

                    if (temp1 != null) {
                        if (temp1.getElementsByTagName("li").length > 2) {
                            last = $('ul#newsTickerB li:last').hide().remove();
                            $('ul#newsTickerB').prepend(last);
                            $('ul#newsTickerB li:first').slideDown("slow");
                        }
                    }
                }
                interval = setInterval(listtickerB, pauseB);

//                function newsticker()
//                {
//                    var temp1=document.getElementById("listticker");
//                    if(temp1!=null){
//                        if(temp1.getElementsByTagName("li").length >10){
//                            last = $('ul#listticker li:last').hide().remove();
//                            $('ul#listticker').prepend(last);
//                            $('ul#listticker li:first').slideDown("slow");
//                        }
//                    }
//                }
                //interval = setInterval(newsticker, pause);

                /*var allowBrowser = false;
                 jQuery.each(jQuery.browser, function(i, val) {
                 browserVer = jQuery.browser.version;
                 if(i == "mozilla" && browserVer.substr(0,3) == "1.9"){
                 allowBrowser = true;
                 }
                 if(i == "msie" && (browserVer.substr(0,3) == "8.0" || browserVer.substr(0,3) == "7.0")){
                 allowBrowser = true;
                 }
                 });
                 
                 if(!allowBrowser){
                 window.location.href = "BrowserInst.jsp";
                 //document.getElementById("browserDiv").style.display = "block";
                 }*/
            });
        </script>
        <script language="javascript">
            <!--
            function doBlink() {
                var blink = document.all.tags("BLINK")
                for (var i = 0; i < blink.length; i++)
                    blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : ""
            }

            function startBlink() {
                if (document.all)
                    setInterval("doBlink()", 1000)
            }
            window.onload = startBlink;
            // -->
        </script>
    </head>
    <body>
        <%
            String lang = null, aboutegp = null, readmore = null, announcements = null, news = null, content_impmsg = null;

            if (request.getParameter("lang") != null && request.getParameter("lang") != "") {
                lang = request.getParameter("lang");
            } else {
                lang = "en_US";
            }
            /*This Code is For RssFeed*/
            if (request.getRequestURL() != null) {
                AppMessage appMessage = new AppMessage();
                String top_str = request.getRequestURL().toString();
                int i_top = 0;
                int j_top = 0;
                for (i_top = 0; i_top < 3; i_top++) {
                    j_top = top_str.lastIndexOf('/');
                }
                top_str = top_str.substring(0, j_top + 1);
                appMessage.setAppUrl(top_str);
                appMessage = null;
            }/*RssFeed Code ends here*/
            MultiLingualService multiLingualService = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");

            String home = null;
            List<TblMultiLangContent> homeContentList = multiLingualService.findContent(lang, "home");

            if (!homeContentList.isEmpty()) {
                for (TblMultiLangContent homeContent : homeContentList) {
                    if (homeContent.getSubTitle().equals("lbl_announcements")) {
                        if ("bn_IN".equals(lang)) {
                            announcements = BanglaNameUtils.getUTFString(homeContent.getValue());
                        } else {
                            announcements = new String(homeContent.getValue());
                        }
                    }
                    if (homeContent.getSubTitle().equals("lbl_news")) {
                        if ("bn_IN".equals(lang)) {
                            news = BanglaNameUtils.getUTFString(homeContent.getValue());
                        } else {
                            news = new String(homeContent.getValue());
                        }
                    }
                    if (homeContent.getSubTitle().equals("content_home")) {
                        if ("bn_IN".equals(lang)) {
                            home = BanglaNameUtils.getUTFString(homeContent.getValue());
                        } else {
                            home = new String(homeContent.getValue());
                        }
                    }
                    if (homeContent.getSubTitle().equals("content_impmsgdetails")) {
                        if ("bn_IN".equals(lang)) {
                            content_impmsg = BanglaNameUtils.getUTFString(homeContent.getValue());
                        } else {
                            content_impmsg = new String(homeContent.getValue());
                        }
                    }
                }
            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">

                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0 20px;">
                        <tr valign="top">
                           
                            <td class="contentArea-Blogin">
                                <%if ("y".equals(request.getParameter("ExistsSession"))) {%>
                                <div id="errMsg" class="responseMsg errorMsg" style="text-align: justify; margin-left:17px; margin-right: 2px; margin-top: 15px;">Already Logged in as another user!</div>
                                <%}%>
                                <br />
                                <%if ("y".equals(request.getParameter("succMsg"))){%>
                                <div id="succMsg" class="responseMsg successMsg" style="font-size: large; margin: 15px auto; box-shadow: 0 0 45px darkslateblue; border: 1px solid powderblue; border-radius: 10px; ">An e-mail has been sent to your registered e-mail ID for verification. Bidder will have to verify the e-mail ID within <%=XMLReader.getMessage("FinalSubMsg")%> from the Date of Registration otherwise the profile will be removed.</div>
                                <%}%>
                            <!--Page Content Start-->


                            <!--                            <marquee width=100% behavior=scroll scrollamount="3" style="height:25px; line-height:25px; color:#ff0000; font-size: 16px; font-weight: bold;">Development Server - This is Test Website only. Live website will be launched soon.</marquee>-->
                            <div>
                                <%=home%>
                                <!--                               <div class="pageHead_1">
                                                                   <strong>About e-Government Procurement (e-GP) System</strong></div>
                                                               <div class="atxt_1 c_t_space">
                                                                National e-Government Procurement (e-GP) portal ( i.e. <a href="http://www.eprocure.gov.bd/" target="_blank">http://www.eprocure.gov.bd</a> )of the Government of Bangladesh is developed, owned and operated by Central Procurement Technical Unit (CPTU), IMED, Ministry of Planning for carrying out the procurement activities of the public agencies (procuring agencies and procuring entities) of the Government of Bangladesh, enhance the efficiency and transparency in public procurement and increase the access to &nbsp;through the implementation of a comprehensive e-GP solution to be used by all government organizations in the Country. Introducing e-GP System in Bangladesh is being carried out under the Public Procurement Reform Project ? II with the support from the World Bank.
                                                               </div> -->
                                <div class="ReadMore">
<!--                                    <a href="<%=request.getContextPath()%>/aboutUs.jsp" target="_self">Read More &gt;&gt;</a>-->
                                </div>
                            </div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space" style="margin-top:21px;">
                                <tr valign="top">
                                    <td width="50%">
                                        <div class="nlistBox_1" style="width: 89%; height: 600px;">
                                            <h3><%=announcements%></h3>
                                            <ul id="listtickerA">
                                                <%
                                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> cmblocList = tenderCommonService.returndata("getAnnouncements", null, null);
                                                    for (SPTenderCommonData sptcd : cmblocList) {
                                                        if(sptcd.getFieldName3().equals("N"))
                                                        {
                                                %>
                                                <li style="display: block; margin-bottom: 5px;">
                                                    <a href="ViewNews.jsp?newsType=N&nId=<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName5()%></a>&nbsp;
                                                    <% if ("Yes".equalsIgnoreCase(sptcd.getFieldName4())) { %>
                                                    <span style="color: red;font-weight: bold; font-style: italic"><blink>- Important</blink></span>
                                                            <% } %>
                                                </li>
                                                <%}}%>
                                            </ul>
                                        </div>
                                    </td>
                                    <td width="50%">
                                        <div class="nlistBox_1" style="width: 89%; height:185px;">
                                            <!--<h3><%//=news%></h3>-->
                                            <h3>Circular</h3>
                                            <ul id="listtickerB">
                                                <!--<li><h3>Circular</h3></li>-->
                                            <%
                                                for (SPTenderCommonData sptcdCircular : cmblocList) {
                                                        if(sptcdCircular.getFieldName3().equals("C"))
                                                        {
                                            %>
                                            <li style="display: block; margin-bottom: 5px;">
                                                <!--<label style="color: purple;font-weight: bold; font-style: italic">Circular: </label>-->
                                                <a href="ViewNews.jsp?newsType=<%=sptcdCircular.getFieldName3()%>&nId=<%=sptcdCircular.getFieldName1()%>" ><%=sptcdCircular.getFieldName5()%></a>&nbsp;
                                                <% if (!sptcdCircular.getFieldName7().equals("") && sptcdCircular.getFieldName7()!=null) { %>
                                                    <span><%=DateUtils.SqlStringDatetoApplicationStringDateWithoutTime(sptcdCircular.getFieldName7())%></span>
                                                <% } %>
                                                <% if ("Yes".equalsIgnoreCase(sptcdCircular.getFieldName4())) { %>
                                                    <span style="color: red;font-weight: bold; font-style: italic"><blink>- Important</blink></span>
                                                            <% } %>
                                            </li>
                                            <%}}%>
                                            </ul>
                                        </div>
                                        <div class="nlistBox_1" style="width: 89%; height:172px; margin-top: 10px;">
                                            <h3>Amendment</h3>
                                            <ul id="listtickerC">
                                            <%
                                                for (SPTenderCommonData sptcdAmendment : cmblocList) {
                                                        if(sptcdAmendment.getFieldName3().equals("A"))
                                                        {
                                            %>
                                            <li style="display: block; margin-bottom: 5px;">
                                                <!--<label style="color: purple;font-weight: bold; font-style: italic">Amendment: </label>-->
                                                <a href="ViewNews.jsp?newsType=<%=sptcdAmendment.getFieldName3()%>&nId=<%=sptcdAmendment.getFieldName1()%>" ><%=sptcdAmendment.getFieldName5()%></a>&nbsp;
                                                <% if (!sptcdAmendment.getFieldName7().equals("") && sptcdAmendment.getFieldName7()!=null) { %>
                                                    <span><%=DateUtils.SqlStringDatetoApplicationStringDateWithoutTime(sptcdAmendment.getFieldName7())%></span>
                                                <% } %>    
                                                <% if ("Yes".equalsIgnoreCase(sptcdAmendment.getFieldName4())) { %>
                                                    <span style="color: red;font-weight: bold; font-style: italic"><blink>- Important</blink></span>
                                                            <% } %>
                                            </li>
                                            <%}}%>
                                            </ul>
                                        </div>
                                        <div class="nlistBox_1" style="width: 89%; height:172px;  margin-top: 10px;">
                                            <h3>Notification</h3>
                                            <ul id="listtickerD">
                                            <%
                                                for (SPTenderCommonData sptcdInstruction : cmblocList) {
                                                        if(sptcdInstruction.getFieldName3().equals("I"))
                                                        {
                                            %>
                                            <li style="display: block; margin-bottom: 5px;">
                                                <!--<label style="color: purple;font-weight: bold; font-style: italic">Instruction/Guideline: </label>-->
                                                <a href="ViewNews.jsp?newsType=<%=sptcdInstruction.getFieldName3()%>&nId=<%=sptcdInstruction.getFieldName1()%>" ><%=sptcdInstruction.getFieldName5()%></a>&nbsp;
                                                <% if (!sptcdInstruction.getFieldName7().equals("") && sptcdInstruction.getFieldName7()!=null) { %>
                                                    <span><%=DateUtils.SqlStringDatetoApplicationStringDateWithoutTime(sptcdInstruction.getFieldName7())%></span>
                                                <% } %>  
                                                <% if ("Yes".equalsIgnoreCase(sptcdInstruction.getFieldName4())) { %>
                                                    <span style="color: red;font-weight: bold; font-style: italic"><blink>- Important</blink></span>
                                                            <% } %>
                                            </li>
                                            <%}}%>
                                            </ul>
                                        </div>
                                            <!--                                            <ul>-->
                                            <%
                                                //List<SPTenderCommonData> cmblocListN = tenderCommonService.returndata("getNews", null, null);
                                                //for (SPTenderCommonData sptcd : cmblocListN) {
%>
                                            <!--                                                <li>                                                    -->
                                            <%--<%=content_impmsg%>--%>
                                            <!--<ul id="newsTickerAvv">
                                                <li style="display: block;text-align:justify;">
                                                   The Government of the People&#39;s Republic of Bangladesh has approved the e-GP guidelines in pursuant to Section 65 of the Public Procurement Act, 2006. As per approved guidelines, e-GP system is being introduced in two phases.  <br />

                                                </li>
                                               &nbsp;
                                                <li style="display: block;text-align:justify;">
                                                   In the first phase, e-Tendering will primarily be introduced on pilot basis, in the CPTU and 16 (sixteen) Procuring Entities (PEs) under 4 (four) sectoral agencies, namely:  Bangladesh Water Development Board (BWDB), Local Government Engineering Department (LGED), Roads and Highways Department (RHD) and Rural Electrification Board (REB). The system will gradually be rolled out to 291 PEs of those 4 sectoral agencies up to district level and ultimately it will be expanded to all the PEs of the government. <br />
                                                </li>
                                                &nbsp;
                                                <li style="display: block;text-align:justify;">
                                                   In the second phase,  e-Contract Management System (e-CMS) will be introduced covering  complete Contract Management processes such as work plan submission, defining milestone, tracking and monitoring progress, generating reports, performing quality checks, generation of running bills, vendor rating and generation of completion certificate.<br />
                                                </li>
                                                &nbsp;
                                                <li style="display: block;">
                                                  For further details, please contact <span class="atxt_1"><a href="HelpDesk.jsp">help desk.</a></span> <br />
                                                </li>
                                            </ul>-->

                                            <!--                                                </li>-->
                                            <%
                                                //}
                                            %>
                                            <!--                                            </ul>-->
                                        <!--</div>-->
                                    </td>
                                </tr>
                            </table>
                            <%try {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
                                    List<TblAdvertisement> ads = advertisementService.findTblAdvertisement("status", Operation_enum.EQ, "published", "location", Operation_enum.EQ, "Bottom", "expiryDate", Operation_enum.GE, format.parse(format.format(new Date())));
                                    if (ads.size() > 0) {%>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space" style="margin-top:px;">
                                <tr valign="top">
                                    <td width="50%">
                                        <div class="" style="width: 96%; height: 150px;">
                                            <%if (ads.size() > 1) {%>
                                            <marquee  scrollamount="3" behavior="scroll">
                                                <div class="">
                                                    <%for (TblAdvertisement tbl : ads) {
                                                            String[] a = tbl.getDimension().split("x");%>
                                                    <a href="<%=tbl.getUrl()%>" target="_blank">
                                                        <img class="linkIcon_1" title="<%=tbl.getDescription()%>" alt="<%=tbl.getDescription()%>" src="<%=request.getContextPath()%>/MakeAdvertisement?funcName=image&adId=<%=tbl.getAdId()%>" HEIGHT="<%=a[0]%>" WIDTH="<%=a[1]%>"/>
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <%}%>
                                            </marquee>
                                        </div>
                                        <%} else {%>
                                        <div class="">
                                            <%for (TblAdvertisement tbl : ads) {
                                                    String[] a = tbl.getDimension().split("x");%>%>
                                            <a href="<%=tbl.getUrl()%>" target="_blank">
                                                <img class="linkIcon_1" title="<%=tbl.getDescription()%>" alt="<%=tbl.getDescription()%>" src="<%=request.getContextPath()%>/MakeAdvertisement?funcName=image&adId=<%=tbl.getAdId()%>" HEIGHT="<%=a[0]%>" WIDTH="<%=a[1]%>"/>
                                            </a>
                                            &nbsp;&nbsp;&nbsp;
                                            <%}%>
                                        </div>
                                        <%}%>
                                        
                                    </td>
                                </tr>
                            </table>
                            <%}
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            %>
                            <!--Page Content End-->
                        </td>
                            <td width="266" style="background-color: #DDD">

                                <!-- Dohatec Start -->
                                <!--<a href="/Index.jsp">
                                    <img style="width: 245px; height: 70px; margin-left: 15px;" alt="eGP" src="/resources/images/e-GPBannerAnim.gif"/>
                                </a>-->
                                <!-- margin-top: 15px; -->
                                <!-- Dohatec End -->
                            <%
                                String msg = "";
                                msg = request.getParameter("msg");
                                if (msg != null && !"".equalsIgnoreCase(msg) && msg.equals("success")) {
                            %>
                            <div class="responseMsg successMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">Password changed successfully.</div>
                            <%                                            }
                                //out.println(request.getHeader("user-agent"));  yrj
                            %>
                            
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                            </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>