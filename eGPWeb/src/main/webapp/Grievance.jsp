<%-- 
    Document   : Grievance
    Created on : Jun 15, 2016, 11:02:11 AM
    Author     : Sristy
--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Grievance</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">

        </script>
    </head>
    <body>
        <%--
            request.getParameter("IRBMembers");  
            request.getParameter("IRBRules");
            request.getParameter("FlowChart");
            request.getParameter("Decision");
        --%>

        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->
                <% if ("IRBMembers".equalsIgnoreCase(request.getParameter("submenu").toString())) { %> 
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea-Blogin">
                            <div class="pageHead_1">IRB Members</div>   

                            <table border="0" cellpadding="0" cellspacing="10" class="formBg_1 t_space" style="" width="100%">
                                <tr>
                                    <td>
                                        As per the Chapter VIII of the Procurement Rules and Regulations 2009, Ministry of Finance hereby establishes an Independent Review Body (IRB) composed of seven members from following Ministries/Agencies:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        <br/>  1. Mr. Choiten Wangchuk, Director General, Department of Public Accounts, Ministry of Finance;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        2. Mr. Rinchen Dorji, Director, Intellectual Property Division, Ministry of Economic Affairs;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        3.  Mr. Tenzin,Director, Department of Engineering Services, Ministry of Works and Human Settlements;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        4. Mr. Tirtha Raj Gurung, Executive Engineer, Construction Development Board;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        5. Mr. Wangdi Gyelsthen, General Secretary, Construction Association of Bhutan;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        6. Mr. Kesang Wangdi, Deputy Secretary General, Bhutan Chamber of Commerce; and
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        7. Mr. Tashi Gyalpo, Chief Attorney, Office of Attorney General
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br/> The IRB shall function in accordance with the Rules and Procedures of IRB February, 2015 issued by Ministry of Finance. The above committee members shall serve for a term of three years which shall be effective from 1st July 2015.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br/>  The Government Procurement and Property Management Division shall serve as the Secretariat to the IRB.
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->     
                        </td>
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                            </td>
                        </tr>
                    </table>
                <% } %>

                <% if ("IRBRules".equalsIgnoreCase(request.getParameter("submenu"))) { %>        
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea-Blogin formStyle_1">  
                            <div class="pageHead_1">Rules and Procedure of IRB</div>     
                            <a style="margin-top: 50px;" href="Independent Review Body - Rules and Procedures.pdf"  target="_blank" class="">Rules and Procedure of Independent Review Body (IRB)</a>
                        </td>
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                        </tr>
                    </table>
                <% } %>

                <% if ("FlowChart".equalsIgnoreCase(request.getParameter("submenu"))) { %>        
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea-Blogin formStyle_1">  
                            <div class="pageHead_1">Complaint Process Flow Chart</div>     
                            <img target="_blank" style="margin-top: 10px; float: left; border-width: .5px; border-style: solid; width: 700px; height: 799px;" src="IRB Flow Chart.jpg" alt=""/>
                        </td>
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                            </td>
                        </tr>
                    </table>
                <% } %>

                <% if ("Decision".equalsIgnoreCase(request.getParameter("submenu"))) { %>        
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea-Blogin formStyle_1">  
                            <div class="pageHead_1">Decision</div>  
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <tr>
                                    <th width="04%" class="t-align-center">Sl. No.</th>
                                    <th width="86%" class="t-align-center">Decision</th>
                                </tr>
                                <tr>
                                    <td width="04%" class="t-align-center">1</td>
                                    <td width="86%" class="t-align-center">
                                        <a href="DecisionCase 01.pdf"  target="_blank" class="">Decision 01</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="04%" class="t-align-center">2</td>
                                    <td width="86%" class="t-align-center">
                                        <a href="DecisionCase 02.pdf"  target="_blank" class="">Decision 02</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="04%" class="t-align-center">3</td>
                                    <td width="86%" class="t-align-center">
                                        <a href="DecisionCase 03.pdf"  target="_blank" class="">Decision 03</a>
                                    </td>
                                </tr>

                            </table>
                        </td>
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                            </td>
                        </tr>
                    </table>
                <% }%>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
