<%--
    Document   : Authenticate
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.model.table.TblTempTendererEsignature"%>
<html xmlns="http://www.w3.org/1999/xhtml">   
    <jsp:useBean id="tempCompanyDocumentsSrBean" class="com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean"/>    
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Attach e-Signature</title>
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile:{required: true},
                        documentBrief:{required:true}

                    },
                    messages: {
                        uploadDocFile:{ required: "<div class='reqF_1'> Please select Document.</div>"},

                        documentBrief:{ required: "<div class='reqF_1'> Please Enter Description of Document.</div>"}
                    }
                }
            );
            });

        </script>

        <script type="text/javascript" src="resources/js/upload.js"> </script>
        <script type="text/javascript" src="dwr/interface/UploadMonitor.js"> </script>
        <script type="text/javascript" src="dwr/engine.js"> </script>
        <script type="text/javascript" src="dwr/util.js"> </script>
        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <style type="text/css">
            body { font: 11px Lucida Grande, Verdana, Arial, Helvetica, sans serif; }
            #progressBar { padding-top: 5px; }
            #progressBarBox { width: 350px; height: 20px; border: 1px inset; background: #eee;}
            #progressBarBoxContent { width: 0; height: 20px; border-right: 1px solid #444; background: #9ACB34; }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#btnFinal').click(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {funName:'addUser'},  function(j){
                        if(j.toString()=="User created."){
                            jAlert("Your Profile is successfully submitted to the administrator for review. You will be notified for approval / rejection status of your profile through your registered mail id","Successful Registered", function(RetVal) {
                            });
                            //dont use window.location.href Yagnesh
                            // window..location..href="<%//request.getContextPath()%>/UserStatus.jsp?status=p";
                        }else{
                            jAlert("User not Created.","User creation alert", function(RetVal) {
                            });
                        }
                         
                    });
                });
            });
        </script>
    </head>
    <body>
        <%
            tempCompanyDocumentsSrBean.setLogUserId(session.getAttribute("userId").toString());
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">New User Registration - Attach e-Signature</div>
                            <jsp:include page="EditNavigation.jsp" ></jsp:include>
                            <%
                                            if(request.getParameter("fq")!=null){
                                            %>
                                            <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                                            <%
                                            }if(request.getParameter("fs")!=null){
                                            %>
                                            <div class="responseMsg errorMsg">
                                                Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                                            </div>
                                            <%
                                            }
                                            %>
                            <form  id="frmUploadDoc" method="post" action="FileUploadServlet" enctype="multipart/form-data" name="frmUploadDoc"><%-- onsubmit="startProgress()"--%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">e-Signature : <span>*</span></td>
                                        <td><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:250px; background:none;"/>
                                            <!--
                                            <div class="t_space"><img src="resources/images/Dashboard/uploadIcn.png" alt="Upload" class="linkIcon_1" /><a href="#" title="Upload">Upload</a></div>
                                            -->
                                            <br/>
                                            <div id="progressBar" style="display: none;">

                                                <div id="theMeter">
                                                    <div id="progressBarText"></div>

                                                    <div id="progressBarBox">
                                                        <div id="progressBarBoxContent"></div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <%
                                                        String jvca = tempCompanyDocumentsSrBean.checkJVCA(Integer.parseInt(request.getSession().getAttribute("userId").toString()));//Integer.parseInt(request.getParameter("userId").toString())
                                                        int tendererId = Integer.parseInt(jvca.substring(0, jvca.length() - 1));
                                            %>
                                            <input type="hidden" value="<%=tendererId%>" name="tendererId" id="hdnTendererId"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Description : <span>*</span></td>
                                        <td><input type="hidden" name="funName" value="uploaddoc" id="hdnFunction"/>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" id="txtDescription" style="width:200px;"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><%--<label class="formBtn_1">
                                                <input type="submit" name="btnSubmit" id="btnSubmit" value="Previous" />
                                            </label>
                                            &nbsp;--%>
                                            <%
                                                        java.util.List<TblTempTendererEsignature> esignList = new java.util.ArrayList<TblTempTendererEsignature>();
                                                        String msg = "";
                                                        esignList = tempCompanyDocumentsSrBean.getTblTempEsign(tendererId);
                                                        int cnt = cnt = esignList.size();
                                                        if ("Next".equals(request.getParameter("btnNext"))) {
                                                            if (esignList.isEmpty()) {
                                                                msg = "Please upload atleast one document.";
                                                                response.sendRedirect("SupportingDocuments.jsp");
                                                            } else {
                                                                response.sendRedirect("Authenticate.jsp");
                                                            }
                                                        }

                                                        if (cnt == 0) {
                                            %>
                                            <label class="formBtn_1">
                                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                                            </label>
                                            &nbsp;                                                                                      
                                            <%
                                                        }
                                           %>
                                        </td>
                                    </tr>
                                </table>
                            </form>                             
                            <%
                                        if (!msg.equals("")) {
                            %>
                            <div class="responseMsg errorMsg"><%=msg%></div>
                            <%
                                        }
                            %>
                            <div>&nbsp;</div>                            
                            <table width="100%" cellspacing="0" class="tableList_2">
                                <tr>
                                    <th style="text-align:center; width:4%;">Sl.  No.</th>
                                    <th>Document Name</th>
                                    <th>Document Description</th>
                                    <th>File Size</th>
                                    <th style="text-align:center; width:10%;">Action</th>
                                </tr>
                                <%
                                            int count = 1;
                                            for (TblTempTendererEsignature ttte : esignList) {
                                %>
                                <tr>
                                    <td style="text-align:center;"><%=count%></td>
                                    <td><%=ttte.getDocumentName()%></td>
                                    <td><%=ttte.getDocumentBrief()%></td>
                                    <td><%=ttte.getDocumentSize()%></td>
                                    <td style="text-align:center;">                                        
                                        <a href="SupportDocServlet?docId=<%=ttte.getEsignatureId()%>&docName=<%=ttte.getDocumentName()%>&funName=removeEsign" title="Remove"><img src="resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                                <%
                                                count++;
                                            }
                                %>
                            </table>
                            <br/>
                            <div align="center">
                            <%
                            if (cnt != 0) {
                                            %>

                            <label class="formBtn_1">
                                   <input type="button" name="btnFinal" id="btnFinal" value="Final Submission"/>
                           </label>
                            <%
                                                        }
                            %>
                            </div>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
