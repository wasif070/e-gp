<%-- 
    Document   : GenerServlet
    Created on : Dec 2, 2011, 4:04:56 PM
    Author     : taher
--%>

<%@page import="java.io.FileWriter"%>
<%@page import="java.io.File"%>
<%
    try {
            String[] tabNames = request.getParameterValues("tabName");
            String daoPath = request.getParameter("daoPath");
            String implPath = request.getParameter("implPath");
            String importVar = request.getParameter("importVar");
            String action = request.getParameter("action");
            if ("genDao".equals(action)) {
                for (String tabName : tabNames) {
                    StringBuilder daoStr = new StringBuilder();
                    daoStr.append("package " + importVar + ".dao.daointerface;");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("import " + importVar + ".dao.generic.GenericDao;");
                    daoStr.append("\n");
                    daoStr.append("import " + importVar + ".model.table." + tabName + ";");
                    daoStr.append("\n");
                    daoStr.append("import java.util.List;");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic interface " + tabName + "Dao extends GenericDao<" + tabName + "> {");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic void add" + tabName + "(" + tabName + " tblObj);");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic void delete" + tabName + "(" + tabName + " tblObj);");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic void update" + tabName + "(" + tabName + " tblObj);");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic List<" + tabName + "> getAll" + tabName + "();");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic List<" + tabName + "> find" + tabName + "(Object... values) throws Exception;");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic List<" + tabName + "> findByCount" + tabName + "(int firstResult,int maxResult,Object... values) throws Exception;");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic long get" + tabName + "Count();");
                    daoStr.append("\n");
                    daoStr.append("\n");
                    daoStr.append("\tpublic void updateOrSave" + tabName + "(List<" + tabName + "> tblObj);");
                    daoStr.append("\n");
                    daoStr.append("}");


                    StringBuilder implStr = new StringBuilder();
                    implStr.append("package " + importVar + ".dao.daoimpl;");
                    implStr.append("\n");
                    implStr.append("\n");
                    implStr.append("\n");
                    implStr.append("import " + importVar + ".dao.daointerface." + tabName + "Dao;");
                    implStr.append("\n");
                    implStr.append("import " + importVar + ".dao.generic.AbcAbstractClass;");
                    implStr.append("\n");
                    implStr.append("import " + importVar + ".model.table." + tabName + ";");
                    implStr.append("\n");
                    implStr.append("import java.util.List;");
                    implStr.append("\n");
                    implStr.append("\n");
                    implStr.append("\n");
                    implStr.append("public class " + tabName + "Impl extends AbcAbstractClass<" + tabName + "> implements " + tabName + "Dao {");
                    implStr.append("\n");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic void add" + tabName + "(" + tabName + " master){");
                    implStr.append("\n");
                    implStr.append("\t\tsuper.addEntity(master);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic void delete" + tabName + "(" + tabName + " master) {");
                    implStr.append("\n");
                    implStr.append("\t\tsuper.deleteEntity(master);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic void update" + tabName + "(" + tabName + " master) {");
                    implStr.append("\n");
                    implStr.append("\t\tsuper.updateEntity(master);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic List<" + tabName + "> getAll" + tabName + "() {");
                    implStr.append("\n");
                    implStr.append("\t\treturn super.getAllEntity();");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic List<" + tabName + "> find" + tabName + "(Object... values) throws Exception {");
                    implStr.append("\n");
                    implStr.append("\t\treturn super.findEntity(values);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic long get" + tabName + "Count() {");
                    implStr.append("\n");
                    implStr.append("\t\treturn super.getEntityCount();");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic List<" + tabName + "> findByCount" + tabName + "(int firstResult, int maxResult, Object... values) throws Exception {");
                    implStr.append("\n");
                    implStr.append("\t\treturn super.findByCountEntity(firstResult, maxResult, values);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("\t@Override ");
                    implStr.append("\n");
                    implStr.append("\tpublic void updateOrSave" + tabName + "(List<" + tabName + "> tblObj) {");
                    implStr.append("\n");
                    implStr.append("\t\tsuper.updateAll(tblObj);");
                    implStr.append("\n");
                    implStr.append("\t}");
                    implStr.append("\n");
                    implStr.append("}");
                    File daoFile = new File(daoPath + "\\" + tabName + "Dao.java");
                    daoFile.createNewFile();
                    FileWriter daofw = new FileWriter(daoFile);
                    daofw.write(daoStr.toString());
                    daofw.flush();
                    daofw.close();

                    File implFile = new File(implPath + "\\" + tabName + "Impl.java");
                    implFile.createNewFile();
                    FileWriter implfw = new FileWriter(implFile);
                    implfw.write(implStr.toString());
                    implfw.flush();
                    implfw.close();
                }
                response.sendRedirect(request.getContextPath() + "/GenerateDao.jsp");
            }
        } catch (Exception e) {
            System.out.println("Error : " + e);
        }
%>