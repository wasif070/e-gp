<%--
    Document   : FAQ
    Created on : May 31-05-2017
    Author     : Nitish Ranjan Bhowmik
--%>
<%@page import="com.cptu.egp.eps.model.table.TblFaqBhutan"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FaqBhutanService"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP FAQ</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<style>
/*FAQS*/
/*----- Accordion -----*/
.accordion, .accordion * {
    -webkit-box-sizing:border-box; 
    -moz-box-sizing:border-box; 
    box-sizing:border-box;
    box-decoration-break: clone;
}
 
.accordion {
    overflow:hidden;
    box-shadow:0px 1px 3px rgba(0,0,0,0.25);
    border-radius:3px;
    background:#f7f7f7;
}
 
/*----- Section Titles -----*/
.accordion-section-title {
    width:100%;
    padding:7px;
    display:inline-block;
    border-bottom:1px solid #1a1a1a;
    
    transition:all linear 0.100s;
    /* Type */
    font-size:1em;
    font-weight: bold;
    
    color:#FF9326;
}
 
.accordion-section-title.active, .accordion-section-title:hover {
    background:#4c4c4c;
    /* Type */
    text-decoration:none;
}
 
.accordion-section:last-child .accordion-section-title {
    border-bottom:none;
}
 
/*----- Section Content -----*/
.accordion-section-content {
    padding:15px;
    display:none;
}
        </style>


    </head>
    <body>

        <script>
            $(document).ready(function () {
                function close_accordion_section() {
                    $('.accordion .accordion-section-title').removeClass('active');
                    $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
                }

                $('.accordion-section-title').click(function (e) {
                    // Grab current anchor value
                    var currentAttrValue = $(this).attr('href');

                    if ($(e.target).is('.active')) {
                        close_accordion_section();
                    } else {
                        close_accordion_section();

                        // Add active class to section title
                        $(this).addClass('active');
                        // Open up the hidden content panel
                        $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
                    }

                    e.preventDefault();
                });
            });

            $(document).ready(function () {
                function close_accordion_section() {
                    // Close everything up
                }

                $('.accordion-section-title').click(function () {
                    // Grab current anchor value
                    var currentAttrValue = $(this).attr('href');

                    // Open and close here
                });
            });



            $('.accordion-section-title').click(function () {
                // Grab current anchor value
                var currentAttrValue = $(this).attr('href');

                $('.accordion .accordion-section-title').removeClass('active');
                $('.accordion .accordion-section-content').slideUp(300).removeClass('open');

                $(this).addClass('active');
                $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
            });


            function close_accordion_section() {
                $('.accordion .accordion-section-title').removeClass('active');
                $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
            }
        </script>

        
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr valign="top">

                        <td class="ff contentArea-Blogin formStyle_1"><!--Page Content Start-->   
                            <div class="pageHead_1">
                                Frequently Asked Questions
                                <span style="float:right;"><a href="eLearning.jsp" class="action-button-goback">Go To e-Learning</a></span>
                
                            </div>
                            
                        </td>
                        
                    </tr>
                        
                        <tr valign="top">

                            <td class="contentArea-Blogin">
                            <%
                                FaqBhutanService FaqBhutanService = (FaqBhutanService) AppContext.getSpringBean("FaqBhutanService");
                                List<TblFaqBhutan> tblFaqBhutan = FaqBhutanService.getAllTblFaqBhutan();
                            %>

                            <%
                                int cnt = 0;
                                for (TblFaqBhutan tblFaqBhutanThis : tblFaqBhutan) {

                                    cnt++;
                            %>

                            

                            <div class="accordion">
                                <div class="accordion-section">
                                    <a class="accordion-section-title" href="#accordion-<%=cnt%>">
                                        <%=cnt%>. <%out.print(tblFaqBhutanThis.getQuestion());%>
                                    </a>

                                    <div id="accordion-<%=cnt%>" class="accordion-section-content">
                                        <p>
                                            <%out.print(tblFaqBhutanThis.getAnswer());%>
                                        </p>
                                    </div><!--end .accordion-section-content-->
                                </div><!--end .accordion-section-->
                            </div><!--end .accordion-->        


                            <% }%>

                        </td>
                        <%--<td width="266" class="td-background">
                           <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                       </td>--%>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
