<%-- 
    Document   : ConnCheck
    Created on : May 27, 2011, 2:06:18 PM
    Author     : eprocure
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.cptu.egp.eps.model.table.TblAuditTrailMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<jsp:useBean id="auditTrailMaster" class="com.cptu.egp.eps.web.servicebean.AuditTrailMasterSrBean"></jsp:useBean>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Connection Checking</title>
    </head>
    <body>
        <%
                    String sessionId = "";

                    if (session.getAttribute("sessionId") != null) {
                        sessionId = session.getAttribute("sessionId").toString();
                    }

                    if (sessionId != null && !"".equalsIgnoreCase(sessionId)) {
                        TblAuditTrailMaster tblAuditTrailMaster = new TblAuditTrailMaster();

                        String b_ipAddress = request.getHeader("X-FORWARDED-FOR");
                        if (b_ipAddress == null) {
                            b_ipAddress = request.getRemoteAddr();
                        }

                        String b_pageName = "";
                        b_pageName = request.getRequestURL().toString();

                        String b_pageReferer = "";
                        if (request.getHeader("referer") != null && !"".equalsIgnoreCase(request.getHeader("referer"))) {
                            b_pageReferer = request.getHeader("referer");
                            b_pageName = b_pageName + " , " + b_pageReferer;
                        }

                        int objectId = 0;
                        String module = "connection check";
                        tblAuditTrailMaster.setSessionId(Integer.parseInt(sessionId));
                        tblAuditTrailMaster.setIpAddress(b_ipAddress);
                        tblAuditTrailMaster.setAuditDt(new java.util.Date());
                        tblAuditTrailMaster.setPageName(b_pageName);
                        tblAuditTrailMaster.setObjectId(objectId);
                        tblAuditTrailMaster.setModule(module);

                        auditTrailMaster.addData(tblAuditTrailMaster);
                    }

                    NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                    long cnt = 0;
                    int loopCnt = 100;
                    for(int i=1;i<=loopCnt;i++){
                        cnt = navRuleUtil.getProcCnt();
                        out.println(" :: cnt -> " + cnt);
                        /*cnt = navRuleUtil.getPreTenderCnt();
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getAmendmentCnt();
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getTECCnt("TOC", "POC");
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getTECCnt("TEC", "PEC");
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getTECCnt("TSC", null);
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getSTDCnt();
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getNoaCnt();
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getEvalCnt();
                        out.println(", cnt -> " + cnt);
                        cnt = navRuleUtil.getPayCnt();
                        out.println(", cnt -> " + cnt);*/
                    }
        %>
        <%
                    org.apache.commons.dbcp.BasicDataSource bds = (org.apache.commons.dbcp.BasicDataSource) AppContext.getSpringBean("dataSource");
                    out.println("<br/> max active ->" + bds.getMaxActive());
                    out.println("<br/> active ->" + bds.getNumActive());
                    out.println("<br/> idle ->" + bds.getNumIdle());
                    out.println("<br/> testpereviction ->" + bds.getNumTestsPerEvictionRun());
                    out.println("<br/>"+ new java.util.Date());
        %>
    </body>
</html>
