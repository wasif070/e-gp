<%-- 
    Document   : Tenders
    Created on : Mar 21, 2011, 5:30:28 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reports</title>
<script type="text/javascript" src="resources/js/ddlevelsmenu.js"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="mainDiv">
    <div class="fixDiv">
         <jsp:include page="resources/common/Top.jsp" ></jsp:include>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td width="266">
                    <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                </td>
                <td class="contentArea-Blogin">
                    <div class="DashboardContainer">
                    <div class="pageHead_1">Reports</div>
                    <ul class="tabPanel_1 t_space">
                           <li><a href="RegistrationDetails.jsp">Registration Details</a></li>
                           <li><a href="AnnualProcurementPlans.jsp">Annual Procurement Plans </a></li>
                           <li><a href="Tenders.jsp" class="sMenu">Tenders/Proposals</a></li>
                         <!-- Edit By Palash, Dohatec-->
                           <li><a href="ReportDownload.jsp">Performance Indicators Report</a></li>
                         <!-- End-->
                    </ul>
                    <div class="tabPanelArea_1">
                      <table width="100%" cellspacing="0" class="tableList_3">
                        <tr>
                          <th width="20%">&nbsp; </th>
                          <th width="25%">No. of Tenders/Proposals
                            Invited</th>
                          <th width="30%">No. of Tenders/Proposals being
                            Processed</th>
                          <th width="25%">No. of Contracts Awarded</th>
                        </tr>
                          <%
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> reportData = commonSearchDataMoreService.getCommonSearchData("GetMDOTenderCount", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                            %>
                        <tr>
                          <td class="t-align-left ff">Ministry :</td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Ministry&status=Published"><%=reportData.get(0).getFieldName1()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Ministry&status=Processed"><%=reportData.get(0).getFieldName2()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Ministry&status=Finalized"><%=reportData.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                          <td class="t-align-left ff">Division :</td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Division&status=Published"><%=reportData.get(0).getFieldName4()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Division&status=Processed"><%=reportData.get(0).getFieldName5()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Division&status=Finalized"><%=reportData.get(0).getFieldName6()%></td>
                        </tr>
                        <tr>
                          <td class="t-align-left ff">Organization :</td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Organization&status=Published"><%=reportData.get(0).getFieldName7()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Organization&status=Processed"><%=reportData.get(0).getFieldName8()%></a></td>
                          <td class="t-align-center"><a href="ViewTenderDetails.jsp?type=Organization&status=Finalized"><%=reportData.get(0).getFieldName9()%></a></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </td>
            </tr>
         </table>
         <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
    </div>
</div>
</body>
</html>
