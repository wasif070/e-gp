/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.eps.service.audit;

/**
 *
 * @author nishit
 */
public class AuditTrail {

    private String ipAddress;
    private Object sessionId;
    private Object userTypeId;
    private StringBuffer url;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Object getSessionId() {
        return sessionId;
    }

    public void setSessionId(Object sessionId) {
        this.sessionId = sessionId;
    }

    public StringBuffer getUrl() {
        return url;
    }

    public void setUrl(StringBuffer url) {
        this.url = url;
    }

    public Object getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Object userTypeId) {
        this.userTypeId = userTypeId;
    }

    public AuditTrail(String ipAddress, Object sessionId, Object userTypeId, StringBuffer url) {
        this.ipAddress = ipAddress;
        this.sessionId = sessionId;
        this.userTypeId = userTypeId;
        //this.url = url;
        this.url = new StringBuffer(convertUrlToHttps(url.toString()));
    }

    public AuditTrail(String ipAddress, Object sessionId, Object userTypeId, String url) {
        this.ipAddress = ipAddress;
        this.sessionId = sessionId;
        this.userTypeId = userTypeId;
        this.url = new StringBuffer(convertUrlToHttps(url));
    }

    public String convertUrlToHttps(String httpurl) {
        String httpsurl = "";
        if (httpurl != null && !"".equals(httpurl.trim())) {
            httpsurl = httpurl;
            if (!httpurl.contains("http://localhost")) {
                if (httpurl.startsWith("http://")) {
                    httpsurl = httpurl.replace("http://", "https://");
                }
            }
        }
        return httpsurl;
    }
}
