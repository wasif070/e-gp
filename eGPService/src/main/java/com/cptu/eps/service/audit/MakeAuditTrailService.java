/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.eps.service.audit;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblGovUserAuditDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererAuditDetailsDao;
import com.cptu.egp.eps.model.table.TblGovUserAuditDetails;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import com.cptu.egp.eps.model.table.TblTendererAuditDetails;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author taher
 */
public class MakeAuditTrailService {

    static final Logger logger = Logger.getLogger(MakeAuditTrailService.class);
    private TblTendererAuditDetailsDao tblTendererAuditDetailsDao;
    private TblGovUserAuditDetailsDao tblGovUserAuditDetailsDao;
    private HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblGovUserAuditDetailsDao getTblGovUserAuditDetailsDao() {
        return tblGovUserAuditDetailsDao;
    }

    public void setTblGovUserAuditDetailsDao(TblGovUserAuditDetailsDao tblGovUserAuditDetailsDao) {
        this.tblGovUserAuditDetailsDao = tblGovUserAuditDetailsDao;
    }

    public TblTendererAuditDetailsDao getTblTendererAuditDetailsDao() {
        return tblTendererAuditDetailsDao;
    }

    public void setTblTendererAuditDetailsDao(TblTendererAuditDetailsDao tblTendererAuditDetailsDao) {
        this.tblTendererAuditDetailsDao = tblTendererAuditDetailsDao;
    }

    /**
     * For inserting Audit for the action
     * @param auditTrail It contains AuditTrail(ipAddress, sessionId, userTypeId, url)
     * @param id It contains Id's(Ex : tenderId,appId etc.)
     * @param idType It contains Type's(Ex : tender,app etc.)
     * @param module Module name is available from EgpModule enum
     * @param action This is the action that current method is performing(Ex : Publishing Tender etc.)
     * @param remarks It contains remarks if captured from UserInterface
     */
    public void generateAudit(AuditTrail auditTrail, int id, String idType, String module, String action, String remarks) {
        TblGovUserAuditDetails govUserAuditDetails = null;
        TblTendererAuditDetails tendererAuditDetails = null;
        try {
            if (auditTrail != null && (Byte) auditTrail.getUserTypeId() == 2) {
                tendererAuditDetails = new TblTendererAuditDetails();
            }
            if (auditTrail != null && (Byte) auditTrail.getUserTypeId() != 2) {
                govUserAuditDetails = new TblGovUserAuditDetails();
            }
            String url = "";
            if(auditTrail!=null && auditTrail.getUrl()!=null){
                if(auditTrail.getUrl().toString().contains("?")){
                    url =  auditTrail.getUrl().toString().split(".jsp")[0];
                    url = url + ".jsp";
                }else{
                    url = auditTrail.getUrl().toString();
                }
            }
            if (tendererAuditDetails != null && action != null && remarks != null && idType != null && auditTrail.getSessionId() != null) {
                tendererAuditDetails.setActionPerformed(action);
                tendererAuditDetails.setActivityDtTime(new Date());
                tendererAuditDetails.setActivityRemarks(remarks);
                tendererAuditDetails.setActivityUrl(url);
                tendererAuditDetails.setIpAddress(auditTrail.getIpAddress());
                tendererAuditDetails.setModuleName(module);
                tendererAuditDetails.setObjectId(id);
                tendererAuditDetails.setObjectType(idType);
                tendererAuditDetails.setTblSessionMaster(new TblSessionMaster((Integer) auditTrail.getSessionId()));
                tblTendererAuditDetailsDao.addTblTendererAuditDetails(tendererAuditDetails);
            }
            if (govUserAuditDetails != null && action != null && remarks != null && idType != null && auditTrail.getSessionId() != null) {
                govUserAuditDetails.setActionPerformed(action);
                govUserAuditDetails.setActivityDtTime(new Date());
                govUserAuditDetails.setActivityRemarks(remarks);
                govUserAuditDetails.setActivityUrl(url);
                govUserAuditDetails.setIpAddress(auditTrail.getIpAddress());
                govUserAuditDetails.setModuleName(module);
                govUserAuditDetails.setObjectId(id);
                govUserAuditDetails.setObjectType(idType);
                govUserAuditDetails.setTblSessionMaster(new TblSessionMaster((Integer) auditTrail.getSessionId()));
                tblGovUserAuditDetailsDao.addTblGovUserAuditDetails(govUserAuditDetails);
            }
        } catch (Exception e) {
            logger.error("Error in generateAudit(" + module + "_" + action + ") : ", e);
        }
    }

    /**
     * Get Audti Trail Activity Details Module Wise.
     * @param moduleName
     * @return List of Object Array
     */
    public List<Object> getAuditActivity(String moduleName) {
        List<Object> list = new ArrayList<Object>();
        logger.debug("getAuditActivity : " + "Starts");
        String modName = "";
        if(moduleName!=null && !"".equalsIgnoreCase(moduleName.trim())){
            modName = "where  moduleName ='"+moduleName+"' and actionPerformed!='' and actionPerformed not like '%exception%' and  actionPerformed not like '%error%' and  actionPerformed not like '%null%' order by actionPerformed ";
        }else{
            modName = "where actionPerformed!='' and actionPerformed not like '%exception%' and  actionPerformed not like '%error%' and actionPerformed not like '%null%' order by actionPerformed ";
        }
        
        try {
            list.addAll(hibernateQueryDao.singleColQuery("select distinct actionPerformed from TblGovUserAuditDetails "+modName));
            list.addAll(hibernateQueryDao.singleColQuery("select distinct actionPerformed from TblTendererAuditDetails "+modName));
            Map<String,Object> map = new HashMap<String, Object>();
            for(Object obj : list){
                map.put(obj.toString(), obj);
                //System.out.println("=========Tital Case =========="+toTitleCase(obj.toString()));
            }
            list.clear();
            list.addAll(map.keySet());
            Collections.sort(list,new CompareAuditActions());
        } catch (Exception e) {
            logger.error("Error in getAuditActivity : ", e);
            e.printStackTrace();
        }
        logger.debug("getAuditActivity : " + "Ends");
        return list;
    }

    /**
     * Convert String to TitleCase.
     * @param original
     * @return String as Title Case.
     */
     public static String toTitleCase( String original ) {
      String words[] = original.split(" " );
      String str="";
      for (int i = 0; i < words.length; i++) {
        str = str +" "+ toInitialCap( words[i] );
      }


      return (str);
    }

    /**
     * Convert String to Init Cap.
     * @param original
     * @return String as Init Cap
     */
    public static String toInitialCap( String original )
    {
        String strInIt="",str2="";
        char ch='0';

            for(int j=0;j<original.length();j++)
            {
                ch=original.charAt(j);
                str2=ch+"";
                if(j == 0 || j == original.indexOf("/")+1)
                    strInIt=strInIt + str2.toUpperCase() ;
                else
                    strInIt=strInIt + str2.toLowerCase() ; 
            }

        return strInIt;
    }
}


class CompareAuditActions implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {       
        return myCompare(o1.toString(),o2.toString());
    }

    public int myCompare(String s1,String s2){
        if(s1.equals("") || s2.equals("")){
            return 0;
        }
        return s1.compareToIgnoreCase(s2);
    }
}