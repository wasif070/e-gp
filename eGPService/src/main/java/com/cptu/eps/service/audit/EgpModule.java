/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.eps.service.audit;

/**
 *
 * @author nishit
 */
public enum EgpModule {
    New_User_Registration("New User Registration"),
    Public_Procurement_Forum("Public Procurement Forum"),
    Message_Box("Message Box"),
    WorkFlow("Work Flow"),
    Configuration("Configuration"),
    STD("STD"),
    Content("Content"),
    Manage_Users("Manage Users"),
    Report("Report"),
    My_Account("My Account"),
    Ask_Procurement_Expert("Ask Procurement Expert"),
    Debarment("Debarment"),
    APP("APP"),
    Tender_Notice("Tender Notice"),
    Tender_Document("Tender Document"),
    Evaluation("Evaluation"),
    Opening("Opening"),
    Bank_Module("Bank Module"),
    Noa("NOA"),
    Commencement_Date("Commencement Date"),
    Delivery_Schedule("Delivery Schedule"),
    Contract_Termination("Contract Termination"),
    Progress_Report("Progress Report"),
    Payment("Payment"),
    Repeat_Order("Repeat Order"),
    Work_Completion_Certificate("Work Completion Certificate"),
    Variation_Order("Variation Order"),
    Work_Program("Work Program"),
    Tender_Opening("Tender Opening"),
    JVCA("JVCA"),
    DELCLARTION("Declaration"),
    WATCH_LIST("Watch List"),
    Pre_Tender_Query_Meeting("Pre Tender Query Meeting"),
    Sub_Contracting("Sub Contracting"),
    Bid_Submission("Bid Submission"),
    CDL("Common Doc Lib"),
    Corrigendum_Amendment("Corrigendum/Amendment"),
    Work_Schedule("Work Schedule"),
    Contract_Signing("Contract Signing"),
    Negotiation("Negotiation"),
    Complaint_Management("ComplaintManagement"),
    Online_Payment("Online Payment"),
    Private_Disscussion_Forum("Private Discussion Forum");

    private String name;

    public String getName() {
        return name;
    }    

    private EgpModule(String name) {
        this.name = name;
    }
    
}
