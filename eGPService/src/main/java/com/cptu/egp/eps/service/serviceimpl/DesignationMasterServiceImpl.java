/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblDesignationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblGradeMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.model.table.TblLoginMaster;

 /*
 * @author Administrator
 */
public class DesignationMasterServiceImpl implements DesignationMasterService
{

    String logUserId = "0";

    final Logger logger = Logger.getLogger(DesignationMasterServiceImpl.class);

    private TblDepartmentMasterDao tblDepartmentMasterDao;
    private TblGradeMasterDao tblGradeMasterDao;
    private TblDesignationMasterDao tblDesignationMasterDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblOfficeAdminDao tblOfficeAdminDao;
    private TblOfficeMasterDao tblOfficeMasterDao;
    private AuditTrail auditTrail;
    
    private MakeAuditTrailService makeAuditTrailService;
    public TblOfficeAdminDao getTblOfficeAdminDao() {
        return tblOfficeAdminDao;
    }

    public void setTblOfficeAdminDao(TblOfficeAdminDao tblOfficeAdminDao) {
        this.tblOfficeAdminDao = tblOfficeAdminDao;
    }

    public TblOfficeMasterDao getTblOfficeMasterDao() {
        return tblOfficeMasterDao;
    }

    public void setTblOfficeMasterDao(TblOfficeMasterDao tblOfficeMasterDao) {
        this.tblOfficeMasterDao = tblOfficeMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao()
    {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao)
    {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public List<TblDepartmentMaster> getMinistry()
    {
        logger.debug("getMinistry : "+logUserId+" Starts");
        List<TblDepartmentMaster> list = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentType", Operation_enum.LIKE, "Ministry", "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            list =  tblDepartmentMasterDao.findTblDepartmentMaster(values);
        }
        catch (Exception ex) {
            logger.error("getMinistry : "+logUserId+" :"+ex);
        }
        logger.debug("getMinistry : "+logUserId+" Ends");
        return list;
    }
    
    @Override
    public int createDesignation(TblDesignationMaster tblDesignationMaster)
    {
        int count = -1;
        String action = "Create Designation";
        logger.debug("createDesignation : "+logUserId+" Ends");
        try {
            tblDesignationMasterDao.addDesignation(tblDesignationMaster);
            count  = tblDesignationMaster.getDesignationId();
        }
        catch (Exception ex) {
            logger.error("createDesignation : "+logUserId+" :"+ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            
            makeAuditTrailService.generateAudit(auditTrail, count, "gradeId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("createDesignation : "+logUserId+" Ends");
        return count;
    }

    @Override
    public List<TblGradeMaster> getGrades()
    {
        logger.debug("getGrades : "+logUserId+" Starts");
        List<TblGradeMaster> list  =new ArrayList<TblGradeMaster>();
        try {
            Object[] values = {"grade", Operation_enum.ORDERBY, Operation_enum.ASC};
            list =  tblGradeMasterDao.findTblGradeMaster(values);
        }
        catch (Exception ex) {
            logger.error("getGrades : "+logUserId+" :"+ex);
        }
        logger.debug("getGrades : "+logUserId+" Ends");
        return list;
    }

    public TblDepartmentMasterDao getTblDepartmentMasterDao()
    {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao)
    {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    public TblDesignationMasterDao getTblDesignationMasterDao()
    {
        return tblDesignationMasterDao;
    }

    public void setTblDesignationMasterDao(TblDesignationMasterDao tblDesignationMasterDao)
    {
        this.tblDesignationMasterDao = tblDesignationMasterDao;
    }

    public TblGradeMasterDao getTblGradeMasterDao()
    {
        return tblGradeMasterDao;
    }

    public void setTblGradeMasterDao(TblGradeMasterDao tblGradeMasterDao)
    {
        this.tblGradeMasterDao = tblGradeMasterDao;
    }

    @Override
    public TblDesignationMaster getDesignationDetails(int designationId)
    {
        logger.debug("getDesignationDetails : "+logUserId+" Ends");
        List<TblDesignationMaster> designationMasters  = null;
        TblDesignationMaster tblDesignationMaster = null;
        try {
            designationMasters = tblDesignationMasterDao.findDesignantion("designationId", Operation_enum.EQ, designationId);
            if (designationMasters != null && !designationMasters.isEmpty()) {
                tblDesignationMaster = designationMasters.get(0);
            }
        }
        catch (Exception ex) {
            logger.error("getDesignationDetails : "+logUserId+" :"+ex);
        }
        logger.debug("getDesignationDetails : "+logUserId+" Ends");
        return tblDesignationMaster;
    }
    
    @Override
    public boolean updateDesignation(TblDesignationMaster tblDesignationMaster)
    {
        logger.debug("updateDesignation : "+logUserId+" Starts");
        boolean flag = false;
        String action = "Edit Designation";
        try {
            tblDesignationMasterDao.updateDesignantion(tblDesignationMaster);
            flag =  true;
        }
        catch (Exception ex) {
            logger.error("updateDesignation : "+logUserId+" :"+ ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblDesignationMaster.getDesignationId(), "designationId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("updateDesignation : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean verifyDesignation(String designationName, short departmentId)
    {
        logger.debug("verifyDesignation : "+logUserId+" Starts");
        boolean flag = false;
        try {
            List<TblDesignationMaster> designationMasters = tblDesignationMasterDao.findDesignantion("designationName", Operation_enum.EQ, designationName, "tblDepartmentMaster", Operation_enum.EQ, new TblDepartmentMaster(departmentId));
            if (designationMasters != null && !designationMasters.isEmpty()) {
                flag =  true;
            }
        }
        catch (Exception ex) {
            logger.error("verifyDesignation : "+logUserId+" :"+ ex);
        }
        logger.debug("verifyDesignation : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<TblDesignationMaster> getAllDesignations(int firstResult,int maxResult,int userId)
    {
        logger.debug("getAllDesignations : "+logUserId+" Starts");
        List<TblDesignationMaster> designationMasters=null;
        List<TblDepartmentMaster> deptDepartmentMasters=null;
        short departmentId=0;
       
        Object values[] = {"designationName", Operation_enum.ORDERBY, Operation_enum.ASC};
        try {
            if (userId != 0) {
                Object adminValues[] = {"approvingAuthorityId",Operation_enum.EQ,userId};
                //Object adminValues[]={"createdBy",Operation_enum.EQ,userId,"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                values=adminValues;
                deptDepartmentMasters = tblDepartmentMasterDao.findTblDepartmentMaster(values);
                    if(deptDepartmentMasters != null&&deptDepartmentMasters.size() >0){
                        departmentId = deptDepartmentMasters.get(0).getDepartmentId();
                        
                        Object deptValues[]={"tblDepartmentMaster",Operation_enum.EQ,new TblDepartmentMaster(departmentId),"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                        values = deptValues;
                    }
                }
            designationMasters=tblDesignationMasterDao.findByCountDesignation(firstResult, maxResult, values);
            
        }
        catch (Exception ex) {
            logger.error("getAllDesignations : "+logUserId+" Ends"+ex);
        }
        logger.debug("getAllDesignations : "+logUserId+" Ends");
       return designationMasters;
    }

    /*@Override
    public List<TblDesignationMaster> getAllDesignationsForPE(int firstResult,int maxResult,int userId,String orderClause,String order)
    {
        List<TblOfficeAdmin>  officeAdmin=null;
        List<TblOfficeMaster> officeMaster =null;
        List<TblDesignationMaster> designationMasters=null;
        int officeId=0;
        int departmentId=0;
        Object e = null;
        if(order.equalsIgnoreCase("asc")){
            e = Operation_enum.ASC;
        }else if(order.equalsIgnoreCase("desc")){
            e = Operation_enum.DESC;
        }
        Object values[] = {orderClause, Operation_enum.ORDERBY, e};
        try {
            if (userId != 0) {
                Object adminValues[] = {"tblLoginMaster",Operation_enum.EQ,new TblLoginMaster(userId)};
                //Object adminValues[]={"createdBy",Operation_enum.EQ,userId,"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                values=adminValues;
                officeAdmin = tblOfficeAdminDao.findTblOfficeAdmin(values);

                if(officeAdmin != null){
                    if(officeAdmin.size() >0){
                        officeId = officeAdmin.get(0).getOfficeAdminId();
                        Object officeMasterValues[]={"officeId",Operation_enum.EQ,officeId,"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                        values = officeMasterValues;
                        officeMaster = tblOfficeMasterDao.findTblOfficeMaster(values);
                        if(officeMaster != null){
                            if(officeMaster.size() >0){
                                departmentId = officeMaster.get(0).getDepartmentId();
                            }
                        }
                        Object designationValues[]={"tblDepartmentMaster",Operation_enum.EQ,new TblDepartmentMaster((short)departmentId),"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                    }else {
                    values = null;
                }
                }

            }
            designationMasters=tblDesignationMasterDao.findByCountDesignation(firstResult, maxResult, values);
            return designationMasters;
        }
        catch (Exception ex) {
            
        }
        return null;
    }*/
     @Override
    public List<Object[]> getAllDesignationsForPE(int firstResult,int maxResult,int userId,String orderClause,String order,int userTypeId)
    {
         logger.debug("getAllDesignationsForPE : "+logUserId+" Starts");
        List<Object[]> list = null;
        String query = "";
        try {
            if (userId != 0) {
                if (userTypeId == 5) {
                    query = "select tdm.designationName,tm.departmentName,tgm.grade,tdm.designationId,tgm.gradeLevel from TblDesignationMaster tdm,TblDepartmentMaster tm , TblGradeMaster tgm where tdm.tblGradeMaster.gradeId = tgm.gradeId and tdm.tblDepartmentMaster.departmentId = tm.departmentId and tm.approvingAuthorityId = " + userId + " order by " + orderClause + " " + order;

                } else {
                    query = "select tdm.designationName,tm.departmentName,tgm.grade,tdm.designationId,tgm.gradeLevel from TblDesignationMaster tdm,TblDepartmentMaster tm , TblGradeMaster tgm where tdm.tblGradeMaster.gradeId = tgm.gradeId and tdm.tblDepartmentMaster.departmentId = tm.departmentId and tdm.createdBy = " + userId + " order by " + orderClause + " " + order;
                }
            }else{
                query = "select tdm.designationName,tm.departmentName,tgm.grade,tdm.designationId,tgm.gradeLevel from TblDesignationMaster tdm,TblDepartmentMaster tm , TblGradeMaster tgm where tdm.tblGradeMaster.gradeId = tgm.gradeId and tdm.tblDepartmentMaster.departmentId = tm.departmentId order by " + orderClause + " " + order;
            }
            list = hibernateQueryDao.createByCountNewQuery(query, firstResult, maxResult);
            
        }
        catch (Exception ex) {
            logger.error("getAllDesignationsForPE : "+logUserId+" Starts"+ex);
        }
        logger.debug("getAllDesignationsForPE : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long getDesigCnt(int userId)
    {
        logger.debug("getDesigCnt : "+logUserId+" Starts");
        List<TblDesignationMaster> designationMasters=null;
        List<TblDepartmentMaster> deptDepartmentMasters=null;
        short departmentId=0;
        long count = -1;
        Object values[] = {"designationName", Operation_enum.ORDERBY, Operation_enum.ASC};
        try {
            if (userId != 0) {
                Object adminValues[] = {"approvingAuthorityId",Operation_enum.EQ,userId};
                //Object adminValues[]={"createdBy",Operation_enum.EQ,userId,"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                values=adminValues;
                deptDepartmentMasters = tblDepartmentMasterDao.findTblDepartmentMaster(values);
                if(deptDepartmentMasters != null){
                    if(deptDepartmentMasters.size() >0){
                        departmentId = deptDepartmentMasters.get(0).getDepartmentId();
                        Object deptValues[]={"tblDepartmentMaster",Operation_enum.EQ,new TblDepartmentMaster(departmentId),"designationId", Operation_enum.ORDERBY, Operation_enum.DESC};
                        values = deptValues;
                    }
                }
            }
             designationMasters=tblDesignationMasterDao.findDesignantion(values);
             count  = designationMasters.size();
        }
        catch (Exception ex) {
            logger.error("getDesigCnt : "+logUserId+" Ends"+ex);
        }
         logger.debug("getDesigCnt : "+logUserId+" Ends");
         return count;
    }

    @Override
    public List<TblDepartmentMaster> getMinistryforAdmin(int userId) {
        logger.debug("getMinistryforAdmin : "+logUserId+" Starts");
        List<TblDepartmentMaster> departmentMasters=null;
        Object values[] = {"approvingAuthorityId",Operation_enum.EQ,userId};
        try {
            departmentMasters = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.debug("getMinistryforAdmin : "+logUserId+" Ends"+ex);
        }
        logger.debug("getMinistryforAdmin : "+logUserId+" Ends");
        return departmentMasters;
    }
    
     @Override
    public List<TblOfficeMaster> getOfficeforAdmin(int userId) {
        List<TblOfficeAdmin>  officeAdmin=null;
        List<TblOfficeMaster> officeMaster =null;
        int officeId=0;
        Object e = Operation_enum.ASC;
        
        Object values[] = {"userId", Operation_enum.ORDERBY, e};
        try {
            if (userId != 0) {
                Object adminValues[] = {"tblLoginMaster",Operation_enum.EQ,new TblLoginMaster(userId)};
                values=adminValues;
                officeAdmin = tblOfficeAdminDao.findTblOfficeAdmin(values);

                if(officeAdmin != null){
                    if(officeAdmin.size() >0){
                        officeId = officeAdmin.get(0).getTblOfficeMaster().getOfficeId();
                        Object officeMasterValues[]={"officeId",Operation_enum.EQ,officeId,"departmentId", Operation_enum.ORDERBY, Operation_enum.DESC};
                        values = officeMasterValues;
                        officeMaster = tblOfficeMasterDao.findTblOfficeMaster(values);                        
                    }else {
                    values = null;
                }
                }
            }         
        }
        catch (Exception ex) {
            logger.error("getOfficeforAdmin : "+logUserId+" Ends"+ex);
        }
         return officeMaster;
    }


    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }


}
