/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public interface CmsNewBankGuarnateeService {
    
    public void setLogUserId(String logUserId);
            
    /**
     * This method will add new Bank Guarantee.
     * @param tblCmsNewBankGuarnatee
     * @return inserted id
     */
    public int addBankGuarantee(TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee);
    
    /**
     * This method will fetch Bank Guarantee details based on tender Id
     * @param tenderId
     * @return Bank Guarantee Details
     */
    public TblCmsNewBankGuarnatee fetchBankGuarantee(int tenderId);
    
    /**
     * This method will check if Bank Maker has make payment for Bank Guarantee or not.
     * @param tenderId
     * @return true if not make payment or false if make payment.
     */
    public List<Object[]> fetchBankGuaranteeForMaker(int tenderId);
    
    /**
     * This method will check if Bank Checker has make payment for Bank Guarantee or not.
     * @param tenderId
     * @return true if not make payment or false if make payment.
     */
    public boolean checkBankGuaranteeForChecker(int tenderId);
    
    /**
     * This method will fetch all Bank Guarantee Details.
     * @param tenderId
     * @return true if not make payment or false if make payment.
     */
    public List<Object[]> bankGuaranteeListing(int tenderId);
    
    /**
     * This method will fetch tender payment status.
     * @param tenderId
     * @return true if not make payment or false if make payment.
     */
    public List<Object[]> getTenderpaymentStatus(int paymentId);
    
    /**
     * This method will fetch Bank Guarantee Details.
     * @param tenderId
     * @param userId
     * @return List of Bank Guarantee Details.
     */
    public List<Object[]> fetchBankGuaranteeList(int tenderId, int userId);
    /**
     * This method will fetch Bank Guarantee Details whose entry has been done by Maker.
     * @param tenderId
     * @param userId
     * @return List of Bank Guarantee Details.
     */
    public List<Object[]> fetchBankGuaranteeListMaker(int tenderId, int userId);
    
    /**
     * This method will fetch Bank Guarantee Details based on Bank Guarantee Id.
     * @param Bank Guarantee Id
     * @return List of Bank Guarantee Details.
     */
    public List<TblCmsNewBankGuarnatee> fetchBankGuaranteeDetails(int bgId);

    //  Dohatec Start   //  For ICT
    public List<Object[]> fetchBankGuaranteeListICT(int tenderId, int userId);
    public List<Object[]> fetchBankGuaranteeDetailsICT(int tenderId, int lotId, int userId);
    public List<Object[]> fetchBankGuaranteeListMakerICT(int tenderId, int userId);
    public List<Object[]> bankGuaranteeListingICT(int tenderId);
    //  Dohatec End
    
}
