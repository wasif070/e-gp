/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderReplyDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderPostQueConfigDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblPreTenderReply;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPostQueConfig;
import com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class TenderPostQueConfigServiceImpl implements TenderPostQueConfigService {

    private static final Logger LOGGER = Logger.getLogger(TenderPostQueConfigServiceImpl.class);
    private String logUserId = "0";
    private TblTenderPostQueConfigDao tblTenderPostQueConfigDao;
    private TblTenderDetailsDao tblTenderDetailsDao;
    private TblPreTenderReplyDao tblPreTenderReplyDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblPreTenderReplyDao getTblPreTenderReplyDao() {
        return tblPreTenderReplyDao;
    }

    public void setTblPreTenderReplyDao(TblPreTenderReplyDao tblPreTenderReplyDao) {
        this.tblPreTenderReplyDao = tblPreTenderReplyDao;
    }

    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    public TblTenderPostQueConfigDao getTblTenderPostQueConfigDao() {
        return tblTenderPostQueConfigDao;
    }

    public void setTblTenderPostQueConfigDao(TblTenderPostQueConfigDao tblTenderPostQueConfigDao) {
        this.tblTenderPostQueConfigDao = tblTenderPostQueConfigDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public boolean saveConfiguration(TblTenderPostQueConfig tblTenderPostQueConfig) {
        boolean flag = false;
        LOGGER.debug("saveConfiguration : " + logUserId + " : Starts");
        String action = null;
        try {
            tblTenderPostQueConfigDao.addTblTenderPostQueConfig(tblTenderPostQueConfig);
            flag = true;
            action = "Configure Clarification on Tender";
        } catch (Exception e) {
            LOGGER.error("saveConfiguration : " + logUserId + " : " + e);
            action = "Error in Configure Clarification on Tender";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderPostQueConfig.getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Tender_Notice.getName(), action, "");
            action = null;
        }
        LOGGER.debug("saveConfiguration : " + logUserId + " : Ends");
        return flag;
    }

    @Override
    public List<TblTenderPostQueConfig> getTenderPostQueConfig(int tenderid) {
        LOGGER.debug("getTenderPostQueConfig : " + logUserId + " : Starts");
        List<TblTenderPostQueConfig> list = null;
        try {
            list = tblTenderPostQueConfigDao.findTblTenderPostQueConfig("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderid));
        } catch (Exception e) {
            LOGGER.error("getTenderPostQueConfig : " + logUserId + " : " + e);
        }
        LOGGER.debug("getTenderPostQueConfig : " + logUserId + " : Starts");
        return list;
    }

    @Override
    public boolean updateConfiguration(TblTenderPostQueConfig tblTenderPostQueConfig) {
        boolean flag = false;
        LOGGER.debug("updateConfiguration : " + logUserId + " : Starts");
        String action = null;
        try {
            tblTenderPostQueConfigDao.updateTblTenderPostQueConfig(tblTenderPostQueConfig);
            //hibernateQueryDao.updateDeleteNewQuery("update TblTenderPostQueConfig set postQueLastDt='"+tblTenderPostQueConfig.getPostQueLastDt()+"', createdDt='"+tblTenderPostQueConfig.getCreatedDt()+"', isQueAnsConfig='"+tblTenderPostQueConfig.getIsQueAnsConfig()+"', createdBy="+tblTenderPostQueConfig.getCreatedBy()+"where ")
            flag = true;
            action = "Edit Clarification on Tender Configuration";
        } catch (Exception e) {
            LOGGER.error("updateConfiguration : " + logUserId + " : " + e);
            action = "Error in Edit Clarification on Tender Configuration";
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderPostQueConfig.getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Tender_Notice.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateConfiguration : " + logUserId + " : Ends");
        return flag;
    }

    @Override
    public List<TblTenderDetails> getPrebidStartDate(String tenderid) {
        int tenderID = Integer.parseInt(tenderid);
        LOGGER.debug("getPrebidStartDate : " + logUserId + "Starts");
        List<TblTenderDetails> list = null;
        try {
            list = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderID));
        } catch (Exception ex) {
            LOGGER.error("getPrebidStartDate : " + logUserId + ex);
        }
        LOGGER.debug("getPrebidStartDate : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<TblPreTenderReply> getRepyData(int userid) {
        LOGGER.debug("getRepyData : " + logUserId + "Starts");
        List<TblPreTenderReply> list = null;
        try {
            list = tblPreTenderReplyDao.findTblPreTenderReply("userId", Operation_enum.EQ, userid);
        } catch (Exception ex) {
            LOGGER.error("getRepyData : " + logUserId + ex);
        }
        LOGGER.debug("getRepyData : " + logUserId + "Ends");
        return list;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
