/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblAskProcurementDao;
import com.cptu.egp.eps.dao.daointerface.TblQuestionCategoryDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblAskProcurement;
import com.cptu.egp.eps.model.table.TblQuestionCategory;
import com.cptu.egp.eps.service.serviceinterface.AskProcurementService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class AskProcurementServiceImpl implements AskProcurementService{

    final Logger logger = Logger.getLogger(AskProcurementServiceImpl.class);

    TblQuestionCategoryDao tblQuestionCategoryDao;
    TblAskProcurementDao tblAskProcurementDao;
    private MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";

    public TblQuestionCategoryDao getTblQuestionCategoryDao() {
        return tblQuestionCategoryDao;
    }

    public void setTblQuestionCategoryDao(TblQuestionCategoryDao tblQuestionCategoryDao) {
        this.tblQuestionCategoryDao = tblQuestionCategoryDao;
    }

    public TblAskProcurementDao getTblAskProcurementDao() {
        return tblAskProcurementDao;
    }

    public void setTblAskProcurementDao(TblAskProcurementDao tblAskProcurementDao) {
        this.tblAskProcurementDao = tblAskProcurementDao;
    }
    
    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
     
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public List<TblQuestionCategory> getAllQusetions() {
        logger.debug("getAllQusetions : " + logUserId + loggerStart);
        List<TblQuestionCategory> tqc = null;
        try {
            tqc = getTblQuestionCategoryDao().getAllTblQuestionCategory();
        } catch (Exception e) {
            logger.error("getAllQusetions : " + logUserId + e);
    }
        logger.debug("getAllQusetions : " + logUserId + loggerEnd);
        return tqc;
    }

    @Override
    public List<TblAskProcurement> getAskProcurementDetails(int id) {
        logger.debug("getAskProcurementDetails : " + logUserId + loggerStart);
        List<TblAskProcurement> tblAskProcurements;
        try{
            tblAskProcurements = getTblAskProcurementDao().findEntity("postedBy",Operation_enum.EQ,id);
        }catch(Exception e){
            tblAskProcurements = null;
            logger.error("getAskProcurementDetails : " + logUserId + e);
        }
        logger.debug("getAskProcurementDetails : " + logUserId + loggerEnd);
        return tblAskProcurements;
    }

    @Override
    public List<TblAskProcurement> getAskProcurement(int id) {
        logger.debug("getAskProcurement : " + logUserId + loggerStart);
        List<TblAskProcurement> tblAskProcurements;
        try{
            tblAskProcurements = getTblAskProcurementDao().findEntity("procQueId",Operation_enum.EQ,id);
        }catch(Exception e){
            tblAskProcurements = null;
            logger.error("getAskProcurement : " + logUserId + e);
        }
        logger.debug("getAskProcurement : " + logUserId + loggerEnd);
        return tblAskProcurements;
    }

    @Override
    public String addTblAskProcurement(TblAskProcurement tblAskProcurement) {
        logger.debug("addTblAskProcurement : " + logUserId + loggerStart);
        String tbString = "";
        String action = "Post Query";
        try{
            getTblAskProcurementDao().addTblAskProcurement(tblAskProcurement);
            tbString = "Values Added";
        }catch(Exception e){
            tbString = "Not Added";
            logger.error("addTblAskProcurement : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblAskProcurement.getPostedBy(), "userId", EgpModule.Ask_Procurement_Expert.getName(), action, "");
            action = null;
        }
        logger.debug("addTblAskProcurement : " + logUserId + loggerEnd);
        return tbString;
    }
}
