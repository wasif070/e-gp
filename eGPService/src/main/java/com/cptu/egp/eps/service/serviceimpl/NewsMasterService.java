/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblNewsMasterDao;
import com.cptu.egp.eps.model.table.TblNewsMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class NewsMasterService {

    private static final Logger LOGGER = Logger.getLogger(NewsMasterService.class);

    private TblNewsMasterDao tblNewsMasterDao;
    private HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblNewsMasterDao getTblNewsMasterDao() {
        return tblNewsMasterDao;
    }

    public void setTblNewsMasterDao(TblNewsMasterDao tblNewsMasterDao) {
        this.tblNewsMasterDao = tblNewsMasterDao;
    } 
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * Method is use for add News details
     * @param newsMaster
     * @return true if successfully added else return false
     */
    public boolean insertNewsDetails(TblNewsMaster newsMaster){
        LOGGER.debug("insertNewsDetails : "+logUserId+" Starts");
        boolean flag;
        String action = "Create News";
        try{
            tblNewsMasterDao.addTblNewsMaster(newsMaster);
            flag = true;
        }catch(Exception e){
           LOGGER.error("insertNewsDetails : "+logUserId+" : "+e);
            flag = false;
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        LOGGER.debug("insertNewsDetails : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Method is use for update News details.
     * @param newsMaster
     * @return true if successfully updated else return false
     */
    public boolean updateNewsDetails(TblNewsMaster newsMaster){
        LOGGER.debug("updateNewsDetails : "+logUserId+" Starts");
        boolean flag;
        String action = "Edit News";
        try{
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblNewsMaster set locationId="+newsMaster.getLocationId()+", newsType='"+newsMaster.getNewsType()+"', isImp='"+newsMaster.getIsImp()+"', newsBrief='"+newsMaster.getNewsBrief()+"', newsDetails='"+newsMaster.getNewsDetails()+"' where newsId="+newsMaster.getNewsId());
            if(i==1){
                flag = true;
            }else{
                flag = false;
            }
        }catch(Exception e){
            LOGGER.error("updateNewsDetails : "+logUserId+" : "+e);
            flag = false;
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateNewsDetails : "+logUserId+" Ends");
        return flag;
    }
    public boolean updateCircularDetails(TblNewsMaster newsMaster){
        LOGGER.debug("updateNewsDetails : "+logUserId+" Starts");
        boolean flag;
        String action = "Edit News";
        try{
            java.util.Date utilDate = newsMaster.getCreatedTime();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblNewsMaster set locationId="+newsMaster.getLocationId()+", newsType='"+newsMaster.getNewsType()+"', isImp='"+newsMaster.getIsImp()+"', newsBrief='"+newsMaster.getNewsBrief()+"', newsDetails='"+newsMaster.getNewsDetails()+"', createdTime='"+sqlDate+"', letterNumber='"+newsMaster.getLetterNumber()+"', fileName='"+newsMaster.getFileName()+"' where newsId="+newsMaster.getNewsId());
            if(i==1){
                flag = true;
            }else{
                flag = false;
            }
        }catch(Exception e){
            LOGGER.error("updateNewsDetails : "+logUserId+" : "+e);
            flag = false;
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateNewsDetails : "+logUserId+" Ends");
        return flag;
    }
    public boolean updateCircularDetailsExceptFile(TblNewsMaster newsMaster){
        LOGGER.debug("updateNewsDetails : "+logUserId+" Starts");
        boolean flag;
        String action = "Edit News";
        try{
            java.util.Date utilDate = newsMaster.getCreatedTime();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblNewsMaster set locationId="+newsMaster.getLocationId()+", newsType='"+newsMaster.getNewsType()+"', isImp='"+newsMaster.getIsImp()+"', newsBrief='"+newsMaster.getNewsBrief()+"', newsDetails='"+newsMaster.getNewsDetails()+"', createdTime='"+sqlDate+"', letterNumber='"+newsMaster.getLetterNumber()+"' where newsId="+newsMaster.getNewsId());
            if(i==1){
                flag = true;
            }else{
                flag = false;
            }
        }catch(Exception e){
            LOGGER.error("updateNewsDetails : "+logUserId+" : "+e);
            flag = false;
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateNewsDetails : "+logUserId+" Ends");
        return flag;
    }

}
