/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTdsSubClauseDao;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTdsSubClause;
import com.cptu.egp.eps.service.serviceinterface.PrepareTds;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>PrepareTdsImpl</b> <Description Goes Here> Nov 11, 2010 11:17:38 PM
 * @author yanki
 */
public class PrepareTdsImpl implements PrepareTds {

    final Logger logger = Logger.getLogger(PrepareTdsImpl.class);
    
    TblTdsSubClauseDao tblTdsSubClauseDao;
    SPTenderCommon sPTenderCommon;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPTenderCommon getsPTenderCommon()
    {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon)
    {
        this.sPTenderCommon = sPTenderCommon;
    }

    public TblTdsSubClauseDao getTblTdsSubClauseDao()
    {
        return tblTdsSubClauseDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    public void setTblTdsSubClauseDao(TblTdsSubClauseDao tblTdsSubClauseDao)
    {
        this.tblTdsSubClauseDao = tblTdsSubClauseDao;
    }
    
    @Override
    public int insertTdsSubClause(TblTdsSubClause tblTdsSubClauseList){
        logger.debug("insertTdsSubClause : "+logUserId+" Starts");
        int flag = 0;
        try{
            tblTdsSubClauseDao.addTblTdsSubClause(tblTdsSubClauseList);
            flag = tblTdsSubClauseList.getTdsSubClauseId();
        }catch(Exception ex){
            logger.debug("insertTdsSubClause : "+logUserId+" : "+ex);
            flag = 0;
        }
        logger.debug("insertTdsSubClause : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<SPTenderCommonData> getTDSSubClause(int ittHeaderId){
        /*
         * Fetching all the information regarding TDS Subclause
         * Used while fetchin all info regarding TDS.
         */
        logger.debug("getTDSSubClause : "+logUserId+" Starts");
        List<SPTenderCommonData> tdsInfo = null;
        try {
            tdsInfo = sPTenderCommon.executeProcedure("getTdsInfo", ittHeaderId+"", "");
        } catch (Exception ex) {
           logger.debug("getTDSSubClause : "+logUserId+" : "+ex);
           tdsInfo = null;
        }
        logger.debug("getTDSSubClause : "+logUserId+" Ends");
        return tdsInfo;
    }

    @Override
    public boolean updateTDSSubClause(TblTdsSubClause tblTdsSubClause){
        logger.debug("updateTDSSubClause : "+logUserId+" Starts");
        boolean flag;
        try{
            hibernateQueryDao.updateDeleteNewQuery("update TblTdsSubClause set tdsSubClauseName = '" + tblTdsSubClause.getTdsSubClauseName() + "' where tdsSubClauseId = '" + tblTdsSubClause.getTdsSubClauseId() + "'");
            flag = true;
        } catch(Exception ex){
            logger.debug("updateTDSSubClause : "+logUserId+" : "+ex);
            flag = false;
        }
        logger.debug("updateTDSSubClause : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean deleteTDSSubClause(TblTdsSubClause tblTdsSubClause) {
        logger.debug("deleteTDSSubClause : "+logUserId+" Starts");
        boolean flag;
        try{
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTdsSubClause where tdsSubClauseId = '" + tblTdsSubClause.getTdsSubClauseId() + "'");
            flag = true;
        } catch(Exception ex){
            logger.debug("deleteTDSSubClause : "+logUserId+" : "+ex);
            flag = false;
        }
        logger.debug("deleteTDSSubClause : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean isTdsSubClauseGenerated(int ittHeaderId){
        logger.debug("isTdsSubClauseGenerated : "+logUserId+" Starts");
        boolean isTdsSubClauseGenerated = false;
        try{
            long count = hibernateQueryDao.countForQuery("TblTdsSubClause", "tblIttHeader.ittHeaderId = '" + ittHeaderId + "'");
            if(count > 0){
                isTdsSubClauseGenerated = true;}
        }catch(Exception ex){
            logger.debug("isTdsSubClauseGenerated : "+logUserId+" : "+ex);
            isTdsSubClauseGenerated = false;
        }
        logger.debug("isTdsSubClauseGenerated : "+logUserId+" Ends");
        return isTdsSubClauseGenerated;
    }
}
