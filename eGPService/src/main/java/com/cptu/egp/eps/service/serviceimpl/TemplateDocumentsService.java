/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateGuildeLinesDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionDocsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTemplateGuildeLines;
import com.cptu.egp.eps.model.table.TblTemplateSectionDocs;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Yagnesh
 */
public class TemplateDocumentsService {

    private String logUserId = "0";
    static final Logger logger = Logger.getLogger(TemplateDocumentsService.class);
    TblTemplateSectionDocsDao tblTemplateSectionDocsDao;
    TblTemplateGuildeLinesDao tblTemplateGuildeLinesDao;

    public TblTemplateGuildeLinesDao getTblTemplateGuildeLinesDao() {
        return tblTemplateGuildeLinesDao;
    }

    public void setTblTemplateGuildeLinesDao(TblTemplateGuildeLinesDao tblTemplateGuildeLinesDao) {
        this.tblTemplateGuildeLinesDao = tblTemplateGuildeLinesDao;
    }

    public void setTblTemplateSectionDocsDao(TblTemplateSectionDocsDao tblTemplateSectionDocsDao) {
        this.tblTemplateSectionDocsDao = tblTemplateSectionDocsDao;
    }

    public TblTemplateSectionDocsDao getTblTemplateSectionDocsDao() {
        return tblTemplateSectionDocsDao;
    }
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     *  Add Template Section Doc
     * @param tDocs = Object of TblTemplateSectionDocs
     * @return = true if Add perform successfully else return false.
     */
    public boolean insertTemplateDocument(TblTemplateSectionDocs tDocs) {
        logger.debug("insertTemplateDocument : " + logUserId + "starts");
        boolean boolAddTDocs = false;
        try {
            tblTemplateSectionDocsDao.addTblTemplateSectionDocs(tDocs);
            boolAddTDocs = true;
        } catch (Exception ex) {
            logger.error("insertTemplateDocument : " + ex);
        }
        logger.debug("insertTemplateDocument : " + logUserId + "ends");
        return boolAddTDocs;
    }

    /**
     * Add Template Guide Line Documents.
     * @param tDocs = Object of TblTemplateGuildeLines
     * @return = true if Add perform successfully else return false.
     */
    public boolean insertGuideLinesDoc(TblTemplateGuildeLines tDocs) {
        logger.debug("insertGuideLinesDoc : " + logUserId + "starts");
        boolean boolAddTDocs = false;
        try {
            tblTemplateGuildeLinesDao.addTblTemplateGuildeLines(tDocs);
            boolAddTDocs = true;
        } catch (Exception ex) {
            logger.error("insertGuideLinesDoc : " + ex);
        }
        logger.debug("insertGuideLinesDoc : " + logUserId + "ends");
        return boolAddTDocs;
    }


    /**
     * Find Template Section Document.
     * @param templateId
     * @param sectionId
     * @return = List of TblTemplateSectionDocs Object.
     */
    public List<TblTemplateSectionDocs> getAllDocForSection(short templateId, short sectionId) {


        logger.debug("getAllDocForSection : " + logUserId + "starts");
        List<TblTemplateSectionDocs> tDocs;
        try {
            tDocs = tblTemplateSectionDocsDao.findTblTemplateSectionDocs("templateId", Operation_enum.EQ, templateId, "sectionId", Operation_enum.EQ, sectionId);
        } catch (Exception ex) {
            logger.error("getAllDocForSection : " + ex);
            tDocs = null;
        }
        logger.debug("getAllDocForSection : " + logUserId + "ends");
        return tDocs;
    }



    /**
     * Get all Template Guide Lines.
     * @param templateId
     * @return = List of TblTemplateGuildeLines Object.
     */
    public List<TblTemplateGuildeLines> getAllDocForGuideLines(int templateId) {
        logger.debug("getAllDocForGuideLines : " + logUserId + "starts");
        List<TblTemplateGuildeLines> tDocs;
        try {
            tDocs = tblTemplateGuildeLinesDao.findTblTemplateGuildeLines("templateId", Operation_enum.EQ, templateId);
        } catch (Exception ex) {
            logger.error("getAllDocForGuideLines : " + ex);
            tDocs = null;
        }
        logger.debug("getAllDocForGuideLines : " + logUserId + "ends");
        return tDocs;
    }

    /**
     * Delete Template Document.
     * @param sectionDocId
     * @return = true if delete perform successfully else return false.
     */
    public boolean deleteTemplateDocument(int sectionDocId) {


        logger.debug("deleteTemplateDocument : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "delete from TblTemplateSectionDocs ttsd where ttsd.sectionDocId = " + sectionDocId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteTemplateDocument : " + ex);
            flag = false;
        }
        logger.debug("deleteTemplateDocument : " + logUserId + "ends");
        return flag;
    }

    /**
     * Delete Guide Line Document
     * @param templateId
     * @param guidenceId
     * @return = true if delete perform successfully else return false.
     */
    public boolean deleteGuidelinesDocument(int templateId,int guidenceId) {


        logger.debug("deleteGuidelinesDocument : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "delete from TblTemplateGuildeLines ttsd where ttsd.guidelinesId='"+guidenceId+"' and ttsd.templateId='"+templateId+"'";
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteGuidelinesDocument : " + ex);
            flag = false;
        }
        logger.debug("deleteGuidelinesDocument : " + logUserId + "ends");
        return flag;
    }




    public void setUserId(String userId) {
        this.logUserId = userId;
    }
}
