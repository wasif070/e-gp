/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblQuizQuestionDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblQuizQuestion;
import static com.cptu.egp.eps.service.serviceimpl.ConfigPaThresholdServiceImpl.LOGGER;
import com.cptu.egp.eps.service.serviceinterface.QuizQuestionService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author feroz
 */
public class QuizQuestionServiceImpl implements QuizQuestionService{
    
    static final Logger LOGGER = Logger.getLogger(ConfigPaThresholdServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblQuizQuestionDao tblQuizQuestionDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    

    @Override
    public void addTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
            LOGGER.debug("addTblQuizQuestion: " + logUserId + LOGGERSTART);
            try {
            getTblQuizQuestionDao().addTblQuizQuestion(tblQuizQuestion);
            } catch (Exception e) {
                LOGGER.error("addTblQuizQuestion: " + logUserId + e);
            }
            LOGGER.debug("addTblQuizQuestion: " + logUserId + LOGGEREND);
    }

    @Override
    public void deleteTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
            LOGGER.debug("deleteTblQuizQuestion: " + logUserId + LOGGERSTART);
            try {
            getTblQuizQuestionDao().deleteTblQuizQuestion(tblQuizQuestion);
            } catch (Exception e) {
                LOGGER.error("deleteTblQuizQuestion: " + logUserId + e);
            }
            LOGGER.debug("deleteTblQuizQuestion: " + logUserId + LOGGEREND);
    }

    @Override
    public String updateTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
            boolean flag = false;
            LOGGER.debug("updateTblQuizQuestion: " + logUserId + LOGGERSTART);
            try {
            getTblQuizQuestionDao().updateTblQuizQuestion(tblQuizQuestion);
            flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblQuizQuestion: " + logUserId + e);
            }
            LOGGER.debug("updateTblQuizQuestion: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc";
            }
            else
            {
                return "failed";
            }
        
    }

    @Override
    public List<TblQuizQuestion> getAllTblQuizQuestion() {
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Starts");
        List<TblQuizQuestion> tblQuizQuestion = null;
        try {
            tblQuizQuestion = getTblQuizQuestionDao().getAllTblQuizQuestion();
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizQuestion : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Ends");
        return tblQuizQuestion;
    }

    @Override
    public List<TblQuizQuestion> findTblQuizQuestion(int id) {
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Starts");
        List<TblQuizQuestion> tblQuizQuestion = null;
        try {
            tblQuizQuestion = getTblQuizQuestionDao().findTblQuizQuestion("tblQuestionModule.moduleId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizQuestion : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Ends");
        return tblQuizQuestion;
    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public List<TblQuizQuestion> findTblQuizQuestionByQid(int id) {
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Starts");
        List<TblQuizQuestion> tblQuizQuestion = null;
        try {
            tblQuizQuestion = getTblQuizQuestionDao().findTblQuizQuestion("questionId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizQuestion : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizQuestion : " + logUserId + " Ends");
        return tblQuizQuestion;
    }
    
    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblConfigPaThresholdDao
     */
    public TblQuizQuestionDao getTblQuizQuestionDao() {
        return tblQuizQuestionDao;
    }

    /**
     * @param tblQuizQuestionDao the tblQuizQuestionDao to set
     */
    public void setTblQuizQuestionDao(TblQuizQuestionDao tblQuizQuestionDao) {
        this.tblQuizQuestionDao = tblQuizQuestionDao;
    }
    
}
