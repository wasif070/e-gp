/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblProgrammeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import com.cptu.egp.eps.service.serviceinterface.ProgrammeMasterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ProgrammeMasterServiceImpl implements ProgrammeMasterService {

    static final Logger logger = Logger.getLogger(ProgrammeMasterServiceImpl.class);
    TblProgrammeMasterDao tblProgMasterDao;
    private String logUserId = "0";
    MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;
    
    public TblProgrammeMasterDao getTblProgMasterDao() {
        return tblProgMasterDao;
    }

    public void setTblProgMasterDao(TblProgrammeMasterDao tblProgMasterDao) {
        this.tblProgMasterDao = tblProgMasterDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    /**
     * Method for checking ProgrammeMaster Name for validation.
     * @param pName
     * @return
     */
    @Override
    public String checkForName(String pName) {
        logger.debug("checkForName : " + logUserId + " Starts");
        String strStatus = "";
        int i = 0;
        try {
             i = tblProgMasterDao.findTblProgrammeMaster("progName", Operation_enum.EQ, pName).size(); //calling Dao method.
            if (i > 0) {
                strStatus = "Programme Name already exist.";
            } else {
                strStatus = "OK";
        }
        } catch (Exception e) {
            logger.error("checkForName : " + logUserId + e);
    }
        logger.debug("checkForName : " + logUserId + " Ends");
        return strStatus;
    }

    @Override
    public String checkForOwner(String pOwner) {
        logger.debug("checkForOwner : " + logUserId + " Starts");
        String strStatus = "";
        int i = 0;
        try {
             i = tblProgMasterDao.findTblProgrammeMaster("progOwner", Operation_enum.EQ, pOwner).size(); //calling Dao method.
            if (i > 0) {
                strStatus = "Programme Owner already exist.";
            } else {
                strStatus = "OK";
        }
        } catch (Exception e) {
            logger.error("checkForOwner : " + logUserId + e);
    }
        logger.debug("checkForOwner : " + logUserId + " Ends");
        return strStatus;
    }

    @Override
    public String editCheckOwner(int pId, String pOwner) {
        logger.debug("editCheckOwner : " + logUserId + " Starts");
        String retMsg = "";
        TblProgrammeMaster tblProgMaster;
        List<TblProgrammeMaster> tblProgrammeMasters = null;
        try {
            tblProgrammeMasters = tblProgMasterDao.findTblProgrammeMaster("progOwner", Operation_enum.LIKE, pOwner);
            if (tblProgrammeMasters.size() > 0) {
                tblProgMaster = tblProgrammeMasters.get(0);
                if (tblProgMaster.getProgId() == pId) {
                    retMsg = "OK";
                } else {
                    retMsg = "Programme Owner already exist.";
                }
            } else {
                retMsg = "OK";
            }
        } catch (Exception e) {
            logger.error("editCheckOwner : " + logUserId + e);
        }
        logger.debug("editCheckOwner : " + logUserId + " Ends");
        return retMsg;
    }

    @Override
    public String editCheckName(int pId, String pName) {
        logger.debug("editCheckName : " + logUserId + " Starts");
        String retMsg = "";
        TblProgrammeMaster tblProgMaster;
        List<TblProgrammeMaster> tblProgrammeMasters = null;
        try {
            tblProgrammeMasters = tblProgMasterDao.findTblProgrammeMaster("progName", Operation_enum.LIKE, pName);
            if (tblProgrammeMasters.size() > 0) {
                tblProgMaster = tblProgrammeMasters.get(0);
                if (tblProgMaster.getProgId() == pId) {
                    retMsg = "OK";
                } else {
                    retMsg = "Programme Name already exist.";
                }
            } else {
                retMsg = "OK";
            }
        } catch (Exception e) {
            logger.error("editCheckName : " + logUserId + e);
        }
        logger.debug("editCheckName : " + logUserId + " Ends");
        return retMsg;
    }

    /**
     *
     * Method for checking ProgrammeMaster code for validation.
     * @param pCode
     * @return
     */
    @Override
    public String checkForCode(String pCode) {
        logger.debug("checkForCode : " + logUserId + " Starts");
        String strStatus = "";
        int i = 0;
        try {
             i = tblProgMasterDao.findTblProgrammeMaster("progCode", Operation_enum.EQ, pCode).size(); //calling Dao method.
            if (i > 0) {
                strStatus = "Programme Code already exist.";
            } else {
                strStatus = "OK";
            }
        } catch (Exception e) {
            logger.error("checkForCode : " + logUserId + e);

        }
        logger.debug("checkForCode : " + logUserId + " Ends");
        return strStatus;
    }

    @Override
    public String editCheckCode(int pId, String pCode) {
        logger.debug("editCheckCode : " + logUserId + " Starts");
        String retMsg = "";
        TblProgrammeMaster tblProgMaster;
        List<TblProgrammeMaster> tblProgrammeMasters = null;
        try {
            tblProgrammeMasters = tblProgMasterDao.findTblProgrammeMaster("progCode", Operation_enum.LIKE, pCode);
            if (tblProgrammeMasters.size() > 0) {
                tblProgMaster = tblProgrammeMasters.get(0);
                if (tblProgMaster.getProgId() == pId) {
                    retMsg = "OK";
                } else {
                    retMsg = "Programme Code already exist.";
                }
            } else {
                retMsg = "OK";
            }
        } catch (Exception e) {
            logger.error("editCheckCode : " + logUserId + e);
        }
        logger.debug("checkForCode : " + logUserId + " Ends");
        return retMsg;
    }

    /**
     * Method for adding the details of ProgrammeMaster.
     * @param tblProgrammeMaster
     * @return
     */
    @Override
    public String addProgrammeMasterDetails(TblProgrammeMaster tblProgrammeMaster) {
        logger.debug("addProgrammeMasterDetails : " + logUserId + " Starts");
        String strReturnMsg = "";
        String action = "Create Project";
        try {
            tblProgMasterDao.addTblProgrammeMaster(tblProgrammeMaster); //calling Dao method.
            strReturnMsg = "Values Added";
        } catch (Exception e) {
            logger.error("addProgrammeMasterDetails : " + logUserId + e);
            strReturnMsg = "Data Not Added";
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblProgrammeMaster.getProgId(), "programId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("addProgrammeMasterDetails : " + logUserId + " Ends");
        return strReturnMsg;

    }

    /**
     *
     * Method for get the details of ProgrammeMaster details.
     * @return
     */
    @Override
    public List<TblProgrammeMaster> getProgrammeMasterDetails(String sord,String sidx) {
        logger.debug("getProgrammeMasterDetails : " + logUserId + " Starts");
        List<TblProgrammeMaster> lst = null;
        //lst = tblProgMasterDao.getAllTblProgrammeMaster(); //calling Dao method.
        if(sord==null && sidx==null)
        {
            lst = tblProgMasterDao.getAllTblProgrammeMaster(); //calling Dao method.
        }
        else
        {
            try {
                Object data = null;
                if ("asc".equals(sord)) {
                    data = Operation_enum.ASC;
                } else {
                    data = Operation_enum.DESC;
                }
                lst = tblProgMasterDao.findTblProgrammeMaster(sidx, Operation_enum.ORDERBY, data);
                if (lst.isEmpty()) {
                    lst = null;
                }
            } catch (Exception ex) {
                logger.error("getProgrammeMasterDetails : " + logUserId + ex);
            }
        }
        logger.debug("getProgrammeMasterDetails : " + logUserId + " Ends");
            return lst;
    }

    /**
     *
     * Method for get perticular id details of ProgrammeMaster.
     * @param id
     * @return
     */
    @Override
    public List<TblProgrammeMaster> getData(int id) {
        logger.debug("getData : " + logUserId + " Starts");
        String action = "View Programme";
        List<TblProgrammeMaster> lst = null;
        try {
            lst = tblProgMasterDao.findTblProgrammeMaster("progId", Operation_enum.EQ, id); //calling Dao method.
            if (lst.isEmpty()) {
                lst = null;
            }
        } catch (Exception e) {
            lst = null;
            logger.error("getData : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, lst.get(0).getProgId(), "programId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("getData : " + logUserId + " Ends");
                return lst;
        }

    /**
     *
     * Method for updating ProgrammeMater details.
     * @param tblProgrammeMaster
     */
    @Override
    public String updateProgrammeMasterDetails(TblProgrammeMaster tblProgrammeMaster) {
        logger.debug("updateProgrammeMasterDetails : " + logUserId + " Starts");
        String strReturnMsg = "";
        String action = "Edit Programme";
        try {
            tblProgMasterDao.updateTblProgrammeMaster(tblProgrammeMaster); //calling Dao method.
            strReturnMsg =  "Values Updated";
        } catch (Exception e) {
            logger.error("updateProgrammeMasterDetails : " + logUserId + e);
            strReturnMsg =  "Not Updated";
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblProgrammeMaster.getProgId(), "programId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("updateProgrammeMasterDetails : " + logUserId + " Ends");
        return  strReturnMsg;
    }

    @Override
    public List<TblProgrammeMaster> getProgrammeMasterBySearch(String columnName, String operator, String searchString) {
        logger.debug("getProgrammeMasterBySearch : " + logUserId + " Starts");
        List<TblProgrammeMaster> tblProgrammeMaster = null;
        try {
            if (operator.equalsIgnoreCase("EQ")) {
                    tblProgrammeMaster = tblProgMasterDao.findTblProgrammeMaster(columnName, Operation_enum.EQ, searchString);
            } else if (operator.equalsIgnoreCase("CN")) {
                tblProgrammeMaster = tblProgMasterDao.findTblProgrammeMaster(columnName, Operation_enum.LIKE, "%" + searchString + "%");
                }
            } catch (Exception ex) {
            logger.error("getProgrammeMasterBySearch : " + logUserId + ex);
            }
        logger.debug("getProgrammeMasterBySearch : " + logUserId + " Ends");
          return tblProgrammeMaster;
        }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    
}
