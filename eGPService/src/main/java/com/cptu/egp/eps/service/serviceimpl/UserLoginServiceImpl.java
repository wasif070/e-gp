/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigurationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmailVerificationCodeDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblSessionMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTrackLoginDao;
import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import com.cptu.egp.eps.model.table.TblTrackLogin;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.service.serviceinterface.UserLoginService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
/* for eventum issue 4297 (JVCA Registration Expiry Problem) */
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import java.text.SimpleDateFormat;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;




import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import sun.awt.AppContext;

/**
 *
 * @author Administrator
 */
public class UserLoginServiceImpl implements UserLoginService {

    final static Logger logger = Logger.getLogger(UserLoginServiceImpl.class);
    TblLoginMasterDao loginMasterDao;
    TblTrackLoginDao trackLoginDao;
    TblConfigurationMasterDao configurationMasterDao;
    TblUserTypeMasterDao userTypeMasterDao;
    TblSessionMasterDao tblSessionMasterDao;
    TblEmailVerificationCodeDao tblEmailVerificationCodeDao;
    UserRegisterService userRegisterService;
    private String userLoginStatus;
    private String ipAddress;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private static final String START = " Starts";
    private static final String END = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;
    private CommonSearchDataMoreService commonSearchDataMoreService;
    private ScBankDevpartnerService scBankDevpartnerService;

    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblEmailVerificationCodeDao getTblEmailVerificationCodeDao() {
        return tblEmailVerificationCodeDao;
    }

    public void setTblEmailVerificationCodeDao(TblEmailVerificationCodeDao tblEmailVerificationCodeDao) {
        this.tblEmailVerificationCodeDao = tblEmailVerificationCodeDao;
    }

    public TblSessionMasterDao getTblSessionMasterDao() {
        return tblSessionMasterDao;
    }

    public void setTblSessionMasterDao(TblSessionMasterDao tblSessionMasterDao) {
        this.tblSessionMasterDao = tblSessionMasterDao;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public TblConfigurationMasterDao getConfigurationMasterDao() {
        return configurationMasterDao;
    }

    public void setConfigurationMasterDao(TblConfigurationMasterDao configurationMasterDao) {
        this.configurationMasterDao = configurationMasterDao;
    }

    public TblLoginMasterDao getLoginMasterDao() {
        return loginMasterDao;
    }

    public void setLoginMasterDao(TblLoginMasterDao loginMasterDao) {
        this.loginMasterDao = loginMasterDao;
    }

    public TblTrackLoginDao getTrackLoginDao() {
        return trackLoginDao;
    }

    public void setTrackLoginDao(TblTrackLoginDao trackLoginDao) {
        this.trackLoginDao = trackLoginDao;
    }

    public TblUserTypeMasterDao getUserTypeMasterDao() {
        return userTypeMasterDao;
    }

    public void setUserTypeMasterDao(TblUserTypeMasterDao userTypeMasterDao) {
        this.userTypeMasterDao = userTypeMasterDao;
    }

    public String getUserLoginStatus() {
        return userLoginStatus;
    }

    public void setUserLoginStatus(String userLoginStatus) {
        this.userLoginStatus = userLoginStatus;
    }

    public UserRegisterService getUserRegisterService() {
        return userRegisterService;
    }

    public void setUserRegisterService(UserRegisterService userRegisterService) {
        this.userRegisterService = userRegisterService;
    }

//    else if (userType == 4) {
//                        returnVal = "resources/common/InboxMessage.jsp";
//                    }
//                    else if (userType == 5) {
//                        returnVal = "resources/common/InboxMessage.jsp";
//                    }
//                    else if (userType == 6) {
//                        returnVal = "resources/common/InboxMessage.jsp";
//                    }
//                    else if (userType == 7) {
//                        returnVal = "resources/common/InboxMessage.jsp";
//                    }
//                    else if (userType == 8) {
//                        returnVal = "admin/CompanyVerification.jsp";
//                    }
    // Table : tbl_LoginMaster,tbl_TrackLogin,tbl_ConfigrationMaster
    @Override
    public String checkLoginStatus(String emailId, String password, String ipAddress) {
        logger.debug("checkLoginStatus : " + logUserId + START);
        String returnVal = "Index.jsp";
        logger.debug(" in checkLoginStatus");
        String isPkiRequired = "";
        String regFeeRequired = "";

        Date validityUpTo = null;
        byte confFailedAttempt = 3;
        List<TblConfigurationMaster> configurationMasters = new ArrayList<TblConfigurationMaster>();

        String status = "";
        String screenToRedirect = "";
        String firstLogin = "";
        int userId = 0;
        byte failedAttempt = 2;

        boolean isEmailValid = false;
        boolean isEmailPasswordValidate = false;
        String isEmailVarify = "";
        /*for eventum issue 4297 (JVCA Registration Expiry Problem)*/
        String isJVCAid = "";
        /*for eventum issue 4297 (JVCA Registration Expiry Problem)*/
        List<TblLoginMaster> loginMasters = new ArrayList<TblLoginMaster>();
        TblLoginMaster master = new TblLoginMaster();
        int userType = 0;
        //TblUserTypeMaster tblUserTypeMaster=null;

        try {
            configurationMasters = configurationMasterDao.getAllTblConfigurationMaster();
            for (TblConfigurationMaster tblconfigurationMaster1 : configurationMasters) {
                isPkiRequired = tblconfigurationMaster1.getIsPkiRequired();
                regFeeRequired = tblconfigurationMaster1.getRegFeeRequired();
                confFailedAttempt = tblconfigurationMaster1.getFailedLoginAttempt();
                logger.debug("confFailedAttempt : " + logUserId + " ::" + confFailedAttempt);
            }
            // check emailid in tbl_LoginMaster
            loginMasters = loginMasterDao.findTblLoginMaster("emailId", Operation_enum.EQ, emailId);

            logger.debug("loginMasters : " + logUserId + " ::" + loginMasters.size());
            for (TblLoginMaster loginMaster : loginMasters) {
                BeanUtils.copyProperties(loginMaster, master);
                isEmailValid = true;
                failedAttempt = loginMaster.getFailedAttempt();
                logger.debug("failedAttempt : " + logUserId + " ::" + failedAttempt);
                if (password.equals(loginMaster.getPassword())) {
                    isEmailPasswordValidate = true;
                    userId = loginMaster.getUserId();
                    status = loginMaster.getStatus();
                    isJVCAid = loginMaster.getIsJvca();
                    /* for resolve issue 4297 jvca registration expire date*/
                    if ("no".equalsIgnoreCase(isJVCAid)) {
                        validityUpTo = loginMaster.getValidUpTo();
                    } else {
                        validityUpTo = jvcaValidityDate(String.valueOf(userId));
                    }
                    userType = loginMaster.getTblUserTypeMaster().getUserTypeId();
                    screenToRedirect = loginMaster.getNextScreen();
                    firstLogin = loginMaster.getFirstLogin();
                    isEmailVarify = loginMaster.getIsEmailVerified();
                }
            }
        } catch (Exception ex) {
            logger.error("checkLoginStatus : " + ex);
        }

        if (isEmailValid) {
            if (confFailedAttempt > failedAttempt) {
                if (isEmailPasswordValidate) {

                    master = userRegisterService.cloneTblLoginMaster(master);
                    master.setFailedAttempt((byte) 0);
                    //master.setTblTempTendererMasters(null);
                    updateLoginMaster(master);

                    this.ipAddress = ipAddress;
                    this.userLoginStatus = "successfully";
                    insertLoginTrack(userId);
//                    TblSessionMaster tblSessionMaster=new TblSessionMaster();
//                    tblSessionMaster.setIpAddress(ipAddress);
//                    tblSessionMaster.setSessionStartDt(new Date());
//                    tblSessionMaster.cloneTblLoginMaster(new TblLoginMaster(userId));
//                    tblSessionMaster.setSessionEndDt(new Date());
//                    int sessionId=insertSessionMaster(tblSessionMaster);
                   SPCommonSearchDataMore spcommonSearchData = null;
                    List<SPCommonSearchDataMore> SPCommonSearchDataMoreList = commonSearchDataMoreService.getCommonSearchData("getMobNoForSendVerifySMS",emailId);
                    if(SPCommonSearchDataMoreList!=null && !SPCommonSearchDataMoreList.isEmpty()){
                     spcommonSearchData = commonSearchDataMoreService.getCommonSearchData("getMobNoForSendVerifySMS",emailId).get(0);
                    }
                    if (userType == 2) {
                        if ("Approved".equalsIgnoreCase(status)) {
                            
                            String userId2 = Integer.toString(userId);
                            List<SPCommonSearchDataMore> list6 = commonSearchDataMoreService.getCommonSearchData("getAdminUserId",userId2);
                            String userId3 = list6.get(0).getFieldName1();
                            int userId4 = Integer.parseInt(userId3);
                            
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("select a.userId from TblBiddingPermission a where a.userId=" + userId4);
                            List<Object[]> list2 = hibernateQueryDao.createNewQuery(sb2.toString());
                            if(list2.isEmpty())
                            {
                                returnVal = "7";
                            }
                            else
                            {
                                boolean isValidDate = true;
                                if (validityUpTo != null) {
                                    Date curDate = new Date();
                                    long timeDiff = calculateDays(curDate, validityUpTo);
                                    if (timeDiff <= 0) {
                                        isValidDate = false;
                                        returnVal = "6";
                                        // Modified for expiry period laps
                                        Calendar calExpiry = Calendar.getInstance(), calCurrent = Calendar.getInstance();
                                        calExpiry.setTime(validityUpTo);
                                        calExpiry.add(Calendar.YEAR, 1);
                                        validityUpTo = calExpiry.getTime();
                                        if (!(validityUpTo.after(curDate) || validityUpTo.equals(curDate))) {
                                            returnVal = "9";
                                        }
                                    }
                                }
                                if (isValidDate && "yes".equalsIgnoreCase(regFeeRequired)) {
                                    if (validityUpTo.after(new Date())) {
                                        if ("yes".equalsIgnoreCase(isPkiRequired)) {
                                            //returnVal = "Pki.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        } else {
                                            if ("no".equalsIgnoreCase(firstLogin)) {
                                                // returnVal="Pki.jsp";
                                                //if (spcommonSearchData.getFieldName2().equals("0"))
                                                //     returnVal = "admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit&hasMobile=No";
                                                // else
                                                //returnVal = "resources/common/InboxMessage.jsp";
                                                returnVal = "resources/common/Dashboard.jsp";

                                            } else {
                                               returnVal = "admin/ChangePassword.jsp";
                                               //returnVal = "resources/common/Dashboard.jsp";
                                            }
                                            //returnVal = "Index.jsp";

                                        }
                                    } else {
                                        returnVal = returnVal;
                                    }
                                } else {
                                    if (isValidDate) {
                                        if ("yes".equalsIgnoreCase(isPkiRequired)) {
                                            //returnVal = "Pki.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        } else {
                                            //returnVal = "Index.jsp";
                                            //TaherT Debarment Condition added.
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("select tdd.debarStartDt,tdd.debarEdnDt from TblDebarmentReq tdr,TblDebarmentDetails tdd");
                                            sb.append(" where (userId in (select tm.tblLoginMaster.userId from TblTendererMaster tm where tm.tblCompanyMaster.companyId in (select a.tblCompanyMaster.companyId from TblTendererMaster a where a.tblLoginMaster.userId=" + userId + ") and tm.tblCompanyMaster.companyId!=1) or userId=" + userId + ") and tdr.debarmentId=tdd.tblDebarmentReq.debarmentId ");
                                            sb.append("and tdd.debarStatus='byhope' and tdr.debarmentStatus in ('appdebaregp') and tdd.debarTypeId=6");
                                            //sb.append("select * from TblBiddingPermission where TblBiddingPermission.userId=" + userId);
                                            List<Object[]> list = hibernateQueryDao.createNewQuery(sb.toString());
                                            if (list.isEmpty()) {
                                                //returnVal = "7";
                                                if ("no".equalsIgnoreCase(firstLogin)) {
                                                    // returnVal="Pki.jsp";
                                                    //if (spcommonSearchData.getFieldName2().equals("0"))
                                                    //    returnVal = "admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit&hasMobile=No";
                                                    //else
                                                   // returnVal = "resources/common/InboxMessage.jsp";
                                                   returnVal = "resources/common/Dashboard.jsp";
                                                } else {
                                                    returnVal = "admin/ChangePassword.jsp";
                                                    //returnVal = "resources/common/Dashboard.jsp";
                                                }
                                            } 
                                            else {
                                                Date startDate = (Date) list.get(0)[0];
                                                Date endDate = (Date) list.get(0)[1];
                                                
                                                if (startDate.before(new Date()) && endDate.after(new Date())) {
                                                    returnVal = "7";
                                                } else {
                                                    if ("no".equalsIgnoreCase(firstLogin)) {
                                                        // returnVal="Pki.jsp";
                                                        //if (spcommonSearchData.getFieldName2().equals("0"))
                                                        //    returnVal = "admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit&hasMobile=No";
                                                        //else
                                                        //returnVal = "resources/common/InboxMessage.jsp";
                                                        returnVal = "resources/common/Dashboard.jsp";
                                                    } else {
                                                        returnVal = "admin/ChangePassword.jsp";
                                                       // returnVal = "resources/common/Dashboard.jsp";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else if ("Incomplete".equalsIgnoreCase(status)) {
                            if ("yes".equalsIgnoreCase(isEmailVarify)) {
                                returnVal = screenToRedirect + ".jsp";
                            } else {
                                returnVal = "1";
                            }
                        } else if ("Pending".equalsIgnoreCase(status)) {
                            returnVal = "UserStatus.jsp?status=p";
                        } else if ("Rejected".equalsIgnoreCase(status)) {
                            returnVal = "UserStatus.jsp?status=r";
                        } else if ("Blacklisted".equalsIgnoreCase(status)) {
                            returnVal = "UserStatus.jsp?status=b";
                        } else if ("suspended".equalsIgnoreCase(status)) {
                            returnVal = "UserStatus.jsp?status=sus";
                        } else if ("disable".equalsIgnoreCase(status)) {
                            returnVal = "UserStatus.jsp?status=dis";
                        } else {
                            returnVal = returnVal;
                        }
                    } else {
                        if ("Approved".equalsIgnoreCase(status)) {
                            if ("yes".equalsIgnoreCase(isPkiRequired)) {
                                if ("no".equalsIgnoreCase(firstLogin)) {
                                    // returnVal="Pki.jsp";
                                    //returnVal = "admin/AdminDashboard.jsp";
                                    returnVal = "resources/common/Dashboard.jsp";
                                } else {
                                    returnVal = "admin/ChangePassword.jsp";
                                    //returnVal = "resources/common/Dashboard.jsp";
                                }
                            } else {
                                if ("no".equalsIgnoreCase(firstLogin)) {
                                    //returnVal = "Index.jsp";
                                    //returnVal="PkiNon.jsp";
                                    //CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");

                                    if (spcommonSearchData.getFieldName2().equals("0")) {
                                        if (userType == 3) {
                                            //returnVal = "admin/GovtUserCreation.jsp?fromWhere=EditProfile&Edit=Edit&hasMobile=No";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        } else if (userType == 4) {
                                            //returnVal = "admin/ManagePEAdmin.jsp?userId=" + userId + "&userTypeid=" + userType + "&hasMobile=No";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        } else if (userType == 5 || userType == 8 || userType == 19 || userType == 20 || userType == 21) {
                                           // returnVal = "admin/ManageAdmin.jsp?userId=" + userId + "&userTypeid=" + userType + "&hasMobile=No";
                                           returnVal = "resources/common/Dashboard.jsp";
                                        } else if (userType == 15) {
                                            //returnVal = "admin/EditSbBranchAdmin.jsp?userId=" + userId + "&partnerType=ScheduleBank&userTypeId=15&mode=edit&hasMobile=No";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        } else if (userType == 7) {
                                            if (getScBankDevpartnerService().isAdmin(userId)) {
                                                //returnVal = "admin/EditSbDevPartAdmin.jsp?userId=" + userId + "&partnerType=ScheduleBank&userTypeId=7&mode=edit&hasMobile=No";
                                                returnVal = "resources/common/Dashboard.jsp";
                                            } else {
                                                //returnVal = "admin/EditSbDevPartUser.jsp?userId=" + userId + "&partnerType=ScheduleBank&userTypeId=7&mode=edit&hasMobile=No";
                                                returnVal = "resources/common/Dashboard.jsp";
                                            }
                                        } else if(userType == 1){
                                            //returnVal = "admin/ListSTD.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        }
                                        else {
                                            //returnVal = "resources/common/InboxMessage.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        }
                                    } else {
                                        if (userType == 3) {
                                            //returnVal = "resources/common/PaDashboard.jsp?viewtype=pending";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        }
                                        
                                        else if(userType == 1 || userType == 4){
                                            //returnVal = "admin/AdminDashboard.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        }

                                        else if(userType == 5){
                                            //returnVal="admin/EditPEOfficeGrid.jsp?deptType=Organization";
                                           // returnVal = "resources/common/RedirectToEditPeOffice.jsp";
                                           returnVal = "resources/common/Dashboard.jsp";
                                        }
                                        else if(userType == 8){
                                            //returnVal="admin/EditPEOfficeGrid.jsp?deptType=Organization";
                                            //returnVal = "admin/CompanyVerification.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                            
                                        }

                                        else {
                                            //returnVal = "resources/common/InboxMessage.jsp";
                                            returnVal = "resources/common/Dashboard.jsp";
                                        }
                                    }
                                } else {
                                    returnVal = "admin/ChangePassword.jsp";
                                    //returnVal = "resources/common/Dashboard.jsp";
                                }
                            }
                        } else if ("Deactive".equalsIgnoreCase(status)) {
                            returnVal = "8";
                        } else {
                            returnVal = "5";
                        }
                    }
                } else {
                    master = userRegisterService.cloneTblLoginMaster(master);
                    master.setFailedAttempt((byte) ++failedAttempt);
                    updateLoginMaster(master);
                    this.ipAddress = ipAddress;
                    userLoginStatus = "unsuccessfully";
                    insertLoginTrack(userId);
                    returnVal = "2";
                }
            } else {
                if (isEmailPasswordValidate) {
                    returnVal = "4a";
                } else {
                    returnVal = "4";
                }
            }
        } else {
            returnVal = "3";
        }
        logger.debug("checkLoginStatus : " + logUserId + END);
        return returnVal;   
    }

    @Override
    public boolean insertLoginTrack(int userId) {
        logger.debug("insertLoginTrack : " + logUserId + START);
        boolean flag;
        TblTrackLogin trackLogin = null;
        try {
            logger.debug(" in insertLoginTrack : ");
            trackLogin = new TblTrackLogin();
            trackLogin.setUserId(userId);
            trackLogin.setLoginAttemptDate(new Date());
            trackLogin.setLoginStatus(userLoginStatus);
            trackLogin.setIpAddress(ipAddress);
            trackLoginDao.addTblTrackLogin(trackLogin);
            flag = true;
        } catch (Exception e) {
            logger.error("insertLoginTrack : " + logUserId + " : " + e);
            flag = false;
        } finally {
            trackLogin = null;
        }
        logger.debug("insertLoginTrack : " + logUserId + END);
        return flag;
    }

    @Override
    public boolean updateLoginMaster(TblLoginMaster loginMaster, String... values) {
        logger.debug("updateLoginMaster : " + logUserId + START);
        boolean flag;
        String action = null;
        try {
            loginMasterDao.updateTblLoginMaster(loginMaster);
            flag = true;
            if (values != null && values.length != 0) {
                if ("hintAns".equalsIgnoreCase(values[0])) {
                    action = "Change Hint Question and Answer";
                } else if ("changePass".equalsIgnoreCase(values[0])) {
                    action = "Change Password";
                }
            }
        } catch (Exception e) {
            logger.debug("updateLoginMaster : " + logUserId + " : " + e);
            action = "Error in " + action + " :" + e.getMessage();
            flag = false;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, loginMaster.getUserId(), "userId", EgpModule.My_Account.getName(), action, "");
            action = null;
        }
        logger.debug("updateLoginMaster : " + logUserId + END);
        return flag;
    }

    @Override
    public String verifyEmail(String mailId, String password) {
        logger.debug("verifyEmail : " + logUserId + START);
        String str = "Error: Verify Mail - Login Exception";
        try {
            Object[] values = {"emailId", Operation_enum.EQ, mailId, "password", Operation_enum.EQ, password};
            List<TblLoginMaster> tblLoginMasters = loginMasterDao.findTblLoginMaster(values);
            int size = tblLoginMasters.size();
            if (size > 0) {
                TblLoginMaster tblLoginMaster = tblLoginMasters.get(0);
                if ("yes".equalsIgnoreCase(tblLoginMaster.getIsEmailVerified())) {
                    str = "Error: This Email ID is already verified, please login from home page using your registered and verified Email ID";
                } else {
                    tblLoginMaster.setIsEmailVerified("Yes");
                    tblLoginMaster.setTransdate(new Date());
                    loginMasterDao.updateTblLoginMaster(tblLoginMaster);
                    str = tblLoginMaster.getNextScreen() + "-" + tblLoginMaster.getUserId();
                }
            } else {
                logger.debug("Verify Mail - Record NOT Found!! : " + logUserId);
                str = "Error:Please Enter the Correct UserName & Password";
            }
        } catch (Exception ex) {
            logger.error("verifyEmail : " + logUserId + "  : " + ex);
        }
        logger.debug("verifyEmail : " + logUserId + END);
        return str;
    }

    @Override
    public Date getLastLoginTime(int userId) {
        logger.debug("getLastLoginTime : " + logUserId + START);
        Date date = null;
        try {
            List<TblSessionMaster> sessionMasters = tblSessionMasterDao.findTblSessionMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
            if (sessionMasters != null && !sessionMasters.isEmpty()) {
                Date lastloginDt = sessionMasters.get(sessionMasters.size() - 1).getSessionStartDt();
                date = lastloginDt;
            }
        } catch (Exception ex) {
            logger.error("getLastLoginTime : " + logUserId + " : " + ex);
        }
        logger.debug("getLastLoginTime : " + logUserId + END);
        return date;
    }

    @Override
    public int insertSessionMaster(TblSessionMaster tblSessionMaster) {
        logger.debug("insertSessionMaster : " + logUserId + START);
        int i;
        try {
            tblSessionMasterDao.addTblSessionMaster(tblSessionMaster);
            i = tblSessionMaster.getSessionId();
        } catch (Exception ex) {
            logger.error("insertSessionMaster : " + logUserId + " : " + ex.getMessage());
            i = -1;
        }
        logger.debug("insertSessionMaster : " + logUserId + END);
        return i;
    }

    @Override
    public boolean updateSessionMaster(int sessionId) {
        logger.debug("updateSessionMaster : " + logUserId + START);
        boolean flag = false;
        try {
            List<TblSessionMaster> tblSessionMasters = tblSessionMasterDao.findTblSessionMaster("sessionId", Operation_enum.EQ, sessionId);
            if (tblSessionMasters != null && !tblSessionMasters.isEmpty()) {
                TblSessionMaster tblSessionMaster = tblSessionMasters.get(0);
                tblSessionMaster.setSessionEndDt(new Date());
                tblSessionMasterDao.updateTblSessionMaster(tblSessionMaster);
                flag = true;
            }

        } catch (Exception ex) {
            logger.error("updateSessionMaster : Session Update Exception:: " + logUserId + " :" + ex.getMessage());
        }
        logger.debug("updateSessionMaster : " + logUserId + END);
        return flag;
    }

    @Override
    public Boolean verifyPwd(String password, Integer userId) {

        logger.debug("verifyPwd : " + logUserId + START);
        boolean flag;
        try {
            List<TblLoginMaster> tblLoginMasters = loginMasterDao.findTblLoginMaster("password", Operation_enum.LIKE, password, "userId", Operation_enum.EQ, userId);
            if (tblLoginMasters.isEmpty()) {
                flag = false;
            } else {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("verifyPwd : " + logUserId + " : " + e.toString());
            flag = false;
        }
        //throw new UnsupportedOperationException("Not supported yet.");
        logger.debug("verifyPwd : " + logUserId + END);
        return flag;
    }

    @Override
    public int verifyPwdforVerification(String password, String emailId) {
        logger.debug("verifyPwdforVerification : " + logUserId + START);
        int i;
        try {
            List<TblLoginMaster> tblLoginMasters = loginMasterDao.findTblLoginMaster("password", Operation_enum.LIKE, password, "emailId", Operation_enum.EQ, emailId);
            if (!tblLoginMasters.isEmpty()) {
                int userId = tblLoginMasters.get(0).getUserId();
                String isVarified = tblLoginMasters.get(0).getIsEmailVerified();
                if (isVarified.equalsIgnoreCase("no")) {
                    i = userId;
                } else {
                    i = userId = -1;
                }
            } else {
                i = 0;
            }
        } catch (Exception ex) {
            logger.debug("verifyPwdforVerification : " + logUserId + " : " + ex);
            i = 0;
        }
        logger.debug("verifyPwdforVerification : " + logUserId + END);
        return i;
    }

    @Override
    public List<TblEmailVerificationCode> getVeriCode(int userId) {
        logger.debug("getVeriCode : " + logUserId + START);
        List<TblEmailVerificationCode> list = null;
        try {
            list = tblEmailVerificationCodeDao.findTblEmailVerificationCode("userId", Operation_enum.EQ, userId);
        } catch (Exception ex) {
            logger.error("getVeriCode : " + logUserId + " : " + ex);
            list = null;
        }
        logger.debug("getVeriCode : " + logUserId + END);
        return list;
    }

    @Override
    public void deleteVerifyCode(int emailVerificationId) {
        logger.debug("deleteVerifyCode : " + logUserId + START);
        TblEmailVerificationCode tblEmailVerificationCode = new TblEmailVerificationCode();
        tblEmailVerificationCode.setEmailVerificationId(emailVerificationId);
        tblEmailVerificationCode.setGeneratedDt(new Date());
        tblEmailVerificationCode.setRandCode("");
        tblEmailVerificationCode.setUserId(0);
        tblEmailVerificationCodeDao.deleteTblEmailVerificationCode(tblEmailVerificationCode);

//        if (tblEmailVerificationCode != null) {
//            tblEmailVerificationCode = null;
//        }
        logger.debug("deleteVerifyCode : " + logUserId + END);
    }

    @Override
    public List<TblLoginMaster> getLoginMasterDetails(int userId) {
        logger.debug("getLoginMasterDetails : " + logUserId + START);
        List<TblLoginMaster> list = null;
        try {
            list = loginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId);
        } catch (Exception ex) {
            logger.debug("getLoginMasterDetails : " + logUserId + "  : " + ex);
            list = null;
        }
        logger.debug("getLoginMasterDetails : " + logUserId + END);
        return list;
    }

    @Override
    public List<TblLoginMaster> getExpiredUsers(Date expiryDate) {
        logger.debug("getExpiredUsers : " + logUserId + START);
        List<TblLoginMaster> listLoginMaster = null;
        try {
            listLoginMaster = loginMasterDao.findTblLoginMaster("validUpTo", Operation_enum.EQ, expiryDate);
        } catch (Exception ex) {
            logger.error("getExpiredUsers : " + logUserId + " : " + ex);
        }
        logger.debug("getExpiredUsers : " + logUserId + " Ends ");
        return listLoginMaster;
    }

    public static long calculateDays(Date dateEarly, Date dateLater) {
        return (dateLater.getTime() - dateEarly.getTime()) / (24 * 60 * 60 * 1000);
    }

    @Override
    public String getRegType(int userId) {
        logger.debug("getRegType : " + logUserId + " Starts ");
        List<TblLoginMaster> listLoginMaster = new ArrayList<TblLoginMaster>();
        String strRegType = "";

        try {
            listLoginMaster = loginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId);
        } catch (Exception ex) {
            logger.error("getRegType : " + logUserId + " : " + ex);
        }
        if (!listLoginMaster.isEmpty()) {
            strRegType = listLoginMaster.get(0).getRegistrationType();
        }
        logger.debug("getRegType : " + logUserId + " Ends ");
        return strRegType;
    }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public String jvcaName(int jvcId) {
        logger.debug("jvcaName : " + logUserId + " Starts ");
        Object obj = null;
        try {
            obj = hibernateQueryDao.createNewQuery("select jvname from TblJointVenture where jvid=" + jvcId).get(0);
        } catch (Exception e) {
            logger.error("jvcaName : " + logUserId + " : " + e);

        }
        logger.debug("jvcaName : " + logUserId + " Ends ");
        return obj.toString();
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    /*for eventum issue 4297 (JVCA Registration Expiry Problem)*/

    /**
     * @return the commonSearchDataMoreService
     */
    public CommonSearchDataMoreService getCommonSearchDataMoreService() {
        return commonSearchDataMoreService;
    }

    /**
     * @param commonSearchDataMoreService the commonSearchDataMoreService to set
     */
    public void setCommonSearchDataMoreService(CommonSearchDataMoreService commonSearchDataMoreService) {
        this.commonSearchDataMoreService = commonSearchDataMoreService;
    }

    /**
     *
     */
    public Date jvcaValidityDate(String userId) {
        logger.debug("jvcaValidityDate : " + userId + " Starts ");
        List<SPCommonSearchDataMore> list = commonSearchDataMoreService.geteGPData("getJVCAExpiryDate", userId);
        String msg = "0";
        Date validityUpTo = null;
        if (!list.isEmpty()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
            try {
                validityUpTo = simpleDateFormat.parse(list.get(0).getFieldName1());
            } catch (Exception e) {
                logger.error("jvcaValidityDate : " + userId + " : Error in jvca validity date check " + e);
            }
        }
        if (msg.equals("0")) {
            logger.error("jvcaValidityDate : " + userId + " : Error in jvca validity date check");
        }
        return validityUpTo;
    }

    /**
     * @return the scBankDevpartnerService
     */
    public ScBankDevpartnerService getScBankDevpartnerService() {
        return scBankDevpartnerService;
    }

    /**
     * @param scBankDevpartnerService the scBankDevpartnerService to set
     */
    public void setScBankDevpartnerService(ScBankDevpartnerService scBankDevpartnerService) {
        this.scBankDevpartnerService = scBankDevpartnerService;
    }
    /*for eventum issue 4297 (JVCA Registration Expiry Problem)*/
}
