/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.UserTypeService;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class UserTypeServiceImpl implements UserTypeService {

    static final Logger logger = Logger.getLogger(UserTypeServiceImpl.class);
    TblUserTypeMasterDao tblusertypemasterdao;

    public TblUserTypeMasterDao getTblusertypemasterdao() {
        return tblusertypemasterdao;
    }

    public void setTblusertypemasterdao(TblUserTypeMasterDao tblusertypemasterdao) {
        this.tblusertypemasterdao = tblusertypemasterdao;
    }

    @Override
    public void addUsertype(TblUserTypeMaster tblUsertypemaster) {

        tblusertypemasterdao.addTblUserTypeMaster(tblUsertypemaster);

    }

    
}
