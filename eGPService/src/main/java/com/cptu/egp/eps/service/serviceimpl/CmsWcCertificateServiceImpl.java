/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertiHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertificateDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import com.cptu.egp.eps.service.serviceinterface.CmsWcCertificateService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsWcCertificateServiceImpl implements CmsWcCertificateService {

    final Logger logger = Logger.getLogger(CmsWcCertificateServiceImpl.class);
    
    private TblCmsWcCertificateDao cmsWcCertificateDao;
    private TblCmsWcCertiHistoryDao tblCmsWcCertiHistoryDao;
    private HibernateQueryDao hibernateQueryDao;
    
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "   ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsWcCertificateDao getCmsWcCertificateDao() {
        return cmsWcCertificateDao;
    }

    public void setCmsWcCertificateDao(TblCmsWcCertificateDao cmsWcCertificateDao) {
        this.cmsWcCertificateDao = cmsWcCertificateDao;
    }

    public TblCmsWcCertiHistoryDao getTblCmsWcCertiHistoryDao() {
        return tblCmsWcCertiHistoryDao;
    }

    public void setTblCmsWcCertiHistoryDao(TblCmsWcCertiHistoryDao tblCmsWcCertiHistoryDao) {
        this.tblCmsWcCertiHistoryDao = tblCmsWcCertiHistoryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    /***
     * This method inserts a TblCmsWcCertificate object in database
     * @param tblCmsWcCertificate
     * @return int
     */
    @Override
    public int insertCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("insertCmsWcCertificate : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsWcCertificateDao.addTblCmsWcCertificate(tblCmsWcCertificate);
            flag = tblCmsWcCertificate.getWcCertId();
        } catch (Exception ex) {
            logger.error("insertCmsWcCertificate : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsWcCertificate : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsWcCertificate object in DB
     * @param tblCmsWcCertificate
     * @return boolean
     */
    @Override
    public boolean updateCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("updateCmsWcCertificate : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsWcCertificateDao.updateTblCmsWcCertificate(tblCmsWcCertificate);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsWcCertificate : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsWcCertificate : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsWcCertificate object in DB
     * @param tblCmsWcCertificate
     * @return boolean
     */
    @Override
    public boolean deleteCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("deleteCmsWcCertificate : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsWcCertificateDao.deleteTblCmsWcCertificate(tblCmsWcCertificate);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsWcCertificate : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsWcCertificate : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsWcCertificate objects from database
     * @return List<TblCmsWcCertificate>
     */
    @Override
    public List<TblCmsWcCertificate> getAllCmsWcCertificate() {
        logger.debug("getAllCmsWcCertificate : " + logUserId + STARTS);
        List<TblCmsWcCertificate> cmsWcCertificateList = new ArrayList<TblCmsWcCertificate>();
        try {
            cmsWcCertificateList = cmsWcCertificateDao.getAllTblCmsWcCertificate();
        } catch (Exception ex) {
            logger.error("getAllCmsWcCertificate : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsWcCertificate : " + logUserId + ENDS);
        return cmsWcCertificateList;
    }

    /***
     * This method returns no. of TblCmsWcCertificate objects from database
     * @return long
     */
    @Override
    public long getCmsWcCertificateCount() {
        logger.debug("getCmsWcCertificateCount : " + logUserId + STARTS);
        long cmsWcCertificateCount = 0;
        try {
            cmsWcCertificateCount = cmsWcCertificateDao.getTblCmsWcCertificateCount();
        } catch (Exception ex) {
            logger.error("getCmsWcCertificateCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsWcCertificateCount : " + logUserId + ENDS);
        return cmsWcCertificateCount;
    }

    /***
     * This method returns TblCmsWcCertificate for the given wcCertId
     * @param int wcCertId
     * @return TblCmsWcCertificate
     */
    @Override
    public TblCmsWcCertificate getCmsWcCertificate(int wcCertId) {
        logger.debug("getCmsWcCertificate : " + logUserId + STARTS);
        TblCmsWcCertificate tblCmsWcCertificate = null;
        List<TblCmsWcCertificate> cmsWcCertificateList = null;
        try {
            cmsWcCertificateList = cmsWcCertificateDao.findTblCmsWcCertificate("wcCertId", Operation_enum.EQ, wcCertId);
        } catch (Exception ex) {
            logger.error("getCmsWcCertificate : " + logUserId + SPACE + ex);
        }
        if (!cmsWcCertificateList.isEmpty()) {
            tblCmsWcCertificate = cmsWcCertificateList.get(0);
        }
        logger.debug("getCmsWcCertificate : " + logUserId + ENDS);
        return tblCmsWcCertificate;
    }

    /***
     * This method gives the TblCmsWcCertificate object for the given contractSignId
     * @param contractSignId
     * @return TblCmsWcCertificate
     */
    @Override
    public TblCmsWcCertificate getCmsWcCertificateForContractSignId(int contractSignId) {
        logger.debug("getCmsWcCertificateForContractSignId : " + logUserId + STARTS);
        TblCmsWcCertificate tblCmsWcCertificate = null;
        List<TblCmsWcCertificate> cmsWcCertificateList = null;
        try {
            cmsWcCertificateList = cmsWcCertificateDao.findTblCmsWcCertificate("contractId", Operation_enum.EQ, contractSignId);
        } catch (Exception ex) {
            logger.error("getCmsWcCertificateForContractSignId : " + logUserId + SPACE + ex);
        }
        if (!cmsWcCertificateList.isEmpty()) {
            tblCmsWcCertificate = cmsWcCertificateList.get(0);
        }
        logger.debug("getCmsWcCertificateForContractSignId : " + logUserId + ENDS);
        return tblCmsWcCertificate;
    }
    
    @Override
    public int insertCmsWcCertihistory(TblCmsWcCertiHistory tblCmsWcCertiHistory){
        logger.debug("insertCmsWcCertihistory : " + logUserId + STARTS);
        int flag = 0;
        try {
            tblCmsWcCertiHistoryDao.addTblCmsWcCertiHistory(tblCmsWcCertiHistory);
            flag = tblCmsWcCertiHistory.getWcCertiHistId();
        } catch (Exception ex) {
            logger.error("insertCmsWcCertihistory : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsWcCertihistory : " + logUserId + ENDS);
        return flag;
    }
    
    @Override
     public int getMaxHistoryCount(int wccertId){
        logger.debug("getMaxHistoryCount : " + logUserId + STARTS);
        List<Object> list = new ArrayList<Object>();
        int historyId = 0;
        try {
            list = hibernateQueryDao.singleColQuery("select max(cwch.wcCertiHistId) as maxcount from TblCmsWcCertiHistory cwch where cwch.wcCertId = "+wccertId);
            if(!list.isEmpty()){
                historyId = Integer.parseInt(list.get(0).toString());
            }
        } catch (Exception ex) {
            logger.error("getMaxHistoryCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getMaxHistoryCount : " + logUserId + ENDS);
        return historyId;
     }
    
    @Override
    public List<Object[]> getWcCetificateDetails(int wcCertiId){

        logger.debug("getWcCetificateDetails : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String sqlQuery = "select tcwch.wcCertiHistId,tcwch.createdDt, tcwc.contractId "
                            + "from TblCmsWcCertificate tcwc, TblCmsWcCertiHistory tcwch "
                            + "where tcwc.wcCertId = tcwch.wcCertId and tcwc.wcCertId = "+wcCertiId
                            + " order by tcwch.wcCertiHistId desc";
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception ex) {
            logger.error("getWcCetificateDetails : " + logUserId + SPACE + ex);
        }
        logger.debug("getWcCetificateDetails : " + logUserId + ENDS);
        return list;
    }
    
    @Override
    public List<Object[]> getWcCetificateHistoryDetails(int wcCertiHistId){
        
        logger.debug("getWcCetificateHistoryDetails : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String sqlQuery = "select "
                            + "tcwch.actualDateOfComplete,tcwch.isWorkComplete,tcwch.vendorRating, "
                            + "tcwch.remarks,tcwc.sendToTenderer from TblCmsWcCertificate tcwc, TblCmsWcCertiHistory tcwch "
                            + "where tcwc.wcCertId = tcwch.wcCertId and tcwch.wcCertiHistId = "+wcCertiHistId;
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception ex) {
            logger.error("getWcCetificateHistoryDetails : " + logUserId + SPACE + ex);
        }
        logger.debug("getWcCetificateHistoryDetails : " + logUserId + ENDS);
        return list;
        
    }
}
