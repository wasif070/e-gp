/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jdk.nashorn.internal.objects.NativeArray;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TenderCommonService {

    private static final Logger LOGGER = Logger.getLogger(TenderCommonService.class);
    private SPTenderCommon sPTenderCommon;
    private HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    /**
     * fetching information from p_get_tendercommondata
     *
     * @param funName method name
     * @param funName1
     * @param funName2
     * @return List of SPTenderCommonData
     */
    public List<SPTenderCommonData> returndata(String funName, String funName1, String funName2) {
        LOGGER.debug("returndata : " + logUserId + " : Starts");
        List<SPTenderCommonData> returndata = null;
        try {
            returndata = sPTenderCommon.executeProcedure(funName, funName1, funName2);
        } catch (Exception e) {
            LOGGER.error("returndata : " + e);
        }
        LOGGER.debug("returndata : " + logUserId + " : Ends");
        return returndata;
    }

    public List<SPTenderCommonData> GetBOQ(String funName, String pageNo, String pageSize) {
        LOGGER.debug("GetBOQ : " + logUserId + " : Starts");
        List<SPTenderCommonData> returndata = null;
        try {
            returndata = sPTenderCommon.executeProcedure(funName, pageNo, pageSize);
        } catch (Exception e) {
            LOGGER.error("GetBOQ : " + e);
        }
        LOGGER.debug("GetBOQ : " + logUserId + " : Ends");
        return returndata;
    }

    /**
     * Get CompanyId and check whether it is company admin
     *
     * @param userId to be checked
     * @return List of {companyId,isAdmin}
     */
    public List<Object[]> getCompnyIDAndCompanyAdmin(int userId) {
        LOGGER.debug("getCompnyIDAndCompanyAdmin : " + logUserId + " : Starts");
        List<Object[]> returndata = null;
        String query = "select tblCompanyMaster.companyId,isAdmin from TblTendererMaster where tblLoginMaster.userId='" + userId + "'";
        try {
            returndata = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            LOGGER.error("getCompnyIDAndCompanyAdmin : " + e);
        }
        LOGGER.debug("getCompnyIDAndCompanyAdmin : " + logUserId + " : Ends");
        return returndata;
    }
    
    public List<Object[]> checkTable(int tableId) {
        LOGGER.debug("checkForm : " + logUserId + " : Starts");
        List<Object[]> returndata = null;
        String query = "select tenderCelId from TblTenderCells where tenderTableId='" + tableId + "'";
        try {
            returndata = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            LOGGER.error("checkForm : " + e);
        }
        LOGGER.debug("checkForm : " + logUserId + " : Ends");
        return returndata;
    }
    public List<Object[]> checkForm(int formId) {
        LOGGER.debug("checkForm : " + logUserId + " : Starts");
        List<Object[]> returndata = null;
        String query = "select tenderTableId from TblTenderTables where tenderFormId='" + formId + "'";
        try {
            returndata = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            LOGGER.error("checkForm : " + e);
        }
        LOGGER.debug("checkForm : " + logUserId + " : Ends");
        return returndata;
    }
    
    public List<Object[]> getCompanyPermissions(String userId) {
        LOGGER.debug("getCompanyPermissions : " + logUserId + " : Starts");
        List<Object[]> returndata = null;
        String query = "select tbp.workType, tbp.workCategroy from TblBiddingPermission tbp where tbp.isReapplied = 0 and tbp.userId='" + userId + "'";
        try {
            returndata = hibernateQueryDao.createQuery(query);
        } catch (Exception e) {
            LOGGER.error("getCompanyPermissions : " + e);
        }
        LOGGER.debug("getCompanyPermissions : " + logUserId + " : Ends");
        return returndata;
    }

    /**
     * Check Rights of Current User for the Tender
     *
     * @param userId to be checked on
     * @param tenderId to be checked for
     * @return 0 or 1 for no rights or have rights
     */
    public long getAllDetailsOfAssignedUser(int userId, int tenderId) {
        LOGGER.debug("getAllDetailsOfAssignedUser : " + logUserId + " : Starts");

        long count = 0;
        String query = "userId='" + userId + "' and tblTenderMaster.tenderId='" + tenderId + "' and isCurrent='yes'";
        try {
            count = hibernateQueryDao.countForNewQuery("TblTenderRights", query);

        } catch (Exception e) {
            LOGGER.error("getAllDetailsOfAssignedUser : " + e);
        }
        LOGGER.debug("getAllDetailsOfAssignedUser : " + logUserId + " : Ends");
        return count;
    }

    /**
     * Particular tender is archive or not
     *
     * @param tenderid tenderid
     * @return count 0 or more
     */
    public long tenderArchiveCnt(String tenderid) {
        long tenderArchiveCnt = 0;
        LOGGER.debug("tenderArchiveCnt : " + logUserId + " : Starts");
        try {
            tenderArchiveCnt = hibernateQueryDao.countForNewQuery("TblTenderDetails ttd", "ttd.submissionDt>current_timestamp() and ttd.tblTenderMaster.tenderId=" + tenderid);
        } catch (Exception e) {
            LOGGER.error("returndata : " + e);
        }
        LOGGER.debug("tenderArchiveCnt : " + logUserId + " : Ends");
        return tenderArchiveCnt;
    }

    /**
     * Particular tender is published or not
     *
     * @param tenderid tenderid
     * @return count 0 or more
     * @throws Exception
     */
    public long tenderPublishCnt(String tenderid) throws Exception {
        long tenderArchiveCnt = 0;
        LOGGER.debug("tenderPublishCnt : " + logUserId + " : Starts");
        try {
            tenderArchiveCnt = hibernateQueryDao.countForNewQuery("TblTenderDetails ttd", "ttd.tenderStatus='Approved' and ttd.tblTenderMaster.tenderId=" + tenderid);
        } catch (Exception e) {
            LOGGER.error("tenderPublishCnt : " + e);
        }
        LOGGER.debug("tenderPublishCnt : " + logUserId + " : Ends");
        return tenderArchiveCnt;
    }

    //to check a tender is in a Under Preparation Stage
    public long tenderUnderPrepCnt(String tenderid) throws Exception {
        long tenderUnderPrepCnt = 0;
        LOGGER.debug("tenderUnderPrepCnt : " + logUserId + " : Starts");
        try {
            tenderUnderPrepCnt = hibernateQueryDao.countForNewQuery("TblTenderDetails ttd", "((ttd.tenderStatus = 'Approved' And ttd.tenderPubDt > getdate()) Or ttd.tenderStatus = 'Pending') and ttd.tblTenderMaster.tenderId=" + tenderid);
        } catch (Exception e) {
            LOGGER.error("tenderUnderPrepCnt : " + e);
        }
        LOGGER.debug("tenderUnderPrepCnt : " + logUserId + " : Ends");
        return tenderUnderPrepCnt;
    }
    
    /**
     * Get PE office name from tbl_TenderDetails
     *
     * @param tenderid from tbl_TenderDetails
     * @return PE office name from tbl_TenderDetails
     * @throws Exception
     */
    public String getPEOfficeName(String tenderid) throws Exception {
        String Peoffice = "";
        LOGGER.debug("getPEOfficeName : " + logUserId + " : Starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select td.peOfficeName from TblTenderDetails td where tblTenderMaster.tenderId='" + tenderid + "'");
            if (list != null || !list.isEmpty()) {
                Peoffice = (String) list.get(0);
            }
        } catch (Exception e) {
            LOGGER.error("getPEOfficeName : " + e);
        }
        LOGGER.debug("getPEOfficeName : " + logUserId + " : Ends");
        return Peoffice;
    }

    /**
     * Fetch Email id
     *
     * @param userId of tbl_LoginMaster
     * @return Email id
     * @throws Exception
     */
    public String getEmailID(String userId) throws Exception {
        String emailId = "";
        LOGGER.debug("getEmailID : " + logUserId + " : Starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tl.emailId from TblLoginMaster tl where tl.userId='" + userId + "'");
            if (!list.isEmpty() || list != null) {
                emailId = (String) list.get(0);
            }
        } catch (Exception e) {
            LOGGER.error("getEmailID : " + e);
        }
        LOGGER.debug("getEmailID : " + logUserId + " : Ends");
        return emailId;
    }

    /**
     * Particular tender is limited[LTM(Limited Tender Method)] or not
     *
     * @param tenderid tenderid
     * @return count 0 or more
     */
    public long chkTenderLimited(String tenderid) {
        long tenderArchiveCnt = 0;
        LOGGER.debug("tenderPublishCnt : " + logUserId + " : Starts");
        List<SPTenderCommonData> list = new ArrayList<SPTenderCommonData>();

        try {
            list = sPTenderCommon.executeProcedure("getIsTenderLimitedStatus", tenderid, null);
            if (!list.isEmpty()) {
                if ("Limited".equalsIgnoreCase(list.get(0).getFieldName1())) {
                    tenderArchiveCnt = 1;
                } else {
                    tenderArchiveCnt = 0;
                }
            }
        } catch (Exception e) {
            LOGGER.error("tenderPublishCnt : " + e);
        }
        LOGGER.debug("tenderPublishCnt : " + logUserId + " : Ends");
        return tenderArchiveCnt;

    }

    /**
     * Whether the Report is saved for particular Lot of Tender
     *
     * @param tenderid from tbl_TenderMaster
     * @param pkgId from tbl_TenderLotSecurity
     * @return 0 or more
     */
    public long checkBidder(String tenderid, String pkgId) {
        long tenderArchiveCnt = 0;
        LOGGER.debug("checkBidder : " + logUserId + " : Starts");
        try {
            tenderArchiveCnt = hibernateQueryDao.countForNewQuery("TblBidderRank tbr", "tbr.pkgLotId=" + Integer.parseInt(pkgId) + " and tbr.tblTenderMaster.tenderId=" + tenderid);
        } catch (Exception e) {
            LOGGER.error("checkBidder : " + e);
        }
        LOGGER.debug("checkBidder : " + logUserId + " : Ends");
        return tenderArchiveCnt;

    }

    /**
     * Particular tender is limited[LTM(Limited Tender Method)] or not
     *
     * @param tenderid tenderid
     * @return count 0 or more
     */
    public long getauthTendCnt(String tenderid) {
        long tenderArchiveCnt = 0;
        LOGGER.debug("chkLimitedTender : " + logUserId + " : Starts");
        try {
            tenderArchiveCnt = hibernateQueryDao.countForNewQuery("TblLimitedTenders tlt", "tlt.tenderId =" + tenderid);
        } catch (Exception e) {
            LOGGER.error("chkLimitedTender : " + e);
        }
        LOGGER.debug("chkLimitedTender : " + logUserId + " : Ends");
        return tenderArchiveCnt;

    }

    /**
     * Check App Pending,Approved or Canceled
     *
     * @param appId to be searched on
     * @return List of {packageId,tenderId,tenderStatus}
     */
    public List<Object[]> getPackagesPendingorApprove(int appId) {
        LOGGER.debug("getPackagesPendingorApprove : " + logUserId + " : Starts");
        List<Object[]> list = null;
        String sqlQuery = "SELECT  t.tblAppPackages.packageId,t.tenderId,td.tenderStatus FROM  TblTenderMaster t,TblTenderDetails td "
                + "where t.tenderId=td.tblTenderMaster.tenderId "
                + "and td.tenderStatus in('Pending','Approved','Cancelled') and t.appId=" + appId;
        try {
            list = hibernateQueryDao.createQuery(sqlQuery);
        } catch (Exception ex) {
            LOGGER.debug("getPackagesPendingorApprove : " + logUserId + " " + ex);
        }
        LOGGER.debug("getPackagesPendingorApprove : " + logUserId + " : Ends");
        return list;
    }

    /**
     * Get all procurement Roles
     *
     * @return List of procurementRole
     */
    public List<Object> getProcurementRoles() {
        LOGGER.debug("getProcurementRoles : " + logUserId + " : Starts");
        List<Object> list = null;
        String sqlQuery = "select pr.procurementRole from TblProcurementRole pr";
        try {
            list = hibernateQueryDao.getSingleColQuery(sqlQuery);
        } catch (Exception ex) {
            LOGGER.debug("getProcurementRoles : " + logUserId + " " + ex);
        }
        LOGGER.debug("getProcurementRoles : " + logUserId + " : Ends");
        return list;
    }
    
    public List<Object> getTenderCreator(int tenderId) {
        LOGGER.debug("getTenderCreator : " + logUserId + " : Starts");
        List<Object> list = null;
        String sqlQuery = "select tm.createdBy from TblTenderMaster tm where tm.tenderId ="+tenderId;
        try {
            list = hibernateQueryDao.getSingleColQuery(sqlQuery);
        } catch (Exception ex) {
            LOGGER.debug("getProcurementRoles : " + logUserId + " " + ex);
        }
        LOGGER.debug("getTenderCreator : " + logUserId + " : Ends");
        return list;
    }

    /**
     * Check whether Tender has been Awarded
     *
     * @param tenderId from tbl_TenderMaster
     * @return true or false for Awarded or Not Awarded
     */
    public boolean istenderAwarded(int tenderId) {
        LOGGER.debug("istenderAwarded : " + logUserId + " : Starts");
        String sqlQuery = "select na.tblTenderMaster.tenderId from TblContractSign t,TblNoaIssueDetails na where t.noaId = na.noaIssueId  and na.tblTenderMaster.tenderId=" + tenderId;
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery(sqlQuery);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error("istenderAwarded : " + logUserId + " : " + ex);
        }
        LOGGER.debug("istenderAwarded : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * Check whether Tender Validity has been extended
     *
     * @param tenderId from tbl_TenderMaster
     * @return true or false for extended or Not extended
     */
    public boolean checkForFirstextends(int tenderId) {
        LOGGER.debug("checkForFirstextends : " + logUserId + " : Starts");
        String sqlQuery = "select tt.tblTenderMaster.tenderId from TblTenderValidityExtDate tt where tt.tblTenderMaster.tenderId='" + tenderId + "'";
        boolean flag = false;
        List<Object[]> list = new ArrayList<Object[]>();

        try {
            list = hibernateQueryDao.createQuery(sqlQuery);
            if (!list.isEmpty()) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("checkForFirstextends : " + logUserId + " : " + ex);
        }
        LOGGER.debug("checkForFirstextends : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * Get List of DocAccess for the Tender
     *
     * @param tenderId from tbl_TenderMaster
     * @return List of SPTenderCommonData
     */
    public List<SPTenderCommonData> getdocAccess(int tenderId) {
        LOGGER.debug("getdocAccess : " + logUserId + " : Starts");
        List<SPTenderCommonData> details = null;
        try {
            details = sPTenderCommon.executeProcedure("getIsTenderLimitedStatus", String.valueOf(tenderId), "");
        } catch (Exception e) {
            LOGGER.error("getdocAccess : " + logUserId + e);
        }
        LOGGER.debug("getdocAccess : " + logUserId + " : Ends");
        return details;
    }

    /**
     * Check whether the bidder is mapped to the Limited Tender
     *
     * @param tenderId from tbl_TenderMaster
     * @param userId bidder userId
     * @return true or false for yes or no
     */
    public boolean CheckForExist(int tenderId, int userId) {
        LOGGER.debug("CheckForExist : " + logUserId + " : Starts");
        String sqlQuery = "SELECT t.tenderId FROM TblLimitedTenders t where t.tenderId='" + tenderId + "' and t.userId='" + userId + "'";
        boolean flag = false;
        List<Object[]> list = new ArrayList<Object[]>();

        try {
            list = hibernateQueryDao.createQuery(sqlQuery);
            if (list != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("CheckForExist : " + logUserId + " : " + ex);
        }
        LOGGER.debug("CheckForExist : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * check whether Tender Opening Date has lapsed or not
     *
     * @param tenderId Tender to be checked
     * @return true or false for Not lapsed or lapsed
     */
    public boolean checkOpeningDate(int tenderId) {
        LOGGER.debug("checkOpeningDate : " + logUserId + " : Starts");
        String sqlQuery = "select tt.openingDt from TblTenderOpenDates tt where tt.tenderId='" + tenderId + "'";
        boolean flag = false;
        List<Object> list = null;

        try {
            Date d1 = new Date();
            int Result;
            list = hibernateQueryDao.singleColQuery(sqlQuery);
            if (list != null) {

                Result = d1.compareTo((Date) list.get(0));
                if (Result > 0) {
                    flag = true;
                }
            }

        } catch (Exception ex) {
            LOGGER.error("checkOpeningDate : " + logUserId + " : " + ex);
        }
        LOGGER.debug("checkOpeningDate : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * Check whether Bidder has done Final Submission for the Tender
     *
     * @param tenderId from tbl_tenderMaster
     * @param userId bidder doing final submission
     * @return true or false for finalsubmission done or finalsubmission not
     * done
     */
    public boolean isFinalSubDone(String tenderId, String userId) {

        LOGGER.debug("isFinalSubDone : " + logUserId + " : Starts");
        boolean flag = false;
        List<Object> list = new ArrayList<Object>();
        try {
            int Result;
            list = hibernateQueryDao.singleColQuery("Select tf.bidSubStatus from TblFinalSubmission tf where tf.tblTenderMaster.tenderId=" + tenderId + " and tf.userId=" + userId);

            if ("finalsubmission".equalsIgnoreCase(list.get(0).toString())) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("isFinalSubDone : " + logUserId + " : " + ex);
        }
        LOGGER.debug("isFinalSubDone : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * Check for CMS for Tender and LotID
     *
     * @param tenderId from tbl_TenderMaster
     * @param lotId from tbl_TenderLotSecurity
     * @return true or false
     */
    public boolean CMSLinkAllow(String tenderId, String lotId) {

        LOGGER.debug("CMSLinkAllow : " + logUserId + " : Starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select tn.pkgLotId from TblNoaIssueDetails tn where tn.tblTenderMaster.tenderId=" + tenderId + " and tn.pkgLotId=" + lotId);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("CMSLinkAllow : " + logUserId + " : " + ex);
        }
        LOGGER.debug("CMSLinkAllow : " + logUserId + " : Ends");
        return flag;
    }

    /**
     * Get CMS Tab who has Noa issued.
     *
     * @param int userId
     * @param int tenderId
     * @return true if user has noa issued or false if not.
     */
    public boolean getCMSTab(int userId, int tenderId) {
        LOGGER.debug("getCMSTab : " + logUserId + " : Starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select ni.noaIssueId from TblNoaIssueDetails ni, TblNoaAcceptance na "
                    + "where ni.noaIssueId = na.tblNoaIssueDetails.noaIssueId "
                    + "and na.acceptRejStatus = 'Approved' "
                    + "and ni.tblTenderMaster.tenderId =" + tenderId + " and ni.userId = " + userId);

            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("getCMSTab : " + logUserId + " : " + ex);
        }
        LOGGER.debug("getCMSTab : " + logUserId + " : Ends");
        return flag;

    }

    /**
     * Get Boq Type of Tender Form.
     *
     * @param formId
     * @return
     */
    public int getBoqTypeId(int formId) {
        LOGGER.debug("getBoqTypeId : " + logUserId + " : Starts");
        int boqTypeId = 0;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select a.tblCmsSrvBoqMaster.srvBoqId from TblCmsTemplateSrvBoqDetail a, TblTenderForms  b where a.tblTemplateSectionForm.formId = b.templateFormId and b.tenderFormId = " + formId);
            if (list != null && !list.isEmpty()) {
                boqTypeId = (Integer) list.get(0);
            }
        } catch (Exception ex) {
            LOGGER.error("getBoqTypeId : " + logUserId + " : " + ex);
        }
        LOGGER.debug("getBoqTypeId : " + logUserId + " : Ends");
        return boqTypeId;

    }

    /**
     * Check whether Tender has been Awarded
     *
     * @param tenderId from tbl_TenderMaster
     * @return true or false for Awarded or Not Awarded
     */
    public SPTenderCommonData getClosedTenderInfo(String tenderId) {
        LOGGER.debug("getClosedTenderInfo : " + logUserId + " : Starts");
        SPTenderCommonData tenderCommonData = new SPTenderCommonData();
        List<SPTenderCommonData> details = null;
        try {
            details = sPTenderCommon.executeProcedure("getClosedTenderInfo", tenderId, "");
            if (!details.isEmpty()) {
                tenderCommonData = details.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("getClosedTenderInfo : " + logUserId + " : " + e);
        }
        LOGGER.debug("getClosedTenderInfo : " + logUserId + " : Ends");
        return tenderCommonData;
    }

    public List<Object[]> getAllTendererInfo(String searchString) {
        LOGGER.debug("getAllTendererInfo : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select cm.companyName,cm.companyRegNumber,lm.emailId,cm.regOffCountry,cm.regOffState,lm.registrationType,cm.legalStatus from TblTendererMaster tm , TblCompanyMaster cm , TblLoginMaster lm where tm.tblCompanyMaster.companyId = cm.companyId and tm.tblLoginMaster.userId = lm.userId" + searchString + "");
        } catch (Exception e) {
            LOGGER.debug("getAllTendererInfo : " + e);
        }
        LOGGER.debug("getAllTendererInfo : " + logUserId + " Ends");
        return list;
    }

    public boolean removeTempAppData(String appId) throws Exception {

        LOGGER.debug("removeTempAppData : " + logUserId + " : Starts");
        SPTenderCommonData tenderCommonData = new SPTenderCommonData();
        List<SPTenderCommonData> details = null;

        try {
            details = sPTenderCommon.executeProcedure("delTempAppPackages", appId, "");
            if (!details.isEmpty()) {
                tenderCommonData = details.get(0);
            }
            if ("true".equalsIgnoreCase(tenderCommonData.getFieldName1())) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            LOGGER.error("TenderCommonService removeTempAppData: error deleting temp app packages data:" + e.getMessage());
            return false;
        }

    }

    public boolean cancelPackageRevision(String packageId) {

        LOGGER.debug("cancelPackageRevision : " + logUserId + " : Starts");
        SPTenderCommonData tenderCommonData = new SPTenderCommonData();
        List<SPTenderCommonData> details = null;

        try {
            details = sPTenderCommon.executeProcedure("cancelAPPRevision", packageId, "");
            if (!details.isEmpty()) {
                tenderCommonData = details.get(0);
            }
            if ("true".equalsIgnoreCase(tenderCommonData.getFieldName1())) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            LOGGER.error("TenderCommonService cancelPackageRevision: error cancelling app package revision packageId:" + packageId + ":" + e.getMessage());
            return false;
        }

    }

    public boolean createTempAppPackages(String packageId) throws Exception {

        LOGGER.debug("createTempAppPackages : " + logUserId + " : Starts");
        SPTenderCommonData tenderCommonData = new SPTenderCommonData();
        List<SPTenderCommonData> details = null;

        try {
            details = sPTenderCommon.executeProcedure("createTempAppPackages", packageId, "");
            if (!details.isEmpty()) {
                tenderCommonData = details.get(0);
            }
            if ("true".equalsIgnoreCase(tenderCommonData.getFieldName1())) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            LOGGER.error("TenderCommonService createTempAppPackages: error  packageId:" + packageId + ":" + e.getMessage());
            return false;
        }

    }
    
    public int getTenderCommitteeChairperson(int tenderId) throws Exception{
        int tcChairPerson = 0;
        LOGGER.debug("getTenderCommitteeChairperson : " + logUserId + " : Starts");
        
        List list = null;
        try {
            list = hibernateQueryDao.nativeSQLQuery("select tcm.userId from tbl_committee tc inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId where tcm.memberRole = 'cp' and tc.committeeType = 'tc' and tenderId = " + tenderId + "", null);
            if (list != null && !list.isEmpty()) {
                tcChairPerson = (Integer) list.get(0);
            }
        } catch (Exception e) {
            LOGGER.debug("getTenderCommitteeChairperson : " + e);
        }
        
        LOGGER.debug("getTenderCommitteeChairperson : " + logUserId + " : Ends");
        return tcChairPerson;
    }
    
    public String getTenderPostQueConfig(int tenderId) throws Exception{
        String queryId = "0";
        LOGGER.debug("getTenderPostQueConfig : " + logUserId + " : Starts");
        
        List list = null;
        try {
            list = hibernateQueryDao.nativeSQLQuery("SELECT postQueConfigId FROM tbl_TenderPostQueConfig WHERE tenderId = " + tenderId + "", null);
            if (list != null && !list.isEmpty()) {
                queryId = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.debug("getTenderPostQueConfig : " + e);
        }
        
        LOGGER.debug("getTenderPostQueConfig : " + logUserId + " : Ends");
        return queryId;
    }
    
    public String getTenderSubmissionDate(int tenderId) throws Exception{
        String date = "0";
        LOGGER.debug("getTenderSubmissionDate : " + logUserId + " : Starts");
        
        List list = null;
        try {
            list = hibernateQueryDao.nativeSQLQuery("SELECT submissionDt FROM tbl_TenderDetails WHERE tenderId = " + tenderId + "", null);
            if (list != null && !list.isEmpty()) {
                date = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.debug("getTenderSubmissionDate : " + e);
        }
        
        LOGGER.debug("getTenderSubmissionDate : " + logUserId + " : Ends");
        return date;
    }
    
    
    public boolean IsLOIAccessible(int tenderId, int userId) throws Exception{
        
        LOGGER.debug("IsLOIAccessible : " + logUserId + " : Starts");
        
        List list = null;
        try {
            
            list = hibernateQueryDao.nativeSQLQuery("SELECT userId from tbl_LoginMaster where userId in ( select userid from tbl_BidConfirmation where tenderId="+tenderId+" and confirmationStatus='accepted' union select userId from tbl_CommitteeMembers where committeeId = (select committeeId from tbl_Committee where tenderId="+tenderId+" and committeeType='TC'))", null);
            
        } catch (Exception e) {
            LOGGER.debug("IsLOIAccessible : " + e);
        }
        LOGGER.debug("IsLOIAccessible : " + logUserId + " : Ends");
        
        return (list != null && !list.isEmpty())?list.contains(userId):false;
        
 
    }
    
   public boolean LOIVisibilityStatus(int tenderId) throws Exception{
   
       
       LOGGER.debug("LOIVisibilityStatus : " + logUserId + " : Starts");
        
        List list = null;
        try {
            
            list = hibernateQueryDao.nativeSQLQuery("select * from tbl_EvalRptSentTC where tenderId = "+tenderId+" and rptStatus = 'sentforloi'", null);
            
        } catch (Exception e) {
            LOGGER.debug("LOIVisibilityStatus : " + e);
        }
        LOGGER.debug("LOIVisibilityStatus : " + logUserId + " : Ends");
        
        return list!= null && !list.isEmpty();
        
   }
    
    public int GetEvalCount(int tenderId) throws Exception{
        
        LOGGER.debug("GetEvalCount : " + logUserId + " : Starts");
        
        List list = null;
        try {
            
            list = hibernateQueryDao.nativeSQLQuery("select max(evalCount) from tbl_EvalRptSentToAA where tenderId = "+tenderId, null);
            
        } catch (Exception e) {
            LOGGER.debug("GetEvalCount : " + e);
        }
        LOGGER.debug("GetEvalCount : " + logUserId + " : Ends");
        
       
        
        if(list.get(0) == null )
        {
            return 0;
        }else{
            return Integer.parseInt(list.get(0).toString());
        }
        
    }
    
    public String GetLOIIssueDate(String tenderId,String lotId,String roundId) throws Exception{
        
        LOGGER.debug("GetEvalCount : " + logUserId + " : Starts");
        
        List list = null;
        try {
            
            list = hibernateQueryDao.nativeSQLQuery("select Convert(varchar(30),sentDate,103) as issueDate from tbl_EvalRptSentTC where tenderId = "+tenderId+" and pkgLotId = "+lotId+" and roundId="+roundId+" and rptStatus = 'sentforloi'", null);
        } catch (Exception e) {
            LOGGER.debug("GetEvalCount : " + e);
        }
        LOGGER.debug("GetEvalCount : " + logUserId + " : Ends");
        
       
        
        return list!= null && !list.isEmpty()?list.get(0).toString():"";
        
    }
    public String getSBDCurrency(int tenderId) {
         LOGGER.debug("getSBDCurrency : " + logUserId + " : Starts");
        String sbdCurrency = "No";
        List<?> obj = null;
        
        try {
            
            String query = "select foreignCurrency from tbl_TemplateMaster where templateId = (select templateId from tbl_TenderStd where tenderId = '"+tenderId+"')";
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (!obj.isEmpty()) {
                for(Object x:obj){
                     sbdCurrency=x.toString();
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getSBDCurrency : " + logUserId + " :" + ex);
        }
       
        LOGGER.debug("getSBDCurrency : " + logUserId + " : Ends");
        return sbdCurrency;
    }
    
    public boolean IsEvalRptSentForLOI(String tenderId, String pckgLotId, String roundId, String evalCount ) {
        
        LOGGER.debug("IsEvalRptSentForLOI : " + logUserId + " : Starts");
        List list = null;
        try {
            
            list = hibernateQueryDao.nativeSQLQuery("select * from tbl_EvalRptSentTC where pkgLotId = "+pckgLotId+" and roundId = "+roundId+" and tenderId = "+tenderId+" and rptStatus = 'sentforloi' and evalcount = "+evalCount, null);
            
        } catch (Exception e) {
            LOGGER.debug("IsEvalRptSentForLOI : " + e);
        }
        LOGGER.debug("IsEvalRptSentForLOI : " + logUserId + " : Ends");
        
        return list!= null && !list.isEmpty();
    }
    
}
