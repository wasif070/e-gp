/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblJvcapartners;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface JvcapartnersService {

    /**
     * Updates JVCA partner details
     * @param jvId
     * @param status
     * @param userId
     * @return
     */
    public int updateJvcaPartner(int jvId,String status,int userId);
    /**
     *Get The List of Partner in Grid
     * @param userId 
     * @param sidx 
     * @param sord 
     * @return List of object in object[0] jvName, [1] JvId, [2] jvAcceptRejStatus,[3] creationDate,[4] jvAccpetDatetime,[5] jvstatus,[6] isNominatedPartner,[7] newJvuserId
     */
    public List<Object []> getPartnerList(int userId,String sidx,String sord);
    /**
     * Get The List of Partner in Grid
     * @param userId : Session UserId
     * @param searchField 
     * @param searchString 
     * @param searchOper 
     * @return List of Object in object[0] jvName, [1] JvId, [2] jvAcceptRejStatus,[3] creationDate,[4] jvAccpetDatetime,[5] jvstatus,[6] isNominatedPartner,[7] newJvuserId
     */
    public List<Object []> getPartnerList(int userId,String searchField,String searchOper,String searchString);
    /**
     * fetch JVCA Name from JVC Id.
     * @param jVid 
     * @return Object
     */
    public Object getJvcaNamefromId(int jVid);
    /**
     * fetch JVCA Partner Details List by JV Id.
     * @param jVid 
     * @return List of object[] [0]companyName,[1]isNominatedPartner, [2]jvAcceptRejStatus, [3]emailId, [4]companyId, [5]userId, [6]jvpartnerId
     */
    public List<Object []> getJvcaPartnerDetailList(int jVid);
    /**
     * check if JVCA is accepted or rejected.
     * @param jvId
     * @return List of Object, jvAcceptRejStatus
     */
    public List<Object> checkAcceptRejectStatus(int jvId);
    /**
     * fetch JVCAPartner Name and Email Address by jvId and userId
     * @param jvId
     * @param userId
     * @return List of object[] [0]Name, [1]emailId
     */
    public List<Object []> getJvcaPartnerNameAndEmail(int jvId,int userId);
    /**
     * For Condition in View Link of Grid PartnerRequest
     * @param jvId
     * @return  List<Object> Email
     */
    public List<Object> getJvcaLeadPartnerEmail(int jvId);
    /**
     * get Next Screen by userId
     * @param userId Session userId
     * @return Object [0] NextScreen , [1] status
     */
    public Object[] getNextScreen(String userId);
    /**
     * Delete JVCA Partner by jvId.
     * @param jvId
     * @return int
     */
    public int deleteJVCAPartner(int jvId);
    /**
     * Update _Partner Details by jvId and list JVCA Partners
     * @param jvId
     * @param listJvcaPartner
     * @return int
     */
    public int updatePartner(int jvId,List<TblJvcapartners> listJvcaPartner);

    /**
     * Get Details of JVCAPartner Detail List
     * @param newUserId
     * @return List<Object[]> [0]companyName, [1]jvRole, [2]isNominatedPartner, [3]jvAcceptRejStatus, [4]emailId, [5]companyId, [6]userId, [7]tendererId
     */
    public List<Object[]> getJvcaPartnerDetailList(String newUserId);
    
    /**
     * Logger purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    public void setAuditTrail(AuditTrail auditTrail);
}
