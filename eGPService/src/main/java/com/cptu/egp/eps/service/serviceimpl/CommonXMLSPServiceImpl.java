/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUpdOpeningEvaluation;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;
import org.omg.CORBA.portable.ApplicationException;

/**
 *
 * @author Administrator
 */
public class CommonXMLSPServiceImpl implements CommonXMLSPService{
    private static final Logger LOGGER = Logger.getLogger(CommonXMLSPServiceImpl.class);
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;
    private SPXMLCommon spXMLCommon;
    private SPAddUpdOpeningEvaluation spAddUpdOpeningEvaluation;
    private String logUserId="0";
    String moduleName=EgpModule.APP.getName();
    public SPXMLCommon getSpXMLCommon() {
        return spXMLCommon;
    }

    public void setSpXMLCommon(SPXMLCommon spXMLCommon) {
        this.spXMLCommon = spXMLCommon;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    @Override
    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    
    
    @Override
    public List<CommonMsgChk> insertDataBySP(String action, String tablename, String xmlData, String conditions, String...values) throws Exception {
        LOGGER.debug("insertDataBySP : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        
        int id = 0;
        String idType = null;
        String auditAction = "";
        String remarks = "";
       
       // moduleName=EgpModule.Evaluation.getName();
        
        if(values!=null && values.length!=0){
            idType = values[0];
            id = Integer.parseInt(values[1]);
            
            auditAction = values[2];
            remarks = values[3];
        }
       // System.out.println("iD="+id);
        try {
            list= spXMLCommon.executeProcedure(action, tablename, xmlData, conditions);
            //list = spAddUpdOpeningEvaluation.executeProcedure(tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename, tablename)
        } catch (Exception e) { 
            LOGGER.error("insertDataBySP : "+logUserId+" "+e);
            auditAction = "Error in ".concat(auditAction);
            e.printStackTrace();
        }finally{
            if(id!=0){
                //System.out.println("----------- Before call generate Audit ----------------------");
                makeAuditTrailService.generateAudit(auditTrail,id, idType,moduleName, auditAction, remarks);
               // System.out.println("----------- After call generate Audit ----------------------");
            }
            auditAction = null;
        }
        LOGGER.debug("insertDataBySP : "+logUserId+" Ends");
        return list;
    }
    
    //insertNOAAcceptance
    @Override
    public List<CommonMsgChk> insertNOAAcceptance(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15, String fieldName16, String fieldName17, String fieldName18, String fieldName19, String fieldName20) throws Exception {
        LOGGER.debug("insertDataBySP : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        
        int id = 0;
        String idType = null;
        String auditAction = "";
        String remarks = "";
       
       // moduleName=EgpModule.Evaluation.getName();
        
//        if(values!=null && values.length!=0){
//            idType = values[0];
//            id = Integer.parseInt(values[1]);
//            
//            auditAction = values[2];
//            remarks = values[3];
//        }
       // System.out.println("iD="+id);
        try {
            //list= spXMLCommon.executeProcedure(action, tablename, xmlData, conditions);
            list = spAddUpdOpeningEvaluation.executeProcedure( fieldName1,  fieldName2,  fieldName3,  fieldName4,  fieldName5,  fieldName6,  fieldName7,  fieldName8,  fieldName9,  fieldName10,  fieldName11,  fieldName12,  fieldName13,  fieldName14,  fieldName15,  fieldName16, fieldName17, fieldName18, fieldName19, fieldName20, null, null, null, null, null, null, null, null, null, null);
        } catch (Exception e) { 
            LOGGER.error("insertDataBySP : "+logUserId+" "+e);
            auditAction = "Error in ".concat(auditAction);
            e.printStackTrace();
        }finally{
            if(id!=0){
                //System.out.println("----------- Before call generate Audit ----------------------");
                makeAuditTrailService.generateAudit(auditTrail,id, idType,moduleName, auditAction, remarks);
               // System.out.println("----------- After call generate Audit ----------------------");
            }
            auditAction = null;
        }
        LOGGER.debug("insertDataBySP : "+logUserId+" Ends");
        return list;
    }

    /**
     * @return the spAddUpdOpeningEvaluation
     */
    public SPAddUpdOpeningEvaluation getSpAddUpdOpeningEvaluation() {
        return spAddUpdOpeningEvaluation;
    }

    /**
     * @param spAddUpdOpeningEvaluation the spAddUpdOpeningEvaluation to set
     */
    public void setSpAddUpdOpeningEvaluation(SPAddUpdOpeningEvaluation spAddUpdOpeningEvaluation) {
        this.spAddUpdOpeningEvaluation = spAddUpdOpeningEvaluation;
    }
}
