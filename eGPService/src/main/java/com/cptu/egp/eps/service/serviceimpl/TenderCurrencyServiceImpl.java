/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderCurrencyDao;
import com.cptu.egp.eps.dao.storedprocedure.SPGetUpdateTenderInfoForICTGoods;
import com.cptu.egp.eps.dao.storedprocedure.SPGetTenderCellValueForIct;
import com.cptu.egp.eps.dao.storedprocedure.SPUpdateTenderBidPlainData;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderCurrency;
import com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ketan Prajapati
 */
public class TenderCurrencyServiceImpl implements TenderCurrencyService {

    static final Logger logger = Logger.getLogger(TenderCurrencyServiceImpl.class);
    TblTenderCurrencyDao tblTenderCurrencyDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private SPGetUpdateTenderInfoForICTGoods spGetUpdateTenderInfoForICTGoods;
    private SPGetTenderCellValueForIct spGetTenderCellValueForIct;
    private SPUpdateTenderBidPlainData spUpdateTenderBidPlainData;

    public TblTenderCurrencyDao getTblTenderCurrencyDao() {
        return tblTenderCurrencyDao;
    }

    public void setTblTenderCurrencyDao(TblTenderCurrencyDao tblTenderCurrencyDao) {
        this.tblTenderCurrencyDao = tblTenderCurrencyDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public boolean addTblTenderCurrency(TblTenderCurrency TblTenderCurrency) {
        logger.debug("addTblTenderCurrency : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            tblTenderCurrencyDao.addTblTenderCurrency(TblTenderCurrency);
            bSuccess = true;
        } catch (Exception e) {
            logger.error("addTblTenderCurrency : " + logUserId + " " + e);
            bSuccess = false;
        }
        logger.debug("addTblTenderCurrency : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public boolean removeTenderCurreny(String strTenderId, String strDefaultCurrencyId) {
        logger.debug("removeTenderCurreny : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderCurrency where tenderId=" + Integer.parseInt(strTenderId) + "and currencyId <> " + Integer.parseInt(strDefaultCurrencyId));
            bSuccess = true;
        } catch (Exception e) {
            logger.error("removeTenderCurreny : " + logUserId + " " + e);
            bSuccess = false;
        }
        logger.debug("removeTenderCurreny : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public boolean removeSpecificTenderCurreny(String strTenderId,String strCurrencyId) {
        logger.debug("removeSpecificTenderCurreny : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderCurrency where tenderId=" + Integer.parseInt(strTenderId) + " and currencyId=" + Integer.parseInt(strCurrencyId));
            bSuccess = true;
        } catch (Exception e) {
            logger.error("removeSpecificTenderCurreny : " + logUserId + " " + e);
            bSuccess = false;
        }
        logger.debug("removeSpecificTenderCurreny : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public List<TblTenderCurrency> getConfiguredCurrency(String strTenderId) throws Exception {
        logger.debug("getConfiguredCurrency : " + logUserId + " Starts");
        logger.debug("getConfiguredCurrency : " + logUserId + " Ends");
        return tblTenderCurrencyDao.findTblTenderCurrency("tenderId", Operation_enum.EQ, Integer.parseInt(strTenderId));
    }

    @Override
    public List<Object[]> getCurrencyTenderwise(int tenderId, int userId) {

        List<Object[]> listCurrency = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select tc.currencyName, ttc.exchangeRate "
                               + "from TblTenderCurrency ttc, TblCurrencyMaster tc "
                             + "where ttc.currencyId = tc.currencyId and ttc.tenderId = "+tenderId+" and createdBy = "+userId
                             + "order by tc.currencyId asc";

            listCurrency = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
              logger.debug("getCurrencyTenderwise : " +ex);
        }

        return listCurrency;
    }

    @Override
    public List<Object[]> getCurrencyTenderwise(int tenderId) {

        List<Object[]> listCurrency = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select tc.currencyName, ttc.exchangeRate, tc.currencyId, tc.currencyShortName "
                               + "from TblTenderCurrency ttc, TblCurrencyMaster tc "
                             + "where ttc.currencyId = tc.currencyId and ttc.tenderId = "+tenderId
                             + " order by tc.currencyShortName asc";

            listCurrency = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
              logger.debug("getCurrencyTenderwise : " +ex);
        }

        return listCurrency;
    }

    @Override
    public long getCurrencyCountForTender(int tenderId, int currencyId){

        long count = 0;
        String from = "TblTenderCurrency";
        String where ="tenderId=" + tenderId + " and currencyId=" + currencyId;

        try{
            count = hibernateQueryDao.countForQuery(from, where);
        }
        catch (Exception ex){
            logger.debug("getCurrencyCountForTender: " + ex);
        }

        return count;
    }

    @Override
    public long getCurrencyCountForTender(int tenderId){

        long count = 0;
        String from = "TblTenderCurrency";
        String where ="tenderId=" + tenderId;

        try{
            count = hibernateQueryDao.countForQuery(from, where);
        }
        catch (Exception ex){
            logger.debug("getCurrencyCountForTender: " + ex);
        }

        return count;
    }

    @Override
    public boolean isAtleastOneCurrencyExchangeRateAdded(int tenderId){
        boolean bExchangeRateAdded = false;

        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
        listCurrencyObj = getCurrencyTenderwise(tenderId);

        double currencySum = 0;

        if(listCurrencyObj.size() > 0){
            for(Object[] obj :listCurrencyObj){

                String str = obj[1].toString();
                currencySum += Double.valueOf(str).doubleValue();
            }
        }

        if(currencySum > 1.000){ //default currency - BTN's exchange rate is 1.000. So if other's rate added, sum must greater than 1.000
            bExchangeRateAdded = true;
        }

        return bExchangeRateAdded;
    }
    
    @Override
    public boolean isCurrencyExchangeRateAdded(int tenderId) {
        boolean bExchangeRateAdded = false;
        long totalCurrency = 0;
        long currencyRateAdded = 0;
        try{
            totalCurrency = hibernateQueryDao.countForQuery("TblTenderCurrency ttc, TblCurrencyMaster tc","ttc.currencyId = tc.currencyId and ttc.tenderId = "+tenderId);
            currencyRateAdded = hibernateQueryDao.countForQuery("TblTenderCurrency ttc, TblCurrencyMaster tc","ttc.currencyId = tc.currencyId and ttc.exchangeRate > 0.00 and ttc.tenderId = "+tenderId);
           
            if(totalCurrency == currencyRateAdded){
                bExchangeRateAdded = true;
            }
            
        }catch(Exception ex){
              logger.debug("isCurrencyExchangeRateAdded : " +ex);
        }
        
        return bExchangeRateAdded;
    }
    
    @Override
    public boolean updateCurrencyExchangeRate(TblTenderCurrency tblTenderCurrency){
        logger.debug("updateCurrencyExchangeRate : "+logUserId+" Starts");
        boolean bSuccess = false;

        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
            hibernateQueryDao.updateDeleteNewQuery("update TblTenderCurrency set exchangeRate=" + tblTenderCurrency.getExchangeRate() + " , createdBy=" + tblTenderCurrency.getCreatedBy() + " , createdDate='" + dateFormat.format(tblTenderCurrency.getCreatedDate())  +"' where tenderId=" + tblTenderCurrency.getTenderId() + " and currencyId=" + tblTenderCurrency.getCurrencyId());
            bSuccess =  true;
        }catch(Exception ex){
            logger.error("updateCurrencyExchangeRate : "+logUserId+" : "+ex.toString());
        }finally{
        }
        logger.debug("updateCurrencyExchangeRate : "+logUserId+" Ends");

        return bSuccess;
    }

    @Override
    public Date getLastDateOfCurrencyExchangeRates(int tenderId){

        List<Object[]> listCurrencyUpdateDate = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select max(ttc.createdDate) from TblTenderCurrency ttc where ttc.tenderId = "+tenderId;

            listCurrencyUpdateDate = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
              logger.debug("getLastDateOfCurrencyExchangeRates : " +ex);
        }

        String strCurrencyUpdateDate = listCurrencyUpdateDate.toString(); //output like [2012-10-11 13:42:57.467]
        strCurrencyUpdateDate = strCurrencyUpdateDate.replace("[", "").replace("]", ""); //output like 2012-10-11 13:42:57.467

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
        Date updateDate = new Date();
        try{
            updateDate = dateFormat.parse(strCurrencyUpdateDate);
        }
        catch(Exception ex) {
            logger.debug("getLastDateOfCurrencyExchangeRates : " +ex);
        }

        return updateDate;
    }

    @Override
    public boolean isTenderListBoxForCurrency(int tenderListId){
        logger.debug("isTenderListBoxForCurrency : "+logUserId+" Starts");

        boolean bResult = false;
        List<Object[]> listListBoxObj = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select tenderListId, listBoxName "
                               + " from TblTenderListBox"
                             + " where tenderListId = "+tenderListId;

            listListBoxObj = hibernateQueryDao.createQuery(sqlQuery);

            if(listListBoxObj.size() > 0){
            for(Object[] obj :listListBoxObj){
                if("Currency".equalsIgnoreCase(obj[1].toString())){
                    bResult = true;
                }
            }
        }

        }catch(Exception ex){
            logger.debug("isTenderListBoxForCurrency : " +ex);
        }

        return bResult;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<Object[]> getTenderFormId(int tenderId)
    {
     logger.debug("getTenderFormId : "+logUserId+" Starts");

        List<Object[]> listTenderForm = null;
        try{
           listTenderForm = getSpGetUpdateTenderInfoForICTGoods().executeProcedure("getTenderFormsId", tenderId);
        }catch(Exception ex){
            logger.debug("getTenderFormId : " +ex);
        }

        return listTenderForm;
    }

    @Override
    public List<Object[]> getTenderTableCellValuesFillesByPE(int tenderTableId)
    {
     logger.debug("getTenderTableCellValuesFillesByPE : "+logUserId+" Starts");

        List<Object[]> listTenderForm = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select cellvalue "
                               + " from TblTenderCells "
                             + " where tenderTableId = " + tenderTableId + " and cellvalue <> ''";
             System.out.println("sqlQuery >>>>>>>>>>" + sqlQuery);
            listTenderForm = hibernateQueryDao.createQuery(sqlQuery);
            System.out.println("listTenderForm >>>>>>>>>>" + listTenderForm);
        }catch(Exception ex){
            logger.debug("getTenderTableCellValuesFillesByPE : " +ex);
        }

        return listTenderForm;
    }

    @Override
    public Date getTenderOpeningDate(int tenderId){

        List<Object[]> listTenderOpeningDate = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select tod.openingDt from TblTenderOpenDates tod where tod.tenderId = "+tenderId;

            listTenderOpeningDate = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
              logger.debug("getTenderOpeingDate : " +ex);
        }

        String strTenderOpeningDate = listTenderOpeningDate.toString(); //output like [2012-10-11 13:42:57.467]
        strTenderOpeningDate = strTenderOpeningDate.replace("[", "").replace("]", ""); //output like 2012-10-11 13:42:57.467

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
        Date tenderOpeningDate = new Date();
        try{
            tenderOpeningDate = dateFormat.parse(strTenderOpeningDate);
        }
        catch(Exception ex) {
            logger.debug("getTenderOpeingDate : " +ex);
        }

        return tenderOpeningDate;
    }

    @Override
    public List<Object[]> getTenderTableCellValuesFillesByTenderer(int tenderFormId)
    {
     logger.debug("getTenderTableCellValuesFillesByTenderer : "+logUserId+" Starts");

        List<Object[]> listTenderForm = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select bidTableId,tenderTableId,cellValue  "
                               + " from TblTenderBidPlainData "
                             + " where tenderFormId = " + tenderFormId + " and cellValue <> 'null'";

            listTenderForm = hibernateQueryDao.createQuery(sqlQuery);
             System.out.println("listTenderForm >>>>>>>>>>" + listTenderForm);
        }catch(Exception ex){
            logger.debug("getTenderTableCellValuesFillesByTenderer : " +ex);
        }

        return listTenderForm;
    }

    @Override
    public List<Object[]> getFormula(int tenderFormId)
    {
    logger.debug("getTenderTableCellValuesFillesByTenderer : "+logUserId+" Starts");

        List<Object[]> listFormula = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select columnId,formula  "
                               + " from TblTenderFormula "
                             + " where isGrandTotal = 'no' and tenderFormId = " + tenderFormId + "";

            listFormula = hibernateQueryDao.createQuery(sqlQuery);
           
        }catch(Exception ex){
            logger.debug("getTenderTableCellValuesFillesByTenderer : " +ex);
        }

        return listFormula;
    }

    /**
     * @return the spGetUpdateTenderInfoForICTGoods
     */
    public SPGetUpdateTenderInfoForICTGoods getSpGetUpdateTenderInfoForICTGoods() {
        return spGetUpdateTenderInfoForICTGoods;
    }

    /**
     * @param spGetUpdateTenderInfoForICTGoods the spGetUpdateTenderInfoForICTGoods to set
     */
    public void setSpGetUpdateTenderInfoForICTGoods(SPGetUpdateTenderInfoForICTGoods spGetUpdateTenderInfoForICTGoods) {
        this.spGetUpdateTenderInfoForICTGoods = spGetUpdateTenderInfoForICTGoods;
    }

    /**
     * @return the spGetTenderCellValueForIct
     */
    public SPGetTenderCellValueForIct getSpGetTenderCellValueForIct() {
        return spGetTenderCellValueForIct;
    }

    /**
     * @param spGetTenderCellValueForIct the spGetTenderCellValueForIct to set
     */
    public void setSpGetTenderCellValueForIct(SPGetTenderCellValueForIct spGetTenderCellValueForIct) {
        this.spGetTenderCellValueForIct = spGetTenderCellValueForIct;
    }

    @Override
    public String getTenderCellValue(String colList,int tenderFormId, int bidTableId, int tenderTableid,int tenderId,int rowId)
    {
    logger.debug("getTenderCellValue : "+logUserId+" Starts");

        String cellValue = null;
        try{
           cellValue = spGetTenderCellValueForIct.executeProcedure(colList,tenderFormId, bidTableId, tenderTableid,tenderId,rowId);
        }catch(Exception ex){
            logger.debug("getTenderCellValue : " +ex);
        }

        return cellValue;
    }

    @Override
    public List<Object[]> getTenderBiddingInfo(int tenderFormId)
    {
     logger.debug("getTenderBiddingInfo : "+logUserId+" Starts");

        List<Object[]> listTenderBddingInfo = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select  bidTableId,tenderTableId  "
                               + " from TblTenderBidTable "
                             + " where tenderFormId  = " + tenderFormId + "";

            listTenderBddingInfo = hibernateQueryDao.createQuery(sqlQuery);
           
        }catch(Exception ex){
            logger.debug("getTenderBiddingInfo : " +ex);
        }

        return listTenderBddingInfo;
    }

    @Override
    public String updateTenderBidPlainData(String cellValueList,String columnIdList,String bidTableIdList,String rowIdList,int tenderFormId)
    {
        logger.debug("getTenderCellValue : "+logUserId+" Starts");

        String cellValue = null;
        try{
           cellValue = spUpdateTenderBidPlainData.executeProcedure(cellValueList, columnIdList, bidTableIdList,rowIdList,tenderFormId);
        }catch(Exception ex){
            logger.debug("getTenderCellValue : " +ex);
        }

        return cellValue;
    }



    /**
     * @return the spUpdateTenderBidPlainData
     */
    public SPUpdateTenderBidPlainData getSpUpdateTenderBidPlainData() {
        return spUpdateTenderBidPlainData;
    }

    /**
     * @param spUpdateTenderBidPlainData the spUpdateTenderBidPlainData to set
     */
    public void setSpUpdateTenderBidPlainData(SPUpdateTenderBidPlainData spUpdateTenderBidPlainData) {
        this.spUpdateTenderBidPlainData = spUpdateTenderBidPlainData;
    }

    @Override
    public List<Object[]> getRowId(int tenderFormId,int bidTableId,int tenderTableId)
    {
     logger.debug("getRowId : "+logUserId+" Starts");

        List<Object[]> listRowId = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select distinct rowId "
                               + " from TblTenderBidPlainData "
                             + " where tenderFormId  = " + tenderFormId + " and bidTableId = "+ bidTableId +" and tenderTableId = "+ tenderTableId +" and cellValue <> ''";

            listRowId = hibernateQueryDao.createQuery(sqlQuery);
             System.out.println("getRowId >>>>>>>>>>" + listRowId);
        }catch(Exception ex){
            logger.debug("getRowId : " +ex);
        }

        return listRowId;
    }
}
