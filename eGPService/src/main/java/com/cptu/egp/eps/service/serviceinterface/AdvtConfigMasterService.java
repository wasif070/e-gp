/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import java.util.List;

public interface AdvtConfigMasterService {

    public List<TblAdvtConfigurationMaster> getConfigMaster();

    public List<TblAdvtConfigurationMaster> getConfigMasterDetails(int configId);

    public String updateConfigMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster);

    public List<TblAdvtConfigurationMaster> getAllConfigMaster();

    public List<TblAdvtConfigurationMaster> getAllTblAdvtConfigurationMaster();

    public String insertConfigMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster);

    public void setLogUserId(String logUserId);

    public List<TblAdvtConfigurationMaster> findTblAdvtConfigurationMaster(Object... values) throws Exception;

    public boolean updateTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster);

    public boolean deleteTblAdvtConfigurationMaster(TblAdvtConfigurationMaster adConfigId);
}
