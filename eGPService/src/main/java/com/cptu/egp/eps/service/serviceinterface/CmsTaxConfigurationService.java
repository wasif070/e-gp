/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsTaxConfigurationService {

    public void setLogUserId(String logUserId);

    public int insertCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public boolean updateCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public boolean deleteCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public List<TblCmsTaxConfig> getAllCmsTaxConfig();

    public long getCmsTaxConfigCount();

    public TblCmsTaxConfig getCmsTaxConfig(int id);

    public TblCmsTaxConfig getLatestCmsTaxConfig(int procurementNatureId);
    
}
