/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;


import com.cptu.egp.eps.dao.daointerface.TblJointVentureDao;
import com.cptu.egp.eps.dao.daointerface.TblJvcapartnersDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblJointVenture;
import com.cptu.egp.eps.model.table.TblJvcapartners;

import com.cptu.egp.eps.service.serviceinterface.JointVentureService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author Administrator
 */
public class JointVentureServiceImpl implements JointVentureService {

    final Logger logger = Logger.getLogger(JointVentureServiceImpl.class);
    private String logUserId ="0";
    
    HibernateQueryDao hibernateQueryDao;
    TblJointVentureDao tblJointVentureDao;
    TblJvcapartnersDao tblJvcapartnersDao;
    MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblJointVentureDao getTblJointVenterDao() {
        return tblJointVentureDao;
    }

    public void setTblJointVenterDao(TblJointVentureDao tblJointVentureDao) {
        this.tblJointVentureDao = tblJointVentureDao;
    }

    public TblJvcapartnersDao getTblJvcapartnersDao() {
        return tblJvcapartnersDao;
    }

    public void setTblJvcapartnersDao(TblJvcapartnersDao tblJvcapartnersDao) {
        this.tblJvcapartnersDao = tblJvcapartnersDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    
     @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public List<Object[]> getCompanyList(String companyName, String emailId) {
        logger.debug("getCompanyList : "+logUserId+" Starts");
        List<Object[]> companyList = null;

        StringBuilder cond = new StringBuilder();
        if(companyName!=null && !"".equalsIgnoreCase(companyName)){
            cond.append("and c.companyName='"+companyName+"'");
        }
        if(emailId!=null && !"".equalsIgnoreCase(emailId)){
            cond.append("and l.emailId='"+emailId+"'");
        }

        try{
            String sqlQuery = "SELECT  c.companyId,c.companyName,l.emailId,l.userId "
                    + "FROM TblLoginMaster l,TblTendererMaster t, TblCompanyMaster c "
                    + "WHERE l.userId=t.tblLoginMaster.userId "
                        + "AND t.tblCompanyMaster.companyId=c.companyId "
                        + "and c.companyId !=1 and l.status='Approved' and l.registrationType != 'media' and l.isJvca='No'"
                        +cond.toString();

            companyList = hibernateQueryDao.createQuery(sqlQuery);
            
        }catch(Exception ex){
            logger.error("getCompanyList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getCompanyList : "+logUserId+" Ends");
        return companyList;
    }

    @Override
    public List<Object[]> jvcaPrposedList(int userId,String sidx,Object e) {
        logger.debug("getCompanyList : "+logUserId+" Starts");
        List<Object[]> list = null;
        try {
            String  sqlQuery = "select distinct jv.jvid,jv.jvname,jv.jvstatus,jp.isNominatedPartner, jv.creationDate, jv.newJvuserId "
                    + "from TblJointVenture jv, TblJvcapartners jp "
                    + "where jv.createdBy=jp.userId and jv.jvid=jp.tblJointVenture.jvid and jv.createdBy="+userId+"  Order by "+sidx+" "+e;
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception ex) {
            logger.error("jvcaPrposedList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getCompanyList : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void createJVCA(TblJointVenture tblJointVenture, List<TblJvcapartners> listJvcaPartner) {
        logger.debug("createJVCA : "+logUserId+" Starts");
        int jvId = 0;
        String action = "JVCA Proposed";
        try{
            tblJointVentureDao.addTblJointVenture(tblJointVenture);
            jvId = tblJointVenture.getJvid();
            for(TblJvcapartners tblJvcapartners:listJvcaPartner){
                tblJvcapartners.setTblJointVenture(new TblJointVenture(jvId));
                tblJvcapartnersDao.addTblJvcapartners(tblJvcapartners);
            }
        }catch(Exception ex){
            logger.error("createJVCA : "+logUserId+" : "+ex.toString());
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, jvId, "JvcaId", EgpModule.JVCA.getName(), action, tblJointVenture.getJvname());
            action = null;
        }
        logger.debug("createJVCA : "+logUserId+" Ends");
    }

    @Override
    public boolean updateStatus(String status, int jvId) {
       logger.debug("updateStatus : "+logUserId+" Starts");
       boolean flag = false;
       try {
            String query = "update TblJointVenture set jvstatus = '"+status+"' where jvid ="+jvId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        }
        catch (Exception ex) {
             logger.error("updateStatus : "+logUserId+" : "+ex.toString());
        }
       logger.debug("updateStatus : "+logUserId+" Ends");
       return flag;
    }


    @Override
    public List<Object[]> preposedJvca(int userId, String searchField, String searchOper, String searchString) {
        logger.debug("preposedJvca : "+logUserId+" Starts");
        List<Object[]> list = null;
        try {String sqlQuery = "select distinct jv.jvid,jv.jvname,jv.jvstatus,jp.isNominatedPartner,jv.creationDate, jv.newJvuserId "
                + "from TblJointVenture jv, TblJvcapartners jp "
                + "where jv.createdBy=jp.userId and jv.jvid=jp.tblJointVenture.jvid and jv.createdBy="+userId+" and "
                + " "+searchField+" "+searchOper+" '"+searchString+"'";
            list =  hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception ex) {
             logger.error("preposedJvca : "+logUserId+" : "+ex.toString());
        }
        logger.debug("preposedJvca : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<TblJointVenture> getJVCADetails(int jvcId){
         logger.debug("getJVCADetails : "+logUserId+" Starts");
         String action = "View JVCA Proposed";
        List<TblJointVenture> listJVC = new ArrayList<TblJointVenture>();
        try{
            listJVC = tblJointVentureDao.findTblJointVenture("jvid",Operation_enum.EQ,jvcId);
        }catch(Exception ex){
            logger.error("getJVCADetails : "+logUserId+" : "+ex.toString());
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, jvcId, "JvcaId", EgpModule.JVCA.getName(), action, listJVC.get(0).getJvname());
            action = null;
        }
        logger.debug("getJVCADetails : "+logUserId+" Ends");
        return listJVC;
    }

    @Override
    public int updateJVCA(int jvcId, int userId,String...values){
        logger.debug("updateJVCA : "+logUserId+" Starts");
        int status = 0;
        String action = "JVCA Invitaion sent by Tenderer";
        String jvcaName = "";
        if(values!=null && values.length>0){
             jvcaName=values[0]; 
        }
        try{
            String sqlQuery = "update TblJointVenture set jvstatus = 'Complete' where jvid = "+jvcId;
            hibernateQueryDao.updateDeleteQuery(sqlQuery);
            sqlQuery = "update TblJvcapartners set jvAcceptRejStatus='Agreed' where userId="+userId;
            hibernateQueryDao.updateDeleteQuery(sqlQuery);
            status =1;
        }catch(Exception ex){
            logger.error("updateJVCA : "+logUserId+" : "+ex.toString());
             action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, jvcId, "JvcaId", EgpModule.JVCA.getName(), action, jvcaName);
            action = null;
        }
        logger.debug("updateJVCA : "+logUserId+" Ends");
        return status;
    }

    @Override
    public int updateJVCAName(int jvId,String jvname) {
        logger.debug("updateJVCAName : "+logUserId+" Starts");
        int status = 0;
        String action = "Edit JVCA Proposed";
        try{
            String sqlQuery = "update TblJointVenture set jvname = '"+jvname+"' where jvid = "+jvId;
            hibernateQueryDao.updateDeleteQuery(sqlQuery);
            status = 1;
        }catch(Exception ex){
            logger.error("updateJVCAName : "+logUserId+" : "+ex.toString());
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, jvId, "JvcaId", EgpModule.JVCA.getName(), action, jvname);
            action = null;
        }
        logger.debug("updateJVCAName : "+logUserId+" Ends");
        return status;
    }

    @Override
    public List<Object[]> getOwnCompanyDetails(int userId){
        logger.debug("getOwnCompanyDetails : "+logUserId+" Starts");
        List<Object[]> companyList = new ArrayList<Object[]>();
        try{
            String sqlQuery = "SELECT  c.companyId,c.companyName,l.emailId,l.userId "
                    + "FROM TblLoginMaster l,TblTendererMaster t, TblCompanyMaster c "
                    + "WHERE l.userId=t.tblLoginMaster.userId "
                        + "AND t.tblCompanyMaster.companyId=c.companyId "
                        + "and c.companyId !=1 and l.status='Approved' and  l.userId = "+userId;

            companyList = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
           logger.error("getOwnCompanyDetails : "+logUserId+" : "+ex.toString());
        }
         logger.debug("getOwnCompanyDetails : "+logUserId+" Ends");
        return companyList;
    }

    @Override
    public List<Object[]> forMailContent(int jvId,int userId) {
        logger.debug("forMailContent : "+logUserId+" Starts");
        List<Object[]> mailList = new ArrayList<Object[]>();
        try {
            String sqlQuery = "select lm.emailId, cm.companyName,  jp.jvAcceptRejStatus,"
                     + "jp.isNominatedPartner,jp.jvRole, jv.createdBy,jv.jvname"
                    + " from TblLoginMaster as lm,TblCompanyMaster as cm, TblJvcapartners as jp,TblJointVenture as jv "
                    + "where lm.userId  = jp.userId and cm.companyId = jp.companyId and jp.tblJointVenture.jvid="+jvId+"and jv.jvid="+jvId;
                    //+ "and lm.userId !="+userId;
            mailList = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("forMailContent : "+logUserId+" : "+e.toString());
        }
        logger.debug("forMailContent : "+logUserId+" Ends");
        return mailList;
       
    }

    @Override
    public List<Object> forMailEmail(int jvId,int userId) {
        logger.debug("forMailContent : "+logUserId+" Starts");
        List<Object> mailList = new ArrayList<Object>();
        try {
            String sqlQuery = "select lm.emailId from TblLoginMaster lm where lm.userId in "
                    + "(select jp.userId from TblJvcapartners jp where jp.userId != "+userId+" and jp.tblJointVenture.jvid="+jvId+")";
            mailList = hibernateQueryDao.singleColQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("forMailEmail : "+logUserId+" : "+e.toString());
        }
        logger.debug("forMailEmail : "+logUserId+" Ends");
        return mailList;
    }
}
