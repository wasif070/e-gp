/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AppViewPkgService {

    /**
     * Fetch APP Package Details
     * @param appId
     * @param packageId
     * @param pkgType
     * @return
     */
    List<CommonAppPkgDetails> getAppPkgDetails(int appId,int packageId,String pkgType);

    /**
     * Set userId at Service layer.
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
    /**
     * Set Audit Trail Object at Service layer.
     * @param Audittrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
