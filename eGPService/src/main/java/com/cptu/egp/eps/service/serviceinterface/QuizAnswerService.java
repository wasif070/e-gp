/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblQuizAnswer;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface QuizAnswerService {
    
    public void addTblQuizAnswer(TblQuizAnswer tblQuizAnswer);
    
    public void deleteTblQuizAnswer(TblQuizAnswer tblQuizAnswer);

    /**
     *
     * @param tblQuizAnswer
     * @return
     */
    public String updateTblQuizAnswer(TblQuizAnswer tblQuizAnswer);

    public List<TblQuizAnswer> getAllTblQuizAnswer();
    
    /**
     *
     * @param id
     * @return
     */
    public List<TblQuizAnswer> findTblQuizAnswer(int id);
    
    public List<TblQuizAnswer> findTblQuizAnswerByAnsId(int id);
    
}
