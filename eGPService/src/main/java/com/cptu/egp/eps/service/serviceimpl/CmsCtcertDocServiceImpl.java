/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsCtcertDocDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import com.cptu.egp.eps.service.serviceinterface.CmsCtcertDocService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsCtcertDocServiceImpl implements CmsCtcertDocService {

    final Logger logger = Logger.getLogger(CmsCtcertDocServiceImpl.class);
    private TblCmsCtcertDocDao cmsCtcertDocDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "   ";

    public TblCmsCtcertDocDao getCmsCtcertDocDao() {
        return cmsCtcertDocDao;
    }

    public void setCmsCtcertDocDao(TblCmsCtcertDocDao cmsCtcertDocDao) {
        this.cmsCtcertDocDao = cmsCtcertDocDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsCtcertDoc object in database
     * @param tblCmsCtcertDoc
     * @return int 
     */
    @Override
    public int insertCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("insertCmsCtcertDoc : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsCtcertDocDao.addTblCmsCtCertDoc(tblCmsCtcertDoc);
            flag = tblCmsCtcertDoc.getCtCertDocId();
        } catch (Exception ex) {
            logger.error("insertCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsCtcertDoc object in DB
     * @param tblCmsCtcertDoc
     * @return boolean
     */
    @Override
    public boolean updateCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("updateCmsCtcertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsCtcertDocDao.updateTblCmsCtCertDoc(tblCmsCtcertDoc);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsCtcertDoc object in DB
     * @param tblCmsCtcertDoc
     * @return boolean
     */
    @Override
    public boolean deleteCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("deleteCmsCtcertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsCtcertDocDao.deleteTblCmsCtCertDoc(tblCmsCtcertDoc);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsCtcertDoc objects from database
     * @return List<TblCmsCtcertDoc>
     */
    @Override
    public List<TblCmsCtcertDoc> getAllCmsCtcertDoc() {
        logger.debug("getAllCmsCtcertDoc : " + logUserId + STARTS);
        List<TblCmsCtcertDoc> cmsCtCertDocList = new ArrayList<TblCmsCtcertDoc>();
        try {
            cmsCtCertDocList = cmsCtcertDocDao.getAllTblCmsCtCertDoc();
        } catch (Exception ex) {
            logger.error("getAllCmsCtcertDoc : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsCtcertDoc : " + logUserId + ENDS);
        return cmsCtCertDocList;
    }

    /***
     *  This method returns no. of TblCmsCtcertDoc objects from database
     * @return long
     */
    @Override
    public long getCmsCtcertDocCount() {
        logger.debug("getCmsCtcertDocCount : " + logUserId + STARTS);
        long cmsCtCertDocCount = 0;
        try {
            cmsCtCertDocCount = cmsCtcertDocDao.getTblCmsCtCertDocCount();
        } catch (Exception ex) {
            logger.error("getCmsCtcertDocCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsCtcertDocCount : " + logUserId + ENDS);
        return cmsCtCertDocCount;
    }

    /***
     * This method returns TblCmsCtcertDoc for the given Id
     * @param int ctCertDocId
     * @return TblCmsCtcertDoc
     */
    @Override
    public TblCmsCtcertDoc getCmsCtcertDoc(int ctCertDocId) {
        logger.debug("getCmsCtcertDoc : " + logUserId + STARTS);
        TblCmsCtcertDoc tblCmsCtCertDoc = null;
        List<TblCmsCtcertDoc> cmsCtCertDocList = null;
        try {
            cmsCtCertDocList = cmsCtcertDocDao.findTblCmsCtCertDoc("ctCertDocId", Operation_enum.EQ, ctCertDocId);
        } catch (Exception ex) {
            logger.error("getCmsCtcertDoc : " + logUserId + SPACE + ex);
        }
        if (!cmsCtCertDocList.isEmpty()) {
            tblCmsCtCertDoc = cmsCtCertDocList.get(0);
        }
        logger.debug("getCmsCtcertDoc : " + logUserId + ENDS);
        return tblCmsCtCertDoc;
    }

    /***
     * This method gives the list of TblCmsCtcertDoc objects for the given wcCertificateId
     * @param int contractTerminationId
     * @return List<TblCmsCtcertDoc>
     */
    @Override
    public List<TblCmsCtcertDoc> getAllCmsCtcertDocForCtId(int contractTerminationId) {
        logger.debug("getAllCmsCtcertDocForCtId : " + logUserId + STARTS);
        List<TblCmsCtcertDoc> cmsCtCertDocList = new ArrayList<TblCmsCtcertDoc>();
        try {
            cmsCtCertDocList = cmsCtcertDocDao.findTblCmsCtCertDoc("contractTerminationId",
                    Operation_enum.EQ, contractTerminationId);
        } catch (Exception ex) {
            logger.error("getAllCmsCtcertDocForCtId : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsCtcertDocForCtId : " + logUserId + ENDS);
        return cmsCtCertDocList;
    }
}
