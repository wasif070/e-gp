/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblListCellDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateCellsDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateColumnsDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateFormulasDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionFormDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateTablesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblListCellDetail;
import com.cptu.egp.eps.model.table.TblTemplateCells;
import com.cptu.egp.eps.model.table.TblTemplateColumns;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import com.cptu.egp.eps.service.serviceinterface.TemplateTableService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yankii
 */
public class TemplateTableImpl implements TemplateTableService {

    TblTemplateTablesDao tblTemplateTablesDao;
    HibernateQueryDao hibernateQueryDao;
    TblTemplateColumnsDao tblTemplateColumnsDao;
    TblTemplateCellsDao tblTemplateCellsDao;
    TblTemplateFormulasDao tblTemplateFormulasDao;
    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TemplateTableImpl.class);
    TblListCellDetailDao tblListCellDetailDao;
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    public void setTblTemplateFormulasDao(TblTemplateFormulasDao tblTemplateFormulasDao) {
        this.tblTemplateFormulasDao = tblTemplateFormulasDao;
    }

    public TblTemplateFormulasDao getTblTemplateFormulasDao() {
        return tblTemplateFormulasDao;
    }

    public TblTemplateCellsDao getTblTemplateCellsDao() {
        return tblTemplateCellsDao;
    }

    public TblTemplateColumnsDao getTblTemplateColumnsDao() {
        return tblTemplateColumnsDao;
    }

    public void setTblTemplateCellsDao(TblTemplateCellsDao tblTemplateCellsDao) {
        this.tblTemplateCellsDao = tblTemplateCellsDao;
    }

    public void setTblTemplateColumnsDao(TblTemplateColumnsDao tblTemplateColumnsDao) {
        this.tblTemplateColumnsDao = tblTemplateColumnsDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setTblTemplateMasterDao(TblTemplateMasterDao tblTemplateMasterDao) {
        this.tblTemplateMasterDao = tblTemplateMasterDao;
    }

    public void setTblTemplateSectionFormDao(TblTemplateSectionFormDao tblTemplateSectionFormDao) {
        this.tblTemplateSectionFormDao = tblTemplateSectionFormDao;
    }
    TblTemplateMasterDao tblTemplateMasterDao;

    public TblTemplateMasterDao getTblTemplateMasterDao() {
        return tblTemplateMasterDao;
    }

    public TblTemplateSectionFormDao getTblTemplateSectionFormDao() {
        return tblTemplateSectionFormDao;
    }
    TblTemplateSectionFormDao tblTemplateSectionFormDao;

    public void setTblTemplateTablesDao(TblTemplateTablesDao tblTemplateTablesDao) {
        this.tblTemplateTablesDao = tblTemplateTablesDao;
    }

    public TblTemplateTablesDao getTblTemplateTablesDao() {
        return tblTemplateTablesDao;
    }

    public TblListCellDetailDao getTblListCellDetailDao() {
        return tblListCellDetailDao;
    }

    public void setTblListCellDetailDao(TblListCellDetailDao tblListCellDetailDao) {
        this.tblListCellDetailDao = tblListCellDetailDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    

    @Override
    public boolean addFormTable(TblTemplateTables tblTables) {
        logger.debug("addFormTable : " + logUserId + "starts");
        boolean boolAddFormTable = false;
        String action = "Add Table";
        try {
            tblTemplateTablesDao.addTblTemplateTables(tblTables);
            boolAddFormTable = true;
        } catch (Exception ex) {
            logger.error("addFormTable : " + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblTables.getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }
        logger.debug("addFormTable : " + logUserId + "ends");
        return boolAddFormTable;
    }

    @Override
    public boolean editFormTable(TblTemplateTables tblTables) {
        logger.debug("editFormTable : " + logUserId + "starts");
        boolean boolEditFormTable = false;
        String action = "Edit Table Details";
        try {
            tblTemplateTablesDao.updateTblTemplateTables(tblTables);
            boolEditFormTable = true;
            
        } catch (Exception ex) {
            logger.error("editFormTable : " + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblTables.getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }
        logger.debug("editFormTable : " + logUserId + "ends");
        return boolEditFormTable;
    }

    @Override
    public boolean deleteFormTable(int tableId) {
        logger.debug("deleteFormTable : " + logUserId + "starts");
        boolean flag = true;
        String action = "Delete Table";
        List<TblTemplateTables> tblTemplateTableses = new ArrayList<TblTemplateTables>();
        try {
            tblTemplateTableses = tblTemplateTablesDao.findTblTemplateTables("tableId",Operation_enum.EQ,tableId);
            String query = "delete from TblTemplateTables ttt where ttt.tableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception ex) {
            logger.error("deleteFormTable : " + ex);
            flag = false;
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblTemplateTableses.get(0).getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }
        logger.debug("deleteFormTable : " + logUserId + "ends");
        return flag;
    }

    @Override
    public List<TblTemplateTables> getFormTables(short templateId, int sectionId, int formId) {
        List<TblTemplateTables> tblTables = null;
        logger.debug("getFormTables : " + logUserId + "starts");
        try {
            tblTables = tblTemplateTablesDao.findTblTemplateTables("tblTemplateMaster", Operation_enum.EQ, new TblTemplateMaster(templateId), "tblTemplateSectionForm", Operation_enum.EQ, new TblTemplateSectionForm(formId), "sectionId", Operation_enum.EQ, sectionId);

        } catch (Exception ex) {
            logger.error("getFormTables : " + ex);
            tblTables = null;
        }
        logger.debug("getFormTables : " + logUserId + "ends");
        return tblTables;
    }

    @Override
    public List<TblTemplateTables> getFormTablesDetails(int tableId) {
        List<TblTemplateTables> tblTable;
        logger.debug("getFormTablesDetails : " + logUserId + "starts");
        try {
            tblTable = tblTemplateTablesDao.findTblTemplateTables("tableId", Operation_enum.EQ, tableId);

        } catch (Exception ex) {
            logger.error("getFormTablesDetails : " + ex);
            tblTable = null;
        }
        logger.debug("getFormTablesDetails : " + logUserId + "ends");
        return tblTable;
    }

    @Override
    public boolean isEntryPresentInTableCols(int tableId) {
        logger.debug("isEntryPresentInTableCols : " + logUserId + "starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTemplateColumns ttc", "ttc.tblTemplateTables.tableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }

        } catch (Exception ex) {
            logger.error("isEntryPresentInTableCols : " + ex);

        }
        logger.debug("isEntryPresentInTableCols : " + logUserId + "ends");
        return isPresent;
    }

    @Override
    public List<TblTemplateColumns> getTableColumns(int tableId, boolean inSortOrder) {
        List<TblTemplateColumns> tblTableCols;
        logger.debug("getTableColumns : " + logUserId + "starts");
        try {
            if (inSortOrder) {
                tblTableCols = tblTemplateColumnsDao.findTblTemplateColumns("tblTemplateTables", Operation_enum.EQ, new TblTemplateTables(tableId), "sortOrder", Operation_enum.ORDERBY, Operation_enum.ASC);
            } else {
                tblTableCols = tblTemplateColumnsDao.findTblTemplateColumns("tblTemplateTables", Operation_enum.EQ, new TblTemplateTables(tableId));
            }

        } catch (Exception ex) {
            logger.error("getTableColumns : " + ex);
            tblTableCols = null;
        }
        logger.debug("getTableColumns : " + logUserId + "ends");
        return tblTableCols;
    }

    @Override
    public List<TblListCellDetail> getListCellDetail(int tableId, short columnId, int cellId) {
        List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
        logger.debug("getListCellDetail : " + logUserId + "starts");
        try {
            listCellDetail = tblListCellDetailDao.findTblListCellDetail("tblTemplateTables", Operation_enum.EQ, new TblTemplateTables(tableId), "columnId", Operation_enum.EQ, columnId, "cellId", Operation_enum.EQ, cellId);
        } catch (Exception ex) {
            logger.error("getListCellDetail : " + ex);
        }
        logger.debug("getListCellDetail : " + logUserId + "ends");
        return listCellDetail;
    }

    public List<TblTemplateCells> getTableCellsTestForm(int tableId) {
        logger.debug("getTableCellsTestForm : " + logUserId + "starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.templateCelId, ttc.tblTemplateTables, ttc.columnId, ");
        strQuery.append(" ttc.cellId, ttc.rowId, ttc.cellDatatype, ttc.cellvalue, tc.showorhide ");
        strQuery.append(" from TblTemplateCells ttc, TblTemplateColumns tc ");
        strQuery.append(" where tc.tblTemplateTables.tableId = ttc.tblTemplateTables.tableId and ttc.columnId = tc.columnId ");
        strQuery.append(" and ttc.tblTemplateTables.tableId = " + tableId + " order by ttc.rowId asc, tc.sortOrder asc");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTemplateCells> tblTableCells = new ArrayList<TblTemplateCells>();
        try {
            //tblTableCells = tblTemplateCellsDao.findTblTemplateCells("tblTemplateTables", Operation_enum.EQ, new TblTemplateTables(tableId), "rowId", Operation_enum.ORDERBY, Operation_enum.ASC, "columnId", Operation_enum.ORDERBY, Operation_enum.ASC);
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                tblTableCells.add(new TblTemplateCells(
                        Integer.parseInt(i[0].toString()),
                        (TblTemplateTables) i[1],
                        Short.parseShort(i[2].toString()),
                        Integer.parseInt(i[3].toString()),
                        Integer.parseInt(i[4].toString()),
                        Byte.parseByte(i[5].toString()),
                        i[6].toString(),
                        Byte.parseByte(i[7].toString())));
                //tblTableCells.add(new TblTemplateCells(Byte.parseByte(i[0].toString()), i[1].toString(), Byte.parseByte(i[2].toString())));
            }
            if (strQuery != null) {
                strQuery = null;
            }

        } catch (Exception ex) {
            logger.error("getTableCellsTestForm : " + ex);
            tblTableCells = null;
        }
        logger.debug("getTableCellsTestForm : " + logUserId + "ends");
        return tblTableCells;
    }

    @Override
    public List<TblTemplateCells> getTableCells(int tableId) {
        logger.debug("getTableCells : " + logUserId + "starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.cellDatatype, ttc.cellvalue, tc.showorhide, ttc.columnId,ttc.cellId");
        strQuery.append(" from TblTemplateCells ttc, TblTemplateColumns tc ");
        strQuery.append(" where tc.tblTemplateTables.tableId = ttc.tblTemplateTables.tableId and ttc.columnId = tc.columnId ");
        strQuery.append(" and ttc.tblTemplateTables.tableId = " + tableId + " order by ttc.rowId asc, tc.sortOrder asc");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTemplateCells> tblTableCells = new ArrayList<TblTemplateCells>();
        try {
            //tblTableCells = tblTemplateCellsDao.findTblTemplateCells("tblTemplateTables", Operation_enum.EQ, new TblTemplateTables(tableId), "rowId", Operation_enum.ORDERBY, Operation_enum.ASC, "columnId", Operation_enum.ORDERBY, Operation_enum.ASC);
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            logger.debug("getTableCells : " + objs.size());
            for (Object[] i : objs) {
                //tblTableCells.add(new TblTemplateCells(Integer.parseInt(i[0].toString()),new TblTemplateTables(Integer.parseInt(i[1].toString())),Short.parseShort(i[2].toString()),Integer.parseInt(i[3].toString()),Integer.parseInt(i[4].toString()),Byte.parseByte(i[5].toString()),i[6].toString()));
                tblTableCells.add(new TblTemplateCells(Byte.parseByte(i[0].toString()), i[1].toString(), Byte.parseByte(i[2].toString()), Short.parseShort(i[3].toString()), Integer.parseInt(i[4].toString())));
            }
            if (strQuery != null) {
                strQuery = null;
            }

        } catch (Exception ex) {
            logger.error("getTableCells : " + ex);
            tblTableCells = null;
        }
        logger.debug("getTableCells : " + logUserId + "ends");
        return tblTableCells;
    }

    @Override
    public boolean updateColNRowCnt(int tableId, short noOfRows, short noOfCols) {
        logger.debug("updateColNRowCnt : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "update TblTemplateTables set noOfRows = " + noOfRows + ", noOfCols = " + noOfCols + " where tableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("updateColNRowCnt : " + ex);
            flag = false;
        }
        logger.debug("updateColNRowCnt : " + logUserId + "ends");
        return flag;
    }

    @Override
    public short getNoOfTablesInForm(int formId) {
        logger.debug("getNoOfTablesInForm : " + logUserId + "starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTemplateTables ttc", "ttc.tblTemplateSectionForm.formId = " + formId);

        } catch (Exception ex) {
            logger.error("getNoOfTablesInForm : " + ex);

        }
        logger.debug("getNoOfTablesInForm : " + logUserId + "ends");
        return cnt;
    }

    @Override
    public short getNoOfColsInTable(int tableId) {
        logger.debug("getNoOfColsInTable : " + logUserId + "starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTemplateColumns ttc", "ttc.tblTemplateTables.tableId = " + tableId);

        } catch (Exception ex) {
            logger.error("getNoOfColsInTable : " + ex);

        }
        logger.debug("getNoOfColsInTable : " + logUserId + "ends");
        return cnt;
    }

    @Override
    public short getNoOfRowsInTable(int tableId, short columnId) {
        short cnt = 0;
        logger.debug("getNoOfRowsInTable : " + logUserId + "starts");
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTemplateCells ttc", "ttc.tblTemplateTables.tableId = " + tableId + " and ttc.columnId = " + columnId);

        } catch (Exception ex) {
            logger.error("getNoOfRowsInTable : " + ex);

        }
        logger.debug("getNoOfRowsInTable : " + logUserId + "ends");
        return cnt;
    }

    @Override
    public List<TblTemplateColumns> getTableAutoColumns(int tableId) {
        logger.debug("getTableAutoColumns : " + logUserId + "starts");
        String strQuery = "select ttc.columnId, ttc.columnHeader from TblTemplateColumns ttc where ttc.tblTemplateTables.tableId = " + tableId + " and ttc.filledBy = 3 and ttc.columnId not in (select ttf.columnId from TblTemplateFormulas ttf where ttf.tableId = " + tableId + ")";
        List<TblTemplateColumns> cols = new ArrayList<TblTemplateColumns>();
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            for (Object[] i : objs) {
                cols.add(new TblTemplateColumns(Short.parseShort(i[0].toString()), (String) i[1]));
            }

        } catch (Exception ex) {
            logger.error("getTableAutoColumns : " + ex);
            objs = null;
            cols = null;

        }
        logger.debug("getTableAutoColumns : " + logUserId + "ends");
        return cols;
    }

    @Override
    public List<TblTemplateFormulas> getTableFormulas(int tableId) {
        logger.debug("getTableFormulas : " + logUserId + "starts");
        List<TblTemplateFormulas> tblTableFormulas;
        try {
            tblTableFormulas = tblTemplateFormulasDao.findTblTemplateFormulas("tableId", Operation_enum.EQ, tableId);

        } catch (Exception ex) {
            logger.error("getTableFormulas : " + ex);
            tblTableFormulas = null;
        }
        logger.debug("getTableFormulas : " + logUserId + "ends");
        return tblTableFormulas;
    }

    @Override
    public boolean addTableFormula(TblTemplateFormulas tblFormulas) {
        logger.debug("addTableFormula : " + logUserId + "starts");
        boolean boolAddFormTable = false;
        try {
            tblTemplateFormulasDao.addTblTemplateFormulas(tblFormulas);
            boolAddFormTable = true;
        } catch (Exception ex) {
            logger.error("addTableFormula : " + ex);
        }
        logger.debug("addTableFormula : " + logUserId + "ends");
        return boolAddFormTable;
    }

    @Override
    public boolean deleteTableFormula(String columns, int tableId) {
        boolean flag = true;
        logger.debug("deleteTableFormula : " + logUserId + "starts");
        try {
            String query = "delete from TblTemplateFormulas ttt where ttt.tableId =" + tableId + " and ttt.columnId in (" + columns + ")";
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteTableFormula : " + ex);
            flag = false;
        }
        logger.debug("deleteTableFormula : " + logUserId + "ends");
        return flag;
    }

    public boolean isAutoColumnPresent(int tableId) {
        logger.debug("isAutoColumnPresent : " + logUserId + "starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTemplateColumns ttc", "ttc.tblTemplateTables.tableId = " + tableId + " and ttc.filledBy = 3");
            if (cnt > 0) {
                isPresent = true;
            }

        } catch (Exception ex) {
            logger.error("isAutoColumnPresent : " + ex);

        }
        logger.debug("isAutoColumnPresent : " + logUserId + "ends");
        return isPresent;
    }

    public boolean atLeastOneFormulaCreated(int tableId) {
        logger.debug("atLeastOneFormulaCreated : " + logUserId + "starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTemplateFormulas ttf", "ttf.tableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }

        } catch (Exception ex) {
            logger.error("atLeastOneFormulaCreated : " + ex);

        }
        logger.debug("atLeastOneFormulaCreated : " + logUserId + "ends");
        return isPresent;
    }

    public boolean isTotalFormulaCreated(int tableId) {
        logger.debug("getCompanyDetails : " + logUserId + "starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTemplateFormulas ttf", "ttf.isGrandTotal = 'yes' and ttf.tableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }

        } catch (Exception ex) {
            logger.error("isTotalFormulaCreated : " + ex);

        }
        logger.debug("isTotalFormulaCreated : " + logUserId + "ends");
        return isPresent;
    }

    public HashMap<Integer, Integer> getGTColumns(int tableId) {
        logger.debug("getGTColumns : " + logUserId + "starts");
        String strQuery = "select ttf.columnId, ttf.isGrandTotal from TblTemplateFormulas ttf where ttf.tableId = " + tableId + " and ttf.isGrandTotal = 'yes'";
        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTemplateFormulas> cols = new ArrayList<TblTemplateFormulas>();
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            Integer j = 0;
            for (Object[] i : objs) {
                cols.add(new TblTemplateFormulas(Integer.parseInt(i[0].toString()), i[1].toString()));
                hm.put(j, (Integer) i[0]);
                j++;
            }

        } catch (Exception ex) {
            logger.error("getGTColumns : " + ex);
            objs = null;
            hm = null;
        }
        logger.debug("getGTColumns : " + logUserId + "ends");
        return hm;
    }

    public short getFormulaCountOfTotal(int tableId) {
        logger.debug("getFormulaCountOfTotal : " + logUserId + "starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTemplateFormulas ttf", "ttf.tableId = " + tableId + " and ttf.formula like '%TOTAL%'");

        } catch (Exception ex) {
            logger.error("getFormulaCountOfTotal : " + ex);

        }
        logger.debug("getFormulaCountOfTotal : " + logUserId + "ends");
        return cnt;
    }

    public short getFormulaCountOfTotal(int tableId, String cols) {
        logger.debug("getFormulaCountOfTotal : " + logUserId + "starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTemplateFormulas ttf", "ttf.tableId = " + tableId + " and ttf.formula like '%TOTAL%' and ttf.columnId in (" + cols + ")");

        } catch (Exception ex) {
            logger.error("getFormulaCountOfTotal : " + ex);

        }
        logger.debug("getFormulaCountOfTotal : " + logUserId + "ends");
        return cnt;
    }

    public boolean deleteFormulaEffectsFromTemplateCells(int tableId) {
        logger.debug("deleteFormulaEffectsFromTemplateCells : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "delete from TblTemplateCells ttc where ttc.tblTemplateTables.tableId = " + tableId + " and ttc.rowId in (select ttt.noOfRows from TblTemplateTables ttt where ttt.tableId = " + tableId + ")";
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteFormulaEffectsFromTemplateCells : " + ex);
            flag = false;
        }
        logger.debug("deleteFormulaEffectsFromTemplateCells : " + logUserId + "ends");
        return flag;
    }

    public boolean deleteFormulaEffectsFromTemplateTables(int tableId) {
        logger.debug("deleteFormulaEffectsFromTemplateTables : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "update TblTemplateTables set noOfRows = noOfRows - 1 where tableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteFormulaEffectsFromTemplateTables : " + ex);
            flag = false;
        }
        logger.debug("deleteFormulaEffectsFromTemplateTables : " + logUserId + "ends");
        return flag;
    }

    public List<TblTemplateCells> getColumnsForGS(int formId) {
        logger.debug("getColumnsForGS : " + logUserId + "starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.cellId, ttc.tblTemplateTables.tableId, tc.columnHeader from TblTemplateCells ttc, TblTemplateColumns tc where ");
        strQuery.append(" tc.columnId = ttc.columnId and ttc.tblTemplateTables.tableId = tc.tblTemplateTables.tableId ");
        strQuery.append(" and ttc.columnId in (select ttf.columnId from TblTemplateFormulas ttf ");
        strQuery.append(" where ttf.formId = " + formId + " and ttf.formula like '%TOTAL%') ");
        strQuery.append(" and ttc.tblTemplateTables.tableId in (select ttt.tableId from TblTemplateTables ttt where ttt.tblTemplateSectionForm.formId = " + formId + ") ");
        strQuery.append(" and ttc.rowId in(select tt.noOfRows from TblTemplateTables tt where tt.tableId in ");
        strQuery.append(" (select t.tableId from TblTemplateTables t where t.tblTemplateSectionForm.formId = " + formId + "))");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTemplateCells> cells = new ArrayList<TblTemplateCells>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                cells.add(new TblTemplateCells(Integer.parseInt(i[0].toString()), new TblTemplateTables(Integer.parseInt(i[1].toString())), i[2].toString()));
            }
            if (strQuery != null) {
                strQuery = null;
            }

        } catch (Exception ex) {
            logger.error("getColumnsForGS : " + ex);
            objs = null;
            cells = null;
        }
        logger.debug("getColumnsForGS : " + logUserId + "ends");
        return cells;
    }

    public List<Object[]> getFormulaColIds(int tenderTableId) {
        logger.debug("getFormulaColIds : " + logUserId + "starts");
        List<Object[]> tenderFormula = null;
        try {
            String query = " select tf.formulaId,tf.formula,tf.columnId from  TblTemplateFormulas tf where tf.tableId = " + tenderTableId + " and tf.formula like '%TOTAL%' ";
            tenderFormula = hibernateQueryDao.createNewQuery(query);


        } catch (Exception ex) {
            logger.error("getFormulaColIds : " + ex);

        }
        logger.debug("getFormulaColIds : " + logUserId + "ends");
        return tenderFormula;
    }

    public boolean updateOnlyRowCnt(int tableId) {
        logger.debug("updateOnlyRowCnt : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "update TblTemplateTables set noOfRows = noOfRows + 1 where tableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("updateOnlyRowCnt : " + ex);
            flag = false;
        }
        logger.debug("updateOnlyRowCnt : " + logUserId + "ends");
        return flag;
    }

    public int getMaxCellCountForTable(int tableId) {
        logger.debug("getMaxCellCountForTable : " + logUserId + "starts");
        List<Object> obj;
        int maxId = -1;
        try {
            String query = "select max(ttc.cellId) as maxCnt from TblTemplateCells ttc where ttc.tblTemplateTables.tableId = " + tableId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null) {
                if (obj.size() > 0) {
                    maxId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }

        } catch (Exception ex) {
            logger.error("getMaxCellCountForTable : " + ex);

        }
        logger.debug("getMaxCellCountForTable : " + logUserId + "ends");
        return maxId;
    }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }
}