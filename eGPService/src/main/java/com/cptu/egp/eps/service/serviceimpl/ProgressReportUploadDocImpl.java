/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsPrDocumentDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsPrDocument;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import com.cptu.egp.eps.service.serviceinterface.ProgressReportUploadDocService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class ProgressReportUploadDocImpl implements ProgressReportUploadDocService{

    private static final Logger LOGGER = Logger.getLogger(ProgressReportUploadDocImpl.class);
    private String logUserId = "0";
    HibernateQueryDao hibernateQueryDao;
    TblCmsPrDocumentDao tblCmsPrDocumentDao;

    public TblCmsPrDocumentDao getTblCmsPrDocumentDao() {
        return tblCmsPrDocumentDao;
    }

    public void setTblCmsPrDocumentDao(TblCmsPrDocumentDao tblCmsPrDocumentDao) {
        this.tblCmsPrDocumentDao = tblCmsPrDocumentDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public List<Object[]> getListForView(String wpId, int prId) {
        LOGGER.debug("getListForEdit : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select td.wpSrNo,td.wpDescription,td.wpUom, td.wpQty,tr.qtyAcceptTillThisPr, ");
            query.append(" tr.qtyDlvrdCurrPr,tr.totalPendingQty,tr.itemEntryDt, tr.remarks,");
            query.append(" td.wpRowId,tr.progressReptDtlId,tpr.progressRepId,td.tenderTableId,tpr.progressRepcreatedDt");
            query.append(" from TblCmsPrMaster tpr, TblCmsWpDetail td,TblCmsPrDetail tr ");
            query.append(" where td.tblCmsWpMaster.wpId=" + wpId + " and td.tblCmsWpMaster.wpId = tpr.tblCmsWpMaster.wpId");
            query.append(" and tpr.progressRepId = tr.tblCmsPrMaster.progressRepId");
            query.append(" and td.tenderTableId=tr.tenderTableId");
            query.append(" and td.wpRowId = tr.rowId and tr.tblCmsPrMaster.progressRepId=" + prId);
            list = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            LOGGER.error("getListForEdit : " + e + "");
        }
        LOGGER.debug("getListForEdit : " + logUserId + "ends");
        return list;
    }

    @Override
    public boolean addPrDocsDetails(TblCmsPrDocument tblCmsPrDocument) {
        LOGGER.debug("addPrDocsDetails : " + logUserId + "Starts");
        boolean flag = false;
        try
        {
            tblCmsPrDocumentDao.addTblCmsPrDocument(tblCmsPrDocument);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("addPrDocsDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("addPrDocsDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public boolean deletePrDocsDetails(int prDocId) {
        LOGGER.debug("deletePrDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        int i=0;
        try {
                i = hibernateQueryDao.updateDeleteNewQuery("delete from TblCmsPrDocument where prDocId="+prDocId+"");
                if(i>0)
                {
                    flag = true;
                }
        } catch (Exception e) {
            LOGGER.error("deletePrDocsDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("deletePrDocsDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<TblCmsPrDocument> getPrDocumentDetails(int prRepId) {
        LOGGER.debug("getPrDocumentDetails : " + logUserId + "Starts");
        List<TblCmsPrDocument> list = null;
        try
        {
            list = tblCmsPrDocumentDao.findTblCmsPrDocument("tblCmsPrMaster",Operation_enum.EQ,new TblCmsPrMaster(prRepId));
        } catch (Exception e) {
            LOGGER.error("getPrDocumentDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("getPrDocumentDetails : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getListForViewForWorks(String wpId, int prId) {
         LOGGER.debug("getListForViewForWorks : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select td.wpSrNo,td.groupId,td.wpDescription,td.wpUom, td.wpQty,tr.qtyAcceptTillThisPr, ");
            query.append(" tr.qtyDlvrdCurrPr,tr.totalPendingQty,tr.itemEntryDt, tr.remarks,");
            query.append(" td.wpRowId,tr.progressReptDtlId,tpr.progressRepId,td.tenderTableId,tpr.progressRepcreatedDt");
            query.append(" from TblCmsPrMaster tpr, TblCmsWpDetail td,TblCmsPrDetail tr ");
            query.append(" where td.tblCmsWpMaster.wpId=" + wpId + " and td.tblCmsWpMaster.wpId = tpr.tblCmsWpMaster.wpId");
            query.append(" and tpr.progressRepId = tr.tblCmsPrMaster.progressRepId");
            query.append(" and td.tenderTableId=tr.tenderTableId");
            query.append(" and td.wpRowId = tr.rowId and tr.tblCmsPrMaster.progressRepId=" + prId+" order BY td.groupId");
            list = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            LOGGER.error("getListForViewForWorks : " + e + "");
        }
        LOGGER.debug("getListForViewForWorks : " + logUserId + "ends");
        return list;
    }

}
