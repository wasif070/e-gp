/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblContentVerificationDao;
import com.cptu.egp.eps.model.table.TblContentVerification;
import com.cptu.egp.eps.service.serviceinterface.ContentVeriConfigService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class ContentVeriConfigServiceImpl implements ContentVeriConfigService {

    static final Logger logger = Logger.getLogger(ContentVeriConfigServiceImpl.class);
    TblContentVerificationDao  tblContentVerificationDao;
    private String logUserId = "0";

    public TblContentVerificationDao getTblContentVerificationDao() {
        return tblContentVerificationDao;
    }

    public void setTblContentVerificationDao(TblContentVerificationDao tblContentVerificationDao) {
        this.tblContentVerificationDao = tblContentVerificationDao;
    }


    @Override
    public void addContent(TblContentVerification content) {
        logger.debug("addContent : " + logUserId + " Starts");
        try {
            tblContentVerificationDao.addTblContentVerification(content);
        } catch (Exception e) {
            logger.error("addContent : " + logUserId + " "+e);
        }
                logger.debug("addContent : " + logUserId + " Ends");
    }

    @Override
    public List<TblContentVerification> getTblContentVerification() {
        logger.debug("getTblContentVerification : " + logUserId + " Starts");
        List<TblContentVerification> list = null;
        try {
            list = getTblContentVerificationDao().getAllTblContentVerification();
        } catch (Exception e) {
            logger.error("getTblContentVerification : " + logUserId + " "+e);
        }
        logger.debug("getTblContentVerification : " + logUserId + " Ends");
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
}
