/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCascadeMasterDao;
import com.cptu.egp.eps.model.table.TblCascadeLinkMaster;
import com.cptu.egp.eps.service.serviceinterface.CascadeMasterService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author tejasree.goriparthi
 */
public class CascadeMasterServiceImpl implements CascadeMasterService{
    private static final Logger LOGGER = Logger.getLogger(CascadeMasterServiceImpl.class);
    TblCascadeMasterDao tblCascadeMasterDao;
    HibernateQueryDao hibernateQueryDao;

    public TblCascadeMasterDao getTblCascadeMasterDao() {
        return tblCascadeMasterDao;
    }

    public void setTblCascadeMasterDao(TblCascadeMasterDao tblCascadeMasterDao) {
        this.tblCascadeMasterDao = tblCascadeMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public List<TblCascadeLinkMaster> getCascadeMasterList(String userTypeId, String parentList) throws Exception {
        LOGGER.debug("getCascadeDetails : " + userTypeId + "Starts");
        List<Object> temp = null;
         List<TblCascadeLinkMaster> cascadeMasterList = new ArrayList<TblCascadeLinkMaster>();
        try {
            StringBuilder query = new StringBuilder();
            query = query.append("from TblCascadeLinkMaster where userTypeId='"+userTypeId+"' and (");
            if(parentList.indexOf(",")!=-1){
                String pageLists[] = parentList.split(",");
                for(int i=0;i<pageLists.length;i++){
                    query = query.append("linkId = '"+pageLists[i]+"' ");
                    if(i+1<pageLists.length){
                        query = query.append("or ");
                    }else{
                        query = query.append(") ");
                    }
                }
            }else{
                query = query.append(" linkId = '"+parentList +"' ) ");
            }
            query = query.append(" order by linkId asc ");
            temp = tblCascadeMasterDao.findTblCascadeMaster(query.toString());
            for(Object obj : temp){
                cascadeMasterList.add((TblCascadeLinkMaster)obj);
            }
        } catch (Exception e) {
            LOGGER.error("getCascadeDetails : " + userTypeId + " " + e);
        }
        LOGGER.debug("addInvoiceAccDetails : " + userTypeId + "Ends");
        return cascadeMasterList;
    }

}
