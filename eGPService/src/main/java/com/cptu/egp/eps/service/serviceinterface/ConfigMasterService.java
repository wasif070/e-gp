/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Naresh.Akurathi
 */
public interface ConfigMasterService {

    /**
     * Get all TblConfigurationMaster
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMaster();

    /**
     *List of TblConfigurationMaster based on usertype
     * @param usertype
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMasterDetails(String usertype);

     /**
     *List of TblConfigurationMaster based on configId
     * @param configId
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMasterDetails(int configId);

     /**
     *Updates TblConfigurationMaster on floginatmpt & pkireq
     * @param floginatmpt
     * @param pkireq
     * @return Values Updated or Not Updated for success or fail
     */
     public String updateConfigMaster(int failLogin, String pkiReq);

    /**
     *Updates configuration master
     * @param fileType
     * @param userType
     * @param singleFile
     * @param totalFile
     * @return Updated or null for success or fail
     */
     public String updateConfigMaster(String fileType,String userType,byte singleFile,byte totalFile);
    
     /**
     *List of all Configuration Master
     * @return List of TblConfigurationMaster
     */
     public List<TblConfigurationMaster> getAllConfigMaster();

    /**
     *Get all UserTypes
     * @return List of TblUserTypeMaster
     */
     public List<Object> getAllUserType();

    /**
     *Inserts configuration
     * @param fileType
     * @param userType
     * @param singleFile
     * @param totalFile
     * @return ok or Error for success or fail
     */
    public String insertConfigMaster(String fileType, String userType,byte singleFile,byte totalFile);

    /**
     *Get all Configuration User Type
     * @return List of ConfigUserType
     */
    public List<Object> getConfigUserType();


    public void setLogUserId(String logUserId);

    public void setAuditTrail(AuditTrail auditTrail);
}
