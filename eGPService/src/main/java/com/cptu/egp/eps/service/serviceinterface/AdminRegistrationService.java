/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblAdminMaster;
//Edited by palash for PE Admin Transfer, Dohatec
import com.cptu.egp.eps.model.table.TblAdminTransfer;
//End
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.view.VwGetAdmin;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface AdminRegistrationService {

    /**
     * Inserting values in tbl_AdminMaster and tbl_OfficeAdmin
     * @param tblAdminMaster
     * @param tblLoginMaster
     * @param str
     * @return : integer 0 in case PE admin not created else userId
     * //Edited by palash for PE Admin Transfer, Dohatec
     */
    public Integer registerPEAdmin(TblAdminMaster tblAdminMaster,TblAdminTransfer tblAdminTransfer,TblLoginMaster tblLoginMaster,String[] str);

    
    /*
     * To Register Organization Admin
     * @param : Object AdminMasterDtBean,Object LoginMasterDtBean
     * @return : integer 0 in case org admin not created else userId
     */
    /**
     * 
     * @param tblAdminMaster
     * @param tblLoginMaster
     * @param officeId
     * @return integer 0 in case org admin not created else userId
     * @throws Exception
     */
    public Integer registerOrganizationAdmin(TblAdminMaster tblAdminMaster,TblLoginMaster tblLoginMaster,int officeId)throws Exception;

    /**
     * Register Content and Procurement Admin
     * @param tblAdminMaster
     * @param tblLoginMaster
     * @return userId if register successfully
     */
    public Integer registerContentAdmin(TblAdminMaster tblAdminMaster,TblLoginMaster tblLoginMaster,String... auditDetails);

    /**
     * register schedule bank and dev partner admin
     * @param tblLoginMaster
     * @param tblPartnerAdmins
     * @return integer 0 in case schedule bank and dev partner admin not created else userId
     */
    public Integer registerScBankDevPartnerAdmin(TblLoginMaster tblLoginMaster,TblPartnerAdmin tblPartnerAdmins);

    /**
     * Fetching information from Tbl_DepartmentMaster
     * @param values where condition
     * @return list of TblDepartmentMaster
     * @throws Exception
     */
    public List<TblDepartmentMaster> findTblOrgMaster(Object... values) throws Exception;

    /**
     * Fetching information from VwGetAdmin
     * @param firstResult - data start from
     * @param maxResult - how much records
     * @param values - where condition
     * @return List of VwGetAdmin
     * @throws Exception
     */
    public List<VwGetAdmin> findVwOrgMaster(int firstResult, int maxResult,Object... values) throws Exception;

    /**
     * Search tbl_office master
     * @param values - where condition
     * @return List of TblOfficeMaster
     * @throws Exception
     */
    public List<TblOfficeMaster> findTblOfficeMaster(Object... values) throws Exception;

    /**
     * Get bank details 
     * @param values - where condition
     * @return TblScBankDevPartnerMaster
     * @throws Exception
     */
    public List<TblScBankDevPartnerMaster> findTblBankMaster(Object... values) throws Exception;

    /**
     * get count of department
     * @param from
     * @param where condition
     * @return float counts of records
     * @throws Exception
     */
    public float getCntDepartmentMaster(String from,String where) throws Exception;

    /**
     * Get admin office using department Id
     * @param deptId
     * @return List of Object [0]officeId, [1]officeName
     * @throws Exception
     */
    public List<Object[]> getAdminOfficeList(int deptId) throws Exception;

    
     public List<Object[]> getOfficeList(int deptId)  throws Exception;
    /**
     * Fetching info from TblDepartmentMaster for Approving Authority
     * @param userId
     * @return department id
     * @throws Exception
     */
    public Short getDepartmentIdByUserId(int userId) throws Exception;

    /**
     * checking unique office id
     * @param officeId
     * @return true if office id exist
     * @throws Exception
     */
    public Boolean chkOfficeAdminCreated(int officeId) throws Exception;

    /**
     * Fetching department name
     * @param userId (approvingAuthorityId of TblDepartmentMaster)
     * @return department Name of particular userid
     * @throws Exception
     */
    public String getDepartmentNameByUserId(int userId) throws Exception;

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    public void setAuditTrail(AuditTrail auditTrail);
}
