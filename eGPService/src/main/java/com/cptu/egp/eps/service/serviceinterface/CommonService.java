/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.UserStatisticsDtBean;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblEmailPrefMaster;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblHintQuestionMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblNationalityMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblSmsPrefMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.model.table.TblAppPermission;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CommonService {

    /**
     * get country listing from Tbl_CountryMaster
     * @return list of countries
     */
    List<TblCountryMaster> countryMasterList() throws Exception;

    List<TblNationalityMaster> nationalityMasterList();

    List<TblHintQuestionMaster> hintQuestionMasterList();

    /**
     * get listing of state from Tbl_StateMaster
     * @return list of states
     */
    public List<TblStateMaster> getState(short countryId);

    /**
     * get listing of ministry
     * @param deptId primary key
     * @return list of TblDepartmentMaster
     */
    List<TblDepartmentMaster> getMinistryList(short deptId);

    /**
     * get listing of division
     * @return list of TblDepartmentMaster
     */
    List<TblDepartmentMaster> getDivisionDefaultList();

    List<TblDepartmentMaster> getDivisionList(short deptId);

    List<TblDepartmentMaster> getOrganization(short deptId);

    /**
     * Fetching Email (checking unique Email Id)
     * @param mailId
     * @return message about existence of unique Email Id
     */
    public String verifyMail(String mailId);

    public String checkForMail(String mailId);

    public String verifyDeptName(String deptName, String deptType);

    public String verifyOfficeName(String officeName, int deptId);
    
    public String verifyOffice(int officeId);
    
    public String verifyDebar(int userId);

    public String verifyNationalId(String nationalId);

    public String verifyComposeMail(String mailId);

    public String getUserName(int userId, int userTypeId);

    public long getCount(String from, String where) throws Exception;

    public void sendMsg(String toEmailId, String subject, String msgText) throws Exception;

    public List<TblStateMaster> getStateByCntName(String countryName);
    
    public List<TblDepartmentMaster> getSubdistrictByStateId(String stateName);

    public List<TblStateMaster> getareaCode(String stateName);

	public List<TblDepartmentMaster> getSubdistrictByStateId2(String stateName);
    
    public List<TblDepartmentMaster> getSubdistrictByStateId4(String stateName);
    public String countryCode(String value, boolean isCntId);

    public String verifyMobileNo(String mobileNo);

    public String verifyUserEmployeeId (String userEmployeeId);

    public UserStatisticsDtBean getUserStatisticsDtBean();

    /**
     * Fetching info from Tbl_CompanyMaster,Tbl_TendererMaster
     * @param userId from tbl_LoginMaster
     * @return Consultant name
     */
    public String getConsultantName(String userId);

    public String getRegType(String userId);

    public Object getProcNature(String tenderId);

    public Object getProcMethod(String tenderId);

    public Object getEventType(String tenderId);

    public String tenderPaidORFree(String tenderId, String lotId);

    public void setUserId(String userId);

    public String getUserId(String email);

    public String getEmailId(String strUserId);

    public String getEmailIdOfDG(String strUserTypeId);

    public Object getDocAvlMethod(String tenderId);

    public List<Object> getLotNo(String tenderId);

    /**
     * Fetching information from Tbl_TenderLotSecurity
     * @param tenderId from tbl_TenderMaster
     * @return list of objects from Tbl_TenderLotSecurity
     */
    public List<Object[]> getLotDetails(String tenderId);

    public List<Object[]> getLotDetailsByPkgLotId(String pkgLotId, String tenderId);

    /**
     * this method is for Fetch the data of lot no and lot Description and lot id from formId
     * @param formId - integer formId
     * @return object[] [0] lotNo, [1] Description, [2] lot lotId
     */
    public List<Object[]> getLotDetiasByFormId(int formId);

    /**
     * this method is for Fetch the data of packageNo No and packageNo Description and lot id from formId
     * @param tenderId - integer tenderId
     * @return object[] [0] pkgNo, [1] pkgDiscription
     */
    public List<Object[]> getPkgDetialByTenderId(int tenderId);

    public List<Object[]> getPkgDetailWithAppPkgId(int tenderId);

    public boolean isUserJVCA(String userId);

    /**
     * this method is for Fetch the data of lot no No and lot Description and lot id from tenderid
     * @param tenderId - String tenderId
     * @return object[] [0] lotNo, [1] lotDiscription, [3] lotId
     */
    public List<Object[]> getLotDetailsByTenderIdforEval(String tenderId);

    /**
     * Fetching information from tbl_TenderLotSecurity
     * @param tenderId from tbl_TenderMaster
     * @param pckLotId from tbl_TenderLotSecurity
     * @return list of objects from tbl_TenderLotSecurity
     */
    public List<Object[]> getLotDetailsByTenderIdAndPkgIdforEval(String tenderId, String pckLotId);

    /**
     * Fetching information from Tbl_CompanyMaster,Tbl_PostQualification,Tbl_TendererMaster
     * @param tenderId from tbl_TenderMaster
     * @param pckLotId from tbl_TenderLotSecurity
     * @return list of objects from Tbl_CompanyMaster,Tbl_PostQualification,Tbl_TendererMaster
     */
    public List<Object[]> getCompanyNameAndEntryDate(String tenderId, String pkgLotId,int evalCount);

    public List<Object[]> getData(String tenderId,int evalCount);

    public int updatedocHash(int companyDocId, String docHash);

    public List<TblLoginMaster> getUserType(int userId);

    /**
     * this method is for Fetch the data from tbl_EvalRoundMaster
     * @param tenderId - int tenderId
     * @return data
     */
    public List<TblEvalRoundMaster> getDataTblEvalRM(int tenderId,int evalCount);

    /**
     * get user types from tbl_UserTypeMaster
     * @return list of user types
     */
    public List<TblUserTypeMaster> getUserTypes();

    public String getAppIdByTenderId(String tenderId);

    public String getFormName(String formId);

    public String getServiceTypeForTender(int tenderId);

    public String verifyMailforPaymentGateway(String mailId);

    public boolean getNoaStatus(int tenderId);

    public String getEmailAlertPreference(String strUserId);
    public String getSmsAlertPreference(String strUserId);

    public String getUserTypeId(String strUserId);

    /**
     * get user email alerts  from tbl_EmailPrefMater
     *  
     */
    public List<TblEmailPrefMaster> getEmailPrefMaster(String Epid);
    public List<TblSmsPrefMaster> getSmsPrefMaster(String Epid);
    public List<Object> getEmailPreferencesByUseTypeId(int userTypeId);
    public List<Object> getSmsPreferencesByUseTypeId(int userTypeId);
    public String getUserTypeIdByMobileNo(String mobileno);

     /*public String getMinistryIdByName();*/

    /* Dohatec Start */
    public Object getProcurementType(String tenderId);
    public Date getServerDateTime() throws ParseException;
     /* Dohatec End */
    public  List<Object[]> checkPD(String tenderId);

    public List<Object[]> getProjectOffice(String userId);

    public Object checkOrgType(int deptId);

    //nafiul
    public List<TblActivityMaster> getActivityMasterData();
    
    /**
     *
     * @param values
     * @return
     */
    public List<TblUserTypeMaster> getUserTypeByUserTypeId(Object... values);
    public String getUserRoleByUserId(int userId);
    public String getBankUserRoleByUserId(int userId);
    public String getCompanyIdByUserId(int userId);
    public int updateTenderMaster(int createdBy, int tenderId);
    public int getOriginalUid(int tenderId);
    
    public List<TblAppPermission> getPermissionByTenderId(String tenderId);
}
