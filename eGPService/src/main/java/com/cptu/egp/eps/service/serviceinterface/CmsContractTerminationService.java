/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsContractTerminationService {

    /**
     * for Setting userId at Service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * add Contract Termination Details To the database
     * @param TblCmsContractTermination
     * @return integer, return 0 - if not submitted otherwise anything else count
     */
    public int insertCmsContractTermination(TblCmsContractTermination TblCmsContractTermination);

    /**
     * update Contract Termination Details To the database
     * @param TblCmsContractTermination
     * @return boolean, true - if updated successfully otherwise false
     */
    public boolean updateCmsContractTermination(TblCmsContractTermination TblCmsContractTermination);

    /**
     * delete the Contract termination details from the database
     * @param TblCmsContractTermination
     * @return boolean, true - if deleted successfully otherwise false
     */
    public boolean deleteCmsContractTermination(TblCmsContractTermination TblCmsContractTermination);

    /**
     * gives Contract Termination details data from database
     * @return list of data
     */
    public List<TblCmsContractTermination> getAllCmsContractTermination();

    /**
     * gives Contract Termination details data Count
     * @return long, return 0 - if no record exist otherwise anything else count
     */
    public long getCmsContractTerminationCount();

    /**
     * gives Contract Termination details data
     * @param id
     * @return Contract Termination table objects
     */
    public TblCmsContractTermination getCmsContractTermination(int id);

    /**
     * gives Contract Termination details data by passing  ContractSign Id
     * @param contractSignId
     * @return Contract Termination table objects
     */
    public TblCmsContractTermination getCmsContractTerminationForContractSignId(int contractSignId);

    /**
     * this method is for changing the Contract Termination status to database
     * @param contractSignId
     * @param columnType
     * @param status
     * @return Contract Termination table objects
     */
    public TblCmsContractTermination changeCTStatus(int contractSignId, String columnType, String status);

    /**
     * gives lotId by passing Contract Termination Id
     * @param ContractTerminationId
     * @return list Objects
     */
    public List<Object> getLotID(int ContractTerminationId);

    /**
     * gives workflow level involved userID
     * @param childId
     * @param objectId
     * @param userId
     * @return list of objects
     */
    public List<Object[]> getWFUserEmailId(int childId,int objectId,int userId);

    /**
     * gives Contract Termination details data
     * @param lotId
     * @return list object
     */
    public List<Object> getContractTerminationDetails(int lotId);

    /**
     * it checks the duplicate entry for Contract Termination details to the database
     * @param contractId
     * @return long, return 0 - if no duplicate entry exist otherwise anything else count
     */
    public long chkDuplicateEntry(String contractId);

    /**
     * gives no of Contract Termination Details history
     * @param ContractId
     * @return list of objects
     */
    public List<TblCmsContractTermination> getCTHistory(int ContractId);
}
