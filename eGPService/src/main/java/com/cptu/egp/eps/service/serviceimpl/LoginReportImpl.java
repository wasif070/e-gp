/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.LoginReportDtBean;
import com.cptu.egp.eps.dao.storedprocedure.SPLoginReport;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class LoginReportImpl {

    final Logger logger = Logger.getLogger(LoginReportImpl.class);
    private String logUserId ="0";

    MakeAuditTrailService makeAuditTrailService;
    SPLoginReport spLoginReport;
    private AuditTrail auditTrail;

    public SPLoginReport getSpLoginReport() {
        return spLoginReport;
    }

    public void setSpLoginReport(SPLoginReport spLoginReport) {
        this.spLoginReport = spLoginReport;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    
    /**
     * Get Login Report of eGP Portal
     * @param typeOfUser userType Id
     * @param searchForDate Registered Date
     * @param dateFrom Search user from Registered Date
     * @param dateTo Search user to Registered Date
     * @param emailId from tbl_LoginMaster
     * @param userId from tbl_LoginMaster
     * @param userTypeId from tbl_LoginMaster
     * @param pageNo page number
     * @param totalPages total pages
     * @param ipaddress IPAddress
     * @return List of LoginReportDtBean
     */
    public List<LoginReportDtBean> loginReportDetails(String typeOfUser, String searchForDate, String dateFrom, String dateTo, String emailId, Integer userId, Integer userTypeId, Integer pageNo, Integer totalPages,String ipaddress) {
        logger.debug("loginReportDetails : "+logUserId+" Starts");
        List<LoginReportDtBean> details = new ArrayList<LoginReportDtBean>();
        String action = "Generate Login Report";
        try{
            details = spLoginReport.executeProcedure(typeOfUser, searchForDate, dateFrom, dateTo, emailId, userId, userTypeId, pageNo, totalPages,ipaddress);
        }catch(Exception ex){
            action = "Error in "+action+" "+ex.getMessage();
            logger.error("loginReportDetails : "+logUserId+" : "+ex.toString());
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("loginReportDetails : "+logUserId+" Ends");
        return details;
    }
}
