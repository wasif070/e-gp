/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBidderLotsDao;
import com.cptu.egp.eps.dao.daointerface.TblBidderLotsHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCancelTenderRequestDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigPreTenderDao;
import com.cptu.egp.eps.dao.daointerface.TblCorrigendumDocsDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalServiceWeightageDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaAcceptanceDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementMethodDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderStdDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderWatchListDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPAddReTenderNotice;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUpdTendernotice;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPgetTenderInfo;
import com.cptu.egp.eps.dao.storedprocedure.SPgetTendererDashboardInfo;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPData;
import com.cptu.egp.eps.dao.storedprocedure.SPEvalCommonSearchData;
import com.cptu.egp.eps.model.table.TblBidderLots;
import com.cptu.egp.eps.model.table.TblBidderLotsHistory;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import com.cptu.egp.eps.model.table.TblCorrigendumDocs;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderStd;
import com.cptu.egp.eps.model.table.TblTenderWatchList;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class TenderServiceImpl implements TenderService {

    static final Logger logger = Logger.getLogger(TenderServiceImpl.class);
    private String logUserId = "0";
    SPAddUpdTendernotice spAddUpdTendernotice;
    SPgetTenderInfo spgetTenderInfo;
    SPgetTendererDashboardInfo spgetTendererDashboardInfo;
    SPEvalCommonSearchData spEvalCommonSearchData;
    TblProcurementMethodDao tblProcurementMethodDao;
    TblTenderWatchListDao tblTenderWatchListDao;
    HibernateQueryDao hibernateQueryDao;
    TblConfigPreTenderDao tblConfigPreTenderDao;
    TblCorrigendumDocsDao tblCorrigendumDocsDao;
    TblTenderStdDao tblTenderStdDao;
    TblTenderDetailsDao tblTenderDetailsDao;
    TblTenderMasterDao tblTenderMasterDao;
    SPAddReTenderNotice sPAddReTenderNotice;
    TblTenderLotSecurityDao tblTenderLotSecurityDao;
    TblBidderLotsDao tblBidderLotsDao;
    TblBidderLotsHistoryDao tblBidderLotsHistoryDao;
    TblCancelTenderRequestDao tblCancelTenderRequestDao;
    TblNoaAcceptanceDao tblNoaAcceptanceDao;
    SpGeteGPData spGeteGPData;
    TblEvalServiceWeightageDao tblEvalServiceWeightageDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public SpGeteGPData getSpGeteGPData() {
        return spGeteGPData;
    }

    public void setSpGeteGPData(SpGeteGPData spGeteGPData) {
        this.spGeteGPData = spGeteGPData;
    }

    public TblCancelTenderRequestDao getTblCancelTenderRequestDao() {
        return tblCancelTenderRequestDao;
    }

    public void setTblCancelTenderRequestDao(TblCancelTenderRequestDao tblCancelTenderRequestDao) {
        this.tblCancelTenderRequestDao = tblCancelTenderRequestDao;
    }

    public TblBidderLotsHistoryDao getTblBidderLotsHistoryDao() {
        return tblBidderLotsHistoryDao;
    }

    public void setTblBidderLotsHistoryDao(TblBidderLotsHistoryDao tblBidderLotsHistoryDao) {
        this.tblBidderLotsHistoryDao = tblBidderLotsHistoryDao;
    }

    public TblBidderLotsDao getTblBidderLotsDao() {
        return tblBidderLotsDao;
    }

    public void setTblBidderLotsDao(TblBidderLotsDao tblBidderLotsDao) {
        this.tblBidderLotsDao = tblBidderLotsDao;
    }

    public TblTenderLotSecurityDao getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    public void setTblTenderLotSecurityDao(TblTenderLotSecurityDao tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }

    public SPAddReTenderNotice getsPAddReTenderNotice() {
        return sPAddReTenderNotice;
    }

    public void setsPAddReTenderNotice(SPAddReTenderNotice sPAddReTenderNotice) {
        this.sPAddReTenderNotice = sPAddReTenderNotice;
    }

    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    public TblTenderStdDao getTblTenderStdDao() {
        return tblTenderStdDao;
    }

    public void setTblTenderStdDao(TblTenderStdDao tblTenderStdDao) {
        this.tblTenderStdDao = tblTenderStdDao;
    }

    public TblCorrigendumDocsDao getTblCorrigendumDocsDao() {
        return tblCorrigendumDocsDao;
    }

    public void setTblCorrigendumDocsDao(TblCorrigendumDocsDao tblCorrigendumDocsDao) {
        this.tblCorrigendumDocsDao = tblCorrigendumDocsDao;
    }

    public TblConfigPreTenderDao getTblConfigPreTenderDao() {
        return tblConfigPreTenderDao;
    }

    public void setTblConfigPreTenderDao(TblConfigPreTenderDao tblConfigPreTenderDao) {
        this.tblConfigPreTenderDao = tblConfigPreTenderDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTenderWatchListDao getTblTenderWatchListDao() {
        return tblTenderWatchListDao;
    }

    public void setTblTenderWatchListDao(TblTenderWatchListDao tblTenderWatchListDao) {
        this.tblTenderWatchListDao = tblTenderWatchListDao;
    }

    public TblProcurementMethodDao getTblProcurementMethodDao() {
        return tblProcurementMethodDao;
    }

    public void setTblProcurementMethodDao(TblProcurementMethodDao tblProcurementMethodDao) {
        this.tblProcurementMethodDao = tblProcurementMethodDao;
    }

    public SPgetTenderInfo getSpgetTenderInfo() {
        return spgetTenderInfo;
    }

    public void setSpgetTenderInfo(SPgetTenderInfo spgetTenderInfo) {
        this.spgetTenderInfo = spgetTenderInfo;
    }

    public SPAddUpdTendernotice getSpAddUpdTendernotice() {
        return spAddUpdTendernotice;
    }

    public void setSpAddUpdTendernotice(SPAddUpdTendernotice spAddUpdTendernotice) {
        this.spAddUpdTendernotice = spAddUpdTendernotice;
    }

    public TblEvalServiceWeightageDao getTblEvalServiceWeightageDao() {
        return tblEvalServiceWeightageDao;
    }

    public void setTblEvalServiceWeightageDao(TblEvalServiceWeightageDao tblEvalServiceWeightageDao) {
        this.tblEvalServiceWeightageDao = tblEvalServiceWeightageDao;
    }

    public TblTenderMasterDao getTblTenderMasterDao() {
        return tblTenderMasterDao;
    }

    public void setTblTenderMasterDao(TblTenderMasterDao tblTenderMasterDao) {
        this.tblTenderMasterDao = tblTenderMasterDao;
    }

    public TblNoaAcceptanceDao getTblNoaAcceptanceDao() {
        return tblNoaAcceptanceDao;
    }

    public void setTblNoaAcceptanceDao(TblNoaAcceptanceDao tblNoaAcceptanceDao) {
        this.tblNoaAcceptanceDao = tblNoaAcceptanceDao;
    }

     /**
     * @return the spEvalCommonSearchData
     */
    public SPEvalCommonSearchData getSpEvalCommonSearchData() {
        return spEvalCommonSearchData;
    }

    /**
     * @param spEvalCommonSearchData the spEvalCommonSearchData to set
     */
    public void setSpEvalCommonSearchData(SPEvalCommonSearchData spEvalCommonSearchData) {
        this.spEvalCommonSearchData = spEvalCommonSearchData;
    }

    @Override   // Two Param 1. pkgDocFeesVcUSD and 2. TenderSecurityAmt_M_USD are added for ICT - Dohatec
    public List<CommonSPReturn> insertSPAddYpTEndernoticeBySP(String bidCategory, String W1, String W2, String W3, String W4, int AppId_inInt, int PackageId_inInt, String TenderType_inVc, String CreatedBy_inVc, String ReoiRfpRefNo_Vc, Date DocEndDate_sDt, Date PreBidStartDt_sDt, Date PreBidEndDt_sDt, Date SubmissionDt_sDt, Date OpeningDt_sDt, String EligibilityCriteria_Vc, String TenderBrief_Vc, String Deliverables_Vc, String OtherDetails_Vc, String ForeignFirm_Vc, String DocAvlMethod_Vc, String EvalType_Vc, String DocFeesMethod_Vc, String DocFeesMode_Vc, String DocOfficeAdd_Vc, Date SecurityLastDt_Dt, String SecuritySubOff_Vc, String Location_Vc, String DocFess_M, String TenderSecurityAmt_M, String TenderSecurityAmt_M_USD, String CompletionTime, String StartTime, String Action_inVc, int Tenderid_inInt, int Corrid_inInt, String PhasingRefNo_inVc, String PhasingOfService_inVc, String PhasingLocation_inVc, String IndStartDt_inVc, String IndEndDt_inVc, String ESignature_inVc, String DigitalSignature_inVc, String TenderLotSecId_inVc, String ContractTypeNew_Vc, Date tenderPubDt_Vc, String PkgDocFees_Vc, String pkgDocFeesVcUSD, BigDecimal EstCost_inM, Integer passingMarks, String ProcurementMethod, String bidSecurityType,String eventType, String... auditInfo) {
        String action = "";
        List<CommonSPReturn> commonSPReturns = new ArrayList<CommonSPReturn>();
        try {
            if(auditInfo != null && auditInfo.length != 0){
                action = auditInfo[0];
            }
            commonSPReturns = spAddUpdTendernotice.executeProcedure(bidCategory, W1, W2, W3, W4, AppId_inInt, PackageId_inInt, TenderType_inVc, CreatedBy_inVc, ReoiRfpRefNo_Vc, (Date) DocEndDate_sDt, (Date) PreBidStartDt_sDt, (Date) PreBidEndDt_sDt, (Date) SubmissionDt_sDt, (Date) OpeningDt_sDt, EligibilityCriteria_Vc, TenderBrief_Vc, Deliverables_Vc, OtherDetails_Vc, ForeignFirm_Vc, DocAvlMethod_Vc, EvalType_Vc, DocFeesMethod_Vc, DocFeesMode_Vc, DocOfficeAdd_Vc, (Date) SecurityLastDt_Dt, SecuritySubOff_Vc, Location_Vc, DocFess_M, TenderSecurityAmt_M, TenderSecurityAmt_M_USD, CompletionTime, StartTime, Action_inVc, Tenderid_inInt, Corrid_inInt, PhasingRefNo_inVc, PhasingOfService_inVc, PhasingLocation_inVc, IndStartDt_inVc, IndEndDt_inVc, ESignature_inVc, DigitalSignature_inVc, TenderLotSecId_inVc, ContractTypeNew_Vc, (Date) tenderPubDt_Vc, PkgDocFees_Vc, pkgDocFeesVcUSD, EstCost_inM, passingMarks,ProcurementMethod,bidSecurityType,eventType);
        } catch (Exception e) {
            logger.error("insertSPAddYpTEndernoticeBySP : " + logUserId + e);
            action = "Error in " + action;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Tenderid_inInt, "tenderId", EgpModule.Tender_Notice.getName(), action, "");
            action = null;
        }
        return commonSPReturns;
    }
    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public List<CommonTenderDetails> gettenderinformationSP(String action, String viewType, int tenderid, Integer appid, Integer userid, Integer procurementNatureId, String procurementType, Integer procurementMethod, String reoiRfpRefNo, String tenderPubDtFrom, String tenderPubDtTo, String status, String ministry, String division, String agency, String peOfficeName, String submissionDtFrom, String submissionDtTo, int page, int recordPerPage,String district,String thana) {
        return spgetTenderInfo.executeProcedure(action, viewType, tenderid, appid, userid, procurementNatureId, procurementType, procurementMethod, reoiRfpRefNo, tenderPubDtFrom, tenderPubDtTo, status, ministry, division, agency, peOfficeName, submissionDtFrom, submissionDtTo, page, recordPerPage,district,thana);
    }

    @Override
    public List<EvalCommonSearchData> gettenderDashboardinformationSP(String action, String viewType, Integer userid,String strPageNo, String strOffset, String procNature, String procMethod, String procType, String tenderId, String tenderPubFrom, String tenderPubTo) {
        return getSpEvalCommonSearchData().executeProcedure(action, viewType, String.valueOf(userid),strPageNo,strOffset,  procNature,  procMethod,  procType,  tenderId,tenderPubFrom,tenderPubTo,"","","","");

        //return spEvalCommonSearchData.executeProcedure(action, viewType, String.valueOf(userid),"","","","","","","","","","","","");
    }

    @Override
    public List<TblProcurementMethod> getAllProcurementMethod() {
        return tblProcurementMethodDao.getAllTblProcurementMethod();
    }

    @Override
    public int checkTenderWatchList(int userid, int tenderId) {
        logger.debug("checkTenderWatchList : " + logUserId + " Starts");
        int iRetVal = 0;
        try {
            Object[] values = {"userId", Operation_enum.EQ, userid, "tenderId", Operation_enum.EQ, tenderId};
            List<TblTenderWatchList> tenderWatchLists = tblTenderWatchListDao.findTblTenderWatchList(values);
            if (tenderWatchLists != null && !tenderWatchLists.isEmpty()) {
                iRetVal = tenderWatchLists.get(0).getWatchListId();
        }
        } catch (Exception ex) {
            logger.error("checkTenderWatchList : " + logUserId + ex);
            iRetVal = 0;
        }
        logger.debug("checkTenderWatchList : " + logUserId + " Ends");
        return iRetVal;
    }

    @Override
    public int addToWatchList(TblTenderWatchList tblTenderWatchList) {
        logger.debug("addToWatchList : " + logUserId + " Starts");
        int iRetVal = 0;
        String action = "Tender Added to Watchlist";
        try {
            tblTenderWatchListDao.addTblTenderWatchList(tblTenderWatchList);
            iRetVal = tblTenderWatchList.getWatchListId();
        } catch (Exception ex) {
            logger.error("addToWatchList : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderWatchList.getUserId(), "userId", EgpModule.WATCH_LIST.getName(), action, "Tender ID = "+tblTenderWatchList.getTenderId());
            action = null;
        }
        logger.debug("addToWatchList : " + logUserId + " Ends");
        return iRetVal;
        }

    @Override
    public boolean removeFromWatchList(int watchListId) {
        logger.debug("removeFromWatchList : " + logUserId + " Starts");
        boolean bSuccess = false;
        String action = "Tender Removed from Watchlist";
        TblTenderWatchList tblTenderWatchList = new TblTenderWatchList();
        try {
            if(tblTenderWatchListDao.findTblTenderWatchList("watchListId",Operation_enum.EQ,watchListId)!=null && !tblTenderWatchListDao.findTblTenderWatchList("watchListId",Operation_enum.EQ,watchListId).isEmpty()){
                tblTenderWatchList = tblTenderWatchListDao.findTblTenderWatchList("watchListId",Operation_enum.EQ,watchListId).get(0);
            }
            String query = "Delete from TblTenderWatchList watchLst where watchLst.watchListId=" + watchListId;
            int cnt = hibernateQueryDao.updateDeleteNewQuery(query);
            if (cnt > 0) {
                bSuccess = true;
        }
        } catch (Exception ex) {
            logger.error("removeFromWatchList : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderWatchList.getUserId(), "userId", EgpModule.WATCH_LIST.getName(), action, "Tender ID = "+tblTenderWatchList.getTenderId());
            action = null;
            tblTenderWatchList = null;
        }
        logger.debug("removeFromWatchList : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public boolean updateStatus(int tenderId, Date tenderValidityDt, Date tenderSecurityDt,String docAccess,String updatePeName) {
        logger.debug("updateStatus : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            //String query="Delete from TblTenderWatchList watchLst where watchLst.watchListId="+watchListId;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd");
            String tenderValidityDtString =  dateFormat.format(tenderValidityDt);
            System.out.println("In tender Service Impl tenderValidityDt------------------>"+tenderValidityDt);
            String tenderSecurityDtString =  dateFormat.format(tenderSecurityDt);
            // removed pubdate - String query = "update TblTenderDetails set tenderStatus = 'Approved',tenderPubDt = current_timestamp(),tenderValidityDt='" + tenderValidityDtString + "',tenderSecurityDt='" + tenderSecurityDtString + "',docAccess='" + docAccess + "' where tblTenderMaster.tenderId = " + tenderId;
           
            String query = "update TblTenderDetails set tenderStatus = 'Approved',tenderValidityDt='" + tenderValidityDtString + "',tenderSecurityDt='" + tenderSecurityDtString + "',docAccess='" + docAccess + "',peName='"+ updatePeName + "' where tblTenderMaster.tenderId = " + tenderId;
            //String query="update TblTenderDetails set tenderStatus = 'Approved',tenderPubDt = current_timestamp(),tenderValidityDt="+ tenderValidityDt + ",tenderSecurityDt=" + tenderSecurityDt +" where tblTenderMaster.tenderId = "+tenderId;
            int cnt = hibernateQueryDao.updateDeleteNewQuery(query);
            if (cnt > 0) {
                bSuccess = true;
        }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("updateStatus : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("updateStatus : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public List<TblCorrigendumDocs> getTenderLotCorriDoc(int tenderid, int corriId, int lotId) {
        logger.debug("getTenderLotCorriDoc : " + logUserId + " Starts");
        List<TblCorrigendumDocs> tenderWatchLists = null;
        try {
            Object[] values = {"corrigendumId", Operation_enum.EQ, corriId, "pkgLotId", Operation_enum.EQ, lotId};
            tenderWatchLists = tblCorrigendumDocsDao.findTblCorrigendumDocs(values);
        } catch (Exception ex) {
            logger.error("getTenderLotCorriDoc : " + logUserId + ex);
        }
        logger.debug("getTenderLotCorriDoc : " + logUserId + " Ends");
        return tenderWatchLists;
    }

    @Override
    public boolean chkTenderLotPaid(String paidFor, int tenderId, int pkgLotId, int userId) {
        logger.debug("chkTenderLotPaid : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            boolean isTenderFree = false;
            List<Object[]> tenderDetail = hibernateQueryDao.createNewQuery("select td.docAvlMethod,td.pkgDocFees from TblTenderDetails td where td.tblTenderMaster.tenderId="+tenderId);
            List<Object> lotSecurity = hibernateQueryDao.singleColQuery("select tl.docFess from TblTenderLotSecurity tl where tl.tblTenderMaster.tenderId="+tenderId);
            if(tenderDetail!=null && !tenderDetail.isEmpty()){
                if(tenderDetail.get(0)[0].toString().equals("Package")){
                   isTenderFree =  ((BigDecimal) tenderDetail.get(0)[1]).compareTo(BigDecimal.ZERO) == 0;
                }else{
                    if(lotSecurity!=null && !lotSecurity.isEmpty()){
                        isTenderFree =  ((BigDecimal) lotSecurity.get(0)).compareTo(BigDecimal.ZERO) == 0;
                    }
                }
            }
            if(!isTenderFree){
                    String hqlQuery = "select tp.status from TblTenderPayment tp where tp.paymentFor like '" + paidFor + "' and tp.tblTenderMaster.tenderId = " + tenderId + " and tp.pkgLotId = " + pkgLotId + " and tp.userId = " + userId;
                    String paidStatus = "";
                    List<Object> listObj = hibernateQueryDao.getSingleColQuery(hqlQuery);
                    if (listObj.size() > 0) {
                        paidStatus = (String) listObj.get(0);
                        if ("paid".equalsIgnoreCase(paidStatus)) {
                            bSuccess = true;
                        }
                    } else {
                        bSuccess = false;
                }
            }else{
                bSuccess = true;
            }
        } catch (Exception e) {
            logger.error("chkTenderLotPaid : " + logUserId + e);
            bSuccess = false;
        }
        logger.debug("chkTenderLotPaid : " + logUserId + " Ends");
        return bSuccess;
        //throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public boolean isSTDDump(int tenderId) {
        logger.debug("isSTDDump : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            int flag = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderStd ts where ts.tenderId=" + tenderId);
            if (flag != 0) {
                bSuccess = true;
            }
        } catch (Exception ex) {
            logger.error("isSTDDump : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("isSTDDump : " + logUserId + " Ends");
        return bSuccess;
    }
    @Override
    public boolean deleteTenderGrandSum(int tenderId) {
        logger.debug("deleteTenderGrandSum : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            int flag = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderGrandSum gs where gs.tblTenderMaster.tenderId=" + tenderId);
            if (flag != 0) {
                bSuccess = true;
            }
        } catch (Exception ex) {
            logger.error("deleteTenderGrandSum : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("deleteTenderGrandSum : " + logUserId + " Ends");
        return bSuccess;
    }
    @Override
    public boolean chkDocMaped(int tenderId) {
        logger.debug("chkDocMaped : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            List<TblTenderStd> size = tblTenderStdDao.findTblTenderStd("tenderId", Operation_enum.EQ, tenderId);
            int length = size.size();
            if (length > 0) {
                bSuccess = true;
            }
        } catch (Exception ex) {
            logger.error("chkDocMaped : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("chkDocMaped : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public List<TblTenderDetails> getTenderDetails(int tenderId){
        logger.debug("getTenderDetails : " + logUserId + " Starts");
        List<TblTenderDetails> list = new ArrayList<TblTenderDetails>();

        try{
            list = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster.tenderId", Operation_enum.EQ, tenderId);
        }catch(Exception ex){
            logger.error("getTenderDetails : " + logUserId + ex);
        }
        logger.debug("getTenderDetails : " + logUserId + " Ends");
        return list;
    }

    @Override
    public CommonMsgChk createTenderFromPQ(int tenderId, String action, String eventType,String userIds,String strLoginUser) {
        return sPAddReTenderNotice.executeProcedure(tenderId, action, eventType,userIds,strLoginUser);
    }

    @Override
    public String getEventType(String tenderId) {
        logger.debug("getEventType : " + logUserId + " Starts");
        String strEventType = null;
        List<Object> list = hibernateQueryDao.getSingleColQuery("select ttd.eventType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId);
        if (!list.isEmpty()) {
            strEventType = list.get(0).toString();
       }
        logger.debug("getEventType : " + logUserId + " Ends");
        return strEventType;
    }

    @Override
    public List<TblTenderLotSecurity> isMultipleLot(int tenderId) {
        logger.debug("isMultipleLot : " + logUserId + " Starts");
        List<TblTenderLotSecurity> list = null;
        try {
            list = tblTenderLotSecurityDao.findTblTenderLotSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception ex) {
            logger.error("isMultipleLot : " + logUserId + ex);
            list = null;
        }
        logger.debug("isMultipleLot : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean insertAll(List<TblBidderLots> list) {
        logger.debug("insertAll : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
                tblBidderLotsDao.updateInsertAllTblBidderLots(list);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("insertAll : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("insertAll : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public List<TblBidderLots> viewBidderLots(int tenderId, int userId) {
        logger.debug("viewBidderLots : " + logUserId + " Starts");
        List<TblBidderLots> list = null;
        try {
            list = tblBidderLotsDao.findTblBidderLots("userId", Operation_enum.EQ, userId, "tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception ex) {
            logger.error("viewBidderLots : " + logUserId + ex);
            list = null;
        }
        logger.debug("viewBidderLots : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean forUpdateTblBidderLot(List<TblBidderLots> list, int tenderId, int userId) {
        logger.debug("forUpdateTblBidderLot : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            List<Object[]> listObj = hibernateQueryDao.createNewQuery("select pkgLotId,insdate,bidderLotId from TblBidderLots where userId=" + userId + "and tenderId=" + tenderId);
        List<TblBidderLotsHistory> listHistory = new ArrayList<TblBidderLotsHistory>();
        StringBuilder id = new StringBuilder();
            for (Object[] objects : listObj) {
                id.append(objects[2] + ",");
                listHistory.add(new TblBidderLotsHistory(0, tenderId, Integer.parseInt(objects[0].toString()), userId, new Date(), (Date) objects[1]));
                /*TblBidderLots tblBidderLots = new TblBidderLots();
                tblBidderLots.setBidderLotId(Integer.parseInt(objects[2].toString()));
                tblBidderLots.setInsdate((Date)objects[1]);
                tblBidderLots.setPkgLotId(Integer.parseInt(objects[0].toString()));
                tblBidderLots.setUserId(userId);
                tblBidderLots.setTenderId(tenderId);
                tblBidderLotsDao.deleteTblBidderLots(tblBidderLots);*/
            }

            tblBidderLotsHistoryDao.updateInsertAllTblBidderLotsHistory(listHistory);
            hibernateQueryDao.updateDeleteNewQuery("delete from TblBidderLots where bidderLotId in (" + id.substring(0, id.length() - 1) + ")");
            tblBidderLotsDao.updateInsertAllTblBidderLots(list);
            List<SPCommonSearchDataMore> dataMore = spGeteGPData.executeProcedure("delUnselectedLotBid", String.valueOf(tenderId), String.valueOf(userId), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            if(dataMore!=null && (!dataMore.isEmpty()) && dataMore.get(0).getFieldName1().equals("1")){
               bSuccess = true;
            }else if(dataMore!=null && (!dataMore.isEmpty()) && dataMore.get(0).getFieldName1().equals("0")){
               logger.error("Error in delUnselectedLotBid(egpData) : " + logUserId + dataMore.get(0).getFieldName2());
            }
        } catch (Exception ex) {
            logger.error("forUpdateTblBidderLot : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("forUpdateTblBidderLot : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<Object[]> getConfiForTender(int tenderId) {
        logger.debug("getConfiForTender : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tcd.isDocFeesReq,tcd.isPerSecFeesReq,tcd.isTenderSecReq,tcd.isTenderValReq from TblTenderDetails ttd,TblConfigDocFees tcd,TblTenderTypes ttt,TblProcurementTypes tpt where ttd.eventType = ttt.tenderType and tcd.tenderTypeId = ttt.tenderTypeId and ttd.procurementType=tpt.procurementType and tpt.procurementTypeId=tcd.procurementTypeId and ttd.procurementMethodId = tcd.procurementMethodId and ttd.tblTenderMaster.tenderId=" + tenderId);
        } catch (Exception e) {
            logger.error("getConfiForTender : " + logUserId + e);
        }
        logger.debug("getConfiForTender : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<TblTenderDetails> getCancelTenDet(int tenderId) {
        logger.debug("getCancelTenDet : " + logUserId + " Starts");
        List<TblTenderDetails> list = null;
        try {
            list = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception e) {
            logger.error("getCancelTenDet : " + logUserId + e);
        }
        logger.debug("getCancelTenDet : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean insertCancelTender(TblCancelTenderRequest tblCancelTenderRequest) {
        logger.debug("insertCancelTender : " + logUserId + " Starts");
        boolean flag = false;
        String action = null;
        try {
            tblCancelTenderRequestDao.addTblCancelTenderRequest(tblCancelTenderRequest);
            flag = true;
            action = "Request for Cancel Tender Notice";
        } catch (Exception e) {
            logger.error("insertCancelTender : " + logUserId + e);
            action = "Error in Request for Cancel Tender Notice";
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblCancelTenderRequest.getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Tender_Notice.getName(), action, tblCancelTenderRequest.getRemarks());
            action = null;
        }
        logger.debug("insertCancelTender : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public List<TblCancelTenderRequest> getDetailsCancelTender(int tenderId) {
        logger.debug("getDetailsCancelTender : " + logUserId + " Starts");
        List<TblCancelTenderRequest> list = new ArrayList<TblCancelTenderRequest>();
        try {
            list = tblCancelTenderRequestDao.findTblCancelTenderRequest("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId));
        } catch (Exception e) {
            logger.error("getDetailsCancelTender : " + logUserId + e);
        }
        logger.debug("getDetailsCancelTender : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean chkTenderStatus(int tenderId) {
        logger.debug("chkTenderStatus : " + logUserId + " Starts");
        long long_cnt = 0;
        boolean flag = false;
        try {
            long_cnt  = hibernateQueryDao.countForNewQuery("TblTenderDetails as td", "td.submissionDt > GETDATE() and td.tenderStatus='approved' and td.tblTenderMaster.tenderId ="+tenderId);
            if(long_cnt>0){
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("chkTenderStatus : " + logUserId + " : "+ex);
        }
        logger.debug("chkTenderStatus : " + logUserId + " Ends");
        return flag;
    }
    @Override
    public List<TblTenderDetails> getTenderStatus(int tenderId) {
        logger.debug("getTenderStatus : " + logUserId + " Starts");
        List<TblTenderDetails> list = null;
        try {
            list = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster",Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception ex) {
            logger.error("getTenderStatus : " + logUserId + " : "+ex);
        }
        logger.debug("getTenderStatus : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<Object> getAllocatedCost(int tenderId) {
        logger.debug("getAllocatedCost : " + logUserId + " Starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tap.allocateBudget from TblAppPackages tap,TblTenderMaster ttm,TblTenderDetails ttd where ttm.tblAppPackages.packageId = tap.packageId and ttd.tblTenderMaster.tenderId=ttm.tenderId and ttm.tenderId = " + tenderId + " and ttd.procurementMethodId in(3,8)");
        } catch (Exception ex) {
            logger.error("getAllocatedCost : " + logUserId + " : "+ex);
        }
        logger.debug("getAllocatedCost : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean insertDomestic(int tenderId,String domesticPref,String domesticPercent) {
        logger.debug("getInsertDomestic : " + logUserId + " Starts");
        boolean flag = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblTenderDetails set domesticPref = '" + domesticPref +"',domesticPercent=" + domesticPercent +" where tblTenderMaster.tenderId="+tenderId);
            flag = true;
        } catch (Exception ex) {
            logger.error("getInsertDomestic : " + logUserId + " : "+ex);
        }
        logger.debug("getInsertDomestic : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean addEvalServiceWeightage(TblEvalServiceWeightage tblEvalServiceWeightage) {
        logger.debug("addEvalServiceWeightage : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblEvalServiceWeightageDao.addTblEvalServiceWeightage(tblEvalServiceWeightage);
            flag = true;
        } catch (Exception ex) {
            logger.error("addEvalServiceWeightage : " + logUserId + " : "+ex);
        }
        logger.debug("addEvalServiceWeightage : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean updateEvalServiceWeightage(TblEvalServiceWeightage tblEvalServiceWeightage) {
        logger.debug("addEvalServiceWeightage : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblEvalServiceWeightageDao.updateTblEvalServiceWeightage(tblEvalServiceWeightage);
            flag = true;
        } catch (Exception ex) {
            logger.error("addEvalServiceWeightage : " + logUserId + " : "+ex);
        }
        logger.debug("addEvalServiceWeightage : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public long getTenderCorriCnt(int tenderId) {
        logger.debug("getTenderCorriCnt : " + logUserId + " Starts");
        long cnt = 0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblCorrigendumMaster tcm","tcm.corrigendumStatus='Approved' and tcm.tenderId = "+tenderId);
        } catch (Exception e) {
            logger.error("getTenderCorriCnt : " + logUserId + e);
        }
        logger.debug("getTenderCorriCnt : " + logUserId + " Ends");
        return cnt;
    }

    @Override
    public long getBeingProcessCnt(int tenderId) {
        logger.debug("getTenderCorriCnt : " + logUserId + " Starts");
        long cnt = 0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblNoaIssueDetails tnid,TblNoaAcceptance tna","tna.tblNoaIssueDetails.noaIssueId = tnid.noaIssueId and tna.acceptRejStatus = 'approved' and tnid.tblTenderMaster.tenderId="+tenderId);
//            System.out.print("CNT "+cnt);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getTenderCorriCnt : " + logUserId + e);
        }
        logger.debug("getTenderCorriCnt : " + logUserId + " Ends");
        return cnt;
    }

    @Override
    public TblTenderMaster getTenderMasterDetails(int tenderId){
        logger.debug("getTenderMasterDetails : " + logUserId + " Starts");
        TblTenderMaster tblTenderMaster = new TblTenderMaster();

        try {
            tblTenderMaster = tblTenderMasterDao.findTblTenderMaster("tenderId",Operation_enum.EQ,tenderId).get(0);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getTenderMasterDetails : " + logUserId + e);
        }
        logger.debug("getTenderMasterDetails : " + logUserId + " Ends");
        return tblTenderMaster;
    }

    @Override
    public TblNoaAcceptance getNoaAcceptance(int noaIssueId) {
        List<TblNoaAcceptance> list = new ArrayList<TblNoaAcceptance>();
        TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
        try {
            list = tblNoaAcceptanceDao.findTblNoaAcceptance("tblNoaIssueDetails",Operation_enum.EQ,new TblNoaIssueDetails(noaIssueId));
            if(list!=null && !list.isEmpty()){
                tblNoaAcceptance = list.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblNoaAcceptance;
    }

    @Override
    public boolean serTenderSecurity(String tenderId,String amt) {
        String query = "update TblTenderLotSecurity set tenderSecurityAmt="+amt+" where tblTenderMaster.tenderId="+tenderId;
        return hibernateQueryDao.updateDeleteQuery(query)!=0;
    }

        @Override
    public String getTenderReportStatus(String tenderId) {
        logger.debug("getEventType : " + logUserId + " Starts");
        String strTenderReportStatus = "NA";
        List<Object> list = hibernateQueryDao.getSingleColQuery("select ter.rptStatus from TblEvalRptSentToAa ter where ter.tblTenderMaster.tenderId=" + tenderId);
        if (!list.isEmpty()) {
            strTenderReportStatus = list.get(0).toString();
       }
        logger.debug("getEventType : " + logUserId + " Ends");
        return strTenderReportStatus;
    }

   

    
}
