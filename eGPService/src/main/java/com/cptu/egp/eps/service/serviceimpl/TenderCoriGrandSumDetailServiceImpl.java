/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderCoriGrandSumDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDetailDao;
import com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import com.cptu.egp.eps.model.table.TblTenderGrandSumDetail;
import com.cptu.egp.eps.service.serviceinterface.TenderCoriGrandSumDetailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDao;
import com.cptu.egp.eps.dao.daointerface.TblTendercorriFormsDao;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTendercorriForms;
import javax.xml.bind.Marshaller.Listener;

/**
 *
 * @author rikin.p
 */
public class TenderCoriGrandSumDetailServiceImpl implements TenderCoriGrandSumDetailService
{

    TblTenderCoriGrandSumDetailDao tblTenderCoriGrandSumDetailDao;
    static final Logger logger = Logger.getLogger(TenderCoriGrandSumDetailServiceImpl.class);
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    TblTenderGrandSumDetailDao tblTenderGrandSumDetailDao;
    TblTenderGrandSumDao tblTenderGrandSumDao;
    TblTendercorriFormsDao tblTendercorriFormsDao;
    TenderCommonService tenderCommonService;

    public TenderCommonService getTenderCommonService()
    {
        return tenderCommonService;
    }

    public void setTenderCommonService(TenderCommonService tenderCommonService)
    {
        this.tenderCommonService = tenderCommonService;
    }

    public TblTendercorriFormsDao getTblTendercorriFormsDao()
    {
        return tblTendercorriFormsDao;
    }

    public void setTblTendercorriFormsDao(TblTendercorriFormsDao tblTendercorriFormsDao)
    {
        this.tblTendercorriFormsDao = tblTendercorriFormsDao;
    }
    
    
    public TblTenderGrandSumDetailDao getTblTenderGrandSumDetailDao()
    {
        return tblTenderGrandSumDetailDao;
    }

    public void setTblTenderGrandSumDetailDao(TblTenderGrandSumDetailDao tblTenderGrandSumDetailDao)
    {
        this.tblTenderGrandSumDetailDao = tblTenderGrandSumDetailDao;
    }

    public TblTenderGrandSumDao getTblTenderGrandSumDao()
    {
        return tblTenderGrandSumDao;
    }

    public void setTblTenderGrandSumDao(TblTenderGrandSumDao tblTenderGrandSumDao)
    {
        this.tblTenderGrandSumDao = tblTenderGrandSumDao;
    }

    public HibernateQueryDao getHibernateQueryDao()
    {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao)
    {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public String getLogUserId()
    {
        return logUserId;
    }

    public void setLogUserId(String logUserId)
    {
        this.logUserId = logUserId;
    }

    public TblTenderCoriGrandSumDetailDao getTblTenderCoriGrandSumDetailDao()
    {
        return tblTenderCoriGrandSumDetailDao;
    }

    public void setTblTenderCoriGrandSumDetailDao(TblTenderCoriGrandSumDetailDao tblTenderCoriGrandSumDetailDao)
    {
        this.tblTenderCoriGrandSumDetailDao = tblTenderCoriGrandSumDetailDao;
    }

    /**
     * @description This method is use for insert Grand summary data into TenderCoriGrandSumDetail Table at the time of Crrigendum not published.
     * @param lstGsDtl
     * @param gsId
     * @param corriId
     * @return 
     */
    public int insertDataForGrandSummary(List<TblTenderCoriGrandSumDetail> lstGsDtl, int gsId, String corriId)
    {
        logger.debug("insertDataForGrandSummary : " + logUserId + " Starts");

        int result = -1;
        try
        {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderCoriGrandSumDetail ttgs where ttgs.tenderSumId = " + gsId + " and corrigendumId= " + corriId);
            for (int i = 0; i < lstGsDtl.size(); i++)
            {
                tblTenderCoriGrandSumDetailDao.addEntity(lstGsDtl.get(i));
                result = 1;
            }
        } catch (Exception ex)
        {

            ex.printStackTrace();
            logger.error("insertDataForGrandSummary : " + logUserId + " : " + ex.toString());
        }
        logger.debug("insertDataForGrandSummary : " + logUserId + " Ends");
        return result;
    }

    /**
     * @description This method is use for for check grand summary data exist for given corrigendum into TenderCoriGrandSumDetail or not?
     * @param tenderGSId
     * @param corriId
     * @return 
     */
    public boolean checkCoriExistInCoriDetailTbl(String tenderGSId, String corriId)
    {
        logger.debug("checkCoriExistInCoriDetailTbl : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append("select tgs.sumDtlId from TblTenderCoriGrandSumDetail tgs ");
        strQuery.append(" where tgs.tenderSumId = " + tenderGSId);
        strQuery.append(" and tgs.corrigendumId= " + corriId);
        List<Object[]> objs = new ArrayList<Object[]>();
        try
        {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            if (objs != null && objs.size() > 0)
            {
                return true;
            } else
            {
                return false;
            }
        } catch (Exception ex)
        {
            logger.error("checkCoriExistInCoriDetailTbl : " + logUserId + " : " + ex.toString());
            objs = null;
        }
        logger.debug("checkCoriExistInCoriDetailTbl : " + logUserId + " Ends");
        return false;
    }

    /**
     * @description This method is use for get Grand summary Form list of given corrigendum.
     * @param tenderGSId
     * @param corriId
     * @return 
     */
    public Map<Integer, Integer> getCorriGSFormsTblDtl(String tenderGSId, String corriId)
    {
        logger.debug("getCorriGSFormsTblDtl : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append("select tgs.tenderFormId, tgs.tenderTableId, tgs.cellId ");
        strQuery.append(" from TblTenderCoriGrandSumDetail tgs ");
        strQuery.append(" where tgs.tenderSumId = " + tenderGSId);
        strQuery.append(" and tgs.corrigendumId = " + corriId);
        List<Object[]> objs = new ArrayList<Object[]>();
        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try
        {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs)
            {
                hm.put((Integer) i[1], (Integer) i[2]);
            }
        } catch (Exception ex)
        {
            logger.error("getCorriGSFormsTblDtl : " + logUserId + " : " + ex.toString());
            objs = null;
        }
        logger.debug("getCorriGSFormsTblDtl : " + logUserId + " Ends");
        return hm;
    }

    /**
     * @description This method is use for get Grand summary Forms Table and cell details of given corrigendum.
     * @param tenderGSId
     * @param corriId
     * @return 
     */
    public Map<Integer, Integer> getCorriGSFormsDtl(String tenderGSId, String corriId)
    {
        logger.debug("getCorriGSFormsDtl : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append("select tgs.tenderFormId, tgs.cellId ");
        strQuery.append(" from TblTenderCoriGrandSumDetail tgs ");
        strQuery.append(" where tgs.tenderSumId = " + tenderGSId);
        strQuery.append(" and tgs.corrigendumId = " + corriId);
        List<Object[]> objs = new ArrayList<Object[]>();
        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try
        {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            Integer j = 0;
            for (Object[] i : objs)
            {
                hm.put(j, (Integer) i[0]);
                j++;
            }
        } catch (Exception ex)
        {
            logger.error("getCorriGSFormsDtl : " + logUserId + " : " + ex.toString());
            objs = null;
        }
        logger.debug("getCorriGSFormsDtl : " + logUserId + " Ends");
        return hm;
    }

    /**
     * @description This method is use to get TenderGrandSumId base on TenderId
     * @param tenderId
     * @return 
     */
    public String getTenderGrandSumId(String tenderId)
    {
        logger.debug("getTenderGrandSumId : " + logUserId + " Starts");
        String strQuery = "select tgs.tenderSumId from TblTenderGrandSum tgs where tgs.tblTenderMaster.tenderId= " + tenderId;
        String tenderGrandSumId = null;
        List<Object> objs = null;
        try
        {
            objs = hibernateQueryDao.singleColQuery(strQuery);
                
            if (objs != null && objs.size() > 0)
            {
                for (Object i : objs)
                {
                    tenderGrandSumId = i.toString();
                }
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error("getTenderGrandSumId : " + logUserId + " : " + ex.toString());
            objs = null;
        }
        logger.debug("getTenderGrandSumId : " + logUserId + " Ends");
        return tenderGrandSumId;

    }
    
    /**
     * @description This function is use for get Tender Form detail base on it's status.
     * @param tenderId
     * @return List<SPTenderCommonData>
     */
    public List<SPTenderCommonData> getChagnedTenderFormDetail(String tenderId)
    {
        logger.debug("getChagnedTenderFormDetail : " + logUserId + " Starts");
        List<SPTenderCommonData> lstCommonData=null;
        try
        {
            lstCommonData=tenderCommonService.returndata("getTenderFormCPCreateP", tenderId + "", "'createp','cp'");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
         logger.error("getChagnedTenderFormDetail : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getChagnedTenderFormDetail : " + logUserId + " Ends");   
        return lstCommonData;
    }

    /**
     * @description This method is use for dump data of TempCorriGrandSumDetail into original GrandSumDetail Table at the time of corrigendum published.
     * 
     * @param tenderGSId
     * @param corriId 
     */
    public int performGrandSumCorriPublish(String tenderId, String corriId)
    {
        logger.debug("performGrandSumCorriPublish : " + logUserId + " Starts");
        List<Object[]> objs = new ArrayList<Object[]>();
        int result = -1;
        StringBuffer strQuery = new StringBuffer();
        List<TblTendercorriForms> lstTenderCorriForm=new ArrayList<TblTendercorriForms>();
        String tenderGSId = null;
        try
        {
            tenderGSId = getTenderGrandSumId(tenderId);
            
            // Getting Form data which status is 'createp' or 'cp' during this corrigendum    
            // Following code will make entry into TblTendercorriForms table for maintain corrigendum wish form change history
            List<SPTenderCommonData> lstCommonData=getChagnedTenderFormDetail(tenderId);
            if(lstCommonData != null)
            {
                for(int i=0;i<lstCommonData.size();i++)
                {
                    TblTendercorriForms tenderCoriForm=new TblTendercorriForms();
                    tenderCoriForm.setCorrigendumId(Integer.parseInt(corriId));
                    //System.out.println(" ------------------- recor no:"+i+"---------------------------");
                    //System.out.println(" Corri id="+ corriId);
                    
                    tenderCoriForm.setFormId(Integer.parseInt(lstCommonData.get(i).getFieldName1()));
                    //System.out.println(" FormId="+ Integer.parseInt(lstCommonData.get(i).getFieldName1()));
                    
                    tenderCoriForm.setFormStatus(lstCommonData.get(i).getFieldName3());
                    //System.out.println(" FormStatus="+ lstCommonData.get(i).getFieldName3());
                    
                    lstTenderCorriForm.add(tenderCoriForm);
                }
                tblTendercorriFormsDao.updateOrSaveTblTendercorriForms(lstTenderCorriForm);
            }

            
            // Following code will transfer grand summary data from TblTenderCoriGrandSumDetail to roiginal table TblTenderGrandSumDetail.
            List<TblTenderGrandSumDetail> lstTenderGrandSumDetail = new ArrayList<TblTenderGrandSumDetail>();
            strQuery = new StringBuffer();
            strQuery.append("select tcgsm.tenderSumId,tcgsm.tenderFormId,tcgsm.cellId,tcgsm.tenderTableId ");
            strQuery.append("from TblTenderCoriGrandSumDetail tcgsm ");
            strQuery.append("where tcgsm.corrigendumId = " + corriId + " and tenderSumId=" + tenderGSId);
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());

           // System.out.println("---------------------------"+objs.size());
            if (tenderGSId != null && objs != null && objs.size() > 0)
            {
                strQuery = new StringBuffer();
                strQuery.append("delete from TblTenderGrandSumDetail tgsd ");
                strQuery.append(" where tgsd.tblTenderGrandSum.tenderSumId = " + tenderGSId);

                
                result = hibernateQueryDao.updateDeleteNewQuery(strQuery.toString());

                
                if (result >= 0) // Condition only perform insert if deletion successed.
                {
                    for (Object[] i : objs)
                    {
                        TblTenderGrandSumDetail objTblGrdSum = new TblTenderGrandSumDetail();
                        objTblGrdSum.setTblTenderGrandSum(new TblTenderGrandSum(Integer.parseInt(i[0].toString())));
                        objTblGrdSum.setTenderFormId(Integer.parseInt(i[1].toString()));
                        objTblGrdSum.setCellId(Integer.parseInt(i[2].toString()));
                        objTblGrdSum.setTenderTableId(Integer.parseInt(i[3].toString()));
                        lstTenderGrandSumDetail.add(objTblGrdSum);
                    }
                    tblTenderGrandSumDetailDao.updateSaveAllTenderGrandSumDetail(lstTenderGrandSumDetail);
                }
            }


        } catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error("performGrandSumCorriPublish : " + logUserId + " : " + ex.toString());
        }
        logger.debug("performGrandSumCorriPublish : " + logUserId + " Ends");
        return result;
    }

    /**
     * @description This function is use to create fresh grand summary of tender after tender corrigendum performed and fresh tender does not created grand summary.
     * @param gs
     * @param lstGsDtl
     * @param corriId
     * @return 
     */
    public int insertCorriGrandSummary(TblTenderGrandSum gs, List<TblTenderCoriGrandSumDetail> lstGsDtl, String corriId)
    {
        logger.debug("insertCorriGrandSummary : " + logUserId + " Starts");
        int gsId = -1;

        try
        {
            tblTenderGrandSumDao.addTblTenderGrandSum(gs);
            gsId = gs.getTenderSumId();
            if(gsId != -1)
            {
                for (int i = 0; i < lstGsDtl.size(); i++)
                {
                    TblTenderCoriGrandSumDetail gsDtl = lstGsDtl.get(i);
                    gsDtl.setTenderSumId(gsId);
                    tblTenderCoriGrandSumDetailDao.addTblTenderCoriGrandSumDetail(gsDtl);
                    if (gsDtl != null)
                    {
                        gsDtl = null;
                    }
                }
            }
        } 
        catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error("insertDataForGrandSummary : " + logUserId + " : " + ex.toString());
        }
        logger.debug("insertDataForGrandSummary : " + logUserId + " Ends");
        return gsId;
    }
}
