/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import com.cptu.egp.eps.model.table.TblTenderTdsSubClause;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface PrepareTenderTds {
    public List<SPTenderCommonData> getTenderTDSSubClause(int ittHeaderId);
    public boolean updateTDSSubClause(TblTenderTdsSubClause tblTenderTdsSubClause);
    public boolean save(TblTenderTdsSubClause tblTenderTdsSubClause);
    public boolean delete(int tdsSubClauseId);
    public void setUserId(String logUserId);
    public boolean updateAllClause(List<TblTenderTdsSubClause> updateList);
    public boolean insertAllClause(List<TblTenderTdsSubClause> addList);
    public boolean saveOrUpdateCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail);
}
