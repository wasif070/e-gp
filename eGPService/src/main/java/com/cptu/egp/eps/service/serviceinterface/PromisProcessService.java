/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.daointerface.TblPromisProcessDao;
import com.cptu.egp.eps.model.table.TblPromisProcess;
import java.util.List;
/**
 *
 * @author Sudhir Chavhan
 */
public interface PromisProcessService {

   public TblPromisProcessDao getTblPromisProcessDao();

   public List<TblPromisProcess> getAllTblPromisProcess() throws Exception;

/* public void addTblPromisIndicator(TblPromisIndicator admin);

   public void deleteTblPromisIndicator(TblPromisIndicator admin);

   public void updateTblPromisIndicator(TblPromisIndicator admin);   

   public List<TblPromisIndicator> findTblPromisIndicator(Object... values) throws Exception;

   public List<TblPromisIndicator> findByCountTblPromisIndicator(int firstResult,int maxResult,Object... values) throws Exception;

   public long getTblPromisIndicatorCount();   */

}
