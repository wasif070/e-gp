/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblJointVenture;
import com.cptu.egp.eps.model.table.TblJvcapartners;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface JointVentureService {
    
    /**
     * Get JvcaPrposedList
     * @param userId From Session UserID
     * @param sidx : Record per page
     * @param e 
     * @return Object[] in [0] jvId, [1] name, [2] jvStatus , [3] isNominatedPartner, [4] CreatedDate, [5] newJvuserId
     */

    public List<Object[]> jvcaPrposedList(int userId,String sidx,Object e);
     
    /**
     * Get company List by company Name and Email Id
     * @param companyName
     * @param emailId
     * @return
     */
    public List<Object[]> getCompanyList(String companyName,String emailId);

    /**
     * Create New JOint venture.
     * @param tblJointVenture
     * @param listJvcaPartner
     */
    public void createJVCA(TblJointVenture tblJointVenture,List<TblJvcapartners> listJvcaPartner);

    /**
     * update status of the JVCA
     * @param status
     * @param jvId
     * @return boolean 
     */
    public boolean updateStatus(String status,int jvId);
   
    /**
     * Propoed new JVCA.
     * @param userId
     * @param searchField
     * @param searchOper
     * @param searchString
     * @return Object[] in [0] jvId, [1] name, [2] jvStatus , [3] isNominatedPartner, [4] CreatedDate, [5] newJvuserId
     */
    public List<Object[]> preposedJvca(int userId,String searchField,String searchOper,String searchString);

    /**
     * Get JVCA Details by jvca id
     * @param jvcId
     * @return
     */
    public List<TblJointVenture> getJVCADetails(int jvcId);

    /**
     * update JVCA Details
     * @param jvcId
     * @param userId 
     * @return
     */
    public int updateJVCA(int jvcId,int userId,String...values);

    /**
     * Update JVCA Name
     * @param jvid
     * @param jvname
     * @return
     */
    public int updateJVCAName(int jvid,String jvname);

    /**
     * Get Company Owner Details.
     * @param userId
     * @return
     */
    public List<Object[]> getOwnCompanyDetails(int userId);

    /**
     * For Logging Purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
     /**
     * Get data for mail the users
     * @param jvId 
     * @param userId 
     * @return List object [0] emailId, object[1] companyName, 
     * Object[2] jvAcceptRejStatus, Object[3] Nominated Partner,[4] JvRole,[5] jvca createdBy,[6] JVCA Name
     */
    public List<Object[]> forMailContent(int jvId,int userId);
    /**
     * Get data for mail the users
     * @param jvId 
     * @param userId 
     * @return List object [0] emailId, 
     */
    public List<Object> forMailEmail(int jvId,int userId);
    
    public void setAuditTrail(AuditTrail auditTrail);
}
