/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface WsOrgMasterService {

    public void setLogUserId(String logUserId);

    public void setAuditTrail(AuditTrail auditTrail);

    public int insertWsOrgMaster(TblWsOrgMaster tblWsOrgMaster);

    public boolean updateWsOrgMaster(TblWsOrgMaster tblWsOrgMaster,String ... actions);

    public boolean deleteWsOrgMaster(TblWsOrgMaster tblWsOrgMaster);

    public List<TblWsOrgMaster> getAllWsOrgMaster();

    public long getWsOrgMasterCount();

    public TblWsOrgMaster getTblWsOrgMaster(int id);

    public TblWsOrgMaster getTblWsOrgMaster(String userEmailId);

    public List<TblWsOrgMaster> getRequiredTblWsOrgMasterList(String whereCondition);
}
