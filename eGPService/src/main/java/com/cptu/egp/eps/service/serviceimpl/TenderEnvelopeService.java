/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderEnvelopesDao;
import com.cptu.egp.eps.model.table.TblTenderEnvelopes;
import org.apache.log4j.Logger;


/**
 *
 * @author dipti
 */
public class TenderEnvelopeService {
    TblTenderEnvelopesDao tblTenderEnvelopesDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId=  "";
    final Logger logger = Logger.getLogger(TenderEnvelopeService.class);
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setTblTenderEnvelopesDao(TblTenderEnvelopesDao tblTenderEnvelopesDao) {
        this.tblTenderEnvelopesDao = tblTenderEnvelopesDao;
    }

    public TblTenderEnvelopesDao getTblTenderEnvelopesDao() {
        return tblTenderEnvelopesDao;
    }

    /**
     * Insert Tender Envelope details
     * @param tte = TblTenderEnvelopes oblect
     */
    public void insertTenderEnvelopes(TblTenderEnvelopes tte){
        tblTenderEnvelopesDao.addTblTenderEnvelopes(tte);
    }

    /**
     * Update Tender Envelope details
     * @param tenId = tender id
     * @param frmId = form id
     * @param envId = evelope id
     */
    public void updateToTenderEnvelope(int tenId, int frmId, short envId){
        logger.debug("updateToTenderEnvelope : "+logUserId+" Starts");
        try{
            hibernateQueryDao.updateDeleteNewQuery("update TblTenderEnvelopes set envelopeId = " + envId + " where tblTenderMaster.tenderId = " + tenId + " and tblTenderForms.tenderFormId = " + frmId);
        }catch(Exception ex){
            logger.error("updateToTenderEnvelope : "+logUserId+" :"+ex);
        }
        logger.debug("updateToTenderEnvelope : "+logUserId+" Ends");
    }

    /**
     * Delete Tender Envelope details.
     * @param tenId
     * @param frmId
     */
    public void deleteFromTenderEnvelope(int tenId, int frmId){
        logger.debug("deleteFromTenderEnvelope : "+logUserId+" Starts");
        try{
            hibernateQueryDao.updateDeleteNewQuery("delete TblTenderEnvelopes tte where tte.tblTenderMaster.tenderId = " + tenId + " and tte.tblTenderForms.tenderFormId = " + frmId);
        }catch(Exception ex){
            logger.error("deleteFromTenderEnvelope : "+logUserId+" :"+ex);
        }
        logger.debug("deleteFromTenderEnvelope : "+logUserId+" Ends");
    }
}
