/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCascadeDetailsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCascadeLinkDetails;
import com.cptu.egp.eps.service.serviceinterface.CascadeDetailsService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author tejasree.goriparthi
 */
public class CascadeDetailsServiceImpl implements CascadeDetailsService{
    private static final Logger LOGGER = Logger.getLogger(CascadeDetailsServiceImpl.class);
    TblCascadeDetailsDao tblCascadeDetailsDao;
    HibernateQueryDao hibernateQueryDao;

    public TblCascadeDetailsDao getTblCascadeDetailsDao() {
        return tblCascadeDetailsDao;
    }

    public void setTblCascadeDetailsDao(TblCascadeDetailsDao tblCascadeDetailsDao) {
        this.tblCascadeDetailsDao = tblCascadeDetailsDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    @Override
    public List<TblCascadeLinkDetails> getCascadeDetailsList(String userTypeId, String status, String pageName,String parentLink) throws Exception {
       LOGGER.debug("getCascadeDetails : " + userTypeId + "Starts");
        List<TblCascadeLinkDetails> cascadeDetailsList = null;
        try {
                if(parentLink!=null && parentLink!="null" && parentLink!=""){
                cascadeDetailsList = tblCascadeDetailsDao.findTblCascadeDetails("userTypeId", Operation_enum.EQ, userTypeId, "status", Operation_enum.EQ, status, "pageName", Operation_enum.EQ, pageName,"parentLinkList",Operation_enum.LIKE,"%,"+parentLink.trim());
             }else{
                cascadeDetailsList = tblCascadeDetailsDao.findTblCascadeDetails("userTypeId", Operation_enum.EQ, userTypeId, "status", Operation_enum.EQ, status, "pageName", Operation_enum.EQ, pageName);
            }
            

        } catch (Exception e) {
            LOGGER.error("getCascadeDetails : " + userTypeId + " " + e);
            
        }
        LOGGER.debug("addInvoiceAccDetails : " + userTypeId + "Ends");
        return cascadeDetailsList;
    }

}
