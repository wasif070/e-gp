/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigPaThresholdDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigPaThreshold;
import com.cptu.egp.eps.service.serviceinterface.ConfigPaThresholdService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author feroz
 */
public class ConfigPaThresholdServiceImpl implements ConfigPaThresholdService{
    
    static final Logger LOGGER = Logger.getLogger(ConfigPaThresholdServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblConfigPaThresholdDao tblConfigPaThresholdDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override        
    public void addTblConfigPaThreshold (TblConfigPaThreshold tblConfigPaThreshold) {
    LOGGER.debug("addTblConfigPaThreshold: " + logUserId + LOGGERSTART);
            try {
            getTblConfigPaThresholdDao().addTblConfigPaThreshold(tblConfigPaThreshold);
            } catch (Exception e) {
                LOGGER.error("addTblConfigPaThreshold: " + logUserId + e);
            }
            LOGGER.debug("addTblConfigPaThreshold: " + logUserId + LOGGEREND);
    }

    @Override        
    public void deleteTblConfigPaThreshold (TblConfigPaThreshold tblConfigPaThreshold) {
    LOGGER.debug("deleteTblConfigPaThreshold: " + logUserId + LOGGERSTART);
            try {
            getTblConfigPaThresholdDao().deleteTblConfigPaThreshold(tblConfigPaThreshold);
            } catch (Exception e) {
                LOGGER.error("deleteTblConfigPaThreshold: " + logUserId + e);
            }
            LOGGER.debug("deleteTblConfigPaThreshold: " + logUserId + LOGGEREND);
    }
    
    @Override        
    public String updateTblConfigPaThreshold (TblConfigPaThreshold tblConfigPaThreshold) {
        boolean flag = false;
    LOGGER.debug("updateTblConfigPaThreshold: " + logUserId + LOGGERSTART);
            try {
            getTblConfigPaThresholdDao().updateTblConfigPaThreshold(tblConfigPaThreshold);
            flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblConfigPaThreshold: " + logUserId + e);
            }
            LOGGER.debug("updateTblConfigPaThreshold: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc";
            }
            else
            {
                return "failed";
            }
    }
    
    @Override        
    public List<TblConfigPaThreshold> getAllTblConfigPaThreshold() {
        LOGGER.debug("getAllTblConfigPaThreshold : " + logUserId + " Starts");
        List<TblConfigPaThreshold> tblConfigPaThreshold = null;
        try {
            tblConfigPaThreshold = getTblConfigPaThresholdDao().getAllTblConfigPaThreshold();
        } catch (Exception ex) {
            LOGGER.error("getAllTblConfigPaThreshold : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblConfigPaThreshold : " + logUserId + " Ends");
        return tblConfigPaThreshold;
    }
    
    @Override        
    public List<TblConfigPaThreshold> findTblConfigPaThreshold(int id) {
        LOGGER.debug("getAllTblConfigPaThreshold : " + logUserId + " Starts");
        List<TblConfigPaThreshold> tblConfigPaThreshold = null;
        try {
            tblConfigPaThreshold = getTblConfigPaThresholdDao().findTblConfigPaThreshold("configPathresholdId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblConfigPaThreshold : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblConfigPaThreshold : " + logUserId + " Ends");
        return tblConfigPaThreshold;
    }

    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblConfigPaThresholdDao
     */
    public TblConfigPaThresholdDao getTblConfigPaThresholdDao() {
        return tblConfigPaThresholdDao;
    }

    /**
     * @param tblConfigPaThresholdDao the tblConfigPaThresholdDao to set
     */
    public void setTblConfigPaThresholdDao(TblConfigPaThresholdDao tblConfigPaThresholdDao) {
        this.tblConfigPaThresholdDao = tblConfigPaThresholdDao;
    }
    
}
