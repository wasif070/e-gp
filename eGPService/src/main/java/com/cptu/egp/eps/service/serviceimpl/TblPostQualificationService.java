/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblPostQualificationDao;
import com.cptu.egp.eps.model.table.TblPostQualification;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class TblPostQualificationService {

    private TblPostQualificationDao qualificationDao;
    private HibernateQueryDao hibernateQueryDao;
    private final Logger logger = Logger.getLogger(TblPostQualificationService.class);
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblPostQualificationDao getQualificationDao() {
        return qualificationDao;
    }

    public void setQualificationDao(TblPostQualificationDao qualificationDao) {
        this.qualificationDao = qualificationDao;
    }

    /**
     * update tbl_PostQualification
     * @param postQualification to be updated
     * @return true if successfully updated else false
     */
    public boolean updatePQ(TblPostQualification postQualification){
        logger.debug("updatePQ :  starts");
        boolean flag = false;
                try{
            qualificationDao.updateTblPostQualification(postQualification);
            flag=true;
        }catch(Exception e){
           logger.error("updatePQ :"+e);
        }
        logger.debug("updatePQ :  Ends");
        return flag;


    }
    /**
     *Fetching info from tbl_PostQualification
     * @param userId from tbl_PostQualification
     * @param pkgId from tbl_PostQualification
     * @return list of objects from tbl_PostQualification
     */
    public List<Object[]> getAll(String userId,String pkgId){
        logger.debug("getAll :  starts");
        List<Object[]> list = null;
        try{
            String query = "select tp.siteVisitReqDt,tp.comments,tp.siteVisit,tp.rank from TblPostQualification tp where tp.userId="+userId+" and tp.postQualStatus='initiated' and pkgLotId="+pkgId;
            list = hibernateQueryDao.createNewQuery(query);

        }catch(Exception e){
            logger.error("getAll :"+e);
        }
        logger.debug("getAll :  Ends");
        return list;

    }

}
