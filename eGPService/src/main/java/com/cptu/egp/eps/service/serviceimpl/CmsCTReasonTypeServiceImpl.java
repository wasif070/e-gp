/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsCTReasonTypeDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsCTReasonType;
import com.cptu.egp.eps.service.serviceinterface.CmsCTReasonTypeService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsCTReasonTypeServiceImpl implements CmsCTReasonTypeService {

    final Logger logger = Logger.getLogger(CmsCTReasonTypeServiceImpl.class);
    private TblCmsCTReasonTypeDao cmsCTReasonTypeDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsCTReasonTypeDao getCmsCTReasonTypeDao() {
        return cmsCTReasonTypeDao;
    }

    public void setCmsCTReasonTypeDao(TblCmsCTReasonTypeDao cmsCTReasonTypeDao) {
        this.cmsCTReasonTypeDao = cmsCTReasonTypeDao;
    }

    /***
     * This method inserts a TblCmsCTReasonType object in database
     * @param tblCmsCTReasonType
     * @return int
     */
    @Override
    public int insertCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("insertCmsCTReasonType : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsCTReasonTypeDao.addTblCmsCTReasonType(tblCmsCTReasonType);
            flag = tblCmsCTReasonType.getCtReasonTypeId();
        } catch (Exception ex) {
            logger.error("insertCmsCTReasonType : " + logUserId + " : " + ex);
            flag = 0;
        }
        logger.debug("insertCmsCTReasonType : " + logUserId + ENDS);
        return flag;
    }

     /****
     * This method updates an TblCmsCTReasonType object in DB
     * @param tblCmsCTReasonType
     * @return boolean
     */
    @Override
    public boolean updateCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("updateCmsCTReasonType : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsCTReasonTypeDao.updateTblCmsCTReasonType(tblCmsCTReasonType);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsCTReasonType : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("updateCmsCTReasonType : " + logUserId + ENDS);
        return flag;
    }

     /****
     * This method deletes an TblCmsCTReasonType object in DB
     * @param tblCmsCTReasonType
     * @return boolean
     */
    @Override
    public boolean deleteCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("deleteCmsCTReasonType : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsCTReasonTypeDao.deleteTblCmsCTReasonType(tblCmsCTReasonType);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsCTReasonType : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteCmsCTReasonType : " + logUserId + ENDS);
        return flag;
    }

     /****
     * This method returns all TblCmsCTReasonType objects from database
     * @return List<TblCmsCTReasonType>
     */
    @Override
    public List<TblCmsCTReasonType> getAllCmsCTReasonType() {
        logger.debug("getAllCmsCTReasonType : " + logUserId + STARTS);
        List<TblCmsCTReasonType> cmsCTReasonTypeList = new ArrayList<TblCmsCTReasonType>();
        try {
            cmsCTReasonTypeList = cmsCTReasonTypeDao.getAllTblCmsCTReasonType();
        } catch (Exception ex) {
            logger.error("getAllCmsCTReasonType : " + logUserId + " : " + ex);
        }
        logger.debug("getAllCmsCTReasonType : " + logUserId + ENDS);
        return cmsCTReasonTypeList;
    }

    /***
     *  This method returns no. of TblCmsCTReasonType objects from database
     * @return long
     */
    @Override
    public long getCmsCTReasonTypeCount() {
        logger.debug("getCmsCTReasonTypeCount : " + logUserId + STARTS);
        long cmsCTReasonTypeCount = 0;
        try {
            cmsCTReasonTypeCount = cmsCTReasonTypeDao.getTblCmsCTReasonTypeCount();
        } catch (Exception ex) {
            logger.error("getCmsCTReasonTypeCount : " + logUserId + " : " + ex);
        }
        logger.debug("getCmsCTReasonTypeCount : " + logUserId + ENDS);
        return cmsCTReasonTypeCount;
    }

     /***
     * This method returns TblCmsCTReasonType for the given Id
     * @param int id
     * @return TblCmsCTReasonType
     */
    @Override
    public TblCmsCTReasonType getCmsCTReasonType(int ctReasonTypeId) {
        logger.debug("getCmsCTReasonType : " + logUserId + STARTS);
        TblCmsCTReasonType tblCmsCTReasonType = null;
        List<TblCmsCTReasonType> cmsCTReasonTypeList = null;
        try {
            cmsCTReasonTypeList = cmsCTReasonTypeDao.findTblCmsCTReasonType("ctReasonTypeId", Operation_enum.EQ, ctReasonTypeId);
        } catch (Exception ex) {
            logger.error("getCmsCTReasonType : " + logUserId + " : " + ex);
        }
        if (!cmsCTReasonTypeList.isEmpty()) {
            tblCmsCTReasonType = cmsCTReasonTypeList.get(0);
        }
        logger.debug("getCmsCTReasonType : " + logUserId + ENDS);
        return tblCmsCTReasonType;
    }

     /***
     * This method returns TblCmsCTReasonType for the given reason
     * @param String reason
     * @return TblCmsCTReasonType
     */
    @Override
    public TblCmsCTReasonType getCmsCTReasonTypeForReason(String reason) {
        logger.debug("getCmsCTReasonTypeForReason : " + logUserId + STARTS);
        TblCmsCTReasonType tblCmsCTReasonType = null;
        List<TblCmsCTReasonType> cmsCTReasonTypeList = null;
        try {
            cmsCTReasonTypeList = cmsCTReasonTypeDao.findTblCmsCTReasonType("ctReason", Operation_enum.EQ, reason);
        } catch (Exception ex) {
            logger.error("getCmsCTReasonTypeForReason : " + logUserId + " : " + ex);
        }
        if (!cmsCTReasonTypeList.isEmpty()) {
            tblCmsCTReasonType = cmsCTReasonTypeList.get(0);
        }
        logger.debug("getCmsCTReasonTypeForReason : " + logUserId + ENDS);
        return tblCmsCTReasonType;
    }
}
