/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.APPSearchDtBean;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPAPPSearch;
import com.cptu.egp.eps.dao.storedprocedure.SearchAppData;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AppAdvSearchServiceImpl implements AppAdvSearchService {

    private static final Logger LOGGER = Logger.getLogger(AppAdvSearchServiceImpl.class);

    private SearchAppData searchAppData;
    private HibernateQueryDao hibernateQueryDao;
    private SPAPPSearch spAPPSearch;

    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";

    public SPAPPSearch getSpAPPSearch() {
        return spAPPSearch;
    }

    public void setSpAPPSearch(SPAPPSearch spAPPSearch) {
        this.spAPPSearch = spAPPSearch;
    }

    public SearchAppData getSearchAppData() {
        return searchAppData;
    }

    public void setSearchAppData(SearchAppData searchAppData) {
        this.searchAppData = searchAppData;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<CommonAppSearchData> getSearchResults(String keyword, String financialYear, String procurementnature, Integer departmentId, Integer employeeId, String cpvCode, String projectName, String procurementType, Integer appid, String appcode, String packageno, BigDecimal pkgEstCost, Integer officeId, String budgetType, String operator, BigDecimal value1, BigDecimal value2, int pageNo, int recordOffset, String status, Integer userId) {
        LOGGER.debug("getSearchResults : " + logUserId + LOGGERSTART);
        List<CommonAppSearchData> commonAppSearchDatas = new ArrayList<CommonAppSearchData>();
        try {
            commonAppSearchDatas = searchAppData.executeProcedure(keyword, financialYear, procurementnature, departmentId, employeeId, cpvCode, projectName, procurementType, appid, appcode, packageno, pkgEstCost, officeId, budgetType, operator, value1, value2, pageNo, recordOffset, status, userId);
        } catch (Exception e) {
            LOGGER.error("getSearchResults : " + logUserId + e);
        }
        LOGGER.debug("getSearchResults : " + logUserId + LOGGEREND);
        return commonAppSearchDatas;
    }

    @Override
    public long countForQuery(String from, String where) throws Exception {
        LOGGER.debug("countForQuery : " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery(from, where);
        } catch (Exception e) {
            LOGGER.error("countForQuery : " + logUserId + e);
    }
        LOGGER.debug("countForQuery : " + logUserId + LOGGEREND);
        return count;
    }

    @Override
    public int getProcMethodId(int appId, int pkgId) {
        LOGGER.debug("getProcMethodId : " + logUserId + LOGGERSTART);
        List<Object> obj = null;
        int procMethodId = -1;
        try {
            String query = "select tap.tblProcurementMethod.procurementMethodId from TblAppPackages tap where tap.tblAppMaster.appId = " + appId + " and tap.packageId = " + pkgId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (!obj.isEmpty() && obj != null) {
                    procMethodId = Integer.parseInt(obj.get(0).toString());
                }
        } catch (Exception ex) {
            LOGGER.error("getProcMethodId : " + logUserId + ex);
            }
        LOGGER.debug("getProcMethodId : " + logUserId + LOGGEREND);
            return procMethodId;
        }

    @Override
    public String getIsPQReq(int appId, int pkgId) {
        LOGGER.debug("getIsPQReq : " + logUserId + LOGGERSTART);
        List<Object> obj = null;
        String isPQReq = "";
        try {
            String query = "select tap.isPqrequired from TblAppPackages tap where tap.tblAppMaster.appId = " + appId + " and tap.packageId = " + pkgId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null && !obj.isEmpty()) {
                    isPQReq = obj.get(0).toString();
                }
        } catch (Exception ex) {
            LOGGER.error("getIsPQReq : " + logUserId + ex);
            }
        LOGGER.debug("getIsPQReq : " + logUserId + LOGGEREND);
            return isPQReq;
        }

    @Override
    public List<APPSearchDtBean> getAPPSearch(String financialYear, int departmentId, int stateId, int officeId, int pageNo, int recordPerPage) {
        LOGGER.debug("getAPPSearch : " + logUserId + LOGGERSTART);
        List<APPSearchDtBean> appSearchDetails = new ArrayList<APPSearchDtBean>();
        try {
        if (appSearchDetails.isEmpty()) {
            appSearchDetails = spAPPSearch.executeProcedure(financialYear, departmentId, stateId, officeId, pageNo, recordPerPage);
        }
        } catch (Exception e) {
            LOGGER.error("getAPPSearch : " + logUserId + e);
        }
        LOGGER.debug("getAPPSearch : " + logUserId + LOGGEREND);
        return appSearchDetails;
    }
    
     @Override
    public int getPackageIdByAppId(int appId) {
        LOGGER.debug("getPackageIdByAppId : " + logUserId + LOGGERSTART);
        List<Object> obj = null;
        String packageId = "";
        try {
            String query = "select packageId  from TblAppPackages  where tblAppMaster.appId = " + appId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null && !obj.isEmpty()) {
                    packageId = obj.get(0).toString();
                }
        } catch (Exception ex) {
            LOGGER.error("getPackageIdByAppId : " + logUserId + ex);
            }
        LOGGER.debug("getPackageIdByAppId : " + logUserId + LOGGEREND);
            return Integer.parseInt(packageId);
        }

      @Override
    public int getTempProcMethodId(int appId, int pkgId) {
        LOGGER.debug("getProcMethodId : " + logUserId + LOGGERSTART);
        List<?> obj = null;
        int procMethodId = -1;
        try {
            String query = "select procurementMethodId from tbl_TempAppPackages where appId = " + appId + " and packageId = " + pkgId;
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (!obj.isEmpty() && obj != null) {
                Byte promethId=(Byte)obj.get(0);
                    procMethodId = promethId;
                }
        } catch (Exception ex) {
            LOGGER.error("getProcMethodId : " + logUserId + ex.toString());
            }
        LOGGER.debug("getProcMethodId : " + logUserId + LOGGEREND);
            return procMethodId;
        }


}
