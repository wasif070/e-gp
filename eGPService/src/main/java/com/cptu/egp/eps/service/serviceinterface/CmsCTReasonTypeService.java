/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsCTReasonType;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsCTReasonTypeService {

    public void setLogUserId(String logUserId);

    public int insertCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public boolean updateCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public boolean deleteCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public List<TblCmsCTReasonType> getAllCmsCTReasonType();

    public long getCmsCTReasonTypeCount();

    public TblCmsCTReasonType getCmsCTReasonType(int id);

    public TblCmsCTReasonType getCmsCTReasonTypeForReason(String ctReason);

}
