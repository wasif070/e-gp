/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblTenderBidPlainDataImpl;
import com.cptu.egp.eps.dao.daoimpl.TblTenderHashImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblTenderHash;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
public class TenderOpeningServiceImpl {

    final Logger logger = Logger.getLogger(TenderOpeningServiceImpl.class);

    private SPCommonSearch sPCommonSearch;
    private TblTenderHashImpl tblTenderHashDao;
    private SPXMLCommon spXMLCommon;
    private TblTenderBidPlainDataImpl tblTenderBidPlainData;
    private HibernateQueryDao hibernateQueryDao;
    private TenderCommonService tenderCommonService;
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TenderCommonService getTenderCommonService() {
        return tenderCommonService;
    }

    public void setTenderCommonService(TenderCommonService tenderCommonService) {
        this.tenderCommonService = tenderCommonService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the sPCommonSearch
     */
    public SPCommonSearch getsPCommonSearch() {
        return sPCommonSearch;
    }

    /**
     * @param sPCommonSearch the sPCommonSearch to set
     */
    public void setsPCommonSearch(SPCommonSearch sPCommonSearch) {
        this.sPCommonSearch = sPCommonSearch;
    }

    /**
     * @return the tblTenderHashDao
     */
    public TblTenderHashImpl getTblTenderHashDao() {
        return tblTenderHashDao;
    }

    /**
     * @param tblTenderHashDao the tblTenderHashDao to set
     */
    public void setTblTenderHashDao(TblTenderHashImpl tblTenderHashDao) {
        this.tblTenderHashDao = tblTenderHashDao;
    }

    /**
     * @return the spXMLCommon
     */
    public SPXMLCommon getSpXMLCommon() {
        return spXMLCommon;
    }

    /**
     * @param spXMLCommon the spXMLCommon to set
     */
    public void setSpXMLCommon(SPXMLCommon spXMLCommon) {
        this.spXMLCommon = spXMLCommon;
    }

    /**
     * Decrypt Data of Tender Form
     * @param funName
     * @param tid
     * @param fid
     * @param field4
     * @param field5
     * @param field6
     * @param field7
     * @param field8
     * @param field9
     * @param field10
     * @return true or false for success or fail
     */
    public List<SPCommonSearchData> getTenderEncriptionData(String funName, String tid, String uid, String fid,
            String field5, String field6, String field7, String field8, String field9, String field10) {
        logger.debug("getTenderEncriptionData : " + logUserId + " Starts");
        List<SPCommonSearchData> commonSearchData = null;
        try {
            commonSearchData = sPCommonSearch.executeProcedure(funName, tid, uid, fid, field5, field6, field7, field8, field9, field10,null);
        } catch (Exception e) {
            logger.error("getTenderEncriptionData : " + logUserId + " : " + e);
        }
        logger.debug("getTenderEncriptionData : " + logUserId + " Ends");
        return commonSearchData;
    }

    /**
     *Get List of TenderHash for Tender
     * @param tenderId from tbl_TenderMaster
     * @return List of TblTenderHash
     */
    public List<TblTenderHash> getTenderHash(int tenderId) {
        logger.debug("getTenderHash : " + logUserId + " Starts");
        List<TblTenderHash> tenderHash = null;
        try {
            tenderHash = tblTenderHashDao.findTblTenderHash("tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception ex) {
            logger.error("getTenderHash : " + logUserId + " : " + ex);
        }
        logger.debug("getTenderHash : " + logUserId + " Ends");
        return tenderHash;
    }

    /**
     *Inserting Bidding plain data
     * @param action
     * @param tableName
     * @param xmlData
     * @param conditions
     * @return List of CommonMsgChk
     */
    public List<CommonMsgChk> insertTenderBidPlainData(String action, String tableName, String xmlData, String conditions) {
        logger.debug("insertTenderBidPlainData : " + logUserId + " Starts");
        List<CommonMsgChk> commonmsgChk = null;
        try {
            commonmsgChk = spXMLCommon.executeProcedure(action, tableName, xmlData, conditions);
        } catch (Exception e) {
            logger.error("insertTenderBidPlainData : " + logUserId + " : " + e);
        }
        logger.debug("insertTenderBidPlainData : " + logUserId + " Ends");
        return commonmsgChk;
    }

    /**
     * @return the tblTenderBidPlainData
     */
    public TblTenderBidPlainDataImpl getTblTenderBidPlainData() {
        return tblTenderBidPlainData;
    }

    /**
     * @param tblTenderBidPlainData the tblTenderBidPlainData to set
     */
    public void setTblTenderBidPlainData(TblTenderBidPlainDataImpl tblTenderBidPlainData) {
        this.tblTenderBidPlainData = tblTenderBidPlainData;
    }

    /**
     *Get Bidder Plain Data count
     * @param tabName
     * @param condition
     * @return count of Plain Data
     */
    public long getTenderBidPlainDataByFormId(String tabName, String condition) {
        logger.debug("getTenderBidPlainDataByFormId : " + logUserId + " Starts");
        long cont = 0;
        try {
            cont = tblTenderBidPlainData.countForQuery(tabName, condition);
        } catch (Exception ex) {
            logger.error("getTenderBidPlainDataByFormId : " + logUserId + " : " + ex);
        }
        logger.debug("getTenderBidPlainDataByFormId : " + logUserId + " Ends");
        return cont;

    }

    /**
     *Get Forms for the Bidder of the Tender
     * @param tenderId  from tbl_TenderMaster
     * @param stat
     * @param pkgLotId  from tbl_TenderLotSecurity
     * @return array of Forms
     */
    public List<Object> getBidFormIds(int tenderId, String stat, String pkgLotId) {
        logger.debug("getBidFormIds : " + logUserId + " Starts");
        List<Object> listt = null;
        String pkgAppender1 = "";
        String pkgAppender2 = "";
        if (pkgLotId != null && (!pkgLotId.equals("null")) && (!pkgLotId.equals("0"))) {
            pkgAppender1 = "and f3.pkgLotId=" + pkgLotId + " ";
            pkgAppender2 = " and tf.pkgLotId=" + pkgLotId;
        }
        StringBuilder query = new StringBuilder();
        query.append("select distinct(f2.tenderFormid) from TblFinalSubmission f1,TblFinalSubDetail f2,TblTenderForms f3  ");
        query.append("where f1.finalSubmissionId=f2.tblFinalSubmission.finalSubmissionId and f1.tblTenderMaster.tenderId = " + tenderId + " and f3.tenderFormId=f2.tenderFormid ");
        query.append(pkgAppender1 + "and (f3.formStatus is null or f3.formStatus not in ('createp','c'))");

        StringBuilder formQuery = new StringBuilder();
        formQuery.append("select distinct tbd.tenderFormId from ");
        formQuery.append("TblTenderBidPlainData tbd,TblTenderForms tf,TblTenderSection ts,TblTenderStd tsd ");
        formQuery.append("where tbd.tenderFormId = tf.tenderFormId and ts.tenderSectionId  = tf.tblTenderSection.tenderSectionId and ");
        formQuery.append("tsd.tenderStdId  = ts.tblTenderStd.tenderStdId and ");
        formQuery.append("tsd.tenderId=" + tenderId + pkgAppender2);
        List<Object> formList = hibernateQueryDao.singleColQuery(formQuery.toString());

        if (!formList.isEmpty()) {
            query.append(" and f3.tenderFormId not in ");
            StringBuilder temp = new StringBuilder();
            for (Object forms : formList) {
                temp.append(forms);
                temp.append(",");
            }
            query.append("(" + temp.substring(0, temp.length() - 1) + ")");
        }

        if (stat.equals("eval")) {
            query.append(" and f3.isPriceBid='yes'");
        } else if (stat.equals("open")) {
            List<SPTenderCommonData> list = tenderCommonService.returndata("getTenderEnvelopeCount", String.valueOf(tenderId), null);
            if (!list.isEmpty()) {
                if (list.get(0).getFieldName1().equals("2")) {
                    query.append(" and f3.isPriceBid='no'");
                }
            }
        }
        try {
            listt = hibernateQueryDao.getSingleColQuery(query.toString());
        } catch (Exception e) {
            logger.error("getBidFormIds : " + logUserId + " : " + e.toString());
            listt = null;
        }
        logger.debug("getBidFormIds : " + logUserId + " Ends");
        return listt;
    }

    /**
     *Count of TblEvalBidderStatus
     * @param tenderId from tbl_TenderMaster
     * @return count of bidder status
     * @throws Exception
     */
    public long getBidderStatusCount(String tenderId) {
        logger.debug("getBidderStatusCount : " + logUserId + " Starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblEvalBidderStatus tbs", "tbs.tenderId=" + tenderId);
        } catch (Exception e) {
            logger.error("getBidderStatusCount : " + logUserId + " : " + e.toString());
        }
        logger.debug("getBidderStatusCount : " + logUserId + " Ends");
        return lng;
    }

    /**
     *Count of BidPlainData
     * @param formId for the formId
     * @return count of BidPlainData
     * @throws Exception
     */
    public long getBidPlainDataCount(String formId) {
        logger.debug("getBidPlainDataCount : " + logUserId + " Starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblTenderBidPlainData tbd", "tbd.tenderFormId=" + formId);
        } catch (Exception e) {
            logger.error("getBidPlainDataCount : " + logUserId + " : " + e.toString());
        }
        logger.debug("getBidPlainDataCount : " + logUserId + " Ends");
        return lng;
    }

    /**
     *Get Eval and Open committe common member count
     * @param tenderId from tbl_TenderMaster
     * @param userId
     * @return count of common member
     * @throws Exception
     */
    public long getCommonMemberCount(String tenderId, String userId) {
        logger.debug("getCommonMemberCount : " + logUserId + " Starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblCommittee tc,TblCommitteeMembers tcm", "tc.committeeType in ('TOC','POC') and tc.committeeId=tcm.tblCommittee.committeeId and tcm.memberFrom='TEC' and tcm.userId=" + userId + " and tc.tblTenderMaster.tenderId=" + tenderId);
        } catch (Exception e) {
            logger.error("getCommonMemberCount : " + logUserId + " : " + e.toString());
        }
        logger.debug("getCommonMemberCount : " + logUserId + " Ends");
        return lng;
    }

    /**
     * Get Report type for autogenerate report to check whether report is generated or not
     * @param tenderId
     * @return
     */
    public List<Object> getAutoGenerateReportType(int tenderId) {
        logger.debug("getAutoGenerateReportType : " + logUserId + " Starts");
        List<Object> obj = new ArrayList<Object>();
        try {
            obj = hibernateQueryDao.singleColQuery("select isTorter from TblReportMaster where tenderId=" + tenderId);
        } catch (Exception e) {
            logger.error("getAutoGenerateReportType : " + logUserId + " : " + e.toString());
        }
        logger.debug("getAutoGenerateReportType : " + logUserId + " Ends");
        return obj;
    }
/**
 * This method returns count of decrypted data of given tenderid
 * @param tenderId
 * @return count of decrypted data
 */
    public long getDecryptedBidderCount(int tenderId) {
        long countDecryptedData = 0;
        logger.debug("getDecryptedBidderCount : " + logUserId + " Starts");
        try {
            countDecryptedData = hibernateQueryDao.countForNewQuery("TblTenderBidPlainData tbpd , TblTenderForms tfrm , TblTenderSection tsc,TblTenderStd tstd ", "tbpd.tenderFormId in (tfrm.tenderFormId) and tfrm.tblTenderSection.tenderSectionId in (tsc.tenderSectionId) and tsc.tblTenderStd.tenderStdId = (tstd.tenderStdId) and tstd.tenderId = " + tenderId);

        } catch (Exception e) {
            logger.error("getDecryptedBidderCount : " + logUserId + " : " + e.toString());
        }
        logger.debug("getDecryptedBidderCount : " + logUserId + " Ends");
        return countDecryptedData;
    }
}
