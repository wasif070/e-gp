/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCorrigendumOfflineDao;
import com.cptu.egp.eps.dao.storedprocedure.SpDeleteCorrigendumOffline;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author salahuddin
 */
public class CorrigendumDetailsOfflineService {
    static final Logger logger = Logger.getLogger(CorrigendumDetailsOfflineService.class);   
    private String starts = " Starts";
    private String ends = " Ends";
    private TblCorrigendumOfflineDao tblCorrigendumOfflineDao;
    private SpDeleteCorrigendumOffline spDeleteCorrigendumOffline;    
    List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;

    public void insertCorriDetailsOffline(List<TblCorrigendumDetailOffline> tblCorrigendumDetailOffline, Integer userID) {
        logger.debug("insertAllCorriDetails : " + userID + starts);        
        getTblCorrigendumOfflineDao().addTblCorrigendumOfflineDetail(tblCorrigendumDetailOffline);

        logger.debug("insertAllCorriDetails : " + userID + ends);
    }
   
    
    public boolean deleteCorriDetailsOffline(List<TblCorrigendumDetailOffline> tblCorrigendumDetailOffline, int userID, int id) {
        logger.debug("insertAllCorriDetails : " + userID + starts);
        
        Boolean isDelPkgDetail;
        try {
            isDelPkgDetail = getSpDeleteCorrigendumOffline().executeProcedure(id).get(0).getFlag();

        } catch (Exception e) {
            isDelPkgDetail = false;
            logger.error("delCorriDetails : " + userID + e);
        }
        logger.debug("delCorriDetails : " + userID);
        return isDelPkgDetail;                
    }

   /* public void updateCorriDetailsOffline(List<TblCorrigendumDetailOffline> tblCorrigendumDetailOffline, Integer userID) {
        logger.debug("insertAllCorriDetails : " + userID + starts);

        getTblCorrigendumOfflineDao().addTblCorrigendumOfflineDetail(tblCorrigendumDetailOffline);
        logger.debug("insertAllCorriDetails : " + userID + ends);
    }*/



    public List<TblCorrigendumDetailOffline> findCorrigendumDetailOffline(Object... values) throws Exception {
        //logger.debug("findCorrigendumDetailOffline : " + logUserId + starts);
        //logger.debug("findCorrigendumDetailOffline : " + logUserId + ends);
        return tblCorrigendumOfflineDao.findTblCorrigendumDetailOffline(values);
    }

    /**
     * @return the tblCorrigendumOfflineDao
     */
    public TblCorrigendumOfflineDao getTblCorrigendumOfflineDao() {
        return tblCorrigendumOfflineDao;
    }

    /**
     * @param tblCorrigendumOfflineDao the tblCorrigendumOfflineDao to set
     */
    public void setTblCorrigendumOfflineDao(TblCorrigendumOfflineDao tblCorrigendumOfflineDao) {
        this.tblCorrigendumOfflineDao = tblCorrigendumOfflineDao;
    }

    /**
     * @return the spDeleteCorrigendumOffline
     */
    public SpDeleteCorrigendumOffline getSpDeleteCorrigendumOffline() {
        return spDeleteCorrigendumOffline;
    }

    /**
     * @param spDeleteCorrigendumOffline the spDeleteCorrigendumOffline to set
     */
    public void setSpDeleteCorrigendumOffline(SpDeleteCorrigendumOffline spDeleteCorrigendumOffline) {
        this.spDeleteCorrigendumOffline = spDeleteCorrigendumOffline;
    }

    
}
