/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;
import com.cptu.egp.eps.model.table. TblAdvertisement;
import java.util.Date;
import java.util.List;


/**
 *
 * @author sambasiva.sugguna
 */

public interface AdvertisementService {

   public Integer addTblAdvertisement(TblAdvertisement tblAdvertisement)  throws Exception;

   public boolean updateTblAdvertisement(TblAdvertisement tblAdvertisement);

   public List<TblAdvertisement> findTblAdvertisement(Object... values) ;

   public List<TblAdvertisement> getAllTblAdvertisement();

   public List<Object> contentAdmMailId();
   
   public void contentAdmMsgBox(String toEmailId, String fromEmaild, String subject, String msgText);

   public List<Object> getImageIdList();

   public boolean deleteTblAdvertisement(TblAdvertisement adId);

}

