/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblListCellDetail;
import com.cptu.egp.eps.model.table.TblTemplateCells;
import com.cptu.egp.eps.model.table.TblTemplateColumns;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TemplateTableService {
    /**
     * Add Form Table
     * @param tblTables
     * @return true if added or false if not.
     */
    public boolean addFormTable(TblTemplateTables tblTables);
    /**
     * Edit Form Table
     * @param tblTables
     * @return true if updated or false if not.
     */
    public boolean editFormTable(TblTemplateTables tblTables);
    /**
     * Delete Form Table
     * @param tableId
     * @return true if deleted or false if not.
     */
    public boolean deleteFormTable(int tableId);
    /**
     * Get All Form Tables
     * @param templateId
     * @param sectionId
     * @param formId
     * @return List Form Tables.
     */
    public List<TblTemplateTables> getFormTables(short templateId, int sectionId, int formId);
    /**
     * Get Form Table Details.
     * @param tableId
     * @return All Form Tables.
     */
    public List<TblTemplateTables> getFormTablesDetails(int tableId);
    /**
     * Check if entry present in table cols.
     * @param tableId
     * @return true if present or false if not.
     */
    public boolean isEntryPresentInTableCols(int tableId);
    /**
     * Get Table Columns
     * @param tableId
     * @param inSortOrder
     * @return List of Columns.
     */
    public List<TblTemplateColumns> getTableColumns(int tableId, boolean inSortOrder);
    /**
     * Get Table Auto Columns.
     * @param tableId
     * @return List of Template columns.
     */
    public List<TblTemplateColumns> getTableAutoColumns(int tableId);
    /**
     * Get Table Cells.
     * @param tableId
     * @return List of Template Cells.
     */
    public List<TblTemplateCells> getTableCells(int tableId);
    /**
     * Update Column and row count.
     * @param tableId
     * @param noOfRows
     * @param noOfCols
     * @return true if updated or false if not.
     */
    public boolean updateColNRowCnt(int tableId, short noOfRows, short noOfCols);
    /**
     * Get No of Tables in form.
     * @param formId
     * @return no. of tables.
     */
    public short getNoOfTablesInForm(int formId);
    /**
     * Get No. of Cols in table.
     * @param tableId
     * @return no of columns.
     */
    public short getNoOfColsInTable(int tableId);
    /**
     * Get No of Rows in Table.
     * @param tableId
     * @param columnId
     * @return No of Rows
     */
    public short getNoOfRowsInTable(int tableId, short columnId);
    /**
     * Get Table Formulas
     * @param tableId
     * @return List of Table Formula.
     */
    public List<TblTemplateFormulas> getTableFormulas(int tableId);
    /**
     * Add Table Formula
     * @param tblFormulas
     * @return true if added or false if not.
     */
    public boolean addTableFormula(TblTemplateFormulas tblFormulas);
    /**
     * Delete Table Formula
     * @param columns
     * @param tableId
     * @return true if deleted or false if not.
     */
    public boolean deleteTableFormula(String columns, int tableId);
    /**
     * Get List Cell Details.
     * @param tableId
     * @param columnId
     * @param cellId
     * @return List of List Cell Details.
     */
    public List<TblListCellDetail> getListCellDetail(int tableId, short columnId, int cellId);

    /**
     * Set user Id at Service layer.
     * @param userId
     */
    public void setUserId(String userId);
}
