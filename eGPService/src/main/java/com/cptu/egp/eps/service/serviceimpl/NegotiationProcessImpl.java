/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblNegNotifyTendererDao;
import com.cptu.egp.eps.dao.daointerface.TblNegQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblNegRemarksHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblNegReplyDao;
import com.cptu.egp.eps.dao.daointerface.TblNegotiationDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonStoredProcedure;
import com.cptu.egp.eps.dao.storedprocedure.CommonStoredProcedureData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblNegNotifyTenderer;
import com.cptu.egp.eps.model.table.TblNegQuery;
import com.cptu.egp.eps.model.table.TblNegRemarksHistory;
import com.cptu.egp.eps.model.table.TblNegReply;
import com.cptu.egp.eps.model.table.TblNegotiation;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class NegotiationProcessImpl {

    final Logger logger = Logger.getLogger(NegotiationProcessImpl.class);

    SPXMLCommon  pXMLCommon;
    SPTenderCommon sPTenderCommon;
    TblNegotiationDao tblNegotiationDao;
    TblNegNotifyTendererDao tblNegNotifyTendererDao;
    HibernateQueryDao hibernateQueryDao;
    TblNegQueryDao tblNegQueryDao;
    TblNegReplyDao tblNegReplyDao;
    private String logUserId = "0";
    TblNegRemarksHistoryDao tblNegRemarksHistoryDao;
    CommonStoredProcedure commonStoredProcedure;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;
    //Add by dohatec for re-evaluation
	private EvaluationService evaluationService;

    public CommonStoredProcedure getCommonStoredProcedure()
    {
        return commonStoredProcedure;
    }

    public void setCommonStoredProcedure(CommonStoredProcedure commonStoredProcedure)
    {
        this.commonStoredProcedure = commonStoredProcedure;
    }
    
    

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public SPXMLCommon getpXMLCommon() {
        return pXMLCommon;
    }

    public void setpXMLCommon(SPXMLCommon pXMLCommon) {
        this.pXMLCommon = pXMLCommon;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }
    
    public TblNegotiationDao getTblNegotiationDao() {
        return tblNegotiationDao;
    }

    public void setTblNegotiationDao(TblNegotiationDao tblNegotiationDao) {
        this.tblNegotiationDao = tblNegotiationDao;
    }

    public TblNegNotifyTendererDao getTblNegNotifyTendererDao() {
        return tblNegNotifyTendererDao;
    }

    public void setTblNegNotifyTendererDao(TblNegNotifyTendererDao tblNegNotifyTendererDao) {
        this.tblNegNotifyTendererDao = tblNegNotifyTendererDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblNegQueryDao getTblNegQueryDao() {
        return tblNegQueryDao;
    }

    public void setTblNegQueryDao(TblNegQueryDao tblNegQueryDao) {
        this.tblNegQueryDao = tblNegQueryDao;
    }

    public TblNegReplyDao getTblNegReplyDao() {
        return tblNegReplyDao;
    }

    public void setTblNegReplyDao(TblNegReplyDao tblNegReplyDao) {
        this.tblNegReplyDao = tblNegReplyDao;
    }

    public TblNegRemarksHistoryDao getTblNegRemarksHistoryDao() {
        return tblNegRemarksHistoryDao;
    }

    public void setTblNegRemarksHistoryDao(TblNegRemarksHistoryDao tblNegRemarksHistoryDao) {
        this.tblNegRemarksHistoryDao = tblNegRemarksHistoryDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
        /**
     * @return the evaluationService
     */
    public EvaluationService getEvaluationService() {
        return evaluationService;
    }

    /**
     * @param evaluationService the evaluationService to set
     */
    public void setEvaluationService(EvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }
    
    /**
     * Insert new Negotiation.
     * @param negotiation
     * @return id of new inserted negotiation.
     */
    public int addNegotiation(TblNegotiation  negotiation){
        logger.debug("addNegotiation : "+logUserId+" Starts");
        int i = 0;
        String action = "Configure Negotiation by TEC-CP";
        try {
        tblNegotiationDao.addTblNegotiation(negotiation);
                i = negotiation.getNegId();
        } catch (Exception e) {
             logger.error("addNegotiation : "+logUserId+" : "+e);
             action = "Error in "+action+": "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, negotiation.getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Negotiation.getName(), action, negotiation.getNegRemarks());
            action = null;
        }
        logger.debug("addNegotiation : "+logUserId+" Ends");
        return i;
    }

    /**
     * Insert data to negotiation notify tenderer.
     * @param notifyTenderer
     */
    public void addNegotiationNotify(TblNegNotifyTenderer notifyTenderer){
        logger.debug("addNegotiationNotify : "+logUserId+" Starts");
        try {
        tblNegNotifyTendererDao.addTblNegNotifyTenderer(notifyTenderer);
        } catch (Exception e) {
            logger.error("addNegotiationNotify : "+logUserId+" : "+e);
    }
        logger.debug("addNegotiationNotify : "+logUserId+" Ends");
    }

    /**
     * Insert update operation by xml
     * @param param1 action to perform
     * @param param2 table name
     * @param param3 table data in xml formate
     * @param param4 condition
     * @return list of flag of operation.
     */
    public List<CommonMsgChk> InsUpdateNegOperationDetailsByXML(String param1,String param2,String param3,String param4){
        logger.debug("InsUpdateNegOperationDetailsByXML : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        try {
            list = pXMLCommon.executeProcedure(param1, param2, param3,param4);
        } catch (Exception e) {
            logger.error("InsUpdateNegOperationDetailsByXML : "+logUserId+" : "+e);
    }
        logger.debug("InsUpdateNegOperationDetailsByXML : "+logUserId+" Ends");
        return list;
    }

    /**
     * check if any negotiation is pending or not.
     * @param tenderId
     * @return 1 if neg. pending or 0 if false.
     */
	//Change by dohatec for re-evaluation
    public int isAnyNegPending(int tenderId){
        logger.debug("isAnyNegPending : "+logUserId+" Starts");
        int negId=0;
        try {
            int evalCount = evaluationService.getEvaluationNo(tenderId);
            String query="select tbn.negId,tbn.negStatus from TblNegotiation tbn where tbn.tblTenderMaster.tenderId="+tenderId+" and tbn.negStatus='Pending' and evalCount = "+evalCount+"";
            List<Object[]> obj=hibernateQueryDao.createNewQuery(query);
            if(obj != null){
                if(obj.size()>0){
                    negId = (Integer)obj.get(0)[0];
                }
            }
        } catch (Exception e) {
            logger.error("isAnyNegPending : "+logUserId+" : "+e);
        }
        logger.debug("isAnyNegPending : "+logUserId+" Ends");
        return negId;
    }

    /**
     * Add Data to negotiation Query.
     * @param tblNegQuery
     * @return id of new Inserted.
     */
    public int addNegQuery(TblNegQuery tblNegQuery){
        logger.debug("addNegQuery : "+logUserId+" Starts");
        int i =0;
        try {
        tblNegQueryDao.addTblNegQuery(tblNegQuery);
            i=tblNegQuery.getNegQueryId();
        } catch (Exception e) {
            logger.error("addNegQuery : "+logUserId+" : "+e);
    }

        logger.debug("addNegQuery : "+logUserId+" Ends");
        return i;
    }

    /**
     * View all Queries of negotiation
     * @param tenderId
     * @param negId
     * @return List of Negotiation Queries.
     */
    public List<TblNegQuery> viewAllQuery(int tenderId, int negId){
        logger.debug("viewAllQuery : "+logUserId+" Starts");
        List<TblNegQuery> listQuery = null;
        try {
            listQuery = tblNegQueryDao.findTblNegQuery("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId),"negId",Operation_enum.EQ,negId);
        } catch (Exception ex) {
            logger.error("viewAllQuery : "+logUserId+" : "+ex);
        }
        logger.debug("viewAllQuery : "+logUserId+" Ends");
        return listQuery;
    }

    /**
     * Check if Doc is available or not.
     * @param queryId
     * @return true if available or false if not.
     */
    public boolean isDOCAvail(int queryId){
        logger.debug("isDOCAvail : "+logUserId+" Starts");
        long count=0;
        boolean flag;
        try{
            count = hibernateQueryDao.countForNewQuery("TblNegQryDocs tblNegQryDocs", "tblNegQryDocs.negQueryId = '" + queryId + "'");
        }catch(Exception ex){
            logger.error("isDOCAvail : "+logUserId+" : "+ex);
        }
        if(count==0){
            flag = false;}
        else{
            flag = true;}
        logger.debug("isDOCAvail : "+logUserId+" Ends");
        return flag;
    }


    /**
     * Check if Negotiation Doc is available or not.
     * @param negId
     * @return true if neg. Doc available or not.false if neg. Doc is not available. 
     */
    public boolean isNegDOCAvail(int negId){
        logger.debug("isNegDOCAvail : "+logUserId+" Starts");
        long count=0;
        boolean flag;
        try{
            count = hibernateQueryDao.countForNewQuery("TblNegotiationDocs tbl_NegotiationDocs", "tbl_NegotiationDocs.tblNegotiation.negId = '" + negId + "'");
        }catch(Exception ex){
            logger.error("isNegDOCAvail : "+logUserId+" : "+ex);
        }
        if(count==0){
            flag = false;}
        else{
            flag = true;}
        logger.debug("isNegDOCAvail : "+logUserId+" Ends");
        return flag;
    }
    
    /**
     * get Neg Query by Query Id.
     * @param tenderId
     * @param negId
     * @param queryId
     * @return List of Negotiation Queries.
     */
     public List<TblNegQuery> getQueryById(int tenderId,int negId,int queryId){
         logger.debug("getQueryById : "+logUserId+" Starts");
        List<TblNegQuery> detailsQuery  = null;
        try{
             detailsQuery = tblNegQueryDao.findTblNegQuery("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId),"negId",Operation_enum.EQ,negId,"negQueryId",Operation_enum.EQ,queryId);
         }catch(Exception e){
              logger.error("getQueryById : "+logUserId+" : "+e);
         }
        logger.debug("getQueryById : "+logUserId+" Ends");
        return detailsQuery;
    }

     /**
      * Add negotiation Query Reply. 
      * @param tblNegReply
      */
     public void addQueryReply(TblNegReply tblNegReply){
         logger.debug("addQueryReply : "+logUserId+" Starts");
         try{
            tblNegReplyDao.addTblNegReply(tblNegReply);
         }
         catch(Exception ex){
             logger.error("addQueryReply : "+logUserId+" : "+ex);
         }
         logger.debug("addQueryReply : "+logUserId+" Ends");
     }

     /**
      * Get all Negotiation Reply by Query Id.
      * @param queryId
      * @return List of Negotiation Reply.
      */
     public List<TblNegReply> getReplyByQueryId(int queryId){
         logger.debug("getReplyByQueryId : "+logUserId+" Starts");
        List<TblNegReply> listNegReply = null;
        try{
            listNegReply = tblNegReplyDao.findTblNegReply("tblNegQuery",Operation_enum.EQ,new TblNegQuery(queryId));
        }catch(Exception ex){
            logger.error("getReplyByQueryId : "+logUserId+" : "+ex);
        }
         logger.debug("getReplyByQueryId : "+logUserId+" Ends");
        return listNegReply;
     }

     /**
      * Fetch Tenderer Email and Mobile No of user
      * @param userId
      * @return Array of email and Mbile no.
      */
     public Object[] getTendererMailAndMobileNo(int userId){
         logger.debug("getTendererMailAndMobileNo : "+logUserId+" Starts");
         Object[] objects = null;
         try {
            objects =hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId="+userId).get(0);
         } catch (Exception e) {
            logger.error("getTendererMailAndMobileNo : "+logUserId+" : "+e);
         }
         logger.debug("getTendererMailAndMobileNo : "+logUserId+" Ends");
         return objects;
     }

     /**
      * Get final Sub status of negotiation.
      * @param negId
      * @return List of Negotiation.
      */
     public List<TblNegotiation> getFinalSubStatus(int negId){
         logger.debug("getFinalSubStatus : "+logUserId+" Starts");
        List<TblNegotiation> listNegotiation = null;

        try{
            listNegotiation = tblNegotiationDao.findTblNegotiation("negId",Operation_enum.EQ,negId);
        }catch(Exception ex){
            logger.error("getFinalSubStatus : "+logUserId+" : "+ex);
        }
         logger.debug("getFinalSubStatus : "+logUserId+" Ends");
        return listNegotiation;
     }

          /**
      * Get final Sub status of negotiation by tenderer.
      * @param negId
      * @return List of Negotiation.
      */
     public List<TblNegNotifyTenderer> getFinalSubStatusTender(int negId){
         logger.debug("getFinalSubStatus : "+logUserId+" Starts");
        List<TblNegNotifyTenderer> listNegotiation = null;

        try{
            listNegotiation = tblNegNotifyTendererDao.findTblNegNotifyTenderer("tblNegotiation",Operation_enum.EQ,new TblNegotiation(negId));
        }catch(Exception ex){
            logger.error("getFinalSubStatus : "+logUserId+" : "+ex);
        }
         logger.debug("getFinalSubStatus : "+logUserId+" Ends");
        return listNegotiation;
     }

     /**
      * Get Tender Details by tender Id.
      * @param tenderId
      * @return Array of Tender details.
      */
     public Object[] getTenderDetails(int tenderId){
         logger.debug("getTenderDetails : "+logUserId+" Starts");
         Object[] objects = null;
         try {
                objects =hibernateQueryDao.createNewQuery("select ttd.procurementNature,ttd.estCost from TblTenderDetails as ttd where ttd.tblTenderMaster.tenderId = "+tenderId).get(0);
         } catch (Exception e) {
             logger.error("getTenderDetails : "+logUserId+" : "+e);
         }
         logger.debug("getTenderDetails : "+logUserId+" Ends");
         return objects;
     }

     /**
      * Check if Bid agreed or not.
      * @param negId
      * @return true if bid is agree or false if not.
      */
     public boolean isBidAgreed(int negId){
         logger.debug("isBidAgreed : "+logUserId+" Starts");
        List<TblNegNotifyTenderer> listNegNotifyTenderer = null;
        boolean flag = false;
        try{
            listNegNotifyTenderer = tblNegNotifyTendererDao.findTblNegNotifyTenderer("tblNegotiation",Operation_enum.EQ,new TblNegotiation(negId));
            if("".equalsIgnoreCase(listNegNotifyTenderer.get(0).getBidAggree())){
                flag = false;
            }else{
                flag = true;
            }
        }catch(Exception ex){
           logger.error("isBidAgreed : "+logUserId+" : "+ex);
        }
        logger.debug("isBidAgreed : "+logUserId+" Ends");
        return flag;
     }

     /**
      * Get final remarks of Negotiation.
      * @param negId
      * @return Final Remarks.
      */
     public String getFinalRemarks(int negId){
         logger.debug("getFinalRemarks : "+logUserId+" Starts");
         String FinalRemarks = null;
         List<TblNegotiation> negList = null;
         
         try{
            negList = tblNegotiationDao.findTblNegotiation("negId",Operation_enum.EQ,negId);
         }catch(Exception ex){
             logger.error("getFinalRemarks : "+logUserId+" : "+ex);
         }
         if(!negList.isEmpty()){
            FinalRemarks = negList.get(0).getNegFinalSubRemarks();}
         logger.debug("getFinalRemarks : "+logUserId+" Ends");
         return FinalRemarks;
     }

     /**
      * Check Final Submission done for given negotiation.
      * @param negId
      * @return String having value "" indicated submission not done/ "Yes" indicate final submission done
      */
     public String checkFinalNegoFinalSubmissionDone(int negId)
     {
         logger.debug("checkFinalNegoFinalSubmissionDone : "+logUserId+" Starts");
         String status = null;
         List<Object> lst = null;
         
         try{
            lst = hibernateQueryDao.getSingleColQuery("select bidAggree from TblNegNotifyTenderer where negId="+negId);
         }catch(Exception ex){
             logger.error("checkFinalNegoFinalSubmissionDone : "+logUserId+" : "+ex);
         }
         if(lst != null && !lst.isEmpty()){
            status = lst.get(0).toString();
         }
         logger.debug("checkFinalNegoFinalSubmissionDone : "+logUserId+" Ends");
         
         return status;
        
     }

     
      /**
      * Get roundid and bidderid.
      * @param negId
      * @return Object of Negotiation.
      */
     public Object[] getRoundIdandBidderId(int negId){
         logger.debug("getFinalSubStatus : "+logUserId+" Starts");
        List<Object[]> listObjRoundUser = null;
        Object[] objRoundBidder = null;

        try{
            String query = "select distinct tn.roundId,tnnt.userId"
                    + " from TblNegotiation tn ,TblNegNotifyTenderer tnnt"
                    + " where tn.negId = tnnt.tblNegotiation.negId and tn.negId ="+negId;
            listObjRoundUser = hibernateQueryDao.createNewQuery(query);
            if(listObjRoundUser!=null && !listObjRoundUser.isEmpty()){
                objRoundBidder = listObjRoundUser.get(0);
            }
        }catch(Exception ex){
            logger.error("getFinalSubStatus : "+logUserId+" : "+ex);
        }
         logger.debug("getFinalSubStatus : "+logUserId+" Ends");
        return objRoundBidder;
     }
     
     /**
      * Update tbl_NegNotifyTenderer record and insert in to tbl_NegRemarkHistory
      * @param negId
      * @return true of false
      */
     public boolean updateWhenResend(int negId,String status){
         logger.debug("updateWhenResend : "+logUserId+" Starts");
        Object listObj = null;
        Object objRoundBidder = null;
        boolean flag = false;
        try{
            String query = "select remarks from TblNegNotifyTenderer where tblNegotiation.negId = "+negId;
            listObj = tblNegNotifyTendererDao.singleColQuery(query).get(0);
            query = null;
            TblNegRemarksHistory tblNegHis = new TblNegRemarksHistory();
            tblNegHis.setNegId(negId);
            tblNegHis.setRemarks(listObj.toString());
            tblNegRemarksHistoryDao.addTblNegRemarksHistory(tblNegHis);
            query = "update TblNegNotifyTenderer tnnt set tnnt.remarks='"+status+"', tnnt.bidAggree='' where tnnt.tblNegotiation.negId=" +negId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        }catch(Exception ex){
            ex.printStackTrace();
            logger.error("updateWhenResend : "+logUserId+" : "+ex);
            listObj = null;
        }
        listObj = null;
         logger.debug("updateWhenResend : "+logUserId+" Ends");
        return flag;
     }
     /**
      * This method will dump tender bid data for that forms which are not configured for negotiation.
      * @param tenderId
      * @param userId
      * @param negId
      * @return true if operation perform successfully else return false
      */
     public boolean dumpNegForms(String tenderId,String userId, String negId)
     {
         logger.debug("dumpNegForms : "+logUserId+" Starts");
         boolean flag=false;
         
         List<CommonStoredProcedureData> list = new ArrayList<CommonStoredProcedureData>();
         
        try{
            // Setting procedure name
            list = commonStoredProcedure.executeProcedure(tenderId,userId,negId,null,null,null,null,null,null,null);
            
            if(!list.isEmpty() && list.get(0).getFieldName1()!= null && list.get(0).getFieldName1().equalsIgnoreCase("true"))
            {
                flag=true;
            }
        }
        catch(Exception ex)
        {
            logger.error("dumpNegForms : " + logUserId + ex);
        }
         logger.debug("dumpNegForms : "+logUserId+" Starts");
         return flag;
     }
     

     /**
      * This method is use to get Tender Information for specific tender search by tender id.
      * @param tenderId
      * @return Array of Object
      */
	//Change by dohatec for re-evaluation
      public Object[] getNegoTenderInfoFromTenderId(int tenderId)
    {
        logger.debug("getNegoTenderInfoFromTenderId : "+logUserId+" Starts");
        int negId= 0;
         List<Object[]> lst = null;
         Object[] objNego = null;
         int evalCount = evaluationService.getEvaluationNo(tenderId);
        try{
            String query = "select a.negId,b.userId,c.peOfficeName,c.reoiRfpRefNo"
                          + " from TblNegotiation a,TblNegNotifyTenderer b ,TblTenderDetails c "
                          +" where a.negId= b.tblNegotiation.negId and a.tblTenderMaster.tenderId=c.tblTenderMaster.tenderId "
                          + "and a.tblTenderMaster.tenderId="+tenderId+" and a.negStatus='Successful' and a.evalCount = "+evalCount;
            lst=hibernateQueryDao.createNewQuery(query);
            if(lst != null && !lst.isEmpty())
            {
                objNego=lst.get(0);
            }
        }catch(Exception ex){
            logger.error("getNegoTenderInfoFromTenderId : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getNegoTenderInfoFromTenderId : "+logUserId+" Ends");
        return objNego;
    }

      /**
       * This method is use to get Tender Information for specific tender search by user id.
       * @param userId
       * @return Array of Object
       */
       public Object[] getTendererInfoFromUserId(int userId)
    {
        logger.debug("getTendererInfoFromUserId : "+logUserId+" Starts");
        int negId= 0;
         List<Object[]> lst = null;
         Object[] objNego = null;
        try{
            String query = "select tcm.companyName,ttm.firstName,ttm.lastName,tcm.companyId " 
                          +" from  TblCompanyMaster tcm,TblTendererMaster ttm ,TblLoginMaster tlm "
                          +" where  tcm.companyId=ttm.tblCompanyMaster.companyId  and tlm.userId=ttm.tblLoginMaster.userId and "
                          +" ttm.tblLoginMaster.userId="+userId;

            lst=hibernateQueryDao.createNewQuery(query);
            if(lst != null && !lst.isEmpty())
            {
                objNego=lst.get(0);
            }
        }catch(Exception ex){
            logger.error("getTendererInfoFromUserId : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getTendererInfoFromUserId : "+logUserId+" Ends");
        return objNego;
    }
      /**
       * Get Notify Tender details.
       * @param negId
       * @return Object of TblNegNotifyTenderer
       */
       public TblNegNotifyTenderer getUserIdFromNegId(int negId){
           List<TblNegNotifyTenderer> list = new ArrayList<TblNegNotifyTenderer>();
           TblNegNotifyTenderer tblNegNotifyTenderer = new TblNegNotifyTenderer();
           try {
               list = tblNegNotifyTendererDao.findTblNegNotifyTenderer("tblNegotiation",Operation_enum.EQ,new TblNegotiation(negId));
               if(list!=null && !list.isEmpty()){
                   tblNegNotifyTenderer = list.get(0);
               }
               
           } catch (Exception e) {
           }
           return tblNegNotifyTenderer;
       }


      
}
