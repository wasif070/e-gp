/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.CommonRssMsg;
import com.cptu.egp.eps.dao.storedprocedure.SPRss;
import com.cptu.egp.eps.service.serviceinterface.RssService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *Implement the RssService Interface
 * @author Administrator
 */
public class RssServiceImpl implements RssService {

    static final Logger LOGGER = Logger.getLogger(RssServiceImpl.class);
    private String logUserId = "0";
    SPRss spRss;

    /**
     * Getter of SPRss
     * @return
     */
    public SPRss getSpRss() {
        return spRss;
    }

    /**
     * Setter of SPRss
     * @param spRss
     */
    public void setSpRss(SPRss spRss) {
        this.spRss = spRss;
    }

    @Override
    public String getRssFeed(String tag, String path) {
        LOGGER.debug("getRssFeed : " + logUserId + " Starts");
        List<CommonRssMsg> list = spRss.executeSPRssProcedure(tag, path);
        LOGGER.debug("getRssFeed : " + logUserId + " Ends");
        return list.get(0).getRssXml();
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
}
