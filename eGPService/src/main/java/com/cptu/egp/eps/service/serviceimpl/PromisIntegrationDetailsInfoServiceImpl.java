package com.cptu.egp.eps.service.serviceimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.storedprocedure.PromisIntegrationDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPSharedPEDetailForPromis;
import com.cptu.egp.eps.service.serviceinterface.PromisIntegrationDetailsInfoService;
import java.util.List;
import org.apache.log4j.Logger;

public class PromisIntegrationDetailsInfoServiceImpl implements PromisIntegrationDetailsInfoService {

    private final Logger logger = Logger.getLogger(PromisIntegrationDetailsInfoServiceImpl.class);
    private String logUserId = "0";
    private SPSharedPEDetailForPromis spSharedPEDetailForPromis;

    public SPSharedPEDetailForPromis getSpSharedPEDetailForPromis() {
        return spSharedPEDetailForPromis;
    }

    public void setSpSharedPEDetailForPromis(SPSharedPEDetailForPromis spSharedPEDetailForPromis) {
        this.spSharedPEDetailForPromis = spSharedPEDetailForPromis;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method returns the all the records of sharedPEdetails for
     * promis system.
     * @return List of PromisIntegrationDetails
     * @throws Exception
     */
    @Override
    public List<PromisIntegrationDetails> getSharedPEDetailForPromis() throws Exception {
        logger.debug("getSharedPEDetailForPromis : " + logUserId + " Starts");
        List<PromisIntegrationDetails> sharedPEDetailForPromisList = null;
        try {
            logger.debug("PromisIntegrationDetailsInfoServiceImpl 1");
            sharedPEDetailForPromisList = spSharedPEDetailForPromis.executeProcedure();
            logger.debug("PromisIntegrationDetailsInfoServiceImpl 2:- Records " + sharedPEDetailForPromisList.size());
        } catch (Exception ex) {
            logger.error("getSharedPEDetailForPromis : " + logUserId + " : " + ex.toString());
            //logger.debug("Exception PromisIntegrationDetailsInfoServiceImpl:-" + ex.toString());
        }
        logger.debug("getSharedPEDetailForPromis : " + logUserId + " Ends");
        //  logger.debug("PromisIntegrationDetailsInfoServiceImpl:-"+sharedPEDetailForPromisList);
        return sharedPEDetailForPromisList;
    }
}
