/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDebarredBiddingPermission;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public interface DebarredBiddingPermissionService {

    public void addDebarredBiddingPermission(TblDebarredBiddingPermission tblDebarredBiddingPermission);
    public String updateDebarredBiddingList(List<TblDebarredBiddingPermission> debarPermissions, String debarredUser, String companyId);
    public List<TblDebarredBiddingPermission> findTblDebarredBiddingPermission(Object... values) throws Exception ;
    public boolean deleteDebarredBiddingPermission(int debarredUser, int companyId, int userId, String reinstateComments, String category);
    public boolean UpdateDebarredBiddingPermission(String debaruserId, String category, String fDate, String uptoDate);
    
}
