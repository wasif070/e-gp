/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author rishita
 */
public class PEOfficeCreationServiceImpl implements PEOfficeCreationService {

    static final Logger LOGGER = Logger.getLogger(PEOfficeCreationServiceImpl.class);
    TblOfficeMasterDao tblOfficeMasterDao;
    TblDepartmentMasterDao tblDepartmentMasterDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private static final String START = " Starts";
    private static final String END = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    public TblOfficeMasterDao getTblOfficeMasterDao() {
        return tblOfficeMasterDao;
    }

    public void setTblOfficeMasterDao(TblOfficeMasterDao tblOfficeMasterDao) {
        this.tblOfficeMasterDao = tblOfficeMasterDao;
    }
    
    
    @Override
    public Short getDzongkhagId(String dzongkhag) throws Exception {
        //LOGGER.debug("getDepartmentIdByUserId : " + logUserId + loggerStart);
        Short dzongkhagId = 0;
        try {
            String departmentType="District";
            String query = "select d.departmentId from TblDepartmentMaster d where d.departmentName = " + dzongkhag+"d.departmentType="+departmentType;
            dzongkhagId = ((Short) (hibernateQueryDao.getSingleColQuery(query).toArray())[0]);
        } catch (Exception e) {
            LOGGER.error("getDzongkhagId : " + logUserId + e);
    }
        //LOGGER.debug("getDzongkhagId : " + logUserId + loggerEnd);
        return dzongkhagId;
    }
    
    
    

    @Override
    public boolean addPEOfficeCreation(TblOfficeMaster tblOfficeMaster) {
        LOGGER.debug("addPEOfficeCreation : " + logUserId + START);
        boolean flag = false;
        String action = null;
        String deptType=null;
       
        deptType=getDepartmentType(tblOfficeMaster.getDepartmentId());
      
        try 
        {
            tblOfficeMasterDao.addTblOfficeMaster(tblOfficeMaster);
            flag = true;
            
            if(deptType != null && deptType.equalsIgnoreCase("Organization"))
            {
                action = "Create PE Office for Organization";
            }
            else if(deptType != null && deptType.equalsIgnoreCase("Division"))
            {
                action = "Create PE Office for Division";
            }
            else
            {
                action = "Create PE Office for Ministry";
            }   
        } catch (Exception e) {
            LOGGER.error("addPEOfficeCreation : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblOfficeMaster.getOfficeId(), "officeId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("addPEOfficeCreation : " + logUserId + END);
        return flag;
    }

    @Override
    public List<TblDepartmentMaster> findTblOfficeMaster(Object... values) {
        LOGGER.debug("findTblOfficeMaster : " + logUserId + START);
        List<TblDepartmentMaster> list = null;
        try {
            list = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception e) {
            LOGGER.error("findTblOfficeMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblOfficeMaster : " + logUserId + END);
        return list;
    }

    @Override
    public long getAllCountOfPEMaster() {
        LOGGER.debug("getAllCountOfPEMaster : " + logUserId + START);
        long lng = 0;
        try {
            lng = tblOfficeMasterDao.getTblOfficeMasterCount();
        } catch (Exception e) {
            LOGGER.error("getAllCountOfPEMaster : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfPEMaster : " + logUserId + END);
        return lng;
    }

    @Override
    public boolean updatePEOfficeMaster(TblOfficeMaster tblOfficeMaster) {
        LOGGER.debug("updatePEOfficeMaster : " + logUserId + START);
        boolean flag = false;
        String action = null;
        String deptType=null;
        deptType=getDepartmentType(tblOfficeMaster.getDepartmentId());
        try {
            tblOfficeMasterDao.updateTblOfficeMaster(tblOfficeMaster);
            flag = true;
            if(deptType != null && deptType.equalsIgnoreCase("Organization"))
            {
                action = "Edit PE Office for Organization";
            }
            else if(deptType != null && deptType.equalsIgnoreCase("Division"))
            {
                action = "Edit PE Office for Division";
            }
            else
            {
                action = "Edit PE Office for Ministry";
            }   
        } catch (Exception e) {
              action = "Error in "+action+" "+e.getMessage();
            LOGGER.error("updatePEOfficeMaster : " + logUserId + e);
        }
        finally
        {
            makeAuditTrailService.generateAudit(auditTrail, tblOfficeMaster.getOfficeId(), "officeId", EgpModule.Manage_Users.getName(), action, "");
            action = null;  
        }
        LOGGER.debug("updatePEOfficeMaster : " + logUserId + END);
        return flag;
    }

    @Override
    public List<TblOfficeMaster> findPEOfficeMaster(int firstResult, int maxResult, Object... values) {
        LOGGER.debug("findPEOfficeMaster : " + logUserId + START);
        List<TblOfficeMaster> list = null;
        try {
            list = tblOfficeMasterDao.findByCountEntity(firstResult, maxResult, values);
        } catch (Exception e) {
            LOGGER.error("findPEOfficeMaster : " + logUserId + e);
        }
        LOGGER.debug("findPEOfficeMaster : " + logUserId + END);
        return list;
    }

    @Override
    public List<TblOfficeMaster> findOfficeMaster(Object... values) {
        LOGGER.debug("findOfficeMaster : " + logUserId + START);
       
        List<TblOfficeMaster> list = null;
        try {
            list = tblOfficeMasterDao.findTblOfficeMaster(values);
           
        } catch (Exception e) {
            LOGGER.error("findOfficeMaster : " + logUserId + e);
            
        } 
        LOGGER.debug("findOfficeMaster : " + logUserId + END);
        return list;
    }

    @Override
    public List<TblOfficeMaster> getAllOffices() {
        LOGGER.debug("getAllOffices : " + logUserId + START);
        List<TblOfficeMaster> list = null;
        try {
            list = tblOfficeMasterDao.getAllTblOfficeMaster();
        } catch (Exception e) {
            LOGGER.error("getAllOffices : " + logUserId + e);
        }
        LOGGER.debug("getAllOffices : " + logUserId + END);
        return list;
    }

    @Override
    public List<Object[]> findPEOfficeMasterList(int firstResult, int maxResult, String deptType, String ascClause) {
        LOGGER.debug("findPEOfficeMasterList : " + logUserId + START);
        List<Object[]> list = null;
        try {
            String TypeOfAscClause = "";
            if (ascClause.contains("departmentName")) {
                TypeOfAscClause = "tdm";
            } else if (ascClause.contains("district")) {
                TypeOfAscClause = "tom";
                ascClause = ascClause.replace("district", "tblStateMaster.stateName");
            } else {
                TypeOfAscClause = "tom";
            }
            list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "'order by " + TypeOfAscClause + "." + ascClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("findPEOfficeMasterList : " + logUserId + e);
        }
        LOGGER.debug("findPEOfficeMasterList : " + logUserId + END);
        return list;
    }

    @Override
    public List<Object[]> findPEOfficeMasterList(int firstResult, int maxResult, String deptType, String searchField, String searchString, String searchOper) {
        LOGGER.debug("findPEOfficeMasterList : " + logUserId + START);
        List<Object[]> list = null;
        try {
            if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            } else if (searchField.equalsIgnoreCase("district")) {
                searchField = "tom.tblStateMaster.stateName";// + searchField;
            } else {
                searchField = "tom." + searchField;
            }
            if (searchOper.equalsIgnoreCase("EQ")) {
                list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "' and " + searchField + " = '" + searchString + "' order by tom.creationDate desc", firstResult, maxResult);
            } else if (searchOper.equalsIgnoreCase("CN")) {
                list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "' and " + searchField + " LIKE '%" + searchString + "%' order by tom.creationDate desc", firstResult, maxResult);
            }
        } catch (Exception e) {
            LOGGER.error("findPEOfficeMasterList : " + logUserId + e);
        }
        LOGGER.debug("findPEOfficeMasterList : " + logUserId + END);
        return list;
    }

    @Override
    public long getAllCountOfPEMasterList(String deptType) {
        LOGGER.debug("getAllCountOfPEMasterList : " + logUserId + START);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "'");
        } catch (Exception e) {
            LOGGER.error("getAllCountOfPEMasterList : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfPEMasterList : " + logUserId + END);
        return lng;
    }

    @Override
    public long getSearchCountOfPEMasterList(String deptType, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfPEMasterList : " + logUserId + START);
        long lng = 0;
        try {
            if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            } else if (searchField.equalsIgnoreCase("district")) {
                searchField = "tom.tblStateMaster.stateName";// + searchField;
            } else {
                searchField = "tom." + searchField;
            }
            if (searchOper.equalsIgnoreCase("EQ")) {
                lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "' and " + searchField + " = '" + searchString + "'");
            } else if (searchOper.equalsIgnoreCase("CN")) {
                lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like '" + deptType + "' and " + searchField + " LIKE '%" + searchString + "%'");
            } else {
                lng = 0;
            }
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfPEMasterList : " + logUserId + e);
        }
        LOGGER.debug("getSearchCountOfPEMasterList : " + logUserId + END);
        return lng;
    }

    @Override
    public long getAllCountOfPEMasterListOrg(String deptType, int userId) {
        LOGGER.debug("getAllCountOfPEMasterListOrg : " + logUserId + START);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "'");
        } catch (Exception e) {
            LOGGER.error("getAllCountOfPEMasterListOrg : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfPEMasterListOrg : " + logUserId + END);
        return lng;
    }

    @Override
    public long getSearchCountOfPEMasterListOrg(String deptType, int userId, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfPEMasterListOrg : " + logUserId + START);
        long lng = 0;
        try {
            if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            } else if (searchField.equalsIgnoreCase("district")) {
                searchField = "tom.tblStateMaster.stateName";// + searchField;
            } else {
                searchField = "tom." + searchField;
            }
            if (searchOper.equalsIgnoreCase("EQ")) {
                lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "' and " + searchField + " = '" + searchString + "'");
            } else if (searchOper.equalsIgnoreCase("CN")) {
                lng = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "' and " + searchField + " LIKE '%" + searchString + "%'");
            } else {
                lng = 0;
            }
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfPEMasterListOrg : " + logUserId + e);
        }
        LOGGER.debug("getSearchCountOfPEMasterListOrg : " + logUserId + END);
        return lng;
    }

    @Override
    public List<TblDepartmentMaster> findDepartmentName(Object... values) {
        LOGGER.debug("findDepartmentName : " + logUserId + START);
        List<TblDepartmentMaster> list = null;
        try {
            list = tblDepartmentMasterDao.findEntity(values);
        } catch (Exception e) {
            LOGGER.error("findDepartmentName : " + logUserId + e);
        }
        LOGGER.debug("findDepartmentName : " + logUserId + END);
        return list;
    }

    @Override
    public List<Object[]> findPEOfficeMasterListORG(int firstResult, int maxResult, String deptType, int userId, String ascClause) {
        LOGGER.debug("findPEOfficeMasterListORG : " + logUserId + START);
        List<Object[]> list = null;
        try {
            String TypeOfAscClause = "";
            if (ascClause.contains("departmentName")) {
                TypeOfAscClause = "tdm";
            } else if (ascClause.contains("district")) {
                TypeOfAscClause = "tom";
                ascClause = ascClause.replace("district", "tblStateMaster.stateName");
            } else {
                TypeOfAscClause = "tom";
            }
            list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "'order by " + TypeOfAscClause + "." + ascClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("findPEOfficeMasterListORG : " + logUserId + e);
        }
        LOGGER.debug("findPEOfficeMasterListORG : " + logUserId + END);
        return list;
    }

    @Override
    public List<TblOfficeMaster> getPEOfficeList(int deptId, int districtId) {
        LOGGER.debug("getPEOfficeList : " + logUserId + START);
        List<TblOfficeMaster> lsttblOfficeMaster = null;
        try {
            List<Object[]> objs = new ArrayList<Object[]>();
            if(districtId!=0){
                objs = hibernateQueryDao.createNewQuery("select tom.officeId,tom.officeName from TblOfficeMaster tom where tom.departmentId = " + deptId + " and tom.tblStateMaster.stateId = " + districtId);
            }else{
                objs = hibernateQueryDao.createNewQuery("select tom.officeId,tom.officeName from TblOfficeMaster tom where tom.departmentId = " + deptId);
            }
            
            lsttblOfficeMaster = new ArrayList<TblOfficeMaster>();

            for (Object[] i : objs) {
                lsttblOfficeMaster.add(new TblOfficeMaster(Integer.parseInt(i[0].toString()), i[1].toString()));
            }
        } catch (Exception e) {
            LOGGER.error("getPEOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getPEOfficeList : " + logUserId + END);
        return lsttblOfficeMaster;
    }

    @Override
    public List<Object[]> findPEOfficeMasterListORG(int firstResult, int maxResult, String deptType, int userId, String searchField, String searchString, String searchOper) {
        LOGGER.debug("findPEOfficeMasterListORG : " + logUserId + START);
        List<Object[]> list = null;
        try {
            if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            } else if (searchField.equalsIgnoreCase("district")) {
                searchField = "tom.tblStateMaster.stateName";// + searchField;
            } else {
                searchField = "tom." + searchField;
            }
            if (searchOper.equalsIgnoreCase("EQ")) {
                list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "' and " + searchField + " ='" + searchString + "' order by tom.creationDate desc", firstResult, maxResult);
            } else if (searchOper.equalsIgnoreCase("CN")) {
                list = hibernateQueryDao.createByCountNewQuery("select tom.officeId,tom.officeName,tom.address,tom.tblStateMaster.stateName,tdm.departmentName from TblDepartmentMaster tdm, TblOfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.approvingAuthorityId = " + userId + " and tdm.departmentType like '" + deptType + "' and " + searchField + " LIKE '%" + searchString + "%' order by tom.creationDate desc", firstResult, maxResult);
            }
        } catch (Exception e) {
            LOGGER.error("findPEOfficeMasterListORG : " + logUserId + e);
        }
        LOGGER.debug("findPEOfficeMasterListORG : " + logUserId + END);
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    public String getDepartmentType(int deptId)
    {
        String deptType=null;
         LOGGER.debug("getDepartmentType : " + logUserId + "starts");
        List<TblDepartmentMaster> list = new ArrayList<TblDepartmentMaster>();
        try {
            list = tblDepartmentMasterDao.findTblDepartmentMaster("departmentId", Operation_enum.EQ, (short)deptId);
            if(list != null && list.size() > 0)
            {
                deptType=list.get(0).getDepartmentType();
            }
        } catch (Exception ex) {
            LOGGER.error("getDepartmentType : " + logUserId + ex.toString());
            ex.printStackTrace();
        }
        LOGGER.debug("getDepartmentType : " + logUserId + "ends");
        
        return deptType;
        
    }
    
}
