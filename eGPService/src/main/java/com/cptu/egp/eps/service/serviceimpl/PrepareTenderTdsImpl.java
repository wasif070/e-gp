/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCorrigendumDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderIttClauseDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderTdsSubClauseDao;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import com.cptu.egp.eps.model.table.TblTenderTdsSubClause;
import com.cptu.egp.eps.service.serviceinterface.PrepareTenderTds;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>PrepareTenderTdsImpl</b> <Description Goes Here> Dec 4, 2010 9:17:35 PM
 * @author yanki
 */
public class PrepareTenderTdsImpl implements PrepareTenderTds {

    static final Logger logger = Logger.getLogger(PrepareTenderTdsImpl.class);
    SPTenderCommon sPTenderCommon;
    HibernateQueryDao hibernateQueryDao;
    TblTenderIttClauseDao tblTenderIttClauseDao;
    TblTenderTdsSubClauseDao tblTenderTdsSubClauseDao;
    TblCorrigendumDetailDao tblCorrigendumDetailsDao;
    private String logUserId = "0";

     public TblCorrigendumDetailDao getTblCorrigendumDetailsDao() {
        return tblCorrigendumDetailsDao;
    }

    public void setTblCorrigendumDetailsDao(TblCorrigendumDetailDao tblCorrigendumDetailsDao) {
        this.tblCorrigendumDetailsDao = tblCorrigendumDetailsDao;
    }
    public TblTenderIttClauseDao getTblTenderIttClauseDao() {
        return tblTenderIttClauseDao;
    }

    public void setTblTenderIttClauseDao(TblTenderIttClauseDao tblTenderIttClauseDao) {
        this.tblTenderIttClauseDao = tblTenderIttClauseDao;
    }

    public TblTenderTdsSubClauseDao getTblTenderTdsSubClauseDao() {
        return tblTenderTdsSubClauseDao;
    }

    public void setTblTenderTdsSubClauseDao(TblTenderTdsSubClauseDao tblTenderTdsSubClauseDao) {
        this.tblTenderTdsSubClauseDao = tblTenderTdsSubClauseDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    @Override
    public List<SPTenderCommonData> getTenderTDSSubClause(int headerId) {
        logger.debug("getTenderTDSSubClause : " + logUserId + " Starts");
        List<SPTenderCommonData> sPTenderCommonData;
        try {
            sPTenderCommonData = sPTenderCommon.executeProcedure("getTenderTdsInfo", headerId + "", "");
        } catch (Exception ex) {
            logger.error("getTenderTDSSubClause : " + logUserId + ex);
            sPTenderCommonData = null;
        }
        logger.debug("getTenderTDSSubClause : " + logUserId + " Ends");
        return sPTenderCommonData;
    }

    @Override
    public boolean updateTDSSubClause(TblTenderTdsSubClause tblTenderTdsSubClause) {
        logger.debug("updateTDSSubClause : " + logUserId + " Starts");
        hibernateQueryDao.updateDeleteNewQuery("update TblTenderTdsSubClause set tenderTdsClauseName = '"
                + tblTenderTdsSubClause.getTenderTdsClauseName() + "' where tenderTdsSubClauseId = '"
                + tblTenderTdsSubClause.getTenderTdsSubClauseId() + "'");
        logger.debug("updateTDSSubClause : " + logUserId + " Ends");
        return true;
    }

    @Override
    public boolean save(TblTenderTdsSubClause tblTenderTdsSubClause) {
        logger.debug("save : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            tblTenderTdsSubClauseDao.addTblTenderTdsSubClause(tblTenderTdsSubClause);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("save : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("save : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public boolean delete(int tdsSubClauseId) {
        hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderTdsSubClause where tenderTdsSubClauseId = "
                + tdsSubClauseId);
        return true;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public boolean updateAllClause(List<TblTenderTdsSubClause> updateList) {
        logger.debug("updateAllClause : " + logUserId + " Starts");
        boolean flag =false;
        try {
            for (TblTenderTdsSubClause tblTenderTdsSubClause : updateList) {
                hibernateQueryDao.updateDeleteNewQuery("update TblTenderTdsSubClause set tenderTdsClauseName = '"
                + tblTenderTdsSubClause.getTenderTdsClauseName() + "' where tenderTdsSubClauseId = '"
                + tblTenderTdsSubClause.getTenderTdsSubClauseId() + "'");
            }
            
            flag = true;
        } catch (Exception e) {
            logger.error("updateAllClause : " + logUserId + " error:");
        }
        
        logger.debug("updateAllClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean insertAllClause(List<TblTenderTdsSubClause> addList) {
        logger.debug("updateTDSSubClause : " + logUserId + " Starts");
        boolean flag =false;
        try {
            for (TblTenderTdsSubClause tblTenderTdsSubClause : addList) {
                tblTenderTdsSubClauseDao.addTblTenderTdsSubClause(tblTenderTdsSubClause);
            }
            
            flag = true;
        } catch (Exception e) {
            logger.error("updateTDSSubClause : " + logUserId + " error:");
        }
        
        logger.debug("updateTDSSubClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean saveOrUpdateCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail) {

        boolean flag=false;
        try{
            if(tblCorrigendumDetail.getCorriDetailId()==0)
            tblCorrigendumDetailsDao.addTblCorrigendumDetail(tblCorrigendumDetail);
            else
             tblCorrigendumDetailsDao.updateTblCorrigendumDetail(tblCorrigendumDetail);
            return true;
        }
        catch(Exception e){
            logger.error("Error in inserting corrigendum detail:"+e.toString());
            return false;
        }

    }
        }
