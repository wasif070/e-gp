/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import java.util.List;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public interface MessageProcessingService {

    public List getInboxData(int id, String messageBoxType,String messType,Integer noOfRecords,
             Integer pageNumber,Integer totalPages,Integer folderId,String searching,String keywords,
            String emialId,String dateFrom,String dateTo,String viewMsg,Integer msgid,String columnName,String order,String sortOpt);

    public void setLogUserId(String logUserId);

    /**
     * Generate Default Archive Folder for all user who does not have generated Archive Folder.
     * @param userId = tbl_MessageFolder.userId
     * @return = tbl_MessageFolder.msgFolderId
     */
    public int createDefaultArchiveFolder(int userId);

    /**
     * Check Archive Folder Exist for given User Id
     * @param uid = tbl_MessageFolder.userId
     * @return = tbl_MessageFolder.msgFolderId
     */
    public int checkArchiveFolderExist(int uid);

    public boolean deleteTrashMessages(int MsgId);
}
