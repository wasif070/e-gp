/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTscstatus;
import java.util.List;

/**
 *
 * @author Administrator
 */

public interface TblTscstatusService {

        public List<Object[]> getMemList(int userId);
        public boolean addData(TblTscstatus tblTscstatus);
        public int tscStatusId(int tenderId);
        public boolean updateData(TblTscstatus tblTscstatus);
        public String getStatus(int tenderId);
        public List<TblTscstatus> getDataforUpdate(int tenderId);
        public void setUserId(String userId);
}
