/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTscstatusDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTscstatus;
import com.cptu.egp.eps.service.serviceinterface.TblTscstatusService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TblTscstatusServiceImpl implements TblTscstatusService {

    HibernateQueryDao hibernateQueryDao;
    TblTscstatusDao tblTscStatusDao;
    private String logUserId = "0";
    static final Logger logger = Logger.getLogger(TblTscstatusServiceImpl.class);

    public TblTscstatusDao getTblTscStatusDao() {
        return tblTscStatusDao;
    }

    public void setTblTscStatusDao(TblTscstatusDao tblTscStatusDao) {
        this.tblTscStatusDao = tblTscStatusDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public List<Object[]> getMemList(int userId) {
        List<Object[]> Result = null;
        logger.debug("getMemList : " + logUserId + "starts");
        try {

            Result = hibernateQueryDao.createNewQuery("select employeeName from TblEmployeeMaster where tblLoginMaster.userId" + userId);
        } catch (Exception ex) {
           logger.error("getBranchAdminListGrid : " +ex);
            Result = null;
        }
        logger.debug("getMemList : " + logUserId + "ends");
        return Result;
    }

    @Override
    public boolean addData(TblTscstatus tblTscstatus) {
        logger.debug("addData : " + logUserId + "starts");
        boolean flag = true;
        try {
        tblTscStatusDao.addTblTscstatus(tblTscstatus);

        } catch (Exception ex) {
            logger.error("addData : " + ex);
            flag = false;
        }
        logger.debug("addData : " + logUserId + "ends");
        return flag;

    }

    @Override
    public int tscStatusId(int tenderId) {
        logger.debug("tscStatusId : " + logUserId + "starts");
        List<TblTscstatus> list = null;
        int Result = -1;
        try {
            list = tblTscStatusDao.findTblTscstatus("tenderId", Operation_enum.EQ, tenderId);
            if (list.size() > 0) {
            int id = list.get(0).getTscStatusId();
                Result = id;
            } else {
                Result = -1;
            }
        } catch (Exception ex) {

            logger.error("tscStatusId" + ex);
            Result = -1;
        }
        logger.debug("tscStatusId : " + logUserId + "ends");
        return Result;

    }

    @Override
    public boolean updateData(TblTscstatus tblTscstatus) {
        logger.debug("updateData : " + logUserId + "starts");
        boolean flag = true;
        try {
        tblTscStatusDao.updateTblTscstatus(tblTscstatus);

        } catch (Exception ex) {
            logger.error("updateData" + ex);
            flag = false;
        }
        logger.debug("updateData : " + logUserId + "ends");
        return flag;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getStatus(int tenderId) {
        //throw new UnsupportedOperationException("Not supported yet.");
         logger.debug("getStatus : "+logUserId+"starts");
        String Result = "";
        List<TblTscstatus> list = null;
        try {
            list = tblTscStatusDao.findTblTscstatus("tenderId", Operation_enum.EQ, tenderId);
            if (list != null && list.size() > 0) {
                    String status = list.get(0).getStatus();
                Result = status;
            } else {
                Result = "Error";
                }
        } catch (Exception ex) {
            logger.error("getStatus : " + ex);
            Result = "Error";
        }
         logger.debug("getStatus : "+logUserId+"ends");
        return Result;
    }

    @Override
    public List<TblTscstatus> getDataforUpdate(int tenderId) {
           logger.debug("getDataforUpdate : "+logUserId+"starts");
        List<TblTscstatus> Result = null;
        try {
            Result = tblTscStatusDao.findTblTscstatus("tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception ex) {
           logger.error("getDataforUpdate : "+ex);
            Result = null;
        }
         logger.debug("getDataforUpdate : "+logUserId+"ends");
        return Result;
    }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }
}
