/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn;
import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblEmployeeFinancialPower;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEmployeeOffices;
import com.cptu.egp.eps.model.table.TblEmployeeRoles;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.view.VwEmpfinancePower;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author parag
 */
public interface GovtUserCreationService {

    /**
     *create government user value inserted in tblLoginMaster and tblEmployeeMaster
     * @param tblLoginMaster
     * @param tblEmployeeMaster
     * @return if created successfully then return employeeId else 0
     */
    public int createGovtUser(TblLoginMaster tblLoginMaster,TblEmployeeMaster tblEmployeeMaster, int oOffice);

    /**
     * create government user specific role , table tblEmployeeRoles
     * @param tblEmployeeRoles
     */
    public void createGovtUserRole(String query);

     /**
      * create employee offices value inserted in tblEmployeeOffices
      * @param tblEmployeeOffices
      */
     public void createEmployeeOffices(TblEmployeeOffices tblEmployeeOffices);

     /**
      * Gives list of offices.
      * @param deptId - departmentId of Tbl_OfficeMaster
      * @return list of TblOfficeMaster
      */
     List<TblOfficeMaster> officeMasterList(int deptId);

     /**
      * Gives deoartment list.
      * @return list of TblDepartmentMaster
      */
     List<TblDepartmentMaster> departmentMasterList();

     /**
      * Gives designation list.
      * @param deptId
      * @return list of TblDesignationMaster
      */
     List<TblDesignationMaster> designationMasterList(short deptId);

     /**
      * Update user data in tblLoginMaster
      * @param userId
      * @return true if updated successfully
      */
     public boolean updateLoginMaster(int userId);

     /**
      * Update employee data in EmployeeMaster
      * @param empId
      * @param finPowerBy
      * @param designationId
      * @return true if updated successfully
      */
     public boolean updateEmployeeMaster(int empId,String finPowerBy,int designationId);

     /**
      * Gives procurement role detail.
      * @param empId - employeeId
      * @return procurement role
      */
     public String getProcurementRole(int empId);

     /**
      * Gives budget types.
      * @return list of TblBudgetType
      */
     public List<TblBudgetType>  getBudgetTypes();

     /**
      * Inserted employee financial power in tbl_EmployeeFinancialPower.
      * @param tblEmployeeFinancialPower
      */
     public void addGovtUserFinancePower(TblEmployeeFinancialPower tblEmployeeFinancialPower);

     // for Edit ---

     /**
      * Get government user data.
      * @param empId EmployeeId from  tbl_employeeMaster
      * @return list of Object [0] emailId,[1] password,[2] employeeName, [3] employeeNameBangla,[4] mobileNo,[5] nationalId,[6] designationId
      */
     public List<Object[]> getGovtUserData(int empId,String...values);

     /**
      * Update government user data.
      * @param tblEmployeeMaster
      * @param empId 
      * @param userId
      * @param dId
      * @param officeId
      * @return if created successfully then return employeeId else 0
      */
     public int editGovtUser(TblEmployeeMaster tblEmployeeMaster,int empId,int userId, int dId, int officeId);

     /**
      * Get government employee data.
      * @param empId
      * @return List of TblEmployeeRoles
      */
     public List<TblEmployeeRoles> getGovtEmpData(int empId);

     /**
      * Get government employee office data.
      * @param empId
      * @return List of TblEmployeeOffices
      */
     public List<TblEmployeeOffices> getGovtEmpOfficeData(int empId);

     /**
      * Get department data.
      * @param deptId
      * @return Object of TblDepartmentMaster
      */
     public TblDepartmentMaster getDepartmentMasterData(short deptId);

     /**
      * Delete employee offices.
      * @param empId
      * @return true or false (boolean)
      */
     public boolean deleteEmployeeOffices(String empId);

     /**
      * Delete employee roles.
      * @param empRoleId
      * @return true or false (boolean)
      */
     public boolean deleteEmployeeRoles(String empRoleId);

     /**
      * Delete employee finance power.
      * @param empRoleId
      * @return true or false (boolean)
      */
     public boolean deleteEmployeeFinanceRole(String empRoleId);

     /**
      * Delete employee finance power.
      * @param empId
      * @return true or false (boolean)
      */
     public boolean deleteEmployeeUserWiseFinancePower(int empId);

     /**
      * Verity national Id.
      * @param nationalId
      * @return successful then ok else already exist
      */
     public String verifyNationalId(String nationalId);

     /**
      * Verify mobile no.
      * @param mobileNo
      * @return successful then ok else already exist
      */
     public String verifyMobileNo(String mobileNo);

     /**
      * Get designation name.
      * @param designationId
      * @return Get designation Name
      */
     public String getDesignationName(int designationId);

     /**
      * Get user id from employee id.
      * @param empId
      * @return int userId
      */
     public int getUserIdFromEmpId(int empId);

     /**
      * Get employee finance power.
      * @param empId
      * @param deptId
      * @return List of VwEmpfinancePower
      */
     public List<VwEmpfinancePower> getGovtUserFinanceRole(int empId,short deptId);
     
     /**
      * Get government employee role.
      * @param empId
      * @return List of SPGovtEmpRolesReturn
      * @throws Exception
      */
     public List<SPGovtEmpRolesReturn> getGovtEmpBySP(int empId) throws Exception ;

     
     /**
      * Update tbl_loginmaster
      * @param userId
      * @param password
      * @return boolean true or false
      * @throws Exception
      */
     public boolean loginMasterUpdate(int userId,String password) throws Exception;

     /**
      * get department id of user
      * @param userId
      * @param userTypeId
      * @return departmentId
      * @throws Exception
      */
     public int getUserTypeWiseDepartmentId(int userId,int userTypeId) throws Exception;

     /**
      * Check the role of employee is available or not
      * @param empId
      * @return boolean true or false
      * @throws Exception
      */
     public boolean isEmployeeDataAvailableInRole(int empId) throws Exception;

     /**
      * employeeId using userId
      * @param userId
      * @return empId
      */
     public int getEmpIdFromUserId(int userId) ;

     /**
      * check that is hope in department
      * @param deptId
      * @return boolean  true or false
      */
     public boolean isDataAvailableForHopeForDeptId(short deptId);

     /**
      * check that is there pe in office
      * @param officeId
      * @return boolean true or false
      */
     public boolean isDataAvailableForPEForOfficeId(String officeId);
     
     /**
      * Get count 
      * @param from
      * @param where
      * @return long
      * @throws Exception
      */
     public long countForQuery(String from,String where) throws Exception;

     /**
      * Get office details of user for mail
      * @param userId
      * @return List Object[] [0]employeeName, [1]officeName, [2]address, [3]city, [4]postCode, [5]departmentName, [6]stateName
      */
     public List<Object[]> getOfficeDetailsforMail(int userId);

     /**
      * Get PE office details for mail
      * @param userId 
      * @return details
      */
     public Object[] getPEOfficeDetailsforMail(int userId);
      /**
      * Get PE Admin office details for mail
      * @param userId 
      * @return details
       * //Edited by palash for PE Admin Transfer, Dohatec
      */
     public List<Object[]> getPEAdminOfficeDetailsforMail(int userId);
     /**
      * Get Ofice List for Pe.
      * @param userId 
      * @return List of TblOfficeMaster
      */
     public List<Object []> officeMasterForPe(int userId);
     
     
     /**
      * Get Ofice List for Pe.
      * @param userId 
      * @return List of TblOfficeMaster
      */
     public List<Object []> dptIdForPe(int userId);

     /**
      * Method to used to find status of employee
      * @param employeeId (int)
      * @return give status of employee
      */
     public String employeeStatus(int employeeId);

     /**
      * This mehtod is used when updating mailId when userRegistration is not completed
      * @param mailId MailId of current user.
      * @param userId userId of that employee
      * @return boolean true or false
      */
     public boolean updateMailId(String mailId,int userId);

     /**
      * 
      * @param logUserId
      */
     public void setLogUserId(String logUserId);

     /**
      * This method is for fetching information
      * @param userId 
      * @return List of TblEmployeeMaster
      */
     public List<TblEmployeeMaster> getDetailsForTransfer(int userId);

     /**
      * get userId from employeeId
      * @param eId
      * @return List of TblEmployeeMaster
      */
     public List<TblEmployeeMaster> getUserIdForUpdate(int eId);

     /**
      * Get history from userId
      * @param userId
      * @return List of TblEmployeeTrasfer
      */
     public List<TblEmployeeTrasfer> getUserHistory(int userId);
     /**
      * For Audit Trail purpose
      * @param auditTrail
      */
      public void setAuditTrail(AuditTrail auditTrail);

 /**
      * 
      * @param logUserId
      */
     public void setUserId(String logUserId);

}
