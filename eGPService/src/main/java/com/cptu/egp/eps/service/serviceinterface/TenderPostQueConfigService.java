/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblPreTenderReply;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderPostQueConfig;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface TenderPostQueConfigService {

    /**
     * this method saves the clarification configuration to database
     * @param tblTenderPostQueConfig :  this is the pojo object
     * @return boolean, true - if data is saved, false- if not saved
     */
    public boolean saveConfiguration(TblTenderPostQueConfig tblTenderPostQueConfig);

    /**
     * this method updates the clarification configuration to database
     * @param tblTenderPostQueConfig
     * @return boolean, true - if data is updated, false- if not updated
     */
    public boolean updateConfiguration(TblTenderPostQueConfig tblTenderPostQueConfig);

    /**
     * Set audit trail
     * @param auditTrail list to be inserted
     */
    public void setAuditTrail(AuditTrail auditTrail);

    /**
     * this method is for getting the clarification data from database
     * @param tenderid
     * @return list, list of data
     */
    public List<TblTenderPostQueConfig> getTenderPostQueConfig(int tenderid);

    /**
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * this method is for getting the prebid starts date from database
     * @param tenderid
     * @return list, list of data
     */
    public List<TblTenderDetails> getPrebidStartDate(String tenderid);

    /**
     * this method is for getting the pretenderreply data
     * @param userid
     * @return list, list of data
     */
    public List<TblPreTenderReply> getRepyData(int userid);
}
