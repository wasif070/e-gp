/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderAuditTrailDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderOpenDatesDao;
import com.cptu.egp.eps.model.table.TblTenderAuditTrail;
import com.cptu.egp.eps.model.table.TblTenderOpenDates;
import com.cptu.egp.eps.service.serviceinterface.TenderAuditTrailService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class TenderAuditTrailServiceImpl implements TenderAuditTrailService {

    private TblTenderAuditTrailDao tblTenderAuditTrailDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblTenderOpenDatesDao tblTenderOpenDatesDao;
    String logUserId = "0";
    final Logger logger = Logger.getLogger(SubContractingServiceImpl.class);
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblTenderOpenDatesDao getTblTenderOpenDatesDao() {
        return tblTenderOpenDatesDao;
    }

    public void setTblTenderOpenDatesDao(TblTenderOpenDatesDao tblTenderOpenDatesDao) {
        this.tblTenderOpenDatesDao = tblTenderOpenDatesDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTenderAuditTrailDao getTblTenderAuditTrailDao() {
        return tblTenderAuditTrailDao;
    }

    public void setTblTenderAuditTrailDao(TblTenderAuditTrailDao tblTenderAuditTrailDao) {
        this.tblTenderAuditTrailDao = tblTenderAuditTrailDao;
    }

    @Override
    public void addTenderAuditTrail(TblTenderAuditTrail tblTenderAuditTrail) {
        logger.debug("addTenderAuditTrail : " + logUserId + " Starts:");
        String action = null;
        try {
            tblTenderAuditTrailDao.addTblTenderAuditTrail(tblTenderAuditTrail);
            action = "Publish Tender Notice";
        } catch (Exception e) {
            logger.error("addTenderAuditTrail : " + logUserId + e);
            action = "Error in Publish Tender Notice";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderAuditTrail.getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Tender_Notice.getName(), action, tblTenderAuditTrail.getRemarks());
            action = null;
        }
        logger.debug("addTenderAuditTrail : " + logUserId + " Ends:");
    }

    @Override
    public List<Object> getenvId(int tenderId) {
        return hibernateQueryDao.getSingleColQuery("select distinct(tn.envelopeId) from TblTenderEnvelopes tn where tn.tblTenderMaster.tenderId=" + tenderId);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addTblTenderOpenDates(TblTenderOpenDates tblTenderOpenDates) {
        tblTenderOpenDatesDao.addTblTenderOpenDates(tblTenderOpenDates);
    }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }
}
