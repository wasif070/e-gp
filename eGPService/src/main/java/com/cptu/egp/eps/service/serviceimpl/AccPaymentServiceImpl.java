/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsCompansiateDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvAccHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvRemarksDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceAccDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceDocumentDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsCompansiateAmt;
import com.cptu.egp.eps.model.table.TblCmsInvAccHistory;
import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDocument;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class AccPaymentServiceImpl implements AccPaymentService {

    private static final Logger LOGGER = Logger.getLogger(AccPaymentServiceImpl.class);
    private String logUserId = "0";
    TblCmsInvoiceAccDetailsDao tblCmsInvoiceAccDetailsDao;
    TblCmsInvoiceDocumentDao tblCmsInvoiceDocumentDao;
    TblCmsInvAccHistoryDao tblCmsInvAccHistoryDao;
    TblCmsInvRemarksDao tblCmsInvRemarksDao;
    TblCmsCompansiateDao tblCmsCompansiateDao;
    HibernateQueryDao hibernateQueryDao;

    public TblCmsCompansiateDao getTblCmsCompansiateDao() {
        return tblCmsCompansiateDao;
    }

    public void setTblCmsCompansiateDao(TblCmsCompansiateDao tblCmsCompansiateDao) {
        this.tblCmsCompansiateDao = tblCmsCompansiateDao;
    }

    public TblCmsInvoiceDocumentDao getTblCmsInvoiceDocumentDao() {
        return tblCmsInvoiceDocumentDao;
    }

    public void setTblCmsInvoiceDocumentDao(TblCmsInvoiceDocumentDao tblCmsInvoiceDocumentDao) {
        this.tblCmsInvoiceDocumentDao = tblCmsInvoiceDocumentDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblCmsInvoiceAccDetailsDao getTblCmsInvoiceAccDetailsDao() {
        return tblCmsInvoiceAccDetailsDao;
    }

    public void setTblCmsInvoiceAccDetailsDao(TblCmsInvoiceAccDetailsDao tblCmsInvoiceAccDetailsDao) {
        this.tblCmsInvoiceAccDetailsDao = tblCmsInvoiceAccDetailsDao;
    }

    public TblCmsInvAccHistoryDao getTblCmsInvAccHistoryDao() {
        return tblCmsInvAccHistoryDao;
    }

    public void setTblCmsInvAccHistoryDao(TblCmsInvAccHistoryDao tblCmsInvAccHistoryDao) {
        this.tblCmsInvAccHistoryDao = tblCmsInvAccHistoryDao;
    }

    public TblCmsInvRemarksDao getTblCmsInvRemarksDao() {
        return tblCmsInvRemarksDao;
    }

    public void setTblCmsInvRemarksDao(TblCmsInvRemarksDao tblCmsInvRemarksDao) {
        this.tblCmsInvRemarksDao = tblCmsInvRemarksDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public boolean addInvoiceAccDetails(TblCmsInvoiceAccDetails tblCmsInvoiceAccDetails) {
        LOGGER.debug("addInvoiceAccDetails : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsInvoiceAccDetailsDao.addTblCmsInvoiceAccDetails(tblCmsInvoiceAccDetails);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("addInvoiceAccDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("addInvoiceAccDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public boolean updateInvoiceAccDetails(TblCmsInvoiceAccDetails tblCmsInvoiceAccDetails) {
        LOGGER.debug("updateInvoiceAccDetails : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsInvoiceAccDetailsDao.updateTblCmsInvoiceAccDetails(tblCmsInvoiceAccDetails);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("updateInvoiceAccDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("updateInvoiceAccDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public boolean addInvoiceDocsDetails(TblCmsInvoiceDocument tblCmsInvoiceDocument) {
        LOGGER.debug("addInvoiceDocsDetails : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsInvoiceDocumentDao.addTblCmsInvoiceDocument(tblCmsInvoiceDocument);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("addInvoiceDocsDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("addInvoiceDocsDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<TblCmsInvoiceDocument> getInvoiceDocDetails(int invoiceId) {
        LOGGER.debug("getInvoiceDocDetails : " + logUserId + "Starts");
        List<TblCmsInvoiceDocument> list = null;
        try {
            list = tblCmsInvoiceDocumentDao.findTblCmsInvoiceDocument("tblCmsInvoiceMaster", Operation_enum.EQ, new TblCmsInvoiceMaster(invoiceId));

        } catch (Exception e) {
            LOGGER.error("getInvoiceDocDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("getInvoiceDocDetails : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<TblCmsInvoiceDocument> getInvoiceDocDetailsbyPassingUsertypeID(int invoiceId, int userTypeId) {
        LOGGER.debug("getInvoiceDocDetailsbyPassingUsertypeID : " + logUserId + "Starts");
        List<TblCmsInvoiceDocument> list = null;
        try {
            list = tblCmsInvoiceDocumentDao.findTblCmsInvoiceDocument("tblCmsInvoiceMaster", Operation_enum.EQ, new TblCmsInvoiceMaster(invoiceId), "userTypeId", Operation_enum.EQ, userTypeId);

        } catch (Exception e) {
            LOGGER.error("getInvoiceDocDetailsbyPassingUsertypeID : " + logUserId + " " + e);
        }
        LOGGER.debug("getInvoiceDocDetailsbyPassingUsertypeID : " + logUserId + "Ends");
        return list;
    }

    @Override
    public boolean deleteInvoiceDocsDetails(int InvoiceDocID) {
        LOGGER.debug("deleteInvoiceDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        int i = 0;
        try {
            i = hibernateQueryDao.updateDeleteNewQuery("delete from TblCmsInvoiceDocument where invoiceDocId=" + InvoiceDocID + "");
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("deleteInvoiceDocsDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("deleteInvoiceDocsDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<Object[]> getPaymentListing(String invStatus, int userId, int pageNo, int Rowsize) {
        LOGGER.debug("getPaymentListing : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ttd.tblTenderMaster.tenderId,wpm.wpLotId,tcim.tblCmsWpMaster.wpId,");
            sb.append("tcim.invoiceId,ttd.reoiRfpRefNo,ttd.peName,tnid.contractNo ");
            sb.append(",tcm.companyName,ttm.firstName,ttm.lastName,tcm.companyId,tcim.invoiceNo ");
            sb.append(",tcs.contractSignId "); //added for get contractId
            sb.append("from TblCmsInvoiceMaster tcim,TblTenderDetails ttd, TblEmployeeMaster tem, TblEmployeeOffices teo, TblNoaIssueDetails tnid, TblCmsWpMaster wpm ");
            sb.append(",TblTendererMaster ttm,TblCompanyMaster tcm,TblNoaAcceptance tna ");
            sb.append(", TblOfficeMaster o "); //added for bugid #4307
            sb.append(", TblContractSign tcs "); //added for get contractId
            sb.append("where tcim.tenderId = ttd.tblTenderMaster.tenderId and tem.tblLoginMaster.userId='" + userId + "' ");
            sb.append("and o.officeId = teo.tblOfficeMaster.officeId and o.officeId = ttd.officeId ");//added for bugid #4307
            sb.append("and tnid.tblTenderMaster.tenderId=ttd.tblTenderMaster.tenderId ");
            sb.append("and wpm.wpId = tcim.tblCmsWpMaster.wpId and wpm.wpLotId = tnid.pkgLotId ");
            sb.append("and tem.employeeId = teo.tblEmployeeMaster.employeeId ");
            sb.append("and ttm.tblCompanyMaster.companyId = tcm.companyId and ttm.tblLoginMaster.userId=tnid.userId ");
            sb.append("and tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tna.acceptRejStatus='approved' ");
            sb.append("and tcs.noaId=tnid.noaIssueId ");//added for get contractId
            if ("sendtope".equalsIgnoreCase(invStatus)) {
                sb.append("and tcim.invStatus in ('sendtope','remarksbype') order by tcim.createdDate desc");
            } else {
                sb.append("and tcim.invStatus='" + invStatus + "' order by tcim.createdDate desc");
            }
            list = hibernateQueryDao.createByCountNewQuery(sb.toString(), pageNo, Rowsize);
        } catch (Exception e) {
            LOGGER.error("getPaymentListing : " + logUserId + " : " + e);
        }
        LOGGER.debug("getPaymentListing : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public long getPaymentListingCount(String invStatus, int userId) {
        LOGGER.debug("getPaymentListingCount : " + logUserId + ": Starts");
        long l = 0;
        try {
            StringBuilder sbFrom = new StringBuilder();
            sbFrom.append("TblCmsInvoiceMaster tcim,TblTenderDetails ttd, TblEmployeeMaster tem, TblEmployeeOffices teo, TblNoaIssueDetails tnid, TblCmsWpMaster wpm ");
            sbFrom.append(",TblTendererMaster ttm,TblCompanyMaster tcm,TblNoaAcceptance tna ");
            sbFrom.append(", TblOfficeMaster o "); //added for bugid #4307
            StringBuilder sbwhere = new StringBuilder();
            sbwhere.append("tcim.tenderId = ttd.tblTenderMaster.tenderId and tem.tblLoginMaster.userId='" + userId + "' ");
            sbwhere.append("and o.officeId = teo.tblOfficeMaster.officeId and o.officeId = ttd.officeId "); //added for bugid #4307
            sbwhere.append("and tnid.tblTenderMaster.tenderId=ttd.tblTenderMaster.tenderId ");
            sbwhere.append("and wpm.wpId = tcim.tblCmsWpMaster.wpId and wpm.wpLotId = tnid.pkgLotId ");
            sbwhere.append("and tem.employeeId = teo.tblEmployeeMaster.employeeId ");
            sbwhere.append("and ttm.tblCompanyMaster.companyId = tcm.companyId and ttm.tblLoginMaster.userId=tnid.userId ");
            sbwhere.append("and tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tna.acceptRejStatus='approved' ");
            if ("sendtope".equalsIgnoreCase(invStatus)) {
                sbwhere.append("and tcim.invStatus in ('sendtope','remarksbype') ");
            } else {
                sbwhere.append("and tcim.invStatus='" + invStatus + "' ");
            }
            l = hibernateQueryDao.countForNewQuery(sbFrom.toString(), sbwhere.toString());
        } catch (Exception e) {
            LOGGER.error("getPaymentListingCount : " + logUserId + " : " + e);
        }
        LOGGER.debug("getPaymentListingCount : " + logUserId + ": Ends");
        return l;
    }

    @Override
    public List<TblCmsInvoiceAccDetails> getInvoiceAccDetails(int invoiceId) {
        LOGGER.debug("getInvoiceAccDetails : " + logUserId + "Starts");
        List<TblCmsInvoiceAccDetails> list = null;
        try {
            list = tblCmsInvoiceAccDetailsDao.findTblCmsInvoiceAccDetails("tblCmsInvoiceMaster", Operation_enum.EQ, new TblCmsInvoiceMaster(invoiceId));

        } catch (Exception e) {
            LOGGER.error("getInvoiceAccDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("getInvoiceAccDetails : " + logUserId + "Ends");
        return list;
    }

    @Override
    public boolean addInvAccHistoryDetails(TblCmsInvAccHistory tblCmsInvAccHistory) {
        LOGGER.debug("addInvAccHistoryDetails : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsInvAccHistoryDao.addTblCmsInvAccHistory(tblCmsInvAccHistory);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("addInvAccHistoryDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("addInvAccHistoryDetails : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public boolean updateStatusInInvMaster(String invStatus, int InvoiceId) {
        LOGGER.debug("updateStatusInInvMaster : " + logUserId + "Starts");
        boolean flag = false;
        try {
            String quS = "";
            if(invStatus.equalsIgnoreCase("sendtotenderer")){
                quS = "update TblCmsInvoiceMaster tcw set tcw.invStatus='" + invStatus + "', tcw.releasedDtNTime = GETDATE() where tcw.invoiceId=" + InvoiceId;
            }else{
                quS = "update TblCmsInvoiceMaster tcw set tcw.invStatus='" + invStatus + "' where tcw.invoiceId=" + InvoiceId;
            }
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("updateStatusInInvMaster : " + logUserId + " " + e);
        }
        LOGGER.debug("updateStatusInInvMaster : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<Object> getInvoiceStatus(int InvoiceId) {
        LOGGER.debug("getInvoiceStatus : " + logUserId + ": Starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select invStatus from TblCmsInvoiceMaster where invoiceId=" + InvoiceId + "");
        } catch (Exception e) {
            LOGGER.error("getInvoiceStatus : " + logUserId + " : " + e);
        }
        LOGGER.debug("getInvoiceStatus : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public List<Object> getInvoiceRemarks(int InvoiceId) {
        LOGGER.debug("getInvoiceRemarks : " + logUserId + ": Starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select remarks from TblCmsInvoiceMaster where invoiceId=" + InvoiceId + "");
        } catch (Exception e) {
            LOGGER.error("getInvoiceRemarks : " + logUserId + " : " + e);
        }
        LOGGER.debug("getInvoiceRemarks : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public boolean saveRemarks(TblCmsInvRemarks tblCmsInvRemarks) {
        LOGGER.debug("saveRemarks : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsInvRemarksDao.addTblCmsInvRemarks(tblCmsInvRemarks);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("saveRemarks : " + logUserId + " " + e);
        }
        LOGGER.debug("saveRemarks : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<TblCmsInvRemarks> getRemarksDetails(int invoiceId) {
        LOGGER.debug("getRemarksDetails : " + logUserId + "Starts");
        List<TblCmsInvRemarks> list = null;
        try {
            list = tblCmsInvRemarksDao.findTblCmsInvRemarks("tblCmsInvoiceMaster", Operation_enum.EQ, new TblCmsInvoiceMaster(invoiceId));

        } catch (Exception e) {
            LOGGER.error("getRemarksDetails : " + logUserId + " " + e);
        }
        LOGGER.debug("getRemarksDetails : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getAllTotalInvoiceAmtForWp(int Wpid) {
        LOGGER.debug("getAllTotalInvoiceAmtForWp : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(tciad.invoiceAmt) ,SUM(tciad.advAdjAmt) ,SUM(tciad.retentionMdeduct) ,SUM(tciad.retentionMadd) , ");
            sb.append("SUM(tciad.pgDeduct) ,SUM(tciad.pgAdd) ,SUM(tciad.vatAmt) ,SUM(tciad.aitAmt) , ");
            sb.append("SUM(tciad.advVatAmt) ,SUM(tciad.advAitAmt) ,SUM(tciad.bonusAmt) ,SUM(tciad.penaltyAmt) , ");
            sb.append("SUM(tciad.ldAmt) ,SUM(tciad.grossAmt),case when SUM(tciad.salvageAdjAmt) is null then '0.0' else SUM(tciad.salvageAdjAmt) end  from TblCmsInvoiceMaster tcim, TblCmsInvoiceAccDetails tciad ");
            sb.append("where tciad.tblCmsInvoiceMaster.invoiceId=tcim.invoiceId and tcim.tblCmsWpMaster.wpId=" + Wpid + " and ");
            sb.append("tcim.invStatus in ('sendtope','remarksbype','sendtotenderer')");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            LOGGER.error("getAllTotalInvoiceAmtForWp : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllTotalInvoiceAmtForWp : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getTotalDeDuctedInvoiceAmtForWp(int Wpid) {
        LOGGER.debug("getAllTotalInvoiceAmtForWp : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(tciad.invoiceAmt) ,SUM(tciad.advAdjAmt),SUM(tciad.grossAmt),case when SUM(tciad.salvageAdjAmt) is null then '0.0' else SUM(tciad.salvageAdjAmt) end ");
            sb.append("from TblCmsInvoiceMaster tcim, TblCmsInvoiceAccDetails tciad ");
            sb.append("where tciad.tblCmsInvoiceMaster.invoiceId=tcim.invoiceId and tcim.tblCmsWpMaster.wpId=" + Wpid + " and ");
            sb.append("tcim.invStatus in ('sendtope','sendtotenderer')");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            LOGGER.error("getAllTotalInvoiceAmtForWp : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllTotalInvoiceAmtForWp : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object> getFirstInvoiceAmountDetails(int ContractId) {
        LOGGER.debug("getFirstInvoiceAmountDetails : " + logUserId + ": Starts");
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select iad.advAmt ");
            sb.append("from TblCmsInvoiceAccDetails iad,TblContractSign cs,TblNoaIssueDetails nid, ");
            sb.append("TblCmsWpMaster wm where iad.tblCmsWpMaster.wpId = wm.wpId and cs.contractSignId=" + ContractId + " ");
            sb.append("and wm.wpLotId = nid.pkgLotId and cs.noaId = nid.noaIssueId");
            list = hibernateQueryDao.singleColQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getFirstInvoiceAmountDetails : " + logUserId + " : " + e);
        }
        LOGGER.debug("getFirstInvoiceAmountDetails : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public List<Object[]> getPEemailID(int tenderID) {
        LOGGER.debug("getPEemailID : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tlm.emailId,tom.officeName,tom.officeId ");
            sb.append("from TblTenderMaster ttm,TblLoginMaster tlm,TblTenderDetails ttd,TblOfficeMaster tom ");
            sb.append("where tlm.userId=ttm.createdBy and tom.officeId=ttd.officeId ");
            sb.append("and ttd.tblTenderMaster.tenderId=ttm.tenderId and ttm.tenderId=" + tenderID + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getPEemailID : " + logUserId + " : " + e);
        }
        LOGGER.debug("getPEemailID : " + logUserId + ": Ends");
        return list;
    }

    public List<Object[]> getAccountantEmailId(int tenderID) {
        LOGGER.debug("getAccountantEmailId : " + logUserId + ": Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select om.officeName,om.officeId,em.employeeId,em.employeeName,lm.emailId ");
            sb.append("from TblTenderDetails td,TblOfficeMaster om,TblEmployeeOffices eo, ");
            sb.append("TblEmployeeMaster em,TblEmployeeRoles er,TblProcurementRole pr,TblLoginMaster lm ");
            sb.append("where td.tblTenderMaster.tenderId=" + tenderID + " and om.officeId=td.officeId ");
            sb.append("and eo.tblOfficeMaster.officeId=om.officeId and em.employeeId=eo.tblEmployeeMaster.employeeId ");
            sb.append("and er.tblEmployeeMaster.employeeId=em.employeeId and pr.procurementRoleId=er.tblProcurementRole.procurementRoleId ");
            sb.append("and lm.userId=em.tblLoginMaster.userId and pr.procurementRoleId='25'");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getAccountantEmailId : " + logUserId + " : " + e);
        }
        LOGGER.debug("getAccountantEmailId : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public List<Object> getTotalInvoiceAmtFromImaster(int tenderId) {
        LOGGER.debug("getTotalInvoiceAmtFromImaster : " + logUserId + "Starts");
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(totalInvAmt) from TblCmsInvoiceMaster where tenderId=" + tenderId + " and invStatus in ('createdbyten','acceptedbype','sendtope','sendtotenderer','remarksbype')");
            list = hibernateQueryDao.singleColQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getTotalInvoiceAmtFromImaster : " + logUserId + " " + e);
        }
        LOGGER.debug("getTotalInvoiceAmtFromImaster : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object> getTotalInvoiceAmtFromImasterForRO(int contractId) {
        LOGGER.debug("getTotalInvoiceAmtFromImasterForRO : " + logUserId + "Starts");
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(ti.totalInvAmt) from TblCmsInvoiceMaster ti, TblContractSign tc,TblNoaIssueDetails tn,TblEvalRoundMaster te,TblCmsRomap tr ");
            sb.append("where te.roundId = tn.roundId and tr.romId=te.romId and tn.noaIssueId=tc.noaId and ti.tblCmsWpMaster.wpId=tr.wpId ");
            sb.append("and tc.contractSignId=" + contractId + " and ti.invStatus in ('createdbyten','acceptedbype','sendtope','sendtotenderer','remarksbype')");
            list = hibernateQueryDao.singleColQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getTotalInvoiceAmtFromImasterForRO : " + logUserId + " " + e);
        }
        LOGGER.debug("getTotalInvoiceAmtFromImaster : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getAllTotalInvoiceAmtFromAc(int lotId) {
        LOGGER.debug("getAllTotalInvoiceAmtFromAc : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(tciad.invoiceAmt) ,SUM(tciad.advAdjAmt) ,SUM(tciad.retentionMdeduct) ,SUM(tciad.retentionMadd) , ");
            sb.append("SUM(tciad.pgDeduct) ,SUM(tciad.pgAdd) ,SUM(tciad.vatAmt) ,SUM(tciad.aitAmt) , ");
            sb.append("SUM(tciad.advVatAmt) ,SUM(tciad.advAitAmt) ,SUM(tciad.bonusAmt) ,SUM(tciad.penaltyAmt) , ");
            sb.append("SUM(tciad.ldAmt) ,SUM(tciad.grossAmt)  from TblCmsInvoiceMaster tcim, TblCmsInvoiceAccDetails tciad, TblCmsWpMaster wm ");
            sb.append("where tciad.tblCmsInvoiceMaster.invoiceId=tcim.invoiceId and tcim.tblCmsWpMaster.wpId=wm.wpId and wm.wpLotId=" + lotId + " and ");
            sb.append("tcim.invStatus in ('sendtotenderer')");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            LOGGER.error("getAllTotalInvoiceAmtFromAc : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllTotalInvoiceAmtFromAc : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getAllTotalInvoiceAmtFromAcForRO(int contractId) {
        LOGGER.debug("getAllTotalInvoiceAmtFromAcForRO : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select SUM(tciad.invoiceAmt) ,SUM(tciad.advAdjAmt) ,SUM(tciad.retentionMdeduct) ,SUM(tciad.retentionMadd) , ");
            sb.append("SUM(tciad.pgDeduct) ,SUM(tciad.pgAdd) ,SUM(tciad.vatAmt) ,SUM(tciad.aitAmt) , ");
            sb.append("SUM(tciad.advVatAmt) ,SUM(tciad.advAitAmt) ,SUM(tciad.bonusAmt) ,SUM(tciad.penaltyAmt) , ");
            sb.append("SUM(tciad.ldAmt) ,SUM(tciad.grossAmt)  from TblCmsInvoiceMaster tcim, TblCmsInvoiceAccDetails tciad, TblCmsWpMaster wm, ");
            sb.append("TblContractSign tc,TblNoaIssueDetails tn,TblEvalRoundMaster te,TblCmsRomap tr ");
            sb.append("where te.roundId = tn.roundId and tr.romId=te.romId and tn.noaIssueId=tc.noaId and wm.isRepeatOrder='yes' ");
            sb.append("and tciad.tblCmsInvoiceMaster.invoiceId=tcim.invoiceId and tcim.tblCmsWpMaster.wpId=wm.wpId and tc.contractSignId=" + contractId + " and ");
            sb.append("tcim.invStatus in ('sendtotenderer')");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            LOGGER.error("getAllTotalInvoiceAmtFromAcForRO : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllTotalInvoiceAmtFromAcForRO : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getAllInvoiceAmtFromImaster(int tenderId) {
        LOGGER.debug("getAllInvoiceAmtFromImaster : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select invoiceId,totalInvAmt from TblCmsInvoiceMaster where tenderId=" + tenderId + " and invStatus in ('accpetedbype','sendtope','sendtotenderer') ORDER BY  invoiceId desc");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getAllInvoiceAmtFromImaster : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllInvoiceAmtFromImaster : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<Object[]> getAllInvoiceAmtFromAc(int lotId) {
        LOGGER.debug("getAllInvoiceAmtFromAc : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcim.invoiceId,tciad.grossAmt from TblCmsInvoiceMaster tcim, TblCmsInvoiceAccDetails tciad, TblCmsWpMaster wm ");
            sb.append("where tciad.tblCmsInvoiceMaster.invoiceId=tcim.invoiceId and tcim.tblCmsWpMaster.wpId=wm.wpId and wm.wpLotId=" + lotId + " and ");
            sb.append("tcim.invStatus in ('sendtotenderer')");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            LOGGER.error("getAllInvoiceAmtFromAc : " + logUserId + " " + e);
        }
        LOGGER.debug("getAllInvoiceAmtFromAc : " + logUserId + "Ends");
        return list;
    }

    @Override
    public String getUserTypeId(String emailId) {
        LOGGER.debug("getUserTypeId : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tlm.tblUserTypeMaster.userTypeId from TblLoginMaster tlm where tlm.emailId='" + emailId + "'");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getUserTypeId : " + logUserId + " " + e);
        }
        LOGGER.debug("getUserTypeId : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getUserId(String emailId) {
        LOGGER.debug("getUserId : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tlm.userId from TblLoginMaster tlm where tlm.emailId='" + emailId + "'");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getUserId : " + logUserId + " " + e);
        }
        LOGGER.debug("getUserId : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getMobileNoWithCountryCode(String userId) {
        LOGGER.debug("getMobileNoWithCountryCode : " + logUserId + "Starts");
        String str = "";
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select cm.countryCode,tm.mobileNo from TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and tm.tblLoginMaster.userId=" + userId);
            list = hibernateQueryDao.createNewQuery(sb.toString());
            if (!list.isEmpty()) {
                Object[] obj = list.get(0);
                if (obj[0] != null && obj[1] != null) {
                    str = obj[0].toString() + obj[1].toString();
                }
            }
        } catch (Exception e) {
            LOGGER.error("getMobileNoWithCountryCode : " + logUserId + " " + e);
        }
        LOGGER.debug("getMobileNoWithCountryCode : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getPEMobileNoWithCountryCode(String userId) {
        LOGGER.debug("getPEMobileNoWithCountryCode : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tem.mobileNo from TblEmployeeMaster tem where tem.tblLoginMaster.userId=" + userId);
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = "+880" + list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getPEMobileNoWithCountryCode : " + logUserId + " " + e);
        }
        LOGGER.debug("getPEMobileNoWithCountryCode : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getProcurementRoleID(String userId) {
        LOGGER.debug("getProcurementRoleID : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select d.procurementRoleId ");
            sb.append("from TblEmployeeRoles a,TblEmployeeMaster b,TblLoginMaster c,TblProcurementRole d ");
            sb.append("where c.userId=b.tblLoginMaster.userId and b.employeeId=a.tblEmployeeMaster.employeeId ");
            sb.append("and c.userId=" + userId + " and a.tblProcurementRole.procurementRoleId=d.procurementRoleId ");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getProcurementRoleID : " + logUserId + " " + e);
        }
        LOGGER.debug("getProcurementRoleID : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getBankName(String TenderPaymentId) {
        LOGGER.debug("getBankName : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ttp.bankName from TblTenderPayment ttp where tenderPaymentId=" + TenderPaymentId + "");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getBankName : " + logUserId + " " + e);
        }
        LOGGER.debug("getBankName : " + logUserId + "Ends");
        return str;
    }
    public String getBranchName(String TenderPaymentId) {
        LOGGER.debug("getBranchName : " + logUserId + "Starts");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ttp.branchName from TblTenderPayment ttp where tenderPaymentId=" + TenderPaymentId + "");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getBranchName : " + logUserId + " " + e);
        }
        LOGGER.debug("getBranchName : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getBankUserMobileNowithCountryCode(String userId) {
        LOGGER.debug("getBankUserMobileNowithCountryCode : " + logUserId + "Starts");
        String str = "";
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcm.countryCode,tpa.mobileNo ");
            sb.append("from TblPartnerAdmin tpa,TblScBankDevPartnerMaster tsdpm,TblCountryMaster tcm ");
            sb.append("where tpa.sbankDevelopId = tsdpm.sbankDevelopId and tpa.tblLoginMaster.userId=" + userId + " and tcm.countryId = tsdpm.tblCountryMaster.countryId");
            list = hibernateQueryDao.createNewQuery(sb.toString());
            if (!list.isEmpty()) {
                Object[] obj = list.get(0);
                if (obj[0] != null && obj[1] != null) {
                    str = obj[0].toString() + obj[1].toString();
                }
            }
        } catch (Exception e) {
            LOGGER.error("getBankUserMobileNowithCountryCode : " + logUserId + " " + e);
        }
        LOGGER.debug("getBankUserMobileNowithCountryCode : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getlotId(int InvoiceId) {
        LOGGER.debug("getlotId : " + logUserId + "Ends");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcm.wpLotId ");
            sb.append("from TblCmsWpMaster tcm,TblCmsInvoiceMaster tcim ");
            sb.append("where tcm.wpId= tcim.tblCmsWpMaster.wpId and tcim.invoiceId=" + InvoiceId + "");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getlotId : " + logUserId + " " + e);
        }
        LOGGER.debug("getlotId : " + logUserId + "Ends");
        return str;
    }

    @Override
    public String getAdvanceContractAmount(int tenderId, int lotId) {
        LOGGER.debug("getAdvanceContractAmount : " + logUserId + "Ends");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tnid.advanceContractAmt ");
            sb.append("from TblNoaIssueDetails tnid,TblNoaAcceptance tna ");
            sb.append("where tnid.tblTenderMaster.tenderId=" + tenderId + " and tnid.pkgLotId=" + lotId + "");
            sb.append("and tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tna.acceptRejStatus='approved'");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getAdvanceContractAmount : " + logUserId + " " + e);
        }
        LOGGER.debug("getAdvanceContractAmount : " + logUserId + "Ends");
        return str;
    }

    @Override
    public boolean addCompansiateAmt(TblCmsCompansiateAmt tblCmsCompansiateAmt) {
        LOGGER.debug("addCompansiateAmt : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblCmsCompansiateDao.addTblCmsCompansiateAmt(tblCmsCompansiateAmt);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("addCompansiateAmt : " + logUserId + " " + e);
        }
        LOGGER.debug("addCompansiateAmt : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<TblCmsCompansiateAmt> getTblCmsCompansiateAmt(int PayId) {
        LOGGER.debug("getTblCmsCompansiateAmt : " + logUserId + "Starts");
        List<TblCmsCompansiateAmt> list = null;
        try {
            list = tblCmsCompansiateDao.findTblCmsCompansiateAmt("paymentId", Operation_enum.EQ, PayId);

        } catch (Exception e) {
            LOGGER.error("getTblCmsCompansiateAmt : " + logUserId + " " + e);
        }
        LOGGER.debug("getTblCmsCompansiateAmt : " + logUserId + "Ends");
        return list;
    }

    public String getEmailId(String strUserId) {
        String strEmailId = null;
        List<Object> list = hibernateQueryDao.singleColQuery("select tlm.emailId from TblLoginMaster tlm where tlm.userId = " + Integer.valueOf(strUserId));
        if (!list.isEmpty()) {
            strEmailId = list.get(0).toString();
        }
        return strEmailId;
    }

    @Override
    public List<Object[]> getCurrencyInAccGenerateBefore(int wpId,int tenderId)
    {
        LOGGER.debug("getCurrencyInAccGenerateBefore : " + logUserId + "Ends");
        String str = "";
        List<Object[]> list = null;
        try {
         StringBuilder sb = new StringBuilder();
            sb.append("select tnid.contractAmt,tcm.currencyShortName from TblNoaIssueDetails tnid,TblCurrencyMaster tcm where tcm.currencyId = tnid.tblCurrencyMaster.currencyId and roundId = (select max(roundId) from TblNoaIssueDetails where tblTenderMaster.tenderId = "+ tenderId +")  and tnid.tblCurrencyMaster.currencyId in ");
            sb.append("(select currencyId from TblCurrencyMaster where currencyShortName in  ");
            sb.append("(select currencyName from TblCmsWpDetail where itemInvStatus != 'advance' and wpDetailId in ");
            sb.append("(select wpDetailId from TblCmsInvoiceDetails where invoiceId in ");
            sb.append("(select tblCmsInvoiceMaster.invoiceId from TblCmsInvoiceAccDetails where tblCmsWpMaster.wpId = "+wpId+"))))");
            list = hibernateQueryDao.createNewQuery(sb.toString());

       } catch (Exception e) {
            LOGGER.error("getCurrencyInAccGenerateBefore : " + logUserId + " " + e);
        }
        LOGGER.debug("getCurrencyInAccGenerateBefore : " + logUserId + "Ends");
     return list;
    }

     @Override
    public String getSalavageContractAmount(int tenderId, int lotId) {
        LOGGER.debug("getSalvageContractAmount : " + logUserId + "Ends");
        String str = "";
        List<Object> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select case when  tnid.salvageAmt is null then '0.00' else tnid.salvageAmt end ");
            sb.append("from TblNoaIssueDetails tnid,TblNoaAcceptance tna ");
            sb.append("where tnid.tblTenderMaster.tenderId=" + tenderId + " and tnid.pkgLotId=" + lotId + "");
            sb.append("and tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tna.acceptRejStatus='approved'");
            list = hibernateQueryDao.singleColQuery(sb.toString());
            if (list.get(0) != null) {
                str = list.get(0).toString();
                        
            }
        } catch (Exception e) {
            LOGGER.error("getSalvageContractAmount : " + logUserId + " " + e);
        }
        LOGGER.debug("getSalvageContractAmount : " + logUserId + "Ends");
        return str;
    }

    
}
