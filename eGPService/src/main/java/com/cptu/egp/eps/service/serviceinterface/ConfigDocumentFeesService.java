/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

/**
 *
 * @author Rokibul
 */

import com.cptu.egp.eps.model.table.TblConfigDocumentFees;
import java.util.List;
public interface ConfigDocumentFeesService {
    /**
     * for Setting userId at Service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
        /**
     * add Contract Termination Details To the database
     * @param TblCmsContractTermination
     * @return integer, return 0 - if not submitted otherwise anything else count
     */
   // public int insertConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees);

    /**
     * update Contract Termination Details To the database
     * @param TblCmsContractTermination
     * @return boolean, true - if updated successfully otherwise false
     */
  //  public boolean updateConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees);

    /**
     * delete the Contract termination details from the database
     * @param TblCmsContractTermination
     * @return boolean, true - if deleted successfully otherwise false
     */
  //  public boolean deleteConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees);

    /**
     * gives Contract Termination details data from database
     * @return list of data
     */
  //  public List<TblConfigDocumentFees> getAllConfigDocumentFees();



    /**
     * gives Contract Termination details data
     * @param id
     * @return Contract Termination table objects
     */
    //public TblConfigDocumentFees getConfigDocumentFees(int id);


    public int getMaxDocumentPriceInBDT(String procurementType);

    public int getMaxDocumentPriceInUSD(String procurementType);
}
