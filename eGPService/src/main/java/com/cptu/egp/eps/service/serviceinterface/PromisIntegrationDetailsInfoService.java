/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.PromisIntegrationDetails;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface PromisIntegrationDetailsInfoService {

    public void setLogUserId(String logUserId);
    /***
     * This method returns the all the records of sharedPEdetails for
     * promis system.
     * @return List of PromisIntegrationDetails
     * @throws Exception
     */
    public List<PromisIntegrationDetails> getSharedPEDetailForPromis() throws Exception;
}
