/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsWcCertificateService {

    /**
     * set userId at service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * adds work completion certificate details to the database
     * @param tblCmsWcCertificate
     * @return integer, return 0 - if not added successfully otherwise else count
     */
    public int insertCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    /**
     * updates work completion certificate details to the database
     * @param tblCmsWcCertificate
     * @return boolean, return true - if updated successfully otherwise false
     */
    public boolean updateCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    /**
     * delete work completion certificates details to the database
     * @param tblCmsWcCertificate
     * @return  boolean, return true - if deleted successfully otherwise false
     */
    public boolean deleteCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    /**
     * gives work completion documents details from the database
     * @return list of table data
     */
    public List<TblCmsWcCertificate> getAllCmsWcCertificate();

    /**
     * gives document details data row count
     * @return long, return 0 - if data is not exist,otherwise else count
     */
    public long getCmsWcCertificateCount();

    /**
     * gives work completion certificate details data
     * @param id
     * @return table object
     */
    public TblCmsWcCertificate getCmsWcCertificate(int id);

    /**
     * gives work completion certificate details data
     * @param contractSignId
     * @return table object
     */
    public TblCmsWcCertificate getCmsWcCertificateForContractSignId(int contractSignId);
    
    /**
     * adds work completion certificate history details to the database
     * @param tblCmsWcCertiHistory
     * @return integer, return 0 - if not added successfully otherwise else count
     */
    public int insertCmsWcCertihistory(TblCmsWcCertiHistory tblCmsWcCertiHistory);
    
    /**
     * gives max history count
     * @param wccertId
     * @return integer, return 0 - if data is not exist otherwise else count
     */
    public int getMaxHistoryCount(int wccertId);
    
    /**
     * gives work completion certificate details
     * @param wcCertiId
     * @return list of objects data
     */
    public List<Object[]> getWcCetificateDetails(int wcCertiId);
    
    /**
     * gives work completion certificate history details
     * @param wcCertiHistId
     * @return list of objects data
     */
    public List<Object[]> getWcCetificateHistoryDetails(int wcCertiHistId);
}
