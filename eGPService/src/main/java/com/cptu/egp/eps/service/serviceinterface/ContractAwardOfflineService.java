/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import com.cptu.eps.service.audit.AuditTrail;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.cptu.egp.eps.dao.storedprocedure.ContractAwardOfflineDetails;

/**
 *
 * @author Sohel
 */
public interface ContractAwardOfflineService {

    /**
     * For Insert in to TblContractAwardedOffline
     * @param TblContractAwardedOffline
     * @return boolean
     */
    public boolean insert(TblContractAwardedOffline contract);

    public List<ContractAwardOfflineDetails> getContractAwardOfflineData(String searchFlag,String status,String procNature,String procMethod,String refNo,String value,String advDateFrom,String advDateTo,String ministry,String action,String comment);
    // public List<ContractAwardOfflineDetails> getContractAwardOfflineData(String searchFlag,String status,String procNature,String procMethod);

    public boolean deleteContractAwardOfflineData(String refNo);

     public List<Object[]> getMinistryForAwardedContractOffline();

     public List<Object[]> getOrgForAwardedContractOffline(String ministry);
     
     public List<Object[]> getMinistryForAwardedContractOfflineHome();

    public List<Object[]> getOrgForAwardedContractOfflineHome(String ministry);
    
}
