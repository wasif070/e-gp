/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;
import com.cptu.egp.eps.model.table.TblAppPermission;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public interface BiddingPermissionService {

    public void addTblAppPermission(TblAppPermission tblAppPermission,int userID);
    public List<TblAppPermission> findTblAppPermission(Object... values) throws Exception ;
    
    public void addTblBiddingPermission(TblBiddingPermission tblBiddingPermission,int userID);
    public List<TblBiddingPermission> findTblBiddingPermission(Object... values) throws Exception ;
    
    
    public void addTblTempBiddingPermission(TblTempBiddingPermission tblTempBiddingPermission,int userID);
    public List<TblTempBiddingPermission> findTblTempBiddingPermission(Object... values) throws Exception ;
    public void setLogUserId(String logUserId);
    public String updateNonDebarredBiddingList(List<TblBiddingPermission> biddingPermissions, String debarredUser, String companyId);
}
