/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderEstCostDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderEstCost;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class TenderEstCost {

    private final Logger logger = Logger.getLogger(TenderEstCost.class);
    private TblTenderEstCostDao tblTenderEstCostDao;
    private HibernateQueryDao hibernateQueryDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTenderEstCostDao getTblTenderEstCostDao() {
        return tblTenderEstCostDao;
    }

    public void setTblTenderEstCostDao(TblTenderEstCostDao tblTenderEstCostDao) {
        this.tblTenderEstCostDao = tblTenderEstCostDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public boolean addEstCost(List<TblTenderEstCost> tblTenderEstCost,String tenderId,String... actionPerformed) {
        logger.debug("addEstCost :  Starts");
        boolean flag = false;
        String action = "";
        try {
            //change by ahsan for fixing double entry issue
            if(actionPerformed != null && actionPerformed.length != 0){
                    action = actionPerformed[0];
             }
            if(action.equals("Add Official estimate cost")){
                 List<Object> lst = hibernateQueryDao.singleColQuery("select count(tblTenderMaster.tenderId) from TblTenderEstCost where tblTenderMaster.tenderId = "+ tenderId);
                 if(lst.get(0).toString().equals("0")){
                     tblTenderEstCostDao.updateOrSaveEstCost(tblTenderEstCost);
                     flag = true;
                 }
            }
            else if(action.equals("Edit Official estimate cost"))
            {
                tblTenderEstCostDao.updateOrSaveEstCost(tblTenderEstCost);
                     flag = true;   
            }
        } catch (Exception e) {
            logger.error("addEstCost : " + e.toString());
            if(actionPerformed != null && actionPerformed.length != 0){
                action = "Error in " + actionPerformed[0];
            }
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTenderEstCost.get(0).getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Tender_Notice.getName(), action, "");
            action = null;
        }
        logger.debug("addEstCost :  Ends");
        return flag;
    }

    public boolean checkForLink(int tenderId) {
        logger.debug("checkForLink :  Starts");
        boolean flag = false;
        try {
            List list = tblTenderEstCostDao.findTblTenderEstCost("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
            if (!list.isEmpty()) {
                flag = true;
            } else {
                flag = false;
            }

        } catch (Exception e) {
            logger.error("checkForLink : " + e.toString());
        }
        logger.debug("checkForLink :  Ends");
        return flag;
    }

    public List<TblTenderEstCost> getData(int tenderId, int pckLotId) {
        logger.debug("getData :  Starts");
        boolean flag = false;
        List<TblTenderEstCost> list = null;
        try {
            list = tblTenderEstCostDao.findTblTenderEstCost("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "pkgLotId", Operation_enum.EQ, pckLotId);

        } catch (Exception e) {
            logger.error("getData : " + e.toString());
        }
        logger.debug("getData :  Ends");
        return list;
    }
}
