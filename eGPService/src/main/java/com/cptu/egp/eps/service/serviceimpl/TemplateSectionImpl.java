/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.service.serviceinterface.TemplateSection;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class TemplateSectionImpl implements TemplateSection {

    TblTemplateSectionsDao tblTemplateSectionsDao;
     private String logUserId="0";
    final Logger logger = Logger.getLogger(TemplateSectionImpl.class);
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public TblTemplateSectionsDao getTblTemplateSectionsDao() {
        return tblTemplateSectionsDao;
    }

    public void setTblTemplateSectionsDao(TblTemplateSectionsDao tblTemplateSectionsDao) {
        this.tblTemplateSectionsDao = tblTemplateSectionsDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    

    @Override
    public boolean createTemplateSection(TblTemplateSections tblTemplateSections) {
        
        logger.debug("createTemplateSection : "+logUserId+"starts");
        boolean loginFlag = false;
        String action  = "Create Section";
        try {
            tblTemplateSectionsDao.addTblTemplateSections(tblTemplateSections);
            loginFlag = true;
        }
        catch (Exception ex) {
            action  = "Error in "+action+ " "+ex.getMessage();
           logger.error("createTemplateSection : "+ex);
            loginFlag = false;
        } finally{
            makeAuditTrailService.generateAudit(auditTrail, tblTemplateSections.getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
        }
        logger.debug("createTemplateSection : "+logUserId+"ends");
        return loginFlag;
    }
    
    @Override
    public List<TblTemplateSections> getTemplateSection(short templateId) {

logger.debug("getTemplateSection : "+logUserId+"starts");
        List<TblTemplateSections> tblTemplateSections;
        try {
            tblTemplateSections = tblTemplateSectionsDao.findTblTemplateSections("templateId", Operation_enum.EQ , templateId);
        }
        catch (Exception ex) {
           logger.error("getTemplateSection : "+ex);
            tblTemplateSections = null;
        }
        logger.debug("getTemplateSection : "+logUserId+"ends");
        return tblTemplateSections;
    }

    @Override
    public List<TblTemplateSections> getSectionType(int sectionId) {

logger.debug("getSectionType : "+logUserId+"starts");
        List<TblTemplateSections> tblTemplateSections;
        try {
            tblTemplateSections = tblTemplateSectionsDao.findTblTemplateSections("sectionId", Operation_enum.EQ , sectionId);
        }
        catch (Exception ex) {
            logger.error("getSectionType : "+ex);
            tblTemplateSections = null;
        }
        logger.debug("getSectionType : "+logUserId+"ends");
        return tblTemplateSections;
    }

    @Override

public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public List<TblTemplateSections> getSecName(short templateId,String type) {
       logger.debug("getSecName : "+logUserId+"starts");
        List<TblTemplateSections> tblTemplateSections;
        try {
            tblTemplateSections = tblTemplateSectionsDao.findTblTemplateSections("templateId", Operation_enum.EQ, templateId,"contentType",Operation_enum.EQ, type);
        }
        catch (Exception ex) {
            logger.error("getSecName : "+ex);
            tblTemplateSections = null;
        }
        logger.debug("getSecName : "+logUserId+"ends");
        return tblTemplateSections;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
