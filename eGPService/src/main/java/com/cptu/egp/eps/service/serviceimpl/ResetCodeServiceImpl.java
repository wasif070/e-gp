/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceinterface.ResetCodeService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ResetCodeServiceImpl implements ResetCodeService {

    static final Logger logger = Logger.getLogger(ResetCodeServiceImpl.class);
    private String logUserId = "0";
    TblLoginMasterDao tblLoginMasterDao;

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    @Override
    public void storeResetCode(int userId, String resetPasswordCode) {
        logger.debug("storeResetCode : " + logUserId + " Starts");
         try {
            List<TblLoginMaster> tblLoginMasters = tblLoginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId);
            int size = tblLoginMasters.size();
            if (size > 0) {
                TblLoginMaster tblLoginMaster = tblLoginMasters.get(0);
                tblLoginMaster.setResetPasswordCode(resetPasswordCode);
                //tblLoginMaster.getCountResetSMS()
                String count = String.valueOf(tblLoginMaster.getCountResetSMS()+1);
                //System.out.print(count+"count");
                tblLoginMaster.setCountResetSMS(Byte.valueOf(count));
                tblLoginMasterDao.updateTblLoginMaster(tblLoginMaster);
            } else {
                logger.debug("storeResetCode : Reset Code - Record NOT Found!!");
            }

        } catch (Exception ex) {
            logger.error("storeResetCode : " + logUserId + ex);
        }
        logger.debug("storeResetCode : " + logUserId + " Ends");
    }

    @Override
    public boolean verifyResetCode(String mailId, String resetPasswordCode) {
        logger.debug("verifyResetCode : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            Object[] values = {"emailId", Operation_enum.LIKE, mailId, "resetPasswordCode", Operation_enum.LIKE, resetPasswordCode};
            List<TblLoginMaster> tblLoginMasters = tblLoginMasterDao.findTblLoginMaster(values);
            int size = tblLoginMasters.size();
            if (size > 0) {
                TblLoginMaster tlm = tblLoginMasters.get(0);
                tlm.setFailedAttempt((byte) 0);
                tlm.setCountResetSMS((byte) 0);
                tblLoginMasterDao.updateTblLoginMaster(tlm);
                bSuccess =  true;
            } else {
                logger.debug("verifyResetCode : Verify Code - Record NOT Found!!");
            }

        } catch (Exception ex) {
            bSuccess = false;
            logger.error("checkForName : " + logUserId + ex);
        }
        logger.debug("verifyResetCode : " + logUserId + " Ends");
        return bSuccess;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
}
