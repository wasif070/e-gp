/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblQuizQuestion;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface QuizQuestionService {
    
    public void addTblQuizQuestion(TblQuizQuestion tblQuizQuestion);
    
    public void deleteTblQuizQuestion(TblQuizQuestion tblQuizQuestion);

    /**
     *
     * @param tblQuizQuestion
     * @return
     */
    public String updateTblQuizQuestion(TblQuizQuestion tblQuizQuestion);

    public List<TblQuizQuestion> getAllTblQuizQuestion();
    
    /**
     *
     * @param id
     * @return
     */
    public List<TblQuizQuestion> findTblQuizQuestion(int id);
    
    /**
     *
     * @param id
     * @return
     */
    public List<TblQuizQuestion> findTblQuizQuestionByQid(int id);
    
}
