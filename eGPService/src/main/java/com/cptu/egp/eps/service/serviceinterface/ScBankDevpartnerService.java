/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ScBankDevpartnerService {

    /**
     * Handling request of ajax call for verify the office name
     * @param name
     * @param isBranch
     * @param partnerType
     * @param headOffice
     * @return 'Already Exists' or 'OK'
     */
    public String verifyOfficeName(String name, String isBranch, String partnerType, int headOffice, String mainOfcId);

    /**
     * Verify the unique national Id
     * @param nationalId
     * @return 'Already Exists' or 'OK'
     */
    public String verifyNationalId(String nationalId);

    /**
     * Verify unique mobile no
     * @param mobileNo
     * @return 'Already Exists' or 'OK'
     */
    public String verifyMobileNo(String mobileNo);

    /**
     * Create Schedule Bank or Development Partner insert in tblScBankDevPartnerMaster
     * @param tblScBankDevPartnerMaster
     * @return 1 or 0
     */
    public int  createScBankDevPartner(TblScBankDevPartnerMaster tblScBankDevPartnerMaster);

    /**
     * Get List of bank Partner
     * @param type
     * @return list of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> getBankPartnerList(String type);

    /**
     * Get list of designation
     * @return List of TblDesignationMaster
     */
    public List<TblDesignationMaster> getDesignationMasters();

    /**
     * Create schedule bank or development partner users
     * @param tblLoginMaster
     * @param tblPartnerAdmin
     * @return 
     */
    public String createScBankDevPartnerUser(TblLoginMaster tblLoginMaster, TblPartnerAdmin tblPartnerAdmin);

    /**
     * Use of view and used in grid 
     * @param userId
     * @return
     */
    public VwGetSbDevPartner getUserInfo(int userId);

    /**
     * Update  TblPartnerAdmin
     * @param tblPartnerAdmin
     * @return
     */
    public boolean updateScBankDevPartnerUser(TblPartnerAdmin tblPartnerAdmin);

    /**
     * update TblScBankDevPartnerMaster
     * @param tblScBankDevPartnerMaster
     * @return
     */
    public boolean  updateScBankDevPartner(TblScBankDevPartnerMaster tblScBankDevPartnerMaster);

    /**
     * get information of scheduled bank information
     * @param sBankDevId
     * @return TblScBankDevPartnerMaster
     */
    public TblScBankDevPartnerMaster getSbDevInfo(int sBankDevId,String...value);

    //public Object[] retriveAdminInfo(int userId);
    /**
     * retrive admin info using userId and userTypeId
     * @param userId
     * @param userTypeId
     * @return Object[] [0]userId, [1]sbankDevelopId, [2]sbDevelopName, [3]emailId, [4]fullName, [5]mobileNo, [6]nationalId, [7]partnerId, [8]isAdmin, [9]designation, [10]isMakerChecker
     */
    public Object[] retriveAdminInfo(int userId, byte userTypeId,String...values);

    /**
     * Bank Name and Id for scheduled bank and development partner administrator
     * @param branchId
     * @return Object[] [0]sbankDevelopId, [1]sbDevelopName
     */
    public Object[] bankNameForAdmin(int branchId);

    /**
     * update tblPartnerAdmin
     * @param tblPartnerAdmin
     */
    public void updateDevelopmentScBankAdmin(TblPartnerAdmin tblPartnerAdmin,String...values);

    /**
     * Get record for Grid
     * @param firstResult
     * @param maxResult
     * @param values
     * @return List of VwGetSbDevPartner
     */
    public List<VwGetSbDevPartner> findAdminList(int firstResult, int maxResult, Object... values);

    /**
     * get count office administrator
     * @param from
     * @param where
     * @return long
     * @throws Exception
     */
    public long getCntOfficeAdmin(String from, String where) throws Exception;

    /**
     * get count of office
     * @param from
     * @param where
     * @return long
     * @throws Exception
     */
    public long getCntOffice(String from, String where) throws Exception;

    /**
     * get List of office for grid
     * @param firstResult
     * @param maxResult
     * @param values
     * @return list of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> findOfficeList(int firstResult, int maxResult, Object... values);

    /**
     * grid search functionality
     * @param partnerType
     * @param userTypeId
     * @param userId
     * @param ascClause
     * @return List of Object[] [0]sbankDevelopId, [1]sbDevelopName, [2]sbDevelopName, [3]stateName, [4]countryName
     */
    public List<Object[]> findBranchOfficeList(String partnerType, int userTypeId, int userId, String ascClause);

    /**
     * grid search functionality
     * @param partnerType
     * @param userTypeId
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return List of Object[] [0]sbankDevelopId, [1]sbDevelopName, [2]sbDevelopName, [3]stateName, [4]countryName
     */
    public List<Object[]> findBranchOfficeList(String partnerType, int userTypeId, int userId, String searchField, String searchString, String searchOper);

    /**
     * grid search functionality
     * @param firstResult
     * @param maxResult
     * @param orderClause
     * @param partnerType
     * @return List of Object[] [0]sbDevelopName, [1]countryName, [2]stateName, [3]sbankDevelopId
     */
    public List<Object[]> findDevPartnerOrg(int firstResult, int maxResult, String orderClause, String partnerType);

    /**
     * grid search functionality
     * @param firstResult
     * @param maxResult
     * @param orderClause
     * @return List of Object[] [0]sbDevelopName, [1]countryName, [2]stateName, [3]sbankDevelopId
     */
    public List<Object[]> findDevPartnerOrg(int firstResult, int maxResult, String orderClause);

    /**
     * check user is admin or not
     * @param userId
     * @return boolean
     */
    public boolean isAdmin(int userId);

    /**
     * Get brancList for egp admin bank admin and branch user
     * @param values
     * @return List of TblScBankDevPartnerMaster
     * @throws Exception
     */
    public List<TblScBankDevPartnerMaster> findBranchList(Object... values) throws Exception;

    /**
     * Check bran checker for branch
     * @param sBankDevelHeadId
     * @param role
     * @return 'already exists' or 'OK'
     */
    public String bankChecker(int sBankDevelHeadId, String role);

    /**
     * get Phone code for sms purpose
     * @param branch
     * @return List of Object (Country Code)
     */
    public List<Object> getPhoneCode(int branch);

    /**
     * Get User List for Grid
     * @param firstResult
     * @param maxResult
     * @param bankId
     * @param type
     * @param orderType
     * @param e
     * @param searchField
     * @param searchType
     * @return List of object[] [0]fullName, [1]emailId, [2]userId, [3]sbDevelopName, [4]isMakerChecker, [5]status, [6]partnerId
     */
    public List<Object[]> getUserListGrid(int firstResult, int maxResult, int bankId, String type, String orderType, String e, String searchField, String searchType);

    /**
     * Get bankName
     * @return List of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> getBankName();

    /**
     * Get information about bank development partner
     * @param offcet
     * @param row
     * @param sbankdevelopid
     * @return List of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> getBankDevPartnerMasterinfo(int offcet, int row, int sbankdevelopid);

    /**
     * Get information about bank development partner
     * @param offcet
     * @param row
     * @param sbankdevelopid
     * @return List of Object[] [0]countryName, [1]sbDevelopName, [2]phoneNo, [3]faxNo, [4]officeAddress, [5]thanaUpzilla, [6]cityTown, [7]stateName, [8]postCode
     */
    public List<Object[]> getBankDevPMasterInfo(int offcet, int row, int sbankdevelopid,String branchName,String thana,String city,String district);

    /**
     * Get Count of brank branch
     * @param sbankdevelopid
     * @return long
     */
    public long getBankBranchCount(int sbankdevelopid);


    public int getAdvanceBankBranchCount(int sbankdevelopid,String branchName,String thana,String city,String district);


    /**
     * For logging purpose
     * @param logUserId
     */
    public void setUserId(String logUserId);

    /**
     * Get BrachAdmin Grid for egp admin and bankAdmin
     * @param firstResult
     * @param maxResult
     * @param bankId
     * @param type
     * @param orderType
     * @param e
     * @param searchField
     * @param searchType
     * @return List of [0]fullName, [1]emailId, [2]userId, [3]sbDevelopName, [4]isMakerChecker, [5]status
     */
    public List<Object []> getBranchAdminListGrid(int firstResult, int maxResult,int bankId, String type,String orderType,String e,String searchField,String searchType);

    /**
     * For Branch and Bank Name
     * @param userId
     * @return List [0] branchId,[1] Branch,[2] BankId,[3] BankName
     */
    public List<Object []> getBankBranchByUserId(int userId);

    /**
     * Get developmentPartnerList
     * @return List of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> getDevelopPartnerList();

    /**
     * Insert record in to partner admin transfer
     * @param tblPartnerAdminTransfer
     * @return boolean
     */
    boolean insertIntopartneradmintransfer(TblPartnerAdminTransfer tblPartnerAdminTransfer);
    
    public void setAuditTrail(AuditTrail auditTrail);

}
