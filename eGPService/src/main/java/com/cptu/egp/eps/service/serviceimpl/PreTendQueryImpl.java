/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderMetDocsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblPreTenderMetDocs;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class PreTendQueryImpl {

    final Logger logger = Logger.getLogger(PreTendQueryImpl.class);

    SPXMLCommon preTendQueryImpl;
    SPTenderCommon pTenderCommon;
    TblPreTenderMetDocsDao  preTenderMetDocsDao;
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }


    public SPXMLCommon getPreTendQueryImpl() {
        return preTendQueryImpl;
    }

    public void setPreTendQueryImpl(SPXMLCommon preTendQueryImpl) {
        this.preTendQueryImpl = preTendQueryImpl;
    }

    public SPTenderCommon getpTenderCommon() {
        return pTenderCommon;
    }

    public void setpTenderCommon(SPTenderCommon pTenderCommon) {
        this.pTenderCommon = pTenderCommon;
    }

    public TblPreTenderMetDocsDao getPreTenderMetDocsDao() {
        return preTenderMetDocsDao;
    }

    public void setPreTenderMetDocsDao(TblPreTenderMetDocsDao preTenderMetDocsDao) {
        this.preTenderMetDocsDao = preTenderMetDocsDao;
    }

    /**
     * this method is for getting the clarification data from database
     * @param
     * @return list, list of data
     */
    public List<CommonMsgChk> InsPreTendQueryOperationDetailsByXML(String param1,String param2,String param3,String param4){
        logger.debug("InsPreTendQueryOperationDetailsByXML : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        try {
            list = preTendQueryImpl.executeProcedure(param1, param2, param3,param4);
        } catch (Exception e) {
            logger.error("InsPreTendQueryOperationDetailsByXML : "+logUserId+" : "+e);
    }
        logger.debug("InsPreTendQueryOperationDetailsByXML : "+logUserId+" Ends");
        return list;
    }

    /**
     * this method is for getting the clarification data from database
     * @param
     * @return list, list of data
     */
    public List<SPTenderCommonData> getDataPreTenderQueryFromSP(String fieldName1,String fieldName2, String fieldName3){
        logger.debug("getDataPreTenderQueryFromSP : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try {
            list =pTenderCommon.executeProcedure(fieldName1, fieldName2, fieldName3);
        } catch (Exception e) {
                logger.error("getDataPreTenderQueryFromSP : "+logUserId+" : "+e);
    }
        logger.debug("getDataPreTenderQueryFromSP : "+logUserId+" Ends");
        return list;
    }

    /**
     * Fetching information from TblPreTenderMetDocs
     * @param tenderid
     * @return boolean, true - if fetched successfully otherwise false
     */
    public boolean isPrebidPublished(int tenderId){
        logger.debug("isPrebidPublished : "+logUserId+" Starts");
        List<TblPreTenderMetDocs> metDocs =null;
         boolean flag=false;
        try {               
                metDocs = preTenderMetDocsDao.findTblPreTenderMetDocs("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"prebidStatus",Operation_enum.EQ,"Approved");
                if(metDocs.size() > 0){
                    flag=true;
                }
                
        } catch (Exception e) {
            logger.error("isPrebidPublished : "+logUserId+" : "+e);
            flag = false;
        }
        logger.debug("isPrebidPublished : "+logUserId+" Ends");
            return flag;
        }
}
