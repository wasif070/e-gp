/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationPEDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPSharedPEDetailForRHD;
import com.cptu.egp.eps.service.serviceinterface.RHDIntegrationPEDetailsService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sudhir Chavhan
 */
public class RHDIntegrationPEDetailsServiceImpl implements RHDIntegrationPEDetailsService {

    final Logger logger = Logger.getLogger(RHDIntegrationPEDetailsServiceImpl.class);
    private String logUserId = "0";
    private SPSharedPEDetailForRHD spSharedPEDetailForRHD;

    public SPSharedPEDetailForRHD getSpSharedPEDetailForRHD() {
        return spSharedPEDetailForRHD;
    }
    public void setSpSharedPEDetailForRHD(SPSharedPEDetailForRHD spSharedPEDetailForRHD) {
        this.spSharedPEDetailForRHD = spSharedPEDetailForRHD;
    }
    
    @Override
    public void setLogUserId(String logUserId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    /***
     * This method returns the all the records of sharedPEdetails only for RHD Dept.
     * @return List of RHDIntegrationDetails
     * @throws Exception
     */
    @Override
    public List<RHDIntegrationPEDetails> getSharedPEDetailForRHD() throws Exception {
        logger.debug("getSharedPEDetailForRHD : " + logUserId + " Starts");
        List<RHDIntegrationPEDetails> sharedPEDetailForRHDList = null;
        try {
            sharedPEDetailForRHDList = spSharedPEDetailForRHD.executeProcedure();
        } catch (Exception ex) {
            logger.error("getSharedPEDetailForRHD : " + logUserId + " : " + ex.toString());
            //System.out.println("Exception PromisIntegrationDetailsInfoServiceImpl:-" + ex.toString());
        }
        logger.debug("getSharedPEDetailForRHD : " + logUserId + " Ends");
        //  System.out.println("PromisIntegrationDetailsInfoServiceImpl:-"+sharedPEDetailForPromisList);
        return sharedPEDetailForRHDList;
    }
}
