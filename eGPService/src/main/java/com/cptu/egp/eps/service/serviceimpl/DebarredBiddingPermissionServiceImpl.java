/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarredBiddingPermissionDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblDebarredBiddingPermission;
import com.cptu.egp.eps.service.serviceinterface.DebarredBiddingPermissionService;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import static com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl.logger;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.text.DateFormatter;
import org.apache.log4j.Logger;
import sun.security.x509.X500Name;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public class DebarredBiddingPermissionServiceImpl implements DebarredBiddingPermissionService {

    private TblDebarredBiddingPermissionDao tblDebarredBiddingPermissionDao;
    private final Logger logger = Logger.getLogger(DebarredBiddingPermissionServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;
    private HibernateQueryDao hibernateQueryDao;

    @Override
    public String updateDebarredBiddingList(List<TblDebarredBiddingPermission> debarPermissions, String debarredUser, String companyId) {
        logger.debug("updateDebarredBiddingList : " + logUserId + loggerStart);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String msg = "success";
        try {
            //int j = hibernateQueryDao.updateDeleteNewQuery("delete from TblDebarredBiddingPermission tbp where tbp.userId = " + debarredUser + "  and tbp.companyId=" + companyId);
            for (int i = 0; i < debarPermissions.size(); i++) {
                getTblDebarredBiddingPermissionDao().addTblDebarredBiddingPermission(debarPermissions.get(i));
            }
        } catch (Exception e) {
            logger.error("updateDebarredBiddingList : " + logUserId + e);
            msg = "fail";
        }
        logger.debug("updateDebarredBiddingList : " + logUserId + loggerEnd);
        return msg;
    }

    @Override
    public void addDebarredBiddingPermission(TblDebarredBiddingPermission tblDebarredBiddingPermission) {
        getLogger().debug("add DebarredBiddingPermission : " + logUserId + loggerStart);
        String action = "";
        try {

            tblDebarredBiddingPermissionDao.addTblDebarredBiddingPermission(tblDebarredBiddingPermission);
        } catch (Exception e) {
            action = "add DebarredBiddingPermission : " + e;
            getLogger().error("add DebarredBiddingPermission : " + logUserId + e);
        } finally {
            action = null;
        }
        getLogger().debug("add DebarredBiddingPermission : " + logUserId + loggerEnd);
    }

    /**
     * @return the tblDebarredBiddingPermissionDao
     */
    public TblDebarredBiddingPermissionDao getTblDebarredBiddingPermissionDao() {
        return tblDebarredBiddingPermissionDao;
    }

    /**
     * @param tblDebarredBiddingPermissionDao the
     * tblDebarredBiddingPermissionDao to set
     */
    public void setTblDebarredBiddingPermissionDao(TblDebarredBiddingPermissionDao tblDebarredBiddingPermissionDao) {
        this.tblDebarredBiddingPermissionDao = tblDebarredBiddingPermissionDao;
    }

    /**
     * @return the makeAuditTrailService
     */
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    /**
     * @param makeAuditTrailService the makeAuditTrailService to set
     */
    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * @return the logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    @Override
    public List<TblDebarredBiddingPermission> findTblDebarredBiddingPermission(Object... values) throws Exception {
        return tblDebarredBiddingPermissionDao.findTblDebarredBiddingPermission(values); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean deleteDebarredBiddingPermission(int debarredUser, int companyId, int userId, String reinstateComments, String category){
        try{
            int j = hibernateQueryDao.updateDeleteNewQuery("update TblDebarredBiddingPermission tbp set tbp.isActive=0, tbp.reinstateComments='"+reinstateComments+"' where tbp.userId = " + debarredUser + "  and tbp.companyId=" + companyId+" and tbp.isActive=1 and tbp.procurementCategory='"+category+"'");
            return true;
        }catch(Exception ex){
            System.out.print(ex);
            return false;
        }
    }
    @Override
    public boolean UpdateDebarredBiddingPermission(String debaruserId, String category, String fDate, String uptoDate){
        try{
            int j = hibernateQueryDao.updateDeleteNewQuery("update TblDebarredBiddingPermission tbp set tbp.debarredFrom='"+fDate+"', tbp.debarredUpto='"+uptoDate+"' where tbp.userId = " + debaruserId + " and tbp.isActive=1 and tbp.procurementCategory='"+category+"'");
            return true;
        }catch(Exception ex){
            System.out.print(ex);
            return false;
        }
    }
}
