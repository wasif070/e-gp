/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblSubContracting;

/**
 *
 * @author Administrator
 */
public interface SubContractingService {
    
   
    /**
     * Get Company details
     * @param emailId
     * @return TblCompanyMaster object
     */
    public TblCompanyMaster getCompanyDetails(String emailId);
    /**
     * This method is for chek the details of subcontractor
     * @param tenderId
     * @param invToUserId
     * @return String true of false
     */
    public String checkSubContractorDetails(Integer tenderId,Integer invToUserId);
    /**
     * This mehtod is used for get the subcontractor details
     * @param tenderId
     * @return TblSubContracting object
     */
    public TblSubContracting getSubcontractDetails(Integer tenderId);
    /**
     * This method is used for emailId of sub Contractor
     * @param tenderId
     * @return tblLoginmaster object
     */
    public TblLoginMaster getSubContEmailId(Integer tenderId);
    /**
     * Logger Perpo
     * @param userId
     */
    public void setUserId(String userId);
    
     /**
     * This method give details of whether user is already do subcontracting or not
     * @param emailId
     * @param tenderId
     * @param pkgLotId
     * @return ok if true else already sent to persons emailID
     */
    public String checkUserEmail(String emailId,int tenderId, int pkgLotId);
    
    /**
     * This method give details of bid widraw status
     * @param tenderId
     * @param userId
     * @return true if bid is not widraw else bid is widraw
     */
    public String getBidWidrawStatus(int tenderId,int userId);

}
