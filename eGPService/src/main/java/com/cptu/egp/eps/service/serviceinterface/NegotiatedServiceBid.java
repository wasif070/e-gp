/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.model.table.TblNegotiatedBid;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class NegotiatedServiceBid {

    private final Logger logger = Logger.getLogger(NegotiatedServiceBid.class);

    TblNegotiatedBid tblNegotiatedBid;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblNegotiatedBid getTblNegotiatedBid() {
        return tblNegotiatedBid;
    }

    public void setTblNegotiatedBid(TblNegotiatedBid tblNegotiatedBid) {
        this.tblNegotiatedBid = tblNegotiatedBid;
    }

    /**
     * fetch Call Value by Tender Table id, formId, rowId, tenderColumnId cellId and isQty.
     * @param tenderTableId
     * @param formId
     * @param rowId
     * @param tenderColumnId
     * @param cellId
     * @param isQty
     * @return no of bids.
     */
    public int getCallValue(int tenderTableId, int formId, int rowId, int tenderColumnId, int cellId, int isQty){
        int cellValue = 0;
        try{
            List<Object> objects = hibernateQueryDao.singleColQuery("SELECT  tblNegotiatedBid.cellValue FROM TblNegotiatedBid as tblNegotiatedBid where tblNegotiatedBid.tenderTableId = "+tenderTableId+" and formId="+formId+" and rowId ="+rowId+" and tenderColumnId = "+tenderColumnId+" and cellId= "+cellId+" and isQty ="+isQty);
            cellValue = Integer.parseInt(objects.get(0).toString());
        }catch(Exception ex){
            logger.error("getCallValue : " + logUserId + ":"+ex);
        }
        return cellValue;
    }
}
