/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UserLoginService {
    
    /**
     * Mehtod give loginStatus s
     * @param emailId
     * @param password
     * @param ipAddress
     * @return
     */
    public String checkLoginStatus(String emailId,String password,String ipAddress);
    /**
     * Update tblloginmaster
     * @param loginMaster
     * @return boolean
     */
    public boolean updateLoginMaster(TblLoginMaster loginMaster,String...values);
    /**
     * Insert Login track record
     * @param userId
     * @return boolean
     */
    public boolean insertLoginTrack(int userId);

    /**
     * Verify username and password
     * @param mailId
     * @param password
     * @return String
     */
    public String verifyEmail(String mailId,String password);

    /**
     * Inser record in to tbl_sessionMaster
     * @param tblSessionMaster
     * @return int
     */
    public int insertSessionMaster(TblSessionMaster tblSessionMaster);
    /**
     * Update tbl_sessionMaster
     * @param sessionId
     * @return boolean
     */
    public boolean updateSessionMaster(int sessionId);
    /**
     * Get Last LoginTime 
     * @param userId
     * @return date
     */
    public Date getLastLoginTime(int userId);

    /**
     * Verify Password
     * @param password
     * @param userId
     * @return boolean
     */
    public Boolean verifyPwd(String password,Integer userId);
    /**
     * Verify Password for verification
     * @param password
     * @param emailId
     * @return int
     */
    public int verifyPwdforVerification(String password,String emailId);
    /**
     * Get Verification Code
     * @param userId
     * @return List of TblEmailVerificationCode
     */
    public List<TblEmailVerificationCode> getVeriCode(int userId);
    /**
     * Delete VerifyCode
     * @param emailVerificationId
     */
    public void deleteVerifyCode(int emailVerificationId);
    /**
     * Get Login master Detail
     * @param userId
     * @return list of TblLoginMaster
     */
    public List<TblLoginMaster> getLoginMasterDetails(int userId);
    /**
     * Get Expired user
     * @param expiryDate
     * @return List of TblLoginMaster
     */
    public List<TblLoginMaster> getExpiredUsers(Date expiryDate);

    /**
     * Get Registration Type
     * @param userId
     * @return String
     */
    public String getRegType(int userId);

    /**
     * For logging purpose
     * @param userId
     */
    public void setUserId(String userId);
    
    /**
     * This method is for when registering jvca it will show name of jvca in readonly mode
     * @param jvcId 
     * @return Name of JVCA
     */
    public String jvcaName(int jvcId);
    
    public void setAuditTrail(AuditTrail auditTrail);
}
