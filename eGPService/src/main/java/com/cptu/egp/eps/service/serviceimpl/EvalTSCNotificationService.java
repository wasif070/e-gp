/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalTSCNotificationDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblEvalTscnotification;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class EvalTSCNotificationService {

    private static final Logger LOGGER = Logger.getLogger(EvalTSCNotificationService.class);
    
    private TblEvalTSCNotificationDao notificationDao;
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public TblEvalTSCNotificationDao getNotificationDao() {
        return notificationDao;
    }

    public void setNotificationDao(TblEvalTSCNotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    
    public boolean addEvalNotification(TblEvalTscnotification tblEvalTscnotification){
        LOGGER.debug("addEvalNotification : " + logUserId + " Starts");
        String action = "Recommendation notified to Chair Person by Technical Sub Committee";
        boolean flag=false;
        try{
            notificationDao.addTblEvalTscnotification(tblEvalTscnotification);
            flag=true;
        }catch(Exception e){
            LOGGER.error("addEvalNotification : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblEvalTscnotification.getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, "");
            action = null;
        }
        LOGGER.debug("addEvalNotification : " + logUserId + " ends");
        return  flag;

    }

    public List<TblEvalTscnotification> findEvalTscNtfctn(String tenderId){
        LOGGER.debug("findEvalTscNtfctn : " + logUserId + " Starts");
        List<TblEvalTscnotification> list = null;
        try{
           list = notificationDao.findTblEvalTscnotification("tenderId",Operation_enum.EQ,Integer.parseInt(tenderId));
        }catch(Exception e){
            LOGGER.error("findEvalTscNtfctn : " + logUserId + e);
        }
        LOGGER.debug("findEvalTscNtfctn : " + logUserId + " ends");
        return  list;

    }
    
    /**
     *Check whether Member has notified Chairperson
     * @param tenderId from tbl_TenderMaster
     * @return true or false for notified or not notified
     */
    public boolean isNotify(String tenderId){
        LOGGER.debug("findEvalTscNtfctn : " + logUserId + " Starts");
        boolean flag = false;
        try{
           if(notificationDao.countForQuery("TblEvalTscnotification", "tenderId="+tenderId)>0){
               flag = true;
           }
        }catch(Exception e){
            LOGGER.error("findEvalTscNtfctn : " + logUserId + e);
        }
        LOGGER.debug("findEvalTscNtfctn : " + logUserId + " ends");
        return  flag;

    }

   
}
