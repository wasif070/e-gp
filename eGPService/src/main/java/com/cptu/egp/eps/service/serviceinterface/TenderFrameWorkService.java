/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderFrameWork;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public interface TenderFrameWorkService {

    public void addTblTenderFrameWork(TblTenderFrameWork tblTenderFrameWork,int userID);
    public void addTblTenderFrameWork(int appid, int packageId,int tenderId,String[] peofficeId, String[] officeName,String[] OfficeDistrict, String[] OfficeAddress, String[] ItemName,String[] Quantity, String[] Unit, int UserID, boolean isMap);
    public List<TblTenderFrameWork> findTblTenderFrameWork(Object... values) throws Exception ;
    public void setLogUserId(String logUserId);
}
