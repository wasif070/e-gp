/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;


import com.cptu.egp.eps.dao.storedprocedure.APPSearchDtBean;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AppAdvSearchService {
   
    /**
     * Get Search Result
     * @param Keyword
     * @param financialYear
     * @param procurementnature
     * @param departmentId
     * @param employeeId
     * @param cpvCode
     * @param projectName
     * @param procurementType
     * @param appid
     * @param appcode
     * @param packageno
     * @param pkgEstCost
     * @param officeId
     * @param budgetType
     * @param operator
     * @param value1
     * @param value2
     * @param pageNo
     * @param recordOffset
     * @param status
     * @param userId
     * @return
     */
    List<CommonAppSearchData> getSearchResults(String keyword,String financialYear,String procurementnature,Integer departmentId,Integer employeeId,String cpvCode,String projectName,String procurementType ,Integer appid,String appcode,String packageno, BigDecimal pkgEstCost,Integer officeId,String budgetType,String operator,BigDecimal value1,BigDecimal value2,int pageNo,int recordOffset ,String status,Integer userId);

    /**
     * Get APP Search Data
     * @param financialYear
     * @param departmentId
     * @param stateId
     * @param officeId
     * @param pageNo
     * @param recordPerPage
     * @return
     */
    List<APPSearchDtBean> getAPPSearch(String financialYear,int departmentId,int stateId,int officeId,int pageNo,int recordPerPage);

    /**
     * Create Query by Table and condition
     * @param from
     * @param where
     * @return
     * @throws Exception
     */
    public long countForQuery(String from,String where) throws Exception;

    /**
     * fetch Procurement Method id
     * @param appId
     * @param pkgId
     * @return
     */
    public int getProcMethodId(int appId, int pkgId);

    /**
     * fetch if is PQ required or not.
     * @param appId
     * @param pkgId
     * @return
     */
    public String getIsPQReq(int appId, int pkgId);

    
    /**
     * Set userId at Service layer.
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * This Function is developed to get packageid based on appid
     * @param appId
     */
    public int getPackageIdByAppId(int appId);

    /**
     * This Function is developed to get procurementid from temp pkg details when app being revised based on appid,pkgId
     * @param appId
     * @param pkgId
     */
    public int getTempProcMethodId(int appId, int pkgId);

}