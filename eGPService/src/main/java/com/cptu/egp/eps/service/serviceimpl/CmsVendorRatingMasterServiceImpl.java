/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsVendorRatingMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import com.cptu.egp.eps.service.serviceinterface.CmsVendorRatingMasterService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsVendorRatingMasterServiceImpl implements CmsVendorRatingMasterService {

    final Logger logger = Logger.getLogger(CmsVendorRatingMasterServiceImpl.class);
    private TblCmsVendorRatingMasterDao cmsVendorRatingMasterDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "  ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsVendorRatingMasterDao getCmsVendorRatingMasterDao() {
        return cmsVendorRatingMasterDao;
    }

    public void setCmsVendorRatingMasterDao(TblCmsVendorRatingMasterDao cmsVendorRatingMasterDao) {
        this.cmsVendorRatingMasterDao = cmsVendorRatingMasterDao;
    }

    /***
     * This method inserts a TblCmsVendorRatingMaster object in database
     * @param tblCmsVendorRatingMaster
     * @return int
     */
    @Override
    public int insertCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("insertCmsVendorRatingMaster : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsVendorRatingMasterDao.addTblCmsVendorRatingMaster(tblCmsVendorRatingMaster);
            flag = tblCmsVendorRatingMaster.getVendorRatingId();
        } catch (Exception ex) {
            logger.error("insertCmsVendorRatingMaster : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsVendorRatingMaster : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsVendorRatingMaster object in DB
     * @param tblCmsVendorRatingMaster
     * @return boolean
     */
    @Override
    public boolean updateCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("updateCmsVendorRatingMaster : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsVendorRatingMasterDao.updateTblCmsVendorRatingMaster(tblCmsVendorRatingMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsVendorRatingMaster : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsVendorRatingMaster : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsVendorRatingMaster object in DB
     * @param tblCmsVendorRatingMaster
     * @return boolean
     */
    @Override
    public boolean deleteCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("deleteCmsVendorRatingMaster : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsVendorRatingMasterDao.deleteTblCmsVendorRatingMaster(tblCmsVendorRatingMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsVendorRatingMaster : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsVendorRatingMaster : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsVendorRatingMaster objects from database
     * @return List<TblCmsVendorRatingMaster>
     */
    @Override
    public List<TblCmsVendorRatingMaster> getAllCmsVendorRatingMaster() {
        logger.debug("getAllCmsVendorRatingMaster : " + logUserId + STARTS);
        List<TblCmsVendorRatingMaster> cmsVendorRatingMasterList = new ArrayList<TblCmsVendorRatingMaster>();
        try {
            cmsVendorRatingMasterList = cmsVendorRatingMasterDao.getAllTblCmsVendorRatingMaster();
        } catch (Exception ex) {
            logger.error("getAllCmsVendorRatingMaster : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsVendorRatingMaster : " + logUserId + ENDS);
        return cmsVendorRatingMasterList;
    }

    /***
     *  This method returns no. of TblCmsVendorRatingMaster objects from database
     * @return long
     */
    @Override
    public long getCmsVendorRatingMasterCount() {
        logger.debug("getCmsVendorRatingMasterCount : " + logUserId + STARTS);
        long cmsVendorRatingMasterCount = 0;
        try {
            cmsVendorRatingMasterCount = cmsVendorRatingMasterDao.getTblCmsVendorRatingMasterCount();
        } catch (Exception ex) {
            logger.error("getCmsVendorRatingMasterCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsVendorRatingMasterCount : " + logUserId + ENDS);
        return cmsVendorRatingMasterCount;
    }

    /***
     * This method returns TblCmsVendorRatingMaster for the given Id
     * @param int vendorRatingId
     * @return TblCmsVendorRatingMaster
     */
    @Override
    public TblCmsVendorRatingMaster getCmsVendorRatingMaster(int vendorRatingId) {
        logger.debug("getCmsVendorRatingMaster : " + logUserId + STARTS);
        TblCmsVendorRatingMaster tblCmsVendorRatingMaster = null;
        List<TblCmsVendorRatingMaster> cmsVendorRatingMasterList = null;
        try {
            cmsVendorRatingMasterList = cmsVendorRatingMasterDao.findTblCmsVendorRatingMaster("vendorRatingId", Operation_enum.EQ, vendorRatingId);
        } catch (Exception ex) {
            logger.error("getCmsVendorRatingMaster : " + logUserId + SPACE + ex);
        }
        if (!cmsVendorRatingMasterList.isEmpty()) {
            tblCmsVendorRatingMaster = cmsVendorRatingMasterList.get(0);
        }
        logger.debug("getCmsVendorRatingMaster : " + logUserId + ENDS);
        return tblCmsVendorRatingMaster;
    }

    /***
     * This method gives the TblCmsVendorRatingMaster object for the given Star Rating
     * @param String rating
     * @return TblCmsVendorRatingMaster
     */
    @Override
    public TblCmsVendorRatingMaster getCmsVendorRatingMasterForRating(String rating) {
        logger.debug("getCmsVendorRatingMasterForRating : " + logUserId + STARTS);
        TblCmsVendorRatingMaster tblCmsVendorRatingMaster = null;
        List<TblCmsVendorRatingMaster> cmsVendorRatingMasterList = null;
        try {
            cmsVendorRatingMasterList = cmsVendorRatingMasterDao.findTblCmsVendorRatingMaster("vendorRatings", Operation_enum.EQ, rating);
        } catch (Exception ex) {
            logger.error("getCmsVendorRatingMasterForRating : " + logUserId + SPACE + ex);
        }
        if (!cmsVendorRatingMasterList.isEmpty()) {
            tblCmsVendorRatingMaster = cmsVendorRatingMasterList.get(0);
        }
        logger.debug("getCmsVendorRatingMasterForRating : " + logUserId + ENDS);
        return tblCmsVendorRatingMaster;
    }
}
