/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblOrgMaster;
import java.util.List;

/**
 *
 * @author Sudhir Chavhan
 */
public interface OrgMasterService {

    public void setOrgId(String orgId);

    public void addTblOrgMaster(TblOrgMaster org);

    public void deleteTblOrgMaster(TblOrgMaster org);

    public void updateTblOrgMaster(TblOrgMaster org);

    public List<TblOrgMaster> getAllTblOrgMaster();

    public List<TblOrgMaster> findTblOrgMaster(Object... values);

    public List<TblOrgMaster> findByCountTblOrgMaster(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblOrgMasterCount();

    public String getWSRights(String org);
}
