/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.FinancialDelegationData;
import com.cptu.egp.eps.dao.storedprocedure.SPFinancialDelegation;
import com.cptu.egp.eps.service.serviceinterface.FinancialDelegationService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Istiak (Dohatec) - Jun 14, 2015
 */
public class FinancialDelegationServiceImpl implements FinancialDelegationService {

    private static final Logger LOGGER = Logger.getLogger(FinancialDelegationServiceImpl.class);
    private SPFinancialDelegation sPFinancialDelegation;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    /**
     * @return the sPFinancialDelegation
     */
    public SPFinancialDelegation getsPFinancialDelegation() {
        return sPFinancialDelegation;
    }

    /**
     * @param sPFinancialDelegation the sPFinancialDelegation to set
     */
    public void setsPFinancialDelegation(SPFinancialDelegation sPFinancialDelegation) {
        this.sPFinancialDelegation = sPFinancialDelegation;
    }

    /**
     * @return the logUserId
     */
    public String getLogUserId() {
        return logUserId;
    }

    /**
     * @param logUserId the logUserId to set
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * @return the makeAuditTrailService
     */
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    /**
     * @param makeAuditTrailService the makeAuditTrailService to set
     */
    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    @Override
    public List<FinancialDelegationData> financialDelegationData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15) {

        List<FinancialDelegationData> list = null;
        String action = "";
        try{
            list = sPFinancialDelegation.executeProcedure(fieldName1, fieldName2, fieldName3, fieldName4, fieldName5, fieldName6, fieldName7, fieldName8, fieldName9, fieldName10, fieldName11, fieldName12, fieldName13, fieldName14, fieldName15);

            if(fieldName1.equalsIgnoreCase("DeleteRows")){
                if("true".equalsIgnoreCase(list.get(0).getFieldName1()) && list != null){
                    action = "Remove Financial Delegation Configuration";
                }else{
                    action = "Error in Remove Financial Delegation Configuration";
                }
                makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            }
            else if(fieldName1.equalsIgnoreCase("DeleteRanks")){
                if("true".equalsIgnoreCase(list.get(0).getFieldName1()) && list != null){
                    action = "Remove Financial Delegation Designation Rank Configuration";
                }else{
                    action = "Error in Remove Financial Delegation Designation Rank Configuration";
                }
                makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            }
        }catch(Exception e){
            LOGGER.error("\nprocTimelineData() : " + e.toString());
            action = "Error in Financial Delegation Configuration. " + e.toString();
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }

        return list;

    }

    @Override
    public int getFinancialDelegationCount() {
        try{
            return Integer.parseInt(sPFinancialDelegation.executeProcedure("CountRules", "", "", "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1());
        }catch(Exception e){
            LOGGER.error("\ngetFinancialDelegationCount() : " + e.toString());
            return 0;
        }
    }

    @Override
    public boolean insertOrUpdate(String[] financialDeligationId, String[] newOldEdit, String[] budgetType, String[] tenderType, String[] procurementMethod, String[] procurementType, String[] procurementNature, String[] tenderEmergency, String[] minValueBDT, String[] maxValueBDT,String[] minProjectValueBDT, String[] maxProjectValueBDT, String[] isBoD,String[] isCorporation ,String[] approvingAuthority,String[] peOfficeLevel) {

        String action = "";
        boolean isSuccess = false;
        try{
            StringBuilder insert = new StringBuilder();
            StringBuilder update = new StringBuilder();
            for(int i = 0; i < financialDeligationId.length; i++)
            {
                if(newOldEdit[i].equalsIgnoreCase("New"))
                {
                    insert.append(" INSERT INTO tbl_FinancialDelegation (BudgetType,TenderType,ProcurementMethod ,ProcurementNature,TenderEmergency,MinValueBDT,MaxValueBDT,ApprovingAuthority,MinProjectValueBDT,MaxProjectValueBDT,IsBoD,PEOfficeLevel,isCorporation) VALUES('");
                    insert.append(budgetType[i]).append("', '");
                    insert.append(tenderType[i]).append("', '");
                    insert.append(procurementMethod[i]).append("', '");
                    insert.append(procurementNature[i]).append("', '");
                    insert.append(tenderEmergency[i]).append("', ");
                    insert.append(minValueBDT[i]).append(", ");
                    insert.append(maxValueBDT[i]).append(", '");
                    insert.append(approvingAuthority[i]).append("', ");
                    insert.append(minProjectValueBDT[i]).append(", ");
                    insert.append(maxProjectValueBDT[i]).append(", '");
                    insert.append(isBoD[i]).append("', '");
                    insert.append(peOfficeLevel[i]).append("', '");
                    insert.append(isCorporation[i]).append("'); ");
                }
                else if(newOldEdit[i].equalsIgnoreCase("Edit"))
                {
                    update.append(" UPDATE tbl_FinancialDelegation SET BudgetType = '").append(budgetType[i]);
                    update.append("', TenderType = '").append(tenderType[i]);
                    update.append("', ProcurementMethod = '").append(procurementMethod[i]);
                    update.append("', ProcurementNature = '").append(procurementNature[i]);
                    update.append("', TenderEmergency = '").append(tenderEmergency[i]);
                    update.append("', MinValueBDT = ").append(minValueBDT[i]);
                    update.append(", MaxValueBDT = ").append(maxValueBDT[i]);
                    update.append(", MinProjectValueBDT = ").append(minProjectValueBDT[i]);
                    update.append(", MaxProjectValueBDT = ").append(maxProjectValueBDT[i]);
                    update.append(", isBoD = '").append(isBoD[i]);
                    update.append("', isCorporation = '").append(isCorporation[i]);
                    update.append("', PEOfficeLevel = '").append(peOfficeLevel[i]);
                    update.append("', ApprovingAuthority = '").append(approvingAuthority[i]);
                    update.append("' WHERE FinancialDeligationId = ").append(financialDeligationId[i]).append("; ");
                }
            }
            String flag = sPFinancialDelegation.executeProcedure("InsertOrUpdate", insert.toString(), update.toString(), "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1();
            if(flag.equalsIgnoreCase("true")){
                action = "Insert/Update Financial Delegation Configuration";
                isSuccess = true;
            } else {
                action = "Error in Insert/Update Financial Delegation Configuration";
                isSuccess = false;
            }
        }catch(Exception e){
            LOGGER.error("\ninsertOrUpdate() : " + e.toString());
            action = "Error in Insert/Update Financial Delegation Configuration. "  + e.toString();
            isSuccess = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }

        return isSuccess;
    }

    @Override
    public int getDelegationRankCnt() {
        try{
            return Integer.parseInt(sPFinancialDelegation.executeProcedure("CountRanks", "", "", "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1());
        }catch(Exception e){
            LOGGER.error("\ngetDelegationRankCnt() : " + e.toString());
            return 0;
        }
    }

    @Override
    public boolean rankInsertOrUpdate(String[] designation, String[] ranksId, String[] newOldEdit) {

        String action = "";
        boolean isSuccess = false;
        try{
            StringBuilder insert = new StringBuilder();
            StringBuilder update = new StringBuilder();
            for(int i = 0; i < ranksId.length; i++)
            {
                if(newOldEdit[i].equalsIgnoreCase("New"))
                {
                    insert.append(" INSERT INTO tbl_DelegationDesignationRank VALUES('");
                    insert.append(designation[i]).append("'); ");
                }
                else if(newOldEdit[i].equalsIgnoreCase("Edit"))
                {
                    update.append(" UPDATE tbl_DelegationDesignationRank SET ApprovingAuthority = '").append(designation[i]);
                    update.append("' WHERE RankId = ").append(ranksId[i]).append("; ");
                }
            }
            String flag = sPFinancialDelegation.executeProcedure("RanksAddUpdate", insert.toString(), update.toString(), "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1();
            if(flag.equalsIgnoreCase("true")){
                action = "Insert/Update Financial Delegation Designation Rank Configuration";
                isSuccess = true;
            } else {
                action = "Error in Insert/Update Financial Delegation Designation Rank Configuration";
                isSuccess = false;
            }
        }catch(Exception e){
            LOGGER.error("\nrankInsertOrUpdate() : " + e.toString());
            action = "Error in Insert/Update Financial Delegation Designation Rank Configuration. "  + e.toString();
            isSuccess = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }

        return isSuccess;
    }

}
