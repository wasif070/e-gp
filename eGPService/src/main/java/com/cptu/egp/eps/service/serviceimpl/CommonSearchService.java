/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Kinjal Shah
 */
public class CommonSearchService {

    private static final Logger LOGGER = Logger.getLogger(CommonSearchService.class);
    private SPCommonSearch sPCommonSearch;
    private String logUserId="0";
    
    public SPCommonSearch getsPCommonSearch() {
        return sPCommonSearch;
    }

    public void setsPCommonSearch(SPCommonSearch sPCommonSearch) {
        this.sPCommonSearch = sPCommonSearch;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * fetching information from p_get_commonsearchdata
     * @param funName1
     * @param funName2
     * @param funName3
     * @param funName4
     * @param funName5
     * @param funName6
     * @param funName7
     * @param funName8
     * @param funName9
     * @param funName10
     * @return list of data, if execute successfully return list, otherwise return empty list(null)
     */
    public List<SPCommonSearchData> searchData(String funName1, String funName2, String funName3, String funName4,String funName5, String funName6,String funName7,String funName8,String funName9,String funName10)
    {
        LOGGER.debug("searchData : "+logUserId+" Starts");
        List<SPCommonSearchData> list = null;
        try {
                list = sPCommonSearch.executeProcedure(funName1, funName2, funName3, funName4, funName5, funName6, funName7, funName8, funName9, funName10, null);
        } catch (Exception e) {
            LOGGER.error("searchData : "+logUserId+" "+e);
    }
        LOGGER.debug("searchData : "+logUserId+" Ends");
        return list;
    }
    
    public List<SPCommonSearchData> searchLotbyDeclaration(String funName1, String funName2, String funName3, String funName4,String funName5, String funName6,String funName7,String funName8,String funName9,String funName10)
    {
        LOGGER.debug("searchLotbyDeclaration : "+logUserId+" Starts");
        List<SPCommonSearchData> list = null;
        try {
                list = sPCommonSearch.executeProcedure(funName1, funName2, funName3, funName4, funName5, funName6, funName7, funName8, funName9, funName10, null);
        } catch (Exception e) {
            LOGGER.error("searchLotbyDeclaration : "+logUserId+" "+e);
    }
        LOGGER.debug("searchLotbyDeclaration : "+logUserId+" Ends");
        return list;
    }
    
    public boolean checkLotidPaid(String funName1, String funName2, String funName3, String funName4,String funName5, String funName6,String funName7,String funName8,String funName9,String funName10)
    {
        LOGGER.debug("checkLotidPaid : "+logUserId+" Starts");
        boolean check = false;
        List<SPCommonSearchData> list = null;
        try {
            list = sPCommonSearch.executeProcedure(funName1, funName2, funName3, funName4, funName5, funName6, funName7, funName8, funName9, funName10, null);
            if(list.size()>0){
                if(list.get(0).getFieldName1().equalsIgnoreCase("true")){
                   check= true; 
                }
            }
        } catch (Exception e) {
            LOGGER.error("checkLotidPaid : "+logUserId+" "+e);
    }
        LOGGER.debug("checkLotidPaid : "+logUserId+" Ends");
        return check;
    }
      
   public String addBidDeclaration(String funName2, String funName3, String funName4, Date dt) {
      LOGGER.debug("addBidDeclaration : "+logUserId+" Starts");
        String msg = "";
        List<SPCommonSearchData> list = null;
        try {
            list = sPCommonSearch.executeProcedure("InsertIntoBIdDeclaration", funName2, funName3, funName4, "Remarks","" ,"" , "", "" , "" , dt);
            if(list.size()>0){
                if(list.get(0).getFieldName1().equalsIgnoreCase("Success")){
                   msg= "Success";
                }
                if(list.get(0).getFieldName1().equalsIgnoreCase("Fail")){
                   msg= "Fail";
                }
            }
        } catch (Exception e) {
            LOGGER.error("addBidDeclaration : "+logUserId+" "+e);
    }
        LOGGER.debug("addBidDeclaration : "+logUserId+" Ends");
        return msg;
    }


}
