/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigEvalMethodDao;
import com.cptu.egp.eps.model.table.TblConfigEvalMethod;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class EvalMethodConfigService {

    private HibernateQueryDao hibernateQueryDao;
    private TblConfigEvalMethodDao tblConfigEvalMethodDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private static final Logger LOGGER = Logger.getLogger(EvalMethodConfigService.class);
    MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblConfigEvalMethodDao getTblConfigEvalMethodDao() {
        return tblConfigEvalMethodDao;
    }

    public void setTblConfigEvalMethodDao(TblConfigEvalMethodDao tblConfigEvalMethodDao) {
        this.tblConfigEvalMethodDao = tblConfigEvalMethodDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

     /**
     *List of ProcurementNature
     * @return List of {procurementNatureId,procurementNature}
     */
    public List<Object[]> getProcurementNature() {
        return hibernateQueryDao.createNewQuery("select tpn.procurementNatureId,tpn.procurementNature from TblProcurementNature tpn");
    }

    /**
     * List of Tender Types
     * @return List of {tenderTypeId,tenderType}
     */
    public List<Object[]> getTenderTypes() {
        return hibernateQueryDao.createNewQuery("select ttt.tenderTypeId,ttt.tenderType from TblTenderTypes ttt");
    }

    /**
     *List of ProcurementMethod
     * @return List of {procurementMethodId,procurementMethod}
     */
    public List<Object[]> getProcurementMethod() {
        return hibernateQueryDao.createNewQuery("select tpm.procurementMethodId,tpm.procurementMethod from TblProcurementMethod tpm");//,tpm.procurementFullName
    }

    /**
     *Inserts into TblConfigEvalMethod
     * @param list to be inserted
     * @return true or false for success or fail
     */
    public boolean insertConfigEvalMethod(List<TblConfigEvalMethod> list) {
        LOGGER.debug("insertConfigEvalMethod : "+logUserId+" Starts");
        boolean flag;
        String action = null;
        try {
            tblConfigEvalMethodDao.updateInsertAllConfigEvalMethod(list);
            flag =  true;
            action = "Add Evaluation Method Rules";
        } catch (Exception e) {
            LOGGER.error("insertConfigEvalMethod : "+logUserId+" :"+e);
            flag =  false;
            action = "Error in Add Evaluation Method Rules : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("insertConfigEvalMethod : "+logUserId+" Ends");
        return flag;
    }

    /**
     *List of all TblConfigEvalMethod
     * @return true or false for success or fail
     */
    public List<TblConfigEvalMethod> getAllTblConfigEvalMethod() {
        return tblConfigEvalMethodDao.getAllTblConfigEvalMethod();
    }

    /**
     *List of all TblConfigEvalMethod by search
     * @param values search criteria
     * @return List of TblConfigEvalMethod
     * @throws Exception
     */
    public List<TblConfigEvalMethod> getTblConfigEvalMethod(Object... values) throws Exception {
        return tblConfigEvalMethodDao.findTblConfigEvalMethod(values);
    }

    /**
     *Updates TblConfigEvalMethod
     * @param list to be updated
     * @return true or false for success or fail
     */
    public boolean updateConfigEvalMethod(List<TblConfigEvalMethod> list) {
        LOGGER.debug("updateConfigEvalMethod : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
        try {
            int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigEvalMethod");
            if (i > 0) {
                tblConfigEvalMethodDao.updateInsertAllConfigEvalMethod(list);
                flag = true;
            }
            action = "Edit Evaluation Method Rules";
        } catch (Exception e) {
            LOGGER.error("updateConfigEvalMethod : "+logUserId+" Ends"+e);
            action = "Error in Edit Evaluation Method Rules : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateConfigEvalMethod : "+logUserId+" Ends");
        return flag;
        
    }

    /**
     *Update TblConfigEvalMethod
     * @param tblConfigEvalMethod to be updated
     * @return true or false for success or fail
     */
    public boolean updateSingleConfigEvalMethod(TblConfigEvalMethod tblConfigEvalMethod) {
        LOGGER.debug("updateSingleConfigEvalMethod : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
        try {
            tblConfigEvalMethodDao.updateTblConfigEvalMethod(tblConfigEvalMethod);
            flag = true;
            action = "Edit Evaluation Method Rules";
        } catch (Exception e) {
            LOGGER.error("updateSingleConfigEvalMethod : "+logUserId+" Ends"+ e);
            action = "Error in Edit Evaluation Method Rules : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateSingleConfigEvalMethod : "+logUserId+" Ends");
        return flag;
    }

    /**
     *Deletes TblConfigEvalMethod on evalMethodConfId
     * @param evalMethodConfId to be deleted on evalMethodConfId
     * @return number of rows deleted
     */
    public int deleteSingleConfigEvalMethod(String evalMethodConfId){
        LOGGER.debug("deleteSingleConfigEvalMethod : "+logUserId+" Starts");
        int cnt = 0;
        String action = null;
        try {
            cnt = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigEvalMethod where evalMethodConfId="+evalMethodConfId);
            action = "Remove Evaluation Method Rules";
        } catch (Exception e) {
            LOGGER.debug("deleteSingleConfigEvalMethod : "+logUserId,e);
            action = "Error in Remove Evaluation Method Rules : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("deleteSingleConfigEvalMethod : "+logUserId+" Ends");
        return cnt;
    }

    /**
     * Unique count of TblConfigEvalMethod
     * @param evalId
     * @param pNature
     * @param tendType
     * @param pMethod
     * @return count
     */
    public long uniqueConfigCount(String evalId,String pNature,String tendType,String pMethod) throws Exception{
        return hibernateQueryDao.countForNewQuery("TblConfigEvalMethod tcm", "tcm.procurementMethodId="+pMethod+" and tenderTypeId="+tendType+" and procurementNatureId="+pNature+" and evalMethodConfId!="+evalId);
    }

    /**
     *Count of all TblConfigEvalMethod
     * @return count
     * @throws Exception
     */
    public long getAllEvalConfigCount() throws Exception{
        return hibernateQueryDao.countForNewQuery("TblConfigEvalMethod tcem","1=1");
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
}
