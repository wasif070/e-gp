/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface DesignationMasterService {

    public List<TblDepartmentMaster> getMinistry();

    public List<TblGradeMaster> getGrades();

    public int createDesignation(TblDesignationMaster tblDesignationMaster);

    public TblDesignationMaster getDesignationDetails(int designationId);

    public boolean updateDesignation(TblDesignationMaster tblDesignationMaster);

    public boolean verifyDesignation(String designationName,short departmentId);

    public List<TblDesignationMaster> getAllDesignations(int firstResult,int maxResult,int userId);

    public List<Object[]> getAllDesignationsForPE(int firstResult,int maxResult,int userId,String orderClause,String order,int userTypeId);
    //public List<TblDesignationMaster> getAllDesignationsForPE(int firstResult,int maxResult,int userId,String orderClause,String order); //malhar'code

    public long getDesigCnt(int userId);
    public List<TblDepartmentMaster> getMinistryforAdmin(int userId);
    
    public List<TblOfficeMaster> getOfficeforAdmin(int userId);

    public void setUserId(String logUserId);
    
    public void setAuditTrail(AuditTrail auditTrail);
}
