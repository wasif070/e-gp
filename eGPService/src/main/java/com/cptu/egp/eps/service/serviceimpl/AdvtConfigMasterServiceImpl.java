/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAdvtConfigurationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblAdvertisement;
import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.AdvtConfigMasterService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author spandana.vaddi
 */
public class AdvtConfigMasterServiceImpl implements AdvtConfigMasterService {

    private static final Logger LOGGER = Logger.getLogger(CommonXMLSPServiceImpl.class);
    private HibernateQueryDao hibernateQueryDao;
    private TblAdvtConfigurationMasterDao configMasterDao;
    private TblUserTypeMasterDao tblUserTypeMasterDao;
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";

    public TblUserTypeMasterDao getTblUserTypeMasterDao() {
        return tblUserTypeMasterDao;
    }

    public void setTblUserTypeMasterDao(TblUserTypeMasterDao tblUserTypeMasterDao) {
        this.tblUserTypeMasterDao = tblUserTypeMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblAdvtConfigurationMasterDao getConfigMasterDao() {
        return configMasterDao;
    }

    public void setConfigMasterDao(TblAdvtConfigurationMasterDao configMasterDao) {
        this.configMasterDao = configMasterDao;
    }

    @Override
    public List<TblAdvtConfigurationMaster> getConfigMaster() {
        LOGGER.debug("getConfigMaster : " + logUserId + STARTS);
        List<TblAdvtConfigurationMaster> list = null;
        try {
            list = configMasterDao.getAllTblAdvtConfigurationMaster();
        } catch (Exception e) {
            LOGGER.error("getConfigMaster : " + logUserId + " " + e);
        }
        LOGGER.debug("getConfigMaster : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblAdvtConfigurationMaster> getAllConfigMaster() {
        LOGGER.debug("getAllConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("getAllConfigMaster : " + logUserId + ENDS);
        return getConfigMasterDao().getAllTblAdvtConfigurationMaster();
    }

    @Override
    public String updateConfigMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster) {
        LOGGER.debug("updateConfigMaster : " + logUserId + STARTS);
        String msg = null;
        String action = null;
        try {
            configMasterDao.updateTblAdvtConfigurationMaster(tblAdvtConfigurationMaster);
            msg = "Updated";
            action = "Edit Document Size, Types";
        } catch (Exception e) {
            LOGGER.debug("updateConfigMaster : " + logUserId + " " + e);
            action = "Error in Edit Document Size, Types : " + e;
        }
        LOGGER.debug("updateConfigMaster : " + logUserId + ENDS);
        return msg;
    }

    @Override
    public String insertConfigMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster) {
        LOGGER.debug("insertConfigMaster : " + logUserId + STARTS);
        String msg = null;
        String duration = tblAdvtConfigurationMaster.getDimensions();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MMMM-dd, hh:mm:ss");
        try {
            configMasterDao.addTblAdvtConfigurationMaster(tblAdvtConfigurationMaster);
            msg = "ok";
        } catch (Exception ex) {
            LOGGER.error("insertConfigMaster : " + logUserId + " " + ex);
            msg = "Error";
        }
        LOGGER.debug("insertConfigMaster : " + logUserId + ENDS);
        return msg;
    }

    @Override
    public List<TblAdvtConfigurationMaster> getConfigMasterDetails(int configId) {
        LOGGER.debug("getConfigMasterDetails : " + logUserId + STARTS);
        List<TblAdvtConfigurationMaster> list = null;
        try {
            list = configMasterDao.findTblAdvtConfigurationMaster("configId", Operation_enum.EQ, (short) configId);
        } catch (Exception ex) {
            LOGGER.debug("getConfigMasterDetails : " + logUserId + " " + ex);
        }
        LOGGER.error("getConfigMasterDetails : " + logUserId + ENDS);
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<TblAdvtConfigurationMaster> getAllTblAdvtConfigurationMaster() {
        LOGGER.debug("getAllTblAdvtConfigurationMaster : " + logUserId + STARTS);
        List<TblAdvtConfigurationMaster> list1 = null;
        try {
            list1 = configMasterDao.getAllTblAdvtConfigurationMaster();
        } catch (Exception ex) {
            LOGGER.debug("getAllTblAdvtConfigurationMaster : " + logUserId + " " + ex);
        }
        LOGGER.error("getAllTblAdvtConfigurationMaster : " + logUserId + ENDS);
        return list1;
    }

    @Override
    public List<TblAdvtConfigurationMaster> findTblAdvtConfigurationMaster(Object... values) {
        LOGGER.debug("findTblAdvtConfigurationMaster : " + logUserId + "loggerStart");
        List<TblAdvtConfigurationMaster> tblAdvtConfigurationMaster;
        try {
            tblAdvtConfigurationMaster = configMasterDao.findTblAdvtConfigurationMaster(values);
        } catch (Exception e) {
            tblAdvtConfigurationMaster = null;
            LOGGER.error("findTblAdvtConfigurationMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblAdvtConfigurationMaster : " + logUserId + "loggerEnd");
        return tblAdvtConfigurationMaster;
    }

    @Override
    public boolean updateTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster) {
        LOGGER.debug("updateTblAdvtConfigurationMaster : " + logUserId + " Starts");
        boolean flag = false;
        String funct = null;
        try {
            configMasterDao.updateTblAdvtConfigurationMaster(tblAdvtConfigurationMaster);
            flag = true;
            funct = "Edit Evaluation Method Rules";
        } catch (Exception e) {
            LOGGER.error("updateTblAdvtConfigurationMaster : " + logUserId + " Ends" + e);
            funct = "Error in Edit Evaluation Method Rules : " + e;
        }
        LOGGER.debug("updateSingleConfigEvalMethod : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean deleteTblAdvtConfigurationMaster(TblAdvtConfigurationMaster adConfigId) {
        LOGGER.debug("deleteCmsAitConfig : " + logUserId + STARTS);
        boolean flag;
        try {
            configMasterDao.deleteTblAdvtConfigurationMaster(adConfigId);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("deleteTblAdvtConfigurationMaster : " + logUserId + " : " + ex);
            flag = false;
        }
        LOGGER.debug("deleteTblAdvtConfigurationMaster : " + logUserId + ENDS);
        return flag;
    }
}
