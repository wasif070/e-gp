/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface EvalTscCommentsService {

    /**
     * For inserting data on TblEvalTscComments
     * @param tblEvalTsccomments
     * @return boolean true and false
     */
    public boolean insertData(TblEvalTsccomments tblEvalTsccomments);
    /**
     *Get comments as per tenderId and formId
     * @param tenderId
     * @param formId
     * @return list [0] commentsId,[1 ]comments [2] employeeName, [3] tscUserId
     */
    public List<Object[]> getComments(int tenderId,int formId);
    /**
     *For deleting Record
     * @param commentId
     * @return boolean true or false
     */
    public boolean delRec(int commentId);
    /**
     *For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
     /**
     *get Comments as per commentId
     * @param commentId
     * @return list object[0] Id, object[1] Comments,  
     */
    public TblEvalTsccomments getComments(int commentId);


    /**
     *Notify Button Display Check
     * @param commentId to be checked on
     * @return true or false for show or hide
     */
    public boolean displayNotifyButton(int commentId);
    
     /**
     *update Comment as per commentId
     * @param commentId
     * @return list object[0] Id, object[1] Comments,  
     */
    public boolean updateComments(int commentId,String comments);
}
