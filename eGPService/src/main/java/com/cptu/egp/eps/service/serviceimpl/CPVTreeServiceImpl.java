/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCpvClassificationDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCpvClassification;
import com.cptu.egp.eps.service.serviceinterface.CPVTreeService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CPVTreeServiceImpl implements CPVTreeService {

    final Logger logger = Logger.getLogger(CPVTreeServiceImpl.class);
    TblCpvClassificationDao tblCpvClassificationDao;
    HibernateQueryDao hibernateQueryDao;
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblCpvClassificationDao getTblCpvClassificationDao() {
        return tblCpvClassificationDao;
    }

    public void setTblCpvClassificationDao(TblCpvClassificationDao tblCpvClassificationDao) {
        this.tblCpvClassificationDao = tblCpvClassificationDao;
    }

    @Override
    public List<TblCpvClassification> getAllData() {
        logger.debug("getAllData : " + logUserId + loggerStart);
        List<TblCpvClassification> tblCpvClassifications;
        try {
            tblCpvClassifications = tblCpvClassificationDao.findCpvClassification("cpvCategory", Operation_enum.EQ, 0);
        } catch (Exception ex) {
            tblCpvClassifications = null;
            logger.error("getAllData : " + logUserId + ex);
        }
        logger.debug("getAllData : " + logUserId + loggerEnd);
        return tblCpvClassifications;
    }

    @Override
    public List<TblCpvClassification> getChildData(int cpvId) {
        logger.debug("getChildData : " + logUserId + loggerStart);
        List<TblCpvClassification> tblCpvClassifications = null;
        try {
            tblCpvClassifications = tblCpvClassificationDao.findCpvClassification("cpvCategory", Operation_enum.EQ, cpvId);
        } catch (Exception ex) {
            logger.error("getChildData : " + logUserId + ex);
        }
        logger.debug("getChildData : " + logUserId + loggerEnd);
        return tblCpvClassifications;
    }

    @Override
    public String findChildDatabyCpvDesc(String cpvDescription) {
        logger.debug("findChildDatabyCpvDesc : " + logUserId + loggerStart);

        List<TblCpvClassification> listCpv = new ArrayList<TblCpvClassification>();
        List<TblCpvClassification> listChild = new ArrayList<TblCpvClassification>();
        List<TblCpvClassification> listChild2 = new ArrayList<TblCpvClassification>();

        String CpvDesc = "";

        try {
            listCpv = tblCpvClassificationDao.findCpvClassification("cpvDescription", Operation_enum.EQ, cpvDescription);
           listChild = tblCpvClassificationDao.findCpvClassification("cpvCategory", Operation_enum.EQ, listCpv.get(0).getCpvId());

            for (TblCpvClassification tblCpvClassification : listChild) {
               CpvDesc = CpvDesc + tblCpvClassification.getCpvDescription() + ";";

               listChild2 = tblCpvClassificationDao.findCpvClassification("cpvCategory", Operation_enum.EQ, tblCpvClassification.getCpvId());
                for (TblCpvClassification tblCpvClassification2 : listChild2) {
                   CpvDesc = CpvDesc + tblCpvClassification2.getCpvDescription() + ";";
               }
           }

           listChild = null;
           listChild2 = null;
           listCpv = null;


        } catch (Exception ex) {
            logger.error("findChildDatabyCpvDesc : " + logUserId + ex);
        }
        logger.debug("findChildDatabyCpvDesc : " + logUserId + loggerEnd);
        return CpvDesc;
    }
}
