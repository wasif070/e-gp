/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface DepartmentCreationService {

    /**
     * set list
     * @param auditTrail list to be set
     */
    public void setAuditTrail(AuditTrail auditTrail);

    /**
     * insert Department into table
     * @param tblDepartmentMaster
     * @return Object
     */
    public boolean addDepartmentCreation(TblDepartmentMaster tblDepartmentMaster);

    /**
     * Fetching Info from deptType
     * @param deptType - Tbl_DepartmentMaster
     * @return List of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getDeptTypeName(String deptType);

    /**
     * Get all records from TblDepartmentMaster
     * @return list of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getAllDepartmentMaster();

    /**
     * fetching information for Department
     * @param firstResult
     * @param maxResult
     * @param values (where condition)
     * @return Object with Data
     * @throws Exception
     */
    public List<TblDepartmentMaster> findDeptCreatMaster(int firstResult, int maxResult, Object... values) throws Exception;

    /**
     * fetching information for Department
     * @param values (where condition)
     * @return Object with Data
     * @throws Exception
     */
    public List<TblDepartmentMaster> findDeptUserMaster(Object... values) throws Exception;

    /**
     * get count from TblDepartmentMaster for particular deptType
     * @param deptType
     * @return number of records for particular deptType
     * @throws Exception
     */
    public long getAllCountOfDept(String deptType) throws Exception;

    /**
     * get count from TblDepartmentMaster including search
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return numbers of records
     * @throws Exception
     */
    public long getSearchCountOfDept(String deptType,String searchField,String searchString,String searchOper) throws Exception;

    /**
     * updating info in to TblDepartmentMaster
     * @param tblDepartmentMaster
     * @return true if updating successfully else false
     */
    public boolean updateDepartmentCreation(TblDepartmentMaster tblDepartmentMaster);

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setUserId(String logUserId);

    public List<TblDepartmentMaster> getDepartmentMasterByName(String DepartmentName,String deptType);

    public List<TblDepartmentMaster> getMinstriesByDeptyTypeAndDeptName(String deptType,String deptName);
}
