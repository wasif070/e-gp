/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmailLogDao;
import com.cptu.egp.eps.model.table.TblEmailLog;
import com.cptu.egp.eps.service.serviceinterface.EmailLogService;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class EmailLogServiceImpl implements EmailLogService {

    String logUserId = "0";

    final Logger logger = Logger.getLogger(EmailLogServiceImpl.class);

    TblEmailLogDao tblEmailLogDao;

    public TblEmailLogDao getTblEmailLogDao() {
        return tblEmailLogDao;
    }

    public void setTblEmailLogDao(TblEmailLogDao tblEmailLogDao) {
        this.tblEmailLogDao = tblEmailLogDao;
    }

    @Override
    public void addEmailLog(TblEmailLog tblEmailLog) {
        logger.debug("addEmailLog : "+logUserId+" Starts");
        try{
            tblEmailLogDao.addTblEmailLog(tblEmailLog);
        }catch(Exception ex){
            logger.debug("addEmailLog : "+logUserId+" Starts"+ex);
        }
        logger.debug("addEmailLog : "+logUserId+" Ends");
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

}
