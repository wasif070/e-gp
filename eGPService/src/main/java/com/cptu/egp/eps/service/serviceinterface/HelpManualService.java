/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblHelpManualDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblHelpManual;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class HelpManualService {

    private final Logger logger = Logger.getLogger(HelpManualService.class);

    TblHelpManualDao tblHelpManualDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    public TblHelpManualDao getTblHelpManualDao() {
        return tblHelpManualDao;
    }

    public void setTblHelpManualDao(TblHelpManualDao tblHelpManualDao) {
        this.tblHelpManualDao = tblHelpManualDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    /**
     * Add Help Manual Data
     * @param tblHelpManual
     * @return "value added" if Help Manual is added or Data not added.
     */
    public String addHelpManual(TblHelpManual tblHelpManual){
        logger.debug("addHelpManual : "+logUserId+" Starts");
        String str =null;
        String action = "Create Help Content";
        try{
            tblHelpManualDao.addTblHelpManual(tblHelpManual);
            str = "Values Added";
        }catch(Exception e){
         logger.error("addHelpManual : "+logUserId+" : "+e);
            str = "Data Not Added";
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("addHelpManual : "+logUserId+" Ends");
        return str;
    }

    /**
     * Fetch All help Manuals Data
     * @param offset
     * @param row
     * @param orderby
     * @param order
     * @return List of all Help Manuals
     */
    public List<TblHelpManual> getAllHelpManual(int offset,int row, String orderby,String order) {
        logger.debug("getAllHelpManual : "+logUserId+" Starts");
        List<TblHelpManual> list = null;
        try{
            List<TblHelpManual> helpManuals = new ArrayList<TblHelpManual>();            
            if("asc".equalsIgnoreCase(orderby)){
                helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row,"isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.ASC);
            }else{
                helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row,"isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.DESC);
            }
            if(helpManuals.isEmpty()){
                list = null;
            }else{
                list = helpManuals;
            }
        }catch(Exception e){
            logger.error("getAllHelpManual : "+logUserId+" : "+e);
            list = null;
        }
        logger.debug("getAllHelpManual : "+logUserId+" Ends");
        return list;
    }

    /**
     * Search Help Manual by help URL
     * @param offset
     * @param row
     * @param searchField
     * @param searchString
     * @param searchOper
     * @param orderby
     * @param order
     * @return List of Searched Help Manual.
     */
    public List<TblHelpManual> searchHelpManual(int offset,int row,String searchField,String searchString,String searchOper,String orderby,String order) {
        logger.debug("searchHelpManual : "+logUserId+" Starts");
        List<TblHelpManual> list = null;
        try{
            List<TblHelpManual> helpManuals = null;
            
            if("eq".equalsIgnoreCase(searchOper)){
                 if("asc".equalsIgnoreCase(orderby))
                    helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row, "helpUrl",Operation_enum.EQ, searchString,"isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.ASC);
                 else
                    helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row, "helpUrl",Operation_enum.EQ, searchString,"isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.DESC);
            }else if("cn".equalsIgnoreCase(searchOper)){
                if("asc".equalsIgnoreCase(orderby))
                    helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row, "helpUrl",Operation_enum.LIKE, "%"+searchString+"%","isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.ASC);
                else
                    helpManuals = tblHelpManualDao.findByCountTblHelpManual(offset, row, "helpUrl",Operation_enum.LIKE, "%"+searchString+"%","isDeleted",Operation_enum.EQ,"no",order,Operation_enum.ORDERBY,Operation_enum.DESC);
            }
            if(helpManuals.isEmpty())
                list=  null;
            else
                list = helpManuals;
        }catch(Exception e){
            logger.error("searchHelpManual : "+logUserId+" : "+e);
            list = null;
        }
        logger.debug("searchHelpManual : "+logUserId+" Ends");
        return list;
    }

    /**
     * fetch Help Manual Search Count
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Search Count
     */
    public long getSearchCount(String searchField,String searchString,String searchOper){
           logger.debug("getSearchCount : "+logUserId+" Starts");
           long lng = 0;
        try{
            List<TblHelpManual> helpManuals = null;
            if("eq".equalsIgnoreCase(searchOper)){
                helpManuals = tblHelpManualDao.findTblHelpManual("helpUrl",Operation_enum.EQ, searchString,"isDeleted",Operation_enum.EQ, "no","helpManualId",Operation_enum.ORDERBY,Operation_enum.ASC);
            }else if("cn".equalsIgnoreCase(searchOper)){
                helpManuals = tblHelpManualDao.findTblHelpManual("helpUrl",Operation_enum.LIKE, "%"+searchString+"%","isDeleted",Operation_enum.EQ, "no","helpManualId",Operation_enum.ORDERBY,Operation_enum.ASC);
            }
            if(helpManuals.isEmpty()){
                lng = 0;}
            else{
                lng = helpManuals.size();}
            
        }catch(Exception e){
            logger.error("getSearchCount : "+logUserId+" : "+e);
            lng = 0;
        }
        logger.debug("getSearchCount : "+logUserId+" Ends");
        return lng;
    }
    
    /**
     * Get Help Manual By Page Name
     * @param pageName
     * @return Get Help Manual
     */
    public TblHelpManual getHelpManual(String pageName) {
        logger.debug("getHelpManual : "+logUserId+" Starts");
        List<TblHelpManual> helpManuals = null;
        try{
            helpManuals = tblHelpManualDao.findTblHelpManual("helpUrl",Operation_enum.EQ, pageName,"isDeleted",Operation_enum.EQ,"no");
            if(helpManuals.isEmpty()){
                helpManuals = null;}
        } catch(Exception ex){
             logger.error("getHelpManual : "+logUserId+" : "+ex);
             helpManuals = null;
        }
        logger.debug("getHelpManual : "+logUserId+" Ends");
        return helpManuals.get(0);
    }

    /**
     * Fetch the Help Manual Data from Id
     * @param id
     * @return Get Help Manual Data from id
     */
    public List<TblHelpManual> getData(int id){
        logger.debug("getData : "+logUserId+" Starts");
        List<TblHelpManual> helplist =null;
        try{
            helplist = tblHelpManualDao.findTblHelpManual("helpManualId",Operation_enum.EQ,id);
            if(helplist.isEmpty()){
                helplist = null;}
        }catch(Exception e){
            logger.error("getData : "+logUserId+" : "+e);
        }
        logger.debug("getData : "+logUserId+" Ends");
        return helplist;
    }

    /**
     * Update Help Manual Details
     * @param tblHelpManual
     * @return update Help Manual Details.
     */
    public String updateHelpManualDetails(TblHelpManual tblHelpManual){
        logger.debug("updateHelpManualDetails : "+logUserId+" Starts");
        String str ="";
        String action = "Edit Help Content";
        try{
           tblHelpManualDao.updateTblHelpManual(tblHelpManual);
           str = "Value Updated";
        }catch(Exception e){
           logger.error("updateHelpManualDetails : "+logUserId+" : "+e);
           str = "Not Updated";
           action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("updateHelpManualDetails : "+logUserId+" Ends");
        return str;
    }

    /**
     * Delete Help Manual 
     * @param helpManual 
     * @return String as per the action
     */
    public String deleteHelpManual(TblHelpManual helpManual){
        logger.debug("deleteHelpManual : "+logUserId+" Starts");
        String str = "";
        try{
            tblHelpManualDao.deleteTblHelpManual(helpManual);
            str = "HelpManual Deleted";
        }catch(Exception e){
            logger.error("deleteHelpManual : "+logUserId+" : "+e);
            str = null;
        }
         logger.debug("deleteHelpManual : "+logUserId+" Ends");
         return str;
    }

    /**
     * Fetch ALL Help Manual Count
     * @return no of help manual.
     */
    public long getTblHelpManualCount(){
         logger.debug("getTblHelpManualCount : "+logUserId+" Starts");
         long lng =0;
         String action = "Delete Help Content";
        try{
             List<TblHelpManual> helpManuals =  tblHelpManualDao.findTblHelpManual("isDeleted",Operation_enum.EQ, "no", "helpManualId",Operation_enum.ORDERBY,Operation_enum.ASC);
             lng = helpManuals.size();
        }catch(Exception e){
            logger.error("getTblHelpManualCount : "+logUserId+" : "+e);
            lng = 0;
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("getTblHelpManualCount : "+logUserId+" Ends");
        return lng;
    }

    /**
     * Change Status of the Help Manual by id
     * @param id
     * @return change status of the Help Manual.
     */
    public String changeStatus(int id){
         logger.debug("changeStatus : "+logUserId+" Starts");
         String str ="";
        try{
            hibernateQueryDao.updateDeleteNewQuery("update TblHelpManual set isDeleted = 'yes' where helpManualId = "+id);
            str = "update";
        }catch(Exception ex){
           logger.error("changeStatus : "+logUserId+" : "+ex);
            str = "notupdate";
        }
         logger.debug("changeStatus : "+logUserId+" Ends");
         return str;
    }

    /**
     * Undo status of the Help Manual by id to is Deleted yes and no.
     * @param id
     * @return undo the status of Help Manual.
     */
    public String undoStatus(int id){
        logger.debug("undoStatus : "+logUserId+" Starts");
        String str ="";
        try{
            hibernateQueryDao.updateDeleteNewQuery("update TblHelpManual set isDeleted = 'no' where helpManualId = "+id);
            str = "update";
        }catch(Exception ex){
           logger.error("undoStatus : "+logUserId+" : "+ex);
            str = "notupdate";
        }
        logger.debug("undoStatus : "+logUserId+" Ends");
        return str;
    }
    
}
