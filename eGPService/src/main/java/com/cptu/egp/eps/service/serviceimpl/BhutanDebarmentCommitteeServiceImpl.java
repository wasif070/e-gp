/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBhutanDebarmentCommitteeDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee;
import com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author feroz
 */
public class BhutanDebarmentCommitteeServiceImpl implements BhutanDebarmentCommitteeService{
    
    static final Logger LOGGER = Logger.getLogger(BhutanDebarmentCommitteeServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblBhutanDebarmentCommitteeDao tblBhutanDebarmentCommitteeDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    

    @Override
    public boolean addTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        boolean doneFlag = false;
        LOGGER.debug("addTblBhutanDebarmentCommittee: " + logUserId + LOGGERSTART);
            try {
            getTblBhutanDebarmentCommitteeDao().addTblBhutanDebarmentCommittee(tblBhutanDebarmentCommittee);
            doneFlag = true;
            } catch (Exception e) {
                LOGGER.error("addTblBhutanDebarmentCommittee: " + logUserId + e);
            }
            LOGGER.debug("addTblBhutanDebarmentCommittee: " + logUserId + LOGGEREND);
            return doneFlag;
    }

    @Override
    public void deleteTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        LOGGER.debug("deleteTblBhutanDebarmentCommittee: " + logUserId + LOGGERSTART);
            try {
            getTblBhutanDebarmentCommitteeDao().deleteTblBhutanDebarmentCommittee(tblBhutanDebarmentCommittee);
            } catch (Exception e) {
                LOGGER.error("deleteTblBhutanDebarmentCommittee: " + logUserId + e);
            }
            LOGGER.debug("deleteTblBhutanDebarmentCommittee: " + logUserId + LOGGEREND);
    }

    @Override
    public String updateTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        boolean flag = false;
    LOGGER.debug("updateTblBhutanDebarmentCommittee: " + logUserId + LOGGERSTART);
            try {
            getTblBhutanDebarmentCommitteeDao().updateTblBhutanDebarmentCommittee(tblBhutanDebarmentCommittee);
            flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblBhutanDebarmentCommittee: " + logUserId + e);
            }
            LOGGER.debug("updateTblBhutanDebarmentCommittee: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc";
            }
            else
            {
                return "failed";
            }
    }

    @Override
    public List<TblBhutanDebarmentCommittee> getAllTblBhutanDebarmentCommittee() {
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Starts");
        List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = null;
        try {
            tblBhutanDebarmentCommittee = getTblBhutanDebarmentCommitteeDao().getAllTblBhutanDebarmentCommittee();
        } catch (Exception ex) {
            LOGGER.error("getAllTblBhutanDebarmentCommittee : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Ends");
        return tblBhutanDebarmentCommittee;
    }

    @Override
    public List<TblBhutanDebarmentCommittee> findTblBhutanDebarmentCommittee(int id) {
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Starts");
        List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = null;
        try {
            tblBhutanDebarmentCommittee = getTblBhutanDebarmentCommitteeDao().findTblBhutanDebarmentCommittee("id", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblBhutanDebarmentCommittee : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Ends");
        return tblBhutanDebarmentCommittee;
    }
    
    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblBhutanDebarmentCommitteeDao
     */
    public TblBhutanDebarmentCommitteeDao getTblBhutanDebarmentCommitteeDao() {
        return tblBhutanDebarmentCommitteeDao;
    }

    /**
     * @param tblBhutanDebarmentCommitteeDao the tblBhutanDebarmentCommitteeDao to set
     */
    public void setTblBhutanDebarmentCommitteeDao(TblBhutanDebarmentCommitteeDao tblBhutanDebarmentCommitteeDao) {
        this.tblBhutanDebarmentCommitteeDao = tblBhutanDebarmentCommitteeDao;
    }
    
}
