/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.CorrigendumInfoDetails;
import com.cptu.egp.eps.service.serviceinterface.CorrigendumDetailsService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Service class for getting the details of TblCorrigendumDetail.
 * @author sreenu
 */
public class CorrigendumDetailsServiceImpl implements CorrigendumDetailsService {

    private static final Logger LOGGER =
            Logger.getLogger(CorrigendumDetailsServiceImpl.class);
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private HibernateQueryDao hibernateQueryDao;

    /***
     * set the userId
     * @param String logUserId
     */
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /***
     * This method returns the below Amendement/Corrigendum details.
     * @return List<CorrigendumInfoDetails>
     */
    @Override
    public List<CorrigendumInfoDetails> getAllCorrigendumDetails() {
        LOGGER.debug("getAllCorrigendumDetails : " + logUserId + " Starts");
        List<CorrigendumInfoDetails> corrigendumDetailsList = 
                new ArrayList<CorrigendumInfoDetails>();
        List<Object[]> corrigendumDetailsObjectList = null;
        StringBuilder query = new StringBuilder();
        query.append("SELECT tcd.corriDetailId, tcd.tblCorrigendumMaster.corrigendumId, ");
        query.append("tcm.tenderId,  ");
        query.append("tcd.fieldName, tcd.oldValue, tcd.newValue, tcm.corrigendumText ");
        query.append("FROM TblCorrigendumDetail tcd, TblCorrigendumMaster tcm ");
        query.append("WHERE tcd.tblCorrigendumMaster.corrigendumId = tcm.corrigendumId ");
        query.append("AND tcm.corrigendumStatus = 'Approved' ORDER BY tcm.tenderId");
        try {
            corrigendumDetailsObjectList = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception ex) {
            LOGGER.error("getAllCorrigendumDetails : " + logUserId + " : " + ex.toString());
        }
        if (corrigendumDetailsObjectList != null && !corrigendumDetailsObjectList.isEmpty()) {
            for (int i = 0; i < corrigendumDetailsObjectList.size(); i++) {
                Object[] tempObjectArray = corrigendumDetailsObjectList.get(i);
                CorrigendumInfoDetails tempCorrigendumInfoDetails =
                        new CorrigendumInfoDetails();  
                tempCorrigendumInfoDetails.setAmendmentText((String)tempObjectArray[6]);
                tempCorrigendumInfoDetails.setCorriDetailId((Integer)tempObjectArray[0]);
                tempCorrigendumInfoDetails.setCorrigendumId((Integer)tempObjectArray[1]);
                tempCorrigendumInfoDetails.setFieldName((String)tempObjectArray[3]);
                tempCorrigendumInfoDetails.setOldValue((String)tempObjectArray[4]);
                tempCorrigendumInfoDetails.setNewValue((String)tempObjectArray[5]);
                tempCorrigendumInfoDetails.setTenderId((Integer)tempObjectArray[2]);
                corrigendumDetailsList.add(tempCorrigendumInfoDetails);
            }
        }
        LOGGER.debug("getAllCorrigendumDetails : " + logUserId + " Ends");
        return corrigendumDetailsList;
    }
}
