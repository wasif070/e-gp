/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderAuditTrail;
import com.cptu.egp.eps.model.table.TblTenderOpenDates;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TenderAuditTrailService {

    /**
     * insert into TblTenderAuditTrail
     * @param tblTenderAuditTrail
     */
    public void addTenderAuditTrail(TblTenderAuditTrail tblTenderAuditTrail);

    /**
     * Set audit trail
     * @param auditTrail list to be inserted
     */
    public void setAuditTrail(AuditTrail auditTrail);

    /**
     * Fetching information from TblTenderEnvelopes
     * @param tenderid
     * @return List of objects
     */
    public List<Object> getenvId(int tenderId);

    /**
     * insert into TblTenderOpenDates
     * @param tblTenderOpenDates
     */
    public void addTblTenderOpenDates(TblTenderOpenDates tblTenderOpenDates);

    public void setUserId(String userId);
}
