/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertDocDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsWcCertDoc;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import com.cptu.egp.eps.service.serviceinterface.CmsWcCertDocService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.OrderBy;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsWcCertDocServiceImpl implements CmsWcCertDocService {

    final Logger logger = Logger.getLogger(CmsWcCertDocServiceImpl.class);
    private TblCmsWcCertDocDao cmsWcCertDocDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "   ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsWcCertDocDao getCmsWcCertDocDao() {
        return cmsWcCertDocDao;
    }

    public void setCmsWcCertDocDao(TblCmsWcCertDocDao cmsWcCertDocDao) {
        this.cmsWcCertDocDao = cmsWcCertDocDao;
    }

    /***
     * This method inserts a TblCmsWcCertDoc object in database
     * @param tblCmsWcCertDoc
     * @return int
     */
    @Override
    public int insertCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("insertCmsWcCertDoc : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsWcCertDocDao.addTblCmsWcCertDoc(tblCmsWcCertDoc);
            flag = tblCmsWcCertDoc.getWcCertDocId();
        } catch (Exception ex) {
            logger.error("insertCmsWcCertDoc : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsWcCertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsWcCertDoc object in DB
     * @param tblCmsWcCertDoc
     * @return boolean
     */
    @Override
    public boolean updateCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("updateCmsWcCertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsWcCertDocDao.updateTblCmsWcCertDoc(tblCmsWcCertDoc);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsWcCertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsWcCertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsWcCertDoc object in DB
     * @param tblCmsWcCertDoc
     * @return boolean
     */
    @Override
    public boolean deleteCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("deleteCmsWcCertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsWcCertDocDao.deleteTblCmsWcCertDoc(tblCmsWcCertDoc);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsWcCertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsWcCertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsWcCertDoc objects from database
     * @return List<TblCmsWcCertDoc>
     */
    @Override
    public List<TblCmsWcCertDoc> getAllCmsWcCertDoc() {
        logger.debug("getAllCmsTaxConfig : " + logUserId + STARTS);
        List<TblCmsWcCertDoc> cmsWcCertDocList = new ArrayList<TblCmsWcCertDoc>();
        try {
            cmsWcCertDocList = cmsWcCertDocDao.getAllTblCmsWcCertDoc();
        } catch (Exception ex) {
            logger.error("getAllCmsTaxConfig : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsTaxConfig : " + logUserId + ENDS);
        return cmsWcCertDocList;
    }

    /***
     *  This method returns no. of TblCmsWcCertDoc objects from database
     * @return long
     */
    @Override
    public long getCmsWcCertDocCount() {
        logger.debug("getCmsTaxConfigCount : " + logUserId + STARTS);
        long cmsWcCertDocCount = 0;
        try {
            cmsWcCertDocCount = cmsWcCertDocDao.getTblCmsWcCertDocCount();
        } catch (Exception ex) {
            logger.error("getCmsTaxConfigCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsTaxConfigCount : " + logUserId + ENDS);
        return cmsWcCertDocCount;
    }

    /***
     * This method returns TblCmsWcCertDoc for the given Id
     * @param int wcCertDocId
     * @return TblCmsWcCertDoc
     */
    @Override
    public TblCmsWcCertDoc getCmsWcCertDoc(int wcCertDocId) {
        logger.debug("getCmsTaxConfig(int) : " + logUserId + STARTS);
        TblCmsWcCertDoc tblCmsWcCertDoc = null;
        List<TblCmsWcCertDoc> cmsWcCertDocList = null;
        try {
            cmsWcCertDocList = cmsWcCertDocDao.findTblCmsWcCertDoc("wcCertDocId", Operation_enum.EQ, wcCertDocId);
        } catch (Exception ex) {
            logger.error("getCmsTaxConfig(int) : " + logUserId + SPACE + ex);
        }
        if (!cmsWcCertDocList.isEmpty()) {
            tblCmsWcCertDoc = cmsWcCertDocList.get(0);
        }
        logger.debug("getCmsTaxConfig(int) : " + logUserId + ENDS);
        return tblCmsWcCertDoc;
    }

    /***
     * This method gives the list of TblCmsWcCertDoc objects for the given wcCertificateId
     * @param int wcCertificateId
     * @return List<TblCmsWcCertDoc>
     */
    @Override
    public List<TblCmsWcCertDoc> getAllCmsWcCertDocForWccId(int wcCertificateId) {
        logger.debug("getAllCmsWcCertDocForWccId : " + logUserId + STARTS);
        List<TblCmsWcCertDoc> cmsWcCertDocList = new ArrayList<TblCmsWcCertDoc>();
        try {
            cmsWcCertDocList = cmsWcCertDocDao.findTblCmsWcCertDoc("tblCmsWcCertificate",Operation_enum.EQ, new TblCmsWcCertificate(wcCertificateId),"wcCertDocId",Operation_enum.ORDERBY,Operation_enum.DESC);
        } catch (Exception ex) {
            logger.error("getAllCmsWcCertDocForWccId : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsWcCertDocForWccId : " + logUserId + ENDS);
        return cmsWcCertDocList;
    }
}
