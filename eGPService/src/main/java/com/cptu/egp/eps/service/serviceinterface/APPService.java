/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;


import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonWeekendsHolidays;
import com.cptu.egp.eps.dao.storedprocedure.SearchMyAppData;
import com.cptu.egp.eps.model.table.TblAppEngEstDoc;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblAppWatchList;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface APPService {

    /**
     * Get APP Details By Store Procedure
     * @param param1
     * @param param2
     * @param param3
     * @return
     * @throws Exception
     */
    public List<CommonAppData> getAPPDetailsBySP(String param1, String param2, String param3) throws Exception;

    /**
     * Fetch User Details for Mail Content
     * @param param1
     * @param param2
     * @param param3
     * @return
     * @throws Exception
     */
    public List<CommonAppData> getUserDetailsForEmail(String param1, String param2, String param3) throws Exception;
    
    /**
     * Fetch Advance APP Search Data by store Procedure
     * @param financialYear
     * @param budgetType
     * @param projectName
     * @param activityName
     * @param Status
     * @param page_inN
     * @param recordPerPageinN
     * @param recordPerPage_inN
     * @param userId
     * @param appId
     * @param appCode
     * @param sortColumn
     * @param sortType
     * @param createdBy
     * @return
     * @throws Exception
     */
    public List<SearchMyAppData> getAPPSearchDetailsBySP(String financialYear, String budgetType, String projectName, String activityName, String Status, int page_inN, int recordPerPageinN,Integer userId,Integer appId,String appCode, Integer createdBy, String sortColumn,String sortType) throws Exception;
    
    boolean CountAPPRevise(String action, int appID, Date LastRevision);
    public String updateAPPcount(String financialYear,String action, int appId);
    /**
     * Insert the APP Details by Store Procedure
     * @param financialYearId
     * @param budgetType
     * @param prjId
     * @param prjName
     * @param officeId
     * @param empId
     * @param aaEmpId
     * @param appCode
     * @param createdBy
     * @param deptId
     * @param aaProcRoleId
     * @param action
     * @param appId
     * @param appType
     * @param entrustingAgency
     * @param refAppId
     * @param revisionCount
     * @param lastRevisionDate
     * @param ActivityName
     * @return
     * @throws Exception
     */
    public List<CommonSPReturn> insertAPDetailsBySP(String financialYearId, int budgetType, int prjId, String prjName, int officeId, int empId, int aaEmpId, String appCode, int createdBy, int deptId, int aaProcRoleId, String action, int appId,
            String appType, String entrustingAgency, Integer refAppId, int revisionCount, Date lastRevisionDate, String ActivityName) throws Exception;

    /**
     * Insert Package Details by Store Procedure.
     * @param appId
     * @param procNature
     * @param serviceType
     * @param pkgNo
     * @param pkgDesc
     * @param bidderCategory
     * @param workCategory
     * @param depoplanWork
     * @param entrustingAgency
     * @param aloBud
     * @param timeFrame
     * @param estCost
     * @param cpvCode
     * @param pkgEstCost
     * @param aaEmpId
     * @param isPQReq
     * @param reoiRfa
     * @param procMtdId
     * @param procType
     * @param sourceFund
     * @param appStatus
     * @param wfStatus
     * @param noOfStages
     * @param action
     * @param pkgId
     * @param lotNo
     * @param lotDesc
     * @param qty
     * @param unit
     * @param lotEstCost
     * @param pkgUrgency
     * @param w1
     * @param w2
     * @param w3
     * @param w4
     * @return
     * @throws Exception
     */
    public List<CommonSPReturn> insertPkgBySP(Integer appId, String procNature, String serviceType, String pkgNo, String pkgDesc, String bidderCategory, String workCategory, String depoplanWork, String entrustingAgency, String timeFrame, Float aloBud, Float estCost, String cpvCode, Float pkgEstCost, Integer aaEmpId, String isPQReq, String reoiRfa, Integer procMtdId, String procType, String sourceFund, String appStatus, String wfStatus, Integer noOfStages, String action, Integer pkgId, String lotNo, String lotDesc, String qty, String unit, String lotEstCost, String pkgUrgency,String w1, String w2, String w3, String w4) throws Exception;

    /**
     * Add APP Engineering Estimate Docs
     * @param tblAppEngEstDoc
     */
    public void addAppEngEstDoc(TblAppEngEstDoc tblAppEngEstDoc);

    /**
     * Get all APP Engineering Estimate Docs
     * @param pckId
     * @return
     * @throws Exception
     */
    public List<TblAppEngEstDoc> getAllAppEngEstDocDetails(int pckId) throws Exception;

    /**
     * Add Details to APP watch List
     * @param tblAppWatchList
     * @return
     */
    public int addToWatchList(TblAppWatchList tblAppWatchList);

    /**
     *Remove Details from Watch List
     * @param watchId
     * @return
     */
    public boolean removeFromWatchList(int  watchId);

    /**
     * Get Calculated Date with given Date and No of Days.
     * @param getDated
     * @param daysN
     * @return
     */
    public List<CommonWeekendsHolidays> getDateWithDiffBySP(Date getDated, Integer daysN);

    /**
     * Get APP Eng estimated Doc Details
     * @param values
     * @return
     * @throws Exception
     */
    public List<TblAppEngEstDoc> getAppEngEstDocDetails(Object... values) throws Exception;

    /**
     * Check if in watch List or not.
     * @param values
     * @return
     */
    public int checkInWatchList(Object... values);

    /**
     * Delete Package Details
     * @param pkgId
     * @return
     * @throws Exception
     */
    public Boolean delPkgDetail(int pkgId,int appId) throws Exception;

    /**
     * Fetch Tenderer Mail by CPV Code
     * @param appId
     * @return
     * @throws Exception
     */
    public String[] getTendererMailByCPVCode(int appId) throws Exception;

    /**
     * get APP Authority Details
     * @param aaEmpId
     * @return
     * @throws Exception
     */
    public String getAppAuthority(int aaEmpId) throws Exception;

    /**
     * check if PE or AU
     * @param userId
     * @return
     * @throws Exception
     */
    public Boolean isPEorAU(int userId) throws Exception;

    /**
     * Check Pkg for for user Id
     * @param pkgNo
     * @param userId
     * @return
     * @throws Exception
     */
    public Boolean chkPkgNo(String pkgNo,int userId) throws Exception;

    /**
     * fetch Package Status by appId and PkgId.
     * @param appId
     * @param pkgId
     * @return
     * @throws Exception
     */
    public String getPkgStatus(int appId,int pkgId) throws Exception;

    /**
     * Get CPV Code by appId
     * @param appId
     * @return
     */
    public List<TblAppPackages> getCPVCodeByappId(int appId);

    /**
     * Is Business Rule is Configured
     * @param pkgId
     * @return
     */
    public boolean isBusRuleConfig(int pkgId);

    /**
     * Is Business Rule is Configured
     * @param pkgId
     * @return
     */
    public boolean isprocMethodRuleConfig(int pkgId);
    
     
    
    /**
     * Is Business Rule is Configured
     * @param pkgNo
     * @param dptType
     * @return
     */
    public String[] getMinMax(String pkgNo, String dptType);
    
    
    
    public boolean isHopaExist(String userID);

    /**
     * get Project End date from Project Id
     * @param prjId
     * @return Project End Date
     */
    public Date getProjectEndDate(int prjId);
    
    /**
     * Set user Id var.
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
    /**
     * check if workflow history link is coming or not.
     * @param appId
     * @return list of Object of child and object id.
     */
    public List<Object[]> getWorkflowHistorylink(int appId);

    /**
     * check file on hand status
     * @param ObjectId
     * @param childId
     * @param userId
     * @param eventId
     * @return
     */
    public List<Object> getFileOnHandStatus(int ObjectId,int childId,int userId,int eventId);
    
    /**
     * Set Audit Trail Object at Service layer.
     * @param Audittrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
    
    /**
     * Check Pkg for for user Id
     * @param pkgNo
     * @param userId
     * @return
     * @throws Exception
     */
    public String chkPkgNoExist(String pkgNo,int userId) throws Exception;
    
    /**
     * Check package dates exists
     * @param pkgNo
     * @return
     * @throws Exception 
     */
    public boolean chkPkgDatesExist(String pkgNo) throws Exception;

    public List<TblAppPackages> getAPPDetailsByPackageNo(String pkgNo) throws Exception;

    public CommonAppData getOfficeInfoFromAPP(String param1, String param2) throws Exception;  
    
    public CommonAppData getAPPFromCode(String code) throws Exception;  
    
    /**
     * @param action
     * @param appId
     * @param pkgNo
     * @return
     * @throws java.lang.Exception
     */
    public List<CommonSPReturn> publishApp(String action,Integer appId, String pkgNo) throws Exception;
}
