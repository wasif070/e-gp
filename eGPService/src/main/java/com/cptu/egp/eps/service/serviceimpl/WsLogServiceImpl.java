/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblWsLogDao;
import com.cptu.egp.eps.model.table.TblWsLog;
import com.cptu.egp.eps.service.serviceinterface.WsLogService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This service class used Database operations for TblWsLog
 * @author Sreenu
 *
 */
public class WsLogServiceImpl implements WsLogService {

    final Logger logger = Logger.getLogger(WsLogServiceImpl.class);
    private HibernateQueryDao hibernateQueryDao;
    private TblWsLogDao tblWsLogDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";

    public TblWsLogDao getTblWsLogDao() {
        return tblWsLogDao;
    }

    public void setTblWsLogDao(TblWsLogDao tblWsLogDao) {
        this.tblWsLogDao = tblWsLogDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

     /***
     * This method inserts a TblWsLog object in database
     * @param TblWsLog tblWsLog
     * @return int
     */
    @Override
    public int insertWsLog(TblWsLog tblWsLog) {
        logger.debug("insertWsLog : " + logUserId + STARTS);
        int flag = 0;
        try {
            tblWsLogDao.addTblWsLog(tblWsLog);
            flag = tblWsLog.getWsLogId();
        } catch (Exception ex) {
            logger.error("insertWsLog : " + logUserId + " : " + ex);
            flag = 0;
        }
        logger.debug("insertWsLog : " + logUserId + ENDS);
        return flag;
    }

     /***
     * This method updates an TblWsLog object in DB
     * @param TblWsLog tblWsLog
     * @return boolean
     */
    @Override
    public boolean updateWsLog(TblWsLog tblWsLog) {
        logger.debug("updateWsLog : " + logUserId + STARTS);
        boolean flag;
        try {
            tblWsLogDao.updateTblWsLog(tblWsLog);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateWsLog : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("updateWsLog : " + logUserId + ENDS);
        return flag;
    }

     /***
     * This method deletes an TblWsLog object in DB
     * @param TblWsLog tblWsLog
     * @return boolean
     */
    @Override
    public boolean deleteWsLog(TblWsLog tblWsLog) {
        logger.debug("deleteWsLog : " + logUserId + STARTS);
        boolean flag;
        try {
            tblWsLogDao.deleteTblWsLog(tblWsLog);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteWsLog : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteWsLog : " + logUserId + ENDS);
        return flag;
    }

     /***
     * This method returns all TblWsLog objects from database
     * @return List<TblWsLog>
     */
    @Override
    public List<TblWsLog> getAllWsLogs() {
        logger.debug("getAllWsLogs : " + logUserId + STARTS);
        List<TblWsLog> tblWsLogsList = null;
        try {
            tblWsLogsList = tblWsLogDao.getAllTblWsLogs();
        } catch (Exception ex) {
            logger.error("getAllWsLogs : " + logUserId + " : " + ex);
        }
        logger.debug("getAllWsLogs : " + logUserId + ENDS);
        return tblWsLogsList;
    }

     /***
     * This method returns no. of TblWsLog objects from database
     * @return long
     */
    @Override
    public long getWsLogCount() {
        logger.debug("getWsLogCount : " + logUserId + STARTS);
        long wsLogsCount = 0;
        try {
            wsLogsCount = tblWsLogDao.getTblWsLogCount();
        } catch (Exception ex) {
            logger.error("getWsLogCount : " + logUserId + " : " + ex);
        }
        logger.debug("getWsLogCount : " + logUserId + ENDS);
        return wsLogsCount;
    }
}
