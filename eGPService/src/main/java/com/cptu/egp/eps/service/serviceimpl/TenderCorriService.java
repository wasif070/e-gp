/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCorriAuditTrailDao;
import com.cptu.egp.eps.dao.daointerface.TblCorrigendumDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblLimitedTendersDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderPhasingDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblCorriAuditTrail;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import com.cptu.egp.eps.model.table.TblLimitedTenders;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderPhasing;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.daointerface.TblAppFrameworkOfficeDao;
import com.cptu.egp.eps.model.table.TblAppFrameworkOffice;

/**
 *
 * @author Administrator
 */
public class TenderCorriService {

    static final Logger logger = Logger.getLogger(TenderCorriService.class);
    TblTenderDetailsDao tblTenderDetailsDao;
    TblTenderPhasingDao tblTenderPhasingDao;
    TblTenderLotSecurityDao tblTenderLotSecurityDao;
    TblCorrigendumDetailDao tblCorrigendumDetailDao;
    HibernateQueryDao hibernateQueryDao;
    TblCorriAuditTrailDao tblCorriAuditTrailDao;
    SPTenderCommon sPTenderCommon;
    TblLimitedTendersDao tblLimitedTendersDao;
    private TblAppFrameworkOfficeDao tblAppFrameworkOfficeDao;
    private SpgetCommonSearchDataMore spgetCommonSearchDataMore;
    MakeAuditTrailService makeAuditTrailService;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private String starts = " Starts";
    private String ends = " Ends";

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public SpgetCommonSearchDataMore getSpgetCommonSearchDataMore() {
        return spgetCommonSearchDataMore;
    }

    public void setSpgetCommonSearchDataMore(SpgetCommonSearchDataMore spgetCommonSearchDataMore) {
        this.spgetCommonSearchDataMore = spgetCommonSearchDataMore;
    }

    public TblLimitedTendersDao getTblLimitedTendersDao() {
        return tblLimitedTendersDao;
    }

    public void setTblLimitedTendersDao(TblLimitedTendersDao tblLimitedTendersDao) {
        this.tblLimitedTendersDao = tblLimitedTendersDao;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    public TblCorriAuditTrailDao getTblCorriAuditTrailDao() {
        return tblCorriAuditTrailDao;
    }

    public void setTblCorriAuditTrailDao(TblCorriAuditTrailDao tblCorriAuditTrailDao) {
        this.tblCorriAuditTrailDao = tblCorriAuditTrailDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblCorrigendumDetailDao getTblCorrigendumDetailDao() {
        return tblCorrigendumDetailDao;
    }

    public void setTblCorrigendumDetailDao(TblCorrigendumDetailDao tblCorrigendumDetailDao) {
        this.tblCorrigendumDetailDao = tblCorrigendumDetailDao;
    }

    public TblTenderLotSecurityDao getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    public void setTblTenderLotSecurityDao(TblTenderLotSecurityDao tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }

    public TblTenderPhasingDao getTblTenderPhasingDao() {
        return tblTenderPhasingDao;
    }

    public void setTblTenderPhasingDao(TblTenderPhasingDao tblTenderPhasingDao) {
        this.tblTenderPhasingDao = tblTenderPhasingDao;
    }

    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    /**
     * Get Tender Details
     *
     * @param values
     * @return Tender details into TblTenderDetails object
     * @throws Exception
     */
    public TblTenderDetails getTenderDetails(Object... values) throws Exception {
        logger.debug("getTenderDetails : " + logUserId + starts);
        logger.debug("getTenderDetails : " + logUserId + ends);
        return tblTenderDetailsDao.findTblTenderDetails(values).get(0);
    }

    /**
     * Get Tender phase information
     *
     * @param values
     * @return
     * @throws Exception
     */
    public List<TblTenderPhasing> findTenderPhasing(Object... values) throws Exception {
        logger.debug("findTenderPhasing : " + logUserId + starts);
        logger.debug("findTenderPhasing : " + logUserId + ends);
        return tblTenderPhasingDao.findTblTenderPhasing(values);
    }

    /**
     * Get Tender Lot Security Details
     *
     * @param values
     * @return
     * @throws Exception
     */
    public List<TblTenderLotSecurity> findTenderLotSecurity(Object... values) throws Exception {
        logger.debug("findTenderLotSecurity : " + logUserId + starts);
        logger.debug("findTenderLotSecurity : " + logUserId + ends);
        return tblTenderLotSecurityDao.findTblTenderLotSecurity(values);
    }

    /**
     * Inserting Corrigendum Details for the Tender
     *
     * @param tblCorrigendumDetails List of TblCorrigendumDetail to be inserted
     * @param isedit check whether it is to create or edit
     * @param corriId required for Editing corrigendum
     */
    public void insertAllCorriDetails(List<TblCorrigendumDetail> tblCorrigendumDetails, boolean isedit, int corriId) {
        logger.debug("insertAllCorriDetails : " + logUserId + starts);
        if (isedit) {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblCorrigendumDetail where corrigendumId=" + corriId);
        }
        tblCorrigendumDetailDao.updateOrSaveAllCorriDetail(tblCorrigendumDetails);
        logger.debug("insertAllCorriDetails : " + logUserId + ends);
    }

    /**
     * Fetch Corrigendum details
     *
     * @param values search parameters
     * @return List of TblCorrigendumDetail
     * @throws Exception
     */
    public List<TblCorrigendumDetail> findCorrigendumDetail(Object... values) throws Exception {
        logger.debug("findCorrigendumDetail : " + logUserId + starts);
        logger.debug("findCorrigendumDetail : " + logUserId + ends);
        return tblCorrigendumDetailDao.findTblCorrigendumDetail(values);
    }

    /**
     * Fetch phaserefferenceno or Lot no base on passed value.
     *
     * @param isphase true then it returns phashrefferenceno else return lotno
     * @param objId phaseid or lotid
     * @return
     */
    public String phaseOrLotSecureDetail(boolean isphase, String objId) {
        logger.debug("phaseOrLotSecureDetail : " + logUserId + starts);
        String value = null;
        if (isphase) {
            value = (String) hibernateQueryDao.singleColQuery("select ttp.phasingRefNo from TblTenderPhasing ttp where ttp.tenderPhasingId=" + objId).get(0);
        } else {
            value = (String) hibernateQueryDao.singleColQuery("select ttls.lotNo from TblTenderLotSecurity ttls where ttls.tenderLotSecId=" + objId).get(0);
        }
        logger.debug("phaseOrLotSecureDetail : " + logUserId + ends);
        return value;
    }

    /**
     * Publish Corrigendum for the tender
     *
     * @param tenderid from tbl_TenderMaster
     * @param corrid from tbl_corrigendumdetail
     * @param remarks added by PE
     * @param action for audit trail
     * @param actionBy PE userId
     * @param eSign for audit trail
     */
    public void publishCorri(String tenderid, String corrid, String remarks, String action, String actionBy, String eSign) {
        logger.debug("publishCorri : " + logUserId + starts);
        String Action = null;
        try {
            List<SPTenderCommonData> commonMsgChk = sPTenderCommon.executeProcedure("updateTenderdetail", tenderid, corrid);
            if (!commonMsgChk.isEmpty()) {
                if (commonMsgChk.get(0).getFieldName1().equalsIgnoreCase("false")) {
                    logger.error("publishCorri : " + commonMsgChk.get(0).getFieldName2());
                }
            }
            tblCorriAuditTrailDao.addTblCorriAuditTrail(new TblCorriAuditTrail(0, Integer.parseInt(tenderid), Integer.parseInt(corrid), remarks, action, Integer.parseInt(actionBy), new Date(), eSign, ""));
            // commented bcoz it is added in stored procedure as part of transaction by GSS helpdeskid:10228 // hibernateQueryDao.updateDeleteNewQuery("update TblCorrigendumMaster set corrigendumStatus='Approved' where corrigendumId=" + corrid);
            Action = "Publish Corrigendum/Amendment";
        } catch (Exception e) {
            Action = "Error in Publish Corrigendum/Amendment" + e;
            logger.error("publishCorri : " + logUserId, e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderid), "tenderId", EgpModule.Corrigendum_Amendment.getName(), Action, remarks);
        }
        logger.debug("publishCorri : " + logUserId + ends);
    }

    /**
     * Get TenderRule Engine for creating corrigendum
     *
     * @param tenderid from tbl_TenderMaster
     * @return amendMinDays,amendPercent
     */
    public Object[] findTenderRuleEngine(String tenderid) {
        logger.debug("findTenderRuleEngine : " + logUserId + starts);
        StringBuilder sb = new StringBuilder();
        Object[] obj = null;
        sb.append("select c.amendMinDays,c.amendPercent from TblConfigAmendment c,TblTenderDetails t,TblTenderTypes tt ");
        sb.append("where c.tblProcurementMethod.procurementMethodId=t.procurementMethodId and tt.tenderType=t.eventType and t.tblTenderMaster.tenderId=" + tenderid);
        List<Object[]> object = hibernateQueryDao.createNewQuery(sb.toString());
        if (!object.isEmpty()) {
            obj = object.get(0);
        }
        logger.debug("findTenderRuleEngine : " + logUserId + ends);
        return obj;
    }

    /**
     * Gives email,mobile no,ref No.,office Name,Closing date,tender Brief as a
     * String Array
     *
     * @param tenderid from tbl_TenderMaster
     * @return email,mobile no,ref No.,office Name,Closing date,tender Brief as
     * a String Array
     */
    public String[] getEmailSMSforCorri(String tenderid) {
        logger.debug("getEmailSMSforCorri : " + logUserId + starts);
        String[] mobMail = new String[10];
        for (SPTenderCommonData commonData : sPTenderCommon.executeProcedure("getEmailIdCPV", tenderid, null)) {
            mobMail[0] = commonData.getFieldName1();//email
            mobMail[1] = commonData.getFieldName2();//mobile no
            mobMail[2] = commonData.getFieldName3();//ref No.
            mobMail[3] = commonData.getFieldName4();//office Name
            mobMail[4] = commonData.getFieldName5();//Closing date
            mobMail[5] = commonData.getFieldName6();//tender Brief
            mobMail[6] = commonData.getFieldName7();//Tenderer Name
            mobMail[7] = commonData.getFieldName8();//Validity Days
            mobMail[8] = commonData.getFieldName9();//PE Adress
            mobMail[9] = commonData.getFieldName10();//PE Designation
        }
        logger.debug("getEmailSMSforCorri : " + logUserId + ends);
        return mobMail;
    }

    /**
     * Get Bidders for Mapping to Tender
     *
     * @param tenderid from tbl_TenderMaster
     * @param searchString criteria for searching
     * @return List of SPTenderCommonData
     */
    public List<SPTenderCommonData> searchBidderForMapping(String tenderid, String searchString) {
        logger.debug("searchBidderForMapping : " + logUserId + starts);
        logger.debug("searchBidderForMapping : " + logUserId + ends);
        return sPTenderCommon.executeProcedure("searchBidder", tenderid, searchString);
    }

    /**
     * Map and Unmap Bidders from the Tender
     *
     * @param userid Bidder userId's
     * @param tenderid from tbl_TenderMaster
     * @param isMap flag indicating map or unmap
     */
    public void mapUnmapBidderWithTender(String[] userid, int tenderid, boolean isMap, int lotId) {
        logger.debug("mapUnmapBidderWithTender : " + logUserId + starts);
        if (isMap) {
            for (int i = 0; i < userid.length; i++) {
                tblLimitedTendersDao.addTblLimitedTenders(new TblLimitedTenders(0, tenderid, Integer.parseInt(userid[i]), new Date(), lotId));
            }
        } else {
            StringBuilder users = new StringBuilder();
            for (int i = 0; i < userid.length; i++) {
                users.append(userid[i] + ",");
            }
            hibernateQueryDao.updateDeleteNewQuery("delete from TblLimitedTenders tlt where tlt.userId in (" + users.substring(0, users.length() - 1) + ") and tlt.tenderId=" + tenderid + " and pkgLotId=" + lotId);
            users = null;
        }
        logger.debug("mapUnmapBidderWithTender : " + logUserId + ends);
    }

    /**
     * Search Bidders for limited Tender
     *
     * @param pkgLotId from tbl_tenderLotSecurity
     * @param tenderid from tbl_Tendermaster
     * @param chkPckLot flag indicating package or lot
     */
    public List<TblLimitedTenders> chkTblLimitedTenders(int tenderid, int pkgLotId, boolean chkPckLot) {
        logger.debug("chkTblLimitedTenders : " + logUserId + starts);
        List<TblLimitedTenders> tblLimitedTenderses = null;
        try {
            if (chkPckLot) {
                tblLimitedTenderses = tblLimitedTendersDao.findTblLimitedTenders("tenderId", Operation_enum.EQ, tenderid, "pkgLotId", Operation_enum.EQ, pkgLotId);
            } else {
                tblLimitedTenderses = tblLimitedTendersDao.findTblLimitedTenders("tenderId", Operation_enum.EQ, tenderid);
            }
        } catch (Exception e) {
            logger.error("chkTblLimitedTenders : " + logUserId + e);
        }
        logger.debug("chkTblLimitedTenders : " + logUserId + ends);
        return tblLimitedTenderses;
    }

    /**
     * Search for CPV
     *
     * @param tenderId from tbl_TenderMaster
     * @return List of procurementMethod,cpvCode & procurementType
     */
    public List<Object[]> getPMethodCPV(String tenderId) {
        logger.debug("getPMethodCPV : " + logUserId + starts);
        logger.debug("getPMethodCPV : " + logUserId + ends);
        return hibernateQueryDao.createNewQuery("select ttd.procurementMethod,ttd.cpvCode,ttd.procurementType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId);
    }

    /**
     * Bidder Search for mapping in case of LTM
     *
     * @param tenderid from tbl_tendermaster
     * @param searchString searchcriteria
     * @param lotId from tbl_tenderLotSecurity
     * @param isLTM check for LTM
     * @return List of SPCommonSearchDataMore
     */
    public List<SPCommonSearchDataMore> searchBidderForMapping(String tenderid, String searchString, String lotId, String isLTM, String procN) {
        logger.debug("searchBidderForMapping : " + logUserId + starts);
        logger.debug("searchBidderForMapping : " + logUserId + ends);
        return spgetCommonSearchDataMore.executeProcedure("searchBidder", tenderid, searchString, lotId, isLTM, procN, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    /**
     * set user id for log purpose
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * set audit data for audit trail entry
     *
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * Changing Configure Mark Status.
     *
     * @param tenderId = TblEvalServiceForms.tenderId &
     * TblEvalServiceForms.tenderId
     * @return true/false weather successfully status change or not.
     */
    public boolean chngConfigMarkStatus(String tenderId) {
        boolean flag = false;
        try {
            long count = hibernateQueryDao.countForNewQuery("TblEvalServiceForms tf", "tf.tenderId=" + tenderId + " and tf.isCurrent='yes'");
            if (count != 0) {
                hibernateQueryDao.updateDeleteNewQuery("delete TblEvalServiceForms  where tenderId=" + tenderId + " and isCurrent='no'");
                hibernateQueryDao.updateDeleteNewQuery("update TblEvalServiceForms set isCurrent='no' where tenderId=" + tenderId + " and isCurrent='yes'");
            }
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean isTenderSectionsUpdated(String tenderId, String corriId) {

        logger.debug("isTenderSectionsUpdated : " + logUserId + starts);
        boolean isTenSecUpdated = false;
        try {
            List<SPTenderCommonData> commonMsgChk = sPTenderCommon.executeProcedure("isTenderSectionsUpdatedinCorrigendum", tenderId, corriId);
            if (!commonMsgChk.isEmpty()) {
                if (commonMsgChk.get(0).getFieldName1().equalsIgnoreCase("true")) {
                    logger.debug("isTenderSectionsUpdated : " + commonMsgChk.get(0).getFieldName2());
                    isTenSecUpdated = true;
                }
            }

        } catch (Exception e) {
            isTenSecUpdated = false;
            logger.error("isTenderSectionsUpdated : " + logUserId, e);
        }
        logger.debug("isTenderSectionsUpdated : " + logUserId + ends);
        return isTenSecUpdated;

    }

    public TblAppFrameworkOfficeDao getTblAppFrameworkOfficeDao() {
        return tblAppFrameworkOfficeDao;
    }

    public void setTblAppFrameworkOfficeDao(TblAppFrameworkOfficeDao tblAppFrameworkOfficeDao) {
        this.tblAppFrameworkOfficeDao = tblAppFrameworkOfficeDao;
    }

    public void mapUnmapPEWithPackage(int appid, int packageId, String[] peofficeId, String[] officeName, String[] state, String[] address, boolean isMap) {
        logger.debug("mapUnmapPEWithPackage : " + logUserId + starts);
        if (isMap) {
            for (int i = 0; i < peofficeId.length; i++) {
                tblAppFrameworkOfficeDao.addTblAppFrameworkOffice(new TblAppFrameworkOffice(Long.parseLong("0"), appid, packageId, Integer.parseInt(peofficeId[i]), officeName[i], state[i], address[i]));
            }
        } else {
//            StringBuilder peoffices = new StringBuilder();
//            for (int i = 0; i < peofficeId.length; i++) {
//                peoffices.append(peofficeId[i] + ",");
//            }
            hibernateQueryDao.updateDeleteNewQuery("delete from TblAppFrameworkOffice tlt where tlt.appid = "+ appid +"  and tlt.packageId=" + packageId);
//            peoffices = null;
        }
        logger.debug("mapUnmapPEWithPackage : " + logUserId + ends);
    }
    
        public List<TblAppFrameworkOffice> findAppFrameworkOffice(Object... values) throws Exception {
        logger.debug("findAppFrameworkOffice : " + logUserId + starts);
        logger.debug("findAppFrameworkOffice : " + logUserId + ends);
        return tblAppFrameworkOfficeDao.findTblAppFrameworkOffice(values);
    }
}
