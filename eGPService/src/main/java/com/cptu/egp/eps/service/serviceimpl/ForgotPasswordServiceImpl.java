/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblForgotPasswordDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblForgotPassword;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceinterface.ForgotPasswordService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;



/**
 *
 * @author Administrator
 */
public class ForgotPasswordServiceImpl implements ForgotPasswordService {

    final Logger logger = Logger.getLogger(ForgotPasswordServiceImpl.class);
    private String logUserId ="0";
    TblLoginMasterDao tblLoginMasterDao;
    TblForgotPasswordDao tblForgotPasswordDao;
    HibernateQueryDao hibernateQueryDao;
    MakeAuditTrailService makeAuditTrailService;
    AuditTrail auditTrail;

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblForgotPasswordDao getTblForgotPasswordDao() {
        return tblForgotPasswordDao;
    }

    public void setTblForgotPasswordDao(TblForgotPasswordDao tblForgotPasswordDao) {
        this.tblForgotPasswordDao = tblForgotPasswordDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao()
    {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao)
    {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public String chkMailId(String mailId,String action)
    {
        logger.debug("chkMailId : "+logUserId+" Starts");
        String result="false";
        String straction = "Reset Password";
        int userId  = 0;
        try {
            List<TblLoginMaster> tblLoginMasters=tblLoginMasterDao.findTblLoginMaster("emailId", Operation_enum.EQ, mailId);
            int size=tblLoginMasters.size();
            if(size>0)
            {
                if(action.equals("forgot")){
                    result=tblLoginMasters.get(0).getHintQuestion();
                    String userType = String.valueOf(tblLoginMasters.get(0).getRegistrationType());
                    if(result.equals("")&&userType.equals("officer")){
                        result = "O:Officer";
                    }
                }else if(action.equals("reset")){
                    result=String.valueOf(tblLoginMasters.get(0).getUserId());
                    userId = tblLoginMasters.get(0).getUserId();
                }
            }
            else
            {
                result="E:Mail-ID Does NOT Exist";
            }
        } catch (Exception ex) {
            logger.error("chkMailId : "+logUserId+" : "+ex.toString());
            straction = "Error in "+straction+" "+ex.getMessage();
            
        }finally{
            if(action.equals("reset")){
                makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), straction, "");
                //auditTrail=null;
            }
            straction = null;
        }
        logger.debug("chkMailId : "+logUserId+" Ends");
        return result;
    }

    @Override
    public boolean verifyHintAnswer(String hintAnswer,String mailId)
    {
        logger.debug("verifyHintAnswer : "+logUserId+" Starts");
       boolean result=false;
        try {
            Object[] values={"emailId", Operation_enum.EQ, mailId,
                             "hintAnswer",Operation_enum.EQ,hintAnswer};
            List<TblLoginMaster> tblLoginMasters=tblLoginMasterDao.findTblLoginMaster(values);
            int size=tblLoginMasters.size();
            if(size>0){
                 result=true;
            }
        } catch (Exception ex) {
            logger.error("verifyHintAnswer : "+logUserId+" : "+ex.toString());
        }
       logger.debug("verifyHintAnswer : "+logUserId+" Ends");
        return result;
    }

    @Override
    public boolean verifyPassword(String mailId, String password)
    {
        logger.debug("verifyPassword : "+logUserId+" Starts");
        boolean result=false;
        try {
            Object[] values={"emailId", Operation_enum.LIKE, mailId,
                             "password",Operation_enum.LIKE,password};
            List<TblLoginMaster> tblLoginMasters=tblLoginMasterDao.findTblLoginMaster(values);
            int size=tblLoginMasters.size();
            if(size>0){
                 result=true;
            }
        } catch (Exception ex) {
            logger.error("verifyPassword : "+logUserId+" : "+ex.toString());
        }
        logger.debug("verifyPassword : "+logUserId+" Ends");
        return result;
    }

    @Override
    public boolean addRandomeCode(TblForgotPassword tblForgotPassword) {
       logger.debug("addRandomeCode : "+logUserId+" Starts");
       boolean flag = false;
       try {
                tblForgotPasswordDao.addTblForgotPassword(tblForgotPassword);
                flag = true;
        } catch (Exception e) {
            logger.error("addRandomeCode : "+logUserId+" : "+e.toString());
        }
       logger.debug("addRandomeCode : "+logUserId+" Ends");
       return flag;
    }

    @Override
    public void deleteRandomeCode(String emailid) {
       logger.debug("deleteRandomeCode : "+logUserId+" Starts");
       try {
              hibernateQueryDao.updateDeleteNewQuery("delete from TblForgotPassword where emailId = '"+emailid+"'");
        } catch (Exception e) {
            logger.error("deleteRandomeCode : "+logUserId+" : "+e.toString());
        }
       logger.debug("deleteRandomeCode : "+logUserId+" Ends");
    }

    @Override
    public List<TblForgotPassword> getForgotData(String emailid) {
       logger.debug("getForgotData : "+logUserId+" Starts");
       List<TblForgotPassword> list =null;
       try {
                list = tblForgotPasswordDao. findTblForgotPassword("emailId",Operation_enum.EQ,emailid);
        } catch (Exception e) {
            logger.error("getForgotData : "+logUserId+" : "+e.toString());
        }
       logger.debug("getForgotData : "+logUserId+" Ends");
       return list;
    }
}
