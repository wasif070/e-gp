/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.model.table.TblContentVerification;
import com.cptu.egp.eps.dao.daointerface.TblContentVerificationDao;
import com.cptu.egp.eps.dao.daointerface.TblPublicProcureForumDao;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblPublicProcureForum;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class PublicForumPostService {

    static final Logger logger = Logger.getLogger(PublicForumPostService.class);
    TblPublicProcureForumDao tblPublicProcureForumDao;
    TblContentVerificationDao tblContentVerificationDao;
    HibernateQueryDao hibernateQueryDao;
    MakeAuditTrailService makeAuditTrailService;
    SPCommonSearch sptender;
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }


    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    

    public TblContentVerificationDao getTblContentVerificationDao() {
        return tblContentVerificationDao;
    }

    public void setTblContentVerificationDao(TblContentVerificationDao tblContentVerificationDao) {
        this.tblContentVerificationDao = tblContentVerificationDao;
    }

    public void setSptender(SPCommonSearch sptender) {
        this.sptender = sptender;
    }

    public SPCommonSearch getSptender() {
        return sptender;
    }

    public TblPublicProcureForumDao getTblPublicProcureForumDao() {
        return tblPublicProcureForumDao;
    }

    public void setTblPublicProcureForumDao(TblPublicProcureForumDao tblPublicProcureForumDao) {
        this.tblPublicProcureForumDao = tblPublicProcureForumDao;
    }

    /**
     * fetching info from Tbl_PublicProcureForum
     * @param ppfId
     * @return list of Tbl_PublicProcureForum
     * @throws Exception
     */
    public List<TblPublicProcureForum> getAllForums(String ppfId) throws Exception {
        logger.debug("getAllForums : " + logUserId + " Starts");
        List<TblPublicProcureForum> procureForums = null;

        try {
            procureForums = tblPublicProcureForumDao.findTblPublicProcureForum("ppfId", Operation_enum.EQ, Integer.parseInt(ppfId));
        } catch (Exception e) {
            logger.error("getAllForums : " + logUserId,e);

        }
        logger.debug("getAllForums : " + logUserId + " Ends");
        return procureForums;
    }

    /**
     * Inserting record in to Tbl_PublicProcureForum
     * @param tblPublicProcureForum
     * @return true if inserted successfully else false
     */
    public boolean saveForum(TblPublicProcureForum tblPublicProcureForum) {
        logger.debug("saveForum : " + logUserId + " Starts");
        boolean bSuccess = false;
        String action = null;
        try {
            tblPublicProcureForumDao.addTblPublicProcureForum(tblPublicProcureForum);
            bSuccess = true;
            action = "Post "+(tblPublicProcureForum.getPpfParentId()==0 ? "Topic":"Reply");
        } catch (Exception e) {
            logger.error("saveForum : " + logUserId + e);
            bSuccess = false;
            action = "Post "+(tblPublicProcureForum.getPpfParentId()==0 ? "Topic ":"Reply ")+e;
        }finally{
             /*if(tblPublicProcureForum.getTblLoginMaster().getTblUserTypeMaster() != null && tblPublicProcureForum.getTblLoginMaster().getTblUserTypeMaster().getUserTypeId() != 2)
            {*/
                    action = action +" by "+tblPublicProcureForum.getFullName();
            /*}
            else
            {
                action = action +" by Tenderer";
            }*/
            makeAuditTrailService.generateAudit(auditTrail, tblPublicProcureForum.getPostedBy(), "userId", EgpModule.Public_Procurement_Forum.getName(), action, tblPublicProcureForum.getComments()!=null ? tblPublicProcureForum.getComments():"");
        }
        logger.debug("saveForum : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Fetching info from Tbl_ContentVerification
     * @param contentVerificationId
     * @return list of TblContentVerification
     * @throws Exception
     */
    public List<TblContentVerification> getContentVerficationStatus(String contentVerificationId) throws Exception {
        return tblContentVerificationDao.findTblContentVerification("contentVerificationId", Operation_enum.EQ, Integer.parseInt(contentVerificationId));
    }

    public List<TblPublicProcureForum> getAllReply(String ppfId) throws Exception {
        logger.debug("getAllReply : " + logUserId + " Starts");
        List<TblPublicProcureForum> procureForums = null;
        
        try {
            procureForums = tblPublicProcureForumDao.findTblPublicProcureForum("ppfParentId", Operation_enum.EQ, Integer.parseInt(ppfId));
        } catch (Exception e) {
            logger.error("getAllReply : " + logUserId,e);
            
        }
        logger.debug("getAllReply : " + logUserId + " Ends");
        return procureForums;
    }

    /**
     * @param funName
     * @param viewType
     * @param keyword
     * @param postedby
     * @param dop
     * @param userId
     * @param page
     * @param recordPerPage
     * @param nine
     * @param ten
     * @return list of SPCommonSearchData
     */
    public List<SPCommonSearchData> getpost(String funName, String viewType, String keyword, String postedby, String dop, String userId, String page, String recordPerPage, String nine, String ten) {
        return sptender.executeProcedure(funName, viewType, keyword, postedby, dop, userId, page, recordPerPage, nine, ten, null);
    }

    /**
     * updating info into Tbl_PublicProcureForum
     * @param tblPublicProcureForum
     * @return true if updated successfully else false
     */
    public boolean updateForum(TblPublicProcureForum tblPublicProcureForum/*,String topicHead*/) {
        logger.debug("updateForum : " + logUserId + " Starts");
        boolean bSuccess = false;
        String action = null;
        try {
            /*if(topicHead!=null){
                hibernateQueryDao.updateDeleteNewQuery("update TblPublicProcureForum set subject='"+topicHead+"' where ppfId="+tblPublicProcureForum.getPpfParentId());
            }*/
            tblPublicProcureForumDao.updateTblPublicProcureForum(tblPublicProcureForum);
            bSuccess = true;
            action = (tblPublicProcureForum.getPpfParentId()==0 ? "Topic":"Reply")+" "+tblPublicProcureForum.getStatus();
        } catch (Exception e) {
            logger.error("updateForum : " + logUserId + e);
            bSuccess = false;
            action = (tblPublicProcureForum.getPpfParentId()==0 ? "Topic ":"Reply ")+tblPublicProcureForum.getStatus()+" "+e;
        }finally{
            
            /*if(tblPublicProcureForum.getTblLoginMaster().getTblUserTypeMaster() != null && tblPublicProcureForum.getTblLoginMaster().getTblUserTypeMaster().getUserTypeId() != 2)
            {*/
                    action = action +" by "+tblPublicProcureForum.getFullName();
            /*}
            else
            {
                action = action +" by Tenderer";
            }*/
            makeAuditTrailService.generateAudit(auditTrail, tblPublicProcureForum.getPostedBy(), "userId", EgpModule.Public_Procurement_Forum.getName(), action, tblPublicProcureForum.getComments()!=null ? tblPublicProcureForum.getComments():"");
        }
        logger.debug("updateForum : " + logUserId + " Ends");
        return bSuccess;
    }

    public boolean deletePPF(String ppfid){
        int cnt = hibernateQueryDao.updateDeleteNewQuery("delete from TblPublicProcureForum where ppfId="+ppfid);
        return cnt!=0;
    }
    /**
     * Set user id for Logging purpose
     * @param logUserId
     */
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * Set audit trail information for audit trail
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }    
}
