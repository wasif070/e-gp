/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface CreateSubSection {
    
    /**
     * Create Subsection
     * @param tblIttHeader
     * @return
     */
    public boolean createSubSection(TblIttHeader tblIttHeader);
    /**
     * Get Subsection by section id
     * @param sectionId
     * @return
     */
    public List<TblIttHeader> getSubSection(int sectionId);
    /**
     * Get Subsection with TDS Appl
     * @param sectionId
     * @return
     */
    public List<Object[]> getSubSectionWithTDSAppl(int sectionId);
    /**
     * Update SubSection
     * @param tblIttHeader
     * @return
     */
    public boolean updateSubSection(TblIttHeader tblIttHeader);
    /**
     * Delete SubSection
     * @param tblIttHeader
     * @return
     */
    public boolean deleteSubSection(TblIttHeader tblIttHeader);
    /**
     * Delete SubSection with Section ID.
     * @param sectionId
     * @return
     */
    public boolean deleteSection(int sectionId);
    /**
     * Set User Id at service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
    /**
     * For Audit Trail
     * @param logUserId
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
