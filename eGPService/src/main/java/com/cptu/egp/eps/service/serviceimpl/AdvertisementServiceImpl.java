/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAdvertisementDao;
import com.cptu.egp.eps.dao.storedprocedure.SPCommon;
import com.cptu.egp.eps.model.table.TblAdvertisement;
import com.cptu.egp.eps.service.serviceinterface.AdvertisementService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Lakshmi.yerramsetti
 */
public class AdvertisementServiceImpl implements AdvertisementService {

    private static final Logger LOGGER = Logger.getLogger(CascadeDetailsServiceImpl.class);
    TblAdvertisementDao tblAdvertisementDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private int adId = 1;
    SPCommon sPCommon;
//    TblAdvertisement tblAdvertisement = new TblAdvertisement();

    public TblAdvertisementDao getTblAdvertisementDao() {
        return tblAdvertisementDao;
    }

    public void setTblAdvertisementDao(TblAdvertisementDao tblAdvertisementDao) {
        this.tblAdvertisementDao = tblAdvertisementDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPCommon getsPCommon() {
        return sPCommon;
    }

    public void setsPCommon(SPCommon sPCommon) {
        this.sPCommon = sPCommon;
    }

    @Override
    public Integer addTblAdvertisement(TblAdvertisement tblAdvertisement) throws Exception {
        LOGGER.debug("addInvoiceAccDetails : " + logUserId + "Starts");
        return tblAdvertisementDao.addTblAdvertisement(tblAdvertisement);
    }

    @Override
    public boolean updateTblAdvertisement(TblAdvertisement tblAdvertisement) {
        LOGGER.debug("updateSubSection : " + adId + " Starts");
        boolean updateSubSection;
        try {
            tblAdvertisementDao.updateEntity(tblAdvertisement);
            updateSubSection = true;
        } catch (Exception ex) {
            updateSubSection = false;
            LOGGER.error("updateSubSection : " + adId + " :" + ex);
        }
        LOGGER.debug("updateSubSection : " + adId + " Ends");
        return updateSubSection;
    }

    @Override
    public List<TblAdvertisement> getAllTblAdvertisement() {
        LOGGER.debug("getAllAppEngEstDocDetails : " + logUserId + "LOGGERSTART");
        List<TblAdvertisement> tblAdvertisement = null;
        try {
            tblAdvertisement = tblAdvertisementDao.getAllTblAdvertisement();
        } catch (Exception e) {
            LOGGER.error("getAllAppEngEstDocDetails : " + logUserId + e);
        }
        LOGGER.debug("getAllAppEngEstDocDetails : " + logUserId + "LOGGEREND");
        return tblAdvertisement;
    }

    @Override
    public List<TblAdvertisement> findTblAdvertisement(Object... values) {
        LOGGER.debug("getAskProcurement : " + logUserId + "loggerStart");
        List<TblAdvertisement> tblAdvertisement;
        try {
            tblAdvertisement = tblAdvertisementDao.findTblAdvertisement(values);
        } catch (Exception e) {
            tblAdvertisement = null;
            LOGGER.error("getAskProcurement : " + logUserId + e);
        }
        LOGGER.debug("getAskProcurement : " + logUserId + "loggerEnd");
        return tblAdvertisement;
    }

    @Override
    public List<Object> contentAdmMailId() {
        try {
            LOGGER.debug("contentAdmMailId : " + logUserId + "loggerStart");
            LOGGER.debug("contentAdmMailId : " + logUserId + "loggerEnd");
            return hibernateQueryDao.singleColQuery("select lm.emailId from TblLoginMaster lm  where lm.tblUserTypeMaster.userTypeId=8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void contentAdmMsgBox(String toEmailId, String fromEmaild, String subject, String msgText) {
        try {
            LOGGER.debug("contentAdmMailId : " + logUserId + "loggerStart");
            sPCommon.executeProcedure(toEmailId, fromEmaild, "", subject, msgText, "", "Low", "Inbox", "", "Live", 0, "No", "No", "0", 0, 0);
            LOGGER.debug("contentAdmMailId : " + logUserId + "loggerEnd");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Object> getImageIdList() {
        List<Object> images = hibernateQueryDao.singleColQuery("select adId from TblAdvertisement where status='published'");
        return images;
		}
     @Override
    public boolean deleteTblAdvertisement(TblAdvertisement adId) {
        LOGGER.debug("deleteTblAdvertisement : " + logUserId + "loggerStart");
        boolean flag;
        try {
            tblAdvertisementDao.deleteTblAdvertisement(adId);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("deleteTblAdvertisement : " + logUserId + " : " + ex);
            flag = false;
        }
        LOGGER.debug("deleteTblAdvertisement : " + logUserId + "loggerEnd");
        return flag;
    }
}
