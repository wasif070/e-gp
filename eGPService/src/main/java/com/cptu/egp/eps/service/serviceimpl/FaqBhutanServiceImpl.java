/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBhutanDebarmentCommitteeDao;
import com.cptu.egp.eps.dao.daointerface.TblFaqBhutanDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee;
import com.cptu.egp.eps.model.table.TblFaqBhutan;
import com.cptu.egp.eps.service.serviceinterface.FaqBhutanService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Nitish Oritro
 */
public class FaqBhutanServiceImpl implements FaqBhutanService
{
    static final Logger LOGGER = Logger.getLogger(FaqBhutanServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblFaqBhutanDao tblFaqBhutanDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    

    @Override
    public boolean addTblFaqBhutan(TblFaqBhutan tblFaqBhutan) 
    {
        boolean doneFlag = false;
        LOGGER.debug("addTblBhutanDebarmentCommittee: " + logUserId + LOGGERSTART);
            try {
                
            getTblFaqBhutanDao().addTblFaqBhutan(tblFaqBhutan);
            doneFlag = true;
            } catch (Exception e) {
                LOGGER.error("addTblFaqBhutan: " + logUserId + e);
            }
            LOGGER.debug("addTblFaqBhutan: " + logUserId + LOGGEREND);
            return doneFlag;
    }

    @Override
    public void deleteTblFaqBhutan(TblFaqBhutan tblFaqBhutan) {
        LOGGER.debug("deleteTblFaqBhutan: " + logUserId + LOGGERSTART);
            try {
            getTblFaqBhutanDao().deleteTblFaqBhutan(tblFaqBhutan);
            } catch (Exception e) {
                LOGGER.error("deleteTblFaqBhutan: " + logUserId + e);
            }
            LOGGER.debug("deleteTblFaqBhutan: " + logUserId + LOGGEREND);
    }

    @Override
    public String updateTblFaqBhutan(TblFaqBhutan tblFaqBhutan) {
        boolean flag = false;
    LOGGER.debug("updateTblFaqBhutan: " + logUserId + LOGGERSTART);
            try {
            getTblFaqBhutanDao().updateTblFaqBhutan(tblFaqBhutan);
            flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblFaqBhutan: " + logUserId + e);
            }
            LOGGER.debug("updateTblFaqBhutan: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc in FaqBhutan";
            }
            else
            {
                return "failed in Update updateTblFaqBhutan";
            }
    }

    @Override
    public List<TblFaqBhutan> getAllTblFaqBhutan() 
    {
        LOGGER.debug("getAllTblFaqBhutan : " + logUserId + " Starts");
        List<TblFaqBhutan> tblFaqBhutan = null;
        try {
            tblFaqBhutan = getTblFaqBhutanDao().getAllTblFaqBhutan();
        } catch (Exception ex) {
            LOGGER.error("getAllTblFaqBhutan : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Ends");
        return tblFaqBhutan;
    }

    @Override
    public List<TblFaqBhutan> findTblFaqBhutan(int id) {
        LOGGER.debug("getAllTblBhutanDebarmentCommittee : " + logUserId + " Starts");
        List<TblFaqBhutan> tblFaqBhutan = null;
        try {
            tblFaqBhutan = getTblFaqBhutanDao().findTblFaqBhutan("id", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblFaqBhutan : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblFaqBhutan : " + logUserId + " Ends");
        return tblFaqBhutan;
    }
    
    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblFaqBhutanDao
     */
    public TblFaqBhutanDao getTblFaqBhutanDao() {
        return tblFaqBhutanDao;
    }

    /**
     * @param tblFaqBhutanDao the tblFaqBhutanDao to set
     */
    public void setTblFaqBhutanDao(TblFaqBhutanDao tblFaqBhutanDao) {
        this.tblFaqBhutanDao = tblFaqBhutanDao;
    }
    
}
