/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblMessageFolder;
import java.util.List;

/**
 *
 * @author Anil.Yada
 */
public interface MessageBoxService {

    /**
     * it adds the folder details to the database
     * @param tblMessageFolder
     * @param userId
     */
    public void addFolder(TblMessageFolder tblMessageFolder, int userId);
    
    /**
     * gives message folder details from the database
     * @param folderName
     * @param userId
     * @return list of table data
     */
    public List<TblMessageFolder> getMessageFolder(String folderName, int userId);
    
    /**
     * set userId at service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
}
