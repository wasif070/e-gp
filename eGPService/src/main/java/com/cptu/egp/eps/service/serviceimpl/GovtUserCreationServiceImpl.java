/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBudgetTypeDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblDesignationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeFinancialPowerDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeOfficesDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeRolesDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeTrasferDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeMasterDao;
import com.cptu.egp.eps.dao.daointerface.VwEmpfinancePowerDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRoles;
import com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn;
import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblEmployeeFinancialPower;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEmployeeOffices;
import com.cptu.egp.eps.model.table.TblEmployeeRoles;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.view.VwEmpfinancePower;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class GovtUserCreationServiceImpl implements GovtUserCreationService {

    final Logger logger = Logger.getLogger(GovtUserCreationServiceImpl.class);
    private String logUserId = "0";
    private String userId = "0";
    TblLoginMasterDao tblLoginMasterDao;
    TblEmployeeMasterDao tblEmployeeMasterDao;
    TblEmployeeRolesDao tblEmployeeRolesDao;
    TblOfficeMasterDao tblOfficeMasterDao;
    TblDepartmentMasterDao tblDepartmentMasterDao;
    TblDesignationMasterDao tblDesignationMasterDao;
    TblEmployeeOfficesDao tblEmployeeOfficesDao;
    UserRegisterService userRegisterService;
    TblBudgetTypeDao tblBudgetTypeDao;
    TblEmployeeFinancialPowerDao tblEmployeeFinancialPowerDao;
    HibernateQueryDao hibernateQueryDao;
    VwEmpfinancePowerDao vwEmpfinancePowerDao;
    SPGovtEmpRoles sPGovtEmpRoles;
    TblEmployeeTrasferDao tblEmployeeTrasferDao;
    SPAddUser sPAddUser;
    private MakeAuditTrailService makeAuditTrailService;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    private AuditTrail auditTrail;

    public TblEmployeeTrasferDao getTblEmployeeTrasferDao() {
        return tblEmployeeTrasferDao;
    }

    public void setTblEmployeeTrasferDao(TblEmployeeTrasferDao tblEmployeeTrasferDao) {
        this.tblEmployeeTrasferDao = tblEmployeeTrasferDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public TblEmployeeMasterDao getTblEmployeeMasterDao() {
        return tblEmployeeMasterDao;
    }

    public void setTblEmployeeMasterDao(TblEmployeeMasterDao tblEmployeeMasterDao) {
        this.tblEmployeeMasterDao = tblEmployeeMasterDao;
    }

    public TblEmployeeRolesDao getTblEmployeeRolesDao() {
        return tblEmployeeRolesDao;
    }

    public void setTblEmployeeRolesDao(TblEmployeeRolesDao tblEmployeeRolesDao) {
        this.tblEmployeeRolesDao = tblEmployeeRolesDao;
    }

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    public TblOfficeMasterDao getTblOfficeMasterDao() {
        return tblOfficeMasterDao;
    }

    public void setTblOfficeMasterDao(TblOfficeMasterDao tblOfficeMasterDao) {
        this.tblOfficeMasterDao = tblOfficeMasterDao;
    }

    public TblDesignationMasterDao getTblDesignationMasterDao() {
        return tblDesignationMasterDao;
    }

    public void setTblDesignationMasterDao(TblDesignationMasterDao tblDesignationMasterDao) {
        this.tblDesignationMasterDao = tblDesignationMasterDao;
    }

    public TblEmployeeOfficesDao getTblEmployeeOfficesDao() {
        return tblEmployeeOfficesDao;
    }

    public void setTblEmployeeOfficesDao(TblEmployeeOfficesDao tblEmployeeOfficesDao) {
        this.tblEmployeeOfficesDao = tblEmployeeOfficesDao;
    }

    public UserRegisterService getUserRegisterService() {
        return userRegisterService;
    }

    public void setUserRegisterService(UserRegisterService userRegisterService) {
        this.userRegisterService = userRegisterService;
    }

    public TblBudgetTypeDao getTblBudgetTypeDao() {
        return tblBudgetTypeDao;
    }

    public void setTblBudgetTypeDao(TblBudgetTypeDao tblBudgetTypeDao) {
        this.tblBudgetTypeDao = tblBudgetTypeDao;
    }

    public TblEmployeeFinancialPowerDao getTblEmployeeFinancialPowerDao() {
        return tblEmployeeFinancialPowerDao;
    }

    public void setTblEmployeeFinancialPowerDao(TblEmployeeFinancialPowerDao tblEmployeeFinancialPowerDao) {
        this.tblEmployeeFinancialPowerDao = tblEmployeeFinancialPowerDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public VwEmpfinancePowerDao getVwEmpfinancePowerDao() {
        return vwEmpfinancePowerDao;
    }

    public void setVwEmpfinancePowerDao(VwEmpfinancePowerDao vwEmpfinancePowerDao) {
        this.vwEmpfinancePowerDao = vwEmpfinancePowerDao;
    }

    public SPGovtEmpRoles getsPGovtEmpRoles() {
        return sPGovtEmpRoles;
    }

    public void setsPGovtEmpRoles(SPGovtEmpRoles sPGovtEmpRoles) {
        this.sPGovtEmpRoles = sPGovtEmpRoles;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public int createGovtUser(TblLoginMaster tblLM, TblEmployeeMaster tblEmMaster, int oOffice) {
        logger.debug("createGovtUser : " + logUserId + " Starts");
        int id = 0;
        String action = "";
        String query = "";
        try {
            //loginmaster
            query = "INSERT INTO tbl_LoginMaster "
                    + "([emailId],[password],[hintQuestion],[hintAnswer]"
                    + ",[registrationType],[isJvca]"
                    + ",[businessCountryName],[nextScreen]"
                    + ",[userTyperId],[isEmailVerified],[failedAttempt],[validUpTo]"
                    + ",[firstLogin],[isPasswordReset]"
                    + ",[resetPasswordCode],[status],[registeredDate],[nationality])"
                    + "VALUES"
                    + "('" + tblLM.getEmailId() + "','" + tblLM.getPassword() + "','" + tblLM.getHintQuestion() + "','" + tblLM.getHintAnswer() + "','"
                    + tblLM.getRegistrationType() + "','" + tblLM.getIsJvca() + "','"
                    + tblLM.getBusinessCountryName() + "','" + tblLM.getNextScreen() + "','"
                    + tblLM.getTblUserTypeMaster().getUserTypeId() + "','" + tblLM.getIsEmailVerified() + "','"
                    + tblLM.getFailedAttempt() + "'," + tblLM.getValidUpTo() + ",'"
                    + tblLM.getFirstLogin() + "','" + tblLM.getIsPasswordReset() + "','"
                    + tblLM.getResetPasswordCode() + "','" + tblLM.getStatus() + "',GETDATE()"
                    + ",'" + tblLM.getNationality() + "')";
            //tblLoginMasterDao.addTblLoginMaster(tblLM);
            tblEmMaster.setTblLoginMaster(tblLM);
            //tblEmployeeMasterDao.addEmployee(tblEmMaster);
            //employeemaster
            query = query + " DECLARE @userID int; "
                    + "SELECT @userID=userId FROM tbl_LoginMaster "
                    + "WHERE emailId = '" + tblLM.getEmailId() + "';"
                    + "INSERT INTO tbl_EmployeeMaster"
                    + "([employeeName],[employeeNameBangla],[designationId],"
                    + "[mobileNo],[nationalId],[userId],[createdBy],"
                    + "[userEmployeeId],[contactAddress])"
                    + "VALUES"
                    + "('" + tblEmMaster.getEmployeeName() + "','" + tblEmMaster.getEmployeeNameBangla() + "'," + tblEmMaster.getTblDesignationMaster().getDesignationId() + ",'"
                    + tblEmMaster.getMobileNo() + "','" + tblEmMaster.getNationalId() + "',@userID,'"
                    + tblEmMaster.getCreatedBy() + "',"
                    + tblEmMaster.getUserEmployeeId() + ",'" + tblEmMaster.getContactAddress()
                    + "')";
            id = tblEmMaster.getEmployeeId();
            query = query + " DECLARE @employeeID int; "
                    + "SELECT @employeeID=employeeId FROM tbl_EmployeeMaster em "
                    + "INNER JOIN tbl_LoginMaster lm ON em.userId=lm.userId "
                    + "WHERE lm.emailId = '" + tblLM.getEmailId() + "'";

            TblEmployeeOffices tblEmployeeOffices = new TblEmployeeOffices();
//            tblEmployeeOffices.setTblEmployeeMaster(new TblEmployeeMaster(empId));
            tblEmployeeOffices.setTblOfficeMaster(new TblOfficeMaster(oOffice));
//            tblEmployeeOffices.setDesignationId(dId);
//            govtUserCreationService.createEmployeeOffices(tblEmployeeOffices);
            //employeeoffices
            query = query + " INSERT INTO [tbl_EmployeeOffices] "
                    + "([employeeId],[officeId],[designationId])"
                    + "VALUES(@employeeID," + oOffice + "," + tblEmMaster.getTblDesignationMaster().getDesignationId() + ")";
            tblEmployeeOffices = null;
            query = query + " UPDATE tbl_EmployeeMaster SET finPowerBy = 'user' WHERE employeeId=@employeeID";
//            govtUserCreationService.updateEmployeeMaster(empId,"user",dId);
            CommonMsgChk commonMsgChk = null;//createGovtUserRole
            commonMsgChk = sPAddUser.executeProcedure(0, "GovtUserCreation", query).get(0);
            if (commonMsgChk.getFlag() == true) {
                id = Integer.parseInt(commonMsgChk.getMsg());
            }

            action = "Create Government User";
        } catch (Exception ex) {
            logger.error("createGovtUser : " + logUserId + " : " + ex.toString());
            action = "Error in Create Government User: " + ex.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblLM.getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        logger.debug("createGovtUser : " + logUserId + " Ends");
        return id;
    }

    @Override
    public void createGovtUserRole(String query) {
        logger.debug("createGovtUserRole : " + logUserId + " Starts");
        CommonMsgChk commonMsgChk = null;//createGovtUserRole
        try {
        commonMsgChk = sPAddUser.executeProcedure(0, "createGovtUserRole", query).get(0);
//        if (commonMsgChk.getFlag() == true) {
//            //id = Integer.parseInt(commonMsgChk.getMsg());
//        }
        
            //tblEmployeeRolesDao.addEmployeeRole(tblEmployeeRoles);
        } catch (Exception ex) {
            logger.error("createGovtUserRole : " + logUserId + " : " + ex.toString());
        }
        logger.debug("createGovtUserRole : " + logUserId + " Ends");
    }

    @Override
    public List<TblOfficeMaster> officeMasterList(int deptId) {
        logger.debug("officeMasterList : " + logUserId + " Starts");
        List<TblOfficeMaster> officeMasters = null;
        try {
            officeMasters = tblOfficeMasterDao.findTblOfficeMaster("departmentId", Operation_enum.EQ, deptId, "officeName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("officeMasterList : " + logUserId + " : " + ex.toString());
        }
        logger.debug("officeMasterList : " + logUserId + " Ends");
        return officeMasters;
    }

    @Override
    public List<TblDepartmentMaster> departmentMasterList() {
        logger.debug("departmentMasterList : " + logUserId + " Starts");
        List<TblDepartmentMaster> list = null;
        try {
            list = tblDepartmentMasterDao.getAllTblDepartmentMaster();
        } catch (Exception ex) {
            logger.error("departmentMasterList : " + logUserId + " : " + ex.toString());
        }
        logger.debug("departmentMasterList : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<TblDesignationMaster> designationMasterList(short deptId) {
        logger.debug("designationMasterList : " + logUserId + " Starts");
        List<TblDesignationMaster> designationMasters = null;
        try {
            designationMasters = tblDesignationMasterDao.findDesignantion("tblDepartmentMaster", Operation_enum.EQ, new TblDepartmentMaster(deptId), "designationName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("designationMasterList : " + logUserId + " : " + ex.toString());
        }
        logger.debug("designationMasterList : " + logUserId + " Ends");
        return designationMasters;
    }

    @Override
    public void createEmployeeOffices(TblEmployeeOffices tblEmployeeOffices) {
        logger.debug("createEmployeeOffices : " + logUserId + " Starts");
        try {
            tblEmployeeOfficesDao.addTblEmployeeOffices(tblEmployeeOffices);
        } catch (Exception ex) {
            logger.error("createEmployeeOffices : " + logUserId + " : " + ex.toString());
        }
        logger.debug("createEmployeeOffices : " + logUserId + " Ends");
    }

    @Override
    public boolean updateLoginMaster(int userId) {
        logger.debug("updateLoginMaster : " + logUserId + " Starts");
        TblLoginMaster loginMasters;
        boolean flag = false;
        try {
            loginMasters = tblLoginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId).get(0);

            TblLoginMaster master = userRegisterService.cloneTblLoginMaster(loginMasters);
            master.setNextScreen("GovtUserFinanceRole");
            tblLoginMasterDao.updateTblLoginMaster(master);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateLoginMaster : " + logUserId + " : " + ex.toString());
        }
        logger.debug("updateLoginMaster : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean updateEmployeeMaster(int empId, String finPowerBy, int designationId) {
        logger.debug("updateEmployeeMaster : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster1;
        boolean flag = false;
        try {
            employeeMaster1 = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, empId).get(0);
            TblEmployeeMaster master = new TblEmployeeMaster();
            master = employeeMaster1;
            master.setTblDesignationMaster(new TblDesignationMaster(designationId));
            master.setFinPowerBy(finPowerBy);
            tblEmployeeMasterDao.updateEmployee(master);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateEmployeeMaster : " + logUserId + " : " + ex.toString());
        }
        logger.debug("updateEmployeeMaster : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public String getProcurementRole(int empId) {
        logger.debug("getProcurementRole : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster1;
        String role = "";
        try {
            employeeMaster1 = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, empId).get(0);
            if ("user".equalsIgnoreCase(employeeMaster1.getFinPowerBy())) {
                role = "99,User Specific";
            } else {
                Object values[] = {"tblEmployeeMaster", Operation_enum.EQ, new TblEmployeeMaster(empId)};
                List<TblEmployeeRoles> employeeRoleses = tblEmployeeRolesDao.findEmployeeRoles(values);
                for (TblEmployeeRoles tblEmployeeRoles : employeeRoleses) {
                    role += tblEmployeeRoles.getEmployeeRoleId() + "," + tblEmployeeRoles.getTblProcurementRole().getProcurementRole() + ",";
                }
            }
        } catch (Exception ex) {
            logger.error("getProcurementRole : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getProcurementRole : " + logUserId + " Ends");
        return role;
    }

    @Override
    public List<TblBudgetType> getBudgetTypes() {
        logger.debug("getBudgetTypes : " + logUserId + " Starts");
        List<TblBudgetType> list = null;
        try {
            list = tblBudgetTypeDao.getAllTblBudgetType();
        } catch (Exception ex) {
            logger.error("getBudgetTypes : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getBudgetTypes : " + logUserId + " Ends");
        return list;
    }

    @Override
    public void addGovtUserFinancePower(TblEmployeeFinancialPower tblEmployeeFinancialPower) {
        logger.debug("addGovtUserFinancePower : " + logUserId + " Starts");
        try {
            tblEmployeeFinancialPowerDao.addEmployeeFinancialPower(tblEmployeeFinancialPower);
        } catch (Exception ex) {
            logger.error("addGovtUserFinancePower : " + logUserId + " : " + ex.toString());
        }
        logger.debug("addGovtUserFinancePower : " + logUserId + " Ends");
    }

    //TblLoginMaster tblLoginMaster,
    @Override
    public int editGovtUser(TblEmployeeMaster tblEmployeeMaster, int empId, int uId, int dId, int officeId) {
        logger.debug("editGovtUser : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster = new TblEmployeeMaster();
        String action = "";
        String moduleName = "";
        try {
            employeeMaster = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, empId).get(0);
            employeeMaster.setEmployeeName(tblEmployeeMaster.getEmployeeName());
            employeeMaster.setEmployeeNameBangla(tblEmployeeMaster.getEmployeeNameBangla());
            employeeMaster.setNationalId(tblEmployeeMaster.getNationalId());
            employeeMaster.setMobileNo(tblEmployeeMaster.getMobileNo());
            employeeMaster.setUserEmployeeId(tblEmployeeMaster.getUserEmployeeId());
            employeeMaster.setContactAddress(tblEmployeeMaster.getContactAddress());
            employeeMaster.setTblEmployeeFinancialPowers(null);
            employeeMaster.setTblEmployeeOfficeses(null);
            employeeMaster.setTblEmployeeRoleses(null);
            employeeMaster.setTblLoginMaster(employeeMaster.getTblLoginMaster());
            tblEmployeeMasterDao.updateEmployee(employeeMaster);
            hibernateQueryDao.updateDeleteNewQuery("update TblEmployeeOffices set designationId='" + dId + "' where employeeId=" + empId + "and officeId=" + officeId);
            hibernateQueryDao.updateDeleteNewQuery("update TblEmployeeMaster set designationId='" + dId + "' where employeeId=" + empId);
            hibernateQueryDao.updateDeleteNewQuery("update TblEmployeeTrasfer set employeeName='" + tblEmployeeMaster.getEmployeeName() + "',employeeNameBangla='" + tblEmployeeMaster.getEmployeeNameBangla() + "',mobileNo='" + tblEmployeeMaster.getMobileNo() + "',nationalId='" + tblEmployeeMaster.getNationalId() + "',userEmployeeId='" + tblEmployeeMaster.getUserEmployeeId() + "',contactAddress='" + tblEmployeeMaster.getContactAddress() + "' where isCurrent='Yes' and tblLoginMaster.userId=" + uId);
            empId = employeeMaster.getEmployeeId();

            if (logUserId.equalsIgnoreCase(String.valueOf(uId))) {
                action = "Goverment User Edit Profile";
                moduleName = EgpModule.My_Account.getName();

            } else {
                action = "Edit Government User";
                moduleName = EgpModule.Manage_Users.getName();
            }
            employeeMaster = null;
        } catch (Exception ex) {
            logger.error("editGovtUser : " + logUserId + " : " + ex.toString());
            action = "Error in Edit Government User :" + ex.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, uId, "userId", moduleName, action, "");
            action = null;
            auditTrail = null;
            moduleName = null;
        }
        logger.debug("editGovtUser : " + logUserId + " Ends");
        return empId;
    }

    @Override
    public List<Object[]> getGovtUserData(int empId, String... values) {
        logger.debug("getGovtUserData : " + logUserId + " Starts");
        StringBuffer buffer = new StringBuffer();
        List<Object[]> objects = new ArrayList<Object[]>();
        try {
            buffer.append("select tlm.emailId as emailid,tlm.password as password,tem.employeeName as employeeName, ");
            buffer.append(" tem.employeeNameBangla as employeeNameBangla,tem.mobileNo as mobileNo,tem.nationalId as nationalId,tem.tblDesignationMaster.designationId,tem.userEmployeeId as userEmployeeId,tem.contactAddress as contactAddress ");
            buffer.append(" from TblLoginMaster tlm , TblEmployeeMaster tem where tem.employeeId=" + empId + " ");//tlm.userId
            buffer.append(" and  tlm.userId = tem.tblLoginMaster.userId ");
            objects = hibernateQueryDao.createNewQuery(buffer.toString());
        } catch (Exception ex) {
            logger.error("getGovtUserData : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getGovtUserData : " + logUserId + " Ends");
        return objects;
    }

    @Override
    public List<TblEmployeeRoles> getGovtEmpData(int empId) {
        logger.debug("getGovtEmpData : " + logUserId + " Starts");
        List<TblEmployeeRoles> tblEmployeeRoles = null;
        try {
            tblEmployeeRoles = tblEmployeeRolesDao.findEmployeeRoles("tblEmployeeMaster", Operation_enum.EQ, new TblEmployeeMaster(empId), "tblDepartmentMaster", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getGovtEmpData : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getGovtEmpData : " + logUserId + " Ends");
        return tblEmployeeRoles;
    }

    @Override
    public List<TblEmployeeOffices> getGovtEmpOfficeData(int empId) {
        logger.debug("getGovtEmpOfficeData : " + logUserId + " Starts");
        List<TblEmployeeOffices> tblEmployeeOfficeses = null;
        try {
            tblEmployeeOfficeses = tblEmployeeOfficesDao.findTblEmployeeOffices("tblEmployeeMaster", Operation_enum.EQ, new TblEmployeeMaster(empId));
        } catch (Exception ex) {
            logger.error("getGovtEmpOfficeData : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getGovtEmpOfficeData : " + logUserId + " Ends");
        return tblEmployeeOfficeses;
    }

    @Override
    public TblDepartmentMaster getDepartmentMasterData(short deptId) {
        logger.debug("getDepartmentMasterData : " + logUserId + " Starts");
        TblDepartmentMaster tblDepartmentMaster = null;
        try {
            tblDepartmentMaster = tblDepartmentMasterDao.findTblDepartmentMaster("departmentId", Operation_enum.EQ, deptId).get(0);
        } catch (Exception ex) {
            logger.error("getDepartmentMasterData : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getDepartmentMasterData : " + logUserId + " Ends");
        return tblDepartmentMaster;
    }

    @Override
    public boolean deleteEmployeeOffices(String empId) {
        logger.debug("deleteEmployeeOffices : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "delete from TblEmployeeOffices teo where teo.tblEmployeeMaster.employeeId in (" + empId + " )";
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteEmployeeOffices : " + logUserId + " : " + ex.toString());
        }
        logger.debug("deleteEmployeeOffices : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean deleteEmployeeRoles(String empRoleId) {
        logger.debug("deleteEmployeeRoles : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "delete from TblEmployeeRoles ter where ter.employeeRoleId in (" + empRoleId + " )";
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteEmployeeRoles : " + logUserId + " : " + ex.toString());
        }
        logger.debug("deleteEmployeeRoles : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public List<VwEmpfinancePower> getGovtUserFinanceRole(int empId, short deptId) {
        logger.debug("getGovtUserFinanceRole : " + logUserId + " Starts");
        List<VwEmpfinancePower> empfinancePowers = null;
        try {
            empfinancePowers = vwEmpfinancePowerDao.findEmpfinancePower("id.employeeId", Operation_enum.EQ, empId, "id.departmentId", Operation_enum.EQ, deptId);
        } catch (Exception ex) {
            logger.error("getGovtUserFinanceRole : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getGovtUserFinanceRole : " + logUserId + " Ends");
        return empfinancePowers;
    }

    @Override
    public boolean deleteEmployeeFinanceRole(String empRoleId) {
        logger.debug("deleteEmployeeFinanceRole : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "delete from TblEmployeeFinancialPower tef where tef.employeeRoleId in (" + empRoleId + " )";
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteEmployeeFinanceRole : " + logUserId + " : " + ex.toString());
        }
        logger.debug("deleteEmployeeFinanceRole : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public String verifyNationalId(String nationalId) {
        logger.debug("verifyNationalId : " + logUserId + " Starts");
        short size = 0;
        String str = "";
        try {
            size = (short) tblEmployeeMasterDao.findEmployee("nationalId", Operation_enum.LIKE, nationalId).size();
            if (size > 0) {
                str = "National-ID already exists";
            } else {
                str = "OK";
            }
        } catch (Exception ex) {
            logger.error("verifyNationalId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("verifyNationalId : " + logUserId + " Ends");
        return str;
    }

    @Override
    public String verifyMobileNo(String mobileNo) {
        logger.debug("verifyMobileNo : " + logUserId + " Starts");
        short size = 0;
        String str = "";
        try {
            size = (short) tblEmployeeMasterDao.findEmployee("mobileNo", Operation_enum.LIKE, mobileNo).size();
            if (size > 0) {
                str = "MobileNo already exists";
            } else {
                str = "OK";
            }
        } catch (Exception ex) {
            logger.error("verifyMobileNo : " + logUserId + " : " + ex.toString());
        }
        logger.debug("verifyMobileNo : " + logUserId + " Ends");
        return str;
    }

    @Override
    public String getDesignationName(int designationId) {
        logger.debug("getDesignationName : " + logUserId + " Starts");
        TblDesignationMaster designationMasters = null;
        String str = null;
        try {
            designationMasters = tblDesignationMasterDao.findDesignantion("designationId", Operation_enum.EQ, designationId).get(0);
            str = designationMasters.getDesignationName();
        } catch (Exception ex) {
            logger.error("getDesignationName : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getDesignationName : " + logUserId + " Ends");
        return str;
    }

    @Override
    public int getUserIdFromEmpId(int empId) {
        logger.debug("getUserIdFromEmpId : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster = null;
        int userId = 0;
        try {
            employeeMaster = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, empId).get(0);
            userId = employeeMaster.getTblLoginMaster().getUserId();
        } catch (Exception ex) {
            logger.error("getUserIdFromEmpId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getUserIdFromEmpId : " + logUserId + " Ends");
        return userId;
    }

    @Override
    public int getEmpIdFromUserId(int userId) {
        logger.debug("getEmpIdFromUserId : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster = null;
        int empId = 0;
        try {
            employeeMaster = tblEmployeeMasterDao.findEmployee("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)).get(0);
            empId = employeeMaster.getEmployeeId();
        } catch (Exception ex) {
            logger.error("getEmpIdFromUserId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getEmpIdFromUserId : " + logUserId + " Ends");
        return empId;
    }

    @Override
    public List<SPGovtEmpRolesReturn> getGovtEmpBySP(int empId) throws Exception {
        logger.debug("getGovtEmpBySP : " + logUserId + " Starts");
        List<SPGovtEmpRolesReturn> list = null;
        try {
            list = sPGovtEmpRoles.executeGovtEmpProcedure(empId);
        } catch (Exception ex) {
            logger.error("getGovtEmpBySP : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getGovtEmpBySP : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean deleteEmployeeUserWiseFinancePower(int empId) {
        logger.debug("deleteEmployeeUserWiseFinancePower : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "delete from TblEmployeeFinancialPower tef where tef.tblEmployeeMaster.employeeId =" + empId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteEmployeeUserWiseFinancePower : " + logUserId + " : " + ex.toString());
        }
        logger.debug("deleteEmployeeUserWiseFinancePower : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean loginMasterUpdate(int userId, String password) throws Exception {
        logger.debug("loginMasterUpdate : " + logUserId + " Starts");
        boolean flag = false;
        String action = "";
        try {
            String query = "update TblLoginMaster tlm set tlm.status = 'approved',tlm.password = '" + password + "'  where tlm.userId =" + userId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
            action = "Complete Government User Registration";
        } catch (Exception ex) {
            logger.error("loginMasterUpdate : " + logUserId + " : " + ex.toString());
            action = "Error in Complete Goverment User Registration :" + ex.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        logger.debug("loginMasterUpdate : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public int getUserTypeWiseDepartmentId(int userId, int userTypeId) throws Exception {
        logger.debug("getUserTypeWiseDepartmentId : " + logUserId + " Starts");
        int deptId = 0;
        try {
            String query = "";
            if (userTypeId == 4) {
                query = "select om.departmentId,om.officeId from TblOfficeAdmin oa , TblOfficeMaster om where oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId=" + userId;
                Object[] deptIdList = hibernateQueryDao.createNewQuery(query).get(0);
                deptId = (Integer) deptIdList[0];
                deptIdList = null;
            }
            if (userTypeId == 5) {
                query = "select tdm.departmentId,tdm.departmentName from TblDepartmentMaster tdm where tdm.approvingAuthorityId =" + userId;
                Object[] deptIdList = hibernateQueryDao.createNewQuery(query).get(0);
                deptId = (Short) deptIdList[0];
                deptIdList = null;

            }
        } catch (Exception ex) {
            logger.error("getUserTypeWiseDepartmentId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getUserTypeWiseDepartmentId : " + logUserId + " Ends");
        return deptId;
    }

    @Override
    public boolean isEmployeeDataAvailableInRole(int empId) throws Exception {
        logger.debug("isEmployeeDataAvailableInRole : " + logUserId + " Starts");
        List<TblEmployeeRoles> employeeRoleses;
        boolean flag = false;
        try {
            employeeRoleses = tblEmployeeRolesDao.findEmployeeRoles("tblEmployeeMaster", Operation_enum.EQ, new TblEmployeeMaster(empId));
            if (employeeRoleses.size() > 0) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("isEmployeeDataAvailableInRole : " + logUserId + " : " + ex.toString());
        }
        logger.debug("isEmployeeDataAvailableInRole : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean isDataAvailableForHopeForDeptId(short deptId) {
        logger.debug("isDataAvailableForHopeForDeptId : " + logUserId + " Starts");
        boolean flag = false;
        TblEmployeeRoles empfinanceRoles = null;
        try {
            empfinanceRoles = tblEmployeeRolesDao.findEmployeeRoles("tblDepartmentMaster", Operation_enum.EQ, new TblDepartmentMaster(deptId), "tblProcurementRole", Operation_enum.EQ, new TblProcurementRole((byte) 6)).get(0);
            if (empfinanceRoles.getEmployeeRoleId() > 0) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("isDataAvailableForHopeForDeptId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("isDataAvailableForHopeForDeptId : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean isDataAvailableForPEForOfficeId(String officeId) {
        logger.debug("isDataAvailableForPEForOfficeId : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "select ter.employeeRoleId,ter.tblEmployeeMaster.employeeId from TblEmployeeRoles ter  where ter.tblEmployeeMaster.employeeId in "
                    + " (select teo.tblEmployeeMaster.employeeId from TblEmployeeOffices teo where teo.tblOfficeMaster.officeId "
                    + " in ( " + officeId + " )) and ter.tblProcurementRole.procurementRoleId = 1";
            List<Object[]> data = hibernateQueryDao.createNewQuery(query);
            if (data.size() > 0) {
                flag = true;
            }
            data = null;
        } catch (Exception ex) {
            logger.error("isDataAvailableForPEForOfficeId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("isDataAvailableForPEForOfficeId : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public long countForQuery(String from, String where) throws Exception {
        logger.debug("countForQuery : " + logUserId + " Starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery(from, where);
        } catch (Exception ex) {
            logger.error("countForQuery : " + logUserId + " : " + ex.toString());
        }
        logger.debug("countForQuery : " + logUserId + " Ends");
        return lng;
    }

    @Override
    public List<Object[]> getOfficeDetailsforMail(int userId) {
        logger.debug("getOfficeDetailsforMail : " + logUserId + " Starts");
        StringBuilder sqlQuery = new StringBuilder();
        List<Object[]> listObject = new ArrayList<Object[]>();
        try {
            sqlQuery.append("select e.employeeName,om.officeName,om.address,om.city,om.postCode,dm.departmentName,sm.stateName "
                    + "from TblEmployeeMaster e, TblEmployeeOffices eo, TblDepartmentMaster dm, TblDesignationMaster dsm,TblOfficeMaster om,TblStateMaster sm "
                    + "where e.employeeId=eo.tblEmployeeMaster.employeeId and "
                    + "dm.departmentId=dsm.tblDepartmentMaster.departmentId and "
                    + "e.tblDesignationMaster.designationId=dsm.designationId and "
                    + "dm.departmentId= dsm.tblDepartmentMaster.departmentId and e.tblLoginMaster.userId=" + userId
                    + " and om.officeId=eo.tblOfficeMaster.officeId and om.tblStateMaster.stateId=sm.stateId");
            listObject = hibernateQueryDao.createQuery(sqlQuery.toString());
        } catch (Exception ex) {
            logger.error("getOfficeDetailsforMail : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getOfficeDetailsforMail : " + logUserId + " Ends");
        return listObject;
    }

    @Override
    public Object[] getPEOfficeDetailsforMail(int userId) {
        logger.debug("getPEOfficeDetailsforMail : " + logUserId + " Starts");
        StringBuilder sqlQuery = new StringBuilder();
        List<Object[]> listObject = null;
        Object[] obj = null;

        try {
            sqlQuery.append("select am.fullName,dm.departmentName, om.officeName,om.address,om.city,om.postCode,sm.stateName "
                    + "from TblLoginMaster l,TblOfficeAdmin oa, TblOfficeMaster om,TblAdminMaster am,TblDepartmentMaster dm, TblStateMaster sm "
                    + "where oa.tblLoginMaster.userId=l.userId and oa.tblLoginMaster.userId= " + userId
                    + " and oa.tblOfficeMaster.officeId=om.officeId and am.tblLoginMaster.userId=l.userId "
                    + "and om.departmentId=dm.departmentId and om.tblStateMaster.stateId=sm.stateId");

            listObject = hibernateQueryDao.createQuery(sqlQuery.toString());

            if (listObject.size() > 0) {
                obj = listObject.get(0);
            }
        } catch (Exception ex) {
            logger.error("getPEOfficeDetailsforMail : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getPEOfficeDetailsforMail : " + logUserId + " Ends");
        return obj;
    }

    //Get PE Admin office details for E-Mail.Edited by palash for PE Admin Transfer, Dohatec
    @Override
    public List<Object[]> getPEAdminOfficeDetailsforMail(int userId) {
        logger.debug("getPEAdminOfficeDetailsforMail : " + logUserId + " Starts");
        StringBuilder sqlQuery = new StringBuilder();
        List<Object[]> listObject = new ArrayList<Object[]>();
        //Object[] obj = null;

        try {
            sqlQuery.append("select am.fullName,dm.departmentName, om.officeName,om.address,om.city,om.postCode,sm.stateName "
                    + "from TblLoginMaster l,TblOfficeAdmin oa, TblOfficeMaster om,TblAdminMaster am,TblDepartmentMaster dm, TblStateMaster sm "
                    + "where oa.tblLoginMaster.userId=l.userId and oa.tblLoginMaster.userId= " + userId
                    + " and oa.tblOfficeMaster.officeId=om.officeId and am.tblLoginMaster.userId=l.userId "
                    + "and om.departmentId=dm.departmentId and om.tblStateMaster.stateId=sm.stateId");

            listObject = hibernateQueryDao.createQuery(sqlQuery.toString());

        } catch (Exception ex) {
            logger.error("getPEAdminOfficeDetailsforMail : " + logUserId + " : " + ex.toString());
        }
        logger.debug("getPEAdminOfficeDetailsforMail : " + logUserId + " Ends");
        return listObject;
    }
//End by Palash

    @Override
    public List<Object[]> officeMasterForPe(int userId) {
        logger.debug("officeMasterForPe : " + logUserId + " Starts");
        List<Object[]> officeList = null;
        try {
            officeList = hibernateQueryDao.createNewQuery("select tom.officeId, tom.officeName from TblOfficeMaster as tom where tom.officeId in (select toa.tblOfficeMaster.officeId  from TblOfficeAdmin as toa where toa.tblLoginMaster.userId=" + userId + ")");
        } catch (Exception ex) {
            logger.error("officeMasterForPe : " + logUserId + " : " + ex.toString());
        }
        logger.debug("officeMasterForPe : " + logUserId + " Ends");
        return officeList;
    }

    @Override
    public List<Object[]> dptIdForPe(int userId) {
        logger.debug("officeMasterForPe : " + logUserId + " Starts");
        List<Object[]> officeList = null;
        try {
            officeList = hibernateQueryDao.createNewQuery("select tom.officeId, tom.departmentId from TblOfficeMaster as tom where tom.officeId in (select toa.tblOfficeMaster.officeId  from TblOfficeAdmin as toa where toa.tblLoginMaster.userId=" + userId + ")");
        } catch (Exception ex) {
            logger.error("officeMasterForPe : " + logUserId + " : " + ex.toString());
        }
        logger.debug("officeMasterForPe : " + logUserId + " Ends");
        return officeList;
    }

    @Override
    public String employeeStatus(int employeeId) {
        logger.debug("employeeStatus : " + logUserId + " Starts");
        List<Object[]> list = null;
        String status = "";
        try {
            list = hibernateQueryDao.createNewQuery("select lm.status from TblLoginMaster as lm where lm.userId in (select em.tblLoginMaster.userId from TblEmployeeMaster as em where em.employeeId=" + employeeId + ")");
            Object obj = list.get(0);
            status = obj.toString();
        } catch (Exception ex) {
            logger.error("employeeStatus : " + logUserId + " : " + ex.toString());
            status = "Error";
        }
        logger.debug("employeeStatus : " + logUserId + " Ends");
        return status;
    }

    @Override
    public boolean updateMailId(String mailId, int userId) {
        logger.debug("updateMailId : " + logUserId + " Starts");
        boolean flag = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set emailId = '" + mailId + "' where userId=(select tblLoginMaster.userId from TblEmployeeMaster where employeeId=" + userId + ")");
            flag = true;
        } catch (Exception ex) {
            logger.error("updateMailId : " + logUserId + " : " + ex.toString());
        }
        logger.debug("updateMailId : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public List<TblEmployeeMaster> getDetailsForTransfer(int userId) {
        logger.debug("getDetailsForTransfer : " + logUserId + " Starts");
        List<TblEmployeeMaster> list = null;
        try {
            list = tblEmployeeMasterDao.findEmployee("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
        } catch (Exception e) {
            logger.error("getDetailsForTransfer : " + logUserId + e);
        }
        logger.debug("getDetailsForTransfer : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<TblEmployeeMaster> getUserIdForUpdate(int eId) {
        logger.debug("getUserIdForUpdate : " + logUserId + " Starts");
        List<TblEmployeeMaster> list = null;
        try {
            list = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, eId);
        } catch (Exception e) {
            logger.error("getUserIdForUpdate : " + logUserId + e);
        }
        logger.debug("getUserIdForUpdate : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<TblEmployeeTrasfer> getUserHistory(int userId) {
        logger.debug("getUserHistory : " + logUserId + " Starts");
        List<TblEmployeeTrasfer> list = null;
        try {
            list = tblEmployeeTrasferDao.findTblEmployeeTrasfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId), "transferDt", Operation_enum.ORDERBY, Operation_enum.DESC);
        } catch (Exception e) {
            logger.error("getUserHistory : " + logUserId + e);
        }
        logger.debug("getUserHistory : " + logUserId + " Ends");
        return list;

    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * @return the sPAddUser
     */
    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    /**
     * @param sPAddUser the sPAddUser to set
     */
    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }

}
