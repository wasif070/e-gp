/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblToDoListDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblToDoList;
import com.cptu.egp.eps.service.serviceinterface.AddTaskService;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public class AddTaskServiceImpl implements AddTaskService {

    TblToDoListDao tblToDoListDao;
    final Logger logger = Logger.getLogger(AddTaskServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblToDoListDao getTblToDoListDao() {
        return tblToDoListDao;
    }

    public void setTblToDoListDao(TblToDoListDao tblToDoListDao) {
        this.tblToDoListDao = tblToDoListDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void addAddTask(TblToDoList tblToDoList, int userId, String status) {
        logger.debug("addAddTask : " + logUserId + loggerStart);
        String action="";
        try {
         tblToDoList.setTblLoginMaster(new TblLoginMaster(userId));
         tblToDoList.setIsComplete(status);
         action="Add Task";
        tblToDoListDao.addTblToDoList(tblToDoList);
        } catch (Exception e) {
             action="Add Task : "+e;
            logger.error("addAddTask : " + logUserId + e);
    }
    finally{
        makeAuditTrailService.generateAudit(auditTrail, userId, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("addAddTask : " + logUserId + loggerEnd);
    }

    @Override
    public List<TblToDoList> getTaskBrief(int userId,String status) {
        logger.debug("getTaskBrief : " + logUserId + loggerStart);
        List<TblToDoList> data = null;
        try {
            if (status.equals("Pending")) {
                status = "No";
                data = tblToDoListDao.findEntity("isComplete", Operation_enum.EQ, status,"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
            }else if (status.equals("Completed")) {
                 status = "Yes";
                data = tblToDoListDao.findEntity("isComplete", Operation_enum.EQ, status,"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
            }
            } catch (Exception e) {
            logger.error("getTaskBrief : " + logUserId + e);
        }
        logger.debug("getTaskBrief : " + logUserId + loggerEnd);
     return data;
    }

   @Override
    public List<TblToDoList> searchTasks(int offcet,int rows,Date startDate, Date endDate, String status, int userId) {
     //Bussiness Logic for Search Tasks
        logger.debug("searchTasks : " + logUserId + loggerStart);
       List<TblToDoList> l = null;
        try {
            if (status.equals("Pending")) {
                status = "No";
                if (startDate != null && endDate != null) {
                    l = tblToDoListDao.findByCountTblToDoList(offcet, rows,"startDate", Operation_enum.GE, startDate, "startDate", Operation_enum.LE, endDate, "isComplete", Operation_enum.EQ, status, "tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
                } else {
                    l = tblToDoListDao.findByCountTblToDoList(offcet, rows, "isComplete", Operation_enum.EQ, status, "tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
                }

            } else if (status.equals("Completed")) {
                status = "Yes";
                if (startDate != null && endDate != null) {
                    l = tblToDoListDao.findByCountTblToDoList(offcet, rows,"startDate", Operation_enum.GE, startDate, "startDate", Operation_enum.LE, endDate, "isComplete", Operation_enum.EQ, status, "tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
                } else {
                    l = tblToDoListDao.findByCountTblToDoList(offcet, rows,"isComplete", Operation_enum.EQ, status, "tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
                  }
                    }

        } catch (Exception e) {
            logger.error("searchTasks : " + logUserId + e);
                  }
        logger.debug("searchTasks : " + logUserId + loggerEnd);
                      return l;

                  }

    public List<TblToDoList> getTasksBytaskId(int taskid) {
        logger.debug("getTasksBytaskId : " + logUserId + loggerStart);
        List<TblToDoList> todoList = null;
        try {
            todoList = tblToDoListDao.findTblToDoList("taskId", Operation_enum.EQ, taskid);

        } catch (Exception ex) {
            logger.error("getTasksBytaskId : " + logUserId + ex);
        }
        logger.debug("getTasksBytaskId : " + logUserId + loggerEnd);
           return todoList;
   }

    public void updateTask(TblToDoList tblToDoList) {
        logger.debug("updateTask : " + logUserId + loggerStart);
        String action="";
        try {
       tblToDoListDao.updateTblToDoList(tblToDoList);
       action = "Complete Task";
        } catch (Exception e) {
            action = "Complete Task : "+e;
            logger.error("updateTask : " + logUserId + e);
        }
        finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Message_Box.getName(), action, "");
            action=null;
        }
        logger.debug("updateTask : " + logUserId + loggerEnd);
    }

   public List getPendingToDoList(int uid) {
        logger.debug("getPendingToDoList : " + logUserId + loggerStart);
        List list;
        Calendar cal = Calendar.getInstance();
        Date dt = new Date();
        cal.setTime(dt);
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        dt = cal.getTime();
        try {            
            list = tblToDoListDao.findTblToDoList("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(uid),"startDate", Operation_enum.LE, new Date(), "endDate", Operation_enum.GE, dt,"isComplete", Operation_enum.EQ, "No");
        } catch (Exception ex) {
            list = null;
            logger.error("getPendingToDoList : " + logUserId + ex);
        }
        logger.debug("getPendingToDoList : " + logUserId + loggerEnd);
        return list;
    }
}
