/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface ManageAdminService {

    /**
     * fetching data for particular userId
     * @param userId
     * @param userTypeid
     * @return Object with data
     */
    public List<Object[]> getManageAdminDetail(int userId, byte userTypeid);

    /**
     * fetching data for Organization Admin
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param orderClause
     * @return Object with data for org admin
     */
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause);

    /**
     * fetching data for Organization Admin (Search)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param userId
     * @param searchString
     * @return Object with data for org admin
     */
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, int userId, String searchString);

    /**
     * fetching data from userid (Search)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, int userId, String searchField, String searchString, String searchOper);

    /**
     * fetching data for Content Admin (Search)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * fetching data for Content Admin (order by)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param orderClause
     * @return Object with data
     */
    public List<Object[]> getContentAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause);

    /**
     * fetching data for Content Admin (Search)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<Object[]> getContentAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * fetching data for PE Admin (order by)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param orderClause
     * @return Object with data
     */
    public List<Object[]> getPEAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause);

    /**
     * fetching data for PE Admin (Search)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<Object[]> getPEAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * for counting records for Org
     * @param userTypeid
     * @return number of records
     */
    public long getAllCountOfOrganization(byte userTypeid);

    /**
     * for counting records for Org
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfOrganization(byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * get count for org
     * @param userTypeid
     * @param userId of tbl_LoginMaster
     * @return number of records
     */
    public long getAllCountOfOrganization(byte userTypeid, int userId);

    /**
     * for counting records for Content
     * @param userTypeid
     * @return number of records
     */
    public long getAllCountOfContent(byte userTypeid);

    /**
     * for counting records for content
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfContent(byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * for counting records for PE
     * @param userTypeid
     * @return number of records
     */
    public long getAllCountOfPE(byte userTypeid);

    /**
     * for counting records for PE
     * @param userTypeid
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfPE(byte userTypeid, String searchField, String searchString, String searchOper);

    /**
     * deleting data particular userId
     * @param query
     * @return 0 incase not able to delete
     */
    public int updateDeleteNewQuery(String query);

    /**
     * fetching data for particular userId
     * @param userTypeid
     * @param userId
     * @return Object with data
     */
    public List<Object[]> getManageAdminContentDetail(int userId, byte userTypeid);
    /**
     * set list
     * @param auditTrail list to be set
     */
    public void setAuditTrail(AuditTrail auditTrail);

    /**
     * get office list
     * @param userId
     * @param userTypeid
     * @return List of offices
     */
    public List<Object> getManagePEAdminDetailOfficeList(int userId, byte userTypeid);

    /**
     * get pe admin details
     * @param userId 
     * @param userTypeid
     * @return List of object
     */
    public List<Object[]> getManagePEAdminDetail(int userId, byte userTypeid);

    /**
     * updating data in tbl_AdminMaster
     * @param tblAdminMaster
     * @return true if updated else false
     */
    public boolean updateAdminMaster(TblAdminMaster tblAdminMaster,String... actionAudit);

    /**
     * updating data in tbl_AdminMaster
     * @param tblAdminMaster
     * @return true if updated else false
     */
    public boolean updatePEAdminMaster(/*TblAdminMaster tblAdminMaster,*/String query, int userID,String... actionAudit);

    /**
     * updating data in tbl_OfficeAdmin
     * @param tblOfficeAdmin
     * @return true if updated else false
     */
    public boolean updateOfficeList(TblOfficeAdmin tblOfficeAdmin);

    /**
     * fetching data for PE admin for particular userId
     * @param userId
     * @return data for userId
     */
    public List<Object[]> getOfficeList(int userId);

    /**
     * fetch office list for particular userId
     * @param userId
     * @return list of offices
     */
    public List<Object[]> getOfficeAllList(int userId);

    /**
     * count records
     * @param userId
     * @param userTypeId
     * @return number of data
     */
    public long getOrganizationAdminGridCount(String userId, String userTypeId);

    /**
     * get count for org admin
     * @param userId
     * @param userTypeId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getOrganizationAdminGridCount(String userId, String userTypeId, String searchField, String searchString, String searchOper);

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
        /**
     * fetching data for Content Admin (order by)
     * @param firstResult
     * @param maxResult
     * @param userTypeid
     * @param orderClause
     * @return Object with data
     */
    public List<Object[]> getOAndMAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause);

}
