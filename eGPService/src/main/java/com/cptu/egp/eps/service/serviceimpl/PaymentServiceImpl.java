/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.model.table.TblPaymentActionRequest;
import com.cptu.egp.eps.service.serviceinterface.*;
import java.util.Date;
import com.cptu.egp.eps.model.table.TblPaymentExtensionActionRequest;
import java.text.SimpleDateFormat;

/**
 *
 * @author shreyansh.shah
 */
public class PaymentServiceImpl implements PaymentService {

    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public boolean addPEPaymentReleaseRequest(TblPaymentActionRequest tblPaymentActionRequest) {
        int count;
        boolean successType;
        try {
            hibernateQueryDao.addEntity(tblPaymentActionRequest);
            successType = true;
        } catch (Exception e) {
            successType = false;
        }
        return successType;

    }
    /**Added By Dohatec for resolving issue 8715 starts **/
    /*
     *
     *
     **/
        public boolean addPEPaymentExtendRequest(TblPaymentExtensionActionRequest tblPaymentExtensionActionRequest) {
        boolean successType;
        try {
            hibernateQueryDao.addEntity(tblPaymentExtensionActionRequest);
            successType = true;
        } catch (Exception e) {
            System.out.println("addPEPaymentExtendRequest Error: "+ e.getMessage());
            successType = false;
        }
        return successType;

    }
         public boolean updatePaymentExtendRequest(String extendRequestID,String ActionComments,String RequestID){
        boolean successType;
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
        Date CurrentDate = new Date();
        try {
            String sqlQueryString="update TblPaymentExtensionActionRequest pear set pear.actionStatus='completed',";
            sqlQueryString = sqlQueryString + " pear.actionCompDt='"+sd.format(CurrentDate)+"',";
            sqlQueryString = sqlQueryString + " pear.actionComments='"+ ActionComments +"' ";
            sqlQueryString = sqlQueryString  +" where pear.actionStatus ='Pending' and pear.paymentId="+extendRequestID;
            sqlQueryString = sqlQueryString  +" and pear.requestId=" + RequestID;
             hibernateQueryDao.updateDeleteNewQuery(sqlQueryString);
            successType = true;
        } catch (Exception e) {
            System.out.println("addPEPaymentExtendRequest Error: "+ e.getMessage());
            successType = false;
        }
        return successType;
         }

         public boolean updateTenderPaymentExtendRequest(String PaymentID,String NewValidityDate,String userId){
        boolean successType;
        SimpleDateFormat sd = new SimpleDateFormat("dd-MMMM-yyyy, HH:mm:ss");
        try {
             String sqlQueryString1="update TblPaymentExtensionActionRequest pear set pear.actionStatus='Not Extended by System' ";
            sqlQueryString1 = sqlQueryString1  +" where pear.actionStatus ='Pending' and pear.paymentId="+PaymentID;
            sqlQueryString1 = sqlQueryString1  +" and pear.requestTo="+userId;
            hibernateQueryDao.updateDeleteNewQuery(sqlQueryString1);
            
            Date Validitydate = sd.parse(NewValidityDate +" 23:59:00");
             sd = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");
            String sqlQueryString="update TblTenderPayment tp set tp.instValidUpto='"+sd.format(Validitydate)+"'";
            sqlQueryString = sqlQueryString  +" where tp.paymentFor ='Performance Security' and tp.tenderPaymentId="+PaymentID;
             hibernateQueryDao.updateDeleteNewQuery(sqlQueryString);
            successType = true;
        } catch (Exception e) {
            System.out.println("addPEPaymentExtendRequest Error: "+ e.getMessage());
            successType = false;
        }
        return successType;
         }
    /**Added By Dohatec for resolving issue 8715 ends **/
}
