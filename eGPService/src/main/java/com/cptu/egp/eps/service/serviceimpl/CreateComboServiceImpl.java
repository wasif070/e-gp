/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblListBoxDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblListBoxMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblListBoxDetail;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateComboService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class CreateComboServiceImpl implements CreateComboService {

    private static final Logger LOGGER = Logger.getLogger(CreateComboServiceImpl.class);
    private TblListBoxMasterDao tblListBoxMasterDao;
    private TblListBoxDetailDao tblListBoxDetailDao;
    private HibernateQueryDao hibernateQueryDao;
    private static final String TEMPLATEFORMID = "templateFormId";
    private static final String LISTBOXNAME = "listBoxName";
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public TblListBoxDetailDao getTblListBoxDetailDao() {
        return tblListBoxDetailDao;
    }

    public void setTblListBoxDetailDao(TblListBoxDetailDao tblListBoxDetailDao) {
        this.tblListBoxDetailDao = tblListBoxDetailDao;
    }

    public TblListBoxMasterDao getTblListBoxMasterDao() {
        return tblListBoxMasterDao;
    }

    public void setTblListBoxMasterDao(TblListBoxMasterDao tblListBoxMasterDao) {
        this.tblListBoxMasterDao = tblListBoxMasterDao;
    }

    @Override
    public boolean addComboNameDetails(TblListBoxMaster tbllistboxmaster, List<TblListBoxDetail> tbllistboxdetail) {
        LOGGER.debug("addComboNameDetails : " + logUserId + STARTS);

        boolean flag = false;
        try {
            tblListBoxMasterDao.addTblListBoxMaster(tbllistboxmaster);
            List<TblListBoxDetail> list = new ArrayList<TblListBoxDetail>();
            for (TblListBoxDetail tblListBoxDetail : tbllistboxdetail) {
                tblListBoxDetail.setTblListBoxMaster(tbllistboxmaster);
                list.add(tblListBoxDetail);
            }
            tblListBoxDetailDao.updateOrSaveAllListBoxDetail(list);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("addComboNameDetails : " + logUserId + " " + e);

            flag = false;
        }
        LOGGER.debug("addComboNameDetails : " + logUserId + ENDS);
        return flag;
    }

    @Override
    public long getComboCount(int templetformid) {
        LOGGER.debug("GetComboCount : " + logUserId + STARTS);
        long cnt = 0;
        try {
            cnt = tblListBoxMasterDao.countForQuery("TblListBoxMaster", "TEMPLATEFORMID = '" + templetformid + "'");
        } catch (Exception ex) {
            LOGGER.error("GetComboCount : " + logUserId + " " + ex);
        }
        LOGGER.debug("GetComboCount : " + logUserId + ENDS);
        return cnt;
    }

    @Override
    public List<TblListBoxMaster> getListBoxMaster(int formId) {
        LOGGER.debug("getListBoxMaster : " + logUserId + STARTS);

        List<TblListBoxMaster> list = null;
        try {
            list = tblListBoxMasterDao.findTblListBoxMaster(TEMPLATEFORMID, Operation_enum.EQ, formId);
        } catch (Exception ex) {
            LOGGER.error("getListBoxMaster : " + logUserId + " " + ex);
        }
        LOGGER.debug("getListBoxMaster : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblListBoxDetail> getListBoxDetail(int id) {
        LOGGER.debug("getListBoxDetail : " + logUserId + STARTS);
        List<TblListBoxDetail> list = null;
        try {
            list = tblListBoxDetailDao.findTblListBoxDetail("tblListBoxMaster", Operation_enum.EQ, new TblListBoxMaster(id));
        } catch (Exception ex) {
            LOGGER.error("getListBoxDetail : " + logUserId + " " + ex);
        }
        LOGGER.debug("getListBoxDetail : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblListBoxMaster> getComboName(int offcet, int row, int templetformid, String orderby, String colName) {
        LOGGER.debug("getComboName : " + logUserId + STARTS);
        List<TblListBoxMaster> list = null;
        try {
            if (orderby == null && colName == null) {
                list = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, TEMPLATEFORMID, Operation_enum.EQ, templetformid);
            } else {
                Object data = null;
                if ("asc".equals(orderby)) {
                    data = Operation_enum.ASC;
                } else {
                    data = Operation_enum.DESC;
                }
                list = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, TEMPLATEFORMID, Operation_enum.EQ, templetformid, colName, Operation_enum.ORDERBY, data);
            }
        } catch (Exception ex) {
            LOGGER.error("getComboName : " + logUserId + " " + ex);
        }
        LOGGER.debug("getComboName : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblListBoxMaster> searchComboName(int offcet, int row, int templetformid, String searchField, String searchString, String searchOper, String orderby, String colName) {
        LOGGER.debug("searchComboName : " + logUserId + STARTS);
            List<TblListBoxMaster> searchcomboname = null;
        try {
            if ("eq".equalsIgnoreCase(searchOper)) {
                if ("asc".equalsIgnoreCase(orderby)) {
                    searchcomboname = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, LISTBOXNAME, Operation_enum.EQ, searchString, TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.ASC);
                } else {
                    searchcomboname = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, LISTBOXNAME, Operation_enum.EQ, searchString, TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.DESC);
            }
            } else if ("cn".equalsIgnoreCase(searchOper)) {
                if ("asc".equalsIgnoreCase(orderby)) {
                    searchcomboname = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, LISTBOXNAME, Operation_enum.LIKE, "%" + searchString + "%", TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.ASC);
                } else {
                    searchcomboname = tblListBoxMasterDao.findByCountTblListBoxMaster(offcet, row, LISTBOXNAME, Operation_enum.LIKE, "%" + searchString + "%", TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.DESC);
                }
            }
        } catch (Exception e) {
            LOGGER.error("searchComboName : " + logUserId + " " + e);
        }
        LOGGER.debug("searchComboName : " + logUserId + ENDS);
                return searchcomboname;
        }

    @Override
    public long getSearchCount(int templetformid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCount : " + logUserId + STARTS);
        long cnt = 0;
        try {
            List<TblListBoxMaster> getsearch = null;
            if ("eq".equalsIgnoreCase(searchOper)) {
                getsearch = tblListBoxMasterDao.findTblListBoxMaster(LISTBOXNAME, Operation_enum.EQ, searchString, TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.ASC);
            } else if ("cn".equalsIgnoreCase(searchOper)) {
                getsearch = tblListBoxMasterDao.findTblListBoxMaster(LISTBOXNAME, Operation_enum.LIKE, "%" + searchString + "%", TEMPLATEFORMID, Operation_enum.EQ, templetformid, LISTBOXNAME, Operation_enum.ORDERBY, Operation_enum.ASC);
            }
            if (!getsearch.isEmpty()) {
                cnt = getsearch.size();
        }
        } catch (Exception e) {
            LOGGER.error("getSearchCount : " + logUserId + " " + e);
    }
        LOGGER.debug("getSearchCount : " + logUserId + ENDS);
        return cnt;
    }

    @Override
    public List<TblListBoxMaster> getComboNameById(int id) {
        LOGGER.debug("getComboNameById : " + logUserId + STARTS);
        List<TblListBoxMaster> list = null;
        try {
            list = tblListBoxMasterDao.findTblListBoxMaster("listBoxId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getComboNameById : " + logUserId + " " + ex);
        }
        LOGGER.debug("getComboNameById : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblListBoxMaster> getListBoxCatWise(int intFormId, String strCat) {
        LOGGER.debug("getListBoxCatWise : " + logUserId + STARTS);
        List<TblListBoxMaster> listBoxDetail = new ArrayList<TblListBoxMaster>();
        try {
            listBoxDetail = tblListBoxMasterDao.findTblListBoxMaster(TEMPLATEFORMID, Operation_enum.EQ, intFormId, "isCalcReq", Operation_enum.EQ, strCat);
        } catch (Exception ex) {
            LOGGER.error("getListBoxCatWise : " + logUserId + " " + ex);
        }
        LOGGER.debug("getListBoxCatWise : " + logUserId + ENDS);
        return listBoxDetail;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public boolean deleteCombo(int listBoxId) {
        LOGGER.debug("deleteCombo : " + logUserId + STARTS);
        boolean flag = false;
        int i=0;
        try {
                i = hibernateQueryDao.updateDeleteNewQuery("delete from TblListBoxMaster where listBoxId="+listBoxId+"");
                if(i>0)
                {
                    flag = true;
                }
        } catch (Exception e) {
            LOGGER.error("deleteCombo : " + logUserId + " " + e);
        }
        LOGGER.debug("deleteCombo : " + logUserId + ENDS);
        return flag;
    }

    @Override
    public List<Object> getSTDStatus(int templateId) {
       LOGGER.debug("getSTDStatus : " + logUserId + STARTS);
       List<Object> list = null;
       try {
               list = hibernateQueryDao.singleColQuery("select ttm.status from TblTemplateMaster ttm where ttm.templateId="+templateId+"");
        } catch (Exception e) {
            LOGGER.error("getSTDStatus : " + logUserId + " " + e);
        }
        LOGGER.debug("getSTDStatus : " + logUserId + ENDS);
        return list;
    }
   
    @Override
    public long getUsedComboCountinStd(int listBoxId, int tenderFormId) {
        LOGGER.debug("getUsedComboCountinStd : " + logUserId + STARTS);
        long cnt = 0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblListCellDetail tld, TblTemplateTables ttt", "tld.tblTemplateTables.tableId=ttt.tableId and ttt.tblTemplateSectionForm.formId="+tenderFormId+" and tld.tblListBoxMaster.listBoxId="+listBoxId+"");
        } catch (Exception ex) {
            LOGGER.error("getUsedComboCountinStd : " + logUserId + " " + ex);
        }
        LOGGER.debug("getUsedComboCountinStd : " + logUserId + ENDS);
        return cnt;
    }
}
