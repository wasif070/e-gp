/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblTenderLotSecurityImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblContractSignDocsDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaIssueDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDocStatisticsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderIttClauseDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderIttSubClauseDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderSectionDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderSectionDocsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderTemplate;
import com.cptu.egp.eps.model.table.TblContractSignDocs;
import com.cptu.egp.eps.model.table.TblNoaDocuments;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import com.cptu.egp.eps.model.table.TblTenderDocStatistics;
import com.cptu.egp.eps.model.table.TblTenderIttClause;
import com.cptu.egp.eps.model.table.TblTenderIttSubClause;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.model.table.TblTenderSectionDocs;
import com.cptu.egp.eps.model.table.TblTenderStd;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
public class TenderDocumentService {

    HibernateQueryDao hibernateQueryDao;
    TblTenderSectionDao tblTenderSectionDao;
    TblTenderDocStatisticsDao tblTenderDocStatisticsDao;
    //TblTenderIttHeaderDao tblTenderIttHeaderDao;
    TblTenderIttClauseDao tblTenderIttClauseDao;
    TblTenderIttSubClauseDao tblTenderIttSubClauseDao;
    private TblTenderLotSecurityImpl tblTenderLotSecurityDao;
    private SPTenderTemplate sPTenderTemplate;
    TblTenderSectionDocsDao tblTenderSectionDocsDao;
    TblNoaDocumentsDao tblNoaDocumentsDao;
    TblNoaIssueDetailsDao tblNoaIssueDetailsDao;
    TblContractSignDocsDao tblContractSignDocsDao;

    final Logger logger = Logger.getLogger(TenderDocumentService.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String getTenderSTDId = "getTenderSTDId : ";

    /**
     * Set user id for log purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *
     * @return
     */
    public SPTenderTemplate getsPTenderTemplate() {
        return sPTenderTemplate;
    }

    /**
     *
     * @param sPTenderTemplate
     */
    public void setsPTenderTemplate(SPTenderTemplate sPTenderTemplate) {
        this.sPTenderTemplate = sPTenderTemplate;
    }

    /**
     *
     * @return
     */
    public TblTenderIttSubClauseDao getTblTenderIttSubClauseDao() {
        return tblTenderIttSubClauseDao;
    }

    /**
     *
     * @param tblTenderIttSubClauseDao
     */
    public void setTblTenderIttSubClauseDao(TblTenderIttSubClauseDao tblTenderIttSubClauseDao) {
        this.tblTenderIttSubClauseDao = tblTenderIttSubClauseDao;
    }

    /**
     *
     * @return
     */
    public TblTenderIttClauseDao getTblTenderIttClauseDao() {
        return tblTenderIttClauseDao;
    }

    /**
     *
     * @param tblTenderIttClauseDao
     */
    public void setTblTenderIttClauseDao(TblTenderIttClauseDao tblTenderIttClauseDao) {
        this.tblTenderIttClauseDao = tblTenderIttClauseDao;
    }

    /**
     *
     * @param hibernateQueryDao
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     *
     * @param tblTenderSectionDao
     */
    public void setTblTenderSectionDao(TblTenderSectionDao tblTenderSectionDao) {
        this.tblTenderSectionDao = tblTenderSectionDao;
    }

    /**
     *
     * @return
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     *
     * @return
     */
    public TblTenderSectionDao getTblTenderSectionDao() {
        return tblTenderSectionDao;
    }

    /**
     *
     * @return
     */
    public TblTenderDocStatisticsDao getTblTenderDocStatisticsDao() {
        return tblTenderDocStatisticsDao;
    }

    /**
     *
     * @param tblTenderDocStatisticsDao
     */
    public void setTblTenderDocStatisticsDao(TblTenderDocStatisticsDao tblTenderDocStatisticsDao) {
        this.tblTenderDocStatisticsDao = tblTenderDocStatisticsDao;
    }

    public TblTenderSectionDocsDao getTblTenderSectionDocsDao() {
        return tblTenderSectionDocsDao;
    }

    public void setTblTenderSectionDocsDao(TblTenderSectionDocsDao tblTenderSectionDocsDao) {
        this.tblTenderSectionDocsDao = tblTenderSectionDocsDao;
    }

    public TblNoaDocumentsDao getTblNoaDocumentsDao() {
        return tblNoaDocumentsDao;
    }

    public void setTblNoaDocumentsDao(TblNoaDocumentsDao tblNoaDocumentsDao) {
        this.tblNoaDocumentsDao = tblNoaDocumentsDao;
    }

    public TblNoaIssueDetailsDao getTblNoaIssueDetailsDao() {
        return tblNoaIssueDetailsDao;
    }

    public void setTblNoaIssueDetailsDao(TblNoaIssueDetailsDao tblNoaIssueDetailsDao) {
        this.tblNoaIssueDetailsDao = tblNoaIssueDetailsDao;
    }

    public TblContractSignDocsDao getTblContractSignDocsDao() {
        return tblContractSignDocsDao;
    }

    public void setTblContractSignDocsDao(TblContractSignDocsDao tblContractSignDocsDao) {
        this.tblContractSignDocsDao = tblContractSignDocsDao;
    }
    
    

    /**
     * it gives the template section name
     * @param tenderStdId
     * @return
     */
    public List<TblTenderSection> getTemplateSection(int tenderStdId) {
        logger.debug("getTemplateSection : " + logUserId + loggerStart);
        List<TblTenderSection> tenderSection;
        try {
            tenderSection = tblTenderSectionDao.findTblTenderSection("tblTenderStd", Operation_enum.EQ, new TblTenderStd(tenderStdId));
        } catch (Exception ex) {
            tenderSection = null;
            logger.debug("getTemplateSection : " + logUserId + ex);
        }
        logger.debug("getTemplateSection : " + logUserId + loggerEnd);
        return tenderSection;
    }

    /**
     * it gives the section type
     * @param sectionId
     * @return
     */
    public String getSectionType(int sectionId) {
        logger.debug("getSectionType : " + logUserId + loggerStart);
        String string = "";
        List<TblTenderSection> tenderSection;
        try {
            tenderSection = tblTenderSectionDao.findTblTenderSection("tenderSectionId", Operation_enum.EQ, sectionId);
            if (tenderSection.size() > 0) {
                string = tenderSection.get(0).getContentType();
            }
        } catch (Exception ex) {
            tenderSection = null;
            logger.debug("getSectionType : " + logUserId + ex);
        }
        logger.debug("getSectionType : " + logUserId + loggerEnd);
        return string;
    }

    /**
     * it gives the tender Id
     * @param tenderId
     * @return
     */
    public int getTenderSTDId(int tenderId) {
        logger.debug(getTenderSTDId + logUserId + loggerStart);
        List<Object> obj;
        int stdId = -1;
        try {
            String query = "select tts.tenderStdId from TblTenderStd tts where tts.tenderId = " + tenderId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }
        } catch (Exception ex) {
            logger.debug(getTenderSTDId + logUserId + ex);
        }
        logger.debug(getTenderSTDId + logUserId + loggerEnd);
            return stdId;
        }

    /**
     * it gives the tender STD Id
     * @param tenderId
     * @param lotId
     * @return
     */
    public int getTenderSTDId(int tenderId, int lotId) {
        logger.debug(getTenderSTDId + logUserId + loggerStart);
        List<Object> obj;
        int stdId = -1;
        try {
            String query = "select tts.tenderStdId from TblTenderStd tts where tts.tenderId = " + tenderId + " and tts.packageLotId = " + lotId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }
        } catch (Exception ex) {
            logger.debug(getTenderSTDId + logUserId + ex);
        }
        logger.debug(getTenderSTDId + logUserId + loggerEnd);
            return stdId;
        }

    /**
     * it gives the clause count
     * @param headerId
     * @return
     */
    public long getClauseCount(int headerId) {
        logger.debug("getClauseCount : " + logUserId + loggerStart);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblTenderIttClause TblTenderIttClause", "TblTenderIttClause.tblTenderIttHeader.tenderIttHeaderId = '" + headerId + "'");
        } catch (Exception ex) {
            count = 0;
            logger.debug("getClauseCount : " + logUserId + ex);
        }
        logger.debug("getClauseCount : " + logUserId + loggerEnd);
        return count;
    }

    /**
     * it gives the sub clause count
     * @param headerId
     * @return
     */
    public long getSubClauseCount(int headerId) {
        logger.debug("getSubClauseCount : " + logUserId + loggerStart);
        long count = 0;
        try {
            //count = hibernateQueryDao.countForNewQuery("TblTenderIttClause TblTenderIttClause", "TblTenderIttClause.tblTenderIttHeader.tenderIttHeaderId = '" + headerId + "'");
            count = hibernateQueryDao.countForQuery("TblTenderIttSubClause", "tblTenderIttClause.tblTenderIttHeader.tenderIttHeaderId = '" + headerId + "'");
        } catch (Exception ex) {
            count = 0;
            logger.debug("getSubClauseCount : " + logUserId + ex);
        }
        logger.debug("getSubClauseCount : " + logUserId + loggerEnd);
        return count;
    }

    /**
     * it gives the subsection name
     * @param headerId
     * @return
     */
    public String getSubSectionName(int headerId) {
        logger.debug("getSubSectionName : " + logUserId + loggerStart);
        String subSectionName = null;
        try {
            List<Object> subSectionNameList = hibernateQueryDao.singleColQuery("select ittHeaderName from TblTenderIttHeader tblTenderIttHeader where tblTenderIttHeader.tenderIttHeaderId = '" + headerId + "'");
            if (subSectionNameList.size() > 0) {
                subSectionName = subSectionNameList.get(0).toString();
            }
        } catch (Exception ex) {
            logger.debug("getSubSectionName : " + logUserId + ex);
        }
        logger.debug("getSubSectionName : " + logUserId + loggerEnd);
        return subSectionName;
    }

    /**
     * it gives the tender clause details
     * @param headerId
     * @return
     */
    public List<TblTenderIttClause> getTenderClauseDetail(int headerId) {
        logger.debug("getTenderClauseDetail : " + logUserId + loggerStart);
        List<TblTenderIttClause> tblIttClause;
        try {
            tblIttClause = tblTenderIttClauseDao.findTblTenderIttClause("tblTenderIttHeader.tenderIttHeaderId", Operation_enum.EQ, headerId);
        } catch (Exception ex) {
            tblIttClause = null;
            logger.debug("getTenderClauseDetail : " + logUserId + ex);
        }
        logger.debug("getTenderClauseDetail : " + logUserId + loggerEnd);
        return tblIttClause;
    }

    /**
     * it gives the tender sub clause detail
     * @param clauseId
     * @return
     */
    public List<TblTenderIttSubClause> getTenderSubClauseDetail(int clauseId) {
        logger.debug("getTenderSubClauseDetail : " + logUserId + loggerStart);
        List<TblTenderIttSubClause> tblIttSubClause;
        try {
            tblIttSubClause = tblTenderIttSubClauseDao.findTblTenderIttSubClause("tblTenderIttClause.tenderIttClauseId", Operation_enum.EQ, clauseId);
        } catch (Exception ex) {
            tblIttSubClause = null;
            logger.debug("getTenderSubClauseDetail : " + logUserId + ex);
        }
        logger.debug("getTenderSubClauseDetail : " + logUserId + loggerEnd);
        return tblIttSubClause;
    }

    /**
     * @return the tblTenderLotSecurityDao
     */
    public TblTenderLotSecurityImpl getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    /**
     * @param tblTenderLotSecurityDao the tblTenderLotSecurityDao to set
     */
    public void setTblTenderLotSecurityDao(TblTenderLotSecurityImpl tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }

    /**
     * it gives the lot details
     * @param tenderId
     * @return
     */
    public List<TblTenderLotSecurity> getLotDetails(int tenderId) {
        logger.debug("getLotDetails : " + logUserId + loggerStart);
         List<TblTenderLotSecurity> tblTenderLotSecurity = null;
        try {
            tblTenderLotSecurity = tblTenderLotSecurityDao.findTblTenderLotSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception ex) {
            logger.debug("getLotDetails : " + logUserId + ex);
        }
        logger.debug("getLotDetails : " + logUserId + loggerEnd);
    return tblTenderLotSecurity;
    }

    /**
     * it gives the lot details by lot id
     * @param tenderId
     * @param appPkgLotId
     * @return
     */
    public List<TblTenderLotSecurity> getLotDetailsByLotId(int tenderId, int appPkgLotId) {
        logger.debug("getLotDetailsByLotId : " + logUserId + loggerStart);
         List<TblTenderLotSecurity> tblTenderLotSecurity = null;
        try {
            tblTenderLotSecurity = tblTenderLotSecurityDao.findTblTenderLotSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "appPkgLotId", Operation_enum.EQ, appPkgLotId);
        } catch (Exception ex) {
            logger.debug("getLotDetailsByLotId : " + logUserId + ex);
        }
        logger.debug("getLotDetailsByLotId : " + logUserId + loggerEnd);
    return tblTenderLotSecurity;
    }

    /**
     * it gives the templateId of STD
     * @param tenderId
     * @return
     */
    public String findStdTemplateId(String tenderId) {
        logger.debug("findStdTemplateId : " + logUserId + loggerStart);
        String str = null;
        try {
            str = String.valueOf(hibernateQueryDao.getSingleColQuery("select ttd.stdTemplateId from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0));
        } catch (Exception e) {
            logger.debug("findStdTemplateId : " + logUserId + e);
    }
        logger.debug("findStdTemplateId : " + logUserId + loggerEnd);
        return str;
    }

    /**
     * it Dumps the std to tender document
     * @param tenderid
     * @param templateid
     * @param userid
     * @return
     */
    public CommonMsgChk dumpSTD(String tenderid, String templateid, String userid) {
        logger.debug("dumpSTD : " + logUserId + loggerStart);
        CommonMsgChk commonMsgChk = null;
        try {
            commonMsgChk = sPTenderTemplate.exeProcedure("insert", Integer.parseInt(tenderid), Integer.parseInt(templateid), Integer.parseInt(userid)).get(0);
        } catch (Exception e) {
            logger.debug("dumpSTD : " + logUserId + e);
    }
        logger.debug("dumpSTD : " + logUserId + loggerEnd);
        return commonMsgChk;
    }

    /**
     * it is checking STD for dump or not
     * @param tenderid
     * @return
     * @throws Exception
     */
    public String checkForDump(String tenderid) throws Exception {
        logger.debug("checkForDump : " + logUserId + loggerStart);
        String str = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select tts.templateName from TblTenderStd tts where tts.tenderId=" + tenderid);
            if (list.isEmpty()) {
                str = null;
            } else {
                str = (String) list.get(0);
        }
        } catch (Exception e) {
            logger.debug("checkForDump : " + logUserId + e);
     }
        logger.debug("checkForDump : " + logUserId + loggerEnd);
        return str;
    }

    /**
     * it delete the STD
     * @param tenderId
     * @param stdTemplateId
     * @return
     */
    public boolean deleteStd(int tenderId, int stdTemplateId) {
        logger.debug("deleteStd : " + logUserId + loggerStart);
        boolean isDeleteStd = false;
        //  Dohatec Start
        boolean isSame = false;
        int flag = 0;
        try {
            List<Object> templateId = hibernateQueryDao.singleColQuery("select ts.tblTemplateMaster.templateId from TblTenderStd ts where ts.tenderId=" + tenderId);
            if(!templateId.isEmpty())
            {
                if(stdTemplateId == Integer.parseInt(templateId.get(0).toString()))
                {
                    isSame = true;
                    flag = 1;
                }               
            }

            if(!isSame)
            {
                flag = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderStd ts where ts.tenderId=" + tenderId);
            }

            if (flag != 0) {
                int flag1 = hibernateQueryDao.updateDeleteNewQuery("update TblTenderDetails td set td.stdTemplateId=" + stdTemplateId + " where td.tblTenderMaster.tenderId=" + tenderId);
                if (flag1 != 0) {
                    isDeleteStd = true;
                }
            }
            //  Dohatec End
            /*  Old Version
                int flag = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderStd ts where ts.tenderId=" + tenderId);
                if (flag != 0) {
                    int flag1 = hibernateQueryDao.updateDeleteNewQuery("update TblTenderDetails td set td.stdTemplateId=" + stdTemplateId + " where td.tblTenderMaster.tenderId=" + tenderId);
                    if (flag1 != 0) {
                        isDeleteStd = true;
                }
            */
        } catch (Exception e) {
            logger.debug("deleteStd : " + logUserId + e);
    }
        logger.debug("deleteStd : " + logUserId + loggerEnd);
        return isDeleteStd;
    }

    /**
     * it adds tender document statistics
     * @param tblTenderDocStatistics
     */
    public void addTenderDocStatistics(TblTenderDocStatistics tblTenderDocStatistics) {
        logger.debug("addTenderDocStatistics : " + logUserId + loggerStart);
        try {
            tblTenderDocStatisticsDao.addTblTenderDocStatistics(tblTenderDocStatistics);
        } catch (Exception ex) {
            logger.debug("addTenderDocStatistics : " + logUserId + ex);
        }
        logger.debug("addTenderDocStatistics : " + logUserId + loggerEnd);
    }

    /**
     * it gives the package number
     * @param tenderId
     * @return {packageNo}
     */
    public Object getPkgNo(String tenderId) {
        logger.debug("getPkgNo : " + logUserId + loggerStart);
        Object obj = null;
        try {
            obj = hibernateQueryDao.singleColQuery("select ttd.packageNo from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
        } catch (Exception e) {
            logger.debug("getPkgNo : " + logUserId + e);
        }
        logger.debug("getPkgNo : " + logUserId + loggerEnd);
        return obj;
    }
    
    
    /*public Object getWorkCategory(String tenderId) {
        logger.debug("getPkgNo : " + logUserId + loggerStart);
        Object obj = null;
        try {
            obj = hibernateQueryDao.singleColQuery("select tp.workCategory from TblAppPackages tp, TblTenderDetails td where tp.packageNo = td.packageNo and td.tenderId ='"+tenderId+"'").get(0);
        } catch (Exception e) {
            logger.debug("getPkgNo : " + logUserId + e);
        }
        logger.debug("getPkgNo : " + logUserId + loggerEnd);
        return obj;
    }*/
    
    
    
    /**
     * it gives the Given Estimated Cost
     * @param tenderId
     * @return {estCost}
     */
    public List<Object[]> getGivenEst(String tenderId) {
        logger.debug("getGivenEst : " + logUserId + loggerStart);
        List<Object[]> list = new ArrayList<Object[]>();
        
        try {
                String query = "select ttec.estCost from TblTenderEstCost ttec where ttec.tblTenderMaster.tenderId=" + tenderId;
                list = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            logger.debug("getGivenEst : " + logUserId + e);
        }
        logger.debug("getGivenEst : " + logUserId + loggerEnd);
        return list;
    }
    
    
       /**
     * it gives bidder category
     * @param packageNo
     * @return {bidderCategory}
     */
     public Object[] getBidderCategory(String packageNo) {
        logger.debug("getPkgNo : " + logUserId + loggerStart);
        Object[] obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select ttd.bidderCategory,ttd.workCategory from TblAppPackages ttd where ttd.packageNo='"+packageNo+"'").get(0);
        } catch (Exception e) {
            logger.debug("getPkgNo : " + logUserId + e);
        }
        logger.debug("getPkgNo : " + logUserId + loggerEnd);
        return obj;
    }
    
    

    /**
     * it gives the package number
     * @param tenderId
     * @return {packageNo}
     */
    public Object getDptId(String tenderId) {
        logger.debug("getPkgNo : " + logUserId + loggerStart);
        Object obj = null;
        try {
            obj = hibernateQueryDao.singleColQuery("select ttd.departmentId from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
        } catch (Exception e) {
            logger.debug("getPkgNo : " + logUserId + e);
        }
        logger.debug("getPkgNo : " + logUserId + loggerEnd);
        return obj;
    }
    
    
  
    
    /**
     * it gives the package number
     * @param dptId
     * @return {packageNo}
     */
    public Object getDptType(String dptId) {
        logger.debug("getPkgNo : " + logUserId + loggerStart);
        Object obj = null;
        try {
            obj = hibernateQueryDao.singleColQuery("select ttd.departmentType from TblDepartmentMaster ttd where ttd.departmentId="+dptId).get(0);
        } catch (Exception e) {
            logger.debug("getPkgNo : " + logUserId + e);
        }
        logger.debug("getPkgNo : " + logUserId + loggerEnd);
        return obj;
    }
    
    

    
    
    /**
     * List of {sectionId,sectionName }
     * @param templateId Tender Template
     * @return List of {sectionId,sectionName }
     */
    public List<Object[]> getTemplateSections(String templateId){
        StringBuilder query = new StringBuilder();
        query.append("select tts.sectionId,tts.sectionName from ");
        query.append("TblTemplateSections tts where tts.templateId="+templateId);
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    /**
     * Count of TblTenderDocStatistics
     * @param tenderId from tbl_TenderMaster
     * @param lotId from tbl_TenderLotSeuciryt
     * @param userId
     * @return count of TblTenderDocStatistics
     */
    public long tendDocStatCount(String tenderId,String lotId,String userId){
        logger.debug("tendDocStatCount : " + logUserId + loggerStart);
        long cnt=0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderDocStatistics tds", "tds.tenderId="+tenderId+" and tds.pkgLotId="+lotId+" and tds.userId="+userId);
        } catch (Exception e) {
            logger.error("tendDocStatCount : ",e);
        }
        logger.debug("tendDocStatCount : " + logUserId + loggerEnd);
        return cnt;
    }

    /**
     * Get Common Mapped document list
     * @param tenderId
     * @param userId
     * @return Document details in array of Objects.
     */
    public List<Object[]> commonDocMapList(String tenderId,String userId){
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String query = "select tbd.formId,tcd.documentName,tcd.documentSize, tcd.companyDocId,tcd.documentBrief from TblBidDocuments tbd,TblCompanyDocuments tcd where tbd.companyDocId=tcd.companyDocId and tbd.tenderId = "+tenderId+" and tbd.userId = "+userId;
            list = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public int getTemplateIdfromSectionId(String sectionId){
        int i=0;
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select templateId from TblTemplateSections where sectionId="+sectionId);
            if(list!=null && !list.isEmpty()){
                i = Integer.parseInt(list.get(0)[0].toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }

    /**
     * Get Tender Document Details
     * @param tenderId
     * @return List of Tender documents as TblTenderSectionDocs objects
     */
    public List<TblTenderSectionDocs> getDocsTenderId(short tenderId){
        List<TblTenderSectionDocs> list = new ArrayList<TblTenderSectionDocs>();
        try {
            list = tblTenderSectionDocsDao.findTblTenderSectionDocs("tenderId",Operation_enum.EQ,tenderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    /**
     * Get NOA document list
     * @param tenderId
     * @param bidderId
     * @return document list
     */
    public List<TblNoaDocuments> getNoaDoc(int tenderId,int bidderId){
        List<TblNoaDocuments> list = new ArrayList<TblNoaDocuments>();
        try {
            list = tblNoaDocumentsDao.findTblNoaDocuments("tenderId",Operation_enum.EQ,tenderId,"userId",Operation_enum.EQ,bidderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Get NOA id
     * @param tenderId
     * @param bidderId
     * @return NOA id
     */
    public int getNoaId(int tenderId,int bidderId){
        List<TblNoaIssueDetails> list = new ArrayList<TblNoaIssueDetails>();
        int noaId = 0;
        try {
            list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"userId",Operation_enum.EQ,bidderId);
            if(list!=null && !list.isEmpty()){
                noaId = list.get(0).getNoaIssueId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.clear();
        list = null;
        return noaId;
    }

    /**
     * Get contract Document list
     * @param noaId
     * @return Contract document list
     */
    public List<TblContractSignDocs> getContractDocList(int noaId){
        List<TblContractSignDocs> list = new ArrayList<TblContractSignDocs>();
        try {
            list = tblContractSignDocsDao.findTblContractSignDocs("noaId",Operation_enum.EQ,noaId);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Get old mapped contract Document list
     * @param Tender Id
     * @param BIdder Id
     * @return Contract document list
     */
        public List<Object[]> mappedWinningDocMapList(String tenderId,String userId){
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String query = "select  distinct tbds.companyDocId from TblContractSign tcs,  TblNoaIssueDetails tnid, TblBidDocuments tbds,  TblBidDocuments tbd,TblCompanyDocuments tcd where tcs.noaId=tnid.noaIssueId and tnid.tblTenderMaster.tenderId = tbds.tenderId and tbd.companyDocId =  tbds.companyDocId and tbd.companyDocId=tcd.companyDocId and tbd.tenderId = "+tenderId+" and tbd.userId = "+userId;
            list = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
