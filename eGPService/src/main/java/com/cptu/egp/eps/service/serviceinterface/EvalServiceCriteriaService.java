/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalBidderMarks;
import com.cptu.egp.eps.model.table.TblEvalBidderStatus;
import com.cptu.egp.eps.model.table.TblEvalSerFormDetail;
import com.cptu.egp.eps.model.table.TblEvalServiceCriteria;
import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *This service is used for update delete and view of Evaluation Passing Mark Module
 * @author Nishith
 */
public interface EvalServiceCriteriaService {

    /**
     *This method is give List of Sub Criteria except qualification Criteria
     * @return in object[0] give sub criteria id , [1] SubCriteria Name object[2] give main criteria name
     */
    public List<Object[]> getCritetria();

    /**
     *This method gives sub criteria of qualification criteria
     * @param criId=Criteria Id
     * @return {subCriteriaId,subCriteria}
     */
    public List<Object[]> getSubCriteria(int criId);

    /**
     *This method is used when inserting a Table
     * @param list objectList of TblEvalServiceForms
     * @return true or false
     */
    public boolean insertTbl(List<TblEvalServiceForms> list);

    /**
     *Gives Data of TblEvalServiceForms when updating a values
     * @param tenderId
     * @return List of TblEvalServiceForms object
     */
    public List<TblEvalServiceForms> listData(int tenderId);

    /**
     *When update 1st it will delete and at this time this method is used
     * @param tenderId = tenderId
     * @return true and false;
     */
    public boolean deleteRecord(int tenderId,String isCorri);

    /**
     *List of Sub Criteria
     * 
     * @return obj[] sub Criteria Id and obj[[1] Sub criteria name
     */
    public List<Object []> getSubCriteria();


    /**
     *Gives data of TblEvalServiceForms and TblEvalServiceCriteria for display
     * @param tenderFormId = tenderFormId
     * @return List of objects {subCriteria,maxMarks,subCriteriaId}
     */
    public List<Object[]> getEvalServiceMarks(int tenderFormId);

    /**
     *insert in to TblEvalSerFormDetail
     * @param tblEvalSerFormDetail = tblEvalSerFormDetail
     * @return true or false;
     */
    public boolean insertTblEvalSerFormDetail(List<TblEvalSerFormDetail> tblEvalSerFormDetail);

    /**
     * delete data from tbl_EvalSerFormDetail
     * @param whereCondition whereCondition
     * @return true and false
     */
    public boolean insertDeleteTblEvalSerFormDetail(String whereCondition);

    /**
     * For View the marks
     * @param tenderId tenderId
     * @return List of object obj[0] subCriteria,[1] maxMarks, [2] formId, [3] subCriteriaId, [4] MainCriteria
     */
    public List<Object []> viewEvalSerMark(int tenderId,String isCorri);

    /**
     * Get Evaluation Bidder Marks
     * @param bidderId userId
     * @param comMemId committee Member userId
     * @param tenderId from tbl_TenderMaster
     * @param formId from tbl_TenderForms
     * @return List of {maxMarks ,actualMarks ,ratingWeightage ,ratedScore,tenderFormId,formName,subCriteria,bidId}
     */
    public List<Object[]> getEvalBidderMarks(String bidderId,String comMemId,String tenderId,String formId);

    /**
     * Get Forms filled by Bidder
     * @param bidderId userId
     * @param tenderId from tbl_TenderMaster
     * @return List of {tenderFormId}
     */
    public List<Object> getBiddersFormId(String bidderId,String tenderId);

    /**
     * Get Forms filled by Bidder
     * @param bidderId userId
     * @param formId from tbl_TenderForms
     * @return List of {bidId}
     */
    public List<Object> getBidIdFrmFormId(String bidderId, String formId);

    /**
     * Get details from tbl_EvalSerFormDetail
     * @param tenderId tenderId , userId userId, tenderFormId tenderFormId,bId bId
     * @return List of object (subCriteria,esFormDetailId,remarks,maxMarks,bidId,ratingWeightage,ratedScore,actualMarks,subCriteriaId)
     */
    public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId, int evalBy,int bId);

    /**
     * Get details from tbl_EvalSerFormDetail
     * @param tenderId tenderId , userId userId, tenderFormId tenderFormId
     * @return List of object {subCriteria,esFormDetailId,remarks,maxMarks,bidId}
     */
    public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId, int evalBy);


    /**
     * Final Submission Bidders
     * @param tenderId from tbl_TenderMaster
     * @return List of {userId,companyName,firstName,lastName}
     */
    public List<Object[]> getFinalSubBidders(String tenderId);

    /**
     * Evaluation Service Report Data
     * @param formId from tbl_TenderForms
     * @param govUserId userId
     * @param tenderId from tbl_TenderMaster
     * @return List of {tenderFormId,formName,subCriteria,maxMarks,subCriteriaId}
     */
	//Change by dohatec for re-evaluation
    public List<Object[]> evalServiceRepData(String formId,String govUserId,String tenderId,int evalCount);

    /**
     * Get all Form for the tenderId
     * @param tenderId from tbl_TenderMaster
     * @return List of {tenderFormId}
     */
    public List<Object> getAllFormId(String tenderId);

    /**
     * Get marks for in case of Service in Tender
     * @param tenderId from tbl_TenderMaster
     * @param formId from tbl_TenderForms
     * @param govUserId
     * @param bidderId bidder userId
     * @param subCriteriaId
     * @return List of {actualMarks ,ratingWeightage ,ratedScore}
     */
	//Change by dohatec for re-evaluation
    public List<Object[]> evalServiceRepBidderData(String tenderId,String formId,String govUserId,String bidderId,String subCriteriaId,int evalCount);

    /**
     * Evaluation Committee Member
     * @param tenderId from tbl_TenderMaster
     * @param userId bidder userId
     * @return List of {userId,employeeName}
     */
    public List<Object[]> getComMemberName(String tenderId,String userId);

    /**
     * Get the marks for the bidder from member
     * @param bidderId bidder userId
     * @param tenderId from tbl_TenderMaster
     * @param formId from tbl_TenderFroms
     * @return List of {maxMarks ,actualMarks ,ratingWeightage ,ratedScore,tenderFormId,formName,subCriteria,bidId,subCriteriaId}
     */
    public List<Object[]> getBidderMarksMem(String bidderId,String tenderId,String formId);

    public List<SPCommonSearchDataMore> getBidderMarksMemSP(String bidderId,String tenderId,String formId,String officerId);

    /**
     * Member Data for Evaluation Service
     * @param tenderId from tbl_TenderMaster
     * @param memId
     * @param userId
     * @param formId from tbl_TenderFroms
     * @param critId
     * @param bidId
     * @return List of {actualMarks ,ratingWeightage ,ratedScore,bidId}
     */
    public List<Object[]> getDataForMembers(String tenderId,String memId,String userId,String formId,String critId,String bidId);

    /**
     * Drop Committee member from Evaluation in Service
     * @param tenderId from tbl_TenderMaster
     * @param memId membere userId
     * @param bidderId
     * @param dropMember
     * @return true or false for success or fail
     */
    public boolean dropMemberFrmReport(String tenderId,String memId,String bidderId,String dropMember);

    /**
     * Get total of actualMarks for the bidder
     * @param tenderId from tbl_TenderMaster
     * @param memId Committee Member userId
     * @param bidderId bidder userId
     * @return sum of actualMarks
     */
    public double getTotal4ComMemReport(String tenderId,String memId,String bidderId);

    /**
     * Get Passing marks for the Tender
     * @param tenderId from tbl_TenderMaster
     * @return passing marks
     */
    public Object getPassingMarksofTender(String tenderId);

    /**
     * Inserts Bidder Marks and Status for Evaluation in Service
     * @param bidderStatus Bidder Status List
     * @param list Bidder Marks list
     * @return true or false for success or fail
     */
    public boolean insertEvalBidderStatus(TblEvalBidderStatus bidderStatus,List<TblEvalBidderMarks> list);

    /**
     * Get Bidders Evaluation Status
     * @param tenderId from tbl_TenderMaster
     * @param statEval or ReEval
     * @return List of {bidderMarks ,firstName,lastName,companyName,userId}
     */
	//Change by dohatec for re-evaluation
    public List<Object[]> getBidderEvalStatus(String tenderId,String stat,int evalCount);

    /**
     * Inserts T1L1 report data
     * @param tenderId from tbl_TenderMaster
     * @param createdBy
     * @param list to be inserted
     * @return true or false for success or fail
     */
    public boolean insertBidderRank(String tenderId,String createdBy,List<TblBidderRank> list);

    public void setLogUserId(String logUserId);

    public List<SPCommonSearchDataMore> getBidderMarksMemWithCrit(String bidderId,String tenderId,String formId,String officerId,String critId);
   
    public List<SPCommonSearchDataMore> getTotal4ComMemReportSP(String bidderId, String tenderId, String officerId);
    /**
     * If evaluation method is technical then and then link hase to view in tenderPrep
     * @param tenderId tenderId
     * @return long count
     */
    public long evalCertiLink(int tenderId);

    /**
     * Bidder Marks for Evaluation in Service
     * @param tenderId from tbl_TenderMaster
     * @param tenderFormId from tbl_TenderFroms
     * @param createdBy member userId
     * @param userId bidder userId
     * @param subCrietId criteriaId
     * @return
     */
	//Change by dohatec for re-evaluation
    public Object getEvalBidderMarks(String tenderId,String tenderFormId ,String createdBy ,String userId ,String subCrietId, int evalCount);

    /**
     *Get Bidder Total for Tender Evaluation
     * @param tenderId from tbl_TenderMaster
     * @param createdBy session userId
     * @param userId bidder UserId
     * @return List of {marks}
     */
	//Change by dohatec for re-evaluation
    public List<Object> getEvalBidderTotal(String tenderId,String createdBy ,String userId, int evalCount);

    public List<SPCommonSearchDataMore> getSeekClariLinkTextLinkSer(String formId,String critId,String cpId,String memId,String bidderId);

    /**
     *Marks availability count
     * @param tenderId from tbl_TenderMaster
     * @param memberId member userId
     * @param bidderId bidder userId
     * @return count
     */
    public long countForMarksAvail(String tenderId,String memberId,String bidderId);

    /**
     *Check for re-Tendering
     * @param tenderId from tbl_TenderMaster
     * @return true or false for retender or no retender
     */
    public boolean reTendering(String tenderId);

    /**
     *Check for hiding reTender
     * @param tenderId from tbl_TenderMaster
     * @return true or false for retender or no retender
     */
    public boolean reTenderingHide(String tenderId);
    
    /**
         * Get TblEvalServiceWeightage data by tenderId
         * @param tenderId tenderId
         * @return List<TblEvalServiceWeightage>
         */
    public TblEvalServiceWeightage getTblEvalServiceByTenderId(int tenderId);
    /**
         * Get T1 data by tenderId from tbl_roundmaster and tbl_bidderRank
         * @param tenderId tenderId
         * @return List<object []> object[0] NameofBidder, object[1] bidderName, object[3] biderId
         */
    public List<Object []> getT1Data(String tenderId);
    /**
         * Get T1 data by tenderId from tbl_bidderRank
         * @param tenderId tenderId, and roundId
         * @return List of tblBidderRank
         */
    public List<TblBidderRank> getT1L1SaveData(int tenderId, int roundId);
    /**
         * Save data of T1 to Tbl_EvalRoundMaster and Tbl_bidderRanks
         * @param tenderId tenderId, and userId
         * @return ture if success else false
         */
    public boolean saveT1Data(int tenderId,int userId);
     /**
         * Save data of T1 to Tbl_EvalRoundMaster and Tbl_bidderRanks
         * @param tenderId tenderId, and userId
         * @return List of Object {userId,amount}
         */
    public List<Object[]> getL1Data(String tenderId, String roundId);
    /**
         * Set audit trial object at service layer
         * @param 
         * @return 
         */
     public void setAuditTrail(AuditTrail auditTrail);
     /**
         * gives Main Criteria list
         * @param 
         * @return List of Object 
         */
     public List<Object[]> getMainCirteriaList();
     /**
         * gives Bidder result data from database
         * @param 
         * @return List of Object 
         */
	//Change by dohatec for re-evaluation
     public List<Object[]> getBidderResult(String tenderId,String userId,String evalType,int evalCount);
     
     public Object getBidderExtensionStatus(String tenderId,String userId);


}
