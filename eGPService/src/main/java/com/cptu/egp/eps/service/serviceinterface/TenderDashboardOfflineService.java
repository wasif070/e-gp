/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import java.util.List;
import com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails;

/**
 *
 * @author Istiak
 */
public interface TenderDashboardOfflineService {

    public List<TenderDashboardOfflineDetails> getTenderDashboardOfflineData(String moduleFlag, String searchFlag, String procNature, String procType, String procMethod, String id, String status, String refNo, String pubDateFrom, String pubDateTo);

    public boolean deleteTenderCORDashboardOfflineData(String moduleFlag, String id);

    public boolean approveTenderCORDashboardOfflineData(String moduleFlag, String id, String comments);

    public List<TenderDashboardOfflineDetails> corrigendumTenderDashboardOfflineData(String moduleFlag, String id);

    public List<TenderDashboardOfflineDetails> getCORLotInfo(String moduleFlag, String id);

    public List<Object[]> getMinistryForTenderOffline();

    public List<Object[]> getOrgForTenderOffline(String ministry);
}
