/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.egp.eps.service.serviceinterface.EvalCommonSearchService;
import com.cptu.egp.eps.dao.storedprocedure.SPEvalCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.service.serviceimpl.SignerImpl;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author Istiak (Dohatec) - 21.Apr.15
 */
public class EvalCommonSearchServiceImpl implements EvalCommonSearchService {

    private static final Logger LOGGER = Logger.getLogger(EvalCommonSearchServiceImpl.class);
    private SPEvalCommonSearchData sPEvalCommonSearchData;
    private String logUserId = "0";

    /**
     * @param logUserId the logUserId to set
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    /*
     * fetching information from p_get_evalcommondata
     * @param funName1
     * @param funName2
     * @param funName3
     * @param funName4
     * @param funName5
     * @param funName6
     * @param funName7
     * @param funName8
     * @param funName9
     * @param funName10
     * @param funName11
     * @param funName12
     * @param funName13
     * @param funName14
     * @param funName15
     * @return list of data, if execute successfully return list, otherwise return empty list(null)
     */

    @Override
    public List<EvalCommonSearchData> evalSearchData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15)
    {
        LOGGER.debug("EvalSearchData : "+logUserId+" Starts");
        List<EvalCommonSearchData> list = null;
        try{
            list = sPEvalCommonSearchData.executeProcedure(fieldName1, fieldName2, fieldName3, fieldName4, fieldName5, fieldName6, fieldName7, fieldName8, fieldName9, fieldName10, fieldName11, fieldName12, fieldName13, fieldName14, fieldName15);
        }catch (Exception e){
            LOGGER.error("EvalSearchData : "+logUserId+" "+e);
        }
        LOGGER.debug("EvalSearchData : "+logUserId+" Ends");
        return list;
    }

    /**
     * @return the sPEvalCommonSearchData
     */
    public SPEvalCommonSearchData getsPEvalCommonSearchData() {
        return sPEvalCommonSearchData;
    }

    /**
     * @param sPEvalCommonSearchData the sPEvalCommonSearchData to set
     */
    public void setsPEvalCommonSearchData(SPEvalCommonSearchData sPEvalCommonSearchData) {
        this.sPEvalCommonSearchData = sPEvalCommonSearchData;
    }
    
    @Override
     public String GrandSumDiscountValue(String TenderId,String UserId,String FormId){
        double grandSumValue = 0.0;
        String cellValue = "0";
        List<EvalCommonSearchData> listGS = evalSearchData("getGrandSum", TenderId, UserId,FormId,"","","","","","","","","","","");
        if (listGS != null || !listGS.isEmpty()) {            
            for(EvalCommonSearchData gs:listGS){
                SignerImpl obj = new SignerImpl();
                obj.setEncrypt(gs.getFieldName2());
                cellValue = obj.getSymDecrypt(gs.getFieldName4());
                if(gs.getFieldName3().equalsIgnoreCase("BOQsalvage")){
                    //grandSumValue = grandSumValue-Double.parseDouble(cellValue);
                    grandSumValue = grandSumValue;
                }else {
                    grandSumValue = grandSumValue+Double.parseDouble(cellValue);
                }
            }
        }
        return String.format("%.4f", grandSumValue);
    }
     @Override
     public String PercantageValue(String TenderId,String UserId,String FormId,String BidId,String RowId){
        double grandSumValue = 0.0;
        String cellValue = "0";
        List<EvalCommonSearchData> listGS = evalSearchData("getPercantageValue", TenderId, UserId,FormId,BidId,RowId,"","","","","","","","","");
        if (listGS != null || !listGS.isEmpty()) {            
            for(EvalCommonSearchData gs:listGS){
                SignerImpl obj = new SignerImpl();
                obj.setEncrypt(gs.getFieldName1());
                cellValue = obj.getSymDecrypt(gs.getFieldName2());
                grandSumValue = Double.parseDouble(cellValue);                
            }
        }
        return String.valueOf(String.format("%.3f", grandSumValue));
    }
}
