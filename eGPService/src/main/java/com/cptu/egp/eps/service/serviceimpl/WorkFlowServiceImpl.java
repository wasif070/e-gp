/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblAppMasterImpl;
import com.cptu.egp.eps.dao.daoimpl.TblDesignationMasterImpl;
import com.cptu.egp.eps.dao.daoimpl.TblEmployeeMasterImpl;
import com.cptu.egp.eps.dao.daoimpl.TblModuleMasterImpl;
import com.cptu.egp.eps.dao.daoimpl.TblPartnerAdminImpl;
import com.cptu.egp.eps.dao.daoimpl.TblProcurementRoleImpl;
import com.cptu.egp.eps.dao.daoimpl.TblTenderDetailsImpl;
import com.cptu.egp.eps.dao.daoimpl.TblWorkFlowDocumentsImpl;
import com.cptu.egp.eps.dao.daoimpl.TblWorkFlowEventConfigImpl;
import com.cptu.egp.eps.dao.daoimpl.TblWorkFlowFileHistoryImpl;
import com.cptu.egp.eps.dao.daoimpl.TblWorkFlowLevelConfigImpl;
import com.cptu.egp.eps.dao.daoimpl.TblWorkFlowRuleEngineImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.AddWorkFlowRule;
import com.cptu.egp.eps.dao.storedprocedure.AppCommonData;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory;
import com.cptu.egp.eps.dao.storedprocedure.SPWfFileHistory;
import com.cptu.egp.eps.dao.storedprocedure.SPWfFileOnHand;
import com.cptu.egp.eps.dao.storedprocedure.WorkFlowEvent;
import com.cptu.egp.eps.dao.storedprocedure.WorkFlowLevel;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEventMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblModuleMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import com.cptu.egp.eps.model.table.TblWorkFlowEventConfig;
import com.cptu.egp.eps.model.table.TblWorkFlowFileHistory;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig;
import com.cptu.egp.eps.model.table.TblWorkflowRuleEngine;
import com.cptu.egp.eps.service.serviceinterface.WorkFlowService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class WorkFlowServiceImpl implements WorkFlowService {

    static final Logger logger = Logger.getLogger(WorkFlowServiceImpl.class);
    private String logUserId = "0";
    private TblModuleMasterImpl tblModuleMasterDao;
    private TblProcurementRoleImpl tblProcurementRoleDao;
    private String workflowruleengine;
    private BasicDataSource basicDataSource;
    private String getappcommondata;
    private String workfloweventconfig;
    private TblWorkFlowEventConfigImpl tblWorkFlowEventConfigDao;
    private String workflowlevelconfig;
    private TblWorkFlowLevelConfigImpl tblWorkFlowLevelConfigDao;
    private TblEmployeeMasterImpl tblEmployeeMasterDao;
    private TblDesignationMasterImpl tblDesignationMasterDao;
    private TblWorkFlowDocumentsImpl tblWorkFlowDocumentDao;
    private TblWorkFlowRuleEngineImpl tblWorkFlowRuleEngineDao;
    private String wffilehistory;
    private String addUpdWffile;
    private TblWorkFlowFileHistoryImpl tblWorkFlowFileHistoryDao;
    private TblPartnerAdminImpl tblPartnerAdminDao;
    private TblAppMasterImpl tblAppMasterDao;
    private TblTenderDetailsImpl tblTenderDetailsDao;
    private HibernateQueryDao hibernateQueryDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    /**
     * Get Work flow Data
     * @return List of Module Master
     */
    @Override
    public List<TblModuleMaster> getWorkFlowData() {
        logger.debug("getWorkFlowData : " + logUserId + " Starts");
        List<TblModuleMaster> moduleMaster = null;
        try {
            moduleMaster = getTblModuleMasterDao().getAllTblModuleMaster();
        } catch (Exception ex) {
            logger.error("getWorkFlowData : " + logUserId + ex);
        }
        logger.debug("getWorkFlowData : " + logUserId + " Ends");
        return moduleMaster;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblModuleMasterDao
     */
    public TblModuleMasterImpl getTblModuleMasterDao() {

        return tblModuleMasterDao;
    }

    /**
     * @param tblModuleMasterDao the tblModuleMasterDao to set
     */
    public void setTblModuleMasterDao(TblModuleMasterImpl tblModuleMasterDao) {
        this.tblModuleMasterDao = tblModuleMasterDao;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    /**
     * Get User Roles.
     * @return List of Procurement role.
     */
    @Override
    public List<TblProcurementRole> getProcurementRoles() {

        List<TblProcurementRole> tblProcurementRoleDao = getTblProcurementRoleDao().getAllTblProcurementRole();
        return tblProcurementRoleDao;
    }

    /**
     * @return the tblProcurementRoleDao
     */
    public TblProcurementRoleImpl getTblProcurementRoleDao() {
        return tblProcurementRoleDao;
    }

    /**
     * @param tblProcurementRoleDao the tblProcurementRoleDao to set
     */
    public void setTblProcurementRoleDao(TblProcurementRoleImpl tblProcurementRoleDao) {
        this.tblProcurementRoleDao = tblProcurementRoleDao;
    }

    /**
     * to create workflow
     * @param initiatorprocurementroleid --> initiatorroleid
     * @param approverprocurementroleid  --> approverroleid
     * @param initiatorwfroleid
     * @param approverwfroleid
     * @param ispublishdaterequired
     * @param eventid
     * @param action
     * @param actionby
     * @param actiondate
     * @param wfinitruleid
     * @return list of CommonMsgChk
     */
    public List<CommonMsgChk> createWorkFlow(Integer initiatorprocurementroleid,
            Integer approverprocurementroleid, String initiatorwfroleid,
            String approverwfroleid, String ispublishdaterequired, Integer eventid,
            String action, Integer actionby, Date actiondate, Integer wfinitruleid) {
        logger.debug("createWorkFlow : " + logUserId + " Starts");
        List<CommonMsgChk> createworkflow = null;
        AddWorkFlowRule addWorkFlowRule = new AddWorkFlowRule(getBasicDataSource(), workflowruleengine);
        createworkflow = addWorkFlowRule.exeProcedure(initiatorprocurementroleid, approverprocurementroleid,
                initiatorwfroleid, approverwfroleid, ispublishdaterequired, eventid,
                action, actionby, actiondate, wfinitruleid);

        logger.debug("createWorkFlow : " + logUserId + " Ends");
        return createworkflow;
    }

    /**
     * Edit Work flow Data.
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @return List of Common App Data
     */
    public List<CommonAppData> editWorkFlowData(String fieldName1, String fieldName2, String fieldName3) {
        logger.debug("editWorkFlowData : " + logUserId + " Starts");
        List<CommonAppData> editworkflowData = null;
        AppCommonData appCommonData = new AppCommonData(getBasicDataSource(), getappcommondata);
        editworkflowData = appCommonData.executeProcedure(fieldName1, fieldName2, fieldName3);
        logger.debug("editWorkFlowData : " + logUserId + " Ends");
        return editworkflowData;

    }

    /**
     * Create work flow Event.
     * @param noOfReviewer
     * @param noOfDays
     * @param isDonorConReq
     * @param eventid
     * @param objectId
     * @param WfRoleId
     * @param Action
     * @param wfEvtConfId
     * @param actiondate
     * @param actionby
     * @return List of common message check.
     */
    public List<CommonMsgChk> createWorkFlowEvent(Integer noOfReviewer, Integer noOfDays,
            String isDonorConReq, Integer eventid, Integer objectId, Integer WfRoleId,
            String Action, Integer wfEvtConfId, Date actiondate, Integer actionby) {
        logger.debug("createWorkFlowEvent : " + logUserId + " Starts");
        List<CommonMsgChk> listdata = null;
        WorkFlowEvent workFlowEvent = new WorkFlowEvent(getBasicDataSource(), workfloweventconfig);
        listdata = workFlowEvent.executeProcedure(noOfReviewer, noOfDays, isDonorConReq,
                eventid, objectId, WfRoleId, Action, wfEvtConfId, actiondate,
                actionby);
        logger.debug("createWorkFlowEvent : " + logUserId + " Ends");
        return listdata;

    }

    /**
     * this method is for to get isDonorConReqired checkbox is checked or not
     * if it is checked it returns Yes otherwise returns No.
     * @param eventid from tbl_EventMaster
     * @param objectid from tbl_WorkFlowEventConfig
     * @return if Donor condition required or not.
     */
    public String isDonorConRequired(String eventid, Integer objectid) {
        logger.debug("isDonorConRequired : " + logUserId + " Starts");
        String isDonorReq = null;
        Short noOfReviewers = null;
        Short eid = Short.parseShort(eventid);
        int oid = objectid.intValue();
        try {
            List eventdata = tblWorkFlowEventConfigDao.findEntity("tblEventMaster", Operation_enum.EQ, new TblEventMaster(eid), "objectId", Operation_enum.EQ, oid);
            if (eventdata.size() > 0) {
                TblWorkFlowEventConfig workflowconf = (TblWorkFlowEventConfig) eventdata.get(0);
                isDonorReq = workflowconf.getIsDonorConReq();
                noOfReviewers = workflowconf.getNoOfReviewer();
            }
        } catch (Exception ex) {
            logger.error("isDonorConRequired : " + logUserId + ex);
        }
        logger.debug("isDonorConRequired : " + logUserId + " Ends");
        return isDonorReq + "_" + noOfReviewers;
    }

    /**
     * To check if workFlowEvent record Exist or not
     * @param eventid
     * @param objectid
     * @return true if work flow event exist else false
     */
    public boolean isWorkFlowEventExist(short eventid, int objectid) {
        logger.debug("isWorkFlowEventExist : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            HibernateTemplate ht = tblWorkFlowEventConfigDao.getHibernateTemplate();
            ht.getSessionFactory();
            List eventdata = tblWorkFlowEventConfigDao.findEntity("tblEventMaster", Operation_enum.EQ, new TblEventMaster(eventid), "objectId", Operation_enum.EQ, objectid);
            if (eventdata.size() > 0) {
                bSuccess = true;
            }
        } catch (Exception ex) {
            logger.error("isWorkFlowEventExist : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("isWorkFlowEventExist : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     *
     * @param EventId
     * @param ObjectId
     * @param ActionDate
     * @param ActivityId
     * @param ChildId
     * @param FileOnHand
     * @param Action
     * @param worfklowId
     * @param actionBy
     * @param WfLevel
     * @param UserId
     * @param WfRoleId
     * @param Procurementroleid
     * @return List of CommonMsgChk
     */
    public List<CommonMsgChk> addWorkFlowLevel(Integer EventId, Integer ObjectId, Date ActionDate,
            Integer ActivityId, Integer ChildId, String FileOnHand, String Action,
            Integer worfklowId, String actionBy, String WfLevel, String UserId,
            String WfRoleId, String Procurementroleid) {
        logger.debug("addWorkFlowLevel : " + logUserId + " Starts");
        String str = "";
        String auditAction = "";
        String idType = "";
        List<CommonMsgChk> listdata = null;
        WorkFlowLevel wfl = new WorkFlowLevel(basicDataSource, workflowlevelconfig);
        
        if(EventId==1){
           idType = "appId";
        }else if(EventId==9 || EventId==10 || EventId==11){
           idType = "contractId"; 
        }else {
           idType = "tenderId"; 
        }
        
        try{
            listdata = wfl.executeProcedure(EventId, ObjectId, ActionDate, ActivityId,
                ChildId, FileOnHand, Action, worfklowId, actionBy, WfLevel,
                UserId, WfRoleId, Procurementroleid);            
            switch(ActivityId)
            {
                case 1:str="for App";break;
                case 2:str="for Tender Notice";break;
                case 3:str="for PreTender Meeting";break;
                case 4:str="for Amendment Approval";break;
                case 5:str="for Tender Opening";break;
                case 6:str="for Evaluation Committee";break;
                case 7:str="for Package Approve";break;    
                case 8:str="for Technical Sub Committee";break;    
                case 9:str="for Cancel Tender";break;    
                case 10:str="for Contract Termination";break;    
                case 11:str="for Variation Order";break;
                case 12:str="for Repeat Order";break;    
            }    
            if("Edit".equals(Action)){
                auditAction = "Edit Workflow "+str;
            }else{
                auditAction = "Create Workflow "+str;
            }
        }catch(Exception ex){
            logger.error("addWorkFlowLevel : " + logUserId + ex);
            if("Edit".equals(Action)){
                auditAction = "Error in Edit Workflow "+str;
            }else{
                auditAction = "Error in Create Workflow "+str;
            }
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,ObjectId , idType, EgpModule.WorkFlow.getName(), auditAction, "");
            auditAction = null;
        }
        logger.debug("addWorkFlowLevel : " + logUserId + " Ends");
        return listdata;
    }

    /**
     *  this is for to get workFlowlevel records based on
     *  objectid,childid,activityid.
     * @param objectid
     * @param childid
     * @param activityid
     * @return list from Tbl_WorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> editWorkFlowLevel(int objectid, int childid, short activityid) {
        logger.debug("editWorkFlowLevel : " + logUserId + " Starts");
        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConf = null;
        try {
            tblWorkFlowLevelConf = tblWorkFlowLevelConfigDao.findTblWorkFlowLevelConfig("tblActivityMaster",
                    Operation_enum.EQ, new TblActivityMaster(activityid),
                    "objectId", Operation_enum.EQ, objectid, "childId",
                    Operation_enum.EQ, childid);

        } catch (Exception ex) {
            logger.error("editWorkFlowLevel : " + logUserId + ex);
        }
        logger.debug("editWorkFlowLevel : " + logUserId + " Ends");
        return tblWorkFlowLevelConf;
    }

    /**
     * this is for getting department and designation names
     * based on userid.
     * @param userid from tbl_LoginMaster
     * @return employ name and designation name
     */
    public String getOfficialAndDesignationName(int userid) {
        logger.debug("getOfficialAndDesignationName : " + logUserId + " Starts");
        TblEmployeeMaster employeeMaster = null;
        TblDesignationMaster designationMaster = null;
        String employName = "";
        String designationName = "";
        String OfficeName ="";
        int empId=0;
        try {
            List<TblEmployeeMaster> tblEmployeeMaster = tblEmployeeMasterDao.findEmployee("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userid));
            employeeMaster = tblEmployeeMaster.get(0);
            designationMaster =
                    employeeMaster.getTblDesignationMaster();            
        empId = employeeMaster.getEmployeeId();    /* Dohatec Start*/
                try {   List<Object> list = null;
                        String sqlQuery = " Select eo.tblOfficeMaster.officeName from TblEmployeeOffices eo "
                        + " where eo.tblEmployeeMaster.employeeId = " + Integer.toString(empId)  ;
                        list = hibernateQueryDao.singleColQuery(sqlQuery);
                        if(!list.isEmpty()){ OfficeName = list.get(0).toString(); }
                    } catch (Exception ex) {
                        logger.error("getGovtEmpOfficeData : "+logUserId+" : "+ex.toString());
                     }                            /* Dohatec End*/
        } catch (Exception ex) {
            logger.error("getOfficialAndDesignationName : " + logUserId + ex);
        }
        employName = employeeMaster.getEmployeeName();
        designationName = designationMaster.getDesignationName();
        logger.debug("getOfficialAndDesignationName : " + logUserId + " Ends");
        return employName + ", [" + designationName + " at " + OfficeName + "]"; // Modified By Dohatec
    }

    /**
     * this is for getting department and designation names
     * @param userid from tbl_LoginMaster
     * @return employ name and designation name
     */
    public String getOfficeNameAndDesignationByDP(int userid) {
        logger.debug("getOfficeNameAndDesignationByDP : " + logUserId + " Starts");
        List<TblPartnerAdmin> partnerAdmin = null;
        TblPartnerAdmin padmin;
        String employName = "";
        String designationName = "";
        try {
            partnerAdmin = tblPartnerAdminDao.findTblPartnerAdmin("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userid));
            if (partnerAdmin.size() > 0) {
                padmin = (TblPartnerAdmin) partnerAdmin.get(0);
                employName = padmin.getFullName();
                designationName = padmin.getDesignation();
            }
        } catch (Exception ex) {
            logger.error("getOfficeNameAndDesignationByDP : " + logUserId + ex);
        }
        logger.debug("getOfficeNameAndDesignationByDP : " + logUserId + " Ends");
        return employName + "," + designationName;
    }

    /**
     * this is for saving workflow documents into database.
     * @param tblWorkFlowDocuments list to be added
     */
    public void upLoadWorkFlowFile(TblWorkFlowDocuments tblWorkFlowDocuments) {
        tblWorkFlowDocumentDao.addEntity(tblWorkFlowDocuments);
    }

    /**
     * this is for fetching all wfdocuments based on userid,acivityid,
     * childid,objectid.
     * @param activityid from tbl_WorkFlowDocuments
     * @param childid from tbl_WorkFlowDocuments
     * @param objectid from tbl_WorkFlowDocuments
     * @param userid from tbl_WorkFlowDocuments
     * @return list of tbl_WorkFlowDocuments
     */
    public List<TblWorkFlowDocuments> getWorkFlowDocuments(int activityid, int childid,
            int objectid, int userid) {
        logger.debug("getWorkFlowDocuments : " + logUserId + " Starts");
        List<TblWorkFlowDocuments> tblWorkFlowDocuments = null;
        try {
            tblWorkFlowDocuments = tblWorkFlowDocumentDao.findTblWorkFlowDocuments("objectId", Operation_enum.EQ, objectid, "childId", Operation_enum.EQ, childid, "userId", Operation_enum.EQ, userid, "activityId", Operation_enum.EQ, activityid);

        } catch (Exception ex) {
            logger.error("getWorkFlowDocuments : " + logUserId + ex);
        }
        logger.debug("getWorkFlowDocuments : " + logUserId + " Ends");
        return tblWorkFlowDocuments;
    }

    /**
     * Fetching details from tbl_WorkFlowDocuments
     * @param docIds - it is comma separated wfDocumentId
     * @return list of tbl_WorkFlowDocuments
     */
    public List<TblWorkFlowDocuments> getDocumetnsByIds(String docIds) {
        logger.debug("getDocumetnsByIds : " + logUserId + " Starts");
        List<TblWorkFlowDocuments> tblWorkFlowDocuments = null;
        try {
            String[] str = docIds.split(",");
            Object[] obj = new Object[str.length];
            for (int i = 0; i < str.length; i++) {
                obj[i] = Integer.valueOf(str[i]);
            }

            tblWorkFlowDocuments = tblWorkFlowDocumentDao.findTblWorkFlowDocuments("wfDocumentId", Operation_enum.IN, obj);
        } catch (Exception ex) {
            logger.error("getDocumetnsByIds : " + logUserId + ex);
        }
        logger.debug("getDocumetnsByIds : " + logUserId + " Ends");
        return tblWorkFlowDocuments;
    }

    /**
     * this is for checking is publisdatereq checkbox is checked or not
     * @param eventid from tbl_WorkflowRuleEngine
     * @return list of tbl_WorkflowRuleEngine
     */
    public List<TblWorkflowRuleEngine> isPublishDateReq(short eventid) {
        logger.debug("isPublishDateReq : " + logUserId + " Starts");
        List<TblWorkflowRuleEngine> tblWorkFlowRuleEngine = null;
        try {
            tblWorkFlowRuleEngine = tblWorkFlowRuleEngineDao.findTblWorkflowRuleEngine("eventId", Operation_enum.EQ, eventid);
        } catch (Exception ex) {
            logger.error("isPublishDateReq : " + logUserId + ex);
        }
        logger.debug("isPublishDateReq : " + logUserId + " Ends");
        return tblWorkFlowRuleEngine;
    }

    /**
     * this is for fetching workflowlevelconfig objects based on
     * userid,eventid,objectid,childid.
     * @param userid from tbl_LoginMaster
     * @param eventid from tbl_WorkFlowLevelConfig
     * @param objectid from tbl_WorkFlowLevelConfig
     * @param childid from tbl_WorkFlowLevelConfig
     * @return list of tbl_WorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getWorkFlowLevelConfig(int userid,
            short eventid, int objectid, int childid) {
        logger.debug("getWorkFlowLevelConfig : " + logUserId + " Starts");
        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
        try {

            tblWorkFlowLevelConfig = tblWorkFlowLevelConfigDao.findTblWorkFlowLevelConfig("tblLoginMaster",
                    Operation_enum.EQ, new TblLoginMaster(userid), "objectId",
                    Operation_enum.EQ, objectid, "childId", Operation_enum.EQ,
                    childid, "eventId", Operation_enum.EQ, eventid);
        } catch (Exception ex) {
            logger.error("getWorkFlowLevelConfig : " + logUserId + ex);
        }
        logger.debug("getWorkFlowLevelConfig : " + logUserId + " Ends");
        return tblWorkFlowLevelConfig;
    }

    /**
     * this method is for fetching all wfHistory based on
     * current login user
     * @param ViewType
     * @param ActivityId
     * @param ObjectId
     * @param ChildId
     * @param UserId
     * @param Page
     * @param RecordPerPage
     * @param sidx 
     * @param sord
     * @return list of CommonSPWfFileHistory
     */
    public List<CommonSPWfFileHistory> getWfHistory(String ViewType, Integer ActivityId,
            Integer ObjectId, Integer ChildId, Integer UserId, Integer Page,
            Integer RecordPerPage, String sidx, String sord, String moduleName, String eventName, String processBy, Integer objectId, String FromProcessDate, String FromProcessTo) {
        logger.debug("getWfHistory : " + logUserId + " Starts");
        List<CommonSPWfFileHistory> commonSpWfFileHistory = null;
        String str = "";
        String auditAction = "";
        String idType = "";        
        try{
        if(ActivityId==1){
           idType = "appId";
        }else if(ActivityId==10 || ActivityId==11 || ActivityId==12){
           idType = "contractId"; 
        }else {
           idType = "tenderId"; 
        }
        switch(ActivityId)
            {
                case 1:str="for App";break;
                case 2:str="for Tender Notice";break;
                case 3:str="for PreTender Meeting";break;
                case 4:str="for Amendment Approval";break;
                case 5:str="for Tender Opening";break;
                case 6:str="for Evaluation Committee";break;
                case 7:str="for Package Approve";break;    
                case 8:str="for Technical Sub Committee";break;    
                case 9:str="for Cancel Tender";break;    
                case 10:str="for Contract Termination";break;    
                case 11:str="for Variation Order";break;
                case 12:str="for Repeat Order";break;    
            }    
            if("history".equals(ViewType)){
                auditAction = "View Workflow history "+str;
            }
            }catch(Exception ex){
                logger.error("addWorkFlowLevel : " + logUserId + ex);
                if("history".equals(ViewType)){
                    auditAction = "View Workflow history "+str;
                }
        }finally{
            if("history".equals(ViewType)){
            makeAuditTrailService.generateAudit(auditTrail,ObjectId , idType, EgpModule.WorkFlow.getName(), auditAction, "");
            auditAction = null;}
        }
        SPWfFileHistory spWfFileHistory = new SPWfFileHistory(basicDataSource, wffilehistory);
        commonSpWfFileHistory = spWfFileHistory.executeProcedure(ViewType, ActivityId,
                ObjectId, ChildId, UserId, Page, RecordPerPage, sidx, sord, moduleName, eventName, processBy, objectId, FromProcessDate, FromProcessTo);

        logger.debug("getWfHistory : " + logUserId + " Ends");
        return commonSpWfFileHistory;
    }

    /**
     * this method is for saving fileprocessing data into
     * database
     * @param activityId
     * @param objectId
     * @param childId
     * @param fromUserId
     * @param action
     * @param remarks
     * @param levelid
     * @param wftrigger
     * @param pubDt
     * @param documentId
     * @return list of CommonMsgChk
     */
    public List<CommonMsgChk> addUpdWffileprocessing(Integer activityId, Integer objectId,
            Integer childId, Integer fromUserId, String action, String remarks,
            Integer levelid, String wftrigger, Date pubDt, String documentId) {
        logger.debug("addUpdWffileprocessing : " + logUserId + " Starts");
        List<CommonMsgChk> commonMsg = null;
        try{
            SPWfFileOnHand spWfFileOnhand = new SPWfFileOnHand(basicDataSource, addUpdWffile);
            commonMsg = spWfFileOnhand.exeProcedure(activityId, objectId, childId, fromUserId,
                    action, remarks, levelid, wftrigger, pubDt, documentId);
        }catch(Exception ex){
            logger.error("addUpdWffileprocessing : " + logUserId + ex);
        }
        logger.debug("addUpdWffileprocessing : " + logUserId + " Ends");
        return commonMsg;

    }

    /**
     * Get Work flow level config data by object id activity id child id and userid
     * @param objectid from tbl_WorkFlowLevelConfig
     * @param activityid from tbl_ActivityMaster
     * @param childid from tbl_WorkFlowLevelConfig
     * @param wflevel from tbl_WorkFlowLevelConfig
     * @param userid from tbl_LoginMaster
     * @return list from tbl_WorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getWorkFlowConfig(int objectid,
            short activityid, int childid, short wflevel, int userid) {
        logger.debug("getWorkFlowConfig : " + logUserId + " Starts");
        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
        try {

            tblWorkFlowLevelConfig = tblWorkFlowLevelConfigDao.findTblWorkFlowLevelConfig("objectId",
                    Operation_enum.EQ, objectid, "childId", Operation_enum.EQ, childid,
                    "tblActivityMaster", Operation_enum.EQ, new TblActivityMaster(activityid),
                    "wfLevel", Operation_enum.EQ, wflevel, "tblLoginMaster",
                    Operation_enum.EQ, new TblLoginMaster(userid));
        } catch (Exception ex) {
            logger.error("getWorkFlowConfig : " + logUserId + ex);
        }
        logger.debug("getWorkFlowConfig : " + logUserId + " Ends");
        return tblWorkFlowLevelConfig;
    }

    /**
     * this method is for delete document from path and regarding record in database.
     * @param tblWorkFlowDocument
     */
    public void deleteDoc(TblWorkFlowDocuments tblWorkFlowDocument) {

        tblWorkFlowDocumentDao.deleteTblWorkFlowDocuments(tblWorkFlowDocument);

    }

    /**
     * @return the basicDataSource
     */
    public BasicDataSource getBasicDataSource() {
        return basicDataSource;
    }

    /**
     * @param basicDataSource the basicDataSource to set
     */
    public void setBasicDataSource(BasicDataSource basicDataSource) {
        this.basicDataSource = basicDataSource;
    }

    /**
     * @return the workflowruleengine
     */
    public String getWorkflowruleengine() {
        return workflowruleengine;
    }

    /**
     * @param workflowruleengine the workflowruleengine to set
     */
    public void setWorkflowruleengine(String workflowruleengine) {
        this.workflowruleengine = workflowruleengine;
    }

    /**
     * @return the getappcommondata
     */
    public String getGetappcommondata() {
        return getappcommondata;
    }

    /**
     * @param getappcommondata the getappcommondata to set
     */
    public void setGetappcommondata(String getappcommondata) {
        this.getappcommondata = getappcommondata;
    }

    /**
     * @return the workfloweventconfig
     */
    public String getWorkfloweventconfig() {
        return workfloweventconfig;
    }

    /**
     * @param workfloweventconfig the workfloweventconfig to set
     */
    public void setWorkfloweventconfig(String workfloweventconfig) {
        this.workfloweventconfig = workfloweventconfig;
    }

    /**
     * @return the tblWorkFlowEventConfigDao
     */
    public TblWorkFlowEventConfigImpl getTblWorkFlowEventConfigDao() {
        return tblWorkFlowEventConfigDao;
    }

    /**
     * @param tblWorkFlowEventConfigDao the tblWorkFlowEventConfigDao to set
     */
    public void setTblWorkFlowEventConfigDao(TblWorkFlowEventConfigImpl tblWorkFlowEventConfigDao) {
        this.tblWorkFlowEventConfigDao = tblWorkFlowEventConfigDao;
    }

    /**
     * @return the workflowlevelconfig
     */
    public String getWorkflowlevelconfig() {
        return workflowlevelconfig;
    }

    /**
     * @param workflowlevelconfig the workflowlevelconfig to set
     */
    public void setWorkflowlevelconfig(String workflowlevelconfig) {
        this.workflowlevelconfig = workflowlevelconfig;
    }

    /**
     * @return the tblWorkFlowLevelConfigDao
     */
    public TblWorkFlowLevelConfigImpl getTblWorkFlowLevelConfigDao() {
        return tblWorkFlowLevelConfigDao;
    }

    /**
     * @param tblWorkFlowLevelConfigDao the tblWorkFlowLevelConfigDao to set
     */
    public void setTblWorkFlowLevelConfigDao(TblWorkFlowLevelConfigImpl tblWorkFlowLevelConfigDao) {
        this.tblWorkFlowLevelConfigDao = tblWorkFlowLevelConfigDao;
    }

    /**
     * @return the tblEmployeeMasterDao
     */
    public TblEmployeeMasterImpl getTblEmployeeMasterDao() {
        return tblEmployeeMasterDao;
    }

    /**
     * @param tblEmployeeMasterDao the tblEmployeeMasterDao to set
     */
    public void setTblEmployeeMasterDao(TblEmployeeMasterImpl tblEmployeeMasterDao) {
        this.tblEmployeeMasterDao = tblEmployeeMasterDao;
    }

    /**
     * @return the tblDesignationMasterDao
     */
    public TblDesignationMasterImpl getTblDesignationMasterDao() {
        return tblDesignationMasterDao;
    }

    /**
     * @param tblDesignationMasterDao the tblDesignationMasterDao to set
     */
    public void setTblDesignationMasterDao(TblDesignationMasterImpl tblDesignationMasterDao) {
        this.tblDesignationMasterDao = tblDesignationMasterDao;
    }

    /**
     * @return the tblWorkFlowDocumentDao
     */
    public TblWorkFlowDocumentsImpl getTblWorkFlowDocumentDao() {
        return tblWorkFlowDocumentDao;
    }

    /**
     * @param tblWorkFlowDocumentDao the tblWorkFlowDocumentDao to set
     */
    public void setTblWorkFlowDocumentDao(TblWorkFlowDocumentsImpl tblWorkFlowDocumentDao) {
        this.tblWorkFlowDocumentDao = tblWorkFlowDocumentDao;
    }

    /**
     * @return the tblWorkFlowRuleEngineDao
     */
    public TblWorkFlowRuleEngineImpl getTblWorkFlowRuleEngineDao() {
        return tblWorkFlowRuleEngineDao;
    }

    /**
     * @param tblWorkFlowRuleEngineDao the tblWorkFlowRuleEngineDao to set
     */
    public void setTblWorkFlowRuleEngineDao(TblWorkFlowRuleEngineImpl tblWorkFlowRuleEngineDao) {
        this.tblWorkFlowRuleEngineDao = tblWorkFlowRuleEngineDao;
    }

    /**
     * @return the wffilehistory
     */
    public String getWffilehistory() {
        return wffilehistory;
    }

    /**
     * @param wffilehistory the wffilehistory to set
     */
    public void setWffilehistory(String wffilehistory) {
        this.wffilehistory = wffilehistory;
    }

    /**
     * @return the addUpdWffile
     */
    public String getAddUpdWffile() {
        return addUpdWffile;
    }

    /**
     * @param addUpdWffile the addUpdWffile to set
     */
    public void setAddUpdWffile(String addUpdWffile) {
        this.addUpdWffile = addUpdWffile;
    }

    /**
     * Get Work flow History by work flow Id.
     * @param hisid - wfHistoryId from tbl_WorkFlowFileHistory
     * @return list of TblWorkFlowFileHistory
     */
    public List<TblWorkFlowFileHistory> getWfFileHistoryById(int hisid) {
        logger.debug("getWfFileHistoryById : " + logUserId + " Starts");
        List<TblWorkFlowFileHistory> tblWorkFlowFileHistorys = null;
        try {
            tblWorkFlowFileHistorys = tblWorkFlowFileHistoryDao.findTblWorkFlowFileHistory("wfHistoryId", Operation_enum.EQ, hisid);
        } catch (Exception ex) {
            logger.error("getWfFileHistoryById : " + logUserId + ex);
        }
        logger.debug("getWfFileHistoryById : " + logUserId + " Ends");
        return tblWorkFlowFileHistorys;
    }

    /**
     * @return the tblWorkFlowFileHistoryDao
     */
    public TblWorkFlowFileHistoryImpl getTblWorkFlowFileHistoryDao() {
        return tblWorkFlowFileHistoryDao;
    }

    /**
     * @param tblWorkFlowFileHistoryDao the tblWorkFlowFileHistoryDao to set
     */
    public void setTblWorkFlowFileHistoryDao(TblWorkFlowFileHistoryImpl tblWorkFlowFileHistoryDao) {
        this.tblWorkFlowFileHistoryDao = tblWorkFlowFileHistoryDao;
    }

    /**
     * @return the tblPartnerAdminDao
     */
    public TblPartnerAdminImpl getTblPartnerAdminDao() {
        return tblPartnerAdminDao;
    }

    /**
     * @param tblPartnerAdminDao the tblPartnerAdminDao to set
     */
    public void setTblPartnerAdminDao(TblPartnerAdminImpl tblPartnerAdminDao) {
        this.tblPartnerAdminDao = tblPartnerAdminDao;
    }

    /**
     * @return the tblAppMasterDao
     */
    public TblAppMasterImpl getTblAppMasterDao() {
        return tblAppMasterDao;
    }

    /**
     * @param tblAppMasterDao the tblAppMasterDao to set
     */
    public void setTblAppMasterDao(TblAppMasterImpl tblAppMasterDao) {
        this.tblAppMasterDao = tblAppMasterDao;
    }

    /**
     * Check Revenue by Approve tender Id.
     * @param appOrTenId appId from Tbl_AppMaster
     * @return list of TblAppMaster
     */
    public List<TblAppMaster> checkRevinueByApporTenId(int appOrTenId) {
        logger.debug("checkRevinueByApporTenId : " + logUserId + " Starts");
        List<TblAppMaster> appMaster = new ArrayList<TblAppMaster>();
        try {
            appMaster = tblAppMasterDao.findTblAppMaster("appId", Operation_enum.EQ, appOrTenId);

        } catch (Exception ex) {
            logger.error("checkRevinueByApporTenId : " + logUserId + ex);
        }
        logger.debug("checkRevinueByApporTenId : " + logUserId + " Ends");
        return appMaster;
    }

    /**
     * @return the tblTenderDetailsDao
     */
    public TblTenderDetailsImpl getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    /**
     * @param tblTenderDetailsDao the tblTenderDetailsDao to set
     */
    public void setTblTenderDetailsDao(TblTenderDetailsImpl tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    /**
     * Check Submission date of tenderId.
     * @param tenderId from tbl_TenderMaster
     * @return list of TblTenderDetails
     */
    public List<TblTenderDetails> checkSubmissionDate(int tenderId) {
        logger.debug("checkSubmissionDate : " + logUserId + " Starts");
        List<TblTenderDetails> tenderDetails = null;
        try {
            tenderDetails = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception ex) {
            logger.error("checkSubmissionDate : " + logUserId + ex);
        }
        logger.debug("checkSubmissionDate : " + logUserId + " Ends");
        return tenderDetails;
    }

    /**
     * fetching info from tbl_ProcurementRole
     * @param proId - procurementRoleId from tbl_ProcurementRole
     * @return list of TblProcurementRole
     */
    public List<TblProcurementRole> getProcurementById(byte proId) {
        logger.debug("getProcurementById : " + logUserId + " Starts");
        List<TblProcurementRole> tblProcurementRoles = null;
        try {
            tblProcurementRoles = tblProcurementRoleDao.findTblProcurementRole("procurementRoleId", Operation_enum.EQ, proId);
        } catch (Exception ex) {
            logger.error("getProcurementById : " + logUserId + ex);
        }
        logger.debug("getProcurementById : " + logUserId + " Ends");
        return tblProcurementRoles;
    }

    /**
     * Delete all work flow levels.
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @return 0 if not deleted successfully
     */
    public int flushWorkflowLevels(int objectId, int childId, short activityId) {
        logger.debug("flushWorkflowLevels: " + logUserId + " Starts");
        int ret = 0;
        ret = tblWorkFlowLevelConfigDao.updateDeleteSQLQuery("delete from tbl_WorkFlowLevelConfig where objectId=" + objectId + " and childId=" + childId + " and activityId=" + activityId);
        logger.debug("flushWorkflowLevels : " + logUserId + " Ends");
        return ret;
    }

    /**
     * count number of records from tbl_WorkFlowLevelConfig
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @return number of records
     */
    public long getCountOfWorkflowlevelConfig(int objectId, int childId, short activityId) {
        logger.debug("getCountOfWorkflowlevelConfig : " + logUserId + " Starts");
        long cont = 0;
        try {
            cont = tblWorkFlowLevelConfigDao.countForQuery("TblWorkFlowLevelConfig", "objectId=" + objectId + " and childId=" + childId + " and activityId=" + activityId);

        } catch (Exception ex) {
            logger.error("getCountOfWorkflowlevelConfig : " + logUserId + ex);
        }
        logger.debug("getCountOfWorkflowlevelConfig : " + logUserId + " Ends");
        return cont;
    }

    /**
     * Delete all Work flow levels.
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @param wflevel from tbl_WorkFlowLevelConfig
     * @return 0 if not deleted successfully
     */
    public int flushWorkflowLevels(int objectId, int childId, short activityId, short wflevel) {
        logger.debug("flushWorkflowLevels : " + logUserId + " Starts");
        int ret = 0;
        ret = tblWorkFlowLevelConfigDao.updateDeleteSQLQuery("delete from tbl_WorkFlowLevelConfig where objectId=" + objectId + " and childId=" + childId + " and activityId=" + activityId + " and wfLevel=" + wflevel);
        logger.debug("flushWorkflowLevels : " + logUserId + " Ends");
        return ret;
    }

    /**
     * get details from tbl_WorkFlowLevelConfig
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_ActivityMaster
     * @param wflevels from tbl_WorkFlowLevelConfig
     * @return list of TblWorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getwflevels(int objectId, int childId, short activityId, String wflevels) {
        logger.debug("getwflevels : " + logUserId + " Starts");
        List<TblWorkFlowLevelConfig> tblwflconfig = null;
        try {
            String arr[] = wflevels.split(",");
            Object[] objects = new Object[arr.length];
            for (int i = 0; i < arr.length; i++) {
                objects[i] = Short.valueOf(arr[i]);
            }
            tblwflconfig = tblWorkFlowLevelConfigDao.findTblWorkFlowLevelConfig("objectId",
                    Operation_enum.EQ, objectId, "childId", Operation_enum.EQ, childId,
                    "tblActivityMaster", Operation_enum.EQ, new TblActivityMaster(activityId), "wfLevel", Operation_enum.IN, objects);

        } catch (Exception ex) {
            logger.error("getwflevels : " + logUserId + ex);
        }
        logger.debug("getwflevels : " + logUserId + " Ends");
        return tblwflconfig;
    }

    /**
     * Check is Donor condition required or not.
     * @param activityId
     * @param if activityId = 1 then
     * objectId - appId from Tbl_AppMaster
     * else objectId - tenderId from Tbl_TenderMaster
     * @return is Donor condition required or not.
     */
    public boolean isDonorCond(int activityId, int objectId) {

        logger.debug("isDonorCond : " + logUserId + " Starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            if (activityId == 1) {
                String sqlQuery = "Select pm.projectName from TblProjectMaster pm, TblAppMaster ap where pm.projectId = ap.projectId "
                        + "and ap.appId =" + objectId + " and pm.sourceOfFund like '%aid%'";
                list = hibernateQueryDao.singleColQuery(sqlQuery);

                if (list.size() > 0) {
                    flag = true;
                }
            } else {
                String sqlQuery = "Select pm.projectName from TblProjectMaster pm, TblAppMaster ap, TblTenderMaster tm  "
                        + "where pm.projectId = ap.projectId and "
                        + "tm.appId = ap.appId "
                        + "and tm.tenderId =" + objectId + " and pm.sourceOfFund like '%aid%'";
                list = hibernateQueryDao.singleColQuery(sqlQuery);

                if (list.size() > 0) {
                    flag = true;
                }
            }
        } catch (Exception ex) {
            logger.error("isDonorCond : " + logUserId + ex.toString());
        }
        logger.debug("isDonorCond : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public int getWorkFlowLevelUser(short eventId, short wfLevel, short actId, int objId) {

        logger.debug("getWorkFlowLevelUser : " + logUserId + " Starts");
        List<TblWorkFlowLevelConfig> tblwflconfig = null;
        int userId = 0;
        try {
            tblwflconfig = tblWorkFlowLevelConfigDao.findTblWorkFlowLevelConfig("eventId", Operation_enum.EQ, eventId, "wfLevel", Operation_enum.EQ, wfLevel, "tblActivityMaster", Operation_enum.EQ, new TblActivityMaster(actId), "objectId", Operation_enum.EQ, objId);
            if (tblwflconfig.size() > 0) {
                TblLoginMaster tblLoginMaster = tblwflconfig.get(0).getTblLoginMaster();
                userId = tblLoginMaster.getUserId();
            }
        } catch (Exception ex) {
            logger.error("getWorkFlowLevelUser : " + logUserId + ex);
        }
        logger.debug("getWorkFlowLevelUser : " + logUserId + " Ends");
        return userId;

    }

    @Override
    public boolean getWorkFlowRuleEngineData(int eventId) {
        logger.debug("getWorkFlowRuleEngineData : " + logUserId + " Starts");
        boolean flag = false;
        try {            
            List<TblWorkflowRuleEngine> list = null;
            list = tblWorkFlowRuleEngineDao.findTblWorkflowRuleEngine("eventId",Operation_enum.EQ,(short)eventId);
            if(list!=null && !list.isEmpty())
            {
                flag = true;
            }            
        } catch (Exception e) {
            logger.error("getWorkFlowRuleEngineData : " + logUserId + e);
        }
        logger.debug("getWorkFlowRuleEngineData : " + logUserId + " Ends");
        return flag;
    }
}
