/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import com.cptu.egp.eps.model.table.TblCmsDateConfigHistory;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import java.util.Date;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface CmsConfigDateService {

    /**
     * for Setting userId at Service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
    /**
     * add commencement date data to database
     * @param tblCmsDateConfig
     * @return boolean, true - if added successfully otherwise false
     */
    public boolean addConfigDates(TblCmsDateConfig tblCmsDateConfig);

    /**
     * add commencement date history data to database
     * @param tblCmsDateConfigHistory
     * @return boolean, true - if added successfully otherwise false
     */
    public boolean addConfigDatesHistory(TblCmsDateConfigHistory tblCmsDateConfigHistory);

    /**
     * gives Noa issue Details data
     * @param tenderId
     * @param lotId
     * @return list of Objects
     */
    public List<Object[]> getNoaissueDetails(int tenderId,int lotId);

    /**
     * gives Noa issue Details data in repeat Order Case
     * @param tenderId
     * @param lotId
     * @param roundId
     * @return list of Objects
     */
    public List<Object[]> getNoaissueDetailsForRO(int tenderId,int lotId,int roundId);

    /**
     * gives Tender Lot Security data in repeat Order Case
     * @param tenderId
     * @param lotId
     * @return list data
     */
    public List<TblTenderLotSecurity> getTenderLotSecurity(int tenderId,int lotId);

    /**
     * updates Commencement date data to database
     * @param tblCmsDateConfig
     * @return boolean, true - if updated successfully otherwise false
     */
    public boolean updateConfigDates(TblCmsDateConfig tblCmsDateConfig);

    /**
     * gives Commencement date data from database
     * @param tenderId
     * @param lotId
     * @param cntId
     * @return list data
     */
    public List<TblCmsDateConfig> getConfigDatesdata(int tenderId,int lotId,int cntId);

    /**
     * gives Commencement date data from database
     * @param tenderId
     * @param lotId
     * @return list data
     */
    public List<TblCmsDateConfig> getConfigDatesdata(int tenderId,int lotId);

    /**
     * gives Max WP Date
     * @param lotid
     * @return list of objects 
     */
    public List<Object[]> getmaxWpDate(int lotid);

    /**
     * gives Contract Info bar Details data 
     * @param tenderId
     * @param lotid
     * @return list of objects 
     */
    public List<Object[]> getContractInfoBar(int tenderId,int lotid);

    //Code Start by Proshanto Kumar Saha,Dohatec
     /**
     * gives Contract Info bar Details data without Contract Sign
     * @param tenderId
     * @param lotid
     * @return list of objects
     */
    
    public List<Object[]> getContractInfoBarWithoutCSign(int tenderId,int lotid);
    //Code End by Proshanto Kumar Saha,Dohatec

    /**
     * gives Contract Info bar Details data
     * @param tenderId
     * @param lotid
     * @param cntId
     * @return list of objects
     */
    public List<Object[]> getContractInfoBar(int tenderId,int lotid,int cntId);

    /**
     * gives Commencement date history data from database
     * @param actContractDtId
     * @return list data
     */
    public List<TblCmsDateConfigHistory> getCmsDateConfigHistorydata(int actContractDtId);

    /**
     * gives Commencement date data from database
     * @param lotId
     * @return boolean, return true - if data exist otherwise false
     */
    public boolean getConfigDatesdatabyPassingLotId(int lotId);

    /**
     * gives Commencement date data from database
     * @param lotId
     * @param cntId
     * @return boolean, return true - if data exist otherwise false
     */
    public boolean getConfigDatesdatabyPassingLotId(int lotId,int cntId);

    /**
     * gives Contract Signing data from database
     * @param Contractid
     * @return list data
     */
    public List<TblContractSign> getContractSigningDate(int Contractid);

    /**
     * gives PE name by passing userId
     * @param userId
     * @return list object
     */
    public List<Object> getPeName(int userId);

    /**
     * gives Tentative Dates for Service case
     * @param tenderId
     * @return list of objects 
     */
    public List<Object[]> getTentativeDatesInServicesCase(int tenderId);

    /**
     * get NOa Issue Id from database
     * @param ContractId
     * @return string 
     */
    public String getNoaId(int ContractId);

    /**
     * gives app creation dates from databse
     * @param tenderId
     * @return list of objects 
     */
    public List<Object[]> getAPPDates(int tenderId);
}
