/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsCtcertDocService {

    public void setLogUserId(String logUserId);

    public int insertCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public boolean updateCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public boolean deleteCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public List<TblCmsCtcertDoc> getAllCmsCtcertDoc();

    public long getCmsCtcertDocCount();

    public TblCmsCtcertDoc getCmsCtcertDoc(int id);

    public List<TblCmsCtcertDoc> getAllCmsCtcertDocForCtId(int contractTeminationId);

}
