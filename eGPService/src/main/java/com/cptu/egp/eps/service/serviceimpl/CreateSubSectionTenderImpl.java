/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderIttHeaderDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.service.serviceinterface.CreateSubSectionTender;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>CreateSubSectionTenderImpl</b> <Description Goes Here> Dec 1, 2010 9:24:48 PM
 * @author yanki
 */
public class CreateSubSectionTenderImpl implements CreateSubSectionTender {
    String logUserId = "0";
    final Logger logger = Logger.getLogger(CreateSubSectionTenderImpl.class);

    public TblTenderIttHeaderDao getTblTenderIttHeaderDao()
    {
        return tblTenderIttHeaderDao;
    }

    public void setTblTenderIttHeaderDao(TblTenderIttHeaderDao tblTenderIttHeaderDao)
    {
        this.tblTenderIttHeaderDao = tblTenderIttHeaderDao;
    }

    

    TblTenderIttHeaderDao tblTenderIttHeaderDao;

    @Override
    public boolean createSubSection(TblTenderIttHeader tblIttHeader)
    {
        logger.debug("createSubSection : "+logUserId+" Starts");
        boolean loginFlag = false;
        try {
            tblTenderIttHeaderDao.addTblTenderIttHeader(tblIttHeader);
            loginFlag = true;
        }
        catch (Exception ex) {
            logger.error("createSubSection : "+logUserId+" :"+ex);
            loginFlag = false;
        }
        logger.debug("createSubSection : "+logUserId+" Ends");
        return loginFlag;
    }

    @Override
    public List<TblTenderIttHeader> getSubSection(int sectionId)
    {
        logger.debug("getSubSection : "+logUserId+" Starts");
        List subSection;
        try {
            subSection = tblTenderIttHeaderDao.findTblTenderIttHeader("tblTenderSection", Operation_enum.EQ, new TblTenderSection(sectionId));
        }
        catch (Exception ex) {
            subSection = null;
            logger.error("getSubSection : "+logUserId+" :"+ex);
        }
        logger.debug("getSubSection : "+logUserId+" Ends");
        return subSection;
    }

    @Override
    public List<Object[]> getSubSectionWithTDSAppl(int sectionId)
    {
        logger.debug("getSubSectionWithTDSAppl : "+logUserId+" Starts");
        List subSection;
        try {
            String qry = "select distinct ittHeader.tenderIttHeaderId, ittHeader.ittHeaderName, ittHeader.templateIttHeaderId from TblTenderIttHeader ittHeader, " +
                    "TblTenderIttClause ittClause, TblTenderIttSubClause subClause where ittHeader.tblTenderSection.tenderSectionId = '" +
                    sectionId + "' " + "and subClause.isTdsApplicable = 'yes' and ittHeader.tenderIttHeaderId = " +
                    "ittClause.tblTenderIttHeader.tenderIttHeaderId and ittClause.tenderIttClauseId = subClause.tblTenderIttClause.tenderIttClauseId order by ittHeader.templateIttHeaderId";
            logger.debug("getSubSectionWithTDSAppl  : "+qry);
            subSection = tblTenderIttHeaderDao.createQuery(qry);
        }
        catch (Exception ex) {
            subSection = null;
            logger.error("getSubSectionWithTDSAppl : "+logUserId+" Ends"+ex);
        }
        logger.debug("getSubSectionWithTDSAppl : "+logUserId+" Ends");
        return subSection;
    }

    @Override
    public boolean updateSubSection(TblTenderIttHeader tblIttHeader)
    {
        logger.debug("updateSubSection : "+logUserId+" Starts");
        boolean updateSubSection;
        try {
            tblTenderIttHeaderDao.updateEntity(tblIttHeader);
            updateSubSection = true;
        }
        catch (Exception ex) {
            updateSubSection = false;
            logger.error("updateSubSection : "+logUserId+" Ends"+ex);
        }
        logger.debug("updateSubSection : "+logUserId+" Ends");
        return updateSubSection;
    }

    @Override
    public boolean deleteSubSection(TblTenderIttHeader tblIttHeader)
    {
        logger.debug("deleteSubSection : "+logUserId+" Starts");
        boolean updateSubSection;
        try {
            tblTenderIttHeaderDao.deleteEntity(tblIttHeader);
            updateSubSection = true;
        }
        catch (Exception ex) {
            updateSubSection = false;
            logger.error("deleteSubSection : "+logUserId+" Starts"+ex);
        }
        logger.debug("deleteSubSection : "+logUserId+" Ends");
        return updateSubSection;
    }

    @Override
    public void setUserId(String logUserId) {
       this.logUserId = logUserId;
    }
}
