/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsVendorRatingMasterService {

    public void setLogUserId(String logUserId);

    public int insertCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public boolean updateCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public boolean deleteCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public List<TblCmsVendorRatingMaster> getAllCmsVendorRatingMaster();

    public long getCmsVendorRatingMasterCount();

    public TblCmsVendorRatingMaster getCmsVendorRatingMaster(int id);

    public TblCmsVendorRatingMaster getCmsVendorRatingMasterForRating(String rating);
}
