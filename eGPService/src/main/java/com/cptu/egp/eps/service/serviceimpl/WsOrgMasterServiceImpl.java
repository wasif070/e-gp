/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblWsOrgMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This service class used Database operations for TblWsOrgMaster
 * @author Sreenu
 */
public class WsOrgMasterServiceImpl implements WsOrgMasterService{

    final Logger logger = Logger.getLogger(WsOrgMasterServiceImpl.class);
    private HibernateQueryDao hibernateQueryDao;
    private TblWsOrgMasterDao tblWsOrgMasterDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblWsOrgMasterDao getTblWsOrgMasterDao() {
        return tblWsOrgMasterDao;
    }

    public void setTblWsOrgMasterDao(TblWsOrgMasterDao tblWsOrgMasterDao) {
        this.tblWsOrgMasterDao = tblWsOrgMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblWsOrgMaster object in database
     * @param TblWsOrgMaster tblWsOrgMaster
     * @return int 
     */
    @Override
    public int insertWsOrgMaster(TblWsOrgMaster tblWsOrgMaster) {
        logger.debug("insertWsOrgMaster : "+logUserId+STARTS);
        int flag = 0;
        String action = null;
        try{
            tblWsOrgMasterDao.addTblWsOrgMaster(tblWsOrgMaster);
            flag = tblWsOrgMaster.getOrgId();
            action = "Add Organization – Web Service Access";
        }catch(Exception ex){
            logger.error("insertWsOrgMaster : "+logUserId+" : "+ex);
            flag = 0;
            action = "Error in Add Organization – Web Service Access : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }
        logger.debug("insertWsOrgMaster : "+logUserId+ENDS);
        return flag;
    }

    /***
     * This method updates an TblWsOrgMaster object in DB
     * @param TblWsOrgMaster tblWsOrgMaster
     * @return boolean
     */
    @Override
    public boolean updateWsOrgMaster(TblWsOrgMaster tblWsOrgMaster,String ... actions) {
        logger.debug("updateWsOrgMaster : " + logUserId + STARTS);
        boolean flag;
        String action = null;
        try {
            tblWsOrgMasterDao.updateTblWsOrgMaster(tblWsOrgMaster);
            flag = true;
            action = (actions.length!=0 ? actions[0]: "") +" Organization – Web Service Access";
        } catch (Exception ex) {
            logger.error("updateWsOrgMaster : " + logUserId + " : " + ex);
            flag = false;
            action = "Error in "+((actions.length!=0 ? actions[0]: "") +" Organization – Web Service Access")+" : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }
        logger.debug("updateWsOrgMaster : " + logUserId + ENDS);
        return flag;
    }

    /***
     * This method deletes an TblWsOrgMaster object in DB
     * @param TblWsOrgMaster tblWsOrgMaster
     * @return boolean
     */
    @Override
    public boolean deleteWsOrgMaster(TblWsOrgMaster tblWsOrgMaster) {
        logger.debug("deleteWsOrgMaster : " + logUserId + STARTS);
        boolean flag;
        try {
            tblWsOrgMasterDao.deleteTblWsOrgMaster(tblWsOrgMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteWsOrgMaster : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteWsOrgMaster : " + logUserId + ENDS);
        return flag;
    }

    /***
     * This method returns all TblWsOrgMaster objects from database
     * @return List<TblWsOrgMaster>
     */
    @Override
    public List<TblWsOrgMaster> getAllWsOrgMaster() {
        logger.debug("getAllWsOrgMaster : " + logUserId + STARTS);
        List<TblWsOrgMaster> wsOrgMastersList = new ArrayList<TblWsOrgMaster>();
        try {
            wsOrgMastersList = tblWsOrgMasterDao.getAllTblWsOrgMaster();
        } catch (Exception ex) {
            logger.error("getAllWsOrgMaster : " + logUserId + " : " + ex);
        }
        logger.debug("getAllWsOrgMaster : " + logUserId + ENDS);
        return wsOrgMastersList;
    }

    /***
     *  This method returns no. of TblWsOrgMaster objects from database
     * @return long
     */
    @Override
    public long getWsOrgMasterCount() {
        logger.debug("getWsOrgMasterCount : " + logUserId + STARTS);
        long wsOrgMasterCount = 0;
        try {
            wsOrgMasterCount = tblWsOrgMasterDao.getCountTblWsOrgMaster();
        } catch (Exception ex) {
            logger.error("getWsOrgMasterCount : " + logUserId + " : " + ex);
        }
        logger.debug("getWsOrgMasterCount : " + logUserId + ENDS);
        return wsOrgMasterCount;
    }

    /***
     * This method returns TblWsOrgMaster for the given Id
     * @param int id
     * @return TblWsOrgMaster
     */
    @Override
    public TblWsOrgMaster getTblWsOrgMaster(int id) {
        logger.debug("getTblWsOrgMaster(int) : " + logUserId + STARTS);
        TblWsOrgMaster wsOrgMasters = null;
        List<TblWsOrgMaster> wsOrgMastersList = null;
        try {
             wsOrgMastersList = tblWsOrgMasterDao.findTblWsOrgMaster("orgId",
                     Operation_enum.EQ, id);
             
        } catch (Exception ex) {
            logger.error("getTblWsOrgMaster(int) : " + logUserId + " : " + ex);
        }
        if(!wsOrgMastersList.isEmpty()){
            wsOrgMasters = wsOrgMastersList.get(0);
        }
        logger.debug("getTblWsOrgMaster(int) : " + logUserId + ENDS);
        return wsOrgMasters;
    }

    /***
     * This method returns TblWsOrgMaster for the given emailId
     * @param String userEmailId
     * @return TblWsOrgMaster
     */
    @Override
    public TblWsOrgMaster getTblWsOrgMaster(String userEmailId) {
        logger.debug("getTblWsOrgMaster(String) : " + logUserId + STARTS);
        TblWsOrgMaster wsOrgMaster = null;
        List<TblWsOrgMaster> wsOrgMastersList = new ArrayList<TblWsOrgMaster>();
        try {
             wsOrgMastersList = tblWsOrgMasterDao.findTblWsOrgMaster("emailId",
                     Operation_enum.EQ, userEmailId);

        } catch (Exception ex) {
            logger.error("getTblWsOrgMaster(String) : " + logUserId + " : " + ex);
        }
        if(!wsOrgMastersList.isEmpty()){
            wsOrgMaster = wsOrgMastersList.get(0);
        }
        logger.debug("getTblWsOrgMaster(String) : " + logUserId + ENDS);
        return wsOrgMaster;
    }

    /***
     * This method gets the required objects from the database.
     * The object name used in the condition is <b>wsOrgMaster</b>.
     * Just pass the query condition to this method.
     * <b>Need to append the WHERE keyword to the above condition</b>
     * @param String whereCondition
     * @return List<TblWsOrgMaster>
     */
    @Override
    public List<TblWsOrgMaster> getRequiredTblWsOrgMasterList(String whereCondition) {
        logger.debug("getRequiredTblWsOrgMasterList : " + logUserId + STARTS);
        List<TblWsOrgMaster> wsOrgMastersList = new ArrayList<TblWsOrgMaster>();
        try {
             wsOrgMastersList = tblWsOrgMasterDao.getRequiredTblWsOrgMasterList(whereCondition);

        } catch (Exception ex) {
            logger.error("getRequiredTblWsOrgMasterList : " + logUserId + " : " + ex);
        }
        logger.debug("getRequiredTblWsOrgMasterList : " + logUserId + ENDS);
        return wsOrgMastersList;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

}
