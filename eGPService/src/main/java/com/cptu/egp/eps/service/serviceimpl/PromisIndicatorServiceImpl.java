/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblPromisIndicatorDao;
import com.cptu.egp.eps.model.table.TblPromisIndicator;
import com.cptu.egp.eps.service.serviceinterface.PromisIndicatorService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sudhir Chavhan
 */
public class PromisIndicatorServiceImpl implements PromisIndicatorService {

    private final Logger logger = Logger.getLogger(PromisIndicatorServiceImpl.class);
    TblPromisIndicatorDao tblPromisIndicatorDao;
    HibernateQueryDao hibernateQueryDao;

    @Override
    public TblPromisIndicatorDao getTblPromisIndicatorDao() {
        return tblPromisIndicatorDao;
    }

    public void setTblPromisIndicatorDao(TblPromisIndicatorDao tblPromisIndicatorDao) {
        this.tblPromisIndicatorDao = tblPromisIndicatorDao;
    }
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
/*
    @Override
    public void addTblPromisIndicator(TblPromisIndicator admin) {
        tblPromisIndicatorDao.addEntity(admin);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblPromisIndicator(TblPromisIndicator admin) {
        tblPromisIndicatorDao.deleteEntity(admin);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblPromisIndicator(TblPromisIndicator admin) {
        tblPromisIndicatorDao.updateEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }   */

    @Override
    public List<TblPromisIndicator> getAllTblPromisIndicator() {

        logger.debug("getAllTblPromisIndicator : "+" Starts");
        List<TblPromisIndicator> lstEntry = null;
        try{
            logger.debug("PromisIndicatorServiceImpl getAllTblPromisIndicator(1) ");
            lstEntry = tblPromisIndicatorDao.getAllTblPromisIndicator();
            logger.debug("PromisIndicatorServiceImpl getAllTblPromisIndicator(2) ");
        }catch(Exception ex){
            logger.error("Error in PromisIndicatorServiceImpl getAllTblPromisIndicator() " + ex.toString());
            logger.error("getAllTblPromisIndicator Error " + ex.toString());
        }
        logger.debug("getAllTblPromisIndicator : "+" Ends");
        return lstEntry;
    }
/*
    @Override
    public List<TblPromisIndicator> findTblPromisIndicator(Object... values) throws Exception {
         return tblPromisIndicatorDao.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPromisIndicator> findByCountTblPromisIndicator(int firstResult, int maxResult, Object... values) throws Exception {
         return tblPromisIndicatorDao.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getTblPromisIndicatorCount() {
         return tblPromisIndicatorDao.getEntityCount();
        // throw new UnsupportedOperationException("Not supported yet.");
    }
*/
}
