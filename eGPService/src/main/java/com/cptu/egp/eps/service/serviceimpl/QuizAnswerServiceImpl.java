/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblQuizAnswerDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblQuizAnswer;
import com.cptu.egp.eps.model.table.TblQuizQuestion;
import static com.cptu.egp.eps.service.serviceimpl.QuizAnswerServiceImpl.LOGGER;
import com.cptu.egp.eps.service.serviceinterface.QuizAnswerService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author feroz
 */
public class QuizAnswerServiceImpl implements QuizAnswerService{
    
    static final Logger LOGGER = Logger.getLogger(ConfigPaThresholdServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblQuizAnswerDao tblQuizAnswerDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    


    @Override
    public void addTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
        LOGGER.debug("addTblQuizAnswer: " + logUserId + LOGGERSTART);
            try {
            getTblQuizAnswerDao().addTblQuizAnswer(tblQuizAnswer);
            } catch (Exception e) {
                LOGGER.error("addTblQuizAnswer: " + logUserId + e);
            }
            LOGGER.debug("addTblQuizAnswer: " + logUserId + LOGGEREND);
    }

    @Override
    public void deleteTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
        LOGGER.debug("deleteTblQuizAnswer: " + logUserId + LOGGERSTART);
            try {
            getTblQuizAnswerDao().deleteTblQuizAnswer(tblQuizAnswer);
            } catch (Exception e) {
                LOGGER.error("deleteTblQuizAnswer: " + logUserId + e);
            }
            LOGGER.debug("deleteTblQuizAnswer: " + logUserId + LOGGEREND);
    }

    @Override
    public String updateTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
            boolean flag = false;
            LOGGER.debug("updateTblQuizAnswer: " + logUserId + LOGGERSTART);
            try {
            getTblQuizAnswerDao().updateTblQuizAnswer(tblQuizAnswer);
            flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblQuizAnswer: " + logUserId + e);
            }
            LOGGER.debug("updateTblQuizAnswer: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc";
            }
            else
            {
                return "failed";
            }
    }

    @Override
    public List<TblQuizAnswer> getAllTblQuizAnswer() {
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Starts");
        List<TblQuizAnswer> tblQuizAnswer = null;
        try {
            tblQuizAnswer = getTblQuizAnswerDao().getAllTblQuizAnswer();
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizAnswer : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Ends");
        return tblQuizAnswer;
    }

    @Override
    public List<TblQuizAnswer> findTblQuizAnswer(int id) {
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Starts");
        List<TblQuizAnswer> tblQuizAnswer = null;
        try {
            tblQuizAnswer = getTblQuizAnswerDao().findTblQuizAnswer("tblQuizQuestion.questionId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizAnswer : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Ends");
        return tblQuizAnswer;
    }
    
     @Override
    public List<TblQuizAnswer> findTblQuizAnswerByAnsId(int id) {
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Starts");
        List<TblQuizAnswer> tblQuizAnswer = null;
        try {
            tblQuizAnswer = getTblQuizAnswerDao().findTblQuizAnswer("answerId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuizAnswer : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuizAnswer : " + logUserId + " Ends");
        return tblQuizAnswer;
    }
    
    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblConfigPaThresholdDao
     */
    public TblQuizAnswerDao getTblQuizAnswerDao() {
        return tblQuizAnswerDao;
    }

    /**
     * @param tblQuizAnswerDao the tblQuizAnswerDao to set
     */
    public void setTblQuizAnswerDao(TblQuizAnswerDao tblQuizAnswerDao) {
        this.tblQuizAnswerDao = tblQuizAnswerDao;
    }
    
}
