/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblPromisProcessDao;
import com.cptu.egp.eps.model.table.TblPromisProcess;
import com.cptu.egp.eps.service.serviceinterface.PromisProcessService;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author Sudhir Chavhan
 */
public class PromisProcessServiceImpl implements PromisProcessService{

    final Logger logger = Logger.getLogger(PromisProcessServiceImpl.class);
    TblPromisProcessDao tblPromisProcessDao;
    HibernateQueryDao hibernateQueryDao;

    @Override
    public TblPromisProcessDao getTblPromisProcessDao() {
        return tblPromisProcessDao;
    }
    public void setTblPromisProcessDao(TblPromisProcessDao tblPromisProcessDao) {
        this.tblPromisProcessDao = tblPromisProcessDao;
    }
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    @Override
    public List<TblPromisProcess> getAllTblPromisProcess() throws Exception {

        logger.debug("getAllTblPromisProcess : "+" Starts");
        List<TblPromisProcess> lstEntry = null;
        try{
            //System.out.println("PromisProcessServiceImpl getAllTblPromisProcess(1) ");
            lstEntry = tblPromisProcessDao.getAllTblPromisProcess();
            //System.out.println("PromisProcessServiceImpl getAllTblPromisProcess(2) ");
        }catch(Exception ex){
            System.out.println("Error in PromisProcessServiceImpl getAllTblPromisProcess() " + ex.toString());
            logger.error("getAllTblPromisProcess Error " + ex.toString());
        }
        logger.debug("getAllTblPromisProcess : "+" Ends");
        return lstEntry;
    }

}
