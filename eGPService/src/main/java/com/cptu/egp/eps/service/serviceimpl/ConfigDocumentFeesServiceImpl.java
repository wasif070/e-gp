/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

/**
 *
 * @author Rokibul
 */
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigDocumentFeesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigDocumentFees;
import com.cptu.egp.eps.service.serviceinterface.ConfigDocumentFeesService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
public class ConfigDocumentFeesServiceImpl implements ConfigDocumentFeesService {
    final Logger logger = Logger.getLogger(CmsContractTerminationServiceImpl.class);
    private TblConfigDocumentFeesDao tblConfigDocumentFeesDao;
    private HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private static final String SPACE = " : ";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

  //  public TblConfigDocumentFeesDao getConfigDocumentFeesDao() {
    //    return configDocumentFeesDao;
   // }

//    public void setConfigDocumentFeesDao(TblConfigDocumentFeesDao configDocumentFeesDao) {
  //      this.configDocumentFeesDao = configDocumentFeesDao;
   // }

    /***
     * This method inserts a TblCmsContractTermination object in database
     * @param tblCmsContractTermination
     * @return 
     */
  /*  @Override
    public int insertConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees) {
        logger.debug("insertConfigDocumentFees : " + logUserId + STARTS);
        int flag = 0;
        try {
            configDocumentFeesDao.addTblConfigDocumentFees(tblConfigDocumentFees);
           // flag = tblConfigDocumentFees.getId();
        } catch (Exception ex) {
            logger.error("insertConfigDocumentFees : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertConfigDocumentFees : " + logUserId + ENDS);
        return flag;
    }
*/
    /****
     * This method updates an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
 /*   @Override
    public boolean updateConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees) {
        logger.debug("updateConfigDocumentFees : " + logUserId + STARTS);
        boolean flag;
        try {
            configDocumentFeesDao.updateTblConfigDocumentFees(tblConfigDocumentFees);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateConfigDocumentFees : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateConfigDocumentFees : " + logUserId + ENDS);
        return flag;
    }
*/
    /****
     * This method deletes an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
 /*   @Override
    public boolean deleteConfigDocumentFees(TblConfigDocumentFees tblConfigDocumentFees) {
        logger.debug("deleteCmsContractTermination : " + logUserId + STARTS);
        boolean flag;
        try {
            configDocumentFeesDao.deleteTblConfigDocumentFees(tblConfigDocumentFees);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteConfigDocumentFees : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteConfigDocumentFees : " + logUserId + ENDS);
        return flag;
    }
*/
    /****
     * This method returns all TblCmsContractTermination objects from database
     * @return List<TblCmsContractTermination>
     */
/*    @Override
    public List<TblConfigDocumentFees> getAllConfigDocumentFees() {
        logger.debug("getAllConfigDocumentFees : " + logUserId + STARTS);
        List<TblConfigDocumentFees> configDocumentFeesList = new ArrayList<TblConfigDocumentFees>();
        try {
            configDocumentFeesList = configDocumentFeesDao.getAllTblConfigDocumentFees();
        } catch (Exception ex) {
            logger.error("getAllConfigDocumentFees : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllConfigDocumentFees : " + logUserId + ENDS);
        return configDocumentFeesList;
    }
*/
    /***
     *  This method returns no. of TblCmsContractTermination objects from database
     * @return long
     */


    /***
     * This method returns TblCmsContractTermination for the given Id
     * @param int contractTerminationId
     * @return TblCmsContractTermination
     */
   /* @Override
    public TblConfigDocumentFees getConfigDocumentFees(int configDocumentFeesId) {
        logger.debug("getConfigDocumentFees : " + logUserId + STARTS);
        TblConfigDocumentFees tblConfigDocumentFees = null;
        List<TblConfigDocumentFees> configDocumentFeesList = null;
        try {
            configDocumentFeesList = configDocumentFeesDao.findTblConfigDocumentFees("configDocumentFeesId", Operation_enum.EQ, configDocumentFeesId,"configDocumentFeesId",Operation_enum.ORDERBY,Operation_enum.DESC);
        } catch (Exception ex) {
            logger.error("getConfigDocumentFees : " + logUserId + SPACE + ex);
        }
        if (!configDocumentFeesList.isEmpty()) {
            tblConfigDocumentFees = configDocumentFeesList.get(0);
        }
        logger.debug("getConfigDocumentFees : " + logUserId + ENDS);
        return tblConfigDocumentFees;
    }*/

    /***
     * This method returns TblCmsContractTermination for the given contractSignId
     * @param contractSignId
     * @return TblCmsContractTermination
     */
    

    @Override
    public int getMaxDocumentPriceInBDT(String procurementType)
    {
        logger.debug("getMaxDocumentPriceInBDT : " + logUserId + STARTS);
        //float MaxDocumentPriceInBDT = 0.0f;
        int MaxDocumentPriceInBDT = 0;
        List<Object> list = null;
        try {
            String queryS = "select tt.maximumDocAmtBdt from TblConfigDocumentFees tt where tt.procurementType='" + procurementType + "'";
            list = hibernateQueryDao.getSingleColQuery(queryS);
           // System.out.println(" MaximumDocAmtBDT : "+list.get(0).toString());
                if (list != null && !list.isEmpty())
                {
                    MaxDocumentPriceInBDT = (int) list.get(0);
                }
          //  MaxDocumentPriceInBDT = Float.valueOf(list.get(0).toString());
            }
        
        catch (Exception e)
        {
            logger.error("getMaxDocumentPriceInBDT : " + e + " starts");
        }
        logger.debug("getMaxDocumentPriceInBDT : " + logUserId + ENDS);

        return MaxDocumentPriceInBDT;

    }

    @Override
        public int getMaxDocumentPriceInUSD(String procurementType)
    {
        logger.debug("getMaxDocumentPriceInUSD : " + logUserId + STARTS);
        //float MaxDocumentPriceInUSD = 0.0f;
        int MaxDocumentPriceInUSD = 0;
        List<Object> list = null;
        try {
            String queryS = "select tt.maximumDocAmtUsd from TblConfigDocumentFees tt where tt.procurementType='" + procurementType + "'";
            list = hibernateQueryDao.getSingleColQuery(queryS);
          //  System.out.println(" MaximumDocAmtUSD : "+list.get(0).toString());
                if (list != null && !list.isEmpty())
                {
                    MaxDocumentPriceInUSD = (int) list.get(0);
                }
            }
        catch (Exception e)
            {
                logger.error("getMaxDocumentPriceInUSD : " + e + STARTS);
            }
        logger.debug("getMaxDocumentPriceInUSD : " + logUserId + ENDS);
        return MaxDocumentPriceInUSD;

    }

    /**
     * @param tblConfigDocumentFeesDao the tblConfigDocumentFeesDao to set
     */
    public void setTblConfigDocumentFeesDao(TblConfigDocumentFeesDao tblConfigDocumentFeesDao) {
        this.tblConfigDocumentFeesDao = tblConfigDocumentFeesDao;
    }
}
