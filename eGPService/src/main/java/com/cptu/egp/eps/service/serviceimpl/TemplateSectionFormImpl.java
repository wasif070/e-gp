/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;


import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvBoqMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsTemplateSrvBoqDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateMandatoryDocDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionFormDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderMandatoryDocDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster;
import com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail;
import com.cptu.egp.eps.model.table.TblTemplateMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.model.table.TblTenderMandatoryDoc;
import com.cptu.egp.eps.service.serviceinterface.TemplateSectionForm;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class TemplateSectionFormImpl implements TemplateSectionForm {

    private String logUserId = "0";
    final static Logger LOGGER = Logger.getLogger(TemplateSectionFormImpl.class);
    TblTemplateSectionFormDao tblTemplateSectionFormDao;
    HibernateQueryDao hibernateQueryDao;
    TblTemplateMandatoryDocDao tblTemplateMandatoryDocDao;
    TblTenderMandatoryDocDao tblTenderMandatoryDocDao;
    TblCmsSrvBoqMasterDao tblCmsSrvBoqMasterDao;
    TblCmsTemplateSrvBoqDetailDao tblCmsTemplateSrvBoqDetailDao;
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    public TblCmsSrvBoqMasterDao getTblCmsSrvBoqMasterDao() {
        return tblCmsSrvBoqMasterDao;
    }

    public void setTblCmsSrvBoqMasterDao(TblCmsSrvBoqMasterDao tblCmsSrvBoqMasterDao) {
        this.tblCmsSrvBoqMasterDao = tblCmsSrvBoqMasterDao;
    }

    public TblCmsTemplateSrvBoqDetailDao getTblCmsTemplateSrvBoqDetailDao() {
        return tblCmsTemplateSrvBoqDetailDao;
    }

    public void setTblCmsTemplateSrvBoqDetailDao(TblCmsTemplateSrvBoqDetailDao tblCmsTemplateSrvBoqDetailDao) {
        this.tblCmsTemplateSrvBoqDetailDao = tblCmsTemplateSrvBoqDetailDao;
    }

    public TblTenderMandatoryDocDao getTblTenderMandatoryDocDao() {
        return tblTenderMandatoryDocDao;
    }

    public void setTblTenderMandatoryDocDao(TblTenderMandatoryDocDao tblTenderMandatoryDocDao) {
        this.tblTenderMandatoryDocDao = tblTenderMandatoryDocDao;
    }

    public TblTemplateMandatoryDocDao getTblTemplateMandatoryDocDao() {
        return tblTemplateMandatoryDocDao;
    }

    public void setTblTemplateMandatoryDocDao(TblTemplateMandatoryDocDao tblTemplateMandatoryDocDao) {
        this.tblTemplateMandatoryDocDao = tblTemplateMandatoryDocDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setTblTemplateSectionFormDao(TblTemplateSectionFormDao tblTemplateSectionFormDao) {
        this.tblTemplateSectionFormDao = tblTemplateSectionFormDao;
    }

    public TblTemplateSectionFormDao getTblTemplateSectionFormDao() {
        return tblTemplateSectionFormDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public boolean addForm(TblTemplateSectionForm tblForm) {


        LOGGER.debug("addForm : " + logUserId + "starts");
        boolean boolAddForm = false;
        String action = "Create Form";
        try {
            tblTemplateSectionFormDao.addTblTemplateForm(tblForm);
            boolAddForm = true;
        } catch (Exception ex) {
            LOGGER.error("addForm : " + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblForm.getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }
        LOGGER.debug("addForm : " + logUserId + "ends");
        return boolAddForm;
    }

    @Override
    public boolean editForm(TblTemplateSectionForm tblForm) {

        String action = "Edit Form";
        LOGGER.debug("editForm : " + logUserId + "starts");
         boolean boolEditForm = false;
        try {
            tblTemplateSectionFormDao.updateTblTemplateForm(tblForm);
            boolEditForm = true;
        } catch (Exception ex) {
            LOGGER.error("editForm : " + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblForm.getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }

        LOGGER.debug("editForm : " + logUserId + "ends");
        return boolEditForm;
    }

    @Override
    public List<TblTemplateSectionForm> getForms(short templateId, int sectionId) {


        LOGGER.debug("getForms : " + logUserId + "starts");
        List<TblTemplateSectionForm> lstForms = null;
        try {
            lstForms = tblTemplateSectionFormDao.findTblTemplateForm("tblTemplateMaster", Operation_enum.EQ, new TblTemplateMaster(templateId),
                    "tblTemplateSections", Operation_enum.EQ, new TblTemplateSections(sectionId), "formId", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            LOGGER.error("getForms : " + ex);
        }

        LOGGER.debug("getForms : " + logUserId + "ends");
        return lstForms;
    }

    @Override
    public List<TblTemplateSectionForm> getSingleForms(int formId) {


        LOGGER.debug("getSingleForms : " + logUserId + "starts");
        List<TblTemplateSectionForm> lstForms = null;
        try {
            lstForms = tblTemplateSectionFormDao.findTblTemplateForm("formId", Operation_enum.EQ, formId);
        } catch (Exception ex) {
            LOGGER.error("getSingleForms : " + ex);
        }

        LOGGER.debug("getSingleForms : " + logUserId + "ends");
        return lstForms;
    }

    @Override
    public boolean deleteForm(int formId) {

        String action = "Delete Form";
        LOGGER.debug("deleteForm : " + logUserId + "starts");
        boolean flag = true;
        List<TblTemplateSectionForm> list = new ArrayList<TblTemplateSectionForm>();
        try {
            list= tblTemplateSectionFormDao.findEntity("formId",Operation_enum.EQ,formId);
            String query = "delete from TblTemplateSectionForm tsf where tsf.formId =" + formId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            LOGGER.error("deleteForm : " + ex);
            flag = false;
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, list.get(0).getTblTemplateMaster().getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }

        LOGGER.debug("deleteForm : " + logUserId + "ends");
        return flag;
    }

    @Override
    public boolean isBOQForm(int formId) {


        LOGGER.debug("isBOQForm : " + logUserId + "starts");
        List<Object> obj;
        boolean isBOQForm = false;
        try {
            String query = "select tsf.isPriceBid from TblTemplateSectionForm tsf where tsf.formId =" + formId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null && !obj.isEmpty()) {
                if (obj.get(0).toString().equals("yes")) {
                        isBOQForm = true;
                    }
                }

        } catch (Exception ex) {
            LOGGER.error("isBOQForm : " + ex);
            isBOQForm = false;
            }

        LOGGER.debug("isBOQForm : " + logUserId + "ends");
            return isBOQForm;
        }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public boolean saveSTDMandDocs(List<TblTemplateMandatoryDoc> mandatoryDocs) {
        LOGGER.debug("saveSTDMandDocs : " + logUserId + "starts");
        boolean flag = false;
        String action = "Prepared Documents List";
        try {
            tblTemplateMandatoryDocDao.updateOrInsertAll(mandatoryDocs);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("saveSTDMandDocs : " + logUserId,e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, mandatoryDocs.get(0).getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
            action = null;
        }
        LOGGER.debug("saveSTDMandDocs : " + logUserId + "ends");
        return flag;
    }

    @Override
    public List<Object[]> getSTDMandDocs(String templateId,String sectionId,String formId){
        return hibernateQueryDao.createNewQuery("select tmd.templateDocId,tmd.documentName from TblTemplateMandatoryDoc tmd where tmd.templateId="+templateId+" and tmd.templateSectionId="+sectionId+" and templateFormId="+formId);
    }

    @Override
    public boolean delSTDMandDoc(String templateDocId){
        boolean flag=false;
        int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblTemplateMandatoryDoc where templateDocId="+templateDocId);
        if(i!=0){
            flag=true;
        }
        return flag;
    }

    @Override
    public boolean updateSTDMandDoc(String templateDocId, String docName) {
        boolean flag=false;
        int i = hibernateQueryDao.updateDeleteNewQuery("update TblTemplateMandatoryDoc set documentName='"+docName+"' where templateDocId="+templateDocId);
        if(i!=0){
            flag=true;
        }
        return flag;
    }

    @Override
    public List<Object[]> getSTDMandDocs(String templateDocId){
        return hibernateQueryDao.createNewQuery("select tmd.templateDocId,tmd.documentName from TblTemplateMandatoryDoc tmd where tmd.templateDocId="+templateDocId);
    }

    //

    @Override
    public boolean saveTendMandDocs(List<TblTenderMandatoryDoc> mandatoryDocs) {
        LOGGER.debug("saveTendMandDocs : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblTenderMandatoryDocDao.updateOrInsertAll(mandatoryDocs);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("saveTendMandDocs : " + logUserId,e);
        }
        LOGGER.debug("saveTendMandDocs : " + logUserId + "ends");
        return flag;
    }

    @Override
    public List<Object[]> getTendMandDocs(String tenderId,String formId){
        return hibernateQueryDao.createNewQuery("select tmd.tenderFormDocId,tmd.documentName,tmd.templateFormId from TblTenderMandatoryDoc tmd where tmd.tenderId="+tenderId+" and tmd.tenderFormId="+formId);
    }

    @Override
    public boolean delTendMandDoc(String tenderDocId){
        boolean flag=false;
        int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderMandatoryDoc where tenderFormDocId="+tenderDocId);
        if(i!=0){
            flag=true;
        }
        return flag;
    }

    @Override
    public boolean updateTendMandDoc(String tenderDocId, String docName) {
        boolean flag=false;
        int i = hibernateQueryDao.updateDeleteNewQuery("update TblTenderMandatoryDoc set documentName='"+docName+"' where tenderFormDocId="+tenderDocId);
        if(i!=0){
            flag=true;
        }
        return flag;
    }

    @Override
    public List<Object[]> getTendMandDocs(String tenderDocId){
        return hibernateQueryDao.createNewQuery("select tmd.tenderFormDocId,tmd.documentName from TblTenderMandatoryDoc tmd where tmd.tenderFormDocId="+tenderDocId);
    }

    @Override
    public boolean addCmsSrvBoqMaster(TblCmsSrvBoqMaster tblcms) {
        LOGGER.debug("addSrvBoqMaster : " + logUserId + "starts");
        boolean boolSrvBoq = false;
        try {
            tblCmsSrvBoqMasterDao.addTblCmsSrvBoqMaster(tblcms);

            boolSrvBoq = true;
        } catch (Exception ex) {
            LOGGER.error("addSrvBoqMaster : " + ex);
        }
        LOGGER.debug("addSrvBoqMaster : " + logUserId + "ends");
        return boolSrvBoq;
    }

    @Override
    public boolean addCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail tblcms) {
        LOGGER.debug("addCmsTemplateSrvBoqDetail : " + logUserId + "starts");
        boolean boolTempSrvBoq = false;
        try {
            tblCmsTemplateSrvBoqDetailDao.addTblCmsTemplateSrvBoqDetail(tblcms);
            boolTempSrvBoq = true;
        } catch (Exception ex) {
            LOGGER.error("addCmsTemplateSrvBoqDetail : " + ex);
        }
        LOGGER.debug("addCmsTemplateSrvBoqDetail : " + logUserId + "ends");
        return boolTempSrvBoq;
    }

    @Override
    public List<TblCmsTemplateSrvBoqDetail> getCmsTemplateSrvBoqDetail(int templateformId) {
        LOGGER.debug("getCmsTemplateSrvBoqDetail : " + logUserId + "starts");
        List<TblCmsTemplateSrvBoqDetail> list = null;
        try {
            list = tblCmsTemplateSrvBoqDetailDao.findTblCmsTemplateSrvBoqDetail("tblTemplateSectionForm" ,Operation_enum.EQ, new TblTemplateSectionForm(templateformId));
        } catch (Exception ex) {
            LOGGER.error("getCmsTemplateSrvBoqDetail : " + ex);
        }
        LOGGER.debug("getCmsTemplateSrvBoqDetail : " + logUserId + "ends");
        return list;
    }

    @Override
    public boolean updateCmsTemplateSrvBoqDetail(int srvBoqDtlId,int srvBoqId,int templateformId) {
        LOGGER.debug("updateCmsTemplateSrvBoqDetail : " + logUserId + "starts");
        boolean flag = false;
        try{                                            
        int i = hibernateQueryDao.updateDeleteNewQuery("update TblCmsTemplateSrvBoqDetail ttsbd set ttsbd.tblCmsSrvBoqMaster.srvBoqId='"+srvBoqId+"', ttsbd.tblTemplateSectionForm.formId='"+templateformId+"' where ttsbd.srvBoqDtlId='"+srvBoqDtlId+"'");
        if(i!=0){
            flag=true;
        }
        }catch(Exception e)
        {
            LOGGER.error("updateCmsTemplateSrvBoqDetail : " + logUserId + "::"+e);
        }
        LOGGER.debug("updateCmsTemplateSrvBoqDetail : " + logUserId + "starts");
        return flag;
    }

    @Override
    public List<Object[]> getCmsSrvBoqMasterdata(int sectionId) {
        LOGGER.debug("getCmsSrvBoqMasterdata : " + logUserId + "starts");
        List<Object[]> list= null;
        try{
                StringBuilder sb = new StringBuilder();
                sb.append("select tcsbm.srvBoqId,tcsbm.srvBoqType from TblCmsSrvBoqMaster tcsbm where tcsbm.srvBoqId not in ");
                sb.append("(select sbm.srvBoqId from TblTemplateSectionForm tsf,TblCmsTemplateSrvBoqDetail tsbd,TblCmsSrvBoqMaster sbm ");
                sb.append("where tsf.tblTemplateSections.sectionId='"+sectionId+"' and tsf.formId=tsbd.tblTemplateSectionForm.formId and ");
                sb.append("sbm.srvBoqId=tsbd.tblCmsSrvBoqMaster.srvBoqId) and tcsbm.srvBoqId !=17");
                list = hibernateQueryDao.createNewQuery(sb.toString());
        }catch(Exception e)
        {
            LOGGER.error("getCmsSrvBoqMasterdata : " + logUserId + "::"+e);
        }
        LOGGER.debug("getCmsSrvBoqMasterdata : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<TblCmsSrvBoqMaster> getSrvBoqMaster(int srvBoqIdId) {
        LOGGER.debug("getSrvBoqMaster : " + logUserId + "starts");
        List<TblCmsSrvBoqMaster> list = null;
        try {
            list = tblCmsSrvBoqMasterDao.findTblCmsSrvBoqMaster("srvBoqId" ,Operation_enum.EQ, srvBoqIdId);
        } catch (Exception ex) {
            LOGGER.error("getSrvBoqMaster : " + ex);
        }
        LOGGER.debug("getSrvBoqMaster : " + logUserId + "ends");
        return list;
    }
    
    @Override
    public List<Object> getFormType(int tenderFormId){
        
        LOGGER.debug("getFormType : " + logUserId + "starts");
        List<Object> list = new ArrayList<Object>();
        try {
            list = hibernateQueryDao.singleColQuery("select tsbd.tblCmsSrvBoqMaster.srvBoqId from TblCmsTemplateSrvBoqDetail tsbd, TblTenderForms tf  where REPLACE(tf.templateFormId,'-','') = tsbd.tblTemplateSectionForm.formId and tf.tenderFormId = "+tenderFormId);
        } catch (Exception ex) {
            LOGGER.error("getFormType : " + ex);
        }
        LOGGER.debug("getFormType : " + logUserId + "ends");
        return list;
    }
}
