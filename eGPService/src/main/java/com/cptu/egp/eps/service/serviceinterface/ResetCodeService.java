/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

/**
 *
 * @author Administrator
 */
public interface ResetCodeService {

    /**
     * insertReset code in tbl_loginmaster 
     * @param userId
     * @param resetPasswordCode
     */
    public void storeResetCode(int userId,String resetPasswordCode);

    /**
     * Verify Reset Password 
     * @param mailId
     * @param resetPasswordCode
     * @return
     */
    public boolean verifyResetCode(String mailId,String resetPasswordCode);

    /**
     * Logging purpose
     * @param logUserId
     */
    public void setUserId(String logUserId);

}
