/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSBoQDetails;
import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPSharedCMSDetailForRHD;
import com.cptu.egp.eps.dao.storedprocedure.SPSharedCMSDetailBoQForRHD;
import com.cptu.egp.eps.service.serviceinterface.RHDIntegrationCMSDetailsService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sudhir Chavhan
 */
public class RHDIntegrationCMSDetailsServiceImpl implements RHDIntegrationCMSDetailsService {

    final Logger logger = Logger.getLogger(RHDIntegrationCMSDetailsServiceImpl.class);
    private String logUserId = "0";
    private SPSharedCMSDetailForRHD spSharedCMSDetailForRHD;
    private SPSharedCMSDetailBoQForRHD spSharedCMSDetailBoQForRHD;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public SPSharedCMSDetailForRHD getSpSharedCMSDetailForRHD() {
        return spSharedCMSDetailForRHD;
    }
    public void setSpSharedCMSDetailForRHD(SPSharedCMSDetailForRHD spSharedCMSDetailForRHD) {
        this.spSharedCMSDetailForRHD = spSharedCMSDetailForRHD;
    }

    public SPSharedCMSDetailBoQForRHD getSpSharedCMSDetailBoQForRHD() {
        return spSharedCMSDetailBoQForRHD;
    }
    public void setSpSharedCMSDetailBoQForRHD(SPSharedCMSDetailBoQForRHD spSharedCMSDetailBoQForRHD) {
        this.spSharedCMSDetailBoQForRHD = spSharedCMSDetailBoQForRHD;
    }
    @Override
    public void setLogUserId(String logUserId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
     /***
     * This method returns the all the records of shared CMS details only for RHD Dept.
     * @return List of RHDCMSIntegrationDetails
     * @throws Exception
     */
    @Override
    public List<RHDIntegrationCMSDetails> getSharedCMSDetailForRHD() throws Exception {

        logger.debug("getSharedCMSDetailForRHD : " + logUserId + " Starts");
        List<RHDIntegrationCMSDetails> sharedCMSDetailForRHDList = null;
        try {
            sharedCMSDetailForRHDList = spSharedCMSDetailForRHD.executeProcedure();
        } catch (Exception ex) {
            logger.error("getSharedCMSDetailForRHD : " + logUserId + " : " + ex.toString());
            System.out.println("Exception RHDIntegrationCMSDetailsServiceImpl:-" + ex.toString());
        }
        logger.debug("getSharedCMSDetailForRHD : " + logUserId + " Ends");
        //System.out.println("RHDIntegrationCMSDetailsServiceImpl:-"+sharedCMSDetailForRHDList);
        return sharedCMSDetailForRHDList;
    }

    @Override
    public List<RHDIntegrationCMSBoQDetails> getBoQForRHD(String tenderId) throws Exception {

        logger.debug("getBoQForRHD : " + logUserId + " Starts");
        List<RHDIntegrationCMSBoQDetails> sharedCMSDetailForRHDBoQList = null;
        try {
            sharedCMSDetailForRHDBoQList = spSharedCMSDetailBoQForRHD.executeProcedure(tenderId);
        } catch (Exception ex) {
            logger.error("getBoQForRHD : " + logUserId + " : " + ex.toString());
            System.out.println("Exception getBoQForRHD RHDIntegrationCMSDetailsServiceImpl:-" + ex.toString());
        }
        logger.debug("getBoQForRHD : " + logUserId + " Ends");
        return sharedCMSDetailForRHDBoQList;
    }
}
