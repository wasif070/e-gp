/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblCompanyMaster;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTempCompanyJointVenture;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import com.cptu.egp.eps.model.table.TblTempTendererEsignature;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.eps.service.audit.AuditTrail;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UserRegisterService {

    /**
     * Registers User's Login Details
     *
     * @param tblLoginMaster TblLoginMaster object
     */
    public boolean registerUserOnPortal(TblLoginMaster tblLoginMaster, String randomNumber);

    /**
     * Registers IndividualConsultant Details.
     *
     * @param tblTempTendererMaster TblTempTendererMaster
     * @param userId userid from session
     * @param isIndiCli Boolean identifying whether from Individual client or
     * not
     * @return Next Page Name
     * @throws Exception
     */
    public String registerIndvidualClient(TblTempTendererMaster tblTempTendererMaster, int userId, boolean isIndiCli) throws Exception;

    /**
     * Cloning TblLoginMaster
     *
     * @param tblLoginMaster
     * @return TblLoginMaster
     */
    public TblLoginMaster cloneTblLoginMaster(TblLoginMaster tblLoginMaster);

    /**
     * Registers Users Company Details
     *
     * @param tblTempCompanyMaster TblTempCompanyMaster object
     * @param tblTempBiddingPermissionList
     * @param userId userid from session
     * @return
     * @throws Exception
     */
    public boolean registerCompanyDetails(TblTempCompanyMaster tblTempCompanyMaster, List<TblTempBiddingPermission> tblTempBiddingPermissionList, TblUserPrefrence tblUserPreference,int userId) throws Exception;

    /**
     * Upload Users supporting Documents
     *
     * @param tblTempCompanyDocuments TblTempCompanyDocuments
     * @param tenderid
     */
    public boolean uploadSupportDocs(TblTempCompanyDocuments tblTempCompanyDocuments, int tenderid);

    public boolean uploadSupportDocsReapply(TblCompanyDocuments tblCompanyDocuments, int tenderid);

    public int getCompanyIdByTendererId(int tendererId);

    /**
     *
     * @return
     */
    public List<TblLoginMaster> getAllUsers();

    /**
     * Search user from TblLoginMaster by Criteria
     *
     * @param values Variable parameter for Searching
     * @return List of TblLoginMaster
     * @throws Exception
     */
    public List<TblLoginMaster> findUserByCriteria(Object... values) throws Exception;

    /**
     * Search Tenderer from TblTempTendererMaster by Criteria
     *
     * @param values Variable parameter for Searching
     * @return List of TblTempTendererMaster
     * @throws Exception
     */
    public List<TblTempTendererMaster> findTendererByCriteria(Object... values) throws Exception;

    /**
     * Search SupportDocs from TblTempCompanyDocuments by Criteria
     *
     * @param values
     * @return List of TblTempCompanyDocuments
     * @throws Exception
     */
    public List<TblTempCompanyDocuments> findUserDocuments(Object... values) throws Exception;

    public List<TblCompanyDocuments> findBidderDocuments(Object... values) throws Exception;

    /**
     * Delete Users uploaded Docs
     *
     * @param tblTempCompanyDocuments TblTempCompanyDocuments object
     */
    public boolean deleteUserDoc(TblTempCompanyDocuments tblTempCompanyDocuments);
    
    public boolean deleteUserDocReapply(TblCompanyDocuments tblCompanyDocuments);

    /**
     * Searching Company by criteria
     *
     * @param searchBy Field on which to search
     * @param searchVal value for the field
     * @return List of Company Name
     * @throws Exception
     */
    public List<String> searchCompanyName(String searchBy, String searchVal) throws Exception;

    /**
     * Inserting JVCA details by SP
     *
     * @param userId userid from session
     * @param jvcaCompId JV company ID
     * @param jvcaRole JV Role
     * @return Message and Flag
     * @throws Exception
     */
    public List<CommonMsgChk> insertJvcaDetailsBySP(int userId, int jvcaCompId, String jvcaRole) throws Exception;

    /**
     * JV Comapany Details from DB
     *
     * @param userId userid from session
     * @return Comapany Name,Regno etc.
     */
    public List<Object[]> getJvCompanyDetail(int userId);

    /**
     * Delete JV Company Details
     *
     * @param tblTempCompanyJointVenture TblTempCompanyJointVenture object
     */
    public boolean deleteJvcaDetail(TblTempCompanyJointVenture tblTempCompanyJointVenture);

    /**
     * Searching TblTempCompanyMaster on CompanyRegNo
     *
     * @param compRegNo CompanyRegNo
     * @param userId userid from session
     * @return List of TblTempCompanyMaster
     * @throws Exception
     */
    public List<TblTempCompanyMaster> companyRegNoCount(String compRegNo, int userId) throws Exception;

    public List<TblTempBiddingPermission> biddingPermissionCount(int userId) throws Exception;

    /**
     * Uploading e-signature Details
     *
     * @param tblTempTendererEsignature TblTempTendererEsignature object
     */
    public boolean uploadEsign(TblTempTendererEsignature tblTempTendererEsignature);

    /**
     * Searching E-signature by criteria
     *
     * @param values variable search parameters
     * @return List of TblTempTendererEsignature
     * @throws Exception
     */
    public List<TblTempTendererEsignature> findUserEsign(Object... values) throws Exception;

    /**
     * Delete E-signature Details of User
     *
     * @param tblTempTendererEsignature TblTempTendererEsignature object
     */
    public boolean deleteUserEsign(TblTempTendererEsignature tblTempTendererEsignature);

    /**
     * Updating TempTendererDetails
     *
     * @param tblTempTendererMaster TblTempTendererMaster object
     */
    public boolean updateTendererDetail(TblTempTendererMaster tblTempTendererMaster);

    /**
     * Search CompanyDetails from TblCompanyMaster
     *
     * @param compRegNo companyRegNo
     * @param userId userid from session
     * @return List of TblCompanyMaster
     * @throws Exception
     */
    public List<TblCompanyMaster> companyRegNoCheck(String compRegNo, int userId) throws Exception;
    
    public List<TblCompanyMaster> companyNameCheck(String companyName, int userId) throws Exception;

    /**
     * Copying Entry from TblCompanyMaster to TblTempCompanyMaster by SP
     *
     * @param cmpRegNo companyReg No.
     * @param userId userid from session
     * @return CompanyId from TblTempCompanyMaster
     */
    public BigDecimal insertTmpCmpMaster(String cmpRegNo, int userId);

    /**
     * Update TempCompanyDetails during edit.
     *
     * @param tblTempCompanyMaster TblTempCompanyMaster object
     */
    public boolean updateTempCompanyDetails(TblTempCompanyMaster tblTempCompanyMaster, List<TblTempBiddingPermission> tblTempBiddingPermissionList, TblUserPrefrence tblUserPreference);

    /**
     * Common Method for getting count
     *
     * @param from Ex : TblTempCompanyMaster ttcm
     * @param where Ex : ttcm.companyId = 1
     * @return count on condition
     * @throws Exception
     */
    public long checkCountByHql(String from, String where) throws Exception;

    /**
     * Updating NextScreen Detail in TblLoginMaster through HQL
     *
     * @param userId userid from session
     * @param pageName Name to be updated
     * @return 1(success) or 0(fail)
     * @throws Exception
     */
    public int updateNextScreenInLogin(int userId, String pageName) throws Exception;

    /**
     * List of Page which is enable or disable during New user registration
     *
     * @param userId userid from session
     * @return List of Page
     */
    public List<String> pageNavigationList(int userId);

    /**
     * Copying Data from TempTables to OriginalTables by SP
     *
     * @param userId userid from session
     * @return flag and Message for success or failure
     */
    public CommonMsgChk addUser(int userId, String actionSP);

    /**
     * Gives Message whether upload quota is full or not
     *
     * @param userId userId from Session
     * @return message success or fail
     */
    public String docSizeCheck(int userId);

    /**
     * Content Admin Mail Id List
     *
     * @return Content Admin Mail Id List
     */
    public List<Object> contentAdmMailId();

    /**
     * Content Admin MessageBox
     *
     * @param toEmailId receiver mailId
     * @param fromEmaild sender mailId
     * @param subject email Subject
     * @param msgText email Body
     */
    public void contentAdmMsgBox(String toEmailId, String fromEmaild, String subject, String msgText);

    /**
     * check whether the user is JVCA
     *
     * @param userId to be checked
     * @return yes or no
     */
    public String isJVCA(String userId);

    /**
     * Updates Tenderer Details
     *
     * @param tblTendererMaster
     * @param tempTendererMaster to be updated
     * @param isIndiCli flag indicating individual consultant
     * @return true or false for success or fail
     */
    public boolean updateTblTenderMaster(TblTendererMaster tblTendererMaster);

    /**
     * Updates Bidder Company Details
     *
     * @param companyMasterDtBean to be updated
     * @return true or false for success or fail
     */
    public boolean updateTblCompanyMaster(TblCompanyMaster tblCompanyMaster);

    public boolean updateTblBiddingPermission(List<TblBiddingPermission> tblBiddingPermissionList, int companyID, int userID);

    /**
     * Get Mandatory Doc for the user
     *
     * @param userId
     * @param isTemp flag for temp tables
     * @return List of TblMandatoryDoc
     * @throws Exception
     */
    public List<TblMandatoryDoc> getMandatoryDocs(String userId, boolean isTemp, boolean isReapplied) throws Exception;

    /**
     * Get TendererId from userId
     *
     * @param userId
     * @return TendererId
     */
    public String getTendererId(String userId);
    
    public String updateCompanyDocumentsStatus(String tendererId);

    /**
     * Get TendererId from userId
     *
     * @param userId
     * @return TendererId
     */
    public String getTendererIdMail(String userId);

    /**
     * Check Document Count for user
     *
     * @param userId
     * @return document count
     */
    public int checkDocCount(String userId, boolean isTemp);

    /**
     * Get bidder mailid and mobile no
     *
     * @param userId
     * @return mailID+MobileNo
     */
    public String getTendererMobEmail(int userId);

    /**
     * Get Master Tender id
     *
     * @param userId
     * @return tenderid
     */
    public String getMaterTendererId(String userId);

    /**
     * insert data into TblLoginMaster ,TblTendererMaster
     *
     * @param tendererMasterDtBean userId password
     * @return true or false
     */
    public boolean createUserByCompanyAdmin(TblLoginMaster tblLoginMaster, TblTendererMaster tblTendererMaster, int userId);

    /**
     * fetching information from
     * TblLoginMaster,TblCompanyMaster,TblTendererMaster
     *
     * @param tendererMasterDtBean userId password
     * @return object
     */
    public Object[] getCompanyAdminDetails(int userId);

    /**
     * Get TblLoginMaster by userid
     *
     * @param userId
     * @param userid
     * @return information from TblLoginMaster
     */
    public Object[] getLoginDetails(int userId);

    /**
     * Get TblTendererMaster by tendererId
     *
     * @param tendererId
     * @return information from TblTendererMaster
     * @throws Exception
     */
    public List<TblTendererMaster> getTendererDetails(int tendererId);

    /**
     * updating information into TblTendererMaster
     *
     * @param tblTendererMaster userId tendererId nationality
     * @return true or false
     */
    public boolean updateCompanyUser(TblTendererMaster tblTendererMaster, int userId, int tendererId, String nationality);

    /**
     * for suspend Company user
     *
     * @param userId activity activityBy status
     * @return true if update information else false
     */
    public boolean updateUserStatus(TblTendererAuditTrail tblTendererAuditTrail, int userId, String status);

    /**
     * Registers JVCA detailss
     *
     * @param tblLoginMaster to be inserted
     * @param jvcId
     * @return 1 or 0 for success or fail
     */
    public int jvcaRegister(TblLoginMaster tblLoginMaster, String jvcId);

    /**
     * Registers JVCA company Details
     *
     * @param tblTempCompanyMaster
     * @param userId
     * @param jvcaId
     * @return true or false for success or fail
     * @throws Exception
     */
    public boolean regJVCompanyDetail(TblCompanyMaster tblTempCompanyMaster, int userId, int jvcaId) throws Exception;

    /**
     * Updates JVCA Company Details
     *
     * @param companyMasterDtBean to be updated
     * @return true or false for success or fail
     */
    public boolean updateJVCompanyDetails(TblCompanyMaster tblCompanyMaster);

    /**
     * Registers JVCA PersonalDetails of tenderer
     *
     * @param tendererMasterDtBean to be inserted
     * @param userid
     * @return true or false for success or fail
     * @throws Exception
     */
    public boolean regJVPersonalDetail(TblTendererMaster tblTendererMaster, int userId) throws Exception;

    /**
     * Updates JVCA PersonalDetails of tenderer
     *
     * @param tempTendererMaster to be updated
     * @return true or false for success or fail
     */
    public boolean updateJVTendererDetail(TblTendererMaster tblTempTendererMaster);

    /**
     * Updates user status
     *
     * @param userId
     * @param status
     * @return true or false for success or fail
     */
    public boolean updateUserStatus(String userId, String status);

    public void setUserId(String userId);

    /**
     * Assign company Admin to another user
     *
     * @param cId companyId
     * @param tId tendererId
     * @return true or false for success or fail
     */
    public boolean assignCompanyAdminRole(String cId, String tId);

    /**
     * JVCA company finalsubmission
     *
     * @param userId finalsubmission to be done
     * @return true or false for success or fail
     */
    public List<Object[]> jvCompMailDetails(String userId);

    public void setAuditTrail(AuditTrail auditTrail);

    /**
     * Update User Status.
     *
     * @param userId
     * @param userStatus
     * @param profileStatus
     * @return true if successfully updated else false.
     */
    public boolean updateUserStatus(String userId, String userStatus, String profileStatus);

    /**
     * Get User Registration info.
     *
     * @param userId
     * @return TblUserRegInfo list if success else empty list.
     */
    public Date getUserRegInfo(String userId);
}
