/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.model.table.TblBidderLots;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import com.cptu.egp.eps.model.table.TblCorrigendumDocs;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderWatchList;
import com.cptu.eps.service.audit.AuditTrail;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TenderService {

    /**
     * insert / update tender information
     * @param bidCategory
     * @param W1
     * @param W2
     * @param W3
     * @param W4 
     * @param appIdinInt
     * @param packageIdinInt
     * @param tenderTypeinVc
     * @param createdByinVc
     * @param reoiRfpRefNoVc
     * @param docEndDatesDt
     * @param preBidStartDtsDt
     * @param preBidEndDtsDt
     * @param submissionDtsDt
     * @param openingDtsDt
     * @param eligibilityCriteriaVc
     * @param tenderBriefVc
     * @param deliverablesVc
     * @param otherDetailsVc
     * @param foreignFirmVc
     * @param docAvlMethodVc
     * @param evalTypeVc
     * @param docFeesMethodVc
     * @param docFeesModeVc
     * @param docOfficeAddVc
     * @param securityLastDtDt
     * @param securitySubOffVc
     * @param locationVc
     * @param docFessM
     * @param tenderSecurityAmtM
     * @param tenderSecurityAmtMUSD
     * @param completionTime
     * @param startTime
     * @param actioninVc
     * @param tenderidinInt
     * @param corridinInt
     * @param phasingRefNoinVc
     * @param phasingOfServiceinVc
     * @param phasingLocationinVc
     * @param indStartDtinVc
     * @param indEndDtinVc
     * @param eSignatureinVc
     * @param digitalSignatureinVc
     * @param tenderLotSecIdinVc
     * @param contractTypeNewVc
     * @param tenderPubDtVc
     * @param pkgDocFeesVc
     * @param pkgDocFeesVcUSD
     * @param estCostinM
     * @param passingMarks
     * @param ProcurementMethod
     * @param bidSecurityType
     * @param eventType
     * @param auditInfo
     * @return 
     */
    // Two Param 1. pkgDocFeesVcUSD and 2. tenderSecurityAmtMUSD are added for ICT - Dohatec
    public List<CommonSPReturn> insertSPAddYpTEndernoticeBySP(String bidCategory, String W1, String W2, String W3, String W4, int appIdinInt, int packageIdinInt, String tenderTypeinVc, String createdByinVc, String reoiRfpRefNoVc, Date docEndDatesDt, Date preBidStartDtsDt, Date preBidEndDtsDt, Date submissionDtsDt, Date openingDtsDt, String eligibilityCriteriaVc, String tenderBriefVc, String deliverablesVc, String otherDetailsVc, String foreignFirmVc, String docAvlMethodVc, String evalTypeVc, String docFeesMethodVc, String docFeesModeVc, String docOfficeAddVc, Date securityLastDtDt, String securitySubOffVc, String locationVc, String docFessM, String tenderSecurityAmtM, String tenderSecurityAmtMUSD, String completionTime, String startTime, String actioninVc, int tenderidinInt, int corridinInt, String phasingRefNoinVc, String phasingOfServiceinVc, String phasingLocationinVc, String indStartDtinVc, String indEndDtinVc, String eSignatureinVc, String digitalSignatureinVc, String tenderLotSecIdinVc, String contractTypeNewVc, Date tenderPubDtVc, String pkgDocFeesVc, String pkgDocFeesVcUSD, BigDecimal estCostinM,Integer passingMarks,String ProcurementMethod, String bidSecurityType, String eventType,String... auditInfo);

    /**
     * set list
     * @param auditTrail list to be set
     */
    public void setAuditTrail(AuditTrail auditTrail);
    /**
     * Fetching information about tender notice information
     * @param action
     * @param tenderid
     * @param viewType
     * @param appid
     * @param userid
     * @param procurementNatureId
     * @param procurementType
     * @param procurementMethod
     * @param reoiRfpRefNo
     * @param tenderPubDtFrom
     * @param tenderPubDtTo
     * @param status
     * @param submissionDtFrom
     * @return List of Notice Details
     */
    //Added district and thana parameter by Proshanto Kumar Saha

    /**
     * Fetching information about tender notice information
     * @param action
     * @param tenderid
     * @param viewType
     * @param appid
     * @param userid
     * @param procurementNatureId
     * @param procurementType
     * @param procurementMethod
     * @param reoiRfpRefNo
     * @param tenderPubDtFrom
     * @param tenderPubDtTo
     * @param status
     * @param ministry
     * @param division
     * @param agency
     * @param peOfficeName
     * @param submissionDtFrom
     * @param thana
     * @param page
     * @param recordPerPage
     * @param submissionDtTo
     * @param district
     * @return List of Notice Details
     */
    public List<CommonTenderDetails> gettenderinformationSP(String action, String viewType, int tenderid, Integer appid, Integer userid, Integer procurementNatureId, String procurementType, Integer procurementMethod, String reoiRfpRefNo, String tenderPubDtFrom, String tenderPubDtTo, String status, String ministry, String division, String agency, String peOfficeName, String submissionDtFrom, String submissionDtTo, int page, int recordPerPage,String district,String thana);

    public List<EvalCommonSearchData> gettenderDashboardinformationSP(String action, String viewType, Integer userid,String strPageNo, String strOffset, String procNature, String procMethod, String procType, String tenderId, String tenderPubFrom, String tenderPubTo);

    public List<TblProcurementMethod> getAllProcurementMethod();

    /**
     * check watch list
     * @param userId
     * @param packageId
     * @return int (id of watch from TblTenderWatchList)
     */
    public int checkTenderWatchList(int userid,int tenderId);

    public int addToWatchList(TblTenderWatchList tblTenderWatchList);

    public boolean removeFromWatchList(int watchListId);

    /**
     * update details into TblTenderDetails
     * @param tenderId, docAccess
     * @param tenderValidityDt
     * @param tenderSecurityDt
     * @return true or false
     */
    public boolean updateStatus(int tenderId,Date tenderValidityDt, Date tenderSecurityDt,String docAccess,String updatePeName);

    public List<TblCorrigendumDocs> getTenderLotCorriDoc(int tenderid,int corriId, int lotId);

    /**
     * fetching details for publish tender
     * @param tenderId
     * @return List of TblTenderDetails
     * @throws Exception
     */
    public List<TblTenderDetails> getTenderDetails(int tenderId) throws Exception;

    public boolean chkTenderLotPaid(String paidFor,int tenderId,int pkgLotId,int userId);

    public boolean chkDocMaped(int tenderId);

    /**
     * this method is for dump STD.
     * @param tenderId
     * @return boolean true- if dump ,false-if not dump
     */
    public boolean isSTDDump(int tenderId);
    /**
     * this method is for deleting data from tblTenderGrandSum
     * @param tenderId
     * @return boolean true- if deleted ,false-if not deleted
     */
    public boolean deleteTenderGrandSum(int tenderId);

    /**
     * For Re-Tender Process
     * @param tenderId
     * @param action = "Insert"
     * @param eventType = "Rejected/Re-Tenderin" / "PQ" / "REOI" / "1 stage-TSTM"
     * @param userIds = REOI max 7 Bidder Mapping
     * @param strLoingUserID = RE-TENDER Actual logged in PE NAME
     * @return
     */
    public CommonMsgChk createTenderFromPQ(int tenderId, String action, String eventType,String userIds,String strLoingUserID);

    /**
     * Get Event Type from Tender Id
     * @param tenderId =TblTenderDetails.tenderId
     * @return TblTenderDetails.eventType
     */
    public String getEventType(String tenderId);

    /**
     *For View of TblTenderLotSecurity
     * @param int tenderId
     * @return List<TblTenderLotSecurity>
     */
    public List<TblTenderLotSecurity> isMultipleLot(int tenderId);

    /**
     * For Insert in to TblBidderLots
     * @param list<TblBiiderLots>
     * @return boolean
     */
    public boolean insertAll(List<TblBidderLots> list);

    /**
     *For View of TblBidderLots
     * @param int tenderId
     * @param int userId(Session)
     * @return List<TblBidderLots>
     */
    public List<TblBidderLots> viewBidderLots(int tenderId,int userId);

    /**
     *For update TblBidderLots
     * @param List<TblBidderLots>
     * @return boolean
     */
    public boolean forUpdateTblBidderLot(List<TblBidderLots> list,int tenderId,int userId);

    public void setUserId(String logUserId);

    /**
     * Records from TblConfigDocFees (Business Rule Configure)
     * @param tenderId from tbl_TenderMaster
     * @return List of TblConfigDocFees
     */
    public List<Object[]> getConfiForTender(int tenderId);

    /**
     *For fetching details
     * @param int tenderId
     * @return list of TblTenderDetails
     */
    public List<TblTenderDetails> getCancelTenDet(int tenderId);

    /**
     *For inserting data in to TblCancelTenderRequest
     * @param TblCancelTenderRequest
     * @return true or false
     */
    public boolean insertCancelTender(TblCancelTenderRequest tblCancelTenderRequest);

    /**
     *For fetching data from TblCancelTenderRequest
     * @param tender Id
     * @return List of TblCancelTenderRequest
     */
    public List<TblCancelTenderRequest> getDetailsCancelTender(int tenderId);
    /**
     *For checking tender is live or not
     * @param tender Id
     * @return if true then tender is live else false
     */
    public boolean chkTenderStatus(int tenderId);

    /**
     * This method is used for fetching tender status
     * @param tenderId
     * @return list for tender details
     */
    public List<TblTenderDetails> getTenderStatus(int tenderId);
    /**
     * this method is for geting allocated budget cost for notice
     * @param tenderId
     * @return allocated budget cost
     */
    public List<Object> getAllocatedCost(int tenderId);

    /**
     * this method is for updating Domestic information
     * @param tenderId ,domesticPref ,domesticPercent
     * @return true or false
     */
    public boolean insertDomestic(int tenderId,String domesticPref,String domesticPercent);

    /**
     * this method is add data to TblEvalServiceWeightage
     * @param weightTec ,weightFin ,tenderId
     * @return true or false
     */
    public boolean addEvalServiceWeightage(TblEvalServiceWeightage tblEvalServiceWeightage);
    /**
     * this method is update data to TblEvalServiceWeightage
     * @param weightTec ,weightFin ,tenderId
     * @return true or false
     */
    public boolean updateEvalServiceWeightage(TblEvalServiceWeightage tblEvalServiceWeightage);

    /**
     * get count for corri tender
     * @param tenderId from tbl_TenderMaster
     * @return number of records in corri tender
     */
    public long getTenderCorriCnt(int tenderId);

    /**
     * geting count for Being processed
     * @param tenderId from tbl_TenderMaster
     * @return number of records
     */
    public long getBeingProcessCnt(int tenderId);

    /**
     * Get Tender Master Details
     * @param tender Id
     * @return TblTenderMaster Object
     */
    public TblTenderMaster getTenderMasterDetails(int tenderId);

    /**
     * Get Tbl_NoaAcceptence status
     * @param noaIssueId
     * @return TblNoaAcceptence Object
     */
    public TblNoaAcceptance getNoaAcceptance(int noaIssueId);

    public boolean serTenderSecurity(String tenderId,String amt);

    public String getTenderReportStatus(String tenderId);
}
