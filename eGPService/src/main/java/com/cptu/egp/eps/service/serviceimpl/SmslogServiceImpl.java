/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblSmslogDao;
import com.cptu.egp.eps.model.table.TblSmslog;
import com.cptu.egp.eps.service.serviceinterface.SmslogService;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SmslogServiceImpl implements SmslogService {

    static final Logger logger = Logger.getLogger(SmslogServiceImpl.class);
    private String logUserId = "0";
    TblSmslogDao tblSmslogDao;

    public TblSmslogDao getTblSmslogDao() {
        return tblSmslogDao;
    }

    public void setTblSmslogDao(TblSmslogDao tblSmslogDao) {
        this.tblSmslogDao = tblSmslogDao;
    }

    @Override
    public void addSmslog(TblSmslog tblSmslog) {
        logger.debug("addSmslog : " + logUserId + " Starts");
        try {
            tblSmslogDao.addTblSmslog(tblSmslog);
        } catch (Exception ex) {
            logger.error("addSmslog : " + logUserId + ex);
        }
        logger.debug("addSmslog : " + logUserId + " Ends");
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
}
