/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author parag
 */
public interface CreateGradeService {

    public short createGrade(TblGradeMaster gradeMaster);
    public void updateGrade(TblGradeMaster gradeMaster);
    public TblGradeMaster getGradeDetails(short gradeId);
    public boolean verifyGrade(String grade);
    public List<TblGradeMaster> getGradeMasterList(int firstResult, int maxResult, Object... values);
    public long getCntGradeMaster();
    public void setUserId(String logUserId);
    
    public void setAuditTrail(AuditTrail auditTrail);
}
