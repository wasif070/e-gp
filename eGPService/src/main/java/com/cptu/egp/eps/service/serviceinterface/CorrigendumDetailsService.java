/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CorrigendumInfoDetails;
import java.util.List;

/**
 * Interface for CorrigendumMaster details service
 * @author Sreenu
 */
public interface CorrigendumDetailsService {
    public void setLogUserId(String logUserId);
    /***
     * This method returns the below Amendement/Corrigendum details.
     * @return List<CorrigendumInfoDetails>
     */
    public List<CorrigendumInfoDetails> getAllCorrigendumDetails();
}
