/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblForgotPassword;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;


/**
 *
 * @author Administrator
 */
public interface ForgotPasswordService {

    /**
     * Check if mail is preent or not for handiling ajax request
     * @param mailId
     * @param action
     * @return String
     */
    public String chkMailId(String mailId,String action);

    /**
     * Verify the hint answer is true or not
     * @param hintAnswer
     * @param mailId
     * @return boolean
     */
    public boolean verifyHintAnswer(String hintAnswer,String mailId);

    /**
     * Verify password is true or false
     * @param mailId
     * @param password
     * @return boolean
     */
    public boolean verifyPassword(String mailId,String password);

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * Insert a record in TblForgotPassword
     * @param tblForgotPassword
     * @return true or flase boolean
     */
    public boolean addRandomeCode(TblForgotPassword tblForgotPassword);

    /**
     * Delete record from TblForgotPassword using emailId
     * @param emailid
     */
    public void deleteRandomeCode(String emailid);

    /**
     * Get data of TblForgotPassword using emailId
     * @param emailid
     * @return List of TblForgotPassword
     */
    public List<TblForgotPassword> getForgotData(String emailid);
    
    public void setAuditTrail(AuditTrail auditTrail);
}
