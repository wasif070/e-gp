/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblConfigPaThreshold;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface ConfigPaThresholdService {
    
    public void addTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);
    
    public void deleteTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);

    /**
     *
     * @param tblConfigPaThreshold
     * @return
     */
    public String updateTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);

    public List<TblConfigPaThreshold> getAllTblConfigPaThreshold();
    
    /**
     *
     * @param id
     * @return
     */
    public List<TblConfigPaThreshold> findTblConfigPaThreshold(int id);
    
}
