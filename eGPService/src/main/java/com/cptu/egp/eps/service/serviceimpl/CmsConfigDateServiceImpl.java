/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsDateConfigDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsDateConfigHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblContractSignDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaIssueDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import com.cptu.egp.eps.model.table.TblCmsDateConfigHistory;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author dixit
 */
public class CmsConfigDateServiceImpl implements CmsConfigDateService {

    private static final Logger LOGGER = Logger.getLogger(CmsConfigDateServiceImpl.class);
    TblCmsDateConfigDao tblCmsDateConfigDao;
    TblCmsDateConfigHistoryDao tblCmsDateConfigHistoryDao;
    TblNoaIssueDetailsDao tblNoaIssueDetailsDao;
    TblTenderLotSecurityDao tblTenderLotSecurityDao;
    TblContractSignDao tblContractSignDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";

    public TblContractSignDao getTblContractSignDao() {
        return tblContractSignDao;
    }

    public void setTblContractSignDao(TblContractSignDao tblContractSignDao) {
        this.tblContractSignDao = tblContractSignDao;
    }
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public TblCmsDateConfigDao getTblCmsDateConfigDao() {
        return tblCmsDateConfigDao;
    }

    public void setTblCmsDateConfigDao(TblCmsDateConfigDao tblCmsDateConfigDao) {
        this.tblCmsDateConfigDao = tblCmsDateConfigDao;
    }
    public TblCmsDateConfigHistoryDao getTblCmsDateConfigHistoryDao() {
        return tblCmsDateConfigHistoryDao;
    }

    public void setTblCmsDateConfigHistoryDao(TblCmsDateConfigHistoryDao tblCmsDateConfigHistoryDao) {
        this.tblCmsDateConfigHistoryDao = tblCmsDateConfigHistoryDao;
    }
    public TblNoaIssueDetailsDao getTblNoaIssueDetailsDao() {
        return tblNoaIssueDetailsDao;
    }

    public void setTblNoaIssueDetailsDao(TblNoaIssueDetailsDao tblNoaIssueDetailsDao) {
        this.tblNoaIssueDetailsDao = tblNoaIssueDetailsDao;
    }
    public TblTenderLotSecurityDao getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    public void setTblTenderLotSecurityDao(TblTenderLotSecurityDao tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public boolean addConfigDates(TblCmsDateConfig tblCmsDateConfig) {
        LOGGER.debug("addConfigDates : " + logUserId + STARTS);
        boolean flag = false;
        try
        {
            tblCmsDateConfigDao.addTblCmsDateConfig(tblCmsDateConfig);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("addConfigDates : " + logUserId + " " + e);            
        }
        LOGGER.debug("addConfigDates : " + logUserId + ENDS);
        return flag;
        
    }

    @Override
    public boolean addConfigDatesHistory(TblCmsDateConfigHistory tblCmsDateConfigHistory) {
        LOGGER.debug("addConfigDatesHistory : " + logUserId + STARTS);
        boolean flag = false;
        try
        {
            tblCmsDateConfigHistoryDao.addTblCmsDateConfigHistory(tblCmsDateConfigHistory);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("addConfigDatesHistory : " + logUserId + " " + e);
        }
        LOGGER.debug("addConfigDatesHistory : " + logUserId + ENDS);
        return flag;
    }

//    public List<TblNoaIssueDetails> getNoaissueDetails(int tenderId,int lotId,int noaIssueId) {
//        List<TblNoaIssueDetails> list= null;
//        LOGGER.debug("getNoaissueDetails : " + logUserId + STARTS);
//        try {
//            list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"pkgLotId",Operation_enum.EQ,lotId,"noaIssueId",Operation_enum.EQ,noaIssueId);
//        } catch (Exception ex) {
//            LOGGER.error("getNoaissueDetails : " + logUserId + " " + ex);
//        }
//        LOGGER.debug("getNoaissueDetails : " + logUserId + ENDS);
//        return list;
//    }
    @Override
    public List<Object[]> getNoaissueDetails(int tenderId,int lotId) {
        LOGGER.debug("getNoaissueDetails : " + logUserId + STARTS);
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tnid.contractNo,tnid.contractAmt,tnid.contractName ");
            sb.append("from TblNoaIssueDetails tnid,TblNoaAcceptance tna ");
            sb.append("where tnid.noaIssueId=tna.tblNoaIssueDetails.noaIssueId ");
            sb.append("and tnid.tblTenderMaster.tenderId="+tenderId+" and tnid.pkgLotId="+lotId+" ");
            sb.append("and tna.acceptRejStatus='approved' ");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception ex) {
            LOGGER.error("getNoaissueDetails : " + logUserId + " " + ex);
        }
        LOGGER.debug("getNoaissueDetails : " + logUserId + ENDS);
        return list;
    }
    @Override
    public List<Object[]> getNoaissueDetailsForRO(int tenderId,int lotId,int roundId) {
        LOGGER.debug("getNoaissueDetailsForRO : " + logUserId + STARTS);
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tnid.contractNo,tnid.contractAmt,tnid.contractName ");
            sb.append("from TblNoaIssueDetails tnid,TblNoaAcceptance tna ");
            sb.append("where tnid.noaIssueId=tna.tblNoaIssueDetails.noaIssueId and tnid.roundId="+roundId+" ");
            sb.append("and tnid.tblTenderMaster.tenderId="+tenderId+" and tnid.pkgLotId="+lotId+" ");
            sb.append("and tna.acceptRejStatus='approved' ");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception ex) {
            LOGGER.error("getNoaissueDetailsForRO : " + logUserId + " " + ex);
        }
        LOGGER.debug("getNoaissueDetailsForRO : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblTenderLotSecurity> getTenderLotSecurity(int tenderId,int lotId) {
        List<TblTenderLotSecurity> list= null;
        LOGGER.debug("getTenderLotSecurity : " + logUserId + STARTS);
        try {            
            list = tblTenderLotSecurityDao.findTblTenderLotSecurity("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"appPkgLotId",Operation_enum.EQ,lotId);
        } catch (Exception ex) {
            LOGGER.error("getTenderLotSecurity : " + logUserId + " " + ex);
        }
        LOGGER.debug("getTenderLotSecurity : " + logUserId + ENDS);
        return list;
    }

    @Override
    public boolean updateConfigDates(TblCmsDateConfig tblCmsDateConfig) {
        LOGGER.debug("updateConfigDates : " + logUserId + STARTS);
        boolean flag = false;
        try
        {
            tblCmsDateConfigDao.updateTblCmsDateConfig(tblCmsDateConfig);
            flag=true;
        } catch (Exception e) {
            LOGGER.error("updateConfigDates : " + logUserId + " " + e);
        }
        LOGGER.debug("updateConfigDates : " + logUserId + ENDS);
        return flag;
    }

    @Override
    public List<TblCmsDateConfig> getConfigDatesdata(int tenderId,int lotId,int cntId) {
        LOGGER.debug("getConfigDatesdata : " + logUserId + STARTS);
        List<TblCmsDateConfig> list = null;
        try
        {
            list = tblCmsDateConfigDao.findTblCmsDateConfig("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"appPkgLotId",Operation_enum.EQ,lotId,"tblContractSign",Operation_enum.EQ,new TblContractSign(cntId));
            
        } catch (Exception e) {
            LOGGER.error("getConfigDatesdata : " + logUserId + " " + e);
        }
        LOGGER.debug("getConfigDatesdata : " + logUserId + ENDS);
        return list;
    }
    @Override
    public List<TblCmsDateConfig> getConfigDatesdata(int tenderId,int lotId) {
        LOGGER.debug("updateConfigDates : " + logUserId + STARTS);
        List<TblCmsDateConfig> list = null;
        try
        {
            list = tblCmsDateConfigDao.findTblCmsDateConfig("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"appPkgLotId",Operation_enum.EQ,lotId);

        } catch (Exception e) {
            LOGGER.error("updateConfigDates : " + logUserId + " " + e);
        }
        LOGGER.debug("updateConfigDates : " + logUserId + ENDS);
        return list;
    }
   
    @Override
    public List<Object[]> getmaxWpDate(int lotid) {
        LOGGER.debug("getmaxWpDate : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
              list = hibernateQueryDao.createNewQuery("select MAX(tcwd.wpEndDate),MAX(tcwd.wpNoOfDays) from TblCmsWpDetail tcwd where tcwd.tblCmsWpMaster.wpId in (select tcwm.wpId from TblCmsWpMaster tcwm where tcwm.wpLotId="+lotid+")");
        } catch (Exception e) {
            LOGGER.error("getmaxWpDate : " + logUserId +" : "+e);
        }
        LOGGER.debug("getmaxWpDate : " + logUserId + ": Ends");
       return list;
    }
    @Override
    public List<Object[]> getContractInfoBar(int tenderId,int lotid) {
        LOGGER.debug("getContractInfoBar : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
              //list = hibernateQueryDao.createNewQuery("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, tcdc.actualStartDt,tcdc.actualEndDate from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId and tnid.pkgLotId=tcdc.appPkgLotId and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"'");
              //list = hibernateQueryDao.createNewQuery("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, tcdc.actualStartDt,tcdc.actualEndDate,tcs.paymentTerms, ttd.procurementNatureId,tcs.contractSignId from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc, TblContractSign tcs where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId and tnid.pkgLotId=tcdc.appPkgLotId and tcdc.tblContractSign.contractSignId=tcs.contractSignId and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"'");
            StringBuilder sb = new StringBuilder();
            sb.append("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, ");
            sb.append("tcdc.actualStartDt,tcdc.actualEndDate,tcs.paymentTerms, ttd.procurementNatureId, ");
            sb.append("tcs.contractSignId,tcm.companyName,tlm.emailId,ttm.firstName,ttm.lastName,tcm.companyId,ttm.tendererId,tlm.userId,tcs.contractSignId from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc, ");
            sb.append("TblNoaAcceptance tna, ");
            sb.append("TblContractSign tcs,TblCompanyMaster tcm,TblLoginMaster tlm,TblTendererMaster ttm where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId ");
            sb.append("and tnid.pkgLotId=tcdc.appPkgLotId and tcdc.tblContractSign.contractSignId=tcs.contractSignId ");
            sb.append("and ttm.tblLoginMaster.userId=tnid.userId and ttm.tblCompanyMaster.companyId=tcm.companyId and ttm.tblLoginMaster.userId=tlm.userId ");
            sb.append("and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"' and tcs.noaId=tnid.noaIssueId and tnid.isRepeatOrder='no' ");
            sb.append("and tnid.noaIssueId=tna.tblNoaIssueDetails.noaIssueId and tna.acceptRejStatus='approved'");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getContractInfoBar : " + logUserId +" : "+e);
        }
        LOGGER.debug("getContractInfoBar : " + logUserId + ": Ends");
       return list;
    }

    //Code Start by Proshanto Kumar Saha,Dohatec
      @Override
    public List<Object[]> getContractInfoBarWithoutCSign(int tenderId,int lotid) {
        LOGGER.debug("getContractInfoBarWithoutCSign : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, ");
            sb.append("tnid.noaIssueId,tnid.perfSecAmt,tlm.userId,ttd.procurementNatureId, ");
            sb.append("ttm.tendererId,tcm.companyName,tlm.emailId,ttm.firstName,ttm.lastName,tcm.companyId from TblTenderDetails ttd, TblNoaIssueDetails tnid, ");
            sb.append("TblNoaAcceptance tna, ");
            sb.append("TblCompanyMaster tcm,TblLoginMaster tlm,TblTendererMaster ttm where ");
            sb.append("ttm.tblLoginMaster.userId=tnid.userId and ttm.tblCompanyMaster.companyId=tcm.companyId and ttm.tblLoginMaster.userId=tlm.userId ");
            sb.append("and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"' and tnid.isRepeatOrder='no' ");
            sb.append("and tnid.noaIssueId=tna.tblNoaIssueDetails.noaIssueId and tna.acceptRejStatus='approved'");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getContractInfoBarWithoutCSign : " + logUserId +" : "+e);
        }
        LOGGER.debug("getContractInfoBarWithoutCSign : " + logUserId + ": Ends");
       return list;
    }
    //Code End by Proshanto Kumar Saha,Dohatec

    @Override
    public List<Object[]> getContractInfoBar(int tenderId,int lotid,int cntId) {
        LOGGER.debug("getContractInfoBar : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
              //list = hibernateQueryDao.createNewQuery("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, tcdc.actualStartDt,tcdc.actualEndDate from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId and tnid.pkgLotId=tcdc.appPkgLotId and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"'");
              //list = hibernateQueryDao.createNewQuery("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, tcdc.actualStartDt,tcdc.actualEndDate,tcs.paymentTerms, ttd.procurementNatureId,tcs.contractSignId from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc, TblContractSign tcs where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId and tnid.pkgLotId=tcdc.appPkgLotId and tcdc.tblContractSign.contractSignId=tcs.contractSignId and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"'");
            StringBuilder sb = new StringBuilder();
            sb.append("select ttd.reoiRfpRefNo, tnid.contractNo, tnid.contractAmt, ");
            sb.append("tcdc.actualStartDt,tcdc.actualEndDate,tcs.paymentTerms, ttd.procurementNatureId, ");
            sb.append("tcs.contractSignId,tcm.companyName,tlm.emailId,ttm.firstName,ttm.lastName,tcm.companyId,ttm.tendererId,tlm.userId,tcs.contractSignId from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc, ");
            sb.append("TblNoaAcceptance tna, ");
            sb.append("TblContractSign tcs,TblCompanyMaster tcm,TblLoginMaster tlm,TblTendererMaster ttm where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId ");
            sb.append("and tnid.pkgLotId=tcdc.appPkgLotId and tcdc.tblContractSign.contractSignId=tcs.contractSignId ");
            sb.append("and ttm.tblLoginMaster.userId=tnid.userId and ttm.tblCompanyMaster.companyId=tcm.companyId and ttm.tblLoginMaster.userId=tlm.userId ");
            sb.append("and ttd.tblTenderMaster.tenderId='"+tenderId+"' and tnid.pkgLotId='"+lotid+"' and tcs.noaId=tnid.noaIssueId and tcs.contractSignId="+cntId+" ");
            sb.append("and tnid.noaIssueId=tna.tblNoaIssueDetails.noaIssueId and tna.acceptRejStatus='approved'");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            LOGGER.error("getContractInfoBar : " + logUserId +" : "+e);
        }
        LOGGER.debug("getContractInfoBar : " + logUserId + ": Ends");
       return list;
    }

    @Override
    public List<TblCmsDateConfigHistory> getCmsDateConfigHistorydata(int actContractDtId) {
        LOGGER.debug("getCmsDateConfigHistorydata : " + logUserId + STARTS);
         List<TblCmsDateConfigHistory> list = null;
        try
        {
            list = tblCmsDateConfigHistoryDao.findTblCmsDateConfigHistory("tblCmsDateConfig",Operation_enum.EQ,new TblCmsDateConfig(actContractDtId),"createdDate",Operation_enum.ORDERBY,Operation_enum.DESC);

        } catch (Exception e) {
            LOGGER.error("getCmsDateConfigHistorydata : " + logUserId + " " + e);
        }
        LOGGER.debug("getCmsDateConfigHistorydata : " + logUserId + ENDS);
        return list;
    }

    @Override
    public boolean getConfigDatesdatabyPassingLotId(int lotId) {
        LOGGER.debug("getConfigDatesdatabyPassingLotId : " + logUserId + STARTS);
        List<TblCmsDateConfig> list = null;
        boolean flag = false;
        try
        {
            list = tblCmsDateConfigDao.findTblCmsDateConfig("appPkgLotId",Operation_enum.EQ,lotId);
            if(list.size()>0)
            {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("getConfigDatesdatabyPassingLotId : " + logUserId + " " + e);
        }
        LOGGER.debug("getConfigDatesdatabyPassingLotId : " + logUserId + ENDS);
        return flag;
    }
    @Override
    public boolean getConfigDatesdatabyPassingLotId(int lotId,int cntId) {
        LOGGER.debug("getConfigDatesdatabyPassingLotId : " + logUserId + STARTS);
        List<TblCmsDateConfig> list = null;
        boolean flag = false;
        try
        {
            list = tblCmsDateConfigDao.findTblCmsDateConfig("appPkgLotId",Operation_enum.EQ,lotId,"tblContractSign",Operation_enum.EQ,new TblContractSign(cntId));
            if(list.size()>0)
            {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("getConfigDatesdatabyPassingLotId : " + logUserId + " " + e);
        }
        LOGGER.debug("getConfigDatesdatabyPassingLotId : " + logUserId + ENDS);
        return flag;
    }

    @Override
    public List<TblContractSign> getContractSigningDate(int Contractid) {
        LOGGER.debug("getContractSigningDate : " + logUserId + STARTS);
        List<TblContractSign> list = null;
        try
        {
            list = tblContractSignDao.findTblContractSign("contractSignId",Operation_enum.EQ,Contractid);
        } catch (Exception e) {
            LOGGER.error("getContractSigningDate : " + logUserId + " " + e);
        }
        LOGGER.debug("getContractSigningDate : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<Object> getPeName(int userId) {
        LOGGER.debug("getPeName : " + logUserId + ": Starts");
        List<Object> list = null;
        try {
              list = hibernateQueryDao.singleColQuery("select tem.employeeName from TblEmployeeMaster tem where tem.tblLoginMaster.userId="+userId+"");
        } catch (Exception e) {
            LOGGER.error("getPeName : " + logUserId +" : "+e);
        }
        LOGGER.debug("getPeName : " + logUserId + ": Ends");
       return list;
    }

    @Override
    public List<Object[]> getTentativeDatesInServicesCase(int tenderId) {
        LOGGER.debug("getTentativeDatesInServicesCase : " + logUserId + ": Starts");
        List<Object[]> list =null;
        try {
            list = hibernateQueryDao.createNewQuery("select MAX(ttp.indStartDt), MAX(ttp.indEndDt) from TblTenderPhasing ttp where ttp.tblTenderMaster.tenderId="+tenderId+"");
        } catch (Exception e) {
            LOGGER.error("getTentativeDatesInServicesCase : " + logUserId +" : "+e);
        }
        LOGGER.debug("getTentativeDatesInServicesCase : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public String getNoaId(int ContractId)
    {
        LOGGER.debug("getNoaId : " + logUserId + ": Starts");
        String str = "";
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select tcs.noaId from TblContractSign tcs where tcs.contractSignId="+ContractId+"");
            if(list.get(0)!=null)
            {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getNoaId : " + logUserId +" : "+e);
        }
        LOGGER.debug("getNoaId : " + logUserId + ": Ends");
        return str;
    }
    @Override
    public List<Object[]> getAPPDates(int tenderId) {
        LOGGER.debug("getAPPDates : " + logUserId + ": Starts");
        List<Object[]> list =null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select taptd.tenderContractSignDt,taptd.tenderContractCompDt ");
            sb.append("from TblAppPqTenderDates taptd,TblTenderMaster ttm ");
            sb.append("where taptd.tblAppMaster.appId=ttm.appId and ttm.tenderId="+tenderId+"");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {            
            LOGGER.error("getAPPDates : " + logUserId +" : "+e);
        }
        LOGGER.debug("getAPPDates : " + logUserId + ": Ends");
        return list;
    }
}
