/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsVendorReqWccDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsVendorReqWcc;
import com.cptu.egp.eps.service.serviceinterface.CmsVendorReqWccService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rikin.p
 */
public class CmsVendorReqWccServiceImpl implements CmsVendorReqWccService {
    
    private TblCmsVendorReqWccDao tblCmsVendorReqWccDao;
    private HibernateQueryDao hibernateQueryDao;
    
    final Logger logger = Logger.getLogger(CmsVendorReqWccServiceImpl.class);

    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "   ";
    
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsVendorReqWccDao getTblCmsVendorReqWccDao() {
        return tblCmsVendorReqWccDao;
    }

    public void setTblCmsVendorReqWccDao(TblCmsVendorReqWccDao tblCmsVendorReqWccDao) {
        this.tblCmsVendorReqWccDao = tblCmsVendorReqWccDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    
    /**
     * This method insert the vendor request for the Work completion certificate
     * @param tblCmsVendorReqWcc
     * @return inserted entry id.
     */
    @Override
    public int addCmsVendorReqWcc(TblCmsVendorReqWcc tblCmsVendorReqWcc){
        logger.debug("addCmsVendorReqWcc : " + logUserId + STARTS);
        int flag = 0;
        try {
            tblCmsVendorReqWccDao.addTblCmsVendorReqWcc(tblCmsVendorReqWcc);
            flag = tblCmsVendorReqWcc.getVenReqWccId();
        } catch (Exception ex) {
            logger.error("addCmsVendorReqWcc : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("addCmsVendorReqWcc : " + logUserId + ENDS);
        return flag;
    }
    
    /**
     * This method will check if any Request for work certificate is Pending. 
     * @param lotId
     * @return return true if request is pending or false if not.
     */
    @Override
    public boolean isRequestPending(int lotId,int cntId){
        logger.debug("isRequestPending : " + logUserId + STARTS);
        List<TblCmsVendorReqWcc> list =  new ArrayList<TblCmsVendorReqWcc>();
        boolean flag = false;
        try {
            list = tblCmsVendorReqWccDao.findTblCmsVendorReqWcc("lotId",Operation_enum.EQ,lotId,"status",Operation_enum.EQ,"Pending","cntId",Operation_enum.EQ,cntId);
            if(!list.isEmpty()){
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("isRequestPending : " + logUserId + SPACE + ex);
        }
        logger.debug("isRequestPending : " + logUserId + ENDS);
        return flag;
    }
    
    /**
     * update the Request when Work completion certificate issue.
     * @param wcCerId
     * @param lotId
     * @return 0 if not updated or false if updated.
     */
    @Override
    public int updateReqest(int wcCerId, int lotId){
        
        logger.debug("updateReqest : " + logUserId + STARTS);
        List<Object[]> list =  new ArrayList<Object[]>();
        int flag = 0;
        try {
            String sqlQuery = "update TblCmsVendorReqWcc tcvr set tcvr.wcCertiHistId= '"+wcCerId+"', tcvr.status = 'Issued' where tcvr.lotId = '"+lotId+"'";
            flag = hibernateQueryDao.updateDeleteQuery(sqlQuery);
        } catch (Exception ex) {
            logger.error("updateReqest : " + logUserId + SPACE + ex);
        }
        logger.debug("updateReqest : " + logUserId + ENDS);
        return flag;
        
    }
}
