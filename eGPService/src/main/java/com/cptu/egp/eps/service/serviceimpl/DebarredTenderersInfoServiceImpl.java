/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.CommonDebarredTendererDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPgetDebarredTenderersInfo;
import com.cptu.egp.eps.service.serviceinterface.DebarredTenderersInfoService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu
 */
public class DebarredTenderersInfoServiceImpl implements DebarredTenderersInfoService {
    final Logger logger = Logger.getLogger(DebarredTenderersInfoServiceImpl.class);
    private String logUserId ="0";
    private SPgetDebarredTenderersInfo sPgetDebarredTenderersInfo;

    public SPgetDebarredTenderersInfo getsPgetDebarredTenderersInfo() {
        return sPgetDebarredTenderersInfo;
    }

    public void setsPgetDebarredTenderersInfo(SPgetDebarredTenderersInfo sPgetDebarredTenderersInfo) {
        this.sPgetDebarredTenderersInfo = sPgetDebarredTenderersInfo;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method returns the all the records of DebarmentDetails
     * @return List of CommonDebarredTendererDetails
     * @throws Exception
     */
    @Override
    public List<CommonDebarredTendererDetails> getAllDebartmentDetails() throws Exception {
        logger.debug("getAllDebartmentDetails : "+logUserId+" Starts");
        List<CommonDebarredTendererDetails> debarmentDetailsList  = null;
        try{
            debarmentDetailsList  = sPgetDebarredTenderersInfo.executeProcedure();
        }catch(Exception ex){
            logger.error("getAllDebartmentDetails : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getAllDebartmentDetails : "+logUserId+" Ends");
        return debarmentDetailsList;
    }

}
