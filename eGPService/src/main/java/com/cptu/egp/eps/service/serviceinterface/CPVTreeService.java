/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCpvClassification;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CPVTreeService {

    /**
     * Get all Parent Cpv Category where CpvCategory is 0
     * @return List of TblCpvClassification
     */
    public List<TblCpvClassification> getAllData();

    /**
     * Get all Child Cpv Category by CpvCategory
     * @param CpvId 
     * @return List of TblCpvClassification
     */
    public List<TblCpvClassification> getChildData(int cpvId);

    /**
     * Get all Child Cpv Category by CpvDescription
     * @param CpvDescription
     * @return List of TblCpvClassification
     */
    public String findChildDatabyCpvDesc(String cpvDescription);
    
    /**
     * Set userId at Service layer.
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
}
