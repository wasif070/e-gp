/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;


import com.cptu.egp.eps.model.table.TblJvcapartners;

import com.cptu.egp.eps.service.serviceinterface.JvcapartnersService;
import com.cptu.egp.eps.dao.daointerface.TblJvcapartnersDao;
import com.cptu.egp.eps.model.table.TblJointVenture;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class JvcapartnersServiceImpl implements JvcapartnersService {

    final Logger logger = Logger.getLogger(JvcapartnersServiceImpl.class);
    private String logUserId ="0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    HibernateQueryDao hibernateQueryDao;
    TblJvcapartnersDao tblJvcapartnersDao;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }    
    
    public TblJvcapartnersDao getTblJvcapartnersDao() {
        return tblJvcapartnersDao;
    }

    public void setTblJvcapartnersDao(TblJvcapartnersDao tblJvcapartnersDao) {
        this.tblJvcapartnersDao = tblJvcapartnersDao;
    }
    
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public int updateJvcaPartner(int jvId, String status, int userId) {
        logger.debug("updateJvcaPartner : "+logUserId+" Starts");
        int upstatus = 0;
        String jvcaName = "";
        String action ="";
        if("Agreed".equalsIgnoreCase(status)){
            action = "Acceptance of JVCA Invitation";
        }else{
            action = "Rejection of JVCA Invitation";
        }
          try {
            String query = "update TblJvcapartners jv set jvAcceptRejStatus = '"+status+"' where jv.tblJointVenture.jvid = "+jvId+" and jv.userId = "+userId+"";
            upstatus = hibernateQueryDao.updateDeleteNewQuery(query);
            Object obj= hibernateQueryDao.singleColQuery("select jvname from TblJointVenture where jvid = "+jvId).get(0);
            jvcaName = obj.toString();
        }
        catch (Exception ex) {
            logger.error("updateJvcaPartner : "+logUserId+" : "+ex.toString());
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, jvId, "JvcaId", EgpModule.JVCA.getName(), action, jvcaName);
            action = null;
        }
        logger.debug("updateJvcaPartner : "+logUserId+" Ends");
        return upstatus;
    }

    //Get JvcaPartner List
    @Override
    public List<Object []> getPartnerList(int userId,String sidx,String sord) {
        logger.debug("getPartnerList : "+logUserId+" Starts");
        List<Object []> list = null;
        try {
            String query = "select distinct jv.jvname,jv.jvid,jp.jvAcceptRejStatus,jv.creationDate, "
                    + "jp.jvAccpetDatetime,jv.jvstatus,jp.isNominatedPartner,jv.newJvuserId"
                    + " from TblJvcapartners as jp,TblJointVenture as jv "
                    + "where jp.userId="+userId+" and jp.tblJointVenture.jvid = jv.jvid "
                    + "and jv.jvstatus in ('Complete','Approved','Rejected') and jv.createdBy!="+userId
                    + " order by "+sidx+" "+sord;
            
            list = hibernateQueryDao.createNewQuery(query);
        } catch (Exception ex) {
            logger.error("getPartnerList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getPartnerList : "+logUserId+" Ends");
        return list;
    }
    
    @Override
    public Object getJvcaNamefromId(int jVid) {
        logger.debug("getJvcaNamefromId : "+logUserId+" Starts");
        List<Object []> jvca= null;
        Object obj = null;
        try
        {
            String query="select tjv.jvname from TblJointVenture as tjv where JVId ="+ jVid;
            jvca  = hibernateQueryDao.createNewQuery(query);
            
            if(jvca!=null&&!jvca.isEmpty()){
                obj = jvca.get(0);
        }
        }
        catch (Exception ex) {
            logger.error("getJvcaNamefromId : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getJvcaNamefromId : "+logUserId+" Ends");
        return obj;
    }
    
    @Override
    public List<Object []> getJvcaPartnerDetailList(int jVid) {
        logger.debug("getJvcaPartnerDetailList : "+logUserId+" Starts");
        List<Object []> jvcaPartner = null;
        try
        {
        String query="select c.companyName,t.jvRole,t.isNominatedPartner,t.jvAcceptRejStatus ,l.emailId,c.companyId,l.userId,t.jvpartnerId " +
                "from TblJvcapartners t,TblCompanyMaster c,TblLoginMaster l,TblTendererMaster tm " +
                "where c.companyId=tm.tblCompanyMaster.companyId and t.userId = tm.tblLoginMaster.userId and tm.tblLoginMaster.userId=l.userId " +
                "and t.companyId=c.companyId and jvid="+ jVid+" order by t.jvpartnerId asc";
           jvcaPartner  =hibernateQueryDao.createNewQuery(query);
           
        }
        catch (Exception ex) {
           logger.error("getJvcaPartnerDetailList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getJvcaPartnerDetailList : "+logUserId+" Ends");
        return jvcaPartner;
    }

    @Override
    public List<Object[]> getJvcaPartnerDetailList(String newUserId) {
        logger.debug("getJvcaPartnerDetailList : "+logUserId+" Starts");
        List<Object []> jvcaPartner = null;
        String action = null;
        try
        {
        String query="select c.companyName,t.jvRole,t.isNominatedPartner,t.jvAcceptRejStatus ,l.emailId,c.companyId,l.userId,tm.tendererId " +
                "from TblJvcapartners t,TblCompanyMaster c,TblLoginMaster l,TblTendererMaster tm,TblJointVenture jv " +
                "where c.companyId=tm.tblCompanyMaster.companyId and t.userId = tm.tblLoginMaster.userId and tm.tblLoginMaster.userId=l.userId " +
                "and t.companyId=c.companyId and t.tblJointVenture.jvid=jv.jvid and jv.newJvuserId="+ newUserId+" order by t.jvpartnerId asc";
           jvcaPartner  =hibernateQueryDao.createNewQuery(query);
           action = "View Profile";
        }
        catch (Exception ex) {
           logger.error("getJvcaPartnerDetailList : "+logUserId+" : "+ex.toString());
           action = "Error in View Profile "+ex;
        }finally{
          makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(newUserId), "userId", EgpModule.My_Account.getName(), action, "");
          action=null;
        }
        logger.debug("getJvcaPartnerDetailList : "+logUserId+" Ends");
        return jvcaPartner;
    }
    
    @Override
    public List<Object> checkAcceptRejectStatus(int jvId) {
        logger.debug("checkAcceptRejectStatus : "+logUserId+" Starts");
        List<Object> listStatus = null;
        
        try {
            String query = "select jv.jvAcceptRejStatus from TblJvcapartners jv where jv.tblJointVenture.jvid="+jvId;
            listStatus = hibernateQueryDao.singleColQuery(query);
        } catch (Exception ex) {
            logger.error("checkAcceptRejectStatus : "+logUserId+" : "+ex.toString());
        }
        logger.debug("checkAcceptRejectStatus : "+logUserId+" Ends");
        return listStatus;
    }

    @Override
    public List<Object[]> getJvcaPartnerNameAndEmail(int jvId, int userId) {
        logger.debug("getJvcaPartnerNameAndEmail : "+logUserId+" Starts");
        List<Object []> jvcaPartner = null;
        try
        {
        String query="select (tm.firstName+' '+tm.middleName+' ' +tm.lastName) as Name,l.emailId from " +
                "TblJvcapartners t,TblCompanyMaster c,TblLoginMaster l,TblTendererMaster tm " +
                "where c.companyId=tm.tblCompanyMaster.companyId and tm.tblLoginMaster.userId=l.userId" +
                " and t.companyId=c.companyId and t.tblJointVenture.jvid="+jvId+" and t.userId = "+userId ;
            jvcaPartner =hibernateQueryDao.createNewQuery(query);            
        }catch (Exception ex) {
             logger.error("getJvcaPartnerNameAndEmail : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getJvcaPartnerNameAndEmail : "+logUserId+" Ends");
                return jvcaPartner;
        }

    @Override
    public List<Object> getJvcaLeadPartnerEmail(int jvId) {
        logger.debug("getJvcaLeadPartnerEmail : "+logUserId+" Starts");
       List<Object> listEmail = null;
       try {
            String query = "select l.emailId from TblJvcapartners t,TblCompanyMaster c,TblLoginMaster l," +
                    "TblTendererMaster tm where c.companyId=tm.tblCompanyMaster.companyId and tm.tblLoginMaster.userId=l.userId and jvRole like 'Lead' " +
                    "and t.companyId=c.companyId and t.tblJointVenture.jvid="+jvId;
            listEmail = hibernateQueryDao.singleColQuery(query);
        } catch (Exception ex){
            logger.error("getJvcaLeadPartnerEmail : "+logUserId+" : "+ex.toString());
        }
       logger.debug("getJvcaLeadPartnerEmail : "+logUserId+" Ends");
                return listEmail;
            }
    //GEt PartnerList When using searching
    public List<Object[]> getPartnerList(int userId, String searchField, String searchOper, String searchString) {
        logger.debug("getPartnerList : "+logUserId+" Starts");
         List<Object []> list = null;
        try{
         String query = "select distinct jv.jvname,jv.jvid,jp.jvAcceptRejStatus,jv.creationDate, "
                    + "jp.jvAccpetDatetime,jv.jvstatus,jp.isNominatedPartner,jv.newJvuserId"
                    + " from TblJvcapartners as jp,TblJointVenture as jv "
                    + "where jp.userId="+userId+" and jp.tblJointVenture.jvid = jv.jvid "
                    + "and jv.jvstatus in ('Complete','Approved','Rejected') and jv.createdBy!="+userId
                    + " and "+searchField+" "+searchOper+" '"+searchString+"'";
            list = hibernateQueryDao.createNewQuery(query);
        }catch(Exception ex){
            logger.error("getPartnerList : "+logUserId+" : "+ex.toString());
        }
         logger.debug("getPartnerList : "+logUserId+" Ends");
        return list;
    }
    //For Next Screen
    @Override
    public Object[] getNextScreen(String userId) {
        logger.debug("getNextScreen : "+logUserId+" Starts");
        Object[] obj = null;
        try{
            obj = hibernateQueryDao.createNewQuery("select tlm.nextScreen,tlm.status from TblLoginMaster tlm where tlm.userId="+userId).get(0);
        }catch(Exception ex){
            logger.error("getNextScreen : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getNextScreen : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public int deleteJVCAPartner(int jvId) {
        logger.debug("deleteJVCAPartner : "+logUserId+" Starts");
        int status = 0;
        try{
        String sqlQuery = "Delete from TblJvcapartners tp where tp.tblJointVenture.jvid = "+jvId;
            hibernateQueryDao.updateDeleteQuery(sqlQuery);
            status =1;
        }catch(Exception ex){
            logger.error("deleteJVCAPartner : "+logUserId+" : "+ex.toString());
        }
         logger.debug("deleteJVCAPartner : "+logUserId+" Ends");
        return status;
    }

    @Override
    public int updatePartner(int jvId, List<TblJvcapartners> listJvcaPartner) {
        logger.debug("updatePartner : "+logUserId+" Starts");
        int status = 0;
        try{
            for(TblJvcapartners tblJvcapartners:listJvcaPartner){
                tblJvcapartners.setTblJointVenture(new TblJointVenture(jvId));
                tblJvcapartnersDao.addTblJvcapartners(tblJvcapartners);
            }
            status = 1;
        }catch(Exception ex){
            logger.error("updatePartner : "+logUserId+" : "+ex.toString());
        }
        logger.debug("updatePartner : "+logUserId+" Ends");
        return status;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
