/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDao;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDocumentDao;
import com.cptu.egp.eps.model.table.TblPriTopicMaster;
import com.cptu.egp.eps.model.table.TblPriTopicReply;
import com.cptu.egp.eps.model.table.TblPriTopicReplyDocument;
import com.cptu.egp.eps.service.serviceinterface.PrivateDissForumService;

/**
 *
 * @author dipal.shah
 */
public class PrivateDissForumServiceImpl implements PrivateDissForumService
{
    final Logger logger = Logger.getLogger(ManageUserRightsImpl.class);
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private String logUserId ="0";
    private int contractId=0;

    TblPriTopicMasterDao tblPriTopicMasterDao;
    TblPriTopicReplyDao tblPriTopicReplyDao;
    TblPriTopicReplyDocumentDao tblPriTopicReplyDocumentDao;
    HibernateQueryDao hibernateQueryDao;


    private AuditTrail auditTrail;

    private MakeAuditTrailService makeAuditTrailService;

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblPriTopicMasterDao getTblPriTopicMasterDao() {
        return tblPriTopicMasterDao;
    }

    public void setTblPriTopicMasterDao(TblPriTopicMasterDao tblPriTopicMasterDao) {
        this.tblPriTopicMasterDao = tblPriTopicMasterDao;
    }

    public TblPriTopicReplyDao getTblPriTopicReplyDao() {
        return tblPriTopicReplyDao;
    }

    public void setTblPriTopicReplyDao(TblPriTopicReplyDao tblPriTopicReplyDao) {
        this.tblPriTopicReplyDao = tblPriTopicReplyDao;
    }

    public TblPriTopicReplyDocumentDao getTblPriTopicReplyDocumentDao() {
        return tblPriTopicReplyDocumentDao;
    }

    public void setTblPriTopicReplyDocumentDao(TblPriTopicReplyDocumentDao tblPriTopicReplyDocumentDao) {
        this.tblPriTopicReplyDocumentDao = tblPriTopicReplyDocumentDao;
    }

    /**
     * This method is use for post Topic.
     * @param objTopicMaster
     * @param objTopicDoc
     * @return boolean if insertion done successfully else return false.
     */
    @Override
    public boolean postTopic(TblPriTopicMaster objTopicMaster, TblPriTopicReplyDocument objTopicDoc)
    {
        logger.debug("postTopic : " + logUserId + STARTS);
        boolean res=true;
        String action="Post Topic";
        String auditRemarks="";
        try
        {
            tblPriTopicMasterDao.addEntity(objTopicMaster);
            auditRemarks="User Id: "+logUserId+" has posted Topic Id:"+objTopicMaster.getTopicId();

           // System.out.println("New Generated Topic Id="+objTopicMaster.getTopicId());
            if(objTopicDoc != null)
            {
                objTopicDoc.setTopicId(objTopicMaster.getTopicId());
                tblPriTopicReplyDocumentDao.addEntity(objTopicDoc);
              //  System.out.println("New Generated Topic Doc Id="+objTopicDoc.getDocId());
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            res=false;
            action="Error in "+action+" "+ex.toString();
        }
        finally
        {
           // makeAuditTrailService.generateAudit(auditTrail, contractId, "contractId", EgpModule.Private_Disscussion_Forum.getName(), action,auditRemarks);
             makeAuditTrailService.generateAudit(auditTrail, objTopicMaster.getTenderId(), "tenderId", EgpModule.Private_Disscussion_Forum.getName(), action,auditRemarks);
            action = null;
        }
        logger.debug("postTopic : " + logUserId + ENDS);
        return res;
    }

    /**
     * This method is use for post Response.
     * @param objTopicMaster
     * @param objTopicDoc
     * @return boolean if insertion done successfully else return false.
     */
    @Override
    public boolean postResponse(TblPriTopicReply objTopicReply, TblPriTopicReplyDocument objReplyDoc)
    {
        logger.debug("postResponse : " + logUserId + STARTS);
        boolean res=true;
        String action="Post Response";
        String auditRemarks="";
        try
        {
            tblPriTopicReplyDao.addEntity(objTopicReply);
            auditRemarks="User Id: "+logUserId+" has posted Response Id:"+objTopicReply.getTopicReplyId();
            //System.out.println("New Generated Response Id="+objTopicReply.getTopicReplyId());
            if(objReplyDoc != null)
            {
                objReplyDoc.setTopicReplyId(objTopicReply.getTopicReplyId());
                tblPriTopicReplyDocumentDao.addEntity(objReplyDoc);
              //  System.out.println("New Generated Response Doc Id="+objReplyDoc.getDocId());
            }
            

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            res=false;
            action="Error in "+action+" "+ex.toString();
        }
        finally
        {
            //makeAuditTrailService.generateAudit(auditTrail, contractId, "contractId", EgpModule.Private_Disscussion_Forum.getName(), action, auditRemarks);
            makeAuditTrailService.generateAudit(auditTrail, objTopicReply.getTblPriTopicMaster().getTenderId(), "tenderId", EgpModule.Private_Disscussion_Forum.getName(), action, auditRemarks);
            action = null;
        }
        logger.debug("postResponse : " + logUserId + ENDS);
        return res;
    }

    /**
     *
     * @param offcet
     * @param row
     * @param tenderId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @param orderby
     * @param colName
     * @return
     */
    @Override
    public List<TblPriTopicMaster> searchTopicName(int offcet, int row, int tenderId, String searchField, String searchString, String searchOper, String orderby, String colName) {
        logger.debug("searchTopicName : " + logUserId + STARTS);
        List<TblPriTopicMaster> getsearchTopicName = null;
        try{
            if("eq".equalsIgnoreCase(searchOper)) {
                if ("asc".equalsIgnoreCase(orderby)) {
                    getsearchTopicName = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "topic", Operation_enum.EQ, searchString, "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.ASC);
                } else {
                    getsearchTopicName = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "topic", Operation_enum.EQ, searchString, "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.DESC);
                }
            }else if("cn".equalsIgnoreCase(searchOper)) {
                if("asc".equalsIgnoreCase(orderby)) {
                    getsearchTopicName = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "topic", Operation_enum.LIKE, "%" + searchString + "%", "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.ASC);
                }else{
                    getsearchTopicName = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "topic", Operation_enum.LIKE, "%" + searchString + "%", "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.DESC);
                }
            }
        }catch (Exception e) {
            logger.error("searchTopicName : " + logUserId + " " + e);
        }
        logger.debug("searchTopicName : " + logUserId + ENDS);
        return getsearchTopicName;
    }

    @Override
    public long getSearchTopicCount(int tenderId, String searchField, String searchString, String searchOper) {
        logger.debug("getSearchTopicCount : " + logUserId + STARTS);
        long cnt = 0;
        try {
            List<TblPriTopicMaster> getsearch = null;
            if ("eq".equalsIgnoreCase(searchOper)) {
                getsearch = tblPriTopicMasterDao.findTblPriTopicMaster("topic", Operation_enum.EQ, searchString, "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.ASC);
            } else if ("cn".equalsIgnoreCase(searchOper)) {
                getsearch = tblPriTopicMasterDao.findTblPriTopicMaster("topic", Operation_enum.LIKE, "%" + searchString + "%", "tenderId", Operation_enum.EQ, tenderId, "topic", Operation_enum.ORDERBY, Operation_enum.ASC);
            }
            if (!getsearch.isEmpty()) {
                cnt = getsearch.size();
            }
        } catch (Exception e) {
            logger.error("getSearchTopicCount : " + logUserId + " " + e);
    }
        logger.debug("getSearchTopicCount : " + logUserId + ENDS);
        return cnt;
    }

    @Override
    public List<TblPriTopicMaster> getTopicName(int offcet, int row, int tenderId, String orderby, String colName) {
        logger.debug("getTopicName : " + logUserId + STARTS);
        List<TblPriTopicMaster> list = null;
        try {
            if (orderby == null && colName == null) {
                list = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "tenderId", Operation_enum.EQ, tenderId,"topicId",Operation_enum.ORDERBY,Operation_enum.DESC);
            } else {
                Object data = null;
                if ("asc".equals(orderby)) {
                    data = Operation_enum.ASC;
                } else {
                    data = Operation_enum.DESC;
                }
                list = tblPriTopicMasterDao.findByCountTblPriTopicMaster(offcet, row, "tenderId", Operation_enum.EQ, tenderId, colName, Operation_enum.ORDERBY, data);
            }
        } catch (Exception ex) {
            logger.error("getTopicName : " + logUserId + " " + ex);
        }
        logger.debug("getTopicName : " + logUserId + ENDS);
        return list;
    }

    @Override
    public long getTopicCount(int tenderId) {
        logger.debug("getTopicName : " + logUserId + STARTS);
        long cnt = 0;
        try {
            cnt = tblPriTopicMasterDao.countForQuery("TblPriTopicMaster", "tenderId = '" + tenderId + "'");
        } catch (Exception ex) {
            logger.error("getTopicName : " + logUserId + " " + ex);
        }
        logger.debug("getTopicName : " + logUserId + ENDS);
        return cnt;
    }

    @Override
    public List<TblPriTopicReply> getReply(int topicId,int status) {
        logger.debug("getReply : " + logUserId + STARTS);
        List<TblPriTopicReply> list = null;
        try {
            if(status==0)
            {
                list = tblPriTopicReplyDao.findTblPriTopicReply("tblPriTopicMaster",Operation_enum.EQ,new TblPriTopicMaster(topicId),"hasSeen",Operation_enum.EQ,status,"postedBy",Operation_enum.NE,Integer.parseInt(logUserId));
            }else{
                list = tblPriTopicReplyDao.findTblPriTopicReply("tblPriTopicMaster",Operation_enum.EQ,new TblPriTopicMaster(topicId));
            }
        } catch (Exception e) {
            logger.error("getReply : " + logUserId + " " + e);
        }
        logger.debug("getReply : " + logUserId + ENDS);
        return list;
    }
    @Override
    public List<TblPriTopicReply> getParticularReply(int topicReplyId){
        logger.debug("getParticularReply : " + logUserId + STARTS);
        List<TblPriTopicReply> list = null;
        try {
                list = tblPriTopicReplyDao.findTblPriTopicReply("topicReplyId",Operation_enum.EQ,topicReplyId);
        } catch (Exception e) {
            logger.error("getParticularReply : " + logUserId + " " + e);
        }
        logger.debug("getParticularReply : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblPriTopicMaster> getTopic(int topicId) {
        logger.debug("getTopic : " + logUserId + STARTS);
        List<TblPriTopicMaster> list = null;
        try {
               list = tblPriTopicMasterDao.findTblPriTopicMaster("topicId",Operation_enum.EQ,topicId);
        } catch (Exception e) {
            logger.error("getTopic : " + logUserId + " " + e);
        }
        logger.debug("getTopic : " + logUserId + ENDS);
        return list;
    }

    /**
     * This method is use to get uploaded document details
     * @param id -- Topic id / Response Id
     * @param type --  'Topic' = if want to get document of specific topic / 'Response' = if want to get document of specific Response
     * @return List of Document Details
     */
    public List<TblPriTopicReplyDocument> getDocumentDetail(int id,String type)
    {
        logger.debug("getDocumentDetail : " + logUserId + STARTS);
        List<TblPriTopicReplyDocument> list = null;
        try
        {
            if(type != null && type.equalsIgnoreCase("Topic"))
               list = tblPriTopicReplyDocumentDao.findTblPriTopicReplyDocument("topicId",Operation_enum.EQ,id);
            else if(type != null && type.equalsIgnoreCase("Response"))
               list = tblPriTopicReplyDocumentDao.findTblPriTopicReplyDocument("topicReplyId",Operation_enum.EQ,id);
        } catch (Exception e) {
            logger.error("getDocumentDetail : " + logUserId + " " + e);
        }
        logger.debug("getDocumentDetail : " + logUserId + ENDS);
        return list;
    }

    /**
     * THis method will update read count for view reply.
     * @param topicReplyId
     * @return true if operation performed successfully else return false.
     */
    public boolean updateReadCountResponse(TblPriTopicReply tblPriTopicReply)
    {
        logger.debug("updateReadCountResponse : " + logUserId + STARTS);
        boolean success=true;
        try
        {
            tblPriTopicReplyDao.updateTblPriTopicReply(tblPriTopicReply);
            success=false;
        }
        catch (Exception e) {
            logger.error("updateReadCountResponse : " + logUserId + " " + e);
        }
        logger.debug("updateReadCountResponse : " + logUserId + ENDS);
        return success;
    }

    /**
     * This method is use to find Total Post.
     * @return Total Post.
     */
    public long getTotalPostedReply(int tenderId)
    {
        logger.debug("getTotalPostedReply : " + logUserId + STARTS);
        long total=0;
        try
        {
            total=hibernateQueryDao.countForQuery("TblPriTopicReply pr, TblPriTopicMaster pm", "pm.topicId= pr.tblPriTopicMaster.topicId and pm.tenderId="+tenderId);
        }
        catch (Exception e) {
            logger.error("getTotalPostedReply : " + logUserId + " " + e);
        }
        logger.debug("getTotalPostedReply : " + logUserId + ENDS);
        return total;
    }
}
