/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ClarificationRptService {
    
    public String forUpdate(int tenderId);

    public List<TblEvalReportMaster> getRptName(int tenderId);

    public void setUserId(String userId);
}
