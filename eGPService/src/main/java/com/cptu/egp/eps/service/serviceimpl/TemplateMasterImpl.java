/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.service.serviceinterface.TemplateMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
public class TemplateMasterImpl implements TemplateMaster {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TemplateMasterImpl.class);
    TblTemplateMasterDao tblTemplateMasterDao;
    HibernateQueryDao hibernateQueryDao;
    AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public TblTemplateMasterDao getTblTemplateMasterDao() {
        return tblTemplateMasterDao;
    }

    public void setTblTemplateMasterDao(TblTemplateMasterDao tblTemplateMasterDao) {
        this.tblTemplateMasterDao = tblTemplateMasterDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public boolean createTemplate(TblTemplateMaster tblTemplateMaster) {
        logger.debug("createTemplate : " + logUserId + "starts");
        boolean loginFlag = false;
        String action = "Create STD";
        try {
            tblTemplateMasterDao.addTblTemplateMaster(tblTemplateMaster);
            loginFlag = true;
        } catch (Exception ex) {
            action = "Error in "+action + " "+ex.getMessage();
            logger.error("createTemplate : " + ex);
            loginFlag = false;
        }finally{
                makeAuditTrailService.generateAudit(auditTrail, tblTemplateMaster.getTemplateId(), "templateId", EgpModule.STD.getName(), action, "");
        }
        logger.debug("createTemplate : " + logUserId + "ends");
        return loginFlag;

    }

    @Override
    public List<TblTemplateMaster> getTemplateList(int fromRecord, int noOfRecords, String sortOrder, String sortingOn, String tStatus) {
        logger.debug("getTemplateList : " + logUserId + "starts");
        List<TblTemplateMaster> tMaster = new ArrayList<TblTemplateMaster>();
        try {
            if(sortingOn.equalsIgnoreCase(""))
            {
                tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus);
            }
            else
            {
                if (sortOrder.equals("asc")) {
                    tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
                } else {
                    tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
                }
            }

        } catch (Exception ex) {

            logger.error("getTemplateList : " + ex);
            tMaster = null;
        }
        logger.debug("getTemplateList : " + logUserId + "ends");
        return tMaster;
    }

    @Override
    public List<TblTemplateMaster> getTemplateList(int fromRecord, int noOfRecords, String sortOrder, String sortingOn, String tStatus, String searchField, String searchString, String searchOper) {
        logger.debug("getTemplateList : " + logUserId + "starts");
        List<TblTemplateMaster> tMaster = new ArrayList<TblTemplateMaster>();
        try {
            if (sortOrder.equals("asc")) {
                if (searchOper.equalsIgnoreCase("EQ")) {
                    if (searchField.equalsIgnoreCase("noOfSections")) {
                        byte searchByte = Byte.parseByte(searchString);
                        tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.EQ, searchByte, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
                    } else {
                        tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.EQ, searchString, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
                    }

                } else if (searchOper.equalsIgnoreCase("CN")) {
                    tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.LIKE, "%" + searchString + "%", sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
                }
                //tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
            } else {
                if (searchOper.equalsIgnoreCase("EQ")) {
                    if (searchField.equalsIgnoreCase("noOfSections")) {
                        byte searchByte = Byte.parseByte(searchString);
                        tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.EQ, searchByte, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
                    } else {
                        tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.EQ, searchString, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
                    }

                } else if (searchOper.equalsIgnoreCase("CN")) {
                    tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus, searchField, Operation_enum.LIKE, "%" + searchString + "%", sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
                }
                //tMaster = tblTemplateMasterDao.findByCountEntity(fromRecord, noOfRecords, "status", Operation_enum.EQ, tStatus,searchField, Operation_enum.LIKE, searchString, sortingOn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
            }

        } catch (Exception ex) {

            logger.error("getTemplateList : " + ex);
            tMaster = null;
        }
        logger.debug("getTemplateList : " + logUserId + "ends");
        return tMaster;
    }

    @Override
    public long getTotalNoOfSTDs(String tStatus) {
        logger.debug("getTotalNoOfSTDs : " + logUserId + "starts");
        long cnt = 0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTemplateMaster ttc", "status = '" + tStatus + "'");

        } catch (Exception ex) {

            logger.error("getTotalNoOfSTDs : " + ex);

        }
        logger.debug("getTotalNoOfSTDs : " + logUserId + "ends");
            return cnt;
        }

    @Override
    public long getSearchNoOfSTDs(String tStatus, String searchField, String searchString, String searchOper) {
        logger.debug("getSearchNoOfSTDs : " + logUserId + "starts");
        long cnt = 0;
        try {
            if (searchOper.equals("eq")) {
                cnt = hibernateQueryDao.countForNewQuery("TblTemplateMaster ttc", "status = '" + tStatus + "' and " + searchField + " = '" + searchString + "'");
            } else if (searchOper.equals("cn")) {
                cnt = hibernateQueryDao.countForNewQuery("TblTemplateMaster ttc", "status = '" + tStatus + "' and " + searchField + " LIKE '%" + searchString + "%'");
            }


        } catch (Exception ex) {

            logger.error("getSearchNoOfSTDs : " + ex);

        }
        logger.debug("getSearchNoOfSTDs : " + logUserId + "ends");
            return cnt;
        }

    @Override
    public List<TblTemplateMaster> getTemplateDtl(short templateId) {
        logger.debug("getTemplateDtl : " + logUserId + "starts");
        List<TblTemplateMaster> tblTemplates;
        try {
            tblTemplates = tblTemplateMasterDao.findTblTemplateMaster("templateId", Operation_enum.EQ, templateId);

        } catch (Exception ex) {

            logger.error("getTemplateDtl : " + ex);
            tblTemplates = null;

        }
        logger.debug("getTemplateDtl : " + logUserId + "ends");
            return tblTemplates;
        }

    @Override
    public boolean updateStatusOfTemplate(String status, short templateId) {
        logger.debug("updateStatusOfTemplate : " + logUserId + "starts");
        boolean flag = true;
        
        String action = "";
        if("A".equalsIgnoreCase(status)){
            action = "Accept STD";
        }if("C".equalsIgnoreCase(status)){
            action = "Cancel STD";
        }
        try {
            String query = "update TblTemplateMaster set status = '" + status + "' where templateId = " + templateId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            action = "Error in "+action + " " + ex.getMessage();
            logger.error("updateStatusOfTemplate : " + ex);
            flag = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, templateId, "templateId", EgpModule.STD.getName(), action, "");
        }
        logger.debug("updateStatusOfTemplate : " + logUserId + "ends");
        return flag;
    }

    @Override
    public boolean deleteTemplate(short templateId) {
        logger.debug("deleteTemplate : " + logUserId + "starts");
        boolean flag = true;
        String action = "Delete STD";
        try {
            String query = "delete from TblTemplateMaster ttm where ttm.templateId =" + templateId;
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            action = "Error in "+action + " " + ex.getMessage();
            logger.error("deleteTemplate : " + ex);
            flag = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, templateId, "templateId", EgpModule.STD.getName(), action, "");
        }
        logger.debug("deleteTemplate : " + logUserId + "ends");
        return flag;
    }

    @Override
    public List<TblTemplateMaster> searchTemplateByName(String templateName) {
        logger.debug("searchTemplateByName : " + logUserId + "starts");
        List<TblTemplateMaster> tblTemplates;
        try {
            tblTemplates = tblTemplateMasterDao.findTblTemplateMaster("templateName", Operation_enum.EQ, templateName);

        } catch (Exception ex) {

            logger.error("searchTemplateByName : " + ex);
            tblTemplates = null;

        }
        logger.debug("searchTemplateByName : " + logUserId + "ends");
            return tblTemplates;
        }

    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail  = auditTrail;
    }
}
