/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;


import com.cptu.egp.eps.dao.storedprocedure.SPGetCurrencyAmount;
import com.cptu.egp.eps.dao.storedprocedure.GetCurrencyAmount;
import java.util.List;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author salahuddin
 */
public class PerformanceSecurityICT {

    static final Logger logger = Logger.getLogger(CorrigendumDetailsOfflineService.class);
    private String starts = " Starts";
    private String ends = " Ends";
    private SPGetCurrencyAmount spGetCurrencyAmount;


    public List<GetCurrencyAmount> getCurrencyAmount(int tenderID, int roundID) {
        logger.debug("get Currency Amount : " + tenderID + starts);

        List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
        try {
            currencyAmount = getSpGetCurrencyAmount().executeProcedure(tenderID, roundID);

        } catch (Exception e) {
            
            logger.error("failed to return currency amount : " + tenderID + e);
        }
        logger.debug("return currency amount : " + tenderID);
         return currencyAmount;
    }

    /**
     * @return the spGetCurrencyAmount
     */
    public SPGetCurrencyAmount getSpGetCurrencyAmount() {
        return spGetCurrencyAmount;
    }

    /**
     * @param spGetCurrencyAmount the spGetCurrencyAmount to set
     */
    public void setSpGetCurrencyAmount(SPGetCurrencyAmount spGetCurrencyAmount) {
        this.spGetCurrencyAmount = spGetCurrencyAmount;
    }

    

}
