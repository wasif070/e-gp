/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblActivityMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblAdminMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblAppPermissionDao;
import com.cptu.egp.eps.dao.daointerface.TblStateMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.UserStatisticsDtBean;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import java.util.ArrayList;
import com.cptu.egp.eps.dao.daointerface.TblCountryMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmailPrefMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRoundMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblExternalMemInfoDao;
import com.cptu.egp.eps.dao.daointerface.TblHintQuestionMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblNationalityMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblRegVasfeePaymentDao;
import com.cptu.egp.eps.dao.daointerface.TblSmsPrefMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.dao.storedprocedure.SPCommon;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblAppPermission;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblEmailPrefMaster;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblExternalMemInfo;
import com.cptu.egp.eps.model.table.TblHintQuestionMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblNationalityMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblSmsPrefMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author Administrator
 */
public class CommonServiceImpl implements CommonService {

    static final Logger logger = Logger.getLogger(CommonServiceImpl.class);
    TblCountryMasterDao tblCountryMasterDao;
    TblNationalityMasterDao tblNationalityMasterDao;
    TblHintQuestionMasterDao tblHintQuestionMasterDao;
    TblEvalRoundMasterDao tblEvalRoundMasterDao;
    HibernateQueryDao hibernateQueryDao;
    SPCommon sPCommon;
    TblExternalMemInfoDao tblExternalMemInfoDao;
    TblUserTypeMasterDao tblUserTypeMasterDao;
    TblRegVasfeePaymentDao tblRegVasfeePaymentDao;
    TblEmailPrefMasterDao tblEmailPrefMasterDao;
    private TblAppPermissionDao tblAppPermissionDao;
    private TblSmsPrefMasterDao tblSmsPrefMasterDao;
    private TblActivityMasterDao tblActivityMasterDao;

    public TblUserTypeMasterDao getTblUserTypeMasterDao() {
        return tblUserTypeMasterDao;
    }

    public void setTblUserTypeMasterDao(TblUserTypeMasterDao tblUserTypeMasterDao) {
        this.tblUserTypeMasterDao = tblUserTypeMasterDao;
    }
    private String userId = "0";
    private static final String starts = " Starts";
    private static final String ends = " Ends";

    public TblEvalRoundMasterDao getTblEvalRoundMasterDao() {
        return tblEvalRoundMasterDao;
    }

    public void setTblEvalRoundMasterDao(TblEvalRoundMasterDao tblEvalRoundMasterDao) {
        this.tblEvalRoundMasterDao = tblEvalRoundMasterDao;
    }

    public TblExternalMemInfoDao getTblExternalMemInfoDao() {
        return tblExternalMemInfoDao;
    }

    public void setTblExternalMemInfoDao(TblExternalMemInfoDao tblExternalMemInfoDao) {
        this.tblExternalMemInfoDao = tblExternalMemInfoDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblHintQuestionMasterDao getTblHintQuestionMasterDao() {
        return tblHintQuestionMasterDao;
    }

    public void setTblHintQuestionMasterDao(TblHintQuestionMasterDao tblHintQuestionMasterDao) {
        this.tblHintQuestionMasterDao = tblHintQuestionMasterDao;
    }

    public TblNationalityMasterDao getTblNationalityMasterDao() {
        return tblNationalityMasterDao;
    }

    public void setTblNationalityMasterDao(TblNationalityMasterDao tblNationalityMasterDao) {
        this.tblNationalityMasterDao = tblNationalityMasterDao;
    }

    public TblCountryMasterDao getTblCountryMasterDao() {
        return tblCountryMasterDao;
    }

    public void setTblCountryMasterDao(TblCountryMasterDao tblCountryMasterDao) {
        this.tblCountryMasterDao = tblCountryMasterDao;
    }

    public SPCommon getsPCommon() {
        return sPCommon;
    }

    public void setsPCommon(SPCommon sPCommon) {
        this.sPCommon = sPCommon;
    }

    /**
     * @return the tblSmsPrefMasterDao
     */
    public TblSmsPrefMasterDao getTblSmsPrefMasterDao() {
        return tblSmsPrefMasterDao;
    }

    /**
     * @param tblSmsPrefMasterDao the tblSmsPrefMasterDao to set
     */
    public void setTblSmsPrefMasterDao(TblSmsPrefMasterDao tblSmsPrefMasterDao) {
        this.tblSmsPrefMasterDao = tblSmsPrefMasterDao;
    }

    public TblRegVasfeePaymentDao getTblRegVasfeePaymentDao() {
        return tblRegVasfeePaymentDao;
    }

    public void setTblRegVasfeePaymentDao(TblRegVasfeePaymentDao tblRegVasfeePaymentDao) {
        this.tblRegVasfeePaymentDao = tblRegVasfeePaymentDao;
    }

    public TblEmailPrefMasterDao getTblEmailPrefMasterDao() {
        return tblEmailPrefMasterDao;
    }

    public void setTblEmailPrefMasterDao(TblEmailPrefMasterDao tblEmailPrefMasterDao) {
        this.tblEmailPrefMasterDao = tblEmailPrefMasterDao;
    }

    @Override
    public List<TblCountryMaster> countryMasterList() throws Exception {
        logger.debug("countryMasterList : " + userId + starts);
        logger.debug("countryMasterList : " + userId + ends);
        return tblCountryMasterDao.findTblCountryMaster("countryName", Operation_enum.ORDERBY, Operation_enum.ASC);
    }

    @Override
    public List<TblNationalityMaster> nationalityMasterList() {
        logger.debug("nationalityMasterList : " + userId + starts);
        logger.debug("nationalityMasterList : " + userId + ends);
        return tblNationalityMasterDao.getAllTblNationalityMaster();
    }

    @Override
    public List<TblHintQuestionMaster> hintQuestionMasterList() {
        logger.debug("hintQuestionMasterList : " + userId + starts);
        logger.debug("hintQuestionMasterList : " + userId + ends);
        return tblHintQuestionMasterDao.getAllTblHintQuestionMaster();
    }
    TblStateMasterDao tblStateMasterDao;

    @Override
    public List<TblStateMaster> getState(short countryId) {
        logger.debug("getState : " + userId + starts);
        List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        try {

            Object[] values = {"tblCountryMaster", Operation_enum.EQ, new TblCountryMaster(countryId), "stateName", Operation_enum.ORDERBY, Operation_enum.ASC};
            stateList = tblStateMasterDao.findTblStateMaster(values);
        } catch (Exception ex) {
            logger.error("getState : " + userId + " : " + ex);
        }
        logger.debug("getState : " + userId + starts);
        return stateList;
    }

    public TblStateMasterDao getTblStateMasterDao() {
        return tblStateMasterDao;
    }

    public void setTblStateMasterDao(TblStateMasterDao tblStateMasterDao) {
        this.tblStateMasterDao = tblStateMasterDao;
    }
    TblLoginMasterDao tblLoginMasterDao;

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    @Override
    public String verifyMail(String mailId) {
        logger.debug("verifyMail : " + userId + starts);
        String str = null;
        int size = 0;
        try {
            Object[] values = {"emailId", Operation_enum.EQ, mailId};
            size = tblLoginMasterDao.findTblLoginMaster(values).size();
        } catch (Exception ex) {
            logger.error("verifyMail : " + userId + " : " + ex);
        }
        if (size > 0) {
            str = "e-mail ID already exist, Please enter another e-mail ID";
        } else {
            str = "OK";
        }
        logger.debug("verifyMail : " + userId + ends);
        return str;
    }
    TblDepartmentMasterDao tblDepartmentMasterDao;

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    @Override
    public List<TblDepartmentMaster> getMinistryList(short deptId) {
        logger.debug("getMinistryList : " + userId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentType", Operation_enum.LIKE, "Ministry", "parentDepartmentId", Operation_enum.EQ, deptId, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getMinistryList : " + userId + " : " + ex);
        }
        logger.debug("getMinistryList : " + userId + ends);
        return deptList;
    }

    @Override
    public List<TblDepartmentMaster> getDivisionList(short deptId) {
        logger.debug("getDivisionList : " + userId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            logger.debug("getDivisionList : " + userId + " In service Dept id :" + deptId);
            Object[] values = {"departmentType", Operation_enum.LIKE, "Division", "parentDepartmentId", Operation_enum.EQ, deptId, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getDivisionList : " + userId + " : " + ex);
        }
        logger.debug("getDivisionList : " + userId + ends);
        return deptList;
    }

    @Override
    public List<TblDepartmentMaster> getOrganization(short deptId) {
        logger.debug("getOrganization : " + userId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            logger.debug("getOrganization : " + userId + " in serviceOrganization  :" + deptId);
            Object[] values = {"departmentType", Operation_enum.LIKE, "Organization", "parentDepartmentId", Operation_enum.EQ, deptId, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getOrganization : " + userId + " :" + ex);
        }
        logger.debug("getOrganization : " + userId + ends);
        return deptList;
    }

    @Override
    public String verifyComposeMail(String mailId) {
        logger.debug("verifyComposeMail : " + userId + starts);
        String msg = null;
        int size = 0;
        try {
            Object[] values = {"emailId", Operation_enum.EQ, mailId};
            size = tblLoginMasterDao.findTblLoginMaster(values).size();
        } catch (Exception ex) {
            logger.error("verifyComposeMail : " + userId + " :" + ex);
        }
        if (size > 0) {
            msg = "OK";
        } else {
            msg = "Mail-ID doesn’t exist in e-GP system";
        }
        logger.debug("verifyComposeMail : " + userId + ends);
        return msg;
    }
    TblAdminMasterDao tblAdminMasterDao;

    public TblAdminMasterDao getTblAdminMasterDao() {
        return tblAdminMasterDao;
    }

    public void setTblAdminMasterDao(TblAdminMasterDao tblAdminMasterDao) {
        this.tblAdminMasterDao = tblAdminMasterDao;
    }
    TblPartnerAdminDao tblPartnerAdminDao;

    public TblPartnerAdminDao getTblPartnerAdminDao() {
        return tblPartnerAdminDao;
    }

    public void setTblPartnerAdminDao(TblPartnerAdminDao tblPartnerAdminDao) {
        this.tblPartnerAdminDao = tblPartnerAdminDao;
    }
    TblTendererMasterDao tblTendererMasterDao;

    public TblTendererMasterDao getTendererMasterDao() {
        return tblTendererMasterDao;
    }

    public void setTendererMasterDao(TblTendererMasterDao tblTendererMasterDao) {
        this.tblTendererMasterDao = tblTendererMasterDao;
    }
    TblEmployeeMasterDao tblEmployeeMasterDao;

    public TblEmployeeMasterDao getTblEmployeeMasterDao() {
        return tblEmployeeMasterDao;
    }

    public void setTblEmployeeMasterDao(TblEmployeeMasterDao tblEmployeeMasterDao) {
        this.tblEmployeeMasterDao = tblEmployeeMasterDao;
    }

    @Override
    public String verifyNationalId(String nationalId) {
        String msg = null;
        logger.debug("verifyNationalId : " + userId + starts);
        short size1 = 0;
        short size2 = 0;
        short size3 = 0;
        short size4 = 0;
        try {
            size1 = (short) tblAdminMasterDao.findTblAdminMaster("nationalId", Operation_enum.LIKE, nationalId).size();
            size2 = (short) tblPartnerAdminDao.findTblPartnerAdmin("nationalId", Operation_enum.LIKE, nationalId).size();
            size3 = (short) tblEmployeeMasterDao.findEmployee("nationalId", Operation_enum.LIKE, nationalId).size();
            size4 = (short) tblTendererMasterDao.findTblTendererMaster("nationalIdNo", Operation_enum.LIKE, nationalId).size();

        } catch (Exception ex) {
            logger.error("verifyNationalId : " + userId + " :" + ex);
        }

        if (size1 > 0 || size2 > 0 || size3 > 0 || size4 > 0) {
            msg = "Citizen ID already exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyNationalId : " + userId + ends);
        return msg;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String verifyMobileNo(String mobileno) {
        logger.debug("verifyMobileNo : " + userId + " Stars");
        String msg = null;
        short size1 = 0;
        short size2 = 0;
        short size3 = 0;
        short size4 = 0;
        try {
            size1 = (short) tblAdminMasterDao.findTblAdminMaster("mobileNo", Operation_enum.LIKE, mobileno).size();
            size2 = (short) tblPartnerAdminDao.findTblPartnerAdmin("mobileNo", Operation_enum.LIKE, mobileno).size();
            size3 = (short) tblEmployeeMasterDao.findEmployee("mobileNo", Operation_enum.LIKE, mobileno).size();
            size4 = (short) tblTendererMasterDao.findTblTendererMaster("mobileNo", Operation_enum.LIKE, mobileno).size();

        } catch (Exception ex) {
            logger.error("verifyMobileNo : " + userId + " :" + ex);
        }

        if (size1 > 0 || size2 > 0 || size3 > 0 || size4 > 0) {
            msg = "Mobile No. already exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyMobileNo : " + userId + ends);
        return msg;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String verifyUserEmployeeId(String userEmployeeId) {
        logger.debug("verifyUserEmployeeId : " + userId + " Stars");
        String msg = null;
        short size1 = 0;
        short size2 = 0;
        short size3 = 0;
        short size4 = 0;
        try {
            //size1 = (short) tblAdminMasterDao.findTblAdminMaster("mobileNo", Operation_enum.LIKE, mobileno).size();
            //size2 = (short) tblPartnerAdminDao.findTblPartnerAdmin("mobileNo", Operation_enum.LIKE, mobileno).size();
            size1 = (short) tblEmployeeMasterDao.findEmployee("userEmployeeId", Operation_enum.LIKE, userEmployeeId).size();
            //size4 = (short) tblTendererMasterDao.findTblTendererMaster("mobileNo", Operation_enum.LIKE, mobileno).size();

        } catch (Exception ex) {
            logger.error("verifyUserEmployeeId : " + userId + " :" + ex);
        }

        //if (size1 > 0 || size2 > 0 || size3 > 0 || size4 > 0) {
         if (size1 > 0 ) {
            msg = "OK";//to avoid the duplicacy checking of userEmployeeId
            //msg = "Employee Id already exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyUserEmployeeId : " + userId + ends);
        return msg;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getUserName(int userId, int userTypeId) {
        logger.debug("getUserName : " + userId + starts);
        StringBuffer nameBuffer = new StringBuffer();
        List<TblAdminMaster> adminMasters = null;
        List<TblPartnerAdmin> partnerAdmins = null;
        List<TblTendererMaster> tendererMasters = null;
        List<TblEmployeeMaster> employeeMasters = null;
        List<TblExternalMemInfo> externalUser = null;

        switch (userTypeId) {
            case 1: //e-GP Admin
            case 4: //PE Admin
            case 5: //Organisation Admin
            case 8: //Content Admin
            case 12: //Procurement Expert
            case 11: //Company Admin
            case 19: // O & M Admin
            case 20: // O & M User
                try {
                    Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    adminMasters = tblAdminMasterDao.findTblAdminMaster(values);
                    if (!adminMasters.isEmpty()) {
                        nameBuffer.append(adminMasters.get(0).getFullName());
                    }
                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (adminMasters != null) {
                        adminMasters = null;
                    }
                }
                break;
            case 21: // DLI User
                try {
                    Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    adminMasters = tblAdminMasterDao.findTblAdminMaster(values);
                    if (!adminMasters.isEmpty()) {
                        nameBuffer.append(adminMasters.get(0).getFullName());
                    }
                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (adminMasters != null) {
                        adminMasters = null;
                    }
                }
                break;
            case 2: // Tenderer
                try {
                    Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    tendererMasters = tblTendererMasterDao.findTblTendererMaster(values);
                    if (!tendererMasters.isEmpty()) {
                        nameBuffer.append(tendererMasters.get(0).getFirstName());
                    }
                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (tendererMasters != null) {
                        tendererMasters = null;
                    }
                }
                break;
            case 3: //Govt User
            case 17: // Review Panel user
            case 18: // RHD users
                try {
                    StringBuilder query = new StringBuilder();
                    query.append("select te.govUserId,em.employeeName from TblEmployeeMaster em,TblEmployeeTrasfer te ");
                    query.append("where em.tblLoginMaster.userId=te.tblLoginMaster.userId and te.isCurrent='yes' and em.tblLoginMaster.userId=" + userId);
                    List<Object[]> list = hibernateQueryDao.createNewQuery(query.toString());
                    /*Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    employeeMasters= tblEmployeeMasterDao.findEmployee(values);
                    if(employeeMasters.size()>0){*/
                    if (!list.isEmpty()) {
                        nameBuffer.append(list.get(0)[0] + "%$" + list.get(0)[1]);
                    }

                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (employeeMasters != null) {
                        employeeMasters = null;
                    }
                }
                break;
            case 6: //Development Admin
            case 7: //Schedule Bank Admin
                Object data = hibernateQueryDao.getSingleColQuery("select tpa.isAdmin from TblPartnerAdmin tpa where tpa.tblLoginMaster.userId=" + userId).get(0);
                if (data.toString().equalsIgnoreCase("no")) {
                    StringBuilder query = new StringBuilder();
                    query.append("select te.partTransId,em.fullName from TblPartnerAdmin em,TblPartnerAdminTransfer te ");
                    query.append("where em.tblLoginMaster.userId=te.tblLoginMaster.userId and te.isCurrent='yes' and em.tblLoginMaster.userId=" + userId);
                    List<Object[]> list = hibernateQueryDao.createNewQuery(query.toString());
                    /*Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    partnerAdmins = tblPartnerAdminDao.findTblPartnerAdmin(values);
                    if(!partnerAdmins.isEmpty()){
                    nameBuffer.append(partnerAdmins.get(0).getFullName());}*/
                    if (!list.isEmpty()) {
                        nameBuffer.append(list.get(0)[0] + "%$" + list.get(0)[1]);
                    }
                } else {
                    nameBuffer.append("0%$" + hibernateQueryDao.getSingleColQuery("select tpa.fullName from TblPartnerAdmin tpa where tpa.tblLoginMaster.userId=" + userId).get(0));
                }
                break;
            case 15://Branch Admin
                try {
                    nameBuffer.append("0%$" + hibernateQueryDao.getSingleColQuery("select tpa.fullName from TblPartnerAdmin tpa where tpa.tblLoginMaster.userId=" + userId).get(0));
                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (partnerAdmins != null) {
                        partnerAdmins = null;
                    }
                }
                break;


            case 14: //External User
                try {
                    Object values[] = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)};
                    externalUser = tblExternalMemInfoDao.findTblExternalMemInfo(values);
                    if (externalUser.size() > 0) {
                        nameBuffer.append(externalUser.get(0).getFullName());
                    }
                } catch (Exception ex) {
                    logger.error("getUserName : " + userId + " :" + ex);
                } finally {
                    if (adminMasters != null) {
                        adminMasters = null;
                    }
                }
                break;
            default:
                break;
        }
        logger.debug("getUserName : " + userId + ends);
        return nameBuffer.toString();
    }

    @Override
    public List<TblDepartmentMaster> getDivisionDefaultList() {
        logger.debug("getDivisionDefaultList : " + userId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentType", Operation_enum.LIKE, "Division", "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getDivisionDefaultList : " + userId + " :" + ex);
        }
        logger.debug("getDivisionDefaultList : " + userId + ends);
        return deptList;
    }

    @Override
    public long getCount(String from, String where) throws Exception {
        logger.debug("getCount : " + userId + starts);
        logger.debug("getCount : " + userId + ends);
        return hibernateQueryDao.countForQuery(from, where);
    }

    @Override
    public String verifyDeptName(String deptName, String deptType) {
        int size = 0;
        String msg = null;
        logger.debug("verifyDeptName : " + userId + starts);
        try {
            Object[] values = {"departmentName", Operation_enum.LIKE, deptName, "departmentType", Operation_enum.LIKE, deptType};
            size = tblDepartmentMasterDao.findTblDepartmentMaster(values).size();
        } catch (Exception ex) {
            logger.error("verifyDeptName : " + userId + " :" + ex);
        }
        if (size > 0) {
            msg = deptType + " Name Already Exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyDeptName : " + userId + ends);
        return msg;
    }

    @Override
    public String verifyOfficeName(String officeName, int deptId) {
        logger.debug("verifyOfficeName : " + userId + starts);
        String msg = null;
        long size = 0;
        try {
            size = hibernateQueryDao.countForNewQuery("TblOfficeMaster tom, TblDepartmentMaster tdm", "tom.departmentId = tdm.departmentId and tom.officeName = '" + officeName + "' and tdm.departmentId = " + deptId);
            //Object values = hibernateQueryDao.createNewQuery("select tom.officeName from TblOfficeMaster tom, TblDepartmentMaster tdm where tom.departmentId = tdm.departmentId and tom.officeName = '" + officeName + "' and tdm.departmentId like '" + deptId + "'");
            //Object[] values = {"departmentName", Operation_enum.LIKE, deptName};


        } catch (Exception ex) {
            logger.error("verifyOfficeName : " + userId + " :" + ex);
        }
        if (size > 0) {
            msg = "Office Name Already Exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyOfficeName : " + userId + ends);
        return msg;

    }

    @Override
    public String verifyOffice(int officeId) {
        logger.debug("verifyOfficeAdmin : " + userId + starts);
        String msg = null;
        long size = 0;
        try {
            size = hibernateQueryDao.countForNewQuery("TblOfficeAdmin toa", "toa.tblOfficeMaster.officeId = " + officeId);
            //Object values = hibernateQueryDao.createNewQuery("select tom.officeName from TblOfficeMaster tom, TblDepartmentMaster tdm where tom.departmentId = tdm.departmentId and tom.officeName = '" + officeName + "' and tdm.departmentId like '" + deptId + "'");
            //Object[] values = {"departmentName", Operation_enum.LIKE, deptName};


        } catch (Exception ex) {
            logger.error("verifyOfficeAdmin : " + userId + " :" + ex);
        }
        if (size > 0) {
            msg = "Admin Already Exists";
        } else {
            msg = "OK";
        }
        logger.debug("verifyOfficeAdmin : " + userId + ends);
        return msg;

    }
    
    
    @Override
    public String verifyDebar(int userId) {
        logger.debug("verifyOfficeAdmin : " + userId + starts);
        String msg = null;
        long size = 0;
        try {
            size = hibernateQueryDao.countForNewQuery("TblBiddingPermission tbp", "tbp.userId = " + userId);
            //Object values = hibernateQueryDao.createNewQuery("select tom.officeName from TblOfficeMaster tom, TblDepartmentMaster tdm where tom.departmentId = tdm.departmentId and tom.officeName = '" + officeName + "' and tdm.departmentId like '" + deptId + "'");
            //Object[] values = {"departmentName", Operation_enum.LIKE, deptName};


        } catch (Exception ex) {
            logger.error("verifyOfficeAdmin : " + userId + " :" + ex);
        }
        if (size > 0) {
            msg = "Ok";
        } else {
            msg = "You are debbared!";
        }
        logger.debug("verifyOfficeAdmin : " + userId + ends);
        return msg;

    }
    
    
    @Override
    public void sendMsg(String toEmailId, String subject, String msgText) throws Exception {
        logger.debug("sendMsg : " + userId + starts);
        sPCommon.executeProcedure(toEmailId, "egp12345@gmail.com", "", subject, msgText, "", "Low", "Inbox", "", "Live", 0, "No", "No", "0", 0, 0);
        logger.debug("sendMsg : " + userId + ends);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblStateMaster> getStateByCntName(String countryName) {
        logger.debug("getStateByCntName : " + userId + starts);
        List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        try {
            short countryId = (Short) hibernateQueryDao.singleColQuery("select tcm.countryId from TblCountryMaster tcm where tcm.countryName='" + countryName + "'").get(0);
            Object[] values = {"tblCountryMaster", Operation_enum.EQ, new TblCountryMaster(countryId), "stateName", Operation_enum.ORDERBY, Operation_enum.ASC};
            stateList = tblStateMasterDao.findTblStateMaster(values);
        } catch (Exception ex) {
            logger.error("getStateByCntName : " + userId + " :" + ex);
        }
        logger.debug("getStateByCntName : " + userId + ends);
        return stateList;

    }
    
    @Override
    public List<TblDepartmentMaster> getSubdistrictByStateId(String stateName)
    {
        logger.debug("getSubdistrictByStateId : " + userId + starts);
        //List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        List<TblDepartmentMaster> stateList = new ArrayList<TblDepartmentMaster>();
        List<TblDepartmentMaster> subDistrictList = new ArrayList<TblDepartmentMaster>();
        try {
            //int stateId = (int) hibernateQueryDao.singleColQuery("select tcm.stateId from tbl_StateMaster tcm where tcm.stateName='" + stateName + "'").get(0);
            stateList = tblDepartmentMasterDao.findTblDepartmentMaster("departmentName", Operation_enum.EQ, stateName, "departmentType", Operation_enum.EQ, "District");
            short departmentId=(short)stateList.get(0).getDepartmentId();
            //System.out.print("ddd"+stateId);
            Object[] values = {"parentDepartmentId", Operation_enum.EQ, departmentId, "departmentType", Operation_enum.EQ, "SubDistrict", "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            //subDistrictList = tblStateMasterDao.findTblStateMaster(values);
            subDistrictList=tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getSubdistrictByStateId : " + userId + " :" + ex);
        }
        logger.debug("getSubdistrictByStateId : " + userId + ends);
        return subDistrictList;
    }
@Override
    public List<TblStateMaster> getareaCode(String stateName)
    {
        logger.debug("getareaCode : " + userId + starts);
        List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        try {
            stateList = tblStateMasterDao.findTblStateMaster("stateName", Operation_enum.EQ, stateName);
        } catch (Exception ex) {
            logger.error("getSubdistrictByStateId : " + userId + " :" + ex);
        }
        logger.debug("getSubdistrictByStateId : " + userId + ends);
        return stateList;
    }
@Override
    public List<TblDepartmentMaster> getSubdistrictByStateId2(String stateName)
    {
        logger.debug("getSubdistrictByStateId : " + userId + starts);
        //List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        List<TblDepartmentMaster> stateList = new ArrayList<TblDepartmentMaster>();
        List<TblDepartmentMaster> subDistrictList = new ArrayList<TblDepartmentMaster>();
        try {
            //int stateId = (int) hibernateQueryDao.singleColQuery("select tcm.stateId from tbl_StateMaster tcm where tcm.stateName='" + stateName + "'").get(0);
            stateList = tblDepartmentMasterDao.findTblDepartmentMaster("departmentName", Operation_enum.EQ, stateName, "departmentType", Operation_enum.EQ, "District");
            short departmentId=(short)stateList.get(0).getDepartmentId();
            //System.out.print("ddd"+stateId);
            Object[] values = {"parentDepartmentId", Operation_enum.EQ, departmentId, "departmentType", Operation_enum.EQ, "Gewog", "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            //subDistrictList = tblStateMasterDao.findTblStateMaster(values);
            subDistrictList=tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getSubdistrictByStateId : " + userId + " :" + ex);
        }
        logger.debug("getSubdistrictByStateId : " + userId + ends);
        return subDistrictList;
    }
    
    @Override
    public List<TblDepartmentMaster> getSubdistrictByStateId4(String stateName)
    {
        logger.debug("getSubdistrictByStateId : " + userId + starts);
        //List<TblStateMaster> stateList = new ArrayList<TblStateMaster>();
        List<TblDepartmentMaster> stateList = new ArrayList<TblDepartmentMaster>();
        List<TblDepartmentMaster> subDistrictList = new ArrayList<TblDepartmentMaster>();
        try {
            //int stateId = (int) hibernateQueryDao.singleColQuery("select tcm.stateId from tbl_StateMaster tcm where tcm.stateName='" + stateName + "'").get(0);
            stateList = tblDepartmentMasterDao.findTblDepartmentMaster("departmentName", Operation_enum.EQ, stateName, "departmentType", Operation_enum.EQ, "SubDistrict");
            short departmentId=(short)stateList.get(0).getDepartmentId();
            //System.out.print("ddd"+stateId);
            Object[] values = {"parentDepartmentId", Operation_enum.EQ, departmentId, "departmentType", Operation_enum.EQ, "Gewog", "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            //subDistrictList = tblStateMasterDao.findTblStateMaster(values);
            subDistrictList=tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            logger.error("getSubdistrictByStateId : " + userId + " :" + ex);
        }
        logger.debug("getSubdistrictByStateId : " + userId + ends);
        return subDistrictList;
    }
    @Override
    public String countryCode(String value, boolean isCntId) {
        logger.debug("countryCode : " + userId + starts);
        StringBuilder sb = new StringBuilder();
        if (isCntId) {
            sb.append("countryId=" + value);
        } else {
            sb.append("countryName='" + value + "'");
        }
        logger.debug("countryCode : " + userId + ends);
        return (String) hibernateQueryDao.singleColQuery("select tcm.countryCode from TblCountryMaster tcm where tcm." + sb.toString()).get(0);
    }

    @Override
    public UserStatisticsDtBean getUserStatisticsDtBean() {
        logger.debug("UserStatisticsDtBean : " + userId + starts);
        UserStatisticsDtBean userStatisticsDtBean = new UserStatisticsDtBean();
        try {
            userStatisticsDtBean.setCountMinistry(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm", "tdm.departmentType like 'Ministry'"));
            userStatisticsDtBean.setCountDivision(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm", "tdm.departmentType like 'Division'"));
            userStatisticsDtBean.setCountOrganization(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm", "tdm.departmentType like 'Organization'"));
            userStatisticsDtBean.setCountPEOfficeMinistry(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like 'Ministry'"));
            userStatisticsDtBean.setCountPEOfficeDivision(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like 'Division'"));
            userStatisticsDtBean.setCountPEOfficeOrganization(hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm, TblOfficeMaster tom", "tdm.departmentId = tom.departmentId and tdm.departmentType like 'Organization'"));
            userStatisticsDtBean.setCountPEAdminUserMinistry(hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om,TblDepartmentMaster tdm", "tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId = 4 and tdm.departmentType = 'Ministry'"));
            userStatisticsDtBean.setCountPEAdminUserDivsion(hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om,TblDepartmentMaster tdm", "tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId = 4 and tdm.departmentType = 'Division'"));
            userStatisticsDtBean.setCountPEAdminUserOrganization(hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om,TblDepartmentMaster tdm", "tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId = 4 and tdm.departmentType = 'Organization'"));
            userStatisticsDtBean.setCountGovtUserMinistry(hibernateQueryDao.countForNewQuery("TblEmployeeMaster e,TblLoginMaster l", "e.employeeId in(select ee.tblEmployeeMaster.employeeId from TblEmployeeOffices ee where ee.tblOfficeMaster.officeId in(select tom.officeId from TblOfficeMaster tom where tom.departmentId in (select tdm.departmentId from TblDepartmentMaster tdm where tdm.departmentType='Ministry')))and l.userId=e.tblLoginMaster.userId and l.status='Approved'"));
            userStatisticsDtBean.setCountGovtUserDivsion(hibernateQueryDao.countForNewQuery("TblEmployeeMaster e,TblLoginMaster l", "e.employeeId in(select ee.tblEmployeeMaster.employeeId from TblEmployeeOffices ee where ee.tblOfficeMaster.officeId in(select tom.officeId from TblOfficeMaster tom where tom.departmentId in (select tdm.departmentId from TblDepartmentMaster tdm where tdm.departmentType='Division')))and l.userId=e.tblLoginMaster.userId and l.status='Approved'"));
            userStatisticsDtBean.setCountGovtUserOrganization(hibernateQueryDao.countForNewQuery("TblEmployeeMaster e,TblLoginMaster l", "e.employeeId in(select ee.tblEmployeeMaster.employeeId from TblEmployeeOffices ee where ee.tblOfficeMaster.officeId in(select tom.officeId from TblOfficeMaster tom where tom.departmentId in (select tdm.departmentId from TblDepartmentMaster tdm where tdm.departmentType='Organization')))and l.userId=e.tblLoginMaster.userId and l.status='Approved'"));
            userStatisticsDtBean.setCountDevelopmentPartner(hibernateQueryDao.countForNewQuery("TblScBankDevPartnerMaster tsbdpm", "tsbdpm.partnerType like 'Development' and tsbdpm.isBranchOffice like 'No'"));
            userStatisticsDtBean.setCountDevelopmentPartnerRCOffices(hibernateQueryDao.countForNewQuery("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType LIKE 'Development' AND scdpMaster.isBranchOffice LIKE 'Yes'"));

            userStatisticsDtBean.setCountDevelopmentPartnerAdminUser(hibernateQueryDao.countForNewQuery("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = 'Development' AND sbdp.id.isAdmin = 'Yes'"));
            userStatisticsDtBean.setCountDevelopmentPartnerUser(hibernateQueryDao.countForNewQuery("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = 'Development' AND sbdp.id.isAdmin = 'No'"));
            userStatisticsDtBean.setCountScheduleBankAdminUsers(hibernateQueryDao.countForNewQuery("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = 'ScheduleBank' AND sbdp.id.isAdmin = 'Yes'"));
            userStatisticsDtBean.setCountScheduleBankUsers(hibernateQueryDao.countForNewQuery("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = 'ScheduleBank' AND sbdp.id.isAdmin = 'No'"));
        } catch (Exception ex) {
            logger.error("UserStatisticsDtBean : " + userId + " :" + ex);

        }
        logger.debug("UserStatisticsDtBean : " + userId + ends);
        return userStatisticsDtBean;
    }

    @Override
    public String getConsultantName(String userId) {
        logger.debug("getConsultantName : " + userId + starts);
        List<Object[]> list = hibernateQueryDao.createNewQuery("select tcm.companyId,tcm.companyName,ttm.firstName,ttm.lastName from TblCompanyMaster tcm,TblTendererMaster ttm where tcm.companyId=ttm.tblCompanyMaster.companyId and ttm.tblLoginMaster.userId=" + userId);
        String name = null;
        for (Object[] objects : list) {
            name = objects[1].toString();
            if ("1".equals(objects[0].toString())) {
                name = objects[2].toString() + " " + objects[3].toString();
            }
        }
        logger.debug("getConsultantName : " + userId + ends);
        return name;
    }

    @Override
    public String getRegType(String userId) {
        String regType = null;
        List<Object> list = hibernateQueryDao.singleColQuery("select tlm.registrationType from TblLoginMaster tlm where tlm.userId=" + userId);
        if (!list.isEmpty()) {
            regType = list.get(0).toString();
        }
        return regType;
    }

    @Override
    public Object getProcNature(String tenderId) {
        logger.debug("getProcNature : " + userId + " : Starts");
        logger.debug("getProcNature : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
    }
    
    @Override
    public List<TblAppPermission> getPermissionByTenderId(String tenderId) {
    
        logger.debug("getPermissionByTenderId : " + tenderId + " : starts");
        String pkgId = hibernateQueryDao.singleColQuery("select ttm.tblAppPackages.packageId from TblTenderMaster ttm where tenderId=" + tenderId).get(0).toString();
        
         List<TblAppPermission> list = new ArrayList<TblAppPermission>();
        try {
            if (!pkgId.isEmpty()) {
                list = getTblAppPermissionDao().findTblAppPermission("packageId", Operation_enum.EQ, Integer.parseInt(pkgId));
            }
        } catch (Exception e) {
            logger.debug("biddingPermissionCount : " + tenderId + " : END");
        }
        return list;
       
    }

    @Override
    public Object getProcMethod(String tenderId) {
        logger.debug("getProcMethod : " + userId + " : Starts");
        logger.debug("getProcMethod : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select ttd.procurementMethod from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
    }

    @Override
    public Object getEventType(String tenderId) {
        logger.debug("getEventType : " + userId + " : Starts");
        logger.debug("getEventType : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select ttd.eventType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
    }

    @Override
    public String tenderPaidORFree(String tenderId, String lotId) {
        logger.debug("tenderPaidORFree : " + userId + " : Starts");
        String stat = "free";
        try {
            if (hibernateQueryDao.countForNewQuery("TblTenderLotSecurity tls", "tls.tblTenderMaster.tenderId=" + tenderId + " and tls.tenderSecurityAmt>0 and tls.appPkgLotId=" + lotId) != 0) {
                stat = "paid";
            }
        } catch (Exception ex) {
            logger.error("tenderPaidORFree : " + userId + " : ", ex);
        }
        logger.debug("tenderPaidORFree : " + userId + " : Ends");
        return stat;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String checkForMail(String mailId) {
        logger.debug("checkForMail : " + userId + starts);
        String str = null;
        int size = 0;
        try {
            Object[] values = {"emailId", Operation_enum.EQ, mailId};
            size = tblLoginMasterDao.findTblLoginMaster(values).size();
        } catch (Exception ex) {
            logger.error("checkForMail : " + userId + " : " + ex);
        }

        if (size > 0) {

            str = "OK";
        } else {
            str = "Email ID does not exist,Please enter another Email ID";
        }
        logger.debug("checkForMail : " + userId + "ends");
        return str;
    }

    @Override
    public String getUserId(String email) {
        String userid = null;
        List<Object> list = hibernateQueryDao.singleColQuery("select tlm.userId from TblLoginMaster tlm where tlm.emailId = '" + email + "'");
        if (!list.isEmpty()) {
            userid = list.get(0).toString();
        }
        return userid;
    }

    @Override
    public String getEmailId(String strUserId) {
        String strEmailId = null;
        List<Object> list = hibernateQueryDao.singleColQuery("select tlm.emailId from TblLoginMaster tlm where tlm.userId = " + Integer.valueOf(strUserId));
        if (!list.isEmpty()) {
            strEmailId = list.get(0).toString();
        }
        return strEmailId;
    }

    @Override
    public Object getDocAvlMethod(String tenderId) {
        logger.debug("getDocAvlMethod : " + userId + " : Starts");
        logger.debug("getDocAvlMethod : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select ttd.docAvlMethod from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId.trim()).get(0);
    }

    @Override
    public List<Object> getLotNo(String tenderId) {
        logger.debug("getLotNo : " + userId + " : Starts");
        logger.debug("getLotNo : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select tls.lotNo from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId=" + tenderId.trim());
    }

    @Override
    public List<Object[]> getLotDetails(String tenderId) {
        logger.debug("getLotDetails : " + userId + " : Starts");
        logger.debug("getLotDetails : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery("select tls.appPkgLotId,tls.lotNo,tls.lotDesc from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId=" + tenderId.trim());
    }

    @Override
    public List<Object[]> getLotDetailsByPkgLotId(String pkgLotId, String tenderId) {
        logger.debug("getLotDetailsByPkgLotId : " + userId + " : Starts");
        logger.debug("getLotDetailsByPkgLotId : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery("select tls.lotNo,tls.lotDesc,tls.appPkgLotId from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId = " + tenderId + " and tls.appPkgLotId=" + pkgLotId);
    }

    @Override
    public List<Object[]> getLotDetiasByFormId(int formId) {
        logger.debug("getLotDetiasByFormId : " + userId + " : Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        String sqlQuery = "select distinct tls.lotNo, tls.lotDesc, tls.appPkgLotId "
                + "from TblTenderLotSecurity tls , TblTenderForms tf "
                + "where tls.appPkgLotId = tf.pkgLotId and tf.tenderFormId=" + formId;
        try {
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("getLotDetiasByFormId : " + userId + " : " + e);
        }

        logger.debug("getLotDetiasByFormId : " + userId + " : Ends");
        return list;
    }

    @Override
    public List<Object[]> getPkgDetialByTenderId(int tenderId) {
        logger.debug("getLotDetiasByFormId : " + userId + " : Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        String sqlQuery = "select packageNo, packageDescription from TblTenderDetails where tblTenderMaster.tenderId=" + tenderId;
        try {
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("getLotDetiasByFormId : " + userId + " : " + e);
        }

        logger.debug("getLotDetiasByFormId : " + userId + " : Ends");
        return list;
    }

    @Override
    public boolean isUserJVCA(String userId) {
        boolean isJvca = false;
        List<Object> list = hibernateQueryDao.singleColQuery("select lm.isJvca from TblLoginMaster lm where lm.userId=" + userId);
        if ((!list.isEmpty()) && list.get(0).toString().equalsIgnoreCase("yes")) {
            isJvca = true;
        }
        return isJvca;
    }

    @Override
    public List<Object[]> getPkgDetailWithAppPkgId(int tenderId) {
        logger.debug("getPkgDetailWithAppPkgId : " + userId + " : Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        String sqlQuery = "select tt.packageNo, tt.packageDescription,tl.appPkgLotId from TblTenderDetails tt,TblTenderLotSecurity tl where tt.tblTenderMaster.tenderId='" + tenderId + "' and tl.tblTenderMaster.tenderId=tt.tblTenderMaster.tenderId";
        try {
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("getPkgDetailWithAppPkgId : " + userId + " : " + e);
        }

        logger.debug("getPkgDetailWithAppPkgId : " + userId + " : Ends");
        return list;
    }

    @Override
    public List<TblLoginMaster> getUserType(int userId) {
        logger.debug("getUserType : " + userId + " : Starts");
        List<TblLoginMaster> list = new ArrayList<TblLoginMaster>();

        try {
            list = tblLoginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId);
        } catch (Exception e) {
            logger.error("getUserType : " + userId + " : " + e);
        }

        logger.debug("getUserType : " + userId + " : Ends");
        return list;
    }

    @Override
    public List<Object[]> getLotDetailsByTenderIdforEval(String tenderId) {
        logger.debug("getLotDetailsByTenderIdforEval : " + userId + " : Starts");
        logger.debug("getLotDetailsByTenderIdforEval : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery("select tls.lotNo,tls.lotDesc, tls.appPkgLotId from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId=" + tenderId.trim());
    }

    @Override
    public List<Object[]> getCompanyNameAndEntryDate(String tenderId, String pkgLotId,int evalCount) {
        logger.debug("getCompanyNameAndEntryDate : " + userId + " : Starts");
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tt.companyName,tp.siteVisitDate,tp.postQaulId,tp.userId,tp.postQualStatus,tp.rank,tp.noaStatus,tt.companyId,ttm.firstName,ttm.lastName ");
            query.append("from TblCompanyMaster tt,TblPostQualification tp,TblTendererMaster ttm  ");
            query.append("where tp.userId=ttm.tblLoginMaster.userId and tt.companyId=ttm.tblCompanyMaster.companyId and tp.pkgLotId=" + pkgLotId + " and tp.evalCount=" + evalCount + " and tp.tblTenderMaster.tenderId=" + tenderId + "order by tp.postQaulId");
        } catch (Exception e) {
            logger.error("getCompanyNameAndEntryDate : " + e);
        }
        logger.debug("getCompanyNameAndEntryDate : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    @Override
    public List<Object[]> getLotDetailsByTenderIdAndPkgIdforEval(String tenderId, String pckLotId) {
        logger.debug("getLotDetailsByTenderIdAndPkgIdforEval : " + userId + " : Starts");
        logger.debug("getLotDetailsByTenderIdAndPkgIdforEval : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery("select tls.lotNo,tls.lotDesc, tls.appPkgLotId from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId=" + tenderId.trim() + " and tls.appPkgLotId='" + pckLotId + "'");

    }

    @Override
    public List<Object[]> getData(String tenderId,int evalCount) {
        logger.debug("getData : " + userId + " : Starts");
        logger.debug("getData : " + userId + " : Ends");
        return hibernateQueryDao.createNewQuery("select tp.pkgLotId,tp.siteVisitDate from TblPostQualification tp where tp.tblTenderMaster.tenderId='" + Integer.parseInt(tenderId) + "' and tp.evalCount="+ evalCount +"");

    }

    @Override
    public List<TblEvalRoundMaster> getDataTblEvalRM(int tenderId,int evalCount) {
        logger.debug("getDataTblEvalRM : " + userId + " : Starts");
        List<TblEvalRoundMaster> list = new ArrayList<TblEvalRoundMaster>();
        try {
            list = tblEvalRoundMasterDao.findTblEvalRoundMaster("tenderId", Operation_enum.EQ, tenderId,"evalCount",Operation_enum.EQ,evalCount);
        } catch (Exception e) {
            logger.error("getDataTblEvalRM : " + e);
        }
        logger.debug("getDataTblEvalRM : " + userId + " : Ends");
        return list;
    }

    @Override
    public int updatedocHash(int companyDocId, String docHash) {
        return hibernateQueryDao.updateDeleteNewQuery("update TblCompanyDocuments cd set cd.docHash ='" + docHash + "' where cd.companyDocId = " + companyDocId);
    }

    @Override
    public String getAppIdByTenderId(String tenderId) {
        logger.debug("getAppIdByTenderId : " + userId + " : Starts");
        String appId = "";
        try {
            appId = hibernateQueryDao.singleColQuery("select appId from TblTenderMaster where tenderId=" + tenderId).get(0).toString();
        } catch (Exception e) {
            logger.error("getAppIdByTenderId : " + e);
        }
        logger.debug("getAppIdByTenderId : " + userId + " : Ends");
        return appId;
    }

    @Override
    public List<TblUserTypeMaster> getUserTypes() {
        logger.debug("getUserTypes : " + userId + " : Starts");
        List<TblUserTypeMaster> details = new ArrayList<TblUserTypeMaster>();
        try {
            details = tblUserTypeMasterDao.getAllTblUserTypeMaster();
        } catch (Exception e) {
            logger.error("getUserTypes : " + userId + e);
        }
        logger.debug("getUserTypes : " + userId + " : Ends");
        return details;
    }

    @Override
    public String getFormName(String formId) {
        List<Object> formList = hibernateQueryDao.singleColQuery("select tf.formName from TblTenderForms tf where tf.tenderFormId=" + formId);
        return formList.isEmpty() ? "" : formList.get(0).toString();
    }

    @Override
    public String getServiceTypeForTender(int tenderId) {
        logger.debug("getServiceTypeForTender : " + userId + " : Starts");
        String contType = "";
        try {
            contType = hibernateQueryDao.singleColQuery("select tt.contractType from TblTenderDetails tt where tt.tblTenderMaster.tenderId=" + tenderId).get(0).toString();
        } catch (Exception e) {
            logger.error("getServiceTypeForTender : " + e);
        }
        logger.debug("getServiceTypeForTender : " + userId + " : Ends");
        return contType;
    }

    @Override
    public String getEmailIdOfDG(String strUserTypeId) {
        logger.debug("getEmailIdOfDG : " + userId + " : Starts");
        String contType = "";
        try {
            contType = hibernateQueryDao.singleColQuery("select tl.emailId from TblLoginMaster tl where tl.tblUserTypeMaster.userTypeId=" + strUserTypeId).get(0).toString();
        } catch (Exception e) {
            logger.error("getEmailIdOfDG : " + e);
        }
        logger.debug("getEmailIdOfDG : " + userId + " : Ends");
        return contType;
    }

    @Override
    public String verifyMailforPaymentGateway(String mailId) {
        logger.debug("verifyMailforPaymentGateway : " + userId + starts);
        String str = null;
        int size = 0;
        try {
            Object[] values = {"emailId", Operation_enum.EQ, mailId};
            size = tblRegVasfeePaymentDao.findTblRegVasfeePayment(values).size();
        } catch (Exception ex) {
            logger.error("verifyMailforPaymentGateway : " + userId + " : " + ex);
        }
        if (size > 0) {
            str = "e-mail ID already exist, Please enter another e-mail ID";
        } else {
            str = "OK";
        }
        logger.debug("verifyMailforPaymentGateway : " + userId + ends);
        return str;
    }

    @Override
    public boolean getNoaStatus(int tenderId) {
        logger.debug("getNoaStatus : " + userId + " Starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            list = hibernateQueryDao.singleColQuery("select tna.acceptRejStatus from TblNoaAcceptance tna, TblNoaIssueDetails tnid where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tnid.tblTenderMaster.tenderId=" + tenderId + " and tna.acceptRejStatus='approved'");
            if (list != null && !list.isEmpty()) {
                if ("approved".equalsIgnoreCase(list.get(0).toString())) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            logger.error("getNoaStatus : " + userId + " : " + e);
        }
        logger.debug("getNoaStatus : " + userId + " Ends");
        return flag;
    }

    @Override
    public String getEmailAlertPreference(String strUserId) {
        logger.debug("EmailAlertPreference : " + userId + " Starts");
        String strEmailAlert = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select emailAlert from  TblUserPrefrence where userId =" + strUserId);
             if (list != null && list.size() > 0) {
                strEmailAlert = list.get(0).toString();
            }
        } catch (Exception e) {

            logger.error("EmailAlertPreference : " + userId + " : " + e);
        }
        logger.debug("EmailAlertPreference : " + userId + " Ends");
        return strEmailAlert;
    }

    @Override
    public String getSmsAlertPreference(String strUserId) {
        logger.debug("SmsAlertPreference : " + userId + " Starts");
        String strEmailAlert = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select smsAlert from  TblUserPrefrence where userId =" + strUserId);
            if (list != null && list.size() > 0) {
                strEmailAlert = list.get(0).toString();
            }
        } catch (Exception e) {

            logger.error("SmsAlertPreference : " + userId + " : " + e);
        }
        logger.debug("SmsAlertPreference : " + userId + " Ends");
        return strEmailAlert;
    }

    @Override
    public String getUserTypeId(String strUserId) {
        logger.debug("getUserTypeId : " + userId + " Starts");
        String usertypeid = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select tblUserTypeMaster.userTypeId from TblLoginMaster  where  userId ='" + strUserId + "'");
            if (list != null && list.size() > 0) {
                usertypeid = list.get(0).toString();
            }
        } catch (Exception e) {
            logger.error("getUserTypeId : " + userId + " : " + e);
        }
        logger.debug("getUserTypeId : " + userId + " Ends");
        return usertypeid;
    }

    @Override
    public List<TblEmailPrefMaster> getEmailPrefMaster(String Epid) {
        logger.debug("emailPrefMaster : " + userId + " Starts");
        List<TblEmailPrefMaster> list = null;

        try {
            String[] str = Epid.split(",");
            Integer[] obj = new Integer[str.length];
            for (int i = 0; i < str.length; i++) {
                obj[i] = Integer.valueOf(str[i]);
            }
           
            list = tblEmailPrefMasterDao.findTblEmailPrefMaster("epid", Operation_enum.IN, obj);
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("getUserTypeId : " + userId + " : " + e);
        }
        logger.debug("emailPrefMaster : " + userId + " Ends");
        return list;
    }

    @Override
    public List<TblSmsPrefMaster> getSmsPrefMaster(String Epid) {
        logger.debug("SmsPrefMaster : " + userId + " Starts");
        List<TblSmsPrefMaster> list = null;

        try {
            String[] str = Epid.split(",");
            Integer[] obj = new Integer[str.length];
            for (int i = 0; i < str.length; i++) {
                obj[i] = Integer.valueOf(str[i]);
            }
            list = tblSmsPrefMasterDao.findTblSmsPrefMaster("spid", Operation_enum.IN, obj);

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("SmsPrefMaster : " + userId + " : " + e);
        }
        logger.debug("SmsPrefMaster : " + userId + " Ends");
        return list;
    }

    @Override
    public List<Object> getEmailPreferencesByUseTypeId(int userTypeId) {
        logger.debug("EmailPreferencesByUseTypeId : " + userId + " Starts");
        List<Object> objs = null;
        try {
            objs = hibernateQueryDao.singleColQuery("select prefranceType from TblEmailPrefMaster  where  (usrtTypeID = '" + userTypeId + "' OR usrtTypeID like '%," + userTypeId + ",%' OR usrtTypeID like '%" + userTypeId + ",%')");

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("EmailPreferencesByUseTypeId : " + userId + " : " + e);
        }
        logger.debug("EmailPreferencesByUseTypeId : " + userId + " Ends");
        return objs;
    }

    @Override
    public List<Object> getSmsPreferencesByUseTypeId(int userTypeId) {
        logger.debug("SmsPreferencesByUseTypeId : " + userId + " Starts");
        List<Object> objs = null;
        try {
            objs = hibernateQueryDao.singleColQuery("select prefranceType from TblSmsPrefMaster  where  (usrtTypeID = '" + userTypeId + "' OR usrtTypeID like '%," + userTypeId + ",%' OR usrtTypeID like '%" + userTypeId + ",%')");
        } catch (Exception e) {

            logger.error("SmsPreferencesByUseTypeId : " + userId + " : " + e);
        }
        logger.debug("SmsPreferencesByUseTypeId : " + userId + " Ends");
        return objs;
    }

    @Override
    public String getUserTypeIdByMobileNo(String mobileno) {
        logger.debug("getUserTypeIdByMobileNo : " + userId + " Starts");
        List<Object> objs = new ArrayList<Object>();
        String str = "";
        String mobileno1 = mobileno;
        try {
            if(mobileno.length() >10){
                mobileno1 = mobileno.substring(mobileno.length()-10, mobileno.length());
            }
            //System.out.println("mobileno1mobileno1>>>>>"+mobileno1);
            List<Object> list1 = hibernateQueryDao.singleColQuery("select tblLoginMaster.userId from TblTendererMaster where mobileNo='" + mobileno1 + "'");
            List<Object> list2 = hibernateQueryDao.singleColQuery("select tblLoginMaster.userId from TblEmployeeMaster where mobileNo = '" + mobileno1 + "'");
            List<Object> list3 = hibernateQueryDao.singleColQuery("select tblLoginMaster.userId from TblPartnerAdmin where mobileNo ='" + mobileno1 + "'");
            if (list1 != null && list1.size() > 0) {
                objs.addAll(list1);
            }
            if (list2 != null && list2.size() > 0) {
                objs.addAll(list2);
            }
            if (list3 != null && list3.size() > 0) {
                objs.addAll(list3);
            }
            if (objs != null && objs.size() > 0) {
                str = objs.get(0).toString();
            }
        } catch (Exception e) {

            logger.error("getUserTypeIdByMobileNo : " + userId + " : " + e);
        }
        logger.debug("getUserTypeIdByMobileNo : " + userId + " Ends");
        return str;
    }

    /* Dohatec Start */
    @Override
    public Object getProcurementType(String tenderId) {
        logger.debug("getProcNature : " + userId + " : Starts");
        logger.debug("getProcNature : " + userId + " : Ends");
        return hibernateQueryDao.singleColQuery("select ttd.procurementType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderId).get(0);
    }
    /* Dohatec End */
    
    @Override
    public Date getServerDateTime() throws ParseException {
        List list = hibernateQueryDao.nativeSQLQuery("select GETUTCDATE()", null);
        Date retVal = null;
        if (list != null && !list.isEmpty()) {
            retVal = (Date) list.get(0);
        }
        return retVal;
    }
     /* Dohatec Start */
    @Override
    public List<Object[]> checkPD(String userId) {
        logger.debug("getProjectRole : " + userId + " : Starts");
        logger.debug("getProjectRole : " + userId + " : Ends");
        List<Object[]> list = null;
        //if(officeId.equals("0") || officeId.equals(""))
       // {
        list = hibernateQueryDao.createNewQuery("select pm.projectName,pm.projectId from TblProjectMaster pm"
                + " where  pm.projectId in ( select pr.tblProjectMaster.projectId from TblProjectRoles pr where pr.tblProcurementRole.procurementRoleId = 19 and pr.tblLoginMaster.userId = "+ userId +")");
      //  }
        /*else{
          list = hibernateQueryDao.createNewQuery("select pm.projectName,pm.projectId from TblProjectMaster pm"
                + " where  pm.projectId in ( select pr.tblProjectMaster.projectId from TblProjectRoles pr,TblProjectOffice po "
                + "where pr.tblProcurementRole.procurementRoleId = 19 and po.projectId = pr.tblProjectMaster.projectId and po.tblOfficeMaster.officeId  = "+ officeId +" and pr.tblLoginMaster.userId = "+ userId +")");
   
        }*/
        return list;
    }

    @Override
    public List<Object[]> getProjectOffice(String projectId) {
        logger.debug("getProjectRole : " + userId + " : Starts");
        logger.debug("getProjectRole : " + userId + " : Ends");
        List<Object[]> list = hibernateQueryDao.createNewQuery("select om.officeId,om.officeName"
                + " from TblOfficeMaster om"
                + " where om.officeId in ( select distinct po.tblOfficeMaster.officeId from TblProjectOffice po where po.projectId = "+ projectId +")");
    return list;
    }
    /* Dohatec End */

    @Override
    public Object checkOrgType(int deptId) {
        logger.debug("checkOrgType : " + userId + starts);
        Object values = null;
        
        try {
            //size = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm", " and tdm.departmentId = " + deptId);
            values = hibernateQueryDao.createNewQuery("select tdm.organizationType from TblDepartmentMaster tdm where tdm.departmentId = '" + deptId + "'");
        } catch (Exception ex) {
            logger.error("checkOrgType : " + userId + " :" + ex);
        }
       
        logger.debug("checkOrgType : " + userId + ends);
        return values;
    }
// nafiul
    @Override
    public List<TblActivityMaster> getActivityMasterData(){
        return tblActivityMasterDao.getAllTblActivityMaster();
    }

    /**
     * @return the tblActivityMasterDao
     */
    public TblActivityMasterDao getTblActivityMasterDao() {
        return tblActivityMasterDao;
    }

    /**
     * @param tblActivityMasterDao the tblActivityMasterDao to set
     */
    public void setTblActivityMasterDao(TblActivityMasterDao tblActivityMasterDao) {
        this.tblActivityMasterDao = tblActivityMasterDao;
    }
    
    @Override
    public List<TblUserTypeMaster> getUserTypeByUserTypeId(Object... values){
        logger.debug("getUserTypeByUserId : " + userId + starts);
        List<TblUserTypeMaster> result = null;
        
        try {
            result = getTblUserTypeMasterDao().findTblUserTypeMaster(values);
        } catch (Exception ex) {
            logger.error("getUserTypeByUserId : " + userId + " :" + ex);
        }
       
        logger.debug("getUserTypeByUserId : " + userId + ends);
        return result;
    }

    /**
     *
     * @param userId
     * @return
     */
    @Override
    public String getUserRoleByUserId(int userId){
        logger.debug("checkOrgType : " + userId + starts);
        String roles = "";
        List<?> obj = null;
        
        try {
            //size = hibernateQueryDao.countForNewQuery("TblDepartmentMaster tdm", " and tdm.departmentId = " + deptId);
            String query = "select pr.procurementRole from tbl_ProcurementRole pr where pr.procurementRoleId in (select er.procurementRoleId from tbl_EmployeeRoles er where er.employeeId=(select em.employeeId from tbl_EmployeeMaster em where em.userId='"+userId+"'))";
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (!obj.isEmpty() && obj != null) {
                for(Object x:obj){
                    if (!roles.equals("")) {
                        roles+=", ";
                    }
                    if(x.toString().equalsIgnoreCase("Hope")){roles+="HOPA";}
                    else if(x.toString().equalsIgnoreCase("Pe")){roles+="PA";}
                    else if(x.toString().equalsIgnoreCase("TOC/POC")){roles+="TOC";}
                    else if(x.toString().equalsIgnoreCase("TEC/PEC")){roles+="TEC";}
                    else roles+=x.toString();
                }
            }
        } catch (Exception ex) {
            logger.error("checkOrgType : " + userId + " :" + ex);
        }
       
        logger.debug("checkOrgType : " + userId + ends);
        return roles;
    }
    
     /**
     *
     * @param userId
     * @return
     */
    @Override
    public String getBankUserRoleByUserId(int userId){
        logger.debug("checkOrgType : " + userId + starts);
        String role = "";
         List<Object[]> list= null;
         String isAdmin ="";
    
        try {
            list = hibernateQueryDao.nativeSQLQuery("select pa.isMakerChecker, pa.isAdmin from tbl_PartnerAdmin pa where pa.userId='"+userId+"'", null);
            
            if (list != null && !list.isEmpty()) {
                for (Object[] obj : list) {
                     role= obj[0].toString();
                    isAdmin= obj[1].toString();
                }
            }
        }
        catch (Exception ex) {
            logger.error("checkOrgType : " + userId + " :" + ex);
        }
       
        logger.debug("checkOrgType : " + userId + ends);
        
        if (role.isEmpty()) {
               if(isAdmin.equalsIgnoreCase("Yes"))
                   return "Bank Admin";   
               else
                   return role;
        }
        else
              return role;
    }
    
     /**
     *
     * @param userId
     * @return
     */
    @Override
    public String getCompanyIdByUserId(int userId){
        logger.debug("checkOrgType : " + userId + starts);
        String companyId = "";
        List<?> obj = null;
        
        try {
            
            String query = "select companyId from tbl_TendererMaster where userId ='"+userId+"'";
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (!obj.isEmpty() && obj != null) {
                for(Object x:obj){
                     companyId=x.toString();
                }
            }
        } catch (Exception ex) {
            logger.error("checkOrgType : " + userId + " :" + ex);
        }
       
        logger.debug("checkOrgType : " + userId + ends);
        return companyId;
    }
    
    @Override
    public int updateTenderMaster(int createdBy, int tenderId) {
        return hibernateQueryDao.updateDeleteNewQuery("update TblTenderMaster tm set tm.createdBy ='" + createdBy + "' where tm.tenderId = " + tenderId);
    }
    
    
   /**
     *
     * @param tenderId
     * @return
     */
    @Override
    public int getOriginalUid(int tenderId){
        logger.debug("getOriginalUid : " + userId + starts);
        String originalUid = "";
        List<?> obj = null;
        
        try {
            
            String query = "select createdBy from tbl_TenderMaster where tenderId ="+tenderId;
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (!obj.isEmpty() && obj != null) {
                for(Object x:obj){
                     originalUid=x.toString();
                }
            }
        } catch (Exception ex) {
            logger.error("getOriginalUid : " + userId + " :" + ex);
        }
       
        logger.debug("getOriginalUid : " + userId + ends);
        return Integer.parseInt(originalUid);
    }

    /**
     * @return the tblAppPermissionDao
     */
    public TblAppPermissionDao getTblAppPermissionDao() {
        return tblAppPermissionDao;
    }

    /**
     * @param tblAppPermissionDao the tblAppPermissionDao to set
     */
    public void setTblAppPermissionDao(TblAppPermissionDao tblAppPermissionDao) {
        this.tblAppPermissionDao = tblAppPermissionDao;
    }
    
    
    
}
