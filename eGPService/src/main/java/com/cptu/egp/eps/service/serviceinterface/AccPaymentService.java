/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsCompansiateAmt;
import com.cptu.egp.eps.model.table.TblCmsInvAccHistory;
import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDocument;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface AccPaymentService {

    /**
     * for Setting userId at Service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * add Invoice Details to database
     * @param tblCmsInvoiceAccDetails
     * @return flag weather data is saved in to database or not
     */
    public boolean addInvoiceAccDetails(TblCmsInvoiceAccDetails tblCmsInvoiceAccDetails);

    /**
     * update Invoice Details to database
     * @param tblCmsInvoiceAccDetails
     * @return flag weather data is updated in to database or not
     */
    public boolean updateInvoiceAccDetails(TblCmsInvoiceAccDetails tblCmsInvoiceAccDetails);
 
    /**
     * add Invoice Downloading Documents Details to database
     * @param tblCmsInvoiceDocument
     * @return flag weather data is saved in to database or not
     */
    public boolean addInvoiceDocsDetails(TblCmsInvoiceDocument tblCmsInvoiceDocument);

    /**
     * gives Invoice Downloading Documents Details data
     * @param invoiceId
     * @return Document data list
     */
    public List<TblCmsInvoiceDocument> getInvoiceDocDetails(int invoiceId);

    /**
     * gives Invoice Downloading Documents Details data according to user type like PE User ,Tenderer
     * @param invoiceId
     * @param userTypeId
     * @return Document data list
     */
    public List<TblCmsInvoiceDocument> getInvoiceDocDetailsbyPassingUsertypeID(int invoiceId,int userTypeId);
    
    /**
     * this method is for deleting Documents Details data
     * @param InvoiceDocID
     * @return boolean, true - if deleted successfully otherwise not
     */
    public boolean deleteInvoiceDocsDetails(int InvoiceDocID);

    /**
     * gives no of Invoices listing data which is listed at Accountant side in MakePayment module main page
     * @param invStatus
     * @param userId
     * @param pageNo
     * @param Rowsize
     * @return Invoice Listing data list
     */
    public List<Object[]> getPaymentListing(String invStatus,int userId,int pageNo,int Rowsize);

    /**
     * gives no of Invoices listing Count for pagination at Accountant side in MakePayment module main page
     * @param invStatus
     * @param userId
     * @return long - count
     */
    public long getPaymentListingCount(String invStatus,int userId);

    /**
     * gives Invoice Details data from database which is calculated by accountant
     * @param invoiceId
     * @return Invoice Detail data list
     */
    public List<TblCmsInvoiceAccDetails> getInvoiceAccDetails(int invoiceId);

    /**
     * add Invoice history Details to database
     * @param tblCmsInvAccHistory
     * @return boolean, true - if added successfully otherwise not
     */
    public boolean addInvAccHistoryDetails(TblCmsInvAccHistory tblCmsInvAccHistory);

    /**
     * this method is for updating invoice status into database table
     * @param invStatus
     * @param InvoiceId
     * @return boolean, true - if updated successfully otherwise not
     */
    public boolean updateStatusInInvMaster(String invStatus, int InvoiceId);

    /**
     * gives Invoice status from database
     * @param InvoiceId
     * @return list Object
     */
    public List<Object> getInvoiceStatus(int InvoiceId);

    /**
     * add remarks details for particular invoice
     * @param tblCmsInvRemarks
     * @return boolean, true - if added successfully otherwise not
     */
    public boolean saveRemarks(TblCmsInvRemarks tblCmsInvRemarks);

    /**
     * gives Invoice Remarks details from database
     * @param invoiceId
     * @return Invoice data list
     */
    public List<TblCmsInvRemarks> getRemarksDetails(int invoiceId);

    /**
     * get no of total Invoice details for any particular wpId
     * @param Wpid
     * @return total Invoice amount data list
     */
    public List<Object[]>  getAllTotalInvoiceAmtForWp(int Wpid);

    /**
     * get no of total Invoice deduction details for any particular wpId
     * @param Wpid
     * @return total Invoice deducted amount data list
     */
    public List<Object[]>  getTotalDeDuctedInvoiceAmtForWp(int Wpid);

    /**
     * gives invoice amount details
     * @param ContractId
     * @return list object
     */
    public List<Object> getFirstInvoiceAmountDetails(int ContractId);

    /**
     * gives PE emailId
     * @param tenderID
     * @return list Object
     */
    public List<Object[]> getPEemailID(int tenderID);

    /**
     * gives accountant emailId
     * @param tenderID
     * @return list object
     */
    public List<Object[]> getAccountantEmailId(int tenderID);

    /**
     * gives total generated invoice amount for any particular tenderId
     * @param tenderId
     * @return list object
     */
    public List<Object>  getTotalInvoiceAmtFromImaster(int tenderId);

    /**
     * gives total generated invoice amount for any particular tenderId in repeat Order case
     * @param contractId
     * @return
     */
    public List<Object>  getTotalInvoiceAmtFromImasterForRO(int contractId);

    /**
     * gives total generated invoice amount from accountant
     * @param lotId
     * @return list of object
     */
    public List<Object[]>  getAllTotalInvoiceAmtFromAc(int lotId);

    /**
     * gives total generated invoice amount from accountant in repeat order case
     * @param contractId
     * @return list of object 
     */
    public List<Object[]>  getAllTotalInvoiceAmtFromAcForRO(int contractId);

    /**
     * gives invoice amount from invoice master table
     * @param tenderId
     * @return list of object
     */
    public List<Object[]>  getAllInvoiceAmtFromImaster(int tenderId);

    /**
     * gives invoice details amount data
     * @param lotId
     * @return list of object
     */
    public List<Object[]>  getAllInvoiceAmtFromAc(int lotId);

    /**
     * gives invoice amount remarks
     * @param InvoiceId
     * @return list object
     */
    public List<Object> getInvoiceRemarks(int InvoiceId);

    /**
     * gives userTypeId by passing emailId
     * @param emailId
     * @return string
     */
    public String  getUserTypeId(String emailId);

    /**
     * gives userId by passing emailId
     * @param emailId
     * @return string
     */
    public String  getUserId(String emailId);

    /**
     * gives mobile no with country code
     * @param userId
     * @return string
     */
    public String  getMobileNoWithCountryCode(String userId);

    /**
     * gives PE mobile no with country code
     * @param userId
     * @return string
     */
    public String  getPEMobileNoWithCountryCode(String userId);

    /**
     * gives procurement role Id
     * @param userId
     * @return string 
     */
    public String  getProcurementRoleID(String userId);

    /**
     * gives Bank name
     * @param TenderPaymentId
     * @return string
     */
    public String  getBankName(String TenderPaymentId);
    
    /**
     * gives Bank Branch Name
     * @param TenderPaymentId
     * @return string
     */
    public String getBranchName(String TenderPaymentId);
    
    /**
     * gives Bank user Mobile no with country code
     * @param userId
     * @return string 
     */
    public String  getBankUserMobileNowithCountryCode(String userId);

    /**
     * gives lotId by passing invoice Id
     * @param InvoiceId
     * @return string
     */
    public String getlotId(int InvoiceId);

    /**
     * gives advance contract amount details for particular tenderId
     * @param tenderId
     * @param lotId
     * @return string 
     */
    public String getAdvanceContractAmount(int tenderId,int lotId);

    /**
     * add compensate amount  to database
     * @param tblCmsCompansiateAmt
     * @return boolean, true - if added otherwise false
     */
    public boolean addCompansiateAmt(TblCmsCompansiateAmt tblCmsCompansiateAmt);

    /**
     * get compensate amount
     * @param PayId
     * @return list data
     */
    public List<TblCmsCompansiateAmt> getTblCmsCompansiateAmt(int PayId);
    
    /**
     * gives emailId
     * @param strUserId
     * @return string 
     */
    public String getEmailId(String strUserId);

    public List<Object[]> getCurrencyInAccGenerateBefore(int wpId,int tenderId);

    /**
     * gives total salvage amount in contract
     * @param strUserId
     * @return string
     */
    public String getSalavageContractAmount(int tenderId,int lotId);

}
