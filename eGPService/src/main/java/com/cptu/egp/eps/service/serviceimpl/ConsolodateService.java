/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsPrDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsPrMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsRoitemsDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsRomapDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsRomasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsTrackVariationDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsVariContractValDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsVariationOrderDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailDocsDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailHistoryrDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWpMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsWpTenderBoqmapDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblCmsPrDetail;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import com.cptu.egp.eps.model.table.TblCmsRoitems;
import com.cptu.egp.eps.model.table.TblCmsRomap;
import com.cptu.egp.eps.model.table.TblCmsRomaster;
import com.cptu.egp.eps.model.table.TblCmsTrackVariation;
import com.cptu.egp.eps.model.table.TblCmsVariContractVal;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import com.cptu.egp.eps.model.table.TblCmsWpDetailDocs;
import com.cptu.egp.eps.model.table.TblCmsWpDetailHistory;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.model.table.TblCmsWptenderBoqmap;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class ConsolodateService {

    TblCmsWpMasterDao tblCmsWpMasterDao;
    TblCmsTrackVariationDao trackVariationDao;
    TblCmsWpTenderBoqmapDao tblCmsWpTenderBoqmapDao;
    TblCmsWpDetailDao tblCmsWpDetailDao;
    HibernateQueryDao hibernateQueryDao;
    TblCmsWpDetailHistoryrDao tblCmsWpDetailHistoryrDao;
    TblCmsPrDetailDao prDetailDao;
    TblCmsPrMasterDao prMasterDao;
    TblCmsInvoiceMasterDao invoiceMasterDao;
    TblCmsInvoiceDetailsDao invoiceDetailsDao;
    TblCmsVariationOrderDao orderDao;
    TblCmsVariContractValDao valDao;
    TblCmsRoitemsDao cmsRoitemsDao;
    TblCmsRomasterDao cmsRomasterDao;
    TblCmsWpDetailDocsDao tblCmsWpDetailDocsDao;
    SpGeteGPCmsDataMore dataMore;
    TblCmsRomapDao tblCmsRomapDao;
    final Logger logger = Logger.getLogger(ConsolodateService.class);
    private String logUserId = "0";
    private SPTenderCommon tendercommon;

    /**
     *
     * @return
     */
    public String getLogUserId() {
        return logUserId;
    }

    /**
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *
     * @return
     */
    public TblCmsTrackVariationDao getTrackVariationDao() {
        return trackVariationDao;
    }

    /**
     *
     * @param trackVariationDao
     */
    public void setTrackVariationDao(TblCmsTrackVariationDao trackVariationDao) {
        this.trackVariationDao = trackVariationDao;
    }

    /**
     *
     * @return
     */
    public TblCmsRomapDao getTblCmsRomapDao() {
        return tblCmsRomapDao;
    }

    /**
     *
     * @param tblCmsRomapDao
     */
    public void setTblCmsRomapDao(TblCmsRomapDao tblCmsRomapDao) {
        this.tblCmsRomapDao = tblCmsRomapDao;
    }

    /**
     *
     * @return
     */
    public TblCmsRomasterDao getCmsRomasterDao() {
        return cmsRomasterDao;
    }

    /**
     *
     * @param cmsRomasterDao
     */
    public void setCmsRomasterDao(TblCmsRomasterDao cmsRomasterDao) {
        this.cmsRomasterDao = cmsRomasterDao;
    }

    /**
     *
     * @return
     */
    public TblCmsRoitemsDao getCmsRoitemsDao() {
        return cmsRoitemsDao;
    }

    /**
     *
     * @param cmsRoitemsDao
     */
    public void setCmsRoitemsDao(TblCmsRoitemsDao cmsRoitemsDao) {
        this.cmsRoitemsDao = cmsRoitemsDao;
    }

    /**
     *
     * @return
     */
    public TblCmsVariContractValDao getValDao() {
        return valDao;
    }

    /**
     *
     * @param valDao
     */
    public void setValDao(TblCmsVariContractValDao valDao) {
        this.valDao = valDao;
    }

    /**
     *
     * @return
     */
    public SpGeteGPCmsDataMore getDataMore() {
        return dataMore;
    }

    /**
     *
     * @param dataMore
     */
    public void setDataMore(SpGeteGPCmsDataMore dataMore) {
        this.dataMore = dataMore;
    }

    /**
     *
     * @return
     */
    public TblCmsInvoiceDetailsDao getInvoiceDetailsDao() {
        return invoiceDetailsDao;
    }

    /**
     *
     * @param invoiceDetailsDao
     */
    public void setInvoiceDetailsDao(TblCmsInvoiceDetailsDao invoiceDetailsDao) {
        this.invoiceDetailsDao = invoiceDetailsDao;
    }

    /**
     *
     * @return
     */
    public TblCmsVariationOrderDao getOrderDao() {
        return orderDao;
    }

    /**
     *
     * @param orderDao
     */
    public void setOrderDao(TblCmsVariationOrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     *
     * @return
     */
    public TblCmsInvoiceMasterDao getInvoiceMasterDao() {
        return invoiceMasterDao;
    }

    /**
     *
     * @param invoiceMasterDao
     */
    public void setInvoiceMasterDao(TblCmsInvoiceMasterDao invoiceMasterDao) {
        this.invoiceMasterDao = invoiceMasterDao;
    }

    /**
     *
     * @return
     */
    public TblCmsWpDetailHistoryrDao getTblCmsWpDetailHistoryrDao() {
        return tblCmsWpDetailHistoryrDao;
    }

    /**
     *
     * @param tblCmsWpDetailHistoryrDao
     */
    public void setTblCmsWpDetailHistoryrDao(TblCmsWpDetailHistoryrDao tblCmsWpDetailHistoryrDao) {
        this.tblCmsWpDetailHistoryrDao = tblCmsWpDetailHistoryrDao;
    }

    /**
     *
     * @return
     */
    public TblCmsWpDetailDao getTblCmsWpDetailDao() {
        return tblCmsWpDetailDao;
    }

    /**
     *
     * @param tblCmsWpDetailDao
     */
    public void setTblCmsWpDetailDao(TblCmsWpDetailDao tblCmsWpDetailDao) {
        this.tblCmsWpDetailDao = tblCmsWpDetailDao;
    }

    /**
     *
     * @return
     */
    public TblCmsWpMasterDao getTblCmsWpMasterDao() {
        return tblCmsWpMasterDao;
    }

    /**
     *
     * @param tblCmsWpMasterDao
     */
    public void setTblCmsWpMasterDao(TblCmsWpMasterDao tblCmsWpMasterDao) {
        this.tblCmsWpMasterDao = tblCmsWpMasterDao;
    }

    /**
     *
     * @return
     */
    public TblCmsWpTenderBoqmapDao getTblCmsWpTenderBoqmapDao() {
        return tblCmsWpTenderBoqmapDao;
    }

    /**
     *
     * @param tblCmsWpTenderBoqmapDao
     */
    public void setTblCmsWpTenderBoqmapDao(TblCmsWpTenderBoqmapDao tblCmsWpTenderBoqmapDao) {
        this.tblCmsWpTenderBoqmapDao = tblCmsWpTenderBoqmapDao;
    }

    /**
     *
     * @return
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     *
     * @param hibernateQueryDao
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsPrDetailDao getPrDetailDao() {
        return prDetailDao;
    }

    /**
     *
     * @param prDetailDao
     */
    public void setPrDetailDao(TblCmsPrDetailDao prDetailDao) {
        this.prDetailDao = prDetailDao;
    }

    /**
     *
     * @return
     */
    public TblCmsPrMasterDao getPrMasterDao() {
        return prMasterDao;
    }

    /**
     *
     * @param prMasterDao
     */
    public void setPrMasterDao(TblCmsPrMasterDao prMasterDao) {
        this.prMasterDao = prMasterDao;
    }

    /**
     *
     * @return
     */
    public TblCmsWpDetailDocsDao getTblCmsWpDetailDocsDao() {
        return tblCmsWpDetailDocsDao;
    }

    /**
     *
     * @param tblCmsWpDetailDocsDao
     */
    public void setTblCmsWpDetailDocsDao(TblCmsWpDetailDocsDao tblCmsWpDetailDocsDao) {
        this.tblCmsWpDetailDocsDao = tblCmsWpDetailDocsDao;
    }

    /**
     * This method is insert data into tbl_cms_wpMaster table
     * @param wpmaster
     * @return flag (true/false) for (success/fail)
     */
    public boolean AddToWpMaster(List<TblCmsWpMaster> wpmaster) {
        logger.debug("AddToWpMaster : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpMasterDao.updateOrSaveEstCost(wpmaster);
            flag = true;
        } catch (Exception e) {
            logger.error("AddToWpMaster : " + e + "starts");
        }
        logger.debug("AddToWpMaster : " + logUserId + "ends");
        return flag;
    }

    /**
     * This method is insert data into tbl_cms_trackVariation table
     * @param tctvs
     * @return flag (true/false) for (success/fail)
     */
    public boolean AddToTrackVariation(List<TblCmsTrackVariation> tctvs) {
        logger.debug("AddToWpMaster : " + logUserId + "starts");
        boolean flag = false;
        try {
            trackVariationDao.updateOrSaveEstCost(tctvs);
            flag = true;
        } catch (Exception e) {
            logger.error("AddToWpMaster : " + e + "starts");
        }
        logger.debug("AddToWpMaster : " + logUserId + "ends");
        return flag;
    }
    /**
     * This method is insert data into tbl_cms_boqTenderMap table
     * @param tenderboqmap
     * @return flag (true/false) for (success/fail)
     */
    public boolean AddToWpChild(List<TblCmsWptenderBoqmap> tenderboqmap) {
        logger.debug("AddToWpChild : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpTenderBoqmapDao.updateOrSaveEstCost(tenderboqmap);
            flag = true;
        } catch (Exception e) {
            logger.error("AddToWpChild : " + e + "starts");
        }
        logger.debug("AddToWpChild : " + logUserId + "ends");
        return flag;
    }
    /**
     * this method delete data of particular row by passing wpId
     * @param wpId
     * @return flag (true/false) for (success/fail)
     */
    public boolean delete(String wpId) {
        logger.debug("delete : " + logUserId + "starts");
        boolean flag = false;
        try {
            String quS = "delete from TblCmsWpMaster tw where tw.wpId=" + wpId;
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            logger.error("delete : " + e + "starts");
        }
        logger.debug("delete : " + logUserId + "ends");
        return flag;
    }
    /**
     * This method delete consolidation if NOA rejects
     * @param lotId
     */
    public void deleteIfNOARejects(String lotId) {
        logger.debug("deleteIfNOARejects : " + logUserId + "starts");
        boolean flag = false;
        try {
            String quS = "delete from TblCmsWpMaster tw where tw.wpLotId=" + lotId;
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            logger.error("deleteIfNOARejects : " + e + "starts");
        }
        logger.debug("deleteIfNOARejects : " + logUserId + "ends");

    }
    /**
     * This method set item status to completed if item received
     * @param rowId
     * @param tenderTableId
     * @return flag (true/false) for (success/fail)
     */
    public boolean updateMasterForQtyZero(String rowId, String tenderTableId) {
        logger.debug("updateMasteForQtyZero : " + logUserId + "starts");
        boolean flag = false;
        try {
            String quS = "update TblCmsWpDetail tcw set tcw.wpItemStatus='completed', tcw.completedDtNTime = GETDATE() where tcw.wpRowId='" + rowId + "' and tcw.tenderTableId='" + tenderTableId + "' and tcw.wpItemStatus = 'pending'";
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            logger.error("updateMasteForQtyZero : " + e + "starts");
        }
        logger.debug("updateMasteForQtyZero : " + logUserId + "ends");
        return flag;
    }
    /**
     * this method get the accepted quantity in progress report
     * @param rowId
     * @param tenderTableId
     * @param wpId
     * @return accepted quantity
     */
    public String getAcceptedQty(String rowId, String tenderTableId, int wpId) {
        logger.debug("getAcceptedQty : " + logUserId + "starts");
        List<Object> flag = null;
        String acceptqty = "";
        List<Object> listt = null;
        int prId = 0;
        try {
            listt = hibernateQueryDao.singleColQuery("select pr.progressRepId from TblCmsPrMaster pr where pr.tblCmsWpMaster.wpId = " + wpId);
            if (!listt.isEmpty() && listt != null) {
                prId = Integer.parseInt(listt.get(listt.size() - 1).toString());
            }
            String quS = "select tp.qtyAcceptTillThisPr from TblCmsPrDetail tp where tp.rowId='" + rowId + "' and tp.tenderTableId='" + tenderTableId + "' and tp.tblCmsPrMaster.progressRepId='" + prId + "'";
            flag = hibernateQueryDao.getSingleColQuery(quS);
            if (!flag.isEmpty()) {
                acceptqty = flag.get(0).toString();
            }
        } catch (Exception e) {
            logger.error("getAcceptedQty : " + e + "starts");
        }
        logger.debug("getAcceptedQty : " + logUserId + "ends");
        return acceptqty;
    }
    /**
     * this method get the all workplan of the particular lot
     * @param pkgId
     * @return list of Object
     */
    public List<Object> getWpId(int pkgId) {
        logger.debug("getWpId : " + logUserId + "starts");
        int flag = 0;
        List<Object> list = null;
        try {
            String query = "select tw.wpId from TblCmsWpMaster tw where tw.wpLotId=" + pkgId + " and tw.isRepeatOrder='no'";
            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty() && list != null) {
                flag = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getWpId : " + e + "starts");
        }
        logger.debug("getWpId : " + logUserId + "ends");
        return list;
    }
        /* Dohatec Start */
    /**
     * this method get the all workplan of the particular lot for Submitted Tender Forms
     * @param pkgId
     * @param tenderId
     * @return list of Object
     */
        public List<Object> getWpIdAdvance(int pkgId) {
        logger.debug("getWpIdAdvance : " + logUserId + "starts");
        int flag = 0;
        List<Object> list = null;
        try {
            String query = "select twm.wpId from TblCmsWpMaster twm,TblCmsWpDetail twd where twm.wpLotId=" + pkgId + " and twm.isRepeatOrder='no' and twd.itemInvStatus = 'advance' and twd.tblCmsWpMaster.wpId = twm.wpId";
            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty() && list != null) {
                flag = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getWpIdAdvance : " + e + "starts");
        }
        logger.debug("getWpIdAdvance : " + logUserId + "ends");
        return list;
    }

    public List<Object> getWpIdForTenderForms(int pkgId,String tenderId) {
        logger.debug("getWpId : " + logUserId + "starts");
        int flag = 0;
        List<Object> list = null;
        try {
                String  query = "select distinct tw.wpId from TblCmsWpMaster tw ";
                        query = query + " , TblTenderForms tf, TblTenderBidForm tbf , TblCmsWptenderBoqmap twb ";
                        query = query + " where tw.wpLotId=" + pkgId + " and tw.isRepeatOrder='no'";
                        query = query + " and tf.pkgLotId = tw.wpLotId and tbf.tblTenderForms.tenderFormId = tf.tenderFormId ";
                        query = query + " and tbf.userId in ";
                        query = query + " ( select tnid.userId FROM TblNoaIssueDetails tnid ";
                        query = query + " where tnid.tblTenderMaster.tenderId = " + tenderId + " and tnid.pkgLotId = " +  pkgId + "  ";
                        query = query + " and tnid.userId not in ( select tna.userId from TblNoaAcceptance tna , TblNoaIssueDetails tni ";
                        query = query + " where tni.tblTenderMaster.tenderId = " + tenderId + " and tna.acceptRejStatus <> 'approved' ";
                        query = query + " and tna.tblNoaIssueDetails.noaIssueId = tni.noaIssueId ))";
                        query = query + " and twb.tblCmsWpMaster.wpId = tw.wpId ";
                        query = query + " and twb.wpTenderFormId = tf.tenderFormId order by tw.wpId ";

            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty() && list != null) {
                flag = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getWpId : " + e + "starts");
        }
        logger.debug("getWpId : " + logUserId + "ends");
        return list;
    }

     /**
     * this method get the all workplan of the particular lot for Submitted Tender Forms
     * @param pkgId
     * @param tenderId
     * @param roundId
     * @return list of Object
     */
    public List<Object> getWpIdForTenderFormsForNoa(int pkgId,String tenderId,String roundId) {
        logger.debug("getWpId : " + logUserId + "starts");
        int flag = 0;
        List<Object> list = null;
        try {
                String  query = "select distinct tw.wpId from TblCmsWpMaster tw ";
                        query = query + " , TblTenderForms tf, TblTenderBidForm tbf , TblCmsWptenderBoqmap twb ";
                        query = query + " where tw.wpLotId=" + pkgId + " and tw.isRepeatOrder='no'";
                        query = query + " and tf.pkgLotId = tw.wpLotId and tbf.tblTenderForms.tenderFormId = tf.tenderFormId ";
                        query = query + " and tbf.userId in ";
                        query = query + " ( select distinct tbr.userId from TblBidderRank tbr where tbr.tblTenderMaster.tenderId = " + tenderId ;
                        query = query + "  and tbr.rank = ( select min(tr.rank) from TblBidderRank tr ";
                        query = query + " where tr.tblTenderMaster.tenderId = " + tenderId ;
                        query = query + " and tr.roundId = " + roundId ;
                        query = query + " and tr.userId not in ( select tna.userId from TblNoaAcceptance tna , TblNoaIssueDetails tni ";
                        query = query + " where tni.tblTenderMaster.tenderId = " + tenderId + " and tna.tblNoaIssueDetails.noaIssueId = tni.noaIssueId ";
                        query = query + "  and tna.acceptRejStatus <> 'approved' )))";
                        query = query + " and twb.tblCmsWpMaster.wpId = tw.wpId ";
                        query = query + " and twb.wpTenderFormId = tf.tenderFormId order by tw.wpId ";

            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty() && list != null) {
                flag = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getWpId : " + e + "starts");
        }
        logger.debug("getWpId : " + logUserId + "ends");
        return list;
    }

    /* Dohatec End */
    /**
     * This method get the wpId for service case
     * @param pkgId
     * @return WpId
     */
    public String getWpIdForService(int pkgId) {
        logger.debug("getWpIdForService : " + logUserId + "starts");
        String flag = "";
        List<Object> list = null;
        try {
            String query = "select tw.wpId from TblCmsWpMaster tw where tw.wpLotId=" + pkgId + " and tw.isRepeatOrder='no' and tw.wpName='16'";
            flag = hibernateQueryDao.getSingleColQuery(query).get(0).toString();

        } catch (Exception e) {
            logger.error("getWpId : " + e + "starts");
        }
        logger.debug("getWpId : " + logUserId + "ends");
        return flag;
    }
    /**
     * this method get the wpId for consolidation of forms
     * @param pkgId
     * @return wpId
     */
    public int getWpIdforconsolidate(int pkgId) {
        logger.debug("getWpIdforconsolidate : " + logUserId + "starts");
        int flag = 0;

        try {
            String query = "select tw.wpId from TblCmsWpMaster tw where tw.wpLotId=" + pkgId;
            List<Object> list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty() && list != null) {
                flag = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getWpIdforconsolidate : " + e + "starts");
        }
        logger.debug("getWpIdforconsolidate : " + logUserId + "ends");
        return flag;
    }
    /**
     * this method get the consolidation of the forms
     * @param wpId
     * @return Consolidation of forms
     */
    public String getConsolidate(String wpId) {
        logger.debug("getConsolidate : " + logUserId + "starts");
        String flag = "Consolidate of ";
        List<Object[]> list = null;
        try {
            String match = "";
            StringBuilder query = new StringBuilder();
            query.append("select distinct tw.wpTenderFormId,tf.formName from TblCmsWptenderBoqmap tw,TblTenderForms tf  ");
            query.append("where tf.tenderFormId=tw.wpTenderFormId and ");
            query.append("tf.tenderFormId in (select tw.wpTenderFormId from TblCmsWptenderBoqmap tw where tblCmsWpMaster.wpId=" + wpId + ")");
            list = hibernateQueryDao.createNewQuery(query.toString());
            if (!list.isEmpty() && list != null) {
                if (list.size() == 1) {
                    for (Object[] obj : list) {
                        match = match + obj[1].toString();
                    }
                } else {
                    int ii = 0;
                    for (Object[] obj : list) {
                        if (ii == 0) {
                            match = match + obj[1].toString();
                        } else {
                            match = match + " and " + obj[1].toString();
                        }
                        ii++;
                    }
                }
                flag = flag + match;
            }
        } catch (Exception e) {
            logger.error("getConsolidate : " + e + "starts");
        }
        logger.debug("getConsolidate : " + logUserId + "ends");
        return flag;
    }
    /**
     * This method add the data in tbl_cms_wpMaster
     * @param cmsWpMaster
     * @return flag (true/false) for (success/fail)
     */
    public boolean add(TblCmsWpMaster cmsWpMaster) {
        logger.debug("add : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpMasterDao.addTblCmsWpMaster(cmsWpMaster);
            flag = true;
        } catch (Exception e) {
            logger.error("add : " + e + "starts");
        }
        logger.debug("add : " + logUserId + "ends");
        return flag;
    }
    /**
     * This method add the data in tbl_cms_boQtenderMap
     * @param boqmap
     * @return flag (true/false) for (success/fail)
     */
    public boolean addinchild(TblCmsWptenderBoqmap boqmap) {
        logger.debug("addinchild : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpTenderBoqmapDao.addTblCmsWptenderBoqmap(boqmap);
            flag = true;
        } catch (Exception e) {
            logger.error("addinchild : " + e + "starts");
        }
        logger.debug("addinchild : " + logUserId + "ends");
        return flag;
    }
    /**
     * this method get then tenderTableId by passing then tenderFormId
     * @param tenderFormId
     * @return list of object
     */
    public List<Object> countTable(String tenderFormId) {
        logger.debug("countTable : " + logUserId + "starts");
        List<Object> cL = null;
        long flag = 0;
        String quS = "select tt.tenderTableId from TblTenderTables tt where tt.tblTenderForms.tenderFormId=" + tenderFormId;
        try {
            cL = hibernateQueryDao.getSingleColQuery(quS);
        } catch (Exception e) {
            logger.error("countTable : " + e + "starts");
        }
        logger.debug("countTable : " + logUserId + "ends");
        return cL;
    }
    /**
     * this method get the tenderFormId by passing wpId
     * @param wpId
     * @return list of object
     */
    public List<Object> getTenderFormId(String wpId) {
        logger.debug("getTenderFormId : " + logUserId + "starts");
        List<Object> cL = null;
        long flag = 0;
        String quS = "select tw.wpTenderFormId from TblCmsWptenderBoqmap tw where tw.tblCmsWpMaster.wpId=" + wpId;
        try {
            cL = hibernateQueryDao.getSingleColQuery(quS);
        } catch (Exception e) {
            logger.error("getTenderFormId : " + e + "starts");
        }
        logger.debug("getTenderFormId : " + logUserId + "ends");
        return cL;
    }
    /**
     * This method get the first form's column to check with other form
     * @param tenderFormId
     * @return list of object
     */
    public List<Object[]> getFirstObject(String tenderFormId) {
        logger.debug("getFirstObject : " + logUserId + "starts");
        String flag = "select tc.columnHeader,tc.dataType from TblTenderColumns tc where tc.tblTenderTables.tenderTableId=" + tenderFormId;
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery(flag);
        } catch (Exception e) {
            logger.error("getFirstObject : " + e + "starts");
        }
        logger.debug("getFirstObject : " + logUserId + "ends");
        return list;
    }
    /**
     * This method get the variation order list
     * @param wpId
     * @return list of variation order
     */
    public List<TblCmsVariationOrder> getListOfVariationOrder(int wpId) {
        logger.debug("getListOfVariationOrder : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId);
        } catch (Exception e) {
            logger.error("getListOfVariationOrder : " + e + "starts");
        }
        logger.debug("getListOfVariationOrder : " + logUserId + "ends");
        return list;
    }
    /**
     * This method get the variation order list for tenderer
     * @param wpId
     * @return list of variation order
     */
    public List<TblCmsVariationOrder> getListOfVariationOrderForTen(int wpId) {
        logger.debug("getListOfVariationOrderForTen : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.EQ, "accepted");
        } catch (Exception e) {
            logger.error("getListOfVariationOrderForTen : " + e + "starts");
        }
        logger.debug("getListOfVariationOrderForTen : " + logUserId + "ends");
        return list;
    }
    /**
     * count the number of variation order placed
     * @param wpId
     * @return count of variation order
     */
    public long countVariationOrder(int wpId) {
        logger.debug("countVariationOrder : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        long count = 0;
        try {
            String query = "select tt.wpId from TblCmsVariationOrder tt where tt.wpId=" + wpId;
            count = hibernateQueryDao.countForNewQuery("TblCmsVariationOrder tt", "tt.wpId=" + wpId);
        } catch (Exception e) {
            logger.error("countVariationOrder : " + e + "starts");
        }
        logger.debug("countVariationOrder : " + logUserId + "ends");
        return count;
    }
    /**
     * this method get the max variation order id
     * @param wpId
     * @return list of variation order
     */
    public List<TblCmsVariationOrder> getMaxVarOrdId(int wpId) {
        logger.debug("getMaxVarOrdId : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.IN, new String[]{"pending","approved"});
        } catch (Exception e) {
            logger.error("getMaxVarOrdId : " + e + "starts");
        }
        logger.debug("getMaxVarOrdId : " + logUserId + "ends");
        return list;
    }

    /**
     * gives max variation Order ID for accepted variation by tenderer
     * @param wpId
     * @return list of data
     */
    public List<TblCmsVariationOrder> getMaxVarOrdIdForAcceptVari(int wpId) {
        logger.debug("getMaxVarOrdIdForAcceptVari : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.EQ, "approved");
        } catch (Exception e) {
            logger.error("getMaxVarOrdIdForAcceptVari : " + e + "starts");
        }
        logger.debug("getMaxVarOrdIdForAcceptVari : " + logUserId + "ends");
        return list;
    }

    /**
     * saves workprogram data to the database
     * @param tcwds
     * @return boolean, return true - if saved successfully otherwise false
     * @throws Exception
     */
    public boolean addToWpDetails(List<TblCmsWpDetail> tcwds) throws Exception {
        logger.debug("addToWpDetails : " + logUserId + "starts");
        boolean flag = false;
        tblCmsWpDetailDao.updateOrSaveEstCost(tcwds);
        flag = true;
        logger.debug("addToWpDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method saves data to the database
     * @param tcwds
     * @return boolean, return true - if saved successfully otherwise false
     */
    public boolean addOneByOneToWpDetails(TblCmsWpDetail tcwds) {
        logger.debug("addOneByOneToWpDetails : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpDetailDao.addTblCmsWpDetail(tcwds);
            flag = true;
        } catch (Exception e) {
            logger.error("addOneByOneToWpDetails : " + e + "starts");
        }
        logger.debug("addOneByOneToWpDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method saves data to the history table in database
     * @param list1
     * @return boolean, return true - if saved successfully otherwise false
     * @throws Exception
     */
    public boolean addToDetailHistory(List<TblCmsWpDetailHistory> list1) throws Exception {
        logger.debug("addToDetailHistory : " + logUserId + "starts");
        boolean flag = false;
        tblCmsWpDetailHistoryrDao.updateOrSaveEstCost(list1);
        flag = true;

        logger.debug("addToDetailHistory : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives date data from database
     * @param parameter
     * @param first
     * @param max
     * @return list of table data
     */
    public List<TblCmsWpDetail> getDatesByWpId(String parameter, int first, int max) {
        logger.debug("getDatesByWpId : " + logUserId + "starts");
        List<TblCmsWpDetail> list = null;
        try {
            list = tblCmsWpDetailDao.findByCountTblCmsWpDetail(first, max, "tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(Integer.parseInt(parameter)));
        } catch (Exception e) {
            logger.error("getDatesByWpId : " + e + "starts");
        }
        logger.debug("getDatesByWpId : " + logUserId + "ends");
        return list;
    }

    /**
     * this method checks the data entry in table
     * @param wpId
     * @param tenderFormId
     * @return boolean, return false - if data is exist otherwise true
     */
    public boolean checkForWpDetailEntry(String wpId, String tenderFormId) {
        logger.debug("checkForWpDetailEntry : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = true;
        String query = "select tw.wpDetailId from TblCmsWpDetail tw,TblCmsWptenderBoqmap tq where tw.tblCmsWpMaster.wpId = tq.tblCmsWpMaster.wpId and tq.wpTenderFormId =" + tenderFormId + " and tw.tblCmsWpMaster.wpId =" + wpId;
        try {
            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("checkForWpDetailEntry : " + e + "starts");
        }
        logger.debug("checkForWpDetailEntry : " + logUserId + "ends");
        return flag;
    }

 /**
     * this method checks the data entry in table
     * @param wpId
     * @param tenderFormId
     * @return boolean, return false - if data is exist otherwise true
     */
    public boolean checkForWpDetailEntrybyTenderTableId(String wpId, String tenderTableId) {
        logger.debug("checkForWpDetailEntry : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = true;
        String query = "select tw.wpDetailId from TblCmsWpDetail tw where tw.tenderTableId =" + tenderTableId + " and tw.tblCmsWpMaster.wpId =" + wpId;
        try {
            list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("checkForWpDetailEntrybyTenderTableId : " + e + "starts");
        }
        logger.debug("checkForWpDetailEntrybyTenderTableId : " + logUserId + "ends");
        return flag;
    }


    /**
     * this method updates the data to the database
     * @param parameter
     * @param first
     * @param max
     * @return list of table data
     */
    public List<TblCmsTrackVariation> editVariOrder(String parameter, int first, int max) {
        logger.debug("editVariOrder : " + logUserId + "starts");
        List<TblCmsTrackVariation> list = null;
        try {
            list = trackVariationDao.findByCountTblCmsTrackVariation(first, max, "variOrdId", Operation_enum.EQ, Integer.parseInt(parameter));
        } catch (Exception e) {
            logger.error("editVariOrder : " + e + "starts");
        }
        logger.debug("editVariOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * gives variation order data list
     * @param parameter
     * @return list of table data
     */
    public List<TblCmsTrackVariation> getVariOrder(String parameter) {
        logger.debug("getVariOrder : " + logUserId + "starts");
        List<TblCmsTrackVariation> list = null;
        try {
            list = trackVariationDao.findTblCmsTrackVariation("variOrdId", Operation_enum.EQ, Integer.parseInt(parameter));
        } catch (Exception e) {
            logger.error("getVariOrder : " + e + "starts");
        }
        logger.debug("getVariOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * gives dates data  for work program module
     * @param parameter
     * @param first
     * @param max
     * @return list of data objects
     */
    public List<Object[]> getDatesByWpIdForWorks(String parameter, int first, int max) {
        logger.debug("getDatesByWpIdForWorks : " + logUserId + "starts");
        List<Object[]> list = null;
        List<Object> listt = null;
        int prId = 0;
        try {
            listt = hibernateQueryDao.singleColQuery("select pr.progressRepId from TblCmsPrMaster pr where pr.tblCmsWpMaster.wpId = " + parameter);
            if (!listt.isEmpty() && listt != null) {
                prId = Integer.parseInt(listt.get(listt.size() - 1).toString());
            }
            String query = "select twp.wpSrNo,twp.groupId,twp.wpDescription,twp.wpUom,twp.wpQty,"
                    + "twp.wpDetailId,twp.wpStartDate,twp.wpEndDate,twp.wpNoOfDays,twp.wpRowId,"
                    + "twp.tenderTableId,twp.wpRate,twp.amendmentFlag from TblCmsWpDetail twp "
                    + "where twp.tblCmsWpMaster.wpId=" + parameter + "  order BY twp.groupId";
            list = hibernateQueryDao.createByCountNewQuery(query, first, max);
        } catch (Exception e) {
            logger.error("getDatesByWpIdForWorks : " + e + "starts");
        }
        logger.debug("getDatesByWpIdForWorks : " + logUserId + "ends");
        return list;
    }

    /**
     * gives dates data for variation order case in work program module
     * @param parameter
     * @param first
     * @param max
     * @return list of data objects
     */
    public List<Object[]> getDatesByWpIdForWorksInVariation(String parameter, int first, int max) {
        logger.debug("getDatesByWpIdForWorksInVariation : " + logUserId + "starts");
        List<Object[]> list = null;
        List<Object> listt = null;
        int prId = 0;
        try {
            listt = hibernateQueryDao.singleColQuery("select pr.progressRepId from TblCmsPrMaster pr where pr.tblCmsWpMaster.wpId = " + parameter);
            if (!listt.isEmpty() && listt != null) {
                prId = Integer.parseInt(listt.get(listt.size() - 1).toString());
            }
            String query = "select twp.wpSrNo,twp.groupId,twp.wpDescription,twp.wpUom,twp.wpQty,twp.wpDetailId,twp.wpStartDate,twp.wpEndDate,twp.wpNoOfDays,twp.wpRowId,"
                    + "twp.tenderTableId,twp.wpRate,twp.amendmentFlag,prd.qtyAcceptTillThisPr from TblCmsWpDetail twp,TblCmsPrDetail prd  "
                    + "where twp.tblCmsWpMaster.wpId=" + parameter + " and prd.tenderTableId = twp.tenderTableId and prd.tblCmsPrMaster.progressRepId='" + prId + "' and prd.rowId=twp.wpRowId order BY twp.groupId";
            list = hibernateQueryDao.createByCountNewQuery(query, first, max);
        } catch (Exception e) {
            logger.error("getDatesByWpIdForWorksInVariation : " + e + "starts");
        }
        logger.debug("getDatesByWpIdForWorksInVariation : " + logUserId + "ends");
        return list;
    }

    /**
     * gives detail data for work program and delivery schedule module
     * @param wpId
     * @return list of table data
     */
    public List<TblCmsWpDetail> getAllDetail(String wpId) {
        logger.debug("getAllDetail : " + logUserId + "starts");
        List<TblCmsWpDetail> list = null;
        try {
            list = tblCmsWpDetailDao.findTblCmsWpDetail("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(Integer.parseInt(wpId)));
        } catch (Exception e) {
            logger.error("getAllDetail : " + e + "starts");
        }
        logger.debug("getAllDetail : " + logUserId + "ends");
        return list;
    }

    /**
     * gives wp detail data for displaying button
     * @param lotId
     * @return boolean, return true - if data is not available otherwise false
     */
    public boolean getWpDetailForButtonDisplay(String lotId, int boqFormCount) {
        logger.debug("getWpDetailForButtonDisplay : " + logUserId + "starts");
        List<TblCmsWpDetail> list = null;
        boolean showButton = false;
        try {
            List<Object> listDetail = this.getWpId(Integer.parseInt(lotId));
            if (!listDetail.isEmpty()) {
                for (int i = 0; i < listDetail.size(); i++) {
                    list = tblCmsWpDetailDao.findTblCmsWpDetail("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(Integer.parseInt(listDetail.get(i).toString())));
                    if (list.isEmpty() && boqFormCount == listDetail.size()) {
                        showButton = true;
                        break;
                    } else {
                        showButton = false;
                    }
                }
            }


        } catch (Exception e) {
            logger.error("getWpDetailForButtonDisplay : " + e + "starts");
        }
        logger.debug("getWpDetailForButtonDisplay : " + logUserId + "ends");
        return showButton;
    }

    /**
     * gives row count for boq listing data
     * @param parameter
     * @return long, return 0 - if boq data is not available otherwise anything else count
     */
    public long getRowcountForBoq(String parameter) {
        logger.debug("getDatesByWpId : " + logUserId + "starts");
        long list = 0;
        try {
            list = tblCmsWpDetailDao.countForQuery("TblCmsWpDetail tcd", "tcd.tblCmsWpMaster.wpId=" + parameter);
        } catch (Exception e) {
            logger.error("getDatesByWpId : " + e + "starts");
        }
        logger.debug("getDatesByWpId : " + logUserId + "ends");
        return list;
    }

    /**
     * this method updates the data of wp detail table to the database
     * @param detailId
     * @param date 
     */
    public void updateTable(int detailId, Date date) {
        logger.debug("updateTable : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsWpDetail tt set tt.wpEndDate='" + new java.sql.Date(date.getTime()) + "' where tt.wpDetailId=" + detailId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTable : " + e + "starts");
        }
        logger.debug("updateTable : " + logUserId + "ends");
    }

    /**
     * this method updates the data of wp detail table to the database
     * @param detailId
     * @param days
     * @param date
     */
    public void updateTable(int detailId, int days, Date date) {
        logger.debug("updateTable : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsWpDetail tt set tt.wpNoOfDays = '" + days + "',tt.wpEndDate='" + new java.sql.Date(date.getTime()) + "' where tt.wpDetailId=" + detailId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTable : " + e + "starts");
        }
        logger.debug("updateTable : " + logUserId + "ends");
    }

    /**
     * this method updates the data of wp detail table to the database
     * @param date
     * @param lotId
     */
    public void changeEndDate(Date date, int lotId) {
        logger.debug("changeEndDate : " + logUserId + "starts");
        try {
            List<Object> list = getWpId(lotId);
            for (Object object : list) {
                List<TblCmsWpDetail> details = getAllDetail(object.toString());
                {
                    for (TblCmsWpDetail tblCmsWpDetail : details) {
                        long days = date.getTime() + ((long) tblCmsWpDetail.getWpNoOfDays() * (24 * 60 * 60 * 1000));
                        Date fdate = new Date();
                        fdate.setTime(days);
                        updateTable(tblCmsWpDetail.getWpDetailId(), fdate);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("changeEndDate : " + e + "starts");
        }
        logger.debug("changeEndDate : " + logUserId + "ends");
    }

    /**
     * this method updates the data of wp detail table to the database in works case
     * @param detailId
     * @param sdate
     * @param edate
     * @param nod
     */
    public void updateTableForWorks(int detailId, Date sdate, Date edate, int nod) {
        logger.debug("updateTableForWorks : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsWpDetail tt set tt.wpEndDate='" + new java.sql.Date(edate.getTime()) + "',tt.wpStartDate='" + new java.sql.Date(sdate.getTime()) + "',tt.wpNoOfDays='" + nod + "' where tt.wpDetailId=" + detailId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTableForWorks : " + e + "starts");
        }
        logger.debug("updateTableForWorks : " + logUserId + "ends");
    }

    /**
     * this method updates the data of wp master table to the database in works case
     * @param wpId
     */
    public void updateTableForWorksForMaster(String wpId) {
        logger.debug("updateTableForWorksForMaster : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsWpMaster tt set tt.createdDate=current_timestamp() where tt.wpId=" + wpId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTableForWorksForMaster : " + e + "starts");
        }
        logger.debug("updateTableForWorksForMaster : " + logUserId + "ends");
    }

    /**
     * gives creation date and time from wp master table
     * @param wpId
     * @return list object
     */
    public Object getCreatedDate(String wpId) {
        logger.debug("getCreatedDate : " + logUserId + "starts");
        Object date = null;
        List<Object> list = null;
        try {
            String queryS = "select tt.createdDate from TblCmsWpMaster tt where tt.wpId=" + wpId;
            list = hibernateQueryDao.getSingleColQuery(queryS);
            if (list != null && !list.isEmpty()) {
                date = list.get(0);
            }
        } catch (Exception e) {
            logger.error("getCreatedDate : " + e + "starts");
        }
        logger.debug("getCreatedDate : " + logUserId + "ends");
        return date;

    }

    /**
     * this method updates the data of variation Order to the database in works case
     * @param detailId
     * @param qty
     */
    public void updateTableForWorksinVariOrder(int detailId, BigDecimal qty) {
        logger.debug("updateTableForWorksinVariOrder : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsWpDetail tt set tt.wpQty='" + qty + "',tt.amendmentFlag ='variationU',tt.wpItemStatus='pending' where tt.wpDetailId=" + detailId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTableForWorksinVariOrder : " + e + "starts");
        }
        logger.debug("updateTableForWorksinVariOrder : " + logUserId + "ends");
    }

    /**
     * this method updates the data of variation track table data to the database
     * @param trackid
     * @param qty
     */
    public void updateTrackvariationTable(int trackid, BigDecimal qty) {
        logger.debug("updateTrackvariationTable : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsTrackVariation tt set tt.qty='" + qty + "' where tt.variTrackId=" + trackid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateTrackvariationTable : " + e + "starts");
        }
        logger.debug("updateTrackvariationTable : " + logUserId + "ends");
    }

    /**
     * this method checks (gives) the data of variation order table
     * @param wpId
     * @return boolean, return true - if variation is to be allowed otherwise false
     */
    public boolean allowVariationOrderOrNot(int wpId) {
        logger.debug("allowVariationOrderOrNot : " + logUserId + "starts");
        boolean flag = true;
        try {
            List<TblCmsVariationOrder> list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.IN, new String[]{"pending", "approved"});
            if (list != null && !list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("allowVariationOrderOrNot : " + e + "starts");
        }
        logger.debug("allowVariationOrderOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives variation Order table data
     * @param wpId
     * @return list of table data
     */
    public List<TblCmsVariationOrder> getVariationOrderList(int wpId) {
        logger.debug("getVariationOrderList : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.EQ, "pending");
        } catch (Exception e) {
            logger.error("getVariationOrderList : " + e + "starts");
        }
        logger.debug("getVariationOrderList : " + logUserId + "ends");
        return list;
    }

    /**
     * gives max variation order list data
     * @param wpId
     * @return list of objects
     */
    public List<Object[]> getMaxVariOrderList(int wpId) {
        logger.debug("getMaxVariOrderList : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select MAX(tcvo.variOrdId),MAX(tcvo.variOrdStatus),MAX(tcvo.variOrdWFStatus) ");
            sb.append("from TblCmsVariationOrder tcvo where wpId=" + wpId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            logger.error("getMaxVariOrderList : " + e + "starts");
        }
        logger.debug("getMaxVariOrderList : " + logUserId + "ends");
        return list;
    }

    /**
     * this method checks(gives) the data for variation order table
     * @param wpId
     * @return boolean, return true - if variation is to be allowed otherwise false
     */
    public boolean allowVariationOrderOrNotForTenderer(int wpId) {
        logger.debug("allowVariationOrderOrNotForTenderer : " + logUserId + "starts");
        boolean flag = true;
        ;
        try {
            List<TblCmsVariationOrder> list = orderDao.findTblCmsVariationOrder("wpId", Operation_enum.EQ, wpId, "variOrdStatus", Operation_enum.EQ, "approved");
            if (list != null && !list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("allowVariationOrderOrNotForTenderer : " + e + "starts");
        }
        logger.debug("allowVariationOrderOrNotForTenderer : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method updates the status of variation order by accepting variation by tenderer
     * @param wpId
     */
    public void tensideVariOrderAccepted(int wpId) {
        logger.debug("tensideVariOrderAccepted : " + logUserId + "starts");
        try {
            String query = "update TblCmsVariationOrder tv set tv.variOrdStatus='accepted' where tv.wpId=" + wpId;
            hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception e) {
            logger.error("tensideVariOrderAccepted : " + e + "starts");
        }
        logger.debug("tensideVariOrderAccepted : " + logUserId + "ends");
    }

    /**
     * this method updates the status of variation order by send to tenderer event
     * @param varId
     */
    public void sendToContractorForApproved(int varId) {
        logger.debug("sendToContractorForApproved : " + logUserId + "starts");
        try {
            String query = "update TblCmsVariationOrder tv set tv.variOrdStatus='approved' where tv.variOrdId=" + varId;
            hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception e) {
            logger.error("sendToContractorForApproved : " + e + "starts");
        }
        logger.debug("sendToContractorForApproved : " + logUserId + "ends");
    }

    /**
     * this method checks the date data
     * @param detailId
     * @return list of objects data
     */
    public List<Object> checkForDate(int detailId) {
        logger.debug("checkForDate : " + logUserId + "starts");
        List<Object> list = null;
        try {
            String qyert = "select tt.wpEndDate from TblCmsWpDetail tt where tt.wpDetailId=" + detailId;
            list = hibernateQueryDao.singleColQuery(qyert);

        } catch (Exception e) {
            logger.error("checkForDate : " + e + "starts");
        }
        logger.debug("checkForDate : " + logUserId + "ends");
        return list;
    }

    /**
     * this method checks the date data in works case
     * @param detailId
     * @return list of Object data
     */
    public List<Object[]> checkForDateForWorks(int detailId) {
        logger.debug("checkForDateForWorks : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String qyert = "select tt.wpStartDate,tt.wpNoOfDays from TblCmsWpDetail tt where tt.wpDetailId=" + detailId;
            list = hibernateQueryDao.createNewQuery(qyert);

        } catch (Exception e) {
            logger.error("checkForDateForWorks : " + e + "starts");
        }
        logger.debug("checkForDateForWorks : " + logUserId + "ends");
        return list;
    }

    /**
     * gives quantity details data for works case
     * @param detailId
     * @return list of object
     */
    public List<Object> getQuantityForWorks(int detailId) {
        logger.debug("getQuantityForWorks : " + logUserId + "starts");
        List<Object> list = null;
        try {
            String qyert = "select tt.wpQty from TblCmsWpDetail tt where tt.wpDetailId=" + detailId;
            list = hibernateQueryDao.singleColQuery(qyert);

        } catch (Exception e) {
            logger.error("getQuantityForWorks : " + e + "starts");
        }
        logger.debug("getQuantityForWorks : " + logUserId + "ends");
        return list;
    }

    /**
     * gives wp detail data
     * @param detailId
     * @return list of objects data
     */
    public List<Object[]> getAllDetailByWpDetailId(int detailId) {
        logger.debug("getAllDetailByWpDetailId : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String query = "select twp.wpSrNo,twp.groupId,twp.wpDescription,twp.wpUom,twp.wpQty,"
                    + "twp.wpDetailId,twp.wpStartDate,twp.wpEndDate,twp.wpNoOfDays,twp.wpRowId,"
                    + "twp.tenderTableId,twp.wpRate,twp.amendmentFlag from TblCmsWpDetail twp "
                    + "where twp.wpDetailId=" + detailId + " ";
            list = hibernateQueryDao.createNewQuery(query);

        } catch (Exception e) {
            logger.error("getAllDetailByWpDetailId : " + e + "starts");
        }
        logger.debug("getAllDetailByWpDetailId : " + logUserId + "ends");
        return list;
    }

    /**
     * gives history data
     * @param wpId
     * @return list of table data
     */
    public List<TblCmsWpDetailHistory> getHistory(int wpId) {
        logger.debug("getHistory : " + logUserId + "starts");
        List<TblCmsWpDetailHistory> list = null;
        try {
            list = tblCmsWpDetailHistoryrDao.findTblCmsWpDetailHistory("wpId", Operation_enum.EQ, wpId);

        } catch (Exception e) {
            logger.error("getHistory : " + e + "starts");
        }
        logger.debug("getHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * this method update the data when finish edit dates event called
     * @param lotId
     */
    public void finishEditDates(int lotId) {
        logger.debug("finishEditDates : " + logUserId + "starts");
        List<TblCmsWpDetailHistory> list = null;
        try {
            String queryS = "update TblCmsWpMaster tt set tt.isDateEdited='yes' where tt.wpLotId=" + lotId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);

        } catch (Exception e) {
            logger.error("finishEditDates : " + e + "starts");
        }
        logger.debug("finishEditDates : " + logUserId + "ends");
    }

    /**
     * this method update the data when sendToPE event called in works case
     * @param wpId
     */
    public void sendToPEInWorks(String wpId) {
        logger.debug("sendToPEInWorks : " + logUserId + "starts");
        List<TblCmsWpDetailHistory> list = null;
        try {
            String queryS = "update TblCmsWpMaster tt set tt.wrkWpStatus='sendtope' where tt.wpId=" + wpId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);

        } catch (Exception e) {
            logger.error("sendToPEInWorks : " + e + "starts");
        }
        logger.debug("sendToPEInWorks : " + logUserId + "ends");
    }

    /**
     * this method check the data to the database
     * @param lotId
     * @return boolean, return true - if data is exist(means edited) otherwise false (means not edited)
     */
    public boolean checkForDateEditedOrNot(int lotId) {
        logger.debug("checkForDateEditedOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String qyert = "select tt.isDateEdited from TblCmsWpMaster tt where tt.isDateEdited='yes' and tt.wpLotId=" + lotId;
            list = hibernateQueryDao.singleColQuery(qyert);
            if (!list.isEmpty() && list != null) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("checkForDateEditedOrNot : " + e + "starts");
        }
        logger.debug("checkForDateEditedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method check the data to the database
     * @param wpId
     * @return boolean, return true - if data is exist otherwise false
     */
    public boolean checkForColorLegen(String wpId) {
        logger.debug("checkForColorLegen : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String query = "select tv.variOrdId from TblCmsVariationOrder tv where tv.wpId='" + wpId + "' and variOrdStatus='accepted'";
            list = hibernateQueryDao.singleColQuery(query.toString());
            if (!list.isEmpty() && list != null) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("checkForColorLegen : " + e + "starts");
        }
        logger.debug("checkForColorLegen : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method check the data to the database
     * @param tenderId
     * @param lotId
     * @return boolean, return true - if data is exist (means already signed) otherwise false (means not signed)
     */
    public boolean checkContractSigning(String tenderId, int lotId) {
        logger.debug("checkContractSigning : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select tn.noaIssueId from TblNoaIssueDetails tn,TblContractSign tcs");
            query.append(" where tn.tblTenderMaster.tenderId=" + tenderId + " and tn.pkgLotId=" + lotId + "");
            query.append(" and tcs.noaId = tn.noaIssueId");
            list = hibernateQueryDao.singleColQuery(query.toString());
            if (!list.isEmpty() && list != null) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("checkContractSigning : " + e + "starts");
        }
        logger.debug("checkContractSigning : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method adds the data to the database
     * @param order
     * @return boolean, return true - is added successfully otherwise not
     */
    public boolean addToVariationTable(TblCmsVariationOrder order) {
        logger.debug("addToVariationTable : " + logUserId + "starts");
        boolean flag = false;
        try {
            orderDao.addTblCmsVariationOrder(order);
            flag = true;

        } catch (Exception e) {
            logger.error("addToVariationTable : " + e + "starts");
        }
        logger.debug("addToVariationTable : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives contract Id data from db
     * @param tenderId
     * @param lotId
     * @return list of objects data
     */
    public List<Object[]> getContractId(String tenderId, String lotId) {
        logger.debug("getContractId : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select tc.contractSignId,tc.contractSignDt from TblNoaIssueDetails tn,TblContractSign tc");
            query.append(" where tn.tblTenderMaster.tenderId=" + tenderId + " and tn.pkgLotId=" + lotId + "");
            query.append(" and tc.noaId = tn.noaIssueId");
            list = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            logger.error("getContractId : " + e + "starts");
        }
        logger.debug("getContractId : " + logUserId + "ends");
        return list;
    }

    /**
     * add the data to the progress report master table
     * @param prMasters
     * @return boolean, return true - is added successfully otherwise not
     */
    public boolean addToPrMaster(List<TblCmsPrMaster> prMasters) {
        logger.debug("addToPrMaster : " + logUserId + "starts");
        boolean flag = false;
        try {
            prMasterDao.updateOrSaveEstCost(prMasters);
            flag = true;
        } catch (Exception e) {
            logger.error("addToPrMaster : " + e + "starts");
        }
        logger.debug("addToPrMaster : " + logUserId + "ends");
        return flag;
    }

    /**
     * add the data to the progress report details table
     * @param prDetails
     * @return boolean, return true - is added successfully otherwise not
     */
    public boolean addTpPrDetail(List<TblCmsPrDetail> prDetails) {
        logger.debug("addTpPrDetail : " + logUserId + "starts");
        boolean flag = false;
        try {
            prDetailDao.updateOrSaveEstCost(prDetails);
            flag = true;
        } catch (Exception e) {
            logger.error("addTpPrDetail : " + e + "starts");
        }
        logger.debug("addTpPrDetail : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method checks the progress report master data
     * @param wpId
     * @return boolean, return true - is data is exist otherwise false
     */
    public boolean checkForPrEntry(int wpId) {
        logger.debug("checkForPrEntry : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsPrMaster> list = null;
        try {
            list = prMasterDao.findTblCmsPrMaster("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(wpId), "status", Operation_enum.EQ, "pending");
            if (!list.isEmpty() && list != null) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("checkForPrEntry : " + e + "starts");
        }
        logger.debug("checkForPrEntry : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method checks the progress report master data
     * @param wpId
     * @return boolean, return true - is data is exist otherwise false
     */
    public boolean checkForPrEntryInWprks(int wpId) {
        logger.debug("checkForPrEntryInWprks : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsPrMaster> list = null;
        try {
            list = prMasterDao.findTblCmsPrMaster("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(wpId), "status", Operation_enum.EQ, "completed");
            if (!list.isEmpty() && list != null) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("checkForPrEntryInWprks : " + e + "starts");
        }
        logger.debug("checkForPrEntryInWprks : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method checks the second time progress report data
     * @param wpId
     * @return boolean, return true - is data is exist otherwise false
     */
    public boolean forSecondPR(int wpId) {
        logger.debug("forSecondPR : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsPrMaster> list = null;
        try {
            list = prMasterDao.findTblCmsPrMaster("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(wpId));
            if (!list.isEmpty() && list != null) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("forSecondPR : " + e + "starts");
        }
        logger.debug("forSecondPR : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives progress report data
     * @param wpId
     * @return list of table data 
     */
    public List<TblCmsPrMaster> getPRHistory(int wpId) {
        logger.debug("getPRHistory : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsPrMaster> list = null;
        try {
            list = prMasterDao.findTblCmsPrMaster("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(wpId), "status", Operation_enum.EQ, "completed");
            if (!list.isEmpty() && list != null) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("getPRHistory : " + e + "starts");
        }
        logger.debug("getPRHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * gives progress report data
     * @param wpId
     * @param first
     * @param max
     * @return list of objects data
     */
    public List<Object[]> getListForEdit(String wpId, int first, int max) {
        logger.debug("getListForEdit : " + logUserId + "starts");
        List<Object[]> list = null;
        List<Object> listt = null;
        int prId = 0;
        try {

            listt = hibernateQueryDao.singleColQuery("select pr.progressRepId from TblCmsPrMaster pr where pr.tblCmsWpMaster.wpId = " + wpId);
            if (!listt.isEmpty() && listt != null) {
                prId = Integer.parseInt(listt.get(listt.size() - 1).toString());
            }
            StringBuilder query = new StringBuilder();
            query.append(" select td.wpSrNo,td.wpDescription,td.wpUom, td.wpQty,tr.qtyAcceptTillThisPr, ");
            query.append(" tr.qtyDlvrdCurrPr,tr.totalPendingQty,tr.itemEntryDt, tr.remarks,");
            query.append(" td.wpRowId,tr.progressReptDtlId,tpr.progressRepId,td.tenderTableId,tpr.progressRepcreatedDt,td.amendmentFlag");
            query.append(" from TblCmsPrMaster tpr, TblCmsWpDetail td,TblCmsPrDetail tr ");
            query.append(" where td.tblCmsWpMaster.wpId=" + wpId + " and td.tblCmsWpMaster.wpId = tpr.tblCmsWpMaster.wpId");
            query.append(" and tpr.progressRepId = tr.tblCmsPrMaster.progressRepId");
            query.append(" and td.tenderTableId=tr.tenderTableId");
            query.append(" and td.wpRowId = tr.rowId and tr.tblCmsPrMaster.progressRepId=" + prId);
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForEdit : " + e + "starts");
        }
        logger.debug("getListForEdit : " + logUserId + "ends");
        return list;
    }

    /**
     * gives progress report data in works case
     * @param wpId
     * @param first
     * @param max
     * @return list of data 
     */
    public List<SPCommonSearchDataMore> getListForEditForWorks(String wpId, int first, int max) {
        logger.debug("getListForEditForWorks : " + logUserId + "starts");
        List<Object> listt = null;
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {

            listt = hibernateQueryDao.singleColQuery("select pr.progressRepId from TblCmsPrMaster pr where pr.tblCmsWpMaster.wpId = " + wpId);
            if (!listt.isEmpty() && listt != null) {
                prId = (listt.get(listt.size() - 1).toString());
            }
            mores = dataMore.executeProcedure("getListForEditForWorks", wpId, prId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);


        } catch (Exception e) {
            System.out.println("getListForEditForWorks : " + e + "starts");
        }
        logger.debug("getListForEditForWorks : " + logUserId + "ends");
        return mores;
    }

    /**
     * gives financial details data
     * @param tenderId
     * @param wpId
     * @param contId
     * @return list of objects data
     */
    public List<SPCommonSearchDataMore> getFinancialDetails(String tenderId, String wpId, String contId) {
        logger.debug("getFinancialDetails : " + logUserId + "starts");
        List<Object> listt = null;
        List<SPCommonSearchDataMore> mores = null;
        try {
            mores = dataMore.executeProcedure("financialProgressReport", tenderId, wpId, contId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            System.out.println("getFinancialDetails : " + e + "starts");
        }
        logger.debug("getFinancialDetails : " + logUserId + "ends");
        return mores;
    }

    /**
     * gives qualified user data access
     * @param tenderId
     * @return list of data
     */
    public List<SPCommonSearchDataMore> getQualifiedUser(String tenderId, String pckLotId) {
        logger.debug("getQualifiedUser : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("getQualifiedUser", tenderId, pckLotId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            System.out.println("getQualifiedUser : " + e + "starts");
        }
        logger.debug("getQualifiedUser : " + logUserId + "ends");
        return mores;
    }

    /**
     * this method is for showing consolidate data
     * @param userId
     * @param tenderId
     * @return list of data
     */
    public List<SPCommonSearchDataMore> isConsolidateShowOrNot(String userId, String tenderId) {
        logger.debug("getQualifiedUser : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("isConsolidateShowOrNot", tenderId, userId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            System.out.println("getQualifiedUser : " + e + "starts");
        }
        logger.debug("getQualifiedUser : " + logUserId + "ends");
        return mores;
    }

    /**
     * gives progress report details data
     * @param wpId
     * @param first
     * @param max
     * @param prId
     * @return list of data objects
     */
    public List<Object[]> getListForView(String wpId, int first, int max, int prId) {
        logger.debug("getListForEdit : " + logUserId + "starts");
        List<Object[]> list = null;
        List<Object> listt = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select td.wpSrNo,td.wpDescription,td.wpUom, td.wpQty,tr.qtyAcceptTillThisPr, ");
            query.append(" tr.qtyDlvrdCurrPr,tr.totalPendingQty,tr.itemEntryDt, tr.remarks,");
            query.append(" td.wpRowId,tr.progressReptDtlId,tpr.progressRepId,td.tenderTableId,tpr.progressRepcreatedDt");
            query.append(" from TblCmsPrMaster tpr, TblCmsWpDetail td,TblCmsPrDetail tr ");
            query.append(" where td.tblCmsWpMaster.wpId=" + wpId + " and td.tblCmsWpMaster.wpId = tpr.tblCmsWpMaster.wpId");
            query.append(" and tpr.progressRepId = tr.tblCmsPrMaster.progressRepId");
            query.append(" and td.tenderTableId=tr.tenderTableId");
            query.append(" and td.wpRowId = tr.rowId and tr.tblCmsPrMaster.progressRepId=" + prId);
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForEdit : " + e + "starts");
        }
        logger.debug("getListForEdit : " + logUserId + "ends");
        return list;
    }

    /**
     * gives progress report details data
     * @param wpId
     * @param first
     * @param max
     * @param prId
     * @return list of data objects
     */
    public List<Object[]> getListForViewForWorks(String wpId, int first, int max, int prId) {
        logger.debug("getListForEdit : " + logUserId + "starts");
        List<Object[]> list = null;
        List<Object> listt = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select td.wpSrNo,td.groupId,td.wpDescription,td.wpUom, tr.qtyDuringThisPr,tr.qtyAcceptTillThisPr, ");
            query.append(" tr.qtyDlvrdCurrPr,tr.totalPendingQty,tr.itemEntryDt, tr.remarks,");
            query.append(" td.wpRowId,tr.progressReptDtlId,tpr.progressRepId,td.tenderTableId,tpr.progressRepcreatedDt,td.amendmentFlag");
            query.append(" from TblCmsPrMaster tpr, TblCmsWpDetail td,TblCmsPrDetail tr ");
            query.append(" where td.tblCmsWpMaster.wpId=" + wpId + " and td.tblCmsWpMaster.wpId = tpr.tblCmsWpMaster.wpId");
            query.append(" and tpr.progressRepId = tr.tblCmsPrMaster.progressRepId");
            query.append(" and td.tenderTableId=tr.tenderTableId");
            query.append(" and td.wpRowId = tr.rowId and tr.tblCmsPrMaster.progressRepId=" + prId + " order BY td.groupId");
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForEdit : " + e + "starts");
        }
        logger.debug("getListForEdit : " + logUserId + "ends");
        return list;
    }

    /**
     * gives progress report table data
     * @param first
     * @param max
     * @param prId
     * @return list of objects data
     */
    public List<Object[]> getListForAnyItemAnyPercent(int first, int max, int prId) {
        logger.debug("getListForAnyItemAnyPercent : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate, pd.qtyAcceptTillThisPr, wd.itemInvGenQty,wd.wpDetailId,wd.currencyName,wd.supplierVat ");
            query.append(" from TblCmsWpDetail wd, TblCmsPrDetail pd");
            query.append(" where progressRepId ='" + prId + "'  and qtyAcceptTillThisPr != 0");
            query.append(" and wd.wpRowId = pd.rowId and wd.tenderTableId = pd.tenderTableId");
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForAnyItemAnyPercent : " + e + "starts");
        }
        logger.debug("getListForAnyItemAnyPercent : " + logUserId + "ends");
        return list;
    }

    /**
     * gives progress report table data
     * @param first
     * @param max
     * @param prId
     * @return list of objects data
     */
    public List<Object[]> getListForAnyItemAnyPercentForWorks(int first, int max, int prId) {
        logger.debug("getListForAnyItemAnyPercent : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.groupId,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate, pd.qtyAcceptTillThisPr, wd.itemInvGenQty,wd.wpDetailId ");
            query.append(" from TblCmsWpDetail wd, TblCmsPrDetail pd");
            query.append(" where progressRepId ='" + prId + "'  and qtyAcceptTillThisPr != 0");
            query.append(" and wd.wpRowId = pd.rowId and wd.tenderTableId = pd.tenderTableId order BY wd.groupId");
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForAnyItemAnyPercent : " + e + "starts");
        }
        logger.debug("getListForAnyItemAnyPercent : " + logUserId + "ends");
        return list;
    }

    /**
     * gives payment type
     * @param tenderId
     * @return string 
     */
    public String getPaymentType(String tenderId) {
        logger.debug("getPaymenttype : " + logUserId + "starts");
        List<Object> list = null;
        String paymentTerms = "";
        try {
            StringBuilder query = new StringBuilder();
            query.append("  select tc.paymentTerms from TblContractSign tc,");
            query.append(" TblNoaAcceptance ta, TblNoaIssueDetails tn");
            query.append(" where tn.tblTenderMaster.tenderId = " + tenderId + " and tn.noaIssueId=tc.noaId");
            query.append(" and ta.tblNoaIssueDetails.noaIssueId=tn.noaIssueId and tn.isRepeatOrder='no' and ta.acceptRejStatus='approved'");
            list = hibernateQueryDao.getSingleColQuery(query.toString());
            if (!list.isEmpty()) {
                paymentTerms = list.get(0).toString();
            }

        } catch (Exception e) {
            logger.error("getPaymentType : " + e + "starts");
        }
        logger.debug("getPaymentType : " + logUserId + "ends");
        return paymentTerms;
    }

    /**
     * gives payment type in repeat order case
     * @param tenderId
     * @param cntId
     * @return String
     */
    public String getPaymentTypeForRO(String tenderId, int cntId) {
        logger.debug("getPaymenttype : " + logUserId + "starts");
        List<Object> list = null;
        String paymentTerms = "";
        try {
            StringBuilder query = new StringBuilder();
            query.append("  select tc.paymentTerms from TblContractSign tc,");
            query.append(" TblNoaAcceptance ta, TblNoaIssueDetails tn,TblContractSign ts ");
            query.append(" where tn.tblTenderMaster.tenderId = " + tenderId + " and tn.noaIssueId=tc.noaId");
            query.append(" and ta.tblNoaIssueDetails.noaIssueId=tn.noaIssueId and ts.noaId=tn.noaIssueId and ts.contractSignId=" + cntId + " and ta.acceptRejStatus='approved'");
            list = hibernateQueryDao.getSingleColQuery(query.toString());
            if (!list.isEmpty()) {
                paymentTerms = list.get(0).toString();
            }

        } catch (Exception e) {
            logger.error("getPaymentType : " + e + "starts");
        }
        logger.debug("getPaymentType : " + logUserId + "ends");
        return paymentTerms;
    }

    /**
     * gives progress report table data
     * @param first
     * @param max
     * @param lotId
     * @param wpId
     * @return list of data objects 
     */
    public List<Object[]> getListAllItemWithFullPercent(int first, int max, int lotId, int wpId) {
        logger.debug("getListAllItemWithFullPercent : " + logUserId + "starts");
        List<Object> list = null;
        List<Object[]> os = null;
        try {
            String query = "select wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId in (select wp.wpId from TblCmsWpMaster wp where wp.wpLotId = '" + lotId + "') and wd.wpItemStatus = 'pending'";
            String query1 = "select wd.wpSrNo,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate,wd.itemInvGenQty,wd.itemInvGenQty,wd.wpDetailId,wd.currencyName,wd.supplierVat from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId = '" + wpId + "' and wd.itemInvStatus = 'pending'";
            list = hibernateQueryDao.singleColQuery(query);

            if (list.isEmpty()) {
                os = hibernateQueryDao.createByCountNewQuery(query1, first, max);
            } else {
                os = null;
            }

        } catch (Exception e) {
            logger.error("getListAllItemWithFullPercent : " + e + "starts");
        }
        logger.debug("getListAllItemWithFullPercent : " + logUserId + "ends");
        return os;
    }

    /**
     * gives progress report table data in works case
     * @param first
     * @param max
     * @param lotId
     * @param wpId
     * @return list of data objects 
     */
    public List<Object[]> getListAllItemWithFullPercentForWorks(int first, int max, int lotId, int wpId) {
        logger.debug("getListAllItemWithFullPercent : " + logUserId + "starts");
        List<Object> list = null;
        List<Object[]> os = null;
        try {
            String query = "select wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId in (select wp.wpId from TblCmsWpMaster wp where wp.wpLotId = '" + lotId + "') and wd.wpItemStatus = 'pending'";
            String query1 = "select wd.wpSrNo,wd.groupId,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate,wd.itemInvGenQty,wd.itemInvGenQty,wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId = '" + wpId + "' and wd.itemInvStatus = 'pending' order BY wd.groupId";
            list = hibernateQueryDao.singleColQuery(query);

            if (list.isEmpty()) {
                os = hibernateQueryDao.createByCountNewQuery(query1, first, max);
            } else {
                os = null;
            }

        } catch (Exception e) {
            logger.error("getListAllItemWithFullPercent : " + e + "starts");
        }
        logger.debug("getListAllItemWithFullPercent : " + logUserId + "ends");
        return os;
    }

    /**
     * this method checks the data to the database
     * @param lotId
     * @return boolean, true - if recieved not otherwise false
     */
    public boolean checkForItemFullyReceivedOrNot(int lotId) {
        logger.debug("checkForItemFullyReceivedOrNot : " + logUserId + "starts");
        List<Object> list = null;
        List<Object> F_list = null;
        boolean flag = false;
        try {
            String qus = "select wp.wpId from TblCmsWpMaster wp where wp.wpLotId =" + lotId;
            F_list = hibernateQueryDao.getSingleColQuery(qus);
            if (F_list != null && !F_list.isEmpty()) {
                String query = "select wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId in "
                        + "(select wp.wpId from TblCmsWpMaster wp where wp.wpLotId = " + lotId + " and wd.isRepeatOrder='no') "
                        + "and wd.wpItemStatus = 'pending' ";
                list = hibernateQueryDao.singleColQuery(query);
                if (list.isEmpty()) {
                    flag = true;
                } else {
                    flag = false;
                }

            } else {
                flag = true;
            }


        } catch (Exception e) {
            logger.error("checkForItemFullyReceivedOrNot : " + e + "starts");
        }
        logger.debug("checkForItemFullyReceivedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method checks the data to the database
     * @param tenderId
     * @return boolean, true - if recieved otherwise false
     */
    public boolean checkForItemFullyReceivedOrNotForServices(int tenderId) {
        logger.debug("checkForItemFullyReceivedOrNotForServices : " + logUserId + "starts");
        long count = 0;
        boolean flag = false;
        try {
            count = hibernateQueryDao.countForNewQuery("TblCmsSrvPaymentSch tcsp", "tcsp.srvFormMapId in ( select tcsfm.srvFormMapId from TblCmsSrvFormMap tcsfm where tcsfm.tenderId=" + tenderId + " and tcsfm.srvBoqId='12' ) and tcsp.status='pending'");
            flag = (count == 0);
        } catch (Exception e) {
            logger.error("checkForItemFullyReceivedOrNotForServices : " + e + "starts");
        }
        logger.debug("checkForItemFullyReceivedOrNotForServices : " + logUserId + "ends");
        return flag;
    }

    /**
     * adds repeat order data 
     * @param lotId
     * @return boolean
     */
    public boolean doRo(int lotId) {
        logger.debug("doRo : " + logUserId + "starts");
        List<Object> list = null;
        List<Object> listt = null;
        List<Object> listF = null;
        boolean flag = false;
        String first = "-1";
        String finalWpId = "";
        String wpId = "";
        try {
            String query = "select max(romId) from TblCmsRomaster wd where wd.lotId=" + lotId;
            list = hibernateQueryDao.singleColQuery(query);
            if (list != null && !list.isEmpty()) {
                String qur = "select wpId from TblCmsRomap where romId=" + list.get(0);
                listt = hibernateQueryDao.singleColQuery(qur);
                if (listt != null && !listt.isEmpty()) {
                    for (Object obj : listt) {
                        finalWpId = finalWpId + "," + obj.toString();
                    }
                    wpId = first + finalWpId;
                    String quetyLast = "select wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId in (" + wpId + ") and wd.wpItemStatus = 'pending'";
                    listF = hibernateQueryDao.getSingleColQuery(quetyLast);
                    if (listF != null && !listF.isEmpty()) {
                        flag = true;
                    } else {
                        updateIfNOARejectForRo(lotId);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("doRo : " + e + "starts");
        }
        logger.debug("doRo : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks the data for repeat order case
     * @param lotId
     * @param cntId
     * @return boolean
     */
    public boolean isCntCmptForRO(int lotId, int cntId) {
        logger.debug("isCntCmptForRO : " + logUserId + "starts");
        List<Object> listForRO = null;
        boolean flag = true;
        String wpId = "";
        try {
            StringBuilder query = new StringBuilder();
            query.append("select wd.wpItemStatus from TblNoaIssueDetails nd, TblContractSign cs,");
            query.append("TblEvalRoundMaster erm, TblCmsRomap rom, TblCmsWpDetail wd ");
            query.append("where cs.noaId = nd.noaIssueId and erm.roundId = nd.roundId and ");
            query.append("rom.romId = erm.romId and wd.tblCmsWpMaster.wpId = rom.wpId and nd.pkgLotId =" + lotId);
            query.append(" and nd.isRepeatOrder = 'yes' and cs.contractSignId =" + cntId + " and wd.wpItemStatus = 'pending'");
            listForRO = hibernateQueryDao.getSingleColQuery(query.toString());
            if (listForRO != null && !listForRO.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("isCntCmptForRO : " + e + "starts");
        }
        logger.debug("isCntCmptForRO : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method updates the status in RO master if noa is rejected in repeat order case 
     * @param lotId
     */
    public void updateIfNOARejectForRo(int lotId) {
        logger.debug("updateIfNOARejectForRo : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblCmsRomaster set isCurrent='no' where lotId=" + lotId + "");
        } catch (Exception e) {
            logger.error("updateIfNOARejectForRo : " + e);
        }
        logger.debug("updateIfNOARejectForRo : " + logUserId + "ends");
    }

    /**
     * this method checks if item received or not
     * @param wpId
     * @return boolean, return true - if not received otherwise false
     */
    public boolean checkForItemFullyReceivedOrNotByWpId(int wpId) {
        logger.debug("checkForItemFullyReceivedOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String query = "select wd.wpDetailId from TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId = '" + wpId + "' and wd.wpItemStatus = 'pending'";
            list = hibernateQueryDao.singleColQuery(query);
            if (list.isEmpty()) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("checkForItemFullyReceivedOrNot : " + e + "starts");
        }
        logger.debug("checkForItemFullyReceivedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives item data for each item full percent case
     * @param first
     * @param max
     * @param prId
     * @param wpId
     * @return list of objects data 
     */
    public List<Object[]> getListForEachItemFullPercent(int first, int max, int prId, int wpId) {
        logger.debug("getListForEachItemFullPercent : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate,wd.itemInvGenQty,pd.qtyAcceptTillThisPr,wd.wpDetailId,wd.currencyName,wd.supplierVat ");
            query.append(" from TblCmsWpDetail wd, TblCmsPrDetail pd,TblCmsPrMaster pm ");
            query.append(" where pm.tblCmsWpMaster.wpId = '" + wpId + "' and pd.tblCmsPrMaster.progressRepId ='" + prId + "' and wd.wpItemStatus='completed' ");
            query.append(" and pm.progressRepId = pd.tblCmsPrMaster.progressRepId and wd.wpRowId = pd.rowId and wd.tenderTableId = pd.tenderTableId and wd.itemInvStatus='pending'");
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForEachItemFullPercent : " + e + "starts");
        }
        logger.debug("listgetListForEachItemFullPercent : " + logUserId + "ends");
        return list;
    }

    /**
     * gives item data for each item full percent works case
     * @param first
     * @param max
     * @param prId
     * @param wpId
     * @return list of objects data 
     */
    public List<Object[]> getListForEachItemFullPercentForWorks(int first, int max, int prId, int wpId) {
        logger.debug("getListForEachItemFullPercent : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.groupId,wd.wpDescription,wd.wpUom,wd.wpQty,wd.wpRate,wd.itemInvGenQty,pd.qtyAcceptTillThisPr,wd.wpDetailId,wd.currencyName ");
            query.append(" from TblCmsWpDetail wd, TblCmsPrDetail pd,TblCmsPrMaster pm ");
            query.append(" where pm.tblCmsWpMaster.wpId = '" + wpId + "' and pd.tblCmsPrMaster.progressRepId ='" + prId + "' and wd.wpItemStatus='completed' ");
            query.append(" and pm.progressRepId = pd.tblCmsPrMaster.progressRepId and wd.wpRowId = pd.rowId and wd.tenderTableId = pd.tenderTableId and wd.itemInvStatus='pending' order BY wd.groupId");
            list = hibernateQueryDao.createByCountQuery(query.toString(), first, max);

        } catch (Exception e) {
            logger.error("getListForEachItemFullPercent : " + e + "starts");
        }
        logger.debug("listgetListForEachItemFullPercent : " + logUserId + "ends");
        return list;
    }

    /**
     * gives work program data
     * @param detailId
     * @return list of data
     */
    public List<TblCmsWpDetail> getDetailFromWpDetail(int detailId) {
        logger.debug("getDetailFromWpDetail : " + logUserId + "starts");
        List<TblCmsWpDetail> list = null;
        try {
            list = tblCmsWpDetailDao.findTblCmsWpDetail("wpDetailId", Operation_enum.EQ, detailId);

        } catch (Exception e) {
            logger.error("getDetailFromWpDetail : " + e + "starts");
        }
        logger.debug("getDetailFromWpDetail : " + logUserId + "ends");
        return list;
    }

    /**
     * saves data to the invoice master table
     * @param tcimds
     * @return boolean, return true - if saved successfully otherwise false
     */
    public boolean addToInvoiceMaster(TblCmsInvoiceMaster tcimds) {
        logger.debug("addToInvoiceMaster : " + logUserId + "starts");
        boolean flag = false;
        try {
            invoiceMasterDao.addTblCmsInvoiceMaster(tcimds);
            flag = true;
        } catch (Exception e) {
            logger.error("addToInvoiceMaster : " + e + "starts");
        }
        logger.debug("addToInvoiceMaster : " + logUserId + "ends");
        return flag;
    }

    /**
     * saves the data to the invoice details table
     * @param tcimds
     * @return boolean, return true - if saved successfully otherwise false
     */
    public boolean addToInvoiceDetail(List<TblCmsInvoiceDetails> tcimds) {
        logger.debug("addToInvoiceDetail : " + logUserId + "starts");
        boolean flag = false;
        try {
            invoiceDetailsDao.updateOrSaveEstCost(tcimds);
            flag = true;
        } catch (Exception e) {
            logger.error("addToInvoiceDetail : " + e + "starts");
        }
        logger.debug("addToInvoiceDetail : " + logUserId + "ends");
        return flag;
    }

    /**
     * update the data
     * @param detailid
     * @param qty
     * @return boolean, return true - if updated successfully otherwise false
     */
    public boolean updateWpDetailForInvoice(int detailid, double qty) {
        logger.debug("updateWpDetailForInvoice : " + logUserId + "starts");
        boolean flag = false;
        try {
            String quS = "update TblCmsWpDetail tcw set tcw.itemInvStatus='completed',tcw.itemInvGenQty=tcw.itemInvGenQty+" + qty + " where tcw.wpDetailId=" + detailid;
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            logger.error("updateWpDetailForInvoice : " + e + "starts");
        }
        logger.debug("updateWpDetailForInvoice : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives invoice Id
     * @param wpId
     * @return list objects
     */
    public List<Object> getInvoiceId(String wpId) {
        logger.debug("getInvoiceId : " + logUserId + "starts");
        List<Object> flag = null;
        try {
            String quS = "select ind.invoiceId from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "'";
            flag = hibernateQueryDao.singleColQuery(quS);

        } catch (Exception e) {
            logger.error("getInvoiceId : " + e + "starts");
        }
        logger.debug("getInvoiceId : " + logUserId + "ends");
        return flag;
    }

    /**
     * ckecks that invoice generated or not
     * @param invId
     * @return boolean, return true - if generated otherwise false
     */
    public boolean checkInvGeneratedOrNot(String invId) {
        logger.debug("getInvoiceId : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select ind.invoiceId from TblCmsInvoiceMaster ind where ind.invoiceId='" + invId + "' and ind.invStatus='createdbype'";
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("getInvoiceId : " + e + "starts");
        }
        logger.debug("getInvoiceId : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives list of invoice generated data
     * @param invid
     * @return list of objects data
     */
    public List<Object[]> getListForInvoice(int invid) {
        logger.debug("getListForInvoice : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.wpDescription,wd.wpUom,id.itemInvQty,id.itemInvAmt,im.totalInvAmt,im.tblCmsWpMaster.wpId,wd.wpRate,wd.wpQty ");
            query.append(" from TblCmsInvoiceDetails id,TblCmsWpDetail wd,TblCmsInvoiceMaster im ");
            query.append(" where id.tblCmsInvoiceMaster.invoiceId = " + invid + " and id.wpDetailId = wd.wpDetailId ");
            query.append(" and im.invoiceId = id.tblCmsInvoiceMaster.invoiceId");
            list = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            logger.error("getListForInvoice : " + e + "starts");
        }
        logger.debug("getListForInvoice : " + logUserId + "ends");
        return list;
    }

    public List<Object[]> getListForICTInvoice(String invNo,int wpId) {
        logger.debug("getListForICTInvoice : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.wpDescription,wd.wpUom,id.itemInvQty,id.itemInvAmt,im.totalInvAmt,im.tblCmsWpMaster.wpId,wd.wpRate,wd.wpQty,wd.currencyName,wd.supplierVat ");
            query.append(" from TblCmsInvoiceDetails id,TblCmsWpDetail wd,TblCmsInvoiceMaster im ");
            query.append(" where im.invoiceNo = '" + invNo + "' and wd.tblCmsWpMaster.wpId = " + wpId + " and id.wpDetailId = wd.wpDetailId ");
            query.append(" and im.invoiceId = id.tblCmsInvoiceMaster.invoiceId");
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getListForICTInvoice : " + e + "starts");
        }
        logger.debug("getListForICTInvoice : " + logUserId + "ends");
        return list;
    }

    /**
     * gives list data for invoice generated in works case
     * @param invid
     * @return list of data
     */
    public List<Object[]> getListForInvoiceForWorks(int invid) {
        logger.debug("getListForInvoiceForWorks : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select wd.wpSrNo,wd.groupId,wd.wpDescription,wd.wpUom,id.itemInvQty,id.itemInvAmt,im.totalInvAmt,im.tblCmsWpMaster.wpId,wd.wpRate,wd.wpQty ");
            query.append(" from TblCmsInvoiceDetails id,TblCmsWpDetail wd,TblCmsInvoiceMaster im ");
            query.append(" where id.tblCmsInvoiceMaster.invoiceId = " + invid + " and id.wpDetailId = wd.wpDetailId ");
            query.append(" and im.invoiceId = id.tblCmsInvoiceMaster.invoiceId order BY wd.groupId");
            list = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            logger.error("getListForInvoiceForWorks : " + e + "starts");
        }
        logger.debug("getListForInvoiceForWorks : " + logUserId + "ends");
        return list;
    }

    /**
     * updates the status of invoice
     * @param wpId
     * @param remarks
     * @return boolean, return true - if updated successfully otherwise false
     */
    public boolean updateStatusInInvMaster(int wpId, String remarks) {
        logger.debug("updateStatusInInvMaster : " + logUserId + "starts");
        boolean flag = false;
        try {
            String quS = "update TblCmsInvoiceMaster tcw set tcw.invStatus='acceptedbype',tcw.remarks='" + remarks + "' where tcw.invoiceId=" + wpId;
            hibernateQueryDao.updateDeleteNewQuery(quS);
            flag = true;
        } catch (Exception e) {
            logger.error("updateStatusInInvMaster : " + e + "starts");
        }
        logger.debug("updateStatusInInvMaster : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives invoice details by invoice Id
     * @param invId
     * @return list of objects data
     */
    public List<TblCmsInvoiceDetails> getAllDetailByInvoiceId(int invId) {
        logger.debug("getAllDetailByInvoiceId : " + logUserId + "starts");
        List<TblCmsInvoiceDetails> list = null;
        try {
            list = invoiceDetailsDao.findTblCmsInvoiceDetails("tblCmsInvoiceMaster", Operation_enum.EQ, new TblCmsInvoiceMaster(invId));
        } catch (Exception e) {
            logger.error("getAllDetailByInvoiceId : " + e + "starts");
        }
        logger.debug("getAllDetailByInvoiceId : " + logUserId + "ends");
        return list;
    }

    /**
     * gives invoice total amount
     * @param InvoiceId
     * @return list objects 
     */
    public List<Object[]> getInvoiceTotalAmt(String InvoiceId) {
        logger.debug("getInvoiceTotalAmt : " + logUserId + "starts");
        List<Object[]> flag = null;
        try {
            String quS = "select distinct ind.totalInvAmt,ind.invoiceId from TblCmsInvoiceMaster ind,TblCmsInvoiceDetails id,TblCmsWpDetail wd where wd.wpDetailId = id.wpDetailId and ind.invoiceId =id.tblCmsInvoiceMaster.invoiceId and ind.invoiceId = " + InvoiceId + "";
            flag = hibernateQueryDao.createNewQuery(quS);

        } catch (Exception e) {
            logger.error("getInvoiceTotalAmt : " + e + "starts");
        }
        logger.debug("getInvoiceTotalAmt : " + logUserId + "ends");
        return flag;
    }

    public List<Object[]> getInvoiceTotalAmtAdv(String InvoiceId) {
        logger.debug("getInvoiceTotalAmt : " + logUserId + "starts");
        List<Object[]> flag = null;
        try {
            String quS = "select ind.totalInvAmt,ind.invoiceId from TblCmsInvoiceMaster ind where ind.invoiceId = " + InvoiceId + "";
            flag = hibernateQueryDao.createNewQuery(quS);

        } catch (Exception e) {
            logger.error("getInvoiceTotalAmt : " + e + "starts");
        }
        logger.debug("getInvoiceTotalAmt : " + logUserId + "ends");
        return flag;
    }

     public List<Object[]> getInvoiceTotalAmtForICT(int InvoiceId,int wpId) {
        logger.debug("getInvoiceTotalAmtForICT : " + logUserId + "starts");
        List<Object[]> flag = null;
        try {
            String quS = "select ind.totalInvAmt,wd.currencyName,ind.invoiceId from TblCmsInvoiceMaster ind,TblCmsInvoiceDetails id,TblCmsWpDetail wd"
                    + " where wd.wpDetailId = id.wpDetailId and ind.invoiceId = id.tblCmsInvoiceMaster.invoiceId and"
                    + " ind.invoiceId in (select im.invoiceId from TblCmsInvoiceMaster im where im.invoiceNo = (select invoiceNo from TblCmsInvoiceMaster where invoiceId = " + InvoiceId + "  ) and im.tblCmsWpMaster.wpId = "+ wpId +" ) order by ind.totalInvAmt desc";
            flag = hibernateQueryDao.createNewQuery(quS);
        } catch (Exception e) {
            logger.error("getInvoiceTotalAmtForICT : " + e + "starts");
        }
        logger.debug("getInvoiceTotalAmtForICT : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives invoice id list
     * @param wpId
     * @return list of objects data
     */
    public List<Object[]> getInvoiceIdList(String wpId) {
        logger.debug("getInvoiceIdList : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String quS = "select ind.invoiceId,ind.invStatus,ind.invoiceNo from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "' and ind.isAdvInv='No'";
            list = hibernateQueryDao.createNewQuery(quS);
        } catch (Exception e) {
            logger.error("getInvoiceIdList : " + e + "starts");
        }
        logger.debug("getInvoiceIdList : " + logUserId + "ends");
        return list;
    }


    public List<Object[]> getInvoiceIdListForICT(String wpId) {
        logger.debug("getInvoiceIdListForICT : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String quS = "select distinct ind.invoiceNo,ind.invStatus from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId=" + Integer.parseInt(wpId) + " and ind.isAdvInv='No' order by ind.invoiceNo desc";
            list = hibernateQueryDao.createNewQuery(quS);
        } catch (Exception e) {
            logger.error("getInvoiceIdListForICT : " + e + "starts");
        }
        logger.debug("getInvoiceIdListForICT : " + logUserId + "ends");
        return list;
    }

    /**
     * it checks work completed or not
     * @param lotId
     * @return boolean, return true - if completed otherwise false
     */
    public boolean isWorkCompleteOrNot(String lotId) {
        logger.debug("isWorkCompleteOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select twc.remarks from TblCmsWcCertificate twc, TblContractSign ts, TblNoaIssueDetails nid where ts.contractSignId=twc.contractId and ts.noaId = nid.noaIssueId and nid.pkgLotId = " + lotId + " and twc.isWorkComplete = 'yes' and nid.isRepeatOrder='no'";
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("isWorkCompleteOrNot : " + e + "starts");
        }
        logger.debug("isWorkCompleteOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * it checks work completed or not in repeat order case
     * @param lotId
     * @param cntId
     * @return boolean, return true - if completed otherwise false
     */
    public boolean isWorkCompleteOrNotForRO(String lotId, int cntId) {
        logger.debug("isWorkCompleteOrNotForRO : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select twc.remarks from TblCmsWcCertificate twc, TblContractSign ts, TblNoaIssueDetails nid "
                    + "where ts.contractSignId=twc.contractId and ts.noaId = nid.noaIssueId "
                    + "and nid.pkgLotId = " + lotId + " and twc.isWorkComplete = 'yes' and ts.contractSignId=" + cntId;
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("isWorkCompleteOrNotForRO : " + e + "starts");
        }
        logger.debug("isWorkCompleteOrNotForRO : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks the date edited or not 
     * @param wpId
     * @param status
     * @return boolean, return true - if edited otherwise false
     */
    public boolean isDateEditedInWorksTenSide(String wpId, String status) {
        logger.debug("isDateEditedInWorksTenSide : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select twc.wpId from TblCmsWpMaster twc where twc.wpId=" + wpId + " and twc.wrkWpStatus='" + status + "'";
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("isDateEditedInWorksTenSide : " + e + "starts");
        }
        logger.debug("isDateEditedInWorksTenSide : " + logUserId + "ends");
        return flag;
    }

    /**
     * it checks the contract is terminated or not
     * @param lotId
     * @return boolean, return true - if terminated  otherwise false
     */
    public boolean isContractTerminatedOrNot(String lotId) {
        logger.debug("isContractTerminatedOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select twc.reason from TblCmsContractTermination twc, TblContractSign ts, TblNoaIssueDetails nid where ts.contractSignId=twc.contractId and ts.noaId = nid.noaIssueId and nid.pkgLotId = " + lotId + " and twc.status = 'approved' and twc.workflowStatus = 'approved' and nid.isRepeatOrder='no'";
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("isContractTerminatedOrNot : " + e + "starts");
        }
        logger.debug("isContractTerminatedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * it checks the contract is terminated or not in repeat order case 
     * @param lotId
     * @param cntId
     * @return boolean, return true - if terminated  otherwise false
     */
    public boolean isContractTerminatedOrNotForRO(String lotId, int cntId) {
        logger.debug("isContractTerminatedOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "select twc.reason from TblCmsContractTermination twc, TblContractSign ts,"
                    + " TblNoaIssueDetails nid where ts.contractSignId=twc.contractId "
                    + "and ts.noaId = nid.noaIssueId and nid.pkgLotId = " + lotId + " "
                    + "and twc.status = 'approved' and ts.contractSignId=" + cntId + " twc.workflowStatus = 'approved' and nid.isRepeatOrder='no'";
            list = hibernateQueryDao.singleColQuery(quS);
            if (list != null && !list.isEmpty()) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("isContractTerminatedOrNot : " + e + "starts");
        }
        logger.debug("isContractTerminatedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * add the data to the database
     * @param contractVals
     */
    public void countContVal(TblCmsVariContractVal contractVals) {
        logger.debug("countContVal : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "update TblCmsVariContractVal tv set tv.isCurrent='no' where tv.lotId=" + contractVals.getLotId();
            hibernateQueryDao.updateDeleteNewQuery(quS);
            valDao.addTblCmsVariContractVal(contractVals);
        } catch (Exception e) {
            logger.error("countContVal : " + e + "starts");
        }
        logger.debug("countContVal : " + logUserId + "ends");

    }

    /**
     * gives history data count for particular wpID
     * @param wpId
     * @return list of objects data
     */
    public List<Object[]> getHistoryDistinctCount(int wpId) {
        logger.debug("getHistoryDistinctCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select distinct(historyCount),createdDate from TblCmsWpDetailHistory where wpId=" + wpId + " ORDER BY  historyCount desc");
        } catch (Exception e) {
            logger.error("getHistoryDistinctCount : " + e + "starts");
        }
        logger.debug("getHistoryDistinctCount : " + logUserId + "ends");
        return list;
    }

    /**
     * gives wp details history count
     * @param wpId
     * @param amendmentFlag
     * @return list objects
     */
    public List<Object> getWPDetailHistMaxCount(int wpId, String amendmentFlag) {
        logger.debug("getWPDetailHistMaxCount : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select MAX(historyCount) from TblCmsWpDetailHistory where wpId =" + wpId + " and amendmentFlag ='" + amendmentFlag + "'");
        } catch (Exception e) {
            logger.error("getWPDetailHistMaxCount : " + e + "starts");
        }
        logger.debug("getWPDetailHistMaxCount : " + logUserId + "ends");
        return list;
    }

    /**
     * gives wp details history data
     * @param wpId
     * @param historyCount
     * @param Procnature
     * @return list of objects data
     */
    public List<Object[]> getWPDetailHistory(int wpId, int historyCount, String Procnature) {
        logger.debug("getWPDetailHistory : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcwd.wpSrNo,tcwd.wpDescription,tcwd.wpUom,tcwd.wpQty,tcwdh.wpEndDate,tcwd.groupId,tcwdh.wpStartDate,tcwdh.wpNoOfDays,tcwdh.wpRate,tcwdh.createdDate, tcwdh.currencyName, isNull(tcwdh.supplierVat, 0) + isNull(tcwdh.inlandOthers,0) as VATOthers ");
            sb.append("from TblCmsWpDetailHistory tcwdh,TblCmsWpDetail tcwd where ");
            sb.append("tcwd.tblCmsWpMaster.wpId=tcwdh.wpId and tcwd.tenderTableId=tcwdh.tenderTableId and ");
            sb.append("tcwd.wpRowId=tcwdh.wpRowId and tcwdh.wpId=" + wpId + " and tcwdh.historyCount=" + historyCount + " ");
            if ("goods".equalsIgnoreCase(Procnature)) {
                sb.append("and tcwdh.amendmentFlag in ('original','originalupdate') ");
            } else {
                sb.append("and tcwdh.amendmentFlag in ('originalbytenderer','originalupdate') order BY tcwd.groupId ");
            }
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getWPDetailHistory : " + e + "starts");
        }
        logger.debug("getWPDetailHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * it deletes the row data
     * @param str
     */
    public void deleteRowForVariation(String str) {
        logger.debug("deleteRowForVariation : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = false;
        try {
            String quS = "delete TblCmsTrackVariation vo where vo.variTrackId in(" + str + ")";
            hibernateQueryDao.updateDeleteNewQuery(quS);
        } catch (Exception e) {
            logger.error("deleteRowForVariation : " + e + "starts");
        }
        logger.debug("deleteRowForVariation : " + logUserId + "ends");
    }

    /**
     * gives variation order details data 
     * @param parameter
     * @return list of data
     */
    public List<TblCmsTrackVariation> viewVariationOderHistory(String parameter) {
        logger.debug("viewVariationOderHistory : " + logUserId + "starts");
        List<TblCmsTrackVariation> flag = null;
        try {
            flag = trackVariationDao.findTblCmsTrackVariation("variOrdId", Operation_enum.EQ, Integer.parseInt(parameter));
        } catch (Exception e) {
            logger.error("viewVariationOderHistory : " + e + "starts");
        }
        logger.debug("viewVariationOderHistory : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives wp details data
     * @param wpId
     * @return list of table data 
     */
    public List<TblCmsWpDetail> getDetailwp(String wpId) {
        logger.debug("getDetailwp : " + logUserId + "starts");
        List<TblCmsWpDetail> list = null;
        try {
            list = tblCmsWpDetailDao.findTblCmsWpDetail("tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(Integer.parseInt(wpId)), "groupId", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception e) {
            logger.error("getDetailwp : " + e + "starts");
        }
        logger.debug("getDetailwp : " + logUserId + "ends");
        return list;
    }

    /**
     * update the wp details table if invoice is rejected by pe
     * @param tenderId
     * @param lotId
     * @param invId
     * @param wpDetailId
     * @param itemInvQty
     * @param remarks
     */
    public void updateWpDetailIfInvReject(String tenderId, String lotId, int invId, int wpDetailId, BigDecimal itemInvQty, String remarks) {
        logger.debug("updateWpDetailIfInvReject : " + logUserId + "starts");
        List<Object> list = null;
        String quS = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcs.paymentTerms from TblTenderDetails ttd, TblNoaIssueDetails tnid, TblCmsDateConfig tcdc, ");
            sb.append("TblContractSign tcs,TblCompanyMaster tcm,TblLoginMaster tlm,TblTendererMaster ttm where tnid.tblTenderMaster.tenderId=tcdc.tblTenderMaster.tenderId ");
            sb.append("and tnid.pkgLotId=tcdc.appPkgLotId and tcdc.tblContractSign.contractSignId=tcs.contractSignId ");
            sb.append("and ttm.tblLoginMaster.userId=tnid.userId and ttm.tblCompanyMaster.companyId=tcm.companyId and ttm.tblLoginMaster.userId=tlm.userId ");
            sb.append("and ttd.tblTenderMaster.tenderId='" + tenderId + "' and tnid.pkgLotId='" + lotId + "'");
            list = hibernateQueryDao.getSingleColQuery(sb.toString());
            if (list.get(0).toString().equalsIgnoreCase("allitem100p")) {
                quS = "update TblCmsWpDetail tcw set itemInvStatus='pending',itemInvGenQty=(itemInvGenQty-" + itemInvQty + ") where wpDetailId=" + wpDetailId;
            } else if (list.get(0).toString().equalsIgnoreCase("itemwise100p")) {
                quS = "update TblCmsWpDetail tcw set itemInvStatus='pending',itemInvGenQty=(itemInvGenQty-" + itemInvQty + ") where wpDetailId=" + wpDetailId;
            } else {
                quS = "update TblCmsWpDetail tcw set itemInvGenQty=(itemInvGenQty-" + itemInvQty + ") where wpDetailId=" + wpDetailId;
            }
            String quSUpd = "update TblCmsInvoiceMaster tcw set invStatus='rejected',remarks='" + remarks + "' where invoiceId=" + invId;
            hibernateQueryDao.updateDeleteNewQuery(quS);
            hibernateQueryDao.updateDeleteNewQuery(quSUpd);
        } catch (Exception e) {
            logger.error("updateWpDetailIfInvReject : " + e + "starts");
        }
        logger.debug("updateWpDetailIfInvReject : " + logUserId + "ends");
    }
    
        public void updateInvoiceMasterIfInvRejectICT(int invId, String remarks) {
        logger.debug("updateWpDetailIfInvReject : " + logUserId + "starts");
        List<Object> list = null;
        String quS = "";
        try {
            
            String quSUpd = "update TblCmsInvoiceMaster tcw set invStatus='rejected',remarks='" + remarks + "' where invoiceId=" + invId;
    
            hibernateQueryDao.updateDeleteNewQuery(quSUpd);
        } catch (Exception e) {
            logger.error("updateWpDetailIfInvReject : " + e + "starts");
        }
        logger.debug("updateWpDetailIfInvReject : " + logUserId + "ends");
    }



    /**
     * gives lot id and tender id
     * @param variOrdId
     * @return list of objects
     */
    public List<Object[]> getlotIdandTenderID(int variOrdId) {
        logger.debug("getlotIdandTenderID : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            //list  = hibernateQueryDao.singleColQuery("select twm.wpLotId from TblCmsWpMaster twm where twm.wpId in ( select tcvo.wpId from TblCmsVariationOrder tcvo where variOrdId="+variOrdId+ ")");
            StringBuilder sb = new StringBuilder();
            sb.append("select tls.tblTenderMaster.tenderId,tls.appPkgLotId ");
            sb.append("from TblTenderLotSecurity tls,TblCmsWpMaster tcw,TblCmsVariationOrder tcvo ");
            sb.append("where tls.appPkgLotId=tcw.wpLotId and tcvo.wpId=tcw.wpId and tcvo.variOrdId=" + variOrdId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getlotIdandTenderID : " + e + "starts");
        }
        logger.debug("getlotIdandTenderID : " + logUserId + "ends");
        return list;

    }

    /**
     * add repeat order data to the database
     * @param romaster
     * @return boolean, return true - if data submitted successfully otherwise false
     */
    public boolean addRepeatOrder(TblCmsRomaster romaster) {
        logger.debug("addRepeatOrder : " + logUserId + "starts");
        boolean flag = false;
        try {
            cmsRomasterDao.addTblCmsRomaster(romaster);
            flag = true;
        } catch (Exception e) {
            logger.error("addRepeatOrder : " + e + "starts");
        }
        logger.debug("addRepeatOrder : " + logUserId + "ends");
        return flag;
    }

    /**
     * add repeat order data to the database
     * @param list
     * @return boolean, return true - if data submitted successfully otherwise false
     */
    public boolean placedOrderQty(List<TblCmsRoitems> list) {
        logger.debug("placedOrderQty : " + logUserId + "starts");
        boolean flag = false;
        try {
            cmsRoitemsDao.updateOrSaveEstCost(list);
            flag = true;
        } catch (Exception e) {
            logger.error("placedOrderQty : " + e + "starts");
        }
        logger.debug("placedOrderQty : " + logUserId + "ends");
        return flag;
    }

    /**
     * update the data to the database
     * @param wpId
     * @param romId
     * @return  list of objects data 
     */
    public List<Object[]> editRepeatOrder(int wpId, int romId) {

        logger.debug("editRepeatOrder : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder builder = new StringBuilder();
        builder.append("select tt.wpDetailId,tt.wpSrNo,tt.wpDescription,");
        builder.append("tt.wpUom,tt.wpQty,tt.wpRate,");
        builder.append("tt.wpRowId,tt.tenderTableId,tt.wpNoOfDays ");
        builder.append("from TblCmsWpDetail tt where tt.wpDetailId ");
        builder.append("not in (select tr.detailId from TblCmsRoitems tr where tr.wpId=" + wpId + " and tr.romId=" + romId + ") and tt.tblCmsWpMaster.wpId=" + wpId + "");
        try {
            list = hibernateQueryDao.createNewQuery(builder.toString());
        } catch (Exception e) {
            logger.error("editRepeatOrder : " + e + "starts");
        }
        logger.debug("editRepeatOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * gives repeat order data
     * @param romId
     * @param wpId
     * @return list of objects data
     */
    public List<Object[]> getRepeatOrderItems(int romId, int wpId) {

        logger.debug("getRepeatOrderItems : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder builder = new StringBuilder();
        builder.append("select tt.detailId,tt.srno,tt.description,");
        builder.append("tt.uom,tt.qty,tt.rate,");
        builder.append("tt.rowid,tt.tendertableid,tt.noofdays ,tt.romId ");
        builder.append("from TblCmsRoitems tt,TblCmsRomaster tm where tm.romId=tt.romId and tt.wpId=" + wpId + " and tt.romId=" + romId + " ORDER BY tt.tendertableid,tt.rowid ");
        try {
            list = hibernateQueryDao.createNewQuery(builder.toString());
        } catch (Exception e) {
            logger.error("getRepeatOrderItems : " + e + "starts");
        }
        logger.debug("getRepeatOrderItems : " + logUserId + "ends");
        return list;
    }

    /**
     * gives max rom id
     * @param wpId
     * @return integer, return 0 - if data is not available otherwise return else count 
     */
    public int getMaxRomId(String wpId) {
        logger.debug("getMaxRomId : " + logUserId + "starts");
        int romId = 0;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tr.romId from TblCmsRoitems tr where tr.wpId=" + wpId + "");
            if (!list.isEmpty()) {
                romId = Integer.parseInt(list.get(list.size() - 1).toString());
            }
        } catch (Exception e) {
            logger.error("getMaxRomId : " + e + "starts");
        }
        logger.debug("getMaxRomId : " + logUserId + "ends");
        return romId;
    }

    /**
     * it deletes the ro table data
     * @param romId
     * @param wpId
     */
    public void deleteRO(int romId, int wpId) {
        logger.debug("deleteRO : " + logUserId + "starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblCmsRoitems tr where tr.romId=" + romId + " and tr.wpId=" + wpId + "");
        } catch (Exception e) {
            logger.error("deleteRO : " + e + "starts");
        }
        logger.debug("deleteRO : " + logUserId + "ends");
    }

    /**
     * gives second wp ro data
     * @param lotId
     * @return list objects 
     */
    public List<Object> checkForSecondWPRO(int lotId) {
        logger.debug("checkForSecondWPRO : " + logUserId + "starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tr.romId from TblCmsRomaster tr where tr.lotId=" + lotId + " and tr.isCurrent='yes' ");

        } catch (Exception e) {
            logger.error("checkForSecondWPRO : " + e + "starts");
        }
        logger.debug("checkForSecondWPRO : " + logUserId + "ends");
        return list;
    }

    /**
     * gives contract value data
     * @param romId
     * @return list objects data
     */
    public List<Object> countNewContractValue(int romId) {
        logger.debug("countNewContractValue : " + logUserId + "starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select SUM(qty*rate) from  TblCmsRoitems  where romId=" + romId);
        } catch (Exception e) {
            System.out.println(e);
            logger.error("countNewContractValue : " + e + "starts");
        }
        logger.debug("countNewContractValue : " + logUserId + "ends");
        return list;
    }
    /**
     * gives contract value data
     * @param romId
     * @return list objects data
     */
    public List<Object> countNewContractValueForWorks(int lotId) {
        logger.debug("countNewContractValueForWorks : " + logUserId + "starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select SUM(wd.wpQty*wd.wpRate) from  TblCmsWpDetail wd where wd.tblCmsWpMaster.wpId in (select wm.wpId from TblCmsWpMaster wm where wm.wpLotId ="+lotId+")");
        } catch (Exception e) {
            System.out.println(e);
            logger.error("countNewContractValueForWorks : " + e + "starts");
        }
        logger.debug("countNewContractValueForWorks : " + logUserId + "ends");
        return list;
    }
    public List<TblCmsVariContractVal> displayNewContractValOrNot(int lotId) {
        logger.debug("displayNewContractValOrNot : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsVariContractVal> list = null;
        try {
            list = valDao.findTblCmsVariContractVal("lotId",Operation_enum.EQ,lotId,"isCurrent",Operation_enum.EQ,"yes");
           
        } catch (Exception e) {
            System.out.println(e);
            logger.error("displayNewContractValOrNot : " + e + "starts");
        }
        logger.debug("displayNewContractValOrNot : " + logUserId + "ends");
        return list;
    }

    /**
     * checks ro placed or not
     * @param wpId
     * @return boolean, return true - if placed otherwise false
     */
    public boolean isROPlacedOrNot(int wpId) {
        logger.debug("isROPlacedOrNot : " + logUserId + "starts");
        boolean flag = false;
        try {

            String query = "select tor.romId from TblCmsRoitems tor,TblCmsRomaster tom "
                    + "where tor.wpId=" + wpId + " and tom.romId=tor.romId and tom.isCurrent='yes'";

            //remove rptOrderStatus = finalise
            List<Object> list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty()) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("isROPlacedOrNot : " + e + "starts");
        }
        logger.debug("isROPlacedOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives contract value
     * @param lotId
     * @param cntValue
     * @return string 
     */
    public String contractValueCheck(int lotId, String cntValue) {
        logger.debug("contractValueCheck : " + logUserId + "starts");
        String flag = "done";
        try {
            int roId = 0;
            BigDecimal bd = new BigDecimal(cntValue).multiply(new BigDecimal(0.50));
            List<Object> romId = checkForSecondWPRO(lotId);
            if (romId != null && !romId.isEmpty()) {
                roId = Integer.parseInt(romId.get(0).toString());
            }
            List<Object> Cvalues = null;
            Cvalues = countNewContractValue(roId);
            double F_cv = Double.parseDouble(bd.toString());
            if (!Cvalues.isEmpty()) {
                if (F_cv < Double.parseDouble(Cvalues.get(0).toString())) {
                    flag = "50percentcheck";
                }
            } else {
                flag = "0check";
            }



        } catch (Exception e) {
            logger.error("contractValueCheck : " + e + "starts");
        }
        logger.debug("contractValueCheck : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks ro finalized or not
     * @param lotId
     * @param Status
     * @return boolean, return true - if finalized otherwise false
     */
    public boolean isROFinalizeOrNot(int lotId, String Status) {
        logger.debug("isROFinalizeOrNot : " + logUserId + "starts");
        boolean flag = false;
        try {
            String query = "select tom.rptOrderStatus from TblCmsRomaster tom "
                    + "where tom.lotId=" + lotId + "  and tom.rptOrderStatus = 'finalise' and tom.romId in (select MAX(romId) from TblCmsRomaster where lotId = " + lotId + " and isCurrent='yes')";
            //and tom.isCurrent='yes'
            List<Object> list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty()) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("isROFinalizeOrNot : " + e + "starts");
        }
        logger.debug("isROFinalizeOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks ro finalized or not in case of noa rejected
     * @param lotId
     * @param romId
     * @return boolean, return true - if finalized otherwise false
     */
    public boolean isROFinalizeOrNotInCaseOfNOAReject(int lotId, int romId) {
        logger.debug("isROFinalizeOrNotInCaseOfNOAReject : " + logUserId + "starts");
        boolean flag = false;
        try {
            String query = "select tom.rptOrderStatus from TblCmsRomaster tom "
                    + "where tom.lotId=" + lotId + "  and tom.rptOrderStatus in ('finalise','completed') and "
                    + "tom.romId=" + romId + "";
            //and tom.isCurrent='yes'
            List<Object> list = hibernateQueryDao.getSingleColQuery(query);
            if (!list.isEmpty()) {
                flag = true;
            }

        } catch (Exception e) {
            logger.error("isROFinalizeOrNotInCaseOfNOAReject : " + e + "starts");
        }
        logger.debug("isROFinalizeOrNotInCaseOfNOAReject : " + logUserId + "ends");
        return flag;
    }

    /**
     * add the data for finalize ro
     * @param lotId
     */
    public void finaliseRO(int lotId) {
        logger.debug("finaliseRO : " + logUserId + "starts");
        try {
            String query = "update TblCmsRomaster tom "
                    + "set tom.rptOrderStatus='finalise' where tom.lotId=" + lotId + " and tom.isCurrent='yes'";
            hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception e) {
            logger.error("finaliseRO : " + e + "starts");
        }
        logger.debug("finaliseRO : " + logUserId + "ends");
    }

    /**
     * gives the data for all ro items  for wf
     * @param romId
     * @return list table objects 
     */
    public List<TblCmsRoitems> allROItemsForWf(int romId) {
        logger.debug("TblCmsRoitems : " + logUserId + "starts");
        List<TblCmsRoitems> list = null;
        try {
            list = cmsRoitemsDao.findTblCmsRoitems("romId", Operation_enum.EQ, romId);
        } catch (Exception e) {
            logger.error("TblCmsRoitems : " + e + "starts");
        }
        logger.debug("TblCmsRoitems : " + logUserId + "ends");
        return list;
    }

    /**
     * adds delivery documents details data
     * @param tblCmsWpDetailDocs
     * @return boolean return true -  if added successfully otherwise false
     */
    public boolean addDeliveryDocsDetails(TblCmsWpDetailDocs tblCmsWpDetailDocs) {
        logger.debug("addDeliveryDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsWpDetailDocsDao.addTblCmsWpDetailDocs(tblCmsWpDetailDocs);
            flag = true;
        } catch (Exception e) {
            logger.error("addDeliveryDocsDetails : " + e);
        }
        logger.debug("addDeliveryDocsDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * it deletes the  delivery documents details data
     * @param wpDetailDocId
     * @return boolean return true -  if deleted successfully otherwise false
     */
    public boolean deleteDeliveryDocsDetails(int wpDetailDocId) {
        logger.debug("deleteDeliveryDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        int i = 0;
        try {
            i = hibernateQueryDao.updateDeleteNewQuery("delete from TblCmsWpDetailDocs where wpDetailDocId=" + wpDetailDocId + "");
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("deleteDeliveryDocsDetails : " + e);
        }
        logger.debug("deleteDeliveryDocsDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * gives delivery documents details data
     * @param keyId
     * @return list table objects 
     */
    public List<TblCmsWpDetailDocs> getDeliveryDocsDetails(int keyId) {
        logger.debug("getDeliveryDocsDetails : " + logUserId + "starts");
        List<TblCmsWpDetailDocs> list = null;
        try {
            list = tblCmsWpDetailDocsDao.findTblCmsWpDetailDocs("keyId", Operation_enum.EQ, keyId);
        } catch (Exception e) {
            logger.error("getDeliveryDocsDetails : " + e);
        }
        logger.debug("getDeliveryDocsDetails : " + logUserId + "ends");
        return list;
    }

    /**
     * gives invoice master table details data
     * @param InvoiceId
     * @return list table objects
     */
    public List<TblCmsInvoiceMaster> getTblCmsInvoiceMaster(int InvoiceId) {
        logger.debug("getTblCmsInvoiceMaster : " + logUserId + "starts");
        List<TblCmsInvoiceMaster> list = null;
        try {
            list = invoiceMasterDao.findTblCmsInvoiceMaster("invoiceId", Operation_enum.EQ, InvoiceId);
        } catch (Exception e) {
            logger.error("getTblCmsInvoiceMaster : " + e);
        }
        logger.debug("getTblCmsInvoiceMaster : " + logUserId + "ends");
        return list;
    }

    /**
     * gives invoice master data with invoice amount
     * @param wpId
     * @return list of array objects data 
     */
    public List<Object[]> getInvoiceMasterDatawithInvAmt(String wpId) {
        logger.debug("getInvoiceMasterDatawithInvAmt : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String quS = "select ind.invoiceId,ind.invStatus,ind.invoiceNo,ind.totalInvAmt from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "' and ind.isAdvInv='Yes'";
            list = hibernateQueryDao.createNewQuery(quS.toString());
        } catch (Exception e) {
            logger.error("getInvoiceMasterDatawithInvAmt : " + e + "starts");
        }
        logger.debug("getInvoiceMasterDatawithInvAmt : " + logUserId + "ends");
        return list;
    }

        public List<Object[]> getInvoiceMasterDatawithInvAmtICT(String wpId) {
        logger.debug("getInvoiceMasterDatawithInvAmt : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String quS = "select distinct ind.invoiceNo,ind.invStatus from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "' and ind.isAdvInv='Yes'";
            list = hibernateQueryDao.createNewQuery(quS.toString());
        } catch (Exception e) {
            logger.error("getInvoiceMasterDatawithInvAmt : " + e + "starts");
        }
        logger.debug("getInvoiceMasterDatawithInvAmt : " + logUserId + "ends");
        return list;
    }

    /**
     * update the invoice master table data
     * @param InvoiceId
     * @param remarks
     * @return boolean, return true - if updated successfully otherwise false
     */
    public boolean updateInvmaster(int InvoiceId, String remarks) {
        logger.debug("updateInvmaster : " + logUserId + "starts");
        boolean flag = false;
        int i = 0;
        try {
            String quSUpd = "update TblCmsInvoiceMaster tcw set invStatus='rejected',remarks='" + remarks + "' where invoiceId=" + InvoiceId;
            i = hibernateQueryDao.updateDeleteNewQuery(quSUpd);
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("updateInvmaster : " + e);
        }
        logger.debug("updateInvmaster : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks the accepted flag
     * @param wpId
     * @return boolean return true - if data is not exist otherwise false
     */
    public boolean getAcceptedFlag(int wpId) {
        logger.debug("getAcceptedFlag : " + logUserId + "starts");
        boolean flag = true;
        List<Object> list = null;
        try {
            String quSUpd = "select ind.invoiceId from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "' and ind.isAdvInv='Yes' and ind.invStatus!='rejected'";
            list = hibernateQueryDao.singleColQuery(quSUpd);
            if (list.get(0) != null) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("getAcceptedFlag : " + e);
        }
        logger.debug("getAcceptedFlag : " + logUserId + "ends");
        return flag;
    }

    /**
     * checks the accepted flag for boq
     * @param wpId
     * @return boolean return true - if data is exist otherwise false
     */
    public boolean getAcceptedFlagforBoq(int wpId) {
        logger.debug("getAcceptedFlagforBoq : " + logUserId + "starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            String quSUpd = "select ind.invoiceId from TblCmsInvoiceMaster ind where ind.tblCmsWpMaster.wpId='" + wpId + "' and ind.isAdvInv='Yes' and ind.invStatus!='rejected' and ind.invStatus!='createdbyten'";
            list = hibernateQueryDao.singleColQuery(quSUpd);
            if (list.get(0) != null) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("getAcceptedFlagforBoq : " + e);
        }
        logger.debug("getAcceptedFlagforBoq : " + logUserId + "ends");
        return flag;
    }

    /**
     * adds the data to romap table
     * @param romap
     */
    public void addToRoMap(TblCmsRomap romap) {
        logger.debug("addToRoMap : " + logUserId + "starts");
        try {
            tblCmsRomapDao.addTblCmsRomap(romap);
        } catch (Exception e) {
            logger.error("addToRoMap : " + e);
        }
        logger.debug("addToRoMap : " + logUserId + "ends");
    }

    /**
     * gives invoice details by LotId
     * @param lotId
     * @return long return 0 - if data is not exist otherwise else count
     */
    public long getInvoiceCountByLotId(int lotId) {
        logger.debug("getInvoiceCountByLotId : " + logUserId + "starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblCmsWpMaster tcwm,TblCmsInvoiceMaster tcim", "tcwm.wpId = tcim.tblCmsWpMaster.wpId and tcwm.isRepeatOrder='No' and tcwm.wpLotId=" + lotId + "");

        } catch (Exception e) {
            logger.error("getInvoiceCountByLotId : " + e);
        }
        logger.debug("getInvoiceCountByLotId : " + logUserId + "ends");
        return lng;
    }

    public  List<Object> getInvoiceCountByLotIdForICT(int lotId) {
        logger.debug("getInvoiceCountByLotId : " + logUserId + "starts");
       List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select distinct tcim.invoiceNo from TblCmsWpMaster tcwm,TblCmsInvoiceMaster tcim where tcwm.wpId = tcim.tblCmsWpMaster.wpId and tcwm.isRepeatOrder='No' and tcwm.wpLotId=" + lotId + "");

        } catch (Exception e) {
            logger.error("getInvoiceCountByLotId : " + e);
        }
        logger.debug("getInvoiceCountByLotId : " + logUserId + "ends");
        return list;
    }

    /**
     * gives contract Id 
     * @param tenderId
     * @return integer return 0 - if data is not exist otherwise else count
     */
    public int getContractId(int tenderId) {
        logger.debug("getContractId : " + logUserId + "starts");
        int lng = 0;
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select ts.contractSignId from TblNoaIssueDetails ns,TblContractSign ts,TblNoaAcceptance na ");
        br.append("where ns.tblTenderMaster.tenderId=" + tenderId + " and ns.noaIssueId=ts.noaId and ns.isRepeatOrder='no' ");
        br.append("and na.tblNoaIssueDetails.noaIssueId=ns.noaIssueId and na.acceptRejStatus='approved'");
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());
            if (list != null && !list.isEmpty()) {
                lng = Integer.parseInt(list.get(0).toString());
            }

        } catch (Exception e) {
            logger.error("getContractId : " + e);
        }
        logger.debug("getContractId : " + logUserId + "ends");
        return lng;
    }

    /**
     * gives contract Id
     * @param noaId
     * @return list of objects data
     */
    public List<Object[]> getContractNo(int noaId) {
        logger.debug("getContractNo : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tn.contractNo,tn.isRepeatOrder ");
        br.append("from TblContractSign tc,TblNoaIssueDetails tn ");
        br.append("where tc.noaId = " + noaId + " and tn.noaIssueId=tc.noaId");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getContractNo : " + e);
        }
        logger.debug("getContractNo : " + logUserId + "ends");
        return list;
    }

   //Code Start by Proshanto Kumar Saha,Dohatec
    /**
     * gives contract Id
     * @param noaId
     * @return list of objects data
     */
    public List<Object[]> getContractNoWithoutCSign(int noaId) {
        logger.debug("getContractNoWithoutCSign : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tn.contractNo,tn.isRepeatOrder ");
        br.append("from TblNoaIssueDetails tn ");
        br.append("where tn.noaIssueId= " + noaId + " ");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getContractNoWithoutCSign : " + e);
        }
        logger.debug("getContractNoWithoutCSign : " + logUserId + "ends");
        return list;
    }
    //Code End by Proshanto Kumar Saha,Dohatec

    /**
     * gives tender Id data
     * @param variOrdId
     * @return list objects 
     */
    public List<Object[]> getTenderID(int variOrdId) {
        logger.debug("getTenderID : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcvo.tenderId,tcvo.wpId ");
            sb.append("from TblCmsVariationOrder tcvo ");
            sb.append("where tcvo.variOrdId=" + variOrdId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getTenderID : " + e + "starts");
        }
        logger.debug("getTenderID : " + logUserId + "ends");
        return list;

    }

    /**
     * gives L1 bidder userId
     * @param tenderId
     * @return string 
     */
    public String getL1bidderuserID(String tenderId) {
        logger.debug("getL1bidderuserID : " + logUserId + "starts");
        List<Object> list = null;
        String paymentTerms = "";
        try {
            StringBuilder query = new StringBuilder();
            query.append(" select tn.userId from TblContractSign tc,");
            query.append(" TblNoaAcceptance ta, TblNoaIssueDetails tn");
            query.append(" where tn.tblTenderMaster.tenderId = " + tenderId + " and tn.noaIssueId=tc.noaId");
            query.append(" and ta.tblNoaIssueDetails.noaIssueId=tn.noaIssueId and tn.isRepeatOrder='no' and ta.acceptRejStatus='approved'");
            list = hibernateQueryDao.getSingleColQuery(query.toString());
            if (!list.isEmpty()) {
                paymentTerms = list.get(0).toString();
            }

        } catch (Exception e) {
            logger.error("getL1bidderuserID : " + e + "starts");
        }
        logger.debug("getL1bidderuserID : " + logUserId + "ends");
        return paymentTerms;
    }

    public List<Object[]> getInvAmtAndMilestoneName(int invoiceId) {
        logger.debug("getInvAmtAndMilestoneName : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcim.totalInvAmt,tcsp.milestone,tcim.invoiceNo ");
            sb.append("from TblCmsInvoiceMaster tcim,TblCmsSrvPaymentSch tcsp ");
            sb.append("where tcim.tillPrid=tcsp.srvPsid and tcim.invoiceId=" + invoiceId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getInvAmtAndMilestoneName : " + e + "starts");
        }
        logger.debug("getInvAmtAndMilestoneName : " + logUserId + "ends");
        return list;
    }

        public List<Object[]> getInvAmtAndMilestoneNameForICT(int invoiceId) {
        logger.debug("getInvAmtAndMilestoneName : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select tcim.totalInvAmt,tcsp.milestone,tcim.invoiceNo ");
            sb.append("from TblCmsInvoiceMaster tcim,TblCmsSrvPaymentSch tcsp ");
            sb.append("where tcim.tillPrid=tcsp.srvPsid and tcim.invoiceId=" + invoiceId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getInvAmtAndMilestoneName : " + e + "starts");
        }
        logger.debug("getInvAmtAndMilestoneName : " + logUserId + "ends");
        return list;
    }
    
    public void updateLumpSumPr(int SrvPsid,int tenderId) {
        logger.debug("updateLumpSumPr : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvPaymentSch tcsps set tcsps.status='pending',tcsps.completedDt=current_timestamp() where tcsps.srvPsid=" + SrvPsid+" and tcsps.tenderId = "+tenderId+"";
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateLumpSumPr : " + e + "starts");
        }
        logger.debug("updateLumpSumPr : " + logUserId + "ends");
    }
    public List<Object> getInvoiceNo(String InvoiceId) {
        logger.debug("getInvoiceNo : " + logUserId + "starts");
        List<Object> flag = null;
        try {
            String quS = "select ind.invoiceNo from TblCmsInvoiceMaster ind where ind.invoiceId='" + InvoiceId + "'";
            flag = hibernateQueryDao.singleColQuery(quS);

        } catch (Exception e) {
            logger.error("getInvoiceNo : " + e + "starts");
        }
        logger.debug("getInvoiceNo : " + logUserId + "ends");
        return flag;
    }

     public List<Object> getTenderFormIdForICT(int tenderId,int userId,int lotId) {
        logger.debug("getTenderFormIdForICT : " + logUserId + "starts");
        List<Object> cL = null;
        long flag = 0;
        StringBuilder query = new StringBuilder();
        query.append("select tbf.tenderFormId from TblTenderBidForm tbf,TblTenderForms tf  ");
        query.append("where tf.tenderFormId=tbf.tenderFormId and ");
        query.append("tbf.userId = "+ userId +" and tbf.tenderId = " + tenderId+ " and tf.pkgLotId = " + lotId + "");
        try {
               cL = hibernateQueryDao.singleColQuery(query.toString());
            
        } catch (Exception e) {
            logger.error("getTenderFormIdForICT : " + e + "starts");
        }
        logger.debug("getTenderFormIdForICT : " + logUserId + "ends");
        return cL;
    }
     
    

    public List<Object[]> getWPDetailIDByCurrency(int wpId, String currency,int prId) {
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "starts");
        List<Object[]> cL = null;
        StringBuilder query = new StringBuilder();
        query.append("select twp.wpDetailId,wpd.qtyDlvrdCurrPr from TblCmsWpDetail twp, TblCmsPrDetail wpd ");
        query.append("where twp.tblCmsWpMaster.wpId = "+ wpId +" and twp.currencyName = '" + currency+ "' and twp.tenderTableId = wpd.tenderTableId and twp.wpRowId = wpd.rowId and wpd.tblCmsPrMaster.progressRepId = " + prId + "");
        try {
               cL = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            logger.error("getWPDetailIDByCurrency : " + e + "starts");
        }
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "ends");
        return cL;
    }

    public List<Object> getWPDetailIDByCurrencyForAdv(int wpId, String currency) {
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "starts");
        List<Object> cL = null;
        StringBuilder query = new StringBuilder();
        query.append("select twp.wpDetailId from TblCmsWpDetail twp ");
        query.append("where twp.tblCmsWpMaster.wpId = "+ wpId +" and twp.currencyName = '" + currency+ "'");
        try {
               cL = hibernateQueryDao.singleColQuery(query.toString());

        } catch (Exception e) {
            logger.error("getWPDetailIDByCurrency : " + e + "starts");
        }
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "ends");
        return cL;
    }

    public List<Object[]> getWPDetailID(int wpId,int prId) {
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "starts");
        List<Object[]> cL = null;
        StringBuilder query = new StringBuilder();
        query.append("select twp.wpDetailId,wpd.qtyAcceptTillThisPr from TblCmsWpDetail twp, TblCmsPrDetail wpd ");
        query.append("where twp.tblCmsWpMaster.wpId = "+ wpId +" and twp.tenderTableId = wpd.tenderTableId and twp.wpRowId = wpd.rowId and wpd.tblCmsPrMaster.progressRepId = " + prId + "");
        try {
               cL = hibernateQueryDao.createNewQuery(query.toString());

        } catch (Exception e) {
            logger.error("getWPDetailIDByCurrency : " + e + "starts");
        }
        logger.debug("getWPDetailIDByCurrency : " + logUserId + "ends");
        return cL;
    }

    public List<Object> getInvoiceIdForICT(String invoiceNo,int wpID) {
        logger.debug("getInvoiceIdForICT : " + logUserId + "starts");
        List<Object> flag = null;
        try {
            String quS = "select im.invoiceId from TblCmsInvoiceMaster im where im.invoiceNo='" + invoiceNo + "' and im.tblCmsWpMaster.wpId = " + wpID + "";
            flag = hibernateQueryDao.singleColQuery(quS);

        } catch (Exception e) {
            logger.error("getInvoiceIdForICT : " + e + "starts");
        }
        logger.debug("getInvoiceIdForICT : " + logUserId + "ends");
        return flag;
    }

        public List<TblCmsInvoiceMaster> getTblCmsInvoiceMasterForICT(String invoiceNo,int wpID) {
        logger.debug("getInvoiceIdForICT : " + logUserId + "starts");
        List<TblCmsInvoiceMaster> list = null;
        try {
            //String quS = "select im.invoiceId from TblCmsInvoiceMaster im where im.invoiceNo='" + invoiceNo + "' and im.tblCmsWpMaster.wpId = " + wpID + "";
            list = invoiceMasterDao.findTblCmsInvoiceMaster("invoiceNo", Operation_enum.EQ, invoiceNo,"tblCmsWpMaster", Operation_enum.EQ, new TblCmsWpMaster(wpID));
          //  flag = hibernateQueryDao.singleColQuery(quS);
        } catch (Exception e) {
            logger.error("getInvoiceIdForICT : " + e + "starts");
        }
        logger.debug("getInvoiceIdForICT : " + logUserId + "ends");
        return list;
    }

     public List<Object[]> returndata(String funName, String funName1, String funName2) {
        logger.debug("returndata : " + logUserId + " : Starts");
        List<SPTenderCommonData> returndata = null;
        List<String> list = new ArrayList<String>();
        List<Object[]> list1= new ArrayList<Object[]>();
        
        try {
            returndata = tendercommon.executeProcedure(funName, funName1, funName2);
            //System.out.print(returndata.size());
            for(int j=0;j<returndata.size();j++)
            {
              Object[] objArr = new Object[11];
                objArr[0] =(returndata.get(j).getFieldName1());
                objArr[1] =(returndata.get(j).getFieldName2());
                objArr[2] =(returndata.get(j).getFieldName3());
                objArr[3] =(returndata.get(j).getFieldName4());
                objArr[4] =(returndata.get(j).getFieldName5());
                objArr[5] =(returndata.get(j).getFieldName6());
                objArr[6] =(returndata.get(j).getFieldName7());
                objArr[7] =(returndata.get(j).getFieldName8());
                objArr[8] =(returndata.get(j).getFieldName9());
                objArr[9] =(returndata.get(j).getFieldName10());
                objArr[10] =(returndata.get(j).getFieldName11());
                //list1. = "sdf";
              
            list1.add(objArr);
             //   System.out.print(list1.get(j)[8]);
            }
            //System.out.print("list1"+list1.size());



        } catch (Exception e) {
            logger.error("returndata : " + e);
        }
        logger.debug("returndata : " + logUserId + " : Ends");
        return list1;
    }

    /**
     * @return the tendercommon
     */
    public SPTenderCommon getTendercommon() {
        return tendercommon;
    }

    /**
     * @param tendercommon the tendercommon to set
     */
    public void setTendercommon(SPTenderCommon tendercommon) {
        this.tendercommon = tendercommon;
    }
    /**
     * this method get the tenderFormId by passing tenderFormId
     * @param tenderFormId
     * @return boolean value if discount form then true otherwise false
     */
    public boolean getDiscountFormId(String tenderFormId) {
        logger.debug("getDiscountFormId : " + logUserId + "starts");
        List<Object> cL = null;
        boolean isPresent = false;
        String quS = "select tenderFormId from TblTenderForms tf where tf.formType = 'BOQsalvage' and tf.tenderFormId=" + tenderFormId;
        try {
            cL = hibernateQueryDao.getSingleColQuery(quS);
            if (cL != null && !cL.isEmpty()) {
                isPresent = true;
            }
        } catch (Exception e) {
            logger.error("getDiscountFormId : " + e + "starts");
        }
        logger.debug("getDiscountFormId : " + logUserId + "ends");
        return isPresent;
    }
}
