/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPEvalProcessInclude;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPData;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CommonSearchDataMoreImpl implements CommonSearchDataMoreService{

    static final Logger logger = Logger.getLogger(CommonSearchDataMoreImpl.class);
    
    SpgetCommonSearchDataMore sPgetCommonSearchDataMore;
    SpGeteGPData spGeteGPData;
    SpGeteGPDataMore spGeteGPDataMore;
    private String logUserId="0";
    private SPEvalProcessInclude spEvalProcessInclude;

    public SpGeteGPData getSpGeteGPData() {
        return spGeteGPData;
    }

    public void setSpGeteGPData(SpGeteGPData spGeteGPData) {
        this.spGeteGPData = spGeteGPData;
    }

    public SpGeteGPDataMore getSpGeteGPDataMore() {
        return spGeteGPDataMore;
    }

    public void setSpGeteGPDataMore(SpGeteGPDataMore spGeteGPDataMore) {
        this.spGeteGPDataMore = spGeteGPDataMore;
    }

    public SpgetCommonSearchDataMore getsPgetCommonSearchDataMore() {
        return sPgetCommonSearchDataMore;
    }

    public void setsPgetCommonSearchDataMore(SpgetCommonSearchDataMore sPgetCommonSearchDataMore) {
        this.sPgetCommonSearchDataMore = sPgetCommonSearchDataMore;
    }

    

    @Override
    public List<SPCommonSearchDataMore> getCommonSearchData(String ... fieldValues) {
        logger.debug("getCommonSearchData : "+logUserId+" Starts");
        List<SPCommonSearchDataMore> list = null;
        String[] fieldValue = new String[20];
        if(fieldValues!=null && fieldValues.length<=20){
            System.arraycopy(fieldValues, 0, fieldValue, 0, fieldValues.length);
        }
        try {
            list = sPgetCommonSearchDataMore.executeProcedure(fieldValue[0],fieldValue[1], fieldValue[2], fieldValue[3], fieldValue[4], fieldValue[5], fieldValue[6], fieldValue[7], fieldValue[8], fieldValue[9], fieldValue[10], fieldValue[11], fieldValue[12], fieldValue[13], fieldValue[14], fieldValue[15], fieldValue[16], fieldValue[17], fieldValue[18], fieldValue[19]);
          
        } catch (Exception e) { 
            logger.error("getCommonSearchData : "+logUserId+" "+e);
    }
        fieldValue = null;
        logger.debug("getCommonSearchData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public List<SPCommonSearchDataMore> geteGPData(String ... fieldValues) {
        logger.debug("geteGPData : "+logUserId+" Starts");
        List<SPCommonSearchDataMore> list = null;
        String[] fieldValue = new String[20];
        if(fieldValues!=null && fieldValues.length<=20){
            System.arraycopy(fieldValues, 0, fieldValue, 0, fieldValues.length);
        }
        try {
            list = spGeteGPData.executeProcedure(fieldValue[0],fieldValue[1], fieldValue[2], fieldValue[3], fieldValue[4], fieldValue[5], fieldValue[6], fieldValue[7], fieldValue[8], fieldValue[9], fieldValue[10], fieldValue[11], fieldValue[12], fieldValue[13], fieldValue[14], fieldValue[15], fieldValue[16], fieldValue[17], fieldValue[18], fieldValue[19]);
        } catch (Exception e) {
            logger.error("geteGPData : "+logUserId+" "+e);
        }
        fieldValue = null;
        logger.debug("geteGPData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<SPCommonSearchDataMore> geteGPDataMore(String ... fieldValues) {
        logger.debug("geteGPDataMore : "+logUserId+" Starts");
        List<SPCommonSearchDataMore> list = null;
        String[] fieldValue = new String[20];
        if(fieldValues!=null && fieldValues.length<=20){
            System.arraycopy(fieldValues, 0, fieldValue, 0, fieldValues.length);
        }
        try {
            list = spGeteGPDataMore.executeProcedure(fieldValue[0],fieldValue[1], fieldValue[2], fieldValue[3], fieldValue[4], fieldValue[5], fieldValue[6], fieldValue[7], fieldValue[8], fieldValue[9], fieldValue[10], fieldValue[11], fieldValue[12], fieldValue[13], fieldValue[14], fieldValue[15], fieldValue[16], fieldValue[17], fieldValue[18], fieldValue[19]);
        } catch (Exception e) {
            logger.error("geteGPDataMore : "+logUserId+" "+e);
    }
        fieldValue = null;
        logger.debug("geteGPDataMore : "+logUserId+" Ends");
        return list;
    }

    /**
     * @return the spEvalProcessInclude
     */
    public SPEvalProcessInclude getSpEvalProcessInclude() {
        return spEvalProcessInclude;
    }

    /**
     * @param spEvalProcessInclude the spEvalProcessInclude to set
     */
    public void setSpEvalProcessInclude(SPEvalProcessInclude spEvalProcessInclude) {
        this.spEvalProcessInclude = spEvalProcessInclude;
    }

    @Override
    public List<SPCommonSearchDataMore> getEvalProcessInclude(String ... fieldValues)
    {
        logger.debug("getEvalProcessInclude : "+logUserId+" Starts");
        List<SPCommonSearchDataMore> list = null;
        String[] fieldValue = new String[20];
        if(fieldValues!=null && fieldValues.length<=20){
                System.arraycopy(fieldValues, 0, fieldValue, 0, fieldValues.length);
        }
        try {
            list = spEvalProcessInclude.executeProcedure(fieldValue[0],fieldValue[1], fieldValue[2], fieldValue[3], fieldValue[4], fieldValue[5], fieldValue[6], fieldValue[7], fieldValue[8], fieldValue[9], fieldValue[10], fieldValue[11], fieldValue[12], fieldValue[13], fieldValue[14], fieldValue[15], fieldValue[16], fieldValue[17], fieldValue[18], fieldValue[19]);
        } catch (Exception e) {
            logger.error("getEvalProcessInclude : "+logUserId+" "+e);
        }
        fieldValue = null;
        logger.debug("getEvalProcessInclude : "+logUserId+" Ends");
        return list;
    }
}
