/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblQuestionModule;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface QuestionModuleService {
    
    public void addTblQuestionModule(TblQuestionModule tblQuestionModule);
    
    public void deleteTblQuestionModule(TblQuestionModule tblQuestionModule);

    /**
     *
     * @param tblConfigPaThreshold
     * @return
     */
    public String updateTblQuestionModule(TblQuestionModule tblQuestionModule);

    public List<TblQuestionModule> getAllTblQuestionModule();
    
    /**
     *
     * @param id
     * @return
     */
    public List<TblQuestionModule> findTblQuestionModule(int id);
    
    
}
