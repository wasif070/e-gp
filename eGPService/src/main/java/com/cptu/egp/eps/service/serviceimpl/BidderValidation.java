/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh.shah
 */
public class BidderValidation {

    final Logger logger = Logger.getLogger(BidderValidation.class);
    private String logUserId = "0";
    HibernateQueryDao hibernateQueryDao;
    TblLoginMasterDao tblLoginMasterDao;
    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    

    /**
     * To get tenderFormId based on tender id and user id 
     * @param tenderId
     * @param userId
     * @return tenderFormId,srvBoqId,srvBoqType
     */ 
    public List<Object[]> getBidderFormId(int tenderId, int userId) {
        List<Object[]> list = null;
        logger.debug("getBidderFormId : " + logUserId + "starts");
        StringBuffer sb = new StringBuffer();
        sb.append("select b.tenderFormId, a.tblCmsSrvBoqMaster.srvBoqId, c.srvBoqType");
        sb.append(" from TblCmsTemplateSrvBoqDetail a, TblTenderForms b, TblCmsSrvBoqMaster c");
        sb.append(" where a.tblTemplateSectionForm.formId = b.templateFormId and a.tblCmsSrvBoqMaster.srvBoqId = c.srvBoqId and b.tenderFormId in");
        sb.append(" (select tblTenderForms.tenderFormId from TblTenderBidForm where tblTenderMaster.tenderId = " + tenderId + " and userId = " + userId + " ) and c.srvBoqId in (13,14,15)");
        try {
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getBidderFormId : " + e + "ends");
        }
        logger.debug("getBidderFormId : " + logUserId + "ends");
        return list;
    }
    
    public List<Object[]> getBidDetails(int tenderId,String columnId,int tenderFormId,int userid) {
        List<Object[]> list = null;
        logger.debug("getBidDetails : "+logUserId + "starts");
        StringBuffer sb = new StringBuffer();
        sb.append("select f.bidId, t.bidTableId, d.cellId, d.cellValue");
        sb.append(" from TblTenderBidForm f, TblTenderBidTable t, TblTenderBidDetail d");
        sb.append(" where f.bidId = t.tblTenderBidForm.bidId and d.tblTenderBidTable.bidTableId = t.bidTableId and d.columnId in( " + columnId);
        sb.append(") and f.tblTenderMaster.tenderId = "+ tenderId +" and f.tblTenderForms.tenderFormId = "+ tenderFormId + " and f.userId = " + userid);
        try{
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch(Exception e)
        {
            logger.error("getBidDetails :" + e + "ends");
        }
        logger.debug("getBidDetails :" + logUserId + "ends");
        return list;
    }

    public List<TblLoginMaster> getPasswordByUserId(int userid)
    {
        List<TblLoginMaster> list = null;
        logger.debug("getPasswordByUserId : "+logUserId + "starts");
        try{
            list = tblLoginMasterDao.findTblLoginMaster("userId",Operation_enum.EQ,userid);
        }catch(Exception e)
        {
            logger.error("getPasswordByUserId :" + e + "ends");
        }
        logger.debug("getPasswordByUserId :" + logUserId + "ends");
        return list;
    }

  




}
