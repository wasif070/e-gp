/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaIssueDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderPerSecDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPerfSecurity;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class TblTenderPerformanceSec {

    private final Logger logger = Logger.getLogger(TblTenderPerformanceSec.class);
    private TblTenderPerSecDao tblTenderPerSecDao;
    private TblNoaIssueDetailsDao tblNoaIssueDetailsDao;
    private HibernateQueryDao hibernateQueryDao;

    public TblNoaIssueDetailsDao getTblNoaIssueDetailsDao() {
        return tblNoaIssueDetailsDao;
    }

    public void setTblNoaIssueDetailsDao(TblNoaIssueDetailsDao tblNoaIssueDetailsDao) {
        this.tblNoaIssueDetailsDao = tblNoaIssueDetailsDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTenderPerSecDao getTblTenderPerSecDao() {
        return tblTenderPerSecDao;
    }

    public void setTblTenderPerSecDao(TblTenderPerSecDao tblTenderPerSecDao) {
        this.tblTenderPerSecDao = tblTenderPerSecDao;
    }

    /**
     * Get Tender Performance Security for the Tender
     * @param tenderId from tbl_TenderMaster
     * @param appPkgId from tbl_TenderLotSecurity
     * @param roundId for the current bidder
     * @return List of TblTenderPerfSecurity
     */
    public List<TblTenderPerfSecurity> getData(int tenderId, String appPkgId,String roundId) {
        logger.debug("getData :  Starts");
        boolean flag = false;
        List<TblTenderPerfSecurity> list = null;
        try {
            list = tblTenderPerSecDao.findTblTenderPerfSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "pkgLotId", Operation_enum.EQ, Integer.parseInt(appPkgId),"roundId",Operation_enum.EQ,Integer.parseInt(roundId));

        } catch (Exception e) {
            logger.error("getData :  "+e);
        }
        logger.debug("getData :  end");
        return list;
    }

    /**
     *Inserting Tender Performance Security
     * @param perfSecuritys to be inserted
     * @return true or false for success or fail
     */
    public boolean addPerfSec(TblTenderPerfSecurity perfSecuritys) {
        logger.debug("addPerfSec :  Starts");
        boolean flag = false;
        try {
            tblTenderPerSecDao.addTblTenderPerfSecurity(perfSecuritys);
            flag = true;
        } catch (Exception e) {
            logger.error("addPerfSec :  "+e);
        }
        logger.debug("addPerfSec :  Ends");
        return flag;
    }

    /**
     *Updates Tender Performance Security
     * @param perfSecuritys to be updated
     * @return  true or false for success or fail
     */
    public boolean updatePerfSec(TblTenderPerfSecurity perfSecuritys) {
        logger.debug("updatePerfSec :  Starts");
        boolean flag = false;
        try {
            tblTenderPerSecDao.updateTblTenderPerfSecurity(perfSecuritys);
            flag = true;
        } catch (Exception e) {
            logger.error("updatePerfSec :  "+e);
        }
        logger.debug("updatePerfSec :  Ends");
        return flag;
    }

    /**
     *Check whether NOA issued for the Tender
     * @param tenderId from tbl_TenderMaster
     * @param pkgLotId from tbl_TenderLotSecurity
     * @return true or false for issued or not issued
     */
    public boolean chkNOAIssued(int tenderId, int pkgLotId) {
        logger.debug("chkNOAIssued :  Starts");
        boolean flag = false;
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tnid.noaIssueId,tnid.tblTenderMaster.tenderId from TblNoaIssueDetails tnid,TblNoaAcceptance tna where tnid.tblTenderMaster.tenderId = " + tenderId + " and tnid.pkgLotId = " + pkgLotId + " and tnid.noaIssueId = tna.tblNoaIssueDetails.noaIssueId and tna.acceptRejStatus = 'approved'");
            if(!list.isEmpty()){
                flag = true;
            }
        } catch (Exception e) {
            logger.error("chkNOAIssued :  "+e);
        }
        logger.debug("chkNOAIssued :  Ends");
        return flag;
    }
    
    /**
     * Get Performance Security data by userId and tenderId
     * @return 
     */
    public TblTenderPerfSecurity getPerfSec(int userId,int tenderId, int lotId){
        logger.debug("getPerfSec :  Starts");
        TblTenderPerfSecurity tblTenderPerfSecurity = new TblTenderPerfSecurity();
        List<TblTenderPerfSecurity> list = new ArrayList<TblTenderPerfSecurity>();
        
        try {
            list = tblTenderPerSecDao.findTblTenderPerfSecurity("createdBy",Operation_enum.EQ,userId,"pkgLotId",Operation_enum.EQ,lotId,"tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId));
            if(!list.isEmpty()){
                tblTenderPerfSecurity = list.get(0);
            }
        } catch (Exception e) {
            logger.error("getPerfSec :  "+e);
        }
        logger.debug("getPerfSec :  Ends");
        return tblTenderPerfSecurity;
    }
}
