/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationPEDetails;
import java.util.List;

/**
 * This method returns the all the records of sharedPEdetails only for RHD Dept.
 * @author Sudhir Chavhan
 */
public interface RHDIntegrationPEDetailsService {

    public void setLogUserId(String logUserId);
    /***
     * This method returns the all the records of sharedPEdetails only for RHD Dept.
     * @return List of RHDIntegrationDetails
     * @throws Exception
     */
    public List<RHDIntegrationPEDetails> getSharedPEDetailForRHD() throws Exception;

}
