/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblGradeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateGradeService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class CreateGradeServiceImpl implements CreateGradeService
{
    final Logger logger = Logger.getLogger(CreateGradeServiceImpl.class);

    TblGradeMasterDao tblGradeMasterDao;
    String logUserId = "0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public TblGradeMasterDao getTblGradeMasterDao()
    {
        return tblGradeMasterDao;
    }

    public void setTblGradeMasterDao(TblGradeMasterDao tblGradeMasterDao)
    {
        this.tblGradeMasterDao = tblGradeMasterDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public short createGrade(TblGradeMaster gradeMaster)
    {
        logger.debug("createGrade : " + logUserId + " Starts");
         short grade = 0;
         String action = "Create Class";
        try
        {
            tblGradeMasterDao.addTblGradeMaster(gradeMaster);
            grade =  gradeMaster.getGradeId();
        }
        catch(Exception ex)
        {
            logger.error("createGrade : "+ex.getMessage());
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, gradeMaster.getGradeId(), "gradeId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("createGrade : " + logUserId + " Ends");
        return grade;
    }

    @Override
    public void updateGrade(TblGradeMaster gradeMaster)
    {
        String action = "Edit Class";
        logger.debug("updateGrade : " + logUserId + " Starts");
        try {
            tblGradeMasterDao.updateTblGradeMaster(gradeMaster);
        } catch (Exception e) {
            logger.error("updateGrade : "+e.getMessage());
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, gradeMaster.getGradeId(), "gradeId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("updateGrade : " + logUserId + " Ends");
    }

    @Override
    public TblGradeMaster getGradeDetails(short gradeId)
    {
        logger.debug("getGradeDetails : "+logUserId+" Starts");
        List<TblGradeMaster> gradeList = null;
        try {
            gradeList = tblGradeMasterDao.findTblGradeMaster("gradeId", Operation_enum.EQ, gradeId);
        }
        catch (Exception ex) {
            logger.error("getGradeDetails : "+logUserId+" :"+ex);
        }
        logger.debug("getGradeDetails : "+logUserId+" Ends");
        return gradeList.get(0);
    }

    @Override
    public boolean verifyGrade(String grade)
    {
        logger.debug("verifyGrade : "+logUserId+" Starts");
        List<TblGradeMaster> gradeList = null;
        boolean flag = false;
        try {
            gradeList = tblGradeMasterDao.findTblGradeMaster("grade", Operation_enum.LIKE, grade);
            if(gradeList!=null && !gradeList.isEmpty()){
                flag =  true;
        }
        }
        catch (Exception ex) {
            logger.error("verifyGrade : "+logUserId+" :"+ex);
            flag =  true;
        }
        logger.debug("verifyGrade : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<TblGradeMaster> getGradeMasterList(int firstResult, int maxResult, Object... values)
    {
            logger.debug("getGradeMasterList : "+logUserId+" Starts");
        List<TblGradeMaster> list = null;
        try {
            list =  tblGradeMasterDao.findByCountTblGradeMaster(firstResult,maxResult,values);
        }
        catch (Exception ex) {
            logger.error("getGradeMasterList : "+logUserId+" :"+ex);
            
        }
        logger.debug("getGradeMasterList : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long getCntGradeMaster()
    {
        logger.debug("getCntGradeMaster : "+logUserId+" Starts");
        long count = -1;
        try {
            count =  tblGradeMasterDao.getTblGradeMasterCount();
        }
        catch (Exception ex) {
            logger.error("getCntGradeMaster : "+logUserId+" :"+ex);
        }
        logger.debug("getCntGradeMaster : "+logUserId+" Ends");
        return count;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
