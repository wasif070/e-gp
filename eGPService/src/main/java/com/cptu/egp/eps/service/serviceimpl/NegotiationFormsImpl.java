/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegotiationFormsDao;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblNegotiationForms;
import com.cptu.egp.eps.dao.daointerface.TblNegBidTableDao;
import com.cptu.egp.eps.dao.daointerface.TblNegBidDtlSrvDao;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class NegotiationFormsImpl {

    public static final Logger logger = Logger.getLogger(NegotiationFormsImpl.class);

    TblNegotiationFormsDao tblNegotiationFormsDao;
    TblNegBidTableDao tblNegBidTableDao;
    TblNegBidDtlSrvDao tblNegBidDtlSrvDao;
    SPXMLCommon  pXMLCommon;
    private String logUserId = "0";

    public TblNegBidDtlSrvDao getTblNegBidDtlSrvDao()
    {
        return tblNegBidDtlSrvDao;
    }

    public void setTblNegBidDtlSrvDao(TblNegBidDtlSrvDao tblNegBidDtlSrvDao)
    {
        this.tblNegBidDtlSrvDao = tblNegBidDtlSrvDao;
    }

    public TblNegBidTableDao getTblNegBidTableDao()
    {
        return tblNegBidTableDao;
    }

    public void setTblNegBidTableDao(TblNegBidTableDao tblNegBidTableDao)
    {
        this.tblNegBidTableDao = tblNegBidTableDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public SPXMLCommon getpXMLCommon() {
        return pXMLCommon;
    }

    public void setpXMLCommon(SPXMLCommon pXMLCommon) {
        this.pXMLCommon = pXMLCommon;
    }
    
    public TblNegotiationFormsDao getTblNegotiationFormsDao() {
        return tblNegotiationFormsDao;
    }

    public void setTblNegotiationFormsDao(TblNegotiationFormsDao tblNegotiationFormsDao) {
        this.tblNegotiationFormsDao = tblNegotiationFormsDao;
    }

    /**
     * Insert Update Negotiation operations by xml.
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @return list of flag of operations 
     */
    public List<CommonMsgChk> InsUpdateNegOperationDetailsByXML(String param1,String param2,String param3,String param4){
        logger.debug("InsUpdateNegOperationDetailsByXML : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        try {
            list = pXMLCommon.executeProcedure(param1, param2, param3,param4);
        } catch (Exception e) {
            logger.error("InsUpdateNegOperationDetailsByXML : "+logUserId+" : "+e);
    }
        logger.debug("InsUpdateNegOperationDetailsByXML : "+logUserId+" Ends");
        return list;
    }
    
    /**
     * Add Negotiaion forms details.
     * @param tblNegotiationForms
     */
    public void addNegForm(TblNegotiationForms tblNegotiationForms){
        logger.debug("addNegForm : "+logUserId+" Starts");
        try{
            tblNegotiationFormsDao.addTblNegotiationForms(tblNegotiationForms);
        }catch(Exception ex){
            logger.error("addNegForm : "+logUserId+" : "+ex);
        }
        logger.debug("addNegForm : "+logUserId+" Ends");
    }

    /**
     * Check if form is selected or not.
     * @param formId
     * @param negId
     * @return true if form is selected or false if not.
     */
    public boolean isSelectedForm(int formId,int negId){
        logger.debug("isSelectedForm : "+logUserId+" Starts");
        boolean flag = false;
        List<TblNegotiationForms> listNegForms = null;
        
        try{
            listNegForms = tblNegotiationFormsDao.findTblNegotiationForms("formId",Operation_enum.EQ,formId,"negId",Operation_enum.EQ,negId);
        }catch(Exception ex){
           logger.error("isSelectedForm : "+logUserId+" : "+ex);
        }

        if(listNegForms.isEmpty())
        {
             flag = false;}
        else{
             flag = true;}
        logger.debug("isSelectedForm : "+logUserId+" Ends");
        return flag;
    }
    
    /**
     *  Get Negotiation form data by negid.
     * @param negId
     * @return List of Negotiation forms.
     */
    public List<TblNegotiationForms> getNegFormData(int negId){
        logger.debug("getNegFormData : "+logUserId+" Starts");
        List<TblNegotiationForms> listforms = null;
        try{
            listforms = tblNegotiationFormsDao.findTblNegotiationForms("negId",Operation_enum.EQ,negId);
        }catch(Exception ex){
           logger.error("getNegFormData : "+logUserId+" : "+ex);
        }
        logger.debug("getNegFormData : "+logUserId+" Ends");
        return listforms;
    }
}
