/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalTsccommentsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import com.cptu.egp.eps.service.serviceinterface.EvalTscCommentsService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author Administrator
 */
public class EvalTscCommentsServiceImpl implements EvalTscCommentsService{

    final Logger logger = Logger.getLogger(EvalTscCommentsServiceImpl.class);
    TblEvalTsccommentsDao tblEvalTsccommentsDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId ="0";
     
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblEvalTsccommentsDao getTblEvalTsccommentsDao() {
        return tblEvalTsccommentsDao;
    }

    public void setTblEvalTsccommentsDao(TblEvalTsccommentsDao tblEvalTsccommentsDao) {
        this.tblEvalTsccommentsDao = tblEvalTsccommentsDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    
    @Override
    public boolean insertData(TblEvalTsccomments tblEvalTsccomments) {
        logger.debug("insertData : "+logUserId+" Starts");
        boolean flag = false;
        try{
        tblEvalTsccommentsDao.addTblEvalTsccomments(tblEvalTsccomments);
            flag=  true;
        }catch(Exception ex){
           logger.error("insertData : "+logUserId+" : "+ex.toString());
        }
        logger.debug("insertData : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<Object[]> getComments(int tenderId,int formId) {
        logger.debug("getComments : "+logUserId+" Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select etc.commentsId,etc.comments,em.employeeName,etc.tscUserId from TblEvalTsccomments as etc,TblEmployeeMaster em where etc.tenderId="+tenderId+" and etc.formId="+formId+" and etc.tscUserId = em.tblLoginMaster.userId");
           } catch (Exception ex) {
            logger.error("getComments : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getComments : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean delRec(int commentId) {
       logger.debug("delRec : "+logUserId+" Starts");
       boolean flag = true;
        try{
        TblEvalTsccomments tblEvalTsccomments = new TblEvalTsccomments();
        tblEvalTsccomments.setCommentsId(commentId);
        tblEvalTsccomments.setComments("");
        tblEvalTsccomments.setCommentsDt(new Date());
        tblEvalTsccomments.setEvalStatus("");
        tblEvalTsccomments.setFormId(0);
        tblEvalTsccomments.setTenderId(0);
        tblEvalTsccomments.setPkgLotId(0);
        tblEvalTsccomments.setTscUserId(0);
        tblEvalTsccommentsDao.deleteTblEvalTsccomments(tblEvalTsccomments);
        }catch(Exception ex){
            logger.error("delRec : "+logUserId+" : "+ex.toString());
            flag = false;
        }
       logger.debug("delRec : "+logUserId+" Ends");
       return flag;
    }

    @Override
    public TblEvalTsccomments getComments(int commentId) {
       logger.debug("getComments "+logUserId+" Starts");
        TblEvalTsccomments tblEvalTsccomments = new TblEvalTsccomments();
        try {
            tblEvalTsccomments = tblEvalTsccommentsDao.findTblEvalTsccomments("commentsId",Operation_enum.EQ,commentId).get(0);
        } catch (Exception e) {
            logger.error("getComments "+logUserId+" :"+e);
        }
        logger.debug("getComments "+logUserId+" Ends");
        return tblEvalTsccomments;
    }

    @Override
    public boolean updateComments(int commentId, String comments) {
        logger.debug("updateComments "+logUserId+" Starts");
        boolean flag =false;
        try {
            String sqlQuery = "update TblEvalTsccomments set comments='"+comments+"' where commentsId="+commentId;
            if(hibernateQueryDao.updateDeleteNewQuery(sqlQuery)>0){
                flag = true;
            }
        } catch (Exception e) {
            logger.error("updateComments "+logUserId+" :"+e);
        }
        logger.debug("updateComments "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean displayNotifyButton(int commentId) {
        logger.debug("displayNotifyButton "+logUserId+" Starts");
        boolean flag = false;
         List<TblEvalTsccomments> list = null;
        try {
           list =  tblEvalTsccommentsDao.findTblEvalTsccomments("tenderId",Operation_enum.EQ,commentId);
           if(!list.isEmpty() && list!=null){
               flag=true;
           }
        } catch (Exception e) {
            logger.error("displayNotifyButton "+logUserId+" :"+e);
        }
        logger.debug("displayNotifyButton "+logUserId+" Ends");
        return flag;
    }
}
