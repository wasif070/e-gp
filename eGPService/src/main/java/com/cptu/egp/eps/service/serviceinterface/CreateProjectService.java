/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectFPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.eps.service.audit.AuditTrail;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author parag
 */
public interface CreateProjectService {

    /**
     *
     */
    public void addProjectDetail();

    /**
     * Get organization detail from TblDepartmentMaster.
     * @return
     */
    public List<TblDepartmentMaster> getOrganizationDetail();

    /**
     * Used Store Procedure to get project detail.
     * @param param1
     * @param param2
     * @param param3
     * @return
     * @throws Exception
     */
    public List<CommonAppData> getDetailsBySP(String param1,String param2,String param3) throws Exception;

    /**
     * Used Store Procedure to insert project detail.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy 
     * @return
     * @throws Exception
     */
    public List<CommonSPReturn> insertProjectDetailsBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,Integer createdBy) throws Exception;

    /**
     * Used Store Procedure to delete and insert detail.
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @return
     * @throws Exception
     */
    public List<CommonMsgChk> projectOperationDetailsBySP(String param1,String param2,String param3,String param4) throws Exception;

    /**
     * Used Store Procedure to get project detail from tbl_ProjectMaster.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy 
     * @return
     * @throws Exception
     */
    public List<SPProjectDetailReturn> getProjectMasterBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,Integer createdBy) throws Exception;

    /**
     * Used Store Procedure to get user wise project roles from tbl_ProjectRoles.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param param 
     * @return
     * @throws Exception
     */
    public List<SPProjectRolesReturn> getProjectRolesBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,int param) throws Exception;

    /**
     *Get list of Procurement method
     * @return List of TblProcurementMethod
     */
    public List<TblProcurementMethod> getListProcurementMethod();

    /**
     * Used Store Procedure to get user wise project financial power from tbl_ProjectFinPower.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @return
     * @throws Exception
     */
    public List<SPProjectFPReturn> getProjectFPBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId) throws Exception;

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setUserId(String logUserId);

    /**
     * For AUdit Trail Purpose
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
