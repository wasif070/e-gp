/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsContractTerminationDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsContractTerminationServiceImpl implements CmsContractTerminationService {

    final Logger logger = Logger.getLogger(CmsContractTerminationServiceImpl.class);
    private TblCmsContractTerminationDao cmsContractTerminationDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private static final String SPACE = " : ";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsContractTerminationDao getCmsContractTerminationDao() {
        return cmsContractTerminationDao;
    }

    public void setCmsContractTerminationDao(TblCmsContractTerminationDao cmsContractTerminationDao) {
        this.cmsContractTerminationDao = cmsContractTerminationDao;
    }

    /***
     * This method inserts a TblCmsContractTermination object in database
     * @param tblCmsContractTermination
     * @return int
     */
    @Override
    public int insertCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("insertCmsContractTermination : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsContractTerminationDao.addTblCmsContractTermination(tblCmsContractTermination);
            flag = tblCmsContractTermination.getContractTerminationId();
        } catch (Exception ex) {
            logger.error("insertCmsContractTermination : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsContractTermination : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
    @Override
    public boolean updateCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("updateCmsContractTermination : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsContractTerminationDao.updateTblCmsContractTermination(tblCmsContractTermination);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsContractTermination : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsContractTermination : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
    @Override
    public boolean deleteCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("deleteCmsContractTermination : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsContractTerminationDao.deleteTblCmsContractTermination(tblCmsContractTermination);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsContractTermination : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsContractTermination : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsContractTermination objects from database
     * @return List<TblCmsContractTermination>
     */
    @Override
    public List<TblCmsContractTermination> getAllCmsContractTermination() {
        logger.debug("getAllCmsContractTermination : " + logUserId + STARTS);
        List<TblCmsContractTermination> cmsContractTerminationList = new ArrayList<TblCmsContractTermination>();
        try {
            cmsContractTerminationList = cmsContractTerminationDao.getAllTblCmsContractTermination();
        } catch (Exception ex) {
            logger.error("getAllCmsContractTermination : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsContractTermination : " + logUserId + ENDS);
        return cmsContractTerminationList;
    }

    /***
     *  This method returns no. of TblCmsContractTermination objects from database
     * @return long
     */
    @Override
    public long getCmsContractTerminationCount() {
        logger.debug("getCmsContractTerminationCount : " + logUserId + STARTS);
        long cmsContractTerminationCount = 0;
        try {
            cmsContractTerminationCount = cmsContractTerminationDao.getTblCmsContractTerminationCount();
        } catch (Exception ex) {
            logger.error("getCmsContractTerminationCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsContractTerminationCount : " + logUserId + ENDS);
        return cmsContractTerminationCount;
    }

    /***
     * This method returns TblCmsContractTermination for the given Id
     * @param int contractTerminationId
     * @return TblCmsContractTermination
     */
    @Override
    public TblCmsContractTermination getCmsContractTermination(int contractTerminationId) {
        logger.debug("getCmsContractTermination : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        List<TblCmsContractTermination> cmsContractTerminationList = null;
        try {
            cmsContractTerminationList = cmsContractTerminationDao.findTblCmsContractTermination("contractTerminationId", Operation_enum.EQ, contractTerminationId,"contractTerminationId",Operation_enum.ORDERBY,Operation_enum.DESC);
        } catch (Exception ex) {
            logger.error("getCmsContractTermination : " + logUserId + SPACE + ex);
        }
        if (!cmsContractTerminationList.isEmpty()) {
            tblCmsContractTermination = cmsContractTerminationList.get(0);
        }
        logger.debug("getCmsContractTermination : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }

    /***
     * This method returns TblCmsContractTermination for the given contractSignId
     * @param contractSignId
     * @return TblCmsContractTermination
     */
    @Override
    public TblCmsContractTermination getCmsContractTerminationForContractSignId(int contractSignId) {
        logger.debug("getCmsContractTerminationForContractSignId : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        List<TblCmsContractTermination> cmsContractTerminationList = null;
        try {
            cmsContractTerminationList = cmsContractTerminationDao.findTblCmsContractTermination("contractId", Operation_enum.EQ, contractSignId,"contractTerminationId",Operation_enum.ORDERBY,Operation_enum.DESC);
        } catch (Exception ex) {
            logger.error("getCmsContractTerminationForContractSignId : " + logUserId + SPACE + ex);
        }
        if (!cmsContractTerminationList.isEmpty()) {
            tblCmsContractTermination = cmsContractTerminationList.get(0);
        }
        logger.debug("getCmsContractTerminationForContractSignId : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }

    /***
     *  This method changes the status of TblCmsContractTermination object for the given contractSignId
     * @param contractSignId
     * @param columnType either <b> status </b> or <b>work flow status</b>
     * @param status
     * @return TblCmsContractTermination
     */
    @Override
    public TblCmsContractTermination changeCTStatus(int contractSignId, String columnType, String status) {
        logger.debug("changeCTStatus : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        tblCmsContractTermination = getCmsContractTerminationForContractSignId(contractSignId);
        if (tblCmsContractTermination != null) {
            if ("status".equalsIgnoreCase(columnType)) {
                tblCmsContractTermination.setStatus(status);
                updateCmsContractTermination(tblCmsContractTermination);
            } else if ("workflowStatus".equalsIgnoreCase(columnType)) {
                tblCmsContractTermination.setWorkflowStatus(status);
                updateCmsContractTermination(tblCmsContractTermination);
            }
        }
        logger.debug("changeCTStatus : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }
    
    @Override
    public List<Object> getLotID(int ContractTerminationId) {
        logger.debug("getLotID : " + logUserId + STARTS);
        List<Object> list = null;
        try {
            StringBuffer sb = new StringBuffer();
            sb.append("select nid.pkgLotId from TblCmsContractTermination ct,TblContractSign cs, ");
            sb.append("TblNoaIssueDetails nid where ct.contractId = cs.contractSignId ");
            sb.append("and cs.noaId = nid.noaIssueId and ct.contractTerminationId="+ContractTerminationId+"");
            list = hibernateQueryDao.getSingleColQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getLotID : " + logUserId + SPACE + e);
        }
        logger.debug("getLotID : " + logUserId + ENDS);
        return list;
    }
   
    @Override
    public List<Object[]> getWFUserEmailId(int childId,int objectId,int userId) {
       logger.debug("getWorkFlowEmailID : " + logUserId + STARTS);
       List<Object[]> list = null;
       try {
            StringBuffer sb = new StringBuffer();
            sb.append("select emailId,userId from TblLoginMaster where userId in ");
            sb.append("( select twflc.tblLoginMaster.userId from TblWorkFlowLevelConfig twflc ");
            sb.append("where twflc.childId="+childId+" and twflc.objectId="+objectId+" and twflc.tblLoginMaster.userId!="+userId+" )");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getWorkFlowEmailID : " + logUserId + SPACE + e);
        }
       logger.debug("getWorkFlowEmailID : " + logUserId + ENDS);
       return list;
    }

    @Override
    public List<Object> getContractTerminationDetails(int lotId){
       logger.debug("getContractTerminationDetails : " + logUserId + STARTS);
       List<Object> list = null;
       try {
            StringBuffer sb = new StringBuffer();
            sb.append("select tcct.contractTerminationId from TblCmsContractTermination tcct ");
            sb.append("where tcct.contractId in ( select tcs.contractSignId from TblContractSign tcs, ");
            sb.append("TblNoaIssueDetails tnid where tcs.noaId=tnid.noaIssueId and tnid.pkgLotId="+lotId+" ) ");
            sb.append("order by contractTerminationId desc");
            
            list = hibernateQueryDao.singleColQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getContractTerminationDetails : " + logUserId + SPACE + e);
        }
       logger.debug("getContractTerminationDetails : " + logUserId + ENDS);
       return list;
    }

    @Override
    public long chkDuplicateEntry(String contractId) {
        logger.debug("chkDuplicateEntry : " + logUserId + STARTS);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblCmsContractTermination tcct", "tcct.contractId in ('" + contractId + "') and tcct.status='pending'");
        } catch (Exception ex) {
            logger.error("chkDuplicateEntry : " + logUserId +ex);
        }
        logger.debug("chkDuplicateEntry : " + logUserId + ENDS);
        return lng;
    }

    @Override
    public List<TblCmsContractTermination> getCTHistory(int ContractId){
       logger.debug("getCTHistory : " + logUserId + STARTS);
       List<TblCmsContractTermination> tblCmsContractTermination = null;
       try {
                      
            tblCmsContractTermination = cmsContractTerminationDao.findTblCmsContractTermination("contractId",Operation_enum.EQ,ContractId,"status",Operation_enum.IN,new String[]{"rejected","approved"});
        } catch (Exception e) {
            logger.error("getCTHistory : " + logUserId + SPACE + e);
        }
       logger.debug("getCTHistory : " + logUserId + ENDS);
       return tblCmsContractTermination;
    }
}
