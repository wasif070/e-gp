/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface PEOfficeCreationService {
    
    
    
    public Short getDzongkhagId(String Dzongkhag) throws Exception;

    /**
     * Create PE Office (Ministry,Division,Organization)
     * @param tblOfficeMaster
     * @return true if PE Office created else false
     */
    public boolean addPEOfficeCreation(TblOfficeMaster tblOfficeMaster);

    /**
     * TblDepartmentMaster List
     * @param values variable parameter
     * @return List of TblDepartmentMaster 
     */
    public List<TblDepartmentMaster> findTblOfficeMaster(Object... values);

    /**
     *Gives count of all PE-Offices
     * @return count of all PE-Offices
     */
    public long getAllCountOfPEMaster();

    /**
     *Gives count of all PE-Offices based on departmentType
     * @param deptType Department Type
     * @return count of all PE-Offices
     */
    public long getAllCountOfPEMasterList(String deptType) ;

    /**
     * GET count for PE offices (search)
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfPEMasterList(String deptType,String searchField,String searchString,String searchOper) ;

    /**
     * count records for PE 
     * @param deptType
     * @param userId
     * @return number of records
     * throws Exception
     */
    public long getAllCountOfPEMasterListOrg(String deptType,int userId) ;

    /**
     * count records for PE (Search)
     * @param deptType
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfPEMasterListOrg(String deptType,int userId,String searchField,String searchString,String searchOper) ;

    /**
     * updating data
     * @param tblOfficeMaster
     * @return true if updated else false
     */
    public boolean updatePEOfficeMaster(TblOfficeMaster tblOfficeMaster);

    /**
     *List of TblOfficeMaster
     * @param firstResult record from
     * @param maxResult max records
     * @param values condition on which search is done
     * @return List of TblOfficeMaster
     */
    public List<TblOfficeMaster> findPEOfficeMaster(int firstResult, int maxResult, Object... values) ;

    /**
     * fetching data for PE Office
     * @param firstResult record from
     * @param maxResult max records
     * @param deptType Department type
     * @param ascClause asc desc info
     * @return information for PE office
     * throws Exception
     */
    public List<Object[]> findPEOfficeMasterList(int firstResult, int maxResult, String deptType,String ascClause) ;

     /**
     * fetching data for PE Office
     * @param firstResult record from
     * @param maxResult max records
     * @param deptType Department type
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return information for PE office
     */
    public List<Object[]> findPEOfficeMasterList(int firstResult, int maxResult, String deptType,String searchField,String searchString,String searchOper) ;

    /**
     * fetching data for PE Office
     * @param firstResult record from
     * @param maxResult max records
     * @param deptType Department type
     * @param userId
     * @param ascClause
     * @return information for PE office
     */
    public List<Object[]> findPEOfficeMasterListORG(int firstResult, int maxResult, String deptType,int userId,String ascClause) ;

    /**
     * fetching data for PE Office
     * @param firstResult record from
     * @param maxResult max records
     * @param deptType Department type
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return information for PE office
     */
    public List<Object[]> findPEOfficeMasterListORG(int firstResult, int maxResult, String deptType,int userId,String searchField,String searchString,String searchOper) ;

    /**
     *List of TblOfficeMaster
     * @param values variable arguments containing conditions
     * @return List of TblOfficeMaster
     */
    public List<TblOfficeMaster> findOfficeMaster(Object... values) ;

    /**
     *All offices from TblOfficeMaster
     * @return List of TblOfficeMaster
     */
    public List<TblOfficeMaster> getAllOffices();

    /**
     *List of TblDepartmentMaster
     * @param values variable arguments containing conditions
     * @return List of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> findDepartmentName(Object... values);

    /**
     *List of TblOfficeMaster
     * @param deptId from tbl_departmentMaster
     * @param districtId bangladesh districtId
     * @return List of TblOfficeMaster
     */
    public List<TblOfficeMaster> getPEOfficeList(int deptId, int districtId) ;
    
    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
    public void setAuditTrail(AuditTrail auditTrail);

}
