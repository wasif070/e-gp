/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;
import java.util.*;
import com.cptu.egp.eps.model.table.*;
import com.cptu.egp.eps.dao.storedprocedure.SearchComplaintPaymentBean; 

/**
 *
 * @author Administrator
 */
public interface ComplaintMgmtService {
   
    /**
     * Get Search Result
     * @return
     */
  
	public List<TblComplaintType> getComplaintTypes();
	//public List<TblComplaintLevel> getComplaintLevels();
        /**
         * this method register the complaint in tbl_cms_complaintmaster table
         * @param complaint
         */
	public void addComplaint(TblComplaintMaster complaint);
        /**
         * this method give the detail of complaint by passing complaint Id
         * @param complaintId
         * @return list Of complaint
         */
	public TblComplaintMaster getComplaintDetails(int complaintId);
        /**
         * This method saves the complaint doc details
         * @param doc
         * @throws Exception
         */
	public void saveDoc(TblComplaintDocs doc) throws Exception;
        /**
         * This method gives the details of tender
         * @param values
         * @return list of tenderMaster
         * @throws Exception
         */
	public List<TblTenderMaster> findByEntity(Object... values) throws Exception;
        /**
         * this method get the government userId
         * @param tenderId
         * @return list of object array
         */
	public List<Object[]> getGovtUser(int tenderId);
        /**
         * this method get the complaint type
         * @param values
         * @return complaint type object
         * @throws Exception
         */
	public List<TblComplaintType> getComplaintTypeById(Object... values) throws Exception;
        /**
         * this method add details of complaint in complaint history
         * @param history
         */
	public void addHistory(TblComplaintHistory history);
        /**
         * This method get the details of complaint
         * @param values
         * @return Complaint Details
         * @throws Exception
         */
	public List<TblComplaintMaster> getComplaintById(Object... values) throws Exception;
        /**
         * this method get the details of complaint levels
         * @param values
         * @return List<TblComplaintLevel>
         * @throws Exception
         */
	public List<TblComplaintLevel> findByLevel(Object... values) throws Exception;
        /**
         * this method get the details of Gov user from employee transfer
         * @param values
         * @return List<TblEmployeeTrasfer>
         * @throws Exception
         */
	public List<TblEmployeeTrasfer>	findByGovUser(Object... values) throws Exception;
        /**
         * This method update the details of complaints
         * @param master
         */
	public void updateMaster(TblComplaintMaster master);
        /**
         * this method get the details of complaint history
         * @param id
         * @param complaintLevelId
         * @param userTypeId
         * @return List<Object[]>
         */
	public List<Object[]> getComplaintHistory(int id,int complaintLevelId,int userTypeId);
        /**
         * this method get the details of document related to complaints
         * @param values
         * @return List<TblComplaintDocs>
         * @throws Exception
         */
	public List<TblComplaintDocs> getDocsList(Object... values) throws Exception;
        /**
         * this method get the details of document related to complaint
         * @param complaintId
         * @param govId
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getDocs(int complaintId,int govId) throws Exception;
        /**
         * This method get the list of complaint registered
         * @param tenderid
         * @param complaintlevelId
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getComplaintList(int tenderid,int complaintlevelId) throws Exception;
        /**
         * This method get the document that are in processing
         * @param id
         * @param levelId
         * @param uploadedBy
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getDocsProcessList(int id,int levelId,int uploadedBy) throws Exception;
        /**
         * this method get the complaint according to search criteria
         * @param complid
         * @param tenderid
         * @param refid
         * @return  List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getSearchList(int complid,int tenderid,int refid) throws Exception;
        /**
         * this method get all the review Panels details
         * @return List<TblReviewPanel>
         * @throws Exception
         */
	public List<TblReviewPanel> getReviewPanels() throws Exception;
        /**
         * this method gets the all pending complaints of review panel
         * @param values
         * @return List<TblComplaintMaster>
         * @throws Exception
         */
	public List<TblComplaintMaster>	getPendingComplaintsByReviewPanel(Object... values)throws Exception;
        /**
         * this method gets the details of government user details
         * @param officerId
         * @param role
         * @return List<Object>
         */
	public List<Object> getGovtUserDetails(int officerId, String role);
        /**
         * this method finds the offices
         * @param values
         * @return List<TblTenderDetails>
         * @throws Exception
         */
	public List<TblTenderDetails> findOffice(Object... values) throws Exception;
        /**
         * this method get the office hierarchy of particular office
         * @param officeid
         * @return List<Object>
         * @throws Exception
         */
	public List<Object> getOfficeHirarchies(int officeid) throws Exception;
        /**
         * this method gets all the complaint that are registered
         * @return List<TblComplaintMaster>
         * @throws Exception
         */
	public List<TblComplaintMaster>	getAllComplaints() throws Exception;
        /**
         * This method add the payment of complaints
         * @param payment
         */
	public void addComplaintPayment(TblComplaintPayments payment);
        /**
         * this method updates complaint payment
         * @param payment
         */
	public void updateComplaintPayment(TblComplaintPayments payment);
        /**
         * this method find the complaint payment with passing some parameter
         * @param values
         * @return List<TblComplaintPayments>
         * @throws Exception
         */
	public List<TblComplaintPayments>	findComplaintPayments(Object... values) throws Exception;
        /**
         * This method finds the login information with passing some parameters
         * @param values
         * @return List<TblLoginMaster>
         * @throws Exception
         */
	public List<TblLoginMaster>	findTblLoginMaster(Object... values) throws Exception;
        /**
         * this method gets the details of tender by reference number
         * @param values
         * @return List<TblTenderDetails>
         * @throws Exception
         */
	public List<TblTenderDetails>	getTenderDetailsByRef(Object... values)throws Exception;
        /**
         * this method search the complaint to achieve search functionality by passing different parameters
         * @param tenderId
         * @param complaintId
         * @param details
         * @param govUserId
         * @param role
         * @param status
         * @param complaintFeePaid
         * @param panelId
         * @return List<TblComplaintMaster>
         * @throws Exception
         */
	public List<TblComplaintMaster>	searchComplaintDetails(int tenderId, int complaintId, TblTenderDetails details,int govUserId, String role, String status, boolean complaintFeePaid,int panelId)throws Exception;
        /**
         * this method get the gov user details by user ID
         * @param userId
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]>  getGovUserDetails(int userId) throws Exception;
        /**
         * this method get the government secretaries
         * @param officeIds
         * @param role
         * @return List<Object>
         * @throws Exception
         */
	public List<Object>  getGovUserSecretaries(int[] officeIds, String role) throws Exception;
        /**
         * this method gets the tender details by tender Id
         * @param complaintId
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getTenderDetails(int complaintId)throws Exception;
        /**
         * this method gets the doc by doc Id
         * @param values
         * @return List<TblComplaintDocs>
         * @throws Exception
         */
	public List<TblComplaintDocs> findByDocId(Object... values) throws Exception;
        /**
         * this method delete the document
         * @param doc
         * @throws Exception
         */
	public void deleteDoc(TblComplaintDocs  doc) throws Exception;
        /**
         * this method gets the complaint history details
         * @param id
         * @param complaintLevelId
         * @return List<Object[]>
         * @throws Exception
         */
	public List<Object[]> getComplaintHistoryDetails(int id,int complaintLevelId) throws Exception;
        /**
         * This method search the complaint by different Parameters
         * @param emailId
         * @param verificationStatus
         * @param paymentFromDate
         * @param paymentToDate
         * @param createdBy
         * @param paymentFor
         * @param verifyFlag
         * @param tenderId
         * @param tenderer
         * @return List<SearchComplaintPaymentBean>
         * @throws Exception
         */
	public List<SearchComplaintPaymentBean> searchComplaintPayments(String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy,String paymentFor, boolean verifyFlag, int tenderId, String tenderer)throws Exception;
        /**
         * this method search the pending complaint by status
         * @param verificationStatus
         * @param paymentFor
         * @return List<SearchComplaintPaymentBean>
         * @throws Exception
         */
        public List<SearchComplaintPaymentBean> searchComplaintPendingPayments(String verificationStatus,String paymentFor)throws Exception;
        /**
         * get the doc details by complaint id
         * @param complaintId
         * @return List<Object[]>
         * @throws Exception
         */
        public List<Object[]> getDocsByComplaintId(int complaintId) throws Exception;
	

	//review panel
        /**
         * This method add the review panel
         * @param reviewPanel
         */
        public void addReviewPanel(TblReviewPanel reviewPanel);
        /**
         *This method update the review panel
         * @param reviewPanel
         */
        public void updateOrSaveReveiwPanel(TblReviewPanel reviewPanel);
        /**
         * This method add the user in loginmaster table
         * @param login
         */
        public void addTblLoginMaster(TblLoginMaster login);
        /**
         * This method add the user in employee table
         * @param tblEmployeeMaster
         */
        public void addEmployee(TblEmployeeMaster tblEmployeeMaster);
        /**
         *  This method add the entry in employee table
         * @param tblEmployeeMaster
         */
        public void updateEmpMaster(TblEmployeeMaster tblEmployeeMaster);
        /**
         *  This method update employee details in employee table
         * @param master
         */
        public void addTblEmployeeTrasfer(TblEmployeeTrasfer master);
 	
 	//for pe
        /**
         * This method get the complaint history for PE
         * @param tenderId
         * @return List<Object[]>
         * @throws Exception
         */
        public List<Object[]> getComplaintHistoryPe(int tenderId) throws Exception;
        /**
         * this method get the complaint history for review panel
         * @param userId
         * @return List<Object[]>
         * @throws Exception
         */
        public List<Object[]> getComplaintHistoryRP(int userId) throws Exception;
        /**
         * This method get the review Panel
         * @param empId
         * @return List<Object[]>
         * @throws Exception
         */
        public List<Object[]> getReviewPanel(int empId) throws Exception;
        /**
         * This method get the tenderer details by tender Id
         * @param tenderId
         * @return List<Object[]>
         * @throws Exception
         */
        public List<Object[]> getComplaintTenderer(int tenderId, int userId) throws Exception;
        /**
         * get the email id by gov user ID
         * @param govUserId
         * @return List<Object>
         * @throws Exception
         */
        public List<Object> getEmailId(int govUserId) throws Exception;
        /**
         * get email id from review panel id
         * @param rpId
         * @return List<Object>
         * @throws Exception
         */
        public List<Object> getEmailIdFromRpId(int rpId) throws Exception;

        /**
         * this method get the partner admins
         * @param userId
         * @param getAllFlag
         * @return List<TblPartnerAdmin>
         * @throws Exception
         */
        public List<TblPartnerAdmin> getTblPartnerAdmin(int userId, boolean getAllFlag) throws Exception;
        /**
         * update complaint for payment
         * @throws Exception
         */
        public void updateComplaintsForPayment()throws Exception;
        /**
         * get emp id from rpId
         * @param rpId
         * @return
         * @throws Exception
         */
        public int getEmpId(int rpId)throws Exception;

        /**
         * this method search the pending complaints
         * @param query
         * @return List<TblComplaintMaster>
         * @throws Exception
         */
        public List<TblComplaintMaster> searchPendingComplaintDetails(String query) throws Exception;

        /**
         * this method search the processed complaints
         * @param query
         * @return List<TblComplaintMaster>
         * @throws Exception
         */
        public List<TblComplaintMaster> searchProcessedComplaintDetails(String query) throws Exception;

    /**
     * this method search pending complaint for DG-CPTU
     * @param i
     * @return List<TblComplaintMaster>
     * @throws Exception
     */
    public List<TblComplaintMaster> searchPendingComplaintDetailsForDGCPTU(int i) throws Exception;

    /**
     * this method search pending complaint for Review Panels
     * @param i
     * @return
     * @throws Exception
     */
    public List<TblComplaintMaster> searchPendingComplaintDetailsRP(int i) throws Exception;

    /**
     * this method search processed complaint for Review Panel
     * @param i
     * @return List<TblComplaintMaster>
     * @throws Exception
     */
    public List<TblComplaintMaster> searchProcessedComplaintDetailsRP(int i) throws Exception;

    /**
     * this method search Payments details
     * @param feetype
     * @param id
     * @param compId
     * @return TblComplaintPayments
     * @throws Exception
     */
    public TblComplaintPayments getPaymentDetails(String feetype, Integer id, Integer compId) throws Exception;

    /**
     * get GovUSerId for dg-cptu
     * @return int
     * @throws Exception
     */
    public int getGovUserId() throws Exception;


}