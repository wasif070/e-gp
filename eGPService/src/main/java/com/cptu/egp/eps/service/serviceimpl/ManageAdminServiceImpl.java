/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAdminMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeAdminDao;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.service.serviceinterface.ManageAdminService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class ManageAdminServiceImpl implements ManageAdminService {

    final static Logger LOGGER = Logger.getLogger(ManageAdminServiceImpl.class);
    TblAdminMasterDao tblAdminMasterDao;
    TblOfficeAdminDao tblOfficeAdminDao;
    HibernateQueryDao hibernateQueryDao;
    private SPAddUser sPAddUser;
    private String logUserId = "0";
    private String starts = " Starts";
    private String ends = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblOfficeAdminDao getTblOfficeAdminDao() {
        return tblOfficeAdminDao;
    }

    public void setTblOfficeAdminDao(TblOfficeAdminDao tblOfficeAdminDao) {
        this.tblOfficeAdminDao = tblOfficeAdminDao;
    }

    public TblAdminMasterDao getTblAdminMasterDao() {
        return tblAdminMasterDao;
    }

    public void setTblAdminMasterDao(TblAdminMasterDao tblAdminMasterDao) {
        this.tblAdminMasterDao = tblAdminMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public List<Object[]> getManageAdminDetail(int userId, byte userTypeid) {
        LOGGER.debug("getManageAdminDetail : " + logUserId + starts);
        List<Object[]> list = null;
        String action = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tdm.departmentName,tam.adminId,tam.phoneNo from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm,TblDepartmentMaster tdm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = tdm.approvingAuthorityId and tam.tblLoginMaster.userId = tdm.approvingAuthorityId and tlm.userId = " + userId + " and tlm.tblUserTypeMaster.userTypeId=" + userTypeid);
            action = "View Profile";
        } catch (Exception e) {
            LOGGER.error("getManageAdminDetail : " + logUserId + e);
            action = "Error in View Profile";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), action, "");
            action = null;
        }
        LOGGER.debug("getManageAdminDetail : " + logUserId + ends);
        return list;
    }

    @Override
    public boolean updateAdminMaster(TblAdminMaster tblAdminMaster,String... actionAudit) {
        LOGGER.debug("updateAdminMaster : " + logUserId + starts);
        boolean flag = false;
        String action = null;
        String moduleName = null;
        try {
            tblAdminMasterDao.updateTblAdminMaster(tblAdminMaster);
            flag = true;
            if(actionAudit != null && actionAudit.length != 0){
                action = actionAudit[0];
                moduleName = actionAudit[1];
            }
        } catch (Exception e) {
            LOGGER.error("updateAdminMaster : " + logUserId + e);
            if(actionAudit != null && actionAudit.length != 0){
                action = "Error in " + actionAudit[0];
            }
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblAdminMaster.getTblLoginMaster().getUserId(), "userId", moduleName, action, "");
            action = null;
        }
        LOGGER.debug("updateAdminMaster : " + logUserId + ends);
        return flag;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public List<Object> getManagePEAdminDetailOfficeList(int userId, byte userTypeid) {
        LOGGER.debug("getManagePEAdminDetailOfficeList : " + logUserId + starts);
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tom.officeName from TblOfficeAdmin toa ,TblLoginMaster tlm,TblOfficeMaster tom where toa.tblLoginMaster.userId = tlm.userId and toa.tblOfficeMaster.officeId = tom.officeId and tlm.userId = " + userId + " and tlm.tblUserTypeMaster.userTypeId = " + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getManagePEAdminDetailOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getManagePEAdminDetailOfficeList : " + logUserId + ends);
        return list;
    }

    @Override
    public List<Object[]> getManagePEAdminDetail(int userId, byte userTypeid) {
        LOGGER.debug("getManagePEAdminDetail : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tlm.userId,tlm.emailId,tam.fullName,tam.adminId,tam.nationalId, tam.mobileNo,tam.phoneNo,om.officeId,om.officeName,om.tblStateMaster.stateId,sm.stateName from TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin o,TblOfficeMaster om,TblStateMaster sm where tlm.userId = tam.tblLoginMaster.userId and sm.stateId=om.tblStateMaster.stateId and o.tblLoginMaster.userId=tam.tblLoginMaster.userId and o.tblOfficeMaster.officeId=om.officeId and tlm.userId = " + userId + " and tlm.tblUserTypeMaster.userTypeId =" + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getManagePEAdminDetail : " + logUserId + e);
        }
        LOGGER.debug("getManagePEAdminDetail : " + logUserId + ends);
        return list;

    }

    @Override
    public boolean updatePEAdminMaster(/*TblAdminMaster tblAdminMaster*/String query, int userID,String... actionAudit) {
        LOGGER.debug("updatePEAdminMaster : " + logUserId + starts);
        boolean flag = false;
        String action = null;
        String moduleName = null;
        try {
//            tblAdminMasterDao.updateTblAdminMaster(tblAdminMaster);
            
            CommonMsgChk commonMsgChk = null;//createGovtUserRole
            commonMsgChk = getsPAddUser().executeProcedure(0, "updatePAAdmin", query).get(0);
            if (commonMsgChk.getFlag() == true) {
                flag = true;
//                count = Integer.parseInt(commonMsgChk.getMsg());
            }
            if(actionAudit != null && actionAudit.length != 0){
                action = actionAudit[0];
                moduleName = actionAudit[1];
            }
        } catch (Exception e) {
            if(actionAudit != null && actionAudit.length != 0){
                action = "Error in " + actionAudit[0];
            }
            LOGGER.error("updatePEAdminMaster : " + logUserId + e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userID, "userId", moduleName, action, "");
            action = null;
        }
        LOGGER.debug("updatePEAdminMaster : " + logUserId + ends);
        return flag;
        //tblOfficeAdminDao.addTblOfficeAdmin(tblOfficeAdmin);
    }

    @Override
    public List<Object[]> getManageAdminContentDetail(int userId, byte userTypeid) {
        LOGGER.debug("getManageAdminContentDetail : " + logUserId + starts);
        List<Object[]> list = null;
        String action = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tam.adminId,tam.phoneNo,tam.rollId from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = " + userId + " and tlm.tblUserTypeMaster.userTypeId=" + userTypeid);
            action = "View Profile";
        } catch (Exception e) {
            LOGGER.error("getManageAdminContentDetail : " + logUserId + e);
            action = "Error in View Profile";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), action, "");
            action = null;
        }
        LOGGER.debug("getManageAdminContentDetail : " + logUserId + ends);
        return list;

    }

    @Override
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause) {
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tdm.departmentName,tam.adminId,tam.phoneNo,tlm.status from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm,TblDepartmentMaster tdm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = tdm.approvingAuthorityId and tam.tblLoginMaster.userId = tdm.approvingAuthorityId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + "order by " + orderClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + ends);
        return list;

    }

    @Override
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            } else if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            }
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tdm.departmentName,tam.adminId,tam.phoneNo from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm,TblDepartmentMaster tdm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = tdm.approvingAuthorityId and tam.tblLoginMaster.userId = tdm.approvingAuthorityId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + " and " + searchField + " LIKE '" + searchString + "' order by tlm.registeredDate desc", firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, int userId, String sortString) {
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            if (sortString.equalsIgnoreCase("om.officeName asc")) {
                sortString = "tom.officeName asc";
            } else if (sortString.equalsIgnoreCase("om.officeName desc")) {
                sortString = "tom.officeName desc";
            }
            //Edit query for find the AdminId, Edited by palash for PE Admin Transfer, Dohatec
            sb.append("select tlm.userId,tlm.emailId,tam.fullName,tom.officeName,tam.adminId ");
            sb.append("from TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin toa,TblOfficeMaster tom ");
            sb.append("where tlm.userId = tam.tblLoginMaster.userId and tam.tblLoginMaster.userId=toa.tblLoginMaster.userId and ");
            sb.append("toa.tblOfficeMaster.officeId in ( select om.officeId from TblDepartmentMaster tdm,TblOfficeMaster om where tdm.approvingAuthorityId=" + userId + " and tdm.departmentId=om.departmentId) ");
            sb.append("and tlm.tblUserTypeMaster.userTypeId = " + userTypeid + " and toa.tblOfficeMaster.officeId=tom.officeId order by " + sortString);
            list = hibernateQueryDao.createByCountNewQuery(sb.toString(), firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public List<Object[]> getOrganizationAdminGrid(int firstResult, int maxResult, byte userTypeid, int userId, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            if (searchField.equalsIgnoreCase("om.officeName")) {
                searchField = "tom.officeName";
            }
            StringBuilder sb = new StringBuilder();
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            }
            sb.append("select tlm.userId,tlm.emailId,tam.fullName,tom.officeName ");
            sb.append("from TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin toa,TblOfficeMaster tom ");
            sb.append("where tlm.userId = tam.tblLoginMaster.userId and tam.tblLoginMaster.userId=toa.tblLoginMaster.userId and ");
            sb.append("toa.tblOfficeMaster.officeId in ( select om.officeId from TblDepartmentMaster tdm,TblOfficeMaster om where tdm.approvingAuthorityId=" + userId + " and tdm.departmentId=om.departmentId) ");
            sb.append("and tlm.tblUserTypeMaster.userTypeId = " + userTypeid + " and toa.tblOfficeMaster.officeId=tom.officeId  and " + searchField + " LIKE '" + searchString + "' order by tlm.registeredDate desc");
            list = hibernateQueryDao.createByCountNewQuery(sb.toString(), firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public long getAllCountOfOrganization(byte userTypeid) {
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm,TblDepartmentMaster tdm", "tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = tdm.approvingAuthorityId and tam.tblLoginMaster.userId = tdm.approvingAuthorityId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getAllCountOfOrganization : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + ends);
        return lng;
    }

    @Override
    public long getSearchCountOfOrganization(byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfOrganization : " + logUserId + starts);
        long lng = 0;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            } else if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            }
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm,TblDepartmentMaster tdm", "tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.userId = tdm.approvingAuthorityId and tam.tblLoginMaster.userId = tdm.approvingAuthorityId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + " and " + searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfOrganization : " + logUserId + e);
        }
        LOGGER.debug("getSearchCountOfOrganization : " + logUserId + ends);
        return lng;
    }

    @Override
    public long getAllCountOfOrganization(byte userTypeid, int userId) {
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin toa", "where tlm.userId = tam.tblLoginMaster.userId and tam.tblLoginMaster.userId=toa.tblLoginMaster.userId and toa.tblOfficeMaster.officeId in ( select om.officeId from TblDepartmentMaster tdm,TblOfficeMaster om where tdm.approvingAuthorityId" + userId + " and tdm.departmentId=om.departmentId) and tlm.tblUserTypeMaster.userTypeId = " + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getAllCountOfOrganization : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + ends);
        return lng;
    }

    @Override
    public List<Object[]> getContentAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause) {//rishita
        LOGGER.debug("getContentAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tam.adminId,tam.phoneNo,tlm.status from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + "order by " + orderClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getContentAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getContentAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public List<Object[]> getContentAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getContentAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            }
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tam.adminId,tam.phoneNo from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + "  and " + searchField + " LIKE '" + searchString + "' order by tlm.registeredDate desc", firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getContentAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getContentAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public long getAllCountOfContent(byte userTypeid) {
        LOGGER.debug("getAllCountOfContent : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm", "tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getAllCountOfContent : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfContent : " + logUserId + ends);
        return lng;
    }

    @Override
    public long getSearchCountOfContent(byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfContent : " + logUserId + starts);
        long lng = 0;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            } else if (searchField.equalsIgnoreCase("departmentName")) {
                searchField = "tdm." + searchField;
            }
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm", "tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + "and " + searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfContent : " + logUserId + e);
        }
        LOGGER.debug("getSearchCountOfContent : " + logUserId + ends);
        return lng;
    }

    @Override
    public List<Object[]> getPEAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause) {//rishita - sorting
        LOGGER.debug("getPEAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tlm.emailId,tam.fullName,tam.adminId,tam.nationalId, tam.mobileNo,tam.phoneNo,om.officeName,tlm.status from TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om where tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId =" + userTypeid + "order by " + orderClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getPEAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getPEAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public List<Object[]> getPEAdminGrid(int firstResult, int maxResult, byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getPEAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            }
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tlm.emailId,tam.fullName,tam.adminId,tam.nationalId, tam.mobileNo,tam.phoneNo,om.officeName from TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om where tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId =" + userTypeid + " and " + searchField + " LIKE '" + searchString + "' order by tlm.registeredDate desc", firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getPEAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getPEAdminGrid : " + logUserId + ends);
        return list;
    }

    @Override
    public long getAllCountOfPE(byte userTypeid) {
        LOGGER.debug("getAllCountOfPE : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om", "tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId =" + userTypeid);
        } catch (Exception e) {
            LOGGER.error("getAllCountOfPE : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfPE : " + logUserId + ends);
        return lng;
    }

    @Override
    public long getSearchCountOfPE(byte userTypeid, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfPE : " + logUserId + starts);
        long lng = 0;
        try {
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "%" + searchString + "%";
            }
            if (searchField.equalsIgnoreCase("fullName")) {
                searchField = "tam." + searchField;
            } else if (searchField.equalsIgnoreCase("emailId")) {
                searchField = "tlm." + searchField;
            }
            lng = hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om", "tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId =" + userTypeid + " and " + searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfPE : " + logUserId + e);
        }
        LOGGER.debug("getSearchCountOfPE : " + logUserId + ends);
        return lng;
        //return hibernateQueryDao.createByCountNewQuery("TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin oa,TblOfficeMaster om where tlm.userId = tam.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and oa.tblLoginMaster.userId = tlm.userId and tlm.tblUserTypeMaster.userTypeId =" + userTypeid + " and " + searchField + " LIKE '" + searchString + "' order by tlm.registeredDate desc", firstResult, maxResult);
    }

    @Override
    public int updateDeleteNewQuery(String query) {
        LOGGER.debug("updateDeleteNewQuery : " + logUserId + starts);
        int i = 0;
        try {
            i = hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception e) {
            LOGGER.error("updateDeleteNewQuery : " + logUserId + e);
        }
        LOGGER.debug("updateDeleteNewQuery : " + logUserId + ends);
        return i;
    }

    @Override
    public boolean updateOfficeList(TblOfficeAdmin tblOfficeAdmin) {
        boolean flag = false;
        LOGGER.debug("updateOfficeList : " + logUserId + starts);
        try {
            tblOfficeAdminDao.addTblOfficeAdmin(tblOfficeAdmin);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("updateOfficeList : " + logUserId + e);
        }
        LOGGER.debug("updateOfficeList : " + logUserId + ends);
        return flag;
    }

    @Override
    public List<Object[]> getOfficeList(int userId) {
        //return hibernateQueryDao.createNewQuery("select tom.officeName,tom.officeId,tom.departmentId from TblOfficeMaster tom,TblOfficeAdmin toa where toa.tblOfficeMaster.officeId = tom.officeId and toa.tblLoginMaster.userId="+userId);
        LOGGER.debug("getOfficeList : " + logUserId + starts);
        List<Object[]> list = null;
        String action = null;
        try {
            list = hibernateQueryDao.createNewQuery("SELECT om.officeName,dm.departmentName,oa.tblOfficeMaster.officeId,dm.departmentId  from TblAdminMaster am, TblOfficeAdmin oa,TblOfficeMaster om,TblDepartmentMaster dm where am.tblLoginMaster.userId = oa.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and om.departmentId = dm.departmentId and  am.tblLoginMaster.userId = " + userId);
            action = "View Profile";
        } catch (Exception e) {
            LOGGER.error("getOfficeList : " + logUserId + e);
            action = "Error in View Profile";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), action, "");
        }
        LOGGER.debug("getOfficeList : " + logUserId + ends);
        return list;

    }

    @Override
    public List<Object[]> getOfficeAllList(int userId) {
        //return hibernateQueryDao.createNewQuery("select tom.officeName,tom.officeId,tdm.departmentName from TblOfficeMaster tom,TblDepartmentMaster tdm where tom.departmentId = tdm.departmentId and tom.departmentId = 19 order by tom.officeName");
        LOGGER.debug("getOfficeAllList : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tom.officeId,tom.officeName from TblOfficeMaster tom where tom.departmentId in ( select dm.departmentId from  TblAdminMaster am,TblOfficeAdmin oa,TblOfficeMaster om,TblDepartmentMaster dm where am.tblLoginMaster.userId = oa.tblLoginMaster.userId and oa.tblOfficeMaster.officeId = om.officeId and om.departmentId = dm.departmentId and am.tblLoginMaster.userId = " + userId + ")");
        } catch (Exception e) {
            LOGGER.error("getOfficeAllList : " + logUserId + e);
        }
        LOGGER.debug("getOfficeAllList : " + logUserId + ends);
        return list;
    }

    @Override
    public long getOrganizationAdminGridCount(String userId, String userTypeId) {
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery(" TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin toa,TblOfficeMaster tom", " tlm.userId = tam.tblLoginMaster.userId and tam.tblLoginMaster.userId=toa.tblLoginMaster.userId and toa.tblOfficeMaster.officeId in ( select om.officeId from TblDepartmentMaster tdm,TblOfficeMaster om where tdm.approvingAuthorityId=" + userId + " and tdm.departmentId=om.departmentId) and tlm.tblUserTypeMaster.userTypeId = " + userTypeId + " and toa.tblOfficeMaster.officeId=tom.officeId");
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGridCount : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long getOrganizationAdminGridCount(String userId, String userTypeId, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery(" TblLoginMaster tlm,TblAdminMaster tam,TblOfficeAdmin toa,TblOfficeMaster tom", " tlm.userId = tam.tblLoginMaster.userId and tam.tblLoginMaster.userId=toa.tblLoginMaster.userId and toa.tblOfficeMaster.officeId in ( select om.officeId from TblDepartmentMaster tdm,TblOfficeMaster om where tdm.approvingAuthorityId=" + userId + " and tdm.departmentId=om.departmentId) and tlm.tblUserTypeMaster.userTypeId = " + userTypeId + " and toa.tblOfficeMaster.officeId=tom.officeId  and " + searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGridCount : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

     @Override
    public List<Object[]> getOAndMAdminGrid(int firstResult, int maxResult, byte userTypeid, String orderClause) {
        LOGGER.debug("getOAndMAdminGrid : " + logUserId + starts);
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createByCountNewQuery("select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId,tam.fullName,tutm.userTypeId,tlm.emailId,tam.adminId,tam.phoneNo,tlm.status,tam.rollId from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm where tlm.userId = tam.tblLoginMaster.userId and tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId and tlm.tblUserTypeMaster.userTypeId=" + userTypeid + "order by " + orderClause, firstResult, maxResult);
        } catch (Exception e) {
            LOGGER.error("getOAndMAdminGrid : " + logUserId + e);
        }
        LOGGER.debug("getOAndMAdminGrid : " + logUserId + ends);
        return list;
    }

    /**
     * @return the sPAddUser
     */
    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    /**
     * @param sPAddUser the sPAddUser to set
     */
    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }
}
