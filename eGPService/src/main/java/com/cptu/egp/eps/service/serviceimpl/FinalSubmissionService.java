/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPGenerateMegaHash;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Kinjal Shah
 */
public class FinalSubmissionService {

    final Logger logger = Logger.getLogger(FinalSubmissionService.class);
    SPGenerateMegaHash sPGenerateMegaHash;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId ="0";
    
    public SPGenerateMegaHash getsPGenerateMegaHash() {
        return sPGenerateMegaHash;
    }

    public void setsPGenerateMegaHash(SPGenerateMegaHash sPGenerateMegaHash) {
        this.sPGenerateMegaHash = sPGenerateMegaHash;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
     
    /**
     * Get Mega Hash for the Bidder for the Tender
     * @param tenderId from tbl_TenderMaster
     * @param userId bidder's userId
     * @return List of CommonSPReturn
     */
    public List<CommonSPReturn> generateMegaHash(int tenderId, int userId)
    {
        logger.debug("generateMegaHash : "+logUserId+" Starts");
        List<CommonSPReturn> cmr = null;
        try{
            cmr = sPGenerateMegaHash.executeProcedure(1, tenderId, userId);
            if(cmr.get(0).getFlag()){
                cmr = sPGenerateMegaHash.executeProcedure(2, tenderId, userId);        
        }else{
                cmr = null;
                cmr = new ArrayList<CommonSPReturn>();
                CommonSPReturn csr = new CommonSPReturn();
                csr.setId(BigDecimal.ZERO);
                csr.setFlag(Boolean.FALSE);
                csr.setMsg("Mega hash Failed for 1st Step tenderId "+tenderId+" userId "+ userId);
                cmr.add(csr);
                logger.error("generateMegaHash : Mega hash Failed for 1st Step tenderId "+tenderId+" userId "+ userId);
        }
        }catch(Exception ex){
            logger.error("generateMegaHash : tenderId "+tenderId+" userId "+logUserId+" : "+ex.toString());
            cmr = null;
            cmr = new ArrayList<CommonSPReturn>();
            CommonSPReturn csr = new CommonSPReturn();
            csr.setId(BigDecimal.ZERO);
            csr.setFlag(Boolean.FALSE);
            csr.setMsg(ex.toString());
            cmr.add(csr);
        }
        logger.debug("generateMegaHash : "+logUserId+" Ends");
            return cmr;
    }

    /**
     *Get Mega Hash for Tender Withdrawal by bidder
     * @param tenderId from tbl_TenderMaster
     * @param userId bidder who withdraws tender
     * @return List of CommonSPReturn
     */
    public List<CommonSPReturn> generateMegaHashWithdrwal(int tenderId, int userId)
    {
        logger.debug("generateMegaHashWithdrwal : "+logUserId+" Starts");
        List<CommonSPReturn> cmr = null;
        try{
            cmr = sPGenerateMegaHash.executeProcedure(2, tenderId, userId);
        }catch(Exception ex){
            logger.error("generateMegaHashWithdrwal : "+logUserId+" : "+ex.toString());
    }
        logger.debug("generateMegaHashWithdrwal : "+logUserId+" Ends");
        return cmr;
    }

    /**
     *Check whether the tender is closed
     * @param tenderId from tbl_TenderMaster
     * @return true or false for closed or open
     */
    public Boolean chkTenderClosed(int tenderId)
    {
        logger.debug("chkTenderClosed : "+logUserId+" Starts");
        boolean flag = false;
        try{
        String query = "select Case When td.submissionDt > GETDATE() Then '1' Else '0' End as retVal from TblTenderDetails td where td.tblTenderMaster.tenderId = "+tenderId;
        String str = (String)hibernateQueryDao.getSingleColQuery(query).get(0);
            if("0".equalsIgnoreCase(str)){
                flag = true;
        }
        }catch(Exception ex){
            logger.error("chkTenderClosed : "+logUserId+" : "+ex.toString());
    }
        logger.debug("chkTenderClosed : "+logUserId+" Ends");
        return flag;
    }
}
