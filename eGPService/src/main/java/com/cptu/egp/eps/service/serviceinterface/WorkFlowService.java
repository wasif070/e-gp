/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface WorkFlowService {

    /**
     * get All work flow Data
     * @return List of Work flow Data
     */
    public List getWorkFlowData();

    /**
     * get All Procurement Roles
     * @return List of Procurement Roles
     */
    public List getProcurementRoles();

    /**
     * Set user Id at Service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
    
    /**
     * Get Work flow level users
     * @param eventId
     * @param wfLevel
     * @param actId
     * @param objId
     * @return
     */
    public int getWorkFlowLevelUser(short eventId,short wfLevel,short actId, int objId);

    /**
     * Set Audit Trail Object at Service layer.
     * @param Audittrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
    
    /**
     * checks workflow rule engine data
     * @param eventId
     * @return boolean, return true - if data is exist otherwise false
     */
    public boolean getWorkFlowRuleEngineData(int eventId);
    
}
