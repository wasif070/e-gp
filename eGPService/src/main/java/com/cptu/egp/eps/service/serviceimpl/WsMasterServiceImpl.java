/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblWsMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblWsMaster;
import com.cptu.egp.eps.service.serviceinterface.WsMasterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This service class used Database operations for TblWsMaster
 * @author Sreenu
 */
public class WsMasterServiceImpl implements WsMasterService{

    final Logger logger = Logger.getLogger(WsOrgMasterServiceImpl.class);
    private HibernateQueryDao hibernateQueryDao;
    private TblWsMasterDao tblWsMasterDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblWsMasterDao getTblWsMasterDao() {
        return tblWsMasterDao;
    }

    public void setTblWsMasterDao(TblWsMasterDao tblWsMasterDao) {
        this.tblWsMasterDao = tblWsMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblWsMaster object in database
     * @param TblWsMaster tblWsMaster
     * @return int 
     */
    @Override
    public int insertWsMaster(TblWsMaster tblWsMaster) {
        logger.debug("insertWsMaster : "+logUserId+STARTS);
        int flag = 0;
        String action = null;
        try{
            tblWsMasterDao.addTblWsMaster(tblWsMaster);
            flag = tblWsMaster.getWsId();
            action = "Configure Web Service Configuration";
        }catch(Exception ex){
            logger.error("insertWsMaster : "+logUserId+" : "+ex);
            flag = 0;
            action = "Error in Configure Web Service Configuration : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }
        logger.debug("insertWsMaster : "+logUserId+ENDS);
        return flag;
    }

   /***
     * This method updates an TblWsMaster object in DB
     * @param TblWsMaster tblWsMaster
     * @return boolean
     */
    @Override
    public boolean updateWsMaster(TblWsMaster tblWsMaster,String...actions) {
        logger.debug("updateWsMaster : " + logUserId + STARTS);
        boolean flag;
        String action = null;
        try {
            tblWsMasterDao.updateTblWsMaster(tblWsMaster);
            flag = true;
            action = (actions.length!=0 ? actions[0]: "") +" Web Service"+(actions.length!=0 && actions[0].equalsIgnoreCase("edit")? " Configuration":"");
        } catch (Exception ex) {
            logger.error("updateWsMaster : " + logUserId + " : " + ex);
            flag = false;
            action = "Error in "+((actions.length!=0 ? actions[0]: "" )+" Web Service"+(actions.length!=0 && actions[0].equalsIgnoreCase("edit")? " Configuration":""))+" : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action=null;
        }
        logger.debug("updateWsMaster : " + logUserId + ENDS);
        return flag;
    }

     /***
     * This method deletes an TblWsMaster object in DB
     * @param TblWsMaster tblWsMaster
     * @return boolean
     */
    @Override
    public boolean deleteWsMaster(TblWsMaster tblWsMaster) {
        logger.debug("deleteWsMaster : " + logUserId + STARTS);
        boolean flag;
        try {
            tblWsMasterDao.deleteTblWsMaster(tblWsMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteWsMaster : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteWsMaster : " + logUserId + ENDS);
        return flag;
    }

    /***
     * This method returns all TblWsMaster objects from database
     * @return List<TblWsMaster>
     */
    @Override
    public List<TblWsMaster> getAllWsMaster() {
        logger.debug("getAllWsMaster : " + logUserId + STARTS);
        List<TblWsMaster> wsMastersList = null;
        try {
            wsMastersList = tblWsMasterDao.getAllTblWsMaster();
        } catch (Exception ex) {
            logger.error("getAllWsMaster : " + logUserId + " : " + ex);
        }
        logger.debug("getAllWsMaster : " + logUserId + ENDS);
        return wsMastersList;
    }

    /***
     * This method returns no. of TblWsMaster objects from database
     * @return long
     */
    @Override
    public long getWsMastersCount() {
        logger.debug("getWsMastersCount : " + logUserId + STARTS);
        long wsMasterCount = 0;
        try {
            wsMasterCount = tblWsMasterDao.getTblWsMasterCount();
        } catch (Exception ex) {
            logger.error("getWsMastersCount : " + logUserId + " : " + ex);
        }
        logger.debug("getWsMastersCount : " + logUserId + ENDS);
        return wsMasterCount;
    }

    /***
     * This method returns TblWsMaster for the given Id
     * @param int id
     * @return TblWsMaster
     */
    @Override
    public TblWsMaster getTblWsMaster(int id) {
        logger.debug("getTblWsMaster(int): " + logUserId + STARTS);
        List<TblWsMaster> tblWsMasterList = new ArrayList<TblWsMaster>();
        TblWsMaster tblWsMaster = null;
        try {
            tblWsMasterList = tblWsMasterDao.findEntity("wsId",
                    Operation_enum.EQ, id);
        } catch (Exception ex) {
            logger.error("getTblWsMaster(int): " + logUserId + " : " + ex);
        }
        if(!tblWsMasterList.isEmpty()){
            tblWsMaster = tblWsMasterList.get(0);
        }
        logger.debug("getTblWsMaster(int) : " + logUserId + ENDS);
        return tblWsMaster;
    }

    /***
     * This method returns TblWsOrgMaster for the given webServiceName
     * @param String webServiceName
     * @return TblWsMaster
     */
    @Override
    public TblWsMaster getTblWsMaster(String webServieName) {
        logger.debug("getTblWsMaster(String): " + logUserId + STARTS);
        List<TblWsMaster> tblWsMasterList = new ArrayList<TblWsMaster>();
        TblWsMaster tblWsMaster = null;
        try {
            tblWsMasterList = tblWsMasterDao.findEntity("wsName",
                    Operation_enum.EQ, webServieName);
        } catch (Exception ex) {
           logger.error("getTblWsMaster(String): " + logUserId + " : " + ex);
        }
        if(!tblWsMasterList.isEmpty()){
            tblWsMaster = tblWsMasterList.get(0);
        }
        logger.debug("getTblWsMaster(String) : " + logUserId + ENDS);
        return tblWsMaster;
    }

    /***
     * This method gets the required objects from the database.
     * The object name used in the condition is <b>wsMaster</b>.
     * Just pass the query condition to this method.
     * <b>Need to append the WHERE keyword to the above condition</b>
     * @param String whereCondition
     * @return List<TblWsMaster>
     */
    @Override
    public List<TblWsMaster> getRequiredTblWsMasterList(String whereCondition) {
        logger.debug("getRequiredTblWsMasterList : " + logUserId + STARTS);
        List<TblWsMaster> tblWsMasterList = new ArrayList<TblWsMaster>();
        try {
            tblWsMasterList = tblWsMasterDao.getRequiredTblWsMasterList(whereCondition);
        } catch (Exception ex) {
           logger.error("getRequiredTblWsMasterList : " + logUserId + " : " + ex);
        }        
        logger.debug("getRequiredTblWsMasterList : " + logUserId + ENDS);
        return tblWsMasterList;
    }    

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
}
