/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblMediaContentDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblMediaContent;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class MediaContentService {

    final Logger logger = Logger.getLogger(MediaContentService.class);
    
    MakeAuditTrailService makeAuditTrailService;
    TblMediaContentDao tblMediaContentDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblMediaContentDao getTblMediaContentDao() {
        return tblMediaContentDao;
    }

    public void setTblMediaContentDao(TblMediaContentDao tblMediaContentDao) {
        this.tblMediaContentDao = tblMediaContentDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    /**
     * fetching info from Tbl_MediaContent
     * @return list of TblMediaContent
     */
    public List<TblMediaContent> getAllMediaContent() {
        logger.debug("getAllMediaContent : "+logUserId+" Starts");
        List<TblMediaContent> list = null;
        try{
        list = tblMediaContentDao.getAllTblMediaContent();
        }catch(Exception e)
        {
            logger.error("getAllMediaContent : "+logUserId+"  : "+e);
    }
        logger.debug("getAllMediaContent : "+logUserId+" Ends");
        return list;
    }

    /**
     * inserting values in tbl_MediaContent
     * @param tblMediaContent
     * @return true if data inserted successfully else false
     */
    public boolean saveMediaContent(TblMediaContent tblMediaContent) {
        logger.debug("saveMediaContent : "+logUserId+" Starts");
        boolean flag;
        String action = "";
        try {
            tblMediaContentDao.addTblMediaContent(tblMediaContent);
            flag = true;
            action = "Post "+tblMediaContent.getContentType()+" Release";
        } catch (Exception e) {
            logger.error("saveMediaContent : "+logUserId+" : "+e);
            action = "Error in "+action+" "+e.getMessage();
            flag = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("saveMediaContent : "+logUserId+" Ends");
        return flag;
    }
    /**
     *  fetching info from Tbl_MediaContent
     * @param strMediaId
     * @return list of TblMediaContent
     */
    public List<TblMediaContent> getMediaDetail(String strMediaId){
        logger.debug("getMediaDetail : "+logUserId+" Starts");
        String action = "";
        List<TblMediaContent> list = null;
        try {
            list = tblMediaContentDao.findTblMediaContent("mediaId",Operation_enum.EQ,Integer.parseInt(strMediaId));
            if("Complaint".equalsIgnoreCase(list.get(0).getContentType())){
                action = "View Complaint Detail";
            }else if("Press".equalsIgnoreCase(list.get(0).getContentType())){
                action = "View Press Release Detail";
            }else{
                action = "View Other Information Detail";
            }
            action = "View "+list.get(0).getContentType()+" Release";
        } catch (Exception e) {
            logger.error("getMediaDetail : "+logUserId+" : "+e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("getMediaDetail : "+logUserId+" Ends");
        return list;
    }
}
