/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.model.table.TblEvalNomination;
import com.cptu.egp.eps.service.serviceinterface.EvalNominationApprovalService;
import com.cptu.egp.eps.dao.daointerface.TblEvalNominationDao;
import org.apache.log4j.Logger;
/**
 *
 * @author Administrator
 */
public class EvalNominationApprovalServiceImpl implements EvalNominationApprovalService{
    String logUserId = "0";
    final Logger logger = Logger.getLogger(EvalNominationApprovalServiceImpl.class);
    public TblEvalNominationDao getTblEvalNominationDao() {
        return tblEvalNominationDao;
    }

    public void setTblEvalNominationDao(TblEvalNominationDao tblEvalNominationDao) {
        this.tblEvalNominationDao = tblEvalNominationDao;
    }

    TblEvalNominationDao tblEvalNominationDao;

    @Override
    public String addTblEvalNomination(TblEvalNomination tblEvalNomination) {
        logger.debug("addTblEvalNomination : "+logUserId+" Starts");
        String val =  null;
       try {
            getTblEvalNominationDao().addTblEvalNomination(tblEvalNomination);
            val =  "Values Added";
        }
        catch (Exception e) {
            logger.error("addTblEvalNomination : "+logUserId+" Starts"+e);
            val = "Not added";
        }
        logger.debug("addTblEvalNomination : "+logUserId+" Ends");
        return val;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

}
