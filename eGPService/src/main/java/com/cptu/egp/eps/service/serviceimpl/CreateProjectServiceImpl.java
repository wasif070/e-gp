/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblProjectFinPowerImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementMethodDao;
import com.cptu.egp.eps.dao.daointerface.TblProjectMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblProjectRolesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.AppCommonData;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.ProjectMaster;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectFPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblProjectMaster;
import com.cptu.egp.eps.model.table.TblProjectRoles;
import com.cptu.egp.eps.service.serviceinterface.CreateProjectService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class CreateProjectServiceImpl implements CreateProjectService {
    private static final Logger LOGGER = Logger.getLogger(CreateProjectServiceImpl.class);
    private TblDepartmentMasterDao tblDepartmentMasterDao;
    private AppCommonData appCommonData;
    private ProjectMaster projectMaster;

    private BasicDataSource basicDataSource;
    private String getappcommondata;

    private SPXMLCommon pProjectRoles;
    private TblProcurementMethodDao procurementMethodDao;

    private TblProjectMasterDao projectMasterDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblProjectRolesDao tblProjectRolesDao;
    private TblProjectFinPowerImpl tblProjectFinPowerDao;
    private MakeAuditTrailService makeAuditTrailService;
    String logUserId = "0";
    private AuditTrail auditTrail;
//    CommonSearchService commonSearchService;
//
//    public CommonSearchService getCommonSearchService()
//    {
//        return commonSearchService;
//    }
//
//    public void setCommonSearchService(CommonSearchService commonSearchService)
//    {
//        this.commonSearchService = commonSearchService;
//    }
    
    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    public AppCommonData getAppCommonData() {
        return appCommonData;
    }

    public void setAppCommonData(AppCommonData appCommonData) {
        this.appCommonData = appCommonData;
    }

    public ProjectMaster getProjectMaster() {
        return projectMaster;
    }

    public void setProjectMaster(ProjectMaster projectMaster) {
        this.projectMaster = projectMaster;
    }

    public SPXMLCommon getpProjectRoles() {
        return pProjectRoles;
    }

    public void setpProjectRoles(SPXMLCommon pProjectRoles) {
        this.pProjectRoles = pProjectRoles;
    }

    public TblProcurementMethodDao getProcurementMethodDao() {
        return procurementMethodDao;
    }

    public void setProcurementMethodDao(TblProcurementMethodDao procurementMethodDao) {
        this.procurementMethodDao = procurementMethodDao;
    }

    public TblProjectMasterDao getProjectMasterDao() {
        return projectMasterDao;
    }

    public void setProjectMasterDao(TblProjectMasterDao projectMasterDao) {
        this.projectMasterDao = projectMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblProjectRolesDao getTblProjectRolesDao() {
        return tblProjectRolesDao;
    }

    public void setTblProjectRolesDao(TblProjectRolesDao tblProjectRolesDao) {
        this.tblProjectRolesDao = tblProjectRolesDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    

    
    

    @Override
    public void addProjectDetail() {
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get Organization Details.
     * @return List of TblDepartmentMaster Objects.
     */
    @Override
    public List<TblDepartmentMaster> getOrganizationDetail() {
        LOGGER.debug("getOrganizationDetail : "+logUserId+" Starts");
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {            
            Object[] values = {"departmentType", Operation_enum.LIKE, "Organization", "departmentName" , Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        }
        catch (Exception ex) {
            LOGGER.error("getOrganizationDetail : "+logUserId+" :"+ex);
        }
        LOGGER.debug("getOrganizationDetail : "+logUserId+" Ends");
        return deptList;
    }

    /**
     * Get Details From Stored Procedure.
     * @param param1 = Function name for which type of data want to get
     * @param param2 = value for search criteria
     * @param param3 = value for search criteria
     * @return List of CommonAppData
     */
    @Override
    public List<CommonAppData> getDetailsBySP(String param1,String param2,String param3){
        LOGGER.debug("getDetailsBySP : "+logUserId+" Starts");
        List<CommonAppData> list= null;
        try{
        list =  appCommonData.executeProcedure(param1, param2, param3);
        }catch (Exception ex){
            LOGGER.error("getDetailsBySP : "+logUserId+" :"+ex);
    }
        LOGGER.debug("getDetailsBySP : "+logUserId+" Ends");
        return list;
    }

//    @Override
//    public List<SPCommonSearchData>  getDetailsBySP_Search(String param1,String param2,String param3, String param4) throws Exception {
//        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
//        return commonSearchService.searchData(param1,"","", param2, param3, param4,"","","","");
//    }

    /**
     * Used Store Procedure to insert project detail.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy
     * @return List of CommonSPReturn Object
     */
    @Override
    public List<CommonSPReturn> insertProjectDetailsBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,Integer createdBy) {
        LOGGER.debug("insertProjectDetailsBySP : "+logUserId+" Starts");
        String strAuditAction = "";
        List<CommonSPReturn> list  = new ArrayList<CommonSPReturn>();
        try{
        list =  projectMaster.executeProcedure(projectName,projectCode,progId,projectCost,projectStartDate,projectEndDate,
          sourceOfFund,sBankDevelopId,action,projectId,createdBy);
            if("create".equalsIgnoreCase(action)){
                strAuditAction = "Create Project";
                projectId = Integer.parseInt(list.get(0).getId().toString());
            }else{
                strAuditAction = "Edit Project";
            }
        }catch (Exception ex){
            list= null;
            LOGGER.error("insertProjectDetailsBySP : "+logUserId+" :"+ex);
            strAuditAction = "Error in "+strAuditAction+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, projectId, "projectId", EgpModule.Content.getName(), strAuditAction, "");
        }
        LOGGER.debug("insertProjectDetailsBySP : "+logUserId+" Ends");
        return list;
        //throw new UnsupportedOperationException("Not supported yet.");
    }


    /**
     * Get Department details by User Id.
     * @param field1 = Function name for which type of data want to get
     * @param field2 = Search Condition value
     * @param field3 = Search Condition value.
     * @return list of CommonAppData object
     */
    public List<CommonAppData> getDepartmentsByUserid(String field1, String field2,
            String field3) {

        List<CommonAppData> departmentdata = null;
        AppCommonData appCommonData = new AppCommonData(basicDataSource, getappcommondata);
        departmentdata = appCommonData.executeProcedure(field1, field2, field3);
        return departmentdata;

    }

    /**
     * @return the basicDataSource
     */
    public BasicDataSource getBasicDataSource() {
        return basicDataSource;
    }

    /**
     * @param basicDataSource the basicDataSource to set
     */
    public void setBasicDataSource(BasicDataSource basicDataSource) {
        this.basicDataSource = basicDataSource;
    }

    /**
     * @return the getappcommondata
     */
    public String getGetappcommondata() {
        return getappcommondata;
    }

    /**
     * @param getappcommondata the getappcommondata to set
     */
    public void setGetappcommondata(String getappcommondata) {
        this.getappcommondata = getappcommondata;
    }


    /**
     * Used Store Procedure to delete and insert Project detail.
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @return List of CommonMsgChk objects
     */
    @Override
    public List<CommonMsgChk> projectOperationDetailsBySP(String param1,String param2,String param3,String param4){
        LOGGER.debug("projectOperationDetailsBySP : "+logUserId+" Starts");
        List<CommonMsgChk> list;
        try{
        list =  pProjectRoles.executeProcedure(param1, param2, param3,param4);
        }catch (Exception ex){
            list = null;
            LOGGER.error("projectOperationDetailsBySP : "+logUserId+" :"+ex);
    }
        LOGGER.debug("projectOperationDetailsBySP : "+logUserId+" Ends");
        return list;
    }

    /**
     * Getting Project Master Details from Stored Procedure
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy
     * @return List of SPProjectDetailReturn objects.
     */
    @Override
    public List<SPProjectDetailReturn> getProjectMasterBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,Integer createdBy){
        LOGGER.debug("getProjectMasterBySP : "+logUserId+" Starts");
        List<SPProjectDetailReturn> list;
        String strAuditAction = "view Project";
        try{
        list = projectMaster.executeGetProcedure(projectName,projectCode,progId,projectCost,projectStartDate,projectEndDate,
          sourceOfFund,sBankDevelopId,action,projectId,createdBy);
        }catch(Exception ex){
            list= null;
            LOGGER.error("getProjectMasterBySP : "+logUserId+" :"+ex);
            strAuditAction = "Error in "+strAuditAction+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, projectId, "projectId", EgpModule.Content.getName(), strAuditAction, "");
            strAuditAction = null;
        }
        LOGGER.debug("getProjectMasterBySP : "+logUserId+" Ends");
        return list;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get Project Role.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param param
     * @return List of SPProjectDetailReturn objects.
     */
    @Override
    public List<SPProjectRolesReturn> getProjectRolesBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId,int param){
        LOGGER.debug("getProjectRolesBySP : "+logUserId+" Starts");
        List<SPProjectRolesReturn> list;
        try{
            list = projectMaster.executeProjectRolesProcedure(projectName,projectCode,progId,projectCost,projectStartDate,projectEndDate,
          sourceOfFund,sBankDevelopId,action,projectId,0);
        }catch(Exception ex){
            list = null;
             LOGGER.error("getProjectRolesBySP : "+logUserId+" :"+ex);
        }
         LOGGER.debug("getProjectRolesBySP : "+logUserId+" Ends");
        return list;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Get Procurement Method
     * @return List of TblProcurementMethod Objects
     */
    @Override
    public List<TblProcurementMethod> getListProcurementMethod() {
         LOGGER.debug("getListProcurementMethod : "+logUserId+" Starts");
         List<TblProcurementMethod> list;
         try{
                list = procurementMethodDao.getAllTblProcurementMethod();
         }catch(Exception ex){
             list= null;
             LOGGER.error("getListProcurementMethod : "+logUserId+" :"+ex);
    }
         LOGGER.debug("getListProcurementMethod : "+logUserId+" Ends");
         return list;
    }


    /**
     * Get Project details and perform operation on Project Master by Stored Procedure.
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action = Action for operation like Create/Update/Delete/GetProject/GetFinPower/GetPartner/EditPartner,etc.
     * @param projectId
     * @return List of SPProjectFPReturn Objects.
     */
     @Override
    public List<SPProjectFPReturn> getProjectFPBySP(String projectName,String projectCode,Integer progId,BigDecimal projectCost,Date projectStartDate,Date projectEndDate,
         String sourceOfFund,String sBankDevelopId,String action,Integer projectId) {
        LOGGER.debug("getProjectFPBySP : "+logUserId+" Starts");
        List<SPProjectFPReturn> list;
        try{
                    list = projectMaster.executeProjectFPReturns(projectName,projectCode,progId,projectCost,projectStartDate,projectEndDate,
          sourceOfFund,sBankDevelopId,action,projectId);
         }catch(Exception ex){
            list = null;
            LOGGER.error("getProjectFPBySP : "+logUserId+" :"+ex);
    }
        LOGGER.debug("getProjectFPBySP : "+logUserId+" Ends");
        return list;
    }

    /**
     * Update Project Master.
     * @param projectId
     * @return true if successfully update record else return false.
     */
    public boolean projectMasterUpdate(int projectId) {
        LOGGER.debug("projectMasterUpdate : "+logUserId+" Starts");
        boolean flag = false;
        String strAuditAction = "Approve Project";
        try{
            LOGGER.debug("projectMasterUpdate : project id : "+projectId);
            String query = "update TblProjectMaster tpm set tpm.projectStatus = 'approved'   where tpm.projectId ="+projectId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag =  true;
        }catch(Exception e){
            LOGGER.error("projectMasterUpdate : "+logUserId+" :"+e);
            strAuditAction = "Error in "+strAuditAction+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, projectId, "projectId", EgpModule.Content.getName(), strAuditAction, "");
        }
        LOGGER.debug("projectMasterUpdate : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Delete Project Partner
     * @param projectId
     * @return return true if deletion perform successfully else return false.
     */
    public boolean deleteProjectPartners(int projectId){
        LOGGER.debug("deleteProjectPartners : "+logUserId+" Starts");
        boolean flag  = false;
        try {
            String query = " delete from TblProjectPartners tpp where tpp.tblProjectMaster.projectId = "+projectId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("deleteProjectPartners : "+logUserId+" :"+e);
        }
        LOGGER.debug("deleteProjectPartners : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Check if Data available for procurement Role 'PD' or 'PE'
     * @param projectId
     * @param prole values like 'PE' or 'PD'
     * @return true if record exist else return false.
     */
    public boolean isDataAvailableForPDForProjectId(int projectId,String prole) {
       LOGGER.debug("isDataAvailableForPDForProjectId : "+logUserId+" Starts");
       boolean flag=false;
       List<TblProjectRoles> projectRoles=null;
        try {
            if("PD".equalsIgnoreCase(prole)){
                projectRoles = tblProjectRolesDao.findTblProjectRoles("tblProjectMaster",Operation_enum.EQ,new TblProjectMaster(projectId),"tblProcurementRole",Operation_enum.EQ,new TblProcurementRole((byte)19));
            }else if("PE".equalsIgnoreCase(prole)){
                projectRoles = tblProjectRolesDao.findTblProjectRoles("tblProjectMaster",Operation_enum.EQ,new TblProjectMaster(projectId),"tblProcurementRole",Operation_enum.EQ,new TblProcurementRole((byte)1));
            }
            if(projectRoles.size() > 0){
                flag=true;
            }
        } catch (Exception ex) {
            LOGGER.error("isDataAvailableForPDForProjectId : "+logUserId+" :"+ex);
            
        }finally{
            LOGGER.debug("isDataAvailableForPDForProjectId : "+logUserId+" Ends");
            projectRoles = null;
            return flag;
        }
    }

    /**
     * Check if Project data available for given user and procurement type.
     * @param projectId
     * @param userId
     * @param procureId
     * @return true if record exist else return false. re
     */
     public boolean isDataAvailableForProjForUser(int projectId,int userId,int procureId) {
         LOGGER.debug("isDataAvailableForProjForUser : "+logUserId+" Starts");
       boolean flag=false;
       List<TblProjectRoles> projectRoles=null;
        try {
            Object[] obj={"tblProjectMaster",Operation_enum.EQ,new TblProjectMaster(projectId),
                          "tblLoginMaster",Operation_enum.EQ,new TblLoginMaster(userId),
                          "tblProcurementRole",Operation_enum.EQ,new TblProcurementRole((byte)procureId)
                         };
            projectRoles = tblProjectRolesDao.findTblProjectRoles(obj);
            if(projectRoles.size() > 0){
                flag=true;
            }
        } catch (Exception ex) {
            LOGGER.error("isDataAvailableForProjForUser : "+logUserId+" :"+ex);
        }finally{
            LOGGER.debug("isDataAvailableForProjForUser : "+logUserId+" Ends");
            projectRoles = null;
            return flag;
        }
    }

     /**
      * Check Project Role exist
      * @param projectId
      * @return return list of TblPRojectRoles objecs.
      */
     public List<TblProjectRoles> checkIsProjectRoleExist(int projectId){
         LOGGER.debug("checkIsProjectRoleExist : "+logUserId+" Starts");
          List<TblProjectRoles> ProjectRoles = null;
        try {
            ProjectRoles = tblProjectRolesDao.findTblProjectRoles("tblProjectMaster",Operation_enum.EQ,new TblProjectMaster(projectId));
        } catch (Exception ex) {
            LOGGER.error("checkIsProjectRoleExist : "+logUserId+" :"+ex);
        }
          LOGGER.debug("checkIsProjectRoleExist : "+logUserId+" Ends");
         return ProjectRoles;
     }

      public long checkIsProjectExist(int projectId){
          LOGGER.debug("checkIsProjectExist : "+logUserId+" Starts");
          long val = 0;
        try {
            val = projectMasterDao.countForQuery("TblProjectMaster", "projectId=" + projectId);
        } catch (Exception ex) {
            LOGGER.error("checkIsProjectExist : "+logUserId+" :"+ex);
        }
          LOGGER.debug("checkIsProjectExist : "+logUserId+" Ends");
         return val;
     }

    /**
     * @return the tblProjectFinPowerDao
     */
    public TblProjectFinPowerImpl getTblProjectFinPowerDao() {
        return tblProjectFinPowerDao;
    }

    /**
     * @param tblProjectFinPowerDao the tblProjectFinPowerDao to set
     */
    public void setTblProjectFinPowerDao(TblProjectFinPowerImpl tblProjectFinPowerDao) {
        this.tblProjectFinPowerDao = tblProjectFinPowerDao;
    }

    /**
     * Setting user id for logging purpose.
     * @param logUserId
     */
    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * Setting audit trail object for perform audit trail entry.
     * @param auditTrail
     */
    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    
}
