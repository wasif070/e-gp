/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigurationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.ConfigMasterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ConfigMasterServiceImpl implements ConfigMasterService {

    private static final Logger LOGGER = Logger.getLogger(CommonXMLSPServiceImpl.class);
    private HibernateQueryDao hibernateQueryDao;
    private TblConfigurationMasterDao configMasterDao;
    private TblUserTypeMasterDao tblUserTypeMasterDao;
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblUserTypeMasterDao getTblUserTypeMasterDao() {
        return tblUserTypeMasterDao;
    }

    public void setTblUserTypeMasterDao(TblUserTypeMasterDao tblUserTypeMasterDao) {
        this.tblUserTypeMasterDao = tblUserTypeMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblConfigurationMasterDao getConfigMasterDao() {
        return configMasterDao;
    }

    public void setConfigMasterDao(TblConfigurationMasterDao configMasterDao) {
        this.configMasterDao = configMasterDao;
    }

    @Override
    public List<TblConfigurationMaster> getConfigMaster() {
        LOGGER.debug("getConfigMaster : " + logUserId + STARTS);
        List<TblConfigurationMaster> list = null;
        try {
            list = getConfigMasterDao().getAllTblConfigurationMaster();
        } catch (Exception e) {
            LOGGER.error("getConfigMaster : " + logUserId + " " + e);

        }
        LOGGER.debug("getConfigMaster : " + logUserId + ENDS);
        return list;
    }

    @Override
    public String updateConfigMaster(int failLogin, String pkiReq) {
        LOGGER.debug("updateConfigMaster : " + logUserId + STARTS);
        String msg = null;
        try {
            getHibernateQueryDao().updateDeleteNewQuery("update TblConfigurationMaster set failedLoginAttempt=" + failLogin + ", isPkiRequired='" + pkiReq + "'");
            msg = "Values Updated";
        } catch (Exception e) {
            LOGGER.error("updateConfigMaster : " + logUserId + " " + e);

            msg = "Not Updated due to Technical Error";
        }
        LOGGER.debug("updateConfigMaster : " + logUserId + ENDS);

        return msg;
    }

    @Override
    public List<TblConfigurationMaster> getConfigMasterDetails(String usertype) {
        LOGGER.debug("getConfigMasterDetails : " + logUserId + STARTS);
        List<TblConfigurationMaster> list = null;
        try {
            list = getConfigMasterDao().findTblConfigurationMaster("userType", Operation_enum.EQ, usertype);
        } catch (Exception e) {
            LOGGER.error("getConfigMasterDetails : " + logUserId + " " + e);

        }
        LOGGER.debug("getConfigMasterDetails : " + logUserId + ENDS);

        return list;
    }

    @Override
    public String updateConfigMaster(String fileType, String userType, byte singleFile, byte totalFile) {
        LOGGER.debug("updateConfigMaster : " + logUserId + STARTS);
        String msg = null;
        String action = null;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblConfigurationMaster set allowedExtension='" + fileType + "',fileSize=" + singleFile + ",totalSize=" + totalFile + " where userType='" + userType + "'");
            // getConfigMasterDao().updateTblConfigurationMaster(configMaster);
            msg = "Updated";
            action = "Edit Document Size, Types";
        } catch (Exception e) {
            LOGGER.debug("updateConfigMaster : " + logUserId + " " + e);
            action = "Error in Edit Document Size, Types : "+e;
        }finally{
           makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }
        LOGGER.debug("updateConfigMaster : " + logUserId + ENDS);

        return msg;
    }

    @Override
    public List<TblConfigurationMaster> getAllConfigMaster() {
        LOGGER.debug("getAllConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("getAllConfigMaster : " + logUserId + ENDS);
        return getConfigMasterDao().getAllTblConfigurationMaster();
    }

    @Override
    public List<Object> getAllUserType() {
        LOGGER.debug("getAllUserType : " + logUserId + STARTS);
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select userType from TblUserTypeMaster where userType not in (select userType from TblConfigurationMaster where userType not in ('common'))");
        } catch (Exception ex) {
            LOGGER.error("getAllUserType : " + logUserId + " " + ex);

        }
        LOGGER.debug("getAllUserType : " + logUserId + ENDS);

        return list;
    }

    @Override
    public String insertConfigMaster(String fileType, String userType, byte singleFile, byte totalFile) {
        LOGGER.debug("insertConfigMaster : " + logUserId + STARTS);
        String msg = null;
        TblConfigurationMaster tblConfigurationMaster = new TblConfigurationMaster();
        tblConfigurationMaster.setAllowedExtension(fileType);
        tblConfigurationMaster.setFileSize(singleFile);
        tblConfigurationMaster.setTotalSize(totalFile);
        tblConfigurationMaster.setAmount(BigDecimal.valueOf(0));
        tblConfigurationMaster.setFailedLoginAttempt((byte) 3);
        tblConfigurationMaster.setValidityInYear((byte) 0);
        tblConfigurationMaster.setRenewalAmount(BigDecimal.valueOf(0));
        tblConfigurationMaster.setIsPkiRequired("No");
        tblConfigurationMaster.setRegFeeRequired("No");
        tblConfigurationMaster.setIsCurrent("no");
        tblConfigurationMaster.setUserType(userType);
        try {
            configMasterDao.addTblConfigurationMaster(tblConfigurationMaster);
            msg = "ok";
        } catch (Exception ex) {
            LOGGER.error("insertConfigMaster : " + logUserId + " " + ex);

            msg = "Error";
        }
        LOGGER.debug("insertConfigMaster : " + logUserId + ENDS);

        return msg;
    }

    @Override
    public List<Object> getConfigUserType() {
        LOGGER.debug("getConfigUserType : " + logUserId + STARTS);
        List<Object> list = null;
        try {
            list = hibernateQueryDao.singleColQuery("select userType from TblConfigurationMaster");
        } catch (Exception ex) {
            LOGGER.error("getConfigUserType : " + logUserId + " " + ex);

        }
        LOGGER.debug("getConfigUserType : " + logUserId + ENDS);

        return list;
    }

    @Override
    public List<TblConfigurationMaster> getConfigMasterDetails(int configId) {
        LOGGER.debug("getConfigMasterDetails : " + logUserId + STARTS);
        List<TblConfigurationMaster> list = null;
        try {
            list = configMasterDao.findTblConfigurationMaster("configId", Operation_enum.EQ, (short) configId);
        } catch (Exception ex) {
            LOGGER.debug("getConfigMasterDetails : " + logUserId + " " + ex);

        }
        LOGGER.error("getConfigMasterDetails : " + logUserId + ENDS);

        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail){
        this.auditTrail = auditTrail;
    }
}
