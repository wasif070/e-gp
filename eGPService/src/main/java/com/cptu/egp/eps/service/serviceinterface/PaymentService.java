/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblPaymentActionRequest;
import com.cptu.egp.eps.model.table.TblPaymentExtensionActionRequest;
import java.util.Date;

/**
 *
 * @author shreyansh.shah
 */
public interface PaymentService {

    public boolean addPEPaymentReleaseRequest(TblPaymentActionRequest tblPaymentActionRequest);
    public boolean addPEPaymentExtendRequest(TblPaymentExtensionActionRequest tblPaymentExtensionActionRequest);
    public boolean updatePaymentExtendRequest(String extendRequestID,String ActionComments,String RequestID);
    public boolean updateTenderPaymentExtendRequest(String PaymentID,String NewValidityDate,String userId);

}
