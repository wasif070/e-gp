/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAskProcurementDao;
import com.cptu.egp.eps.model.table.TblAskProcurement;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class AskProcurementImpl {

    private static final Logger LOGGER = Logger.getLogger(AskProcurementImpl.class);
    private TblAskProcurementDao tblAskProcurementDao;
    private HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private static final String SEARCHOPERLIKE = " like ";
    private MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblAskProcurementDao getTblAskProcurementDao() {
        return tblAskProcurementDao;
    }

    public void setTblAskProcurementDao(TblAskProcurementDao tblAskProcurementDao) {
        this.tblAskProcurementDao = tblAskProcurementDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    

    /**
     * Fetching information from Tbl_AskProcurement and Tbl_QuestionCategory
     * @param offset
     * @param rows
     * @return list of objects
     */
    public List<Object[]> getListAskProcurement(int offset, int rows) {
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            //return hibernateQueryDao.createNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId");
            obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.postedByName,tap.userTypeId from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId", offset, rows);
        } catch (Exception e) {
            LOGGER.error("getListAskProcurement : " + logUserId + e);
        }
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Fetching information of question asked by particular user of Procurement Expert
     * @param not containing String of weather it is replied or not
     * @param offset
     * @param rows
     * @return list of objects of particular user
     */
    public List<Object[]> getListAskPro(int offset, int rows, String not) {
        LOGGER.debug("getListAskPro : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            //return hibernateQueryDao.createNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId");
            obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.postedByName  from TblAskProcurement tap,TblQuestionCategory tqc  where tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answerDate is " + not + " NULL order by tap.postedDate desc", offset, rows);
        } catch (Exception e) {
            LOGGER.error("getListAskPro : " + logUserId + e);
        }
        LOGGER.debug("getListAskPro : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Fetching information of question asked by particular user of Procurement Expert
     * in descending order by when answer is given
     * @param not containing String of weather it is replied or not
     * @param offset
     * @param rows
     * @return list of objects of particular user
     */
    public List<Object[]> getListAskProOrder(int offset, int rows, String not) {
        LOGGER.debug("getListAskProOrder : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            //return hibernateQueryDao.createNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId");
            obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.postedByName  from TblAskProcurement tap,TblQuestionCategory tqc  where tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answerDate is " + not + " NULL order by tap.answerDate desc", offset, rows);
        } catch (Exception e) {
            LOGGER.error("getListAskProOrder : " + logUserId + e);
        }
        LOGGER.debug("getListAskProOrder : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Fetching information of question asked by particular user
     * @param postedBy session id
     * @param offset
     * @param rows
     * @return list of objects of particular user
     */
    public List<Object[]> getListAskProcurementTender(int postedBy, int offset, int rows) {
        LOGGER.debug("getListAskProcurementTender : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answeredBy!=null order by tap.postedDate desc", offset, rows);
        } catch (Exception e) {
            LOGGER.error("getListAskProcurementTender : " + logUserId + e);
        }
        LOGGER.debug("getListAskProcurementTender : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Fetching information of question asked by particular user
     * @param postedBy session id
     * @param offset
     * @param rows
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return list of objects of particular user
     */
    public List<Object[]> getListAskProcurementTender(int postedBy, int offset, int rows, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getListAskProcurementTender : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            if (searchField.equalsIgnoreCase("category")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchString = "%" + searchString + "%";
                    searchOper = SEARCHOPERLIKE;
                }
                searchField = "tqc.catagory";
            } else if (searchField.equalsIgnoreCase("question")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchOper = SEARCHOPERLIKE;
                    searchString = "%" + searchString + "%";
                }
                searchField = "tap.question";
            }
            obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId and " + searchField + searchOper + "'" + searchString + "' and tap.answeredBy!=null order by tap.postedDate desc", offset, rows);
        } catch (Exception e) {
            LOGGER.error("getListAskProcurementTender : " + logUserId + e);
        }
        LOGGER.debug("getListAskProcurementTender : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Fetching data for Procurement Expert when procurement Expert gives reply
     * @param procQueId primary key
     * @return List of data
     */
    public List<Object[]> getListAskProcurement(int procQueId) {
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        String action = "";
        try {
            obj = hibernateQueryDao.createNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.postedByName,tap.userTypeId from TblAskProcurement tap,TblQuestionCategory tqc where  tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.procQueId=" + procQueId);
            if(obj!=null && !obj.isEmpty()){
                if(obj.get(0)[6]!=null && obj.get(0)[6].toString().trim().length()!=0){
                    action = "View Reply";
                }else{
                    action = "View Query";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getListAskProcurement : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Ask_Procurement_Expert.getName(), action, "");
            action = null;
        }
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * count number of records from Tbl_QuestionCategory, Tbl_AskProcurement
     * @return number of records
     * @throws Exception
     */
    public long countListAskProcurement() throws Exception {
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc", "tap.tblQuestionCategory.queCatId = tqc.queCatId");
        } catch (Exception e) {
            LOGGER.error("countListAskProcurement " + logUserId + e);
        }
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGEREND);
        return count;
    }

    /**
     * count number of records for particular user of Procurement Expert
     * @param not containing String of weather it is replied or not
     * @return number of records of particular user
     * @throws Exception
     */
    public long countListAskPro(String not) throws Exception {
        LOGGER.debug("countListAskPro : " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc,TblUserTypeMaster tutm,TblLoginMaster tlm", "tap.tblQuestionCategory.queCatId = tqc.queCatId and tutm.userTypeId = tap.userTypeId and tlm.userId=tap.postedBy and tap.answerDate is " + not + " NULL");
        } catch (Exception e) {
            LOGGER.error("countListAskPro : " + logUserId + e);
        }
        LOGGER.debug("countListAskPro : " + logUserId + LOGGEREND);
        return count;
    }

    /**
     * count number of records for particular user of Procurement Expert
     * @param not containing String of weather it is replied or not
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records of particular user
     * @throws Exception
     */
    public long countListAskProcurement(String searchField, String searchString, String searchOper, String not) throws Exception {
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            if (searchField.equalsIgnoreCase("category")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchString = "%" + searchString + "%";
                    searchOper = SEARCHOPERLIKE;
                }
                searchField = "tqc.catagory";
            } else if (searchField.equalsIgnoreCase("question")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchOper = SEARCHOPERLIKE;
                    searchString = "%" + searchString + "%";
                }
                searchField = "tap.question";
            }
            if ("not".equalsIgnoreCase(not)) {
                count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc", "tap.tblQuestionCategory.queCatId = tqc.queCatId and " + searchField + searchOper + "'" + searchString + "'and tap.answerDate != null");
            } else {
                count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc", "tap.tblQuestionCategory.queCatId = tqc.queCatId and " + searchField + searchOper + "'" + searchString + "'and tap.answerDate = null");
            }
        } catch (Exception e) {
            LOGGER.error("countListAskProcurement " + logUserId + e);
        }
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGEREND);
        return count;
    }

    /**
     * count number of records for particular user
     * @param postedBy session id
     * @return number of records of particular user
     * @throws Exception
     */
    public long countListAskProcurement(int postedBy) throws Exception {
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc", "tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answeredBy!=null");
        } catch (Exception e) {
            LOGGER.error("countListAskProcurement " + logUserId + e);
        }
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGEREND);
        return count;
    }

    /**
     * count number of records for particular user
     * @param postedBy session id
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records of particular user
     * @throws Exception
     */
    public long countListAskProcurement(int postedBy, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGERSTART);
        long count = 0;
        try {
            if (searchField.equalsIgnoreCase("category")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchString = "%" + searchString + "%";
                    searchOper = SEARCHOPERLIKE;
                }
                searchField = "tqc.catagory";
            } else if (searchField.equalsIgnoreCase("question")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchOper = SEARCHOPERLIKE;
                    searchString = "%" + searchString + "%";
                }
                searchField = "tap.question";
            }
            count = hibernateQueryDao.countForNewQuery("TblAskProcurement tap,TblQuestionCategory tqc", "tap.tblQuestionCategory.queCatId = tqc.queCatId  and tap.answeredBy!=null and " + searchField + searchOper + "'" + searchString + "'");
        } catch (Exception e) {
            LOGGER.error("countListAskProcurement " + logUserId + e);
        }
        LOGGER.debug("countListAskProcurement " + logUserId + LOGGEREND);
        return count;
    }

    /**
     * updating data for Procurement Expert when procurement Expert gives reply
     * @param tblAskProcurement
     * @return true if updated successfully
     */
    public boolean updateAskProcuremennt(TblAskProcurement tblAskProcurement) {
        LOGGER.debug("updateAskProcuremennt : " + logUserId + LOGGERSTART);
        boolean isUpdate = false;
        String action = "Reply to Query";
        try {
            tblAskProcurementDao.updateTblAskProcurement(tblAskProcurement);
            isUpdate = true;
        } catch (Exception e) {
            isUpdate = false;
            LOGGER.error("updateAskProcuremennt : " + logUserId + e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblAskProcurement.getAnsweredBy(), "userId", EgpModule.Ask_Procurement_Expert.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateAskProcuremennt : " + logUserId + LOGGEREND);
        return isUpdate;
    }

    /**
     * Fetching data for Procurement Expert
     * @param searchField
     * @param searchString
     * @param searchOper
     * @param offset
     * @param rows
     * @param not
     * @return data for Procurement Expert
     */
    public List<Object[]> getListAskProcurement(String searchField, String searchString, String searchOper, int offset, int rows, String not,String orderClause,String orderType) {
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            if (searchField.equalsIgnoreCase("category")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchString = "%" + searchString + "%";
                    searchOper = SEARCHOPERLIKE;
                }
                searchField = "tqc.catagory";
            } else if (searchField.equalsIgnoreCase("question")) {
                if (searchOper.equalsIgnoreCase("eq")) {
                    searchOper = "=";
                } else if (searchOper.equalsIgnoreCase("cn")) {
                    searchOper = SEARCHOPERLIKE;
                    searchString = "%" + searchString + "%";
                }
                searchField = "tap.question";
            }
            if ("not".equalsIgnoreCase(not)) {
                obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.answeredByName from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answerDate != null and " + searchField + searchOper + "'" + searchString + "'"+orderClause+orderType, offset, rows);
            } else {
                //obj =  hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId and "+searchField + searchOper + "'" + searchString + "'", offset, rows);
                //obj =  hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.answeredByName,tutm.userType,tlm.emailId from TblAskProcurement tap,TblQuestionCategory tqc,TblUserTypeMaster tutm,TblLoginMaster tlm where tap.tblQuestionCategory.queCatId = tqc.queCatId and tutm.userTypeId= tap.userTypeId and tlm.userId=tap.postedBy and "+searchField + searchOper + "'" + searchString + "'", offset, rows);
                obj = hibernateQueryDao.createByCountNewQuery("select tqc.queCatId,tqc.catagory,tap.procQueId,tap.question,tap.postedDate,tap.postedBy,tap.answer,tap.answerDate,tap.answeredBy,tap.answeredByName from TblAskProcurement tap,TblQuestionCategory tqc where tap.tblQuestionCategory.queCatId = tqc.queCatId and tap.answerDate = null and " + searchField + searchOper + "'" + searchString + "'"+orderClause+orderType, offset, rows);
            }
        } catch (Exception e) {
            LOGGER.error("getListAskProcurement : " + logUserId + e);
        }
        LOGGER.debug("getListAskProcurement : " + logUserId + LOGGEREND);
        return obj;
    }

    public boolean deleteAPE(String procQueId){
        int cnt = hibernateQueryDao.updateDeleteNewQuery("delete from TblAskProcurement where procQueId="+procQueId);
        return cnt!=0;
    }
}
