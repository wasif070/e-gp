/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblRoleMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblRoleMenuRightsDao;
import com.cptu.egp.eps.dao.daointerface.TblUserMenuRightsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblMenuMaster;
import com.cptu.egp.eps.model.table.TblRoleMaster;
import com.cptu.egp.eps.model.table.TblUserMenuRights;
import com.cptu.egp.eps.model.table.TblRoleMenuRights;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
/**
 *
 * @author dipal.shah
 */
public class ManageUserRightsImpl {

    final Logger logger = Logger.getLogger(ManageUserRightsImpl.class);

    private String logUserId ="0";
    
    TblUserMenuRightsDao tblUserMenuRightsDao;
    TblRoleMenuRightsDao tblRoleMenuRightsDao;
     
    HibernateQueryDao hibernateQueryDao;

    TblRoleMasterDao tblRoleMasterDao;
    private AuditTrail auditTrail;

    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblRoleMenuRightsDao getTblRoleMenuRightsDao() {
        return tblRoleMenuRightsDao;
    }

    public void setTblRoleMenuRightsDao(TblRoleMenuRightsDao tblRoleMenuRightsDao) {
        this.tblRoleMenuRightsDao = tblRoleMenuRightsDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblUserMenuRightsDao getTblUserMenuRightsDao() {
        return tblUserMenuRightsDao;
    }

    public void setTblUserMenuRightsDao(TblUserMenuRightsDao tblUserMenuRightsDao) {
        this.tblUserMenuRightsDao = tblUserMenuRightsDao;
    }

    public TblRoleMasterDao getTblRoleMasterDao() {
        return tblRoleMasterDao;
    }

    public void setTblRoleMasterDao(TblRoleMasterDao tblRoleMasterDao) {
        this.tblRoleMasterDao = tblRoleMasterDao;
    }

    

    /**
     * Method to get sub Menu details on the bases of given parent menu id.
     * @param menuId
     * @return Sub menu details as List of TblMenuMaster objects.
     */
    public List<TblMenuMaster> getMenuList(int menuId) {
       List<TblMenuMaster> lstMenuMaster = new ArrayList<TblMenuMaster>();
        String strQuery = " select menuId,menuName,parentMenuId,menuLink,menuImage,activation from TblMenuMaster" +
                          " where activation=1 and parentMenuId="+menuId +
                          " order by menuId";
        //System.out.println(strQuery);
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            Integer j = 0;
            for(Object[] i : objs){
                lstMenuMaster.add(new TblMenuMaster(Integer.parseInt(i[0].toString()),i[1].toString(),Integer.parseInt(i[2].toString()),i[3].toString(),i[4].toString(),Integer.parseInt(i[5].toString())));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            //logger.error("insertDataForGrandSummary : "+logUserId+" : "+ex.toString());
        }
        //logger.debug("insertDataForGrandSummary : "+logUserId+" Ends");
          return lstMenuMaster;
   }

   /**
    * Method to get menuId list for which rights given to user id.
    * @param userId
    * @return menuId list
    */
    public ArrayList getUserMenuRightsList(int userId) {
        List<TblUserMenuRights> listUserRights = null;
        ArrayList list = new ArrayList();
        try {
                listUserRights=tblUserMenuRightsDao.findtblUserMenuRights("userId", Operation_enum.EQ,userId);
            for (TblUserMenuRights obj : listUserRights) {
                    list.add(obj.getMenuId());
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getUserMenuRightsList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getUserMenuRightsList : "+logUserId+" Ends");
          return list;
    }

   /**
    * Method to get user details from user id.
    * @param userId
    * @return user name, email id
    */
    public List<Object[]> getUserDetails(int userId) {
        String strQuery = " select a.fullName,b.emailId from TblAdminMaster a, TblLoginMaster b "
                + " where a.tblLoginMaster.userId = b.userId "
                + " and b.userId=" + userId;
        //System.out.println(strQuery);
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
           // System.out.println("is object is null="+(objs == null));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getUserDetails : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getUserDetails : "+logUserId+" Ends");
          return objs;
    }


    /**
    * Method to assign rights to user on given menu list.
    * @param lstUserMenuRigths list of TblUserMenuRights object
    * @param  uId userId to whom rights is going to assign.
    * @return 0 if operation fails else return non zero integer value.
    */
    public int assignRights(List<TblUserMenuRights> lstUserMenuRigths, int uId) {
        int res = 0;
        String action="Assign User Rights";
        try {
            res = hibernateQueryDao.updateDeleteNewQuery("delete from TblUserMenuRights where userId=" + uId);
            //System.out.println("delete count=" + res);
            for (TblUserMenuRights obj : lstUserMenuRigths) {
                tblUserMenuRightsDao.addEntity(obj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("assignRights : "+logUserId+" : "+ex.toString());
            action="Error in "+ action + ex.toString();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        logger.debug("assignRights : "+logUserId+" Ends");
        return res;
    }

    /**
    * Method use for get list of menu rights detail for given user id
    * @param  uId userId whose rights want to fetch.
    * @return list of user rights details.
    */
    public Map<Integer, String> getUserMenuRightsMap(int uId) {

       
        String strQuery = "select b.menuId,b.menuName from TblUserMenuRights a, TblMenuMaster b"
                + " where a.menuId=b.menuId and a.userId=" + uId;

        //System.out.println(strQuery);
        List<Object[]> objs = new ArrayList<Object[]>();
        Map<Integer,String> objMap= new HashMap<Integer,String>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            for (Object[] objects : objs) {
             objMap.put((Integer)objects[0],objects[1].toString());
             //   System.out.println(objects[1].toString()+" = "+objects[0].toString());
            }
           // System.out.println("is object is null="+(objs == null));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getUserMenuRightsMap : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getUserMenuRightsMap : "+logUserId+" Ends");
          return objMap;
    }

   /**
    * Method use for get list of menu rights detail for given user id
    * @param  uId userId whose rights want to fetch.
    * @return list of user rights details.
    */
    public Map<Integer, String> getUserMenuRightsMap(int uId, int userTypeId) {
        String strQuery = null;
        List<Object[]> objs = new ArrayList<Object[]>();
        int rollId=0;
        Map<Integer,String> objMap= new HashMap<Integer,String>();
        try {
            if (userTypeId == 20) {
                strQuery = "select a.rollId from TblAdminMaster a"
                        + " where a.tblLoginMaster.userId=" + uId;
                List<Object> obj= new ArrayList<Object>();
               // System.out.println("strQuery="+strQuery);
                obj = hibernateQueryDao.singleColQuery(strQuery);
               // System.out.println("obje.size="+     obj.size());
                for (Object objects : obj) {
                    if (objects != null && !objects.toString().equalsIgnoreCase("0")) {
                        rollId=Integer.parseInt(objects.toString());
                    }
                }
            }


            if(rollId > 0) // Rights role wise
            {
                strQuery = "select b.menuId,b.menuName from TblRoleMenuRights a, TblMenuMaster b"
                        + " where a.menuId=b.menuId and a.rollId=" + rollId;
            } else // Rights Individual
            {
                strQuery = "select b.menuId,b.menuName from TblUserMenuRights a, TblMenuMaster b"
                        + " where a.menuId=b.menuId and a.userId=" + uId;
            }
           // System.out.println(strQuery);
                objs = hibernateQueryDao.createNewQuery(strQuery);
            for (Object[] objects : objs) {
                 objMap.put((Integer)objects[0],objects[1].toString());
                 //   System.out.println(objects[1].toString()+" = "+objects[0].toString());
                }
               // System.out.println("is object is null="+(objs == null));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getUserMenuRightsMap : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getUserMenuRightsMap : "+logUserId+" Ends");
          return objMap;
    }
   
     /**
    * Method to assign rights to role on given menu list.
    * @param lstUserMenuRigths list of TblUserMenuRights object
    * @param  uId userId to whom rights is going to assign.
    * @return 0 if operation fails else return non zero integer value.
    */
    public int assignRoleRights(List<TblRoleMenuRights> lstRoleMenuRigths, int roleId) {
        int res = 0;
        try {
            res = hibernateQueryDao.updateDeleteNewQuery("delete from TblRoleMenuRights where rollId=" + roleId);
            //System.out.println("delete count=" + res);
            for (TblRoleMenuRights obj : lstRoleMenuRigths) {
                tblRoleMenuRightsDao.addEntity(obj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("assignRoleRights : "+logUserId+" : "+ex.toString());
        }
        logger.debug("assignRoleRights : "+logUserId+" Ends");
        return res;
    }

    /**
    * Method to get Role Name.
    * @param roleId
    * @return RoleName
    */
    public String getRoleDetails(int roleId) {
       String roleName=null;
        String strQuery = " select rollName from TblRoleMaster "
                + " where rollId=" + roleId;
        //System.out.println(strQuery);
        List<Object> objs = null;
        try {
            objs = hibernateQueryDao.getSingleColQuery(strQuery);
            if (objs != null && objs.size() > 0) {
                roleName = (String)objs.get(0);
                //System.out.println("rollname="+roleName);
            }

        } catch (Exception ex) {
            roleName=null;
            ex.printStackTrace();
            logger.error("getRoleDetails : "+logUserId+" : "+ex.toString());
        }
          logger.debug("getRoleDetails : "+logUserId+" Ends");
          return roleName;
    }

   /**
    * Method to get Role Id on the basis of given roleName.
    * @param roleName
    * @return roleId
    */
    public int getRoleDetails(String roleName) {
       Integer roleId=0;
        String strQuery = " select rollId from TblRoleMaster "
                + " where rollName='" + roleName + "'";
        //System.out.println(strQuery);
        List<Object> objs = null;
        try {
            objs = hibernateQueryDao.getSingleColQuery(strQuery);
            if (objs != null && objs.size() > 0) {
                roleId = (Integer)objs.get(0);
            }

        } catch (Exception ex) {
            roleId=0;
            ex.printStackTrace();
            logger.error("getRoleDetails : "+logUserId+" : "+ex.toString());
        }
          logger.debug("getRoleDetails : "+logUserId+" Ends");
          return roleId;
    }

   /**
    * Method to get menuId list for which rights given to role Id.
    * @param roleId
    * @return menuId list
    */
    public ArrayList getRoleMenuRightsList(int roleId) {
        List<TblRoleMenuRights> listRoleRights = null;
        ArrayList list = new ArrayList();
        try {
                listRoleRights=tblRoleMenuRightsDao.findtblRoleMenuRights("rollId", Operation_enum.EQ,roleId);
            for (TblRoleMenuRights obj : listRoleRights) {
                    list.add(obj.getMenuId());
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getRoleMenuRightsList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getRoleMenuRightsList : "+logUserId+" Ends");
          return list;
    }


   /**
    * Method use for insert role details
    * @param tblRoleMaster
    * @return roleId
    */
    public int addRoles(TblRoleMaster tblRoleMaster) {
       String action="Create User Rights Role";
       int roleId=0;
        try {
                tblRoleMasterDao.addTblRoleMaster(tblRoleMaster);
                roleId=getRoleDetails(tblRoleMaster.getRollName());
        } catch (Exception ex) {
            ex.printStackTrace();
            roleId=0;
            logger.error("addRoles : "+logUserId+" : "+ex.toString());
            action ="Error in "+action+ex.toString();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        logger.debug("addRoles : "+logUserId+" Ends");
        return roleId;
    }

   /**
    * Method use for update role details
    * @param tblRoleMaster
    * @return roleId
    */
    public int updateRoles(TblRoleMaster tblRoleMaster) {
       String action="Update User Rights Role";
       int roleId=0;
        try {
                tblRoleMasterDao.updateTblRoleMaster(tblRoleMaster);
                roleId=tblRoleMaster.getRollId();
        } catch (Exception ex) {
            ex.printStackTrace();
            roleId=0;
            logger.error("updateRoles : "+logUserId+" : "+ex.toString());
            action ="Error in "+action+ex.toString();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        logger.debug("updateRoles : "+logUserId+" Ends");
        return roleId;
    }

   /**
    * Method to check Role Name already exist.
    * @param Role Name
    * @return boolean value true if role name already exist else return false.
    */
    public boolean checkRoleNameExist(String roleName, int... roleId) {
       String objeRoleName=null;

        String strQuery = " select rollName from TblRoleMaster "
                + " where rollName='" + roleName + "'";
        
        if (roleId != null && roleId[0] != 0) {
        strQuery += " and rollId!="+roleId[0];
       }
       // System.out.println(strQuery);
        List<Object> objs = null;
        try {
            objs = hibernateQueryDao.getSingleColQuery(strQuery);
            if (objs != null && objs.size() > 0) {
                objeRoleName = (String)objs.get(0);
                //System.out.println("rollname="+roleName);
            }

        } catch (Exception ex) {
            objeRoleName=null;
            ex.printStackTrace();
            logger.error("getRoleDetails : "+logUserId+" : "+ex.toString());
        }
          logger.debug("getRoleDetails : "+logUserId+" Ends");

        if (objeRoleName != null) {
            return true;
        } else {
            return false;
        }
    }

   /**
    * Method use for get role details.
    * @return List of TblRoleMaster Objects
    */
    public List<TblRoleMaster> getAllActiveRoles(int firstResult, int maxResult, String sortColumn, String sortType) {
    //    logger.debug("getBanglaStates : " + logUserId + loggerStart);
         List<TblRoleMaster> tblRoleMaster = null;
        try {
            if (sortType.equals("asc")) {
                tblRoleMaster = tblRoleMasterDao.findByCountTblRoleMaster(firstResult, maxResult, "status", Operation_enum.EQ, 1, sortColumn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.ASC);
            } else {
                tblRoleMaster = tblRoleMasterDao.findByCountTblRoleMaster(firstResult, maxResult, "status", Operation_enum.EQ, 1, sortColumn, Operation_enum.ORDERBY, Operation_enum.ORDERBY.DESC);
            }
        } catch (Exception ex) {
            logger.error("getAllActiveRoles : " + logUserId + ex);
        }
   //     logger.debug("getBanglaStates : " + logUserId + loggerEnd);
         return tblRoleMaster;
    }

    /**
    * Method use for get role details.
    * @return List of TblRoleMaster Objects
    */
   public List<TblRoleMaster> getAllActiveRoles() {
    //    logger.debug("getBanglaStates : " + logUserId + loggerStart);
         List<TblRoleMaster> tblRoleMaster = null;
        try {
            tblRoleMaster = tblRoleMasterDao.findTblRoleMaster("status",Operation_enum.EQ,1);
        } catch (Exception ex) {
            logger.error("getAllActiveRoles : " + logUserId + ex);
        }
   //     logger.debug("getBanglaStates : " + logUserId + loggerEnd);
         return tblRoleMaster;
    }


   /**
    * Method use for delete User Menu rights if admin assign rolebased rights instead of individual.
    * @param userId
    */
    public void deleteUserMenuRights(int userId) {
        String strQuery = " delete from TblUserMenuRights "
                + " where userId=" + userId;
        logger.debug("deleteUserMenuRights : "+logUserId+" Starts");
        try {
             hibernateQueryDao.updateDeleteQuery(strQuery);

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("deleteUserMenuRights : "+logUserId+" : "+ex.toString());
        }
          logger.debug("deleteUserMenuRights : "+logUserId+" Ends");
   }

  /**
   * Method use for get o&m user details in case of Edit /View Operation.
   * @param userId
   * @return list of Object array containing o&m user details.
   */
    public List<Object[]> getOandMUserDetails(int userId) {
        logger.debug("getOandMUserDetails : "+logUserId+" Starts");
        String strQuery = "select tlm.userId,tutm.userType,tam.mobileNo,tam.nationalId, "
                        + "tam.fullName,tutm.userTypeId,tlm.emailId,tam.adminId,tam.phoneNo, tam.rollId "
                        + "from TblLoginMaster tlm,TblAdminMaster tam,TblUserTypeMaster tutm "
                        + "where tlm.userId = tam.tblLoginMaster.userId and "
                        + "tutm.userTypeId=tlm.tblUserTypeMaster.userTypeId "
                        + "and tlm.userId = " + userId ;

        //System.out.println(strQuery);
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
           // System.out.println("is object is null="+(objs == null));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("getOandMUserDetails : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getOandMUserDetails : "+logUserId+" Ends");
          return objs;
    }
}
