/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsAitConfigurationService {

    public void setLogUserId(String logUserId);

    public int insertCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public boolean updateCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public boolean deleteCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public List<TblCmsAitConfig> getAllCmsAitConfig();

    public long getCmsAitConfigCount();

    public TblCmsAitConfig getCmsAitConfig(int id);

    public List<TblCmsAitConfig> getCmsAitConfigForFianancialYear(int financialYearId);

    public List<String> getCmsAitConfigFianacialYearsList();

}
