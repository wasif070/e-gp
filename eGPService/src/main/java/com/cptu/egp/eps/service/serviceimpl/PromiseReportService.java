/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.egp.eps.dao.storedprocedure.SPPromiseReport;
import com.cptu.egp.eps.dao.storedprocedure.SPPromiseReportData;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Shreyansh shah
 */
public class PromiseReportService {

    private static final Logger LOGGER = Logger.getLogger(PromiseReportService.class);
    private SPPromiseReport sPPromiseReport;
    private String logUserId="0";
    
    public SPPromiseReport getsPPromiseReport() {
        return sPPromiseReport;
    }

    public void setsPPromiseReport(SPPromiseReport sPPromiseReport) {
        this.sPPromiseReport = sPPromiseReport;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * fetching information from p_get_pomisereport
     * @param funName1
     * @param funName2
     * @param funName3
     * @param funName4
     * @param funName5
     * @param funName6
     * @param funName7
     * @param funName8
     * @param funName9
     * @param funName10
     * @return list of data, if execute successfully return list, otherwise return empty list(null)
     */
  //  public List<SPPromiseReportData> searchData(String funName1, String funName2, String funName3, String funName4,String funName5, String funName6,String funName7,String funName8,String funName9,String funName10)
    public List<SPPromiseReportData> searchData(String funName1, String funName2, String funName3, String funName4,String funName5, String funName6,String funName7,String funName8,String funName9,String funName10)
    {
        LOGGER.debug("searchData : "+logUserId+" Starts");
        List<SPPromiseReportData> list = null;
        try {
                //list = sPPromiseReport.executeProcedure(funName1, funName2, funName3, funName4, funName5, funName6, funName7, funName8, funName9, funName10);
                list = sPPromiseReport.executeProcedure(funName1, funName2, funName3, funName4, funName5, funName6,funName7,funName8,funName9,funName10);
        } catch (Exception e) {
            LOGGER.error("searchData : "+logUserId+" "+e);
    }
        LOGGER.debug("searchData : "+logUserId+" Ends");
        return list;
    }

}
