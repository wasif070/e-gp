/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author parag
 */
public class TendererTabServiceImpl {

    final Logger logger = Logger.getLogger(TendererTabServiceImpl.class);
    private String logUserId ="0";

    HibernateQueryDao hibernateQueryDao;
    
   
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
     
    /**
     * Give declartion status of tenderer as per userId and tenderId
     * @param tendererId : UserId
     * @param tenderId
     * @return String accepted or rejected
     */
    public String isTendererDeclarationStepOver(int tendererId,int tenderId){

        logger.debug("isTendererDeclarationStepOver : "+logUserId+" Starts");
        String data=null;
        try {
           List<Object> id = hibernateQueryDao.singleColQuery("select tbc.confirmationStatus from TblBidConfirmation tbc where tbc.userId = "+tendererId+"  and tbc.tblTenderMaster.tenderId = " + tenderId);
           if(!id.isEmpty()){
              data = id.get(0).toString();
           }
        } catch (Exception ex) {
             logger.error("isTendererDeclarationStepOver : "+logUserId+" : "+ex.toString());
        }
        logger.debug("isTendererDeclarationStepOver : "+logUserId+" Ends");
        return data;
    }
    
    /**
     * Give true if opening date is >  then current date else false
     * @param tenderId
     * @return boolean true if opening date is >  then current date else false
     */
    public boolean isOpeningDateTime(int tenderId){
        logger.debug("isOpeningDateTime : "+logUserId+" Starts");
        boolean flag = true;
        List<Object> list = new ArrayList<Object>();
        try {
            String sqlQuery = "select openingDt "
                    + "from TblTenderDetails "
                    + "where tenderId="+tenderId+" and openingDt < current_timestamp() order by openingDt desc";
             list = hibernateQueryDao.singleColQuery(sqlQuery);
            if(!list.isEmpty()){
                flag = false;
            }
        } catch (Exception e) {
            logger.error("isOpeningDateTime : "+logUserId+" : "+e.toString());
        }
        logger.debug("isOpeningDateTime : "+logUserId+" Ends");
        return flag;
    }
}
