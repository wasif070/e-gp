/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Naresh.Akurathi
 */
public interface FinancialYearService {

    /**
     * Method for adding the FinanacialYear details and also updating the previous status of Default FinancialYear field.
     * @param status
     * @param tblFinancialYear 
     * @return
     */
    public String addFinancialYearService(String status, TblFinancialYear tblFinancialYear);

    /**
     * Method to get all FinancialYear Details from Dao.
     * @return List of TblFinancialYear
     */
    public List<TblFinancialYear> getFinancialYearDetails();

    /**
     *List of TblFinancialYear on id
     * @param id to be searched on
     * @return List of TblFinancialYear
     */
    public List<TblFinancialYear> getFinancialYearData(int id);

     /**
      *Update Financial Year
      * @param tblFinancialYear to be updated
      * @return Updated or Not Updated for success or fail
      */
     public String updateFinancialYear(TblFinancialYear tblFinancialYear);
       
     public void setLogUserId(String logUserId);

     /**
      *Get all Financial Year
      * @param offset
      * @param row
      * @return List of TblFinancialYear
      */
     public List<TblFinancialYear> getallFinancialYearDetails(int offset , int row);

     /**
      *Count for Financial Year
      * @return count
      */
     public long countallFinancialYearDetails();

     public void setAuditTrail(AuditTrail auditTrail);
}
