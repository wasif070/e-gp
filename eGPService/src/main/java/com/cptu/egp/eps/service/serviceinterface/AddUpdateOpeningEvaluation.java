/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import java.util.List;
/**
 *
 * @author rokibul
 */
public interface AddUpdateOpeningEvaluation {
    /**
     * For doing operation
     * @param fieldValues includes 20 param
     * @return List of information
     */
    public List<CommonMsgChk> addUpdOpeningEvaluation(String... fieldValues);

    public void setLogUserId(String logUserId);
}
