/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsPrDocument;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface ProgressReportUploadDocService {

    public void setLogUserId(String logUserId);

    public List<Object[]> getListForView(String wpId,int prId);

    public List<Object[]> getListForViewForWorks(String wpId,int prId);

    public boolean addPrDocsDetails(TblCmsPrDocument tblCmsPrDocument);

    public boolean deletePrDocsDetails(int prDocId);

    public List<TblCmsPrDocument> getPrDocumentDetails(int prRepId);

}
