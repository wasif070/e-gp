/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderListBox;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TendererComboService {

    /**
     * gives the combo data list categary wise
     * @param intFormId
     * @param strCat is the string which defines categary in the case of yes or no
     * @return combo data list
     */
    public List<TblTenderListBox> getTenderListBoxCatWise(int intFormId,String strCat);

    /**
     * gives the combo data list categary wise for Payment Schedule Form
     * @param intFormId
     * @param strCat is the string which defines categary in the case of yes or no
     * @return combo data list
     */
    public List<Object[]> getTenderListBoxForPS(int intFormId,String strCat);
   /**
    * adds the combo name data to database
    * @param tbltenderlistbox
    * @param tbltenderlistdetail
    * @return flag weather data is not saved into database
    */
   public boolean addComboNameDetailsFromTenderer(TblTenderListBox tbltenderlistbox, List<TblTenderListDetail> tbltenderlistdetail);

   /**
    * gives the combo name data list
    * @param offcet for paging facility
    * @param row used for sorting facility
    * @param templetformid which is come from tenderer form
    * @param searchField for searching
    * @param searchString is the string which user want to find from table
    * @param searchOper for searching
    * @param orderby which is define in which oreder u want to sort asc or desc
    * @param colName defines column name on which sorting is applied
    * @return combo data list
    */
   public List<TblTenderListBox> searchComboNameFromTenderer(int offcet,int row,int templetformid, String searchField,String searchString,String searchOper,String orderby,String colName);

   /**
    * gives combo row count
    * @param templetformid which is come from tenderer
    * @param searchField for searching
    * @param searchString is the string which user want to find from table
    * @param searchOper defines column name on which sorting is applied
    * @return total count
    */
   public long getSearchCountFromTenderer(int templetformid,String searchField, String searchString, String searchOper);

   /**
    * gives combo name list
    * @param offcet used for paging facility
    * @param row used for sorting facility
    * @param templetformid which is come from tenderer form
    * @param orderby which is define in which oreder u want to sort asc or desc
    * @param colName defines column name on which sorting is applied
    * @return combo name data list
    */
   public List<TblTenderListBox> getComboNameFromTenderer(int offcet, int row,int templetformid, String orderby, String colName);

   /**
    * gives combo count
    * @param templetformid comes from tenderer form
    * @return total count for combo data row
    */
   public long getComboCountFromTenderer(int templetformid);

   /**
    * gives combo data value list
    * @param id for TblTenderListBox
    * @return combo data value list
    */
   public List<TblTenderListDetail> getListBoxDetailFromTenderer(int id);


   /**
    * gives combo data value list
    * @param id for TblTenderListBox
    * @return combo data value list
    */
   public List<TblTenderListDetail> getTenderListBoxDetail(int id);

   /**
    * this method is for deleting combo from database
    * @param tenderlistId
    * @return boolean, return true - deleted successfully otherwise not
    */
   public boolean deleteCombo(int tenderlistId);

   /**
    * this method is for getting tender status
    * @param tenderId
    * @return list, list of data
    */
   public List<Object> getTenderStatus(int tenderId);

   public void setLogUserId(String logUserId);
}
