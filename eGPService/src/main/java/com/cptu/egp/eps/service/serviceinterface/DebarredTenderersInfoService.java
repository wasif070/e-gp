/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonDebarredTendererDetails;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface DebarredTenderersInfoService {

    public void setLogUserId(String logUserId);
    /***
     * This method returns the all the records of DebarmentDetails
     * @return List of CommonDebarredTendererDetails
     * @throws Exception
     */
    public List<CommonDebarredTendererDetails> getAllDebartmentDetails() throws Exception;

}
