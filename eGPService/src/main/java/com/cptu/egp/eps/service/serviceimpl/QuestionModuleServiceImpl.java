/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigPaThresholdDao;
import com.cptu.egp.eps.dao.daointerface.TblQuestionModuleDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblQuestionModule;
import com.cptu.egp.eps.service.serviceinterface.QuestionModuleService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author feroz
 */
public class QuestionModuleServiceImpl implements QuestionModuleService{

    
    static final Logger LOGGER = Logger.getLogger(QuestionModuleServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    
    public HibernateQueryDao hibernateQueryDao; 
    public TblQuestionModuleDao tblQuestionModuleDao;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    
    
    @Override
    public void addTblQuestionModule(TblQuestionModule tblQuestionModule) {
        boolean done = false;
        LOGGER.debug("addTblConfigPaThreshold: " + logUserId + LOGGERSTART);
            try {
            getTblQuestionModuleDao().addTblQuestionModule(tblQuestionModule);
            done = true;
            } catch (Exception e) {
                LOGGER.error("addTblConfigPaThreshold: " + logUserId + e);
            }
            LOGGER.debug("addTblConfigPaThreshold: " + logUserId + LOGGEREND);
    }

    @Override
    public void deleteTblQuestionModule(TblQuestionModule tblQuestionModule) {
            LOGGER.debug("deleteTblQuestionModule: " + logUserId + LOGGERSTART);
            try {
            getTblQuestionModuleDao().deleteTblQuestionModule(tblQuestionModule);
            } catch (Exception e) {
                LOGGER.error("deleteTblQuestionModule: " + logUserId + e);
            }
            LOGGER.debug("deleteTblQuestionModule: " + logUserId + LOGGEREND);
    }

    @Override
    public String updateTblQuestionModule(TblQuestionModule tblQuestionModule) {
        boolean flag = false;
        LOGGER.debug("updateTblQuestionModule: " + logUserId + LOGGERSTART);
            try {
                getTblQuestionModuleDao().updateTblQuestionModule(tblQuestionModule);
                flag = true;
            } catch (Exception e) {
                LOGGER.error("updateTblQuestionModule: " + logUserId + e);
            }
            LOGGER.debug("updateTblQuestionModule: " + logUserId + LOGGEREND);
            if(flag)
            {
                return "editSucc";
            }
            else
            {
                return "failed";
            }
    }

    @Override
    public List<TblQuestionModule> getAllTblQuestionModule() {
        LOGGER.debug("getAllTblQuestionModule : " + logUserId + " Starts");
        List<TblQuestionModule> tblQuestionModule = null;
        try {
            tblQuestionModule = getTblQuestionModuleDao().getAllTblQuestionModule();
        } catch (Exception ex) {
            LOGGER.error("getAllTblQuestionModule : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getAllTblQuestionModule : " + logUserId + " Ends");
        return tblQuestionModule;
    }

    @Override
    public List<TblQuestionModule> findTblQuestionModule(int id) {
        LOGGER.debug("findTblQuestionModule : " + logUserId + " Starts");
        List<TblQuestionModule> tblQuestionModule = null;
        try {
            tblQuestionModule = getTblQuestionModuleDao().findTblQuestionModule("moduleId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("findTblQuestionModule : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("findTblQuestionModule : " + logUserId + " Ends");
        return tblQuestionModule;
    }
    
    
    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * @return the tblQuestionModuleDao
     */
    public TblQuestionModuleDao getTblQuestionModuleDao() {
        return tblQuestionModuleDao;
    }

    /**
     * @param tblQuestionModuleDao the tblQuestionModuleDao to set
     */
    public void setTblQuestionModuleDao(TblQuestionModuleDao tblQuestionModuleDao) {
        this.tblQuestionModuleDao = tblQuestionModuleDao;
    }
    
}
