/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalReportMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class OfficerTabServiceImpl {

    final Logger logger = Logger.getLogger(OfficerTabServiceImpl.class);

    TblTenderDetailsDao tblTenderDetailsDao;
    TblEvalReportMasterDao tblEvalReportMasterDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    
    public TblEvalReportMasterDao getTblEvalReportMasterDao() {
        return tblEvalReportMasterDao;
    }

    
    public void setTblEvalReportMasterDao(TblEvalReportMasterDao tblEvalReportMasterDao) {
        this.tblEvalReportMasterDao = tblEvalReportMasterDao;
    }

    
    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    
    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    /**
     * Get Procurement nature using tenderId
     * @param tenderId
     * @return List of TblTenderDetails
     */
    public List<TblTenderDetails> getProcurementNature(int tenderId){
        logger.debug("getProcurementNature : "+logUserId+" Starts");
        List<TblTenderDetails> list = null;
        try {
            list = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception e) {
            logger.error("getProcurementNature : "+logUserId+" : "+e);
    }
        logger.debug("getProcurementNature : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get status of approving authority
     * @param tenderId
     * @return List of TblEvalReportMaster
     */
    public List<TblEvalReportMaster> getStatusAA(int tenderId){
        logger.debug("getStatusAA : "+logUserId+" Starts");
        List<TblEvalReportMaster> list = null;
        try {
            list = tblEvalReportMasterDao.findTblEvalReportMaster("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception e) {
            logger.error("getStatusAA : "+logUserId+" : "+e);
    }
        logger.debug("getStatusAA : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get SubContract Count as per userid and tenderId
     * @param tenderId tenderId 
     * @param userId bidderId
     * @return count of record
     */
    public long subContractCount(String tenderId,String userId){
        logger.debug("subContractCount : "+logUserId+" Starts");
        long cnt=0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblSubContracting tsc", "tsc.tblTenderMaster.tenderId="+tenderId+" and tsc.invToUserId="+userId+" and tsc.invAcceptStatus='approved'");
        } catch (Exception ex) {
            logger.error("subContractCount : "+logUserId,ex);
        }
        logger.debug("subContractCount : "+logUserId+" Ends");
        return cnt;
    }
    
    /**
     * Get tenderClosed or not
     * @param tenderId tenderId 
     * @return boolean true or false
     */
    public boolean isTenderClosed(String tenderId){
        logger.debug("subContractCount : "+logUserId+" Starts");
        long cnt=0;
        boolean flag = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderDetails tsc", "tsc.tblTenderMaster.tenderId="+tenderId+" and tsc.submissionDt>current_timestamp");
            if(cnt>0){
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("subContractCount : "+logUserId,ex);
        }
        logger.debug("subContractCount : "+logUserId+" Ends");
        return flag;
    }
}
