/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblDesignationMaster;

/**
 *
 * @author parag
 */
public interface CreateDesignationService {

    public void createDesignation(TblDesignationMaster  designationMaster);

     public void setUserId(String logUserId);
}
