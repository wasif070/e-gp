/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblWsLog;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface WsLogService {

    public void setLogUserId(String logUserId);
    public int insertWsLog(TblWsLog tblWsLog);
    public boolean updateWsLog(TblWsLog tblWsLog);
    public boolean deleteWsLog(TblWsLog tblWsLog);
    public List<TblWsLog> getAllWsLogs();
    public long getWsLogCount();
}
