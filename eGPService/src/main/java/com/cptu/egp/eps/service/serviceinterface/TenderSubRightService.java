/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderRights;
import java.util.List;




/**
 *
 * @author Ketan
 */
public interface TenderSubRightService {

  
    public List<Object[]> getActiveUserOfCmpnyByUserId(String strUserId);

    public void setLogUserId(String logUserId);
    
    public List<TblTenderDetails> getTenderDetails(String strTenderId);

    public boolean isJvcaUser(String strUserId);

    public boolean assingTenderRights(TblTenderRights tblTenderRights);

    public List<SPCommonSearchData> getTenderRightAssignee(String strFunName,String strUserId,String strTenderId);

}
