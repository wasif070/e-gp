/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblConfigurationMasterImpl;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
public class CheckExtensionService {


static final Logger logger = Logger.getLogger(CheckExtensionService.class);
private TblConfigurationMasterImpl tblConfigurationMasterDao;
private String logUserId="0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * @return the tblConfigurationMasterDao
     */
    public TblConfigurationMasterImpl getTblConfigurationMasterDao() {
        return tblConfigurationMasterDao;
    }

    /**
     * @param tblConfigurationMasterDao the tblConfigurationMasterDao to set
     */
    public void setTblConfigurationMasterDao(TblConfigurationMasterImpl tblConfigurationMasterDao) {
        this.tblConfigurationMasterDao = tblConfigurationMasterDao;
    }

    /**
     * get details from Tbl_ConfigurationMaster
     * @param userType
     * @return configuration of particular user
     */
    public TblConfigurationMaster getConfigurationMaster(String userType){
        logger.debug("getConfigurationMaster : "+logUserId+" Starts");
        List<TblConfigurationMaster> tblConfigurationMasters = null;
        TblConfigurationMaster tblConfigurationMaster = null;
        try {
            tblConfigurationMasters = tblConfigurationMasterDao.findTblConfigurationMaster("userType", Operation_enum.EQ, userType);
            if(tblConfigurationMasters.size()>0){
              tblConfigurationMaster = tblConfigurationMasters.get(0);
            }
        } catch (Exception ex) {
            logger.error("getConfigurationMaster : "+logUserId+" "+ex);
        }
        logger.debug("getConfigurationMaster : "+logUserId+" Ends");
     return tblConfigurationMaster;
    }

}
