/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import java.util.List;

/**
 *
 * @author Istiak (Dohatec) - 29.Apr.15
 */
public interface EvalCommonSearchService {

    public List<EvalCommonSearchData> evalSearchData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15);
    public String GrandSumDiscountValue(String TenderId,String UserId,String FormId);
    public String PercantageValue(String TenderId,String UserId,String FormId,String BidId,String RowId);
}
