/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblUserPrefrenceDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.egp.eps.service.serviceinterface.UserPrefService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class UserPrefServiceImpl implements UserPrefService {

    private static final Logger LOGGER = Logger.getLogger(UserPrefServiceImpl.class);
    private TblUserPrefrenceDao tblUserPrefrenceDao;
    private HibernateQueryDao hibernateQueryDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    /**
     *
     * @return
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     *
     * @param hibernateQueryDao
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    private String logUserId = "0";

    /**
     *
     * @return
     */
    public TblUserPrefrenceDao getTblUserPrefrenceDao() {
        return tblUserPrefrenceDao;
    }

    /**
     *
     * @param tblUserPrefrenceDao
     */
    public void setTblUserPrefrenceDao(TblUserPrefrenceDao tblUserPrefrenceDao) {
        this.tblUserPrefrenceDao = tblUserPrefrenceDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * this method save the preference of the user to database
     * @param tblUserprefrence - this is the pojo objects
     * @return boolean, true-if saved, false - if not saved
     */
    @Override
    public boolean addemailsmsAlert(TblUserPrefrence tblUserprefrence) {
        boolean flag = false;
        String action = "";
        LOGGER.debug("addemailsmsAlert : " + logUserId + ": Starts");
        try {
            tblUserPrefrenceDao.addTblUserPrefrence(tblUserprefrence);
            flag = true;
            action = "Edit Preference";
        } catch (Exception e) {
            LOGGER.error("addemailsmsAlert : " + logUserId + " : " + e.getMessage());
            action = "Error in Edit Preference :" + e.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblUserprefrence.getUserId(), "userId", EgpModule.My_Account.getName(), action, "");
            action = null;
        }
        LOGGER.debug("addemailsmsAlert : " + logUserId + ": Ends");
        return flag;
    }

    /**
     * this method for getting the preference of the particular user from database by passing the user id
     * @param id - this is the user id  for particular user
     * @return list of data
     */
    @Override
    public List<TblUserPrefrence> getemailsmsAlert(int id) {
        String action = "";
        LOGGER.debug("getemailsmsAlert : " + logUserId + ": Starts");
        List<TblUserPrefrence> list = null;
        try {
            list = tblUserPrefrenceDao.findTblUserPrefrence("userId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            LOGGER.error("getemailsmsAlert : " + logUserId + " : " + ex.getMessage());
        }
        LOGGER.debug("getemailsmsAlert : " + logUserId + ": Ends");
        return list;
    }

    /**
     * this method update the preference of the user to database
     * @param tbluserpreference - this is the pojo objects
     * @return boolean, true-if updated, false - if not updated
     */
    @Override
    public boolean updateemailsmsAlert(TblUserPrefrence tbluserpreference) {
        LOGGER.debug("updateemailsmsAlert : " + logUserId + ": Starts");
        boolean flag = false;
        String action = "";
        try {
            tblUserPrefrenceDao.updateTblUserPrefrence(tbluserpreference);
            flag = true;
            action = "Edit Preference";
        } catch (Exception e) {
            LOGGER.error("updateemailsmsAlert : " + logUserId + " : " + e);
            action = "Error in Edit Preference :" + e.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tbluserpreference.getUserId(), "userId", EgpModule.My_Account.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateemailsmsAlert : " + logUserId + ": Ends");
        return flag;

    }

    /**
     * this method for getting the preference of the particular user from database by passing the user id and ('sms' or 'email')
     * @param id - this is the user id  for particular user
     * @param what - for what preference user want form database 'sms' or 'email'
     * @return boolean, if true - yes('sms' or 'email') , if false -  No('sms' or 'email')
     */
    @Override
    public boolean getpreferenceStatus(int id, String what) {
        LOGGER.debug("getpreferenceStatus : " + logUserId + ": Starts");
        boolean flag = false;
        try {
            List<TblUserPrefrence> status = getemailsmsAlert(id);
            if (status.isEmpty()) {
                flag = true;
            } else {
                if ("sms".equalsIgnoreCase(what)) {
                    String smsStatus = status.get(0).getSmsAlert();
                    if ("Yes".equalsIgnoreCase(smsStatus)) {
                        flag = true;
                    } else {
                        flag = false;
                    }
                } else if ("email".equalsIgnoreCase(what)) {
                    String emailStatus = status.get(0).getEmailAlert();
                    if ("Yes".equalsIgnoreCase(emailStatus)) {
                        flag = true;
                    } else {
                        flag = false;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getpreferenceStatus : " + logUserId + " : " + e);
        }
        LOGGER.debug("getpreferenceStatus : " + logUserId + ": Ends");
        return flag;
    }

    /**
     *
     * @param logUserId
     */
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * this method for getting the preference of the particular user by passing the email id
     * @param email id - this is the email id of the particular user
     * @return list of data
     */
    @Override
    public List<Object[]> getEmailSmsStatus(String email) {
        LOGGER.debug("getPrefStatus : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tup.emailAlert,tup.smsAlert from TblUserPrefrence tup,TblLoginMaster tlm where tlm.userId=tup.userId and tlm.emailId='" + email.trim() + "'");
        } catch (Exception e) {
            LOGGER.error("getPrefStatus : " + logUserId + " : " + e);
        }
        LOGGER.debug("getPrefStatus : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public List<Object[]> findEmail(String userTypeId) {
        LOGGER.debug("findEmail : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery(
                    "from TblEmailPrefMaster where  (usrtTypeID = '" + userTypeId + "' OR usrtTypeID like '%," + userTypeId + ",%' OR usrtTypeID like '%" + userTypeId + ",%' OR usrtTypeID like '%," + userTypeId + "%' ) AND state ='o' ");
        } catch (Exception e) {
            LOGGER.error("findEmail : " + logUserId + " : " + e);
        }

        LOGGER.debug("findEmail : " + logUserId + ": Ends");
        return list;
    }

    @Override
    public List<Object[]> findSms(String userTypeId) {
        LOGGER.debug("findSms : " + logUserId + ": Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery(
                    "from TblSmsPrefMaster where  (usrtTypeID = '" + userTypeId + "' OR usrtTypeID like '%," + userTypeId + ",%' OR usrtTypeID like '%" + userTypeId + ",%' OR usrtTypeID like '%," + userTypeId + "%') AND state ='o' ");
        } catch (Exception e) {
            LOGGER.error("findSms : " + logUserId + " : " + e);
        }
        LOGGER.debug("findSms : " + logUserId + ": Ends");
        return list;
    }

 /*   @Override
    public int updateTblEmailnSmsPrefMaster(String email, String sms, String userId) {
        LOGGER.debug("updateTblEmailnSmsPrefMaster : " + logUserId + ": Starts");


            int status = hibernateQueryDao.updateDeleteNewQuery(
                    "UPDATE TblUserPrefrence  SET  emailAlert='" + email + "',smsAlert='" + sms + "' WHERE  userId='" + userId + "'");
       System.out.println("hibernate query"+"UPDATE TblUserPrefrence  SET  emailAlert='" + email + "',smsAlert='" + sms + "' WHERE  userId='" + userId + "'");
        LOGGER.debug("updateTblEmailnSmsPrefMaster : " + logUserId + ": Ends");
        return status;
    }
  * 
  */
}
