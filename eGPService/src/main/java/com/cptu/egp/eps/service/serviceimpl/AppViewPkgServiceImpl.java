/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPAppPkgDetails;
import com.cptu.egp.eps.service.serviceinterface.AppViewPkgService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AppViewPkgServiceImpl implements AppViewPkgService{

    private static final Logger LOGGER = Logger.getLogger(AppViewPkgServiceImpl.class);

    SPAppPkgDetails spAppPkgDetails;

    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;
    
    public SPAppPkgDetails getSpAppPkgDetails()
    {
        return spAppPkgDetails;
    }

    public void setSpAppPkgDetails(SPAppPkgDetails spAppPkgDetails)
    {
        this.spAppPkgDetails = spAppPkgDetails;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public List<CommonAppPkgDetails> getAppPkgDetails(int appId, int packageId, String pkgType)
    {
        String auditAction = "";
        LOGGER.debug("getAppPkgDetails : " + logUserId + LOGGERSTART);
        List<CommonAppPkgDetails> pkgDetailses=new ArrayList<CommonAppPkgDetails>();
        try{
            pkgDetailses=spAppPkgDetails.executeProcedure(appId, packageId, pkgType);
            auditAction = "View Package";
        }catch(Exception ex){
            LOGGER.error("getAppPkgDetails : " + logUserId + ex);
             auditAction = "Error in View Package";
        }finally {
            // Following audit trail event commented because dual entry occure in database.
           // makeAuditTrailService.generateAudit(auditTrail,appId , "appId", EgpModule.APP.getName(), auditAction, "Package Id="+packageId);
            auditAction = null;
        }
        LOGGER.debug("getAppPkgDetails : " + logUserId + LOGGEREND);
        return pkgDetailses;
    }
}
