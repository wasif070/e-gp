/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;


import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPGetTenderDashboardOffline;
import com.cptu.egp.eps.service.serviceinterface.TenderDashboardOfflineService;
import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import java.util.List;

/**
 *
 * @author Istiak
 */
public class TenderDashboardOfflineServiceImpl implements TenderDashboardOfflineService {


   private static final Logger LOGGER = Logger.getLogger(TenderDashboardOfflineServiceImpl.class);
   private SPGetTenderDashboardOffline spGetTenderDashboardOffline;
   private HibernateQueryDao hibernateQueryDao;
   private String logUserId = "0";
   private String getMinistryList = "getMinistryList : ";
   private String getOrgList = "getMinistryList : ";
   private String loggerStart = " Starts";
   private String loggerEnd = " Ends";

    public SPGetTenderDashboardOffline getSpGetTenderDashboardOffline() {
        return spGetTenderDashboardOffline;
    }

   
    public void setSpGetTenderDashboardOffline(SPGetTenderDashboardOffline spGetTenderDashboardOffline) {
        this.spGetTenderDashboardOffline = spGetTenderDashboardOffline;
    }

    @Override
    public List<TenderDashboardOfflineDetails> getTenderDashboardOfflineData(String moduleFlag, String searchFlag, String procNature, String procType, String procMethod, String id, String status, String refNo, String pubDateFrom, String pubDateTo)
    {
        return getSpGetTenderDashboardOffline().executeProcedure(moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo);
    }

    @Override
    public boolean deleteTenderCORDashboardOfflineData(String moduleFlag, String id)
    {
        return getSpGetTenderDashboardOffline().executeProcedure(moduleFlag, id).get(0).isFlag();
    }

    @Override
    public boolean approveTenderCORDashboardOfflineData(String moduleFlag, String id, String comments)
    {
        return getSpGetTenderDashboardOffline().executeProcedure(moduleFlag, id, comments).get(0).isFlag();
    }

    @Override
    public List<TenderDashboardOfflineDetails> corrigendumTenderDashboardOfflineData(String moduleFlag, String id)
    {
        return getSpGetTenderDashboardOffline().executeProcedure(moduleFlag, id);
    }

    @Override
    public List<TenderDashboardOfflineDetails> getCORLotInfo(String moduleFlag, String id)
    {
        return getSpGetTenderDashboardOffline().executeProcedure(moduleFlag, id);
    }

    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public List<Object[]> getMinistryForTenderOffline() {
        LOGGER.debug(getMinistryList + logUserId + loggerStart);
        List<Object[]> listMinister = new ArrayList<Object[]>();

        try {
            String query = "select distinct ministryOrDivision from TblTenderDetailsOffline";
            listMinister = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listMinister.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getMinistryList + logUserId + ex);
        }
        LOGGER.debug(getMinistryList + logUserId + loggerEnd);
            return listMinister;
        }

    @Override
    public List<Object[]> getOrgForTenderOffline(String ministry)
    {
    LOGGER.debug(getOrgList + logUserId + loggerStart);
        List<Object[]> listOrg = new ArrayList<Object[]>();
        String replaceMinistryName = ministry;
        if(replaceMinistryName.contains("'"))
            replaceMinistryName = replaceMinistryName.replace("'", "''");
        try {

            String query = "select distinct agency from TblTenderDetailsOffline where ministryOrDivision = '"+ replaceMinistryName +"'";
            listOrg = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listOrg.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getOrgList + logUserId + ex);
        }
        LOGGER.debug(getOrgList + logUserId + loggerEnd);
            return listOrg;
    }

}
