/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderFrameWorkDao;
import com.cptu.egp.eps.model.table.TblTenderFrameWork;
import com.cptu.egp.eps.service.serviceinterface.TenderFrameWorkService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.apache.log4j.Logger;
import java.util.Date;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public class TenderFrameWorkServiceImpl implements TenderFrameWorkService {

    TblTenderFrameWorkDao tblTenderFrameWorkDao;
    //TblTenderFrameWork tblTenderFrameWork;
    final Logger logger = Logger.getLogger(TenderFrameWorkServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblTenderFrameWorkDao getTblTenderFrameWorkDao() {
        return tblTenderFrameWorkDao;
    }

    public void setTblTenderFrameWorkDao(TblTenderFrameWorkDao tblTenderFrameWorkDao) {
        this.tblTenderFrameWorkDao = tblTenderFrameWorkDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void addTblTenderFrameWork(TblTenderFrameWork tblTenderFrameWork,int userId) {
        logger.debug("add TblTenderFrameWork : " + logUserId + loggerStart);
        String action="";
        try {
         action="Add TblTenderFrameWork";
         tblTenderFrameWorkDao.addTblTenderFrameWork(tblTenderFrameWork);
        } catch (Exception e) {
             action="Add TblTenderFrameWork : "+e;
            logger.error("add TblTenderFrameWork : " + logUserId + e);
    }
    finally{
        makeAuditTrailService.generateAudit(auditTrail, userId, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("add TblTenderFrameWork : " + logUserId + loggerEnd);
    }

    @Override
    public void addTblTenderFrameWork(int appid, int packageId,int tenderId,String[] peofficeId, String[] officeName, String[] OfficeDistrict, String[] OfficeAddress, String[] ItemName,String[] Quantity, String[] Unit, int UserID, boolean isMap){
        Date CurrDate = new Date();
    if (isMap) {
            for (int i = 0; i < peofficeId.length; i++) {
                TblTenderFrameWork tblTenderFrameWork = new TblTenderFrameWork();
                tblTenderFrameWork.setTenderFrameWorkOfficeId(Long.parseLong("0"));
                tblTenderFrameWork.setAppid(appid);
                tblTenderFrameWork.setPackageId(packageId);
                tblTenderFrameWork.setTenderId(tenderId);
                tblTenderFrameWork.setPeofficeId(Integer.parseInt(peofficeId[i]));
                tblTenderFrameWork.setPeofficeName(officeName[i]);
                tblTenderFrameWork.setOfficeDistrict(OfficeDistrict[i]);
                tblTenderFrameWork.setOfficeAddress(OfficeAddress[i]);
                tblTenderFrameWork.setItemName(ItemName[i]);
                tblTenderFrameWork.setQuantity(Float.parseFloat(Quantity[i]));
                tblTenderFrameWork.setUnit(Unit[i]);
                tblTenderFrameWork.setCreateDate(CurrDate);
                tblTenderFrameWork.setCreateBy(UserID);

                tblTenderFrameWorkDao.addTblTenderFrameWork(tblTenderFrameWork);
            }
        }
        else {
            StringBuilder peoffices = new StringBuilder();
            StringBuilder ItemNames = new StringBuilder();
            for (int i = 0; i < peofficeId.length; i++) {
                peoffices.append(peofficeId[i]+",");
                ItemNames.append(ItemName[i]+",");
            }
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderFrameWork tlt where tlt.peofficeId in (" + peoffices.substring(0, peoffices.length()-1) + ") and tlt.itemName in (" + ItemNames.substring(0, ItemNames.length()-1) + ") and tlt.tenderId=" + tenderId );
            peoffices=null;
        }
    }

    @Override
    public List<TblTenderFrameWork> findTblTenderFrameWork(Object... values) throws Exception {
        return tblTenderFrameWorkDao.findTblTenderFrameWork(values); //To change body of generated methods, choose Tools | Templates.
    }


}
