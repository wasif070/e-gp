/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblSubContractingDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCompanyMaster;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.model.table.TblSubContracting;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.SubContractingService;
import com.cptu.eps.service.audit.EgpModule;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SubContractingServiceImpl implements SubContractingService{
    TblLoginMasterDao tblLoginMasterDao;
    TblCompanyMasterDao tblCompanyMasterDao;
    TblSubContractingDao tblSubContractingDao;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    private String logUserId="0";
    final Logger logger = Logger.getLogger(SubContractingServiceImpl.class);


    public void setTblSubContractingDao(TblSubContractingDao tblSubContractingDao) {
        this.tblSubContractingDao = tblSubContractingDao;
    }

    public TblSubContractingDao getTblSubContractingDao() {
        return tblSubContractingDao;
    }
    
    public TblCompanyMasterDao getTblCompanyMasterDao() {
        return tblCompanyMasterDao;
    }

    public void setTblCompanyMasterDao(TblCompanyMasterDao tblCompanyMasterDao) {
        this.tblCompanyMasterDao = tblCompanyMasterDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }


    @Override
    public TblCompanyMaster getCompanyDetails(String emailId) {
        TblCompanyMaster tblcompany = null;
        logger.debug("getCompanyDetails : "+logUserId+"starts");
        Object values[] ={"emailId",Operation_enum.LIKE,emailId,"tblUserTypeMaster",Operation_enum.EQ,new TblUserTypeMaster((byte) 2)};
        try
        {
            List<TblLoginMaster> loginMasters=tblLoginMasterDao.findTblLoginMaster(values);
            if(loginMasters!=null && loginMasters.size() > 0)
            {
                TblLoginMaster tblLoginMaster=loginMasters.get(0);
                int userId=tblLoginMaster.getUserId();
                Object values1[]={"tblLoginMaster",Operation_enum.EQ,new TblLoginMaster(userId)};
                List<TblCompanyMaster> companyMasters=tblCompanyMasterDao.findTblCompanyMaster(values1);
                if(companyMasters!=null && companyMasters.size()>0){
                    tblcompany = companyMasters.get(0);
                }

            }

        }
        catch(Exception ex)
        {
            logger.error("getCompanyDetails"+ex);
        }
        logger.debug("getCompanyDetails"+logUserId+ "ends");
        return tblcompany;
    }

  

    @Override
     public String checkSubContractorDetails(Integer tenderId,Integer invToUserId){
         logger.debug("checkSubContractorDetails : "+logUserId+"starts");
         String val = "";
        Object values[]={"tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"invToUserId",Operation_enum.EQ,invToUserId};
        try
        {
             List<TblSubContracting> subContracting=tblSubContractingDao.findTblSubContracting(values);

             if(subContracting!=null && subContracting.size() > 0)
             {
                  val =  "tenderId and Userid is already exist...";
             }
             else
             {
                val = "";
             }
              logger.debug("checkSubContractorDetails : "+logUserId+"ends");
        }

        catch(Exception ex)
        {
            logger.error("checkSubContractorDetails : "+ex);
             val =  "Error";
        }
        
        return val;
     }

    @Override
     public TblSubContracting getSubcontractDetails(Integer tenderId){
        TblSubContracting tblsubcontracting = null;
         logger.debug("getSubcontractDetails : "+logUserId+"starts");
        Object values[] ={"tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId)};
        try
        {
            List<TblSubContracting> subContracting=tblSubContractingDao.findTblSubContracting(values);
             if(subContracting!=null && subContracting.size() > 0){
                 tblsubcontracting = subContracting.get(0);
        }
        }
        catch(Exception ex)
        {
            logger.error("getSubcontractDetails : "+ex);
        }
        logger.debug("getSubcontractDetails : "+logUserId+"ends");
         return tblsubcontracting;
     }

    @Override
    public TblLoginMaster getSubContEmailId(Integer tenderId){
        TblLoginMaster tblmaster= null;
        logger.debug("getSubContEmailId : "+logUserId+"starts");
        Object values[] ={"tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId)};
        try
        {
            List<TblSubContracting> subContracting=tblSubContractingDao.findTblSubContracting(values);
            if(subContracting!=null && subContracting.size() > 0)
            {
                TblSubContracting sub = subContracting.get(0);
                int userId=sub.getInvToUserId();
                Object values1[]={"userId",Operation_enum.EQ,userId};

                 List<TblLoginMaster> tblLoginMasters=tblLoginMasterDao.findTblLoginMaster(values1);
                 if(tblLoginMasters!=null && tblLoginMasters.size() > 0){
                  tblmaster = tblLoginMasters.get(0);
            }
            }


        }
        catch(Exception ex)
        {
            logger.error("getSubContEmailId : "+ex);
        }
        logger.debug("getSubContEmailId : "+logUserId+"ends");
        return tblmaster;
    }

    @Override
   public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public String checkUserEmail(String emailId, int tenderId, int pkgLotId) {
        logger.debug("checkUserEmail : "+logUserId+"Starts");
        String status = "Error";
        try {
            List<Object[]> list = new ArrayList<Object[]>();
        long i = hibernateQueryDao.countForQuery("TblLoginMaster", "emailId='"+emailId+"'");
        if(i>0){
        list = hibernateQueryDao.createNewQuery("select subConId from TblSubContracting where tenderId="+tenderId+" and pkgLotId = "+pkgLotId+" and invToUserId in (select userId from TblLoginMaster where emailId = '"+emailId+"' and isJvca='no')");
        if(list.isEmpty()){
            status = "OK";
        }else{
            status = "Request already sent to "+emailId;
        }
        }else{
            status = "Email ID does not exist, Please enter another Email ID";
        }
        } catch (Exception e) {
            logger.error("checkUserEmail : "+e);
        }
        logger.debug("checkUserEmail : "+logUserId+"ends");
        return status;
    }

    @Override
    public String getBidWidrawStatus(int tenderId, int userId) {
        logger.debug("getBidWidrawStatus : "+logUserId+" Starts");
         String flag  = "error";
        try {
           
            List<Object> obj = new ArrayList<Object>();
            String sqlQuery  = "select bidSubStatus from TblFinalSubmission where tblTenderMaster.tenderId = "+tenderId+" "
                    + "and userId="+userId;
            obj = hibernateQueryDao.singleColQuery(sqlQuery);
                if(obj!=null && !obj.isEmpty()){
                        flag = obj.get(0).toString();
                }
                obj.clear();
                obj= null;
        } catch (Exception e) {
            logger.error("getBidWidrawStatus : "+e);
        }
        logger.debug("getBidWidrawStatus : "+logUserId+"ends");
        return flag;
    }
                    }
