/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsNewBankGuarnateeDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee;
import com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rikin.p
 */
public class CmsNewBankGuarnateeServiceImpl implements CmsNewBankGuarnateeService {
    
    final Logger logger = Logger.getLogger(CmsNewBankGuarnateeServiceImpl.class);

    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";
    private final static String SPACE = "   ";
    
    private TblCmsNewBankGuarnateeDao tblCmsNewBankGuarnateeDao;
    private HibernateQueryDao hibernateQueryDao;
    
    
    public TblCmsNewBankGuarnateeDao getTblCmsNewBankGuarnateeDao() {
        return tblCmsNewBankGuarnateeDao;
    }

    public void setTblCmsNewBankGuarnateeDao(TblCmsNewBankGuarnateeDao tblCmsNewBankGuarnateeDao) {
        this.tblCmsNewBankGuarnateeDao = tblCmsNewBankGuarnateeDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    
    @Override
     public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    @Override
     public int addBankGuarantee(TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee){
        logger.debug("addBankGuarantee : " + logUserId + STARTS);
        int bankGurid = 0;
        try {
            tblCmsNewBankGuarnateeDao.addTblCmsNewBankGuarnatee(tblCmsNewBankGuarnatee);
            bankGurid= tblCmsNewBankGuarnatee.getBankGid();
        } catch (Exception ex) {
            logger.error("addBankGuarantee : " + logUserId + SPACE + ex);
        }
        logger.debug("addBankGuarantee : " + logUserId + ENDS);
        return bankGurid;
     }
     
    
    @Override
    public TblCmsNewBankGuarnatee fetchBankGuarantee(int tenderId){
        logger.debug("fetchBankGuarantee : " + logUserId + STARTS);
        TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = new TblCmsNewBankGuarnatee();
        try {
            List<TblCmsNewBankGuarnatee> listTblCmsNewBankGuarnatee = tblCmsNewBankGuarnateeDao.findTblCmsNewBankGuarnatee("tenderId",Operation_enum.EQ,tenderId);
            if(!listTblCmsNewBankGuarnatee.isEmpty()){
                tblCmsNewBankGuarnatee = listTblCmsNewBankGuarnatee.get(0);
            }else{
                tblCmsNewBankGuarnatee = null;
            }
        } catch (Exception ex) {
            logger.error("fetchBankGuarantee : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuarantee : " + logUserId + ENDS);
        return tblCmsNewBankGuarnatee;
    }
    
    @Override
    public  List<Object[]> fetchBankGuaranteeForMaker(int tenderId){
        logger.debug("checkBankGuaranteeForMaker : " + logUserId + STARTS);
        boolean flag= false;
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select tp.tenderPaymentId,tp.pkgLotId,tp.userId , tp.isVerified  "
                                                  + "from TblTenderPayment tp "
                                                  + "where tp.tblTenderMaster.tenderId = "+tenderId+" and tp.paymentFor = 'Bank Guarantee' "
                                                  + "and tp.isLive = 'Yes'");
        } catch (Exception ex) {
            logger.error("checkBankGuaranteeForMaker : " + logUserId + SPACE + ex);
        }
        logger.debug("checkBankGuaranteeForMaker : " + logUserId + ENDS);
        return list;
    }
    
    @Override
    public boolean checkBankGuaranteeForChecker(int tenderId){
        logger.debug("checkBankGuaranteeForMaker : " + logUserId + STARTS);
        boolean flag= false;
        List<Object> list = new ArrayList<Object>();
        try {
            list = hibernateQueryDao.singleColQuery("select tp.tenderPaymentId from TblTenderPayment tp where tp.tblTenderMaster.tenderId = "+tenderId+" and tp.paymentFor = 'Bank Guarantee'");
            if(!list.isEmpty()){
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("checkBankGuaranteeForMaker : " + logUserId + SPACE + ex);
        }
        logger.debug("checkBankGuaranteeForMaker : " + logUserId + ENDS);
        return flag;
    }
    
    @Override
    public List<Object[]> bankGuaranteeListing(int tenderId){
        logger.debug("bankGuaranteeListing : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select  "
                    + "bg.bankGid, bg.tenderId,bg.pkgLotId,lm.emailId, nid.contractNo, nid.pkgLotId, "
                    + "tcm.companyName,ttm.firstName,ttm.lastName,tcm.companyId,bg.paymentId, bg.userId, nid.noaIssueId "
                    + "from TblCmsNewBankGuarnatee bg, TblLoginMaster lm, "
                    + "TblNoaIssueDetails nid, TblTendererMaster ttm,  TblCompanyMaster tcm "
                    + "where ttm.tblLoginMaster.userId = bg.userId  and ttm.tblCompanyMaster.companyId = tcm.companyId  "
                    + "and bg.tenderId = "+tenderId+" and lm.userId = bg.userId  "
                    + "and nid.tblTenderMaster.tenderId = bg.tenderId");
            
        } catch (Exception ex) {
            logger.error("bankGuaranteeListing : " + logUserId + SPACE + ex);
        }
        logger.debug("bankGuaranteeListing : " + logUserId + ENDS);
        return list;
    }
    
    public List<Object[]> getTenderpaymentStatus(int paymentId){
        logger.debug("bankGuaranteeListing : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select tp.tenderPaymentId, tp.isVerified,tp.isLive from TblTenderPayment tp where tp.tenderPaymentId = "+paymentId);
        } catch (Exception ex) {
            logger.error("bankGuaranteeListing : " + logUserId + SPACE + ex);
        }
        logger.debug("bankGuaranteeListing : " + logUserId + ENDS);
        return list;
    }
    
    @Override
    public List<Object[]> fetchBankGuaranteeList(int tenderId, int userId){
        logger.debug("fetchBankGuaranteeList : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select distinct tcm.companyId,tm.firstName,tm.lastName,nbg.bankGid,tn.contractNo,tn.isRepeatOrder,tlm.registrationType,tcm.companyName from TblCmsNewBankGuarnatee nbg,"
                    + "TblTendererMaster tm,TblNoaIssueDetails tn,TblLoginMaster tlm,TblCompanyMaster tcm where tn.tblTenderMaster.tenderId=nbg.tenderId and tlm.userId=tm.tblLoginMaster.userId and tcm.companyId=tm.tblCompanyMaster.companyId and nbg.userId = tm.tblLoginMaster.userId and nbg.createdBy = "+userId+" and "
                    + "nbg.tenderId = "+tenderId+" and nbg.paymentId = 0");
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeList : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<Object[]> fetchBankGuaranteeListMaker(int tenderId, int userId){
        logger.debug("fetchBankGuaranteeList : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select tm.firstName,tm.lastName,nbg.bankGid,tn.contractNo,tn.isRepeatOrder,tlm.registrationType,tcm.companyId,tcm.companyName from TblCmsNewBankGuarnatee nbg,"
                    + "TblTendererMaster tm,TblNoaIssueDetails tn,TblLoginMaster tlm,TblCompanyMaster tcm, TblTenderPayment tp  where tn.tblTenderMaster.tenderId=nbg.tenderId and tlm.userId=tm.tblLoginMaster.userId and tcm.companyId=tm.tblCompanyMaster.companyId and nbg.userId = tm.tblLoginMaster.userId and nbg.createdBy = "+userId+" and "
                    + "nbg.tenderId = "+tenderId+" and tp.isVerified = 'no' and tp.tenderPaymentId = nbg.paymentId");
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeList : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<TblCmsNewBankGuarnatee> fetchBankGuaranteeDetails(int bgId) {
        
        logger.debug("fetchBankGuaranteeDetails : " + logUserId + STARTS);
        List<TblCmsNewBankGuarnatee> list = new ArrayList<TblCmsNewBankGuarnatee>();
        try {
            list = tblCmsNewBankGuarnateeDao.findTblCmsNewBankGuarnatee("bankGid",Operation_enum.EQ,bgId);
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeDetails : " + logUserId + ENDS);
        return list;
    }

    //  Dohatec Start
    @Override
    public List<Object[]> fetchBankGuaranteeListICT(int tenderId, int userId){
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select distinct tcm.companyId,tm.firstName,tm.lastName,tn.contractNo,tn.isRepeatOrder,tlm.registrationType,tcm.companyName from TblCmsNewBankGuarnatee nbg,"
                    + "TblTendererMaster tm,TblNoaIssueDetails tn,TblLoginMaster tlm,TblCompanyMaster tcm where tn.tblTenderMaster.tenderId=nbg.tenderId and tlm.userId=tm.tblLoginMaster.userId and tcm.companyId=tm.tblCompanyMaster.companyId and nbg.userId = tm.tblLoginMaster.userId and nbg.createdBy = "+userId+" and "
                    + "nbg.tenderId = "+tenderId+" and nbg.paymentId = 0");
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeList : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<Object[]> fetchBankGuaranteeDetailsICT(int tenderId, int lotId, int userId){
        logger.debug("fetchBankGuaranteeDetailsICT : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select tcn.bgAmt, tcn.lastDtOfPayment, tcn.validityDays, tcn.lastDtOfValidity, tcn.remarks, tcn.currencyName  from TblCmsNewBankGuarnatee tcn "
                    + "where tcn.tenderId = "+tenderId+" and tcn.pkgLotId = "+lotId+" and tcn.userId="+userId);
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeDetails : " + logUserId + ENDS);
        return list;
    }


    @Override
    public List<Object[]> bankGuaranteeListingICT(int tenderId)
    {
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select "
                    + "distinct bg.bankGid, bg.tenderId,bg.pkgLotId,lm.emailId, nid.contractNo, nid.pkgLotId, "
                    + "tcm.companyName, ttm.firstName, ttm.lastName, tcm.companyId, bg.paymentId, bg.userId "
                    + "from TblCmsNewBankGuarnatee bg, TblLoginMaster lm, "
                    + "TblNoaIssueDetails nid, TblTendererMaster ttm, TblCompanyMaster tcm "
                    + "where ttm.tblLoginMaster.userId = bg.userId  and ttm.tblCompanyMaster.companyId = tcm.companyId "
                    + "and bg.tenderId = "+tenderId+" and lm.userId = bg.userId "
                    + "and nid.tblTenderMaster.tenderId = bg.tenderId");
        } catch (Exception ex) {
            logger.error("bankGuaranteeListingICT : " + logUserId + SPACE + ex);
        }
        logger.debug("bankGuaranteeListingICT : " + logUserId + ENDS);
        return list;
    }

    @Override
    public List<Object[]> fetchBankGuaranteeListMakerICT(int tenderId, int userId){
        logger.debug("fetchBankGuaranteeList : " + logUserId + STARTS);
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            list = hibernateQueryDao.createNewQuery("select distinct tm.firstName,tm.lastName,tn.contractNo,tn.isRepeatOrder,tlm.registrationType,tcm.companyId,tcm.companyName from TblCmsNewBankGuarnatee nbg,"
                    + "TblTendererMaster tm,TblNoaIssueDetails tn,TblLoginMaster tlm,TblCompanyMaster tcm, TblTenderPayment tp  where tn.tblTenderMaster.tenderId=nbg.tenderId and tlm.userId=tm.tblLoginMaster.userId and tcm.companyId=tm.tblCompanyMaster.companyId and nbg.userId = tm.tblLoginMaster.userId and nbg.createdBy = "+userId+" and "
                    + "nbg.tenderId = "+tenderId+" and tp.isVerified = 'no' and tp.tenderPaymentId = nbg.paymentId");
        } catch (Exception ex) {
            logger.error("fetchBankGuaranteeList : " + logUserId + SPACE + ex);
        }
        logger.debug("fetchBankGuaranteeList : " + logUserId + ENDS);
        return list;
    }

    //  Dohatec End
}
