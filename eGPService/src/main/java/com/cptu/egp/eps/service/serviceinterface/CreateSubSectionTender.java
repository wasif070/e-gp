/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface CreateSubSectionTender {
    /**
     * Create Sub Section
     * @param tblIttHeader
     * @return
     */
    public boolean createSubSection(TblTenderIttHeader tblIttHeader);
    /**
     * Get SubSection by section id
     * @param sectionId
     * @return
     */
    public List<TblTenderIttHeader> getSubSection(int sectionId);
    /**
     * Get Sub section with TDS Appl.
     * @param sectionId
     * @return
     */
    public List<Object[]> getSubSectionWithTDSAppl(int sectionId);
    /**
     * Update Sub Section
     * @param tblIttHeader
     * @return
     */
    public boolean updateSubSection(TblTenderIttHeader tblIttHeader);
    /**
     * Delete Sub Section
     * @param tblIttHeader
     * @return
     */
    public boolean deleteSubSection(TblTenderIttHeader tblIttHeader);
    /**
     * Set User Id at service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
}
