/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblMultiLangContentDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblMultiLangContent;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class MultiLingualService {

    public static final Logger logger = Logger.getLogger(MultiLingualService.class);

    TblMultiLangContentDao tblMultiLangContentDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    

    public TblMultiLangContentDao getTblMultiLangContentDao() {
        return tblMultiLangContentDao;
    }

    public void setTblMultiLangContentDao(TblMultiLangContentDao tblMultiLangContentDao) {
        this.tblMultiLangContentDao = tblMultiLangContentDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    
    /**
     * Search Multi language Content by language and title.
     * @param lang
     * @param title
     * @return List of Multi language Content.
     */
    public List<TblMultiLangContent> findContent(String lang,String title){
        logger.debug("findContent : "+logUserId+" Starts");
         List<TblMultiLangContent> list = null;
        try{
            list = tblMultiLangContentDao.findTblMultiLangContent("language",Operation_enum.EQ,lang,"title",Operation_enum.EQ,title);
        }catch(Exception e){
            logger.error("findContent : "+logUserId+" : "+e);
        }
        logger.debug("findContent : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get Language Data by order and with specified offset.
     * @param offset
     * @param rows
     * @param orderby
     * @param order
     * @return List of Multi Language content.
     */
    public List<TblMultiLangContent> getLanguageData(int offset, int rows, String orderby, String order){
           logger.debug("getLanguageData : "+logUserId+" Starts");
           List<TblMultiLangContent> list = null;
        try{
            if("asc".equalsIgnoreCase(order)){
               list = tblMultiLangContentDao.findByCountEntity(offset, rows, orderby ,Operation_enum.ORDERBY,Operation_enum.ASC);
            } else if ("desc".equalsIgnoreCase(order)){
               list = tblMultiLangContentDao.findByCountEntity(offset, rows, orderby ,Operation_enum.ORDERBY,Operation_enum.DESC);
            }else{
                list = tblMultiLangContentDao.findByCountEntity(offset, rows, "contentId" ,Operation_enum.ORDERBY,Operation_enum.ASC);
            }
        }catch(Exception e){
            logger.error("getLanguageData : "+logUserId+" : "+e);
        }
        logger.debug("getLanguageData : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get Total Count.
     * @return total no of count.
     */
    public long getTotalCount(){
        logger.debug("getTotalCount : "+logUserId+" Starts");
        long i;
        try{
            i = tblMultiLangContentDao.getTblMultiLangContentCount();
        }catch(Exception e){
            logger.error("getTotalCount : "+logUserId+" : "+e);
            i = 0;
        }
        logger.debug("getTotalCount : "+logUserId+" Ends");
        return i;
    }

    /**
     * Update multi language Content.
     * @param title
     * @param subtitle
     * @param language
     * @param displaytitle
     * @param Content
     * @param id
     * @return Status of the operation.
     */
    public String updateLangContent(String title, String subtitle, String language, String displaytitle, byte[] Content,int id){
        logger.debug("updateLangContent : "+logUserId+" Starts");
        String str = "";
        String action = "Edit Multilingual Content";
        try{
            TblMultiLangContent multiLangContent = new TblMultiLangContent(id, title, subtitle, Content, language);
            multiLangContent.setDisplayTitle(displaytitle);
            tblMultiLangContentDao.updateTblMultiLangContent(multiLangContent);
            str = "Value updated";
        }catch(Exception e){
            logger.error("updateLangContent : "+logUserId+" : "+e);
            str = "Value not updated";
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), action, "");
            action = null;
        }
        logger.debug("updateLangContent : "+logUserId+" Ends");
        return str;
    }

    /**
     * Get Multi language Data by Id.
     * @param id
     * @return List of MultiLanguage Content.
     */
    public List<TblMultiLangContent> getData(int id){
        logger.debug("getData : "+logUserId+" Starts");
        List<TblMultiLangContent> list =null;
        try{
            List<TblMultiLangContent> langContentList = tblMultiLangContentDao.findTblMultiLangContent("contentId",Operation_enum.EQ,id);
            if(!langContentList.isEmpty()){
                list = langContentList;
            }
                
        }catch(Exception e){
            logger.error("getData : "+logUserId+" : "+e);
        }
        logger.debug("getData : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get Multi Language Data for Grid view.
     * @param firstResult
     * @param maxResult
     * @param orderType
     * @param e
     * @param searchField
     * @param searchType
     * @return list of Object Array
     */
    public List<Object[]> getSeachLangGrid(int firstResult, int maxResult, String orderType, String e, String searchField, String searchType) {
        logger.debug("getSeachLangGrid : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            //return hibernateQueryDao.createByCountNewQuery("select PA.fullName, LM.emailId, LM.userId, SBP.sbDevelopName  from TblPartnerAdmin PA inner join TblScBankDevPartnerMaster as SBP, SBP.sbankDevelopId=PA.sbankDevelopId inner join TblLoginMaster as LM on PA.tblLoginMaster.userId=LM.userId where isAdmin='no' And (SBP.sBankDevelopId in (select distinct sBankDevelopId from TblScBankDevPartnerMaster Where partnerType='"+type+"' and sBankDevelHeadId="+bankId+") or SBP.sBankDevelopId="+bankId+")",firstResult, maxResult);
            String query = "select LM.contentId,LM.title,LM.displayTitle,LM.language,LM.subTitle,LM.value"
                    + " from TblMultiLangContent LM"
                    + " where "
                    + searchField + " " + searchType
                    + " order by " + orderType + " " + e;
            logger.debug("getSeachLangGrid  :" + logUserId + " Query : " + query);
            list = hibernateQueryDao.createByCountNewQuery(query, firstResult, maxResult);
        } catch (Exception ex) {
            logger.error("getSeachLangGrid : " + logUserId + ex);
            list = null;
        }
        logger.debug("getSeachLangGrid : " + logUserId + " Ends");
        return list;
    }

    /**
     * Get search count for Multi Language.
     * @param from
     * @param where
     * @return long value having count value
     * @throws Exception
     */
    public long getSearchCount(String from, String where) throws Exception {
         return hibernateQueryDao.countForQuery(from, where);
    }

}
