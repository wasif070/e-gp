/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblExternalMemInfoDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.model.table.TblExternalMemInfo;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class TblExternalMemInfoServiceImpl {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(SubContractingServiceImpl.class);
    TblExternalMemInfoDao tblExternalMemInfoDao;
    TblLoginMasterDao tblLoginMasterDao;
    HibernateQueryDao hibernateQueryDao;
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public TblExternalMemInfoDao getTblExternalMemInfoDao() {
        return tblExternalMemInfoDao;
    }

    public void setTblExternalMemInfoDao(TblExternalMemInfoDao tblExternalMemInfoDao) {
        this.tblExternalMemInfoDao = tblExternalMemInfoDao;
    }

    /**
     * Register External Member Info
     * @param tblExternalMemInfo
     * @return true if values inserted into TblExternalMemInfo
     */
    public boolean insertInToTblExternalMemInfo(TblExternalMemInfo tblExternalMemInfo) {
        logger.debug("insertInToTblExternalMemInfo : " + logUserId + "starts");
        boolean flag = true;
        try {
            tblExternalMemInfoDao.addTblExternalMemInfo(tblExternalMemInfo);

        } catch (Exception e) {
            logger.error("insertInToTblExternalMemInfo : " + e);
            flag = false;
        }
        logger.debug("insertInToTblExternalMemInfo : " + logUserId + "ends");
        return flag;
    }

    /**
     * Register External Member Info
     * @param tblLoginMaster
     * @return true if values inserted into TblLoginMaster
     */
    public boolean insertInToTblLoginMaster(TblLoginMaster tblLoginMaster) {
        logger.debug("insertInToTblLoginMaster : " + logUserId + "starts");
        boolean flag = true;
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);

        } catch (Exception e) {
            logger.error("insertInToTblLoginMaster : " + e);
            flag = false;
        }
        logger.debug("insertInToTblLoginMaster : " + logUserId + "ends");
        return flag;

    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * fetching details from Tbl_ExternalMemInfo  ,Tbl_LoginMaster
     * @param userId from Tbl_LoginMaster
     * @return list of objects
     */
    public List<Object[]> detailsExtMem(int userId) {
        List<Object[]> detailsExtMem = null;
        logger.debug("detailsExtMem : " + logUserId + "starts");
        String action = null;
        try {
            detailsExtMem = hibernateQueryDao.createNewQuery("select temi.memberId,tlm.userId,temi.fullName,temi.nameBangla,temi.nationalId,temi.phoneNo,temi.mobileNo,temi.createdBy,temi.creationDt,tlm.emailId,tlm.userId from TblExternalMemInfo temi ,TblLoginMaster tlm where temi.tblLoginMaster.userId = tlm.userId and tlm.userId=" + userId);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("detailsExtMem : " + e);
            action = "View Profile";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("detailsExtMem : " + logUserId + "ends");
        return detailsExtMem;
    }

    /**
     * updating information for External Member
     * @param tblExternalMemInfo
     * @return true if updated successfully
     */
    public boolean updateExternalMem(TblExternalMemInfo tblExternalMemInfo) {
        logger.debug("updateExternalMem : " + logUserId + "starts");
        boolean flag = true;
        String action = null;
        try {
            System.out.println("log user id="+logUserId);
            tblExternalMemInfoDao.updateEntity(tblExternalMemInfo);
            action = "Edit Profile";
        } catch (Exception e) {
            logger.error("updateExternalMem ::" + e);
            flag = false;
            action = "Error in Edit Profile";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblExternalMemInfo.gettblLoginMaster().getUserId(), "userId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("updateExternalMem : " + logUserId + "ends");
        return flag;
    }

    /**
     * Fetching information for External Member with Sorting
     * @param offset
     * @param limit
     * @param ascClause
     * @return object with information about External Member
     * @throws Exception
     */
    public List<Object[]> findExternalMasterList(int offset, int limit, String ascClause) {
        List<Object[]> detailsExtMem = null;
        logger.debug("findExternalMasterList : " + logUserId + "starts");
        try {
            detailsExtMem = hibernateQueryDao.createByCountNewQuery("select tlm.emailId,temi.fullName,temi.creationDt,temi.memberId,tlm.userId from TblExternalMemInfo temi ,TblLoginMaster tlm where tlm.userId = temi.tblLoginMaster.userId order by " + ascClause, offset, limit);
        } catch (Exception e) {
            logger.error("findExternalMasterList : " + e);
        }
        logger.debug("findExternalMasterList : " + logUserId + "ends");
        return detailsExtMem;

    }

    /**
     * count records of External Members
     * @return number of records
     * @throws Exception
     */
    public long getAllCountOfExternalMember() throws Exception {
        long getAllCountOfExternalMember = 0;
        logger.debug("getAllCountOfExternalMember : " + logUserId + "starts");
        try {
            getAllCountOfExternalMember = hibernateQueryDao.countForNewQuery("TblExternalMemInfo temi ,TblLoginMaster tlm", "tlm.userId = temi.tblLoginMaster.userId");
        } catch (Exception e) {
            logger.error("getAllCountOfExternalMember : " + e);
        }
        logger.debug("getAllCountOfExternalMember : " + logUserId + "ends");
        return getAllCountOfExternalMember;
    }

    /**
     * Fetching information for External Member with Search
     * @param offset
     * @param limit
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return object with information about External Member
     */
    public List<Object[]> findExternalMemberList_Search(int offset, int limit, String searchField, String searchString, String searchOper) {
        logger.debug("findExternalMemberList_Search : " + logUserId + "starts");
        List<Object[]> Result = null;
        if (searchField.equalsIgnoreCase("emailId")) {
            searchField = "tlm." + searchField;
        } else if (searchField.equalsIgnoreCase("fullName")) {
            searchField = "temi." + searchField;
        }
        if (searchOper.equalsIgnoreCase("EQ")) {
            Result = hibernateQueryDao.createByCountNewQuery("select tlm.emailId,temi.fullName,temi.creationDt,temi.memberId,tlm.userId from TblExternalMemInfo temi ,TblLoginMaster tlm where tlm.userId = temi.tblLoginMaster.userId and " + searchField + " = '" + searchString + "' order by temi.creationDt desc", offset, limit);
        } else if (searchOper.equalsIgnoreCase("CN")) {
            Result = hibernateQueryDao.createByCountNewQuery("select tlm.emailId,temi.fullName,temi.creationDt,temi.memberId,tlm.userId from TblExternalMemInfo temi ,TblLoginMaster tlm where tlm.userId = temi.tblLoginMaster.userId and " + searchField + " LIKE '%" + searchString + "%' order by temi.creationDt desc", offset, limit);
        } else {
            Result = null;
        }
        logger.debug("findExternalMemberList_Search : " + logUserId + "ends");
        return Result;
    }

    /**
     * count records of External Members (Searching)
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     * @throws Exception
     */
    public long getAllCountOfExternalMember(String searchField, String searchString, String searchOper) throws Exception {
        logger.debug("getAllCountOfExternalMember : " + logUserId + "starts");
        long ret = 0;
        if (searchField.equalsIgnoreCase("emailId")) {
            searchField = "tlm." + searchField;
        } else if (searchField.equalsIgnoreCase("fullName")) {
            searchField = "temi." + searchField;
        }
        if (searchOper.equalsIgnoreCase("EQ")) {
            ret = hibernateQueryDao.countForNewQuery("TblExternalMemInfo temi ,TblLoginMaster tlm", "tlm.userId = temi.tblLoginMaster.userId and " + searchField + " = '" + searchString + "'");
        } else if (searchOper.equalsIgnoreCase("CN")) {
            ret = hibernateQueryDao.countForNewQuery("TblExternalMemInfo temi ,TblLoginMaster tlm", "tlm.userId = temi.tblLoginMaster.userId and " + searchField + " LIKE '%" + searchString + "%'");
        } else {
            ret = 0;
        }
        logger.debug("getAllCountOfExternalMember : " + logUserId + "ends");
        return ret;
    }

    public void setUserId(String userId) {
        this.logUserId = userId;
    }
}
