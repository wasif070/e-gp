/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblToDoList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public interface AddTaskService {

    public void addAddTask(TblToDoList tblToDoList, int userId, String status);

    public List<TblToDoList> getTaskBrief(int userId, String status);

    public List<TblToDoList> searchTasks(int offcet,int rows,Date startDate, Date endDate, String status, int userId);

    public void setLogUserId(String logUserId);
}
