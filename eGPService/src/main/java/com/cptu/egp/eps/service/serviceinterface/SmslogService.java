/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblSmslog;

/**
 *
 * @author Administrator
 */
public interface SmslogService {

    /**
     * Insert Data to the SMS Log table.
     * @param tblSmslog
     */
    public void addSmslog(TblSmslog tblSmslog);

     /**
     * Set user Id at service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
    
}
