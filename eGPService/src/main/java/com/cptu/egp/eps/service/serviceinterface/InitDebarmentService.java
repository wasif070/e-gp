/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblDebarmentComMembers;
import com.cptu.egp.eps.model.table.TblDebarmentComments;
import com.cptu.egp.eps.model.table.TblDebarmentCommittee;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import com.cptu.egp.eps.model.table.TblDebarmentReq;
import com.cptu.egp.eps.model.table.TblDebarmentResp;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;

/**
 *
 * @author TaherT
 */
public interface InitDebarmentService {

    /**
     * Saves Debarment Request initiated by PE
     * @param tblDebarmentReq object which is saved
     * @param tblDebarmentDetails contains the reason for which tenderer is debared
     * @return flag whether transaction is completed or not
     */
    public boolean saveInitDebarmentRequest(TblDebarmentReq tblDebarmentReq,TblDebarmentDetails tblDebarmentDetails);

    /**
     *Saves the response of Tenderer for call of PE
     * @param tblDebarmentResp  object which is saved
     * @return flag whether transaction is completed or not
     */
    public boolean saveBidderResponse(TblDebarmentResp tblDebarmentResp);

    /**
     *Gives list of companyName and userId
     * @param searchString where conditions are placed on companyRegNumber,companyName,emailId
     * @return list of companyName and userId
     */
    public List<SPTenderCommonData> searchCompanyForDebar(String searchString);

    /**
     *List of debarment data for display in JqGrid
     * @param status debarmentStatus
     * @param offset paging offset
     * @param row paging row
     * @return List of debarment data for display in JqGrid
     * @throws Exception throws exception when data fetch creates problem
     */
    public List<Object[]> findDebarMentListing(String status, int offset, int row) throws Exception;

    /**
     *Count of debarment data for display in JqGrid
     * @param status  debarmentStatus
     * @return total count
     * @throws Exception throws exception when data fetch creates problem
     */
    public long debarMentListingCount(String status) throws Exception;

    /**
     *Get Response of tenderer
     * @param userId tendererId
     * @param debarId debarmentId of TblDebarmentReq
     * @return Response of tenderer
     * @throws Exception response fetching error
     */
    public List<Object[]> findDebarReqForResp(String userId,String debarId) throws Exception;

    /**
     *Return the List of data based on debarmentType
     * @param debarType 1(getsingletender),2(getPackage),3(getProject),4(getProcuringEntity),5(getorganisation)
     * @param userId peId or hopeId
     * @return  List of data based on debarmentType
     * @throws Exception data fetching problem from SP
     */
    public List<SPTenderCommonData> searchDataForDebarType(String debarType, String userId) throws Exception;

    /**
     *List of TblDebarmentTypes
     * @return List of TblDebarmentTypes
     */
    public List<TblDebarmentTypes> getDebarTypes();

    /**
     *Debarment Clarification of Tenderer,PE
     * @param debarId  debarmentId of TblDebarmentReq
     * @param status debarmentStatus
     * @return ebarment Clarification of Tenderer,PE
     * @throws Exception
     */
    public List<Object[]> viewTendererDebarClari(String debarId,String status) throws Exception;

    /**
     *Checks whether tenderer response is available or not
     * @param debarId  debarmentId of TblDebarmentReq
     * @return count 0 or 1
     * @throws Exception
     */
    public long isResponseAvail(String debarId) throws Exception;

    /**
     *List of Tenderer Clarification
     * @param debarId  debarmentId of TblDebarmentReq
     * @param status debarmentStatus
     * @return
     * @throws Exception
     */
    public List<SPTenderCommonData> viewTendererDebarClari(int debarId,String status)throws Exception;

    /**
     *Updates PE Debarment status
     * @param debarId  debarmentId of TblDebarmentReq
     * @param debarStatus of TblDebarmentReq
     * @param comments of TblDebarmentComments
     * @param hopeId of TblDebarmentReq
     * @return no. of rows updated
     * @throws Exception
     */
    public int updateDebarByPE(String debarId,String debarStatus,String comments,String hopeId) throws Exception;

    /**List of TblDebarmentDetails
     *Criteria query in TblDebarmentDetails
     * @param values variable parameter for query
     * @return List of TblDebarmentDetails
     * @throws Exception
     */
    public List<TblDebarmentDetails> getDebarDetails(Object ... values)throws Exception;

    /**
     *Updates HOPE Debarment status
     * @param debarId  debarmentId of TblDebarmentReq
     * @param debarIds of TblDebarmentDetails
     * @param debarStatus of TblDebarmentReq
     * @param comments of TblDebarmentComments
     * @param debarBy of TblDebarmentDetails
     * @param debarStart  of TblDebarmentReq
     * @param debarEnd  of TblDebarmentReq
     * @param debarTypeId  of TblDebarmentReq
     * @return flag true(success) or false(failure)
     * @throws Exception
     */
    public boolean updateDebarByHope(String debarId,String debarIds, String debarStatus, String comments, String debarBy, Date debarStart, Date debarEnd, String debarTypeId) throws Exception;

    /**
     *Create committee using tables tbl_DebarmentCommittee,tbl_DebarmentComMembers
     * @param committee of TblDebarmentCommittee
     * @param comMembers of List of TblDebarmentComMembers
     */
    public void createDebarCommittee(TblDebarmentCommittee committee,List<TblDebarmentComMembers> comMembers);

    /**
     *Committee Existence check and committee details fetching
     * @param debarId  debarmentId of TblDebarmentReq
     * @return List of data from TblDebarmentCommittee
     */
    public List<Object[]> isCommitteeExist(String debarId);

    /**
     *SP returning committee user's with their details based on debarCommitteeId
     * @param debarComId of TblDebarmentCommittee
     * @return Data from SP
     */
    public List<SPTenderCommonData> savedDebarmentComUser(String debarComId);

    /**
     *Update Debarment committee details
     * @param comId  of TblDebarmentCommittee
     * @param comName committee name from TblDebarmentCommittee
     * @param comMembers of List of TblDebarmentComMembers
     */
    public void updateDebarCommittee(String comId,String comName,List<TblDebarmentComMembers> comMembers);

    /**
     *Update TblDebarmentCommittee comStatus to publish
     * @param comId of TblDebarmentCommittee
     * @return  flag true(success) or false(failure)
     */
    public boolean publishDebarCommitte(String comId);

    /**
     *Get List of details related to hope
     * @param debarId of TblDebarmentReq
     * @return List of details related to hope
     */
    public List<Object[]> findHopeAns(String debarId);//,String hopeId);
    /**
     *Get List of details related to hope
     * @param debarId of TblDebarmentReq
     * @return List of details related to hope
     */
    public List<Object[]> findSendHopeAns(String debarId);//,String hopeId);

    /**
     *List of data related to committee member
     * @param userId member id
     * @param debarId of TblDebarmentReq
     * @return List of data related to committee member
     */
    public List<Object[]> getCommitteDetails(String userId,String debarId);

    /**
     *Inserting Committee Members comments
     * @param debarId of TblDebarmentReq
     * @param comments of TblDebarmentComments
     * @param userId tenderer userId
     * @param commentsBy officer userId
     * @return flag true(success) or false(failure)
     */
    public boolean committeMemComments(String debarId,String comments,String userId,String commentsBy);

    /**
     *Getting comments of committee members
     * @param debarId of TblDebarmentReq
     * @param userId tenderer userId
     * @param commentsBy officer userId
     * @return List<TblDebarmentComments>
     * @throws Exception
     */
    public List<TblDebarmentComments> getCommitteeMemComments(String debarId,String userId,String commentsBy) throws Exception;

    /**
     *List of [0 employeeName,1 memberType,2 comments,3 commentsBy]
     * @param debarId of TblDebarmentReq
     * @return List of [0 employeeName,1 memberType,2 comments,3 commentsBy]
     */
    public List<Object[]> getWholeCommitteComments(String debarId);

    /**
     *If 1 than it is chairperson
     * @param debarId of TblDebarmentReq
     * @param userId officer userId
     * @return
     */
    public long isChairperson(String debarId,String userId);

    /**
     *Inserting chairperson review in TblDebarmentComments
     * @param debarId of TblDebarmentReq
     * @param userId tenderer userId
     * @param comments chairperson comments
     * @param debarStatus status related to debarment
     * @param commentsBy chairperson userId
     * @return  flag true(success) or false(failure)
     */
    public boolean chaipersonReview(String debarId,String userId,String comments,String debarStatus,String commentsBy);

    /**
     *Hope Id from TblDebarmentReq
     * @param debarId from TblDebarmentReq
     * @return HopeId
     */
    public String getHopeIdForDebar(String debarId);
    /**
     *Hope Email-Id from TblDebarmentReq
     * @param debarId from TblDebarmentReq
     * @return HopeId
     */
    public String getHopeEmailIdForDebar(String debarId);

    /**
     *Get Tenderer Response for Debarment
     * @param values variable arguments
     * @return List of TblDebarmentResp
     * @throws Exception
     */
    public List<TblDebarmentResp> findDebarResponse(Object ... values) throws Exception;

    /**
     *Returns number of committee members
     * @param debarId of TblDebarmentReq
     * @return number of committee members
     * @throws Exception
     */
    public long countCommitteeMember(String debarId) throws Exception;

    /**
     *Saves Documents uploaded by Tenderer and Officers.
     * @param tblDebarmentDocs instance which is saved in DB
     * @return flag true(success) or false(failure)
     */
    public boolean saveDocument(TblDebarmentDocs tblDebarmentDocs);

    /**
     *Get Documents from TblDebarmentDocs
     * @param values variable arguments
     * @return List of TblDebarmentDocs
     */
    public List<TblDebarmentDocs> getDebarDocs(Object ... values);

    /**
     *Deletes documents from TblDebarmentDocs
     * @param debarDocId of TblDebarmentDocs
     * @return count of rows deleted
     */
    public int deleteDebarDocs(String debarDocId);

    /**
     *Updating TblDebarmentReq debarmentStatus while passing debarment from one officer to another
     * @param debarId of TblDebarmentReq
     * @param status to be saved in debarmentStatus
     * @return count of rows updated
     */
    public int updateDebarStatus(String debarId,String status);

    /**
     *Object Array of [0 userId,1 debarmentBy,2 hopeId,3 tblDebarmentTypes.debarTypeId]
     * @param debarId of TblDebarmentReq
     * @return Object Array of [0 userId,1 debarmentBy,2 hopeId,3 tblDebarmentTypes.debarTypeId]
     */
    public Object[] findPETenderHopeId(String debarId);

    /**
     *Get officeName from TblOfficeMaster
     * @param userId from TblEmployeeMaster
     * @return officeName
     */
    public Object getOfficeName(String userId);

    /**
     *e-MailId of PE from TblDebarmentReq and TblLoginMaster
     * @param debarId from TblDebarmentReq
     * @return PE e-MailId
     */
    public Object getPEEmailId(String debarId);

    /**
     *e-MailId of HOPE from TblDebarmentReq and TblLoginMaster
     * @param debarId from TblDebarmentReq
     * @return HOPE e-MailId
     */
    public Object getHOPEEmailId(String debarId);

    /**
     *e-MailId of Tenderer from TblDebarmentReq and TblLoginMaster
     * @param debarId from TblDebarmentReq
     * @return Tenderer e-MailId
     */
    public Object getTendererEmailId(String debarId);

    /**
     *e-MailIds of all CommitteeMembers from TblDebarmentComMembers and TblLoginMaster
     * @param debarId  from TblDebarmentReq
     * @return e-MailIds of all CommitteeMembers
     */
    public List<Object> getDebarCommitteeEmailIds(String debarId);

    /**
     **e-MailId of EgpAdmin
     * @return e-MailId of EgpAdmin
     */
    public Object getEgpAdmin();

    public void setLogUserId(String logUserId);

    /**
     * It gives PE emailId based on Debarment Id
     * @param debarId from TblDebarmentReq
     * @return emailId from TblLoginMaster
     */
    public List<Object[]> findPeSatisfy(String debarId);

    public void setAuditTrail(AuditTrail auditTrail);
}
