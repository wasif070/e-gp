/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;


import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import java.util.List;
/**
 *
 * @author Ketan Prajapati
 */
public interface CurrencyMasterService {

    /**
     * Get All Currency Data
     * @return
     * @throws Exception
     */
    public List<TblCurrencyMaster> getAllCurrency() throws Exception;
    /**
     * Get All Currency Data sorted by Currency Short Name
     * @author Md. Khaled Ben Islam from Dohatec
     * @return
     * @throws Exception
     */
    public List<TblCurrencyMaster> getAllCurrencySortByShortName() throws Exception;

    /**
     * Get Currency Details
     * @param iCurrencyid
     * @return
     * @throws Exception
     */
    public List<TblCurrencyMaster> getCurrencyDetails(int iCurrencyid) throws Exception;

    /**
     * Get Currency Details
     * @author Md. Khaled Ben Islam from Dohatec
     * @param strCurrencyShortName
     * @return
     * @throws Exception
     */
    public List<TblCurrencyMaster> getCurrencyDetails(String strCurrencyShortName) throws Exception;

    /**
     * Set User Id at Service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
}
