/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderListBoxDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderListDetailDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderListBox;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import com.cptu.egp.eps.service.serviceinterface.TendererComboService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TendererComboServiceImpl implements TendererComboService {

    private static final Logger LOGGER = Logger.getLogger(TendererComboServiceImpl.class);

    private TblTenderListBoxDao tblTenderListBoxDao;
    private TblTenderListDetailDao tblTenderListDetailDao;
    private HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public TblTenderListBoxDao gettblTenderListBoxDao() {
        return tblTenderListBoxDao;
    }

    public void settblTenderListBoxDao(TblTenderListBoxDao tblTenderListBoxDao) {
        this.tblTenderListBoxDao = tblTenderListBoxDao;
    }

    public TblTenderListDetailDao gettblTenderListDetailDao() {
        return tblTenderListDetailDao;
    }

    public void settblTenderListDetailDao(TblTenderListDetailDao tblTenderListDetailDao) {
        this.tblTenderListDetailDao = tblTenderListDetailDao;
    }

    @Override
    public List<TblTenderListBox> getTenderListBoxCatWise(int intFormId,String strCat){
        LOGGER.debug("getTenderListBoxCatWise : "+logUserId+" Starts");
        List<TblTenderListBox> listBoxDetail = new ArrayList<TblTenderListBox>();
        try{
            listBoxDetail = tblTenderListBoxDao.findTblTenderListBox("tenderFormId", Operation_enum.EQ, intFormId, "isCalcReq",Operation_enum.EQ, strCat);
        }catch(Exception ex){
            LOGGER.error("getTenderListBoxCatWise : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getTenderListBoxCatWise : "+logUserId+" Ends");
        return listBoxDetail;
    }

    @Override
    public List<Object[]> getTenderListBoxForPS(int intFormId,String strCat){
        LOGGER.debug("getTenderListBoxForPS : "+logUserId+" Starts");
        List<Object[]> listBoxDetail = new ArrayList<Object[]>();
        try{
            listBoxDetail = hibernateQueryDao.createNewQuery("select tld.itemText,tld.itemText from TblTenderListBox tl, TblTenderListDetail tld "
                                    + "where tl.tenderListId = tld.tblTenderListBox.tenderListId and tl.tenderFormId = "+intFormId+" and tl.isCalcReq = '"+strCat+"'");
        }catch(Exception ex){
            LOGGER.error("getTenderListBoxForPS : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getTenderListBoxForPS : "+logUserId+" Ends");
        return listBoxDetail;
    }

    @Override
    public boolean addComboNameDetailsFromTenderer(TblTenderListBox tbltenderlistbox, List<TblTenderListDetail> tbltenderlistdetail) {
        LOGGER.debug("addComboNameDetailsFromTenderer : "+logUserId+" Starts");
        boolean flag = false;
    try {
            tblTenderListBoxDao.addTblTenderListBox(tbltenderlistbox);
            List<TblTenderListDetail> list = new ArrayList<TblTenderListDetail>();
            for (TblTenderListDetail tbltenderListDetail : tbltenderlistdetail) {
                tbltenderListDetail.setTblTenderListBox(tbltenderlistbox);
                list.add(tbltenderListDetail);
            }
            tblTenderListDetailDao.updateOrSaveAllTblTenderListDetail(list);
             flag = true;
        } catch (Exception e) {
            LOGGER.error("addComboNameDetailsFromTenderer : "+logUserId+" : "+e);
            flag = false;
        }
        LOGGER.debug("addComboNameDetailsFromTenderer : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<TblTenderListBox> searchComboNameFromTenderer(int offcet, int row, int templetformid, String searchField, String searchString, String searchOper, String orderby, String colName) {
        LOGGER.debug("searchComboNameFromTenderer : "+logUserId+" Starts");
        List<TblTenderListBox> searchcomboname = null;
        try{
            if("eq".equalsIgnoreCase(searchOper)){
                 if("asc".equalsIgnoreCase(orderby)){
                    searchcomboname = tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row, "listBoxName",Operation_enum.EQ, searchString,"tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.ASC);}
                 else{
                    searchcomboname = tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row, "listBoxName",Operation_enum.EQ, searchString,"tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.DESC);}
            }else if("cn".equalsIgnoreCase(searchOper)){
                if("asc".equalsIgnoreCase(orderby)){
                    searchcomboname = tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row, "listBoxName",Operation_enum.LIKE, "%"+searchString+"%","tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.ASC);}
                else{
                    searchcomboname= tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row, "listBoxName",Operation_enum.LIKE, "%"+searchString+"%","tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.DESC);}
            }
            if(searchcomboname.isEmpty()){
                searchcomboname = null;}

        }catch(Exception e){
            LOGGER.error("searchComboNameFromTenderer : "+logUserId+" : "+e);
            searchcomboname = null;
        }
        LOGGER.debug("searchComboNameFromTenderer : "+logUserId+" Ends");
        return searchcomboname;
    }

    @Override
    public long getSearchCountFromTenderer(int templetformid,String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountFromTenderer : "+logUserId+" Starts");
        long lng =0;
        try{
            List<TblTenderListBox> getsearch = null;
            if("eq".equalsIgnoreCase(searchOper)){
                getsearch = tblTenderListBoxDao.findTblTenderListBox("listBoxName",Operation_enum.EQ, searchString,"tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.ASC);
            }else if("cn".equalsIgnoreCase(searchOper)){
                getsearch = tblTenderListBoxDao.findTblTenderListBox("listBoxName",Operation_enum.LIKE, "%"+searchString+"%","tenderFormId",Operation_enum.EQ, templetformid,"listBoxName",Operation_enum.ORDERBY,Operation_enum.ASC);
            }
            if(getsearch.isEmpty()){
                lng = 0;}
            else{
                lng = getsearch.size();}

        }catch(Exception e){
            LOGGER.error("getSearchCountFromTenderer : "+logUserId+" : "+e);
            lng = 0;
        }
        LOGGER.debug("getSearchCountFromTenderer : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public List<TblTenderListBox> getComboNameFromTenderer(int offcet, int row, int templetformid, String orderby, String colName) {
        LOGGER.debug("getComboNameFromTenderer : "+logUserId+" Starts");
        List<TblTenderListBox> list = null;
        try {
            if(orderby==null && colName==null){
                list = tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row,"tenderFormId",Operation_enum.EQ, templetformid);
            }else{
                Object data = null;
                if("asc".equals(orderby)){
                    data= Operation_enum.ASC;
                } else{
                    data= Operation_enum.DESC;
                }
                list = tblTenderListBoxDao.findByCountTblTenderListBox(offcet, row,"tenderFormId",Operation_enum.EQ, templetformid,colName,Operation_enum.ORDERBY, data);
            }
        } catch (Exception ex) {
            LOGGER.error("getComboNameFromTenderer : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getComboNameFromTenderer : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long getComboCountFromTenderer(int templetformid) {
        LOGGER.debug("getComboCountFromTenderer : "+logUserId+" Starts");
        long lng =0;
        try {
            lng = tblTenderListBoxDao.countForQuery("TblTenderListBox", "tenderFormId = '" + templetformid + "'");
        } catch (Exception ex) {
             LOGGER.error("getComboCountFromTenderer : "+logUserId+" : "+ex);
            lng = 0;
        }
        LOGGER.debug("getComboCountFromTenderer : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public List<TblTenderListDetail> getListBoxDetailFromTenderer(int id) {
        LOGGER.debug("getListBoxDetailFromTenderer : "+logUserId+" Starts");
         List<TblTenderListDetail> list = null;
        try{
           list = tblTenderListDetailDao.findTblTenderListDetail("tblTenderListBox",Operation_enum.EQ,new TblTenderListBox(id));
        }catch(Exception ex){
          LOGGER.error("getListBoxDetailFromTenderer : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getListBoxDetailFromTenderer : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<TblTenderListDetail> getTenderListBoxDetail(int id) {
        LOGGER.debug("getTenderListBoxDetail : "+logUserId+" Starts");
        List<TblTenderListDetail> list = null;
        try{
           list = tblTenderListDetailDao.findTblTenderListDetail("tblTenderListBox",Operation_enum.EQ,new TblTenderListBox(id));
        }catch(Exception ex){
          LOGGER.error("getTenderListBoxDetail : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getTenderListBoxDetail : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public boolean deleteCombo(int tenderlistId) {
        LOGGER.debug("deleteCombo : " + logUserId + "Starts");
        boolean flag = false;
        int i=0;
        try {
                i = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderListBox where tenderListId="+tenderlistId+"");
                if(i>0)
                {
                    flag = true;
                }
        } catch (Exception e) {
            LOGGER.error("deleteCombo : " + logUserId + " " + e);
        }
        LOGGER.debug("deleteCombo : " + logUserId + "Ends");
        return flag;

    }

    @Override
    public List<Object> getTenderStatus(int tenderId) {
       LOGGER.debug("getTenderStatus : " + logUserId + "Starts");
       List<Object> list = null;
       try {
               list = hibernateQueryDao.singleColQuery("select ttd.tenderStatus from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId+"");
       } catch (Exception e) {
           LOGGER.error("getTenderStatus : " + logUserId + " " + e);
       }
       LOGGER.debug("getTenderStatus : " + logUserId + "Ends");
       return list;

    }
}
