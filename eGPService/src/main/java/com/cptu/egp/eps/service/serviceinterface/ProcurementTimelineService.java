/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.ProcurementTimelineData;
import java.util.List;

/**
 *
 * @author Istiak (Dohatec) - 26.May.15
 */
public interface ProcurementTimelineService {

    public List<ProcurementTimelineData> procTimelineData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15);

    public int getAllRuleCount();

    public boolean insertOrUpdate(String[] ruleId, String[] NewOldEdit, String[] appAuthority, String[] procNature, String[] noOfDaysForTSC, String[] noOfDaysForTECorPEC, String[] noOfDaysForApproval);
}
