/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CommonSearchDataMoreService {

    public List<SPCommonSearchDataMore> getCommonSearchData(String... fieldValues);

    /**
     * For doing operation
     * @param fieldValues includes 20 param
     * @return List of information
     */
    public List<SPCommonSearchDataMore> geteGPData(String... fieldValues);
    
    /**
     * For doing operation
     * @param fieldValues includes 20 param
     * @return List of information
     */
    public String getCurrentDate();

    /**
     * For doing operation
     * @param fieldValues includes 20 param
     * @return List of information
     */
    public List<SPCommonSearchDataMore> geteGPDataMore(String... fieldValues);

    public void setLogUserId(String logUserId);
	//Create by Dohatec for optimizing processing time of EvalProcessInclude.jsp page.
    public List<SPCommonSearchDataMore> getEvalProcessInclude(String... fieldValues);
}
