/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public interface AppReviseConfigurationService {

    public String addTblAppreviseConfiguration(TblAppreviseConfiguration TblAppreviseConfiguration,int userID);
    public String updateTblAppreviseConfiguration(TblAppreviseConfiguration TblAppreviseConfiguration,int userID);
    public boolean isexistConfiguration(String financialYear);
    public List<TblAppreviseConfiguration> getAllConfiguration();
    public List<TblAppreviseConfiguration> getAppConfigurationDetails(String appConfgId);
    public void setLogUserId(String logUserId);
    public void setAuditTrail(AuditTrail auditTrail);
}
