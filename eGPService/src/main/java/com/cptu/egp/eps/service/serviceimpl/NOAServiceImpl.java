/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblContractSignDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaAcceptanceDao;
import com.cptu.egp.eps.dao.daointerface.TblNoaIssueDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblReportMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderPaymentDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import com.cptu.egp.eps.model.table.TblReportMaster;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPayment;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class NOAServiceImpl {

    final Logger logger = Logger.getLogger(NOAServiceImpl.class);
    TblNoaIssueDetailsDao tblNoaIssueDetailsDao;
    TblNoaAcceptanceDao tblNoaAcceptanceDao;
    TblTenderPaymentDao tblTenderPaymentDao;
    HibernateQueryDao hibernateQueryDao;
    TblContractSignDao tblContractSignDao;
    TblTenderLotSecurityDao tblTenderLotSecurityDao;
    TblReportMasterDao tblReportMasterDao;

    public TblReportMasterDao getTblReportMasterDao() {
        return tblReportMasterDao;
    }

    public void setTblReportMasterDao(TblReportMasterDao tblReportMasterDao) {
        this.tblReportMasterDao = tblReportMasterDao;
    }

    public TblTenderLotSecurityDao getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    public void setTblTenderLotSecurityDao(TblTenderLotSecurityDao tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblContractSignDao getTblContractSignDao() {
        return tblContractSignDao;
    }

    public void setTblContractSignDao(TblContractSignDao tblContractSignDao) {
        this.tblContractSignDao = tblContractSignDao;
    }

    public TblTenderPaymentDao getTblTenderPaymentDao() {
        return tblTenderPaymentDao;
    }

    public void setTblTenderPaymentDao(TblTenderPaymentDao tblTenderPaymentDao) {
        this.tblTenderPaymentDao = tblTenderPaymentDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblNoaAcceptanceDao getTblNoaAcceptanceDao() {
        return tblNoaAcceptanceDao;
    }

    public void setTblNoaAcceptanceDao(TblNoaAcceptanceDao tblNoaAcceptanceDao) {
        this.tblNoaAcceptanceDao = tblNoaAcceptanceDao;
    }

    public TblNoaIssueDetailsDao getTblNoaIssueDetailsDao() {
        return tblNoaIssueDetailsDao;
    }

    public void setTblNoaIssueDetailsDao(TblNoaIssueDetailsDao tblNoaIssueDetailsDao) {
        this.tblNoaIssueDetailsDao = tblNoaIssueDetailsDao;
    }

    /**
     * insert into TblNoaIssueDetails , TblNoaAcceptance
     * @param tblNoaIssueDetails
     * return true if inserted successfully else false
     */
    public boolean addTblNoaIssueDetails(TblNoaIssueDetails tblNoaIssueDetails) {
        logger.debug("addTblNoaIssueDetails : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblNoaIssueDetailsDao.addTblNoaIssueDetails(tblNoaIssueDetails);
            flag = true;
        } catch (Exception e) {
            logger.error("addTblNoaIssueDetails : " + logUserId + " : " + e);
        }
        logger.debug("addTblNoaIssueDetails : " + logUserId + " Ends");
        return flag;
    }

    /** for updating information into TblNoaIssueDetails
     *
     * @param tblNoaIssueDetails
     * @return true if updated successfully else false
     */
    public boolean updateTblNoaIssueDetails(TblNoaIssueDetails tblNoaIssueDetails) {
        //tblNoaIssueDetailsDao.addTblNoaIssueDetails(tblNoaIssueDetails);
        logger.debug("updateTblNoaIssueDetails : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblNoaIssueDetailsDao.updateTblNoaIssueDetails(tblNoaIssueDetails);
            flag = true;
        } catch (Exception e) {
            logger.error("updateTblNoaIssueDetails : " + logUserId + " : " + e);
        }
        logger.debug("updateTblNoaIssueDetails : " + logUserId + " Ends");
        return flag;
    }

    /**
     * insert into TblNoaIssueDetails , TblNoaAcceptance
     * @param tblNoaAcceptance
     * return true if inserted successfully else false
     */
    public boolean addTblNoaAcceptance(TblNoaAcceptance tblNoaAcceptance) {
        logger.debug("addTblNoaAcceptance : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblNoaAcceptanceDao.addTblNoaAcceptance(tblNoaAcceptance);
            flag = true;
        } catch (Exception e) {
            logger.error("addTblNoaAcceptance : " + logUserId + " : " + e);
        }
        logger.debug("addTblNoaAcceptance : " + logUserId + " Ends");
        return flag;
    }

    /**
     * for updating information into TblNoaAcceptance
     * @param tblNoaAcceptance
     * return true if updated successfully else false
     */
    public boolean updateTblNoaAcceptance(TblNoaAcceptance tblNoaAcceptance) {
        //tblNoaAcceptanceDao.addTblNoaAcceptance(tblNoaAcceptance);
        logger.debug("updateTblNoaAcceptance : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblNoaAcceptanceDao.updateTblNoaAcceptance(tblNoaAcceptance);
            flag = true;
        } catch (Exception e) {
            logger.error("updateTblNoaAcceptance : " + logUserId + " : " + e);
        }
        logger.debug("updateTblNoaAcceptance : " + logUserId + " Ends");
        return flag;
    }

    /**
     * fetching information TblNoaAcceptance, TblNoaIssueDetails, TblTenderLotSecurity
     * @param tenderId of Tbl_TenderMaster
     * @return List of objects
     */
    public List<Object[]> getDetailsNOA(int tenderId, int pkgLotId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getDetailsNOA : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,tna.userId,tnid.noaAcceptDt,tnid.rank,tnid.companyName,tnid.advanceContractAmt,tnid.contractAmt,tna.acceptRejType from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.pkgLotId=" + pkgLotId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.isRepeatOrder='no'");
        } catch (Exception e) {
            logger.error("getDetailsNOA : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOA : " + logUserId + " Ends");
        return obj;
    }
    public List<Object[]> getDetailsNOAforInvoice(int tenderId, int pkgLotId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getDetailsNOA : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,tna.userId,tnid.noaAcceptDt,tnid.rank,tnid.companyName,tnid.advanceContractAmt from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus='approved' and tnid.pkgLotId=" + pkgLotId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.isRepeatOrder='no'");
        } catch (Exception e) {
            logger.error("getDetailsNOA : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOA : " + logUserId + " Ends");
        return obj;
    }

    public List<Object[]> getDetailsNOAforInvoiceICT(int tenderId, int pkgLotId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getDetailsNOA : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,tna.userId,tnid.noaAcceptDt,tnid.rank,tnid.companyName,tnid.advanceContractAmt,tnid.contractAmt,tcm.currencyShortName from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls,TblCurrencyMaster tcm where tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus='approved' and tnid.pkgLotId=" + pkgLotId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.isRepeatOrder='no' and tcm.currencyId = tnid.tblCurrencyMaster.currencyId"
                    + " and tna.userId = tnid.userId and tnid.roundId = (select max(tnid.roundId) from TblNoaIssueDetails tnid,TblNoaAcceptance tna where tnid.pkgLotId=" + pkgLotId + " and tnid.noaIssueId = tna.tblNoaIssueDetails.noaIssueId)"
                    + " and tna.acceptRejDt = (select max(tna.acceptRejDt) from TblNoaIssueDetails tnid,TblNoaAcceptance tna where tnid.pkgLotId=" + pkgLotId + " and tnid.noaIssueId = tna.tblNoaIssueDetails.noaIssueId)");
        } catch (Exception e) {
            logger.error("getDetailsNOA : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOA : " + logUserId + " Ends");
        return obj;
    }
    public List<Object[]> getDetailsNOAForRO(int tenderId, int pkgLotId,int roundId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getDetailsNOAForRO : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,"
                    + "tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,"
                    + "tls.lotNo,tls.lotDesc,tna.userId,tnid.noaAcceptDt,tnid.rank,tnid.companyName "
                    + "from TblContractSign ts,TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls"
                    + " where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and "
                    + "tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and"
                    + " tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus='approved' "
                    + "and tnid.pkgLotId=" + pkgLotId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" "
                    + "and tnid.isRepeatOrder='yes' and ts.noaId=tnid.noaIssueId and tnid.roundId="+roundId+"");
        } catch (Exception e) {
            logger.error("getDetailsNOAForRO : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOAForRO : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetching information from TblNoaAcceptance, TblNoaIssueDetails, TblTenderLotSecurity
     * @param tenderId of Tbl_TenderMaster
     * @return list of Objects
     */
    public List<Object[]> getNOAListing(int tenderId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getNOAListing : " + logUserId + " Starts");
        List<Object[]> obj = null;

        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,tnid.userId,tnid.companyName,tnid.contractName,tnid.roundId,tnid.advanceContractAmt,tnid.contractAmt from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus = 'approved' and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.isRepeatOrder='no'");
        } catch (Exception e) {
            logger.error("getNOAListing : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListing : " + logUserId + " Ends");
        return obj;
    }
    public List<Object[]> getNOAListingForRO(int tenderId,int roundId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getNOAListingForRO : " + logUserId + " Starts");
        List<Object[]> obj = null;

        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,"
                    + "tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,"
                    + "tnid.userId,tnid.companyName,tnid.contractName,ts.contractSignId from TblNoaAcceptance tna,"
                    + " TblNoaIssueDetails tnid, TblTenderLotSecurity tls,TblContractSign ts where "
                    + "tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and "
                    + "tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and"
                    + " tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus = 'approved' "
                    + "and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.roundId="+roundId+" "
                    + "and tnid.isRepeatOrder='yes' and ts.noaId=tnid.noaIssueId");
        } catch (Exception e) {
            logger.error("getNOAListingForRO : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListingForRO : " + logUserId + " Ends");
        return obj;
    }
    public List<Object[]> getNOAListingForROForPF(int tenderId,int roundId) {
        //return hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tnid.contractSignDt,tls.lotNo,tls.lotDesc,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid, TblTenderLotSecurity tls where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and tls.appPkgLotId = tnid.pkgLotId and tnid.tblTenderMaster.tenderId=" +tenderId);
        logger.debug("getNOAListingForROForPF : " + logUserId + " Starts");
        List<Object[]> obj = null;

        try {
            obj = hibernateQueryDao.createQuery("select tnid.pkgLotId,tnid.noaIssueId,tnid.contractNo,"
                    + "tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tls.lotNo,tls.lotDesc,"
                    + "tnid.userId,tnid.companyName,tnid.contractName from TblNoaAcceptance tna,"
                    + " TblNoaIssueDetails tnid, TblTenderLotSecurity tls where "
                    + "tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and "
                    + "tls.tblTenderMaster.tenderId =tnid.tblTenderMaster.tenderId and"
                    + " tls.appPkgLotId = tnid.pkgLotId and tna.acceptRejStatus = 'approved' "
                    + "and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.roundId="+roundId+" "
                    + "and tnid.isRepeatOrder='yes'");
        } catch (Exception e) {
            logger.error("getNOAListingForROForPF : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListingForROForPF : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetching information from TblNoaAcceptance, TblNoaIssueDetails
     * @param tenderId of Tbl_TenderMaster
     * @return List of Objects
     */
    public List<Object[]> getDetailsNOAService(int tenderId) {
        logger.debug("getDetailsNOAService : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tnid.noaIssueId,tnid.contractNo,tnid.createdDt,tna.acceptRejStatus,tna.acceptRejDt,tnid.userId from TblNoaAcceptance tna, TblNoaIssueDetails tnid where tna.tblNoaIssueDetails.noaIssueId=tnid.noaIssueId and tnid.tblTenderMaster.tenderId=" + tenderId + " and pkglotId=0");
        } catch (Exception e) {
            logger.error("getDetailsNOAService : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOAService : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetching information from TblNoaAcceptance,TblNoaIssueDetails
     * @param noaIssueId
     * @return List of objects
     */
    public List<Object[]> getListNOA(int noaIssueId) {
        logger.debug("getListNOA : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tna.noaAcceptId,tna.comments,tna.acceptRejDt,tna.acceptRejStatus,tna.acceptRejType,tnid.noaIssueId,tnid.tblTenderMaster.tenderId,tnid.pkgLotId,tnid.contractNo,tnid.contractDt,tnid.userId,tnid.contractAmt,tnid.contractAmtWords,tnid.noaIssueDays,tnid.noaAcceptDt,tnid.perfSecAmt,tnid.perfSecAmtWords,tnid.perSecSubDays,tnid.perSecSubDt,tnid.contractSignDays,tnid.contractSignDt,tnid.createdBy,tnid.createdDt,tnid.contractName,tnid.clauseRef from TblNoaAcceptance tna,TblNoaIssueDetails tnid where tna.tblNoaIssueDetails.noaIssueId = tnid.noaIssueId and tnid.noaIssueId=" + noaIssueId);
        } catch (Exception e) {
            logger.error("getListNOA : " + logUserId + " : " + e);
        }
        logger.debug("getListNOA : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetching information from TblNoaIssueDetails
     * @param naoIssueId
     * @return List of TblNoaIssueDetails
     */
    public List<TblNoaIssueDetails> getDetailsCS(int noaIssueId) {
        logger.debug("getDetailsCS : " + logUserId + " Starts");
        List<TblNoaIssueDetails> list = null;
        try {
            list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("noaIssueId", Operation_enum.EQ, noaIssueId);
        } catch (Exception e) {
            logger.error("getDetailsCS : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsCS : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblTenderPayment
     * @param tenderId of Tbl_TenderMaster
     * @param pkgLotId
     * @param userId
     * @return List of TblTenderPayment
     */
    public List<TblTenderPayment> getPaymentDetails(int tenderId, int pkgLotId, int userId) {
        logger.debug("getPaymentDetails : " + logUserId + " Starts");
        List<TblTenderPayment> list = null;
        try {
            list = tblTenderPaymentDao.findTblTenderPayment("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "pkgLotId", Operation_enum.EQ, pkgLotId, "userId", Operation_enum.EQ, userId, "paymentFor", Operation_enum.LIKE, "Performance Security","isLive",Operation_enum.EQ,"yes","isVerified",Operation_enum.EQ,"yes");
        } catch (Exception e) {
            logger.error("getPaymentDetails : " + logUserId + " : " + e);
        }
        logger.debug("getPaymentDetails : " + logUserId + " Ends");
        return list;
    }

    public List<TblTenderPayment> getPaymentDetailsForRO(int tenderId, int pkgLotId, int userId,int noaId) {
        logger.debug("getPaymentDetailsForRO : " + logUserId + " Starts");
        List<TblTenderPayment> list = null;
        try {
            list = tblTenderPaymentDao.findTblTenderPayment("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "pkgLotId", Operation_enum.EQ, pkgLotId, "userId", Operation_enum.EQ, userId, "paymentFor", Operation_enum.LIKE, "Performance Security","isLive",Operation_enum.EQ,"yes","isVerified",Operation_enum.EQ,"yes","extValidityRef",Operation_enum.EQ,noaId);
        } catch (Exception e) {
            logger.error("getPaymentDetailsForRO : " + logUserId + " : " + e);
        }
        logger.debug("getPaymentDetailsForRO : " + logUserId + " Ends");
        return list;
    }


    /**
     * insert into TblContractSign
     * @param contractSignDtBean
     * return true if inserted successfully else false
     */
    public boolean addTblContractSign(TblContractSign tblContractSign) {
        logger.debug("addTblContractSign : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblContractSignDao.addTblContractSign(tblContractSign);
            flag = true;
        } catch (Exception e) {
            logger.error("addTblContractSign : " + logUserId + " : " + e);
        }
        logger.debug("addTblContractSign : " + logUserId + " Ends");
        return flag;
    }

    /**
     * fetching information from TblContractSign
     * @param noaIssueId
     * @return true or false
     */
    public boolean isAvlTCS(int noaIssueId) {
        logger.debug("isAvlTCS : " + logUserId + " Starts");
        boolean flag = false;
        try {
            List<TblContractSign> tblContractSign = tblContractSignDao.findTblContractSign("noaId", Operation_enum.EQ, noaIssueId);
            int size = tblContractSign.size();
            if (size > 0) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("isAvlTCS : " + logUserId + " : " + e);
        }
        logger.debug("isAvlTCS : " + logUserId + " Ends");
        return flag;
    }

    /**
     * fetching information from TblContractSign
     * @param noaIssueId - noaId of Tbl_ContractSign
     * @return true or false
     */
    public List<TblContractSign> getCSID(int noaIssueId) {
        logger.debug("getCSID : " + logUserId + " Starts");
        List<TblContractSign> list = null;
        try {
            list = tblContractSignDao.findTblContractSign("noaId", Operation_enum.EQ, noaIssueId);
        } catch (Exception e) {
            logger.error("getCSID : " + logUserId + " : " + e);
        }
        logger.debug("getCSID : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblContractSign
     * @param contractSignId
     * @return List of TblContractSign
     */
    public List<TblContractSign> getDetailsContractSign(int contractSignId) {
        logger.debug("getDetailsContractSign : " + logUserId + " Starts");
        List<TblContractSign> list = null;
        try {
            list = tblContractSignDao.findTblContractSign("contractSignId", Operation_enum.EQ, contractSignId);
        } catch (Exception e) {
            logger.error("getDetailsContractSign : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsContractSign : " + logUserId + " Ends");
        return list;
    }

    /**
     * updating information into TblContractSign
     * @param contractSignDtBean
     * @return true or false
     */
    public boolean updateContractSign(TblContractSign tblContractSign) {
        logger.debug("updateContractSign : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblContractSignDao.updateTblContractSign(tblContractSign);
            flag = true;
        } catch (Exception e) {
            logger.error("updateContractSign : " + logUserId + " : " + e);
        }
        logger.debug("updateContractSign : " + logUserId + " Ends");
        return flag;
    }
    //contract Signing listing bidder side

    /**
     * fetching information from TblContractSign ,TblNoaIssueDetails
     * @param tenderId
     * @param userId
     * @return List Of objects
     */
    public List<Object[]> getCSListingBidder(int tenderId, int userId) {
        logger.debug("getCSListingBidder : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tcs.contractSignId,tcs.lastDtContractDt,tcs.contractSignDt,tcs.contractSignLocation, tcs.isPubAggOnWeb,tcs.createdDt,tcs.createdBy,tcs.witnessInfo,tcs.noaId,tnid.pkgLotId,tnid.roundId from TblContractSign tcs,TblNoaIssueDetails tnid where tcs.noaId = tnid.noaIssueId and tnid.userId = " + userId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.isRepeatOrder='no'");
        } catch (Exception e) {
            logger.error("getCSListingBidder : " + logUserId + " : " + e);
        }
        logger.debug("getCSListingBidder : " + logUserId + " Ends");
        return obj;
    }
    public List<Object[]> getCSListingBidderForRO(int tenderId, int userId,int roundId) {
        logger.debug("getCSListingBidder : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select tcs.contractSignId,tcs.lastDtContractDt,tcs.contractSignDt,tcs.contractSignLocation, tcs.isPubAggOnWeb,tcs.createdDt,tcs.createdBy,tcs.witnessInfo,tcs.noaId from TblContractSign tcs,TblNoaIssueDetails tnid where tcs.noaId = tnid.noaIssueId and tnid.userId = " + userId + " and tnid.tblTenderMaster.tenderId=" + tenderId+" and tnid.roundId="+roundId+" and tnid.isRepeatOrder='yes'");
        } catch (Exception e) {
            logger.error("getCSListingBidder : " + logUserId + " : " + e);
        }
        logger.debug("getCSListingBidder : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetch data form tbl_tenderMaster
     * @param tenderId
     * return List of objects
     */
    public List<Object[]> getDataTenderDetails(String tenderId) {
        logger.debug("getDataTenderDetails : " + logUserId + " Starts");
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createQuery("select reoiRfpRefNo,peOfficeName from TblTenderDetails where tblTenderMaster.tenderId = " + tenderId);
        } catch (Exception e) {
            logger.error("getDataTenderDetails : " + logUserId + " : " + e);
        }
        logger.debug("getDataTenderDetails : " + logUserId + " Ends");
        return obj;
    }

    /**
     * fetch email id from Tbl_LoginMaster
     * @param userId of Tbl_LoginMaster
     * return email id of particular user
     */
    public List<Object> getEmailIdNOA(int userId) {
        logger.debug("getEmailIdNOA : " + logUserId + " Starts");
        List<Object> obj = null;
        try {
            obj = hibernateQueryDao.singleColQuery("select emailId from TblLoginMaster where userId=" + userId);
        } catch (Exception e) {
            logger.error("getEmailIdNOA : " + logUserId + " : " + e);
        }
        logger.debug("getEmailIdNOA : " + logUserId + " Ends");
        return obj;
    }
    /**
     * fetch data form Tbl_LoginMaster
     * @param userId
     * return List of objects
     */
    public List<TblNoaIssueDetails> getUserIdNOA(int noaid) {
        logger.debug("getUserIdNOA : " + logUserId + " Starts");
        List<TblNoaIssueDetails> list = new ArrayList<TblNoaIssueDetails>();
        try {
            list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("noaIssueId",Operation_enum.EQ,noaid);
        } catch (Exception e) {
            logger.error("getUserIdNOA : " + logUserId + " : " + e);
        }
        logger.debug("getUserIdNOA : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetch data form tbl_TenderLotSecurity
     * @param appPkgLotId
     * return list
     */
    public List<TblTenderLotSecurity> getLotDetailsNOA(int appPkgLotId) {
        logger.debug("getLotDetailsNOA : " + logUserId + " Starts");
        List<TblTenderLotSecurity> listing = null;
        try {
            listing = tblTenderLotSecurityDao.findTblTenderLotSecurity("appPkgLotId", Operation_enum.EQ, appPkgLotId);
        } catch (Exception e) {
            logger.error("getLotDetailsNOA : " + logUserId + " : " + e);
        }
        logger.debug("getLotDetailsNOA : " + logUserId + " Ends");
        return listing;
    }

    /**
     * checking unique contract no
     * @param contractNo
     * @return string containing message that it exists or not
     */
    public String verifyContractNo(String contractNo) {
        int size = 0;
        String msg = null;
        logger.debug("verifyContractNo : " + logUserId + " Starts");
        try {
            Object[] values = {"contractNo", Operation_enum.LIKE, contractNo};
            size = tblNoaIssueDetailsDao.findTblNoaIssueDetails(values).size();
        } catch (Exception ex) {
            logger.error("verifyContractNo : " + logUserId + " :" + ex);
        }
        if (size > 0) {
            msg = "Contract No. must be unique";
        } else {
            msg = "OK";
        }
        logger.debug("verifyContractNo : " + logUserId + " Ends");
        return msg;
    }

    /**
     * fetching rept type
     * @param tenderId of tbl_TenderMaster
     * @return rept type
     */
    public String getReptType(int tenderId) {
        String reptType = null;
        logger.debug("getReptType : " + logUserId + " Starts");
        try {
            for (TblReportMaster tblReportMaster : tblReportMasterDao.findTblReportMaster("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"isTorter",Operation_enum.EQ,"TER")) {
                reptType = tblReportMaster.getReportType();
            }
        } catch (Exception ex) {
            logger.error("getReptType : " + logUserId + " :" + ex);
        }
        logger.debug("getReptType : " + logUserId + " Ends");
        return reptType;
    }

    /**
     * Fetching data from Tbl_NoaIssueDetails
     * @param tenderId of tbl_TenderMaster
     * @param contUserId
     * @param lotId
     * @return list of TblNoaIssueDetails
     */
    public List<TblNoaIssueDetails> getPerformanceSecAmt(int tenderId, int contUserId, int lotId) {
        logger.debug("getDetailsCS : " + logUserId + " Starts");
        List<TblNoaIssueDetails> list = null;
        try {
            list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "pkgLotId", Operation_enum.EQ, lotId, "userId", Operation_enum.EQ, contUserId);
        } catch (Exception e) {
            logger.error("getDetailsCS : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsCS : " + logUserId + " Ends");
        return list;
    }

    public boolean entryForUniqueIssueNOA(int userId,int tenderId,boolean flag, int lotId) {
        logger.debug("entryForUniqueIssueNOA : " + logUserId + " Starts");
        boolean flagg = false;
        List<TblNoaIssueDetails> list = null;
        try {
            if(flag)
            {
                list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "userId", Operation_enum.EQ, userId,"isRepeatOrder", Operation_enum.EQ, "yes","pkgLotId",Operation_enum.EQ,lotId);
            }
            else{
                list = tblNoaIssueDetailsDao.findTblNoaIssueDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId), "userId", Operation_enum.EQ, userId,"isRepeatOrder", Operation_enum.EQ, "no","pkgLotId",Operation_enum.EQ,lotId);
            }
            if(list==null || list.isEmpty()){
                flagg = true;
            }
        } catch (Exception e) {
            logger.error("entryForUniqueIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("entryForUniqueIssueNOA : " + logUserId + " Ends");
        return flagg;
    }
}
