/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblFinancialYearDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.service.serviceinterface.FinancialYearService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class FinancialYearServiceImpl  implements FinancialYearService{

    private static final Logger LOGGER = Logger.getLogger(FinancialYearServiceImpl.class);
    private String logUserId ="0";
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    private TblFinancialYearDao tblFinancialYearDao;

    public TblFinancialYearDao getTblFinancialYearDao() {
        return tblFinancialYearDao;
    }

    public void setTblFinancialYearDao(TblFinancialYearDao tblFinancialYearDao) {
        this.tblFinancialYearDao = tblFinancialYearDao;
    }

    private HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    /**
     * Method for adding the FinanacialYear details and also updating the previous status of Default FinancialYear field.
     * @param status
     * @param FinancialYear
     * @return
     */
    @Override
    public String addFinancialYearService(String status, TblFinancialYear FinancialYear) {

        LOGGER.debug("addFinancialYearService : "+logUserId+" Starts");
        String action=null;
        String str = "";
        try{         
            if("Yes".equals(status)){ //checking for value of input field about DefaultFinancialYear.
                    TblFinancialYear tblFinancialYear =new TblFinancialYear();
                    List<TblFinancialYear> list = tblFinancialYearDao.findTblFinancialYear("isCurrent",Operation_enum.EQ,status); //checking that if any Yes status is their in table.
                    if(list.isEmpty()){
                        tblFinancialYearDao.addTblFinancialYear(FinancialYear);
                        str = "Values Added";
                    }else{
                        Iterator it = list.iterator();
                        while(it.hasNext())
                        {
                            tblFinancialYear = (TblFinancialYear)it.next();
                        }
                         if(tblFinancialYear != null || status.equals(tblFinancialYear.getIsCurrent())){ //getting status(isCurrent) from table.
                            try{
                               //update the previous status.
                                tblFinancialYear.setIsCurrent("No");
                                tblFinancialYearDao.updateTblFinancialYear(tblFinancialYear); //updating the previous status.
                                tblFinancialYearDao.addTblFinancialYear(FinancialYear); //adding the new record of FinancialYear.
                                str =  "Values Added";
                            }catch(Exception ex){
                                LOGGER.error("addFinancialYearService : "+logUserId+" : "+ex.toString());
                            }
                    }
                    }
            }else{
                try{
                    tblFinancialYearDao.addTblFinancialYear(FinancialYear);
                    str = "Values Added";
                }catch(Exception ex){
                    LOGGER.error("addFinancialYearService : "+logUserId+" : "+ex.toString());
                }                
            }
          action = "Configure Financial Year";
        }catch(Exception ex){
            LOGGER.error("addFinancialYearService : "+logUserId+" : "+ex.toString());
            action = "Error in Configure Financial Year : "+ex;
       }finally{
            makeAuditTrailService.generateAudit(auditTrail,Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
       LOGGER.debug("addFinancialYearService : "+logUserId+" Ends");
       return str;
    }

    /**
     * Method to get all FinancialYear Details from Dao.
     * @return
     */
    @Override
    public List<TblFinancialYear> getFinancialYearDetails() {
        LOGGER.debug("getFinancialYearDetails : "+logUserId+" Starts");
        List<TblFinancialYear> details = null;
        try{
            details = tblFinancialYearDao.getAllTblFinancialYear(); //calling the Dao call to get the details.
        }catch(Exception ex){
           LOGGER.error("getFinancialYearDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getFinancialYearDetails : "+logUserId+" Ends");
            return details;
        }

    @Override
    public List<TblFinancialYear> getFinancialYearData(int id) {
        LOGGER.debug("getFinancialYearData : "+logUserId+" Starts");
        List<TblFinancialYear> list = null;
        try{
            list = getTblFinancialYearDao().findTblFinancialYear("financialYearId",Operation_enum.EQ,id);
        }catch(Exception ex){
           LOGGER.error("getFinancialYearData : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getFinancialYearData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public String updateFinancialYear(TblFinancialYear tblFinancialYear) {
        LOGGER.debug("updateFinancialYear : "+logUserId+" Starts");
        String str = "";
        String action = null;
        try{
            if(tblFinancialYear.getIsCurrent().equalsIgnoreCase("Yes")){
            hibernateQueryDao.updateDeleteNewQuery("update TblFinancialYear set isCurrent='No' where financialYearId!=0");
            }
            getTblFinancialYearDao().updateTblFinancialYear(tblFinancialYear);
            str =  "Value Updated";
            action = "Edit Financial Year Configuration";
        }catch(Exception ex){
            LOGGER.error("updateFinancialYear : "+logUserId+" : "+ex.toString());
            action = "Error in Edit Financial Year Configuration : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateFinancialYear : "+logUserId+" Ends");
        return str;
    }

    @Override
    public List<TblFinancialYear> getallFinancialYearDetails(int offset, int row) {
        LOGGER.debug("getallFinancialYearDetails "+logUserId+" Starts");
        List<TblFinancialYear> list = null;
        try {
                list = tblFinancialYearDao.findByCountTblFinancialYear(offset,row,"financialYear",Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception e) {
            LOGGER.error("getallFinancialYearDetails "+logUserId+" :"+e);
        }
        LOGGER.debug("getallFinancialYearDetails "+logUserId+" Ends");
        return list;
    }

    @Override
    public long countallFinancialYearDetails(){
        LOGGER.debug("countallFinancialYearDetails : "+logUserId+" Starts");
        long lng = 0;
        try {
          lng =  tblFinancialYearDao.getEntityCount();
        } catch (Exception ex) {
            LOGGER.error("countallFinancialYearDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("countallFinancialYearDetails : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
}
