/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderCellsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderColumnsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderFormulaDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderListCellDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderTablesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore;
import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.model.table.TblTenderColumns;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.model.table.TblTenderFormula;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import com.cptu.egp.eps.model.table.TblTenderTables;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yagnesh
 */
public class TenderTableService {

    static final Logger logger = Logger.getLogger(TenderTableService.class);
    private String logUserId = "0";
    HibernateQueryDao hibernateQueryDao;
    TblTenderTablesDao tblTenderTablesDao;
    TblTenderCellsDao tblTenderCellsDao;
    TblTenderColumnsDao tblTenderColumnsDao;
    TblTenderFormulaDao tblTenderFormulaDao;
    TblTenderListCellDetailDao tblTenderListCellDetailDao;
    MakeAuditTrailService makeAuditTrailService;
    SpGeteGPCmsDataMore spGeteGPCmsDataMore;
    AuditTrail auditTrail;

    public SpGeteGPCmsDataMore getSpGeteGPCmsDataMore() {
        return spGeteGPCmsDataMore;
    }

    public void setSpGeteGPCmsDataMore(SpGeteGPCmsDataMore spGeteGPCmsDataMore) {
        this.spGeteGPCmsDataMore = spGeteGPCmsDataMore;
    }

    
    public void setTblTenderCellsDao(TblTenderCellsDao tblTenderCellsDao) {
        this.tblTenderCellsDao = tblTenderCellsDao;
    }

    public void setTblTenderColumnsDao(TblTenderColumnsDao tblTenderColumnsDao) {
        this.tblTenderColumnsDao = tblTenderColumnsDao;
    }

    public void setTblTenderFormulaDao(TblTenderFormulaDao tblTenderFormulaDao) {
        this.tblTenderFormulaDao = tblTenderFormulaDao;
    }

    public void setTblTenderTablesDao(TblTenderTablesDao tblTenderTablesDao) {
        this.tblTenderTablesDao = tblTenderTablesDao;
    }

    public TblTenderCellsDao getTblTenderCellsDao() {
        return tblTenderCellsDao;
    }

    public TblTenderColumnsDao getTblTenderColumnsDao() {
        return tblTenderColumnsDao;
    }

    public TblTenderFormulaDao getTblTenderFormulaDao() {
        return tblTenderFormulaDao;
    }

    public TblTenderTablesDao getTblTenderTablesDao() {
        return tblTenderTablesDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public TblTenderListCellDetailDao getTblTenderListCellDetailDao() {
        return tblTenderListCellDetailDao;
    }

    public void setTblTenderListCellDetailDao(TblTenderListCellDetailDao tblTenderListCellDetailDao) {
        this.tblTenderListCellDetailDao = tblTenderListCellDetailDao;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    

    /**
     * Add Tender Table.
     * @param tblTables
     * @param tenderId
     * @return true if insert perform successfully else return false.
     */
    public boolean addFormTable(TblTenderTables tblTables,int tenderId) {
        logger.debug("addFormTable : " + logUserId + " Starts");
        boolean boolAddFormTable = false;
        String auditAction = "";
        int auditappId = tenderId;
        try {
            tblTenderTablesDao.addTblTenderTables(tblTables);
            boolAddFormTable = true;
            auditAction = "Create Table";
        }catch (Exception ex) {
            logger.error("addFormTable : " + logUserId + ex);
            auditAction = "Error in Create Table";
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, tblTables.getTableName());
            auditAction = null;
        }
        logger.debug("addFormTable : " + logUserId + " Ends");
        return boolAddFormTable;
    }

    /**
     * Edit Tender Table
     * @param tblTables
     * @param tenderId
     * @return true if edit perform successfully else return false.
     */
     public boolean editFormTable(TblTenderTables tblTables,int tenderId) {
        logger.debug("editFormTable : " + logUserId + " Starts");
        boolean boolEditFormTable = false;
        String auditAction = "";
        int auditappId = tenderId;
        try {
            tblTenderTablesDao.updateTblTenderTables(tblTables);
            boolEditFormTable = true;
            auditAction = "Edit Table Details";
        } catch (Exception ex) {
            logger.error("editFormTable : " + logUserId + ex);
            auditAction = "Error in Edit Table Details";
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, tblTables.getTableName());
            auditAction = null;
        }
        
        logger.debug("editFormTable : " + logUserId + " Ends");
        return boolEditFormTable;
    }

     /**
      * Delete Tender Table
      * @param tableId
      * @param tenderId
      * @return true if delete perform successfully else return false.
      */
    public boolean deleteFormTable(int tableId, int tenderId) {
        logger.debug("deleteFormTable : " + logUserId + " Starts");
        boolean bSuccess = false;
        String auditAction = "";
        int auditappId = tenderId;
        
        List<TblTenderTables> tblTable;
        try {
            tblTable = tblTenderTablesDao.findTblTenderTables("tenderTableId", Operation_enum.EQ, tableId);
        } catch (Exception ex) {
            logger.error("getFormTablesDetails : " + logUserId + ex);
            tblTable = null;
        }
        String tableName = tblTable.get(0).getTableName();
        
        try {
            String query = "delete from TblTenderTables ttt where ttt.tenderTableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess = true;
            auditAction = "Delete Table";
        } catch (Exception ex) {
            logger.error("deleteFormTable : " + logUserId + ex);
            bSuccess =  false;
            auditAction = "Error in Delete Table";
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, tableName);
            auditAction = null;
        }
        logger.debug("deleteFormTable : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Get Tender Table Details
     * @param formId
     * @param values
     * @return List of Tender Table Details
     */
    public List<TblTenderTables> getFormTables(int formId,String...values) {
        logger.debug("getFormTables : " + logUserId + " Starts");
        List<TblTenderTables> tblTables = new ArrayList<TblTenderTables>();
        String action = "Tender Document Forms View";
        try {
            tblTables = tblTenderTablesDao.findTblTenderTables("tblTenderForms", Operation_enum.EQ, new TblTenderForms(formId));
        } catch (Exception ex) {
            logger.error("getFormTables : " + logUserId + ex);
            tblTables = null;
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
            if(values!=null && values.length>0){
                makeAuditTrailService.generateAudit(auditTrail,Integer.parseInt(values[0]) , "tenderId", EgpModule.Tender_Document.getName(), action, "Table Name : "+tblTables.get(0).getTableName());
            }
            action = null;
        }
        logger.debug("getFormTables : " + logUserId + " Ends");
        return tblTables;
    }

    /**
     * Get Tender Table Details
     * @param tableId
     * @return List of Tender Table Details
     */
    public List<TblTenderTables> getFormTablesDetails(int tableId) {
        logger.debug("getFormTablesDetails : " + logUserId + " Starts");
        List<TblTenderTables> tblTable;
        try {
            tblTable = tblTenderTablesDao.findTblTenderTables("tenderTableId", Operation_enum.EQ, tableId);
        } catch (Exception ex) {
            logger.error("getFormTablesDetails : " + logUserId + ex);
            tblTable = null;
        }
        logger.debug("getFormTablesDetails : " + logUserId + " Ends");
        return tblTable;
    }

    /**
     * Check if entry in tender column exist for given table?
     * @param tableId
     * @return ture if it found any single or more than one column exist else return false.
     */
    public boolean isEntryPresentInTableCols(int tableId) {
        logger.debug("isEntryPresentInTableCols : " + logUserId + " Starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderColumns ttc", "ttc.tblTenderTables.tenderTableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }
        } catch (Exception ex) {
            logger.error("isEntryPresentInTableCols : " + logUserId + ex);
        }
        logger.debug("isEntryPresentInTableCols : " + logUserId + " Ends");
            return isPresent;
        }

    /**
     * GEt column details like fill by, data type, size, etc.
     * @param tableId
     * @param inSortOrder
     * @return
     */
    public List<TblTenderColumns> getTableColumns(int tableId, boolean inSortOrder) {
        logger.debug("getTableColumns : " + logUserId + " Starts");
        List<TblTenderColumns> tblTableCols;
        try {
            if (inSortOrder) {
                tblTableCols = tblTenderColumnsDao.findTblTenderColumns("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "sortOrder", Operation_enum.ORDERBY, Operation_enum.ASC);
            } else {
                tblTableCols = tblTenderColumnsDao.findTblTenderColumns("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId));
            }
        } catch (Exception ex) {
            logger.error("getTableColumns : " + logUserId + ex);
            tblTableCols = null;
        }
        logger.debug("getTableColumns : " + logUserId + " Ends");
        return tblTableCols;
    }

    /**
     * Get Tender Table Cell Details for Test Form
     * @param tableId
     * @return Tender Cell details list.
     */
    public List<TblTenderCells> getTableCellsTestForm(int tableId) {
        logger.debug("getTableCellsTestForm : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.tenderCelId, ttc.tblTenderTables, ttc.rowId, ttc.cellDatatype, ");
        strQuery.append(" ttc.cellvalue, ttc.columnId, ttc.templateTableId, ttc.templateColumnId, ttc.cellId, ");
        strQuery.append(" tc.showorhide ");
        strQuery.append(" from TblTenderCells ttc, TblTenderColumns tc ");
        strQuery.append(" where tc.tblTenderTables.tenderTableId = ttc.tblTenderTables.tenderTableId and ttc.columnId = tc.columnId ");
        strQuery.append(" and ttc.tblTenderTables.tenderTableId = " + tableId + " order by ttc.rowId asc, tc.sortOrder asc");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderCells> tblTableCells = new ArrayList<TblTenderCells>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            logger.debug("getTableCellsTestForm -- : " + objs.size());
            for (Object[] i : objs) {
                tblTableCells.add(new TblTenderCells(
                        Integer.parseInt(i[0].toString()),
                        (TblTenderTables) i[1],
                        Integer.parseInt(i[2].toString()),
                        Byte.parseByte(i[3].toString()),
                        i[4].toString(),
                        Short.parseShort(i[5].toString()),
                        Integer.parseInt(i[6].toString()),
                        Short.parseShort(i[7].toString()),
                        Integer.parseInt(i[8].toString()),
                        Byte.parseByte(i[9].toString())));
                //tblTableCells.add(new TblTenderCells(Byte.parseByte(i[0].toString()), i[1].toString(), Byte.parseByte(i[2].toString())));
            }
        } catch (Exception ex) {
            logger.error("getTableCellsTestForm : " + logUserId + ex);
            tblTableCells = null;
        }
        logger.debug("getTableCellsTestForm : " + logUserId + " Ends");
        return tblTableCells;
    }

    /**
     * Get Table Cell details
     * @param tableId
     * @return Cell Detail list
     */
    public List<TblTenderCells> getTableCells(int tableId) {
        logger.debug("getTableCells : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.cellDatatype, ttc.cellvalue, tc.showorhide, ttc.columnId , ttc.cellId ");
        strQuery.append(" from TblTenderCells ttc, TblTenderColumns tc ");
        strQuery.append(" where tc.tblTenderTables.tenderTableId = ttc.tblTenderTables.tenderTableId and ttc.columnId = tc.columnId ");
        strQuery.append(" and ttc.tblTenderTables.tenderTableId = " + tableId + " order by ttc.rowId asc, tc.sortOrder asc");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderCells> tblTableCells = new ArrayList<TblTenderCells>();
        try {
            //tblTableCells = tblTenderCellsDao.findTblTenderCells("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "rowId", Operation_enum.ORDERBY, Operation_enum.ASC, "columnId", Operation_enum.ORDERBY, Operation_enum.ASC);
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            logger.debug("getTableCells -- : " + objs.size());
            for (Object[] i : objs) {
                //tblTableCells.add(new TblTemplateCells(Integer.parseInt(i[0].toString()),new TblTemplateTables(Integer.parseInt(i[1].toString())),Short.parseShort(i[2].toString()),Integer.parseInt(i[3].toString()),Integer.parseInt(i[4].toString()),Byte.parseByte(i[5].toString()),i[6].toString()));
                tblTableCells.add(new TblTenderCells(Byte.parseByte(i[0].toString()), i[1].toString(), Byte.parseByte(i[2].toString()), Short.parseShort(i[3].toString()), Integer.parseInt(i[4].toString())));
            }
        } catch (Exception ex) {
            logger.error("getTableCells : " + logUserId + ex);
            tblTableCells = null;
        }
        logger.debug("getTableCells : " + logUserId + " Ends");
        return tblTableCells;
    }


    /**
     * Get Cell details for Consolidate.
     * @param tableId
     * @return Cell Details
     */
    public List<TblTenderCells> getCelldetailsForConsolidate(int tableId) {
        logger.debug("getTableCells : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.cellDatatype, ttc.cellvalue, tc.showorhide, ttc.columnId , ttc.cellId ");
        strQuery.append(" from TblTenderCells ttc, TblTenderColumns tc ");
        strQuery.append(" where tc.tblTenderTables.tenderTableId = ttc.tblTenderTables.tenderTableId and ttc.columnId = tc.columnId and tc.columnId<=6 ");
        strQuery.append(" and ttc.tblTenderTables.tenderTableId = " + tableId + " order by ttc.rowId asc, tc.sortOrder asc");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderCells> tblTableCells = new ArrayList<TblTenderCells>();
        try {
            //tblTableCells = tblTenderCellsDao.findTblTenderCells("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "rowId", Operation_enum.ORDERBY, Operation_enum.ASC, "columnId", Operation_enum.ORDERBY, Operation_enum.ASC);
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            logger.debug("getTableCells -- : " + objs.size());
            for (Object[] i : objs) {
                //tblTableCells.add(new TblTemplateCells(Integer.parseInt(i[0].toString()),new TblTemplateTables(Integer.parseInt(i[1].toString())),Short.parseShort(i[2].toString()),Integer.parseInt(i[3].toString()),Integer.parseInt(i[4].toString()),Byte.parseByte(i[5].toString()),i[6].toString()));
                tblTableCells.add(new TblTenderCells(Byte.parseByte(i[0].toString()), i[1].toString(), Byte.parseByte(i[2].toString()), Short.parseShort(i[3].toString()), Integer.parseInt(i[4].toString())));
            }
        } catch (Exception ex) {
            logger.error("getTableCells : " + logUserId + ex);
            tblTableCells = null;
        }
        logger.debug("getTableCells : " + logUserId + " Ends");
        return tblTableCells;
    }



    /**
     * Update noofrows, noocols count for tender table.
     * @param tableId
     * @param noOfRows
     * @param noOfCols
     * @return true if update perform successfully else return false.
     */
    public boolean updateColNRowCnt(int tableId, short noOfRows, short noOfCols) {
        logger.debug("updateColNRowCnt : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            String query = "update TblTenderTables set noOfRows = " + noOfRows + ", noOfCols = " + noOfCols + " where tenderTableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess =  true;
        } catch (Exception ex) {
            logger.error("updateColNRowCnt : " + logUserId + ex);
            bSuccess =  false;
        }
        logger.debug("updateColNRowCnt : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Update only row count for tender table.
     * @param tableId
     * @return true if update perform successfully  else return false.
     */
    public boolean updateOnlyRowCnt(int tableId) {
        logger.debug("updateOnlyRowCnt : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            String query = "update TblTenderTables set noOfRows = noOfRows + 1 where tenderTableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess =  true;
        } catch (Exception ex) {
            logger.error("updateOnlyRowCnt : " + logUserId + ex);
            bSuccess  = false;
        }
        logger.debug("updateOnlyRowCnt : " + logUserId + " Ends");
        return bSuccess;

    }

    /**
     * Get No of Table in Form as count value
     * @param formId
     * @return no of table count in given form
     */
    public short getNoOfTablesInForm(int formId) {
        logger.debug("getNoOfTablesInForm : " + logUserId + " Starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTenderTables ttt", "ttt.tblTenderForms.tenderFormId = " + formId);
        } catch (Exception ex) {
            logger.error("getNoOfTablesInForm : " + logUserId + ex);
            cnt = 0;
        }
        logger.debug("getNoOfTablesInForm : " + logUserId + " Ends");
            return cnt;
        }

    /**
     * Get No. of column count in given table.
     * @param tableId
     * @return no. of. column count.
     */
    public short getNoOfColsInTable(int tableId) {
        logger.debug("getNoOfColsInTable : " + logUserId + " Starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTenderColumns ttc", "ttc.tblTenderTables.tenderTableId = " + tableId);
        } catch (Exception ex) {
            logger.error("getNoOfColsInTable : " + logUserId + ex);
           cnt = 0;
        }
        logger.debug("getNoOfColsInTable : " + logUserId + " Ends");
            return cnt;
        }

    /**
     * Get No. of Rows in table.
     * @param tableId
     * @param columnId
     * @return no. of.rows in table.
     */
    public short getNoOfRowsInTable(int tableId, short columnId) {
        logger.debug("getNoOfRowsInTable : " + logUserId + " Starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTenderCells ttc", "ttc.tblTenderTables.tenderTableId = " + tableId + " and ttc.columnId = " + columnId);
        } catch (Exception ex) {
            logger.error("getNoOfRowsInTable : " + logUserId + ex);
            cnt = 0;
        }
        logger.debug("getNoOfRowsInTable : " + logUserId + " Ends");
            return cnt;
        }

    /**
     * Get Auto column details.
     * @param tableId
     * @return Get list of AUto column details
     */
    public List<TblTenderColumns> getTableAutoColumns(int tableId) {
        logger.debug("getTableAutoColumns : " + logUserId + " Starts");
        String strQuery = "select ttc.columnId, ttc.columnHeader from TblTenderColumns ttc where ttc.tblTenderTables.tenderTableId = " + tableId + " and ttc.filledBy = 3 and ttc.columnId not in (select ttf.columnId from TblTenderFormula ttf where ttf.tenderTableId = " + tableId + ")";
        List<TblTenderColumns> cols = new ArrayList<TblTenderColumns>();
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            for (Object[] i : objs) {
                cols.add(new TblTenderColumns(Short.parseShort(i[0].toString()), (String) i[1]));
            }
        } catch (Exception ex) {
            logger.error("getTableAutoColumns : " + logUserId + ex);
            objs = null;
            cols =  null;
        }
        logger.debug("getTableAutoColumns : " + logUserId + " Ends");
        return cols;
    }

    /**
     * Get Tender Table formula Details for given table id.
     * @param tableId
     * @return Tender formula details
     */
    public List<TblTenderFormula> getTableFormulas(int tableId) {
        logger.debug("getTableFormulas : " + logUserId + " Starts");
        List<TblTenderFormula> tblTableFormulas;
        try {
            tblTableFormulas = tblTenderFormulaDao.findTblTenderFormula("tenderTableId", Operation_enum.EQ, tableId);
        } catch (Exception ex) {
            logger.error("getTableFormulas : " + logUserId + ex);
            tblTableFormulas = null;
        }
        logger.debug("getTableFormulas : " + logUserId + " Ends");
        return tblTableFormulas;
    }

    /**
     * Add Tender Formula
     * @param tblFormulas
     * @return true if insertion perform successfully else return false
     */
    public boolean addTableFormula(TblTenderFormula tblFormulas) {
        logger.debug("addTableFormula : " + logUserId + " Starts");
        boolean boolAddFormTable = false;
        try {
            tblTenderFormulaDao.addTblTenderFormula(tblFormulas);
            boolAddFormTable = true;
        } catch (Exception ex) {
            logger.error("addTableFormula : " + logUserId + ex);
        }

        logger.debug("addTableFormula : " + logUserId + " Ends");
        return boolAddFormTable;
    }

    /**
     * Get Maximum cell count for given table id
     * @param tableId
     * @return maximum cell count of tender table
     */
    public int getMaxCellCountForTable(int tableId) {
        logger.debug("getMaxCellCountForTable : " + logUserId + " Starts");
        List<Object> obj;
        int maxId = -1;
        try {
            String query = "select max(ttc.cellId) as maxCnt from TblTenderCells ttc where ttc.tblTenderTables.tenderTableId = " + tableId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null) {
                if (obj.size() > 0) {
                    maxId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }
        } catch (Exception ex) {
            logger.error("getMaxCellCountForTable : " + logUserId + ex);
            maxId = -1;
        }
        logger.debug("getMaxCellCountForTable : " + logUserId + " Ends");
            return maxId;
    }

    /**
     * Get Tender column details
     * @param tableId
     * @return tender column detail list
     */
    public List<TblTenderColumns> getTenderColumns(int tableId){
        
        logger.debug("getTenderColumns : " + logUserId + " Starts");
        List<TblTenderColumns> list = new ArrayList<TblTenderColumns>();
        try {
            list = tblTenderColumnsDao.findTblTenderColumns("tblTenderTables.tenderTableId", Operation_enum.EQ, tableId);
        } catch (Exception ex) {
            logger.error("getTenderColumns : " + logUserId + ex);
        }
        logger.debug("getTenderColumns : " + logUserId + " Ends");
        return list;
    }

    /**
     * Delete tender table formula.
     * @param columns
     * @param tableId
     * @return true if delete done successfully else return false.
     */
    public boolean deleteTableFormula(String columns, int tableId) {
        logger.debug("deleteTableFormula : " + logUserId + " Starts");
        boolean  bSuccess = false;
        try {
            String query = "delete from TblTenderFormula ttt where ttt.tenderTableId =" + tableId + " and ttt.columnId in (" + columns + ")";
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("deleteTableFormula : " + logUserId + ex);
            bSuccess =  false;
        }
        logger.debug("deleteTableFormula : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Check if auto column exist in given table id?
     * @param tableId
     * @return true if auto column exist else return false.
     */
    public boolean isAutoColumnPresent(int tableId) {
        logger.debug("isAutoColumnPresent : " + logUserId + " Starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderColumns ttc", "ttc.tblTenderTables.tenderTableId = " + tableId + " and ttc.filledBy = 3");
            if (cnt > 0) {
                isPresent = true;
            }
        } catch (Exception ex) {
            logger.error("isAutoColumnPresent : " + logUserId + ex);
            isPresent = false;
        }
        logger.debug("isAutoColumnPresent : " + logUserId + " Ends");
            return isPresent;
        }

    /**
     * Check at least one formula created for given table id?
     * @param tableId
     * @return true if at least one formula exist else return false.
     */
    public boolean atLeastOneFormulaCreated(int tableId) {
        logger.debug("atLeastOneFormulaCreated : " + logUserId + " Starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderFormula ttf", "ttf.tenderTableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }
        } catch (Exception ex) {
            logger.error("atLeastOneFormulaCreated : " + logUserId + ex);
            isPresent = false;
        }
        logger.debug("atLeastOneFormulaCreated : " + logUserId + " Ends");
            return isPresent;
        }

    /**
     * Check if Formula type 'TOTAL' exist for given table id.
     * @param tableId
     * @return true if exist else return false.
     */
    public boolean isTotalFormulaCreated(int tableId) {
        logger.debug("isTotalFormulaCreated : " + logUserId + " Starts");
        long cnt = 0;
        boolean isPresent = false;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblTenderFormula ttf", "ttf.isGrandTotal = 'yes' and ttf.tenderTableId = " + tableId);
            if (cnt > 0) {
                isPresent = true;
            }
        } catch (Exception ex) {
            logger.error("isTotalFormulaCreated : " + logUserId + ex);
            isPresent = false;
        }
        logger.debug("isTotalFormulaCreated : " + logUserId + " Ends");
            return isPresent;
        }

    /**
     * Get Grand Total Column for given table id.
     * @param tableId
     * @return column id, isGrandTotal field of Grand Total.
     */
    public HashMap<Integer, Integer> getGTColumns(int tableId) {
        logger.debug("getGTColumns : " + logUserId + " Starts");
        String strQuery = "select ttf.columnId, ttf.isGrandTotal from TblTenderFormula ttf where ttf.tenderTableId = " + tableId + " and ttf.isGrandTotal = 'yes'";
        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderFormula> cols = new ArrayList<TblTenderFormula>();
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            Integer j = 0;
            for (Object[] i : objs) {
                cols.add(new TblTenderFormula(Short.parseShort(i[0].toString()), i[1].toString()));
                hm.put(j, (Integer) i[0]);
                j++;
            }
        } catch (Exception ex) {
            logger.error("getGTColumns : " + logUserId + ex);
            objs = null;
            hm =  null;
        }
        logger.debug("getGTColumns : " + logUserId + " Ends");
        return hm;
    }

    /**
     * Get count for 'Total' type formula
     * @param tableId
     * @return count of 'Total' type formula.
     */
    public short getFormulaCountOfTotal(int tableId) {
        logger.debug("getFormulaCountOfTotal : " + logUserId + " Starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTenderFormula ttf", "ttf.tenderTableId = " + tableId + " and ttf.formula like '%TOTAL%'");
        } catch (Exception ex) {
            logger.error("getFormulaCountOfTotal : " + logUserId + ex);
            cnt = 0;
        }
        logger.debug("getFormulaCountOfTotal : " + logUserId + " Ends");
            return cnt;
        }

    /**
     * Get formula count.
     * @param tableId
     * @param cols
     * @return formula count 
     */
    public short getFormulaCountOfTotal(int tableId, String cols) {
        logger.debug("getFormulaCountOfTotal : " + logUserId + " Starts");
        short cnt = 0;
        try {
            cnt = (short) hibernateQueryDao.countForNewQuery("TblTenderFormula ttf", "ttf.tenderTableId = " + tableId + " and ttf.formula like '%TOTAL%' and ttf.columnId in (" + cols + ")");
        } catch (Exception ex) {
            logger.error("getFormulaCountOfTotal : " + logUserId + ex);
            cnt = 0;
        }
        logger.debug("getFormulaCountOfTotal : " + logUserId + " Ends");
            return cnt;
        }

    /**
     * Delete tender cell details for given table id.
     * @param tableId
     * @return true if delete perform successfully else return false
     */
    public boolean deleteFormulaEffectsFromTenderCells(int tableId) {
        logger.debug("deleteFormulaEffectsFromTenderCells : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            String query = "delete from TblTenderCells ttc where ttc.tblTenderTables.tenderTableId = " + tableId + " and ttc.rowId in (select ttt.noOfRows from TblTenderTables ttt where ttt.tenderTableId = " + tableId + ")";
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("deleteFormulaEffectsFromTenderCells : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("deleteFormulaEffectsFromTenderCells : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Delete formula effect from tender table by updating noofrows -1 for given tender id.
     * @param tableId
     * @return
     */
    public boolean deleteFormulaEffectsFromTenderTables(int tableId) {
        logger.debug("deleteFormulaEffectsFromTenderTables : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            String query = "update TblTenderTables set noOfRows = noOfRows - 1 where tenderTableId =" + tableId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            bSuccess =  true;
        } catch (Exception ex) {
            logger.error("deleteFormulaEffectsFromTenderTables : " + logUserId + ex);
            bSuccess =  false;
        }
        logger.debug("deleteFormulaEffectsFromTenderTables : " + logUserId + " Ends");
        return bSuccess;
    }

    /**
     * Get Column details for Grand summary
     * @param formId
     * @return list of tender cell details.
     */
    public List<TblTenderCells> getColumnsForGS(int formId) {
        logger.debug("getColumnsForGS : " + logUserId + " Starts");
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttc.cellId, ttc.tblTenderTables.tenderTableId, tc.columnHeader from TblTenderCells ttc, TblTenderColumns tc where ");
        strQuery.append(" tc.columnId = ttc.columnId and ttc.tblTenderTables.tenderTableId = tc.tblTenderTables.tenderTableId ");
        strQuery.append(" and ttc.columnId in (select ttf.columnId from TblTenderFormula ttf ");
        strQuery.append(" where ttf.tblTenderForms.tenderFormId = " + formId + " and ttf.formula like '%TOTAL%') ");
        strQuery.append(" and ttc.tblTenderTables.tenderTableId in (select ttt.tenderTableId from TblTenderTables ttt where ttt.tblTenderForms.tenderFormId = " + formId + ") ");
        strQuery.append(" and ttc.rowId in(select tt.noOfRows from TblTenderTables tt where tt.tenderTableId in ");
        strQuery.append(" (select t.tenderTableId from TblTenderTables t where t.tblTenderForms.tenderFormId = " + formId + "))");

        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderCells> cells = new ArrayList<TblTenderCells>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                cells.add(new TblTenderCells(Integer.parseInt(i[0].toString()), new TblTenderTables(Integer.parseInt(i[1].toString())), i[2].toString()));
            }
            if (strQuery != null) {
                strQuery = null;
            }
        } catch (Exception ex) {
            logger.error("getColumnsForGS : " + logUserId + ex);
            objs = null;
            cells = null;
        }
        logger.debug("getColumnsForGS : " + logUserId + " Ends");
        return cells;
    }

    /**
     * Get Formula column id.
     * @param tenderTableId
     * @retur Formula column id list.
     */
    public List<Object[]> getFormulaColIds(int tenderTableId) {
        logger.debug("getFormulaColIds : " + logUserId + " Starts");
        List<Object[]> tenderFormula = null;
        try {
            String query = " select tf.tenderFormulaId,tf.formula,tf.columnId from  TblTenderFormula tf where tf.tenderTableId = " + tenderTableId + " and tf.formula like '%TOTAL%' ";
            tenderFormula = hibernateQueryDao.createNewQuery(query);
        } catch (Exception ex) {
            logger.error("getFormulaColIds : " + logUserId + ex);
            tenderFormula = null;
        }
        logger.debug("getFormulaColIds : " + logUserId + " Ends");
            return tenderFormula;
        }

    /**
     * Get Tender cell Details
     * @param tableId
     * @param columnId
     * @param cellId
     * @return Tender cell details.
     */
    public List<TblTenderListCellDetail> getTenderListCellDetail(int tableId, short columnId, int cellId) {
        logger.debug("getTenderListCellDetail : " + logUserId + " Starts");
        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
        try {
            listCellDetail = tblTenderListCellDetailDao.findTblTenderListCellDetail("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "columnId", Operation_enum.EQ, columnId, "cellId", Operation_enum.EQ, cellId);
        } catch (Exception ex) {
            logger.error("getTenderListCellDetail : " + logUserId + ex);
        }
        logger.debug("getTenderListCellDetail : " + logUserId + " Ends");
        return listCellDetail;
    }

    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    public List<SPCommonSearchDataMore> AllowFormToDisplay(String tenderId) {
        logger.debug("AllowFormToDisplay : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = spGeteGPCmsDataMore.executeProcedure("avoidepW2STD", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            System.out.println("AllowFormToDisplay : " + e + "starts");
        }
        logger.debug("AllowFormToDisplay : " + logUserId + "ends");
        return mores;
    }
    /**
     * Get Cell Values row order wise
     * @param tenderId
     * @param formId
     * @param userId
     * @return cell values
     */
    public List<TblTenderCells> getCellValueForRow(int tenderId, int formId,int userId) {
       logger.debug("getCellValueForRow : " + logUserId + " Starts");
        StringBuilder strQuery = new StringBuilder();
        List<SPCommonSearchDataMore> mores = this.AllowFormToDisplay(tenderId+"");
        List<Object> objePWtwoB = this.getFillByPEForePWtwoB(formId);
        if(mores!= null && !mores.isEmpty() && objePWtwoB!=null && !objePWtwoB.isEmpty()){
        //if(mores!= null && !mores.isEmpty()){
            strQuery.append(" select tcc.cellvalue ");
            strQuery.append(" from TblTenderForms tf, TblTenderTables tt, TblTenderColumns tc, TblTenderCells tcc ");
            strQuery.append(" where tf.tenderFormId = tt.tblTenderForms.tenderFormId and tt.tenderTableId = tc.tblTenderTables.tenderTableId ");
            strQuery.append(" and tt.tenderTableId = tcc.tblTenderTables.tenderTableId and tc.columnId = tcc.columnId ");
            strQuery.append(" and tf.tenderFormId = "+formId+" and tc.columnType = 7 order by tcc.rowId asc");
        }else{
            strQuery.append(" select tbpd.cellValue from TblTenderBidPlainData tbpd,");
            strQuery.append(" TblTenderColumns tc, TblTenderBidForm tbf, TblTenderBidTable tbt");
            strQuery.append(" where tbf.tblTenderMaster.tenderId = "+tenderId+" and tbf.userId='"+userId+"'");
            strQuery.append(" and tbf.tblTenderForms.tenderFormId = "+formId+" and tbf.bidId = tbt.tblTenderBidForm.bidId and tbt.bidTableId = tbpd.tblTenderBidTable.bidTableId");
            strQuery.append(" and tc.columnType = 7 and tbpd.tblTenderBidTable.tenderTableId = tc.tblTenderTables.tenderTableId ");
            strQuery.append(" and tbpd.tenderColId = tc.columnId order by tbpd.rowId asc ");
        }
        List<Object> objs = new ArrayList<Object>();
        List<TblTenderCells> tblTableCells = new ArrayList<TblTenderCells>();
        try {
            //tblTableCells = tblTenderCellsDao.findTblTenderCells("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "rowId", Operation_enum.ORDERBY, Operation_enum.ASC, "columnId", Operation_enum.ORDERBY, Operation_enum.ASC);
            objs = hibernateQueryDao.singleColQuery(strQuery.toString());
            logger.debug("getCellValueForRow -- : " + objs.size());
            for (Object i : objs) {
                System.out.println(i.toString());
                tblTableCells.add(new TblTenderCells(i.toString()));
            }
        } catch (Exception ex) {
            logger.error("getCellValueForRow : " + logUserId + ex);
            tblTableCells = null;
        }
        logger.debug("getCellValueForRow : " + logUserId + " Ends");
        return tblTableCells;
    }
     public List<Object> getTenderType(int tenderId) {
        logger.debug("getTenderType : " + logUserId + " Starts");
        List<Object> tenderType = null;
        try {
            String query = "select td.procurementType from TblTenderDetails td where td.tblTenderMaster.tenderId = '"+ tenderId +"' and  procurementNature in('Goods','Works') and procurementMethod='OTM' and procurementType='ICT'";
            tenderType = hibernateQueryDao.singleColQuery(query);
          //  System.out.print(tenderType);
        } catch (Exception ex) {
            logger.error("getTenderType : " + logUserId + ex);
            tenderType = null;
        }
        logger.debug("getTenderType : " + logUserId + " Ends");
            return tenderType;
        }

     private List<Object> getFillByPEForePWtwoB(int formId) {
       logger.debug("getFillByPEForePWtwoB : " + logUserId + " Starts");
       StringBuilder strQuery = new StringBuilder();
        List<Object> tenderType = null;
        try {
            strQuery.append(" select tc.filledBy ");
            strQuery.append(" from TblTenderForms tf, TblTenderTables tt, TblTenderColumns tc, TblTenderCells tcc ");
            strQuery.append(" where tf.tenderFormId = tt.tblTenderForms.tenderFormId and tt.tenderTableId = tc.tblTenderTables.tenderTableId ");
            strQuery.append(" and tt.tenderTableId = tcc.tblTenderTables.tenderTableId and tc.columnId = tcc.columnId ");
            strQuery.append(" and tf.tenderFormId = "+formId+" and tc.columnType = 7 and tc.filledBy = 1 and tc.columnId IN (8,9,10) order by tcc.rowId asc");
            tenderType = hibernateQueryDao.singleColQuery(strQuery.toString());
          //  System.out.print(tenderType);
        } catch (Exception ex) {
            logger.error("getFillByPEForePWtwoB : " + logUserId + ex);
            tenderType = null;
        }
        logger.debug("getFillByPEForePWtwoB : " + logUserId + " Ends");
            return tenderType;
    }
}
