/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPSearchAppReport;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class APPReportService {

   private static final Logger LOGGER = Logger.getLogger(APPReportService.class);
    
    HibernateQueryDao hibernateQueryDao;
    SPSearchAppReport spSearchAppReport;

    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPSearchAppReport getSpSearchAppReport() {
        return spSearchAppReport;
    }

    public void setSpSearchAppReport(SPSearchAppReport spSearchAppReport) {
        this.spSearchAppReport = spSearchAppReport;
    }
    
    /**
     * Getting  Department Id of given User Id.
     * @param userId as tbl_LoginMaster.userId
     * @return deptId as Tbl_DepartmentMaster.departmentId
     */
    public short getDepartmentIdByPEId(int userId)
    {
        LOGGER.debug("getDepartmentIdByPEId : " + logUserId + LOGGERSTART);
        Short i = 0;
        try {
        String Query = "select om.departmentId from TblOfficeMaster om,TblOfficeAdmin oa where om.officeId = oa.tblOfficeMaster.officeId and oa.tblLoginMaster.userId = "+userId;

        List<Object> listObj = hibernateQueryDao.singleColQuery(Query);
            if(!listObj.isEmpty()){
                i = Short.valueOf(listObj.get(0).toString());
            }
        } catch (Exception e) {
            LOGGER.error("getDepartmentIdByPEId : " + logUserId + " : " + e);
    }
        LOGGER.debug("getDepartmentIdByPEId : " + logUserId + LOGGEREND);
        return i;
    }

    /**
     * Getting Department Name from Deparment Id
     * @param deptId as Tbl_DepartmentMaster.departmentId
     * @return Departement Name from Tbl_DepartmentMaster.departmentName
     */
    public String getDepartmentNameByDeptId(int deptId)
    {
        LOGGER.debug("getDepartmentNameByDeptId : " + logUserId + LOGGERSTART);
        String data = "";
        try {
        String Query = "select d.departmentName from TblDepartmentMaster d where d.departmentId = "+deptId;
        //return  (String)hibernateQueryDao.getSingleColQuery(Query).get(0);
        List<Object> listObj = hibernateQueryDao.singleColQuery(Query);
            if(!listObj.isEmpty()){
                data = (String)listObj.get(0);
        }
        } catch (Exception e) {
            LOGGER.error("getDepartmentNameByDeptId : " + logUserId + " : " + e);
    }
        LOGGER.debug("getDepartmentNameByDeptId : " + logUserId + LOGGEREND);
        return data;
    }

    /**
     * It will retrun Office Name of given user.
     * @param userId as tbl_LoginMaster.userId
     * @return Office Name of PE from TblOfficeMaster.officeName
     */
    public String getOfficeNameByPEId(int userId)
    {
        LOGGER.debug("getOfficeNameByPEId : " + logUserId + LOGGERSTART);
        String data = "";
        try {
        String Query = "select om.officeName from TblOfficeMaster om,TblOfficeAdmin oa where om.officeId=oa.tblOfficeMaster.officeId and oa.tblLoginMaster.userId = "+userId;
        //return  (String)hibernateQueryDao.getSingleColQuery(Query).get(0);
        List<Object> listObj = hibernateQueryDao.singleColQuery(Query);
            if(!listObj.isEmpty()){
                data = listObj.get(0).toString();
        }
        } catch (Exception e) {
            LOGGER.error("getOfficeNameByPEId : " + logUserId + e);
    }
        LOGGER.debug("getOfficeNameByPEId : " + logUserId + LOGGEREND);
        return data;
    }

    /**
     * It will return office Id of given user.
     * @param userId as tbl_LoginMaster.userId
     * @return OfficeId of user from TblOfficeMaster.officeName
     */
    public short getOfficeIdByPEId(int userId)
    {
        Short i = 0;
        LOGGER.debug("getOfficeIdByPEId : " + logUserId + LOGGERSTART);
        try {
        String Query = "select om.officeId from TblOfficeMaster om,TblOfficeAdmin oa where om.officeId=oa.tblOfficeMaster.officeId and oa.tblLoginMaster.userId = "+userId;
        //return  Short.valueOf(hibernateQueryDao.getSingleColQuery(Query).get(0).toString());
        List<Object> listObj = hibernateQueryDao.singleColQuery(Query);
            if(!listObj.isEmpty()){
                i = Short.valueOf(listObj.get(0).toString());
        }
        } catch (Exception e) {
            LOGGER.error("getOfficeIdByPEId : " + logUserId + e);
        }
        LOGGER.debug("getOfficeIdByPEId : " + logUserId + LOGGEREND);
        return i;

    }

    /**
     * Getting Project List base on Office Id and Budget Type.
     * @param officeId as tbl_OfficeMaster.officeId
     * @param budType as Tbl_ProjectMaster.sourceOfFund
     * @return list of Objects Tbl_projectMaster's ProjectId, Project Name from TblProjectMaster
     */
    public List<Object[]> getProjectByOfficeAndBudget(int officeId,String budType)
    {
        LOGGER.debug("getProjectByOfficeAndBudget : " + logUserId + LOGGERSTART);
        List<Object[]> objects = null;
        try {
        StringBuilder Query = new StringBuilder();
        Query.append("select distinct pm.projectId,pm.projectName from TblProjectMaster pm,TblProjectOffice po where pm.projectId = po.projectId ");
            if(!"All".equalsIgnoreCase(budType)){
            Query.append(" and pm.sourceOfFund like '%").append(budType).append("%' ");
            }
        Query.append(" and po.tblOfficeMaster.officeId = ").append(officeId);
            objects = hibernateQueryDao.createNewQuery(Query.toString());
        } catch (Exception e) {
            LOGGER.error("getProjectByOfficeAndBudget : " + logUserId + e);
    }
        LOGGER.debug("getProjectByOfficeAndBudget : " + logUserId + LOGGEREND);
        return objects;
    }

    /**
     * Getting APP Details base on appId.
     * @param appId as TblAppMaster.appId
     * @return array of Objects having details for APP.
     */
    public Object[] getAppDetailByAppId(int appId)
    {
        Object[] obj = null;
        LOGGER.debug("getAppDetailByAppId : " + logUserId + LOGGERSTART);
         try {
        String Query = "select am.tblDepartmentMaster.departmentId,am.tblOfficeMaster.officeId,am.financialYear,budgetType,projectName from TblAppMaster am where am.appId = "+appId;
        List<Object[]> listObj = hibernateQueryDao.createNewQuery(Query);
            if(!listObj.isEmpty()){
                obj = listObj.get(0);
        }
         } catch (Exception e) {
             LOGGER.error("getAppDetailByAppId : " + logUserId + e);
    }
        LOGGER.debug("getAppDetailByAppId : " + logUserId + LOGGEREND);
        return obj;
    }

    /**
     * Getting APP Details base on action,deptId, OfficeId,Financial year, budgetType, Project Id, Procurement Nature, Package Id.
     * @param action
     * @param deptId
     * @param officeId
     * @param financeYear
     * @param budgetType
     * @param prjId
     * @param procNature
     * @param packageId
     * @param appId
     * @return list of APP Package Details base on passed values.
     */
    public List<CommonAppPkgDetails> getAppDetails(String action,int deptId,int officeId,String financeYear,int budgetType,int prjId,String procNature,int packageId,int appId){
        LOGGER.debug("getAppDetails : " + logUserId + LOGGERSTART);
        List<CommonAppPkgDetails> commonAppPkgDetailses = null;
        try {
            commonAppPkgDetailses = spSearchAppReport.executeProcedure(action, deptId, officeId, financeYear, budgetType, prjId, procNature, packageId,appId);
        } catch (Exception e) {
            LOGGER.error("getAppDetails : " + logUserId + e);
    }
        LOGGER.debug("getAppDetails : " + logUserId + LOGGEREND);
        return commonAppPkgDetailses;
    }

    /**
     * Gettging APP details base on APP Id.
     * @param appId
     * @return array of Object having details of APP.
     */

    public Object[] getDetailByAppId(int appId)
    {
        LOGGER.debug("getDetailByAppId : " + logUserId + LOGGERSTART);
        Object[] obj = null;
        try {
        String Query = "select am.tblDepartmentMaster.departmentId,am.tblOfficeMaster.officeId,am.financialYear,am.budgetType,am.projectId from TblAppMaster am where am.appId = "+appId;
        List<Object[]> listObj = hibernateQueryDao.createNewQuery(Query);
            if(!listObj.isEmpty()){
                obj =  listObj.get(0);
        }
        } catch (Exception e) {
            LOGGER.error("getDetailByAppId : " + logUserId + e);
    }
        LOGGER.debug("getDetailByAppId : " + logUserId + LOGGEREND);
        return obj;
    }
    
}
