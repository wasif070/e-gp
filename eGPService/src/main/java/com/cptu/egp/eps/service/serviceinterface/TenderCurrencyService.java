/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderCurrency;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ketan Prajapati
 */
public interface TenderCurrencyService {

    /**
     * Add Currency
     * @param admin
     * @return true if added or false if not.
     */
    public boolean addTblTenderCurrency(TblTenderCurrency admin);

    /**
     * Remove Tender Currency.
     * @param strTenderId
     * @param strDefaultCurrencyId
     * @return true if removed or false if not.
     */
    public boolean removeTenderCurreny(String strTenderId,String strDefaultCurrencyId);

    /**
     * Remove a specific Currency added to a particular tender.
     * @author Md. Khaled Ben Islam from Dohatec
     * @param strTenderId
     * @param strCurrencyId
     * @return true if removed or false if not.
     */
    public boolean removeSpecificTenderCurreny(String strTenderId, String strCurrencyId);

    /**
     * Get Configured Currency.
     * @param strTenderId
     * @return List of Configured Currency.
     * @throws Exception
     */
    public List<TblTenderCurrency> getConfiguredCurrency(String strTenderId) throws Exception;

    /**
     * Get Currency Tender wise.
     * @param tenderId
     * @param userId
     * @return List of Currency.
     */
    public List<Object[]> getCurrencyTenderwise(int tenderId, int userId);

    /**
     * Get Currency Tender wise
     * @param tenderId
     * @return List of Currency
     */
    public List<Object[]> getCurrencyTenderwise(int tenderId);

    /**
     * Get how many times a particular Currency added to a Particular Tender
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tenderId
     * @param  currencyId
     * @return Number of times the particular Currency added to the tender in database
     */
    public long getCurrencyCountForTender(int tenderId, int currencyId);

    /**
     * Get how many Currencies added to a Particular Tender
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tenderId
     * @return Number of Currencies added to the tender in database
     */
    public long getCurrencyCountForTender(int tenderId);

    /**
     * Check whether at least One Foreign Currency (except BTN) Exchange Rate added to a Particular Tender
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tenderId
     * @return true if exchange rate of at least one currency added
     */
    public boolean isAtleastOneCurrencyExchangeRateAdded(int tenderId);

    /**
     *
     * @param tenderId
     * @return
     */
    public boolean isCurrencyExchangeRateAdded(int tenderId);
    /**
     * Update the exchange rate of a Currency, added to a particular tender
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tblTenderCurrency
     * @return true if updated or false if not.
     */
    public boolean updateCurrencyExchangeRate(TblTenderCurrency tblTenderCurrency);

    /**
     * Get the last update date on which Currency Exchange rates are added/updated
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tenderId
     * @return last date on which currency exchange rate updated
     */
    public Date getLastDateOfCurrencyExchangeRates(int tenderId);

    /**
     *  Check whether a particular list box (ComboBox) is designed to be bind with a List of Currency
     *  @author Md. Khaled Ben Islam from Dohatec
     *  @param tenderListId ID of respective list box
     *  @return true if Yes or false otherwise
     */
    public boolean isTenderListBoxForCurrency(int tenderListId);

    /**
     * Get the Opening date of a particular Tender
     * @author Md. Khaled Ben Islam from Dohatec
     * @param tenderId
     * @return Tender opening date
     */
    public Date getTenderOpeningDate(int tenderId);

    public List<Object[]> getTenderFormId(int tenderId);

    /**
     * Set user Id at Service layer.
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    public List<Object[]> getTenderTableCellValuesFillesByPE(int tenderTableId);

    /**
     *
     */
    public List<Object[]> getTenderTableCellValuesFillesByTenderer(int tenderFormId);

    public List<Object[]> getFormula(int tenderFormId);

    public String getTenderCellValue(String colList,int tenderFormId, int bidTableId, int tenderTableid,int tenderId,int rowId );

    public List<Object[]> getTenderBiddingInfo(int tenderFormId);

    public String updateTenderBidPlainData(String cellValueList,String columnIdList,String bidTableIdList,String rowIdList,int tenderFormId);

    public List<Object[]> getRowId(int tenderFormId,int bidTableId,int tenderTableId);


}

