/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee;
import com.cptu.egp.eps.model.table.TblFaqBhutan;
import java.util.List;

/**
 *
 * @author Nitish Ranjan Bhowmik
 */
public interface FaqBhutanService 
{
    public boolean addTblFaqBhutan(TblFaqBhutan tblFaqBhutan);
    
    public void deleteTblFaqBhutan(TblFaqBhutan tblFaqBhutan);

    public String updateTblFaqBhutan(TblFaqBhutan tblFaqBhutan);

    public List<TblFaqBhutan> getAllTblFaqBhutan();
    
    public List<TblFaqBhutan> findTblFaqBhutan(int id);
    
    
}
