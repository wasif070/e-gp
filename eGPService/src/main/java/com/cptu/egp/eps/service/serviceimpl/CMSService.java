/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvAttSheetDtlDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvAttSheetMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCchistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCnsltCompDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvFormMapDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPaymentSchDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPshistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvReExHistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvReExpenseDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSalaryReDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSrvariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSshistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSsvariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvStaffDaysCntDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvStaffSchDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTchistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTcvariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTeamCompDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWorkPlanDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWphistoryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWpVariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsVariationOrderDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPsvariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvRevariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCcvariDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWrkSchDocDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetDtl;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster;
import com.cptu.egp.eps.model.table.TblCmsSrvCchistory;
import com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp;
import com.cptu.egp.eps.model.table.TblCmsSrvFormMap;
import com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch;
import com.cptu.egp.eps.model.table.TblCmsSrvPshistory;
import com.cptu.egp.eps.model.table.TblCmsSrvReExHistory;
import com.cptu.egp.eps.model.table.TblCmsSrvReExpense;
import com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe;
import com.cptu.egp.eps.model.table.TblCmsSrvSrvari;
import com.cptu.egp.eps.model.table.TblCmsSrvSshistory;
import com.cptu.egp.eps.model.table.TblCmsSrvSsvari;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffDaysCnt;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffSch;
import com.cptu.egp.eps.model.table.TblCmsSrvTchistory;
import com.cptu.egp.eps.model.table.TblCmsSrvTcvari;
import com.cptu.egp.eps.model.table.TblCmsSrvTeamComp;
import com.cptu.egp.eps.model.table.TblCmsSrvWorkPlan;
import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.model.table.TblCmsSrvWphistory;
import com.cptu.egp.eps.model.table.TblCmsSrvWpvari;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import com.cptu.egp.eps.model.table.TblCmsSrvPsvari;
import com.cptu.egp.eps.model.table.TblCmsSrvRevari;
import com.cptu.egp.eps.model.table.TblCmsSrvCcvari;
import com.cptu.egp.eps.model.table.TblCmsSrvWrkSchDoc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 * 
 * 12	Payment Schedule
 * 13	Team Composition
 * 14	Staffing Schedule
 * 15	Salary Reimbursable
 * 16	Reimbursable Expense
 * 17	Others
 * 18	Consultant Composition
 * 19	Work Plan
 * Abbreviation
 *
 * SS :- staffing schedule
 * SR :- salary reimbursable
 * TC :- Team composition
 * PS :- Payment Schedule
 * CC :- consultant Composition
 * RE :- Reimbursable Expenses
 */
public class CMSService {

    HibernateQueryDao hibernateQueryDao;
    TblCmsSrvReExpenseDao expenseDao;
    TblCmsSrvSalaryReDao salaryReDao;
    TblCmsSrvStaffSchDao staffSchDao;
    TblCmsSrvTeamCompDao teamCompDao;
    TblCmsSrvTchistoryDao tblCmsSrvTchistoryDao;
    TblCmsSrvWorkPlanDao workPlanDao;
    TblCmsSrvFormMapDao formMapDao;
    SpGeteGPCmsDataMore dataMore;
    TblCmsSrvReExpenseDao tblCmsSrvReExpenseDao;
    TblCmsSrvCnsltCompDao tblCmsSrvCnsltCompDao;
    TblCmsSrvPaymentSchDao paymentSchDao;
    TblCmsSrvStaffDaysCntDao daysCntDao;
    TblCmsSrvAttSheetDtlDao sheetDtlDao;
    TblCmsSrvAttSheetMasterDao sheetMasterDao;
    TblCmsSrvWphistoryDao srvWpHistoryDao;
    TblCmsSrvSshistoryDao srvSsHistoryDao;
    TblCmsSrvPshistoryDao srvPsHistoryDao;
    TblCmsSrvCchistoryDao srvCchistoryDao;
    TblCmsSrvReExHistoryDao historyDao;
    TblCmsSrvWpVariDao srvWpVariDao;
    TblCmsVariationOrderDao orderDao;
    TblCmsSrvTcvariDao tcvariDao;
    TblCmsSrvSsvariDao ssvariDao;
    TblCmsSrvSrvariDao srvariDao;
    TblCmsSrvPsvariDao srvpsvariDao;
    TblCmsSrvRevariDao srvrevariDao;
    TblCmsSrvCcvariDao srvccvariDao;
    TblCmsSrvWrkSchDocDao tblCmsSrvWrkSchDocDao;
    final Logger logger = Logger.getLogger(CMSService.class);
    private String logUserId = "0";

    /**
     *
     * @return
     */
    public String getLogUserId() {
        return logUserId;
    }

    /**
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *
     * @return
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     *
     * @param hibernateQueryDao
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvReExpenseDao getExpenseDao() {
        return expenseDao;
    }

    /**
     *
     * @param expenseDao
     */
    public void setExpenseDao(TblCmsSrvReExpenseDao expenseDao) {
        this.expenseDao = expenseDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvSalaryReDao getSalaryReDao() {
        return salaryReDao;
    }

    /**
     *
     * @param salaryReDao
     */
    public void setSalaryReDao(TblCmsSrvSalaryReDao salaryReDao) {
        this.salaryReDao = salaryReDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvStaffSchDao getStaffSchDao() {
        return staffSchDao;
    }

    /**
     *
     * @param staffSchDao
     */
    public void setStaffSchDao(TblCmsSrvStaffSchDao staffSchDao) {
        this.staffSchDao = staffSchDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvTeamCompDao getTeamCompDao() {
        return teamCompDao;
    }

    /**
     *
     * @param teamCompDao
     */
    public void setTeamCompDao(TblCmsSrvTeamCompDao teamCompDao) {
        this.teamCompDao = teamCompDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvWorkPlanDao getWorkPlanDao() {
        return workPlanDao;
    }

    /**
     *
     * @param workPlanDao
     */
    public void setWorkPlanDao(TblCmsSrvWorkPlanDao workPlanDao) {
        this.workPlanDao = workPlanDao;
    }

    /**
     *
     * @return
     */
    public SpGeteGPCmsDataMore getDataMore() {
        return dataMore;
    }

    /**
     *
     * @param dataMore
     */
    public void setDataMore(SpGeteGPCmsDataMore dataMore) {
        this.dataMore = dataMore;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvReExpenseDao getTblCmsSrvReExpenseDao() {
        return tblCmsSrvReExpenseDao;
    }

    /**
     *
     * @param tblCmsSrvReExpenseDao
     */
    public void setTblCmsSrvReExpenseDao(TblCmsSrvReExpenseDao tblCmsSrvReExpenseDao) {
        this.tblCmsSrvReExpenseDao = tblCmsSrvReExpenseDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvCnsltCompDao getTblCmsSrvCnsltCompDao() {
        return tblCmsSrvCnsltCompDao;
    }

    /**
     * 
     * @param tblCmsSrvCnsltCompDao
     */
    public void setTblCmsSrvCnsltCompDao(TblCmsSrvCnsltCompDao tblCmsSrvCnsltCompDao) {
        this.tblCmsSrvCnsltCompDao = tblCmsSrvCnsltCompDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvFormMapDao getFormMapDao() {
        return formMapDao;
    }

    /**
     *
     * @param formMapDao
     */
    public void setFormMapDao(TblCmsSrvFormMapDao formMapDao) {
        this.formMapDao = formMapDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvPaymentSchDao getPaymentSchDao() {
        return paymentSchDao;
    }

    /**
     *
     * @param paymentSchDao
     */
    public void setPaymentSchDao(TblCmsSrvPaymentSchDao paymentSchDao) {
        this.paymentSchDao = paymentSchDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvTchistoryDao getTblCmsSrvTchistoryDao() {
        return tblCmsSrvTchistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvStaffDaysCntDao getDaysCntDao() {
        return daysCntDao;
    }

    /**
     *
     * @param daysCntDao
     */
    public void setDaysCntDao(TblCmsSrvStaffDaysCntDao daysCntDao) {
        this.daysCntDao = daysCntDao;
    }

    /**
     *
     * @param tblCmsSrvTchistoryDao
     */
    public void setTblCmsSrvTchistoryDao(TblCmsSrvTchistoryDao tblCmsSrvTchistoryDao) {
        this.tblCmsSrvTchistoryDao = tblCmsSrvTchistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvAttSheetDtlDao getSheetDtlDao() {
        return sheetDtlDao;
    }

    /**
     *
     * @param sheetDtlDao
     */
    public void setSheetDtlDao(TblCmsSrvAttSheetDtlDao sheetDtlDao) {
        this.sheetDtlDao = sheetDtlDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvAttSheetMasterDao getSheetMasterDao() {
        return sheetMasterDao;
    }

    /**
     *
     * @param sheetMasterDao
     */
    public void setSheetMasterDao(TblCmsSrvAttSheetMasterDao sheetMasterDao) {
        this.sheetMasterDao = sheetMasterDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvWphistoryDao getSrvWpHistoryDao() {
        return srvWpHistoryDao;
    }

    /**
     *
     * @param srvWpHistoryDao
     */
    public void setSrvWpHistoryDao(TblCmsSrvWphistoryDao srvWpHistoryDao) {
        this.srvWpHistoryDao = srvWpHistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvSshistoryDao getSrvSsHistoryDao() {
        return srvSsHistoryDao;
    }

    /**
     *
     * @param srvSsHistoryDao
     */
    public void setSrvSsHistoryDao(TblCmsSrvSshistoryDao srvSsHistoryDao) {
        this.srvSsHistoryDao = srvSsHistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvPshistoryDao getSrvPsHistoryDao() {
        return srvPsHistoryDao;
    }

    /**
     *
     * @param srvPsHistoryDao
     */
    public void setSrvPsHistoryDao(TblCmsSrvPshistoryDao srvPsHistoryDao) {
        this.srvPsHistoryDao = srvPsHistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvCchistoryDao getSrvCchistoryDao() {
        return srvCchistoryDao;
    }

    /**
     *
     * @param srvCchistoryDao
     */
    public void setSrvCchistoryDao(TblCmsSrvCchistoryDao srvCchistoryDao) {
        this.srvCchistoryDao = srvCchistoryDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvReExHistoryDao getHistoryDao() {
        return historyDao;
    }

    /**
     *
     * @param historyDao
     */
    public void setHistoryDao(TblCmsSrvReExHistoryDao historyDao) {
        this.historyDao = historyDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvWpVariDao getSrvWpVariDao() {
        return srvWpVariDao;
    }

    /**
     *
     * @param srvWpVariDao
     */
    public void setSrvWpVariDao(TblCmsSrvWpVariDao srvWpVariDao) {
        this.srvWpVariDao = srvWpVariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsVariationOrderDao getOrderDao() {
        return orderDao;
    }

    /**
     *
     * @param orderDao
     */
    public void setOrderDao(TblCmsVariationOrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvSrvariDao getSrvariDao() {
        return srvariDao;
    }

    /**
     *
     * @param srvariDao
     */
    public void setSrvariDao(TblCmsSrvSrvariDao srvariDao) {
        this.srvariDao = srvariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvSsvariDao getSsvariDao() {
        return ssvariDao;
    }

    /**
     *
     * @param ssvariDao
     */
    public void setSsvariDao(TblCmsSrvSsvariDao ssvariDao) {
        this.ssvariDao = ssvariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvTcvariDao getTcvariDao() {
        return tcvariDao;
    }

    /**
     *
     * @param tcvariDao
     */
    public void setTcvariDao(TblCmsSrvTcvariDao tcvariDao) {
        this.tcvariDao = tcvariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvCcvariDao getSrvccvariDao() {
        return srvccvariDao;
    }

    /**
     *
     * @param srvccvariDao
     */
    public void setSrvccvariDao(TblCmsSrvCcvariDao srvccvariDao) {
        this.srvccvariDao = srvccvariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvPsvariDao getSrvpsvariDao() {
        return srvpsvariDao;
    }

    /**
     *
     * @param srvpsvariDao
     */
    public void setSrvpsvariDao(TblCmsSrvPsvariDao srvpsvariDao) {
        this.srvpsvariDao = srvpsvariDao;
    }

    /**
     *
     * @return
     */
    public TblCmsSrvRevariDao getSrvrevariDao() {
        return srvrevariDao;
    }

    /**
     *
     * @param srvrevariDao
     */
    public void setSrvrevariDao(TblCmsSrvRevariDao srvrevariDao) {
        this.srvrevariDao = srvrevariDao;
    }
    /**
     *
     * @return
     */
    public TblCmsSrvWrkSchDocDao getTblCmsSrvWrkSchDocDao() {
        return tblCmsSrvWrkSchDocDao;
    }

    /**
     *
     * @param tblCmsSrvWrkSchDocDao
     */
    public void setTblCmsSrvWrkSchDocDao(TblCmsSrvWrkSchDocDao tblCmsSrvWrkSchDocDao) {
        this.tblCmsSrvWrkSchDocDao = tblCmsSrvWrkSchDocDao;
    }

    /**
     * This method get salary reimbursable date from TC and SS
     * @param tcId
     * @param varId
     * @return List<Object[]>
     */
    public List<Object[]> getSalReDataFromTCAndSS(int tcId, int varId) {
        logger.debug("getSalReDataFromTCAndSS : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select distinct (ss.workFrom), SUM(ss.noOfDays) from ");
        br.append("TblCmsSrvTcvari tc,TblCmsSrvSsvari ss ");
        br.append("where tc.variOrdId=" + varId + " and tc.variOrdId=ss.variOrdId ");
        br.append("and tc.srvTcvariId=ss.srvTcvariId ");
        br.append("and tc.srvTcvariId =" + tcId);
        br.append(" group by ss.workFrom ");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getSalReDataFromTCAndSS : " + e);
        }
        logger.debug("getSalReDataFromTCAndSS : " + logUserId + "ends");
        return list;
    }

    /**
     * his method get salary reimbursable date from SR and SS
     * @param varId
     * @return List<Object[]>
     */
    public List<Object[]> getSalRemDataFrmSSandSR(int varId) {
        logger.debug("getSalRemDataFrmSSandSR : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tc.empName,ss.workFrom,tc.variOrdId,tc.srvTcvariId from ");
        br.append("TblCmsSrvTcvari tc,TblCmsSrvSsvari ss ");
        br.append("where tc.variOrdId=" + varId + " and tc.variOrdId=ss.variOrdId ");
        br.append("and tc.srvTcvariId=ss.srvTcvariId ");
        br.append("and tc.srvTcvariId not in (select tsr.srvTcvariId from TblCmsSrvSrvari tsr where tsr.variOrdId=" + varId + ")");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getSalRemDataFrmSSandSR : " + e);
        }
        logger.debug("getSalRemDataFrmSSandSR : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get all forms for service case
     * @param tenderId
     * @return List<Object[]>
     */
    public List<Object[]> getAllFormsForService(int tenderId) {
        logger.debug("getAllFormsForService : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tf.formName, tf.tenderFormId,sbm.srvBoqId ");
        br.append("from TblCmsTemplateSrvBoqDetail tsbd, TblTenderForms tf,");
        br.append("TblCmsSrvBoqMaster sbm,TblTenderStd tsd,TblTenderSection tss ");
        //br.append("where tf.templateFormId = tsbd.tblTemplateSectionForm.formId and sbm.srvBoqId not in (17) and sbm.srvBoqId = tsbd.tblCmsSrvBoqMaster.srvBoqId ");
        br.append("where replace(tf.templateFormId,'-','') = tsbd.tblTemplateSectionForm.formId and sbm.srvBoqId not in (17) and sbm.srvBoqId = tsbd.tblCmsSrvBoqMaster.srvBoqId ");
        br.append("and tss.tblTenderStd.tenderStdId=tsd.tenderStdId and tsd.tenderId=" + tenderId + " and tss.contentType='form' ");
        br.append("and tf.tblTenderSection.tenderSectionId=tss.tenderSectionId "); //order by sbm.sortOrder
        // status of form added so only approved forms only selected. also consider forms approved during corrigendum, and dont consider forms cancelled during corrigendum.
        br.append("and (tf.formStatus is null or tf.formStatus in ('p','cp','a')) ");

        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getAllFormsForService : " + e);
        }
        logger.debug("getAllFormsForService : " + logUserId + "ends");
        return list;
    }

    /**
     * this method give count for variation order
     * @param tenderId
     * @return long
     */
    public long countVariationOrderForSrv(int tenderId) {
        logger.debug("countVariationOrderForSrv : " + logUserId + "starts");
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblCmsVariationOrder tt", "tt.tenderId=" + tenderId);
        } catch (Exception e) {
            logger.error("countVariationOrderForSrv : " + e + "starts");
        }
        logger.debug("countVariationOrderForSrv : " + logUserId + "ends");
        return count;
    }

    /**
     * This method get the list of variation order for service case
     * @param tenderId
     * @return List<TblCmsVariationOrder>
     */
    public List<TblCmsVariationOrder> getListOfVariationOrderForSrv(int tenderId) {
        logger.debug("getListOfVariationOrderForSrv : " + logUserId + "starts");
        List<TblCmsVariationOrder> list = null;
        try {
            list = orderDao.findTblCmsVariationOrder("tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception e) {
            logger.error("getListOfVariationOrderForSrv : " + e + "starts");
        }
        logger.debug("getListOfVariationOrderForSrv : " + logUserId + "ends");
        return list;
    }

    /**
     * this method update variation order status
     * @param varOrdId
     * @param status
     */
    public void updateVarOrderWithStatus(int varOrdId, String status) {
        logger.debug("updateVarOrderWithStatus : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblCmsVariationOrder set variOrdStatus='" + status + "' where variOrdId=" + varOrdId);
        } catch (Exception e) {
            logger.error("updateVarOrderWithStatus : " + e + "starts");
        }
        logger.debug("updateVarOrderWithStatus : " + logUserId + "ends");
    }

    /**
     * This method get the data to dump for service case to run CMS
     * @param userId
     * @param tenderFormId
     * @return List<Object>
     */
    public List<Object> getDataForDump(int userId, int tenderFormId) {
        logger.debug("getDataForDump : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tbp.cellValue from TblTenderBidTable tbt,TblTenderBidForm tbf,TblTenderBidPlainData tbp ");
        br.append("where tbt.tblTenderBidForm.bidId=tbf.bidId and tbp.tblTenderBidTable.bidTableId=tbt.bidTableId and tbf.tblTenderForms.tenderFormId=" + tenderFormId + " ");
        br.append("and tbf.userId=" + userId + "");
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());

        } catch (Exception e) {
            logger.error("addToSalaryRE : " + logUserId + "ends");
        }
        logger.debug("getDataForDump : " + logUserId + "ends");
        return list;
    }
    /**
     * this method get the data for dump from negotiation table
     * @param userId
     * @param tenderFormId
     * @return List<Object>
     */
    public List<Object> getDataForDumpFrmNEG(int userId, int tenderFormId) {
        logger.debug("getDataForDump : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tbp.cellValue from TblNegBidTable tbt,TblNegBidForm tbf,TblNegBidDtlSrv tbp ");
        br.append("where tbt.bidId=tbf.bidId and tbp.negBidTableId=tbt.negBidTableId and tbf.tenderFormId=").append(tenderFormId).append(" ");
        br.append("and tbf.userId=").append(userId).append("");
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());

        } catch (Exception e) {
            logger.error("addToSalaryRE : " + logUserId + "ends");
        }
        logger.debug("getDataForDump : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get the data for dump from CC
     * @param userId
     * @param tenderFormId
     * @param rowId
     * @return List<Object>
     */
    public List<Object> getDataForDumpForCC(int userId, int tenderFormId, int rowId) {
        logger.debug("getDataForDumpForCC : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tbp.cellValue from TblTenderBidTable tbt,TblTenderBidForm tbf,TblTenderBidPlainData tbp ");
        br.append("where tbt.tblTenderBidForm.bidId=tbf.bidId and tbp.tblTenderBidTable.bidTableId=tbt.bidTableId and tbf.tblTenderForms.tenderFormId=" + tenderFormId + " ");
        br.append("and tbf.userId=" + userId + " and tbp.rowId=" + rowId);
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());

        } catch (Exception e) {
            logger.error("getDataForDumpForCC : " + logUserId + "ends");
        }
        logger.debug("getDataForDumpForCC : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get the description from the item
     * @param itemId
     * @param tenderFormId
     * @return List<Object>
     */
    public List<Object> getItemDescById(int itemId, int tenderFormId) {
        logger.debug("getItemDescById : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select b.itemText From TblTenderListBox a ");
        br.append("inner join TblTenderListDetail b on ");
        br.append("a.tenderListId = b.tblTenderListBox.tenderListId ");
        br.append("where a.tenderFormId = " + tenderFormId + " and b.itemId = " + itemId + "");
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());

        } catch (Exception e) {
            logger.error("getItemDescById : " + logUserId + "ends");
        }
        logger.debug("getItemDescById : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get the progress report for time based tender
     * @param srvFormMapId
     * @param monthDays
     * @return List<Object[]>
     */
    public List<Object[]> getListForPRForTB(int srvFormMapId, int monthDays) {
        logger.debug("getListForPRForTB : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tsr.srNo,tct.empName,tct.positionAssigned,tsr.workFrom,(tsr.empMonthSalary/" + monthDays + "),");
        br.append("(tcc.offSiteCnt-tcc.usedOffSiteCnt),(tcc.onSiteCnt-tcc.usedOnSiteCnt),tct.srvTcid ");
        br.append(" from TblCmsSrvSalaryRe tsr,TblCmsSrvStaffDaysCnt tcc,TblCmsSrvTeamComp tct where ");
        br.append("tct.srvTcid=tsr.srvTcid and tcc.srvTcid = tct.srvTcid and tsr.srvTcid=tcc.srvTcid and tsr.srvFormMapId=" + srvFormMapId);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());

        } catch (Exception e) {
            logger.error("getListForPRForTB : " + logUserId + "ends");
        }
        logger.debug("getListForPRForTB : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get qualified user
     * @param tenderId
     * @return List<SPCommonSearchDataMore>
     */
    public List<SPCommonSearchDataMore> getQualifiedUser(String tenderId, String pckLotId) {
        logger.debug("getQualifiedUser : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("getQualifiedUser", tenderId, pckLotId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            logger.error("getQualifiedUser : " + logUserId + "ends");
        }
        logger.debug("getQualifiedUser : " + logUserId + "ends");
        return mores;
    }
    public List<SPCommonSearchDataMore> getAwardedUserId(String tenderId) {
        logger.debug("getAwardedUserId : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("getAwardedUserId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            logger.error("getAwardedUserId : " + logUserId + "ends");
        }
        logger.debug("getAwardedUserId : " + logUserId + "ends");
        return mores;
    }

    /**
     * This method add details of work plan in the table
     * @param workplan
     */
    public void addToWorkPlan(List<TblCmsSrvWorkPlan> workplan) throws Exception {
        logger.debug("addToWorkPlan : " + logUserId + "starts");
        try {
            workPlanDao.updateOrSaveEstCost(workplan);
        } catch (Exception e) {
            logger.error("addToSalaryRE : " + logUserId + "ends");
        }
        logger.debug("addToWorkPlan : " + logUserId + "ends");
    }

    /**
     * this method add details of team in team composition table
     * @param teamComps
     */
    public void addToTeamComp(List<TblCmsSrvTeamComp> teamComps) throws Exception {
        logger.debug("addToTeamComp : " + logUserId + "starts");

        teamCompDao.updateOrSaveEstCost(teamComps);

        logger.debug("addToTeamComp : " + logUserId + "ends");
    }

    /**
     * add details to salary Reimbursable
     * @param salaryRes
     */
    public void addToSalaryRE(List<TblCmsSrvSalaryRe> salaryRes) throws Exception {
        logger.debug("addToSalaryRE : " + logUserId + "starts");
        salaryReDao.updateOrSaveEstCost(salaryRes);
        logger.debug("addToSalaryRE : " + logUserId + "ends");
    }

    /**
     * add details to RE
     * @param reExpenses
     */
    public void addToRE(List<TblCmsSrvReExpense> reExpenses) throws Exception {
        logger.debug("addToRE : " + logUserId + "starts");

        expenseDao.updateOrSaveEstCost(reExpenses);

        logger.debug("addToRE : " + logUserId + "ends");
    }

    /**
     * add details to staffing schedule
     * @param schs
     */
    public void addToStaffSch(List<TblCmsSrvStaffSch> schs) throws Exception {
        logger.debug("addToStaffSch : " + logUserId + "starts");

        staffSchDao.updateOrSaveEstCost(schs);

        logger.debug("addToStaffSch : " + logUserId + "ends");
    }

    /**
     * get reimbursable data
     * @param srvFormMapId
     * @return List<TblCmsSrvReExpense> 
     */
    public List<TblCmsSrvReExpense> getSrvReExpensesData(int srvFormMapId) {
        logger.debug("addSrvReExpensesDate : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvReExpense> list = null;
        try {
            list = tblCmsSrvReExpenseDao.findTblCmsSrvReExpense("srvFormMapId", Operation_enum.EQ, srvFormMapId);

        } catch (Exception e) {
            logger.error("addSrvReExpensesDate : " + logUserId + "ends");
        }
        logger.debug("addSrvReExpensesDate : " + logUserId + "ends");
        return list;
    }

    /**
     * Add to reimbursable expenses
     * @param tblCmsSrvReExpensee
     * @return boolean
     */
    public boolean addSrvReExpensesData(List<TblCmsSrvReExpense> tblCmsSrvReExpensee) {
        logger.debug("addSrvReExpensesData : " + logUserId + "starts");
        boolean flag = false;
        try {
            List<TblCmsSrvReExpense> list = new ArrayList<TblCmsSrvReExpense>();
            for (TblCmsSrvReExpense tblCmsSrvReExpense : tblCmsSrvReExpensee) {
                list.add(tblCmsSrvReExpense);
            }
            tblCmsSrvReExpenseDao.updateOrSaveEstCost(list);
            flag = true;
        } catch (Exception e) {
            logger.error("addSrvReExpensesData : " + logUserId + "ends");
        }
        logger.debug("addSrvReExpensesData : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method updates data of Re expenses
     * @param srvREId
     * @param qty
     * @param unitCost
     * @param totalAmt
     */
    public void updateSrvReExpensesData(int srvREId, String qty, String unitCost, String totalAmt) {
        logger.debug("updateSrvReExpensesData : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsSrvReExpense tcsre set tcsre.qty = '" + new BigDecimal(qty) + "',tcsre.unitCost='" + new BigDecimal(unitCost) + "',tcsre.totalAmt='" + new BigDecimal(totalAmt) + "' where tcsre.srvReid=" + srvREId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSrvReExpensesData : " + e + "starts");
        }
        logger.debug("updateSrvReExpensesData : " + logUserId + "ends");
    }

    /**
     * This method get the consultant composition data
     * @param srvFormMapId
     * @return List<TblCmsSrvCnsltComp>
     */
    public List<TblCmsSrvCnsltComp> getSrvConsltCompoData(int srvFormMapId) {
        logger.debug("getSrvConsltCompoData : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvCnsltComp> list = null;
        try {
            list = tblCmsSrvCnsltCompDao.findTblCmsSrvCnsltComp("srvFormMapId", Operation_enum.EQ, srvFormMapId);

        } catch (Exception e) {
            logger.error("getSrvConsltCompoData : " + logUserId + "ends");
        }
        logger.debug("getSrvConsltCompoData : " + logUserId + "ends");
        return list;
    }

    /**
     * this method update consultant composition data for time base
     * @param srvCCId
     * @param months
     * @param keyNosPE
     * @param supportNosPE
     * @param consultantName
     * @param keyNos
     * @param supportNos
     * @param totalNosPE
     */
    public void updateSrvConsltCompoDataTimebase(int srvCCId, BigDecimal months, int keyNosPE, int supportNosPE, String consultantName, int keyNos, int supportNos, int totalNosPE) {
        logger.debug("updateSrvConsltCompoData : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvCnsltComp tcscc set tcscc.months='" + months + "',tcscc.keyNosPe='" + keyNosPE + "',tcscc.supportNosPe='" + supportNosPE + "',tcscc.consultantName='" + consultantName + "',tcscc.keyNos='" + keyNos + "',tcscc.supportNos='" + supportNos + "',tcscc.totalNosPe='" + totalNosPE + "' where tcscc.srvCcid=" + srvCCId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSrvConsltCompoData : " + e + "starts");
        }
        logger.debug("updateSrvConsltCompoData : " + logUserId + "ends");
    }

    /**
     * this method update consultant composition data for lumpsum
     * @param srvCCId
     * @param keyNosPE
     * @param supportNosPE
     * @param consultantName
     * @param keyNos
     * @param supportNos
     */
    public void updateSrvConsltCompoDataLumpSum(int srvCCId, int keyNosPE, int supportNosPE, String consultantName, int keyNos, int supportNos) {
        logger.debug("updateSrvConsltCompoData : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvCnsltComp tcscc set tcscc.keyNosPe='" + keyNosPE + "',tcscc.supportNosPe='" + supportNosPE + "',tcscc.consultantName='" + consultantName + "',tcscc.keyNos='" + keyNos + "',tcscc.supportNos='" + supportNos + "' where tcscc.srvCcid=" + srvCCId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSrvConsltCompoData : " + e + "starts");
        }
        logger.debug("updateSrvConsltCompoData : " + logUserId + "ends");
    }

    /**
     * this method get the data of payment schedule
     * @param srvFormMapId
     * @return List<TblCmsSrvPaymentSch>
     */
    public List<TblCmsSrvPaymentSch> getPaymentScheduleData(int srvFormMapId) {
        logger.debug("getPaymentScheduleData : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvPaymentSch> list = null;
        try {
            list = paymentSchDao.findTblCmsSrvPaymentSch("srvFormMapId", Operation_enum.EQ, srvFormMapId);

        } catch (Exception e) {
            logger.error("getPaymentScheduleData : " + logUserId + "ends");
        }
        logger.debug("getPaymentScheduleData : " + logUserId + "ends");
        return list;
    }

    /**
     * This method update payment schedule data
     * @param SrvPsid
     * @param peenddate
     * @param endDate
     */
    public void updateSrvPaymentScheduleData(int SrvPsid, Date peenddate, Date endDate) {
        logger.debug("updateSrvPaymentScheduleData : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvPaymentSch tcsps set tcsps.peenddate='" + new java.sql.Date(peenddate.getTime()) + "',tcsps.endDate='" + new java.sql.Date(endDate.getTime()) + "' where tcsps.srvPsid=" + SrvPsid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSrvPaymentScheduleData : " + e + "starts");
        }
        logger.debug("updateSrvPaymentScheduleData : " + logUserId + "ends");
    }

    /**
     * this method get the task assign
     * @param srvFormMapId
     * @return List<TblCmsSrvTeamComp>
     */
    public List<TblCmsSrvTeamComp> getSrvTeamCompoTaskAssigData(int srvFormMapId) {
        logger.debug("getSrvTeamCompoTaskAssigData : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvTeamComp> list = null;
        try {
            list = teamCompDao.findTblCmsSrvTeamComp("srvFormMapId", Operation_enum.EQ, srvFormMapId);

        } catch (Exception e) {
            logger.error("getSrvTeamCompoTaskAssigData : " + logUserId + "ends");
        }
        logger.debug("getSrvTeamCompoTaskAssigData : " + logUserId + "ends");
        return list;
    }

    /**
     * this method update team composition and task assign
     * @param srvTCId
     * @param nameOfStafftxt
     * @param FirmOrganizationtxt
     * @param PositionConsulttxt
     * @param ConsultCategorytxt
     * @param AreaOfExpertisetxt
     * @param TaskAssignedtxt
     */
    public void updateSrvTeamCompoTaskAssigData(int srvTCId, String nameOfStafftxt, String FirmOrganizationtxt, String PositionConsulttxt, String ConsultCategorytxt, String AreaOfExpertisetxt, String TaskAssignedtxt) {
        logger.debug("updateSrvTeamCompoTaskAssigData : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvTeamComp tcstc set tcstc.empName='" + nameOfStafftxt + "',tcstc.organization='" + FirmOrganizationtxt + "',tcstc.positionDefined='" + PositionConsulttxt + "',tcstc.consultPropCat='" + ConsultCategorytxt + "',tcstc.areaOfExpertise='" + AreaOfExpertisetxt + "',tcstc.taskAssigned='" + TaskAssignedtxt + "' where tcstc.srvTcid=" + srvTCId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSrvTeamCompoTaskAssigData : " + e + "starts");
        }
        logger.debug("updateSrvTeamCompoTaskAssigData : " + logUserId + "ends");
    }

    /**
     * this method add data in history of team composition
     * @param tblCmsSrvTchistory
     * @return boolean
     */
    public boolean addSrvTChistoryData(List<TblCmsSrvTchistory> tblCmsSrvTchistory) throws Exception {
        logger.debug("addSrvTChistoryData : " + logUserId + "starts");
        boolean flag = false;

        tblCmsSrvTchistoryDao.updateOrSaveAllCmsSrvTchistory(tblCmsSrvTchistory);
        flag = true;

        logger.debug("addSrvTChistoryData : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method get max history count of TC
     * @param formMapId
     * @return List<Object>
     */
    public List<Object> getSrvTCHistMaxCount(int formMapId) {
        logger.debug("getSrvTCHistMaxCount : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select Distinct(histCnt) from TblCmsSrvTchistory where srvFormMapId =" + formMapId + " ORDER BY  histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvTCHistMaxCount : " + e + "starts");
        }
        logger.debug("getSrvTCHistMaxCount : " + logUserId + "ends");
        return list;
    }

    /**
     * This method update Payment schedule in case of lump sum
     * @param SrvPsid
     */
    public void updateLumpSumPr(int SrvPsid) {
        logger.debug("updateLumpSumPr : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvPaymentSch tcsps set tcsps.status='Completed',tcsps.completedDt=current_timestamp() where tcsps.srvPsid=" + SrvPsid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateLumpSumPr : " + e + "starts");
        }
        logger.debug("updateLumpSumPr : " + logUserId + "ends");
    }

    /**
     * This method add details to form map table
     * @param map
     */
    public void addToFormMap(TblCmsSrvFormMap map) {
        logger.debug("addToFormMap : " + logUserId + "starts");
        try {
            formMapDao.addTblCmsSrvFormMap(map);
        } catch (Exception e) {
            logger.error("addToFormMap : " + logUserId + "ends");
        }
        logger.debug("addToFormMap : " + logUserId + "ends");
    }

    /**
     * This method get the all service work plan
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getAllServiceWorkPlan(int formMapId) {
        logger.debug("getAllFormsForService : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select  srwp.srNo,srwp.activity,srwp.startDt,srwp.noOfDays,srwp.endDt,srwp.remarks,srwp.srvFormMapId,srwp.srvWorkPlanId ");
        br.append("from TblCmsSrvWorkPlan srwp,TblCmsSrvFormMap sfmp where srwp.srvFormMapId = sfmp.srvFormMapId ");
        br.append("and srwp.srvFormMapId  = " + formMapId);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getAllServiceWorkPlan : " + e);
        }
        logger.debug("getAllServiceWorkPlan : " + logUserId + "ends");
        return list;
    }

    /**
     * this method gets the fomrMap Id on success of dumping data
     * @param tenderId
     * @param formId
     * @return List<Object>
     */
    public List<Object> dataSucssDump(int tenderId, int formId) {
        logger.debug("dataSucssDump : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tsm.srvFormMapId from TblCmsSrvFormMap tsm where tsm.tenderId=" + tenderId + " and tsm.formId=" + formId);

        } catch (Exception e) {
            logger.error("dataSucssDump : " + e);
        }
        logger.debug("dataSucssDump : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get the srmForm Map ID
     * @param tenderId
     * @param srvBoqId
     * @return List<Object>
     */
    public List<Object> getSrvFormMapId(int tenderId, int srvBoqId) {
        logger.debug("getSrvFormMapId : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tsm.srvFormMapId from TblCmsSrvFormMap tsm where tsm.tenderId=" + tenderId + " and tsm.srvBoqId=" + srvBoqId);

        } catch (Exception e) {
            logger.error("getSrvFormMapId : " + e);
        }
        logger.debug("getSrvFormMapId : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get the data for Consultant Composition
     * @param tenderFormId
     * @param colNo
     * @return
     */
    public List<TblTenderCells> getDataForCC(int tenderFormId, int colNo) {
        logger.debug("getDataForCC : " + logUserId + "starts");
        StringBuilder br = new StringBuilder();
        br.append("select tc.cellvalue from TblTenderCells tc,TblTenderTables tt,TblTenderForms tf ");
        br.append("where tt.tblTenderForms.tenderFormId=tf.tenderFormId and tc.tblTenderTables.tenderTableId=tt.tenderTableId ");
        br.append("and tf.tenderFormId=" + tenderFormId + " and columnId<=" + colNo + " order BY tc.rowId,tc.columnId");
        List<Object> objs = new ArrayList<Object>();
        List<TblTenderCells> tblTableCells = new ArrayList<TblTenderCells>();
        try {
            objs = hibernateQueryDao.getSingleColQuery(br.toString());
            for (Object i : objs) {
                tblTableCells.add(new TblTenderCells(i.toString()));
            }
        } catch (Exception e) {
            logger.error("getDataForCC : " + e);
            tblTableCells = null;
        }
        logger.debug("getDataForCC : " + logUserId + "ends");
        return tblTableCells;
    }

    /**
     * This method add data to consultant composition
     * @param comps
     */
    public void addToConComp(List<TblCmsSrvCnsltComp> comps) throws Exception {
        logger.debug("addToConComp : " + logUserId + "starts");

        tblCmsSrvCnsltCompDao.updateOrSaveEstCost(comps);

        logger.debug("addToConComp : " + logUserId + "ends");
    }

    /**
     * This method add data to Payment Schedule
     * @param paymentSch
     */
    public void addToPaymentSch(List<TblCmsSrvPaymentSch> paymentSch) throws Exception {
        logger.debug("addToConComp : " + logUserId + "starts");

        paymentSchDao.updateOrSaveEstCost(paymentSch);

        logger.debug("addToConComp : " + logUserId + "ends");
    }

    /**
     * This method add data to staff input days total
     * @param srvStaffDaysCnts
     */
    public void addToStaffdayCount(List<TblCmsSrvStaffDaysCnt> srvStaffDaysCnts) throws Exception {
        logger.debug("addToStaffdayCount : " + logUserId + "starts");

        daysCntDao.updateOrSaveEstCost(srvStaffDaysCnts);

        logger.debug("addToStaffdayCount : " + logUserId + "ends");
    }

    /**
     * This method gets the service workplan list
     * @param srvBoqId
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getserviceWorkPlanList(int srvBoqId, int formMapId) {
        logger.debug("getserviceWorkPlanList : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tcw.srNo,tcw.activity,tcw.startDt,tcw.noOfDays,tcw.endDt,tcw.remarks,tcw.srvFormMapId,tcw.srvWorkPlanId ");
        br.append("from tbl_CMS_SrvFormMap tcm,tbl_CMS_SrvWorkPlan tcw where tcm.srvBoqId=" + srvBoqId);
        br.append(" and tcm.srvFormMapId=tcw.srvFormMapId and tcm.srvFormMapId=" + formMapId);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getserviceWorkPlanList : " + e);
        }
        logger.debug("getserviceWorkPlanList : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get the list of attendance sheet
     * @param SheetId
     * @return List<Object[]>
     */
    public List<Object[]> ViewAttSheetDtls(int SheetId) {
        logger.debug("ViewAttSheetDtls : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tsr.srno,tsr.empName,tsr.positionAssigned,tad.noOfDays,tad.rate,taa.monthId,taa.year,tad.workFrom,tad.attSheetDtlId,taa.attSheetId,(tad.noOfDays*tad.rate),tad.remarksByPe,tsr.srvTcid ");
        br.append("from TblCmsSrvTeamComp tsr,TblCmsSrvAttSheetDtl tad,TblCmsSrvAttSheetMaster taa ");
        br.append("where tad.srvTcid=tsr.srvTcid and taa.attSheetId=tad.tblCmsSrvAttSheetMaster.attSheetId and tad.tblCmsSrvAttSheetMaster.attSheetId=" + SheetId);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("ViewAttSheetDtls : " + e);
        }
        logger.debug("ViewAttSheetDtls : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get the team composition data
     * @param srvFormMapId
     * @return List<TblCmsSrvTeamComp>
     */
    public List<TblCmsSrvTeamComp> getTeamCompositionData(int srvFormMapId) {
        logger.debug("getTeamCompositionData : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvTeamComp> list = null;
        try {
            list = teamCompDao.findTblCmsSrvTeamComp("srvFormMapId", Operation_enum.EQ, srvFormMapId);

        } catch (Exception e) {
            logger.error("getTeamCompositionData : " + logUserId + "ends");
        }
        logger.debug("getTeamCompositionData : " + logUserId + "ends");
        return list;
    }

    /**
     * This method gets the staff schedule data
     * @param srvFormMapId
     * @return List<TblCmsSrvStaffSch>
     */
    public List<TblCmsSrvStaffSch> getStaffScheduleData(int srvFormMapId) {
        logger.debug("getStaffScheduleData : " + logUserId + "starts");
        boolean flag = false;
        List<Object[]> list = null;
        List<TblCmsSrvStaffSch> tmplist = new ArrayList<TblCmsSrvStaffSch>();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ssch.srvSsid,ssch.srvTcid,ssch.workFrom,ssch.startDt,ssch.endDt,ssch.noOfDays,stc.empName,ssch.srvFormMapId,ssch.srno");
            sb.append(" from TblCmsSrvStaffSch ssch,TblCmsSrvTeamComp stc");
            sb.append(" where ssch.srvTcid = stc.srvTcid and ssch.srvFormMapId =" + srvFormMapId);
            list = hibernateQueryDao.createNewQuery(sb.toString());
            for (Object[] i : list) {
                tmplist.add(new TblCmsSrvStaffSch(Integer.parseInt(i[0].toString()), Integer.parseInt(i[1].toString()), i[2].toString(), (Date) i[3], (Date) i[4], Integer.parseInt(i[5].toString()), i[6].toString(), Integer.parseInt(i[7].toString()), i[8].toString()));
            }
        } catch (Exception e) {
            logger.error("getStaffScheduleData : " + logUserId + "ends");
        }
        logger.debug("getStaffScheduleData : " + logUserId + "ends");
        return tmplist;
    }

    /**
     * This method get the salary reimbursable data by passing srvFormMapID
     * @param srvFormMapId
     * @return List<TblCmsSrvSalaryRe> 
     */
    public List<TblCmsSrvSalaryRe> getSalaryReimbursData(int srvFormMapId) {
        logger.debug("getSalaryReimbursData : " + logUserId + "starts");
        boolean flag = false;
        List<Object[]> list = null;
        List<TblCmsSrvSalaryRe> tmplist = new ArrayList<TblCmsSrvSalaryRe>();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select ssch.srvSrid,ssch.srvTcid,ssch.workFrom,ssch.workMonths,ssch.empMonthSalary,ssch.totalSalary,ssch.noOfDays,stc.empName,ssch.srNo,ssch.srvFormMapId");
            sb.append(" from TblCmsSrvSalaryRe ssch,TblCmsSrvTeamComp stc");
            sb.append(" where ssch.srvTcid = stc.srvTcid and ssch.srvFormMapId =" + srvFormMapId);
            list = hibernateQueryDao.createNewQuery(sb.toString());
            for (Object[] i : list) {
                tmplist.add(new TblCmsSrvSalaryRe(Integer.parseInt(i[0].toString()), Integer.parseInt(i[1].toString()), i[2].toString(), new BigDecimal(i[3].toString()), new BigDecimal(i[4].toString()), new BigDecimal(i[5].toString()), Integer.parseInt(i[6].toString()), i[7].toString(), i[8].toString(), Integer.parseInt(i[9].toString())));
            }
        } catch (Exception e) {
            logger.error("getSalaryReimbursData : " + logUserId + "ends");
        }
        logger.debug("getSalaryReimbursData : " + logUserId + "ends");
        return tmplist;
    }

    /**
     * This method get the name of combo values by passing it's item value
     * @param integer
     * @param i
     * @return List<SPCommonSearchDataMore>
     */
    public List<SPCommonSearchDataMore> getListBoxItemName(Integer integer, int i) {
        logger.debug("getListBoxItemName : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("getListBoxTextForService", integer + "", i + "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            System.out.println("getListBoxItemName : " + e + "starts");
        }
        logger.debug("getListForEditForWorks : " + logUserId + "ends");
        return mores;
    }

    /**
     * This method adds data to attendance sheet master
     * @param master
     */
    public void addToAttMaster(TblCmsSrvAttSheetMaster master) {
        logger.debug("addToAttMaster : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            sheetMasterDao.addTblCmsSrvAttSheetMaster(master);
        } catch (Exception e) {
            System.out.println("addToAttMaster : " + e + "starts");
        }
        logger.debug("addToAttMaster : " + logUserId + "ends");
    }

    /**
     * This method add data to attendance sheet details
     * @param dtls
     */
    public void addToAttSheetDetail(List<TblCmsSrvAttSheetDtl> dtls) {
        logger.debug("addToAttSheetDetail : " + logUserId + "starts");
        try {
            sheetDtlDao.updateOrSaveEstCost(dtls);
        } catch (Exception e) {
            System.out.println("addToAttSheetDetail : " + e + "starts");
        }
        logger.debug("addToAttSheetDetail : " + logUserId + "ends");
    }

    /**
     * this method checks that attendance sheet of particular month and year has been prepare or not
     * @param monId
     * @param yearId
     * @param tenderId
     * @return long (1=prepared / 0=not prepared
     */
    public long checkAttExist(String monId, String yearId, String tenderId) {
        logger.debug("checkAttExist : " + logUserId + "starts");
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblCmsSrvAttSheetMaster tat", "tat.monthId=" + monId + " and year=" + yearId + " and tat.tenderId=" + tenderId);
        } catch (Exception e) {
            System.out.println("checkAttExist : " + e + "starts");
        }
        logger.debug("checkAttExist : " + logUserId + "ends");
        return count;
    }

    /**
     * This method get all attendance sheet data by tenderId
     * @param tenderId
     * @return List<TblCmsSrvAttSheetMaster>
     */
    public List<TblCmsSrvAttSheetMaster> getAllAttSheetDtls(int tenderId) {
        logger.debug("getAllAttSheetDtls : " + logUserId + "starts");
        List<TblCmsSrvAttSheetMaster> list = null;
        try {
            list = sheetMasterDao.findTblCmsSrvAttSheetMaster("tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception e) {
            System.out.println("getAllAttSheetDtls : " + e + "starts");
        }
        logger.debug("getAllAttSheetDtls : " + logUserId + "ends");
        return list;
    }

    /**
     * This method update sheet details
     * @param sheetid
     * @param status
     * @param Remarks
     */
    public void updateSheetDtls(String sheetid, String status, String Remarks) {
        logger.debug("updateSheetDtls : " + logUserId + "starts");
        List<TblCmsSrvAttSheetMaster> list = null;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblCmsSrvAttSheetDtl set remarksByPe='" + Remarks + "' where attSheetDtlId=" + sheetid);
        } catch (Exception e) {
            System.out.println("updateSheetDtls : " + e + "starts");
        }
        logger.debug("updateSheetDtls : " + logUserId + "ends");
    }

    /**
     * This method update sheet details for tenderer side
     * @param sheetid
     * @param NoOfDays
     */
    public void updateSheetDtlsForTend(String sheetid, String NoOfDays) {
        logger.debug("updateSheetDtlsForTend : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblCmsSrvAttSheetDtl set noOfDays='" + NoOfDays + "' where attSheetDtlId=" + sheetid);
        } catch (Exception e) {
            System.out.println("updateSheetDtlsForTend : " + e + "starts");
        }
        logger.debug("updateSheetDtlsForTend : " + logUserId + "ends");
    }

    /**
     * this method updates staff input days total
     * @param tcId
     * @param onsitecnt
     * @param offsitecnt
     * @param editcase
     */
    public void updateDaysCnt(String tcId, String onsitecnt, String offsitecnt, String editcase) {
        logger.debug("updateDaysCnt : " + logUserId + "starts");
        try {
            if ("".equals(editcase)) {
                hibernateQueryDao.updateDeleteNewQuery("update TblCmsSrvStaffDaysCnt set usedOnSiteCnt=usedOnSiteCnt+" + Integer.parseInt(onsitecnt) + ",usedOffSiteCnt=usedOffSiteCnt+" + Integer.parseInt(offsitecnt) + " where srvTcid=" + tcId);
            } else {
                hibernateQueryDao.updateDeleteNewQuery("update TblCmsSrvStaffDaysCnt set usedOnSiteCnt=" + Integer.parseInt(onsitecnt) + ",usedOffSiteCnt=" + Integer.parseInt(offsitecnt) + " where srvTcid=" + tcId);
            }

        } catch (Exception e) {
            System.out.println("updateDaysCnt : " + e + "starts");
        }
        logger.debug("updateDaysCnt : " + logUserId + "ends");
    }

    /**
     * This method update the status of sheet master
     * @param sheetid
     * @param status
     */
    public void updateSheetMaster(String sheetid, String status) {
        logger.debug("updateSheetMaster : " + logUserId + "starts");
        List<TblCmsSrvAttSheetMaster> list = null;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblCmsSrvAttSheetMaster set status='" + status + "' where attSheetId=" + sheetid);
        } catch (Exception e) {
            System.out.println("updateSheetMaster : " + e + "starts");
        }
        logger.debug("updateSheetMaster : " + logUserId + "ends");
    }

    /**
     * This method is for from when edit attendance sheet will be active.
     * @param wpId
     * @param prId
     * @return boolean (false / true)
     */
    public boolean editLinkForAttOrNot(String wpId, int prId) {
        logger.debug("editLinkForAttOrNot : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = true;
        try {
            list = hibernateQueryDao.getSingleColQuery("select ti.tblCmsWpMaster.wpId wpId from TblCmsInvoiceMaster ti where ti.tblCmsWpMaster.wpId=" + wpId + " and ti.tillPRId=" + prId);
            if (list != null && !list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            System.out.println("editLinkForAttOrNot : " + e + "starts");
        }
        logger.debug("editLinkForAttOrNot : " + logUserId + "ends");
        return flag;
    }

    /**
     * This method get wpId from the lot Id
     * @param lotId
     * @return List<Object>
     */
    public List<Object> getWpIdFrmLotId(int lotId) {
        logger.debug("getWpIdFrmLotId : " + logUserId + "starts");
        List<Object> list = null;
        boolean flag = true;
        try {
            list = hibernateQueryDao.getSingleColQuery("select tm.wpId from TblCmsWpMaster tm where tm.wpLotId=" + lotId + " and tm.wpName='ATS'");
            if (list != null && !list.isEmpty()) {
                flag = false;
            }
        } catch (Exception e) {
            System.out.println("getWpIdFrmLotId : " + e + "starts");
        }
        logger.debug("getWpIdFrmLotId : " + logUserId + "ends");
        return list;
    }

    /**
     * This method gets the staff input total days 
     * @param tenderId
     * @param lotid
     * @return List<Object[]>
     */
    public List<Object[]> getStaffInputTotal(int tenderId, int lotid) {
        logger.debug("getStaffInputTotal : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select a.empName, a.positionAssigned, b.offSiteCnt, b.onSiteCnt, b.totalCnt ");
        br.append("from TblCmsSrvTeamComp a, TblCmsSrvStaffDaysCnt b ");
        br.append("where a.srvTcid = b.srvTcid and b.tenderId = " + tenderId + " and b.lotId=" + lotid);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
            System.out.println("list size is :" + list.size());
        } catch (Exception e) {
            logger.error("getStaffInputTotal : " + e);
        }
        logger.debug("getStaffInputTotal : " + logUserId + "ends");
        return list;
    }

    /**
     * this method update salary reimbursable data
     * @param SrvSrid
     * @param workmonths
     * @param totSalary
     */
    public void updateSalReimbursable(int SrvSrid, float workmonths, String totSalary) {
        logger.debug("updateSalReimbursable : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvSalaryRe tsrvsal set tsrvsal.workMonths= " + workmonths + ", tsrvsal.totalSalary= '" + new BigDecimal(totSalary) + "' where tsrvsal.srvSrid=" + SrvSrid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateSalReimbursable : " + e + "starts");
        }
        logger.debug("updateSalReimbursable : " + logUserId + "ends");
    }

    /**
     * This method update staffing schedule
     * @param srvSsId
     * @param workFrom
     * @param startDate
     * @param noOfDays
     * @param endDate
     */
    public void updateStaffingSchedule(int srvSsId, String workFrom, Date startDate, int noOfDays, Date endDate) {
        logger.debug("updateStaffingSchedule : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvStaffSch  set workFrom='" + workFrom + "', startDt ='" + new java.sql.Date(startDate.getTime()) + "', noOfDays=" + noOfDays + ", endDt ='" + new java.sql.Date(endDate.getTime()) + "' where srvSsid=" + srvSsId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateStaffingSchedule : " + e + "starts");
        }
        logger.debug("updateStaffingSchedule : " + logUserId + "ends");
    }

    /**
     * This method update staff Days count
     * @param srvTcId
     * @param onsitetot
     * @param offsitetot
     * @param tot
     */
    public void updateStaffDayCount(int srvTcId, int onsitetot, int offsitetot, int tot) {
        logger.debug("updateStaffDayCount : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvStaffDaysCnt  set onSiteCnt=" + onsitetot + ", offSiteCnt =" + offsitetot + ", totalCnt=" + tot + " where srvTcid=" + srvTcId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("updateStaffDayCount : " + e + "starts");
        }
        logger.debug("updateStaffDayCount : " + logUserId + "ends");
    }

    /**
     * This method send attendance sheet to consultant for edit
     * @param sheetId
     */
    public void sendToConsultant(String sheetId) {
        logger.debug("sendToConsultant : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "update TblCmsSrvAttSheetMaster  set status='sendtoconsultant' where attSheetId=" + sheetId;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("sendToConsultant : " + e + "starts");
        }
        logger.debug("sendToConsultant : " + logUserId + "ends");
    }

    /**
     * This method check the attendance sheet table
     * @param sheetId
     * @return
     */
    public String checkAttStatus(String sheetId) {
        logger.debug("checkAttStatus : " + logUserId + "starts");
        String queryS = "";
        String status = "";
        try {
            queryS = "select tam.status from TblCmsSrvAttSheetMaster tam where tam.attSheetId = " + sheetId;
            status = hibernateQueryDao.getSingleColQuery(queryS).get(0).toString();
        } catch (Exception e) {
            logger.error("checkAttStatus : " + e + "starts");
        }
        logger.debug("checkAttStatus : " + logUserId + "ends");
        return status;
    }

    /**
     * this method get the invoice data
     * @param SrvPsid
     * @param tenderId
     * @return List<Object[]> 
     */
    public List<Object[]> getInvoiceData(int SrvPsid, int tenderId) {
        logger.debug("getInvoiceData : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tcim.invoiceId,tcim.totalInvAmt,tcim.invStatus,tcim.isAdvInv,tcim.invoiceNo,tcim.remarks,tcim.conRemarks ");
        br.append("from TblCmsInvoiceMaster tcim where tcim.tillPrid=" + SrvPsid + " and tcim.tenderId=" + tenderId + "");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getInvoiceData : " + e);
        }
        logger.debug("getInvoiceData : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get payment schedule data by its primary key
     * @param srvPSId
     * @return List<TblCmsSrvPaymentSch>
     */
    public List<TblCmsSrvPaymentSch> getPaymentScheduleDatabySrvPSId(int srvPSId) {
        logger.debug("getPaymentScheduleDatabySrvPSId : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvPaymentSch> list = null;
        try {
            list = paymentSchDao.findTblCmsSrvPaymentSch("srvPsid", Operation_enum.EQ, srvPSId);

        } catch (Exception e) {
            logger.error("getPaymentScheduleDatabySrvPSId : " + logUserId + "ends");
        }
        logger.debug("getPaymentScheduleDatabySrvPSId : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get srvFormMap Id by tenderId
     * @param tenderId
     * @return
     */
    public List<Object[]> getSrvFormMapId(int tenderId) {
        logger.debug("getSrvFormMapId : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tcsf.srvFormMapId,tcsf.lotId,tcsf.formId,tcwb.tblCmsWpMaster.wpId ");
        br.append("from TblCmsSrvFormMap tcsf,TblCmsWptenderBoqmap tcwb where ");
        br.append("tcsf.srvBoqId='12' and tcsf.tenderId=" + tenderId + " and tcsf.formId=tcwb.wpTenderFormId");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getSrvFormMapId : " + e);
        }
        logger.debug("getSrvFormMapId : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get woId by tenderId
     * @param tenderId
     * @return List<Object[]>
     */
    public List<Object[]> getWpId(int tenderId) {
        logger.debug("getSrvFormMapId : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tcsf.srvFormMapId,tcsf.lotId,tcsf.formId,tcwb.tblCmsWpMaster.wpId ");
        br.append("from TblCmsSrvFormMap tcsf,TblCmsWptenderBoqmap tcwb where ");
        br.append("tcsf.srvBoqId='13' and tcsf.tenderId=" + tenderId + " and tcsf.formId=tcwb.wpTenderFormId");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getSrvFormMapId : " + e);
        }
        logger.debug("getSrvFormMapId : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get srvTc history data
     * @param srvTCId
     * @return List<TblCmsSrvTchistory>
     */
    public List<TblCmsSrvTchistory> getSrvTcHistoryData(int srvTCId) {
        logger.debug("getSrvTcHistoryData : " + logUserId + "starts");
        boolean flag = false;
        List<TblCmsSrvTchistory> list = null;
        try {
            list = tblCmsSrvTchistoryDao.findTblCmsSrvTchistory("srvTcid", Operation_enum.EQ, srvTCId, "histCnt", Operation_enum.EQ, 0);

        } catch (Exception e) {
            logger.error("getSrvTcHistoryData : " + logUserId + "ends");
        }
        logger.debug("getSrvTcHistoryData : " + logUserId + "ends");
        return list;
    }

    /**
     * This method get service TC history data in detail
     * @param srvFormMapId
     * @param histCnt
     * @return
     */
    public List<Object[]> getSrvTcHistoryDetailData(int srvFormMapId, int histCnt) {
        logger.debug("getSrvTcHistoryDetailData : " + logUserId + "starts");
        boolean flag = false;
        List<Object[]> list = null;
        StringBuffer sb = new StringBuffer();
        sb.append("select tch.empName,tch.organization,tch.positionDefined,tch.consultPropCat,tch.areaOfExpertise,tch.taskAssigned,");
        sb.append(" tc.srno,tc.positionAssigned,tc.staffCat,tch.createdDate");
        sb.append(" from TblCmsSrvTeamComp tc,TblCmsSrvTchistory tch");
        sb.append(" where tc.srvTcid = tch.srvTcid and tch.srvFormMapId = " + srvFormMapId + " and tch.histCnt=" + histCnt);
        try {
            list = hibernateQueryDao.createNewQuery(sb.toString());

        } catch (Exception e) {
            logger.error("getSrvTcHistoryDetailData : " + logUserId + "ends");
        }
        logger.debug("getSrvTcHistoryDetailData : " + logUserId + "ends");
        return list;
    }

    /**
     * This method add details to workplan hostory details
     * @param wphistory
     */
    public void addToWorkPlanHistory(List<TblCmsSrvWphistory> wphistory) throws Exception {
        logger.debug("addToWorkPlanHistory : " + logUserId + "starts");

        srvWpHistoryDao.updateOrSaveEstCost(wphistory);


        logger.debug("addToWorkPlanHistory : " + logUserId + "ends");
    }

    /**
     * This method get service work plan history count
     * @param srvFormMapId
     * @return List<Object>
     */
    public List<Object> getSrvWpHistMaxCount(int srvFormMapId) {
        logger.debug("getSrvTCHistMaxCount : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select MAX(histCnt) from TblCmsSrvWphistory where srvFormMapId =" + srvFormMapId + "");
        } catch (Exception e) {
            logger.error("getSrvTCHistMaxCount : " + e + "starts");
        }
        logger.debug("getSrvTCHistMaxCount : " + logUserId + "ends");
        return list;
    }

    /**
     * This method adds data to team composition and task assigned
     * @param tblCmsSrvTeamComp
     * @return boolean
     */
    public boolean addSrvTeamCompoTaskAssigData(List<TblCmsSrvTeamComp> tblCmsSrvTeamComp) {
        logger.debug("addSrvTeamCompoTaskAssigData : " + logUserId + "starts");
        boolean flag = false;
        try {
            teamCompDao.updateOrSaveEstCost(tblCmsSrvTeamComp);
            flag = true;
        } catch (Exception e) {
            logger.error("addSrvTeamCompoTaskAssigData : " + logUserId + " " + e);
            flag = false;
        }
        logger.debug("addSrvTeamCompoTaskAssigData : " + logUserId + "ends");
        return flag;

    }

    /**
     * This method get TC history Count
     * @param srvFormMapId
     * @return List<Object[]>
     */
    public List<Object[]> getTCHistoryDistinctCount(int srvFormMapId) {
        logger.debug("getTCHistoryDistinctCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select distinct(tcst.histCnt),tcst.createdDate from TblCmsSrvTchistory tcst where tcst.srvFormMapId=" + srvFormMapId + " ORDER BY tcst.histCnt desc");
        } catch (Exception e) {
            logger.error("getTCHistoryDistinctCount : " + e + "starts");
        }
        logger.debug("getTCHistoryDistinctCount : " + logUserId + "ends");
        return list;
    }

    /**
     * This method checks the days that will be input in attendance sheet
     * @param tcId
     * @param workfrm
     * @param nod
     * @return long
     */
    public long checkDaysFromAtt(String tcId, String workfrm, String nod) {
        logger.debug("checkDaysFromAtt : " + logUserId + "starts");
        long count = 0;
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select ts.usedOffSiteCnt,ts.usedOnSiteCnt from TblCmsSrvStaffDaysCnt ts where ts.srvTcid=" + tcId);
            if (list != null && !list.isEmpty()) {
                if ("home".equalsIgnoreCase(workfrm)) {
                    if (Integer.parseInt(nod) < Integer.parseInt(list.get(0)[0].toString())) {
                        count = 1;
                    }
                } else {
                    if (Integer.parseInt(nod) < Integer.parseInt(list.get(0)[1].toString())) {
                        count = 1;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("checkDaysFromAtt " + e + "starts");
        }
        logger.debug("checkDaysFromAtt : " + logUserId + "ends");
        return count;
    }

    /**
     * This method set month in salary reimbursable table 
     * @param tcid
     * @param month
     * @param WF
     */
    public void setMonthInSalaryRe(int tcid, BigDecimal month, String WF) {
        logger.debug("setMonthInSalaryRe : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsSrvSalaryRe set workMonths = '" + month + "' where workFrom='" + WF + "' and srvTcid=" + tcid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("setMonthInSalaryRe : " + e + "starts");
        }
        logger.debug("setMonthInSalaryRe : " + logUserId + "ends");
    }

    /**
     * This method set month in salary Reimbursable for variation order
     * @param tcid
     * @param month
     * @param WF
     */
    public void setMonthInSalaryReForVari(int tcid, BigDecimal month, String WF) {
        logger.debug("setMonthInSalaryReForVari : " + logUserId + "starts");
        try {
            String queryS = "update TblCmsSrvSrvari set workMonths = '" + month + "' where workFrom='" + WF + "' and srvTcvariId=" + tcid;
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("setMonthInSalaryReForVari : " + e + "starts");
        }
        logger.debug("setMonthInSalaryReForVari : " + logUserId + "ends");
    }

    /**
     * This method get work plan history count
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getSrvWpHistDistCount(int formMapId) {
        logger.debug("getSrvWpHistDistCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select Distinct(tcsw.histCnt),tcsw.createdDate from TblCmsSrvWphistory tcsw where tcsw.srvFormMapId = " + formMapId + " ORDER BY  tcsw.histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvWpHistDistCount : " + e + "starts");
        }
        logger.debug("getSrvWpHistDistCount : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get the total number of days from SS
     * @param tcId
     * @param WF
     * @return List<Object>
     */
    public List<Object> getTotalNoOfDaysFrmSS(int tcId, String WF) {
        logger.debug("getTotalNoOfDaysFrmSS : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select  sum(srwp.noOfDays)");
        br.append(" from TblCmsSrvSsvari srwp");
        br.append(" where srwp.srvTcvariId=" + tcId + " and srwp.workFrom='" + WF + "'");
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());
        } catch (Exception e) {
            logger.error("getTotalNoOfDaysFrmSS : " + e);
        }
        logger.debug("getTotalNoOfDaysFrmSS : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get service work plan history
     * @param formMapId
     * @param histCnt
     * @return List<Object[]>
     */
    public List<Object[]> getServiceWorkPlanHistory(int formMapId, int histCnt) {
        logger.debug("getServiceWorkPlanHistory : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select  srwp.srNo,srwp.activity,srwp.startDt,srwp.noOfDays,srwp.endDt,srwp.remarks,srwp.srvFormMapId,srwp.srvWorkPlanId,srwp.histCnt,srwp.createdDate");
        br.append(" from TblCmsSrvWphistory srwp");
        br.append(" where srwp.srvFormMapId  = " + formMapId + " and srwp.histCnt=" + histCnt);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
            //     System.out.println("service workplan history :"+br.toString());
        } catch (Exception e) {
            logger.error("getServiceWorkPlanHistory : " + e);
        }
        logger.debug("getServiceWorkPlanHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * this method adds data to staffing schedule history
     * @param sshistory
     */
    public void addToStaffScheduleHistory(List<TblCmsSrvSshistory> sshistory) throws Exception {
        logger.debug("addToStaffScheduleHistory : " + logUserId + "starts");

        srvSsHistoryDao.updateOrSaveEstCost(sshistory);

        logger.debug("addToWorkPlanHistory : " + logUserId + "ends");
    }

    /**
     * this method get service history count distinct
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getSrvSsHistDistCount(int formMapId) {
        logger.debug("getSrvSsHistDistCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select Distinct(tcss.histCnt),tcss.createdDate from TblCmsSrvSshistory tcss where tcss.srvFormMapId = " + formMapId + " ORDER BY  tcss.histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvSsHistDistCount : " + e + "starts");
        }
        logger.debug("getSrvSsHistDistCount : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get staffing schedule history data
     * @param formMapId
     * @param histCnt
     * @return List<Object[]>
     */
    public List<Object[]> getStaffScheduleHistory(int formMapId, int histCnt) {
        logger.debug("getStaffScheduleHistory : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select ssh.srno,ssh.workFrom,ssh.startDt,ssh.noOfDays,ssh.endDt,ssh.srvFormMapId,ssh.srvSshistId,ssh.histCnt,ss.srno,ss.empName,ssh.createdDate");
        br.append(" from TblCmsSrvSshistory ssh,TblCmsSrvStaffSch ss");
        br.append(" where ss.srvSsid = ssh.srvSsid and ssh.srvFormMapId  = " + formMapId + " and ssh.histCnt=" + histCnt);
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getStaffScheduleHistory : " + e);
        }
        logger.debug("getStaffScheduleHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * add to payment schedule history table
     * @param pshistory
     */
    public void addToPaymentScheduleHistory(List<TblCmsSrvPshistory> pshistory) throws Exception {
        logger.debug("addToPaymentScheduleHistory : " + logUserId + "starts");

        srvPsHistoryDao.updateOrSaveEstCost(pshistory);

        logger.debug("addToPaymentScheduleHistory : " + logUserId + "ends");
    }

    /**
     * get payment schedule history count
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getSrvPsHistDistCount(int formMapId) {
        logger.debug("getSrvPsHistDistCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select Distinct(tcsp.histCnt),tcsp.createdDate from TblCmsSrvPshistory tcsp where tcsp.srvFormMapId = " + formMapId + " ORDER BY  tcsp.histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvPsHistDistCount : " + e + "starts");
        }
        logger.debug("getSrvPsHistDistCount : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get consultant composition history data
     * @param formMapId
     * @param histCnt
     * @return List<TblCmsSrvCchistory> 
     */
    public List<TblCmsSrvCchistory> getCChistory(int formMapId, int histCnt) {
        logger.debug("getCChistory : " + logUserId + "starts");
        List<TblCmsSrvCchistory> list = null;
        try {
            list = srvCchistoryDao.findTblCmsSrvCchistory("srvFormMapId", Operation_enum.EQ, formMapId, "histCnt", Operation_enum.EQ, histCnt);
        } catch (Exception e) {
            logger.error("getCChistory : " + e);
        }
        logger.debug("getCChistory : " + logUserId + "ends");
        return list;
    }

    /**
     * this method get payment schedule history data
     * @param formMapId
     * @param histCnt
     * @return List<Object[]>
     */
    public List<Object[]> getPaymentScheduleHistory(int formMapId, int histCnt) {
        logger.debug("getPaymentScheduleHistory : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuffer sb = new StringBuffer();
        sb.append("select psh.srvPsid,psh.srvFormMapId,ps.srNo,ps.milestone,ps.percentOfCtrVal,ps.description,psh.peEndDate,psh.endDate,psh.createdDate");
        sb.append(" from TblCmsSrvPshistory psh,TblCmsSrvPaymentSch ps");
        sb.append(" where ps.srvPsid = psh.srvPsid and psh.srvFormMapId = " + formMapId + " and psh.histCnt=" + histCnt);
        try {
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getPaymentScheduleHistory : " + e);
        }
        logger.debug("getPaymentScheduleHistory : " + logUserId + "ends");
        return list;
    }

    /**
     * this method allow to finalize variation order for service case
     * @param varId
     * @return long
     */
    public long allowFinaliseVari(int varId) {
        long cc = 0;
        try {
            logger.debug("allowFinaliseVari : " + logUserId + "starts");
            cc = hibernateQueryDao.countForNewQuery("TblCmsSrvTcvari tcc", "tcc.variOrdId=" + varId + " and tcc.srvTcvariId not in (select tc.srvTcvariId from TblCmsSrvTcvari tc,TblCmsSrvSsvari ss,TblCmsSrvSrvari sr where tc.srvTcvariId=ss.srvTcvariId and tc.srvTcvariId=sr.srvTcvariId and tc.variOrdId=" + varId + ")");
            logger.debug("allowFinaliseVari : " + logUserId + "ends");

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CMSService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cc;
    }

    /**
     * get RE history data
     * @param formMapId
     * @param histCnt
     * @return List<TblCmsSrvReExHistory> 
     */
    public List<TblCmsSrvReExHistory> getREhistory(int formMapId, int histCnt) {
        logger.debug("getREhistory : " + logUserId + "starts");
        List<TblCmsSrvReExHistory> list = null;
        try {
            list = historyDao.findTblCmsSrvReExHistory("srvFormMapId", Operation_enum.EQ, formMapId, "histCnt", Operation_enum.EQ, histCnt);
        } catch (Exception e) {
            logger.error("getREhistory : " + e);
        }
        logger.debug("getREhistory : " + logUserId + "ends");
        return list;
    }

    /**
     * Ad data to CC history
     * @param srvCchistory
     */
    public void AddToCCHistory(List<TblCmsSrvCchistory> srvCchistory) throws Exception {
        logger.debug("AddToCCHistory : " + logUserId + "starts");

        srvCchistoryDao.updateOrSaveEstCost(srvCchistory);

        logger.debug("AddToCCHistory : " + logUserId + "ends");
    }

    /**
     * add data to Reimbursable expenses
     * @param srvCchistory
     */
    public void AddToREHistory(List<TblCmsSrvReExHistory> srvCchistory) throws Exception {
        logger.debug("AddToREHistory : " + logUserId + "starts");

        historyDao.updateOrSaveEstCost(srvCchistory);

        logger.debug("AddToREHistory : " + logUserId + "ends");
    }

    /**
     * get service consultant composition history count
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getSrvCCHistDistCount(int formMapId) {
        logger.debug("getSrvCCHistDistCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select Distinct(tcsc.histCnt),tcsc.createdDate from TblCmsSrvCchistory tcsc where tcsc.srvFormMapId = " + formMapId + " ORDER BY  tcsc.histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvCCHistDistCount : " + e + "starts");
        }
        logger.debug("getSrvCCHistDistCount : " + logUserId + "ends");
        return list;
    }

    /**
     * get reimbursable history count details
     * @param formMapId
     * @return List<Object[]>
     */
    public List<Object[]> getSrvREHistDistCount(int formMapId) {
        logger.debug("getSrvREHistDistCount : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select Distinct(tcsr.histCnt),tcsr.createdDate from TblCmsSrvReExHistory tcsr where tcsr.srvFormMapId = " + formMapId + " ORDER BY  tcsr.histCnt desc");
        } catch (Exception e) {
            logger.error("getSrvREHistDistCount : " + e + "starts");
        }
        logger.debug("getSrvREHistDistCount : " + logUserId + "ends");
        return list;
    }

    /**
     * get details of team composition variation order
     * @param varId
     * @return List<TblCmsSrvTcvari>
     */
    public List<TblCmsSrvTcvari> getDetailOfTCForVari(int varId) {
        logger.debug("getDetailOfYTCForVari : " + logUserId + "starts");
        List<TblCmsSrvTcvari> list = null;
        try {
            list = tcvariDao.findTblCmsSrvTcvari("variOrdId", Operation_enum.EQ, varId);
        } catch (Exception e) {
            logger.error("getDetailOfYTCForVari : " + e + "starts");
        }
        logger.debug("getDetailOfYTCForVari : " + logUserId + "ends");
        return list;
    }

    /**
     * get details of staffing schedule variation order
     * @param varId
     * @return List<TblCmsSrvSsvari>
     */
    public List<TblCmsSrvSsvari> getDetailOfSSForVari(int varId) {
        logger.debug("getDetailOfYSSForVari : " + logUserId + "starts");
        List<TblCmsSrvSsvari> list = null;
        try {
            list = ssvariDao.findTblCmsSrvSsvari("variOrdId", Operation_enum.EQ, varId);
        } catch (Exception e) {
            logger.error("getDetailOfYSSForVari : " + e + "starts");
        }
        logger.debug("getDetailOfYSSForVari : " + logUserId + "ends");
        return list;
    }

    /**
     * get details of SR for variation order
     * @param varId
     * @return List<TblCmsSrvSrvari>
     */
    public List<TblCmsSrvSrvari> getDetailOfSRForVari(int varId) {
        logger.debug("getDetailOfYSRForVari : " + logUserId + "starts");
        List<TblCmsSrvSrvari> list = null;
        try {
            list = srvariDao.findTblCmsSrvSrvari("variOrdId", Operation_enum.EQ, varId);
        } catch (Exception e) {
            logger.error("getDetailOfYSRForVari : " + e + "starts");
        }
        logger.debug("getDetailOfYSRForVari : " + logUserId + "ends");
        return list;
    }

    /**
     *  add details to SR variation order
     * @param srvaris
     */
    public void addToSrVari(List<TblCmsSrvSrvari> srvaris) {
        logger.debug("addToSrVari : " + logUserId + "starts");
        try {
            srvariDao.updateOrSaveEstCost(srvaris);
        } catch (Exception e) {
            logger.error("addToSrVari : " + e + "starts");
        }
        logger.debug("addToSrVari : " + logUserId + "ends");
    }

    /**
     * add details to staffing schedule variation order
     * @param srvaris
     */
    public void addToSsVari(List<TblCmsSrvSsvari> srvaris) {
        logger.debug("addToSsVari : " + logUserId + "starts");
        try {
            ssvariDao.updateOrSaveEstCost(srvaris);
        } catch (Exception e) {
            logger.error("addToSsVari : " + e + "starts");
        }
        logger.debug("addToSsVari : " + logUserId + "ends");
    }

    /**
     * add details to team composition
     * @param srvTcvaris
     */
    public void addToTcVari(List<TblCmsSrvTcvari> srvTcvaris) {
        logger.debug("addToTcVari : " + logUserId + "starts");
        try {
            tcvariDao.updateOrSaveEstCost(srvTcvaris);
        } catch (Exception e) {
            logger.error("addToTcVari : " + e + "starts");
        }
        logger.debug("addToTcVari : " + logUserId + "ends");
    }

    /**
     * delete from team composition variation order
     * @param variDtlId
     */
    public void deleteFromTCVari(String variDtlId) {
        logger.debug("deleteFromTCVari : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete TblCmsSrvTcvari where srvTcvariId in (" + variDtlId + ")");
            hibernateQueryDao.updateDeleteNewQuery("delete TblCmsSrvSrvari where srvTcvariId in (" + variDtlId + ")");
            hibernateQueryDao.updateDeleteNewQuery("delete TblCmsSrvSsvari where srvTcvariId in (" + variDtlId + ")");
        } catch (Exception e) {
            logger.error("deleteFromTCVari : " + e + "starts");
        }
        logger.debug("deleteFromTCVari : " + logUserId + "ends");
    }

    /**
     * delete from staffing schedule variation order
     * @param variDtlId
     */
    public void deleteFromSSVari(String variDtlId) {
        logger.debug("deleteFromSSVari : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete TblCmsSrvSsvari where srvSsvariId in (" + variDtlId + ")");
        } catch (Exception e) {
            logger.error("deleteFromSSVari : " + e + "starts");
        }
        logger.debug("deleteFromSSVari : " + logUserId + "ends");
    }

    /**
     * delete data from SR variation order
     * @param variDtlId
     */
    public void deleteFromSRVari(String variDtlId) {
        logger.debug("deleteFromSRVari : " + logUserId + "starts");
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete TblCmsSrvSrvari where srvSrvariId in (" + variDtlId + ")");
        } catch (Exception e) {
            logger.error("deleteFromSRVari : " + e + "starts");
        }
        logger.debug("deleteFromSRVari : " + logUserId + "ends");
    }

    /**
     * add data to work plan variation order
     * @param srvWpvari
     */
    public void AddToSrvWpVari(List<TblCmsSrvWpvari> srvWpvari) {
        logger.debug("AddToSrvWpVari : " + logUserId + "starts");
        try {
            srvWpVariDao.updateOrSaveEstCost(srvWpvari);
        } catch (Exception e) {
            logger.error("AddToSrvWpVari : " + e);
        }
        logger.debug("AddToSrvWpVari : " + logUserId + "ends");
    }

    /**
     * get work plan variation order
     * @param variOrderId
     * @return List<TblCmsSrvWpvari>
     */
    public List<TblCmsSrvWpvari> getSrvWpVariationOrder(int variOrderId) {
        logger.debug("getSrvWpVariationOrder : " + logUserId + "starts");
        List<TblCmsSrvWpvari> list = null;
        try {
            list = srvWpVariDao.findTblCmsSrvWpvari("variOrdId", Operation_enum.EQ, variOrderId);
        } catch (Exception e) {
            logger.error("getSrvWpVariationOrder : " + e);
        }
        logger.debug("getSrvWpVariationOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * delete workplan variation order data 
     * @param srvWpvariId
     */
    public void deleteSrvWpVariationOrder(String srvWpvariId) {
        logger.debug("deleteSrvWpVariationOrder : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "delete TblCmsSrvWpvari  where srvWpvariId in (" + srvWpvariId + ")";
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("deleteSrvWpVariationOrder : " + e + "starts");
        }
        logger.debug("deleteSrvWpVariationOrder : " + logUserId + "ends");
    }

    /**
     * add data to payment schedule variation order
     * @param srvPsVari
     */
    public void AddToSrvPsVari(List<TblCmsSrvPsvari> srvPsVari) {
        logger.debug("AddToSrvPsVari : " + logUserId + "starts");
        try {
            srvpsvariDao.updateOrSaveTblCmsSrvPsvari(srvPsVari);
        } catch (Exception e) {
            logger.error("AddToSrvPsVari : " + e);
        }
        logger.debug("AddToSrvPsVari : " + logUserId + "ends");
    }

    /**
     * get payment schedule variation order
     * @param variOrderId
     * @return
     */
    public List<TblCmsSrvPsvari> getSrvPsVariationOrder(int variOrderId) {
        logger.debug("getSrvPsVariationOrder : " + logUserId + "starts");
        List<TblCmsSrvPsvari> list = null;
        try {
            list = srvpsvariDao.findTblCmsSrvPsvari("variOrdId", Operation_enum.EQ, variOrderId);
        } catch (Exception e) {
            logger.error("getSrvPsVariationOrder : " + e);
        }
        logger.debug("getSrvPsVariationOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * delete payment schedule variation order
     * @param srvPsvariId
     */
    public void deleteSrvPsVariationOrder(String srvPsvariId) {
        logger.debug("deleteSrvPsVariationOrder : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "delete TblCmsSrvPsvari  where srvPsvariId in (" + srvPsvariId + ")";
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("deleteSrvPsVariationOrder : " + e + "starts");
        }
        logger.debug("deleteSrvPsVariationOrder : " + logUserId + "ends");
    }

    /**
     * add consultant composition variation order
     * @param srvCcVari
     */
    public void AddToSrvCcVari(List<TblCmsSrvCcvari> srvCcVari) {
        logger.debug("AddToSrvCcVari : " + logUserId + "starts");
        try {
            srvccvariDao.updateOrSaveTblCmsSrvCcvari(srvCcVari);
        } catch (Exception e) {
            logger.error("AddToSrvCcVari : " + e);
        }
        logger.debug("AddToSrvCcVari : " + logUserId + "ends");
    }

    /**
     * this method get Consultant composition variation order
     * @param variOrderId
     * @return
     */
    public List<TblCmsSrvCcvari> getSrvCcVariationOrder(int variOrderId) {
        logger.debug("getSrvCcVariationOrder : " + logUserId + "starts");
        List<TblCmsSrvCcvari> list = null;
        try {
            list = srvccvariDao.findTblCmsSrvCcvari("variOrdId", Operation_enum.EQ, variOrderId);
        } catch (Exception e) {
            logger.error("getSrvCcVariationOrder : " + e);
        }
        logger.debug("getSrvCcVariationOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * this method delete srvVariaiont Order of Consultant Composition
     * @param srvCcvariId
     */
    public void deleteSrvCcVariationOrder(String srvCcvariId) {
        logger.debug("deleteSrvCcVariationOrder : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "delete TblCmsSrvCcvari  where srvCcvariId in (" + srvCcvariId + ")";
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("deleteSrvCcVariationOrder : " + e + "starts");
        }
        logger.debug("deleteSrvCcVariationOrder : " + logUserId + "ends");
    }

    /**
     * this method adds details to reimbursable expenses
     * @param srvReVari
     */
    public void AddToSrvReVari(List<TblCmsSrvRevari> srvReVari) {
        logger.debug("AddToSrvReVari : " + logUserId + "starts");
        try {
            srvrevariDao.updateOrSaveTblCmsSrvRevari(srvReVari);
        } catch (Exception e) {
            logger.error("AddToSrvReVari : " + e);
        }
        logger.debug("AddToSrvReVari : " + logUserId + "ends");
    }

    /**
     * this method get RE variation order
     * @param variOrderId
     * @return List<TblCmsSrvRevari> 
     */
    public List<TblCmsSrvRevari> getSrvReVariationOrder(int variOrderId) {
        logger.debug("getSrvReVariationOrder : " + logUserId + "starts");
        List<TblCmsSrvRevari> list = null;
        try {
            list = srvrevariDao.findTblCmsSrvRevari("variOrdId", Operation_enum.EQ, variOrderId);
        } catch (Exception e) {
            logger.error("getSrvReVariationOrder : " + e);
        }
        logger.debug("getSrvReVariationOrder : " + logUserId + "ends");
        return list;
    }

    /**
     * this method delete RE variation order
     * @param srvCcvariId
     */
    public void deleteSrvReVariationOrder(String srvCcvariId) {
        logger.debug("deleteSrvReVariationOrder : " + logUserId + "starts");
        String queryS = "";
        try {
            queryS = "delete TblCmsSrvRevari  where srvRevariId in (" + srvCcvariId + ")";
            hibernateQueryDao.updateDeleteNewQuery(queryS);
        } catch (Exception e) {
            logger.error("deleteSrvReVariationOrder : " + e + "starts");
        }
        logger.debug("deleteSrvReVariationOrder : " + logUserId + "ends");
    }

    /**
     * this method get the employee name
     * @param tcId
     * @return String
     */
    public String getEmpName(int tcId) {
        logger.debug("getEmpName : " + logUserId + "starts");
        String queryS = "";
        List<Object> getEmpName = null;
        String name = "";
        try {
            queryS = "select tc.empName from TblCmsSrvTcvari tc where tc.srvTcvariId=" + tcId;
            getEmpName = hibernateQueryDao.getSingleColQuery(queryS);
            if (getEmpName != null && !getEmpName.isEmpty()) {
                name = getEmpName.get(0).toString();
            }
        } catch (Exception e) {
            logger.error("getEmpName : " + e + "starts");
        }
        logger.debug("getEmpName : " + logUserId + "ends");
        return name;
    }

    /**
     * this method get list item name for RE by item value
     * @param integer
     * @param i
     * @return List<SPCommonSearchDataMore>
     */
    public List<SPCommonSearchDataMore> getListBoxItemNameForRe(Integer integer, int i) {
        logger.debug("getListBoxItemNameForRe : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("getListBoxTextForRE", integer + "", i + "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            System.out.println("getListBoxItemNameForRe : " + e + "starts");
        }
        logger.debug("getListBoxItemNameForRe : " + logUserId + "ends");
        return mores;
    }

    /**
     * this method get list item name for RE by item value
     * @param integer
     * @param i
     * @return List<SPCommonSearchDataMore>
     */
    public void deleteInCaseOfService(int tenderId) {
        logger.debug("deleteInCaseOfService : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = dataMore.executeProcedure("FlushIfErrorInService", tenderId + "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        } catch (Exception e) {
            System.out.println("deleteInCaseOfService : " + e + "starts");
        }
        logger.debug("deleteInCaseOfService : " + logUserId + "ends");

    }

    /**
     * this method get srv BOQ type
     * @param srvFormMapId
     * @return string
     */
    public String getsrvBoqType(int srvFormMapId) {
        logger.debug("getsrvBoqType : " + logUserId + "starts");
        String str = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select sbm.srvBoqType from TblCmsSrvBoqMaster sbm, ");
            sb.append("TblCmsSrvFormMap sfm where sbm.srvBoqId = sfm.srvBoqId and sfm.srvFormMapId = " + srvFormMapId + "");
            List<Object> list = hibernateQueryDao.getSingleColQuery(sb.toString());
            if (!list.isEmpty() && list != null) {
                str = list.get(0).toString();
            }
        } catch (Exception e) {
            logger.error("getsrvBoqType : " + e + "starts");
        }
        logger.debug("getsrvBoqType : " + logUserId + "ends");
        return str;
    }

    /**
     * this method get work schedule doc details
     * @param keyId
     * @param variOrdId
     * @return List<TblCmsSrvWrkSchDoc>
     */
    public List<TblCmsSrvWrkSchDoc> getWorkScheduleDocsDetails(int keyId, int variOrdId) {
        logger.debug("getWorkScheduleDocsDetails : " + logUserId + "starts");
        List<TblCmsSrvWrkSchDoc> list = null;
        try {
            list = tblCmsSrvWrkSchDocDao.findTblCmsSrvWrkSchDoc("srvFormMapId", Operation_enum.EQ, keyId, "variOrdId", Operation_enum.EQ, variOrdId);
        } catch (Exception e) {
            logger.error("getWorkScheduleDocsDetails : " + e);
        }
        logger.debug("getWorkScheduleDocsDetails : " + logUserId + "ends");
        return list;
    }

    /**
     * this method delete work schedule document details
     * @param wsDocId
     * @param variOrdId
     * @return boolean 
     */
    public boolean deleteWorkScheduleDocsDetails(int wsDocId, int variOrdId) {
        logger.debug("deleteWorkScheduleDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        int i = 0;
        try {
            i = hibernateQueryDao.updateDeleteNewQuery("delete from TblCmsSrvWrkSchDoc where wsDocId=" + wsDocId + " and variOrdId = " + variOrdId + "");
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("deleteWorkScheduleDocsDetails : " + e);
        }
        logger.debug("deleteWorkScheduleDocsDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method adds work schedule doc details
     * @param tblCmsSrvWrkSchDoc
     * @return boolean
     */
    public boolean addWorkScheduleDocsDetails(TblCmsSrvWrkSchDoc tblCmsSrvWrkSchDoc) {
        logger.debug("addWorkScheduleDocsDetails : " + logUserId + "starts");
        boolean flag = false;
        try {
            tblCmsSrvWrkSchDocDao.addTblCmsSrvWrkSchDoc(tblCmsSrvWrkSchDoc);
            flag = true;
        } catch (Exception e) {
            logger.error("addWorkScheduleDocsDetails : " + e);
        }
        logger.debug("addWorkScheduleDocsDetails : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method checks that item added in variation order or not
     * @param varId
     * @return boolean 
     */
    public boolean checkIfItemAddedInVariOrder(int varId) {
        logger.debug("checkIfItemAddedInVariOrder : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        boolean flag = false;
        try {
            mores = dataMore.executeProcedure("CheckForVariOrderSerCase", varId + "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            if (!mores.isEmpty()) {
                if (Integer.parseInt(mores.get(0).getFieldName1()) > 0) {
                    flag = true;
                } else {
                    flag = false;
                }
            }
        } catch (Exception e) {
            logger.error("checkIfItemAddedInVariOrder : " + e + "starts");
        }
        logger.debug("checkIfItemAddedInVariOrder : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method gets the wpID by passing tenderId and boqID
     * @param tenderId
     * @param SrvBoqId
     * @return List<Object[]>
     */
    public List<Object[]> getWpID(int tenderId, int SrvBoqId) {
        logger.debug("getWpID : " + logUserId + "starts");
        String str = "";
        List<Object[]> list = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select b.tblCmsWpMaster.wpId,b.wpTenderFormId from TblCmsWptenderBoqmap b,");
            sb.append(" TblCmsSrvFormMap a where b.wpTenderFormId = a.formId and a.tenderId=" + tenderId + " and a.srvBoqId=" + SrvBoqId + "");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        } catch (Exception e) {
            logger.error("getWpID : " + e + "starts");
        }
        logger.debug("getWpID : " + logUserId + "ends");
        return list;
    }

    public List<Object> getContractValue(int tenderId) {
        logger.debug("getContractValue : " + logUserId + "starts");
        List<Object> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select ns.contractAmt from TblNoaIssueDetails ns where ns.tblTenderMaster.tenderId =").append(tenderId);
        try {
            list = hibernateQueryDao.getSingleColQuery(br.toString());
        } catch (Exception e) {
            logger.error("getContractValue : " + e);
        }
        logger.debug("getContractValue : " + logUserId + "ends");
        return list;
    }

    public boolean getAcceptedFlaginLumpsum(int tillPrid, int tenderId) {
        logger.debug("getAcceptedFlaginLumpsum : " + logUserId + "starts");
        boolean flag = true;
        List<Object> list = null;
        try {
            String quSUpd = "select ind.invoiceId from TblCmsInvoiceMaster ind where ind.tillPrid='" + tillPrid + "' and ind.tenderId = " + tenderId + " and ind.isAdvInv='No' and ind.invStatus!='rejected'";
            list = hibernateQueryDao.singleColQuery(quSUpd);
            if (list.get(0) != null) {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("getAcceptedFlaginLumpsum : " + e);
        }
        logger.debug("getAcceptedFlaginLumpsum : " + logUserId + "ends");
        return flag;
    }

    public List<Object[]> getInvoiceDatainlumpsum(int SrvPsid, int tenderId) {
        logger.debug("getInvoiceDatainlumpsum : " + logUserId + "starts");
        List<Object[]> list = null;
        StringBuilder br = new StringBuilder();
        br.append("select tcim.invoiceId,tcim.totalInvAmt,tcim.invStatus,tcim.isAdvInv,tcim.invoiceNo,tcim.remarks,tcim.conRemarks ");
        br.append("from TblCmsInvoiceMaster tcim where tcim.tillPrid=" + SrvPsid + " and tcim.tenderId=" + tenderId + " and tcim.invStatus!='rejected'");
        try {
            list = hibernateQueryDao.createNewQuery(br.toString());
        } catch (Exception e) {
            logger.error("getInvoiceDatainlumpsum : " + e);
        }
        logger.debug("getInvoiceDatainlumpsum : " + logUserId + "ends");
        return list;
    }
}
