/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCascadeLinkDetails;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public interface CascadeDetailsService {
/**
     * Get Cascade Details By Query
     * @param userTypeId
 *   * @param param1
     * @param param2
     * @return
     * @throws Exception
     */
    public List<TblCascadeLinkDetails> getCascadeDetailsList(String userTypeId, String status, String pageName, String parentLink) throws Exception;
}
