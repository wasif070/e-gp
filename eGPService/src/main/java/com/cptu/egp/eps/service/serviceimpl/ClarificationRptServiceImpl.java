/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import com.cptu.egp.eps.service.serviceinterface.ClarificationRptService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ClarificationRptServiceImpl implements ClarificationRptService{

    static final Logger logger = Logger.getLogger(ClarificationRptServiceImpl.class);
    TblEvalReportMasterDao tblEvalReportMasterDao;
    private String logUserId="0";


    public TblEvalReportMasterDao getTblEvalReportMasterDao() {
        return tblEvalReportMasterDao;
    }

    public void setTblEvalReportMasterDao(TblEvalReportMasterDao tblEvalReportMasterDao) {
        this.tblEvalReportMasterDao = tblEvalReportMasterDao;
    }

    @Override
    public String forUpdate(int tenderId) {
        logger.debug("forUpdate : "+logUserId+" Starts");
        long result = 0;
        String data=null;
        try {
            result = tblEvalReportMasterDao.countForQuery("TblEvalReportMaster", "tenderId=" + tenderId);
            if(result>0){
                data =  "Update";
            }else{
                data = "Create";
            }

        } catch (Exception ex) {
            logger.error("forUpdate : "+logUserId+" "+ex);
            data = "Error";
        }
        logger.debug("forUpdate : "+logUserId+" Ends");
        return data;
    }

    @Override
    public List<TblEvalReportMaster> getRptName(int tenderId) {
        logger.debug("getRptName : "+logUserId+" Starts");
        List<TblEvalReportMaster> result = null;
        try {
            result = tblEvalReportMasterDao.findTblEvalReportMaster("tblTenderMaster.tenderId", Operation_enum.EQ, tenderId);            
        } catch (Exception ex) {
            logger.error("getRptName : "+logUserId+" "+ex);
        }
        logger.debug("getRptName : "+logUserId+" Ends");
            return result;
            }
    
    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
        }

}
