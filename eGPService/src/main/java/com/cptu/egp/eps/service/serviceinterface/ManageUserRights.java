/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblMenuMaster;
import com.cptu.egp.eps.model.table.TblRoleMaster;
import com.cptu.egp.eps.model.table.TblUserMenuRights;
import com.cptu.egp.eps.model.table.TblRoleMenuRights;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dipal.shah
 */
public interface ManageUserRights {

    /**
     * Method to get sub Menu details on the bases of given parent menu id.
     * @param menuId
     * @return Sub menu details as List of TblMenuMaster objects.
     */
    public List<TblMenuMaster> getMenuList(int menuId);

    public List getUserMenuRightsList(int userId);

    public List<Object[]> getUserDetails(int userId);

    /**
    * Method to assign rights to user on given menu list.
    * @param lstUserMenuRigths list of TblUserMenuRights object
    * @param  uId userId to whom rights is going to assign.
    * @return 0 if operation fails else return non zero integer value.
    */
   public int assignRights(List <TblUserMenuRights> lstUserMenuRigths,int uId);

    /**
    * Method to assign rights to role on given menu list.
    * @param lstUserMenuRigths list of TblUserMenuRights object
    * @param  uId userId to whom rights is going to assign.
    * @return 0 if operation fails else return non zero integer value.
    */
   public int assignRoleRights(List <TblRoleMenuRights> lstRoleMenuRigths,int roleId);

   /**
    * Method to get menuId list for which rights given to role Id.
    * @param roleId
    * @return menuId list
    */
   public ArrayList getRoleMenuRightsList(int roleId);

   /**
    * Method to get Role Name.
    * @param roleId
    * @return RoleName
    */
   public String getRoleDetails(int roleId);

   /**
    * Method to get Role Id on the basis of given roleName.
    * @param roleName
    * @return roleId
    */
   public int getRoleDetails(String roleName);

   /**
    * Method use for insert role details
    * @param tblRoleMaster
    * @return roleId
    */
   public int addRoles(TblRoleMaster tblRoleMaster);

   /**
    * Method use for update role details
    * @param tblRoleMaster
    * @return roleId
    */
   public int updateRoles(TblRoleMaster tblRoleMaster);

   /**
    * Method to check Role Name already exist.
    * @param Role Name
    * @return boolean value true if role name already exist else return false.
    */
   public boolean getRoleDetails(String roleName,int... roleId);

   /**
    * Method use for get role details.
    * @return List of TblRoleMaster Objects
    */
   public List<TblRoleMaster> getAllActiveRoles();


   /**
    * Method use for delete User Menu rights if admin assign rolebased rights instead of individual.
    * @param userId
    */
   public void deleteUserMenuRights(int userId);

   /**
   * Method use for get o&m user details in case of Edit /View Operation.
   * @param userId
   * @return list of Object array containing o&m user details.
   */
   public List<Object[]> getOandMUserDetails(int userId);


    /**
    * Method use for get role details.
    * @return List of TblRoleMaster Objects
    */
   public List<TblRoleMaster> getAllActiveRoles(int firstResult,int maxResult);

}
