/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidDeclarationDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBidDeclaration;
import com.cptu.egp.eps.service.serviceinterface.BidDeclarationService;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public class BidDeclartionServiceImpl implements BidDeclarationService {

    private TblBidDeclarationDao tblBidDeclarationDao;
    final Logger logger = Logger.getLogger(BidDeclartionServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    

  

    

    @Override
    public String addBidDeclaration(TblBidDeclaration tblBidDeclarationList) {
        logger.debug("add TblBidDeclaration : " + logUserId + loggerStart);
        String action="";
        String msg = "";
        try {         
         tblBidDeclarationDao.addTblBidDeclaration(tblBidDeclarationList);
         msg="Success";
        } catch (Exception e) {
             action="Add TblBidDeclaration : "+e;
            logger.error("add TblBidDeclaration : " + logUserId + e);
    }
    finally{        
        action=null;
        }
        logger.debug("add TblBidDeclaration : " + logUserId + loggerEnd);
     return msg;
    }

    
    /**
     * @return the tblBidDeclarationDao
     */
    public TblBidDeclarationDao getTblBidDeclarationDao() {
        return tblBidDeclarationDao;
    }

    /**
     * @param tblBidDeclarationDao the tblBidDeclarationDao to set
     */
    public void setTblBidDeclarationDao(TblBidDeclarationDao tblBidDeclarationDao) {
        this.tblBidDeclarationDao = tblBidDeclarationDao;
    }

    
}
