/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblHolidayMasterImpl;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblHolidayMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
public class HolidayMasterServiceImpl {

    private static final Logger LOGGER = Logger.getLogger(HolidayMasterServiceImpl.class);
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private TblHolidayMasterImpl tblHolidayMasterDao;
    MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * @return the tblHolidayMasterDao
     */
    public TblHolidayMasterImpl getTblHolidayMasterDao() {
        return tblHolidayMasterDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * @param tblHolidayMasterDao the tblHolidayMasterDao to set
     */
    public void setTblHolidayMasterDao(TblHolidayMasterImpl tblHolidayMasterDao) {
        this.tblHolidayMasterDao = tblHolidayMasterDao;
    }

    /**
     * Insert Holidays
     * @param tblHolidayMaster to be inserted
     */
    public void addTblHolidayMaster(TblHolidayMaster tblHolidayMaster) {
        LOGGER.debug("addTblHolidayMaster : " + logUserId + " Starts");
        String action = null;
        try {
            action = "Configure Holiday";
            TblHolidayMaster temp;
            try{
                temp = tblHolidayMasterDao.findTblHolidayMaster("holidayDate", Operation_enum.EQ, tblHolidayMaster.getHolidayDate()).get(0);
            }
            catch(IndexOutOfBoundsException ex){
                temp =null;
            }
            if (temp != null) {
                temp.setDescription(tblHolidayMaster.getDescription());
                tblHolidayMasterDao.updateTblHolidayMaster(temp);
            }
            else tblHolidayMasterDao.addTblHolidayMaster(tblHolidayMaster);
        } catch (Exception ex) {
            LOGGER.error("addTblHolidayMaster : " + logUserId + " : " + ex.toString());
            action = "Error in Configure Holiday : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        
        LOGGER.debug("addTblHolidayMaster : " + logUserId + " Ends");
    }

    /**
     *Get all Holidays
     * @return List of TblHolidayMaster
     */
    public List<TblHolidayMaster> getHolidayMasterData() {
        LOGGER.debug("getHolidayMasterData : " + logUserId + " Starts");
        List<TblHolidayMaster> tblHolidayMaster = null;
        try {
            tblHolidayMaster = tblHolidayMasterDao.getAllTblHolidayMaster();
        } catch (Exception ex) {
            LOGGER.error("getHolidayMasterData : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getHolidayMasterData : " + logUserId + " Ends");
        return tblHolidayMaster;
    }

    /**
     *Delete Holiday
     * @param tblHolidayMaster to be deleted
     */
    public void deleteHolidayMaster(TblHolidayMaster tblHolidayMaster) {
        LOGGER.debug("deleteHolidayMaster : " + logUserId + " Starts");
        String action = null;
        try {
            tblHolidayMasterDao.deleteTblHolidayMaster(tblHolidayMaster);
            action = "Remove Holiday";
        } catch (Exception ex) {
            LOGGER.error("deleteHolidayMaster : " + logUserId + " : " + ex.toString());
            action = "Error in Remove Holiday : "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("deleteHolidayMaster : " + logUserId + " Ends");
    }

    /**
     *Counts Holidays
     * @return count
     */
    public long countHolidayMasterData() {
        LOGGER.debug("countHolidayMasterData : " + logUserId + " Starts");
        long l = 0;
        try {
            l = tblHolidayMasterDao.getEntityCount();
        } catch (Exception ex) {
            LOGGER.error("countHolidayMasterData : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("countHolidayMasterData : " + logUserId + " Ends");
        return l;
    }

    /**
     * List of TblHolidayMaster on criteria
     * @param offset
     * @param recordoffset
     * @return List of TblHolidayMaster
     */
    public List<TblHolidayMaster> getHolidayConfigurationMasterData(int offset, int recordoffset) {
        LOGGER.debug("getHolidayConfigurationMasterData : " + logUserId + " Starts");
        List<TblHolidayMaster> tblHolidayMaster = null;
        try {
            tblHolidayMaster = tblHolidayMasterDao.findByCountTblHolidayMaster(offset, recordoffset, "holidayDate", Operation_enum.ORDERBY, Operation_enum.DESC);
        } catch (Exception ex) {
            LOGGER.error("getHolidayConfigurationMasterData : " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("getHolidayConfigurationMasterData : " + logUserId + " Ends");
        return tblHolidayMaster;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
