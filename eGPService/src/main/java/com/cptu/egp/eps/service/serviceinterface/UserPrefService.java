/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UserPrefService {
/**
 * this method save the preference of the user to database
 * @param tblUserprefrence - this is the pojo objects
 * @return boolean, true-if saved, false - if not saved
 */
public boolean addemailsmsAlert(TblUserPrefrence tblUserprefrence);
/**
 * this method for getting the preference of the particular user from database by passing the user id
 * @param id - this is the user id  for particular user
 * @return list of data
 */
public List<TblUserPrefrence> getemailsmsAlert(int id);
/**
 * this method update the preference of the user to database
 * @param tbluserpreference - this is the pojo objects
 * @return boolean, true-if updated, false - if not updated
 */
public boolean updateemailsmsAlert(TblUserPrefrence tbluserpreference);

public boolean getpreferenceStatus(int id, String what);
/**
 * this method for getting the preference of the particular user from database by passing the user id and ('sms' or 'email')
 * @param id - this is the user id  for particular user
 * @param what - for what preference user want form database 'sms' or 'email'
 * @return boolean, if true - yes('sms' or 'email') , if false -  No('sms' or 'email')
 */
public void setLogUserId(String logUserId);
/**
 * this method for getting the preference of the particular user by passing the email id
 * @param email id - this is the email id of the particular user
 * @return list of data
 */
public List<Object[]> getEmailSmsStatus(String email);

public void setAuditTrail(AuditTrail auditTrail);
public List<Object[]> findEmail(String userTypeId);
public List<Object[]> findSms(String userTypeId);
}
