/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblAskProcurement;
import com.cptu.egp.eps.model.table.TblQuestionCategory;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Naresh.Akurathi
 */
public interface AskProcurementService {

    /**
     * Fetching all information related TblQuestionCategory
     * @return List of TblQuestionCategory
     */
    public List<TblQuestionCategory> getAllQusetions();

    /**
     * Fetching all information for particular user for ask procurement
     * @param id - postedBy of tbl_AskProcurements
     * @return List of TblAskProcurement
     */
    public List<TblAskProcurement> getAskProcurementDetails(int id);

    /**
     * Fetching all information
     * @param id primary key of Tbl_AskProcurement (procQueId)
     * @return List of TblAskProcurement
     */
    public List<TblAskProcurement> getAskProcurement(int id);

     /**
     * Insert values for asking question to Procurement Expert
     * @param tblAskProcurement
     * @return message about values inserted or not
     */
    public String addTblAskProcurement(TblAskProcurement tblAskProcurement);

    /**
     * user for logger
     * @param logUserId
     */
    public void setLogUserId(String logUserId);
    
    /**
     * user for AuditTrail
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
