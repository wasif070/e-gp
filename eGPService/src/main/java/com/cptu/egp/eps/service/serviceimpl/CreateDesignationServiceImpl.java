/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblDesignationMasterDao;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateDesignationService;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class CreateDesignationServiceImpl implements CreateDesignationService{
    final Logger logger = Logger.getLogger(CreateDesignationServiceImpl.class);

    private String logUserId="0";
    
    TblDesignationMasterDao  tblDesignationMasterDao;

    public TblDesignationMasterDao getTblDesignationMasterDao() {
        return tblDesignationMasterDao;
    }

    public void setTblDesignationMasterDao(TblDesignationMasterDao tblDesignationMasterDao) {
        this.tblDesignationMasterDao = tblDesignationMasterDao;
    }


    @Override
    public void createDesignation(TblDesignationMaster designationMaster) {
        try{
            logger.debug("createDesignation : "+logUserId+" Starts");
        tblDesignationMasterDao.addDesignation(designationMaster);
        }catch(Exception ex){
            logger.error("createDesignation : "+logUserId+" :"+ex);
    }
        logger.debug("createDesignation : "+logUserId+" Ends");
    }

    @Override
    public void setUserId(String logUserId) {
            this.logUserId = logUserId;
    }

}
