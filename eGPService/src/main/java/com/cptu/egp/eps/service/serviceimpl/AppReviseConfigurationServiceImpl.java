/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblAppreviseConfigurationDao;
import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
import com.cptu.egp.eps.service.serviceinterface.AppReviseConfigurationService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public class AppReviseConfigurationServiceImpl implements AppReviseConfigurationService {

    private TblAppreviseConfigurationDao tblAppreviseConfigurationDao;
    final Logger logger = Logger.getLogger(TenderFrameWorkServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;
    private HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblAppreviseConfigurationDao getTblAppreviseConfigurationDao() {
        return tblAppreviseConfigurationDao;
    }

    public void setTblAppreviseConfigurationDao(TblAppreviseConfigurationDao TblAppreviseConfigurationDao) {
        this.tblAppreviseConfigurationDao = TblAppreviseConfigurationDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public String addTblAppreviseConfiguration(TblAppreviseConfiguration TblAppreviseConfiguration,int userId) {
        logger.debug("add TblAppreviseConfiguration : " + logUserId + loggerStart);
        String action="";
        String msg = "";
        try {
         action="Add TblAppreviseConfiguration";
         tblAppreviseConfigurationDao.addTblAppreviseConfiguration(TblAppreviseConfiguration);
         msg = "Value Added";
        } catch (Exception e) {
             action="Add TblAppreviseConfiguration : "+e;
            logger.error("add TblAppreviseConfiguration : " + logUserId + e);
    }
    finally{
            getMakeAuditTrailService().generateAudit(auditTrail, userId, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("add TblAppreviseConfiguration : " + logUserId + loggerEnd);
        return msg;
    }
    
    @Override
    public String updateTblAppreviseConfiguration(TblAppreviseConfiguration TblAppreviseConfiguration,int userID) {
        logger.debug("update TblAppreviseConfiguration : " + logUserId + loggerStart);
        String action="";
        String msg = "";
        int update = 0;
        try {
         action="update TblAppreviseConfiguration";
         java.sql.Date StartsqlDate = new java.sql.Date(TblAppreviseConfiguration.getStartDate().getTime());
         java.sql.Date EndsqlDate = new java.sql.Date(TblAppreviseConfiguration.getEndDate().getTime());
         java.sql.Date LastsqlDate = new java.sql.Date(TblAppreviseConfiguration.getLastUpdatedDate().getTime());
         update = hibernateQueryDao.updateDeleteNewQuery("update TblAppreviseConfiguration set startDate='" + StartsqlDate + "',endDate='" + EndsqlDate + "',maxReviseCount=" + TblAppreviseConfiguration.getMaxReviseCount() + ",userId=" + TblAppreviseConfiguration.getUserId() + ",lastUpdatedDate='" + LastsqlDate + "' where financialYear='" + TblAppreviseConfiguration.getFinancialYear() + "'");
         msg = "Value Updated";
        } catch (Exception e) {
             action="update TblAppreviseConfiguration : "+e;
            logger.error("update TblAppreviseConfiguration : " + logUserId + e);
    }
    finally{
            getMakeAuditTrailService().generateAudit(auditTrail, userID, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("update TblAppreviseConfiguration : " + logUserId + loggerEnd);
        return msg;
    }
    
    @Override
    public boolean isexistConfiguration(String financialYear){
        logger.debug("isexistConfiguration : " + logUserId + loggerStart);
          List<TblAppreviseConfiguration> list = null;
          boolean exist = false;
          try {
              list = tblAppreviseConfigurationDao.findTblAppreviseConfiguration("financialYear", Operation_enum.EQ, financialYear);
              System.out.println("List Size------------>>>>>>"+list.size());
              if(list.size()>0)
              {
               System.out.println("(Exist List with Financial Year)------------>>>>>>"+list.size());
               exist = true;
              }
          } catch (Exception ex) {
              logger.error("isexistConfiguration : " + logUserId + " " + ex);
          }
        logger.debug("isexistConfiguration : " + logUserId + loggerEnd);
      return exist;
    }
    @Override
    public List<TblAppreviseConfiguration> getAllConfiguration(){
        logger.debug("isexistConfiguration : " + logUserId + loggerStart);
        logger.debug("getConfigMasterDetails : " + logUserId + loggerEnd);
        return tblAppreviseConfigurationDao.getAllTblAppreviseConfiguration();
    }
    @Override
    public List<TblAppreviseConfiguration> getAppConfigurationDetails(String appConfgId){
        logger.debug("getAppConfigurationDetails : " + logUserId + loggerStart);
          List<TblAppreviseConfiguration> list = null;          
          try {
              list = tblAppreviseConfigurationDao.findTblAppreviseConfiguration("financialYear", Operation_enum.EQ, appConfgId);
          } catch (Exception ex) {
              logger.error("getAppConfigurationDetails : " + logUserId + " " + ex);
          }
        logger.debug("getAppConfigurationDetails : " + logUserId + loggerEnd);
      return list;
    }
}
