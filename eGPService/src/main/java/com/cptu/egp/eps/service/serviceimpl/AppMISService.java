/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author taher
 */
public class AppMISService {

    final Logger logger = Logger.getLogger(AppMISService.class);
    private String logUserId ="0";

    private SpgetCommonSearchDataMore spgetCommonSearchDataMore;
    private HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SpgetCommonSearchDataMore getSpgetCommonSearchDataMore() {
        return spgetCommonSearchDataMore;
    }

    public void setSpgetCommonSearchDataMore(SpgetCommonSearchDataMore spgetCommonSearchDataMore) {
        this.spgetCommonSearchDataMore = spgetCommonSearchDataMore;
    }

    /**
     * Retrieves App MIS report for Egp-Admin
     * @param subFun It's a subfunction inside a function
     * @param depId departmentId from tbl_DepartmentMaster
     * @param offId officeId from tbl_OfficeMaster
     * @returnList of SPCommonSearchDataMore
     */
    public List<SPCommonSearchDataMore> getAppMIS(String subFun,String depId,String offId){
        return spgetCommonSearchDataMore.executeProcedure("GetAppMIS", subFun,offId, depId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    /**
     *Get List of OfficeMaster
     * @param userId session userId
     * @param userTypeId session userTypeId
     * @return
     */
    public List<Object[]> getOfficeList(String userId,String userTypeId){
         logger.debug("getOfficeList : "+logUserId+" Starts");
        List<Object []> officeList = null;
        try{
            if(userTypeId.equals("4")){
                officeList = hibernateQueryDao.createNewQuery("select tom.officeId, tom.officeName from TblOfficeMaster as tom where tom.officeId in (select toa.tblOfficeMaster.officeId  from TblOfficeAdmin as toa where toa.tblLoginMaster.userId="+userId+")");
            }else if(userTypeId.equals("5")){
                officeList = hibernateQueryDao.createNewQuery("select tom.officeId,tom.officeName from TblOfficeMaster tom where tom.departmentId in (select tdm.departmentId from TblDepartmentMaster tdm where tdm.approvingAuthorityId="+userId+")");
            }
            else if(userTypeId.equals("3")){
                officeList = hibernateQueryDao.createNewQuery("select officeId,officeName from TblOfficeMaster where departmentId = (select om.departmentId from TblOfficeMaster om where om.officeId = (select eo.tblOfficeMaster.officeId from TblEmployeeOffices eo where eo.tblEmployeeMaster.employeeId = (select em.employeeId from TblEmployeeMaster em where em.tblLoginMaster.userId = "+userId+")))");
            }
        }catch (Exception ex){
            logger.error("getOfficeList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getOfficeList : "+logUserId+" Ends");
        return officeList;
    }

    /**
     *Get Name of the Department for userId
     * @param userId
     * @return name of the Department
     */
    public String getOrgAdminOrgName(String userId){
        String data="";
        List<Object> list = hibernateQueryDao.getSingleColQuery("select tdm.departmentName from TblDepartmentMaster tdm where tdm.approvingAuthorityId="+userId);
        if(!list.isEmpty()){
            data = list.get(0).toString();
        }
        return data;
    }

    public Object[] getOrgAdminOrgName(int userId){
        Object[] data = null;
        List<Object[]> list = hibernateQueryDao.createNewQuery("select tdm.departmentId,tdm.departmentName from TblDepartmentMaster tdm where tdm.approvingAuthorityId="+userId);
        if(!list.isEmpty()){
            data = list.get(0);
        }
        return data;
    }
    public Object[] getOrgAdminOrgNameByUserId(int userId){
        Object[] data = null;
        List<Object[]> list = hibernateQueryDao.createNewQuery("select tdm.departmentId,tdm.departmentName from TblDepartmentMaster tdm where tdm.departmentId = (select om.departmentId from TblOfficeMaster om where om.officeId = (select eo.tblOfficeMaster.officeId from TblEmployeeOffices eo where eo.tblEmployeeMaster.employeeId = (select em.employeeId from TblEmployeeMaster em where em.tblLoginMaster.userId = "+userId+")))");
        if(!list.isEmpty()){
            data = list.get(0);
        }
        return data;
    }
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
}
