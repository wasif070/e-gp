/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalBidderStatusDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.EvalClariPostBidderService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author Administrator
 */
public class EvalClariPostBidderServiceImpl implements EvalClariPostBidderService{

    TblEvalBidderStatusDao tblEvalBidderStatusDao;
    
    TblTenderDetailsDao tblTenderDetailsDao;

    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    String logUserId = "0";

    final Logger logger = Logger.getLogger(EvalClariPostBidderServiceImpl.class);

    public TblEvalBidderStatusDao getTblEvalBidderStatusDao() {
        return tblEvalBidderStatusDao;
    }

    public void setTblEvalBidderStatusDao(TblEvalBidderStatusDao tblEvalBidderStatusDao) {
        this.tblEvalBidderStatusDao = tblEvalBidderStatusDao;
    }
    
    
    @Override

    public boolean isChecked(String tenderId, String userId,String lotId) {
        logger.debug("isChecked : "+logUserId+" Starts");
         long i=0;
         boolean flag = false;
        try {
            i =tblEvalBidderStatusDao.countForQuery("TblEvalBidderStatus", "tenderId="+tenderId+" and userId="+userId+"and pkgLotId="+lotId);
            if(i>0){
                flag = true;
        }
        }
        catch (Exception ex) {
            logger.error("isChecked : "+logUserId+" :"+ex);
        }
         logger.debug("isChecked : "+logUserId+" Ends");
         return flag;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
        }

    @Override
    public  String getPeOffice(int tenderId){
        logger.debug("getPeOffice : "+logUserId+" Starts");
        String peOffice = "";
        try {
               List<TblTenderDetails> tblTenderDetails = new ArrayList<TblTenderDetails>();
               tblTenderDetails = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId)); 
               if(tblTenderDetails!=null && !tblTenderDetails.isEmpty()){
               peOffice = tblTenderDetails.get(0).getPeOfficeName();
               }
               tblTenderDetails.clear();
               tblTenderDetails = null;
        } catch (Exception e) {
            logger.error("getPeOffice : "+logUserId+" :"+e);
        }
        logger.debug("getPeOffice : "+logUserId+" Ends");
        return peOffice;
    }
}
