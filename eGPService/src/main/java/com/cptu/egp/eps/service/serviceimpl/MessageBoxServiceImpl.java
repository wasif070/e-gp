/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblMessageFolderDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblMessageFolder;
import com.cptu.egp.eps.service.serviceinterface.MessageBoxService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Anil.Yada
 * Added By to commit
 */
public class MessageBoxServiceImpl implements MessageBoxService{

    final Logger logger = Logger.getLogger(MessageBoxServiceImpl.class);

    TblMessageFolderDao tblMessageFolderDao;
    private String logUserId = "0";
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblMessageFolderDao getTblMessageFolderDao() {
        return tblMessageFolderDao;
    }

    
    public void setTblMessageFolderDao(TblMessageFolderDao tblMessageFolderDao) {
        this.tblMessageFolderDao = tblMessageFolderDao;
    }

    /**
     * 
     * Method for Adding New Folder to MessageBox.
     */
    @Override
    public void addFolder(TblMessageFolder tblMessageFolder, int userId) {
        
        logger.debug("addFolder : "+logUserId+" Starts");
        String action="";
        try{
            tblMessageFolder.setTblLoginMaster(new TblLoginMaster(userId));//setting Value to ModelBean.

            tblMessageFolderDao.addTblMessageFolder(tblMessageFolder);//Callingthe Dao Function.
            action = "Add Folder";
        } catch (Exception e) {
            action = "Add Folder :"+e;
            logger.error("addFolder : "+logUserId+" : "+e);
        }
        finally{
            makeAuditTrailService.generateAudit(auditTrail, userId, "useId", EgpModule.Message_Box.getName(), action, "");
            action=null;
        }
        logger.debug("addFolder : "+logUserId+" Ends");
    }

    /**
     * 
     * Getting Folder Names from Database and verifying with given FolderName.
     * @param folderName
     * @param userid
     * @return
     */
    @Override
    public List<TblMessageFolder> getMessageFolder(String folderName, int userid) {
        logger.debug("getMessageFolder : "+logUserId+" Starts");
         List<TblMessageFolder> list =null;         
        try {
            Object[] values = {"folderName", Operation_enum.EQ, folderName, "tblLoginMaster", Operation_enum.EQ,
                new TblLoginMaster(userid)};
            list = tblMessageFolderDao.findTblMessageFolder(values);//Calling Dao Class method.            
        } catch (Exception e) {             
            logger.error("getMessageFolder : "+logUserId+" : "+e);
        }
        logger.debug("getMessageFolder : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        }
}
