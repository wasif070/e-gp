/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsWcCertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface CmsWcCertDocService {

    /**
     * set userId at service layer
     * @param logUserId
     */
    public void setLogUserId(String logUserId);

    /**
     * adds work completion certificate details to the database
     * @param tblCmsWcCertDoc
     * @return integer, return 0 - if not added successfully otherwise else count
     */
    public int insertCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    /**
     * updates work completion certificate details to the database
     * @param tblCmsWcCertDoc
     * @return boolean, return true - if updated successfully otherwise false
     */
    public boolean updateCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    /**
     * delete work completion Documents details to the database
     * @param tblCmsWcCertDoc
     * @return boolean, return true - if deleted successfully otherwise false
     */
    public boolean deleteCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    /**
     * gives work completion documents details from the database
     * @return list of table data
     */
    public List<TblCmsWcCertDoc> getAllCmsWcCertDoc();

    /**
     * gives document details data row count
     * @return long, return 0 - if data is not exist,otherwise else count
     */
    public long getCmsWcCertDocCount();

    /**
     * gives work completion certificate details data
     * @param id
     * @return table object
     */
    public TblCmsWcCertDoc getCmsWcCertDoc(int id);

    /**
     * gives work completion documents details data
     * @param wcCertificateId
     * @return list of table data object
     */
    public List<TblCmsWcCertDoc> getAllCmsWcCertDocForWccId(int wcCertificateId);
}
