/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.service.serviceinterface.DepartmentCreationService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class DepartmentCreationServiceImpl implements DepartmentCreationService {

    TblDepartmentMasterDao tblDepartmentMasterDao;
    HibernateQueryDao hibernateQueryDao;
    private String logUserId = "0";
    private final String starts = " Starts";
    private final String ends = " Ends";
    static final Logger LOGGER = Logger.getLogger(DepartmentCreationServiceImpl.class);
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    @Override
    public boolean addDepartmentCreation(TblDepartmentMaster tblDepartmentMaster) {
        LOGGER.debug("addDepartmentCreation : " + logUserId + starts);
        boolean flag = false;
        String action = null;
        String auditObjType=null;
        try
        {
            tblDepartmentMasterDao.addDepartmentMaster(tblDepartmentMaster);
            flag = true;
            if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Division"))
            {
                action = "Create Division";
                auditObjType="divisionId";
            }
            else if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Ministry"))
            {
                action = "Create Ministry";
                auditObjType="ministryId";
            }
            else if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Organization"))
            {
                action = "Create Organization";
                auditObjType="organizationId";
            }
            else if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("District"))
            {
                action = "Create District";
                auditObjType="DistrictId";
            }
            else
            {
                action = "Create Ministry";
                auditObjType="ministryId";
            }
        } catch (Exception ex) {
            LOGGER.error("addDepartmentCreation : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblDepartmentMaster.getDepartmentId(), auditObjType, EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("addDepartmentCreation : " + logUserId + ends);
        return flag;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public List<TblDepartmentMaster> getDeptTypeName(String deptType) {
        LOGGER.debug("getDeptTypeName : " + logUserId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentType", Operation_enum.LIKE, deptType, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            LOGGER.error("getDeptTypeName : " + logUserId + ex);
        }
        LOGGER.debug("getDeptTypeName : " + logUserId + ends);
        return deptList;
    }

    @Override
    public List<TblDepartmentMaster> getAllDepartmentMaster() {
        LOGGER.debug("getAllDepartmentMaster : " + logUserId + starts);
        List<TblDepartmentMaster> list = new ArrayList<TblDepartmentMaster>();
        try {
            list = tblDepartmentMasterDao.getAllTblDepartmentMaster();
        } catch (Exception ex) {
            LOGGER.error("getAllDepartmentMaster : " + logUserId + ex);
        }
        LOGGER.debug("getAllDepartmentMaster : " + logUserId + ends);
        return list;
    }

    @Override
    public List<TblDepartmentMaster> findDeptCreatMaster(int firstResult, int maxResult, Object... values) {
        LOGGER.debug("findDeptCreatMaster : " + logUserId + starts);
        List<TblDepartmentMaster> list = new ArrayList<TblDepartmentMaster>();
        try {
            list = tblDepartmentMasterDao.findByCountEntity(firstResult, maxResult, values);
        } catch (Exception ex) {
            LOGGER.error("findDeptCreatMaster : " + logUserId + ex);
        }
        LOGGER.debug("findDeptCreatMaster : " + logUserId + ends);
        return list;
    }

    @Override
    public long getAllCountOfDept(String deptType) throws Exception {
        //return tblDepartmentMasterDao.getTblDepartmentMasterCount();
        LOGGER.debug("getAllCountOfDept : " + logUserId + starts);
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblDepartmentMaster t", "t.departmentType like '" + deptType + "'");
        } catch (Exception ex) {
            LOGGER.error("getAllCountOfDept : " + logUserId + ex);
        }
        LOGGER.debug("getAllCountOfDept : " + logUserId + ends);
        return count;
    }

    @Override
    public long getSearchCountOfDept(String deptType, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getSearchCountOfDept : " + logUserId + starts);
        long count = 0;
        //return tblDepartmentMasterDao.getTblDepartmentMasterCount();
        try {
            if (searchOper.equalsIgnoreCase("EQ")) {
                count = hibernateQueryDao.countForNewQuery("TblDepartmentMaster t", "t.departmentType like '" + deptType + "' and " + searchField + " = '" + searchString + "'");
            } else if (searchOper.equalsIgnoreCase("CN")) {
                count = hibernateQueryDao.countForNewQuery("TblDepartmentMaster t", "t.departmentType like '" + deptType + "' and " + searchField + " LIKE '%" + searchString + "%'");
            }
        } catch (Exception ex) {
            LOGGER.error("getSearchCountOfDept : " + logUserId + ex);
        }
        LOGGER.debug("getSearchCountOfDept : " + logUserId + ends);
        return count;

    }

    @Override
    public List<TblDepartmentMaster> findDeptUserMaster(Object... values) {
        LOGGER.debug("findDeptUserMaster : " + logUserId + starts);
        List<TblDepartmentMaster> list = new ArrayList<TblDepartmentMaster>();
        try {
            list = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            LOGGER.error("findDeptUserMaster : " + logUserId + ex);
        }
        LOGGER.debug("findDeptUserMaster : " + logUserId + ends);
        return list;
    }

    @Override
    public boolean updateDepartmentCreation(TblDepartmentMaster tblDepartmentMaster) {
        boolean flag = false;
        LOGGER.debug("updateDepartmentCreation : " + logUserId + starts);
        String action = null;
        String auditObjType=null;
        try {
            tblDepartmentMasterDao.updateTblDepartmentMaster(tblDepartmentMaster);
            flag = true;
            if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Division"))
            {
                action = "Edit Division";
                auditObjType="divisionId";
            }
            else if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Ministry"))
            {
                action = "Edit Ministry";
                auditObjType="ministryId";
            }
            else if(tblDepartmentMaster.getDepartmentType()!= null && tblDepartmentMaster.getDepartmentType().equalsIgnoreCase("Organization"))
            {
                action = "Edit Organization";
                auditObjType="organizationId";
            }
            else
            {
                action = "Edit Ministry";
                auditObjType="ministryId";
            }
        } catch (Exception ex) {
            LOGGER.error("updateDepartmentCreation : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        } 
        finally {
            makeAuditTrailService.generateAudit(auditTrail, tblDepartmentMaster.getDepartmentId(), auditObjType, EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateDepartmentCreation : " + logUserId + ends);
        return flag;
    }

    public List<TblDepartmentMaster> getChildDepartments(short id, boolean show) {
        LOGGER.debug("getChildDepartments : " + logUserId + starts);
        StringBuilder strQuery = new StringBuilder();
        strQuery.append(" select tdm.departmentId, tdm.departmentName, tdm.departmentType, tdm.parentDepartmentId from TblDepartmentMaster tdm where tdm.status = 'Approved' ");
        if (id != 0) {
            strQuery.append(" and tdm.parentDepartmentId = ").append(id);
        } else {
            if (show) {
                strQuery.append(" and tdm.departmentType = 'Ministry Root'");
            } else {
                strQuery.append(" and tdm.parentDepartmentId = 1");
            }
        }
        strQuery.append(" order by tdm.departmentType asc, tdm.departmentName asc");
        //strQuery.append(" and tdm.departmentId in (12, 13,14,15,16,17) ");
        List<TblDepartmentMaster> depts = new ArrayList<TblDepartmentMaster>();
        List<Object[]> objs = new ArrayList<Object[]>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                depts.add(new TblDepartmentMaster(Short.parseShort(i[0].toString()), (String) i[1], (String) i[2], Short.parseShort(i[3].toString())));
            }
        } catch (Exception ex) {
            LOGGER.error("getChildDepartments : " + logUserId + ex);
            depts = null;
        }
        LOGGER.debug("getChildDepartments : " + logUserId + ends);
        return depts;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    public List<TblDepartmentMaster> getDepartmentMasterByName(String ministryName,String deptType){
     LOGGER.debug("getDeptIdbyName : " + logUserId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentType", Operation_enum.LIKE, deptType, "departmentName",Operation_enum.EQ,ministryName, Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            LOGGER.error("getDeptIdbyName : " + logUserId + ex);
        }
        LOGGER.debug("getDeptIdbyName : " + logUserId + ends);
        return deptList;

    }

    public List<TblDepartmentMaster> getMinstriesByDeptyTypeAndDeptName(String deptType,String deptName){
        LOGGER.debug("getMinstriesByDeptyTypeAndDeptName : " + logUserId + starts);
        List<TblDepartmentMaster> deptList = new ArrayList<TblDepartmentMaster>();
        try {
            Object[] values = {"departmentName",Operation_enum.EQ,deptName,"departmentType", Operation_enum.LIKE, deptType, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC};
            deptList = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception ex) {
            LOGGER.error("getMinstriesByDeptyTypeAndDeptName : " + logUserId + ex);
        }
        LOGGER.debug("getMinstriesByDeptyTypeAndDeptName : " + logUserId + ends);
        return deptList;

     }
}
