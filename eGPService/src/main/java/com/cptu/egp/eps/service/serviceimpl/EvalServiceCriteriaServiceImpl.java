/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBidderRankDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalBidderMarksDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalBidderStatusDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRoundMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalSerFormDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalServiceCriteriaDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalServiceFormsDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalServiceWeightageDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalBidderMarks;
import com.cptu.egp.eps.model.table.TblEvalBidderStatus;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblEvalSerFormDetail;
import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderValAcceptance;
import com.cptu.egp.eps.model.table.TblTenderValidityExt;
import com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class EvalServiceCriteriaServiceImpl implements EvalServiceCriteriaService {

   
    private final Logger logger = Logger.getLogger(EvalServiceCriteriaServiceImpl.class);
    
    private TblEvalServiceFormsDao tblEvalServiceFormsDao;
    private TblEvalServiceCriteriaDao tblEvalServiceCriteriaDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblEvalSerFormDetailDao tblEvalSerFormDetailDao;
    private TblEvalBidderStatusDao tblEvalBidderStatusDao;
    private TblBidderRankDao tblBidderRankDao;
    private SpgetCommonSearchDataMore spgetCommonSearchDataMore;
    private TblEvalBidderMarksDao tblEvalBidderMarksDao;
    private TblEvalServiceWeightageDao tblEvalServiceWeightagedao;
    private TblEvalRoundMasterDao tblEvalRoundMasterDao;
    private String logUserId ="0";
    private MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;
    private EvaluationService evaluationService; //Change by dohatec for re-evaluation

    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    
    public TblEvalBidderMarksDao getTblEvalBidderMarksDao() {
        return tblEvalBidderMarksDao;
    }

    public void setTblEvalBidderMarksDao(TblEvalBidderMarksDao tblEvalBidderMarksDao) {
        this.tblEvalBidderMarksDao = tblEvalBidderMarksDao;
    }

    
    public SpgetCommonSearchDataMore getSpgetCommonSearchDataMore() {
        return spgetCommonSearchDataMore;
    }

    public void setSpgetCommonSearchDataMore(SpgetCommonSearchDataMore spgetCommonSearchDataMore) {
        this.spgetCommonSearchDataMore = spgetCommonSearchDataMore;
    }

    public TblBidderRankDao getTblBidderRankDao() {
        return tblBidderRankDao;
    }

    public void setTblBidderRankDao(TblBidderRankDao tblBidderRankDao) {
        this.tblBidderRankDao = tblBidderRankDao;
    }

    public TblEvalBidderStatusDao getTblEvalBidderStatusDao() {
        return tblEvalBidderStatusDao;
    }

    public void setTblEvalBidderStatusDao(TblEvalBidderStatusDao tblEvalBidderStatusDao) {
        this.tblEvalBidderStatusDao = tblEvalBidderStatusDao;
    }

    public TblEvalSerFormDetailDao getTblEvalSerFormDetailDao() {
        return tblEvalSerFormDetailDao;
    }

    public void setTblEvalSerFormDetailDao(TblEvalSerFormDetailDao tblEvalSerFormDetailDao) {
        this.tblEvalSerFormDetailDao = tblEvalSerFormDetailDao;
    }

    public TblEvalServiceFormsDao getTblEvalServiceFormsDao() {
        return tblEvalServiceFormsDao;
    }

    public void setTblEvalServiceFormsDao(TblEvalServiceFormsDao tblEvalServiceFormsDao) {
        this.tblEvalServiceFormsDao = tblEvalServiceFormsDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblEvalServiceCriteriaDao getTblEvalServiceCriteriaDao() {
        return tblEvalServiceCriteriaDao;
    }

    public void setTblEvalServiceCriteriaDao(TblEvalServiceCriteriaDao tblEvalServiceCriteriaDao) {
        this.tblEvalServiceCriteriaDao = tblEvalServiceCriteriaDao;
    }

    public TblEvalServiceWeightageDao getTblEvalServiceWeightagedao() {
        return tblEvalServiceWeightagedao;
    }

    public void setTblEvalServiceWeightagedao(TblEvalServiceWeightageDao tblEvalServiceWeightagedao) {
        this.tblEvalServiceWeightagedao = tblEvalServiceWeightagedao;
    }

    public TblEvalRoundMasterDao getTblEvalRoundMasterDao() {
        return tblEvalRoundMasterDao;
    }

    public void setTblEvalRoundMasterDao(TblEvalRoundMasterDao tblEvalRoundMasterDao) {
        this.tblEvalRoundMasterDao = tblEvalRoundMasterDao;
    }
    
    /**
     * @return the evaluationService
     */
    public EvaluationService getEvaluationService() {
        return evaluationService;
    }

    /**
     * @param evaluationService the evaluationService to set
     */
    public void setEvaluationService(EvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<Object[]> getCritetria() {

        logger.debug("getCritetria : "+logUserId+" Starts");
        List<Object[]> listObj = null;
        try {
            listObj = hibernateQueryDao.createNewQuery("select subCriteriaId, subCriteria, mainCriteria from TblEvalServiceCriteria");
        } catch (Exception ex) {
            logger.error("getCritetria : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getCritetria : "+logUserId+" Ends");
        return listObj;
    }

    @Override
    public List<Object[]> getSubCriteria(int criId) {

        logger.debug("getSubCriteria : "+logUserId+" Starts");
        List<Object[]> listObj = null;
        try {
            listObj = hibernateQueryDao.createNewQuery("select subCriteriaId,subCriteria from TblEvalServiceCriteria where criteriaId=" + criId);
        } catch (Exception ex) {
            logger.error("getSubCriteria : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getSubCriteria : "+logUserId+" Ends");
        return listObj;
    }

    @Override
    public boolean insertTbl(List<TblEvalServiceForms> tblEvalServiceForms) {
        logger.debug("insertTbl : "+logUserId+" Starts");
        boolean flag = true;
        try {
            tblEvalServiceFormsDao.updateOrInsertAll(tblEvalServiceForms);
        } catch (Exception ex) {
             logger.error("insertTbl : "+logUserId+" : "+ex.toString());
            flag = false;
        }
        logger.debug("insertTbl : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<TblEvalServiceForms> listData(int tenderId) {
        logger.debug("listData : "+logUserId+" Starts");
        List<TblEvalServiceForms> list = null;
        try {
            list = tblEvalServiceFormsDao.findTblEvalServiceForms("tenderId", Operation_enum.EQ, tenderId);
        } catch (Exception ex) {
            logger.error("listData : "+logUserId+" : "+ex.toString());
        }
        logger.debug("listData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean deleteRecord(int tenderId,String isCorri) {
        logger.debug("deleteRecord : "+logUserId+" Starts");
        boolean flag = true;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete TblEvalServiceForms where tenderId=" + tenderId + " and isCurrent = '"+isCorri+"'");
        } catch (Exception ex) {
            logger.error("deleteRecord : "+logUserId+" : "+ex.toString());
            flag = false;
        }
        logger.debug("deleteRecord : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<Object[]> getSubCriteria() {
        logger.debug("getSubCriteria : "+logUserId+" Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select subCriteriaId,subCriteria from TblEvalServiceCriteria");
        } catch (Exception ex) {
            logger.error("getSubCriteria : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getSubCriteria : "+logUserId+" Ends");
        return list;
    }

    @Override    
    public List<Object[]> getEvalServiceMarks(int tenderFormId) {
        logger.debug("getEvalServiceMarks : "+logUserId+" Starts");
        List<Object[] > list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tec.subCriteria,tef.maxMarks,tec.subCriteriaId,tec.mainCriteria from TblEvalServiceForms tef,TblEvalServiceCriteria tec where tef.subCrietId=tec.subCriteriaId and tef.tenderFormId=" + tenderFormId);
        }catch(Exception ex){
            logger.error("getEvalServiceMarks : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getEvalServiceMarks : "+logUserId+" Ends");
        return list;
    }
    
    @Override
    public List<Object[]> viewEvalSerMark(int tenderId,String isCorri) {
        logger.debug("viewEvalSerMark : "+logUserId+" Starts");
        List<Object[] > list = null;
        try{
            if("yes".equalsIgnoreCase(isCorri)){
                isCorri = hibernateQueryDao.countForNewQuery("TblEvalServiceForms", "tenderId = "+tenderId+" and isCurrent = 'yes'")==0 ? "no" : "yes";
            }
        String query = "select esc.subCriteria,tes.maxMarks ,tes.tenderFormId,tes.subCrietId, esc.mainCriteria"
                + " from TblEvalServiceCriteria as esc ,TblEvalServiceForms as tes"
                + " where esc.subCriteriaId=tes.subCrietId and tes.tenderId="+tenderId+" and isCurrent = '"+isCorri+"'"
                +" order by tes.tenderFormId";
            list = hibernateQueryDao.createNewQuery(query);
        }catch (Exception ex){
            logger.error("viewEvalSerMark : "+logUserId+" : "+ex.toString());
        }
        logger.debug("viewEvalSerMark : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean insertTblEvalSerFormDetail(List<TblEvalSerFormDetail> tblEvalSerFormDetail) {
        logger.debug("insertTblEvalSerFormDetail : "+logUserId+" Starts");
        String action = "TEC Evaluate the form";
        boolean flag=  true;
        try {
            tblEvalSerFormDetailDao.updateInsertAllEvalSerFormDetail(tblEvalSerFormDetail);
        } catch (Exception ex) {
            logger.error("insertTblEvalSerFormDetail : "+logUserId+" : "+ex.toString());
            flag = false;
            action = "Error in "+action+" "+ex.getMessage();
        }finally{
             makeAuditTrailService.generateAudit(auditTrail, tblEvalSerFormDetail.get(0).getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, tblEvalSerFormDetail.get(0).getRemarks()!=null? tblEvalSerFormDetail.get(0).getRemarks() : "");
        }
        logger.debug("insertTblEvalSerFormDetail : "+logUserId+" Ends");
        return flag;
    }

	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> getEvalBidderMarks(String bidderId, String comMemId, String tenderId,String formId) {
        logger.debug("getEvalBidderMarks : "+logUserId+" Starts");
        List<Object[]> list = null;
        int evalCount = evaluationService.getEvaluationNo(Integer.parseInt( tenderId));
        try{
        StringBuilder sb = new StringBuilder();
        sb.append("select esd.maxMarks ,esd.actualMarks ,esd.ratingWeightage ,esd.ratedScore,tf.tenderFormId,tf.formName,esc.subCriteria,esd.bidId,esc.mainCriteria ");
        sb.append("from TblEvalSerFormDetail esd,TblTenderForms tf,TblEvalServiceCriteria esc ");
        sb.append("where esd.tblTenderForms.tenderFormId=tf.tenderFormId and esd.subCriteriaId=esc.subCriteriaId and esd.tenderId="+tenderId+" and esd.evalBy="+comMemId+" and esd.userId="+bidderId+" and tf.tenderFormId="+formId+" and esd.evalCount = "+ evalCount +" order by esd.bidId ");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        }catch(Exception ex){
            logger.error("getEvalBidderMarks : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getEvalBidderMarks : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object> getBiddersFormId(String bidderId, String tenderId) {
        logger.debug("getBiddersFormId : "+logUserId+" Starts");
        List<Object> list = null;
        try{
            list = hibernateQueryDao.singleColQuery("select distinct tf.tenderFormId from TblTenderBidForm tbf,TblTenderForms  tf where tbf.tblTenderMaster.tenderId="+tenderId+" and  tbf.userId="+bidderId+" and tbf.tblTenderForms.tenderFormId=tf.tenderFormId and tf.isPriceBid='no'");
        }catch(Exception ex){
            logger.error("getBiddersFormId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getBiddersFormId : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object> getBidIdFrmFormId(String bidderId, String formId) {
        logger.debug("getBidIdFrmFormId : "+logUserId+" Starts");
        List<Object> list = null;
        try{
            list = hibernateQueryDao.singleColQuery("select tbf.bidId from TblTenderBidForm tbf where tbf.userId="+bidderId+" and tbf.tblTenderForms.tenderFormId="+formId);
        }catch(Exception ex){
            logger.error("getBidIdFrmFormId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getBidIdFrmFormId : "+logUserId+" Ends");
        return list;
    }

    /**
     * Get details from tbl_EvalSerFormDetail
     * @param tenderId tenderId , userId userId, tenderFormId tenderFormId,bId bId
     * @return List of {0. subCriteria,1. esFormDetailId,2. remarks,3. maxMarks,4. bidId,5. ratingWeightage,6. ratedScore,7. actualMarks,8. subCriteriaId,9. mainCriteria}
     */
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId,int evalBy,int bId) {
        logger.debug("getDetailsEvalSerMark : "+logUserId+" Starts");
        List<Object[]> list = null;
        int evalCount = evaluationService.getEvaluationNo(tenderId);
        try {
            list = hibernateQueryDao.createNewQuery("select tesc.subCriteria,tesfd.esFormDetailId,tesfd.remarks,tesfd.maxMarks,tesfd.bidId,tesfd.ratingWeightage,tesfd.ratedScore,tesfd.actualMarks,tesfd.subCriteriaId,tesc.mainCriteria from TblEvalSerFormDetail tesfd , TblEvalServiceCriteria tesc where tesfd.subCriteriaId = tesc.subCriteriaId and tesfd.userId = " + userId + " and tesfd.tenderId = " + tenderId + " and tesfd.tblTenderForms.tenderFormId = "+tenderFormId+"and tesfd.evalBy="+evalBy + "and tesfd.bidId= " + bId + " and tesfd.evalCount="+evalCount+" order by tesfd.esFormDetailId");
        } catch (Exception ex) {
            logger.error("getDetailsEvalSerMark : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getDetailsEvalSerMark : "+logUserId+" Ends");
        return list;
    }
    /**
     * Get details from tbl_EvalSerFormDetail
     * @param tenderId tenderId , userId userId, tenderFormId tenderFormId
     * @return List of object
     */
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId,int evalBy) {
        logger.debug("getDetailsEvalSerMark : "+logUserId+" Starts");
        List<Object[]> list = null;
        int evalCount = evaluationService.getEvaluationNo(tenderId);
        try {
            list = hibernateQueryDao.createNewQuery("select tesc.subCriteria,tesfd.esFormDetailId,tesfd.remarks,tesfd.maxMarks,tesfd.bidId from TblEvalSerFormDetail tesfd , TblEvalServiceCriteria tesc where tesfd.subCriteriaId = tesc.subCriteriaId and tesfd.userId = " + userId + " and tesfd.tenderId = " + tenderId + " and tesfd.tblTenderForms.tenderFormId = "+tenderFormId+" and tesfd.evalBy="+evalBy + " and tesfd.evalCount="+evalCount+" order by tesfd.esFormDetailId");
        } catch (Exception ex) {
            logger.error("getDetailsEvalSerMark : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getDetailsEvalSerMark : "+logUserId+" Ends");
        return list;
    }

    /**
     * delete data from tbl_EvalSerFormDetail
     * @param whereCondition whereCondition
     * @return true and false
     */
    @Override
    public boolean insertDeleteTblEvalSerFormDetail(String whereCondition) {
        logger.debug("insertDeleteTblEvalSerFormDetail : "+logUserId+" Starts");
        boolean flag = true;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete TblEvalSerFormDetail tesfd where tesfd.esFormDetailId in("+ whereCondition +")");
        } catch (Exception ex) {
            logger.error("insertDeleteTblEvalSerFormDetail : "+logUserId+" : "+ex.toString());
            flag = false;
        }
        logger.debug("insertDeleteTblEvalSerFormDetail : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<Object[]> getFinalSubBidders(String tenderId) {
        logger.debug("getFinalSubBidders : "+logUserId+" Starts");
        List<Object[]> list = null;
        //int evalCount=0;
        try{
            //List<Object> listEval = hibernateQueryDao.getSingleColQuery("select MAX(ter.evalCount) from TblEvalRptSentToAa ter where tblTenderMaster.tenderId="+tenderId);
            //if(!listEval.isEmpty() && listEval!=null){
           //     evalCount = Integer.parseInt(listEval.get(0).toString());
           // }


            //if(evalCount==0){
            list =hibernateQueryDao.createNewQuery("select tfs.userId,tcm.companyName,ttm.firstName,ttm.lastName from TblFinalSubmission tfs,TblCompanyMaster tcm,TblTendererMaster ttm "
                    + "where tfs.bidSubStatus='finalsubmission' and tfs.tblTenderMaster.tenderId="+tenderId+" and tcm.companyId=ttm.tblCompanyMaster.companyId and ttm.tblLoginMaster.userId = tfs.userId ");
                    
           // }
            //else{ dine by dohatec and comment out
           // evalCount = evalCount - 1;
            //list =hibernateQueryDao.createNewQuery("select tfs.userId,tcm.companyName,ttm.firstName,ttm.lastName from TblFinalSubmission tfs,TblCompanyMaster tcm,TblTendererMaster ttm TblPostQualification pq "
            //        + "where tfs.bidSubStatus='finalsubmission' and tfs.tblTenderMaster.tenderId="+tenderId+" and tcm.companyId=ttm.tblCompanyMaster.companyId and ttm.tblLoginMaster.userId = tfs.userId "
            //        + "pq.noaStatus = 'pending' and pq.evalCount = "+evalCount+" and pq.userId = tfs.userId");
            //}
        }catch(Exception ex){
            logger.error("getFinalSubBidders : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getFinalSubBidders : "+logUserId+" Ends");
        return list;
    }
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> evalServiceRepData(String formId,String govUserId,String tenderId,int evalCount) {
        logger.debug("evalServiceRepData : "+logUserId+" Starts");
        List<Object[]> list = null;

        try{
        StringBuilder query = new StringBuilder();
        query.append("select distinct tf.tenderFormId,tf.formName,esc.subCriteria,esd.maxMarks,esd.subCriteriaId,esc.mainCriteria ");
        query.append("from TblEvalSerFormDetail esd,TblTenderForms tf,TblEvalServiceCriteria esc ");
        query.append("where tf.tenderFormId="+formId+" and tf.tenderFormId=esd.tblTenderForms.tenderFormId and esd.subCriteriaId=esc.subCriteriaId and esd.evalBy="+govUserId+" and esd.evalCount="+evalCount+" and esd.tenderId="+tenderId+" order by esd.subCriteriaId");
            list = hibernateQueryDao.createNewQuery(query.toString());
        }catch(Exception ex){
            logger.error("evalServiceRepData : "+logUserId+" : "+ex.toString());
    }
        logger.debug("evalServiceRepData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object> getAllFormId(String tenderId) {
        logger.debug("getAllFormId : "+logUserId+" Starts");
        List<Object> list = null;
        try{
            list = hibernateQueryDao.singleColQuery("select distinct tf.tenderFormId from TblTenderBidForm tbf,TblTenderForms  tf where tbf.tblTenderMaster.tenderId="+tenderId+" and tbf.tblTenderForms.tenderFormId=tf.tenderFormId and tf.isPriceBid='no'");
        }catch(Exception ex){
             logger.error("getAllFormId : "+logUserId+" : "+ex.toString());
    }
         logger.debug("getAllFormId : "+logUserId+" Ends");
        return list;
    }
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> evalServiceRepBidderData(String tenderId,String formId, String govUserId, String bidderId, String subCriteriaId,int evalCount) {
        logger.debug("evalServiceRepBidderData : "+logUserId+" Starts");
        List<Object[]> list = null;
       
        try{
        StringBuilder query = new StringBuilder();
        query.append("select esd.actualMarks ,esd.ratingWeightage ,esd.ratedScore ");
        query.append("from TblEvalSerFormDetail esd,TblTenderForms tf,TblEvalServiceCriteria esc ");
        query.append("where esd.tblTenderForms.tenderFormId=tf.tenderFormId and esd.subCriteriaId=esc.subCriteriaId ");
        query.append("and esd.tenderId="+tenderId+" and ");
        query.append("esd.evalBy="+govUserId+" and ");
        query.append("esd.userId="+bidderId+" and tf.tenderFormId="+formId+" and esd.subCriteriaId="+subCriteriaId+" and esd.evalCount = "+ evalCount +"");
            list = hibernateQueryDao.createNewQuery(query.toString());
        }catch(Exception ex){
            logger.error("evalServiceRepBidderData : "+logUserId+" : "+ex.toString());
        }
        logger.debug("evalServiceRepBidderData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getComMemberName(String tenderId,String userId) {
        logger.debug("getComMemberName : "+logUserId+" Starts");
        List<Object[]> list = new ArrayList<Object[]>();

        try{
        StringBuilder query = new StringBuilder();
        query.append("select tem.tblLoginMaster.userId,tem.employeeName from TblEmployeeMaster tem where tem.tblLoginMaster.userId in (select distinct esd.evalBy from TblEvalSerFormDetail esd where ");
            query.append("esd.tenderId="+tenderId+" and esd.userId="+userId+" and esd.isMemberDropped='No' and esd.evalBy in (select escp.sentBy from TblEvalSentQueToCp escp where escp.tenderId="+tenderId+" and escp.sentFor='evaluation'))");

            //query.append(" union ");
            list.addAll(hibernateQueryDao.createNewQuery(query.toString()));
            query.delete(0, query.length());

            query.append("select distinct em.tblLoginMaster.userId,em.employeeName ");
            query.append("from TblEmployeeMaster em ,TblEvalSerFormDetail ems ,TblCommittee c ,TblCommitteeMembers cm ");
            query.append("where ");
            query.append("em.tblLoginMaster.userId = ems.evalBy and c.tblTenderMaster.tenderId=ems.tenderId and c.committeeId=cm.tblCommittee.committeeId and ");
            query.append("c.committeStatus='approved' and c.committeeType in ('TEC','PEC') ");
            query.append("and cm.memberRole='cp' and cm.userId=ems.evalBy ");
            query.append("and c.tblTenderMaster.tenderId = "+tenderId+" and ems.userId ="+userId+" ");

            //query.append(" union ");
            list.addAll(hibernateQueryDao.createNewQuery(query.toString()));
            query.delete(0, query.length());
            
        query.append("select temi.tblLoginMaster.userId,temi.fullName from TblExternalMemInfo temi where temi.tblLoginMaster.userId in (select distinct esd.evalBy from TblEvalSerFormDetail esd where ");
            query.append("esd.tenderId="+tenderId+" and esd.userId="+userId+" and esd.isMemberDropped='No' and esd.evalBy in (select escp.sentBy from TblEvalSentQueToCp escp where escp.tenderId="+tenderId+" and escp.sentFor='evaluation'))");
            list.addAll(hibernateQueryDao.createNewQuery(query.toString()));
            //list = hibernateQueryDao.createNewQuery(query.toString());
        }catch(Exception ex){
            logger.error("getComMemberName : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getComMemberName : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getBidderMarksMem(String bidderId,String tenderId,String formId) {
        logger.debug("getBidderMarksMem : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
        StringBuilder sb = new StringBuilder();
        List<Object[]> memList = getComMemberName(tenderId,bidderId);
        int i =0;
        if(!memList.isEmpty()){
            i = Integer.parseInt(memList.get(0)[0].toString());
        }
        sb.append("select esd.maxMarks ,esd.actualMarks ,esd.ratingWeightage ,esd.ratedScore,tf.tenderFormId,tf.formName,esc.subCriteria,esd.bidId,esc.subCriteriaId ");
        sb.append("from TblEvalSerFormDetail esd,TblTenderForms tf,TblEvalServiceCriteria esc ");
        sb.append("where esd.tblTenderForms.tenderFormId=tf.tenderFormId and esd.subCriteriaId=esc.subCriteriaId and esd.tenderId="+tenderId+" and esd.evalBy="+i+" and esd.userId="+bidderId+"  and tf.tenderFormId="+formId+" order by esd.bidId ");
            list = hibernateQueryDao.createNewQuery(sb.toString());
        }catch(Exception ex){
           logger.error("getBidderMarksMem : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getBidderMarksMem : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getDataForMembers(String tenderId,String memId,String userId,String formId,String critId,String bidId){
        logger.debug("getDataForMembers : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
        StringBuilder query = new StringBuilder();
        query.append("select esd.actualMarks ,esd.ratingWeightage ,esd.ratedScore,esd.bidId from ");
        query.append("TblEvalSerFormDetail esd where esd.tenderId="+tenderId+" and esd.evalBy="+memId+" and ");
        query.append("esd.userId="+userId+" and esd.tblTenderForms.tenderFormId="+formId+" and subCriteriaId="+critId+" and bidId="+bidId);
            list = hibernateQueryDao.createNewQuery(query.toString());
        }catch(Exception ex){
            logger.error("getDataForMembers : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getDataForMembers : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean dropMemberFrmReport(String tenderId,String memId,String bidderId,String dropMember){
        logger.debug("dropMemberFrmReport : "+logUserId+" Starts");
        boolean flag = true;
        try{
        int i = hibernateQueryDao.updateDeleteNewQuery("update TblEvalSerFormDetail set isMemberDropped='"+dropMember+"' where tenderId="+tenderId+" and evalBy="+memId+" and userId="+bidderId);
            if(i<=0){
                flag=  false;
        }
        }catch(Exception ex){
            logger.error("dropMemberFrmReport : "+logUserId+" : "+ex.toString());
            flag = false;
    }
        logger.debug("dropMemberFrmReport : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public double getTotal4ComMemReport(String tenderId,String memId,String bidderId) {
        logger.debug("getTotal4ComMemReport : "+logUserId+" Starts");
        double sum = 0;
        try{
        for (Object object : hibernateQueryDao.singleColQuery("select esd.actualMarks from TblEvalSerFormDetail esd where esd.tenderId="+tenderId+" and esd.evalBy="+memId+" and esd.userId="+bidderId)) {
            sum+=Double.parseDouble(object.toString());
            }
        }catch(Exception ex){
            logger.error("getTotal4ComMemReport : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getTotal4ComMemReport : "+logUserId+" Ends");
        return sum;
    }

    @Override
    public Object getPassingMarksofTender(String tenderId){
        logger.debug("getPassingMarksofTender : "+logUserId+" Starts");
        Object obj = null;
        List<Object> list = null;
        
        try{
            list = hibernateQueryDao.getSingleColQuery("select tdd.passingMarks from TblTenderDetails tdd where tdd.tblTenderMaster.tenderId="+tenderId);
        if(list.isEmpty()){
                obj = "0";
            }else{
                obj = list.get(0);
        }
        }catch(Exception ex){
            logger.error("getPassingMarksofTender : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getPassingMarksofTender : "+logUserId+" Ends");
        return obj;
    }
	//Change by dohatec for re-evaluation
    @Override
    public boolean insertEvalBidderStatus(TblEvalBidderStatus bidderStatus,List<TblEvalBidderMarks> list) {
        logger.debug("insertEvalBidderStatus : "+logUserId+" Starts");
        boolean flag = true;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblEvalBidderStatus where tenderId="+bidderStatus.getTenderId()+" and userId="+bidderStatus.getUserId()+" and evalStatus='"+bidderStatus.getEvalStatus()+"' and evalCount = '"+bidderStatus.getEvalCount()+"'");
            tblEvalBidderStatusDao.addTblEvalBidderStatus(bidderStatus);
            if(!list.isEmpty()){
                hibernateQueryDao.updateDeleteNewQuery("delete from TblEvalBidderMarks where userId="+list.get(0).getUserId()+" and tenderId="+list.get(0).getTenderId()+" and createdBy="+list.get(0).getCreatedBy()+"' and evalCount = '"+list.get(0).getEvalCount()+"'");
                tblEvalBidderMarksDao.updateInsAllEvalBidderMarks(list);
    }
        } catch (Exception ex) {
            logger.error("insertEvalBidderStatus : "+logUserId+" : "+ex.toString());
            flag = false;
        }
        logger.debug("insertEvalBidderStatus : "+logUserId+" Ends");
        return flag;
    }
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> getBidderEvalStatus(String tenderId,String stat,int evalCount){
        logger.debug("getBidderEvalStatus : "+logUserId+" Starts");
        List<Object[]> list = null;
        StringBuilder query = new StringBuilder();
        
        try {
            query.append("select tbs.bidderMarks ,ttm.firstName,ttm.lastName,tcm.companyName,tbs.userId ");
            query.append("from TblEvalBidderStatus tbs,TblCompanyMaster tcm, TblTendererMaster ttm ");
            query.append("where tbs.tenderId="+tenderId+" and tbs.evalStatus='"+stat+"' and ttm.tblLoginMaster.userId=tbs.userId and tcm.companyId = ttm.tblCompanyMaster.companyId and result='pass' and tbs.evalCount = "+evalCount+"");
        list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception ex) {
           logger.error("getBidderEvalStatus : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getBidderEvalStatus : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean insertBidderRank(String tenderId,String createdBy,List<TblBidderRank> list){
        logger.debug("insertBidderRank : "+logUserId+" Starts");
        boolean flag = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblBidderRank tbr where tbr.tblTenderMaster.tenderId="+tenderId+" and tbr.createdBy="+createdBy);
            tblBidderRankDao.updateOrSaveBidderRank(list);
            flag = true;
        } catch (Exception ex) {
            logger.error("insertBidderRank : "+logUserId+" : "+ex.toString());
        }
        logger.debug("insertBidderRank : "+logUserId+" Ends");
        return flag;
    }
    @Override
    public List<SPCommonSearchDataMore> getBidderMarksMemSP(String bidderId,String tenderId,String formId,String officerId) {
        logger.debug("getBidderMarksMem : "+logUserId+" Starts");
        List<SPCommonSearchDataMore> list=null;
        try{
            /*StringBuilder sb = new StringBuilder();
            List<Object[]> memList = getComMemberName(tenderId,bidderId);
            int i =0;
            if(!memList.isEmpty()){
                i = Integer.parseInt(memList.get(0)[0].toString());
            }*/            
            list = spgetCommonSearchDataMore.executeProcedure("getBidderMarksMem", tenderId, formId, bidderId, officerId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }catch(Exception ex){
           logger.error("getBidderMarksMem : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getBidderMarksMem : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<SPCommonSearchDataMore> getBidderMarksMemWithCrit(String bidderId, String tenderId, String formId, String officerId, String critId) {
            return spgetCommonSearchDataMore.executeProcedure("getBidderMarksMemWithCrit", tenderId, formId, bidderId, officerId, critId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
   }

    @Override
    public List<SPCommonSearchDataMore> getTotal4ComMemReportSP(String bidderId, String tenderId, String officerId) {
        return spgetCommonSearchDataMore.executeProcedure("getTotal4ComMemReport", tenderId, bidderId, officerId,null,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null);
   }

    @Override
    public long evalCertiLink(int tenderId) {
         logger.debug("evalCertiLink : "+logUserId+" Starts");
         long cnt = 0;
         String from = "TblConfigEvalMethod tcm,TblTenderDetails ttd,TblTenderTypes ttt";
         String where = "tcm.procurementMethodId=ttd.procurementMethodId "
                 + "and tcm.procurementNatureId=ttd.procurementNatureId "
                 + "and ttd.evalType=ttt.tenderType "
                 + "and tcm.tenderTypeId=ttt.tenderTypeId "
                 + "and ttd.tblTenderMaster.tenderId="+tenderId
                 + " and tcm.evalMethod=1";
        try {
            cnt = hibernateQueryDao.countForNewQuery(from, where);
        } catch (Exception e) {
            logger.error("evalCertiLink : "+logUserId+" :"+e);
        }
        logger.debug("evalCertiLink : "+logUserId+" Ends");
        return cnt;
    }
	//Change by dohatec for re-evaluation
    @Override
    public Object getEvalBidderMarks(String tenderId,String tenderFormId ,String createdBy ,String userId ,String subCrietId, int evalCount){
        logger.debug("getEvalBidderMarks : "+logUserId+" Starts");        
        Object marks="0";
        try {
        List<Object>  list =  hibernateQueryDao.singleColQuery("select tem.marks from TblEvalBidderMarks tem where tem.tenderId="+tenderId+" and tem.tenderFormId="+tenderFormId+" and tem.createdBy="+createdBy+" and tem.userId="+userId+" and tem.subCrietId="+subCrietId+" and tem.evalCount = "+evalCount);
        if(!list.isEmpty()){
            marks = list.get(0);
        }
        } catch (Exception e) {
            logger.error("getEvalBidderMarks : "+logUserId,e);
        }
        logger.debug("getEvalBidderMarks : "+logUserId+" Ends");
        return marks;
    }
	//Change by dohatec for re-evaluation
    @Override
    public List<Object> getEvalBidderTotal(String tenderId ,String createdBy ,String userId, int evalCount){
        logger.debug("getEvalBidderTotal : "+logUserId+" Starts");
        List<Object>  list = null;
        try {
             list = hibernateQueryDao.singleColQuery("select tem.marks from TblEvalBidderMarks tem where tem.tenderId="+tenderId+" and tem.createdBy="+createdBy+" and tem.userId="+userId+ " and tem.evalCount = "+evalCount);
        } catch (Exception e) {
            logger.error("getEvalBidderTotal : "+logUserId,e);
        }
        logger.debug("getEvalBidderTotal : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<SPCommonSearchDataMore> getSeekClariLinkTextLinkSer(String formId,String critId,String cpId,String memId,String bidderId){
        return spgetCommonSearchDataMore.executeProcedure("seekClariLinkTextLinkSer", formId, critId, cpId, memId, bidderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    @Override
    public long countForMarksAvail(String tenderId, String memberId, String bidderId) {
        logger.debug("countForMarksAvail : "+logUserId+" Starts");
        long count=0;
        try {
            count=hibernateQueryDao.countForNewQuery("TblEvalBidderStatus tbs", "tbs.tenderId="+tenderId+" and tbs.userId="+bidderId+" and tbs.evalBy="+memberId);
        } catch (Exception ex) {
            logger.error("countForMarksAvail : "+logUserId,ex);
        }
        logger.debug("countForMarksAvail : "+logUserId+" Ends");
        return count;
    }

    @Override
    public boolean reTendering(String tenderId) {
        logger.debug("reTendering : "+logUserId+" Starts");
        boolean flag = false;
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select ta.rptStatus,ta.userId from TblEvalRptSentToAa ta where ta.tblTenderMaster.tenderId="+tenderId+" and ta.rptStatus='Rejected / Re-Tendering'");
            long countttls = hibernateQueryDao.countForNewQuery("TblTenderLotSecurity ttls", "ttls.tblTenderMaster.tenderId = " + tenderId + "group by appPkgLotId");
            long countter = hibernateQueryDao.countForNewQuery("TblEvalRptSentToAa ter", "ter.tblTenderMaster.tenderId = " + tenderId + " and ter.rptStatus='Rejected / Re-Tendering'");
            if(!list.isEmpty()){
                if(countter == countttls){
                    flag=true;
                }
            }
        } catch (Exception ex) {
            logger.error("reTendering : "+logUserId+" : "+ex.toString());
        }
        logger.debug("reTendering : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean reTenderingHide(String tenderId) {
        logger.debug("reTenderingHide : "+logUserId+" Starts");
        boolean flag = false;
        List<Object> list = null;
        try {
            list = hibernateQueryDao.getSingleColQuery("select td.reTenderId from TblTenderDetails td where reTenderId="+tenderId);
            if(!list.isEmpty()){
                flag=true;
            }
        } catch (Exception ex) {
            logger.error("reTenderingHide : "+logUserId+" : "+ex.toString());
        }
        logger.debug("reTenderingHide : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public TblEvalServiceWeightage getTblEvalServiceByTenderId(int tenderId) {
        logger.debug("reTenderingHide : "+logUserId+" Starts");
         TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
        try {
            tblEvalServiceWeightage =  tblEvalServiceWeightagedao.findTblEvalServiceWeightage("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId)).get(0);
        } catch (Exception ex) {
            logger.error("reTenderingHide : "+logUserId+" : "+ex.toString());
        }
        logger.debug("reTenderingHide : "+logUserId+" Ends");
        return tblEvalServiceWeightage;
    }

    @Override
    public List<Object[]> getT1Data(String tenderId) {
        logger.debug("getT1Data : "+logUserId+" Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String sqlQuery = "select amount, companyName, userId from TblBidderRank "
                    + "where roundId in (select roundId from TblEvalRoundMaster "
                    + "where reportType='T1' and tenderId = "+tenderId+" and reportId = 0)"
                    + "and userId not in (select na.userId  from TblNoaAcceptance na , TblNoaIssueDetails nd where nd.noaIssueId = na.tblNoaIssueDetails.noaIssueId and nd.tblTenderMaster.tenderId = "+tenderId+" and na.acceptRejStatus = 'decline')";
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("getT1Data : "+logUserId+" : "+e.toString());
        }
        logger.debug("getT1Data : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<TblBidderRank> getT1L1SaveData(int tenderId, int roundId) {
        // @return List<object []> object[0] bidderId, object[1] rank, object[2] amount
        logger.debug("getT1L1SaveData : "+logUserId+" Starts");
        List<TblBidderRank> list = new ArrayList<TblBidderRank>();
        try {
            list = tblBidderRankDao.findTblBidderRank("roundId",Operation_enum.EQ,roundId,"tblTenderMaster",Operation_enum.EQ,(new TblTenderMaster(tenderId)));
        } catch (Exception e) {
            logger.error("getT1L1SaveData : "+logUserId+" : "+e.toString());
        }
        logger.debug("getT1L1SaveData : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getL1Data(String tenderId, String roundId){
        StringBuilder query = new StringBuilder();
        query.append("select br.userId,br.amount from TblBidderRank br where br.roundId in");
        query.append(" (select MAX(rm.roundId) from TblEvalRoundMaster rm where rm.reportType='L1' and rm.tenderId="+tenderId+" and roundId<="+roundId+")");
        return hibernateQueryDao.createNewQuery(query.toString());
    }
	//Change by dohatec for re-evaluation
     @Override
    public boolean saveT1Data(int tenderId,int userId) {
        logger.debug("getT1L1SaveData : "+logUserId+" Starts");
        boolean flag = false;
        List<Object[]> list = new ArrayList<Object[]>();
        int evalCount = evaluationService.getEvaluationNo(tenderId);
        try {
            long cnt = hibernateQueryDao.countForQuery("TblEvalRoundMaster", "pkgLotId = 0 and reportType = 'T1' and tenderId="+tenderId+" and evalCount = "+evalCount);
            if(cnt==0){
                String sqlQuery  = "select ebs.bidderMarks, ebs.userId,cm.companyId,cm.companyName,tm.firstName,tm.lastName "
                        + "from TblEvalBidderStatus ebs ,TblTendererMaster tm, TblCompanyMaster cm "
                        + "where cm.companyId=tm.tblCompanyMaster.companyId and tm.tblLoginMaster.userId = ebs.userId and "
                        + "ebs.tenderId = "+tenderId+" and ebs.result = 'pass' and ebs.evalCount = "+evalCount+" order by ebs.bidderMarks desc";
                list = hibernateQueryDao.createNewQuery(sqlQuery);
                if(!list.isEmpty()){
                    TblEvalRoundMaster tblEvalRoundMaster = new TblEvalRoundMaster(0,tenderId,0,"T1",0,Integer.parseInt(list.get(0)[1].toString()),new Date(),userId,evalCount);
                    tblEvalRoundMasterDao.addTblEvalRoundMaster(tblEvalRoundMaster);
                    List<TblBidderRank> listAdd = new ArrayList<TblBidderRank>();
                    int i = 1;
                    for (Object[] obj: list) {
                            TblBidderRank tblBidderRank = new TblBidderRank();
                            if("1".equalsIgnoreCase(obj[2].toString())){
                            tblBidderRank.setCompanyName(obj[4]+"-"+obj[5]);
                            }else{
                                tblBidderRank.setCompanyName(obj[3].toString());
                            }
                            tblBidderRank.setRank(i);
                            tblBidderRank.setTblTenderMaster(new TblTenderMaster(tenderId));
                            tblBidderRank.setPkgLotId(0);
                            tblBidderRank.setCreatedBy(userId);
                            tblBidderRank.setCreatedTime(new Date());
                            tblBidderRank.setRoundId(tblEvalRoundMaster.getRoundId());
                            tblBidderRank.setAmount(new BigDecimal(obj[0].toString()));
                            tblBidderRank.setUserId(Integer.parseInt(obj[1].toString()));
                            tblBidderRank.setRowId(0);
                            tblBidderRank.setTableId(0);
                            tblBidderRank.setReportId(0);
                            listAdd.add(tblBidderRank);
                            i++;
                    }
                    tblBidderRankDao.updateAll(listAdd);
                }
            }
                flag = true;
        } catch (Exception e) {
            logger.debug("getT1L1SaveData : "+logUserId+" "+e);
        }
        logger.debug("getT1L1SaveData : "+logUserId+" Ends");
        return flag;
    }
    
    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public List<Object[]> getMainCirteriaList() {
        List<Object[]> list = null;
        logger.debug("getMainCirteriaList : "+logUserId+" Starts");
        try {
            list = hibernateQueryDao.createNewQuery("select distinct criteriaId,mainCriteria from TblEvalServiceCriteria order by mainCriteria asc");
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("getMainCirteriaList : "+logUserId+" "+e);
        }
        logger.debug("getMainCirteriaList : "+logUserId+" Ends");
        return list;
    }
	//Change by dohatec for re-evaluation
    @Override
    public List<Object[]> getBidderResult(String tenderId, String userId, String evalType,int evalCount) {
        List<Object[]> list = null;
        if(userId.equals("0")){
            list = hibernateQueryDao.createNewQuery("select eb.result,eb.remarks,eb.userId from TblEvalBidderStatus eb where eb.tenderId="+tenderId+" and eb.evalStatus='"+evalType+"' and eb.evalCount = '"+evalCount+"' order by userId");
        }else{
            list = hibernateQueryDao.createNewQuery("select eb.result,eb.remarks from TblEvalBidderStatus eb where eb.tenderId="+tenderId+" and eb.userId="+userId+" and eb.evalStatus='"+evalType+"' and eb.evalCount = '"+evalCount+"'");
        }
        return list;
    }

     @Override
    public Object getBidderExtensionStatus(String tenderId,String userId) {
        logger.debug("getBidderExtensionStatus : "+logUserId+" Starts");
        Object status="OK";
        try {
        //List<Object>  list =  hibernateQueryDao.singleColQuery("select status from TblTenderValAcceptance where tblTenderValidityExtDate in (select top 1 valExtDtId from TblTenderValidityExt where tenderId = "+ tenderId + " and govUserId = "+ userId + " and extStatus = 'Approved' order by valExtDtId desc)");
        List<Object>  list =  hibernateQueryDao.singleColQuery("select valExtDtId from TblTenderValidityExtDate where tenderId = "+ tenderId + " and extStatus = 'Approved' order by valExtDtId desc");
        if(list!= null && !list.isEmpty()){
            //status = list.get(0);
            List<Object>  list1 =  hibernateQueryDao.singleColQuery("select status from TblTenderValAcceptance where userId="+userId+" and  valExtDtId  ="+list.get(0).toString());
            if(list1!= null && !list1.isEmpty()){

                if("pending".equalsIgnoreCase(list1.get(0).toString().trim())){
                    status= "<div style='color: Red;' >Tenderer/Consultant not yet accepted(pending) the extension request</div>";
                }
                else if("reject".equalsIgnoreCase(list1.get(0).toString().trim())){
                   status= "<div style='color: Red;' >Tenderer/Consultant rejected the extension request</div>";
                }
                list1.clear();
                list1 = null;
            }
            list.clear();
            list = null;
        }
        } catch (Exception e) {
            logger.error("getBidderExtensionStatus : "+logUserId,e);
            e.printStackTrace();
        }
        logger.debug("getBidderExtensionStatus : "+logUserId+" Ends");
        return status;
    }

}
