/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDebriefdetailDocDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDebriefingDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc;
import com.cptu.egp.eps.model.table.TblTenderDebriefing;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.TenderDebriefingService;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author dixit
 */
public class TenderDebriefingServiceImpl implements TenderDebriefingService{

private static final Logger LOGGER = Logger.getLogger(TenderDebriefingServiceImpl.class);
private String logUserId = "0";

private  TblTenderDebriefingDao tblTenderDebriefingDao;
private  TblTenderDebriefdetailDocDao tblTenderDebriefdetailDocDao;
private HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public TblTenderDebriefdetailDocDao getTblTenderDebriefdetailDocDao() {
        return tblTenderDebriefdetailDocDao;
    }

    public void setTblTenderDebriefdetailDocDao(TblTenderDebriefdetailDocDao tblTenderDebriefdetailDocDao) {
        this.tblTenderDebriefdetailDocDao = tblTenderDebriefdetailDocDao;
    }

    public TblTenderDebriefingDao getTblTenderDebriefingDao() {
        return tblTenderDebriefingDao;
    }

    public void setTblTenderDebriefingDao(TblTenderDebriefingDao tblTenderDebriefingDao) {
        this.tblTenderDebriefingDao = tblTenderDebriefingDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public int saveTenderDebriefing(TblTenderDebriefing tblTenderDebriefing) {
        int debriefId = 0;
        LOGGER.debug("saveTenderDebriefing : "+logUserId+" : Starts");
        try {
               tblTenderDebriefingDao.addTblTenderDebriefing(tblTenderDebriefing);
               debriefId = tblTenderDebriefing.getDebriefId();
        } catch (Exception e) {
            LOGGER.error("saveTenderDebriefing : "+logUserId+" : "+e);
        }
        LOGGER.debug("saveTenderDebriefing : "+logUserId+" : Ends");
        return debriefId;
    }

    @Override
    public List<TblTenderDebriefing> getDebriefQuestionData(int tenderid,int userid) {
        LOGGER.debug("getDebriefQuestionData : " + logUserId + "Starts");
        List<TblTenderDebriefing> list = null;
        try {
            list = tblTenderDebriefingDao.findTblTenderDebriefing("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderid),"quePostedBy",Operation_enum.EQ,userid);
        } catch (Exception ex) {
            LOGGER.error("getDebriefQuestionData : " + logUserId +ex);
        }
        LOGGER.debug("getDebriefQuestionData : " + logUserId + "Ends");
        return list;


    }

    @Override
    public List<TblTenderDebriefing> getDebriefQuestionDataForPE(int tenderid) {
        LOGGER.debug("getDebriefQuestionData : " + logUserId + "Starts");
        List<TblTenderDebriefing> list = null;
        try {
            list = tblTenderDebriefingDao.findTblTenderDebriefing("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderid));
        } catch (Exception ex) {
            LOGGER.error("getDebriefQuestionData : " + logUserId +ex);
        }
        LOGGER.debug("getDebriefQuestionData : " + logUserId + "Ends");
        return list;
    }

    @Override
    public List<TblTenderDebriefing> getDebriefData(int debriefid) {
        LOGGER.debug("getDebriefData : " + logUserId + "Starts");
        List<TblTenderDebriefing> list = null;
        try {
            list = tblTenderDebriefingDao.findTblTenderDebriefing("debriefId",Operation_enum.EQ,debriefid);
        } catch (Exception ex) {
            LOGGER.error("getDebriefData : " + logUserId +ex);
        }
        LOGGER.debug("getDebriefData : " + logUserId + "Ends");
        return list;

    }

    @Override
    public int updateDeriefData(int debriefid, int userid,String replytext) {
        int i = 0;
        LOGGER.debug("updateDeriefData : "+logUserId+" : Starts");
        try {
               i = hibernateQueryDao.updateDeleteNewQuery("update TblTenderDebriefing set replyText='"+replytext+"', answeredBy='"+userid+"', answeredDt=current_timestamp() where debriefId='"+debriefid+"'");
        } catch (Exception e) {
            LOGGER.error("updateDeriefData : "+logUserId+" : "+e);
        }
        LOGGER.debug("updateDeriefData : "+logUserId+" : Ends");
        return i;
    }

    @Override
    public List<Object[]> getCompanyName(int userid) {
        LOGGER.debug("getCompanyName : " + logUserId + "Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createNewQuery("select tcm.companyName from TblCompanyMaster tcm,TblLoginMaster tlm where tcm.tblLoginMaster.userId=tlm.userId and tlm.userId='"+userid+"'");
        } catch (Exception ex) {
            LOGGER.error("getCompanyName : " + logUserId +ex);
        }
        LOGGER.debug("getCompanyName : " + logUserId + "Ends");
        return list;
    }

    @Override
    public boolean saveDebriefingDocsDetail(TblTenderDebriefdetailDoc tblTenderDebriefdetailDoc) {
        LOGGER.debug("saveDebriefingDocsDetail : " + logUserId + "Starts");
        boolean flag = false;
        try {
            tblTenderDebriefdetailDocDao.addTblTenderDebriefdetailDoc(tblTenderDebriefdetailDoc);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("saveDebriefingDocsDetail : " + logUserId +ex);
        }
        LOGGER.debug("saveDebriefingDocsDetail : " + logUserId + "Ends");
        return flag;
    }

    @Override
    public List<TblTenderDebriefdetailDoc> getDebriefDetailData(int DetailDocId) {
        LOGGER.debug("getDebriefDetailData : " + logUserId + "Starts");
        List<TblTenderDebriefdetailDoc> list = null;
        try {
            list = tblTenderDebriefdetailDocDao.findTblTenderDebriefdetailDoc("keyId",Operation_enum.EQ,DetailDocId);
        } catch (Exception ex) {
            LOGGER.error("getDebriefDetailData : " + logUserId +ex);
        }
        LOGGER.debug("getDebriefDetailData : " + logUserId + "Ends");
        return list;
    }

    @Override
    public boolean  deleteDebriefDocsDetails(int DetailDocId) {
        LOGGER.debug("deleteDebriefDocsDetails : " + logUserId + "Starts");
        boolean flag = false;
        int i = 0;
        try {
            i = hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderDebriefdetailDoc where debDetailDocId=" + DetailDocId + "");
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("deleteDebriefDocsDetails : " + logUserId +e);
        }
        LOGGER.debug("deleteDebriefDocsDetails : " + logUserId + "Ends");
        return flag;
    }
}
