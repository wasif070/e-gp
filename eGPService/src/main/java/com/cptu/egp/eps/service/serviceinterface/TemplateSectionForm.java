/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster;
import com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail;
import com.cptu.egp.eps.model.table.TblTemplateMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.model.table.TblTenderMandatoryDoc;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TemplateSectionForm {
    /**
     * Add Form
     * @param tblForm
     * @return true if added else false
     */
    public boolean addForm(TblTemplateSectionForm tblForm);
    /**
     * Edit form
     * @param tblForm
     * @return true if added else false
     */
    public boolean editForm(TblTemplateSectionForm tblForm);
    /**
     * Delete form
     * @param formId
     * @return true if added else false
     */
    public boolean deleteForm(int formId);
    /**
     * Check if price bid form or not.
     * @param formId
     * @return true if added else false
     */
    public boolean isBOQForm(int formId);
    /**
     * Get form List by template and section id
     * @param templateId
     * @param sectionId
     * @return List of templates
     */
    public List<TblTemplateSectionForm> getForms(short templateId, int sectionId);
    /**
     * Get form Details
     * @param formId
     * @return List of form Data
     */
    public List<TblTemplateSectionForm> getSingleForms(int formId);
    /**
     * Set User Id at service layer
     * @param userId
     */
    public void setUserId(String userId);
    /**
     * Save STD Docs.
     * @param mandatoryDocs
     * @return true if added else false
     */
    public boolean saveSTDMandDocs(List<TblTemplateMandatoryDoc> mandatoryDocs);
    /**
     * Save STD Docs.
     * @param templateDocId
     * @return true if added else false
     */
    public boolean delSTDMandDoc(String templateDocId);
    /**
     * Delete STD Man. Docs
     * @param templateDocId
     * @param docName
     * @return true if added else false
     */
    public boolean updateSTDMandDoc(String templateDocId,String docName);
    /**
     * Update STD Mandatory Docs.
     * @param templateDocId
     * @return true if added else false
     */
    public List<Object[]> getSTDMandDocs(String templateDocId);
    /**
     * Get all STD Mandatory Docs.
     * @param templateId
     * @param sectionId
     * @param formId
     * @return List of Docs.
     */
    public List<Object[]> getSTDMandDocs(String templateId,String sectionId,String formId);
    /**
     * Save all STD Mandatory Docs.
     * @param mandatoryDocs
     * @return List of Docs.
     */
    public boolean saveTendMandDocs(List<TblTenderMandatoryDoc> mandatoryDocs);
    /**
     * Delete Man. docs.
     * @param tenderDocId
     * @return true if deleted else false
     */
    public boolean delTendMandDoc(String tenderDocId);
    /**
     * Update Tender Mand. Docs.
     * @param tenderDocId
     * @param docName
     * @return true if updated else false
     */
    public boolean updateTendMandDoc(String tenderDocId,String docName);
    /**
     * update tender Mand. Docs.
     * @param tenderDocId
     * @return true if added else false 
     */
    public List<Object[]> getTendMandDocs(String tenderDocId);
    /**
     * Get Tender Mand. Docs.
     * @param tenderId
     * @param formId
     * @return List Of Mand. Docs.
     */
    public List<Object[]> getTendMandDocs(String tenderId,String formId);
    /**
     * Add CMS Service BOq Master
     * @param tblcms
     * @return true if added else false
     */
    public boolean addCmsSrvBoqMaster(TblCmsSrvBoqMaster tblcms);
    /**
     * * Add CMS Service BOq Master Details.
     * @param tblcms
     * @return true if added else false
     */
    public boolean addCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail tblcms);
    /**
     * Get CMS Template Srv BoQ Details.
     * @param templateformId
     * @return List of Template Srvice BoQ Details.
     */
    public List<TblCmsTemplateSrvBoqDetail> getCmsTemplateSrvBoqDetail(int templateformId);
    /**
     * Update Service BOQ Details.
     * @param srvBoqDtlId
     * @param srvBoqId
     * @param templateformId
     * @return
     */
    public boolean updateCmsTemplateSrvBoqDetail(int srvBoqDtlId,int srvBoqId,int templateformId);
    /**
     * Get CMS Srv Data
     * @param sectionId
     * @return List of Data
     */
    public List<Object[]> getCmsSrvBoqMasterdata(int sectionId);
    /**
     * Get Srv BoQ Master
     * @param srvBoqIdId
     * @return
     */
    public List<TblCmsSrvBoqMaster> getSrvBoqMaster(int srvBoqIdId);
    
    /**
     * Get Form Type.
     * @param tenderFormId
     * @return
     */
    public List<Object> getFormType(int tenderFormId);
}
