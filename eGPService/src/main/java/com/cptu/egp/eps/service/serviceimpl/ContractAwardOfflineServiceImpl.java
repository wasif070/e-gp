/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import com.cptu.egp.eps.dao.daointerface.TblContractAwardedOfflineDao;
import com.cptu.egp.eps.dao.storedprocedure.ContractAwardOfflineDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPGetContractAwardOffline;
import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import com.cptu.egp.eps.service.serviceinterface.ContractAwardOfflineService;
import java.util.List;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;


/**
 *
 * @author Sohel
 */
public class ContractAwardOfflineServiceImpl implements ContractAwardOfflineService  {
    
    private static final Logger LOGGER = Logger.getLogger(ContractAwardOfflineServiceImpl.class);
    private String logUserId = "0";
    private TblContractAwardedOfflineDao tblContractAwardedOfflineDao;
    private SPGetContractAwardOffline spGetContractAwardOffline;
   private HibernateQueryDao hibernateQueryDao;
   private String getMinistryList = "getMinistryList : ";
   private String getOrgList = "getMinistryList : ";
   private String loggerStart = " Starts";
   private String loggerEnd = " Ends";

    public void setTblContractAwardedOfflineDao(TblContractAwardedOfflineDao objTbl) {
        this.tblContractAwardedOfflineDao = objTbl;
    }

    public SPGetContractAwardOffline getSpGetContractAwardOffline() {
        return spGetContractAwardOffline;
    }

    public void setSpGetContractAwardOffline(SPGetContractAwardOffline spGetContractAwardOffline) {
        this.spGetContractAwardOffline = spGetContractAwardOffline;
    }
    

    @Override
    public boolean insert(TblContractAwardedOffline contract) {
        //LOGGER.debug("addCmsTemplateSrvBoqDetail : " + logUserId + "starts");
        boolean boolTempSrvBoq = false;
        try {
            tblContractAwardedOfflineDao.addTblContractAwardedOffline(contract);
            boolTempSrvBoq = true;
        } catch (Exception ex) {
            //LOGGER.error("addCmsTemplateSrvBoqDetail : " + ex);
        }
        //LOGGER.debug("addCmsTemplateSrvBoqDetail : " + logUserId + "ends");
        return boolTempSrvBoq;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<ContractAwardOfflineDetails> getContractAwardOfflineData(String searchFlag,String status,String procNature,String procMethod,String refNo,String value,String advDateFrom,String advDateTo,String ministry,String action,String comment)
    {
    return getSpGetContractAwardOffline().executeProcedure(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry,action,comment);
    }



    
    /*public boolean deleteContractAwardOfflineData(int conAwardOfflineId) {
        String Query = "";
        LOGGER.debug("deleteContractAwardOfflineData : " + logUserId + "starts");
        boolean flag = false;
        int i = 0;
        try {
            Query= "DELETE FROM tbl_ContractAwardedOffline WHERE conAwardOfflineId = " + conAwardOfflineId + "";
            i = hibernateQueryDao.updateDeleteSQLNewQuery("delete from tbl_ContractAwardedOffline WHERE conAwardOfflineId = " + conAwardOfflineId + "");
            if (i > 0) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("deleteContractAwardOfflineData : " + logUserId + " " + e);
            LOGGER.error("Query : " + Query + " " + e);
        }
         
        }
        LOGGER.debug("deleteContractAwardOfflineData : " + logUserId + "Ends");
        return flag;
    }*/
    
    @Override
    public boolean deleteContractAwardOfflineData(String refNo) {

        Boolean isDelPkgDetail;
        String auditAction = "";
        try {
            isDelPkgDetail = spGetContractAwardOffline.executeProcedure(refNo).get(0).getFlag();
          
        } catch (Exception e) {
            isDelPkgDetail = false;
            LOGGER.error("delContractAward : " + logUserId + e);
        }
        LOGGER.debug("delContractAward : " + logUserId);
        return isDelPkgDetail;
    }

    @Override
    public List<Object[]> getMinistryForAwardedContractOffline() {
        LOGGER.debug(getMinistryList + logUserId + loggerStart);
        List<Object[]> listMinister = new ArrayList<Object[]>();
       
        try {
            String query = "select distinct ministry from TblContractAwardedOffline";
            listMinister = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listMinister.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getMinistryList + logUserId + ex);
        }
        LOGGER.debug(getMinistryList + logUserId + loggerEnd);
            return listMinister;
        }
    
        @Override
        public List<Object[]> getMinistryForAwardedContractOfflineHome() {
        LOGGER.debug(getMinistryList + logUserId + loggerStart);
        List<Object[]> listMinister = new ArrayList<Object[]>();

        try {
            String query = "select distinct ministry from TblContractAwardedOffline where Status = 'Approved' ";
            listMinister = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listMinister.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getMinistryList + logUserId + ex);
        }
        LOGGER.debug(getMinistryList + logUserId + loggerEnd);
            return listMinister;
        }

    @Override
    public List<Object[]> getOrgForAwardedContractOffline(String ministry)
    {
    LOGGER.debug(getOrgList + logUserId + loggerStart);
        List<Object[]> listOrg = new ArrayList<Object[]>();

        try {
            String query = "select distinct agency from TblContractAwardedOffline where ministry = '"+ ministry +"'";
            listOrg = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listOrg.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getOrgList + logUserId + ex);
        }
        LOGGER.debug(getOrgList + logUserId + loggerEnd);
            return listOrg;
    }
    
    @Override
    public List<Object[]> getOrgForAwardedContractOfflineHome(String ministry)
    {
    LOGGER.debug(getOrgList + logUserId + loggerStart);
        List<Object[]> listOrg = new ArrayList<Object[]>();

        try {
            String query = "select distinct agency from TblContractAwardedOffline where ministry = '"+ ministry +"' and Status = 'Approved'";
            listOrg = getHibernateQueryDao().createQuery(query);
            System.out.print("Size : "+listOrg.size());
           /* if (obj != null) {
                if (obj.size() > 0) {
                    stdId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }*/
        } catch (Exception ex) {
            LOGGER.debug(getOrgList + logUserId + ex);
        }
        LOGGER.debug(getOrgList + logUserId + loggerEnd);
            return listOrg;
    }

    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
}
