/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface OfflineDataService {

     /**
     * Set user Id var.
     * @param logUserId
     */

    public void setLogUserId(String logUserId);

    /**
     * save Offline data rec
     */
    public void saveTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);

    /**
     * delete Offline data rec
     */
    public void deleteTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);
    /**
     * update Offline data rec
     */
    public void updateTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);

    /**
     * get the count for TblTenderDetailsOffline table
     */
    public long getCountForTblTenderDetailOfflineData();

    /**
     * get the total offline Data recs
     * @return TblTenderDetailsOffline
     */
    public List<TblTenderDetailsOffline> getAllTblTenderDetailOfflineData();

    /**
     * get TblTenderDetailsOffline by Id
     * @return list of TblTenderDetailsOffline
     */
    public List<TblTenderDetailsOffline> getTblTenderDetailOfflineDataById(int tenderOLId);

    /*
     * save TblContractAwardedOfflinedata
     */
    public void saveTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline);

    /**
     * delete Offline data rec
     */
    public void deleteTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline);
    /**
     * update Offline data rec
     */
    public void updateTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline);
     /**
     * get TblTenderDetailsOffline by Id
     * @return list of TblTenderDetailsOffline
     */
    public List<TblContractAwardedOffline> getTblContractAwardedOfflineById(int tenderOLId);

     /**
     * get the count for TblContractAwardedOffline table
     */
    public long getCountForTblContractAwardedOfflineData();
    /**
     * get the total offline Data recs
     * @return TblContractAwardedOffline
     */
    public List<TblContractAwardedOffline> getAllTblContractAwardedOfflineData();
}
