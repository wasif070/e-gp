/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblWsMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface WsMasterService {
    public void setLogUserId(String logUserId);
    /**
     * Inserts Web Service
     * @param tblWsMaster to be inserted
     * @return Id or 0 for success or fail
     */
    public int insertWsMaster(TblWsMaster tblWsMaster);
    /**
     *Updates TblWsMaster
     * @param tblWsMaster to be updated
     * @return true or false for success or fail
     */
    public boolean updateWsMaster(TblWsMaster tblWsMaster,String...actions);
    /**
     *Deletes TblWsMaster
     * @param tblWsMaster to be delete
     * @return true or false for success or fail
     */
    public boolean deleteWsMaster(TblWsMaster tblWsMaster);
    /**
     *Get all TblWsMaster
     * @return List of TblWsMaster
     */
    public List<TblWsMaster> getAllWsMaster();
    /**
     *Get counts of all TblWsMaster
     * @return count of TblWsMaster
     */
    public long getWsMastersCount();
    /**
     *Get all TblWsMaster based on id
     * @param id search on
     * @return TblWsMaster
     */
    public TblWsMaster getTblWsMaster(int id);
    /**
     *Get all TblWsMaster based on wsName
     * @param wsName search on
     * @return TblWsMaster
     */
    public TblWsMaster getTblWsMaster(String wsName);
    /**
     *Get all TblWsMaster based on whereCondition
     * @param whereCondition search on
     * @return List of TblWsMaster
     */
    public List<TblWsMaster> getRequiredTblWsMasterList(String whereCondition);

    public void setAuditTrail(AuditTrail auditTrail);
}
