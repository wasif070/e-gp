/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBidModificationDao;
import com.cptu.egp.eps.dao.daointerface.TblFinalSubDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblFinalSubmissionDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderListCellDetailDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonFormData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPInsertMultiTablesXML;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderFormDtl;
import com.cptu.egp.eps.model.table.TblBidModification;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.egp.eps.model.table.TblFinalSubDetail;
import com.cptu.egp.eps.model.table.TblFinalSubmission;
import com.cptu.egp.eps.model.table.TblTenderListBox;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class TenderBidServiceImpl implements TenderBidService {

    SPTenderFormDtl spTenderFormDtl;
    SPInsertMultiTablesXML spInsertMultiTablesXML;
    TblBidModificationDao tblBidModificationDao;
    HibernateQueryDao hibernateQueryDao;
    SPXMLCommon spXMLCommon;
    TblFinalSubmissionDao tblFinalSubmissionDao;
    TblTenderListCellDetailDao tblTenderListCellDetailDao;
    TblFinalSubDetailDao tblFinalSubDetailDao;
    SPTenderCommon sPTenderCommon;
    
    private String logUserId = "0";
    static final Logger logger = Logger.getLogger(SubContractingServiceImpl.class);

    public void setTblFinalSubDetailDao(TblFinalSubDetailDao tblFinalSubDetailDao) {
        this.tblFinalSubDetailDao = tblFinalSubDetailDao;
    }

    public void setTblFinalSubmissionDao(TblFinalSubmissionDao tblFinalSubmissionDao) {
        this.tblFinalSubmissionDao = tblFinalSubmissionDao;
    }

    public TblFinalSubDetailDao getTblFinalSubDetailDao() {
        return tblFinalSubDetailDao;
    }

    public TblFinalSubmissionDao getTblFinalSubmissionDao() {
        return tblFinalSubmissionDao;
    }

    public void setSpXMLCommon(SPXMLCommon spXMLCommon) {
        this.spXMLCommon = spXMLCommon;
    }

    public SPXMLCommon getSpXMLCommon() {
        return spXMLCommon;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblBidModificationDao getTblBidModificationDao() {
        return tblBidModificationDao;
    }

    public void setTblBidModificationDao(TblBidModificationDao tblBidModificationDao) {
        this.tblBidModificationDao = tblBidModificationDao;
    }

    public SPTenderFormDtl getSpTenderFormDtl() {
        return spTenderFormDtl;
    }

    public void setSpTenderFormDtl(SPTenderFormDtl spTenderFormDtl) {
        this.spTenderFormDtl = spTenderFormDtl;
    }

    public SPInsertMultiTablesXML getSpInsertMultiTablesXML() {
        return spInsertMultiTablesXML;
    }

    public void setSpInsertMultiTablesXML(SPInsertMultiTablesXML spInsertMultiTablesXML) {
        this.spInsertMultiTablesXML = spInsertMultiTablesXML;
    }

    public TblTenderListCellDetailDao getTblTenderListCellDetailDao() {
        return tblTenderListCellDetailDao;
    }

    public void setTblTenderListCellDetailDao(TblTenderListCellDetailDao tblTenderListCellDetailDao) {
        this.tblTenderListCellDetailDao = tblTenderListCellDetailDao;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }
    
    @Override
    public List<CommonFormData> getTenderFormDetails(int tableId, int formId, int columnId, int userId, int bidId, int tenderId, String funName) {
        return spTenderFormDtl.executeProcedure(tableId, formId, columnId, userId, bidId, tenderId, funName);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<CommonFormData> getTenderColumnDetails(int tableId, int formId, int columnId, int userId, int bidId, int tenderId, String funName) {
        return spTenderFormDtl.executeProcedure(tableId, formId, columnId, userId, bidId, tenderId, funName);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public boolean insertFormDetails(String action, String tableName1, String tableName2, String tableName3, String tableName4, String xmlStr1, String xmlStr2, String xmlStr3, String xmlStr4, String table1PK, String table2PK, String table3PK, String condition) {
        return spInsertMultiTablesXML.executeProcedure(action, tableName1, tableName2, tableName3, tableName4, xmlStr1, xmlStr2, xmlStr3, xmlStr4, table1PK, table2PK, table3PK, condition).getFlag();
    }

    @Override
    public void updateFinalSubStatus(int tenderid, int userid, String reason, boolean isModify, String ipAddr) {
        logger.debug("updateFinalSubStatus : " + logUserId + "starts");
        String status = null;
        if (isModify) {
            status = "modify";
        } else {
            status = "withdrawal";
        }
        hibernateQueryDao.updateDeleteNewQuery("update TblFinalSubmission set bidSubStatus='" + status + "' where tblTenderMaster.tenderId=" + tenderid + " and userId=" + userid);
        tblBidModificationDao.addTblBidModification(new TblBidModification(0, status, reason, new Date(), tenderid, userid, ipAddr));
    }

    @Override
    public String[] getTendererMobileNEmail(String userId) {
        logger.debug("getTendererMobileNEmail : " + logUserId + "starts");
        String[] data = new String[3];
        try {
            Object[] objects = hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId=" + userId).get(0);

        for (int i = 0; i < data.length; i++) {
                data[i] = (String) objects[i];
        }
        } catch (Exception e) {
            logger.error("getTendererMobileNEmail : " + e);
        }
        logger.debug("getTendererMobileNEmail : " + logUserId + "ends");
        return data;
    }

    @Override
    public List<TblBidModification> findBidModifyHistory(String userId, String tenderid) throws Exception {
        return tblBidModificationDao.findTblBidModification("userId", Operation_enum.EQ, Integer.parseInt(userId), "tenderId", Operation_enum.EQ, Integer.parseInt(tenderid), "bidStatus", Operation_enum.EQ, "modify");
    }

    @Override
    public boolean deleteBidFormDetails(int formId, int userId, int bidId) {
        logger.debug("deleteBidFormDetails : " + logUserId + "starts");
        boolean flag = true;
        try {
            hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderBidForm tbf where tbf.tblTenderForms.tenderFormId = " + formId + " and tbf.userId=" + userId + " and tbf.bidId = " + bidId);
            hibernateQueryDao.updateDeleteNewQuery("delete from TblFinalSubDetail tfd where tfd.tenderFormid = " + formId + " and tfd.bidId = " + bidId);
        } catch (Exception e) {
            logger.error("deleteBidFormDetails : " + e);
            flag = false;
        }
        logger.debug("deleteBidFormDetails : " + logUserId + "ends");
        return flag;
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getBuyerPwdHash(int tenderId) {
        logger.debug("getBuyerPwdHash : " + logUserId + "starts");
        String getBuyerPwdHash = "";
        try {
            getBuyerPwdHash = (String) hibernateQueryDao.getSingleColQuery("select th.tenderHash from TblTenderHash th where th.tenderId = " + tenderId).get(0);
        } catch (Exception e) {
            logger.error("getBuyerPwdHash : " + e);
    }
        logger.debug("getBuyerPwdHash : " + logUserId + "ends");
        return getBuyerPwdHash;
    }

    @Override
    public boolean insertBidEncrypt(String tableName, String xmlBidEncrypt, String whereCond, boolean isInEditMode) {
        logger.debug("insertBidEncrypt : " + logUserId + "starts");
        boolean boolRet = false;
        List<CommonMsgChk> insertDtl = null;
        try {
            if (isInEditMode) {
                boolRet = false;
            } else {
                insertDtl = spXMLCommon.executeProcedure("insert", tableName, xmlBidEncrypt, whereCond);
            }
            boolRet = insertDtl.get(0).getFlag();
            logger.debug("insertBidEncrypt : MESSAGE WHILE EXECUTING STORED PROCEDURE OF COLUMNS AND CELLS ----->" + insertDtl.get(0).getMsg());
        } catch (Exception ex) {
            logger.error("insertBidEncrypt : " + ex);
        }
        logger.debug("insertBidEncrypt : " + logUserId + "ends");
        return boolRet;
    }

    @Override
    public int insertIntoFS(TblFinalSubmission tblFS) {
        logger.debug("insertIntoFS : " + logUserId + "starts");
        int fsId = -1;
        try {
            tblFinalSubmissionDao.addTblFinalSubmission(tblFS);
            fsId = tblFS.getFinalSubmissionId();
        } catch (Exception ex) {
            logger.error("insertIntoFS : " + ex);
        }
        logger.debug("insertIntoFS : " + logUserId + "ends");
        return fsId;
    }

    @Override
    public boolean insertIntoFSDtl(TblFinalSubDetail tblFSDtl) {
        logger.debug("insertIntoFSDtl : " + logUserId + "starts");
        boolean boolAddFSDtl = false;
        try {
            tblFinalSubDetailDao.addTblFinalSubDetail(tblFSDtl);
            boolAddFSDtl = true;
        } catch (Exception ex) {
            logger.error("insertIntoFSDtl : " + ex);
        }
        logger.debug("insertIntoFSDtl : " + logUserId + "ends");
        return boolAddFSDtl;
    }

    @Override
    public int getFinalSubId(int tenderId, int userId) {
        logger.debug("getFinalSubId : " + logUserId + "starts");
        List<Object> obj;
        int fsId = -1;
        try {
            obj = hibernateQueryDao.getSingleColQuery("select tfs.finalSubmissionId from TblFinalSubmission tfs where tfs.tblTenderMaster.tenderId = " + tenderId + " and tfs.userId = " + userId);
            if (obj != null) {
                if (obj.size() > 0) {
                    fsId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }

        } catch (Exception ex) {
            logger.error("getFinalSubId : " + ex);

        }
        logger.debug("getFinalSubId : " + logUserId + "ends");
            return fsId;
        }

    //fetch buidder ID
    @Override
    public List<Object> getBidId(int userId, int tenderFormId) {
        return hibernateQueryDao.singleColQuery("select ttbf.bidId from TblTenderBidForm ttbf where ttbf.userId=" + userId + " and ttbf.tblTenderForms.tenderFormId=" + tenderFormId);
    }

    @Override
    public List<TblTenderListCellDetail> getTenderListCellDetail(int formId, int tableId, short columnId, int cellId){

        logger.debug("getTenderListCellDetail : " + logUserId + " Starts");
        List<Object> listCellDetail = new ArrayList<Object>();
        List<TblTenderListCellDetail> listtendListCellDetails = new ArrayList<TblTenderListCellDetail>();
        
        try {
            
            String Query = "select tl.tenderListId "
                            + "from TblTenderListBox tl,TblTenderListCellDetail tcl "
                            + "where tl.tenderListId = tcl.tblTenderListBox.tenderListId and "
                            + "tl.tenderFormId = "+formId+" and tcl.tblTenderTables.tenderTableId = "+ tableId +" and tcl.columnId = "+columnId+" and tcl.cellId = "+cellId;
            
            listCellDetail = hibernateQueryDao.singleColQuery(Query);
            
            if(!listCellDetail.isEmpty()){
                TblTenderListCellDetail tblTenderListCellDetail = new TblTenderListCellDetail();
                tblTenderListCellDetail.setTblTenderListBox(new TblTenderListBox(Integer.parseInt(listCellDetail.get(0).toString())));
                listtendListCellDetails.add(tblTenderListCellDetail);
            }
            
            //listCellDetail = tblTenderListCellDetailDao.findTblTenderListCellDetail("tblTenderTables", Operation_enum.EQ, new TblTenderTables(tableId), "columnId", Operation_enum.EQ, columnId, "cellId", Operation_enum.EQ, cellId);
        } catch (Exception ex) {
            logger.error("getTenderListCellDetail : " + logUserId + ex);
        }
        logger.debug("getTenderListCellDetail : " + logUserId + " Ends");
        return listtendListCellDetails;
    }
    
    @Override
    public void setUserId(String userId) {
        this.logUserId = userId;
    }

    @Override
    public Object[] finalSubTenderDetails(String tenderId, String userId) {
        StringBuilder query = new StringBuilder();
        query.append("select tom.officeName,ttd.packageNo,ttd.packageDescription,tfs.ipAddress,tfs.tenderHash,tfs.finalSubmissionDt,ttd.reoiRfpRefNo ");
        query.append("from TblTenderDetails ttd ,TblOfficeMaster tom,TblFinalSubmission tfs ");
        query.append("where ttd.tblTenderMaster.tenderId="+tenderId+" and ttd.officeId = tom.officeId and tfs.tblTenderMaster.tenderId = ttd.tblTenderMaster.tenderId ");
        query.append("and tfs.userId="+userId);
        List<Object[]> list = hibernateQueryDao.createNewQuery(query.toString());
        Object[] data=null;
        if(!list.isEmpty()){
            data = list.get(0);
        }
        return data;
    }

    @Override
    public List<Object[]> finalSubLotDetails(String tenderId,String userId){
        StringBuilder query = new StringBuilder();
        query.append("select tls.lotNo,tls.lotDesc from TblTenderLotSecurity tls ");
        query.append("where  tls.tblTenderMaster.tenderId = "+tenderId+" and tls.appPkgLotId in (select tbl.pkgLotId from TblBidderLots tbl where tbl.userId="+userId+" and tbl.tenderId="+tenderId+")");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    
    @Override
    public int getCellForGrandTotalCaption(int tenderFormId, int tenderTableId){
        logger.debug("getCellForGrandTotalCaption : " + logUserId + " Starts");
        List<SPTenderCommonData> list = new ArrayList<SPTenderCommonData>();
        StringBuilder query = new StringBuilder();
        int cellId = -1;
        
        try{
            list = sPTenderCommon.executeProcedure("getCellForGrandTotalCaption", ""+tenderFormId, ""+tenderTableId);
            if(!list.isEmpty()){
                cellId = Integer.parseInt(list.get(0).getFieldName2());
            }
        }
        catch(Exception ex){
            logger.error("getCellForGrandTotalCaption : " + logUserId + ex);
        }
        logger.debug("getCellForGrandTotalCaption : " + logUserId + " Ends");
        return cellId;
    }

    @Override
    public long checkBidFormSubmited(int tenderId, int formId, int userId) {
        logger.debug("checkBidFormSubmited : " + logUserId + " Starts");
        long result=0;

        try {
            result= hibernateQueryDao.countForNewQuery("TblTenderBidForm t", "t.tblTenderMaster.tenderId="+tenderId+" and t.tblTenderForms.tenderFormId="+formId+" and t.userId="+userId);
        } catch (Exception ex) {
            logger.error("checkBidFormSubmited : " + logUserId + ex);
        }
        logger.debug("checkBidFormSubmited : " + logUserId + " Ends");
        return result;
    }

     @Override
    public long checkBidFormEncriptedAndSaved(int tenderId, int formId, int userId) {
        logger.debug("checkBidFormSubmited : " + logUserId + " Starts");
        long result=0;

        try {
            result= hibernateQueryDao.countForNewQuery("TblTenderBidEncrypt a,TblTenderBidTable b, TblTenderBidForm c", "a.bidTableId=b.bidTableId and b.tblTenderBidForm.bidId=c.bidId and c.tblTenderMaster.tenderId="+tenderId+" and c.tblTenderForms.tenderFormId="+formId+" and c.userId="+userId);
        } catch (Exception ex) {
            logger.error("checkBidFormSubmited : " + logUserId + ex);
        }
        logger.debug("checkBidFormSubmited : " + logUserId + " Ends");
        return result;
    }
}
