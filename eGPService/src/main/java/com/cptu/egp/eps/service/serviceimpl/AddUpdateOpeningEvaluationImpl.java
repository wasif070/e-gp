/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUpdOpeningEvaluation;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
/**
 *
 * @author rokibul
 */
public class AddUpdateOpeningEvaluationImpl implements AddUpdateOpeningEvaluation{
    static final Logger logger = Logger.getLogger(CommonSearchDataMoreImpl.class);
    private SPAddUpdOpeningEvaluation spAddUpdOpeningEvaluation;
    private String logUserId="0";
    
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<CommonMsgChk> addUpdOpeningEvaluation(String ... fieldValues) {
        logger.debug("addUpdOpeningEvaluation : "+logUserId+" Starts");
        List<CommonMsgChk> list = null;
        String[] fieldValue = new String[30];
        if(fieldValues!=null && fieldValues.length<=30){
                System.arraycopy(fieldValues, 0, fieldValue, 0, fieldValues.length);
        }
        try {
            list = getSpAddUpdOpeningEvaluation().executeProcedure(fieldValue[0],fieldValue[1], fieldValue[2], fieldValue[3], fieldValue[4], fieldValue[5], fieldValue[6], fieldValue[7], fieldValue[8], fieldValue[9], fieldValue[10], fieldValue[11], fieldValue[12], fieldValue[13], fieldValue[14], fieldValue[15], fieldValue[16], fieldValue[17], fieldValue[18], fieldValue[19], fieldValue[20], fieldValue[21], fieldValue[22], fieldValue[23], fieldValue[24], fieldValue[25], fieldValue[26], fieldValue[27], fieldValue[28] , fieldValue[29]);
        } catch (Exception e) {
            logger.error("addUpdOpeningEvaluation : "+logUserId+" "+e);
    }
        //fieldValue = null;
        logger.debug("addUpdOpeningEvaluation : "+logUserId+" Ends");
        return list;
    }

    /**
     * @return the spAddUpdOpeningEvaluation
     */
    public SPAddUpdOpeningEvaluation getSpAddUpdOpeningEvaluation() {
        return spAddUpdOpeningEvaluation;
    }

    /**
     * @param spAddUpdOpeningEvaluation the spAddUpdOpeningEvaluation to set
     */
    public void setSpAddUpdOpeningEvaluation(SPAddUpdOpeningEvaluation spAddUpdOpeningEvaluation) {
        this.spAddUpdOpeningEvaluation = spAddUpdOpeningEvaluation;
    }

}
