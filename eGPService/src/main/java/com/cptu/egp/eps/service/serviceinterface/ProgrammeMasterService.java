/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Naresh.Akurathi
 */
public interface ProgrammeMasterService {

    /**
     *
     * Method for checking ProgrammeMaster code for validation.
     * @param pCode
     * @return 
     */
    public String checkForCode(String pCode);

    /**
     * Method for checking ProgrammeMaster code for validation.
     * @param pName
     * @return
     */
    public String checkForName(String pName);

    /**
     * Method for checking ProgrammeMaster owner for validation.
     * @param pOwner
     * @return
     */
    public String checkForOwner(String pOwner);    

    /**
     * Method for check Program Code already Exist or not at Edit Time.
     * @param pId
     * @param pCode
     * @return
     */
    public String editCheckCode(int pId, String pCode);

    /**
     * Method for check Program Name already exist or not at Edit Time
     * @param pId
     * @param pName
     * @return
     */
    public String editCheckName(int pId, String pName);

    /**
     * Method for check Program Ownder already exist or not at Edit Time
     * @param pId
     * @param pOwner
     * @return
     */
    public String editCheckOwner(int pId, String pOwner);

    /**
     *  Method for adding the details of ProgrammeMaster.
     * @param tblProgrammeMaster
     * @return
     */
    public String addProgrammeMasterDetails(TblProgrammeMaster tblProgrammeMaster);

    /**
     * Method for updating ProgrammeMater details.
     * @param tblProgrammeMaster
     * @return
     */
    public String updateProgrammeMasterDetails(TblProgrammeMaster tblProgrammeMaster);

    /**
     * Method for get the details of ProgrammeMaster.
     * @param sord = Sort Order ('asc'/'dec') or Null
     * @param sidx = field name for order.
     * @return = List of TblProgrammeMaster object
     */
    public List<TblProgrammeMaster> getProgrammeMasterDetails(String sord,String sidx);

    /**
     * Method for get particular ProgrammeMaster detail.
     * @param id = progId
     * @return = List of TblProgrammeMaster object
     */
    public List<TblProgrammeMaster> getData(int id);

    /**
     * Get ProgrammeMaster Detail base on search criteria.
     * @param columnName
     * @param operator = can be 'EQ' = Equals/'CN' = like
     * @param searchString 
     * @return = List of TblProgrammeMaster object
     */
    public List<TblProgrammeMaster> getProgrammeMasterBySearch(String columnName,String operator,String searchString);

    /**
     * Set UserId for logging purpose.
     * @param logUserId
     */
    public void setUserId(String logUserId);

    /**
     * Set AuditTrail data for perform Audit Trail Entry
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail);

}
