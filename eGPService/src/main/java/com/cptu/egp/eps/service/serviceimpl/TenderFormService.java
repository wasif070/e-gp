/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderFormsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderSectionDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.model.table.TblTenderStd;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.storedprocedure.CommonFormData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPCreateFixedRateBOQ;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderFormDtl;
import com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore;

/**
 *
 * @author yagnesh
 */
public class TenderFormService {

    HibernateQueryDao hibernateQueryDao;
    TblTenderFormsDao tblTenderFormsDao;
    TblTenderLotSecurityDao tblTenderLotSecurityDao;
    TblTenderSectionDao tblTenderSectionDao;
    MakeAuditTrailService makeAuditTrailService;
    AuditTrail auditTrail;
    private String logUserId = "0";
    SPTenderFormDtl spTenderFormDtl;
    SpGeteGPCmsDataMore spGeteGPCmsDataMore;
    SPCreateFixedRateBOQ sPCreateFixedRateBOQ;

    public SPCreateFixedRateBOQ getsPCreateFixedRateBOQ() {
        return sPCreateFixedRateBOQ;
    }

    public void setsPCreateFixedRateBOQ(SPCreateFixedRateBOQ sPCreateFixedRateBOQ) {
        this.sPCreateFixedRateBOQ = sPCreateFixedRateBOQ;
    }

    public SpGeteGPCmsDataMore getSpGeteGPCmsDataMore() {
        return spGeteGPCmsDataMore;
    }

    public void setSpGeteGPCmsDataMore(SpGeteGPCmsDataMore spGeteGPCmsDataMore) {
        this.spGeteGPCmsDataMore = spGeteGPCmsDataMore;
    }

    public SPTenderFormDtl getSpTenderFormDtl() {
        return spTenderFormDtl;
    }

    public void setSpTenderFormDtl(SPTenderFormDtl spTenderFormDtl) {
        this.spTenderFormDtl = spTenderFormDtl;
    }


    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    final Logger logger = Logger.getLogger(TenderFormService.class);

    public TblTenderLotSecurityDao getTblTenderLotSecurityDao() {
        return tblTenderLotSecurityDao;
    }

    public void setTblTenderLotSecurityDao(TblTenderLotSecurityDao tblTenderLotSecurityDao) {
        this.tblTenderLotSecurityDao = tblTenderLotSecurityDao;
    }
    
    public TblTenderSectionDao getTblTenderSectionDao() {
        return tblTenderSectionDao;
    }

    public void setTblTenderSectionDao(TblTenderSectionDao tblTenderSectionDao) {
        this.tblTenderSectionDao = tblTenderSectionDao;
    }

    public void setTblTenderFormsDao(TblTenderFormsDao tblTenderFormsDao) {
        this.tblTenderFormsDao = tblTenderFormsDao;
    }

    public TblTenderFormsDao getTblTenderFormsDao() {
        return tblTenderFormsDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * Add Tender Form
     * @param tblForm
     * @param tenderId
     * @return true if insertion done successfully else return false
     */
    public boolean addForm(TblTenderForms tblForm, int tenderId) {
        logger.debug("addForm : " + logUserId + " Starts");
        boolean boolAddForm = false;
        String auditAction = "";
        int auditappId = tenderId;
        try {
            tblTenderFormsDao.addTblTenderForms(tblForm);
            boolAddForm = true;
            auditAction = "Create Form";
        } catch (Exception ex) {
            logger.error("addForm : " + logUserId + " Starts" + ex);
            auditAction = "Error in Create Form ";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, auditappId, "tenderId", EgpModule.Tender_Document.getName(), auditAction, tblForm.getFormName());
            auditAction = null;
        }
        logger.debug("addForm : " + logUserId + " Ends");
        return boolAddForm;
    }

    /**
     * Edit Tender Form
     * @param tblForm
     * @param tenderId
     * @return true if insertion done successfully else return false
     */
    public boolean editForm(TblTenderForms tblForm, int tenderId) {
        logger.debug("editForm : " + logUserId + " Starts");
        boolean boolEditForm = false;
        String auditAction = "";
        int auditappId = tenderId;

        try {
            tblTenderFormsDao.updateTblTenderForms(tblForm);
            boolEditForm = true;
            auditAction = "Edit Form";
        } catch (Exception ex) {
            logger.debug("editForm : " + logUserId + " :" + ex);
            auditAction = "Error in Edit Form ";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, auditappId, "tenderId", EgpModule.Tender_Document.getName(), auditAction, tblForm.getFormName());
            auditAction = null;
        }
        logger.debug("editForm : " + logUserId + " Ends");
        return boolEditForm;
    }

    /**
     * Get List of Tender Form details
     * @param tenderSectionId
     * @return Tender from Detail List
     */
    public List<TblTenderForms> getForms(int tenderSectionId) {
        logger.debug("getForms : " + logUserId + " Starts");
        List<TblTenderForms> lstForms;
        try {
            lstForms = tblTenderFormsDao.findTblTenderForms("tblTenderSection", Operation_enum.EQ, new TblTenderSection(tenderSectionId),
                    "tenderFormId", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getForms : " + logUserId + " :" + ex);
            lstForms = null;
        }
        logger.debug("getForms : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Get Technical form details
     * @param tenderSectionId
     * @return Technical Form detail list
     */
    public List<TblTenderForms> getFormsPackage(int tenderSectionId) {
        logger.debug("getFormsPackage : " + logUserId + " Starts");
        List<TblTenderForms> lstForms;
        try {
            lstForms = tblTenderFormsDao.findTblTenderForms("tblTenderSection", Operation_enum.EQ, new TblTenderSection(tenderSectionId), "isPriceBid", Operation_enum.EQ, "no",
                    "tenderFormId", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getFormsPackage : " + logUserId + " Starts" + ex);
            lstForms = null;
        }
        logger.debug("getFormsPackage : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Get Form Price bid from details.
     * @param tenderSectionId
     * @param appPkgLotId
     * @return  Tender from Detail List
     */
    public List<TblTenderForms> getFormsLot(int tenderSectionId, int appPkgLotId) {
        List<TblTenderForms> lstForms;
        logger.debug("getFormsLot : " + logUserId + " Starts");
        try {
            lstForms = tblTenderFormsDao.findTblTenderForms("tblTenderSection", Operation_enum.EQ, new TblTenderSection(tenderSectionId), "isPriceBid", Operation_enum.EQ, "yes",
                    "pkgLotId", Operation_enum.EQ, appPkgLotId, "tenderFormId", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getFormsLot : " + logUserId + " Starts" + ex);
            lstForms = null;
        }
        logger.debug("getFormsLot : " + logUserId + " Starts");
        return lstForms;
    }

    /**
     * Get Tender lot details
     * @param tenderId
     * @return Tender lot detail list
     */
    public List<TblTenderLotSecurity> getLotDetails(int tenderId) {
        logger.debug("getLotDetails : " + logUserId + " Starts");
        List<TblTenderLotSecurity> lstForms;
        try {
            lstForms = tblTenderLotSecurityDao.findTblTenderLotSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(tenderId));
        } catch (Exception ex) {
            logger.error("getLotDetails : " + logUserId + " :" + ex);
            lstForms = null;
        }
        logger.debug("getLotDetails : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Get specific form detail
     * @param tFormId
     * @return form detail
     */
    public List<TblTenderForms> getSingleForms(int tFormId) {
        logger.debug("getSingleForms : " + logUserId + " Starts");
        List<TblTenderForms> lstForms;
        try {
            lstForms = tblTenderFormsDao.findTblTenderForms("tenderFormId", Operation_enum.EQ, tFormId);
        } catch (Exception ex) {
            logger.error("getSingleForms : " + logUserId + " :" + ex);
            lstForms = null;
        }
        logger.debug("getSingleForms : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Delete tender form
     * @param formId
     * @return true if deletion perform successfully else return false.
     */
    public boolean deleteForm(int formId) {
        logger.debug("deleteForm : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "delete from TblTenderForms ttf where ttf.tenderFormId =" + formId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteForm : " + logUserId + " Ends" + ex);
        }
        logger.debug("deleteForm : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Check form is BOQ type or not
     * @param formId
     * @return true if form is boq form else false.
     */
    public boolean isBOQForm(int formId) {
        logger.debug("isBOQForm : " + logUserId + " Starts");
        List<Object> obj;
        boolean isBOQForm = false;
        try {
            String query = "select ttf.isPriceBid from TblTenderForms ttf where ttf.tenderFormId =" + formId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null && obj.size() > 0) {
                if (obj.get(0).toString().equals("yes")) {
                    isBOQForm = true;
                }
            }
        } catch (Exception ex) {
            logger.error("isBOQForm : " + logUserId + " :" + ex);

        }
        logger.debug("isBOQForm : " + logUserId + " Ends");
        return isBOQForm;
    }

    /**
     * Get Price bid form details for grand summary creation
     * @param tenderSectionId
     * @return list of Price bid from detail
     */
    public List<TblTenderForms> getPriceBidForms(int tenderSectionId) {
        logger.debug("isBOQForm : " + logUserId + " Starts");
        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderForms> lstForms = new ArrayList<TblTenderForms>();
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttf.tenderFormId, ttf.formName,ttf.formStatus from TblTenderForms ttf where ttf.tblTenderSection.tenderSectionId = " + tenderSectionId + " ");
        strQuery.append(" and ttf.isPriceBid = 'yes' and ttf.tenderFormId in ");
        strQuery.append(" (select distinct (tf.tblTenderForms.tenderFormId) from TblTenderFormula tf");
        strQuery.append(" where tf.formula like '%TOTAL%' ");
        strQuery.append(" and tf.tblTenderForms.tenderFormId in (select t.tenderFormId from TblTenderForms t where t.tblTenderSection.tenderSectionId = " + tenderSectionId + ") ");
        strQuery.append(" ) order by ttf.tenderFormId asc ");
        try {
            /*lstForms = tblTenderFormsDao.findTblTenderForms("tblTenderSection", Operation_enum.EQ, new TblTenderSection(tenderSectionId),
            "isPriceBid", Operation_enum.EQ, "yes", "formName", Operation_enum.ORDERBY, Operation_enum.ASC);*/
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                String frmStatus=null;
                if(i[2] != null)
                {
                    frmStatus=i[2].toString();
                }

                lstForms.add(new TblTenderForms(Integer.parseInt(i[0].toString()), i[1].toString(),frmStatus));
            }
            if (strQuery != null) {
                strQuery = null;
            }
        } catch (Exception ex) {
            logger.error("isBOQForm : " + logUserId + " :" + ex);
            lstForms = null;
        }
        logger.debug("isBOQForm : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Get Price bid form details for grand summary creation
     * @param tenderSectionId
     * @param pckLotId
     * @return list of Price bid from detail
     */
    public List<TblTenderForms> getPriceBidForms(int tenderSectionId, int pckLotId)
    {
        logger.debug("getPriceBidForms : " + logUserId + " Starts");
        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderForms> lstForms = new ArrayList<TblTenderForms>();
        StringBuffer strQuery = new StringBuffer();
        strQuery.append(" select ttf.tenderFormId, ttf.formName,ttf.formStatus from TblTenderForms ttf where ttf.tblTenderSection.tenderSectionId = " + tenderSectionId + " and ttf.pkgLotId = " + pckLotId);
        strQuery.append(" and ttf.isPriceBid = 'yes' and ttf.tenderFormId in ");
        strQuery.append(" (select distinct (tf.tblTenderForms.tenderFormId) from TblTenderFormula tf");
        strQuery.append(" where tf.formula like '%TOTAL%' ");
        strQuery.append(" and tf.tblTenderForms.tenderFormId in (select t.tenderFormId from TblTenderForms t where t.tblTenderSection.tenderSectionId = " + tenderSectionId + "and ttf.pkgLotId = " + pckLotId + ") ");
        strQuery.append(" ) order by ttf.tenderFormId asc ");
        try {
            /*lstForms = tblTenderFormsDao.findTblTenderForms("tblTenderSection", Operation_enum.EQ, new TblTenderSection(tenderSectionId),
            "isPriceBid", Operation_enum.EQ, "yes", "formName", Operation_enum.ORDERBY, Operation_enum.ASC);*/
            objs = hibernateQueryDao.createNewQuery(strQuery.toString());
            for (Object[] i : objs) {
                String frmStatus=null;
                if(i[2] != null)
                {
                    frmStatus=i[2].toString();
                }

                lstForms.add(new TblTenderForms(Integer.parseInt(i[0].toString()), i[1].toString(),frmStatus));
            }
            if (strQuery != null) {
                strQuery = null;
            }
        } catch (Exception ex) {
            logger.error("getPriceBidForms : " + logUserId + " :" + ex);
            ex.printStackTrace();
            lstForms = null;
        }
        logger.debug("getPriceBidForms : " + logUserId + " Ends");
        return lstForms;
    }

    /**
     * Check if tender event type is RFP
     * @param tenderId
     * @return true if tender is RFP else return false
     */
    public boolean isTenderEvtTypeRFP(int tenderId) {
        logger.debug("isTenderEvtTypeRFP : " + logUserId + " Starts");
        List<Object> obj;
        boolean isRFP = false;
        try {
            String query = "select ttd.eventType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId =" + tenderId;
            obj = hibernateQueryDao.getSingleColQuery(query);
            if (obj != null && obj.size() > 0) {
                if (obj.get(0).toString().equals("RFP")) {
                    isRFP = true;
                }
            }
        } catch (Exception ex) {
            logger.error("isTenderEvtTypeRFP : " + logUserId + " :" + ex);
        }
        logger.debug("isTenderEvtTypeRFP : " + logUserId + " Ends");
        return isRFP;
    }

    /**
     * Cancel Tender Form
     * @param sectionId
     * @param formId
     * @return true if form canceled successfully else return false.
     */
    public boolean cancelTenderForm(int sectionId, int formId) {
        logger.debug("cancelTenderForm : " + logUserId + " Starts");
        boolean flag = false;
        try {
            String query = "update TblTenderForms set formStatus = 'cp' where tblTenderSection.tenderSectionId = " + sectionId + " and tenderFormId = " + formId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception ex) {
            logger.debug("cancelTenderForm : " + logUserId + " :" + ex);
        }
        logger.debug("cancelTenderForm : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Get Forms Details
     * @param tenderSectionId
     * @param isPriceBid
     * @param pkgLotId
     * @return Form Details in list of Object type
     */
    public List<Object[]> getForms(int tenderSectionId, String isPriceBid, int pkgLotId) {
        logger.debug("getForms : " + logUserId + " Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            StringBuilder query = new StringBuilder();
            query.append("select ttf.tenderFormId,ttf.formName,ttf.formStatus from TblTenderForms ttf");
            query.append(" where ttf.tblTenderSection.tenderSectionId=" + tenderSectionId);
            if (pkgLotId != 0) {
                query.append(" and ttf.pkgLotId=" + pkgLotId);
            }
            query.append(" and ttf.isPriceBid='" + isPriceBid + "' order by ttf.tenderFormId asc");
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception ex) {
            logger.debug("getForms : " + logUserId + " :" + ex);
        }
        return list;
    }

    public List<SPCommonSearchDataMore> AllowFormToDisplay(String tenderId) {
        logger.debug("AllowFormToDisplay : " + logUserId + "starts");
        List<SPCommonSearchDataMore> mores = null;
        String prId = "0";
        try {
            mores = spGeteGPCmsDataMore.executeProcedure("avoidepW2STD", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            System.out.println("AllowFormToDisplay : " + e + "starts");
        }
        logger.debug("AllowFormToDisplay : " + logUserId + "ends");
        return mores;
    }

    /**
     * Get Boq Form List.
     * @param pkgId
     * @param tenderId
     * @return List of BOQ forms
     */
    public List<Object[]> getAllBoQForms(int pkgId,int tenderId) {
        logger.debug("getAllBoQForms : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId,tf.formName,case when (templateFormId = 0 and FormType is not null) then cast(substring(FormType,32,len(FormType)) as int) else templateFormId end as templateFormId,tf.formType ");
            query.append(" from TblTenderForms tf,TblTenderStd ts, TblTenderSection tss ");
            query.append(" where ts.tenderStdId = tss.tblTenderStd.tenderStdId and tss.contentType = 'Form' and tf.pkgLotId=").append(pkgId).append( " and tf.isPriceBid='yes'");
            query.append(" and tf.tblTenderSection.tenderSectionId = tss.tenderSectionId and ts.tenderId =").append(tenderId).append(" and tf.tenderFormId not in ");
            query.append("(select wp.wpTenderFormId from TblCmsWptenderBoqmap wp) ");
            query.append(" and tf.tenderFormId in (select ttf.tblTenderForms.tenderFormId from TblTenderFormula ttf)");

            List<SPCommonSearchDataMore> mores = this.AllowFormToDisplay(tenderId+"");
            if(mores!= null && !mores.isEmpty()){
                String avoidFormId = mores.get(0).getFieldName1();
                query.append(" and (((tf.formStatus is null or tf.formStatus not in ('createp','c')) and tf.templateFormId > 0) or (tf.templateFormId = 0 and tf.formStatus is not null))");
                query.append(" and ((tf.templateFormId not in (").append(avoidFormId).append( ") and (tf.formStatus is null or tf.formStatus in ('p'))) or (cast(substring(FormType,32,len(FormType)) as int) != ").append(avoidFormId).append("))");
            } else {
                query.append(" and (tf.formStatus=null or tf.formStatus not in ('createp','c','BOQSalvage'))");
            }
            //System.out.println("Query "+query.toString());
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getAllBoQForms : " + e);
        }
        logger.debug("getAllBoQForms : " + logUserId + "ends");

        return list;
    }
    /**
     * Get Boq Form List.added for sbdcurrency
     * @param pkgId
     * @param tenderId
     * @param userId
     * @return List of BOQ forms
     */
    public List<Object[]> getAllBoQForms(int pkgId,int tenderId,int userId) {
        logger.debug("getAllBoQForms : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId,tf.formName,case when (templateFormId = 0 and FormType is not null) then cast(substring(FormType,32,len(FormType)) as int) else templateFormId end as templateFormId,tf.formType ");
            query.append(" from TblTenderForms tf,TblTenderStd ts, TblTenderSection tss ");
            query.append(" where ts.tenderStdId = tss.tblTenderStd.tenderStdId and tss.contentType = 'Form' and tf.pkgLotId=").append(pkgId).append( " and tf.isPriceBid='yes'");
            query.append(" and tf.tblTenderSection.tenderSectionId = tss.tenderSectionId and ts.tenderId =").append(tenderId).append(" and tf.tenderFormId not in ");
            query.append("(select wp.wpTenderFormId from TblCmsWptenderBoqmap wp) ");
            query.append(" and tf.tenderFormId in (select ttf.tblTenderForms.tenderFormId from TblTenderFormula ttf)");
            query.append(" and tf.tenderFormId in (select tbf.tblTenderForms.tenderFormId from TblTenderBidForm tbf where tbf.tblTenderMaster.tenderId =").append(tenderId).append( " and tbf.userId=").append(userId).append(")");
             
            List<SPCommonSearchDataMore> mores = this.AllowFormToDisplay(tenderId+"");
            if(mores!= null && !mores.isEmpty()){
                String avoidFormId = mores.get(0).getFieldName1();
                query.append(" and (((tf.formStatus is null or tf.formStatus not in ('createp','c')) and tf.templateFormId > 0) or (tf.templateFormId = 0 and tf.formStatus is not null))");
                query.append(" and ((tf.templateFormId not in (").append(avoidFormId).append( ") and (tf.formStatus is null or tf.formStatus in ('p'))) or (cast(substring(FormType,32,len(FormType)) as int) != ").append(avoidFormId).append("))");
            } else {
                query.append(" and (tf.formStatus=null or tf.formStatus not in ('createp','c','BOQSalvage'))");
            }
            //System.out.println("Query "+query.toString());
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getAllBoQForms : " + e);
        }
        logger.debug("getAllBoQForms : " + logUserId + "ends");

        return list;
    }

    /**
     * Get All BOQ forms for.
     * @param pkgId
     * @return form details in list of Object array.
     */
    public List<Object[]> getAllBoQFormsForRO(int pkgId) {
        logger.debug("getAllBoQFormsForRO : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId,tf.formName from TblTenderForms tf");
            query.append(" where tf.pkgLotId=" + pkgId + " and tf.isPriceBid='yes'");
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getAllBoQFormsForRO : " + e);
        }

        logger.debug("getAllBoQFormsForRO : " + logUserId + "ends");

        return list;

    }

    /**
     * Delete Configure Marks.
     * @param tenderFormId
     * @return true if operation perform successfully else return false.
     */
    public boolean deleteConfigMark(int tenderFormId){
        return hibernateQueryDao.updateDeleteNewQuery("delete from TblEvalServiceForms where tenderFormId = "+tenderFormId)!=0;
    }

    /**
     * Check Grand summary exist into system for given formid, table id, tender id.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return 0 if grand summary not exist else return non 0 values
     */
    public int CheckExistInGrandSum(int tableId, int formId, int tenderId, String funName)
    {
        int check=0;
        List<CommonFormData> lst= spTenderFormDtl.executeProcedure(tableId, formId, 0, 0, 0, tenderId, funName);
        if(lst != null && lst.size() > 0)
        {
            check = lst.get(0).getTableId();
        }
        return check;
    }

    /**
     * Check Grand summary exist into system for given formid, table id, tender id for given corrigendum.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return 0 if not exist else return 1
     */
    public int CheckExistInGrandSum(int tableId, int formId, int tenderId, String funName,int corriId)
    {
        int check=0;
        List<CommonFormData> lst= spTenderFormDtl.executeProcedure(tableId, formId, corriId, 0, 0, tenderId, funName);
        if(lst != null && lst.size() > 0)
        {
            check = lst.get(0).getTableId();
        }
        return check;
    }

    /**
     * delete Grand Summary.
     * @param tenderSumId
     * @return true if successfully deleted else return false
     */
    public boolean deleteGrandSum(int tenderSumId)
    {
        return hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderGrandSum where tenderSumId = "+tenderSumId)!=0;
    }

    /**
     * Delete Corrigendum grand sum details.
     * @param tenderSumId
     * @param corriId
     * @return true if successfully deleted else return false
     */
    public boolean deleteCorriGrandSumDetail(int tenderSumId,int corriId)
    {
        return hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderCoriGrandSumDetail where corrigendumId = "+corriId+" and tenderSumId="+tenderSumId)!=0;
    }

    /**
     * Check Grand summary Need to Edit at Corrigendum time?
     * @param tenderId
     * @param corriId
     * @param funName
     * @return
     */
    public String checkGrandSumNeedAtCorri( int tenderId,int corriId, String funName)
    {
        String msg="";
        List<CommonFormData> lst= spTenderFormDtl.executeProcedure(0, 0,  corriId,0, 0, tenderId, funName);
        if(lst != null && lst.size() > 0)
        {
            msg = lst.get(0).getFormula();
        }
        return msg;
    }

    /*
     * Returns list of all the forms cancelled in corrigendum in case of services
     * @param sectionId
     * @return list of array of object (templateFormId, Form Name)
     */
    public List<Object[]> getFormsCancelledInCorriForSrv(int sectionId) {
        logger.debug("getFormsCancelledInCorriForSrv : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("selecT tf.formName, tf.templateFormId, sbm.srvBoqType, sbm.srvBoqId ");
            query.append("From TblTenderForms tf, TblCmsTemplateSrvBoqDetail tsbd, TblCmsSrvBoqMaster sbm ");
            query.append("where tf.templateFormId = tsbd.tblTemplateSectionForm.formId and sbm.srvBoqId = tsbd.tblCmsSrvBoqMaster.srvBoqId  ");
            query.append("and tf.tblTenderSection.tenderSectionId = " + sectionId + " and tf.formStatus  in ('cp', 'c') and sbm.srvBoqId != 17 ");
            // Added 'c' in above line query and 'a' in below line query. For forms canceled in one corrigendum and now needs to create in other corrigendum. Eventum Id: 5914
            query.append("and tf.templateFormId not in ");
            query.append("(select ttf.templateFormId from TblTenderForms ttf where ttf.tblTenderSection.tenderSectionId = " + sectionId + " ");
            query.append("and ttf.formStatus in ('createp', 'a') and ttf.templateFormId  != 0) ");
            logger.error("getFormsCancelledInCorriForSrv : " + logUserId + " : " + query.toString());
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getFormsCancelledInCorriForSrv : " + logUserId + " : " + e);
        }
        logger.debug("getFormsCancelledInCorriForSrv : " + logUserId + "ends");
        return list;

    }

    /*
     * Reutrn type of form
     * @param formId
     * @return String
     */
    public String getTypeOfFormForService(int formId) {
        logger.debug("getTypeOfFormForService : " + logUserId + "starts");
        List<Object> list = new ArrayList<Object>();
        StringBuilder query = new StringBuilder();
        String strTypeOfForm = "";
        try {
            query.append("selecT sbm.srvBoqType from TblTenderForms tf, TblCmsTemplateSrvBoqDetail tsbd, TblCmsSrvBoqMaster sbm ");
            query.append("where tf.tenderFormId = " + formId + " and REPLACE(tf.templateFormId,'-','') = tsbd.tblTemplateSectionForm.formId ");
            query.append("and tsbd.tblCmsSrvBoqMaster.srvBoqId = sbm.srvBoqId ");
            logger.error("getTypeOfFormForService : " + logUserId + " : " + query.toString());
            list = hibernateQueryDao.getSingleColQuery(query.toString());
            if(list != null && !list.isEmpty()){
                for (Object objd : list) {
                    strTypeOfForm = objd.toString();
                }
            }
        } catch (Exception e) {
            logger.error("getTypeOfFormForService : " + logUserId + " : " + e);
        }
        logger.debug("getTypeOfFormForService : " + logUserId + "ends");
        return strTypeOfForm;

    }
/*  Reutrn validity of PriceBid Forms for ICT (Started by dohatec)
 *  @param pkgId
 *  @param formId
 *  @return String
 */
    public String getPriceBidFormsICT(int pkgId,int formId) {
        logger.debug("getFormsForValidate : " + logUserId + "starts");
        String Valid = "Invalid";
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId from TblTenderForms tf");
            query.append(" where tf.pkgLotId=" + pkgId + " and tf.isPriceBid='yes' ");
            query.append(" and tf.tenderFormId=" + formId + " ");
            list = hibernateQueryDao.createNewQuery(query.toString());
            if(!list.isEmpty() && list != null){
                Valid = "Valid";
            }
        } catch (Exception e) {
            logger.error("getFormsForValidate : " + e);
        }
        logger.debug("getFormsForValidate : " + logUserId + "ends");
        return Valid;
    }
    public boolean createFixedRateOrSalvageForm(String tenderId,String sectionId,String boqFixedOrSalvage){
         boolean isSuccess=false;
        try{
           List<SPTenderCommonData> list=sPCreateFixedRateBOQ.executeProcedure("createFixedOrSalvageBOQ", tenderId, sectionId, boqFixedOrSalvage);
            if(!list.isEmpty()){
               SPTenderCommonData data= list.get(0);
              if("1".equalsIgnoreCase(data.getFieldName1()))
                       isSuccess=true;
             }
           
        }catch(Exception e){
            e.printStackTrace();
           logger.error("TEnderFormService createFixedRateOrSalvageForm"+logUserId+" Error:"+e);
           isSuccess=false;
        }finally{
          makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "tenderId", EgpModule.Tender_Document.getName(), "Crete"+boqFixedOrSalvage, "form created :"+isSuccess);
        }

        return isSuccess;

    }
    /**
     * Get All BOQ forms for.
     * @param pkgId
     * @return form details in list of Object array.
     */
    public List<Object[]> getAllBoQFormsForNOA(int pkgId) {
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId,tf.formName from TblTenderForms tf");
            query.append(" where tf.pkgLotId=" + pkgId + " and tf.isPriceBid='yes'");
            query.append(" and tf.tenderFormId in (select ttf.tblTenderForms.tenderFormId from TblTenderFormula ttf)");
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getAllBoQFormsForNOA : " + e);
        }
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "ends");
        return list;
    }
    /**
     * Get All BOQ forms for NOA added for sbdcurrency.
     * @param pkgId
     * @return form details in list of Object array.
     */
    public List<Object[]> getAllBoQFormsForNOA(int pkgId,int tenderId, int userId) {
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tf.tenderFormId,tf.formName from TblTenderForms tf");
            query.append(" where tf.pkgLotId=" + pkgId + " and tf.isPriceBid='yes'");
            query.append(" and tf.tenderFormId in (select ttf.tblTenderForms.tenderFormId from TblTenderFormula ttf)");
            query.append(" and tf.tenderFormId in (select tbf.tblTenderForms.tenderFormId from TblTenderBidForm tbf where tbf.tblTenderMaster.tenderId =").append(tenderId).append( " and tbf.userId=").append(userId).append(")");
            list = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("getAllBoQFormsForNOA : " + e);
        }
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "ends");
        return list;
    }
    
    //Added by Emtaz on 29/march/2018
    public boolean IsThisGoodsLargeTender(int sectionId)
    {
        boolean flag = false;
        logger.debug("IsThisGoodsLargeTender : " + logUserId + "starts");
        List<Object[]> list = new ArrayList<Object[]>();
        StringBuilder query = new StringBuilder();
        try
        {
            query.append("select tts.tblTemplateMaster.templateId, tts.tenderId from TblTenderSection ttse, TblTenderStd tts");
            query.append(" where ttse.tblTenderStd.tenderStdId = tts.tenderStdId and ttse.tenderSectionId = "+sectionId);
            list = hibernateQueryDao.createNewQuery(query.toString());
            short templateId = (short)list.get(0)[0];
            if(templateId == 72)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
            flag = false;
        }
        
        return flag;
    }
}
