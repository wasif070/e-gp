/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBiddingPermissionDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmailVerificationCodeDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblMandatoryDocDao;
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyJointVentureDao;
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTempBiddingPermissionDao;
import com.cptu.egp.eps.dao.daointerface.TblTempTendererEsignatureDao;
import com.cptu.egp.eps.dao.daointerface.TblTempTendererMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererAuditTrailDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblUserPrefrenceDao;
import com.cptu.egp.eps.dao.daointerface.TblUserRegInfoDao;
import com.cptu.egp.eps.dao.daointerface.VwJvcaCompanySearchDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.AppCommonData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.dao.storedprocedure.SPCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPInsertCompMaster;
import com.cptu.egp.eps.dao.storedprocedure.SPInsertJvcaDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPPageNavigate;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTempCompanyJointVenture;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.egp.eps.model.table.TblTempTendererEsignature;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.egp.eps.model.table.TblUserRegInfo;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.model.view.VwJvcaCompanySearch;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.ObjDoubleConsumer;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author
 */
public class UserRegisterServiceImpl implements UserRegisterService {

    static final Logger logger = Logger.getLogger(UserRegisterServiceImpl.class);
    TblLoginMasterDao tblLoginMasterDao;
    TblTempTendererMasterDao tblTempTendererMasterDao;
    TblTempCompanyMasterDao tblTempCompanyMasterDao;
    TblUserPrefrenceDao tblUserPrefrenceDao;
    TblTempBiddingPermissionDao tblTempBiddingPermissionDao;
    TblBiddingPermissionDao tblBiddingPermissionDao;
    TblTempCompanyDocumentsDao tblTempCompanyDocumentsDao;
    TblCompanyDocumentsDao tblCompanyDocumentsDao;
    VwJvcaCompanySearchDao vwJvcaCompanySearchDao;
    SPInsertJvcaDetails sPInsertJvcaDetails;
    TblTempCompanyJointVentureDao tblTempCompanyJointVentureDao;
    HibernateQueryDao hibernateQueryDao;
    TblTempTendererEsignatureDao tblTempTendererEsignatureDao;
    TblCompanyMasterDao tblCompanyMasterDao;
    SPInsertCompMaster sPInsertCompMaster;
    SPPageNavigate sPPageNavigate;
    SPAddUser sPAddUser;
    AppCommonData appCommonData;
    SPCommon sPCommon;
    TblTendererMasterDao tblTendererMasterDao;
    TblMandatoryDocDao tblMandatoryDocDao;
    TblEmailVerificationCodeDao tblEmailVerificationCodeDao;
    TblTendererAuditTrailDao tblTendererAuditTrailDao;
    TblUserRegInfoDao tblUserRegInfoDao;
    private String userId = "0";
    private AuditTrail auditTrail;
    private static String cntQuery = "select tcm.countryName from TblCountryMaster tcm where tcm.countryId=";
    private static final String START = " Starts";
    private static final String END = " Ends";
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblTendererAuditTrailDao getTblTendererAuditTrailDao() {
        return tblTendererAuditTrailDao;
    }

    public void setTblTendererAuditTrailDao(TblTendererAuditTrailDao tblTendererAuditTrailDao) {
        this.tblTendererAuditTrailDao = tblTendererAuditTrailDao;
    }

    public TblEmailVerificationCodeDao getTblEmailVerificationCodeDao() {
        return tblEmailVerificationCodeDao;
    }

    public void setTblEmailVerificationCodeDao(TblEmailVerificationCodeDao tblEmailVerificationCodeDao) {
        this.tblEmailVerificationCodeDao = tblEmailVerificationCodeDao;
    }

    public TblMandatoryDocDao getTblMandatoryDocDao() {
        return tblMandatoryDocDao;
    }

    public void setTblMandatoryDocDao(TblMandatoryDocDao tblMandatoryDocDao) {
        this.tblMandatoryDocDao = tblMandatoryDocDao;
    }

    public TblTendererMasterDao getTblTendererMasterDao() {
        return tblTendererMasterDao;
    }

    public void setTblTendererMasterDao(TblTendererMasterDao tblTendererMasterDao) {
        this.tblTendererMasterDao = tblTendererMasterDao;
    }

    public SPCommon getsPCommon() {
        return sPCommon;
    }

    public void setsPCommon(SPCommon sPCommon) {
        this.sPCommon = sPCommon;
    }

    public AppCommonData getAppCommonData() {
        return appCommonData;
    }

    public void setAppCommonData(AppCommonData appCommonData) {
        this.appCommonData = appCommonData;
    }

    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }

    public SPPageNavigate getsPPageNavigate() {
        return sPPageNavigate;
    }

    public void setsPPageNavigate(SPPageNavigate sPPageNavigate) {
        this.sPPageNavigate = sPPageNavigate;
    }

    public SPInsertCompMaster getsPInsertCompMaster() {
        return sPInsertCompMaster;
    }

    public void setsPInsertCompMaster(SPInsertCompMaster sPInsertCompMaster) {
        this.sPInsertCompMaster = sPInsertCompMaster;
    }

    public TblCompanyMasterDao getTblCompanyMasterDao() {
        return tblCompanyMasterDao;
    }

    public void setTblCompanyMasterDao(TblCompanyMasterDao tblCompanyMasterDao) {
        this.tblCompanyMasterDao = tblCompanyMasterDao;
    }

    public TblTempTendererEsignatureDao getTblTempTendererEsignatureDao() {
        return tblTempTendererEsignatureDao;
    }

    public void setTblTempTendererEsignatureDao(TblTempTendererEsignatureDao tblTempTendererEsignatureDao) {
        this.tblTempTendererEsignatureDao = tblTempTendererEsignatureDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTempCompanyJointVentureDao getTblTempCompanyJointVentureDao() {
        return tblTempCompanyJointVentureDao;
    }

    public void setTblTempCompanyJointVentureDao(TblTempCompanyJointVentureDao tblTempCompanyJointVentureDao) {
        this.tblTempCompanyJointVentureDao = tblTempCompanyJointVentureDao;
    }

    public SPInsertJvcaDetails getsPInsertJvcaDetails() {
        return sPInsertJvcaDetails;
    }

    public void setsPInsertJvcaDetails(SPInsertJvcaDetails sPInsertJvcaDetails) {
        this.sPInsertJvcaDetails = sPInsertJvcaDetails;
    }

    public VwJvcaCompanySearchDao getVwJvcaCompanySearchDao() {
        return vwJvcaCompanySearchDao;
    }

    public void setVwJvcaCompanySearchDao(VwJvcaCompanySearchDao vwJvcaCompanySearchDao) {
        this.vwJvcaCompanySearchDao = vwJvcaCompanySearchDao;
    }

    public TblTempCompanyDocumentsDao getTblTempCompanyDocumentsDao() {
        return tblTempCompanyDocumentsDao;
    }

    public void setTblTempCompanyDocumentsDao(TblTempCompanyDocumentsDao tblTempCompanyDocumentsDao) {
        this.tblTempCompanyDocumentsDao = tblTempCompanyDocumentsDao;
    }

    public TblTempCompanyMasterDao getTblTempCompanyMasterDao() {
        return tblTempCompanyMasterDao;
    }

    public void setTblTempCompanyMasterDao(TblTempCompanyMasterDao tblTempCompanyMasterDao) {
        this.tblTempCompanyMasterDao = tblTempCompanyMasterDao;
    }

    public TblTempTendererMasterDao getTblTempTendererMasterDao() {
        return tblTempTendererMasterDao;
    }

    public void setTblTempTendererMasterDao(TblTempTendererMasterDao tblTempTendererMasterDao) {
        this.tblTempTendererMasterDao = tblTempTendererMasterDao;
    }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public TblUserRegInfoDao getTblUserRegInfoDao() {
        return tblUserRegInfoDao;
    }

    public void setTblUserRegInfoDao(TblUserRegInfoDao tblUserRegInfoDao) {
        this.tblUserRegInfoDao = tblUserRegInfoDao;
    }

    @Override
    public boolean registerUserOnPortal(TblLoginMaster tblLoginMaster, String randomNumber) {
        logger.debug("registerUserOnPortal : " + userId + START);
        boolean flag = false;
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            tblEmailVerificationCodeDao.addTblEmailVerificationCode(new TblEmailVerificationCode(0, tblLoginMaster.getUserId(), randomNumber, new Date()));
            flag = true;
        } catch (Exception e) {
            logger.error("registerUserOnPortal : " + userId, e);
        }
        logger.debug("registerUserOnPortal : " + userId + END);
        return flag;
    }

    @Override
    public String registerIndvidualClient(TblTempTendererMaster tblTempTendererMaster, int userId, boolean isIndiCli) throws Exception {
        logger.debug("registerIndvidualClient : " + userId + START);
        String query = "";
        tblTempTendererMaster.setTblLoginMaster(new TblLoginMaster(userId));
        if (isIndiCli) {
//            tblTempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(1));
//            TblTempBiddingPermission tblTempBiddingPermission = new TblTempBiddingPermission();
//            tblTempBiddingPermission.setCompanyId(1);
//            tblTempBiddingPermission.setProcurementCategory("Service");
//            tblTempBiddingPermission.setUserId(userId);
//            tblTempBiddingPermission.setWorkType("Consulting");
            query = query + "INSERT INTO [dbo].[tbl_TempBiddingPermission]"
                    + "([ProcurementCategory] ,[WorkType],[WorkCategroy],[CompanyID],[UserId]) "
                    + "VALUES('Service', 'Consulting', '', 1, " + userId + ") ";
            TblTempCompanyMaster tblTempCompanyMaster = new TblTempCompanyMaster();
            tblTempCompanyMaster.setCompanyId(1);
            tblTempTendererMaster.setTblTempCompanyMaster(tblTempCompanyMaster);
            //getTblTempBiddingPermissionDao().addTblTempBiddingPermission(tblTempBiddingPermission);
        }

        tblTempTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempTendererMaster.getCountry()).get(0));
        query = query + " INSERT INTO [dbo].[tbl_TempTendererMaster] "
                + "([userId],[companyId],[isAdmin],[title],[firstName],[middleName],[lasatName],"
                + "[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],"
                + "[postcode],[phoneNo],[mobileNo],[faxNo],[comments],"
                + "[website],[designation],[department],[nationality],[emailAddress],[subDistrict],[tinDocName], [UserType]"
                + ")VALUES(" + userId + ", " + tblTempTendererMaster.getTblTempCompanyMaster().getCompanyId() + ", '" + tblTempTendererMaster.getIsAdmin() + "','" + tblTempTendererMaster.getTitle() + "','" + tblTempTendererMaster.getFirstName() + "','" + tblTempTendererMaster.getMiddleName() + "','" + tblTempTendererMaster.getLasatName()
                + "','" + tblTempTendererMaster.getNationalIdNo() + "','" + tblTempTendererMaster.getAddress1() + "','" + tblTempTendererMaster.getAddress2() + "','"
                + tblTempTendererMaster.getCountry() + "','" + tblTempTendererMaster.getState() + "','" + tblTempTendererMaster.getCity() + "','" + tblTempTendererMaster.getUpJilla() + "','"
                + tblTempTendererMaster.getPostcode() + "','" + tblTempTendererMaster.getPhoneNo() + "','" + tblTempTendererMaster.getMobileNo() + "','" + tblTempTendererMaster.getFaxNo() + "','" + tblTempTendererMaster.getComments() + "','"
                + tblTempTendererMaster.getWebsite() + "','" + tblTempTendererMaster.getDesignation() + "','" + tblTempTendererMaster.getDepartment() + "','"
                + tblTempTendererMaster.getNationality() + "','" + tblTempTendererMaster.getEmailAddress() + "','" + tblTempTendererMaster.getSubDistrict() + "','" + tblTempTendererMaster.getTinDocName() + "'," + tblTempTendererMaster.getUserType()
                + ")";
        //tblTempTendererMasterDao.addTblTempTendererMaster(tblTempTendererMaster);
        TblLoginMaster tblLoginMaster = tblLoginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId).get(0);
        TblLoginMaster tlm = cloneTblLoginMaster(tblLoginMaster);
        StringBuilder sb = new StringBuilder();
        if (tlm.getIsJvca().equals("no")) {
            sb.append("SupportingDocuments");
        } else {
            sb.append("JvcaDetails");
        }
        tlm.setNextScreen(sb.toString());
        query = query + " UPDATE tbl_LoginMaster SET nextScreen = '" + sb.toString() + "' WHERE userId = " + tlm.getUserId();
        //tblLoginMasterDao.updateTblLoginMaster(tlm);
        logger.debug("registerIndvidualClient : " + userId + END);
        CommonMsgChk commonMsgChk = null;
        commonMsgChk = sPAddUser.executeProcedure(userId, "RegisterIndividual", query).get(0);
        if (commonMsgChk.getFlag() == false) {
            sb.delete(0, sb.length()-1);
            sb.append("IndividualConsultant");
        }
        return sb.toString();
    }

    @Override
    public TblLoginMaster cloneTblLoginMaster(TblLoginMaster tblLoginMaster) {
        logger.debug("cloneTblLoginMaster : " + userId + START);
        TblLoginMaster tlm = new TblLoginMaster();
        tlm.setBusinessCountryName(tblLoginMaster.getBusinessCountryName());
        tlm.setEmailId(tblLoginMaster.getEmailId());
        tlm.setFailedAttempt(tblLoginMaster.getFailedAttempt());
        tlm.setFirstLogin(tblLoginMaster.getFirstLogin());
        tlm.setHintAnswer(tblLoginMaster.getHintAnswer());
        tlm.setHintQuestion(tblLoginMaster.getHintQuestion());
        tlm.setIsEmailVerified(tblLoginMaster.getIsEmailVerified());
        tlm.setIsJvca(tblLoginMaster.getIsJvca());
        tlm.setIsPasswordReset(tblLoginMaster.getIsPasswordReset());
        tlm.setNationality(tblLoginMaster.getNationality());
        tlm.setNextScreen(tblLoginMaster.getNextScreen());
        tlm.setPassword(tblLoginMaster.getPassword());
        tlm.setRegisteredDate(tblLoginMaster.getRegisteredDate());
        tlm.setRegistrationType(tblLoginMaster.getRegistrationType());
        tlm.setResetPasswordCode(tblLoginMaster.getResetPasswordCode());
        tlm.setStatus(tblLoginMaster.getStatus());
        tlm.setTblUserTypeMaster(tblLoginMaster.getTblUserTypeMaster());
        tlm.setUserId(tblLoginMaster.getUserId());
        tlm.setValidUpTo(tblLoginMaster.getValidUpTo());
        logger.debug("cloneTblLoginMaster : " + userId + END);
        return tlm;
    }

    @Override //List<TblTempBiddingPermission> tblTempBiddingPermissionList,
    public boolean registerCompanyDetails(TblTempCompanyMaster tblTempCompanyMaster, List<TblTempBiddingPermission> tblTempBiddingPermissionList, TblUserPrefrence tblUserPreference, int userId) throws Exception {
        logger.debug("registerCompanyDetails : " + userId + START);
        boolean flag = false;
        String query = "";
        tblTempCompanyMaster.setTblLoginMaster(new TblLoginMaster(userId));
        tblTempCompanyMaster.setRegOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getRegOffCountry()).get(0));
        tblTempCompanyMaster.setCorpOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getCorpOffCountry()).get(0));
        tblTempCompanyMaster.setOriginCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getOriginCountry()).get(0));
        tblUserPreference.setUserId(userId);
        query = query + "INSERT INTO [dbo].[tbl_TempCompanyMaster] ([userId],[companyName],[establishmentYear],[regOffAddress],"
                + "[regOffCountry],[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],[regOffPhoneNo],[regOffFaxNo],"
                + "[corpOffAddress],[corpOffCountry],[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode],[corpOffPhoneno],[corpOffFaxNo],[specialization],[legalStatus],"
                + "[website],[originCountry],[regOffSubDistrict],[regOffMobileNo],[corpOffSubDistrict],[corpOffMobMobileNo]) "
                + "VALUES(" + userId + ",'" + tblTempCompanyMaster.getCompanyName() + "','" + tblTempCompanyMaster.getEstablishmentYear() + "','" + tblTempCompanyMaster.getRegOffAddress()
                + "','" + tblTempCompanyMaster.getRegOffCountry() + "','" + tblTempCompanyMaster.getRegOffState() + "','" + tblTempCompanyMaster.getRegOffCity() + "','" + tblTempCompanyMaster.getRegOffUpjilla() + "','" + tblTempCompanyMaster.getRegOffPostcode() + "','" + tblTempCompanyMaster.getRegOffPhoneNo() + "','" + tblTempCompanyMaster.getRegOffFaxNo()
                + "','" + tblTempCompanyMaster.getCorpOffAddress() + "','" + tblTempCompanyMaster.getCorpOffCountry() + "','" + tblTempCompanyMaster.getCorpOffState() + "','" + tblTempCompanyMaster.getCorpOffCity() + "','" + tblTempCompanyMaster.getCorpOffUpjilla() + "','" + tblTempCompanyMaster.getCorpOffPostcode() + "','" + tblTempCompanyMaster.getCorpOffPhoneno() + "','" + tblTempCompanyMaster.getCorpOffFaxNo()
                + "','" + tblTempCompanyMaster.getSpecialization() + "','" + tblTempCompanyMaster.getLegalStatus()
                + "','" + tblTempCompanyMaster.getWebsite() + "','" + tblTempCompanyMaster.getOriginCountry() + "','" + tblTempCompanyMaster.getRegOffSubDistrict() + "','" + tblTempCompanyMaster.getRegOffMobileNo() + "','" + tblTempCompanyMaster.getCorpOffSubDistrict() + "','" + tblTempCompanyMaster.getCorpOffMobMobileNo()
                + "');";
        //tblTempCompanyMasterDao.addTblTempCompanyMaster(tblTempCompanyMaster);
        query = query + "INSERT INTO tbl_UserPrefrence (userId, emailAlert, smsAlert) VALUES ("
                + tblUserPreference.getUserId() + ",'" + tblUserPreference.getEmailAlert() + "','" + tblUserPreference.getSmsAlert() + "');";
        //tblUserPrefrenceDao.addTblUserPrefrence(tblUserPreference);
        query = query + "DECLARE @companyID INT ";
        TblLoginMaster tblLoginMaster = tblLoginMasterDao.findTblLoginMaster("userId", Operation_enum.EQ, userId).get(0);
        query = query + "SELECT @companyID = companyId FROM tbl_TempCompanyMaster WHERE userId = " + tblUserPreference.getUserId() + ";";
        //tblTempCompanyMaster = tblTempCompanyMasterDao.findTblTempCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)).get(0);

        for (int i = 0; i < tblTempBiddingPermissionList.size(); i++) {
            TblTempBiddingPermission tblTempBiddingPermission = new TblTempBiddingPermission();
            tblTempBiddingPermission.setCompanyId(tblTempCompanyMaster.getCompanyId());
            tblTempBiddingPermission.setUserId(userId);
            tblTempBiddingPermission.setProcurementCategory(tblTempBiddingPermissionList.get(i).getProcurementCategory());
            tblTempBiddingPermission.setWorkType(tblTempBiddingPermissionList.get(i).getWorkType());
            tblTempBiddingPermission.setWorkCategroy(tblTempBiddingPermissionList.get(i).getWorkCategroy());
            query = query + " INSERT INTO [dbo].[tbl_TempBiddingPermission]"
                    + "([ProcurementCategory],[WorkType],[WorkCategroy],[CompanyID],[UserId])VALUES('"
                    + tblTempBiddingPermissionList.get(i).getProcurementCategory() + "','" + tblTempBiddingPermissionList.get(i).getWorkType()
                    + "','" + tblTempBiddingPermissionList.get(i).getWorkCategroy() + "', @companyID" + "," + tblLoginMaster.getUserId()
                    + ");";
            //getTblTempBiddingPermissionDao().addTblTempBiddingPermission(tblTempBiddingPermission);
        }
        TblLoginMaster tlm = cloneTblLoginMaster(tblLoginMaster);
        query = query + "UPDATE tbl_LoginMaster SET nextScreen = 'PersonalDetails' WHERE userId = " + tlm.getUserId();
        //tlm.setNextScreen("PersonalDetails");
        //tblLoginMasterDao.updateTblLoginMaster(tlm);
        CommonMsgChk commonMsgChk = null;
        commonMsgChk = sPAddUser.executeProcedure(userId, "RegisterCompanyDetails", query).get(0);
        if (commonMsgChk.getFlag() == true) {
            flag = true;
        }
        logger.debug("registerCompanyDetails : " + userId + END);
        return flag;
    }

    @Override
    public boolean uploadSupportDocs(TblTempCompanyDocuments tblTempCompanyDocuments, int tenderid) {
        logger.debug("uploadSupportDocs : " + userId + START);
        boolean flag = false;
        try {
            tblTempCompanyDocuments.setTblTempTendererMaster(new TblTempTendererMaster(tenderid));
            tblTempCompanyDocumentsDao.addTblTempCompanyDocuments(tblTempCompanyDocuments);
            flag = true;
        } catch (Exception e) {
            logger.error("uploadSupportDocs : " + userId, e);
        }
        logger.debug("uploadSupportDocs : " + userId + END);
        return flag;
    }

    @Override
    public boolean uploadSupportDocsReapply(TblCompanyDocuments tblCompanyDocuments, int tenderid) {
        logger.debug("uploadSupportDocsReapply : " + userId + START);
        boolean flag = false;
        try {
            tblCompanyDocuments.setTblTendererMaster(new TblTendererMaster(tenderid));
//            tblCompanyDocuments.setIsReapplied(1);
            tblCompanyDocuments.setDocStatus("Approved");
            getTblCompanyDocumentsDao().addTblCompanyDocuments(tblCompanyDocuments);
            flag = true;
        } catch (Exception e) {
            logger.error("uploadSupportDocsReapply : " + userId, e);
        }
        logger.debug("uploadSupportDocsReapply : " + userId + END);
        return flag;
    }

    @Override
    public int getCompanyIdByTendererId(int tendererId) {
        logger.debug("getCompanyIdByTendererId : " + userId + START);
        int companyId = 0;
        try {
            companyId = (int) hibernateQueryDao.singleColQuery("select tblCompanyMaster.companyId from TblTendererMaster where tendererId=" + tendererId).get(0);

        } catch (Exception e) {
            logger.error("getCompanyIdByTendererId : " + userId, e);
        }
        logger.debug("getCompanyIdByTendererId : " + userId + END);
        return companyId;
    }

    @Override
    public List<TblLoginMaster> getAllUsers() {
        logger.debug("getAllUsers : " + userId + START);
        logger.debug("getAllUsers : " + userId + END);
        return tblLoginMasterDao.getAllTblLoginMaster();
    }

    @Override
    public List<TblLoginMaster> findUserByCriteria(Object... values) throws Exception {
        logger.debug("findUserByCriteria : " + userId + START);
        List<TblLoginMaster> list = new ArrayList<TblLoginMaster>();
        String action = null;
        try {
            list = tblLoginMasterDao.findTblLoginMaster(values);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("findUserByCriteria " + userId, e);
            action = "Error in View Profile " + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, (Integer) values[2], "userId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("findUserByCriteria : " + userId + END);
        return list;
    }

    @Override
    public List<TblTempTendererMaster> findTendererByCriteria(Object... values) throws Exception {
        logger.debug("findTendererByCriteria : " + userId + START);
        logger.debug("findTendererByCriteria : " + userId + END);
        return tblTempTendererMasterDao.findTblTempTendererMaster(values);
    }

    @Override
    public List<TblTempCompanyDocuments> findUserDocuments(Object... values) throws Exception {
        logger.debug("findUserDocuments : " + userId + START);
        logger.debug("findUserDocuments : " + userId + END);
        return tblTempCompanyDocumentsDao.findTblTempCompanyDocuments(values);
    }

    @Override
    public List<TblCompanyDocuments> findBidderDocuments(Object... values) throws Exception {
        logger.debug("findBidderDocuments : " + userId + START);
        logger.debug("findBidderDocuments : " + userId + END);
        return tblCompanyDocumentsDao.findTblCompanyDocuments(values);
    }

    @Override
    public boolean deleteUserDoc(TblTempCompanyDocuments tblTempCompanyDocuments) {
        logger.debug("deleteUserDoc : " + userId + START);
        boolean flag = false;
        try {
            tblTempCompanyDocumentsDao.deleteTblTempCompanyDocuments(tblTempCompanyDocuments);
            flag = true;
        } catch (Exception e) {
            logger.error("deleteUserDoc : " + userId, e);
        }
        logger.debug("deleteUserDoc : " + userId + END);
        return flag;
    }

    @Override
    public boolean deleteUserDocReapply(TblCompanyDocuments tblCompanyDocuments) {
        logger.debug("deleteUserDocReapply : " + userId + START);
        boolean flag = false;
        try {
            tblCompanyDocumentsDao.deleteTblCompanyDocuments(tblCompanyDocuments);
            flag = true;
        } catch (Exception e) {
            logger.error("deleteUserDocReapply : " + userId, e);
        }
        logger.debug("deleteUserDocReapply : " + userId + END);
        return flag;
    }

    @Override
    public List<String> searchCompanyName(String searchBy, String searchVal) throws Exception {
        logger.debug("searchCompanyName : " + userId + START);
        List<String> msg = new ArrayList<String>();
        List<VwJvcaCompanySearch> jvcaCompanySearchs = new ArrayList<VwJvcaCompanySearch>();
        jvcaCompanySearchs = vwJvcaCompanySearchDao.findJvcaCompanySearch("id." + searchBy, Operation_enum.EQ, searchVal);
        if (jvcaCompanySearchs.isEmpty()) {
            msg.add("Company not found,0");
//            if (searchBy.equals("emailId")) {
//                msg.add("Email-Id or Company by Email-Id does not exist,0");
//            } else if (searchBy.equals("companyRegNumber")) {
//                msg.add("Company RegNo. does not exist,0");
//            } else {
//                msg.add("Company Name does not exist,0");
//            }
        } else {
            for (VwJvcaCompanySearch vwJvcaCompanySearch : jvcaCompanySearchs) {
                msg.add(vwJvcaCompanySearch.getId().getCompanyName() + "," + vwJvcaCompanySearch.getId().getEmailId() + "," + vwJvcaCompanySearch.getId().getCompanyRegNumber() + "," + vwJvcaCompanySearch.getId().getCompanyId());
            }
        }
        logger.debug("searchCompanyName : " + userId + END);
        return msg;

    }

    @Override
    public List<CommonMsgChk> insertJvcaDetailsBySP(int userId, int jvcaCompId, String jvcaRole) throws Exception {
        logger.debug("insertJvcaDetailsBySP : " + userId + START);
        logger.debug("insertJvcaDetailsBySP : " + userId + END);
        return sPInsertJvcaDetails.executeProcedure(userId, jvcaCompId, jvcaRole);
    }

    @Override
    public List<Object[]> getJvCompanyDetail(int userId) {
        logger.debug("getJvCompanyDetail : " + userId + START);
        logger.debug("getJvCompanyDetail : " + userId + END);
        return hibernateQueryDao.createNewQuery("select tcjv.ventureId as ventureId, tcjv.tblCompanyMaster.companyId as companyId,cm.companyName as companyName,cm.companyRegNumber as compRegNo,tcjv.jvRole as jvRole from TblCompanyMaster cm,TblTempCompanyMaster tcm, TblTempCompanyJointVenture tcjv where tcm.tblLoginMaster.userId=" + userId + " and tcjv.tblTempCompanyMaster.companyId=tcm.companyId and tcjv.tblCompanyMaster.companyId=cm.companyId");
    }

    @Override
    public boolean deleteJvcaDetail(TblTempCompanyJointVenture tblTempCompanyJointVenture) {
        logger.debug("deleteJvcaDetail : " + userId + START);
        boolean flag = false;
        try {
            tblTempCompanyJointVentureDao.deleteTblTempCompanyJointVenture(tblTempCompanyJointVenture);
            flag = true;
        } catch (Exception e) {
            logger.error("deleteJvcaDetail : " + userId, e);
        }
        logger.debug("deleteJvcaDetail : " + userId + END);
        return flag;
    }

    @Override
    public List<TblTempCompanyMaster> companyRegNoCount(String compRegNo, int userId) throws Exception {
        logger.debug("companyRegNoCount : " + userId + START);
//        hibernateQueryDao.countForQuery("TblCompanyMaster cm", "cm.companyRegNumber=" + compRegNo);
        List<TblTempCompanyMaster> list = null;
        if (userId == 0) {

            list = tblTempCompanyMasterDao.findTblTempCompanyMaster("companyRegNumber", Operation_enum.EQ, compRegNo);
        } else {
            list = tblTempCompanyMasterDao.findTblTempCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
        }
        logger.debug("companyRegNoCount : " + userId + END);
        return list;
    }

    @Override
    public List<TblTempBiddingPermission> biddingPermissionCount(int userId) throws Exception {
        logger.debug("biddingPermissionCount : " + userId + START);

        List<TblTempBiddingPermission> list = null;
        if (userId != 0) {
            list = tblTempBiddingPermissionDao.findTblTempBiddingPermission("userId", Operation_enum.EQ, userId);
        }
        logger.debug("biddingPermissionCount : " + userId + END);
        return list;
    }

    @Override
    public boolean uploadEsign(TblTempTendererEsignature tblTempTendererEsignature) {
        logger.debug("uploadEsign : " + userId + START);
        boolean flag = false;
        try {
            tblTempTendererEsignatureDao.addTblTempTendererEsignature(tblTempTendererEsignature);
            flag = true;
        } catch (Exception e) {
            logger.error("uploadEsign : " + userId, e);
        }
        logger.debug("uploadEsign : " + userId + END);
        return flag;
    }

    @Override
    public List<TblTempTendererEsignature> findUserEsign(Object... values) throws Exception {
        logger.debug("findUserEsign : " + userId + START);
        logger.debug("findUserEsign : " + userId + END);
        return tblTempTendererEsignatureDao.findTblTempTendererEsignature(values);
    }

    @Override
    public boolean deleteUserEsign(TblTempTendererEsignature tblTempTendererEsignature) {
        logger.debug("deleteUserEsign : " + userId + START);
        boolean flag = false;
        try {
            tblTempTendererEsignatureDao.deleteTblTempTendererEsignature(tblTempTendererEsignature);
            flag = true;
        } catch (Exception e) {
            logger.error("deleteUserEsign : " + userId, e);
        }
        logger.debug("deleteUserEsign : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateTendererDetail(TblTempTendererMaster tblTempTendererMaster) {
        logger.debug("updateTendererDetail : " + userId + START);
        boolean flag = false;
        try {
            tblTempTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempTendererMaster.getCountry()).get(0));
            tblTempTendererMasterDao.updateTblTempTendererMaster(tblTempTendererMaster);
            flag = true;
        } catch (Exception e) {
            logger.error("updateTendererDetail : " + userId, e);
        }
        logger.debug("updateTendererDetail : " + userId + END);
        return flag;
    }

    @Override
    public List<TblCompanyMaster> companyRegNoCheck(String compRegNo, int userId) throws Exception {
        logger.debug("companyRegNoCheck : " + userId + START);
        List<TblCompanyMaster> master = new ArrayList<TblCompanyMaster>();
        if (userId == 0) {
            master = tblCompanyMasterDao.findTblCompanyMaster("companyRegNumber", Operation_enum.EQ, compRegNo);
        } else {
            master = tblCompanyMasterDao.findTblCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
        }
        logger.debug("companyRegNoCheck : " + userId + END);
        return master;
    }

    @Override
    public List<TblCompanyMaster> companyNameCheck(String companyName, int userId) throws Exception {
        logger.debug("companyNameCheck : " + userId + START);
        List<TblCompanyMaster> master = new ArrayList<TblCompanyMaster>();
        if (userId == 0) {
            master = tblCompanyMasterDao.findTblCompanyMaster("companyName", Operation_enum.EQ, companyName);
        } else {
            master = tblCompanyMasterDao.findTblCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
        }
        logger.debug("companyNameCheck : " + userId + END);
        return master;
    }

    @Override
    public BigDecimal insertTmpCmpMaster(String cmpRegNo, int userId) {
        logger.debug("insertTmpCmpMaster : " + userId + START);
        logger.debug("insertTmpCmpMaster : " + userId + END);
        return sPInsertCompMaster.executeProcedure(cmpRegNo, userId);
    }

    @Override
    public boolean updateTempCompanyDetails(TblTempCompanyMaster tblTempCompanyMaster, List<TblTempBiddingPermission> tblTempBiddingPermissionList, TblUserPrefrence tblUserPreference) {
        logger.debug("updateTempCompanyDetails : " + userId + START);
        boolean flag = false;
        String query = "";
//        Transaction tx = null;
//        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//        Session session = sessionFactory.openSession();
//        try {
//            tx = session.beginTransaction();
        tblTempCompanyMaster.setRegOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getRegOffCountry()).get(0));
        tblTempCompanyMaster.setCorpOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getCorpOffCountry()).get(0));
        tblTempCompanyMaster.setOriginCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getOriginCountry()).get(0));

        //int prefUpdate = hibernateQueryDao.updateDeleteQuery("UPDATE TblUserPrefrence SET emailAlert='' , smsAlert='' where userId = " + tblTempCompanyMaster.getTblLoginMaster().getUserId());
        query = query + "UPDATE [dbo].[tbl_TempCompanyMaster] SET "
                + "[companyName] = '" + tblTempCompanyMaster.getCompanyName() + "' ,[companyNameInBangla] = '"
                + "',[companyRegNumber] = '',[establishmentYear] = '" + tblTempCompanyMaster.getEstablishmentYear()
                + "',[regOffAddress] = '" + tblTempCompanyMaster.getRegOffAddress() + "', [regOffCountry] ='" + tblTempCompanyMaster.getRegOffCountry()
                + "',[regOffState] = '" + tblTempCompanyMaster.getRegOffState() + "',[regOffCity] ='" + tblTempCompanyMaster.getRegOffCity()
                + "',[regOffUpjilla] = '" + tblTempCompanyMaster.getRegOffUpjilla() + "',[regOffPostcode] ='" + tblTempCompanyMaster.getRegOffPostcode()
                + "',[regOffPhoneNo] = '" + tblTempCompanyMaster.getRegOffPhoneNo() + "',[regOffFaxNo] = '" + tblTempCompanyMaster.getRegOffFaxNo()
                + "',[corpOffAddress] = '" + tblTempCompanyMaster.getCorpOffAddress() + "',[corpOffCountry] = '" + tblTempCompanyMaster.getCorpOffCountry()
                + "',[corpOffState] = '" + tblTempCompanyMaster.getCorpOffState() + "',[corpOffCity] = '" + tblTempCompanyMaster.getCorpOffCity()
                + "',[corpOffUpjilla] = '" + tblTempCompanyMaster.getCorpOffUpjilla() + "',[corpOffPostcode] = '" + tblTempCompanyMaster.getCorpOffPostcode()
                + "',[corpOffPhoneno] = '" + tblTempCompanyMaster.getCorpOffPhoneno() + "',[corpOffFaxNo] = '" + tblTempCompanyMaster.getCorpOffFaxNo()
                + "',[specialization] = '" + tblTempCompanyMaster.getSpecialization() + "',[legalStatus] = '" + tblTempCompanyMaster.getLegalStatus()
                + "',[tinNo] = '',[website] = '" + tblTempCompanyMaster.getWebsite() + "',[tinDocName] = '"
                + "',[statutoryCertificateNo] = '',[originCountry] = '"+tblTempCompanyMaster.getOriginCountry()
                + "',[workCategory] = '',[tradeLicenseNumber] = '"
                + "',[regOffSubDistrict] = '" + tblTempCompanyMaster.getRegOffSubDistrict() + "',[regOffMobileNo] = '" + tblTempCompanyMaster.getRegOffMobileNo()
                + "',[corpOffSubDistrict] = '" + tblTempCompanyMaster.getCorpOffSubDistrict() + "',[corpOffMobMobileNo] = '" + tblTempCompanyMaster.getCorpOffMobMobileNo()
                + "' WHERE userId = " + tblTempCompanyMaster.getTblLoginMaster().getUserId();
        //tblTempCompanyMasterDao.updateTblTempCompanyMaster(tblTempCompanyMaster);
        query = query + " UPDATE [dbo].[tbl_UserPrefrence] SET emailAlert = '" + tblUserPreference.getEmailAlert()
                + "', smsAlert = '" + tblUserPreference.getSmsAlert() + "' WHERE userId = " + tblTempCompanyMaster.getTblLoginMaster().getUserId();
        //tblUserPrefrenceDao.updateTblUserPrefrence(tblUserPreference);
        //delete previous bidding permission
        query = query + " DELETE FROM [dbo].[tbl_TempBiddingPermission] WHERE UserId = " + tblTempCompanyMaster.getTblLoginMaster().getUserId();
//            List<TblTempBiddingPermission> prevBiddingPermissionList = getTblTempBiddingPermissionDao().findTblTempBiddingPermission("companyId", Operation_enum.EQ, tblTempCompanyMaster.getCompanyId());
//            for (int i = 0; i < prevBiddingPermissionList.size(); i++) {
//                //getTblTempBiddingPermissionDao().deleteTblTempBiddingPermission(prevBiddingPermissionList.get(i));
//            }
        //add new permission after deleting previous permission
//            if (prevBiddingPermissionList.isEmpty()) {
        for (int i = 0; i < tblTempBiddingPermissionList.size(); i++) {
            TblTempBiddingPermission tblTempBiddingPermission = new TblTempBiddingPermission();
            tblTempBiddingPermission.setCompanyId(tblTempCompanyMaster.getCompanyId());
            tblTempBiddingPermission.setUserId(tblTempCompanyMaster.getTblLoginMaster().getUserId());
            tblTempBiddingPermission.setProcurementCategory(tblTempBiddingPermissionList.get(i).getProcurementCategory());
            tblTempBiddingPermission.setWorkType(tblTempBiddingPermissionList.get(i).getWorkType());
            tblTempBiddingPermission.setWorkCategroy(tblTempBiddingPermissionList.get(i).getWorkCategroy());
//                query = query + " UPDATE [dbo].[tbl_TempBiddingPermission] SET [ProcurementCategory] = '" + tblTempBiddingPermissionList.get(i).getProcurementCategory()
//                        + "',[WorkType] = '" + tblTempBiddingPermissionList.get(i).getWorkType() + "', [WorkCategroy] = '" + tblTempBiddingPermissionList.get(i).getWorkCategroy()
//                        + "' WHERE UserId = " + tblTempCompanyMaster.getTblLoginMaster().getUserId();
            query = query + " INSERT INTO [dbo].[tbl_TempBiddingPermission]"
                    + "([ProcurementCategory],[WorkType],[WorkCategroy],[CompanyID],[UserId])VALUES('"
                    + tblTempBiddingPermissionList.get(i).getProcurementCategory() + "','" + tblTempBiddingPermissionList.get(i).getWorkType()
                    + "','" + tblTempBiddingPermissionList.get(i).getWorkCategroy() + "', " + tblTempCompanyMaster.getCompanyId() + "," + tblTempCompanyMaster.getTblLoginMaster().getUserId()
                    + ");";
            //getTblTempBiddingPermissionDao().addTblTempBiddingPermission(tblTempBiddingPermission);
        }
//            }
//            tx.commit();
        CommonMsgChk commonMsgChk = null;
        commonMsgChk = sPAddUser.executeProcedure(tblTempCompanyMaster.getTblLoginMaster().getUserId(), "RegisterCompanyDetailsUpdate", query).get(0);
        if (commonMsgChk.getFlag() == true) {
            flag = true;
        }

//        } catch (Exception e) {
//            logger.error("updateTempCompanyDetails : " + userId, e);
//            tx.rollback();
//        }
        logger.debug("updateTempCompanyDetails : " + userId + END);
        return flag;
    }

    @Override
    public long checkCountByHql(String from, String where) throws Exception {
        logger.debug("checkCountByHql : " + userId + START);
        logger.debug("checkCountByHql : " + userId + END);
        return hibernateQueryDao.countForNewQuery(from, where);
    }

    @Override
    public int updateNextScreenInLogin(int userId, String pageName) throws Exception {
        logger.debug("updateNextScreenInLogin : " + userId + START);
        logger.debug("updateNextScreenInLogin : " + userId + END);
        return hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set nextScreen='" + pageName + "' where userId=" + userId);
    }

    @Override
    public List<String> pageNavigationList(int userId) {
        logger.debug("pageNavigationList : " + userId + START);
        List<String> pageList = new ArrayList<String>();
        StringTokenizer tokenizer = new StringTokenizer(sPPageNavigate.executeProcedure(userId), ",");
        while (tokenizer.hasMoreElements()) {
            pageList.add((String) tokenizer.nextElement());
        }
        logger.debug("pageNavigationList : " + userId + END);
        return pageList;
    }

    @Override
    public CommonMsgChk addUser(int userId, String actionSP) {
        logger.debug("addUser : " + userId + START);
        CommonMsgChk commonMsgChk = null;
        String action = null;
        try {
            commonMsgChk = sPAddUser.executeProcedure(userId, actionSP, "").get(0);
            action = "Final Submission of a Profile";
        } catch (Exception e) {
            action = "Error in  Final Submission of a Profile : " + e;
            logger.error("addUser : " + userId, e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.New_User_Registration.getName(), action, "");
            action = null;
        }
        logger.debug("addUser : " + userId + END);
        return commonMsgChk;
    }

    @Override
    public String docSizeCheck(int userId) {
        logger.debug("docSizeCheck : " + userId + START);
        logger.debug("docSizeCheck : " + userId + END);
        return appCommonData.executeProcedure("checkQuota", String.valueOf(userId), null).get(0).getFieldName1();
    }

    @Override
    public List<Object> contentAdmMailId() {
        logger.debug("contentAdmMailId : " + userId + START);
        logger.debug("contentAdmMailId : " + userId + END);
        return hibernateQueryDao.singleColQuery("select lm.emailId from TblLoginMaster lm  where lm.tblUserTypeMaster.userTypeId=8");
    }

    @Override
    public void contentAdmMsgBox(String toEmailId, String fromEmaild, String subject, String msgText) {
        logger.debug("contentAdmMsgBox : " + userId + START);
        sPCommon.executeProcedure(toEmailId, fromEmaild, "", subject, msgText, "", "Low", "Inbox", "", "Live", 0, "No", "No", "0", 0, 0);
        logger.debug("contentAdmMsgBox : " + userId + END);
    }

    @Override
    public String isJVCA(String userId) {
        logger.debug("isJVCA : " + userId + START);
        logger.debug("isJVCA : " + userId + END);
        return hibernateQueryDao.singleColQuery("select tlm.isJvca from TblLoginMaster tlm where tlm.userId=" + userId).get(0).toString();
    }

    @Override
    public boolean updateTblTenderMaster(TblTendererMaster tblTendererMaster) {
        logger.debug("updateTblTenderMaster : " + userId + START);
        boolean flag = false;
        String action = null;
        try {
            tblTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTendererMaster.getCountry()).get(0));
            tblTendererMasterDao.updateTblTendererMaster(tblTendererMaster);
            flag = true;
            action = "Edit Profile";
        } catch (Exception e) {
            logger.error("updateTblTenderMaster : " + userId, e);
            action = "Error in Edit Profile " + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblTendererMaster.getTendererId(), "tendererId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("updateTblTenderMaster : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateTblCompanyMaster(TblCompanyMaster tblCompanyMaster) {
        logger.debug("updateTblCompanyMaster : " + userId + START);
        boolean flag = false;
        String action = null;
        try {
            tblCompanyMaster.setRegOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblCompanyMaster.getRegOffCountry()).get(0));
            tblCompanyMaster.setOriginCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblCompanyMaster.getOriginCountry()).get(0));
            tblCompanyMaster.setCorpOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblCompanyMaster.getCorpOffCountry()).get(0));
            tblCompanyMasterDao.updateTblCompanyMaster(tblCompanyMaster);
            flag = true;
            action = "Edit Profile";
        } catch (Exception e) {
            logger.error("updateTblCompanyMaster : " + userId, e);
            action = "Error in Edit Profile" + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblCompanyMaster.getCompanyId(), "companyId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("updateTblCompanyMaster : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateTblBiddingPermission(List<TblBiddingPermission> tblBiddingPermissionList, int companyID, int userID) {
        logger.debug("updateTblBiddingPermission : " + userId + START);
        boolean flag = false;
        String action = null;
        try {

            for (int i = 0; i < tblBiddingPermissionList.size(); i++) {
                TblBiddingPermission tblBiddingPermission = new TblBiddingPermission();
                tblBiddingPermission.setCompanyId(companyID);
                tblBiddingPermission.setUserId(userID);
                tblBiddingPermission.setProcurementCategory(tblBiddingPermissionList.get(i).getProcurementCategory());
                tblBiddingPermission.setWorkType(tblBiddingPermissionList.get(i).getWorkType());
                tblBiddingPermission.setWorkCategroy(tblBiddingPermissionList.get(i).getWorkCategroy());
                tblBiddingPermission.setIsReapplied(1);

                tblBiddingPermissionDao.addTblBiddingPermission(tblBiddingPermission);
            }

            flag = true;
            action = "Re-apply";
        } catch (Exception e) {
            logger.error("updateTblBiddingPermission : " + userId, e);
            action = "Error in Re-Apply" + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblBiddingPermissionList.get(0).getCompanyId(), "companyId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("updateTblBiddingPermission : " + userId + END);
        return flag;
    }

    @Override
    public List<TblMandatoryDoc> getMandatoryDocs(String userId, boolean isTemp, boolean isReapplied) throws Exception {
        logger.debug("getMandatoryDocs : " + userId + START);
        String regType = hibernateQueryDao.singleColQuery("select registrationType from TblLoginMaster where userId=" + userId).get(0).toString();
        String query = "";
        List<TblMandatoryDoc> mandatoryDocList = new ArrayList<TblMandatoryDoc>();

        if (isTemp) {
            query = "select distinct ProcurementCategory from tbl_TempBiddingPermission where UserId = " + userId;
        } else if (isReapplied) {
            query = "select distinct ProcurementCategory from tbl_BiddingPermission where IsReapplied = 1 and UserId = " + userId;
        } else {
            query = "select distinct ProcurementCategory from tbl_BiddingPermission where IsReapplied = 0 and UserId = " + userId;
        }
        List lstBidCat = hibernateQueryDao.nativeSQLQuery(query, null);
        //query for User type - added by Sristy
        if (isTemp) {
            query = "SELECT UserType FROM tbl_TempTendererMaster WHERE userId = " + userId;
        } else {
            query = "SELECT UserType FROM tbl_TendererMaster WHERE userId = " + userId;
        }
        List listUserType = hibernateQueryDao.nativeSQLQuery(query, null);

        //nishith-start - 11/05/2016
        Object permission[] = new Object[lstBidCat.size() + 1];
        permission[0] = "All";
        int index = 1;
        for (Iterator it = lstBidCat.iterator(); it.hasNext();) {
            permission[index++] = it.next();
        }
        //permission[index++] = listUserType.get(0);
        //nishith-end
        if (permission.length >= 1) {
            String data1 = null;
            String country = null;
            String table1 = null;
            String table2 = null;
            String joinString = null;
            if (isTemp) {
                table1 = "TblTempCompanyMaster";
                table2 = "TblTempTendererMaster";
                joinString = "tblTempCompanyMaster";
            } else {
                table1 = "TblCompanyMaster";
                table2 = "TblTendererMaster";
                joinString = "tblCompanyMaster";
            }
            if (regType.charAt(0) == 'c') {
                data1 = "company";
                //country=hibernateQueryDao.singleColQuery("select regOffCountry from "+table1+" where tblLoginMaster.userId="+userId).get(0).toString();
                StringBuilder sb = new StringBuilder();
                sb.append("select tcm.regOffCountry ");
                sb.append("from TblLoginMaster tlm," + table2 + " ttm," + table1 + " tcm ");
                sb.append("where ttm." + joinString + ".companyId=tcm.companyId and tlm.userId=ttm.tblLoginMaster.userId and tlm.userId=" + userId);
                country = hibernateQueryDao.singleColQuery(sb.toString()).get(0).toString();
            } else if (regType.charAt(0) == 'g') {
                data1 = "govownedent";
                country = "bangladesh";
            } else if (regType.charAt(0) == 'm') {
                data1 = "media";
                //country=hibernateQueryDao.singleColQuery("select regOffCountry from "+table1+" where tblLoginMaster.userId="+userId).get(0).toString();
                StringBuilder sb = new StringBuilder();
                sb.append("select tcm.regOffCountry ");
                sb.append("from TblLoginMaster tlm," + table2 + " ttm," + table1 + " tcm ");
                sb.append("where ttm." + joinString + ".companyId=tcm.companyId and tlm.userId=ttm.tblLoginMaster.userId and tlm.userId=" + userId);
                country = hibernateQueryDao.singleColQuery(sb.toString()).get(0).toString();
            } else {
                data1 = "individual";
                country = hibernateQueryDao.singleColQuery("select country from " + table2 + " where tblLoginMaster.userId=" + userId).get(0).toString();
            }
//        if (!(regType.StartsWith("c") || regType.StartsWith("s"))) {
//            data1 = "individual";
//            country= hibernateQueryDao.singleColQuery("select country from TblTempTendererMaster where tblLoginMaster.userId="+userId).get(0).toString();
//        } else {
//            data1 = "company";
//            country=hibernateQueryDao.singleColQuery("select regOffCountry from TblTempCompanyMaster where tblLoginMaster.userId="+userId).get(0).toString();
//        }
            String isBang = null;
            if (country.equalsIgnoreCase("bangladesh")) {
                isBang = "yes";
            } else {
                isBang = "no";
            }
            logger.debug("getMandatoryDocs : " + userId + END);

            //nishith-modified
            List<TblMandatoryDoc> docList = tblMandatoryDocDao.findTblMandatoryDoc("regType", Operation_enum.EQ, data1, "isBangladesh", Operation_enum.EQ, isBang, "procurementNature", Operation_enum.IN, permission);
            //sristy-modified
            if (!listUserType.get(0).equals(0)) {
                mandatoryDocList = tblMandatoryDocDao.findTblMandatoryDoc("regType", Operation_enum.EQ, data1, "isBangladesh", Operation_enum.EQ, isBang, "userType", Operation_enum.EQ, listUserType.get(0));
            }
            mandatoryDocList.addAll(docList);
        }
        return mandatoryDocList;
    }

    @Override
    public String getTendererId(String userId) {
        logger.debug("getTendererId : " + userId + START);
        logger.debug("getTendererId : " + userId + END);
        return hibernateQueryDao.getSingleColQuery("select tttm.tendererId from TblTempTendererMaster tttm where tttm.tblLoginMaster.userId=" + userId).get(0).toString();
    }

    @Override
    public String updateCompanyDocumentsStatus(String tendererId) {
        logger.debug("updateCompanyDocumentsStatus : " + userId + START);
//        List update = null;
        int update = hibernateQueryDao.updateDeleteSQLNewQuery("UPDATE tbl_CompanyDocuments SET isReapplied = 1 WHERE tendererId = " + tendererId + "");
        logger.debug("updateCompanyDocumentsStatus : " + userId + END);
        if (update> 0) {
            return "OK";
        } else {
            return "NOT OK";
        }
    }

    @Override
    public String getTendererIdMail(String userId) {
        logger.debug("getTendererIdMail : " + userId + START);
        logger.debug("getTendererIdMail : " + userId + END);
        return hibernateQueryDao.getSingleColQuery("select tttm.tendererId from TblTendererMaster tttm where tttm.tblLoginMaster.userId=" + userId).get(0).toString();
    }

    @Override
    public int checkDocCount(String userId, boolean isTemp) {
        logger.debug("checkDocCount : " + userId + START);

        String query = "";
        if (isTemp) {
            query = "select distinct ProcurementCategory from tbl_TempBiddingPermission where UserId = " + userId;
        } else {
            query = "select distinct ProcurementCategory from tbl_BiddingPermission where IsReapplied=1 and UserId = " + userId;
        }
        List lstBidCat = hibernateQueryDao.nativeSQLQuery(query, null);

        //nishith-14/05/2016
        String permission = "";
        int index = 0;
        for (Iterator it = lstBidCat.iterator(); it.hasNext();) {
            permission = (index == 0 ? permission + "('" : permission + "','");
            permission = permission + it.next();
            index++;
        }
        permission = index == 0 ? permission + "'All'" : permission + "','All')";
        //ends-ok

        String regType = hibernateQueryDao.singleColQuery("select registrationType from TblLoginMaster where userId=" + userId).get(0).toString();
        String data1 = null;
        String country = null;
        if (regType.charAt(0) == 'c') {
            data1 = "company";
            if (isTemp) {
                country = hibernateQueryDao.singleColQuery("select regOffCountry from TblTempCompanyMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            } else {
                country = hibernateQueryDao.singleColQuery("select regOffCountry from TblCompanyMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            }
        } else if (regType.charAt(0) == 'g') {
            data1 = "govownedent";
            country = "bangladesh";
        } else if (regType.charAt(0) == 'm') {
            data1 = "media";
            if (isTemp) {
                country = hibernateQueryDao.singleColQuery("select regOffCountry from TblTempCompanyMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            } else {
                country = hibernateQueryDao.singleColQuery("select regOffCountry from TblCompanyMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            }
        } else {
            data1 = "individual";
            if (isTemp) {
                country = hibernateQueryDao.singleColQuery("select country from TblTempTendererMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            } else {
                country = hibernateQueryDao.singleColQuery("select country from TblTendererMaster where tblLoginMaster.userId=" + userId).get(0).toString();
            }
        }
//        if (!(regType.StartsWith("c") || regType.StartsWith("s"))) {
//            data1 = "individual";
//            country= hibernateQueryDao.singleColQuery("select country from TblTempTendererMaster where tblLoginMaster.userId="+userId).get(0).toString();
//        } else {
//            data1 = "company";
//            country=hibernateQueryDao.singleColQuery("select regOffCountry from TblTempCompanyMaster where tblLoginMaster.userId="+userId).get(0).toString();
//        }
        String isBang = null;
        if (country.equalsIgnoreCase("bangladesh")) {
            isBang = "yes";
        } else {
            isBang = "no";
        }
        //nishith-modified added filtering option-procurementNature

        List<Object> list;
        
        if (isTemp) {
            list = hibernateQueryDao.getSingleColQuery("select mandatoryDocId from TblMandatoryDoc where procurementNature in " + permission + " and docTypeValue not in(select documentType from TblTempCompanyDocuments where tblTempTendererMaster.tendererId=" + getTendererId(userId) + ") and  regType='" + data1 + "' and isBangladesh='" + isBang + "' and isMandatory='yes'");
        } else {
            list = hibernateQueryDao.getSingleColQuery("select mandatoryDocId from TblMandatoryDoc where procurementNature in " + permission + " and docTypeValue not in(select documentTypeId from TblCompanyDocuments where tblTendererMaster.tendererId=" + getTendererIdMail(userId) + ") and  regType='" + data1 + "' and isBangladesh='" + isBang + "' and isMandatory='yes'");
        }
        int i;
        if (list.isEmpty()) {
            i = 0;
        } else {
            i = list.size();
        }
        logger.debug("checkDocCount : " + userId + END);
        return i;
    }

    @Override
    public String getTendererMobEmail(int userId) {
        logger.debug("getTendererMobEmail : " + userId + START);
        String emailMobile = "";
        try {
            Object[] objects = hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId=" + userId).get(0);
            emailMobile = (String) objects[0] + "$" + (String) objects[1] + (String) objects[2];
        } catch (Exception ex) {
            logger.error("getTendererMobEmail : " + userId + " : " + ex.toString());
        }
        logger.debug("getTendererMobEmail : " + userId + END);
        return emailMobile;
    }

    @Override
    public String getMaterTendererId(String userId) {
        logger.debug("getMaterTendererId : " + userId + START);
        logger.debug("getMaterTendererId : " + userId + END);
        return hibernateQueryDao.getSingleColQuery("select ttm.tendererId from TblTendererMaster ttm where ttm.tblLoginMaster.userId=" + userId).get(0).toString();
    }

    @Override
    public boolean createUserByCompanyAdmin(TblLoginMaster tblLoginMaster, TblTendererMaster tblTendererMaster, int userId) {
        logger.debug("createUserByCompanyAdmin : " + userId + START);
        boolean flag;
        try {
            Object[] obj = hibernateQueryDao.createNewQuery("select tlm.registrationType,tlm.isJvca,tlm.businessCountryName,tlm.tblUserTypeMaster.userTypeId,tlm.nationality,tcm.tblCompanyMaster.companyId,tlm.validUpTo from TblLoginMaster tlm,TblTendererMaster tcm where tlm.userId = tcm.tblLoginMaster.userId and tlm.userId=" + userId).get(0);
            tblLoginMaster.setRegistrationType((String) obj[0]);
            tblLoginMaster.setIsJvca((String) obj[1]);
            tblLoginMaster.setBusinessCountryName((String) obj[2]);
            tblLoginMaster.setTblUserTypeMaster(new TblUserTypeMaster((Byte) obj[3]));
            tblLoginMaster.setValidUpTo((Date) obj[6]);
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            tblTendererMaster.setTblLoginMaster(tblLoginMaster);
            tblTendererMaster.setTblCompanyMaster(new TblCompanyMaster((Integer) obj[5]));
            tblTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTendererMaster.getCountry()).get(0));
            tblTendererMasterDao.addTblTendererMaster(tblTendererMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("createUserByCompanyAdmin : " + userId + " : " + ex);
            flag = false;
        }
        logger.debug("createUserByCompanyAdmin : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateCompanyUser(TblTendererMaster tblTendererMaster, int userId, int tendererId, String nationality) {
        logger.debug("updateCompanyUser : " + userId + START);
        boolean flag;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster tlm set tlm.nationality = '" + nationality + "' where tlm.userId= " + userId);
            tblTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTendererMaster.getCountry()).get(0));
            tblTendererMasterDao.updateTblTendererMaster(tblTendererMaster);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCompanyUser : " + userId + " : " + ex);
            flag = false;
        }
        logger.debug("updateCompanyUser : " + userId + END);
        return flag;
    }

    @Override
    public Object[] getCompanyAdminDetails(int userId) {
        logger.debug("getCompanyAdminDetails : " + userId + START);
        logger.debug("getCompanyAdminDetails : " + userId + END);
        return hibernateQueryDao.createNewQuery("select tcm.companyName,tlm.emailId,ttm.firstName,ttm.lastName from TblLoginMaster tlm,TblCompanyMaster tcm,TblTendererMaster ttm where tlm.userId = ttm.tblLoginMaster.userId and ttm.tblCompanyMaster.companyId = tcm.companyId and tlm.userId=" + userId).get(0);
    }

    @Override
    public Object[] getLoginDetails(int userId) {
        logger.debug("getLoginDetails : " + userId + START);
        logger.debug("getLoginDetails : " + userId + END);
        return hibernateQueryDao.createNewQuery("select tlm.emailId,tlm.nationality  from TblLoginMaster tlm where tlm.userId=" + userId).get(0);
    }

    @Override
    public List<TblTendererMaster> getTendererDetails(int tendererId) {
        logger.debug("getTendererDetails : " + userId + START);
        List<TblTendererMaster> list = null;
        try {
            list = tblTendererMasterDao.findTblTendererMaster("tendererId", Operation_enum.EQ, tendererId);
        } catch (Exception ex) {
            logger.error("getTendererDetails : : " + userId + " : " + ex);
            //list = null;
        }
        logger.debug("getTendererDetails : " + userId + END);
        return list;
    }

    @Override
    public boolean updateUserStatus(TblTendererAuditTrail tblTendererAuditTrail, int userId, String status) {
        logger.debug("updateUserStatus : " + userId + START);
        boolean flag;
        try {
            tblTendererAuditTrailDao.addTblTendererAuditTrail(tblTendererAuditTrail);
            hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster tlm set tlm.status = '" + status + "' where tlm.userId = " + userId);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateUserStatus : : " + userId + " : " + ex);
            flag = false;
        }
        logger.debug("updateUserStatus : " + userId + END);
        return flag;
    }

    @Override
    public int jvcaRegister(TblLoginMaster tblLoginMaster, String jvcId) {
        logger.debug("jvcaRegister : " + userId + START);
        int i;
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            if (hibernateQueryDao.updateDeleteNewQuery("update TblJointVenture set newJVUserId=" + tblLoginMaster.getUserId() + " where JVId=" + jvcId) > 0) {
                i = tblLoginMaster.getUserId();
            } else {
                i = 0;
            }
        } catch (Exception e) {
            logger.error("jvcaRegister Ends :" + userId + " : " + e);
            i = 0;
        }
        logger.debug("jvcaRegister : " + userId + END);
        return i;
    }

    @Override
    public boolean regJVCompanyDetail(TblCompanyMaster tblTempCompanyMaster, int userId, int jvcaId) throws Exception {
        logger.debug("regJVCompanyDetail : " + userId + START);
        boolean flag;
        String partnerCompId = "";
        String query = "";

        List lstCompanyId = hibernateQueryDao.nativeSQLQuery("select companyId from tbl_JVCAPartners where JVId = " + jvcaId, null);
        if (lstCompanyId != null && !lstCompanyId.isEmpty()) {
            for (Object compId : lstCompanyId) {
                partnerCompId = partnerCompId + compId.toString() + ",";
            }
        }
        partnerCompId = partnerCompId.substring(0, partnerCompId.length() - 1);

        try {
            tblTempCompanyMaster.setTblLoginMaster(new TblLoginMaster(userId));
            tblTempCompanyMaster.setRegOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getRegOffCountry()).get(0));
            tblTempCompanyMaster.setCorpOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getCorpOffCountry()).get(0));
            //tblCompanyMasterDao.addTblCompanyMaster(tblTempCompanyMaster);
            query = query + "INSERT INTO tbl_CompanyMaster ([companyName],[userId],[legalStatus],[establishmentYear],[specialization],[regOffAddress],[regOffCountry],"
                    + "[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],"
                    + "[regOffPhoneNo],[regOffFaxNo],[regOffSubDistrict],[regOffMobileNo],"
                    + "[corpOffAddress],[corpOffCountry],"
                    + "[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode],"
                    + "[corpOffPhoneNo],[corpOffFaxNo],[corpOffSubDistrict],[corpOffMobMobileNo]) VALUES "
                    + "('" + tblTempCompanyMaster.getCompanyName() + "'," + userId + ",'','','','" + tblTempCompanyMaster.getRegOffAddress() + "','" + tblTempCompanyMaster.getRegOffCountry() + "','"
                    + tblTempCompanyMaster.getRegOffState() + "','" + tblTempCompanyMaster.getRegOffCity() + "','" + tblTempCompanyMaster.getRegOffUpjilla() + "','" + tblTempCompanyMaster.getRegOffPostcode() + "','"
                    + tblTempCompanyMaster.getRegOffPhoneNo() + "','" + tblTempCompanyMaster.getRegOffFaxNo() + "','" + tblTempCompanyMaster.getRegOffSubDistrict() + "','" + tblTempCompanyMaster.getRegOffMobileNo() + "','"
                    + tblTempCompanyMaster.getRegOffAddress() + "','" + tblTempCompanyMaster.getRegOffCountry() + "','"
                    + tblTempCompanyMaster.getRegOffState() + "','" + tblTempCompanyMaster.getRegOffCity() + "','" + tblTempCompanyMaster.getRegOffUpjilla() + "','" + tblTempCompanyMaster.getRegOffPostcode() + "','"
                    + tblTempCompanyMaster.getRegOffPhoneNo() + "','" + tblTempCompanyMaster.getRegOffFaxNo() + "','" + tblTempCompanyMaster.getRegOffSubDistrict() + "','" + tblTempCompanyMaster.getRegOffMobileNo()
                    + "') ";
            //List<TblCompanyMaster> compId = tblCompanyMasterDao.findTblCompanyMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));

            query = query + " DECLARE @companyID INT;"
                    + " SELECT @companyID = companyId FROM tbl_CompanyMaster WHERE userId = " + userId
                    + " INSERT INTO tbl_BiddingPermission (ProcurementCategory, WorkType, WorkCategroy, CompanyID, UserId, IsReapplied)"
                    + " SELECT distinct ProcurementCategory, WorkType, WorkCategroy, @companyID, " + userId + ", 0 from tbl_BiddingPermission "
                    + " WHERE CompanyID IN (" + partnerCompId + ") AND  IsReapplied = 0";
            query = query + " UPDATE tbl_LoginMaster SET nextScreen='JVPersonalDetails' WHERE userId=" + userId;
            CommonMsgChk commonMsgChk = null;
            commonMsgChk = sPAddUser.executeProcedure(userId, "RegisterJVCACompany", query).get(0);
            if (commonMsgChk.getFlag() == true) {
                //if (hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set nextScreen='JVPersonalDetails' where userId=" + userId) > 0) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("regJVCompanyDetail : " + userId + " : " + e);
            flag = false;
        }
        logger.debug("regJVCompanyDetail : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateJVCompanyDetails(TblCompanyMaster tblTempCompanyMaster) {
        logger.debug("updateJVCompanyDetails : " + userId + START);
        boolean flag;
        try {
            tblTempCompanyMaster.setRegOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getRegOffCountry()).get(0));
            tblTempCompanyMaster.setCorpOffCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempCompanyMaster.getCorpOffCountry()).get(0));
            tblCompanyMasterDao.updateTblCompanyMaster(tblTempCompanyMaster);
            flag = true;
        } catch (Exception e) {
            logger.error("updateJVCompanyDetails : " + userId + " :" + e);
            flag = false;
        }
        logger.debug("updateJVCompanyDetails : " + userId + END);
        return flag;
    }

    @Override
    public boolean regJVPersonalDetail(TblTendererMaster tblTempTendererMaster, int userId) throws Exception {
        logger.debug("regJVPersonalDetail : " + userId + START);
        boolean flag;
        try {
            tblTempTendererMaster.setTblLoginMaster(new TblLoginMaster(userId));
            tblTempTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempTendererMaster.getCountry()).get(0));
            tblTempTendererMaster.setTblCompanyMaster(new TblCompanyMaster(getCompanyMasterId(userId)));
            tblTendererMasterDao.addTblTendererMaster(tblTempTendererMaster);
            flag = true;
        } catch (Exception e) {
            logger.error("regJVPersonalDetail : " + userId + " : " + e);
            flag = false;
        }
        logger.debug("regJVPersonalDetail : " + userId + END);
        return flag;

    }

    public int getCompanyMasterId(int userId) {
        logger.debug("getCompanyMasterId : " + userId + START);
        logger.debug("getCompanyMasterId : " + userId + END);
        return (Integer) hibernateQueryDao.getSingleColQuery("select tcm.companyId from TblCompanyMaster tcm where tcm.tblLoginMaster.userId=" + userId).get(0);
    }

    @Override
    public boolean updateJVTendererDetail(TblTendererMaster tblTempTendererMaster) {
        logger.debug("updateJVTendererDetail : " + userId + START);
        boolean flag;
        try {
            tblTempTendererMaster.setCountry((String) hibernateQueryDao.singleColQuery(cntQuery + tblTempTendererMaster.getCountry()).get(0));
            tblTendererMasterDao.updateTblTendererMaster(tblTempTendererMaster);
            flag = true;
        } catch (Exception e) {

            logger.error("updateJVTendererDetail : " + userId + " : " + e);
            flag = false;
        }
        logger.debug("updateJVTendererDetail : " + userId + END);
        return flag;
    }

    @Override
    public boolean updateUserStatus(String userId, String status) {
        logger.debug("updateUserStatus : " + userId + START);
        boolean flag;
        String action = "Tendrer has completed Registration JVCA";
        try {
            Date d = new Date();
            d.setYear(d.getYear() + 1);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd");
            hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set status='" + status + "',validUpTo='" + dateFormat.format(d) + "' where userId=" + userId);
            flag = true;
        } catch (Exception e) {
            action = "error in " + action + " " + e.getMessage();
            logger.error("updateUserStatus : " + userId + " : " + e);
            flag = false;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(userId), "userId", EgpModule.JVCA.getName(), action, "");
            action = null;
        }
        logger.debug("updateUserStatus : " + userId + END);
        return flag;
    }

    @Override
    public boolean assignCompanyAdminRole(String cId, String tId) {
        logger.debug("assignCompanyAdminRole : " + userId + START);
        boolean flag = false;
        List<?> obj = null;
        String newUserId = "";
        try {
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblTendererMaster set isAdmin='no' where tblCompanyMaster.companyId=" + cId);
            int j = hibernateQueryDao.updateDeleteNewQuery("update TblTendererMaster set isAdmin='yes' where tendererId=" + tId);

            String query = "select userId from tbl_TendererMaster where tendererId ='" + tId + "'";
            obj = hibernateQueryDao.nativeSQLQuery(query, null);
            if (obj != null && !obj.isEmpty()) {
                for (Object x : obj) {
                    newUserId = x.toString();
                }
            }

            int k = hibernateQueryDao.updateDeleteNewQuery("update TblBiddingPermission set userId = " + newUserId + " where companyId = " + cId);
            if (i != 0 && j != 0 && k != 0) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("assignCompanyAdminRole : " + userId, e);

        }
        logger.debug("assignCompanyAdminRole : " + userId + END);
        return flag;
    }

    @Override
    public List<Object[]> jvCompMailDetails(String userId) {
        logger.debug("jvCompMailDetails : " + userId + START);
        List<Object[]> temp = null;
        try {
            StringBuilder query = new StringBuilder();
            query.append("select lm.emailId ,jv.jvname,tm.firstName,tm.lastName,cm.companyId,cm.companyName,jp.isNominatedPartner,jp.jvRole,jp.jvAcceptRejStatus ");
            query.append("from TblJointVenture jv,TblJvcapartners jp,TblLoginMaster lm,TblTendererMaster tm,TblCompanyMaster cm  ");
            query.append("where lm.userId = jp.userId and lm.userId = tm.tblLoginMaster.userId and ");
            query.append("tm.tblCompanyMaster.companyId = cm.companyId and ");
            query.append("jv.jvid = jp.tblJointVenture.jvid and jv.newJvuserId=" + userId + " order by jp.jvpartnerId asc");
            temp = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("jvCompMailDetails : " + userId, e);
        }
        logger.debug("jvCompMailDetails : " + userId + END);
        return temp;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    @Override
    public boolean updateUserStatus(String userId, String userStatus, String profileStatus) {
        boolean flag = false;
        try {
            if (hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set status='" + userStatus + "',transdate=current_timestamp() where userId=" + userId) > 0) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("setUserStatus : " + userId, e);
        }
        return flag;
    }

    @Override
    public Date getUserRegInfo(String userId) {
        Date approvalDate = null;
        try {
            approvalDate = (Date) (hibernateQueryDao.singleColQuery("select tbl.actionDate from TblUserRegInfo tbl where userId=" + userId).get(0));
        } catch (Exception ex) {
            logger.error("getUserRegInfo : " + userId, ex);
        }
        return approvalDate;
    }

    public TblTempBiddingPermissionDao getTblTempBiddingPermissionDao() {
        return tblTempBiddingPermissionDao;
    }

    public void setTblTempBiddingPermissionDao(TblTempBiddingPermissionDao tblTempBiddingPermissionDao) {
        this.tblTempBiddingPermissionDao = tblTempBiddingPermissionDao;
    }

    /**
     * @return the tblBiddingPermissionDao
     */
    public TblBiddingPermissionDao getTblBiddingPermissionDao() {
        return tblBiddingPermissionDao;
    }

    /**
     * @param tblBiddingPermissionDao the tblBiddingPermissionDao to set
     */
    public void setTblBiddingPermissionDao(TblBiddingPermissionDao tblBiddingPermissionDao) {
        this.tblBiddingPermissionDao = tblBiddingPermissionDao;
    }

    /**
     * @return the tblCompanyDocumentsDao
     */
    public TblCompanyDocumentsDao getTblCompanyDocumentsDao() {
        return tblCompanyDocumentsDao;
    }

    /**
     * @param tblCompanyDocumentsDao the tblCompanyDocumentsDao to set
     */
    public void setTblCompanyDocumentsDao(TblCompanyDocumentsDao tblCompanyDocumentsDao) {
        this.tblCompanyDocumentsDao = tblCompanyDocumentsDao;
    }

    /**
     * @return the tblUserPrefrenceDao
     */
    public TblUserPrefrenceDao getTblUserPrefrenceDao() {
        return tblUserPrefrenceDao;
    }

    /**
     * @param tblUserPrefrenceDao the tblUserPrefrenceDao to set
     */
    public void setTblUserPrefrenceDao(TblUserPrefrenceDao tblUserPrefrenceDao) {
        this.tblUserPrefrenceDao = tblUserPrefrenceDao;
    }

//    private static class HibernateUtil {
//
//        public HibernateUtil() {
//        }
//        
//        private static SessionFactory sessionFactory;
//     
//    static{
//        try {
//            sessionFactory = new Configuration().configure().buildSessionFactory();
//        } catch (Throwable e) {
//            throw new ExceptionInInitializerError(e);
//        }
//    }
// 
//    public static SessionFactory getSessionFactory(){
//        return sessionFactory;
//    }
//     
//    public static void shutDown(){
//        //closes caches and connections
//        getSessionFactory().close();
//    }
//    }
}
