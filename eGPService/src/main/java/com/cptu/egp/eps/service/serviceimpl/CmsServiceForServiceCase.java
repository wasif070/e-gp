/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvFormMapDao;
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvLumpsumDtlDao;
import java.util.AbstractCollection;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class CmsServiceForServiceCase {

    TblCmsSrvLumpsumDtlDao dtlDao;
    HibernateQueryDao hibernateQueryDao;
    TblCmsSrvFormMapDao formMapDao;
    final Logger logger = Logger.getLogger(ConsolodateService.class);
    private String logUserId = "0";

    public TblCmsSrvFormMapDao getFormMapDao() {
        return formMapDao;
    }

    public void setFormMapDao(TblCmsSrvFormMapDao formMapDao) {
        this.formMapDao = formMapDao;
    }

    public TblCmsSrvLumpsumDtlDao getDtlDao() {
        return dtlDao;
    }

    public void setDtlDao(TblCmsSrvLumpsumDtlDao dtlDao) {
        this.dtlDao = dtlDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public List<Object[]> getDatesForEditInService(int wpId, int first, int max) {
        logger.debug("getDatesForEditInService : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            StringBuilder builder = new StringBuilder();
            builder.append("select tss.srNo,tss.milestone,tss.description,tss.percentOfCtrVal,");
            builder.append("tss.pestartdate,tss.peenddate,tss.startDate,tss.endDate,tss.srvLumpsumDtlId ");
            builder.append("from TblCmsSrvLumpsumDtl tss where tss.wpId="+wpId+" ");
            list = hibernateQueryDao.createByCountNewQuery(builder.toString(), first, max);
        } catch (Exception e) {
            logger.error("getDatesForEditInService : " + e + "starts");
        }
        logger.debug("getDatesForEditInService : " + logUserId + "ends");
        return list;
    }
    public void updateDates(int lumpsumid,Date sdate,Date edate) {
        logger.debug("updateDates : " + logUserId + "starts");
        List<Object[]> list = null;
        try {
            String query = "update TblCmsSrvLumpsumDtl tss set tss.startDate='"+new java.sql.Date(sdate.getTime())+"',tss.endDate='"+new java.sql.Date(edate.getTime())+"' where tss.srvLumpsumDtlId='"+lumpsumid+"'";
            hibernateQueryDao.updateDeleteNewQuery(query);
        } catch (Exception e) {
            logger.error("updateDates : " + e + "starts");
        }
        logger.debug("updateDates : " + logUserId + "ends");

    }
}
