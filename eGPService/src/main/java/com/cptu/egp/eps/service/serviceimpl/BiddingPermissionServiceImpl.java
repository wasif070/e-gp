/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblTempBiddingPermissionDao;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import com.cptu.egp.eps.dao.daointerface.TblBiddingPermissionDao;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.dao.daointerface.TblAppPermissionDao;
import com.cptu.egp.eps.model.table.TblAppPermission;
import com.cptu.egp.eps.service.serviceinterface.BiddingPermissionService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import java.util.List;

/**
 *
 * @author G. M. Rokibul Hasan
 */
public class BiddingPermissionServiceImpl implements BiddingPermissionService {

    TblTempBiddingPermissionDao tblTempBiddingPermissionDao;
    private TblAppPermissionDao tblAppPermissionDao;
    private TblBiddingPermissionDao tblBiddingPermissionDao;
    final Logger logger = Logger.getLogger(BiddingPermissionServiceImpl.class);
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private MakeAuditTrailService makeAuditTrailService;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }
    
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    public TblTempBiddingPermissionDao getTblTempBiddingPermissionDao() {
        return tblTempBiddingPermissionDao;
    }

  
    public void setTblTempBiddingPermissionDao(TblTempBiddingPermissionDao tblTempBiddingPermissionDao) {
        this.tblTempBiddingPermissionDao = tblTempBiddingPermissionDao;
    }


    public TblAppPermissionDao getTblAppPermissionDao() {
        return tblAppPermissionDao;
    }

  
    public void setTblAppPermissionDao(TblAppPermissionDao tblAppPermissionDao) {
        this.tblAppPermissionDao = tblAppPermissionDao;
    }

    
    public TblBiddingPermissionDao getTblBiddingPermissionDao() {
        return tblBiddingPermissionDao;
    }

  
    public void setTblBiddingPermissionDao(TblBiddingPermissionDao tblBiddingPermissionDao) {
        this.tblBiddingPermissionDao = tblBiddingPermissionDao;
    }

    

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public void addTblTempBiddingPermission(TblTempBiddingPermission tblTempBiddingPermission,int userID) {
        logger.debug("add TblTempBiddingPermission : " + logUserId + loggerStart);
        String action="";
        try {
         action="Add TblTempBiddingPermission";
            tblTempBiddingPermissionDao.addTblTempBiddingPermission(tblTempBiddingPermission);
        } catch (Exception e) {
             action="Add TblTempBiddingPermission : "+e;
            logger.error("add TblTempBiddingPermission : " + logUserId + e);
    }
    finally{
        makeAuditTrailService.generateAudit(auditTrail, userID, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("add TblTempBiddingPermission : " + logUserId + loggerEnd);
    }

   
    @Override
    public List<TblTempBiddingPermission> findTblTempBiddingPermission(Object... values) throws Exception {
        return tblTempBiddingPermissionDao.findTblTempBiddingPermission(values); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void addTblAppPermission(TblAppPermission tblAppPermission,int userID){
         logger.debug("add TblAppPermission : " + logUserId + loggerStart);
        String action="";
        try {
         action="Add TblAppPermission";
         tblAppPermissionDao.addTblAppPermission(tblAppPermission);
        } catch (Exception e) {
             action="Add TblAppPermission : "+e;
            logger.error("add TblAppPermission : " + logUserId + e);
    }
    finally{
        makeAuditTrailService.generateAudit(auditTrail, userID, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("add TblAppPermission : " + logUserId + loggerEnd);
    }
    
    
    
    @Override
    public List<TblAppPermission> findTblAppPermission(Object... values) throws Exception {
        return tblAppPermissionDao.findTblAppPermission(values); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void addTblBiddingPermission(TblBiddingPermission tblBiddingPermission,int userID){
        logger.debug("add TblBiddingPermission : " + logUserId + loggerStart);
        String action="";
        try {
         action="Add TblBiddingPermission";
            tblBiddingPermissionDao.addTblBiddingPermission(tblBiddingPermission);
        } catch (Exception e) {
             action="Add TblBiddingPermission : "+e;
            logger.error("add TblBiddingPermission : " + logUserId + e);
    }
    finally{
        makeAuditTrailService.generateAudit(auditTrail, userID, "useId", EgpModule.Message_Box.getName(), action, "");
        action=null;
        }
        logger.debug("add TblBiddingPermission : " + logUserId + loggerEnd);
    }
    
       
    @Override
    public List<TblBiddingPermission> findTblBiddingPermission(Object... values) throws Exception {
        return tblBiddingPermissionDao.findTblBiddingPermission(values); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String updateNonDebarredBiddingList(List<TblBiddingPermission> biddingPermissions, String debarredUser, String companyId) {
        logger.debug("updateNonDebarredBiddingList : " + logUserId + loggerStart);
        String msg = "success";
        try {
            int j = hibernateQueryDao.updateDeleteNewQuery("delete from TblBiddingPermission tlt where tlt.companyId = "+ companyId +"  and tlt.userId=" + debarredUser);
            for (int i = 0; i < biddingPermissions.size(); i++) {
                tblBiddingPermissionDao.addTblBiddingPermission(biddingPermissions.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("updateNonDebarredBiddingList : " + logUserId + e);
            msg = "fail";
        }
        logger.debug("updateNonDebarredBiddingList : " + logUserId + loggerEnd);
        return msg;
    }

    }
