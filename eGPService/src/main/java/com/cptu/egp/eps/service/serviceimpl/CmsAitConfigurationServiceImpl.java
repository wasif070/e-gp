/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsAitConfigDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import com.cptu.egp.eps.service.serviceinterface.CmsAitConfigurationService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsAitConfigurationServiceImpl implements CmsAitConfigurationService {

    final Logger logger = Logger.getLogger(CmsAitConfigurationServiceImpl.class);
    private TblCmsAitConfigDao cmsAitConfigDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsAitConfigDao getCmsAitConfigDao() {
        return cmsAitConfigDao;
    }

    public void setCmsAitConfigDao(TblCmsAitConfigDao cmsAitConfigDao) {
        this.cmsAitConfigDao = cmsAitConfigDao;
    }

    /***
     * This method inserts a TblCmsAitConfig object in database
     * @param tblCmsAitConfig
     * @return int
     */
    @Override
    public int insertCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("insertCmsAitConfig : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsAitConfigDao.addTblCmsAitConfig(tblCmsAitConfig);
            flag = tblCmsAitConfig.getAitConfigId();
        } catch (Exception ex) {
            logger.error("insertCmsAitConfig : " + logUserId + " : " + ex);
            flag = 0;
        }
        logger.debug("insertCmsAitConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsAitConfig object in DB
     * @param tblCmsAitConfig
     * @return boolean
     */
    @Override
    public boolean updateCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("updateCmsAitConfig : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsAitConfigDao.updateTblCmsAitConfig(tblCmsAitConfig);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsAitConfig : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("updateCmsAitConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsAitConfig object in DB
     * @param tblCmsAitConfig
     * @return boolean
     */
    @Override
    public boolean deleteCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("deleteCmsAitConfig : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsAitConfigDao.deleteTblCmsAitConfig(tblCmsAitConfig);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsAitConfig : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteCmsAitConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsAitConfig objects from database
     * @return List<TblCmsAitConfig>
     */
    @Override
    public List<TblCmsAitConfig> getAllCmsAitConfig() {
        logger.debug("getAllCmsTaxConfig : " + logUserId + STARTS);
        List<TblCmsAitConfig> cmsAitConfigList = new ArrayList<TblCmsAitConfig>();
        try {
            cmsAitConfigList = cmsAitConfigDao.getAllTblCmsAitConfig();
        } catch (Exception ex) {
            logger.error("getAllCmsTaxConfig : " + logUserId + " : " + ex);
        }
        logger.debug("getAllCmsTaxConfig : " + logUserId + ENDS);
        return cmsAitConfigList;
    }

    /***
     *  This method returns no. of TblCmsAitConfig objects from database
     * @return long
     */
    @Override
    public long getCmsAitConfigCount() {
        logger.debug("getCmsAitConfigCount : " + logUserId + STARTS);
        long cmsAitConfigCount = 0;
        try {
            cmsAitConfigCount = cmsAitConfigDao.getTblCmsAitConfigCount();
        } catch (Exception ex) {
            logger.error("getCmsAitConfigCount : " + logUserId + " : " + ex);
        }
        logger.debug("getCmsAitConfigCount : " + logUserId + ENDS);
        return cmsAitConfigCount;
    }

    /***
     * This method returns TblCmsAitConfig for the given Id
     * @param int id
     * @return TblCmsAitConfig
     */
    @Override
    public TblCmsAitConfig getCmsAitConfig(int id) {
        logger.debug("getCmsAitConfig : " + logUserId + STARTS);
        TblCmsAitConfig tblCmsAitConfig = null;
        List<TblCmsAitConfig> cmsAitConfigList = null;
        try {
            cmsAitConfigList = cmsAitConfigDao.findTblCmsAitConfig("aitConfigId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            logger.error("getCmsAitConfig(int) : " + logUserId + " : " + ex);
        }
        if (!cmsAitConfigList.isEmpty()) {
            tblCmsAitConfig = cmsAitConfigList.get(0);
        }
        logger.debug("getCmsAitConfig(int) : " + logUserId + ENDS);
        return tblCmsAitConfig;
    }

    /***
     * This method gives the list of CmsAitConfig objects for the given financial year id
     * @param financialYearId
     * @return List<TblCmsAitConfig>
     */
    @Override
    public List<TblCmsAitConfig> getCmsAitConfigForFianancialYear(int financialYearId) {
        logger.debug("getCmsAitConfigWithFianancialYear : " + logUserId + STARTS);
        List<TblCmsAitConfig> cmsAitConfigList = null;
        try {
            cmsAitConfigList = cmsAitConfigDao.findTblCmsAitConfig("financialyearId", Operation_enum.EQ, financialYearId);
        } catch (Exception ex) {
            logger.error("getCmsAitConfigWithFianancialYear : " + logUserId + " : " + ex);
        }
        logger.debug("getCmsAitConfigWithFianancialYear : " + logUserId + ENDS);
        return cmsAitConfigList;
    }

    /***
     * This method gives the Ids of financial years in the TblCmsAitConfig objects.
     * @return List<String>
     */
    @Override
    public List<String> getCmsAitConfigFianacialYearsList() {
        logger.debug("getCmsAitConfigFianacialYearsList : " + logUserId + STARTS);
        List<String> financialYearsList = new ArrayList<String>();
        List<Object[]> cmsAitConfigList = new ArrayList<Object[]>();
        StringBuffer queryStringBuffer = new StringBuffer();
        String queryString = "SELECT "
                + "tblCmsAitConfig.financialyearId "
                + "FROM TblCmsAitConfig tblCmsAitConfig "
                + "GROUP BY  tblCmsAitConfig.financialyearId";
        queryStringBuffer.append(queryString);
        try {
            cmsAitConfigList = cmsAitConfigDao.createQuery(queryStringBuffer.toString());
        } catch (Exception ex) {
            logger.error("getCmsAitConfigFianacialYearsList : " + logUserId + " : " + ex);
        }
        if (cmsAitConfigList != null && !cmsAitConfigList.isEmpty()) {
            for (int i = 0; i < cmsAitConfigList.size(); i++) {
                Object tempObjectArray = cmsAitConfigList.get(i);
                financialYearsList.add(tempObjectArray + "");
            }
        }
        logger.debug("getCmsAitConfigFianacialYearsList : " + logUserId + ENDS);
        return financialYearsList;
    }
}
