/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import java.util.*;
import com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService;
import com.cptu.egp.eps.dao.daointerface.*;
import com.cptu.egp.eps.dao.daoimpl.*;
import com.cptu.egp.eps.model.table.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;

import com.cptu.egp.eps.dao.storedprocedure.SearchComplaintPaymentBean;

/**
 * 
 * @author Administrator
 */
public class ComplaintMgmtServiceImpl implements ComplaintMgmtService {

    /**
     * Get Search Result
     * @param officerId
     * @param tenderId
     * @param tendererId
     * @param complaintStatus
     * @return
     */
    final static Logger log = Logger.getLogger(ComplaintMgmtServiceImpl.class);
    private TblComplaintMasterDao complaintMasterDao;
    private TblComplaintLevelDao complaintLevelDao;
    private TblComplaintTypeDao complaintTypeDao;
    private TblComplaintHistoryDao complaintHistoryDao;
    private TblComplaintDocsDao complaintDocsDao;
    private TblTenderMasterDao tenderMasterDao;
    private TblEmployeeTrasferDao tblEmployeeTrasferDao;
    private TblReviewPanelDao tblReviewPanelDao;
    private TblTenderDetailsDao tblTenderDetailsDao;
    private TblEmployeeOfficesImpl tblEmployeeOffices;
    private TblComplaintPaymentDao complaintPaymentDao;
    private TblLoginMasterDao tblLoginMasterDao;
    private TblEmployeeMasterDao tblEmployeeMasterDao;
    private HibernateQueryDao hibernateQueryDao;

    @Autowired
    public void setTblEmployeeMasterDao(TblEmployeeMasterDao tblEmployeeMasterDao) {
        this.tblEmployeeMasterDao = tblEmployeeMasterDao;
    }

    @Autowired
    public void setComplaintPaymentDao(TblComplaintPaymentDao complaintPaymentDao) {
        this.complaintPaymentDao = complaintPaymentDao;
    }

    @Autowired
    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    @Autowired
    public void setComplaintMasterDao(TblComplaintMasterDao complaintMasterDao) {
        this.complaintMasterDao = complaintMasterDao;
    }

    @Autowired
    public void setComplaintLevelDao(TblComplaintLevelDao complaintLevelDao) {
        this.complaintLevelDao = complaintLevelDao;
    }

    @Autowired
    public void setComplaintTypeDao(TblComplaintTypeDao complaintTypeDao) {
        this.complaintTypeDao = complaintTypeDao;
    }

    @Autowired
    public void setComplaintHistoryDao(
            TblComplaintHistoryDao complaintHistoryDao) {
        this.complaintHistoryDao = complaintHistoryDao;
    }

    @Autowired
    public void setComplaintDocsDao(TblComplaintDocsDao complaintDocsDao) {
        this.complaintDocsDao = complaintDocsDao;
    }

    @Autowired
    public void setTenderMasterDao(TblTenderMasterDao tenderMasterDao) {
        this.tenderMasterDao = tenderMasterDao;
    }

    @Autowired
    public void setTblEmployeeTrasferDao(
            TblEmployeeTrasferDao tblEmployeeTrasferDao) {
        this.tblEmployeeTrasferDao = tblEmployeeTrasferDao;
    }

    @Autowired
    public void setTblReviewPanelDao(TblReviewPanelDao tblReviewPanelDao) {
        this.tblReviewPanelDao = tblReviewPanelDao;
    }

    @Autowired
    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    @Autowired
    public void setTblEmployeeOffices(TblEmployeeOfficesImpl tblEmployeeOffices) {
        this.tblEmployeeOffices = tblEmployeeOffices;
    }

    public List<TblComplaintType> getComplaintTypes() {
        return complaintTypeDao.getComplaintTypes();
    }

    @Autowired
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     * This is the method to add a complaint in the datebase
     * table ComplaintMaster by calling addEntity method of
     * super class i.e AbcAbstract class which uses
     * hibernate template
     * return type is entity
     */
    public void addComplaint(TblComplaintMaster complaint) {
        log.info("complaint sent from service to daoimpl");
        complaintMasterDao.addComplaint(complaint);
    }

    /**
     * This is the method to get all the complaint details from the 
     * database table ComplaintMaster by passing the 
     * complaintId which is of Integer type.
     * return type is entity
     */
    public TblComplaintMaster getComplaintDetails(int complaintId) {
        return complaintMasterDao.getComplaintDetails(complaintId);
    }

    /**
     * not used
     */
    public List<TblComplaintDocs> getComplaintDocs(Object... values)
            throws Exception {
        return complaintDocsDao.getComplaintDocs();
    }

    /**
     * This method is used to save the uploaded documents
     * in the complaintDocs table
     * object is passed to this method
     */
    public void saveDoc(TblComplaintDocs doc) throws Exception {
        complaintDocsDao.saveDoc(doc);
    }

    public List<TblTenderMaster> findByEntity(Object... values)
            throws Exception {
        return tenderMasterDao.findTblTenderMaster(values);
    }

    /**
     * This is the method to get PE governmentUserId of 
     * a particular tender from the table employeeTrasfer 
     * table by passing the tenderId parameter and by calling 
     * createQuery method of super class i.e AbcAbstract
     * class 
     * return type is listObject 
     */
    public List<Object[]> getGovtUser(int tenderId) {
        log.error("in complaint service govuser id");
        return complaintMasterDao.getGovtUserByTenderId(tenderId);
    }

    public List<TblComplaintType> getComplaintTypeById(Object... values)
            throws Exception {
        return complaintTypeDao.getComplaintTypeById(values);
    }

    /**
     * This is the method to save the complaint in the
     * complaintHistory table by passing the
     * TblComplaintHistory object
     */
    public void addHistory(TblComplaintHistory history) {
        complaintHistoryDao.addHistory(history);
    }

    /**
     * This method is used to retrieve the complaints from 
     * complaintMaster table by passing either the tenderId
     * or complaintId
     * returns List<TblComplaintMaster>
     */
    public List<TblComplaintMaster> getComplaintById(Object... values)
            throws Exception {
        return complaintMasterDao.getComplaintById(values);
    }

    /**
     * This method is used to retrieve the complaint list
     * based on the complaint level
     * returns List<TblComplaintLevel> values
     */
    public List<TblComplaintLevel> findByLevel(Object... values)
            throws Exception {
        return complaintLevelDao.findByLevel(values);
    }

    /**
     * This method is used to retrieve the govUserId from
     * employeeTrasfer table by passing the employee role
     * who has logged in
     */
    public List<TblEmployeeTrasfer> findByGovUser(Object... values)
            throws Exception {
        return tblEmployeeTrasferDao.findTblEmployeeTrasfer(values);
    }

    /**
     * This is the method to update a complaint in the datebase
     * table ComplaintMaster by calling updateEntity method of
     * super class i.e AbcAbstract class which uses
     * hibernate template
     * return type is entity
     */
    public void updateMaster(TblComplaintMaster master) {
        complaintMasterDao.updateMaster(master);
    }

    /**
     * This method is used to retrieve the complaint details from
     * the history table by passing complaintId,complaintLevelId
     * and userTypeId
     */
    public List<Object[]> getComplaintHistory(int id, int complaintLevelId, int userTypeId) {
        log.error("in complaint history");
        return complaintHistoryDao.getComplaintHistory(id, complaintLevelId, userTypeId);
    }

    /**
     * This method retrieves the list of documents from the table
     * complaintDocs by passing the complaintId as parameter
     * returns List<TblComplaintDocs> for tenderer
     */
    public List<TblComplaintDocs> getDocsList(Object... values)
            throws Exception {
        log.error("in complaint management service");
        return complaintDocsDao.getComplaintDocs(values);
    }

    /**
     * This method retreives the documents list based on complaintId,
     * complaintLevelId from complaintDocs table
     * returns listObjects
     */
    public List<Object[]> getDocs(int complaintId, int complaintLevelId) throws Exception {
        return complaintDocsDao.getDocs(complaintId, complaintLevelId);
    }


    /**
     * not used
     */
    public List<Object[]> getComplaintList(int tenderid, int complaintlevelId)
            throws Exception {
        return complaintMasterDao.getComplaintList(tenderid, complaintlevelId);
    }

    /**
     * This method retrieves the document list based on complaintId,
     * complaintLevelId,uploadedBy from complaintDocs table
     * returns listObjects
     */
    public List<Object[]> getDocsProcessList(int id, int levelId, int uploadedBy)
            throws Exception {
        return complaintDocsDao.getDocsProcessList(id, levelId, uploadedBy);
    }

    /**
     * not used
     */
    public List<Object[]> getSearchList(int complid, int tenderid, int refid)
            throws Exception {
        return complaintMasterDao.getSearchList(complid, tenderid, refid);
    }

    /**
     * Getting all ReviewPanels
     */
    public List<TblReviewPanel> getReviewPanels() throws Exception {
        return tblReviewPanelDao.getReviewPanels();
    }

    /**
     * This is the method to search all the complaints 
     * which are in pending state from the table 
     * complaintMaster by passing reviewPanelId and by calling 
     * findEntity method of super class i.e AbcAbstract
     * class 
     * returns List<TblComplaintMaster>
     */
    public List<TblComplaintMaster> getPendingComplaintsByReviewPanel(Object... values) throws Exception {
        return complaintMasterDao.getPendingComplaintsByReviewPanel(values);
    }

    /**
     * This is the method to get the HOPE governmentUserId
     * from the employeeTrasfer table by passing officeId and
     * role as the two parameters
     * Native query is used to retrieve the governmentUserId
     * return type is listObject
     */
    public List<Object> getGovtUserDetails(int officeId, String role) {
        log.error("in complaint service govuser>>>>>>>>>getGovtUserDetails(int officerId, String role)");
        return complaintMasterDao.getGovtUserDetails(officeId, role);
    }

    /**
     * This method is used to retrieve list of complaints
     * for DGCPTU from the table complaintMaster
     * returns List<TblComplaintMaster>
     */
    public List<TblComplaintMaster> getAllComplaints() throws Exception {
        log.error("Branch maker / checker complaint in complaintmgmtserviceimpl class");
        return complaintMasterDao.getAllComplaints();
    }

    /**
     * This method is used to retrieve the officeId of the tenderer
     * from tenderDetails table by passing tenderId as parameter
     * returns List<TblTenderDetails>
     */
    public List<TblTenderDetails> findOffice(Object... values) throws Exception {
        log.error("in compl mgmt service");
        return tblTenderDetailsDao.findTblTenderDetails(values);
    }

    /**
     * This method is used to retrieve the officeHierarchies from the
     * employeeOffices table by passing officeId as the parameter
     * returns listObject
     */
    public List<Object> getOfficeHirarchies(int officeid) throws Exception {
        return tblEmployeeOffices.getOfficeHirarchies(officeid, null);
    }

    /**
     * This method is used to add the payment details in the table
     * complaintPayment by passing the instance of TblComplaintPayments
     */
    public void addComplaintPayment(TblComplaintPayments payment) {
        complaintPaymentDao.addComplaintPayment(payment);
    }

    /**
     * This method is used to update the payment details in the table
     * complaintPayment by passing the instance of TblComplaintPayments
     */
    public void updateComplaintPayment(TblComplaintPayments payment) {
        complaintPaymentDao.updateComplaintPayment(payment);
    }

    /**
     * This method is used to retrieve record of the complaint
     * from complaintPayment table based on complaintPaymentId
     * returns List<TblComplaintPayments>
     */
    public List<TblComplaintPayments> findComplaintPayments(Object... values) throws Exception {
        return complaintPaymentDao.findComplaintPayments(values);
    }

    /**
     * This is the method to search all the complaints for HOPE
     * by passing the parameters tenderId,complaintId,details,govUserId,
     * role,status,complaintFeePaid from complaintMaster table
     * returns List<TblComplaintMaster>
     */
    public List<TblComplaintMaster> searchComplaintDetails(int tenderId, int complaintId, TblTenderDetails details, int govUserId, String role, String status, boolean complaintFeePaid, int panelId) throws Exception {
        return complaintMasterDao.searchComplaintDetails(tenderId, complaintId, details, govUserId, role, status, complaintFeePaid, panelId);
    }

    /**
     *
     */
    public List<Object[]> getTenderDetails(int getTenderDetails) throws Exception {
        return complaintMasterDao.getTenderDetails(getTenderDetails);
    }

    /**
     * not used
     */
    public List<TblLoginMaster> findTblLoginMaster(Object... values) throws Exception {
        return tblLoginMasterDao.findTblLoginMaster(values);
    }

    /**
     * This method is used to retrieve the governmentUserId of the 
     * user who has logged in the session 
     * from employeeTrasfer table by passing the userId(retrieved from session) 
     * return type is array listObject
     */
    public List<Object[]> getGovUserDetails(int userId) throws Exception {
        return complaintMasterDao.getGovUserDetails(userId);
    }

    /**
     *  This is the method to retrieve the government secretaries
     *  from employeeTrasfer table
     *  by passing array of officeId's and role as parameters 
     *   return type is listObject
     */
    public List<Object> getGovUserSecretaries(int[] officeIds, String role) throws Exception {
        return complaintMasterDao.getGovUserSecretaries(officeIds, role);
    }

    /**
     *
     */
    public List<TblTenderDetails> getTenderDetailsByRef(Object... values) throws Exception {
        return tblTenderDetailsDao.findTblTenderDetails(values);
    }

    /**
     * This method is used to retreive a particular document from complaintDocs
     * table by passing the complaintDocId as the parameter
     * returns List<TblComplaintDocs>
     */
    public List<TblComplaintDocs> findByDocId(Object... values) throws Exception {
        return complaintDocsDao.getComplaintDocs(values);
    }

    /**
     * This method is used to delete a particular document from complaintDocs
     * table by passing the instance of TblComplaintDocs as the parameter
     */
    public void deleteDoc(TblComplaintDocs doc) throws Exception {
        complaintDocsDao.deleteTblComplaintDocs(doc);
    }

    /**
     * This method is used to search for the complaints when branchMaker logsIn 
     * by passing the params
     * String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy, String paymentFor
     * return List<TblComplaintPayments>
     */
    public List<SearchComplaintPaymentBean> searchComplaintPayments(String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy, String paymentFor, boolean verifyFlag, int tenderId, String tenderer) throws Exception {
        return complaintMasterDao.searchComplaintPayments(emailId, verificationStatus, paymentFromDate, paymentToDate, createdBy, paymentFor, verifyFlag, tenderId, tenderer);
    }

    /**
     * This method is to add reviewPanel details in the
     * table reviewPanel by passing the instance of TblReviewPanel
     */
    public void addReviewPanel(TblReviewPanel reviewPanel) {
        tblReviewPanelDao.addReviewPanel(reviewPanel);
    }

    /**
     * This method is to add reviewPanel details in the
     * table loginMaster by passing the instance of TblLoginMaster
     */
    public void addTblLoginMaster(TblLoginMaster login) {
        tblLoginMasterDao.addTblLoginMaster(login);
    }

    /**
     * This method is to add reviewPanel details in the
     * table employeeMaster by passing the instance of TblEmployeeMaster
     */
    public void addEmployee(TblEmployeeMaster tblEmployeeMaster) {
        tblEmployeeMasterDao.addEmployee(tblEmployeeMaster);
    }

    /**
     * This method is to add reviewPanel details in the
     * table employeeTrasfer by passing the instance of TblEmployeeTrasfer
     */
    public void addTblEmployeeTrasfer(TblEmployeeTrasfer master) {
        tblEmployeeTrasferDao.addTblEmployeeTrasfer(master);
    }

    /**
     * This method is used to retreive the complaint details
     * from the compliantHistory table by passing complaintId
     * and complaintLevelId as parameters
     * returns array of listObjects
     */
    public List<Object[]> getComplaintHistoryDetails(int id, int complaintLevelId) throws Exception {
        return complaintHistoryDao.getComplaintHistoryDetails(id, complaintLevelId);
    }

    /**
     * This method is used to retreive all the complaints related to PE
     * from complaintHistory table by passing tenderId
     * returns listObject
     */
    public List<Object[]> getComplaintHistoryPe(int tenderId) throws Exception {
        return complaintHistoryDao.getComplaintHistoryPe(tenderId);
    }
    public List<Object[]> getComplaintHistoryRP(int userId) throws Exception {
        return complaintHistoryDao.getComplaintHistoryRP(userId);
    }

    public List<Object[]> getComplaintTenderer(int tenderId, int userId) throws Exception {
        return complaintMasterDao.getComplaintTenderer(tenderId, userId);
    }

    public List<Object> getEmailId(int govUserId) throws Exception {
        return complaintMasterDao.getEmailId(govUserId);
    }

    public List<TblPartnerAdmin> getTblPartnerAdmin(int userId, boolean getAllFlag) throws Exception {

        return complaintMasterDao.getTblPartnerAdmin(userId, getAllFlag);
    }

    public void updateComplaintsForPayment() throws Exception {
        complaintMasterDao.updateComplaintsForPayment();
    }

    @Override
    public List<Object[]> getReviewPanel(int empId) throws Exception {
    log.debug("getReviewPanel");
        StringBuffer buffer = new StringBuffer();
        List<Object[]> objects = new ArrayList<Object[]>();
        try {
            buffer.append("select tlm.emailId as emailid,tlm.password as password,tem.employeeName as employeeName, ");
            buffer.append(" tem.employeeNameBangla as employeeNameBangla,tem.mobileNo as mobileNo,tem.nationalId as nationalId,tem.tblDesignationMaster.designationId,");
            buffer.append(" tr.reviewPanelName,tr.userId, tr.reviewMembers from TblLoginMaster tlm , TblEmployeeMaster tem,TblReviewPanel tr where tem.employeeId=" + empId + " ");//tlm.userId
            buffer.append(" and  tlm.userId = tem.tblLoginMaster.userId and tr.userId = tem.tblLoginMaster.userId  ");
            objects = hibernateQueryDao.createNewQuery(buffer.toString());
        } catch (Exception ex) {
            log.error("getReviewPanel");
        }
         log.debug("getReviewPanel");
        return objects;
    }

    @Override
    public int getEmpId(int rpId) throws Exception {
         log.debug("getEmpId");
        int empId=0;
        try{
            List<Object> list = hibernateQueryDao.getSingleColQuery("select te.employeeId from TblEmployeeMaster te,TblReviewPanel tr where tr.userId=te.tblLoginMaster.userId and tr.reviewPanelId="+rpId);
            if(list!=null && !list.isEmpty()){
                empId = (Integer)list.get(0);
            }
        }catch(Exception e){
             log.error("getEmpId");
        }
         log.debug("getEmpId");
        return empId;
    }

    @Override
    public void updateOrSaveReveiwPanel(TblReviewPanel reviewPanel) {
        this.tblReviewPanelDao.updateOrSaveReviewPanel(reviewPanel);
    }

    @Override
    public void updateEmpMaster(TblEmployeeMaster tblEmployeeMaster) {
        this.tblEmployeeMasterDao.updateEmployee(tblEmployeeMaster);
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetails(String query) throws Exception {
        return complaintMasterDao.searchPendingComplaintDetails(query);
    }

    @Override
    public List<TblComplaintMaster> searchProcessedComplaintDetails(String query) throws Exception {
       return complaintMasterDao.searchProcessedComplaintDetails(query);
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetailsForDGCPTU(int i) throws Exception {
        return complaintMasterDao.searchPendingComplaintDetailsForDGCPTU(i);
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetailsRP(int i) throws Exception {
        return complaintMasterDao.searchPendingComplaintDetailsRP(i);
    }

    @Override
    public List<TblComplaintMaster> searchProcessedComplaintDetailsRP(int i) throws Exception {
       return complaintMasterDao.searchProcessedComplaintDetailsRP(i);
    }

        /**
     * This method retreives the documents list based on complaintId,
     * complaintLevelId from complaintDocs table
     * returns listObjects
     */
    public List<Object[]> getDocsByComplaintId(int complaintId) throws Exception {
        return complaintDocsDao.getDocsByComplaintId(complaintId);
    }

    @Override
    public List<Object> getEmailIdFromRpId(int rpId) throws Exception {
         return complaintMasterDao.getEmailIdFromRpId(rpId);
    }

    @Override
    public List<SearchComplaintPaymentBean> searchComplaintPendingPayments(String verificationStatus, String paymentFor) throws Exception {
       return complaintMasterDao.searchComplaintPendingPayments(verificationStatus,paymentFor);
    }

    @Override
    public TblComplaintPayments getPaymentDetails(String feetype, Integer id, Integer compId) throws Exception {
        return complaintPaymentDao.getPaymentDetails(feetype,id,compId);
    }

    @Override
    public int getGovUserId() throws Exception {
        return complaintPaymentDao.getGovUserId();
    }
}
