/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

/**
 *Service is used For rss feed module
 * @author Administrator
 */
public interface RssService {

    /**
     * Get rss feed from sp
     * @param tag weekly or daily
     * @param path name of jsp page
     * @return RssFeed in String
     */
    public String getRssFeed(String tag,String path);
    /**
     * For set LoguserId
     * @param logUserId
     */
    public void setUserId(String logUserId);

}
