/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSDetails;
import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSBoQDetails;
import java.util.List;

/**
 * This method returns the all the records of shared CMS details only for RHD Dept.
 * @author Sudhir Chavhan
 */
public interface RHDIntegrationCMSDetailsService {

    public void setLogUserId(String logUserId);
    /***
     * This method returns the all the records of shared CMS details only for RHD Dept.
     * @return List of RHDIntegrationDetails
     * @throws Exception
     */
    public List<RHDIntegrationCMSDetails> getSharedCMSDetailForRHD() throws Exception;
    /*
     * This method returns BoQ data from TenderId
     */
    public List<RHDIntegrationCMSBoQDetails> getBoQForRHD(String tenderId) throws Exception;
    //public List<Object[]> getBoQForRHD(int tenderId) throws Exception;
}
