/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblConfigAmendment;
import com.cptu.egp.eps.model.table.TblConfigNoa;
import com.cptu.egp.eps.model.table.TblConfigPreTender;
import com.cptu.egp.eps.model.table.TblConfigProcurement;
import com.cptu.egp.eps.model.table.TblConfigStd;
import com.cptu.egp.eps.model.table.TblConfigTec;
import com.cptu.egp.eps.model.table.TblFinancialPowerByRole;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTenderTypes;
import java.util.List;

/**
 *
 * @author Naresh.Akurathi
 */
public interface ConfigPreTenderRuleService {

    /**
      * List of all TenderTypes
      * @return List of TblTenderTypes
    */
    public List<TblTenderTypes> getTenderTypes();

     /**
     *List of all ProcurementMethod
     * @return List of TblProcurementMethod
     */
    public List<TblProcurementMethod> getProcurementMethod();

    /**
     *List of all ProcurementNature
     * @return List of TblProcurementNature
     */
    public List<TblProcurementNature> getProcurementNature();

     /**
     *List of all ProcurementTypes
     * @return List of TblProcurementTypes
     */
    public List<TblProcurementTypes> getProcurementTypes();

    /**
     *Inserts PreTender Configuration
     * @param master
     * @return if error than returns error else nothing
     */
    public String addConfigPreTenderRule(TblConfigPreTender master);

    /**
      *Updates Pre Tender Configuration
      * @param master row to be saved in tbl_ConfigPreTender
      * @return if error than returns error else nothing
    */
    public String updateConfigPreTenderRule(TblConfigPreTender master);

    /**
     *Deletes all Pre Tender Configuration
     * @return if error than returns error else nothing
     */
    public String delAllConfigPreTenderrule();

    /**
     *Delete selected Pre Tender Configuration
     * @param master to be deleted
     * @return if error than returns error else nothing
     */
    public String dellConfigPreTenderrule(TblConfigPreTender master);

    /**
     *List of all Pre Tender Configuration
     * @return List of TblConfigPrefTender
     */
    public List<TblConfigPreTender> getAllConfigPreTenderRule();

    /**
     *List of Pre Tender Configuration based on id
     * @param id search criteria
     * @return List of TblConfigPrefTender
     */
    public List<TblConfigPreTender> getConfigPreTenderRule(int id);

    /**
     *List of all Tender Templates
     * @return List of TblTemplateMaster
     */
    public List<TblTemplateMaster> getAllTemplateMaster();

    /**
     *List of all Accepted Templates
     * @return List of TblTemplateMaster
     */
    public List<TblTemplateMaster> getAcceptTemplateMaster();

    /**
     *Inserting STD configuration
     * @param configStd value of tbl_ConfigStd
     * @return if error than returns error else nothing
     */
    public String addTblConfigStd(TblConfigStd configStd);

     /**
      *Deletes particular STD configuration
      * @param configStd instance to be deleted
      * @return if error than returns Not Deleted else Deleted
      */
    public String delConfigStd(TblConfigStd configStd);

    /**
     *Delete all STD configuration
     * @return if error than returns Not Deleted else Deleted
     */
    public String delAllConfigStd();

    /**
      *Fetch all STD Configuration
      * @return List of TblConfigStd
      */
    public List<TblConfigStd> getAllConfigStd();

   /**
      *Fetch STD Configuration by id
      * @param id search is to be done
      * @return List of TblConfigStd
      */
    public List<TblConfigStd> getConfigStd(int id);

    /**
      *Update TblConfigStd
      * @param configStd to be updated
      * @return if error than returns Updated else Not Updated
      */
    public String updateConfigStd(TblConfigStd configStd);

    /**
       *Inserting TEC Configuration
       * @param configTec to be inserted
       * @return if error than returns Not Added else Values Added
       */
    public String addTblConfigTec(TblConfigTec configTec);

    /**
      *List of TblConfigTec based on id
      * @param id to be searched on
      * @return List of TblConfigTec
      */
    public List<TblConfigTec> getTblConfigTec(int id);

    /**
      *List of TEC configuration based on committee type
     * @param commitType
     * @return List of TblConfigTec
      */
    public List<TblConfigTec> getCommiteeTypeData(Object commitType);

    /**
      *Delete TEC configuration based on configTec
      * @param configTec to be deleted
      * @return if error than returns Not Deleted else Deleted
      */
    public String delConfigTec(TblConfigTec configTec);

    /**
      *Delete all TEC Configuration
     * @param configTec
      */
    public void delAllConfigTec(TblConfigTec configTec);

    /**
      *Updates TEC Configuration based on configTec
      * @param configTec to be updated
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String updateConfigTec(TblConfigTec configTec);

    /**
      *Insert Amendment/Corrigendum Configuration
     * @param configAmnd
     * @return if error than returns Not Added else Values Value Added
      */
    public String addTblConfigAmendment(TblConfigAmendment configAmnd);

    /**
      *Get all Amendment/Corrigendum Configuration
      * @return List of TblConfigAmendment
      */
    public List<TblConfigAmendment> getTblConfigAmendment();

    /**
      *Get Amendment/Corrigendum Configuration based on id
      * @param id on which search is done
      * @return List of TblConfigAmendment
      */
    public List<TblConfigAmendment> getTblConfigAmendment(int id);

    /**
      *Delete Amendment/Corrigendum Configuration
      * @param configAmnd which is to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delConfingAmendment(TblConfigAmendment configAmnd);

    /**
      *Delete All Amendment/Corrigendum Configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delAllConfingAmendment();

    /**
      *Update Amendment/Corrigendum Configuration
      * @param configAmnd to be updated
      * @return if error than returns Not Updated else Values Updated
      */
    public String updateConfingAmendment(TblConfigAmendment configAmnd);

    /**
      *List of Budget Types
      * @return List of TblBudgetType
      */
    public List<TblBudgetType> getBudgetType();

    /**
      *Insert Procurement Configuration
      * @param configProcurement to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
    public String addConfigProcurement(TblConfigProcurement configProcurement);

    /**
      *Get all Procurement Configuration
      * @return List of TblConfigProcurement
      */
    public List<TblConfigProcurement> getConfigProcurement();

    /**
      *Get Procurement Configuration based on id
      * @param id to be searched
      * @return List of TblConfigProcurement
      */
    public List<TblConfigProcurement> getConfigProcurement(int id);

    /**
      *Deletes Procurement Configuration
      * @param tblConfigProcurement to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delConfigProcurement(TblConfigProcurement tblConfigProcurement);

    /**
      *Updates Procurement Configuration
      * @param tblConfigProcurement to be updated
      * @return if error than returns Not Updated else Values Updated
      */
    public String updateConfigProcurement(TblConfigProcurement tblConfigProcurement);

    //public String deleteConfigProcurement(TblConfigProcurement tblConfigProcurement);

    /**
      *Delete all Procurement Configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delAllConfigProcurement();

    /**
     * Count of Pre Tender Configuration
     * @return counts
     */
    public long getPreTenderCnt();

    /**
     *Count of STD configuration
     * @return count
     */
    public long getConfigSTDCnt();

    /**
     *TEC committee configuration count
     * @param type1 search criteria 1
     * @param type2search criteria 2
     * @return count
     */
    public long getConfigTECCnt(String type1,String type2);

    /**
     *Count of Amendment configuration
     * @return count
     */
    public long getConfigAmendmentCnt();

    /**
     *Count of Procurement Configuration
     * @return count
     */
    public long getConfigProcurementCnt();

    /**
      *Inserts NOA configuration
      * @param tblConfigNoa to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
    public String addConfigNoa(TblConfigNoa tblConfigNoa);

    /**
      *Get all NOA configuration
      * @return List of TblConfigNoa
      */
    public List<TblConfigNoa> getAllConfigNoa();

    /**
      *Deletes all NOA configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delAllConfigNoa();

    /**
      *Get all NOA configuration based on id
      * @param id to be searched
      * @return List of TblConfigNoa
      */
    public List<TblConfigNoa> getConfigNoa(int id);

    /**
      *Updates NOA configuration
      * @param tblConfigNoa to be updated
      * @return if error than returns Not Updated else Values Updated
      */
    public String updateConfigNoa(TblConfigNoa tblConfigNoa);

    /**
      *Deletes NOA configuration
      * @param tblConfigNoa to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delConfigNoa(TblConfigNoa tblConfigNoa);

    /**
     *count of NOA Configuration
     * @return count
     */
    public long getConfigNoaCount();

    /**
     *Get all Financial Years
     * @return List of TblFinancialYear
     */
    public List<TblFinancialYear> getFinancialYear();

    /**
     *Count of existing FinancialYear in DB
     * @return no of entries in DB
     */
    public long getFinancialYearCount();

    /**
      *Get Procurement Roles
      * @return List of TblProcurementRole
      */
    public List<TblProcurementRole> getProcureRole();

    /**
      *Inserts  Financial Power Based on Role
      * @param tblFinancialPowerByRole to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
    public String addTblFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole);

    /**
      *Deletes all Financial Power Based on Role
      * @return if error than returns Not Deleted else Values Deleted
      */
    public String delAllFinancialPowerByRole();

    /**
      *Get all Financial Power Based on Role
      * @return List of TblFinancialPowerByRole
      */
    public List<TblFinancialPowerByRole> getAllFinancialPowerByRole();

    /**
     *Count of Financial Power
     * @return count
     */
    public long getFinancialPowerByRoleCnt();

    /**
       *Get Financial Power Based on Role based on id
       * @param id to be searched on
       * @return List of TblFinancialPowerByRole
       */
    public List<TblFinancialPowerByRole> getFinancialPowerByRole(int id);

    /**
       *Deletes Financial Power Based on Role
       * @param tblFinancialPowerByRole to be deleted
       * @return if error than returns Not Deleted else Values Deleted
       */
    public String delFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole);

    /**
       *Updates Financial Power Based on Role
       * @param tblFinancialPowerByRole to be updated
       * @return if error than returns Not Updated else Values Updated
       */
    public String updateFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole);

    public void setLogUserId(String logUserId);

    /**
       *Get Tender Types based on id
       * @param tenderTypeId to be searched on
       * @return Tender Type
       */
    public String getTenderType(int tenderTypeId);


    /**
     *Check unique Procurement Method Configuration count
     * @param configProcurementId
     * @param budgetType
     * @param tenderType
     * @param procMethod
     * @param procType
     * @param procNature
     * @param typeofImergency
     * @param minvalue
     * @param maxvalue
     * @return count
     */
    public long procMethodUniqueConfigCount(String configProcurementId,String budgetType,String tenderType,String procMethod,String procType,String procNature,String typeofImergency,String minvalue,String maxvalue);
  
     /**
     *Check unique Procurement Method Configuration count
     * @param budgetType
     * @param tenderType
     * @param procMethod
     * @param procType
     * @param procNature
     * @param typeofImergency
     * @param minvalue
     * @param maxvalue
     * @return count
     */
    public long procMethodUniqueConfigCount2(String budgetType,String tenderType,String procMethod,String procType,String procNature,String typeofImergency,String minvalue,String maxvalue);
  
    /**
     *
     * @param configProcurementId
     * @param budgetType
     * @param tenderType
     * @param procMethod
     * @param procType
     * @param procNature
     * @param typeofImergency
     * @param minvalue
     * @param maxvalue
     * @return
     */
    public long procMethodUniqueConfigCountEdit(String configProcurementId, String budgetType,String tenderType,String procMethod,String procType,String procNature,String typeofImergency,String minvalue,String maxvalue);
    /**
      *Check unique PreMethod Configuration count
     * @param preTenderMeetId
     * @param tenderType
     * @param procMethod
     * @param procType
     * @return count
     */
    public long preMethodUniqueConfigCount(String preTenderMeetId, String tenderType, String procMethod, String procType);
    
    /**
      *Check Amendment Configuration count
     * @param configAmendmentId
     * @param procType
     * @param procMethod
     * @return count
     */
    public long amendmentMethodUniqueConfigCount(String configAmendmentId,String procType, String procMethod);
    
    /**
     *Check TSC Configuration count
     * @param configtecId
     * @param commityType
     * @param procNature
     * @return count
     */
    public long tscformationMethodUniqueConfigCount(String configtecId,String commityType, String procNature);

    /**
     *Check STD Configuration count
     * @param configStdId
     * @param tenderType
     * @param procNature
     * @param procMethod
     * @param procType
     * @param operator
     * @param valueinbdtaka
     * @param stdname
     * @return count
     */
    public long stdselectionMethodUniqueConfigCount(String configStdId,String tenderType, String procNature,String procMethod,String procType, String operator,String valueinbdtaka,String stdname);

    /**
     *Check NOA Configuration count
     * @param configNoaId
     * @param procNature
     * @param procMethod
     * @param procType
     * @param minvalue
     * @param maxvalue
     * @return count
     */
    public long noabusinessMethodUniqueConfigCount(String configNoaId,String procNature, String procMethod,String procType,String minvalue,String maxvalue);

    /**
    *Check TOC Configuration count
     * @param confid
     * @param comitype
     * @param pNature
     * @return count
     */
    public long tocuniqueConfigCount(String confid,String comitype,String pNature);

    /**
     *Check TEC Configuration count
     * @param confid
     * @param comitype
     * @param pNature
     * @param minvalue
     * @param maxvalue
     * @return count
     */
    public long tecuniqueConfigCount(String confid,String comitype,String pNature,String minvalue,String maxvalue);

    
//    public long uniqueConfigCount(String confid,String comitype,String pNature) throws Exception{
//        return hibernateQueryDao.countForNewQuery("TblConfigTec tc,TblProcurementNature tp","tc.tblProcurementNature.procurementNatureId=tp.procurementNatureId and tc.committeeType='"+comitype+"' and tc.configTec not in ("+confid+") and tp.procurementNatureId="+pNature);
//     }
//
//     public long TECuniqueConfigCount(String confid,String comitype,String pNature) throws Exception{
//        return hibernateQueryDao.countForNewQuery("TblConfigTec tc,TblProcurementNature tp","tc.tblProcurementNature.procurementNatureId=tp.procurementNatureId and tc.committeeType='"+comitype+"' and tc.configTec not in ("+confid+") and tp.procurementNatureId="+pNature);
//     }

}
