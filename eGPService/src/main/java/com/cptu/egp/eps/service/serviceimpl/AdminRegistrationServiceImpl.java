/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblStateMasterImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAdminMasterDao;
//Edited by palash for PE Admin Transfer, Dohatec
import com.cptu.egp.eps.dao.daointerface.TblAdminTransferDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblOfficeMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblScBankDevPartnerMasterDao;
import com.cptu.egp.eps.dao.daointerface.VwGetAdminDao;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.model.table.TblAdminMaster;
//import com.cptu.egp.eps.model.table.TblAdminTransfer;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
//Edited by palash for PE Admin Transfer, Dohatec
import com.cptu.egp.eps.model.table.TblAdminTransfer;
import com.cptu.egp.eps.model.view.VwGetAdmin;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Sanjay
 */
public class AdminRegistrationServiceImpl implements AdminRegistrationService {

    final static Logger LOGGER = Logger.getLogger(AdminRegistrationServiceImpl.class);

    TblAdminMasterDao tblAdminMasterDao;
    //Edited by palash for PE Admin Transfer, Dohatec
    TblAdminTransferDao tblAdminTransferDao;
    TblLoginMasterDao tblLoginMasterDao;
    TblOfficeAdminDao tblOfficeAdminDao;
    TblDepartmentMasterDao tblDepartmentMasterDao;
    TblOfficeMasterDao tblOfficeMasterDao;
    TblScBankDevPartnerMasterDao tblScBankDevPartnerMasterDao;
    TblPartnerAdminDao tblPartnerAdminDao;
    HibernateQueryDao hibernateQueryDao;
    VwGetAdminDao vwGetAdminDao;
    SPAddUser sPAddUser;
    private TblStateMasterImpl tblStateMasterDao;
    private String logUserId = "0";
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblPartnerAdminDao getTblPartnerAdminDao() {
        return tblPartnerAdminDao;
    }

    public void setTblPartnerAdminDao(TblPartnerAdminDao tblPartnerAdminDao) {
        this.tblPartnerAdminDao = tblPartnerAdminDao;
    }

    public TblScBankDevPartnerMasterDao getTblScBankDevPartnerMasterDao() {
        return tblScBankDevPartnerMasterDao;
    }

    public void setTblScBankDevPartnerMasterDao(TblScBankDevPartnerMasterDao tblScBankDevPartnerMasterDao) {
        this.tblScBankDevPartnerMasterDao = tblScBankDevPartnerMasterDao;
    }

    public TblOfficeMasterDao getTblOfficeMasterDao() {
        return tblOfficeMasterDao;
    }

    public void setTblOfficeMasterDao(TblOfficeMasterDao tblOfficeMasterDao) {
        this.tblOfficeMasterDao = tblOfficeMasterDao;
    }

    public TblAdminMasterDao getTblAdminMasterDao() {
        return tblAdminMasterDao;
    }

    public void setTblAdminMasterDao(TblAdminMasterDao tblAdminMasterDao) {
        this.tblAdminMasterDao = tblAdminMasterDao;
    }
//Edited by palash for PE Admin Transfer, Dohatec

    public TblAdminTransferDao getTblAdminTransferDao() {
        return tblAdminTransferDao;
    }

    public void setTblAdminTransferDao(TblAdminTransferDao tblAdminTransferDao) {
        this.tblAdminTransferDao = tblAdminTransferDao;
    }

    //End
    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public TblOfficeAdminDao getTblOfficeAdminDao() {
        return tblOfficeAdminDao;
    }

    public void setTblOfficeAdminDao(TblOfficeAdminDao tblOfficeAdminDao) {
        this.tblOfficeAdminDao = tblOfficeAdminDao;
    }

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public VwGetAdminDao getVwGetAdminDao() {
        return vwGetAdminDao;
    }

    public void setVwGetAdminDao(VwGetAdminDao vwGetAdminDao) {
        this.vwGetAdminDao = vwGetAdminDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * insert values for PE admin
     *
     * @param tblAdminMaster
     * @param tblLoginMaster
     * @param str
     * @return true if inserted else false //Insert Data in to adminTransfer
     * table when register PE admin. Edited by palash for PE Admin Transfer,
     * Dohatec
     */
    @Override
    public Integer registerPEAdmin(TblAdminMaster tblAdminMaster, TblAdminTransfer tblAdminTransfer, TblLoginMaster tblLoginMaster, String[] str) {
        LOGGER.debug("registerPEAdmin : " + logUserId + loggerStart);
        int count = 0;
        String action = null;
        String query = "";
        try {
            //TblOfficeAdmin tblOfficeAdmin = new TblOfficeAdmin();
//            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            query = "INSERT INTO [dbo].[tbl_LoginMaster]"
                    + "([emailId],[password],[hintQuestion],[hintAnswer]"
                    + ",[registrationType],[isJvca]"
                    + ",[businessCountryName],[nextScreen]"
                    + ",[userTyperId],[isEmailVerified]"
                    + ",[failedAttempt],[validUpTo]"
                    + ",[firstLogin],[isPasswordReset],[resetPasswordCode]"
                    + ",[status],[registeredDate],[nationality],[transDate]"
                    + ",[countResetSMS])"
                    + "VALUES('" + tblLoginMaster.getEmailId() + "','" + tblLoginMaster.getPassword() + "','', '','"
                    + tblLoginMaster.getRegistrationType() + "','" + tblLoginMaster.getIsJvca() + "','"
                    + tblLoginMaster.getBusinessCountryName() + "','" + tblLoginMaster.getNextScreen() + "','"
                    + tblLoginMaster.getTblUserTypeMaster().getUserTypeId() + "','" + tblLoginMaster.getIsEmailVerified() + "','"
                    + tblLoginMaster.getFailedAttempt() + "',null,'" + tblLoginMaster.getFirstLogin() + "','"
                    + tblLoginMaster.getIsPasswordReset() + "','" + tblLoginMaster.getResetPasswordCode() + "','"
                    + tblLoginMaster.getStatus() + "',GETDATE(),'" + tblLoginMaster.getNationality() + "',"
                    + "null,0)";
            tblAdminMaster.setTblLoginMaster(tblLoginMaster);
            tblAdminTransfer.setTblLoginMaster(tblLoginMaster);
//            tblAdminMasterDao.addTblAdminMaster(tblAdminMaster);
            query = query + "DECLARE @v_UserId_Int INT"
                    + " SET @v_UserId_Int=IDENT_CURRENT('dbo.tbl_LoginMaster')"
                    + " INSERT INTO [dbo].[tbl_AdminMaster]"
                    + "([userId],[fullName]"
                    + ",[nationalId],[mobileNo]"
                    + ",[phoneNo],[rollId])"
                    + "VALUES(@v_UserId_Int,'" + tblAdminMaster.getFullName() + "','" + tblAdminMaster.getNationalId()
                    + "','" + tblAdminMaster.getMobileNo() + "','" + tblAdminMaster.getPhoneNo() + "','" + tblAdminMaster.getRollId()
                    + "')";
            tblAdminTransfer.setTblAdminMaster(tblAdminMaster);
//            tblAdminTransferDao.addTblAdminTransfer(tblAdminTransfer);
            query = query + "DECLARE @v_AdminId_Int INT "
                    + " SET @v_AdminId_Int=IDENT_CURRENT('dbo.tbl_AdminMaster')"
                    + " INSERT INTO [dbo].[tbl_AdminTransfer]"
                    + "([userId],[fullName],[nationalId]"
                    + ",[adminId],[emailId],[transferedBy]"
                    + ",[transferDt],[replacedBy],[replacedByEmailId]"
                    + ",[comments],[isCurrent],[mobileNo])"
                    + "VALUES(@v_UserId_Int,'" + tblAdminTransfer.getFullName() + "','" + tblAdminTransfer.getNationalId()
                    + "',@v_AdminId_Int,'" + tblAdminTransfer.getTblLoginMaster().getEmailId() + "',null,null, null,null,''"
                    + ",'" + tblAdminTransfer.getIsCurrent() + "','" + tblAdminTransfer.getMobileNo()
                    + "')";
            for (int i = 0; i < str.length; i++) {
//                tblOfficeAdmin.setTblLoginMaster(tblLoginMaster);
//                tblOfficeAdmin.setAdminType("PEAdmin");
//                tblOfficeAdmin.setTblOfficeMaster(new TblOfficeMaster(Integer.parseInt(str[i])));
//                tblOfficeAdminDao.addTblOfficeAdmin(tblOfficeAdmin);
                query = query + " INSERT INTO [dbo].[tbl_OfficeAdmin]"
                        + "([userId],[officeId],[adminType])"
                        + "VALUES(@v_UserId_Int," + Integer.parseInt(str[i]) + ",'PEAdmin')";

            }
//            count = tblLoginMaster.getUserId();
            CommonMsgChk commonMsgChk = null;//createGovtUserRole
            commonMsgChk = sPAddUser.executeProcedure(0, "createPAAdmin", query).get(0);
            if (commonMsgChk.getFlag() == true) {
                count = Integer.parseInt(commonMsgChk.getMsg());
            }
            action = "Create PA Admin";
        } catch (Exception e) {
            count = 0;
            LOGGER.error("registerPEAdmin : " + logUserId + e);
            action = "Error in Create PA Admin";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblLoginMaster.getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("registerPEAdmin : " + logUserId + loggerEnd);
        return count;
    }

    /*
     * Author : Sanjay Prajapati
     * Method : registerOrganizationAdmin() - To Register Organization Admin
     * Param : Object TblAdminMaster,Object TblLoginMaster, in officeId
     * Return : Boolean
     */
    @Override
    public Integer registerOrganizationAdmin(TblAdminMaster tblAdminMaster, TblLoginMaster tblLoginMaster, int officeId) throws Exception {
        LOGGER.debug("registerOrganizationAdmin : " + logUserId + loggerStart);
        int count = 0;
        String action = null;
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            tblAdminMaster.setTblLoginMaster(tblLoginMaster);
            tblAdminMasterDao.addTblAdminMaster(tblAdminMaster);
            TblDepartmentMaster tblDepartmentMaster = tblDepartmentMasterDao.findTblDepartmentMaster("departmentId", Operation_enum.EQ, (short) officeId).get(0);
            tblDepartmentMaster.setApprovingAuthorityId(tblLoginMaster.getUserId());
            TblDepartmentMaster departmentMaster = new TblDepartmentMaster();
            BeanUtils.copyProperties(tblDepartmentMaster, departmentMaster);
            departmentMaster.setTblEmployeeRoleses(null);
            tblDepartmentMasterDao.updateTblDepartmentMaster(departmentMaster);
            count = tblLoginMaster.getUserId();
            action = "Create Organization Admin";
        } catch (Exception e) {
            count = 0;
            LOGGER.error("registerOrganizationAdmin : " + logUserId + e);
            action = "Error in Create Organization Admin";
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblLoginMaster.getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("registerOrganizationAdmin : " + logUserId + loggerEnd);
        return count;
    }

    @Override
    public Integer registerContentAdmin(TblAdminMaster tblAdminMaster, TblLoginMaster tblLoginMaster, String... auditDetails) {
        LOGGER.debug("registerContentAdmin : " + logUserId + loggerStart);
        int count = 0;
        String action = null;
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            tblAdminMaster.setTblLoginMaster(tblLoginMaster);
            tblAdminMasterDao.addTblAdminMaster(tblAdminMaster);
            count = tblLoginMaster.getUserId();
            if (auditDetails != null && auditDetails.length != 0) {
                action = auditDetails[0];
            }
        } catch (Exception e) {
            count = 0;
            LOGGER.error("registerContentAdmin : " + logUserId + e);
            e.printStackTrace();
            if (auditDetails != null && auditDetails.length != 0) {
                action = "Error in " + auditDetails[0];
            }
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblLoginMaster.getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
            action = null;
        }
        LOGGER.debug("registerContentAdmin : " + logUserId + loggerEnd);
        return count;
    }

    @Override
    public List<TblDepartmentMaster> findTblOrgMaster(Object... values) throws Exception {
        LOGGER.debug("findTblOrgMaster : " + logUserId + loggerStart);
        List<TblDepartmentMaster> tblDepartmentMaster = null;
        try {
            tblDepartmentMaster = tblDepartmentMasterDao.findTblDepartmentMaster(values);
        } catch (Exception e) {
            LOGGER.error("findTblOrgMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblOrgMaster : " + logUserId + loggerEnd);
        return tblDepartmentMaster;
    }

    @Override
    public List<TblOfficeMaster> findTblOfficeMaster(Object... values) throws Exception {
        LOGGER.debug("findTblOfficeMaster : " + logUserId + loggerStart);
        List<TblOfficeMaster> tom = null;
        try {
            tom = tblOfficeMasterDao.findTblOfficeMaster(values);
        } catch (Exception e) {
            LOGGER.error("findTblOfficeMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblOfficeMaster : " + logUserId + loggerEnd);
        return tom;
    }

    @Override
    public List<TblScBankDevPartnerMaster> findTblBankMaster(Object... values) throws Exception {
        LOGGER.debug("findTblBankMaster : " + logUserId + loggerStart);
        List<TblScBankDevPartnerMaster> tsbdpm = null;
        try {
            tsbdpm = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster(values);
        } catch (Exception e) {
            LOGGER.error("findTblBankMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblBankMaster : " + logUserId + loggerEnd);
        return tsbdpm;
    }

    @Override
    public Integer registerScBankDevPartnerAdmin(TblLoginMaster tblLoginMaster, TblPartnerAdmin tblPartnerAdmin) {
        LOGGER.debug("registerScBankDevPartnerAdmin : " + logUserId + loggerStart);
        int count = 0;
        String action = null;
        try {

            if (tblLoginMaster.getTblUserTypeMaster() != null && tblLoginMaster.getTblUserTypeMaster().getUserTypeId() == 6) {
                action = "Create Development Partner Admin";
            } else if (tblLoginMaster.getTblUserTypeMaster() != null && tblLoginMaster.getTblUserTypeMaster().getUserTypeId() == 15) {
                action = "Create Scheduled Bank Branch Admin";
            } else if (tblLoginMaster.getTblUserTypeMaster() != null && tblLoginMaster.getTblUserTypeMaster().getUserTypeId() == 7) {
                action = "Create Scheduled Bank Admin";
            }

            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            tblPartnerAdmin.setTblLoginMaster(tblLoginMaster);
            tblPartnerAdminDao.addTblPartnerAdmin(tblPartnerAdmin);
            count = tblLoginMaster.getUserId();
        } catch (Exception e) {
            count = 0;
            action = "Error in " + action + " " + e.getMessage();
            LOGGER.error("registerScBankDevPartnerAdmin : " + logUserId + e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblLoginMaster.getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
        }
        LOGGER.debug("registerScBankDevPartnerAdmin : " + logUserId + loggerEnd);
        return count;
    }

    @Override
    public float getCntDepartmentMaster(String from, String where) throws Exception {
        LOGGER.debug("getCntDepartmentMaster : " + logUserId + loggerStart);
        float cntDeptMaster = 0.0f;
        try {
            cntDeptMaster = hibernateQueryDao.countForQuery(from, where);
        } catch (Exception e) {
            LOGGER.error("getCntDepartmentMaster : " + logUserId + e);
        }
        LOGGER.debug("getCntDepartmentMaster : " + logUserId + loggerEnd);
        return cntDeptMaster;
    }

    @Override
    public List<VwGetAdmin> findVwOrgMaster(int firstResult, int maxResult, Object... values) throws Exception {
        LOGGER.debug("findVwOrgMaster : " + logUserId + loggerStart);
        List<VwGetAdmin> vwGetAdmin = null;
        try {
            vwGetAdmin = vwGetAdminDao.findByVwGetAdmin(firstResult, maxResult, values);
        } catch (Exception e) {
            LOGGER.error("findVwOrgMaster : " + logUserId + e);
        }
        LOGGER.debug("findVwOrgMaster : " + logUserId + loggerEnd);
        return vwGetAdmin;
    }

    @Override
    public List<Object[]> getAdminOfficeList(int deptId) throws Exception {
        LOGGER.debug("getAdminOfficeList : " + logUserId + loggerStart);
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createNewQuery("select om.officeId,om.officeName from TblOfficeMaster om where om.officeId not in (select tom.tblOfficeMaster.officeId from TblOfficeAdmin tom,TblOfficeMaster om where om.officeId=tom.tblOfficeMaster.officeId and om.departmentId = " + deptId + ") and  om.departmentId = " + deptId);
        } catch (Exception e) {
            LOGGER.error("getAdminOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getAdminOfficeList : " + logUserId + loggerEnd);
        return obj;
    }

    public List<Object[]> getOfficeList(int deptId) throws Exception {
        LOGGER.debug("getAdminOfficeList : " + logUserId + loggerStart);
        List<Object[]> obj = null;
        try {
            obj = hibernateQueryDao.createNewQuery("select om.officeId,om.officeName from TblOfficeMaster om where om.officeId not in (select tom.tblOfficeMaster.officeId from TblOfficeAdmin tom,TblOfficeMaster om where om.officeId=tom.tblOfficeMaster.officeId and om.departmentId = " + deptId + ") and  om.departmentId = " + deptId);
        } catch (Exception e) {
            LOGGER.error("getAdminOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getAdminOfficeList : " + logUserId + loggerEnd);
        return obj;
    }

    /**
     * @return the tblStateMasterDao
     */
    public TblStateMasterImpl getTblStateMasterDao() {
        return tblStateMasterDao;
    }

    /**
     * @param tblStateMasterDao the tblStateMasterDao to set
     */
    public void setTblStateMasterDao(TblStateMasterImpl tblStateMasterDao) {
        this.tblStateMasterDao = tblStateMasterDao;
    }

    /**
     * fetching states from country
     *
     * @param countryId
     * @return list of states
     */
    public List<TblStateMaster> getBanglaStates(short countryId) {
        LOGGER.debug("getBanglaStates : " + logUserId + loggerStart);
        List<TblStateMaster> tblStateMasters = null;
        try {
            tblStateMasters = tblStateMasterDao.findTblStateMaster("tblCountryMaster", Operation_enum.EQ, new TblCountryMaster(countryId), "stateName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            LOGGER.error("getBanglaStates : " + logUserId + ex);
        }
        LOGGER.debug("getBanglaStates : " + logUserId + loggerEnd);
        return tblStateMasters;
    }

    public List<TblOfficeMaster> getOfficesByStateId(short statId, int deptId) {
        LOGGER.debug("getOfficesByStateId : " + logUserId + loggerStart);
        List<TblOfficeMaster> tblOfficeMaster = null;
        try {
            tblOfficeMaster = tblOfficeMasterDao.findTblOfficeMaster("tblStateMaster", Operation_enum.EQ, new TblStateMaster(statId), "departmentId", Operation_enum.EQ, deptId);

        } catch (Exception ex) {
            LOGGER.error("getOfficesByStateId : " + logUserId + ex);
        }
        LOGGER.debug("getOfficesByStateId : " + logUserId + loggerEnd);
        return tblOfficeMaster;
    }

    @Override
    public Short getDepartmentIdByUserId(int userId) throws Exception {
        LOGGER.debug("getDepartmentIdByUserId : " + logUserId + loggerStart);
        Short deptIdUserId = 0;
        try {
            String query = "select d.departmentId from TblDepartmentMaster d where d.approvingAuthorityId = " + userId;
            deptIdUserId = ((Short) (hibernateQueryDao.getSingleColQuery(query).toArray())[0]);
        } catch (Exception e) {
            LOGGER.error("getDepartmentIdByUserId : " + logUserId + e);
        }
        LOGGER.debug("getDepartmentIdByUserId : " + logUserId + loggerEnd);
        return deptIdUserId;
    }

    @Override
    public Boolean chkOfficeAdminCreated(int officeId) throws Exception {
        LOGGER.debug("chkOfficeAdminCreated : " + logUserId + loggerStart);
        Boolean isCreated = false;
        try {
            long cnt = hibernateQueryDao.countForNewQuery("TblOfficeAdmin", "tblOfficeMaster.officeId = " + officeId);
            if (cnt > 0) {
                isCreated = true;
            } else {
                isCreated = false;
            }
        } catch (Exception e) {
            LOGGER.error("chkOfficeAdminCreated : " + logUserId + e);
        }
        LOGGER.debug("chkOfficeAdminCreated : " + logUserId + loggerEnd);
        return isCreated;
    }

    @Override
    public String getDepartmentNameByUserId(int userId) throws Exception {
        LOGGER.debug("getDepartmentNameByUserId : " + logUserId + loggerStart);
        String string = null;
        try {
            String query = "select d.departmentName from TblDepartmentMaster d where d.approvingAuthorityId = " + userId;
            string = ((String) (hibernateQueryDao.getSingleColQuery(query).toArray())[0]);
        } catch (Exception e) {
            LOGGER.error("getDepartmentNameByUserId : " + logUserId + e);
        }
        LOGGER.debug("getDepartmentNameByUserId : " + logUserId + loggerEnd);
        return string;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * @return the sPAddUser
     */
    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    /**
     * @param sPAddUser the sPAddUser to set
     */
    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }
}
