/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBudgetTypeDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigAmendmentDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigNoaDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigPreTenderDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigProcurementDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigStdDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigTecDao;
import com.cptu.egp.eps.dao.daointerface.TblFinancialPowerByRoleDao;
import com.cptu.egp.eps.dao.daointerface.TblFinancialYearDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementMethodDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementNatureDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementRoleDao;
import com.cptu.egp.eps.dao.daointerface.TblProcurementTypesDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderTypesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblConfigAmendment;
import com.cptu.egp.eps.model.table.TblConfigNoa;
import com.cptu.egp.eps.model.table.TblConfigPreTender;
import com.cptu.egp.eps.model.table.TblConfigProcurement;
import com.cptu.egp.eps.model.table.TblConfigStd;
import com.cptu.egp.eps.model.table.TblConfigTec;
import com.cptu.egp.eps.model.table.TblFinancialPowerByRole;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTenderTypes;
import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ConfigPreTenderRuleServiceImpl implements ConfigPreTenderRuleService {

    static final Logger logger = Logger.getLogger(ConfigPreTenderRuleServiceImpl.class);
    HibernateQueryDao hibernateQueryDao;    
    TblTenderTypesDao tblTenderTypesDao;
    TblProcurementMethodDao tblProcurementMethodDao;
    TblProcurementNatureDao tblProcurementNatureDao;
    TblProcurementTypesDao tblProcurementTypesDao;
    TblTemplateMasterDao tblTemplateMasterDao;
    TblConfigPreTenderDao tblConfigPreTenderDao;
    TblConfigStdDao tblConfigStdDao;
    TblConfigTecDao tblConfigTecDao;
    TblConfigAmendmentDao tblConfigAmendmentDao;
    TblBudgetTypeDao tblBudgetTypeDao;
    TblConfigProcurementDao tblConfigProcurementDao;
    TblConfigNoaDao tblConfigNoaDao;
    TblFinancialYearDao tblFinancialYearDao;
    TblProcurementRoleDao tblProcurementRoleDao;
    TblFinancialPowerByRoleDao tblFinancialPowerByRoleDao;
    private String deleted = "Deleted";
    private String ntdeleted = "Not Deleted";
    private String logUserId = "0";
    private String starts = " Starts";
    private String ends = " Ends";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    public TblFinancialPowerByRoleDao getTblFinancialPowerByRoleDao() {
        return tblFinancialPowerByRoleDao;
    }

    public void setTblFinancialPowerByRoleDao(TblFinancialPowerByRoleDao tblFinancialPowerByRoleDao) {
        this.tblFinancialPowerByRoleDao = tblFinancialPowerByRoleDao;
    }

    public TblProcurementRoleDao getTblProcurementRoleDao() {
        return tblProcurementRoleDao;
    }

    public void setTblProcurementRoleDao(TblProcurementRoleDao tblProcurementRoleDao) {
        this.tblProcurementRoleDao = tblProcurementRoleDao;
    }

    public TblFinancialYearDao getTblFinancialYearDao() {
        return tblFinancialYearDao;
    }

    public void setTblFinancialYearDao(TblFinancialYearDao tblFinancialYearDao) {
        this.tblFinancialYearDao = tblFinancialYearDao;
    }

    public TblConfigNoaDao getTblConfigNoaDao() {
        return tblConfigNoaDao;
    }

    public void setTblConfigNoaDao(TblConfigNoaDao tblConfigNoaDao) {
        this.tblConfigNoaDao = tblConfigNoaDao;
    }

    public TblConfigProcurementDao getTblConfigProcurementDao() {
        return tblConfigProcurementDao;
    }

    public void setTblConfigProcurementDao(TblConfigProcurementDao tblConfigProcurementDao) {
        this.tblConfigProcurementDao = tblConfigProcurementDao;
    }

    public TblBudgetTypeDao getTblBudgetTypeDao() {
        return tblBudgetTypeDao;
    }

    public void setTblBudgetTypeDao(TblBudgetTypeDao tblBudgetTypeDao) {
        this.tblBudgetTypeDao = tblBudgetTypeDao;
    }

    public TblConfigAmendmentDao getTblConfigAmendmentDao() {
        return tblConfigAmendmentDao;
    }

    public void setTblConfigAmendmentDao(TblConfigAmendmentDao tblConfigAmendmentDao) {
        this.tblConfigAmendmentDao = tblConfigAmendmentDao;
    }

    public TblConfigTecDao getTblConfigTecDao() {
        return tblConfigTecDao;
    }

    public void setTblConfigTecDao(TblConfigTecDao tblConfigTecDao) {
        this.tblConfigTecDao = tblConfigTecDao;
    }

    public TblConfigStdDao getTblConfigStdDao() {
        return tblConfigStdDao;
    }

    public void setTblConfigStdDao(TblConfigStdDao tblConfigStdDao) {
        this.tblConfigStdDao = tblConfigStdDao;
    }

    public TblTemplateMasterDao getTblTemplateMasterDao() {
        return tblTemplateMasterDao;
    }

    public void setTblTemplateMasterDao(TblTemplateMasterDao tblTemplateMasterDao) {
        this.tblTemplateMasterDao = tblTemplateMasterDao;
    }

    public TblConfigPreTenderDao getTblConfigPreTenderDao() {
        return tblConfigPreTenderDao;
    }

    public void setTblConfigPreTenderDao(TblConfigPreTenderDao tblConfigPreTenderDao) {
        this.tblConfigPreTenderDao = tblConfigPreTenderDao;
    }

    public TblProcurementTypesDao getTblProcurementTypesDao() {
        return tblProcurementTypesDao;
    }

    public void setTblProcurementTypesDao(TblProcurementTypesDao tblProcurementTypesDao) {
        this.tblProcurementTypesDao = tblProcurementTypesDao;
    }

    public TblProcurementNatureDao getTblProcurementNatureDao() {
        return tblProcurementNatureDao;
    }

    public void setTblProcurementNatureDao(TblProcurementNatureDao tblProcurementNatureDao) {
        this.tblProcurementNatureDao = tblProcurementNatureDao;
    }

    public TblProcurementMethodDao getTblProcurementMethodDao() {
        return tblProcurementMethodDao;
    }

    public void setTblProcurementMethodDao(TblProcurementMethodDao tblProcurementMethodDao) {
        this.tblProcurementMethodDao = tblProcurementMethodDao;
    }

    public TblTenderTypesDao getTblTenderTypesDao() {
        return tblTenderTypesDao;
    }

    public void setTblTenderTypesDao(TblTenderTypesDao tblTenderTypesDao) {
        this.tblTenderTypesDao = tblTenderTypesDao;
    }

    @Override
    public List<TblTenderTypes> getTenderTypes() {

        return tblTenderTypesDao.getAllTblTenderTypes();
        //return null;
    }

    @Override
    public List<TblProcurementMethod> getProcurementMethod() {
        return tblProcurementMethodDao.getAllTblProcurementMethod();
        //return null;
    }

    @Override
    public List<TblProcurementNature> getProcurementNature() {
        return tblProcurementNatureDao.getAllTblProcurementNature();
        //return null;
    }

    @Override
    public List<TblProcurementTypes> getProcurementTypes() {
        return tblProcurementTypesDao.getAllTblProcurementTypes();
    }

    @Override
    public String addConfigPreTenderRule(TblConfigPreTender master) {
        logger.debug("addConfigPreTenderRule : " + logUserId + starts);
        String msg = null;
        try {
            tblConfigPreTenderDao.addTblConfigPreTender(master);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addConfigPreTenderRule : " + logUserId + " " + e);
            msg = "Not Added";
        }
        logger.debug("addConfigPreTenderRule : " + logUserId + ends);
        return msg;

    }

    @Override
    public String delAllConfigPreTenderrule() {
        logger.debug("delAllConfigPreTenderrule : " + logUserId + starts);
        String msg = null;
        try {
            Collection<TblConfigPreTender> col = getTblConfigPreTenderDao().getAllTblConfigPreTender();
            getTblConfigPreTenderDao().deleteAllTblConfigPreTender(col);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllConfigPreTenderrule : " + logUserId + " " + e);
            msg = "Error ..";
        }
        logger.debug("delAllConfigPreTenderrule : " + logUserId + ends);
        return msg;
        }

    @Override
    public String dellConfigPreTenderrule(TblConfigPreTender master) {
        logger.debug("dellConfigPreTenderrule : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigPreTenderDao().deleteTblConfigPreTender(master);
            msg = deleted;
        } catch (Exception e) {
            logger.error("dellConfigPreTenderrule : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("dellConfigPreTenderrule : " + logUserId + ends);
        return msg;
    }

    @Override
    public List<TblConfigPreTender> getAllConfigPreTenderRule() {
        logger.debug("getAllConfigPreTenderRule : " + logUserId + starts);
        List<TblConfigPreTender> list = null;

        try {
            list = getTblConfigPreTenderDao().getAllTblConfigPreTender();
        } catch (Exception e) {
            logger.error("getAllConfigPreTenderRule : " + logUserId + " " + e);

        }
        logger.debug("getAllConfigPreTenderRule : " + logUserId + ends);
        return list;
        }

    @Override
    public List<TblConfigPreTender> getConfigPreTenderRule(int id) {
        logger.debug("getConfigPreTenderRule : " + logUserId + starts);
        List<TblConfigPreTender> list = null;
        try {
            list = getTblConfigPreTenderDao().findTblConfigPreTender("preTenderMeetId", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getConfigPreTenderRule : " + logUserId + " " + e);
            list = null;
        }
        logger.debug("getConfigPreTenderRule : " + logUserId + ends);
        return list;
        }

    @Override
    public String updateConfigPreTenderRule(TblConfigPreTender master) {
        logger.debug("updateConfigPreTenderRule : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigPreTenderDao().updateTblConfigPreTender(master);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfigPreTenderRule : " + logUserId + " " + e);
            msg = "Error at updating";
        }
        logger.debug("updateConfigPreTenderRule : " + logUserId + ends);
        return msg;
        }

    @Override
    public List<TblTemplateMaster> getAllTemplateMaster() {
        logger.debug("getAllTemplateMaster : " + logUserId + starts);
        List<TblTemplateMaster> list = null;
        try {
            list = getTblTemplateMasterDao().getAllTblTemplateMaster();

        } catch (Exception e) {
            logger.error("getAllTemplateMaster : " + logUserId + " " + e);
        }
        logger.debug("getAllTemplateMaster : " + logUserId + ends);
        return list;
        }

    @Override
    public List<TblTemplateMaster> getAcceptTemplateMaster() {
        logger.debug("getAcceptTemplateMaster : " + logUserId + starts);
        List<TblTemplateMaster> list = null;
        try {
            list = getTblTemplateMasterDao().findTblTemplateMaster("status", Operation_enum.EQ, "A");
        } catch (Exception e) {
            logger.error("getAcceptTemplateMaster : " + logUserId + " " + e);
        }
        logger.debug("getAcceptTemplateMaster : " + logUserId + ends);
        return list;
    }

    @Override
    public String addTblConfigStd(TblConfigStd configStd) {
        logger.debug("addTblConfigStd : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigStdDao().addTblConfigStd(configStd);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addTblConfigStd : " + logUserId + " " + e);
            msg = "Not added";
        }
        logger.debug("addTblConfigStd : " + logUserId + ends);
        return msg;
        }

    @Override
    public String delConfigStd(TblConfigStd configStd) {
        logger.debug("delConfigStd : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigStdDao().deleteTblConfigStd(configStd);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delConfigStd : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delConfigStd : " + logUserId + ends);
        return msg;
    }

    @Override
    public String delAllConfigStd() {
        logger.debug("delAllConfigStd : " + logUserId + starts);
        String msg = null;
        try {
            List<TblConfigStd> lst = getTblConfigStdDao().getAllTblConfigStd();
            getTblConfigStdDao().deleteAllTblConfigStd(lst);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllConfigStd : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delAllConfigStd : " + logUserId + ends);
        return msg;
        }

    @Override
    public List<TblConfigStd> getAllConfigStd() {
        logger.debug("getAllConfigStd : " + logUserId + starts);
        List<TblConfigStd> list = null;
        try {

            list = getTblConfigStdDao().getAllTblConfigStd();
        } catch (Exception e) {
            logger.error("getAllConfigStd : " + logUserId + " " + e);
        }
        logger.debug("getAllConfigStd : " + logUserId + ends);
        return list;
        }

    @Override
    public List<TblConfigStd> getConfigStd(int id) {
        logger.debug("getConfigStd : " + logUserId + starts);
        List<TblConfigStd> list = null;
        try {
            list = getTblConfigStdDao().findTblConfigStd("configStdId", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getConfigStd : " + logUserId + " " + e);

        }
        logger.debug("getConfigStd : " + logUserId + ends);
        return list;
    }

    @Override
    public String updateConfigStd(TblConfigStd configStd) {
        logger.debug("updateConfigStd : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigStdDao().updateTblConfigStd(configStd);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfigStd : " + logUserId + " " + e);
            msg = "Not Updated";
        }
        logger.debug("updateConfigStd : " + logUserId + ends);
        return msg;
    }

    @Override
    public String addTblConfigTec(TblConfigTec configTec) {
        logger.debug("addTblConfigTec : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigTecDao().addTblConfigTec(configTec);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addTblConfigTec : " + logUserId + " " + e);
            msg = "Not Added";
        }
        logger.debug("addTblConfigTec : " + logUserId + ends);
        return msg;
        }

    @Override
    public List<TblConfigTec> getTblConfigTec(int id) {
        logger.debug("getTblConfigTec : " + logUserId + starts);
        List<TblConfigTec> list = null;
        try {
            list = getTblConfigTecDao().findTblConfigTec("configTec", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getTblConfigTec : " + logUserId + " " + e);
        }
        logger.debug("getTblConfigTec : " + logUserId + ends);
        return list;
    }

    @Override
    public List<TblConfigTec> getCommiteeTypeData(Object commitType) {
        logger.debug("getCommiteeTypeData : " + logUserId + starts);
        List<TblConfigTec> list = null;
        try {
            list = getTblConfigTecDao().findTblConfigTec("committeeType", Operation_enum.IN, commitType);
        } catch (Exception e) {
            logger.error("getCommiteeTypeData : " + logUserId + " " + e);
        }
        logger.debug("getCommiteeTypeData : " + logUserId + ends);
        return list;
        }

    @Override
    public String delConfigTec(TblConfigTec configTec) {
        logger.debug("delConfigTec : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigTecDao().deleteTblConfigTec(configTec);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delConfigTec : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delConfigTec : " + logUserId + ends);
        return msg;
    }

    @Override
    public String updateConfigTec(TblConfigTec configTec) {
        logger.debug("updateConfigTec : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigTecDao().updateTblConfigTec(configTec);
            ;
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfigTec : " + logUserId + " " + e);
            msg = "Not Updated";
        }
        logger.debug("updateConfigTec : " + logUserId + ends);
        return msg;
    }

    @Override
    public void delAllConfigTec(TblConfigTec configTec) {
        logger.debug("delAllConfigTec : " + logUserId + starts);
        try {
            getTblConfigTecDao().deleteTblConfigTec(configTec);
        } catch (Exception e) {
            logger.error("delAllConfigTec : " + logUserId + " " + e);
        }
        logger.debug("delAllConfigTec : " + logUserId + ends);
        }

    @Override
    public String addTblConfigAmendment(TblConfigAmendment configAmnd) {
        logger.debug("addTblConfigAmendment : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigAmendmentDao().addTblConfigAmendment(configAmnd);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addTblConfigAmendment : " + logUserId + " " + e);
            msg = "Not Added";
        }
        logger.debug("addTblConfigAmendment : " + logUserId + ends);
        return msg;
        }

    @Override
    public List<TblConfigAmendment> getTblConfigAmendment() {
        logger.debug("getTblConfigAmendment : " + logUserId + starts);
        List<TblConfigAmendment> list = null;
        try {
            list = getTblConfigAmendmentDao().getAllTblConfigAmendment();
        } catch (Exception e) {
            logger.error("getTblConfigAmendment : " + logUserId + " " + e);
        }
        logger.debug("getTblConfigAmendment : " + logUserId + ends);
        return list;
        }

    @Override
    public List<TblConfigAmendment> getTblConfigAmendment(int id) {
        logger.debug("getTblConfigAmendment : " + logUserId + starts);
        List<TblConfigAmendment> list = null;
        try {
            list = getTblConfigAmendmentDao().findTblConfigAmendment("configAmendmentId", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getTblConfigAmendment : " + logUserId + " " + e);
        }
        logger.debug("getTblConfigAmendment : " + logUserId + ends);
        return list;
        }

    @Override
    public String delConfingAmendment(TblConfigAmendment configAmnd) {
        logger.debug("delConfingAmendment : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigAmendmentDao().deleteTblConfigAmendment(configAmnd);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delConfingAmendment : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delConfingAmendment : " + logUserId + ends);
        return msg;

    }

    @Override
    public String delAllConfingAmendment() {
        logger.debug("delAllConfingAmendment : " + logUserId + starts);
        String msg = null;
        try {
            List<TblConfigAmendment> lst = getTblConfigAmendmentDao().getAllTblConfigAmendment();
            getTblConfigAmendmentDao().deleteAllTblConfigAmendment(lst);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllConfingAmendment : " + logUserId + " " + e);
            msg = "Not deleted due to Error";
        }
        logger.debug("delAllConfingAmendment : " + logUserId + ends);
        return msg;
        }

    @Override
    public String updateConfingAmendment(TblConfigAmendment configAmnd) {
        logger.debug("updateConfingAmendment : " + logUserId + starts);
        String msg = null;
        try {

            getTblConfigAmendmentDao().updateTblConfigAmendment(configAmnd);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfingAmendment : " + logUserId + " " + e);
            msg = "Not Updated";
        }
        logger.debug("updateConfingAmendment : " + logUserId + ends);
        return msg;
    }

    @Override
    public List<TblBudgetType> getBudgetType() {
        logger.debug("getBudgetType : " + logUserId + starts);
        List<TblBudgetType> list = null;
        try {
            list = getTblBudgetTypeDao().getAllTblBudgetType();
        } catch (Exception e) {
            logger.error("getBudgetType : " + logUserId + " " + e);
        }
        logger.debug("getBudgetType : " + logUserId + ends);
        return list;
        }

    @Override
    public String addConfigProcurement(TblConfigProcurement configProcurement) {
        logger.debug("addConfigProcurement : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigProcurementDao().addTblConfigProcurement(configProcurement);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addConfigProcurement : " + logUserId + " " + e);
            msg = "Not Added";
        }
        logger.debug("addConfigProcurement : " + logUserId + ends);
        return msg;
        }

    @Override
    public List<TblConfigProcurement> getConfigProcurement() {
        logger.debug("getConfigProcurement : " + logUserId + starts);
        List<TblConfigProcurement> list = null;
        try {
            list = getTblConfigProcurementDao().getAllTblConfigProcurement();
        } catch (Exception e) {
            logger.error("getConfigProcurement : " + logUserId + " " + e);
        }
        logger.debug("getConfigProcurement : " + logUserId + ends);
        return list;
        }

    @Override
    public String delAllConfigProcurement() {
        logger.debug("delAllConfigProcurement : " + logUserId + starts);
        String msg = null;
        try {
            List<TblConfigProcurement> col = getTblConfigProcurementDao().getAllTblConfigProcurement();
            getTblConfigProcurementDao().deleteAllTblConfigProcurement(col);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllConfigProcurement : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delAllConfigProcurement : " + logUserId + ends);
        return msg;
        }

    @Override
    public long getPreTenderCnt() {
        logger.debug("getPreTenderCnt : " + logUserId + starts);
        logger.debug("getPreTenderCnt: " + logUserId + ends);
        return tblConfigPreTenderDao.getTblConfigPreTenderCount();
    }

    @Override
    public long getConfigSTDCnt() {
        logger.debug("getConfigSTDCnt : " + logUserId + starts);
        logger.debug("getConfigSTDCnt : " + logUserId + ends);
        return tblConfigStdDao.getTblConfigStdCount();
    }

    @Override
    public long getConfigTECCnt(String type1, String type2) {
        logger.debug("getConfigTECCnt : " + logUserId + starts);

        int cnt = 0;
        Object[] para = {type1, type2};
        if (type2 == null) {
            Object[] singlePara = {type1};
            para = singlePara;
        }
        Object[] values = {"committeeType", Operation_enum.IN, para};
        try {
            List<TblConfigTec> configTecList = tblConfigTecDao.findTblConfigTec(values);
            if (configTecList != null && !configTecList.isEmpty()) {
                cnt = configTecList.size();
            }
        } catch (Exception ex) {
            logger.error("getConfigTECCnt : " + logUserId + " " + ex);
        }
        logger.debug("getConfigTECCnt : " + logUserId + ends);
        return cnt;
        }

    @Override
    public long getConfigAmendmentCnt() {
        logger.debug("getConfigAmendmentCnt : " + logUserId + starts);
        logger.debug("getConfigAmendmentCnt : " + logUserId + ends);
        return tblConfigAmendmentDao.getTblConfigAmendmentCount();
    }

    @Override
    public long getConfigProcurementCnt() {
        logger.debug("getConfigProcurementCnt : " + logUserId + starts);
        logger.debug("getConfigProcurementCnt : " + logUserId + ends);
        return tblConfigProcurementDao.getTblConfigProcurementCount();
    }

     @Override
    public List<TblConfigProcurement> getConfigProcurement(int id) {
        logger.debug("getConfigProcurement : " + logUserId + starts);
        List<TblConfigProcurement> list = null;
        try {
            list = getTblConfigProcurementDao().findEntity("configProcurementId", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getConfigProcurement : " + logUserId + " " + e);
        }
        logger.debug("getConfigProcurement : " + logUserId + ends);
        return list;
    }

     @Override
    public String delConfigProcurement(TblConfigProcurement tblConfigProcurement) {
        logger.debug("delConfigProcurement : " + logUserId + starts);
        String msg = null;
        try {
             getTblConfigProcurementDao().deleteTblConfigProcurement(tblConfigProcurement);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delConfigProcurement : " + logUserId + " " + e);
            msg = ntdeleted;
    }
        logger.debug("delConfigProcurement : " + logUserId + ends);
        return msg;
    }

      @Override
    public String updateConfigProcurement(TblConfigProcurement tblConfigProcurement) {
        logger.debug("updateConfigProcurement : " + logUserId + starts);
        String msg = null;
        try {
             getTblConfigProcurementDao().updateTblConfigProcurement(tblConfigProcurement);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfigProcurement : " + logUserId + " " + e);
            msg = "Not Updated";
         }
        logger.debug("updateConfigProcurement : " + logUserId + ends);
        return msg;
      }

    @Override
    public String addConfigNoa(TblConfigNoa tblConfigNoa) {
        logger.debug("addConfigNoa : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigNoaDao().addTblConfigNoa(tblConfigNoa);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addConfigNoa : " + logUserId + " " + e);
            msg = "Not Added";
        }
        logger.debug("addConfigNoa : " + logUserId + ends);
        return msg;
    }

    @Override
    public List<TblConfigNoa> getAllConfigNoa() {
        logger.debug("getAllConfigNoa : " + logUserId + starts);
        List<TblConfigNoa> lst = null;
        try {
            lst = getTblConfigNoaDao().getAllTblConfigNoa();
        } catch (Exception e) {
            logger.error("getAllConfigNoa : " + logUserId + " " + e);
        }
        logger.debug("getAllConfigNoa : " + logUserId + ends);
        return lst;
    }

    @Override
    public String delAllConfigNoa() {
        logger.debug("delAllConfigNoa : " + logUserId + starts);
        String msg = null;
        try {
            List<TblConfigNoa> lst = getTblConfigNoaDao().getAllTblConfigNoa();
            getTblConfigNoaDao().deleteAllTblConfigNoa(lst);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllConfigNoa : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delAllConfigNoa : " + logUserId + ends);
        return msg;
    }

    @Override
    public List<TblConfigNoa> getConfigNoa(int id) {
        logger.debug("getConfigNoa : " + logUserId + starts);
        List<TblConfigNoa> list = null;
        try {
            list = getTblConfigNoaDao().findTblConfigNoa("configNoaId", Operation_enum.EQ, id);

        } catch (Exception e) {
            logger.error("getConfigNoa : " + logUserId + " " + e);
        }
        logger.debug("getConfigNoa : " + logUserId + ends);
        return list;
    }

    @Override
    public String updateConfigNoa(TblConfigNoa tblConfigNoa) {
        logger.debug("updateConfigNoa : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigNoaDao().updateTblConfigNoa(tblConfigNoa);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateConfigNoa : " + logUserId + " " + e);
            msg = "Not Updated";
        }
        logger.debug("updateConfigNoa : " + logUserId + ends);
        return msg;
    }

    @Override
    public String delConfigNoa(TblConfigNoa tblConfigNoa) {
        logger.debug("delConfigNoa : " + logUserId + starts);
        String msg = null;
        try {
            getTblConfigNoaDao().deleteTblConfigNoa(tblConfigNoa);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delConfigNoa : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delConfigNoa : " + logUserId + ends);
        return msg;
    }

    @Override
    public long getConfigNoaCount() {
        logger.debug("getConfigNoaCount : " + logUserId + starts);
        logger.debug("getConfigNoaCount : " + logUserId + ends);
        return getTblConfigNoaDao().getTblConfigNoaCount();
    }

    @Override
    public List<TblFinancialYear> getFinancialYear() {
        logger.debug("getFinancialYear : " + logUserId + starts);
        logger.debug("getFinancialYear : " + logUserId + ends);
        return getTblFinancialYearDao().getAllTblFinancialYear();
    }

    @Override
    public long getFinancialYearCount() {
        logger.debug("getFinancialYearCount : " + logUserId + starts);
        logger.debug("getFinancialYearCount : " + logUserId + ends);
        return getTblFinancialYearDao().getTblFinancialYearCount();
    }

    @Override
    public List<TblProcurementRole> getProcureRole() {
        logger.debug("getProcureRole : " + logUserId + starts);
        List<TblProcurementRole> list = null;
        try {
            list = getTblProcurementRoleDao().getAllTblProcurementRole();
        } catch (Exception e) {
            logger.error("getProcureRole : " + logUserId + " " + e);
        }
        logger.debug("getProcureRole : " + logUserId + ends);
        return list;
    }

    @Override
    public String addTblFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
        logger.debug("addTblFinancialPowerByRole : " + logUserId + starts);
        String msg = null;
        try {
            getTblFinancialPowerByRoleDao().addTblFinancialPowerByRole(tblFinancialPowerByRole);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addTblFinancialPowerByRole : " + logUserId + " " + e);
            msg = "";
        }
        logger.debug("addTblFinancialPowerByRole : " + logUserId + ends);
        return msg;
    }

    @Override
    public String delAllFinancialPowerByRole() {
        logger.debug("delAllFinancialPowerByRole : " + logUserId + starts);
        String msg = null;
        try {
            List<TblFinancialPowerByRole> lst = getTblFinancialPowerByRoleDao().getAllTblFinancialPowerByRole();
            getTblFinancialPowerByRoleDao().deleteAll(lst);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delAllFinancialPowerByRole : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delAllFinancialPowerByRole : " + logUserId + ends);
        return msg;
    }

    @Override
    public List<TblFinancialPowerByRole> getAllFinancialPowerByRole() {
        logger.debug("getAllFinancialPowerByRole : " + logUserId + starts);
        logger.debug("getAllFinancialPowerByRole : " + logUserId + ends);
        return getTblFinancialPowerByRoleDao().getAllTblFinancialPowerByRole();
    }

    @Override
    public long getFinancialPowerByRoleCnt() {
        logger.debug("getFinancialPowerByRoleCnt : " + logUserId + starts);
        logger.debug("getFinancialPowerByRoleCnt : " + logUserId + ends);
        return getTblFinancialPowerByRoleDao().getTblFinancialPowerByRoleCount();
    }

    @Override
    public List<TblFinancialPowerByRole> getFinancialPowerByRole(int id) {
        logger.debug("getFinancialPowerByRole : " + logUserId + starts);
        List<TblFinancialPowerByRole> list = null;
        try {
            list = getTblFinancialPowerByRoleDao().findTblFinancialPowerByRole("financialPowerByRoleId", Operation_enum.EQ, id);
        } catch (Exception e) {
            logger.error("getFinancialPowerByRole : " + logUserId + " " + e);
        }
        logger.debug("getFinancialPowerByRole : " + logUserId + ends);
        return list;
    }

    @Override
    public String delFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
        logger.debug("delFinancialPowerByRole : " + logUserId + starts);
        String msg = null;
        try {
            getTblFinancialPowerByRoleDao().deleteTblFinancialPowerByRole(tblFinancialPowerByRole);
            msg = deleted;
        } catch (Exception e) {
            logger.error("delFinancialPowerByRole : " + logUserId + " " + e);
            msg = ntdeleted;
        }
        logger.debug("delFinancialPowerByRole : " + logUserId + ends);
        return msg;
    }

    @Override
    public String updateFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
        logger.debug("updateFinancialPowerByRole : " + logUserId + starts);
        String msg = null;
        try {
            getTblFinancialPowerByRoleDao().updateTblFinancialPowerByRole(tblFinancialPowerByRole);
            msg = "Updated";
        } catch (Exception e) {
            logger.error("updateFinancialPowerByRole : " + logUserId + " " + e);
            msg = "Not Updated";
        }
        logger.debug("updateFinancialPowerByRole : " + logUserId + ends);
        return msg;
    }

    @Override
    public String getTenderType(int tenderTypeId) {
        logger.debug("getTenderType : " + logUserId + starts);
        List<TblTenderTypes> list = null;
        String tenderType = "";
        try {

            list = (List<TblTenderTypes>) tblTenderTypesDao.findTblTenderTypes("tenderTypeId",Operation_enum.EQ,Byte.parseByte(tenderTypeId+""));
            if(!list.isEmpty())
            {
                tenderType = list.get(0).getTenderType();
            }
        } catch (Exception e) {
            logger.error("getTenderType : " + logUserId + " " + e);
           
        }
        logger.debug("getTenderType : " + logUserId + ends);
        return tenderType;
    }


    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public long procMethodUniqueConfigCount(String configProcurementId, String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) {
        logger.debug("procMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigProcurement tcp", "tcp.configProcurementId not in ('" + configProcurementId + "') and tcp.tblBudgetType.budgetTypeId='" + budgetType + "' and tcp.tblTenderTypes.tenderTypeId='" + tenderType + "' and tcp.tblProcurementMethod.procurementMethodId='" + procMethod + "' and tcp.tblProcurementTypes.procurementTypeId='" + procType + "' and tcp.tblProcurementNature.procurementNatureId='" + procNature + "' and tcp.isNationalDisaster='" + typeofImergency + "'");// and tcp.minValue='"+minvalue+"' and tcp.maxValue='"+maxvalue+"'");
            //return hibernateQueryDao.countForNewQuery("TblConfigTec tc,TblProcurementNature tp","tc.tblProcurementNature.procurementNatureId=tp.procurementNatureId and tc.committeeType='"+comitype+"' and tc.configTec not in ("+confid+") and tp.procurementNatureId="+pNature);
        } catch (Exception ex) {
            logger.error("procMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("procMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }
    
    @Override
    public long procMethodUniqueConfigCount2(String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) {
        logger.debug("procMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            String query = "";
            query = "SELECT * FROM tbl_ConfigProcurement WHERE budgetTypeId = '" + budgetType + "' and tenderTypeId= '"+ tenderType +"' and procurementMethodId= '"+ procMethod +"' and procurementTypeId ='"+procType+"' and procurementNatureId='"+procNature+"' and isNationalDisaster='"+typeofImergency+"'";
            List lstBidCat = hibernateQueryDao.nativeSQLQuery(query, null);
            if(lstBidCat.size()==0)
            {
                lng =0;
            }
            else
            {
                lng = lstBidCat.size();
            }
            //lng = hibernateQueryDao.countForNewQuery("TblConfigProcurement tcp", "tcp.tblBudgetType.budgetTypeId='" + budgetType + "' and tcp.tblTenderTypes.tenderTypeId='" + tenderType + "' and tcp.Area='" + Area + "' and tcp.tblProcurementMethod.procurementMethodId='" + procMethod + "' and tcp.tblProcurementTypes.procurementTypeId='" + procType + "' and tcp.tblProcurementNature.procurementNatureId='" + procNature + "' and tcp.isNationalDisaster='" + typeofImergency + "'");// and tcp.minValue='"+minvalue+"' and tcp.maxValue='"+maxvalue+"'");
            //return hibernateQueryDao.countForNewQuery("TblConfigTec tc,TblProcurementNature tp","tc.tblProcurementNature.procurementNatureId=tp.procurementNatureId and tc.committeeType='"+comitype+"' and tc.configTec not in ("+confid+") and tp.procurementNatureId="+pNature);
        } catch (Exception ex) {
            logger.error("procMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("procMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }
    
    @Override
    public long procMethodUniqueConfigCountEdit(String configProcurementId, String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) {
        logger.debug("procMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            String query = "";
            query = "SELECT * FROM tbl_ConfigProcurement WHERE configProcurementId not in ('" + configProcurementId + "') and budgetTypeId = '" + budgetType + "' and tenderTypeId= '"+ tenderType +"' and procurementMethodId= '"+ procMethod +"' and procurementTypeId ='"+procType+"' and procurementNatureId='"+procNature+"' and isNationalDisaster='"+typeofImergency+"'";
            List lstBidCat = hibernateQueryDao.nativeSQLQuery(query, null);
            if(lstBidCat.size()==0)
            {
                lng =0;
            }
            else
            {
                lng = lstBidCat.size();
            }
            //lng = hibernateQueryDao.countForNewQuery("TblConfigProcurement tcp", "tcp.tblBudgetType.budgetTypeId='" + budgetType + "' and tcp.tblTenderTypes.tenderTypeId='" + tenderType + "' and tcp.Area='" + Area + "' and tcp.tblProcurementMethod.procurementMethodId='" + procMethod + "' and tcp.tblProcurementTypes.procurementTypeId='" + procType + "' and tcp.tblProcurementNature.procurementNatureId='" + procNature + "' and tcp.isNationalDisaster='" + typeofImergency + "'");// and tcp.minValue='"+minvalue+"' and tcp.maxValue='"+maxvalue+"'");
            //return hibernateQueryDao.countForNewQuery("TblConfigTec tc,TblProcurementNature tp","tc.tblProcurementNature.procurementNatureId=tp.procurementNatureId and tc.committeeType='"+comitype+"' and tc.configTec not in ("+confid+") and tp.procurementNatureId="+pNature);
        } catch (Exception ex) {
            logger.error("procMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("procMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long preMethodUniqueConfigCount(String preTenderMeetId, String tenderType, String procMethod, String procType) {
        logger.debug("preMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigPreTender tct", "tct.preTenderMeetId not in ('" + preTenderMeetId + "') and tct.tblTenderTypes.tenderTypeId='" + tenderType + "' and tct.tblProcurementMethod.procurementMethodId='" + procMethod + "' and tct.tblProcurementTypes.procurementTypeId='" + procType + "'");            
        } catch (Exception ex) {
            logger.error("preMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("preMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long amendmentMethodUniqueConfigCount(String configAmendmentId, String procType, String procMethod) {
        logger.debug("amendmentMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigAmendment tca", "tca.configAmendmentId not in ('" + configAmendmentId + "') and tca.tblProcurementTypes.procurementTypeId='" + procType + "' and tca.tblProcurementMethod.procurementMethodId='" + procMethod + "'");
        } catch (Exception ex) {
            logger.error("amendmentMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("amendmentMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long tscformationMethodUniqueConfigCount(String configtecId, String commityType, String procNature) {
        logger.debug("tscformationMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigTec tct", "tct.configTec not in ('" + configtecId + "') and tct.committeeType='" + commityType + "' and tct.tblProcurementNature.procurementNatureId='" + procNature + "'");
        } catch (Exception ex) {
            logger.error("tscformationMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("tscformationMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long stdselectionMethodUniqueConfigCount(String configStdId, String tenderType, String procNature, String procMethod, String procType, String operator,String valueinbdtaka,String stdname) {
       logger.debug("stdselectionMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigStd tcs", "tcs.configStdId not in ('" + configStdId + "') and tcs.tenderTypeId='" + tenderType + "' and tcs.tblProcurementNature.procurementNatureId='" + procNature + "' and tcs.tblProcurementMethod.procurementMethodId='"+procMethod+"' and tcs.tblProcurementTypes.procurementTypeId='"+procType+"'and tcs.operator='"+operator+"' and tcs.tenderValue='"+valueinbdtaka+"' and tcs.tblTemplateMaster.templateId='"+stdname+"'");
        } catch (Exception ex) {
            logger.error("stdselectionMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("stdselectionMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long noabusinessMethodUniqueConfigCount(String configNoaId, String procNature, String procMethod, String procType,String minvalue,String maxvalue) {
        logger.debug("noabusinessMethodUniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigNoa tcn", "tcn.configNoaId not in ('" + configNoaId + "') and tcn.tblProcurementNature.procurementNatureId='" + procNature + "' and tcn.tblProcurementMethod.procurementMethodId='"+procMethod+"' and tcn.tblProcurementTypes.procurementTypeId='"+procType+"' and tcn.minValue='"+minvalue+"' and tcn.maxValue='"+maxvalue+"'");
        } catch (Exception ex) {
            logger.error("noabusinessMethodUniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("noabusinessMethodUniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long tocuniqueConfigCount(String confid, String comitype, String pNature) {
        logger.debug("tocuniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigTec tc", "tc.configTec not in ('" + confid + "') and tc.committeeType='" + comitype + "' and tc.tblProcurementNature.procurementNatureId='"+pNature+"'");
        } catch (Exception ex) {
            logger.error("tocuniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("tocuniqueConfigCount : " + logUserId + ends);
        return lng;
    }

    @Override
    public long tecuniqueConfigCount(String confid, String comitype, String pNature,String minvalue,String maxvalue) {
        logger.debug("tecuniqueConfigCount : " + logUserId + starts);
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblConfigTec tc", "tc.configTec not in ('" + confid + "') and tc.committeeType='" + comitype + "' and tc.tblProcurementNature.procurementNatureId='"+pNature+"'");// and tc.minTenderVal'"+minvalue+"' and tc.maxTenderVal='"+maxvalue+"'");
        } catch (Exception ex) {
            logger.error("tecuniqueConfigCount : " + logUserId +ex);
        }
        logger.debug("tecuniqueConfigCount : " + logUserId + ends);
        return lng;
    }
}
