/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author Sanjay Prajapati
 */
public interface CommonXMLSPService {

    public void setLogUserId(String logUserId);
    
      public void setModuleName(String moduleName);

    /**
     * Inserting or updating values into specified table
     * @param action - weather it is update, delete or insert
     * @param tablename - name of table
     * @param xmlData - field name and values
     * @param conditions - where condition
     * @param values
     * @return List of CommonMsgChk
     * @throws Exception
     */
    public List<CommonMsgChk> insertDataBySP(String action,String tablename,String xmlData,String conditions,String ... values) throws Exception;
    
    /**
     *
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @param fieldName11
     * @param fieldName12
     * @param fieldName13
     * @param fieldName14
     * @param fieldName15
     * @param fieldName16
     * @param fieldName17
     * @param fieldName18
     * @param fieldName19
     * @param fieldName20
     * @return
     * @throws Exception
     */
    public List<CommonMsgChk> insertNOAAcceptance(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15, String fieldName16, String fieldName17, String fieldName18, String fieldName19, String fieldName20) throws Exception;

    /**
     * Set Audit Trail Object at Service layer.
     * @param Audittrail
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
