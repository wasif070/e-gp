/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.FinancialDelegationData;
import java.util.List;

/**
 *
 * @author Istiak (Dohatec) - Jun 14, 2015
 */
public interface FinancialDelegationService {

    public List<FinancialDelegationData> financialDelegationData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15);

    public int getFinancialDelegationCount();

    public boolean insertOrUpdate(String[] financialDeligationId, String[] newOldEdit, String[] budgetType, String[] tenderType, String[] procurementMethod, String[] procurementType, String[] procurementNature, String[] tenderEmergency, String[] minValueBDT, String[] maxValueBDT, String[] minProjectValueBDT, String[] maxProjectValueBDT, String[] isBoD, String[] isCorporation,String[] approvingAuthority,String[] peOfficeLevel);

    public int getDelegationRankCnt();

    public boolean rankInsertOrUpdate(String[] designation, String[] ranksId, String[] newOldEdit);

}
