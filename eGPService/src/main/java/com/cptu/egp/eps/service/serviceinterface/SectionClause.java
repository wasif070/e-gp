/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblIttClause;
import com.cptu.egp.eps.model.table.TblIttSubClause;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface SectionClause {
    /**
     * Get Section name with Header Id.
     * @param ittHeaderId
     * @return
     */
    public String getSectionName(int ittHeaderId);
    /**
     * Create Sub section clause
     * @param tblIttSubClause
     * @return true if created else false
     */
    public boolean createSubSectionClause(TblIttSubClause tblIttSubClause);
    /**
     * Create Sub section clause
     * @param tblIttClause
     * @return true if created else false
     */
    public boolean createSectionClause(TblIttClause tblIttClause);
    /**
     * Get Clause Details by header Id.
     * @param ittHeaderId
     * @return List of Clause Details
     */
    public List<Object[]> getClauseDetailsTDS(int ittHeaderId);
    /**
     * Get Clause Count By Header Id
     * @param ittHeaderId
     * @return count
     */
    public long getClauseCount(int ittHeaderId);
    /**
     * Get Sub clause Details.
     * @param ittClauseId
     * @return List of Sub clause Details.
     */
    public List<TblIttSubClause> getSubClauseDetailsTDS(int ittClauseId);
    /**
     * Get Clause Details.
     * @param ittHeaderId
     * @return List of clause Details.
     */
    public List<TblIttClause> getClauseDetails(int ittHeaderId);
    /**
     * Get Sub clause Details by clause Id.
     * @param ittClauseId
     * @return Sub clause Details.
     */
    public List<TblIttSubClause> getSubClauseDetails(int ittClauseId);
    /**
     * update Clause details.
     * @param tblIttClause
     * @return true if updated or false if not.
     */
    public boolean updateClause(TblIttClause tblIttClause);
    /**
     * update Sub Clause details.
     * @param tblIttSubClause
     * @return true if updated or false if not.
     */
    public boolean updateSubClause(TblIttSubClause tblIttSubClause);
    /**
     * delete Clause details.
     * @param tblIttClause
     * @return true if deleted or false if not.
     */
    public boolean deleteClause(TblIttClause tblIttClause);
    /**
     * delete Sub Clause details.
     * @param tblIttSubClause
     * @return true if deleted or false if not.
     */
    public boolean deleteSubClause(TblIttSubClause tblIttSubClause);
    /**
     * Set user Id at service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);

}
