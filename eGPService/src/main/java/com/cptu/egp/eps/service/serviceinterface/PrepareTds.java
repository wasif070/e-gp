/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTdsSubClause;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface PrepareTds {
    public int insertTdsSubClause(TblTdsSubClause tblTdsSubClauseList);
    public List<SPTenderCommonData> getTDSSubClause(int ittHeaderId);
    public boolean updateTDSSubClause(TblTdsSubClause tblTdsSubClause);
    public boolean deleteTDSSubClause(TblTdsSubClause tblTdsSubClause);
    public boolean isTdsSubClauseGenerated(int ittHeaderId);
    public void setLogUserId(String logUserId);
}
