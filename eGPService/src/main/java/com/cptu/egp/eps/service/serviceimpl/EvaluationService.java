/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.model.table.TblEvalReportClarification;
import java.util.List;
import com.cptu.egp.eps.dao.daointerface.TblEvalReportClarificationDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalReportDocsDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRptApproveDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRptForwardToAaDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRptSentToAaDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalTernotifyMemberDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalTscDocsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblEvalReportDocs;
import com.cptu.egp.eps.model.table.TblEvalRptForwardToAa;
import com.cptu.egp.eps.model.table.TblEvalRptSentToAa;
import com.cptu.egp.eps.model.table.TblEvalTernotifyMember;
import com.cptu.egp.eps.model.table.TblEvalTscDocs;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT,rishita
 */
public class EvaluationService {

    private HibernateQueryDao hibernateQueryDao;
    TblEvalReportClarificationDao tblEvalReportClarificationDao;
    TblEvalRptApproveDao tblEvalRptApproveDao;
    TblEvalRptSentToAaDao tblEvalRptSentToAaDao;
    TblEvalReportDocsDao tblEvalReportDocsDao;
    TblEvalTscDocsDao tblEvalTscDocsDao;
    TblEvalTernotifyMemberDao tblEvalTernotifyMemberDao;
    TblEvalRptForwardToAaDao tblEvalRptForwardToAaDao;
    private AuditTrail auditTrail;
    MakeAuditTrailService makeAuditTrailService;
    static final Logger logger = Logger.getLogger(EvaluationService.class);
    private String logUserId = "0";

    public TblEvalRptForwardToAaDao getTblEvalRptForwardToAaDao() {
        return tblEvalRptForwardToAaDao;
    }

    public void setTblEvalRptForwardToAaDao(TblEvalRptForwardToAaDao tblEvalRptForwardToAaDao) {
        this.tblEvalRptForwardToAaDao = tblEvalRptForwardToAaDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    public TblEvalTscDocsDao getTblEvalTscDocsDao() {
        return tblEvalTscDocsDao;
    }

    public TblEvalTernotifyMemberDao getTblEvalTernotifyMemberDao() {
        return tblEvalTernotifyMemberDao;
    }

    public void setTblEvalTernotifyMemberDao(TblEvalTernotifyMemberDao tblEvalTernotifyMemberDao) {
        this.tblEvalTernotifyMemberDao = tblEvalTernotifyMemberDao;
    }

    public void setTblEvalTscDocsDao(TblEvalTscDocsDao tblEvalTscDocsDao) {
        this.tblEvalTscDocsDao = tblEvalTscDocsDao;
    }

    public TblEvalReportClarificationDao getTblEvalReportClarificationDao() {
        return tblEvalReportClarificationDao;
    }

    public void setTblEvalReportClarificationDao(TblEvalReportClarificationDao tblEvalReportClarificationDao) {
        this.tblEvalReportClarificationDao = tblEvalReportClarificationDao;
    }

    public TblEvalRptApproveDao getTblEvalRptApproveDao() {
        return tblEvalRptApproveDao;
    }

    public void setTblEvalRptApproveDao(TblEvalRptApproveDao tblEvalRptApproveDao) {
        this.tblEvalRptApproveDao = tblEvalRptApproveDao;
    }

    public TblEvalRptSentToAaDao getTblEvalRptSentToAaDao() {
        return tblEvalRptSentToAaDao;
    }

    public void setTblEvalRptSentToAaDao(TblEvalRptSentToAaDao tblEvalRptSentToAaDao) {
        this.tblEvalRptSentToAaDao = tblEvalRptSentToAaDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblEvalReportDocsDao getTblEvalReportDocsDao() {
        return tblEvalReportDocsDao;
    }

    public void setTblEvalReportDocsDao(TblEvalReportDocsDao tblEvalReportDocsDao) {
        this.tblEvalReportDocsDao = tblEvalReportDocsDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * This method give package or Lot No and description
     * @param tenderid
     * @param pNature
     * @return if service then {packageNo,packageDescription} else {appPkgLotId,lotNo,lotDesc}
     */
    public List<Object[]> lotofPkgDetail(String tenderid,String pNature){
        StringBuilder query = new StringBuilder();
        if(pNature.equalsIgnoreCase("Services")){
            query.append("select ttd.packageNo,ttd.packageDescription from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderid);
        }else{
            query.append("select tls.appPkgLotId,tls.lotNo,tls.lotDesc from TblTenderLotSecurity tls where tls.tblTenderMaster.tenderId="+tenderid);
        }
        return hibernateQueryDao.createNewQuery(query.toString());
    }
    /**
     * Add record to tbl_evalRptSentToAa
     * @param tblEvalRptSentToAa
     * @return true if success else false
     */
    public boolean inserToTblEvalRptSent(TblEvalRptSentToAa tblEvalRptSentToAa){
        logger.debug("inserToTblEvalRptSent : " + logUserId + " Starts");
        boolean flag = false;
        String action = "Send report to AA by TEC CP";
        try {
            tblEvalRptSentToAaDao.addTblEvalRptSentToAa(tblEvalRptSentToAa);
            flag = true;
        } catch (Exception e) {
            logger.error("inserToTblEvalRptSent : " + logUserId + e);
            action  = "Error in "+action+" "+e.getMessage();
        }finally{
             makeAuditTrailService.generateAudit(auditTrail, tblEvalRptSentToAa.getTblTenderMaster().getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, tblEvalRptSentToAa.getRemarks());
             action = null;
        }
        logger.debug("inserToTblEvalRptSent : " + logUserId + " Ends");
        return flag;

    }

    /**
     * Add record to tbl_evalRptForwardToAa
     * @param tblEvalRptForwardToAa
     * @return true if success else false
     */
    public boolean inserToTblEvalRptForward(TblEvalRptForwardToAa tblEvalRptForwardToAa){
        logger.debug("inser into TblEvalRptForwardToAa : " + logUserId + " Starts");
        boolean flag = false;
        String action = "Send report to "+tblEvalRptForwardToAa.getActionUserRole()+" by TEC CP";
        try {
            tblEvalRptForwardToAaDao.addTblEvalRptForwardToAa(tblEvalRptForwardToAa);
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("inserToTblEvalRptSent : " + logUserId + e);
            action  = "Error in "+action+" "+e.getMessage();
        }finally{
             makeAuditTrailService.generateAudit(auditTrail, tblEvalRptForwardToAa.getTblTenderMaster().getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, tblEvalRptForwardToAa.getSentUserRemarks());
             action = null;
        }
        logger.debug("inser into TblEvalRptForwardToAa : " + logUserId + " Ends");
        return flag;

    }

    /**
     * insert values in to tbl_EvalReportClarification
     * @param tblEvalReportClarification - list to be inserted
     * @return true if list inserted successfully else false
     */
    public boolean insertEvalReportClarification(TblEvalReportClarification tblEvalReportClarification) {
        logger.debug("insertEvalReportClarification : " + logUserId + " Starts");
        boolean flag = false;
        String action = "AA ask clarification to TEC CP";
        try {
            tblEvalReportClarificationDao.addTblEvalReportClarification(tblEvalReportClarification);
            flag = true;
        } catch (Exception e) {
            logger.error("insertEvalReportClarification : " + logUserId + e);
            action  = "Error in "+action+" "+e.getMessage();
        }finally{
             makeAuditTrailService.generateAudit(auditTrail, tblEvalReportClarification.getTblEvalRptSentToAa().getTblTenderMaster().getTenderId(), "tenderId", EgpModule.Evaluation.getName(), action, "");
             action = null;
        }
        logger.debug("insertEvalReportClarification : " + logUserId + " Ends");
        return flag;
    }

    /**
     * updating information to tbl_EvalReportClarification
     * @param answer from tbl_EvalReportClarification
     * @param answeredByUserId from tbl_EvalReportClarification
     * @param answeredByGovUserId from tbl_EvalReportClarification
     * @param evalRptClariId from tbl_EvalReportClarification
     * @return true if updated successfully else false
     */public boolean updateEvalReportClarification(String answer, int answeredByUserId, int answeredByGovUserId,int evalRptClariId,String tenderId) {
        logger.debug("updateEvalReportClarification : " + logUserId + " Starts");
        boolean flag = false;
        String action  = "TEC CP gives clarification";
        try {
            int size = 0;
            size = hibernateQueryDao.updateDeleteNewQuery("update TblEvalReportClarification set answer='" + answer + "',answerDt=current_timestamp,answeredByUserId=" + answeredByUserId + ",answeredByGovUserId=" + answeredByGovUserId + " where evalRptClariId = " + evalRptClariId);
            if(size != 0){
                flag = true;
            }
        } catch (Exception e) {
            logger.error("updateEvalReportClarification : " + logUserId + e);
            action  = "Error in "+action+" "+e.getMessage();
        }finally{
             makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "tenderId", EgpModule.Evaluation.getName(), action, "");
             action = null;
        }
        logger.debug("updateEvalReportClarification : " + logUserId + " Ends");
        return flag;
    }
     /**
      * For report Processing page
      * @param tenderId
      * @param lotId
      * @return List of Object {rptStatus, pkgLotId,evalRptToAaid, envelopeId, roundId} from tbl_evalRptSentToAa
      */
     public List<Object[]> forReprotProcess(int tenderId,int lotId){
        logger.debug("forReprotProcess : " + logUserId + " Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            String sqlQuery = "select rptStatus, pkgLotId,evalRptToAaid, envelopeId, roundId from TblEvalRptSentToAa where tblTenderMaster.tenderId="+tenderId+" and pkgLotId="+lotId;
            list = hibernateQueryDao.createNewQuery(sqlQuery);
        } catch (Exception e) {
            logger.error("forReprotProcess : " + logUserId + e);
        }

        logger.debug("forReprotProcess : " + logUserId + " Ends");
        return list;
    }
    
     /**
      * get data from TblEvalRptSentToAa
      * @param aaId
      * @param rId
      * @return object TblEvalRptSentToAa
      */
     public TblEvalRptSentToAa getTblEvalRptSentToAa(int aaId,int rId){
        logger.debug("getCommentsBytenderId : " + logUserId + " Starts");
         TblEvalRptSentToAa tblEvalRptSentToAa = new TblEvalRptSentToAa();
        try {
            //String sqlQuery = "select evalRptToAaid, remarks , aaRemarks, rptStatus, sentRole, sentBy from TblEvalRptSentToAa where evalRptToAaid="+aaId;
            tblEvalRptSentToAa = tblEvalRptSentToAaDao.findTblEvalRptSentToAa("evalRptToAaid",Operation_enum.EQ,aaId,"roundId",Operation_enum.EQ,rId).get(0);
        } catch (Exception e) {
            logger.error("getCommentsBytenderId : " + logUserId + e);
        }
        logger.debug("getCommentsBytenderId : " + logUserId + " Ends");
        return tblEvalRptSentToAa;
    }
     /**
      * get record of tbl_evalRptSentToAa
      * @param tenderId
      * @param lotId
      * @param rptId
      * @param rId
      * @return object TblEvalRptSentToAa
      */
     public TblEvalRptSentToAa getTblEvalRptSentToAa(int tenderId,int lotId,int rptId,int rId){
        logger.debug("getCommentsBytenderId : " + logUserId + " Starts");
         TblEvalRptSentToAa tblEvalRptSentToAa = new TblEvalRptSentToAa();
        try {
            //String sqlQuery = "select evalRptToAaid, remarks , aaRemarks, rptStatus, sentRole, sentBy from TblEvalRptSentToAa where evalRptToAaid="+aaId;
            tblEvalRptSentToAa = tblEvalRptSentToAaDao.findTblEvalRptSentToAa("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"pkgLotId",Operation_enum.EQ,lotId,"envelopeId",Operation_enum.EQ,rptId,"roundId",Operation_enum.EQ,rId).get(0);
        } catch (Exception e) {
            logger.error("getCommentsBytenderId : " + logUserId + e);
        }
        logger.debug("getCommentsBytenderId : " + logUserId + " Ends");
        return tblEvalRptSentToAa;
    }

     /**
      * get data from TblEvalRptForwardToAa
      * @param aaId
      * @param rId
      * @return object TblEvalRptForwardToAa
      */
     public TblEvalRptForwardToAa getTblEvalRptForwardToAa(int aaId,int rId){
        logger.debug("getCommentsBytenderId : " + logUserId + " Starts");
         TblEvalRptForwardToAa tblEvalRptForwardToAa = new TblEvalRptForwardToAa();
        try {
            //String sqlQuery = "select evalRptToAaid, remarks , aaRemarks, rptStatus, sentRole, sentBy from TblEvalRptSentToAa where evalRptToAaid="+aaId;
            tblEvalRptForwardToAa = tblEvalRptForwardToAaDao.findTblEvalRptForwardToAa("evalFfrptId",Operation_enum.EQ,aaId,"roundId",Operation_enum.EQ,rId).get(0);
        } catch (Exception e) {
            logger.error("getCommentsBytenderId : " + logUserId + e);
        }
        logger.debug("getCommentsBytenderId : " + logUserId + " Ends");
        return tblEvalRptForwardToAa;
    }

         /**
     * Update Tbl_EvalRptForwardToAa
     * @param tblEvalRptForwardToAa
     * @return true if success else return false
     */
    public boolean updateEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptForwardToAa){
        logger.debug("updateEvalForwardRptToAa : " + logUserId + " Starts");
        boolean flag = false;
        String action = tblEvalRptForwardToAa.getActionUserRole()+ " "+tblEvalRptForwardToAa.getRptStatus()+" the reports";
        try {
            String sqlQuery = "update TblEvalRptForwardToAa set rptStatus='"+tblEvalRptForwardToAa.getRptStatus()+"', actionUserRemarks='"+tblEvalRptForwardToAa.getActionUserRemarks()+"' , actionDt=current_timestamp(), action='Forward to "+tblEvalRptForwardToAa.getActionUserRole() +"' where evalFfrptId="+tblEvalRptForwardToAa.getEvalFfrptId();
            hibernateQueryDao.updateDeleteNewQuery(sqlQuery);
            flag = true;
        } catch (Exception e) {
            logger.error("updateEvalForwardRptToAa : " + logUserId + e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblEvalRptForwardToAa.getTblTenderMaster().getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, tblEvalRptForwardToAa.getActionUserRemarks());
             action = null;
        }
        logger.debug("updateEvalForwardRptToAa : " + logUserId + " Ends");
        return flag;
    }

     /**
      * get record of tbl_evalRptSentToAa
      * @param tenderId
      * @param lotId
      * @param rptId
      * @param rId
      * @return object TblEvalRptSentToAa
      */
     public TblEvalRptForwardToAa getTblEvalRptForwardToAa(int tenderId,int lotId,int rId){
        logger.debug("getCommentsBytenderId : " + logUserId + " Starts");
         TblEvalRptForwardToAa tblEvalRptForwardToAa = new TblEvalRptForwardToAa();
        try {            
            tblEvalRptForwardToAa = tblEvalRptForwardToAaDao.findTblEvalRptForwardToAa("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"pkgLotId",Operation_enum.EQ,lotId,"roundId",Operation_enum.EQ,rId).get(0);
        } catch (Exception e) {
            logger.error("getCommentsBytenderId : " + logUserId + e);
        }
        logger.debug("getCommentsBytenderId : " + logUserId + " Ends");
        return tblEvalRptForwardToAa;
    }

    /**
     * Update Tbl_EvalRptSentToAa
     * @param tblEvalRptSentToAa
     * @return true if success else return false
     */
    public boolean updateEvalSentRptToAa(TblEvalRptSentToAa tblEvalRptSentToAa){
        logger.debug("updateEvalSentRptToAa : " + logUserId + " Starts");
        boolean flag = false;
        String action = "AA "+tblEvalRptSentToAa.getRptStatus()+" the reports";
        try {
            String sqlQuery = "update TblEvalRptSentToAa set rptStatus='"+tblEvalRptSentToAa.getRptStatus()+"', aaRemarks='"+tblEvalRptSentToAa.getAaRemarks()+"' , rptApproveDt=current_timestamp() where evalRptToAaid="+tblEvalRptSentToAa.getEvalRptToAaid();
            hibernateQueryDao.updateDeleteNewQuery(sqlQuery);
            flag = true;
        } catch (Exception e) {
            logger.error("updateEvalSentRptToAa : " + logUserId + e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblEvalRptSentToAa.getTblTenderMaster().getTenderId(), "TenderId", EgpModule.Evaluation.getName(), action, tblEvalRptSentToAa.getAaRemarks());
             action = null;
        }
        logger.debug("updateEvalSentRptToAa : " + logUserId + " Ends");
        return flag;
    }
    /**
     * fetching information from tbl_EvalRptSentToAa
     * @param evalRptToAaid from tbl_EvalRptSentToAa
     * @return list of tbl_EvalRptSentToAa
     */
    public List<TblEvalReportClarification> fetchDataFromEvalReportClari(int evalRptToAaid) {
        logger.debug("fetchDataFromEvalReportClari : " + logUserId + " Starts");
        List<TblEvalReportClarification> list = new ArrayList<TblEvalReportClarification>();
        try {
            list = tblEvalReportClarificationDao.findTblEvalReportClarification("tblEvalRptSentToAa",Operation_enum.EQ,new TblEvalRptSentToAa(evalRptToAaid));
        } catch (Exception e) {
            logger.error("fetchDataFromEvalReportClari : " + logUserId + e);
        }
        logger.debug("fetchDataFromEvalReportClari : " + logUserId + " Ends");
        return list;
    }
    /**
     * fetching info from tbl_EvalReportClarification
     * @param evalRptClariId from tbl_EvalReportClarification
     * @return list of tbl_EvalReportClarification
     */public List<TblEvalReportClarification> fetchDataFromERC(int evalRptClariId) {
        logger.debug("fetchDataFromERC : " + logUserId + " Starts");
        List<TblEvalReportClarification> list = new ArrayList<TblEvalReportClarification>();
        try {
            list = tblEvalReportClarificationDao.findTblEvalReportClarification("evalRptClariId",Operation_enum.EQ,evalRptClariId);
        } catch (Exception e) {
            logger.error("fetchDataFromERC : " + logUserId + e);
        }
        logger.debug("fetchDataFromERC : " + logUserId + " Ends");
        return list;
    }

     /**
      * For delete Tbl_EvalReportDocs
      * @param id
      * @return number of row deleted
      */
     public int removeUploadDoc(int id){
        logger.debug("removeUploadDoc : " + logUserId + " Starts");
        int i_cnt = 0;
        try {
            i_cnt = hibernateQueryDao.updateDeleteNewQuery("delete TblEvalReportDocs where evalRptDocId="+id);
        } catch (Exception e) {
            logger.error("removeUploadDoc : " + logUserId + e);
        }
        logger.debug("removeUploadDoc : " + logUserId + " Ends");
        return i_cnt;

    }

    /**
     * Insert record in Tbl_EvalReportDocs
     * @param tblEvalReportDocs
     * @return true if success else false
     */
    public boolean addUploadDoc(TblEvalReportDocs tblEvalReportDocs){
        logger.debug("addUploadDoc : " + logUserId + " Starts");
        boolean flag = false;
        try {
           tblEvalReportDocsDao.addTblEvalReportDocs(tblEvalReportDocs);
           flag = true;
        } catch (Exception e) {
            logger.error("addUploadDoc : " + logUserId + e);
        }
        logger.debug("addUploadDoc : " + logUserId + " Ends");
        return flag;
    }
    
    /**
     * Get list of Docs uploaded from tbl_EvalReportDocs
     * @param tenderId
     * @param lotId
     * @param roundId
     * @return List of Tbl_EvalReportDocs
     */
    public List<TblEvalReportDocs> getListOfDocs(int tenderId, int lotId,int roundId){
        logger.debug("getListOfDocs : " + logUserId + " Starts");
        List<TblEvalReportDocs> list = new ArrayList<TblEvalReportDocs>();
        try {
           list =  tblEvalReportDocsDao.findTblEvalReportDocs("pkgLotId",Operation_enum.EQ,lotId,"tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId),"roundId",Operation_enum.EQ,roundId);
        } catch (Exception e) {
            logger.error("getListOfDocs : " + logUserId + e);
        }
        logger.debug("getListOfDocs : " + logUserId + " Ends");
        return list;
    }
    public List<TblEvalRptSentToAa> fetchRoleSC(int evalRptToAaid) {
        logger.debug("fetchRoleSC : " + logUserId + " Starts");
        List<TblEvalRptSentToAa> list = new ArrayList<TblEvalRptSentToAa>();
        try {
            list = tblEvalRptSentToAaDao.findTblEvalRptSentToAa("evalRptToAaid",Operation_enum.EQ,evalRptToAaid);
        } catch (Exception e) {
            logger.error("fetchRoleSC : " + logUserId + e);
        }
        logger.debug("fetchRoleSC : " + logUserId + " Ends");
        return list;
    }
    /**
     * Get email and mobile number of AA for maling purpose
     * @param userId
     * @return Object {emailId , mobileNo}
     */
    public Object[] getEmailMobileOfAA(int userId){
        logger.debug("getEmailMobileOfAA : " + logUserId + " Start");
        Object[] obj = null;
        try {
            String sqlQuery = "select l.emailId , e.mobileNo  from TblEmployeeMaster e, TblLoginMaster l  where e.tblLoginMaster.userId=l.userId and l.userId="+userId;
            obj = hibernateQueryDao.createNewQuery(sqlQuery).get(0);
        } catch (Exception e) {
            logger.error("getEmailMobileOfAA : " + logUserId + e);
        }
        logger.debug("getEmailMobileOfAA : " + logUserId + " Ends");
        return obj;
    }
    /**
     * Insert recrod in Tbl_EvalTscDocs
     * @param tblEvalTscDocs
     * @return true if success else return false
     */
    public boolean insertTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs){
        logger.debug("insertTblEvalTscDocs : " + logUserId + " Start");
        boolean flag = false;
        try {
            tblEvalTscDocsDao.addTblEvalTscDocs(tblEvalTscDocs);
            flag = true;
        } catch (Exception e) {
            logger.error("insertTblEvalTscDocs : " + logUserId + e);
        }
        logger.debug("insertTblEvalTscDocs : " + logUserId + " Ends");
        return flag;
    }
    /**
     * Get list of Docs inserted in tbl_EvalTscDocs using tenderId
     * @param TenderId
     * @return List of Tbl_EvalTscDocs
     */
    public List<TblEvalTscDocs> getDocDetails(String TenderId){
        logger.debug("getDocDetails : " + logUserId + " Start");
        List<TblEvalTscDocs> list = null;
        try {
            list = tblEvalTscDocsDao.findTblEvalTscDocs("tblTenderMaster",Operation_enum.EQ,(new TblTenderMaster(Integer.parseInt(TenderId))));
        } catch (Exception e) {
            logger.error("getDocDetails : " + logUserId + e);
        }
        logger.debug("getDocDetails : " + logUserId + " Ends");
        return list;
    }
    /**
     * Delete record from Tbl_EvalTscDocs
     * @param tenderId
     * @param docId
     * @return true if success else return false
     */
    public boolean deleteDoc(int tenderId,int docId) {


        logger.debug("deleteGuidelinesDocument : " + logUserId + "starts");
        boolean flag = true;
        try {
            String query = "delete from TblEvalTscDocs ttsd where ttsd.tscDocId='"+docId+"' and ttsd.tblTenderMaster.tenderId='"+tenderId+"'";
            hibernateQueryDao.updateDeleteNewQuery(query);

        } catch (Exception ex) {
            logger.error("deleteGuidelinesDocument : " + ex);
            flag = false;
        }
        logger.debug("deleteGuidelinesDocument : " + logUserId + "ends");
        return flag;
    }
    /**
     * Get list of Tbl_EvalTscDocs using tenderId and pkgId
     * @param TenderId
     * @param pkgId
     * @return List of Tbl_EvalTscDocs
     */
    public List<TblEvalTscDocs> getDocDetails(String TenderId,String pkgId){
        logger.debug("getDocDetails : " + logUserId + " Start");
        List<TblEvalTscDocs> list = null;
        try {
            list = tblEvalTscDocsDao.findTblEvalTscDocs("tblTenderMaster",Operation_enum.EQ,(new TblTenderMaster(Integer.parseInt(TenderId))),"pkgLotId",Operation_enum.EQ,Integer.parseInt(pkgId));
        } catch (Exception e) {
            logger.error("getDocDetails : " + logUserId + e);
        }
        logger.debug("getDocDetails : " + logUserId + " Ends");
        return list;
    }
    public boolean addEvalNotify(TblEvalTernotifyMember tblEvalTernotifyMember){
        logger.debug("addEvalNotify : " + logUserId + " Start");
        boolean flag = false;
        try {
            tblEvalTernotifyMemberDao.addTblEvalTernotifyMember(tblEvalTernotifyMember);
            flag = true;
        } catch (Exception e) {
            logger.error("addEvalNotify : " + logUserId + e);
        }
        logger.debug("addEvalNotify : " + logUserId + " Ends");
        return flag;
    }
    
    /**
     * for reprot Process View Page
     * @param tenderId
     * @param lotId
     * @return List of Object {rptStatus, pkgLotId,evalRptToAaid, envelopeId, roundId} from Tbl_EvalRptSentToAa
     */
    public List<Object[]> forReprotProcessView(int tenderId,int lotId){
        logger.debug("forReprotProcess : " + logUserId + " Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            StringBuffer sqlQuery = new StringBuffer();
            sqlQuery.append("select rptStatus, pkgLotId,evalRptToAaid, envelopeId, roundId from TblEvalRptSentToAa where tblTenderMaster.tenderId="+tenderId);
                    if(lotId != -1){
                        sqlQuery.append(" and pkgLotId="+lotId);
                    }
                    sqlQuery.append(" and rptStatus in ('Rejected / Re-Tendering','Approved')");
            list = hibernateQueryDao.createNewQuery(sqlQuery.toString());
        } catch (Exception e) {
            logger.error("forReprotProcess : " + logUserId + e);
        }
        
        logger.debug("forReprotProcess : " + logUserId + " Ends");
        return list;
    }

    /**
     * updating data to tbl_EvalRptSentToAa
     * @param evalRptToAaid from tbl_EvalRptSentToAa
     * @param rptStatus from tbl_EvalRptSentToAa
     * @return true if list updated successfully else false
     */public boolean updateTblEvalRptSentToAA(int evalRptToAaid, String rptStatus) {
        logger.debug("updateTblEvalRptSentToAA : " + logUserId + " Start");
        boolean flag = false;
        try {
            hibernateQueryDao.updateDeleteNewQuery("update TblEvalRptSentToAa ter set ter.rptStatus = '" + rptStatus + "' where ter.evalRptToAaid = " + evalRptToAaid);
            flag = true;
        } catch (Exception e) {
            logger.error("updateTblEvalRptSentToAA : " + logUserId + e);
        }
        logger.debug("updateTblEvalRptSentToAA : " + logUserId + " Ends");
        return flag;
    }

     //dohtec start for reevaluation
//     public int getEvaluationNo(int tenderId) {
//        logger.debug("getEvaluationNo : " + logUserId + " Start");
//        int evalCount = 0;
//        try {
//            List<Object> list = hibernateQueryDao.getSingleColQuery("select MAX(ter.evalCount) from TblEvalRptSentToAa ter where tblTenderMaster.tenderId="+tenderId);
//            if(!list.isEmpty() && list!=null){
//                evalCount = Integer.parseInt(list.get(0).toString());
//            }
//       
//        } catch (Exception e) {
//            logger.error("getEvaluationNo : " + logUserId + e);
//        }
//        logger.debug("getEvaluationNo : " + logUserId + " Ends");
//        return evalCount;
//    }
	//dohatec end
     // added by alam ahsan
     public int getEvaluationNo(int tenderId) {
        logger.debug("getEvaluationNo : " + logUserId + " Start");
        int evalCount = 0;
        List<Object> list = null;
        try {
            if(hibernateQueryDao.countForNewQuery("TblEvalRptSentToAa", "tblTenderMaster.tenderId="+tenderId)>0)
            {
                list = hibernateQueryDao.getSingleColQuery("select MAX(ter.evalCount) from TblEvalRptSentToAa ter where tblTenderMaster.tenderId="+tenderId);
           // if(list.size()>0){
                 evalCount = Integer.parseInt(list.get(0).toString());
            }
           // }
       
        } catch (Exception e) {
            logger.error("getEvaluationNo : " + logUserId + e);
        }
        logger.debug("getEvaluationNo : " + logUserId + " Ends");
        return evalCount;
    }
     //end alam ahsan

}
