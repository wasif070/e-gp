/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author dipti
 */
public interface TemplateMaster {
    /**
     * Create Template
     * @param tblTemplateMaster
     * @return true if created else false.
     */
    public boolean createTemplate(TblTemplateMaster tblTemplateMaster);
    /**
     * Get Template List
     * @param fromRecord
     * @param noOfRecords
     * @param sortOrder
     * @param sortingOn
     * @param tStatus
     * @return List of Templates
     */
    public List<TblTemplateMaster> getTemplateList(int fromRecord, int noOfRecords, String sortOrder, String sortingOn, String tStatus);
    /**
     * Get Template List
     * @param fromRecord
     * @param noOfRecords
     * @param sortOrder
     * @param sortingOn
     * @param tStatus
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return List of Templates
     */
    public List<TblTemplateMaster> getTemplateList(int fromRecord, int noOfRecords, String sortOrder, String sortingOn, String tStatus, String searchField, String searchString, String searchOper);
    /**
     * Get Total no of STD.
     * @param tStatus
     * @return no of count
     */
    public long getTotalNoOfSTDs(String tStatus);
    /**
     * Get Searched no of STD.
     * @param tStatus
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return no of count
     */
    public long getSearchNoOfSTDs(String tStatus, String searchField, String searchString, String searchOper);
    /**
     * Get Template Details.
     * @param templateId
     * @return template details
     */
    public List<TblTemplateMaster> getTemplateDtl(short templateId);
    /**
     * Update Template Status.
     * @param status
     * @param templateId
     * @return true if updated or false if not.
     */
    public boolean updateStatusOfTemplate(String status, short templateId);
    /**
     * Delete Template
     * @param templateId
     * @return true if deleted o false if not.
     */
    public boolean deleteTemplate(short templateId);
    /**
     * Search Template by Name
     * @param txtSection
     * @return Template Details.
     */
    public List<TblTemplateMaster> searchTemplateByName(String txtSection);

    /**
     * Set user ID at service layer
     * @param userId
     */
    public void setUserId(String userId);
    
    /**
     * Set Audit Trail at service layer
     * @param userId
     */
    public void setAuditTrail(AuditTrail auditTrail);
}
