/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblOrgMasterDao;
import com.cptu.egp.eps.model.table.TblOrgMaster;
import com.cptu.egp.eps.service.serviceinterface.OrgMasterService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sudhir Chavhan
 */
public class OrgMasterServiceImpl implements OrgMasterService {
    private final Logger logger = Logger.getLogger(OrgMasterServiceImpl.class);
    private TblOrgMasterDao tblOrgMasterDao;
    private HibernateQueryDao hibernateQueryDao;
    private String orgId = "0";

    //important points setter and getter points
    public TblOrgMasterDao getTblOrgMasterDao() {
        return tblOrgMasterDao;
    }

    public void setTblOrgMasterDao(TblOrgMasterDao tblOrgMasterDao) {
        this.tblOrgMasterDao = tblOrgMasterDao;
    }

    @Override
    public void setOrgId(String orgId) {
        this.orgId = orgId;
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public void addTblOrgMaster(TblOrgMaster org) {
        tblOrgMasterDao.addEntity(org);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblOrgMaster(TblOrgMaster org) {
        tblOrgMasterDao.deleteEntity(org);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblOrgMaster(TblOrgMaster org) {
        tblOrgMasterDao.updateEntity(org);
        //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblOrgMaster> getAllTblOrgMaster() {
        return tblOrgMasterDao.getAllEntity();
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblOrgMaster> findByCountTblOrgMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return tblOrgMasterDao.findByCountEntity(firstResult, maxResult, values);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getTblOrgMasterCount() {
        return tblOrgMasterDao.getTblOrgMasterCount();
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblOrgMaster> findTblOrgMaster(Object... values) {
        List<TblOrgMaster> list = null;
        try {
            logger.debug("OrgMasterServiceImpl.java 1 :-" + values);
            list = tblOrgMasterDao.findTblOrgMaster(values);
            logger.debug("OrgMasterServiceImpl.java 2:-" + list);
        } catch (Exception e) {
            logger.error("Exception in findTblOrgMaster " + e.getMessage());
        }
        logger.debug("findOfficeMaster : " + list);
        return list;
    }

    @Override
    public String getWSRights(String org) {
        String temp = "";
        try {
            logger.debug("Org id :- " + org);
            String Query = " select WS_Rights from TblOrgMaster where Org_Id=" + org;
            logger.debug("Org Query :- " + Query);
            List<Object[]> listObj = hibernateQueryDao.createNewQuery(Query);
            logger.debug("Org 2nd:-" + listObj);
            /*     if(!listObj.isEmpty()){
            temp = listObj.get(0).toString();
            }*/
            Object[] orgtemp = listObj.toArray();
            logger.debug("Org 3rd");
            temp = orgtemp[0].toString();
            logger.debug("Org 4th:-" + temp);

        } catch (Exception e) {
            logger.error("Exception in getWSRights " + e.getMessage());

        }
        return temp;

    }
}
