/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigDocFeesDao;
import com.cptu.egp.eps.model.table.TblConfigDocFees;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class TenPaymentConfigService {

    private HibernateQueryDao hibernateQueryDao;
    private TblConfigDocFeesDao tblConfigDocFeesDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private static final Logger LOGGER = Logger.getLogger(TenPaymentConfigService.class);
    MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblConfigDocFeesDao getTblConfigDocFeesDao() {
        return tblConfigDocFeesDao;
    }

    public void setTblConfigDocFeesDao(TblConfigDocFeesDao tblConfigDocFeesDao) {
        this.tblConfigDocFeesDao = tblConfigDocFeesDao;
    }

    /**
     * List of Tender Types
     * @return List of {tenderTypeId,tenderType}
     */
    public List<Object[]> getTenderTypes() {
        return hibernateQueryDao.createNewQuery("select ttt.tenderTypeId,ttt.tenderType from TblTenderTypes ttt");
    }

    /**
     *List of ProcurementMethod
     * @return List of {procurementMethodId,procurementMethod}
     */
    public List<Object[]> getProcurementMethod() {
        return hibernateQueryDao.createNewQuery("select tpm.procurementMethodId,tpm.procurementMethod from TblProcurementMethod tpm");//,tpm.procurementFullName
    }

    /**
     *List of Procurement Types
     * @return List of {procurementTypeId,procurementType}
     */
    public List<Object[]> getProcuremenTypes() {
        return hibernateQueryDao.createNewQuery("select tpt.procurementTypeId,tpt.procurementType from TblProcurementTypes tpt");//,tpm.procurementFullName
    }

    /**
     *Inserts Documents Payment Configuration
     * @param list to be inserted
     * @return true or false for success or fail
     */
    public boolean insertConfigTenPayment(List<TblConfigDocFees> list) {
        LOGGER.debug("insertConfigTenPayment : " + logUserId + " Starts");
        boolean flag;
        String action = null;
        try {
            tblConfigDocFeesDao.updateInsAllConfigDocFees(list);
            flag = true;
            action = "Add Tender Payment Configuration";
        } catch (Exception e) {
            LOGGER.error("insertConfigTenPayment : " + logUserId + " :" + e);
            flag = false;
            action = "Error in Add Tender Payment Configuration : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("insertConfigTenPayment : " + logUserId + " Ends");
        return flag;
    }

    /**
     *List of All Documents Payment Configuration
     * @return List of TblConfigDocFees
     */
    public List<TblConfigDocFees> getAllTblConfigDocFees() {
        return tblConfigDocFeesDao.getAllTblConfigDocFees();
    }

    /**
     *List of All Documents Payment Configuration
     * @param values to be searched on
     * @return List of TblConfigDocFees
     * @throws Exception
     */
    public List<TblConfigDocFees> getTblConfigDocFees(Object... values) throws Exception {
        return tblConfigDocFeesDao.findTblConfigDocFees(values);
    }

    /**
     *Updates TblConfigDocFees
     * @param list to be updated
     * @return true or false for success or fail
     */
    public boolean updateConfigTenPayment(List<TblConfigDocFees> list) {
        LOGGER.debug("updateConfigTenPayment : " + logUserId + " Starts");
        boolean flag = false;
        String action = null;
        try {
            int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigDocFees");
            if (i > 0) {
                tblConfigDocFeesDao.updateInsAllConfigDocFees(list);
                flag = true;
            }
            action = "Edit Tender Payment Configuration";
        } catch (Exception e) {
            LOGGER.error("updateConfigTenPayment : " + logUserId + " Ends" + e);
            action = "Error in Edit Tender Payment Configuration : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateConfigTenPayment : " + logUserId + " Ends");
        return flag;

    }

    /**
     *Updates TblConfigDocFees
     * @param tblConfigDocFees to be updated
     * @return true or false for success or fail
     */
    public boolean updateSingleConfigTenPayment(TblConfigDocFees tblConfigDocFees) {
        LOGGER.debug("updateSingleConfigTenPayment : " + logUserId + " Starts");
        boolean flag = false;
        String action = null;
        try {
            tblConfigDocFeesDao.updateTblConfigDocFees(tblConfigDocFees);
            flag = true;
            action = "Edit Tender Payment Configuration";
        } catch (Exception e) {
            LOGGER.error("updateSingleConfigTenPayment : " + logUserId + " Ends" + e);
            action = "Error in Edit Tender Payment Configuration : "+e;
       }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateSingleConfigTenPayment : " + logUserId + " Ends");
        return flag;
    }

    /**
     *Deletes TblConfigDocFees by docFeesId
     * @param docFeesId to be deleted
     * @return no of rows deleted
     */
    public int deleteSingleConfigTenPayment(String docFeesId) {
        LOGGER.debug("deleteSingleConfigTenPayment : "+logUserId+" Starts");
        int cnt = 0;
        String action = null;
        try {
            cnt = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigDocFees where docFeesId=" + docFeesId);
            action = "Remove Tender Payment Configuration";
        } catch (Exception e) {
            LOGGER.debug("deleteSingleConfigTenPayment : "+logUserId,e);
            action = "Error in Remove Tender Payment Configuration : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("deleteSingleConfigTenPayment : "+logUserId+" Ends");
        return cnt;
    }

    /**
     *Get Unique Document Payment configuration count
     * @param docFeesId
     * @param pType
     * @param tendType
     * @param pMethod
     * @return count
     * @throws Exception
     */
    public long uniqueConfigCount(String docFeesId, String pType, String tendType, String pMethod) throws Exception {
        return hibernateQueryDao.countForNewQuery("TblConfigDocFees tcm", "tcm.procurementMethodId=" + pMethod + " and tenderTypeId=" + tendType + " and procurementTypeId=" + pType + " and docFeesId!=" + docFeesId);
    }

    /**
     *Count of all Tender Payment Configuration
     * @return count
     * @throws Exception
     */
    public long getAllTenPayementConfigCount() throws Exception {
        return hibernateQueryDao.countForNewQuery("TblConfigDocFees tcem", "1=1");
    }
 
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
