/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daoimpl.TblMessageDocumentImpl;
import com.cptu.egp.eps.dao.daoimpl.TblMessageInBoxImpl;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblMessageFolderDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.MsgListing;
import com.cptu.egp.eps.dao.storedprocedure.SPCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPMessageListing;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblMessageDocument;
import com.cptu.egp.eps.model.table.TblMessageFolder;
import com.cptu.egp.eps.service.serviceinterface.MessageProcessingService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.Date;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class MessageProcessingServiceImpl implements MessageProcessingService {

    final Logger logger = Logger.getLogger(MessageProcessingServiceImpl.class);

    private String procName;
    private BasicDataSource basicDataSource;
    private TblLoginMasterDao loginMasterDao;
    private TblMessageFolderDao messageFolderDao;
    private String mailmessage;
    private TblMessageDocumentImpl tblMessageDocumentDao;
    private TblMessageInBoxImpl tblMessageInBoxDao;
    private String logUserId = "0";
    private MakeAuditTrailService makeAuditTrailService;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    /**
     *
     * to get data based on messageboxtype (Inbox,Sent,Draft,Trash).
     *
     * @param id
     * @param messageBoxType
     * @param messType
     * @param noOfRecords
     * @param pageNumber
     * @param totalPages
     * @param folderId
     * @param searching
     * @param keywords
     * @param emialId
     * @param dateFrom
     * @param dateTo
     * @param viewMsg
     * @param msgid
     * @return msgListing object it contains flag,message
     * regarding procedure operation.
     */
    @Override
    public List<MsgListing> getInboxData(int id, String messageBoxType, String messType, Integer noOfRecords,
            Integer pageNumber, Integer totalPages, Integer folderId, String searching, String keywords,
            String emialId, String dateFrom, String dateTo, String viewMsg, Integer msgid,String columnName,String order,String sortOpt) {
        logger.debug("getInboxData : "+logUserId+" Starts");
        List<MsgListing> inputdata = null;
        String straction = "";
        try {
            if("Inboxpre".equalsIgnoreCase(messageBoxType))
            {
                straction = "View Inbox Message";
                messageBoxType = messageBoxType.substring(0, messageBoxType.length() - 3);
            }else if("Sentpre".equalsIgnoreCase(messageBoxType))
            {
                straction = "View Sent Message";
                messageBoxType = messageBoxType.substring(0, messageBoxType.length() - 3);
            }
            else if("Trashpre".equalsIgnoreCase(messageBoxType))
            {
                straction = "View Trash Message";
                messageBoxType = messageBoxType.substring(0, messageBoxType.length() - 3);
            }
            Integer uid = new Integer(id);
            SPMessageListing spMessagelisting = new SPMessageListing(basicDataSource, procName);
            inputdata = spMessagelisting.executeProcedure(messageBoxType, uid,
                    folderId, messType, noOfRecords, pageNumber, totalPages, searching, keywords,
                    emialId, dateFrom, dateTo, viewMsg, msgid,columnName,order,sortOpt);
        } catch (Exception ex) {
           straction = straction + " : "+ex;
           logger.error("getInboxData : "+logUserId+" : "+ex);
        }finally{
            if(!"".equalsIgnoreCase(straction) && straction!=null)
            {
                makeAuditTrailService.generateAudit(auditTrail, msgid, "messageId", EgpModule.Message_Box.getName(), straction, "");
                straction=null;
            }
        }
           logger.debug("getInboxData : "+logUserId+" Ends");
        return inputdata;
    }
    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *  to get folders by loginuserid
     * @param uid current login userId
     * @return tblMessageFolder object
     */
    public List<TblMessageFolder> getFolderDataByUserid(int uid) {
        logger.debug("getFolderDataByUserid : "+logUserId+" Starts");
        List folderdata = null;
        try {
            folderdata = messageFolderDao.findTblMessageFolder("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(uid));
           logger.debug("getFolderDataByUserid : "+folderdata);
        } catch (Exception ex) {
            logger.error("getFolderDataByUserid : "+logUserId+" : "+ex);
        }
        logger.debug("getFolderDataByUserid : "+logUserId+" Ends");
        return folderdata;
    }

    /**
     *
     * to compose mail and to edit draft message
     * @param id
     * @param to
     * @param from
     * @param cc
     * @param sub
     * @param priority
     * @param mstxt
     * @param procURL
     * @param msfBoxType
     * @param action
     * @param msgStatus
     * @param folderid
     * @param msgReadStatus
     * @param isMsgReply
     * @param msgDocId
     * @param updmsgId
     * @param messageInboxId
     * @return it returns spcommon contains information regarding
     * message
     */
    public List composeEmail(int id, String to, String from, String cc, String sub, String priority,
            String mstxt, String procURL, String msfBoxType, String action, String msgStatus,
            Integer folderid, String msgReadStatus, String isMsgReply,
            String msgDocId, Integer updmsgId, Integer messageInboxId) {
        logger.debug("composeEmail : "+logUserId+" Starts");
        List<CommonMsgChk> statusData = null;
        String fromEmail = from;
        String straction="";
        int mId=0;
        try {
            if("Trash".equalsIgnoreCase(action))
            {
                straction = "Trash Message";
            }
            else if("Reply".equalsIgnoreCase(action))
            {
                straction = "Reply";
            }
            else if("Reply To All".equalsIgnoreCase(action))
            {
                straction = "Reply To All";
            }
            else if("Forward".equalsIgnoreCase(action))
            {
                straction = "Forward";
            }
            else if("Update".equalsIgnoreCase(action))
            {
                straction = "Move to Folder";
            }
            else if("create".equalsIgnoreCase(action))
            {
                straction = "Compose Message";
                if("Draft".equalsIgnoreCase(msfBoxType))
                {
                    straction = "Save as Draft Message";
                }
            }
            else if("Edit".equalsIgnoreCase(action))
            {
                 if("Draft".equalsIgnoreCase(msfBoxType))
                {
                    straction = "Edit Draft Message";
                }
            }
            else
            {
                auditTrail=null;
            }
            if (!msfBoxType.equals("Trash") && !msfBoxType.equals("Reply")
                    && !msfBoxType.equals("Reply To All") && !msfBoxType.equals("Forward")) {
                List loginmastor = loginMasterDao.findEntity("userId", Operation_enum.EQ, id);
                TblLoginMaster tblLoginMastor = (TblLoginMaster) loginmastor.get(0);
                fromEmail = tblLoginMastor.getEmailId();
            }
            SPCommon spCommon = new SPCommon(basicDataSource, mailmessage);
            statusData = spCommon.executeProcedure(to, fromEmail, cc, sub, mstxt, procURL,
                    priority, msfBoxType, action, msgStatus, folderid,
                    msgReadStatus, isMsgReply, msgDocId, updmsgId, messageInboxId);                       
            if(statusData!=null && !statusData.isEmpty() && statusData.get(0).getId()!=null){                
                mId=statusData.get(0).getId();
            }else{
                mId = messageInboxId;
            }
        } catch (Exception ex) {
            straction = straction+": "+ex;
            logger.error("composeEmail : "+logUserId+" : "+ex);
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, mId, "messageId", EgpModule.Message_Box.getName(), straction, "");
            action=null;
        }
        logger.debug("composeEmail : "+logUserId+" Ends");
        return statusData;
    }

    public List getLoginmailIdByuserId(int uid){
        logger.debug("getLoginmailIdByuserId : "+logUserId+" Starts");
        List loginmastor = null;
        try {
           loginmastor  = loginMasterDao.findEntity("userId", Operation_enum.EQ, uid);

        } catch (Exception ex) {
            logger.error("getLoginmailIdByuserId : "+logUserId+" : "+ex);
        }
            logger.debug("getLoginmailIdByuserId : "+logUserId+" Ends ");
       return loginmastor;
    }
    /**
     * @return the procName
     */
    public String getProcName() {
        return procName;
    }

    /**
     * @param procName the procName to set
     */
    public void setProcName(String procName) {
        this.procName = procName;
    }

    /**
     * @return the basicDataSource
     */
    public BasicDataSource getBasicDataSource() {
        return basicDataSource;
    }

    /**
     * @param basicDataSource the basicDataSource to set
     */
    public void setBasicDataSource(BasicDataSource basicDataSource) {
        this.basicDataSource = basicDataSource;
    }

    /**
     * @return the loginMasterDao
     */
    public TblLoginMasterDao getLoginMasterDao() {
        return loginMasterDao;
    }

    /**
     * @param loginMasterDao the loginMasterDao to set
     */
    public void setLoginMasterDao(TblLoginMasterDao loginMasterDao) {
        this.loginMasterDao = loginMasterDao;
    }

    /**
     * @return the messageFolderDao
     */
    public TblMessageFolderDao getMessageFolderDao() {
        return messageFolderDao;
    }

    /**
     * @param messageFolderDao the messageFolderDao to set
     */
    public void setMessageFolderDao(TblMessageFolderDao messageFolderDao) {
        this.messageFolderDao = messageFolderDao;
    }

    /**
     * @return the mailmessage
     */
    public String getMailmessage() {
        return mailmessage;
    }

    /**
     * @param mailmessage the mailmessage to set
     */
    public void setMailmessage(String mailmessage) {
        this.mailmessage = mailmessage;
    }

    /**
     *
     * @param tblMessageDocumetn
     */
    public void addMessageDocument(TblMessageDocument tblMessageDocumetn) {
        logger.debug("addMessageDocument : "+logUserId+" Starts ");
        try {
        tblMessageDocumentDao.addTblMessageDocument(tblMessageDocumetn);
        } catch (Exception e) {
            logger.error("addMessageDocument : "+logUserId+" : "+e);
    }
        logger.debug("addMessageDocument : "+logUserId+" Ends ");
        
    }

    /**
     * @return the tblMessageDocumentDao
     */
    public TblMessageDocumentImpl getTblMessageDocumentDao() {
        return tblMessageDocumentDao;
    }

    /**
     * @param tblMessageDocumentDao the tblMessageDocumentDao to set
     */
    public void setTblMessageDocumentDao(TblMessageDocumentImpl tblMessageDocumentDao) {
        this.tblMessageDocumentDao = tblMessageDocumentDao;
    }

    /**
     * @return the tblMessageInBoxDao
     */
    public TblMessageInBoxImpl getTblMessageInBoxDao() {
        return tblMessageInBoxDao;
    }

    /**
     * @param tblMessageInBoxDao the tblMessageInBoxDao to set
     */
    public void setTblMessageInBoxDao(TblMessageInBoxImpl tblMessageInBoxDao) {
        this.tblMessageInBoxDao = tblMessageInBoxDao;
    }

    public int updateReadMessage(String sqlquery){
         logger.debug("updateReadMessage : "+logUserId+" Starts ");
         int ret = 0;
         try {
         ret =  tblMessageInBoxDao.updateDeleteSQLQuery(sqlquery);
        } catch (Exception e) {
            logger.error("updateReadMessage : "+logUserId+" : "+e);
        }
         logger.debug("updateReadMessage : "+logUserId+" Ends ");
         return ret;
    }

     public long getTotalRecievedMsgs(String from , String where){
         logger.debug("getTotalRecievedMsgs : "+logUserId+" Starts ");
          long ret = 0;
        try {
              ret = tblMessageInBoxDao.countForQuery(from, where);
        } catch (Exception ex) {
            logger.error("getTotalRecievedMsgs : "+logUserId+" : "+ex);
        }
         logger.debug("getTotalRecievedMsgs : "+logUserId+" Ends ");
      return ret;
   }

       public long getTotalUnreadMsgs(String from , String where){
          logger.debug("getTotalUnreadMsgs : "+logUserId+" Starts ");
          long ret = 0;
        try {
              ret = tblMessageInBoxDao.countForQuery(from, where);
        } catch (Exception ex) {
            logger.error("getTotalUnreadMsgs : "+logUserId+" : "+ex);
        }
           logger.debug("getTotalUnreadMsgs : "+logUserId+" Ends");
      return ret;
   }

    /**
     * Generate Default Archive Folder for all user who does not have generated Archive Folder.
     * @param userId = tbl_MessageFolder.userId
     * @return = tbl_MessageFolder.msgFolderId
     */
   @Override 
    public int createDefaultArchiveFolder(int userId)
    {
        logger.debug("createDefaultArchiveFolder : "+logUserId+" Starts");
        int folderId = 0;
        List <TblMessageFolder> list=null;
        try
        {
            folderId=checkArchiveFolderExist(userId);
            if(folderId == 0) // Generate Archive Folder for given User Id.
            {
                TblMessageFolder ob = new TblMessageFolder();
                ob.setCreationDate(new Date());
                ob.setFolderName("Archive");
                ob.setTblLoginMaster(new TblLoginMaster(userId));
                messageFolderDao.addEntity(ob);
                folderId=checkArchiveFolderExist(userId);
            }
        }
        catch (Exception ex)
        {
            logger.error("createDefaultArchiveFolder : "+logUserId+" : "+ex);
        }
        logger.debug("createDefaultArchiveFolder : "+logUserId+" Ends");
        return folderId;
    }

    /**
     * Check Archive Folder Exist for given User Id
     * @param uid = tbl_MessageFolder.userId
     * @return = tbl_MessageFolder.msgFolderId
     */
    @Override
    public int checkArchiveFolderExist(int uid) {
        logger.debug("checkArchiveFolderExist : "+logUserId+" Starts");
        int folderId = 0;
        List <TblMessageFolder> list=null;
        try
        {
            list = messageFolderDao.findEntity("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(uid),"folderName",Operation_enum.EQ,"Archive");
            if(list != null && list.get(0)!= null && list.get(0).getMsgFolderId() != 0)
            {
                folderId=list.get(0).getMsgFolderId();
            }
        }
        catch (Exception ex)
        {
            logger.error("checkArchiveFolderExist : "+logUserId+" : "+ex);
        }
        logger.debug("checkArchiveFolderExist : "+logUserId+" Ends");
        return folderId;
    }
    @Override
    public boolean deleteTrashMessages(int MsgId) {
        logger.debug("deleteTrashMessages : "+logUserId+" Starts");
        boolean flag = false;
        int i=0,j=0,k=0;
        try {
              i = hibernateQueryDao.updateDeleteNewQuery("delete from TblMessage where msgId="+MsgId+"");
              //j = hibernateQueryDao.updateDeleteNewQuery("delete from TblMessageInBox tmt where tmt.tblMessage.msgId="+MsgId+"");
              //k = hibernateQueryDao.updateDeleteNewQuery("delete from TblMessageTrash tmt where tmt.tblMessage.msgId="+MsgId+"");
              if(i>0)
              {
                 flag = true;
              }              
        } catch (Exception e) {
            logger.error("deleteTrashMessages : "+logUserId+" : "+e);
        }
        logger.debug("deleteTrashMessages : "+logUserId+" Ends");
        return flag;
    }

}
