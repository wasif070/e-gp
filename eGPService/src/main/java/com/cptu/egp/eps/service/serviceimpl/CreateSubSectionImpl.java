/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;


import com.cptu.egp.eps.dao.daointerface.TblIttHeaderDao;
import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.service.serviceinterface.CreateSubSection;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class CreateSubSectionImpl implements CreateSubSection {
    final Logger logger = Logger.getLogger(CreateSubSectionImpl.class);
    TblIttHeaderDao headerDao;
    TblTemplateSectionsDao tblTemplateSectionsDao;
    String logUserId = "0";
    MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public TblTemplateSectionsDao getTblTemplateSectionsDao()
    {
        return tblTemplateSectionsDao;
    }

    public void setTblTemplateSectionsDao(TblTemplateSectionsDao tblTemplateSectionsDao)
    {
        this.tblTemplateSectionsDao = tblTemplateSectionsDao;
    }
    
    public TblIttHeaderDao getHeaderDao() {
        return headerDao;
    }

    public void setHeaderDao(TblIttHeaderDao headerDao) {
        this.headerDao = headerDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    @Override
    public boolean createSubSection(TblIttHeader tblIttHeader) {
        logger.debug("createSubSection : "+logUserId+" Starts");
        boolean loginFlag = false;
        try {
            headerDao.addTblIttHeader(tblIttHeader);
            loginFlag = true;
        }
        catch (Exception ex) {
            logger.error("createSubSection : "+logUserId+" :"+ex);
            loginFlag = false;
        }
        logger.debug("createSubSection : "+logUserId+" Ends");
        return loginFlag;
    }

    public List<TblIttHeader> getSubSection(int sectionId) {
        logger.debug("getSubSection : "+logUserId+" Starts");
        List subSection;
        try {
            subSection = headerDao.findTblIttHeader("tblTemplateSections", Operation_enum.EQ, new TblTemplateSections(sectionId));
        }
        catch (Exception ex) {
            subSection = null;
            logger.error("getSubSection : "+logUserId+" :"+ex);
        }
        logger.debug("getSubSection : "+logUserId+" Ends");
        return subSection;
    }

    @Override
    public List<Object[]> getSubSectionWithTDSAppl(int sectionId){
        logger.debug("getSubSectionWithTDSAppl : "+logUserId+" Starts");
        List subSection;
        try {
            //subSection = headerDao.findTblIttHeader("tblTemplateSections", Operation_enum.EQ, new TblTemplateSections(sectionId));
            String qry = "select distinct ittHeader.ittHeaderId, ittHeader.ittHeaderName from TblIttHeader ittHeader, " +
                    "TblIttClause ittClause, TblIttSubClause subClause where ittHeader.tblTemplateSections.sectionId = '" + sectionId + "' " +
                    "and subClause.isTdsApplicable = 'yes' and ittHeader.ittHeaderId = ittClause.tblIttHeader.ittHeaderId and " +
                    "ittClause.ittClauseId = subClause.tblIttClause.ittClauseId";
           logger.debug("getSubSectionWithTDSAppl  : "+qry);
            subSection = headerDao.createQuery(qry);
        }
        catch (Exception ex) {
            subSection = null;
            logger.error("getSubSectionWithTDSAppl : "+logUserId+" :"+ex);
        }
        logger.debug("getSubSectionWithTDSAppl : "+logUserId+" Ends");
        return subSection;
    }

    @Override
    public boolean updateSubSection(TblIttHeader tblIttHeader){
        logger.debug("updateSubSection : "+logUserId+" Starts");
        boolean updateSubSection;
        try {
            headerDao.updateEntity(tblIttHeader);
            updateSubSection = true;
        }
        catch (Exception ex) {
            updateSubSection = false;
            logger.error("updateSubSection : "+logUserId+" :"+ex);
        }
        logger.debug("updateSubSection : "+logUserId+" Ends");
        return updateSubSection;
    }

    @Override
    public boolean deleteSubSection(TblIttHeader tblIttHeader){
        logger.debug("deleteSubSection : "+logUserId+" Starts");
        boolean updateSubSection;
        try {
            headerDao.deleteEntity(tblIttHeader);
            updateSubSection = true;
        }
        catch (Exception ex) {
            updateSubSection = false;
            logger.error("deleteSubSection : "+logUserId+" :"+ex);
        }
        logger.debug("deleteSubSection : "+logUserId+" Ends");
        return updateSubSection;
    }

    @Override
    public boolean deleteSection(int sectionId)
    {
        logger.debug("deleteSection : "+logUserId+" Starts");
        boolean flag;
        String action = "Delete Section";
        TblTemplateSections tblTemplateSections = new TblTemplateSections();
        try{
            tblTemplateSections = tblTemplateSectionsDao.findTblTemplateSections("sectionId",Operation_enum.EQ,sectionId).get(0);
            tblTemplateSectionsDao.updateDeleteQuery("delete from TblTemplateSections where sectionId = " + sectionId);
            flag = true;
        } catch(Exception ex){
            logger.error("deleteSection : "+logUserId+" :"+ex);
            flag = false;
            action = "error In "+action+" "+ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblTemplateSections.getTemplateId(), "templetId", EgpModule.STD.getName(), action, "");
        }
        logger.debug("deleteSection : "+logUserId+" Ends");
        return flag;

    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

}
