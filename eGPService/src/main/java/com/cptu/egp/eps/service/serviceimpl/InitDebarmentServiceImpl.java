/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentComMembersDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentCommentsDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentCommitteeDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentDocsDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentReqDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentRespDao;
import com.cptu.egp.eps.dao.daointerface.TblDebarmentTypesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblDebarmentComMembers;
import com.cptu.egp.eps.model.table.TblDebarmentComments;
import com.cptu.egp.eps.model.table.TblDebarmentCommittee;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import com.cptu.egp.eps.model.table.TblDebarmentReq;
import com.cptu.egp.eps.model.table.TblDebarmentResp;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import com.cptu.egp.eps.service.serviceinterface.InitDebarmentService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class InitDebarmentServiceImpl implements InitDebarmentService {

    final Logger logger = Logger.getLogger(InitDebarmentServiceImpl.class);
    private String logUserId ="0";
    
    TblDebarmentReqDao tblDebarmentReqDao;
    TblDebarmentRespDao tblDebarmentRespDao;
    SPTenderCommon sPTenderCommon;
    HibernateQueryDao hibernateQueryDao;
    TblDebarmentTypesDao tblDebarmentTypesDao;
    TblDebarmentDetailsDao tblDebarmentDetailsDao;
    TblDebarmentCommentsDao tblDebarmentCommentsDao;
    TblDebarmentCommitteeDao tblDebarmentCommitteeDao;
    TblDebarmentComMembersDao tblDebarmentComMembersDao;
    TblDebarmentDocsDao tblDebarmentDocsDao;
    MakeAuditTrailService makeAuditTrailService;
    AuditTrail auditTrail;


    public TblDebarmentDocsDao getTblDebarmentDocsDao() {
        return tblDebarmentDocsDao;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public void setTblDebarmentDocsDao(TblDebarmentDocsDao tblDebarmentDocsDao) {
        this.tblDebarmentDocsDao = tblDebarmentDocsDao;
    }

    public TblDebarmentComMembersDao getTblDebarmentComMembersDao() {
        return tblDebarmentComMembersDao;
    }

    public void setTblDebarmentComMembersDao(TblDebarmentComMembersDao tblDebarmentComMembersDao) {
        this.tblDebarmentComMembersDao = tblDebarmentComMembersDao;
    }

    public TblDebarmentCommentsDao getTblDebarmentCommentsDao() {
        return tblDebarmentCommentsDao;
    }

    public void setTblDebarmentCommentsDao(TblDebarmentCommentsDao tblDebarmentCommentsDao) {
        this.tblDebarmentCommentsDao = tblDebarmentCommentsDao;
    }

    public TblDebarmentCommitteeDao getTblDebarmentCommitteeDao() {
        return tblDebarmentCommitteeDao;
    }

    public void setTblDebarmentCommitteeDao(TblDebarmentCommitteeDao tblDebarmentCommitteeDao) {
        this.tblDebarmentCommitteeDao = tblDebarmentCommitteeDao;
    }

    public TblDebarmentDetailsDao getTblDebarmentDetailsDao() {
        return tblDebarmentDetailsDao;
    }

    public void setTblDebarmentDetailsDao(TblDebarmentDetailsDao tblDebarmentDetailsDao) {
        this.tblDebarmentDetailsDao = tblDebarmentDetailsDao;
    }

    public TblDebarmentTypesDao getTblDebarmentTypesDao() {
        return tblDebarmentTypesDao;
    }

    public void setTblDebarmentTypesDao(TblDebarmentTypesDao tblDebarmentTypesDao) {
        this.tblDebarmentTypesDao = tblDebarmentTypesDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    public TblDebarmentRespDao getTblDebarmentRespDao() {
        return tblDebarmentRespDao;
    }

    public void setTblDebarmentRespDao(TblDebarmentRespDao tblDebarmentRespDao) {
        this.tblDebarmentRespDao = tblDebarmentRespDao;
    }

    public TblDebarmentReqDao getTblDebarmentReqDao() {
        return tblDebarmentReqDao;
    }

    public void setTblDebarmentReqDao(TblDebarmentReqDao tblDebarmentReqDao) {
        this.tblDebarmentReqDao = tblDebarmentReqDao;
    }

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    @Override
    public boolean saveInitDebarmentRequest(TblDebarmentReq tblDebarmentReq, TblDebarmentDetails tblDebarmentDetails) {
        logger.debug("saveInitDebarmentRequest : "+logUserId+" Starts");
        
        boolean flag = false;
        String action=null;
        try {
            tblDebarmentReqDao.addTblDebarmentReq(tblDebarmentReq);
            tblDebarmentDetails.setTblDebarmentReq(tblDebarmentReq);
            tblDebarmentDetailsDao.addTblDebarmentDetails(tblDebarmentDetails);
            flag =  true;
            action = "Debarment Initiated by PA";
        } catch (Exception ex) {
            logger.error("saveInitDebarmentRequest : "+logUserId+" : "+ex.toString());
            action = "Error in Debarment Initiated by PA "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblDebarmentReq.getDebarmentBy(), "userId", EgpModule.Debarment.getName(), action, "");
        }
         logger.debug("saveInitDebarmentRequest : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public boolean saveBidderResponse(TblDebarmentResp tblDebarmentResp) {
        logger.debug("saveBidderResponse : "+logUserId+" Starts");
        boolean flag = false;
        String action=null;
        try {
            tblDebarmentRespDao.addTblDebarmentResp(tblDebarmentResp);
            flag = true;
            action = "Debarment Clarificaiton submitted by Bidder";
        } catch (Exception ex) {
            logger.error("saveBidderResponse : "+logUserId+" : "+ex.toString());
            action = "Error in Debarment Clarificaiton submitted by Bidder "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblDebarmentResp.getUserId(), "userId", EgpModule.Debarment.getName(), action, "");
        }
        logger.debug("saveBidderResponse : "+logUserId+" Ends");
        return flag;           
    }

    @Override
    public List<SPTenderCommonData> searchCompanyForDebar(String searchString) {
        logger.debug("searchCompanyForDebar : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try{
            list = sPTenderCommon.executeProcedure("getCmpfordebar", searchString, null);
        }catch(Exception ex){
           logger.error("searchCompanyForDebar : "+logUserId+" : "+ex.toString());
    }
         logger.debug("searchCompanyForDebar : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> findDebarMentListing(String status, int offset, int row) throws Exception {
        logger.debug("findDebarMentListing : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createByCountNewQuery("select tdr.debarmentId,(select em.employeeName from TblEmployeeMaster em where em.tblLoginMaster.userId=tdr.debarmentBy) as empName,tdr.clarificationReqDt,tdr.debarmentStatus,tcm.companyName,tm.firstName,tm.lastName,tdr.clarification from TblDebarmentReq tdr,TblTendererMaster tm,TblCompanyMaster tcm where tdr.userId=tm.tblLoginMaster.userId and tm.tblCompanyMaster.companyId=tcm.companyId and tdr.debarmentStatus " + status, offset, row);
        }catch(Exception ex){
            logger.error("findDebarMentListing : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findDebarMentListing : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long debarMentListingCount(String status) throws Exception {
        logger.debug("debarMentListingCount : "+logUserId+" Starts");
        long lng = 0;
        try{
            lng = hibernateQueryDao.countForNewQuery("TblDebarmentReq tdr,TblTendererMaster tm,TblCompanyMaster tcm", "tdr.userId=tm.tblLoginMaster.userId and tm.tblCompanyMaster.companyId=tcm.companyId and tdr.debarmentStatus " + status);
        }catch(Exception ex){
             logger.error("debarMentListingCount : "+logUserId+" : "+ex.toString());
    }
        logger.debug("debarMentListingCount : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public List<Object[]> findDebarReqForResp(String userId,String debarId) throws Exception {
        logger.debug("findDebarReqForResp : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tdr.debarmentId,tdr.clarification,tdr.lastResponseDt,tdr.debarmentStatus,tcm.companyName,tm.firstName,tm.lastName,tdr.debarmentBy,tdr.hopeId from TblDebarmentReq tdr,TblTendererMaster tm,TblCompanyMaster tcm where tdr.userId=tm.tblLoginMaster.userId and tm.tblCompanyMaster.companyId=tcm.companyId and tdr.debarmentId="+debarId+" and tm.tblLoginMaster.userId=" + userId);
        }catch(Exception ex){
            logger.error("findDebarReqForResp : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findDebarReqForResp : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<SPTenderCommonData> searchDataForDebarType(String debarType, String userId) throws Exception {
        logger.debug("searchDataForDebarType : "+logUserId+" Starts");
        String data = null;
        List<SPTenderCommonData> list = null;

        try{
        if (debarType.equals("1")) {
            data = "getsingletender";
        } else if (debarType.equals("2")) {
            data = "getPackage";
        } else if (debarType.equals("3")) {
            data = "getProject";
        } else if (debarType.equals("4")) {
            data = "getProcuringEntity";
        } else if (debarType.equals("5")) {
            data = "getorganisation";
        }
            list = sPTenderCommon.executeProcedure(data, userId, null);
        }catch(Exception ex){
            logger.error("searchDataForDebarType : "+logUserId+" : "+ex.toString());
    }
        logger.debug("searchDataForDebarType : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<TblDebarmentTypes> getDebarTypes() {
        List<TblDebarmentTypes> list = null;
        logger.debug("getDebarTypes : "+logUserId+" Starts");
        try{
            list = tblDebarmentTypesDao.getAllTblDebarmentTypes();
        }catch(Exception ex){
            logger.error("getDebarTypes : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getDebarTypes : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long isResponseAvail(String debarId) throws Exception {
        logger.debug("isResponseAvail : "+logUserId+" Starts");
        long lng = 0;
        try{
            lng = hibernateQueryDao.countForNewQuery("TblDebarmentResp tdr", "tdr.tblDebarmentReq.debarmentId=" + debarId);
        }catch(Exception ex){
            logger.error("isResponseAvail : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isResponseAvail : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public List<Object[]> viewTendererDebarClari(String debarId,String status) throws Exception {
       logger.debug("viewTendererDebarClari : "+logUserId+" Starts");
       List<Object[]> list = null;
       try{
            list = hibernateQueryDao.createNewQuery("select tdr.clarification,tdr.lastResponseDt,tcm.companyName,tm.firstName,tm.lastName,tdr.userId,tdr.hopeId from TblDebarmentReq tdr,TblTendererMaster tm,TblCompanyMaster tcm where tdr.userId=tm.tblLoginMaster.userId and tm.tblCompanyMaster.companyId=tcm.companyId and tdr.debarmentStatus='"+status+"' and tdr.debarmentId=" + debarId);
       }catch(Exception ex){
           logger.error("viewTendererDebarClari : "+logUserId+" : "+ex.toString());
    }
        logger.debug("viewTendererDebarClari : "+logUserId+" Ends");
       return list;
    }

    @Override
    public List<SPTenderCommonData> viewTendererDebarClari(int debarId,String status) throws Exception {
        logger.debug("viewTendererDebarClari : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try{
            list = sPTenderCommon.executeProcedure("TendererDebarDetail", String.valueOf(debarId), status);
        }catch(Exception ex){
            logger.error("viewTendererDebarClari : "+logUserId+" : "+ex.toString());
    }
        logger.debug("viewTendererDebarClari : "+logUserId+" Ends");
        return list;
    }

    @Override
    public int updateDebarByPE(String debarId,String debarStatus,String comments,String hopeId) throws Exception {
        logger.debug("updateDebarByPE : "+logUserId+" Starts");
        int i = 0;
        String action=null;
        try{
             i = hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentReq set debarmentStatus='"+debarStatus+"',comments='"+comments+"',hopeId="+hopeId+" where debarmentId="+debarId);
             action = "Debarment clarification Accepted by PA "+(debarStatus.equals("pesatisfy")?"Satisfactory":"Unsatisfactory");
        }catch(Exception ex){
            logger.error("updateDebarByPE : "+logUserId+" : "+ex.toString());
            action = "Error in Debarment clarification Accepted by PA "+(debarStatus.equals("pesatisfy")?"Satisfactory":"Unsatisfactory")+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Debarment.getName(), action, comments);
        }
        logger.debug("updateDebarByPE : "+logUserId+" Ends");
        return i;
    }

    @Override
    public List<TblDebarmentDetails> getDebarDetails(Object ... values) throws Exception {
        logger.debug("getDebarDetails : "+logUserId+" Starts");
        List<TblDebarmentDetails> list  = null;
        try{
            list  = tblDebarmentDetailsDao.findTblDebarmentDetails(values);
        }catch(Exception ex){
            logger.error("getDebarDetails : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getDebarDetails : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean updateDebarByHope(String debarId,String debarIds, String debarStatus, String comments, String debarBy, Date debarStart, Date debarEnd, String debarTypeId) throws Exception {
       logger.debug("updateDebarByHope : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
       try {
           int i = hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentReq tbr set tbr.debarmentStatus='"+debarStatus+"' where tbr.debarmentId="+debarId);
           if(i>0){
               TblDebarmentDetails details = new TblDebarmentDetails(0, new TblDebarmentReq(Integer.parseInt(debarId)), debarIds, Integer.parseInt(debarTypeId), "byhope", Integer.parseInt(debarBy), new Date());
               details.setDebarStartDt(debarStart);
               details.setDebarEdnDt(debarEnd);
               tblDebarmentDetailsDao.addTblDebarmentDetails(details);
               tblDebarmentCommentsDao.addTblDebarmentComments(new TblDebarmentComments(0, Integer.parseInt(debarId), Integer.parseInt(debarBy), new Date(), comments, "hope"));
               flag = true;
               action = "Processed Debarment requests by Hope "+(debarStatus.equals("sendtoegp")?"Approve Debarment":debarStatus.equals("hopesatisfy")?"Disapprove Debarment":"Form review Committee");
            }
       }catch (Exception ex) {
            logger.error("updateDebarByHope : "+logUserId+" : "+ex.toString());
            action = "Error in Processed Debarment requests by Hope "+(debarStatus.equals("sendtoegp")?"Approve Debarment":debarStatus.equals("hopesatisfy")?"Disapprove Debarment":"Form review Committee")+ex;
       }finally{
           makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Debarment.getName(), action, comments);
       }
        logger.debug("updateDebarByHope : "+logUserId+" Ends");        
       return flag;
    }

    @Override
    public void createDebarCommittee(TblDebarmentCommittee committee, List<TblDebarmentComMembers> comMembers) {
        logger.debug("createDebarCommittee : "+logUserId+" Starts");
        String action=null;
        try {
            tblDebarmentCommitteeDao.addTblDebarmentCommittee(committee);
            for (TblDebarmentComMembers tblDebarmentComMembers : comMembers) {
                tblDebarmentComMembers.setTblDebarmentCommittee(committee);
                tblDebarmentComMembersDao.addTblDebarmentComMembers(tblDebarmentComMembers);
            }
            action="Created Debarment Committee by Hope";
        } catch (Exception e) {
            logger.error("createDebarCommittee : "+logUserId,e);
            action="Error in Created Debarment Committee by Hope "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, committee.getCreatedBy(), "userId", EgpModule.Debarment.getName(), action, "");
        }
        logger.debug("createDebarCommittee : "+logUserId+" Ends");
    }

    @Override
    public List<Object[]> isCommitteeExist(String debarId) {
        logger.debug("isCommitteeExist : "+logUserId+" Starts");
        List<Object[]> obj = null;
        try {
            obj =  hibernateQueryDao.createNewQuery("select tdc.debarCommitteeId,tdc.comStatus,tdc.committeeName from TblDebarmentCommittee tdc where tdc.tblDebarmentReq.debarmentId="+ debarId);
        } catch (Exception ex) {
            logger.error("isCommitteeExist : "+logUserId+" : "+ex.toString());
        }
        logger.debug("isCommitteeExist : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public List<SPTenderCommonData> savedDebarmentComUser(String debarComId) {
        logger.debug("savedDebarmentComUser : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try{
            list = sPTenderCommon.executeProcedure("SavedDebarmentComUser", debarComId, null);
        }catch(Exception ex){
             logger.error("savedDebarmentComUser : "+logUserId+" : "+ex.toString());
    }
        logger.debug("savedDebarmentComUser : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void updateDebarCommittee(String comId, String comName, List<TblDebarmentComMembers> comMembers) {
        logger.debug("updateDebarCommittee : "+logUserId+" Starts");
        String action=null;
        try{
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentCommittee set committeeName='"+comName+"' where debarCommitteeId="+comId);
            if(i>0){
                int j = hibernateQueryDao.updateDeleteNewQuery("delete from TblDebarmentComMembers where tblDebarmentCommittee.debarCommitteeId="+comId);
                if(j>0){
                    tblDebarmentComMembersDao.updateSaveAllDebarComMembers(comMembers);
                }
            }
            action = "Edit Debarment Committee by Hope";
        }catch(Exception ex){
            logger.error("updateDebarCommittee : "+logUserId+" : "+ex.toString());
            action = "Error in Edit Debarment Committee by Hope "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Debarment.getName(), action, "");
        }
        logger.debug("updateDebarCommittee : "+logUserId+" Ends");
        
    }

    @Override
    public boolean  publishDebarCommitte(String comId) {
        logger.debug("publishDebarCommitte : "+logUserId+" Starts");
        boolean flag = false;
        String action=null;
        try{
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentCommittee set comStatus='published' where debarCommitteeId="+comId);
            if(i>0){
                    flag = true;
            }
            action = "Published Debarment Committee by Hope";
        }catch(Exception ex){
             logger.error("publishDebarCommitte : "+logUserId+" : "+ex.toString());
             action = "Error in Published Debarment Committee by Hope "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Debarment.getName(), action, "");
        }
         logger.debug("publishDebarCommitte : "+logUserId+" Ends");
        return flag;
    }
    
    @Override
    public List<Object[]> findHopeAns(String debarId){//, String hopeId) {
        logger.debug("findHopeAns : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tdd.debarIds,tdd.debarStartDt,tdd.debarEdnDt,tdc.comments,tdd.debarTypeId from TblDebarmentDetails tdd,TblDebarmentComments tdc where tdd.debarStatus='byhope' and tdd.debarmentBy=tdc.userId and tdd.tblDebarmentReq.debarmentId=tdc.debarmentId and tdd.tblDebarmentReq.debarmentId="+debarId);
        }catch(Exception ex){
           logger.error("findHopeAns : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findHopeAns : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> findSendHopeAns(String debarId){//, String hopeId) {
        logger.debug("findSendHopeAns : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tdd.debarIds,tdd.debarStartDt,tdd.debarEdnDt,'',tdd.debarTypeId from TblDebarmentDetails tdd where tdd.debarStatus='bype' and tdd.tblDebarmentReq.debarmentId="+debarId);
        }catch(Exception ex){
           logger.error("findSendHopeAns : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findSendHopeAns : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getCommitteDetails(String userId,String debarId) {
        logger.debug("getCommitteDetails : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tdc.committeeName,tdc.createdBy,tdcm.memberType from TblDebarmentCommittee tdc,TblDebarmentComMembers tdcm where tdc.debarCommitteeId=tdcm.tblDebarmentCommittee.debarCommitteeId and tdc.tblDebarmentReq.debarmentId="+debarId+" and tdcm.userId="+userId);
        }catch(Exception ex){
            logger.error("getCommitteDetails : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getCommitteDetails : "+logUserId+" Ends");
        return list;
    }

    @Override
    public boolean committeMemComments(String debarId, String comments, String userId,String commentsBy) {
        logger.debug("committeMemComments : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
       try {
            tblDebarmentCommentsDao.addTblDebarmentComments(new TblDebarmentComments(0, Integer.parseInt(debarId), Integer.parseInt(userId) , new Date(), comments, commentsBy));
            flag = true;
            action = "Processed Debarment Requests by debarment "+(commentsBy.equals("chairperson") ? "Committee CP" : "Committee Member");
        } catch (Exception ex) {
           logger.error("committeMemComments : "+logUserId+" : "+ex.toString());
           action = "Error in Processed Debarment Requests by debarment "+(commentsBy.equals("chairperson") ? "Committee CP" : "Committee Member")+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(userId), "userId", EgpModule.Debarment.getName(), action, comments);
        }
        logger.debug("committeMemComments : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public List<TblDebarmentComments> getCommitteeMemComments(String debarId,String userId,String commentsBy) throws Exception{
        logger.debug("getCommitteeMemComments : "+logUserId+" Starts");
        List<TblDebarmentComments> list = null;
        try{
            list = tblDebarmentCommentsDao.findTblDebarmentComments("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"userId",Operation_enum.EQ,Integer.parseInt(userId),"commentsBy",Operation_enum.EQ,commentsBy);
        }catch(Exception ex){
           logger.error("getCommitteeMemComments : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getCommitteeMemComments : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<Object[]> getWholeCommitteComments(String debarId) {
        logger.debug("getWholeCommitteComments : "+logUserId+" Starts");
        List<Object[]>  list = null;
        try{
        StringBuilder sb = new StringBuilder();
        sb.append("select tem.employeeName,tdcm.memberType,tdc.comments,tdc.commentsBy ");
        sb.append("from TblDebarmentComments tdc,TblEmployeeMaster tem,TblDebarmentCommittee tdcom,TblDebarmentComMembers tdcm ");
        sb.append("where tdc.userId=tem.tblLoginMaster.userId and tdcom.debarCommitteeId=tdcm.tblDebarmentCommittee.debarCommitteeId ");
        sb.append("and tdcom.tblDebarmentReq.debarmentId=tdc.debarmentId and tdcm.userId=tdc.userId and tdc.debarmentId="+debarId);
            list = hibernateQueryDao.createNewQuery(sb.toString());
        }catch(Exception ex){
            logger.error("getWholeCommitteComments : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getWholeCommitteComments : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long isChairperson(String debarId,String userId){
        logger.debug("isChairperson : "+logUserId+" Starts");
        long lng = 0;
        try {
            lng = hibernateQueryDao.countForNewQuery("TblDebarmentCommittee tdc,TblDebarmentComMembers tdcm", "tdcm.memberType='cp' and tdc.debarCommitteeId=tdcm.tblDebarmentCommittee.debarCommitteeId and tdc.tblDebarmentReq.debarmentId ="+debarId+" and tdcm.userId="+userId);
        } catch (Exception ex) {
            logger.error("isChairperson : "+logUserId+" : "+ex.toString());
        }
        logger.debug("isChairperson : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public boolean chaipersonReview(String debarId,String userId,String comments,String debarStatus,String commentsBy){
        logger.debug("chaipersonReview : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
        try {
            tblDebarmentCommentsDao.addTblDebarmentComments(new TblDebarmentComments(0, Integer.parseInt(debarId), Integer.parseInt(userId), new Date(), comments, commentsBy));
            if(hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentReq tbr set tbr.debarmentStatus='"+debarStatus+"' where tbr.debarmentId="+debarId)==1){
                flag =  true;
                if(commentsBy.equals("hopecp")){
                    action = "Processed Debarment requests by Hope "+(debarStatus.equals("sendtoegp")?"Approve Debarment":"Disapprove Debarment");
                }else{
                    action = "Processed Debarment requests by e-GP Admin "+(debarStatus.equals("appdebaregp")?"Approve Debarment":"Disapprove Debarment");
                    
                }
            }            
        } catch (Exception ex) {
            if(commentsBy.equals("hopecp")){
                 action = "Error in Processed Debarment requests by Hope "+(debarStatus.equals("sendtoegp")?"Approve Debarment":"Disapprove Debarment")+ex;
           }else{
                 action = "Error in Processed Debarment requests by e-GP Admin "+(debarStatus.equals("appdebaregp")?"Approve Debarment":"Disapprove Debarment")+ex;

            }
            logger.error("chaipersonReview : "+logUserId+" : "+ex.toString());
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(userId), "userId", EgpModule.Debarment.getName(), action, comments);
        }
        logger.debug("chaipersonReview : "+logUserId+" Ends");
        return flag;
    }

    @Override
    public String getHopeIdForDebar(String debarId){
        logger.debug("getHopeIdForDebar : "+logUserId+" Starts");
        String str = "";
        try{
        List<Object> list = hibernateQueryDao.getSingleColQuery("select hopeId from TblDebarmentReq where debarmentId="+debarId);
            if(!list.isEmpty()){
                str =  list.get(0).toString();
        }
        }catch(Exception ex){
            logger.error("getHopeIdForDebar : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getHopeIdForDebar : "+logUserId+" Ends");
        return str;
    }

    @Override
    public String getHopeEmailIdForDebar(String debarId){
        logger.debug("getHopeIdForDebar : "+logUserId+" Starts");
        String str = "";
        try{
        List<Object> list = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm where tlm.userId in (select hopeId from TblDebarmentReq where debarmentId="+debarId+")");
            if(!list.isEmpty()){
                str =  list.get(0).toString();
        }
        }catch(Exception ex){
            logger.error("getHopeIdForDebar : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getHopeIdForDebar : "+logUserId+" Ends");
        return str;
    }

    @Override
    public List<TblDebarmentResp> findDebarResponse(Object ... values) throws Exception{
         logger.debug("findDebarResponse : "+logUserId+" Starts");
        List<TblDebarmentResp> list = null;
        try{
            list = tblDebarmentRespDao.findTblTblDebarmentResp(values);
        }catch(Exception ex){
            logger.error("findDebarResponse : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findDebarResponse : "+logUserId+" Ends");
        return list;
    }

    @Override
    public long countCommitteeMember(String debarId) throws Exception {
        logger.debug("countCommitteeMember : "+logUserId+" Starts");
        long lng = 0;
        try{
            lng = hibernateQueryDao.countForNewQuery("TblDebarmentCommittee tc,TblDebarmentComMembers tcm","tc.debarCommitteeId=tcm.tblDebarmentCommittee.debarCommitteeId and tc.tblDebarmentReq.debarmentId="+debarId+" and tcm.memberType='m'");
        }catch(Exception ex){
           logger.error("countCommitteeMember : "+logUserId+" : "+ex.toString());
    }
        logger.debug("countCommitteeMember : "+logUserId+" Ends");
        return lng;
    }

    @Override
    public boolean saveDocument(TblDebarmentDocs tblDebarmentDocs) {
        logger.debug("saveDocument : "+logUserId+" Starts");
        boolean status = false;
        try {
            tblDebarmentDocsDao.addTblDebarmentDocs(tblDebarmentDocs);
            status = true;
        } catch (Exception ex) {
            logger.error("saveDocument : "+logUserId+" : "+ex.toString());
        }
        logger.debug("saveDocument : "+logUserId+" Ends");
        return status;
    }

    @Override
    public List<TblDebarmentDocs> getDebarDocs(Object... values) {
        logger.debug("getDebarDocs : "+logUserId+" Starts");
        List<TblDebarmentDocs> list = null;   
        try {
            list = tblDebarmentDocsDao.findTblDebarmentDocs(values);
        } catch (Exception ex) {
           logger.error("getDebarDocs : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getDebarDocs : "+logUserId+" Ends");
        return list;
    }

    @Override
    public int deleteDebarDocs(String debarDocId) {
        logger.debug("deleteDebarDocs : "+logUserId+" Starts");
        int status = 0;
        try{
            status = hibernateQueryDao.updateDeleteNewQuery("delete from TblDebarmentDocs tdd where tdd.debarDocId="+debarDocId);
        }catch(Exception ex){
            logger.error("deleteDebarDocs : "+logUserId+" : "+ex.toString());
    }
        logger.debug("deleteDebarDocs : "+logUserId+" Ends");
        return status;
    }

    @Override
    public int updateDebarStatus(String debarId,String status){
        logger.debug("updateDebarStatus : "+logUserId+" Starts");
        int status1 = 0;
        String action = null;
        try{
            status1 = hibernateQueryDao.updateDeleteNewQuery("update TblDebarmentReq tbr set tbr.debarmentStatus='"+status+"' where tbr.debarmentId="+debarId);
            action = "Debarment Request sent to Bidder by PA";
        }catch(Exception ex){
            logger.error("updateDebarStatus : "+logUserId+" : "+ex.toString());
            action = "Error in Debarment Request sent to Bidder by PA "+ex;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Debarment.getName(), action, "");
        }
        logger.debug("updateDebarStatus : "+logUserId+" Ends");
        return status1;
    }

    @Override
    public Object[] findPETenderHopeId(String debarId){
        logger.debug("findPETenderHopeId : "+logUserId+" Starts");
        Object[] obj = null;
        try{
            if(hibernateQueryDao.countForNewQuery("TblDebarmentDetails tdd", "tdd.debarStatus='byhope' and tdd.tblDebarmentReq.debarmentId="+debarId)==0){
                obj = hibernateQueryDao.createNewQuery("select tdr.userId,tdr.debarmentBy,0,tdr.tblDebarmentTypes.debarTypeId from TblDebarmentReq  tdr where tdr.debarmentId="+debarId).get(0);
            }else{
                obj = hibernateQueryDao.createNewQuery("select tdr.userId,tdr.debarmentBy,tdr.hopeId,tdr.tblDebarmentTypes.debarTypeId from TblDebarmentReq  tdr where tdr.debarmentId="+debarId).get(0);
            }
        }catch(Exception ex){
            logger.error("findPETenderHopeId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findPETenderHopeId : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public Object getOfficeName(String userId) {
        logger.debug("getOfficeName : "+logUserId+" Starts");
        Object obj = null;
        try{
        List<Object> os = hibernateQueryDao.getSingleColQuery("select om.officeName from TblEmployeeMaster e,TblEmployeeOffices eo,TblOfficeMaster om where e.employeeId=eo.tblEmployeeMaster.employeeId and om.officeId=eo.tblOfficeMaster.officeId and e.tblLoginMaster.userId="+userId);
            if(!os.isEmpty()){
               obj = os.get(0);
       }
        }catch(Exception ex){
            logger.error("getOfficeName : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getOfficeName : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public Object getPEEmailId(String debarId) {
        logger.debug("getPEEmailId : "+logUserId+" Starts");
        Object obj = null;
        try{
             obj = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm,TblDebarmentReq tdr where tdr.debarmentId="+debarId+" and tlm.userId=tdr.debarmentBy").get(0);
        }catch(Exception ex){
            logger.error("getPEEmailId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getPEEmailId : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public List<Object> getDebarCommitteeEmailIds(String debarId){
        logger.debug("getDebarCommitteeEmailIds : "+logUserId+" Starts");
        List<Object> obj = null;
        try{
             obj = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm,TblDebarmentCommittee tdc,TblDebarmentComMembers tdcm where tdc.tblDebarmentReq.debarmentId="+debarId+" and tdc.debarCommitteeId = tdcm.tblDebarmentCommittee.debarCommitteeId and tlm.userId in (tdcm.userId)");
        }catch(Exception ex){
            logger.error("getDebarCommitteeEmailIds : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getDebarCommitteeEmailIds : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public Object getEgpAdmin(){
        logger.debug("getEgpAdmin : "+logUserId+" Starts");
        Object obj = null;
        try{
             obj = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm where tlm.tblUserTypeMaster.userTypeId=1").get(0);
        }catch(Exception ex){
            logger.error("getEgpAdmin : "+logUserId+" : "+ex.toString());
     }
        logger.debug("getEgpAdmin : "+logUserId+" Ends");
        return obj;
     }

    @Override
    public Object getHOPEEmailId(String debarId) {
        logger.debug("getHOPEEmailId : "+logUserId+" Starts");
        Object obj = null;
        try{
             obj = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm,TblDebarmentReq tdr where tdr.debarmentId="+debarId+" and tlm.userId=tdr.hopeId").get(0);
        }catch(Exception ex){
            logger.error("getHOPEEmailId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getHOPEEmailId : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public Object getTendererEmailId(String debarId) {
        logger.debug("getTendererEmailId : "+logUserId+" Starts");
        Object obj = null;
        try{
             obj = hibernateQueryDao.getSingleColQuery("select tlm.emailId from TblLoginMaster tlm,TblDebarmentReq tdr where tdr.debarmentId="+debarId+" and tlm.userId=tdr.userId").get(0);
        }catch(Exception ex){
             logger.error("getTendererEmailId : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getTendererEmailId : "+logUserId+" Ends");
        return obj;
    }

    @Override
    public List<Object[]> findPeSatisfy(String debarId) {
        logger.debug("findPeSatisfy : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            list = hibernateQueryDao.createNewQuery("select tdd.debarIds,tdd.debarStartDt,tdd.debarEdnDt,tdc.comments,tdd.debarTypeId from TblDebarmentDetails tdd,TblDebarmentReq tdc where tdd.debarStatus='bype' and tdd.debarmentBy=tdc.debarmentBy and tdd.tblDebarmentReq.debarmentId=tdc.debarmentId and tdd.tblDebarmentReq.debarmentId="+debarId);
        }catch(Exception ex){
           logger.error("findPeSatisfy : "+logUserId+" : "+ex.toString());
    }
        logger.debug("findPeSatisfy : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
