/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsTaxConfigDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import com.cptu.egp.eps.service.serviceinterface.CmsTaxConfigurationService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsTaxConfigurationServiceImpl implements CmsTaxConfigurationService {

    final Logger logger = Logger.getLogger(CmsTaxConfigurationServiceImpl.class);
    private TblCmsTaxConfigDao cmsTaxConfigDao;
    private String logUserId = "0";
    private final static String STARTS = " Starts ";
    private final static String ENDS = " Ends ";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblCmsTaxConfigDao getCmsTaxConfigDao() {
        return cmsTaxConfigDao;
    }

    public void setCmsTaxConfigDao(TblCmsTaxConfigDao cmsTaxConfigDao) {
        this.cmsTaxConfigDao = cmsTaxConfigDao;
    }

    /***
     * This method inserts a TblCmsTaxConfig object in database
     * @param tblCmsTaxConfig
     * @return int
     */
    @Override
    public int insertCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("insertCmsTaxConfig : " + logUserId + STARTS);
        int flag = 0;
        try {
            cmsTaxConfigDao.addTblCmsTaxConfig(tblCmsTaxConfig);
            flag = tblCmsTaxConfig.getTaxConfigId();
        } catch (Exception ex) {
            logger.error("insertCmsTaxConfig : " + logUserId + " : " + ex);
            flag = 0;
        }
        logger.debug("insertCmsTaxConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsTaxConfig object in DB
     * @param tblCmsTaxConfig
     * @return boolean
     */
    @Override
    public boolean updateCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("updateCmsTaxConfig : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsTaxConfigDao.updateTblCmsTaxConfig(tblCmsTaxConfig);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateCmsTaxConfig : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("updateCmsTaxConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsTaxConfig object in DB
     * @param tblCmsTaxConfig
     * @return boolean
     */
    @Override
    public boolean deleteCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("deleteCmsTaxConfig : " + logUserId + STARTS);
        boolean flag;
        try {
            cmsTaxConfigDao.deleteTblCmsTaxConfig(tblCmsTaxConfig);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteCmsTaxConfig : " + logUserId + " : " + ex);
            flag = false;
        }
        logger.debug("deleteCmsTaxConfig : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblWsOrgMaster objects from database
     * @return List<TblCmsTaxConfig>
     */
    @Override
    public List<TblCmsTaxConfig> getAllCmsTaxConfig() {
        logger.debug("getAllCmsTaxConfig : " + logUserId + STARTS);
        List<TblCmsTaxConfig> cmsTaxConfigList = new ArrayList<TblCmsTaxConfig>();
        try {
            cmsTaxConfigList = cmsTaxConfigDao.getAllTblCmsTaxConfig();
        } catch (Exception ex) {
            logger.error("getAllCmsTaxConfig : " + logUserId + " : " + ex);
        }
        logger.debug("getAllCmsTaxConfig : " + logUserId + ENDS);
        return cmsTaxConfigList;
    }

    /***
     *  This method returns no. of TblCmsTaxConfig objects from database
     * @return long
     */
    @Override
    public long getCmsTaxConfigCount() {
        logger.debug("getCmsTaxConfigCount : " + logUserId + STARTS);
        long cmsTaxConfigCount = 0;
        try {
            cmsTaxConfigCount = cmsTaxConfigDao.getTblCmsTaxConfigCount();
        } catch (Exception ex) {
            logger.error("getCmsTaxConfigCount : " + logUserId + " : " + ex);
        }
        logger.debug("getCmsTaxConfigCount : " + logUserId + ENDS);
        return cmsTaxConfigCount;
    }

    /***
     * This method returns TblCmsTaxConfig for the given Id
     * @param int id
     * @return TblCmsTaxConfig
     */
    @Override
    public TblCmsTaxConfig getCmsTaxConfig(int id) {
        logger.debug("getCmsTaxConfig(int) : " + logUserId + STARTS);
        TblCmsTaxConfig tblCmsTaxConfig = null;
        List<TblCmsTaxConfig> cmsTaxConfigList = null;
        try {
            cmsTaxConfigList = cmsTaxConfigDao.findTblCmsTaxConfig("taxConfigId", Operation_enum.EQ, id);
        } catch (Exception ex) {
            logger.error("getCmsTaxConfig(int) : " + logUserId + " : " + ex);
        }
        if (!cmsTaxConfigList.isEmpty()) {
            tblCmsTaxConfig = cmsTaxConfigList.get(0);
        }
        logger.debug("getCmsTaxConfig(int) : " + logUserId + ENDS);
        return tblCmsTaxConfig;
    }

    @Override
    public TblCmsTaxConfig getLatestCmsTaxConfig(int procurementNatureId) {
        logger.debug("getLatestCmsTaxConfig : " + logUserId + STARTS);
        TblCmsTaxConfig tblCmsTaxConfig = null;
        List<TblCmsTaxConfig> cmsTaxConfigList = null;
        try {
            cmsTaxConfigList = cmsTaxConfigDao.findTblCmsTaxConfig("procurementNatureId", Operation_enum.EQ,
                    procurementNatureId, "isCurrent", Operation_enum.EQ, "yes");
        } catch (Exception ex) {
            logger.error("getLatestCmsTaxConfig : " + logUserId + " : " + ex);
        }
        if (!cmsTaxConfigList.isEmpty()) {
            tblCmsTaxConfig = cmsTaxConfigList.get(0);
        }
        logger.debug("getLatestCmsTaxConfig : " + logUserId + ENDS);
        return tblCmsTaxConfig;
    }
}
