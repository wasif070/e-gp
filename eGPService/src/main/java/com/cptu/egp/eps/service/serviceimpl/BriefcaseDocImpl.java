/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBidDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererFolderMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblBidDocuments;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblTendererFolderMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class BriefcaseDocImpl {

    private static final Logger LOGGER = Logger.getLogger(BriefcaseDocImpl.class);
    private TblCompanyDocumentsDao companyDocumentsDao;
    private TblTendererMasterDao tblTendererMasterDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblTendererFolderMasterDao tblTendererFolderMasterDao;
    private TblBidDocumentsDao tblBidDocumentsDao;
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";

    /**
     *For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *Getter TblCompanyDocumentsDao
     * @return
     */
    public TblCompanyDocumentsDao getCompanyDocumentsDao() {
        return companyDocumentsDao;
    }

    /**
     *setter TblCompanyDocumentsDao
     * @param companyDocumentsDao
     */
    public void setCompanyDocumentsDao(TblCompanyDocumentsDao companyDocumentsDao) {
        this.companyDocumentsDao = companyDocumentsDao;
    }

    /**
     *Getter TblTendererMasterDao
     * @return
     */
    public TblTendererMasterDao getTblTendererMasterDao() {
        return tblTendererMasterDao;
    }

    /**
     *Setter TblTendererMasterDao
     * @param tblTendererMasterDao
     */
    public void setTblTendererMasterDao(TblTendererMasterDao tblTendererMasterDao) {
        this.tblTendererMasterDao = tblTendererMasterDao;
    }

    /**
     *Getter HibernateQueryDao
     * @return
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     *setter HibernateQueryDao
     * @param hibernateQueryDao
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    /**
     *Getter TblTendererFolderMasterDao
     * @return
     */
    public TblTendererFolderMasterDao getTblTendererFolderMasterDao() {
        return tblTendererFolderMasterDao;
    }

    /**
     *setter TblTendererFolderMasterDao
     * @param tblTendererFolderMasterDao
     */
    public void setTblTendererFolderMasterDao(TblTendererFolderMasterDao tblTendererFolderMasterDao) {
        this.tblTendererFolderMasterDao = tblTendererFolderMasterDao;
    }

    /**
     *Getter TblBidDocumentsDao
     * @return
     */
    public TblBidDocumentsDao getTblBidDocumentsDao() {
        return tblBidDocumentsDao;
    }

    /**
     *Setter TblBidDocumentsDao
     * @param tblBidDocumentsDao
     */
    public void setTblBidDocumentsDao(TblBidDocumentsDao tblBidDocumentsDao) {
        this.tblBidDocumentsDao = tblBidDocumentsDao;
    }

    /**
     * this method is for getting the tenderer id from database by passing user id
     * @param userId this is the particular user id which is passed
     * @return integer, if there is a match case in database then return that tenderer id otherwise return 0
     */
    public int getTendererIdFromUserId(int userId) {
        LOGGER.debug("getTendererIdFromUserId : " + logUserId + LOGGERSTART);
        int id = 0;
        try {
            TblTendererMaster tendererMaster = tblTendererMasterDao.findTblTendererMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId)).get(0);
            id = tendererMaster.getTendererId();
        } catch (Exception e) {
            LOGGER.error("getTendererIdFromUserId : " + logUserId + e);
    }
        LOGGER.debug("getTendererIdFromUserId : " + logUserId + LOGGEREND);
        return id;
    }

    /**
     * this method for uploading brief case documents
     * @param tblCompanyDocuments - this is the pojo object
     * @param tenderid - this is the particular tenderer id for whom the doc is going to upload
     * @return integer, if uploaded successfully return int and if not then return 0
     */
    public int uploadBriefcaseDocs(TblCompanyDocuments tblCompanyDocuments, int tenderid) {
        LOGGER.debug("uploadBriefcaseDocs : " + logUserId + LOGGERSTART);
        int companyDocId = 0;
        try {
            tblCompanyDocuments.setTblTendererMaster(new TblTendererMaster(tenderid));
            companyDocumentsDao.addTblCompanyDocuments(tblCompanyDocuments);
            companyDocId = tblCompanyDocuments.getCompanyDocId();
        } catch (Exception e) {
            LOGGER.error("uploadBriefcaseDocs : " + logUserId + e);
        }
        LOGGER.debug("uploadBriefcaseDocs : " + logUserId + LOGGEREND);
            return companyDocId;
        }

    /**
     * this method is for mapping the form documents
     * @param tblBidDocuments this is the pojo objects which is passed
     */
    public void mapFormDoc(TblBidDocuments tblBidDocuments) {
        LOGGER.debug("mapFormDoc : " + logUserId + LOGGERSTART);
        try {
        tblBidDocumentsDao.addTblBidDocuments(tblBidDocuments);
        } catch (Exception e) {
            LOGGER.error("mapFormDoc : " + logUserId + e);
    }
        LOGGER.debug("mapFormDoc : " + logUserId + LOGGEREND);
    }

    /**
     * this method is for updating the briefcase document status by passing the particular documents id
     * @param docId - this is the particular documents id
     * @param status - this is the status
     * @return boolean, ture -if updated, false -if failed
     */
    public boolean updateBriefcaseDocsStatus(int docId, String status) {
        LOGGER.debug("updateBriefcaseDocsStatus : " + logUserId + LOGGERSTART);
        boolean isUpdated;
        try {
            String query = "update TblCompanyDocuments tcd set tcd.docStatus = '" + status + "'  where tcd.companyDocId =" + docId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            isUpdated = true;
        } catch (Exception e) {
            isUpdated = false;
            LOGGER.error("updateBriefcaseDocsStatus : " + logUserId + e);
    }
        LOGGER.debug("updateBriefcaseDocsStatus : " + logUserId + LOGGEREND);
        return isUpdated;
    }

    /**
     * this method is for deleting the briefcase documents from the database by passing the particular docid
     * @param docId - this is the particular id which is passed
     * @return boolean, true - if deleted, false - if failed
     */
    public boolean deleteBriefcaseDocs(int docId) {
        LOGGER.debug("deleteBriefcaseDocs : " + logUserId + LOGGERSTART);
        boolean isDeleted;
        try {
            String query = " delete from TblCompanyDocuments tcd where tcd.companyDocId = " + docId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            isDeleted = true;
        } catch (Exception e) {
            isDeleted = false;
            LOGGER.error("deleteBriefcaseDocs : " + logUserId + e);
        }
        LOGGER.debug("deleteBriefcaseDocs : " + logUserId + LOGGEREND);
        return isDeleted;
    }

    /**
     * this method is for adding the tenderer folder data to database
     * @param folderMaster - this is the pojo object which is passed
     */
    public void addTendererFolderMaster(TblTendererFolderMaster folderMaster) {
        LOGGER.debug("addTendererFolderMaster : " + logUserId + LOGGERSTART);
        try {
        tblTendererFolderMasterDao.addTblTendererFolderMaster(folderMaster);
        } catch (Exception e) {
            LOGGER.error("addTendererFolderMaster : " + logUserId + e);
    }
        LOGGER.debug("addTendererFolderMaster : " + logUserId + LOGGEREND);
    }

    /**
     * this method is for updating the status of mapping of the documents to the map folder
     * @param comDocId - this is the document id
     * @param folderId - this is the map folder id
     * @return boolean, true - if updated, false - if failed
     */
    public boolean updateBriefcaseDocsToMapWithFolder(int comDocId, int folderId) {
        LOGGER.debug("updateBriefcaseDocsToMapWithFolder : " + logUserId + LOGGERSTART);
        boolean isUpdated;
        try {
            String query = "update TblCompanyDocuments tcd set tcd.folderId = " + folderId + "  where tcd.companyDocId =" + comDocId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            isUpdated = true;
        } catch (Exception e) {
            isUpdated = false;
            LOGGER.error("updateBriefcaseDocsToMapWithFolder : " + logUserId + e);
        }
        LOGGER.debug("updateBriefcaseDocsToMapWithFolder : " + logUserId + LOGGEREND);
        return isUpdated;
    }

    /**
     * this method is for verifying the folder name to database in the case of duplication
     * @param folderName - this is the name which is going to match in the database
     * @param tenderid - this is the tenderer id for whom folders name
     * @return string, if match - return "Folder name already exists" if not - "OK"
     */
    public String verifyFolderName(String folderName, int tenderid) {
        LOGGER.debug("verifyFolderName : " + logUserId + LOGGERSTART);
        String msg = null;
        long size = 0;
        try {
            size = tblTendererFolderMasterDao.countForQuery("TblTendererFolderMaster", "folderName = '" + folderName + "' and tblTendererMaster.tendererId = " + tenderid);
            if (size > 0) {
                msg = "Folder name already exists";
            } else {
                msg = "OK";
        }
        } catch (Exception ex) {
            LOGGER.error("verifyFolderName : " + logUserId + ex);
        }
        LOGGER.debug("verifyFolderName : " + logUserId + LOGGEREND);
        return msg;
        }

    /**
     * this method is for deleting the data of the documents which is already mapped with map doc file
     * @param formId - this is the form id
     * @param userId - this is the user id
     * @param comDocId - this is the document id
     * @return boolean, true - if deleted successfully, false - if failed
     */
    public boolean deleteDataMapDocsWithFile(int formId, int userId, int comDocId) {
        LOGGER.debug("deleteDataMapDocsWithFile : " + logUserId + LOGGERSTART);
        boolean isDeleted;
        try {
            String query = " delete from TblBidDocuments tbd where tbd.formId = " + formId + " and tbd.userId =" + userId + " and tbd.companyDocId=" + comDocId;
            hibernateQueryDao.updateDeleteNewQuery(query);
            isDeleted = true;
        } catch (Exception e) {
            isDeleted = false;
            LOGGER.error("deleteDataMapDocsWithFile : " + logUserId + e);
        }
        LOGGER.debug("deleteDataMapDocsWithFile : " + logUserId + LOGGEREND);
        return isDeleted;
    }

    /**
     * this method is for getting the status of mapped and unmapped documents
     * @param formId - this is the form id
     * @param userId - this is the user id
     * @param tenderId - this is the tenderer id
     * @return boolean, true - if mapped, flase - if not
     */
    public boolean isDocumentMapped(int formId, int userId, int tenderId) {
        LOGGER.debug("isDocumentMapped : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            String query = " select tbd.companyDocId,tbd.formId from TblBidDocuments tbd where tbd.formId = " + formId + " and tbd.userId =" + userId + " and tbd.tenderId =" + tenderId;
             List<Object[]> mappedDoc = hibernateQueryDao.createNewQuery(query);
            if (mappedDoc != null) {
                if (!mappedDoc.isEmpty()) {
                     flag = true;
                 }
             }

        } catch (Exception e) {
            flag = false;
            LOGGER.error("isDocumentMapped : " + logUserId + e);
        }
        LOGGER.debug("isDocumentMapped : " + logUserId + LOGGEREND);
             return flag;
        }

    /**
     * this method is for getting the remaining documents from the database
     * @param tenderId - this is the tenderer id
     * @param formId - this is the form id
     * @param userId - this is the user id
     * @return long, if there is number of documents then return count , if not then return 0
     */
    public long getRemainingDocCount(String tenderId,String formId,String userId){
        LOGGER.debug("getRemainingDocCount : " + logUserId + LOGGERSTART);
        long cnt=0;
        try {
            StringBuilder where = new StringBuilder();
            where.append("tmd.tenderFormId="+formId+" and tmd.tenderId="+tenderId+" and tmd.tenderFormDocId not in ");
            where.append("(select distinct tbd.manDocId from TblBidDocuments tbd where tbd.tenderId="+tenderId+" and tbd.formId="+formId+" and tbd.userId="+userId+")");
            cnt = hibernateQueryDao.countForNewQuery("TblTenderMandatoryDoc tmd", where.toString());
        } catch (Exception ex) {
            LOGGER.error("getRemainingDocCount "+logUserId,ex);
    }
        LOGGER.debug("getRemainingDocCount : " + logUserId + LOGGEREND);
        return cnt;
    }
}
