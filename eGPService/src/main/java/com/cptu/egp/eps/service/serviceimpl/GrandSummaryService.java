/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDetailDao;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import com.cptu.egp.eps.model.table.TblTenderGrandSumDetail;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
public class GrandSummaryService {

    final Logger logger = Logger.getLogger(GrandSummaryService.class);
    private String logUserId ="0";
    
    TblTenderGrandSumDao tblTenderGrandSumDao;
    TblTenderGrandSumDetailDao tblTenderGrandSumDetailDao;
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public TblTenderGrandSumDao getTblTenderGrandSumDao() {
        return tblTenderGrandSumDao;
    }

    public TblTenderGrandSumDetailDao getTblTenderGrandSumDetailDao() {
        return tblTenderGrandSumDetailDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setTblTenderGrandSumDao(TblTenderGrandSumDao tblTenderGrandSumDao) {
        this.tblTenderGrandSumDao = tblTenderGrandSumDao;
    }

    public void setTblTenderGrandSumDetailDao(TblTenderGrandSumDetailDao tblTenderGrandSumDetailDao) {
        this.tblTenderGrandSumDetailDao = tblTenderGrandSumDetailDao;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * Add grand summary details
     * @param gs Grand summary master information
     * @param lstGsDtl Grand summary detail information
     * @param delFlag true if delete/insert perform else insert operation perform
     * @return no of records affected from operation
     */
    public int insertDataForGrandSummary(TblTenderGrandSum gs, List<TblTenderGrandSumDetail> lstGsDtl, int delFlag){
        logger.debug("insertDataForGrandSummary : "+logUserId+" Starts");
        int gsId = -1;
        System.out.println("delFlag="+delFlag);
        try{
            delFlag=isGrandSummaryCreated(gs.getTblTenderMaster().getTenderId(),gs.getPkgLotId());
            if(delFlag != -1){
                hibernateQueryDao.updateDeleteNewQuery("delete from TblTenderGrandSum ttgs where ttgs.tenderSumId = " + delFlag);
            }
            tblTenderGrandSumDao.addTblTenderGrandSum(gs);
            gsId = gs.getTenderSumId();
            if(gs!=null){
                gs = null;
            }
            for(int i=0; i<lstGsDtl.size(); i++){
                TblTenderGrandSumDetail gsDtl = lstGsDtl.get(i);
                gsDtl.setTblTenderGrandSum(new TblTenderGrandSum(gsId));
                tblTenderGrandSumDetailDao.addTblTenderGrandSumDetail(gsDtl);
                if(gsDtl!=null){
                    gsDtl = null;
                }
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
            logger.error("insertDataForGrandSummary : "+logUserId+" : "+ex.toString());
        }
        logger.debug("insertDataForGrandSummary : "+logUserId+" Ends");
            return gsId;
        }

    /**
     * check if grand summary created or not
     * @param tenderId
     * @param porlId
     * @return 0 if not created else return non 0 value
     */
    public int isGrandSummaryCreated(int tenderId, int porlId){
        logger.debug("isGrandSummaryCreated : "+logUserId+" Starts");
        List<Object> obj;
        int gsId = -1;
        try{
            obj = hibernateQueryDao.getSingleColQuery("select tgs.tenderSumId from TblTenderGrandSum tgs where tgs.tblTenderMaster.tenderId = " + tenderId+" and tgs.pkgLotId = "+porlId);
            if(obj != null){
                if(obj.size() > 0){
                    gsId = Integer.parseInt(obj.get(0).toString());
                }
                obj = null;
            }
        }catch(Exception ex){
            logger.error("isGrandSummaryCreated : "+logUserId+" : "+ex.toString());
        }
        logger.debug("isGrandSummaryCreated : "+logUserId+" Ends");
            return gsId;
        }

    /**
     * Get Grand summary Name
     * @param tenderGSId
     * @return Grand Summary Name
     */
    public String getGrandSummaryName(int tenderGSId){
        logger.debug("getGrandSummaryName : "+logUserId+" Starts");
        List<Object> obj;
        String gsName = "";
        try{
            obj = hibernateQueryDao.getSingleColQuery("select tgs.summaryName from TblTenderGrandSum tgs where tgs.tenderSumId = " + tenderGSId);
            if(obj != null){
                if(obj.size() > 0){
                    gsName = obj.get(0).toString();
                }
                obj = null;
            }
        }catch(Exception ex){
             logger.error("getGrandSummaryName : "+logUserId+" : "+ex.toString());
        }
         logger.debug("getGrandSummaryName : "+logUserId+" Ends");
            return gsName;
        }

    /**
     * Get Grand Summary Form details
     * @param tenderGSId
     * @param getFormId if true then it will return tenderformid Map else return CellId Map
     * @return tenderformid / cellid
     */
    public Map<Integer, Integer> getGSFormsDtl(int tenderGSId, boolean getFormId) {
        logger.debug("getGSFormsDtl : "+logUserId+" Starts");
        String strQuery = "select tgs.tenderFormId, tgs.cellId from TblTenderGrandSumDetail tgs where tgs.tblTenderGrandSum.tenderSumId = " + tenderGSId;
        List<Object[]> objs = new ArrayList<Object[]>();
        List<TblTenderGrandSumDetail> cols = new ArrayList<TblTenderGrandSumDetail>();
        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            Integer j = 0;
            for(Object[] i : objs){
                cols.add(new TblTenderGrandSumDetail(Integer.parseInt(i[0].toString()), Integer.parseInt(i[1].toString())));
                if(getFormId){
                    hm.put(j, (Integer) i[0]);
                }else{
                    hm.put(j, (Integer) i[1]);
                }
                j++;
            }
        } catch (Exception ex) {
            logger.error("getGSFormsDtl : "+logUserId+" : "+ex.toString());
            objs = null;
        }
         logger.debug("getGSFormsDtl : "+logUserId+" Ends");
        return hm;
    }

    /**
     * Get Grand Summary Form id and table id
     * @param tenderGSId
     * @return map having formid, table id
     */
     public Map<Integer, Integer> getGSFormsTblDtl(int tenderGSId) {
        logger.debug("getGSFormsDtl : "+logUserId+" Starts");
        String strQuery = "select tgs.tenderFormId,tgs.tenderTableId, tgs.cellId from TblTenderGrandSumDetail tgs where tgs.tblTenderGrandSum.tenderSumId = " + tenderGSId;
        List<Object[]> objs = new ArrayList<Object[]>();
        Map<Integer, Integer> hm = new HashMap<Integer, Integer>();
        try {
            objs = hibernateQueryDao.createNewQuery(strQuery);
            for(Object[] i : objs)
            {
                hm.put((Integer) i[1], (Integer) i[2]);
            }
        } catch (Exception ex) {
            logger.error("getGSFormsDtl : "+logUserId+" : "+ex.toString());
            objs = null;
        }
         logger.debug("getGSFormsDtl : "+logUserId+" Ends");
        return hm;
    }

     /**
      * To check whether grand summary created or not by passing tenderid
      * @param tenderId
      * @return -1 grand summary created else it will return generated grand summary id
      */
      public int isGrandSummaryCreated(int tenderId){
        logger.debug("isGrandSummaryCreated : "+logUserId+" Starts");
        List<Object> obj;
        int gsId = -1;
        List<Object> LotNo;
        try{
            obj = hibernateQueryDao.getSingleColQuery("select tgs.tenderSumId from TblTenderGrandSum tgs where tgs.tblTenderMaster.tenderId = " + tenderId);
            LotNo = hibernateQueryDao.getSingleColQuery("select COUNT(tenderLotId) from TblTenderLots where tenderId = " + tenderId);
            if(obj != null){
                if(obj.size() > 0){
                    long lotno = (long) LotNo.get(0);
                    if(obj.size() == (int)lotno){
                        gsId = Integer.parseInt(obj.get(0).toString());
                     }
                }
                obj = null;
            }
        }catch(Exception ex){
            logger.error("isGrandSummaryCreated : "+logUserId+" : "+ex.toString());
        }
        logger.debug("isGrandSummaryCreated : "+logUserId+" Ends");
            return gsId;
        }
}
