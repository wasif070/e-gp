/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblContentVerification;
import java.util.List;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public interface ContentVeriConfigService {


    public void addContent(TblContentVerification tblContentVerification);
    
    public List<TblContentVerification> getTblContentVerification();

    public void setLogUserId(String logUserId);
}
