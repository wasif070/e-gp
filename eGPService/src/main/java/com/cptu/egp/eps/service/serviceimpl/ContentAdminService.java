/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyDocumentsDao;
import com.cptu.egp.eps.dao.daointerface.TblCompanyMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblRegDocReceiptDao;
import com.cptu.egp.eps.dao.daointerface.TblBiddingPermissionDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererAuditTrailDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblUserRegInfoDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPUserApproval;
import com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblRegDocReceipt;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import com.cptu.egp.eps.model.table.TblUserRegInfo;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ContentAdminService {

    static final Logger logger = Logger.getLogger(ContentAdminService.class);
    SPUserApproval sPUserApproval;

    public SPUserApproval getsPUserApproval() {
        return sPUserApproval;
    }
    TblCompanyMasterDao tblCompanyMasterDao;
    TblBiddingPermissionDao tblBiddingPermissionDao;
    TblTendererMasterDao tblTendererMasterDao;
    TblCompanyDocumentsDao tblCompanyDocumentsDao;
    HibernateQueryDao hibernateQueryDao;
    TblTendererAuditTrailDao tblTendererAuditTrailDao;
    TblRegDocReceiptDao tblRegDocReceiptDao;
    TblUserRegInfoDao tblUserRegInfoDao;
    SPAddUser sPAddUser;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private String starts = " Starts";
    private String ends = " Ends";
    SPTenderCommon sPTenderCommon;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    public TblRegDocReceiptDao getTblRegDocReceiptDao() {
        return tblRegDocReceiptDao;
    }

    public void setTblRegDocReceiptDao(TblRegDocReceiptDao tblRegDocReceiptDao) {
        this.tblRegDocReceiptDao = tblRegDocReceiptDao;
    }

    public TblTendererAuditTrailDao getTblTendererAuditTrailDao() {
        return tblTendererAuditTrailDao;
    }

    public void setTblTendererAuditTrailDao(TblTendererAuditTrailDao tblTendererAuditTrailDao) {
        this.tblTendererAuditTrailDao = tblTendererAuditTrailDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblCompanyDocumentsDao getTblCompanyDocumentsDao() {
        return tblCompanyDocumentsDao;
    }

    public void setTblCompanyDocumentsDao(TblCompanyDocumentsDao tblCompanyDocumentsDao) {
        this.tblCompanyDocumentsDao = tblCompanyDocumentsDao;
    }

    public TblTendererMasterDao getTblTendererMasterDao() {
        return tblTendererMasterDao;
    }

    public void setTblTendererMasterDao(TblTendererMasterDao tblTendererMasterDao) {
        this.tblTendererMasterDao = tblTendererMasterDao;
    }

    public TblCompanyMasterDao getTblCompanyMasterDao() {
        return tblCompanyMasterDao;
    }

    public void setTblCompanyMasterDao(TblCompanyMasterDao tblCompanyMasterDao) {
        this.tblCompanyMasterDao = tblCompanyMasterDao;
    }

    public void setsPUserApproval(SPUserApproval sPUserApproval) {
        this.sPUserApproval = sPUserApproval;
    }

    public TblUserRegInfoDao getTblUserRegInfoDao() {
        return tblUserRegInfoDao;
    }

    public void setTblUserRegInfoDao(TblUserRegInfoDao tblUserRegInfoDao) {
        this.tblUserRegInfoDao = tblUserRegInfoDao;
    }

    /**
     * CompanyVerification Grid Data from SP.
     *
     * @param email search on email
     * @param cmpRegNo search on company RegNo
     * @param cmpName search on Company Name
     * @param status search on Status
     * @param regType search on Company RegNo
     * @param regDateFrom search on UserReg Date
     * @param regDateTo search on UserReg Date
     * @param page search on page No
     * @param recPerPage search on Record per page
     * @return List of data for grid
     */
    public List<UserApprovalBean> userApprovalList(String email, String cmpRegNo, String cmpName, String status, String regType, Date regDateFrom, Date regDateTo, int page, int recPerPage, Integer companyId, String sortString) {
        logger.debug("userApprovalList : " + logUserId + starts);
        logger.debug("userApprovalList : " + logUserId + ends);
        return sPUserApproval.executeProcedure(email, cmpRegNo, cmpName, status, regType, regDateFrom, regDateTo, page, recPerPage, companyId, sortString);
    }

    /**
     * List of TblCompanyMaster on search criteria
     *
     * @param values variable parameters
     * @return List of TblCompanyMaster
     * @throws Exception
     */
    public List<TblCompanyMaster> findCompanyMaster(Object... values) throws Exception {
        logger.debug("findCompanyMaster : " + logUserId + starts);
        List<TblCompanyMaster> list = new ArrayList<TblCompanyMaster>();
        String action = null;
        try {
            list = tblCompanyMasterDao.findTblCompanyMaster(values);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("findCompanyMaster : " + logUserId, e);
            action = "Error in View Profile " + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, values[2] instanceof TblLoginMaster ? ((TblLoginMaster) values[2]).getUserId() : (Integer) values[2], values[2] instanceof TblLoginMaster ? "userId" : "companyId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("findCompanyMaster : " + logUserId + ends);
        return list;
    }

    public List<TblBiddingPermission> findBiddingpermission(Object... values) throws Exception {
        logger.debug("findBiddingPermission : " + logUserId + starts);
        List<TblBiddingPermission> list = new ArrayList<TblBiddingPermission>();
        String action = null;
        try {
            //list = tblCompanyMasterDao.findTblCompanyMaster(values);
            list = tblBiddingPermissionDao.findTblBiddingPermission(values);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("findBiddingPermission : " + logUserId, e);
            action = "Error in View Profile " + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, values[2] instanceof TblLoginMaster ? ((TblLoginMaster) values[2]).getUserId() : (Integer) values[2], values[2] instanceof TblLoginMaster ? "userId" : "companyId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("findBiddingPermission : " + logUserId + ends);
        return list;
    }

    /**
     * List of TblTendererMaster on search criteria
     *
     * @param values variable parameters
     * @return List of TblTendererMaster
     * @throws Exception
     */
    public List<TblTendererMaster> findTblTendererMasters(Object... values) throws Exception {
        logger.debug("findTblTendererMasters : " + logUserId + starts);
        List<TblTendererMaster> list = new ArrayList<TblTendererMaster>();
        String action = null;
        try {
            list = tblTendererMasterDao.findTblTendererMaster(values);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("findTblTendererMasters : " + logUserId, e);
            action = "Error in View Profile " + e;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, values[2] instanceof TblLoginMaster ? ((TblLoginMaster) values[2]).getUserId() : (Integer) values[2], values[2] instanceof TblLoginMaster ? "userId" : "tendererId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("findTblTendererMasters : " + logUserId + ends);
        return list;
    }
    
    
    public List<TblTendererAuditTrail> findTblTendererAuditTrail(Object... values) throws Exception {
        logger.debug("findTblTendererAuditTrail : " + logUserId + starts);
        List<TblTendererAuditTrail> list = new ArrayList<TblTendererAuditTrail>();
        String action = null;
        try {
            list = tblTendererAuditTrailDao.findTblTendererAuditTrail(values);
            action = "View Profile";
        } catch (Exception e) {
            logger.error("findTblTendererAuditTrail : " + logUserId, e);
            action = "Error in View Records " + e;
//        } finally {
//            makeAuditTrailService.generateAudit(auditTrail, values[2] instanceof TblLoginMaster ? ((TblLoginMaster) values[2]).getUserId() : (Integer) values[2], values[2] instanceof TblLoginMaster ? "userId" : "tendererId", EgpModule.My_Account.getName(), action, "");
        }
        logger.debug("findTblTendererAuditTrail : " + logUserId + ends);
        return list;
    }

    /**
     * List of TblCompanyDocuments on search criteria
     *
     * @param values variable parameters
     * @return List of TblCompanyDocuments
     * @throws Exception
     */
    public List<TblCompanyDocuments> findCompanyDocumentses(Object... values) throws Exception {
        logger.debug("findCompanyDocumentses : " + logUserId + starts);
        logger.debug("findCompanyDocumentses : " + logUserId + ends);
        return tblCompanyDocumentsDao.findTblCompanyDocuments(values);
    }

    /**
     * JV company Details by HQL
     *
     * @param companyId companyId from TblCompanyMaster
     * @return companyName and companyRegNo
     */
    public List<Object[]> findJvCompanyDetail(int companyId) {
        logger.debug("findJvCompanyDetail : " + logUserId + starts);
        logger.debug("findJvCompanyDetail : " + logUserId + ends);
        return hibernateQueryDao.createNewQuery("select cm.companyName, cm.companyRegNumber,cjv.jvRole from TblCompanyJointVenture cjv,TblCompanyMaster cm  where cjv.tblCompanyMaster.companyId=cm.companyId and cjv.companyId=" + companyId);
    }

    /**
     * Change User Status and makes entry in Log Table about event
     *
     * @param adminId person who changed status
     * @param userId person whose status changed
     * @param status (pending,approved,rejected)
     */
    public String changeUserStatus(int adminId, int userId, String status, String comments, String cmpId, boolean exeIsAdmin) {
        logger.debug("changeUserStatus : " + logUserId + starts);
        String emailMobile = null;
        String action = null;
        try {
            TblUserRegInfo tblUserRegInfo = new TblUserRegInfo();

            Object[] objects = hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo,lm.validUpTo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId=" + userId).get(0);
            emailMobile = (String) objects[0] + "$" + (String) objects[1] + (String) objects[2];

            //Date validUpto = (Date) objects[3];
            Calendar cal = Calendar.getInstance();
            Date validUpto = cal.getTime();
            validUpto.setYear(validUpto.getYear() + 50);
            DateFormat dtFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");;

            int i = hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set validUpTo = '" + dtFormat.format(validUpto) + "',status='" + status + "' where userId=" + userId);
            if (i == 1) {

                tblUserRegInfo.setAction("Registration");
                tblUserRegInfo.setActionBy(adminId);
                tblUserRegInfo.setUserId(userId);
                tblUserRegInfo.setActionDate(new java.util.Date());
                tblUserRegInfo.setExpiryDate(dtFormat.parse(dtFormat.format(validUpto)));
                tblUserRegInfoDao.addTblUserRegInfo(tblUserRegInfo);

                if (exeIsAdmin) {
                    int j = hibernateQueryDao.updateDeleteNewQuery("update TblTendererMaster set isAdmin='no' where tblLoginMaster.userId not in (" + userId + ") and tblCompanyMaster.companyId=" + cmpId);
                    if (j == 1) {
                        tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));
                    }
                } else {
                    tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));
                }
            }
            action = "Verification of Profile";
        } catch (Exception e) {
            action = "Error In Verification of Profile : " + e;
            logger.error("changeUserStatus : " + logUserId, e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.New_User_Registration.getName(), action, comments);
            action = null;
        }
        logger.debug("changeUserStatus : " + logUserId + ends);
        return emailMobile;
    }

    public String rejectUser(int adminId, int userId, String status, String comments, String cmpId, boolean exeIsAdmin) {
        logger.debug("rejectUser : " + logUserId + starts);
        String emailMobile = null;
        String action = null;
        boolean flag = false;

        try {
            TblUserRegInfo tblUserRegInfo = new TblUserRegInfo();

            Object[] objects = hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo,lm.validUpTo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId=" + userId).get(0);
            emailMobile = (String) objects[0] + "$" + (String) objects[1] + (String) objects[2];

            //Date validUpto = (Date) objects[3];
            Calendar cal = Calendar.getInstance();
            Date validUpto = cal.getTime();
            validUpto.setYear(validUpto.getYear() + 1);
            DateFormat dtFormat = new SimpleDateFormat("yyyy-MMMM-dd, HH:mm:ss");;

            int i = hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set validUpTo = '" + dtFormat.format(validUpto) + "' where userId=" + userId);
            if (i == 1) {

                tblUserRegInfo.setAction("Registration");
                tblUserRegInfo.setActionBy(adminId);
                tblUserRegInfo.setUserId(userId);
                tblUserRegInfo.setActionDate(new java.util.Date());
                tblUserRegInfo.setExpiryDate(dtFormat.parse(dtFormat.format(validUpto)));
                tblUserRegInfoDao.addTblUserRegInfo(tblUserRegInfo);

                if (exeIsAdmin) {
                    int j = hibernateQueryDao.updateDeleteNewQuery("update TblTendererMaster set isAdmin='no' where tblLoginMaster.userId not in (" + userId + ") and tblCompanyMaster.companyId=" + cmpId);
                    if (j == 1) {
                        tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));
                    }
                } else {
                    tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));
                }
                List<CommonMsgChk> commonMsgList = new ArrayList<CommonMsgChk>();
                commonMsgList = sPAddUser.executeProcedure(userId, "BidderRejection", "");
                //CommonMsgChk commonMsgChk;
                //commonMsgChk = sPAddUser.executeProcedure(userId, "BidderRejection", "").get(0);
                action = "Rejection of Profile";
                if (commonMsgList.get(0).getFlag() == true) {
                    flag = true;
                }
            }

        } catch (Exception e) {
            action = "Error In Verification of Profile : " + e;
            logger.error("rejectUser : " + logUserId, e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.New_User_Registration.getName(), action, comments);
            action = null;
        }
        logger.debug("rejectUser : " + logUserId + ends);
        return emailMobile;
    }

    public String changeProcurementCategoryStatus(int adminId, int userId, String status, String comments, String cmpId, boolean exeIsAdmin) {
        logger.debug("changeProcurementCategoryStatus : " + logUserId + starts);
        String emailMobile = null;
        String action = null;
        try {
            TblUserRegInfo tblUserRegInfo = new TblUserRegInfo();

            Object[] objects = hibernateQueryDao.createNewQuery("select lm.emailId,cm.countryCode,tm.mobileNo,lm.validUpTo from TblLoginMaster lm,TblTendererMaster tm,TblCountryMaster cm where tm.country=cm.countryName and lm.userId=tm.tblLoginMaster.userId and lm.userId=" + userId).get(0);
            emailMobile = (String) objects[0] + "$" + (String) objects[1] + (String) objects[2];

            if (status.equalsIgnoreCase("approved")) {
                //update bidding permission
                int deletePermission = hibernateQueryDao.updateDeleteNewQuery("DELETE FROM TblBiddingPermission WHERE userId = " + userId + " and companyId=" + cmpId + " AND isReapplied=0");
                int updatePermission = hibernateQueryDao.updateDeleteNewQuery("UPDATE TblBiddingPermission set isReapplied=0 where userId = " + userId + " and companyId=" + cmpId);
                //update tenderer company documents
                int tendererId = (int) hibernateQueryDao.singleColQuery("select tendererId from TblTendererMaster where tblLoginMaster.userId = " + userId + " and tblCompanyMaster.companyId=" + cmpId).get(0);
                //int deleteDocs = hibernateQueryDao.updateDeleteNewQuery("DELETE FROM TblCompanyDocuments WHERE tblTendererMaster.tendererId = " + tendererId + " AND IsReapplied=0");
                int updateDocs = hibernateQueryDao.updateDeleteNewQuery("UPDATE TblCompanyDocuments set isReapplied=0 where tblTendererMaster.tendererId  = " + tendererId);
                tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));

            } else {
                int deletePermission = hibernateQueryDao.updateDeleteNewQuery("DELETE FROM TblBiddingPermission WHERE isReapplied=1 AND userId = " + userId + " and companyId=" + cmpId);
                int tendererId = (int) hibernateQueryDao.singleColQuery("select tendererId from TblTendererMaster where tblLoginMaster.userId = " + userId + " and tblCompanyMaster.companyId=" + cmpId).get(0);
                //int deleteDocs = hibernateQueryDao.updateDeleteNewQuery("DELETE FROM TblCompanyDocuments WHERE tblTendererMaster.tendererId = " + tendererId + " AND IsReapplied=1");
                int updateDocs = hibernateQueryDao.updateDeleteNewQuery("UPDATE TblCompanyDocuments set isReapplied=0 where tblTendererMaster.tendererId  = " + tendererId);
                tblTendererAuditTrailDao.addTblTendererAuditTrail(new TblTendererAuditTrail(0, userId, new Date(), status + "@" + comments, String.valueOf(adminId)));
            }

            action = "Verification of Profile";
        } catch (Exception e) {
            action = "Error In Verification of Profile : " + e;
            logger.error("changeProcurementCategoryStatus : " + logUserId, e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.New_User_Registration.getName(), action, comments);
            action = null;
        }
        logger.debug("changeProcurementCategoryStatus : " + logUserId + ends);
        return emailMobile;
    }

    public Object getCompanyId(String userId) {
        logger.debug("getCompanyId : " + logUserId + starts);
        List<Object> list = hibernateQueryDao.singleColQuery("select ttm.tblCompanyMaster.companyId from TblCompanyMaster tcm,TblTendererMaster ttm where ttm.isAdmin='yes' and ttm.tblCompanyMaster.companyId=tcm.companyId and ttm.tblLoginMaster.userId=" + userId);
        Object data = null;
        if (!list.isEmpty()) {
            data = list.get(0);
        }
        logger.debug("getCompanyId : " + logUserId + ends);
        return data;
    }

    public Object[] getCompanyTendererId(String userId) {
        logger.debug("getCompanyTendererId : " + logUserId + starts);
        logger.debug("getCompanyTendererId : " + logUserId + ends);
        return hibernateQueryDao.createNewQuery("select ttm.tblCompanyMaster.companyId,ttm.tendererId, tlm.registrationType, tlm.isJvca, ttm.isAdmin from TblTendererMaster ttm, TblLoginMaster tlm where ttm.tblLoginMaster.userId = tlm.userId and ttm.tblLoginMaster.userId=" + userId).get(0);
    }

    public boolean insertReceiptDoc(TblRegDocReceipt tblRegDocReceipt) {
        logger.debug("insertReceiptDoc : " + logUserId + starts);
        boolean flag = false;
        String action = null;
        try {
            tblRegDocReceiptDao.addTblRegDocReceipt(tblRegDocReceipt);
            flag = true;
            action = "Receipt of Physical Documents";
        } catch (Exception e) {
            action = "Error in Receipt of Physical Documents : " + e;
            logger.error("insertReceiptDoc : " + logUserId + " " + e);
            flag = false;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, tblRegDocReceipt.getTblLoginMaster().getUserId(), "userId", EgpModule.New_User_Registration.getName(), action, tblRegDocReceipt.getComments());
            action = null;
        }
        logger.debug("insertReceiptDoc : " + logUserId + ends);
        return flag;
    }

    public SPTenderCommonData emailAndCompanyName(String userId) {
        logger.debug("emailAndCompanyName : " + logUserId + starts);
        logger.debug("emailAndCompanyName : " + logUserId + ends);
        return sPTenderCommon.executeProcedure("GetCompNameByUserid", userId, null).get(0);
    }

    public List<TblRegDocReceipt> getDocReceipt(int userId) throws Exception {
        logger.debug("getDocReceipt : " + logUserId + starts);
        logger.debug("getDocReceipt : " + logUserId + ends);
        return tblRegDocReceiptDao.findTblRegDocReceipt("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
    }

    public String contentAdminReciept(String userId) {
        logger.debug("contentAdminReciept : " + logUserId + starts);
        List<Object> list = hibernateQueryDao.getSingleColQuery("select tam.fullName from TblAdminMaster tam where tam.tblLoginMaster.userId=" + userId);
        String data = null;
        if (!list.isEmpty()) {
            data = list.get(0).toString();
        }
        logger.debug("contentAdminReciept : " + logUserId + ends);
        return data;
    }

    public List<Object[]> getCompanyAdminData(int offset, int row, String cId, String status, String strSortCol, String strSortOrd) {
        logger.debug("getCompanyAdminData : " + logUserId + starts);
        StringBuilder sb = new StringBuilder();
        sb.append("select tlm.userId,ttm.tendererId,tlm.emailId,ttm.firstName,ttm.lastName,ttm.country,ttm.state,ttm.department,tlm.status ");
        sb.append("from TblLoginMaster tlm,TblTendererMaster ttm ");
        sb.append("where tlm.userId=ttm.tblLoginMaster.userId and ttm.isAdmin='no' and tlm.status='" + status + "' and ttm.tblCompanyMaster.companyId=" + cId + " order by " + strSortCol + " " + strSortOrd);
        logger.debug("getCompanyAdminData : " + logUserId + ends);
        return hibernateQueryDao.createByCountNewQuery(sb.toString(), offset, row);
    }

    public long getCountCompanyAdminData(String cId, String status) throws Exception {
        logger.debug("getCountCompanyAdminData : " + logUserId + starts);
        logger.debug("getCountCompanyAdminData : " + logUserId + ends);
        return hibernateQueryDao.countForNewQuery("TblLoginMaster tlm,TblTendererMaster ttm", "tlm.userId=ttm.tblLoginMaster.userId and ttm.isAdmin='no' and tlm.status='" + status + "' and ttm.tblCompanyMaster.companyId=" + cId);
    }

    public String jvcaName(String newUserId) {
        logger.debug("jvcaName : " + logUserId + " Starts ");
        Object obj = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select jv.jvname from TblJointVenture jv where jv.newJvuserId=" + newUserId);
            if (!list.isEmpty()) {
                obj = list.get(0);
            }

        } catch (Exception e) {
            logger.error("jvcaName : " + logUserId + " : " + e);

        }
        logger.debug("jvcaName : " + logUserId + " Ends ");
        return obj.toString();
    }
    
    public String jvcaID(String newUserId) {
        logger.debug("jvcaID : " + logUserId + " Starts ");
        Object obj = null;
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select jv.jvid from TblJointVenture jv where jv.newJvuserId=" + newUserId);
            if (!list.isEmpty()) {
                obj = list.get(0);
            }

        } catch (Exception e) {
            logger.error("jvcaID : " + logUserId + " : " + e);

        }
        logger.debug("jvcaID : " + logUserId + " Ends ");
        return obj.toString();
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public Object getUserApprovedTenderId(String userId) {
        logger.debug("getCompanyTendererId : " + logUserId + starts);
        logger.debug("getCompanyTendererId : " + logUserId + ends);
        return hibernateQueryDao.createNewQuery("select status from TblLoginMaster where userId = " + userId);
    }

    public TblBiddingPermissionDao getTblBiddingPermissionDao() {
        return tblBiddingPermissionDao;
    }

    public void setTblBiddingPermissionDao(TblBiddingPermissionDao tblBiddingPermissionDao) {
        this.tblBiddingPermissionDao = tblBiddingPermissionDao;
    }

    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }

}
