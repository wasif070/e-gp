/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDesignationMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminTransferDao;
import com.cptu.egp.eps.dao.daointerface.TblScBankDevPartnerMasterDao;
import com.cptu.egp.eps.dao.daointerface.VwSBDevPartnerDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.util.NumberUtils;

/**
 *
 * @author Administrator
 */
public class ScBankDevpartnerServiceImpl implements ScBankDevpartnerService {

    static final Logger logger = Logger.getLogger(ScBankDevpartnerServiceImpl.class);
    private String logUserId = "0";
    TblScBankDevPartnerMasterDao tblScBankDevPartnerMasterDao;
    TblDesignationMasterDao tblDesignationMasterDao;
    TblLoginMasterDao tblLoginMasterDao;
    TblPartnerAdminDao tblPartnerAdminDao;
    VwSBDevPartnerDao vwSBDevPartnerDao;
    HibernateQueryDao hibernateQueryDao;
    TblPartnerAdminTransferDao tblPartnerAdminTransferDao;
    private MakeAuditTrailService makeAuditTrailService;
    private AuditTrail auditTrail;

    public TblPartnerAdminTransferDao getTblPartnerAdminTransferDao() {
        return tblPartnerAdminTransferDao;
    }

    public void setTblPartnerAdminTransferDao(TblPartnerAdminTransferDao tblPartnerAdminTransferDao) {
        this.tblPartnerAdminTransferDao = tblPartnerAdminTransferDao;
    }

    

    public VwSBDevPartnerDao getVwSBDevPartnerDao() {
        return vwSBDevPartnerDao;
    }

    public void setVwSBDevPartnerDao(VwSBDevPartnerDao vwSBDevPartnerDao) {
        this.vwSBDevPartnerDao = vwSBDevPartnerDao;
    }

    public TblDesignationMasterDao getTblDesignationMasterDao() {
        return tblDesignationMasterDao;
    }

    public void setTblDesignationMasterDao(TblDesignationMasterDao tblDesignationMasterDao) {
        this.tblDesignationMasterDao = tblDesignationMasterDao;
    }

    public TblScBankDevPartnerMasterDao getTblScBankDevPartnerMasterDao() {
        return tblScBankDevPartnerMasterDao;
    }

    public void setTblScBankDevPartnerMasterDao(TblScBankDevPartnerMasterDao tblScBankDevPartnerMasterDao) {
        this.tblScBankDevPartnerMasterDao = tblScBankDevPartnerMasterDao;
    }
    
     public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    @Override
    public int createScBankDevPartner(TblScBankDevPartnerMaster tblScBankDevPartnerMaster) {
        logger.debug("createScBankDevPartner : " + logUserId + " Starts");
        int iRetValue = 0;
        String action = "";
        
        if("Development".equalsIgnoreCase(tblScBankDevPartnerMaster.getPartnerType())){
            if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                 action  = "Create Regional/Country Office";
            }else{
                action  = "Create Development Partner";
            }
        }else{
            if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                action  = "Create Scheduled Bank Branch";
            }else{
                action  = "Create Scheduled Bank";
            }
        }
        try {
            tblScBankDevPartnerMasterDao.addTblScBankDevPartnerMaster(tblScBankDevPartnerMaster);
            iRetValue = tblScBankDevPartnerMaster.getSbankDevelopId();
            
        } catch (Exception ex) {
            logger.error("createScBankDevPartner : " + logUserId + ex);
            iRetValue = -1;
            action = action + " Error " +ex.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getPartnerType()+"Id", EgpModule.Manage_Users.getName(), action, "");
            auditTrail=null;
        }
        logger.debug("createScBankDevPartner : " + logUserId + " Ends");
        return iRetValue;
        }

    @Override
    public List<TblScBankDevPartnerMaster> getBankPartnerList(String type) {
        logger.debug("createScBankDevPartner : " + logUserId + " Ends");
        List<TblScBankDevPartnerMaster> list = null;
        try {
            Object[] values = {"partnerType", Operation_enum.LIKE, type, "isBranchOffice", Operation_enum.LIKE, "No", "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC};
            list = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster(values);

        } catch (Exception ex) {
            logger.error("createScBankDevPartner : " + logUserId + ex);
            list = new ArrayList<TblScBankDevPartnerMaster>();
        }
        logger.debug("createScBankDevPartner : " + logUserId + " Ends");
        return list;
        }

    @Override
    public List<TblDesignationMaster> getDesignationMasters() {
        logger.debug("getDesignationMasters : " + logUserId + " Starts");
        List<TblDesignationMaster> list = null;
        try {
            list = tblDesignationMasterDao.getAllDesinations();
        } catch (Exception ex) {
            logger.error("getDesignationMasters : " + logUserId + ex);
            list = new ArrayList<TblDesignationMaster>();
        }
        logger.debug("getDesignationMasters : " + logUserId + " Ends");
        return list;
        }

    public TblLoginMasterDao getTblLoginMasterDao() {
        return tblLoginMasterDao;
    }

    public void setTblLoginMasterDao(TblLoginMasterDao tblLoginMasterDao) {
        this.tblLoginMasterDao = tblLoginMasterDao;
    }

    public TblPartnerAdminDao getTblPartnerAdminDao() {
        return tblPartnerAdminDao;
    }

    public void setTblPartnerAdminDao(TblPartnerAdminDao tblPartnerAdminDao) {
        this.tblPartnerAdminDao = tblPartnerAdminDao;
    }

    @Override
    public String createScBankDevPartnerUser(TblLoginMaster tblLoginMaster, TblPartnerAdmin tblPartnerAdmin) {
        logger.debug("createScBankDevPartnerUser : " + logUserId + " Starts");
        boolean loginFlag = false, transactionFlag = false;
        String iRetValue = "0";
        String partnerid;
        int uid = 0,pid = 0;
        String userId;
        String action = "";
        if(7 == tblLoginMaster.getTblUserTypeMaster().getUserTypeId()){
                action = "Create Scheduled Bank User";
        }else{
                action = "Create Development Partner User";
        }
        try {
            tblLoginMasterDao.addTblLoginMaster(tblLoginMaster);
            loginFlag = true;
            tblPartnerAdmin.setTblLoginMaster(new TblLoginMaster(tblLoginMaster.getUserId()));
            tblPartnerAdminDao.addTblPartnerAdmin(tblPartnerAdmin);
            pid = tblPartnerAdmin.getPartnerId();
            uid = tblLoginMaster.getUserId();
            iRetValue = pid+","+uid;

        } catch (Exception ex) {
            logger.error("createScBankDevPartnerUser : " + logUserId + ex);
            if (loginFlag && !transactionFlag) {
                tblLoginMasterDao.deleteTblLoginMaster(new TblLoginMaster(tblLoginMaster.getUserId()));
            }
            iRetValue = "-1";
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, uid, "userId", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("createScBankDevPartnerUser : " + logUserId + " Ends");
        return iRetValue;
    }

    @Override
    public VwGetSbDevPartner getUserInfo(int userId) {
        logger.debug("getUserInfo : " + logUserId + " Starts");
        VwGetSbDevPartner vwGetSbDevPartner = null;
        try {
            Object[] values = {"id.userId", Operation_enum.EQ, userId};
            List<VwGetSbDevPartner> vwGetSbDevPartners = vwSBDevPartnerDao.findVwSBDevPartner(values);
            vwGetSbDevPartner = vwGetSbDevPartners.get(0);
        } catch (Exception ex) {
            logger.error("getUserInfo : " + logUserId + ex);
            vwGetSbDevPartner = null;
        }
        logger.debug("getUserInfo : " + logUserId + " Ends");
        return vwGetSbDevPartner;
        }

    @Override
    public boolean updateScBankDevPartnerUser(TblPartnerAdmin tblPartnerAdmin) {
        logger.debug("updateScBankDevPartnerUser : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            tblPartnerAdminDao.updateTblPartnerAdmin(tblPartnerAdmin);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("updateScBankDevPartnerUser : " + logUserId + ex);
            bSuccess = false;
        }
        logger.debug("updateScBankDevPartnerUser : " + logUserId + " Ends");
        return bSuccess;
        }

    @Override
    public boolean updateScBankDevPartner(TblScBankDevPartnerMaster tblScBankDevPartnerMaster) {
        logger.debug("updateScBankDevPartner : " + logUserId + " Starts");
        boolean bSuccess = false;
        String action = "";
        if("Development".equalsIgnoreCase(tblScBankDevPartnerMaster.getPartnerType())){
            if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                 action  = "Edit Regional/Country Office";
            }else{
                action  = "Edit Development Partner";
            }
            }else{
                if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                    action  = "Edit Scheduled Bank Branch";
                }else{
                    action  = "Edit Scheduled Bank";
                }
        }
         try {
            tblScBankDevPartnerMasterDao.updateTblScBankDevPartnerMaster(tblScBankDevPartnerMaster);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("updateScBankDevPartner : " + logUserId + ex);
            action = "Error In "+action+ex.getMessage();
            bSuccess = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getPartnerType()+"Id", EgpModule.Manage_Users.getName(), action, "");
        }
        logger.debug("updateScBankDevPartner : " + logUserId + " Ends");
        return bSuccess;
        }

    @Override
    public TblScBankDevPartnerMaster getSbDevInfo(int sBankDevId,String...value) {
        logger.debug("getSbDevInfo : " + logUserId + " Starts");
        TblScBankDevPartnerMaster tblScBankDevPartnerMaster = null;
        String action = "";
         try {
            Object[] values = {"sbankDevelopId", Operation_enum.EQ, sBankDevId};
            List<TblScBankDevPartnerMaster> scBankDevPartnerMasters = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster(values);
            tblScBankDevPartnerMaster = scBankDevPartnerMasters.get(0);
            if("Development".equalsIgnoreCase(tblScBankDevPartnerMaster.getPartnerType())){
            if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                 action  = "View Regional/Country Office";
            }else{
                action  = "View Development Partner";
            }
            }else{
                if("yes".equalsIgnoreCase(tblScBankDevPartnerMaster.getIsBranchOffice())){
                    action  = "View Scheduled Bank Branch";
                }else{
                    action  = "View Scheduled Bank";
                }
            }
        } catch (Exception ex) {
            logger.error("getSbDevInfo : " + logUserId + ex);
            action = "Error In "+action+ex.getMessage();
        }finally{
             if(value==null || value.length==0){
                makeAuditTrailService.generateAudit(auditTrail, tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getPartnerType()+"Id", EgpModule.Manage_Users.getName(), action, "");
            }
        }
        logger.debug("getSbDevInfo : " + logUserId + " Ends");
        return tblScBankDevPartnerMaster;
        }

    @Override
    public String verifyOfficeName(String name, String isBranch, String partnerType, int headOfficeId,String mainOfcId) {
        logger.debug("verifyOfficeName : " + logUserId + " Starts");
        String strStatus = "";
        List<Object> listHeadOfc = new ArrayList<Object>();
        int hdevelHeadId = 0;
        

        try {

            String query = " select sbankDevelHeadId from TblScBankDevPartnerMaster where sbankDevelopId = "+mainOfcId;
            listHeadOfc = getHibernateQueryDao().singleColQuery(query);
            if(listHeadOfc!=null && listHeadOfc.size() > 0 ){
                hdevelHeadId = Integer.parseInt(listHeadOfc.get(0).toString());
            }
            else
                hdevelHeadId = headOfficeId;

           

            Object[] values = {"sbDevelopName", Operation_enum.LIKE, name.trim(), "isBranchOffice", Operation_enum.LIKE, isBranch, "partnerType", Operation_enum.EQ, partnerType};

            if (hdevelHeadId!= 0) {
            Object[] branchValues = {"sbDevelopName", Operation_enum.LIKE, name.trim(), "isBranchOffice", Operation_enum.LIKE, isBranch, "partnerType", Operation_enum.EQ, partnerType, "sbankDevelHeadId", Operation_enum.EQ, hdevelHeadId};
            values = branchValues;
            }
            List<TblScBankDevPartnerMaster> scDevPartMasters = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster(values);
            if (!scDevPartMasters.isEmpty()) {
                strStatus = "Already Exists";
            } else {
                strStatus = "OK";
            }
        } catch (Exception ex) {
            logger.error("verifyOfficeName : " + logUserId + ex);
            strStatus = "";
            }
        logger.debug("verifyOfficeName : " + logUserId + " Ends");
        return strStatus;
        }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public Object[] retriveAdminInfo(int userId, byte userTypeId,String...values) {
        logger.debug("retriveAdminInfo : " + logUserId + " Starts");
        Object[] objAdminInfo = null;
        String action = "";
        if(values!=null && values.length!=0){
            action = values[0];
        }
        if(userTypeId==6){
         if(action != null)   
                action = "View Profile of Development Partner "+action;
         else
             action=null;
         
        }
        if(userTypeId==7){
        if(action != null)   
                    action = "View Profile of Scheduled Bank "+action;
        else
            action =null;
        }
        if(userTypeId==15){
            action = "View Profile of Scheduled Bank Branch Admin";
        }
        String query = "select tpa.tblLoginMaster.userId,tsbdpm.sbankDevelopId,tsbdpm.sbDevelopName,tlm.emailId, tpa.fullName,tpa.mobileNo,tpa.nationalId,tpa.partnerId,tpa.isAdmin,tpa.designation,tpa.isMakerChecker from TblScBankDevPartnerMaster tsbdpm, TblUserTypeMaster tutm,TblLoginMaster tlm, TblPartnerAdmin tpa where tsbdpm.sbankDevelopId = tpa.sbankDevelopId and tutm.userTypeId = tlm.tblUserTypeMaster.userTypeId and tlm.userId = tpa.tblLoginMaster.userId and tlm.tblUserTypeMaster.userTypeId = " + userTypeId + " and tpa.tblLoginMaster.userId = " + userId;
        try {
            List<Object[]> adminInfo = hibernateQueryDao.createNewQuery(query);
            if (adminInfo != null && !adminInfo.isEmpty()) {
                objAdminInfo = adminInfo.get(0);
        }
        } catch (Exception ex) {
            logger.error("retriveAdminInfo : " + logUserId + ex);
            objAdminInfo = null;
        }finally{
            if(logUserId.equalsIgnoreCase(String.valueOf(userId))){
                makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.My_Account.getName(), action, "");
            }else{
                makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.Manage_Users.getName(), action, "");
            }
            
        }
        logger.debug("retriveAdminInfo : " + logUserId + " Ends");
        action = null;
        return objAdminInfo;
    }

    @Override
    public void updateDevelopmentScBankAdmin(TblPartnerAdmin tblPartnerAdmin,String...values) {
        logger.debug("updateDevelopmentScBankAdmin : " + logUserId + " Starts");
        String action = "";
        String type = "";
        if(values!=null && values.length!=0){
            if("6".equalsIgnoreCase(values[0])){
                if("yes".equalsIgnoreCase(tblPartnerAdmin.getIsAdmin())){
                    type = "Development Partner Admin";
                }else{
                    type = "Development Partner User";
                }
                
            }
            if("7".equalsIgnoreCase(values[0])){
                if("yes".equalsIgnoreCase(tblPartnerAdmin.getIsAdmin())){
                    type = "Scheduled Bank Admin";
                }else{
                    type = "Scheduled Bank User";
                }
            }
            if("15".equalsIgnoreCase(values[0])){
                type = "Scheduled Bank Branch Admin";
            }
            action = "Edit Profile of "+type;
        }
        try {
            tblPartnerAdminDao.updateTblPartnerAdmin(tblPartnerAdmin);
        } catch (Exception e) {
            logger.error("updateDevelopmentScBankAdmin : " + logUserId + e);
            action = "Error in "+action+":"+e.getMessage();
        }finally{
            if(String.valueOf(tblPartnerAdmin.getTblLoginMaster().getUserId()).equalsIgnoreCase(logUserId)){
                makeAuditTrailService.generateAudit(auditTrail, tblPartnerAdmin.getTblLoginMaster().getUserId(), "userId", EgpModule.My_Account.getName(), action, "");
            }else{
                makeAuditTrailService.generateAudit(auditTrail, tblPartnerAdmin.getTblLoginMaster().getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");              
            }
          
        }
        action = null;
        type=null;
        logger.debug("updateDevelopmentScBankAdmin : " + logUserId + " Ends");
    }

    @Override
    public List<VwGetSbDevPartner> findAdminList(int firstResult, int maxResult, Object... values) {
        logger.debug("findAdminList : " + logUserId + " Starts");
        List<VwGetSbDevPartner> list = null;
        try {
            list = vwSBDevPartnerDao.findByCountVwSBDevPartner(firstResult, maxResult, values);
        } catch (Exception ex) {
            logger.error("findAdminList : " + logUserId + ex);
            list = null;
        }
        logger.debug("findAdminList : " + logUserId + " Ends");
        return list;
        }

    @Override
    public long getCntOfficeAdmin(String from, String where) throws Exception {
         return hibernateQueryDao.countForQuery(from, where);
    }

    @Override
    public long getCntOffice(String from, String where) throws Exception {
        return hibernateQueryDao.countForQuery(from, where);
    }

    @Override
    public List<TblScBankDevPartnerMaster> findOfficeList(int firstResult, int maxResult, Object... values) {
        logger.debug("findOfficeList : " + logUserId + " Starts");
        List<TblScBankDevPartnerMaster> list = null;
         try {
            list = tblScBankDevPartnerMasterDao.findByCountEntity(firstResult, maxResult, values);
        } catch (Exception ex) {
            logger.error("findOfficeList : " + logUserId + ex);
            list = null;
        }
        logger.debug("findOfficeList : " + logUserId + " Ends");
        return list;
        }

    @Override
    public String verifyNationalId(String nationalId) {
        logger.debug("verifyNationalId : " + logUserId + " Starts");
        String strStatus = "";
        try {
            Object[] values = {"nationalId", Operation_enum.LIKE, nationalId};
            List<TblPartnerAdmin> adminMasters = tblPartnerAdminDao.findTblPartnerAdmin(values);
            if (adminMasters != null) {
                strStatus = "Ok";
            }
        } catch (Exception ex) {
            logger.error("verifyNationalId : " + logUserId + ex);
            strStatus = "";
            }
        logger.debug("verifyNationalId : " + logUserId + " Ends");
        return strStatus;
        }

    @Override
    public String verifyMobileNo(String mobileNo) {
        logger.debug("verifyMobileNo : " + logUserId + " Starts");
        String strStatus = "";
        try {
            Object[] values = {"mobileNo", Operation_enum.LIKE, mobileNo};
            List<TblPartnerAdmin> adminMasters = tblPartnerAdminDao.findTblPartnerAdmin(values);
            if (!adminMasters.isEmpty()) {
                strStatus = "Mobile No Already Exists";
            } else {
                strStatus = "Ok";
            }
        } catch (Exception ex) {
            logger.error("verifyMobileNo : " + logUserId + ex);
            strStatus = "";
            }
        logger.debug("verifyMobileNo : " + logUserId + " Ends");
        return strStatus;
        }

    @Override
    public List<Object[]> findBranchOfficeList(String partnerType, int userTypeId, int userId, String ascClause) {
        logger.debug("findBranchOfficeList : " + logUserId + " Starts");
        String TypeOfAscClause = "";
        if (ascClause.contains("headOffice")) {
          ascClause = ascClause.replace("headOffice", "scdphead.sbDevelopName");
          TypeOfAscClause = ascClause;
        } else if (ascClause.contains("name")) {
          ascClause = ascClause.replace("name", "scdpBr.sbDevelopName");
          TypeOfAscClause = ascClause;
        } else if (ascClause.contains("country")) {
          ascClause = ascClause.replace("country", "cm.countryName ");
          TypeOfAscClause = ascClause;
        } else if (ascClause.contains("state")) {
          ascClause = ascClause.replace("state", "sm.stateName");
          TypeOfAscClause = ascClause;
        } else if (ascClause.equalsIgnoreCase("")) {//rishita - default sorting created date
            TypeOfAscClause = "scdpBr.createdDate DESC";
        } else {
            TypeOfAscClause = "scdphead.sbDevelopName , scdpBr.sbDevelopName ASC";
        }
        StringBuilder query = new StringBuilder("select scdpBr.sbankDevelopId,scdphead.sbDevelopName,scdpBr.sbDevelopName,sm.stateName,cm.countryName"
                + " from TblScBankDevPartnerMaster scdpBr,TblStateMaster sm,TblCountryMaster cm,TblScBankDevPartnerMaster scdphead where "
                + "sm.stateId=scdpBr.tblStateMaster.stateId and sm.tblCountryMaster.countryId=cm.countryId and "
                + "scdpBr.sbankDevelHeadId=scdphead.sbankDevelopId and scdpBr.isBranchOffice='Yes' AND "
                + "scdpBr.partnerType='" + partnerType + "'");
        if (userTypeId != 1) {
            query.append(" AND scdpBr.sbankDevelHeadId=" + userId);
        }
        query.append(" ORDER BY " + TypeOfAscClause);
        logger.debug("findBranchOfficeList : Query: " + query.toString());
        logger.debug("findBranchOfficeList : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    @Override
    public List<Object[]> findBranchOfficeList(String partnerType, int userTypeId, int userId, String searchField, String searchString, String searchOper) {
        logger.debug("findBranchOfficeList : " + logUserId + " Starts");
        if (searchField.equalsIgnoreCase("headOffice")) {
            searchField = "scdphead.sbDevelopName";
        } else if (searchField.equalsIgnoreCase("name")) {
            searchField = "scdpBr.sbDevelopName";
        } else if (searchField.equalsIgnoreCase("country")) {
            searchField = "cm.countryName";
        } else if (searchField.equalsIgnoreCase("state")) {
            searchField = "sm.stateName";
        }
        StringBuilder query = new StringBuilder("select scdpBr.sbankDevelopId,scdphead.sbDevelopName,"
                + "scdpBr.sbDevelopName,sm.stateName,cm.countryName from TblScBankDevPartnerMaster scdpBr,"
                + "TblStateMaster sm,TblCountryMaster cm,TblScBankDevPartnerMaster scdphead where "
                + "sm.stateId=scdpBr.tblStateMaster.stateId and sm.tblCountryMaster.countryId=cm.countryId and "
                + "scdpBr.sbankDevelHeadId=scdphead.sbankDevelopId and scdpBr.isBranchOffice='Yes' AND "
                + "scdpBr.partnerType='" + partnerType + "'");
        if (userTypeId != 1) {
            query.append(" AND scdpBr.sbankDevelHeadId=" + userId);
        }

        if (searchOper.equalsIgnoreCase("EQ")) {
            query.append(" AND " + searchField + " = '" + searchString + "' ");
        } else if (searchOper.equalsIgnoreCase("CN")) {
            query.append(" AND " + searchField + " LIKE '%" + searchString + "%' ");
        }
        query.append(" ORDER BY scdpBr.sbankDevelopId DESC");
        logger.debug("findBranchOfficeList  : Query: " + query.toString());
        logger.debug("findBranchOfficeList : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    @Override
    public boolean isAdmin(int userId) {
        logger.debug("isAdmin : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            Object[] values = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId), "isAdmin", Operation_enum.EQ, "Yes"};
            List<TblPartnerAdmin> adminMasters = tblPartnerAdminDao.findTblPartnerAdmin(values);
            if (!adminMasters.isEmpty()) {
                bSuccess = true;
            } else {
                bSuccess = false;
            }
        } catch (Exception ex) {
            logger.error("isAdmin : " + logUserId + ex);
            bSuccess = false;
            }
        logger.debug("isAdmin : " + logUserId + " Ends");
        return bSuccess;
        }

    @Override
    public List<TblScBankDevPartnerMaster> findBranchList(Object... values) throws Exception {
         return tblScBankDevPartnerMasterDao.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Object[]> findDevPartnerOrg(int firstResult, int maxResult, String orderClause) {
        return hibernateQueryDao.createByCountNewQuery("select tsbdpm.sbDevelopName,tcm.countryName,tsm.stateName,tsbdpm.sbankDevelopId from TblScBankDevPartnerMaster tsbdpm,TblStateMaster tsm,TblCountryMaster tcm where tsm.stateId = tsbdpm.tblStateMaster.stateId and tcm.countryId = tsbdpm.tblCountryMaster.countryId and tsbdpm.partnerType like 'Development' and tsbdpm.isBranchOffice like 'No' order by " + orderClause, firstResult, maxResult);
    }

    @Override
    public List<Object[]> findDevPartnerOrg(int firstResult, int maxResult, String orderClause, String partnerType) {
        return hibernateQueryDao.createByCountNewQuery("select tsbdpm.sbDevelopName,tcm.countryName,tsm.stateName,tsbdpm.sbankDevelopId from TblScBankDevPartnerMaster tsbdpm,TblStateMaster tsm,TblCountryMaster tcm where tsm.stateId = tsbdpm.tblStateMaster.stateId and tcm.countryId = tsbdpm.tblCountryMaster.countryId and tsbdpm.partnerType like '" + partnerType + "' and tsbdpm.isBranchOffice like 'No' order by " + orderClause, firstResult, maxResult);
    }

    @Override
    public String bankChecker(int sBankDevelHeadId, String role) {
        logger.debug("bankChecker : " + logUserId + " Starts");
        String strStatus = "";
        try {
            long counter = hibernateQueryDao.countForNewQuery("TblPartnerAdmin pa, TblScBankDevPartnerMaster as scbdpm", "pa.sbankDevelopId=scbdpm.sbankDevelopId and scbdpm.sbankDevelopId=" + sBankDevelHeadId + " and pa.isMakerChecker='" + role + "'");
            if (counter > 0) {
                strStatus = "Role is already assigned";
            } else {
                strStatus = "OK";
            }
        } catch (Exception ex) {
            logger.error("bankChecker : " + logUserId + ex);
            strStatus = "Error";
        }
        logger.debug("bankChecker : " + logUserId + " Ends");
        return strStatus;
    }

    @Override
    public List<Object> getPhoneCode(int branch) {
        logger.debug("getPhoneCode : " + logUserId + " Starts");
        List<Object> list = null;
        //throw new UnsupportedOperationException("Not supported yet.");
        list = hibernateQueryDao.getSingleColQuery("select tcm.countryCode from TblCountryMaster tcm ,TblScBankDevPartnerMaster tsc where tcm.countryId = tsc.tblCountryMaster.countryId and tsc.sbankDevelopId=" + branch);
        if (list.size() <= 0) {
            list = null;
        }
        logger.debug("getPhoneCode : " + logUserId + " Ends");
           return list;
        }

    @Override
    public List<Object[]> getUserListGrid(int firstResult, int maxResult, int bankId, String type, String orderType, String e, String searchField, String searchType) {
        logger.debug("getUserListGrid : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
        //return hibernateQueryDao.createByCountNewQuery("select PA.fullName, LM.emailId, LM.userId, SBP.sbDevelopName  from TblPartnerAdmin PA inner join TblScBankDevPartnerMaster as SBP, SBP.sbankDevelopId=PA.sbankDevelopId inner join TblLoginMaster as LM on PA.tblLoginMaster.userId=LM.userId where isAdmin='no' And (SBP.sBankDevelopId in (select distinct sBankDevelopId from TblScBankDevPartnerMaster Where partnerType='"+type+"' and sBankDevelHeadId="+bankId+") or SBP.sBankDevelopId="+bankId+")",firstResult, maxResult);
            String query = "select PA.fullName, LM.emailId, LM.userId, SBP.sbDevelopName,PA.isMakerChecker,LM.status,PA.partnerId "
                    + "from TblPartnerAdmin PA ,TblScBankDevPartnerMaster SBP,TblLoginMaster LM "
                    + "where PA.tblLoginMaster.userId=LM.userId and SBP.sbankDevelopId=PA.sbankDevelopId and "
                    + "isAdmin='no' And (SBP.sbankDevelopId in (select distinct sbankDevelopId from "
                    + "TblScBankDevPartnerMaster Where partnerType='" + type + "' and sbankDevelHeadId=" + bankId + ") "
                    + "or SBP.sbankDevelopId=" + bankId + ")" + searchField + "" + searchType + " "
                    + "order by " + orderType + " " + e + "";
            logger.debug("getUserListGrid  :" + logUserId + " Query : " + query);
            list = hibernateQueryDao.createByCountNewQuery(query, firstResult, maxResult);
        } catch (Exception ex) {
            logger.error("getUserListGrid : " + logUserId + ex);
            list = null;
        }
        logger.debug("getUserListGrid : " + logUserId + " Ends");
        return list;
    }

    @Override
    public Object[] bankNameForAdmin(int branchId) {
        logger.debug("bankNameForAdmin : " + logUserId + " Starts");
        Object[] obj = null;
        String query = "select sbankDevelopId,sbDevelopName from TblScBankDevPartnerMaster where sbankDevelopId in (select sbankDevelHeadId from  TblScBankDevPartnerMaster where sbankDevelopId=" + branchId + ")";
        //throw new UnsupportedOperationException("Not supported yet.");
        try {
            List<Object[]> adminInfo = hibernateQueryDao.createNewQuery(query);
            if (adminInfo != null && !adminInfo.isEmpty()) {
                obj = adminInfo.get(0);
            }
        } catch (Exception e) {
            logger.error("bankNameForAdmin : " + logUserId + e);
            obj = null;
        }
        logger.debug("bankNameForAdmin : " + logUserId + " Ends");
        return obj;

    }

    @Override
    public List<TblScBankDevPartnerMaster> getBankName() {
        logger.debug("getBankName : " + logUserId + " Starts");
        List<TblScBankDevPartnerMaster> list = null;
        try {
            list = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster("isBranchOffice", Operation_enum.EQ, "No", "partnerType", Operation_enum.EQ, "ScheduleBank", "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getBankName : " + logUserId + ex);
            list = new ArrayList<TblScBankDevPartnerMaster>();
        }
        logger.debug("getBankName : " + logUserId + " Ends");
        return list;
        }

    @Override
    public List<TblScBankDevPartnerMaster> getBankDevPartnerMasterinfo(int offcet, int row, int sbankdevelopid) {
        logger.debug("getBankDevPartnerMasterinfo : " + logUserId + " Starts");
        List<TblScBankDevPartnerMaster> list = null;
         try {
            /*if(orderby==null && colName==null){
                return tblScBankDevPartnerMasterDao.findByCountTblScBankDevPartnerMaster(offcet, row,"sbankDevelHeadId",Operation_enum.EQ, sbankdevelopid);
            }else{
                Object data = null;
                if(orderby.equals("asc")){
                    data= Operation_enum.ASC;
                } else{
                    data= Operation_enum.DESC;
                }
                return tblScBankDevPartnerMasterDao.findByCountTblScBankDevPartnerMaster(offcet, row,"sbankDevelHeadId",Operation_enum.EQ, sbankdevelopid,colName,Operation_enum.ORDERBY, data);
            }*/
            list = tblScBankDevPartnerMasterDao.findByCountTblScBankDevPartnerMaster(offcet, row, "sbankDevelHeadId", Operation_enum.EQ, sbankdevelopid, "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception ex) {
            logger.error("getBankDevPartnerMasterinfo : " + logUserId + ex);
            list = null;
        }
        logger.debug("getBankDevPartnerMasterinfo : " + logUserId + " Ends");
        return list;
    }

    @Override
    public long getBankBranchCount(int sbankdevelopid) {
        logger.debug("getBankBranchCount : " + logUserId + " Starts");
        long lRetValue = 0;
        try {
            lRetValue = tblScBankDevPartnerMasterDao.countForQuery("TblScBankDevPartnerMaster", "sbankDevelHeadId = '" + sbankdevelopid + "'");
        } catch (Exception ex) {
            logger.error("getBankBranchCount : " + logUserId + ex);
            lRetValue = 0;
        }
        logger.debug("getBankBranchCount : " + logUserId + " Ends");
        return lRetValue;
    }

    @Override
    public List<Object[]> getBankDevPMasterInfo(int offcet, int row, int sbankdevelopid,String branchName,String thana,String city,String district) {
        logger.debug("getBankDevPMasterInfo : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createByCountNewQuery("select tcm.countryName,tsbdpm.sbDevelopName,tsbdpm.phoneNo,tsbdpm.faxNo,tsbdpm.officeAddress,tsbdpm.thanaUpzilla,tsbdpm.cityTown,tsm.stateName,tsbdpm.postCode from TblScBankDevPartnerMaster tsbdpm,TblCountryMaster tcm,TblStateMaster tsm where tcm.countryId = tsbdpm.tblCountryMaster.countryId and tsm.stateId =tsbdpm.tblStateMaster.stateId and tsbdpm.sbankDevelHeadId =" + sbankdevelopid + " and sbDevelopName like '%"+branchName+"%' and thanaUpzilla like '%"+thana+"%' and cityTown like '%"+city+"%' and stateName like '%"+district+"%' order by tsbdpm.sbDevelopName", offcet, row);
        } catch (Exception e) {
            logger.debug("getBankDevPMasterInfo : "+e);
    }
        logger.debug("getBankDevPMasterInfo : " + logUserId + " Ends");
        return list;
    }

    @Override
    public int getAdvanceBankBranchCount(int sbankdevelopid,String branchName,String thana,String city,String district)
    {
    logger.debug("getBankDevPMasterInfo : " + logUserId + " Starts");
    List<Object[]> list = null;
        try {
            list = hibernateQueryDao.createQuery("select tcm.countryName,tsbdpm.sbDevelopName,tsbdpm.phoneNo,tsbdpm.faxNo,tsbdpm.officeAddress,tsbdpm.thanaUpzilla,tsbdpm.cityTown,tsm.stateName,tsbdpm.postCode from TblScBankDevPartnerMaster tsbdpm,TblCountryMaster tcm,TblStateMaster tsm where tcm.countryId = tsbdpm.tblCountryMaster.countryId and tsm.stateId =tsbdpm.tblStateMaster.stateId and tsbdpm.sbankDevelHeadId =" + sbankdevelopid + " and sbDevelopName like '%"+branchName+"%' and thanaUpzilla like '%"+thana+"%' and cityTown like '%"+city+"%' and stateName like '%"+district+"%' order by tsbdpm.sbDevelopName");
        } catch (Exception e) {
            logger.debug("getBankDevPMasterInfo : "+e);
        }
        logger.debug("getBankDevPMasterInfo : " + logUserId + " Ends");
        return list.size();
    }


     @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    @Override
    public List<Object[]> getBranchAdminListGrid(int firstResult, int maxResult, int bankId, String type, String orderType, String e, String searchField, String searchType) {
    logger.debug("getBranchAdminListGrid : " + logUserId + " Starts");
    List<Object[]> list = null;
    try{
    String query = "select PA.fullName, LM.emailId, LM.userId, SBP.sbDevelopName,PA.isMakerChecker,LM.status  "
                    + "from TblPartnerAdmin PA ,TblScBankDevPartnerMaster SBP,TblLoginMaster LM "
                    + "where PA.tblLoginMaster.userId=LM.userId and SBP.sbankDevelopId=PA.sbankDevelopId and "
                    + "isAdmin='yes'And PA.isMakerChecker='BranchAdmin' And (SBP.sbankDevelopId in (select distinct sbankDevelopId from "
                    + "TblScBankDevPartnerMaster Where partnerType='"+type+"' and sbankDevelHeadId="+bankId+") "
                    + "or SBP.sbankDevelopId="+bankId+")"+searchField+""+searchType+" "
                    + "order by "+orderType+" "+e+"";
           logger.debug("getBranchAdminListGrid :query: " +query);
           list = hibernateQueryDao.createByCountNewQuery(query,firstResult, maxResult);
        }catch(Exception ex)
        {
            logger.error("getBranchAdminListGrid : " +ex);
    }
        logger.debug("getBranchAdminListGrid : " + logUserId + " Ends");
        return list;
    }

    @Override
    public List<Object[]> getBankBranchByUserId(int userId) {
        logger.debug("getBankBranchByUserId : " + logUserId + " Starts");
        List<Object[]> list = new ArrayList<Object[]>();
        try {
            /*String query = "Select  sbdp.sbankDevelopId, sbdp.sbDevelopName, "
                    + "sbdp2.sbankDevelopId, sbdp2.sbDevelopName "
                    + "from TblPartnerAdmin pa Inner Join TblScBankDevPartnerMaster sbdp "
                    + "with pa.sbankDevelopId=sbdp.sbankDevelopId Inner Join TblScBankDevPartnerMaster sbdp2 "
                    + "with sbdp.sbankDevelHeadId=sbdp2.sbankDevelopId Where pa.tblLoginMaster.userId="+userId;
            */
             String query = "Select  sbdp.sbankDevelopId, sbdp.sbDevelopName, "
                    + "sbdp2.sbankDevelopId, sbdp2.sbDevelopName "
                    + "from TblPartnerAdmin pa,TblScBankDevPartnerMaster sbdp,"
                    + "TblScBankDevPartnerMaster sbdp2"
                    + " where pa.sbankDevelopId=sbdp.sbankDevelopId and sbdp.sbankDevelHeadId=sbdp2.sbankDevelopId and pa.tblLoginMaster.userId="+userId;
            list = hibernateQueryDao.createNewQuery(query);
        } catch (Exception e) {
            logger.error("getBankBranchByUserId : " + logUserId + " :"+e);
        }
        logger.debug("getBankBranchByUserId : " + logUserId + " Ends");
        return list;
    }
    


    @Override
    public List<TblScBankDevPartnerMaster> getDevelopPartnerList(){
        logger.debug("getBranchAdminListGrid : " + logUserId + " Starts");
        List<TblScBankDevPartnerMaster> list = null;
        try{
            list = tblScBankDevPartnerMasterDao.findTblScBankDevPartnerMaster("partnerType",Operation_enum.EQ,"Development","isBranchOffice",Operation_enum.EQ,"No");
        }catch(Exception ex){
            logger.error("getBranchAdminListGrid : " +ex);
        }
        logger.debug("getBranchAdminListGrid : " + logUserId + " Ends");
        return list;
    }

    @Override
    public boolean insertIntopartneradmintransfer(TblPartnerAdminTransfer tblPartnerAdminTransfer) {
        logger.debug("insertIntopartneradmintransfer : " + logUserId + " Starts");
       boolean flag = false;
        try{
            tblPartnerAdminTransferDao.addTblPartnerAdminTransfer(tblPartnerAdminTransfer);
            flag= true;
        }catch(Exception ex){
            logger.error("getBranchAdminListGrid : " +ex);
        }
        logger.debug("getBranchAdminListGrid : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

   
}
