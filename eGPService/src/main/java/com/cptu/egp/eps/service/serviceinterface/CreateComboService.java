/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblListBoxDetail;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface CreateComboService {

    /**
     * add combo data into database
     * @param tbllistboxmaster object which is saved
     * @param tbllistboxdetail object which is saved
     * @return flag weather data is saved in to database or not
     */
    public boolean addComboNameDetails(TblListBoxMaster tbllistboxmaster, List<TblListBoxDetail> tbllistboxdetail);

   /**
    * gives combo data list for displaying in jq grid
    * @param offcet for paging facility
    * @param row for sorting facility
    * @param templetformid for particular formid
    * @param orderby which is define in which oreder u want to sort asc or desc
    * @param colName defines column name on which sorting is applied
    * @return combo data list
    */
   public List<TblListBoxMaster> getComboName(int offcet, int row,int templetformid, String orderby, String colName);

   /**
    * gives combo name
    * @param id for particular id
    * @return combo name list
    */
   public List<TblListBoxMaster> getComboNameById(int id);

   /**
    * gives combo row count for particular id
    * @param templetformid
    * @return combo row count
    */
   public long getComboCount(int templetformid);

   /**
    * gives combo data list by passing form id
    * @param formId
    * @return combo name list
    */
   public List<TblListBoxMaster> getListBoxMaster(int formId);

   /**
    * gives combo data list by passing
    * @param id
    * @return combo data value list
    */
   public List<TblListBoxDetail> getListBoxDetail(int id);

   /**
    * gives combo name data list
    * @param offcet used for paging facility
    * @param row for used sorting facility
    * @param templetformid
    * @param searchField for searching
    * @param searchString is the string which user want to find from table
    * @param searchOper
    * @param orderby which is define in which oreder u want to sort asc or desc
    * @param colName defines column name on which sorting is applied
    * @return combo data value list
    */
   public List<TblListBoxMaster> searchComboName(int offcet,int row,int templetformid, String searchField,String searchString,String searchOper,String orderby,String colName);

   /**
    * gives row count from database for particular form id
    * @param templetformid is the id which user want to fetch combo data for that
    * @param searchField for searching
    * @param searchString is the string which user want to find from table
    * @param searchOper
    * @return
    */
   public long getSearchCount(int templetformid,String searchField, String searchString, String searchOper);

   /**
    * gives combo data list categary wise
    * @param intFormId
    * @param strCat is the string which defines categary in the case of yes or no
    * @return combo data list 
    */
   public List<TblListBoxMaster> getListBoxCatWise(int intFormId,String strCat);

   /**
    * this method is for deleting combo from database
    * @param listBoxId
    * @return boolean, true - if deleted successfully otherwise not
    */
   public boolean deleteCombo(int listBoxId);

   /**
    * this method is for getting the STD status
    * @param templateId
    * @return list, list of data
    */
   public List<Object> getSTDStatus(int templateId);

   /**
    * this method is for getting count of already used combo somewhere in the std
    * @param listBoxId
    * @param tenderFormId
    * @return long, return 0 - not used otherwise used
    */
   public long getUsedComboCountinStd(int listBoxId,int tenderFormId);

   /**
    * Set userId at Service layer
    * @param logUserId 
    */
   public void setLogUserId(String logUserId);
   
}
