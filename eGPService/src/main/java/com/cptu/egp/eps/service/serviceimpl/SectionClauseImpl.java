/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblIttClauseDao;
import com.cptu.egp.eps.dao.daointerface.TblIttSubClauseDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblIttClause;
import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblIttSubClause;
import com.cptu.egp.eps.service.serviceinterface.SectionClause;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class SectionClauseImpl implements SectionClause {

    static final Logger logger = Logger.getLogger(SectionClauseImpl.class);
    private String logUserId = "0";

    public TblIttClauseDao getTblIttClauseDao() {
        return tblIttClauseDao;
    }

    public void setTblIttClauseDao(TblIttClauseDao tblIttClauseDao) {
        this.tblIttClauseDao = tblIttClauseDao;
    }

    public TblIttSubClauseDao getTblIttSubClauseDao() {
        return tblIttSubClauseDao;
    }

    public void setTblIttSubClauseDao(TblIttSubClauseDao tblIttSubClauseDao) {
        this.tblIttSubClauseDao = tblIttSubClauseDao;
    }
    TblIttClauseDao tblIttClauseDao;
    TblIttSubClauseDao tblIttSubClauseDao;

    public boolean createSectionClause(TblIttClause tblIttClause) {
        logger.debug("createSectionClause : " + logUserId + " Starts");
        boolean loginFlag = false;
        try {
            tblIttClauseDao.addEntity(tblIttClause);
            loginFlag = true;
        } catch (Exception ex) {
            logger.error("createSectionClause : " + logUserId + ex);
            loginFlag = false;
        }
        logger.debug("createSectionClause : " + logUserId + " Ends");
        return loginFlag;
    }

    public boolean createSubSectionClause(TblIttSubClause tblIttSubClause) {
        logger.debug("createSubSectionClause : " + logUserId + " Starts");
        boolean loginFlag = false;
        try {
            tblIttSubClauseDao.addTblIttSubClause(tblIttSubClause);
            loginFlag = true;
        } catch (Exception ex) {
            logger.error("createSubSectionClause : " + logUserId + ex);
            loginFlag = false;
        }
        logger.debug("createSubSectionClause : " + logUserId + " Ends");
        return loginFlag;
    }
    HibernateQueryDao hibernateQueryDao;

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    @Override
    public String getSectionName(int ittHeaderId) {
        logger.debug("getSectionName : " + logUserId + " Starts");
        List<Object> tblIttHeaderList;
        String tblIttHeaderName = "";

        try {
            logger.debug("getSectionName  : Query>>>" + "select tth.ittHeaderName from TblIttHeader tth where tth.ittHeaderId = '" + ittHeaderId + "'");
            tblIttHeaderList = hibernateQueryDao.getSingleColQuery("select tth.ittHeaderName from TblIttHeader tth where tth.ittHeaderId = '" + ittHeaderId + "'");
            tblIttHeaderName = tblIttHeaderList.get(0).toString();
        } catch (Exception ex) {
            tblIttHeaderName = "";
            logger.error("getSectionName : " + logUserId + ex);
        }
        return tblIttHeaderName;
    }

    @Override
    public List<Object[]> getClauseDetailsTDS(int ittHeaderId) {
        /*
         * This function fetches Clasues containing
         * any subclause with TDS Applicable
         */
        logger.debug("getClauseDetails_TDS : " + logUserId + " Starts");

        List<Object[]> tblIttClause;
        try {
            String qry = "select distinct ittH.ittClauseId,ittH.ittClauseName from TblIttClause ittH, TblIttSubClause ittSC "
                    + "where ittSC.tblIttClause.ittClauseId = ittH.ittClauseId and ittSC.isTdsApplicable = 'yes' and "
                    + "ittH.tblIttHeader.ittHeaderId = '" + ittHeaderId + "'";
            //tblIttClause = tblIttClauseDao.findEntity("tblIttHeader", Operation_enum.EQ, new TblIttHeader(ittHeaderId));
            tblIttClause = tblIttClauseDao.createQuery(qry);
        } catch (Exception ex) {
            logger.error("getClauseDetails_TDS : " + logUserId + ex);
            tblIttClause = null;
        }
        logger.debug("getClauseDetails_TDS : " + logUserId + " Ends");
        return tblIttClause;
    }

    @Override
    public List<TblIttClause> getClauseDetails(int ittHeaderId) {
        logger.debug("getClauseDetails : " + logUserId + " Starts");
        List<TblIttClause> tblIttClause;
        try {
            tblIttClause = tblIttClauseDao.findEntity("tblIttHeader", Operation_enum.EQ, new TblIttHeader(ittHeaderId));
        } catch (Exception ex) {
            tblIttClause = null;
            logger.error("getClauseDetails : " + logUserId + ex);
        }
        logger.debug("getClauseDetails : " + logUserId + " Ends");
        return tblIttClause;
    }

    @Override
    public List<TblIttSubClause> getSubClauseDetailsTDS(int ittClauseId) {
        /*
         * This function fetches sub Clasues with TDS Applicable
         */
        logger.debug("getSubClauseDetails_TDS : " + logUserId + " Starts");
        List<TblIttSubClause> tblIttSubClause;
        try {
            tblIttSubClause = tblIttSubClauseDao.findEntity("tblIttClause", Operation_enum.EQ, new TblIttClause(ittClauseId), "isTdsApplicable", Operation_enum.EQ, "yes");
        } catch (Exception ex) {
            logger.error("getSubClauseDetails_TDS : " + logUserId + ex);
            tblIttSubClause = null;
        }
        logger.debug("getSubClauseDetails_TDS : " + logUserId + " Ends");
        return tblIttSubClause;
    }

    @Override
    public List<TblIttSubClause> getSubClauseDetails(int ittClauseId) {
        logger.debug("getSubClauseDetails : " + logUserId + " Starts");
        List<TblIttSubClause> tblIttSubClause;
        try {
            tblIttSubClause = tblIttSubClauseDao.findEntity("tblIttClause", Operation_enum.EQ, new TblIttClause(ittClauseId));
        } catch (Exception ex) {
            logger.error("getSubClauseDetails : " + logUserId + ex);
            tblIttSubClause = null;
        }
        logger.debug("getSubClauseDetails : " + logUserId + " Starts");
        return tblIttSubClause;
    }

    @Override
    public long getClauseCount(int ittHeaderId) {
        logger.debug("getClauseCount : " + logUserId + " Starts");
        long count = 0;
        try {
            count = hibernateQueryDao.countForNewQuery("TblIttClause tblIttClause", "tblIttClause.tblIttHeader.ittHeaderId = '" + ittHeaderId + "'");
        } catch (Exception ex) {
            logger.error("getClauseCount : " + logUserId + ex);
            count = 0;
        }
        logger.debug("getClauseCount : " + logUserId + " Ends");
        return count;
    }

    @Override
    public boolean updateClause(TblIttClause tblIttClause) {
        logger.debug("updateClause : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblIttClauseDao.updateTblIttClause(tblIttClause);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateClause : " + logUserId + ex);
            flag = false;
        }
        logger.debug("updateClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean updateSubClause(TblIttSubClause tblIttSubClause) {
        logger.debug("updateSubClause : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblIttSubClauseDao.updateTblIttSubClause(tblIttSubClause);
            flag = true;
        } catch (Exception ex) {
            logger.error("updateSubClause : " + logUserId + ex);
            flag = false;
        }
        logger.debug("updateSubClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean deleteClause(TblIttClause tblIttClause) {
        logger.debug("deleteClause : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblIttClauseDao.deleteEntity(tblIttClause);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteClause : " + logUserId + ex);
            flag = false;
        }
        logger.debug("deleteClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public boolean deleteSubClause(TblIttSubClause tblIttSubClause) {
        logger.debug("deleteSubClause : " + logUserId + " Starts");
        boolean flag = false;
        try {
            tblIttSubClauseDao.deleteEntity(tblIttSubClause);
            flag = true;
        } catch (Exception ex) {
            logger.error("deleteSubClause : " + logUserId + ex);
            flag = false;
        }
        logger.debug("deleteSubClause : " + logUserId + " Ends");
        return flag;
    }

    @Override
    public void setUserId(String logUserId) {
          this.logUserId = logUserId;
    }
}
