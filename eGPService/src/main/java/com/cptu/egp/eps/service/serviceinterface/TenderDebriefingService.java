/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc;
import com.cptu.egp.eps.model.table.TblTenderDebriefing;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface TenderDebriefingService {

/**
 *
 * @param logUserId
 */
public void setLogUserId(String logUserId);

/**
 * this method saves the debriefing question data to database
 * @param tblTenderDebriefing
 * @return boolean, true - if saved successfully, false - if not saved
 */
public int saveTenderDebriefing(TblTenderDebriefing tblTenderDebriefing);

/**
 * this method is for getting the question data for particular tenderer by passing the userid and tenderid
 * @param tenderid - this is the tenderid
 * @param userid - thsi is the particular userid
 * @return list, list of data
 */
public List<TblTenderDebriefing> getDebriefQuestionData(int tenderid,int userid);

/**
 * this method is for getting the question data for particular officer by passing the tenderid
 * @param tenderid - this is the tenderid
 * @return list, list of data
 */
public List<TblTenderDebriefing> getDebriefQuestionDataForPE(int tenderid);

/**
 * this method is for getting the whole debriefing data
 * @param debriefid - this is the primary key
 * @return list, list of data
 */
public List<TblTenderDebriefing> getDebriefData(int debriefid);

/**
 * this method updates the debriefing data
 * @param debriefid
 * @param userid
 * @param replytext
 * @return integer, if return 1 - updated successfully, otherwise not
 */
public int updateDeriefData(int debriefid,int userid,String replytext);

/**
 * this method is for getting the company name for particular tenderer
 * @param userid
 * @return list, list of data
 */
public List<Object[]> getCompanyName(int userid);

public boolean  saveDebriefingDocsDetail(TblTenderDebriefdetailDoc tblTenderDebriefdetailDoc);

public List<TblTenderDebriefdetailDoc> getDebriefDetailData(int DetailDocId);

public boolean deleteDebriefDocsDetails(int DetailDocId);

}
