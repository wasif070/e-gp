/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPXMLCommon;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
public class SpXMLCommonImpl {

    static final Logger logger = Logger.getLogger(SpXMLCommonImpl.class);
    private String logUserId = "0";
    SPXMLCommon spXMLCommon;
    MakeAuditTrailService makeAuditTrailService;
    AuditTrail auditTrail;
    
    public void setLogUserId(String userId) {
        this.logUserId = userId;
    }

    public void setSpXMLCommon(SPXMLCommon spXMLCommon) {
        this.spXMLCommon = spXMLCommon;
    }

    public SPXMLCommon getSpXMLCommon() {
        return spXMLCommon;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    /**
     * Perform Insert operation in database using xml data source.
     * @param tableName
     * @param tableMatrixRoot
     * @param isColumn
     * @param isInEditMode
     * @param whereCond
     * @return true if operation perform successfully else return false.
     */
    public boolean insertTableMatrix(String tableName, String tableMatrixRoot, boolean isColumn, boolean isInEditMode, String whereCond) 
    {
        logger.debug("insertTableMatrix : " + logUserId + " Starts");
        boolean boolRet = false;
        List<CommonMsgChk> insertDtl;
        try {
            if (isInEditMode) {
                if (isColumn) {
                    insertDtl = spXMLCommon.executeProcedure("insdel", tableName, tableMatrixRoot, whereCond);
                } else {
                    insertDtl = spXMLCommon.executeProcedure("insdel", tableName, tableMatrixRoot, whereCond);
                }
            } else {
                if (isColumn) {
                    insertDtl = spXMLCommon.executeProcedure("insert", tableName, tableMatrixRoot, whereCond);
                } else {
                    insertDtl = spXMLCommon.executeProcedure("insert", tableName, tableMatrixRoot, whereCond);
                }
            }
            boolRet = insertDtl.get(0).getFlag();
            logger.debug("insertTableMatrix :" + logUserId + " MESSAGE WHILE EXECUTING STORED PROCEDURE OF COLUMNS AND CELLS -----> " + insertDtl.get(0).getMsg());
        } catch (Exception ex) {
            logger.error("insertTableMatrix : " + logUserId + ex);
            }
        logger.debug("insertTableMatrix : " + logUserId + " Ends");
        return boolRet;
    }
}
