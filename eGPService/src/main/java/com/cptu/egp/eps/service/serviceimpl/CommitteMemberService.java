/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDomesticPrefDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderHashDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean;
import com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean;
import com.cptu.egp.eps.dao.storedprocedure.SPInsertMultiTablesXML;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblTenderDomesticPref;
import com.cptu.egp.eps.model.table.TblTenderHash;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class CommitteMemberService {

    static final Logger logger = Logger.getLogger(CommitteMemberService.class);
    
    SPTenderCommon sPTenderCommon;
   SPInsertMultiTablesXML sPInsertMultiTablesXML;
   HibernateQueryDao hibernateQueryDao;
   TblTenderHashDao tblTenderHashDao;
   TblTenderDomesticPrefDao tblTenderDomesticPrefDao;
   MakeAuditTrailService makeAuditTrailService;
   private String logUserId="0";
   private AuditTrail auditTrail;
   private String starts=" Starts";
   private String ends=" Ends";

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    
    public TblTenderDomesticPrefDao getTblTenderDomesticPrefDao() {
        return tblTenderDomesticPrefDao;
    }

    public void setTblTenderDomesticPrefDao(TblTenderDomesticPrefDao tblTenderDomesticPrefDao) {
        this.tblTenderDomesticPrefDao = tblTenderDomesticPrefDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblTenderHashDao getTblTenderHashDao() {
        return tblTenderHashDao;
    }

    public void setTblTenderHashDao(TblTenderHashDao tblTenderHashDao) {
        this.tblTenderHashDao = tblTenderHashDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SPInsertMultiTablesXML getsPInsertMultiTablesXML() {
        return sPInsertMultiTablesXML;
    }

    public void setsPInsertMultiTablesXML(SPInsertMultiTablesXML sPInsertMultiTablesXML) {
        this.sPInsertMultiTablesXML = sPInsertMultiTablesXML;
    }
    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }
    TblDepartmentMasterDao tblDepartmentMasterDao;

    public TblDepartmentMasterDao getTblDepartmentMasterDao() {
        return tblDepartmentMasterDao;
    }

    public void setTblDepartmentMasterDao(TblDepartmentMasterDao tblDepartmentMasterDao) {
        this.tblDepartmentMasterDao = tblDepartmentMasterDao;
    }
    /**
     * Committe Creation Details
     * @param tenderid tenderid from request
     * @param funName evalcom,opencom
     * @return CommitteDtBean in DAO Bean
     */
    public CommitteDtBean findCommitteDetail(int tenderid,String funName){
        logger.debug("findCommitteDetail : "+logUserId+starts);
        CommitteDtBean dtBean = new CommitteDtBean();
        List<SPTenderCommonData> sPTenderCommonDatas = sPTenderCommon.executeProcedure(funName, String.valueOf(tenderid), null);
        for (SPTenderCommonData sPTenderCommonData : sPTenderCommonDatas) {
            dtBean.setMaxMemReq(Integer.parseInt(sPTenderCommonData.getFieldName4()));
            dtBean.setMaxTenderVal(new BigDecimal(sPTenderCommonData.getFieldName2()));
            dtBean.setMinMemFromPe(Integer.parseInt(sPTenderCommonData.getFieldName6()));
            dtBean.setMinMemFromTec(Integer.parseInt(sPTenderCommonData.getFieldName7()));
            dtBean.setMinMemOutSidePe(Integer.parseInt(sPTenderCommonData.getFieldName5()));
            dtBean.setMinMemReq(Integer.parseInt(sPTenderCommonData.getFieldName3()));
            dtBean.setMinTenderVal(new BigDecimal(sPTenderCommonData.getFieldName1()));
            dtBean.setpNature(sPTenderCommonData.getFieldName8());
        }
        logger.debug("findCommitteDetail : "+logUserId+ends);
        return dtBean;
    }

    /**
     *Search on tenderid and funName returns Member details
     * @param object search parameter
     * @param funName samepe,otheragency
     * @return List of OfficeMemberDtBean
     */
    public List<OfficeMemberDtBean> findOfficeMember(String object,String funName){
        logger.debug("findOfficeMember : "+logUserId+starts);
        logger.debug("findOfficeMember : "+logUserId+ends);
        List<SPTenderCommonData> sptcds = sPTenderCommon.executeProcedure(funName,object, null);
        List<OfficeMemberDtBean> officeMemberDtBeans = new ArrayList<OfficeMemberDtBean>();
        for (SPTenderCommonData sPTenderCommonData : sptcds) {
            OfficeMemberDtBean memberDtBean = new OfficeMemberDtBean();
            memberDtBean.setUserid(Integer.parseInt(sPTenderCommonData.getFieldName1()));
            memberDtBean.setEmpName(sPTenderCommonData.getFieldName2().substring(0, sPTenderCommonData.getFieldName2().indexOf("-")));
            memberDtBean.setDesgName(sPTenderCommonData.getFieldName2().substring(sPTenderCommonData.getFieldName2().indexOf("-")+1,sPTenderCommonData.getFieldName2().length()));
            memberDtBean.setProcureRole(sPTenderCommonData.getFieldName3());
            memberDtBean.setGovUserId(sPTenderCommonData.getFieldName4());
            officeMemberDtBeans.add(memberDtBean);
        }
        return officeMemberDtBeans;
    }
    public List<OfficeMemberDtBean> findOfficeMember(String tenderId,String object,String funName){
        logger.debug("findOfficeMember : "+logUserId+starts);
        logger.debug("findOfficeMember : "+logUserId+ends);
        List<SPTenderCommonData> sptcds = sPTenderCommon.executeProcedure(funName,object,tenderId);
        List<OfficeMemberDtBean> officeMemberDtBeans = new ArrayList<OfficeMemberDtBean>();
        for (SPTenderCommonData sPTenderCommonData : sptcds) {
            OfficeMemberDtBean memberDtBean = new OfficeMemberDtBean();
            memberDtBean.setUserid(Integer.parseInt(sPTenderCommonData.getFieldName1()));
            memberDtBean.setEmpName(sPTenderCommonData.getFieldName2().substring(0, sPTenderCommonData.getFieldName2().indexOf("-")));
            memberDtBean.setDesgName(sPTenderCommonData.getFieldName2().substring(sPTenderCommonData.getFieldName2().indexOf("-")+1,sPTenderCommonData.getFieldName2().length()));
            memberDtBean.setProcureRole(sPTenderCommonData.getFieldName3());
            memberDtBean.setGovUserId(sPTenderCommonData.getFieldName4());
            officeMemberDtBeans.add(memberDtBean);
        }
        return officeMemberDtBeans;
    }

    /**
     * Organization List
     * @param values variable parameter
     * @return List of Department search on organization
     * @throws Exception
     */
    public List<TblDepartmentMaster> organizationList(Object...values)throws Exception{
        logger.debug("organizationList : "+logUserId+starts);
        logger.debug("organizationList : "+logUserId+ends);
        return tblDepartmentMasterDao.findTblDepartmentMaster(values);
    }
    /**
     *Creates Committe
     * @param xml1 XML for Single entry in tbl_Committee
     * @param xml2 XML for Multiple entry in tbl_CommitteeMembers
     * @return success message and id of tbl_Committee
     */
    public CommonSPReturn createCommitteMem(String xml1,String xml2,String ... values){
        logger.debug("createCommitteMem : "+logUserId+starts);
        CommonSPReturn sPReturn = new CommonSPReturn();
        String commType = "Evaluation";
        String action = null;
        try {
            if(values.length!=0 && values[1]!=null){
                if(values[1].equals("TOC") || values[1].equals("POC")){
                    commType = "Opening";
                }else if(values[1].equals("TSC")){
                    commType = "Technical Sub";
                }
            }        
            sPReturn = sPInsertMultiTablesXML.executeProcedure("insert", "tbl_Committee", "tbl_CommitteeMembers", null, null, xml1, xml2, null, null, "committeeId", "comMemberId", null,null);
            action = "Create "+commType+" Committee";
        } catch (Exception e) {
            logger.error("createCommitteMem : "+logUserId,e);
            action = "Error in Create "+commType+" Committee "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,(values.length!=0 && values[0]!=null) ? Integer.parseInt(values[0]) : 0 ,"tenderId", "Opening".equals(commType) ? EgpModule.Opening.getName() : EgpModule.Evaluation.getName(), action, "");
        }
        logger.debug("createCommitteMem : "+logUserId+ends);
        return sPReturn;
    }

    /**
     *Search on tenderid and funName returns Member details
     * @param object search parameter
     * @param funName based on which respective function is called from tendercommondata
     * @return List of CommitteMemDtBean
     */
    public List<CommitteMemDtBean> findCommitteMember(String object,String funName){
        logger.debug("findCommitteMember : "+logUserId+starts);
        List<SPTenderCommonData> sptcds = sPTenderCommon.executeProcedure(funName,object, null);
        List<CommitteMemDtBean> officeMemberDtBeans = new ArrayList<CommitteMemDtBean>();
        for (SPTenderCommonData sPTenderCommonData : sptcds) {
            officeMemberDtBeans.add(new CommitteMemDtBean(sPTenderCommonData.getFieldName1(),sPTenderCommonData.getFieldName2() , sPTenderCommonData.getFieldName3(), sPTenderCommonData.getFieldName4(), sPTenderCommonData.getFieldName5(), sPTenderCommonData.getFieldName6(),sPTenderCommonData.getFieldName7(),sPTenderCommonData.getFieldName8(),sPTenderCommonData.getFieldName9()));
        }
        logger.debug("findCommitteMember : "+logUserId+ends);
        return officeMemberDtBeans;
    }

    /**
     * In Edit these method update committe member details
     * @param commId committee Id
     * @param xml1 tbl_committe xml
     * @param xml2 tbl_committemember xml
     */
    public void updateCommitteMem(String commId,String xml1,String xml2,String ... values){
        logger.debug("updateCommitteMem : "+logUserId+starts);
        String action = null;
        String commType = "Evaluation";
        try {
             if(values.length!=0 && values[1]!=null){
                if(values[1].equals("TOC") || values[1].equals("POC")){
                    commType = "Opening";
                }else if(values[1].equals("TSC")){
                    commType = "Technical Sub";
                }
            }    
            sPInsertMultiTablesXML.executeProcedure("insdel", "tbl_Committee", "tbl_CommitteeMembers", null, null, xml1, xml2, null, null, "committeeId", "comMemberId", null,"committeeId="+commId);
            action = "Edit "+commType+" Committee";
        } catch (Exception e) {
            logger.error("createCommitteMem : "+logUserId,e);
            action = "Error in Edit "+commType+" Committee "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,(values.length!=0 && values[0]!=null) ? Integer.parseInt(values[0]) : 0 ,"tenderId", "Opening".equals(commType) ? EgpModule.Opening.getName() : EgpModule.Evaluation.getName(), action, "");
            action = null;
        }
        logger.debug("updateCommitteMem : "+logUserId+ends);        
    }

    /**
     * Publish Committe
     * @param commId ID of committe to be published
     * @param remarks remarks for approval
     * @param tenderId tenderId from tbl_TenderMaster
     * @return if update 1 else 0
     */
    public List<Object> publishComm(String commId,String remarks,String tenderId,String ... values){
        logger.debug("publishComm : "+logUserId+starts);
        List<Object> list = null;
        String action = null;
        String comType = "Evaluation";
        try {
            if(values.length!=0 && values[0]!=null){
                if(values[0].equals("open")){
                    comType = "Opening";
                }else if(values[0].equals("tsc")){
                    comType = "Technical Sub";
                }
            }
            int i =hibernateQueryDao.updateDeleteNewQuery("update TblCommittee set remarks='"+remarks+"',committeStatus='approved',publishDate=current_timestamp() where committeeId="+commId);
            if(i!=0){
                list = new ArrayList<Object>();
                StringBuilder query = new StringBuilder();
                query.append("select om.officeName,dm.departmentName from TblOfficeMaster om,TblDepartmentMaster dm ");
                query.append("where om.officeId in (select td.officeId from TblTenderDetails td where td.tblTenderMaster.tenderId="+tenderId+") ");
                query.append("and om.departmentId = dm.departmentId");
                List<Object[]> obj = hibernateQueryDao.createNewQuery(query.toString());
                if(!obj.isEmpty()){
                    list.add(obj.get(0)[0]);
                    list.add(obj.get(0)[1]);
                }
                query.delete(0, query.length());
                obj.clear();
                query.append("select cm.userId,cm.memberFrom from TblCommitteeMembers cm where cm.tblCommittee.committeeId="+commId+"");
                obj = hibernateQueryDao.createNewQuery(query.toString());
                query.delete(0, query.length());
                List userIds = new ArrayList();
                for (Object[] objects : obj) {
                    userIds.add(objects[0]);
                }
                query.append("select lm.emailId from TblLoginMaster lm where lm.userId in ("+userIds.toString().substring(1, userIds.toString().length()-1)+")");
                list.addAll(hibernateQueryDao.singleColQuery(query.toString()));
                query.delete(0, query.length());
                list.add("junk_data");
                for (Object[] objects : obj) {
                    List<Object> mob = null;
                    if(objects[1].toString().equals("External Member")){
                        mob = hibernateQueryDao.singleColQuery("select emi.mobileNo from TblExternalMemInfo emi where emi.tblLoginMaster.userId="+objects[0].toString());
                        if(mob!=null && (!mob.isEmpty())){
                            list.add("+880"+mob.get(0));
                        }
                    }else{
                        mob = hibernateQueryDao.singleColQuery("select em.mobileNo from TblEmployeeMaster em where em.tblLoginMaster.userId="+objects[0].toString());
                        if(mob!=null && (!mob.isEmpty())){
                            String mobile = "+880"+mob.get(0).toString();
                            list.add(mobile.replace("+8800", "+880"));
                        }
                    }
                }
                obj=null;
                userIds=null;
            }
            action = "Notify "+comType+" Committee Members";
        } catch (Exception e) {
            logger.error("publishComm : "+logUserId,e);
            action = "Error in Notify "+comType+" Committee Members "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,Integer.parseInt(tenderId) ,"tenderId", "Opening".equals(comType) ? EgpModule.Opening.getName() : EgpModule.Evaluation.getName(), action, remarks);
        }        
        logger.debug("publishComm : "+logUserId+ends);
        return list;
    }
    
    
    
    /**
     * Cancel Tender Mail
     * @param tenderId tenderId from tbl_TenderMaster
     * @return if update 1 else 0
     */
    public List<Object> cancelTenderMail(String tenderId){
        logger.debug("cancelTenderMail : "+logUserId+starts);
        List<Object> list = null;
        String action = null;
        String comType = "Opening";
        try {
            
                list = new ArrayList<Object>();
                StringBuilder query = new StringBuilder();
                query.append("select om.officeName,dm.departmentName from TblOfficeMaster om,TblDepartmentMaster dm ");
                query.append("where om.officeId in (select td.officeId from TblTenderDetails td where td.tblTenderMaster.tenderId="+tenderId+") ");
                query.append("and om.departmentId = dm.departmentId");
                List<Object[]> obj = hibernateQueryDao.createNewQuery(query.toString());
                if(!obj.isEmpty()){
                    list.add(obj.get(0)[0]);
                    list.add(obj.get(0)[1]);
                }
                query.delete(0, query.length());
                obj.clear();
                
                
                query.append("select cm.committeeId from TblCommittee cm where cm.tblTenderMaster.tenderId="+tenderId+"and committeeType='TOC'");
                obj = hibernateQueryDao.createNewQuery(query.toString());
                String opCom = obj.toString();
                String opComId = "";
                int length = opCom.length();
                for(int i=1;i<length-1;i++)
                {
                    opComId+=opCom.charAt(i);
                }
                query.delete(0, query.length());
                obj.clear();
                
                query.append("select cm.userId,cm.memberFrom from TblCommitteeMembers cm where cm.tblCommittee.committeeId="+opComId+"");
                obj = hibernateQueryDao.createNewQuery(query.toString());
                query.delete(0, query.length());
                List userIds = new ArrayList();
                for (Object[] objects : obj) {
                    userIds.add(objects[0]);
                }
                query.append("select lm.emailId from TblLoginMaster lm where lm.userId in ("+userIds.toString().substring(1, userIds.toString().length()-1)+")");
                list.addAll(hibernateQueryDao.singleColQuery(query.toString()));
                query.delete(0, query.length());
                list.add("junk_data");
                for (Object[] objects : obj) {
                    List<Object> mob = null;
                    if(objects[1].toString().equals("External Member")){
                        mob = hibernateQueryDao.singleColQuery("select emi.mobileNo from TblExternalMemInfo emi where emi.tblLoginMaster.userId="+objects[0].toString());
                        if(mob!=null && (!mob.isEmpty())){
                            list.add("+880"+mob.get(0));
                        }
                    }else{
                        mob = hibernateQueryDao.singleColQuery("select em.mobileNo from TblEmployeeMaster em where em.tblLoginMaster.userId="+objects[0].toString());
                        if(mob!=null && (!mob.isEmpty())){
                            String mobile = "+880"+mob.get(0).toString();
                            list.add(mobile.replace("+8800", "+880"));
                        }
                    }
                }
                obj=null;
                userIds=null;
            
            action = "Notify "+comType+" Committee Members";
        } catch (Exception e) {
            logger.error("CancelTenderMail : "+logUserId,e);
            action = "Error in Notify "+comType+" Committee Members "+e;
        }finally{
            
        }        
        logger.debug("cancelTenderMail : "+logUserId+ends);
        return list;
    }
    
    
    
    

    /**
     *
     * @param userIds
     * @return
     */
    public String openComMemEncry(String userIds[]){
        logger.debug("openComMemEncry : "+logUserId+starts);
        String passwords="";
        StringBuffer sb = new StringBuffer();
        sb.append("select password from TblLoginMaster where userId="+userIds[0]);
        for (int i = 1; i < userIds.length; i++) {
            sb.append(" or userId="+userIds[i]);
        }
        List<Object> list = hibernateQueryDao.singleColQuery(sb.toString());
        for (Object object : list) {
            passwords+=(String)object;
        }
        logger.debug("openComMemEncry : "+logUserId+ends);
        return passwords;
    }

    /**
     * Hash Insertion of two member's password of committee
     * @param xml String for insertion into tbl_TenderHash
     */
    public void addComEncryDetails(String xml,String ... values){
        logger.debug("addComEncryDetails : "+logUserId+starts);
        String action = null;
        try {
           sPInsertMultiTablesXML.executeProcedure("insert", "tbl_TenderHash", null, null, null, xml, null, null, null, "tenderHashId", null, null,null);
           action = "Add Committee members for encryption-Decryption";
        } catch (Exception e) {
            logger.error("addComEncryDetails : "+logUserId,e);
            action="Error in Add Committee members for encryption-Decryption "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,(values.length!=0 && values[0]!=null) ? Integer.parseInt(values[0]) : 0 ,"tenderId",  EgpModule.Tender_Notice.getName(), action, "");
        }
        logger.debug("addComEncryDetails : "+logUserId+ends);
    }
    
    /**
     *
     * @param from condition string for checking
     * @param where Tables of which count is required
     * @return count for the query
     * @throws Exception
     */
    public long checkCountByHql(String from, String where)throws Exception {
        logger.debug("checkCountByHql : "+logUserId+starts);
        logger.debug("checkCountByHql : "+logUserId+ends);
        return hibernateQueryDao.countForNewQuery(from, where);
    }
    
    public long cntTECByTidAndCS(String tenderid,String commStatus)throws Exception{
        logger.debug("cntTECByTidAndCS : "+logUserId+starts);
        logger.debug("cntTECByTidAndCS : "+logUserId+ends);
        
        if(!"".equalsIgnoreCase(commStatus)){
            commStatus = "and tc.committeStatus= '"+commStatus+"'";
        }
        
        String from = "TblCommittee tc";
        String where = "tc.tblTenderMaster.tenderId=" + tenderid + " and tc.committeeType in ('TEC','PEC')"+commStatus;
        return hibernateQueryDao.countForNewQuery(from, where);
    }

    /**
     *Gives List of tbl_TenderHash based on criteria
     * @param values variable parameter criteria
     * @return List of tbl_TenderHash
     * @throws Exception
     */
    public List<TblTenderHash> findTenderHash(Object...values)throws Exception{
        logger.debug("findTenderHash : "+logUserId+starts);
        logger.debug("findTenderHash : "+logUserId+ends);
        return tblTenderHashDao.findTblTenderHash(values);
    }

    /**
     *For Edition of committe member hash details
     * @param userIds members userids
     * @param userid session id
     * @param encryString passwords hashcord
     * @param tenderid id of the tender on which updation is done
     */
    public void updateComMemEncry(String userIds, String userid,String encryString,String tenderid) {
        logger.debug("updateComMemEncry : "+logUserId+starts);
        String action = null;
        try {
           hibernateQueryDao.updateDeleteNewQuery("update TblTenderHash set tenderHashUserId='"+userIds+"', tenderHash='"+encryString+"', creationDate=current_timestamp(), createdBy="+userid+" where tenderId="+tenderid);
           action = "Edit Committee members for encryption-Decryption";
        } catch (Exception e) {
            logger.error("addComEncryDetails : "+logUserId,e);
            action="Error in Edit Committee members for encryption-Decryption "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,Integer.parseInt(tenderid),"tenderId",  EgpModule.Tender_Notice.getName(), action, "");
        }
        logger.debug("updateComMemEncry : "+logUserId+ends);        
    }

    /**
     *This method returns committee member - memberRole & employeeName
     * @param commId Committee Id from tbl_Committee
     * @return returns committee member - memberRole & employeeName
     */
    public List<Object[]> findCommMemberById(String commId){
        logger.debug("findCommMemberById : "+logUserId+starts);
        logger.debug("findCommMemberById : "+logUserId+ends);
        List<Object[]> list = new ArrayList<Object[]>();
        List<Object> comList = new ArrayList<Object>();
        StringBuilder query = new StringBuilder();
        query.append("select c.memberRole,em.employeeName,c.memberFrom from TblCommitteeMembers c,TblEmployeeMaster em where c.tblCommittee.committeeId="+commId+" and c.userId=em.tblLoginMaster.userId");
        list.addAll(hibernateQueryDao.createNewQuery(query.toString()));        
        boolean isExt = false;
        query.delete(0, query.length());
        query.append("select c.committeeType from TblCommittee c where c.committeeId="+commId);
        comList = hibernateQueryDao.getSingleColQuery(query.toString());
        if(!comList.isEmpty()){
            if(comList.get(0).toString().equals("TSC")){
                isExt = true;
            }
        }
        query.delete(0, query.length());
        if(isExt){
            query.append("select c.memberRole,em.fullName,c.memberFrom from TblCommitteeMembers c,TblExternalMemInfo em where c.tblCommittee.committeeId="+commId+" and c.userId=em.tblLoginMaster.userId");
            list.addAll(hibernateQueryDao.createNewQuery(query.toString()));
        }
        return list;
    }
    
    /**
     * Based on function name method returns CommitteDtBean
     * @param tenderid from tbl_tenderMaster
     * @param funName on base of funName different functions from tendercommondata is called
     * @return List of CommitteDtBean
     */
    public List<CommitteDtBean> findCommitteDetailHistory(int tenderid,String funName){
        logger.debug("findCommitteDetailHistory : "+logUserId+starts);
        List<CommitteDtBean> dtBeanList = new ArrayList<CommitteDtBean>();
        List<SPTenderCommonData> sPTenderCommonDatas = sPTenderCommon.executeProcedure(funName, String.valueOf(tenderid), null);
        for (SPTenderCommonData sPTenderCommonData : sPTenderCommonDatas) {
            CommitteDtBean dtBean = new CommitteDtBean();
            dtBean.setMaxMemReq(Integer.parseInt(sPTenderCommonData.getFieldName4()));
            //dtBean.setMaxTenderVal(new BigDecimal(sPTenderCommonData.getFieldName2()));
            dtBean.setMinMemFromPe(Integer.parseInt(sPTenderCommonData.getFieldName6()));
            dtBean.setMinMemFromTec(Integer.parseInt(sPTenderCommonData.getFieldName7()));
            dtBean.setMinMemOutSidePe(Integer.parseInt(sPTenderCommonData.getFieldName5()));
            dtBean.setMinMemReq(Integer.parseInt(sPTenderCommonData.getFieldName3()));
            //dtBean.setMinTenderVal(new BigDecimal(sPTenderCommonData.getFieldName1()));
            dtBean.setpNature(sPTenderCommonData.getFieldName8());
            dtBean.setCommitteeId(sPTenderCommonData.getFieldName9());
            dtBeanList.add(dtBean);
        }
        logger.debug("findCommitteDetailHistory : "+logUserId+ends);
        return dtBeanList;
    }

    /**
     *Finds History of the previously configured committee for the tenderId
     * @param object ex : tenderId
     * @param funName on base of funName different functions from tendercommondata is called
     * @param commId committeeId from tbl_Committee
     * @return List of CommitteDtBean
     */
    public List<CommitteMemDtBean> findCommitteMemberHistory(String object,String funName,String commId){
         logger.debug("findCommitteMemberHistory : "+logUserId+starts);
        List<SPTenderCommonData> sptcds = sPTenderCommon.executeProcedure(funName,object, commId);
        List<CommitteMemDtBean> officeMemberDtBeans = new ArrayList<CommitteMemDtBean>();
        for (SPTenderCommonData sPTenderCommonData : sptcds) {
            officeMemberDtBeans.add(new CommitteMemDtBean(sPTenderCommonData.getFieldName1(),sPTenderCommonData.getFieldName2() , sPTenderCommonData.getFieldName3(), sPTenderCommonData.getFieldName4(), sPTenderCommonData.getFieldName5(), sPTenderCommonData.getFieldName6(),sPTenderCommonData.getFieldName7()));
        }
        logger.debug("findCommitteMemberHistory : "+logUserId+ends);
        return officeMemberDtBeans;
    }

     /**
      *Gives the officeId from which the tender is floated
      * @param tenderId from tbl_tenderMaster
      * @return officeId
      */
     public String getTenderOfficeId(String tenderId){
         String id = "0";
         List<Object> list = hibernateQueryDao.singleColQuery("select ttd.officeId from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId);
         if(!list.isEmpty()){
             id=list.get(0).toString();
         }
         return id;
     }

     /**
     * insert values in TblTenderDomesticPref
      * @param tenderDomesticPref List of TblTenderDomesticPref to be inserted
      * @return true or false
     */
     public boolean saveTblTenderDomesticPref(List<TblTenderDomesticPref> tenderDomesticPref){
         logger.debug("saveTblTenderDomesticPref : "+logUserId+starts);
         boolean flag = false;
         try {
             tblTenderDomesticPrefDao.updateOrSaveTenderDomesticPref(tenderDomesticPref);
             flag = true;
         } catch (Exception e) {
             logger.error("saveTblTenderDomesticPref : "+logUserId+e);
         }
         logger.debug("saveTblTenderDomesticPref : "+logUserId+ends);
         return flag;
     }

     /**
     * fetching values in TblTenderDomesticPref
     * @param tenderId
     * @return 0 in case of date not available else more than 0
     */
     public int getTblTenderDomesticPref(int tenderId){
         logger.debug("getTblTenderDomesticPref : "+logUserId+starts);
         int size = 0;
         try {
             List<TblTenderDomesticPref> tblTenderDomesticPref = new ArrayList<TblTenderDomesticPref>();
             tblTenderDomesticPref = tblTenderDomesticPrefDao.findTblTenderDomesticPref("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId));
             if(!tblTenderDomesticPref.isEmpty()){
                 size = tblTenderDomesticPref.size();
             }
         } catch (Exception e) {
             logger.error("getTblTenderDomesticPref : "+logUserId+e);
         }
         logger.debug("getTblTenderDomesticPref : "+logUserId+ends);
         return size;
     }
     /**
     * fetching values in TblTenderDomesticPref
     * @param tenderId
     * @return details from TblTenderDomesticPref
     */
     public List<TblTenderDomesticPref> getDetailsTblTenderDomesticPref(int tenderId){
         logger.debug("getTblTenderDomesticPref : "+logUserId+starts);
         List<TblTenderDomesticPref> tblTenderDomesticPref = new ArrayList<TblTenderDomesticPref>();
         try {
             tblTenderDomesticPref = tblTenderDomesticPrefDao.findTblTenderDomesticPref("tblTenderMaster",Operation_enum.EQ,new TblTenderMaster(tenderId));
         } catch (Exception e) {
             logger.error("getTblTenderDomesticPref : "+logUserId+e);
         }
         logger.debug("getTblTenderDomesticPref : "+logUserId+ends);
         return tblTenderDomesticPref;
     }

     /**
      *Get List of userId based on tenderId and committeeType(TEC(PEC),TOC(POC))
      * @param tenderId from tbl_TenderMaster
      * @param comType type of committee(TEC(PEC),TOC(POC))
      * @return List of userId in Committee
      */
     public List<Object> getCommitteeMemIds(String tenderId,String comType){
        return hibernateQueryDao.singleColQuery("select cm.userId from TblCommittee c,TblCommitteeMembers cm  where c.committeeId= cm.tblCommittee.committeeId and c.tblTenderMaster.tenderId="+tenderId+" and c.committeeType in ("+comType+") and c.committeStatus='approved'");
     }

      public List<Object> getCommitteeCPId(String tenderId){
        return hibernateQueryDao.singleColQuery("select cm.userId from TblCommittee c,TblCommitteeMembers cm  where c.committeeId= cm.tblCommittee.committeeId and c.tblTenderMaster.tenderId="+tenderId+" and c.committeeType in ('TEC','PEC') and c.committeStatus='approved' and cm.memberRole = 'cp'");
     }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }     
}
