/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCmsVendorReqWcc;

/**
 *
 * @author rikin.p
 */
public interface CmsVendorReqWccService {
    
    public int addCmsVendorReqWcc(TblCmsVendorReqWcc tblCmsVendorReqWcc);
    
    public boolean isRequestPending(int lotId,int cntId);
    
    public void setLogUserId(String logUserId);
    
    public int updateReqest(int wcCerId, int lotId);
    
}
