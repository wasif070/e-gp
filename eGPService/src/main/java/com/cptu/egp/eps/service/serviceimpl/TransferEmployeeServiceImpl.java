/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAdminMasterDao;
/*
Edit by palash, Dohatec
 */
import com.cptu.egp.eps.dao.daointerface.TblAdminTransferDao;
//End
import com.cptu.egp.eps.dao.daointerface.TblEmployeeMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEmployeeTrasferDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminDao;
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminTransferDao;
import com.cptu.egp.eps.dao.daointerface.TblUserActivationHistoryDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPAddUser;
import com.cptu.egp.eps.model.table.TblAdminMaster;
/*
Edit by palash, Dohatec
 */
import com.cptu.egp.eps.model.table.TblAdminTransfer;
//End
import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.model.table.TblUserActivationHistory;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita,TaherT
 */
public class TransferEmployeeServiceImpl {

    TblEmployeeTrasferDao tblEmployeeTrasferDao;
    TblPartnerAdminDao tblPartnerAdminDao;
    TblPartnerAdminTransferDao tblPartnerAdminTransferDao;
    TblUserActivationHistoryDao tblUserActivationHistoryDao;
    HibernateQueryDao hibernateQueryDao;
    TblEmployeeMasterDao tblEmployeeMasterDao;
    TblAdminMasterDao tblAdminMasterDao;
    //Edited by palash for PE Admin Transfer, Dohatec
    TblAdminTransferDao tblAdminTransferDao;
    //End
    SPAddUser sPAddUser;
    private String logUserId = "0";
    private final String starts = " Starts";
    private final String ends = " Ends";
    static final Logger LOGGER = Logger.getLogger(TransferEmployeeServiceImpl.class);
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    public TblEmployeeMasterDao getTblEmployeeMasterDao() {
        return tblEmployeeMasterDao;
    }

    public void setTblEmployeeMasterDao(TblEmployeeMasterDao tblEmployeeMasterDao) {
        this.tblEmployeeMasterDao = tblEmployeeMasterDao;
    }

    public TblPartnerAdminDao getTblPartnerAdminDao() {
        return tblPartnerAdminDao;
    }

    public void setTblPartnerAdminDao(TblPartnerAdminDao tblPartnerAdminDao) {
        this.tblPartnerAdminDao = tblPartnerAdminDao;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblEmployeeTrasferDao getTblEmployeeTrasferDao() {
        return tblEmployeeTrasferDao;
    }

    public void setTblEmployeeTrasferDao(TblEmployeeTrasferDao tblEmployeeTrasferDao) {
        this.tblEmployeeTrasferDao = tblEmployeeTrasferDao;
    }

    public TblPartnerAdminTransferDao getTblPartnerAdminTransferDao() {
        return tblPartnerAdminTransferDao;
    }

    public void setTblPartnerAdminTransferDao(TblPartnerAdminTransferDao tblPartnerAdminTransferDao) {
        this.tblPartnerAdminTransferDao = tblPartnerAdminTransferDao;
    }

    /*
Edit by palash, Dohatec
     */
    public TblAdminTransferDao getTblAdminTransferDao() {
        return tblAdminTransferDao;
    }

    public void setTblAdminTransferDao(TblAdminTransferDao tblAdminTransferDao) {
        this.tblAdminTransferDao = tblAdminTransferDao;
    }
//End

    public TblUserActivationHistoryDao getTblUserActivationHistoryDao() {
        return tblUserActivationHistoryDao;
    }

    public void setTblUserActivationHistoryDao(TblUserActivationHistoryDao tblUserActivationHistoryDao) {
        this.tblUserActivationHistoryDao = tblUserActivationHistoryDao;
    }

    public TblAdminMasterDao getTblAdminMasterDao() {
        return tblAdminMasterDao;
    }

    public void setTblAdminMasterDao(TblAdminMasterDao tblAdminMasterDao) {
        this.tblAdminMasterDao = tblAdminMasterDao;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * inserting values in Tbl_EmployeeTrasfer when user transfer
     *
     * @param tblEmployeeTrasfer
     * @return true if inserted successfully else false
     */
    public boolean insertInToTblEmployeeMaster(TblEmployeeTrasfer tblEmployeeTrasfer) {

        LOGGER.debug("insertInToTblEmployeeMaster : " + logUserId + starts);
        boolean flag = false;
        try {
            tblEmployeeTrasferDao.addTblEmployeeTrasfer(tblEmployeeTrasfer);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("insertInToTblEmployeeMaster : " + logUserId + e);
        }
        LOGGER.debug("insertInToTblEmployeeMaster : " + logUserId + ends);
        return flag;
    }
    
    
    public List<TblEmployeeTrasfer> findTblEmployeeMaster(int userId, String action, String isCurrent) {

        LOGGER.debug("findTblEmployeeMaster : " + logUserId + starts);
        List<TblEmployeeTrasfer> list = new ArrayList<>();
        try {
            list = tblEmployeeTrasferDao.findTblEmployeeTrasfer("tblLoginMaster.userId", Operation_enum.EQ, userId,"action", Operation_enum.EQ, action,"isCurrent", Operation_enum.EQ, isCurrent);
        } catch (Exception e) {
            LOGGER.error("findTblEmployeeMaster : " + logUserId + e);
        }
        LOGGER.debug("findTblEmployeeMaster : " + logUserId + ends);
        return list;
    }

    /**
     * updating info when user replace
     *
     * @param tblpartneradmin
     * @param partnerId of Tbl_PartnerAdminTransfer
     * @param userId of tbl_LoginMaster
     * @param password tbl_LoginMaster
     * @return true if updated successfully
     */
    public boolean replaceuser(TblPartnerAdminTransfer tblpartneradmin, int partnerId, int userId, String password) {

        LOGGER.debug("replaceuser : " + logUserId + starts);
        boolean flag = false;
        String action = "Transfer Development Partner User";
        if (!"".equalsIgnoreCase(tblpartneradmin.getIsMakerChecker())) {
            action = "Transfer Scheduled Bank User";
        }
        try {
            String query = "update TblPartnerAdminTransfer set isCurrent = 'no' where partnerId='" + partnerId + "' and userId='" + userId + "'";
            String query1 = "update TblLoginMaster set hintQuestion = '' , hintAnswer = '',firstLogin = 'yes',failedAttempt = '0',password = '" + password + "' where userId = '" + userId + "' ";

            hibernateQueryDao.updateDeleteNewQuery(query);
            hibernateQueryDao.updateDeleteNewQuery(query1);
            tblPartnerAdminTransferDao.addTblPartnerAdminTransfer(tblpartneradmin);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("replaceuser : " + logUserId + e);
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, userId, "userId", EgpModule.Manage_Users.getName(), action, tblpartneradmin.getComments());
            action = null;
        }
        LOGGER.debug("replaceuser : " + logUserId + ends);
        return flag;
    }

    /**
     * updating info for tbl_partneradmin
     *
     * @param tblpartneradmin
     * @return true if updated successfully
     */
    public boolean updatePartnerAdmin(TblPartnerAdmin tblpartneradmin) {

        LOGGER.debug("updatePartnerAdmin : " + logUserId + starts);
        boolean flag = false;
        try {
            tblPartnerAdminDao.updateTblPartnerAdmin(tblpartneradmin);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("updatePartnerAdmin : " + logUserId + e);
        }
        LOGGER.debug("updatePartnerAdmin : " + logUserId + ends);
        return flag;
    }

    /**
     * updating data in to Tbl_PartnerAdminTransfer
     *
     * @param fullname
     * @param makerchecker
     * @param designation
     * @param natId
     * @param partnetid
     * @param status
     * @return true if updated successfully
     */
    public boolean updatePartnerAdmintransfer(String fullname, String makerchecker, String designation, String natId, int partnetid, String status) {

        LOGGER.debug("updatePartnerAdmintransfer : " + logUserId + starts);
        boolean flag = false;
        try {
            String query = "update TblPartnerAdminTransfer set fullName = '" + fullname + "',isMakerChecker='" + makerchecker + "',designation='" + designation + "',nationalId='" + natId + "' where partnerId='" + partnetid + "' and isCurrent = '" + status + "'";
            hibernateQueryDao.updateDeleteNewQuery(query);
            flag = true;
        } catch (Exception e) {
            LOGGER.error("updatePartnerAdmintransfer : " + logUserId + e);
        }
        LOGGER.debug("updatePartnerAdmintransfer : " + logUserId + ends);
        return flag;
    }

    /**
     * get details for particular employeeId
     *
     * @param employeeId
     * @return list of object code added by palash, dohatec.
     */
    public List<Object[]> getDetailsExistingUser(int employeeId, int userId) {
        List<Object[]> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + starts);
        try {
            if (employeeId != 0) {
                list = hibernateQueryDao.createNewQuery("select tlm.emailId,tem.employeeName,tem.employeeNameBangla,tem.nationalId,tem.mobileNo,td.departmentName,tdm.designationName,tlm.userId, tem.userEmployeeId, tem.contactAddress from TblLoginMaster tlm , TblEmployeeMaster tem , TblDesignationMaster  tdm ,TblDepartmentMaster td where tlm.userId = tem.tblLoginMaster.userId and tdm.designationId = tem.tblDesignationMaster.designationId and td.departmentId = tdm.tblDepartmentMaster.departmentId and tem.employeeId = " + employeeId);
            } else {
                list = hibernateQueryDao.createNewQuery("select lm.emailId, am.fullName, om.officeNameInBangla, am.nationalId, am.mobileNo, om.officeName, om.departmentId, lm.userId "
                        + "from TblLoginMaster lm, TblAdminMaster am, TblOfficeAdmin oa, TblOfficeMaster om "
                        + "where lm.userId = am.tblLoginMaster.userId and oa.tblLoginMaster.userId = lm.userId and om.officeId = oa.tblOfficeMaster.officeId and am.tblLoginMaster.userId = " + userId);
            }
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + ends);
        return list;
    }

    /**
     * get office name for particular employeeId
     *
     * @param employeeId
     * @return office name
     */
    public List<Object> getDetailsExistingUserOffice(int employeeId) {
        List<Object> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + starts);
        try {
            list = hibernateQueryDao.getSingleColQuery("select om.officeName from TblEmployeeOffices eo,TblOfficeMaster om where eo.tblOfficeMaster.officeId = om.officeId and eo.tblEmployeeMaster.employeeId=" + employeeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + ends);
        return list;
    }

    /**
     * get procurementRole for particular employeeId
     *
     * @param employeeId
     * @return procurementRole
     */
    public List<Object> getDetailsExistingUserPR(int employeeId) {
        List<Object> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + starts);
        try {
            list = hibernateQueryDao.getSingleColQuery("select pr.procurementRole from TblEmployeeRoles er,TblProcurementRole pr where er.tblProcurementRole.procurementRoleId=pr.procurementRoleId and er.tblEmployeeMaster.employeeId=" + employeeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + ends);
        return list;
    }

    /**
     * get details from emalid and usertypeid for PE Admin
     *
     * @param emailId of user
     * @param userTypeId
     * @return list of objects //Edited by palash for PE Admin Transfer, Dohatec
     */
    public List<Object[]> getDetailsFromEmailId1(String emailId, int userTypeId) {
        List<Object[]> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + starts);
        try {
            list = hibernateQueryDao.createNewQuery("select tam.fullName,tam.phoneNo,tam.nationalId,tam.mobileNo from TblLoginMaster tlm,TblAdminMaster tam where tlm.userId = tam.tblLoginMaster.userId and tlm.emailId = '" + emailId + "' and tlm.status = 'approved' and tlm.tblUserTypeMaster.userTypeId = " + userTypeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + ends);
        return list;
    }

    //End
    /**
     * get details from emalid and usertypeid
     *
     * @param emailId of user
     * @param userTypeId
     * @return list of objects
     */
    public List<Object[]> getDetailsFromEmailId(String emailId, int userTypeId) {
        List<Object[]> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + starts);
        try {
            list = hibernateQueryDao.createNewQuery("select tem.employeeName,tem.employeeNameBangla,tem.nationalId,tem.mobileNo, tem.userEmployeeId, tem.contactAddress from TblLoginMaster tlm,TblEmployeeMaster tem where tlm.userId = tem.tblLoginMaster.userId and tlm.emailId = '" + emailId + "' and tlm.status = 'approved' and tlm.tblUserTypeMaster.userTypeId = " + userTypeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + ends);
        return list;
    }

    /**
     * get details from emalid and usertypeid
     *
     * @param emailId of user
     * @param userTypeId
     * @return list of objects
     */
    public List<Object[]> getDetailByEmailId(String emailId, int userTypeId) {
        List<Object[]> list = null;
        LOGGER.debug("getDetailByEmailId : " + logUserId + starts);
        try {
            list = hibernateQueryDao.createNewQuery("select tem.fullName,tem.nationalId,tem.mobileNo from TblLoginMaster tlm,TblPartnerAdmin tem where tlm.userId = tem.tblLoginMaster.userId and tlm.emailId = '" + emailId + "' and tlm.tblUserTypeMaster.userTypeId = " + userTypeId);
        } catch (Exception e) {
            LOGGER.error("getDetailByEmailId : " + logUserId + e);
        }
        LOGGER.debug("getDetailByEmailId : " + logUserId + ends);
        return list;
    }

    /**
     * updating information for account status
     *
     * @param strUserId
     * @param strStatus
     * @return true if updated else false
     */
    public boolean changeStatus(String strUserId, String strStatus, String... actionAudit) {
        LOGGER.debug("changeStatus : " + logUserId + "Starts");
        boolean bSuccess = false;
        String strQuery = "update TblLoginMaster tlm set tlm.status = '" + strStatus + "'  where tlm.userId = " + strUserId;
        String action = "";
        String remark = "";
        try {
            strStatus = strStatus.equals("deactive") ? "Deactive" : "Activate";
            hibernateQueryDao.updateDeleteNewQuery(strQuery);
            bSuccess = true;
            if (actionAudit != null && actionAudit.length != 0) {
                action = actionAudit[0];
                if (actionAudit.length >= 2) {
                    if ("6".equalsIgnoreCase(actionAudit[2])) {
                        action = strStatus + " Development Partner Admin";
                        if ("user".equalsIgnoreCase(actionAudit[0])) {
                            action = strStatus + " Development Partner User";
                        }
                    } else if ("7".equalsIgnoreCase(actionAudit[2])) {
                        action = strStatus + " Scheduled Bank Admin";
                        if ("user".equalsIgnoreCase(actionAudit[0])) {
                            action = strStatus + " Scheduled Bank User";
                        }
                    } else if ("15".equalsIgnoreCase(actionAudit[2])) {
                        action = strStatus + " Scheduled Bank Branch Admin";
                    } else if ("3".equalsIgnoreCase(actionAudit[2])) {
                        action = strStatus + " Goverment User";
                    }
                    remark = actionAudit[1];
                }
            }
        } catch (Exception e) {
            LOGGER.error("changeStatus : " + logUserId + e);
            action = "Error in " + action;
        } finally {
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(strUserId), "userId", EgpModule.Manage_Users.getName(), action, remark);
            action = null;
        }
        LOGGER.debug("changeStatus : " + logUserId + "Ends");
        return bSuccess;
    }

    /**
     * fetching information of partner transfer
     *
     * @param strUserId
     * @param strPartnerId
     * @return data for partner transfer
     */
    public List<TblPartnerAdminTransfer> getPartnerTransferDetail(String strUserId, String strPartnerId) {
        LOGGER.debug("getPartnerTransfer : " + logUserId + "Starts");
        List<TblPartnerAdminTransfer> list = new ArrayList<TblPartnerAdminTransfer>();
        try {
            list = tblPartnerAdminTransferDao.findTblPartnerAdminTransfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(strUserId)), "partnerId", Operation_enum.EQ, Integer.parseInt(strPartnerId));
        } catch (Exception e) {
            LOGGER.error("getPartnerTransfer : " + logUserId + e);
        }
        LOGGER.debug("getPartnerTransfer : " + logUserId + "Ends");
        return list;
    }

    /**
     * get partnerId
     *
     * @param strUserId
     * @return String - partnerId
     */
    public String getPartnerId(String strUserId) {
        LOGGER.debug("getPartnerTransfer : " + logUserId + "Starts");
        String partnerId = "";
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select tlm.partnerId from TblPartnerAdmin tlm where tlm.tblLoginMaster.userId=" + Integer.parseInt(strUserId));
            if (!list.isEmpty()) {
                partnerId = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getPartnerTransfer : " + logUserId + e);
        }
        LOGGER.debug("getPartnerTransfer : " + logUserId + "Ends");
        return partnerId;
    }

    /**
     * fetching information for govt user
     *
     * @param strUserId
     * @param strEmpId
     * @return data for govt user
     */
    public List<TblEmployeeTrasfer> getGovtUserDetail(String strUserId, String strEmpId) {
        LOGGER.debug("getGovtUserId : " + logUserId + "Starts");
        List<TblEmployeeTrasfer> list = new ArrayList<TblEmployeeTrasfer>();
        try {
            list = tblEmployeeTrasferDao.findTblEmployeeTrasfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(strUserId)), "employeeId", Operation_enum.EQ, Integer.parseInt(strEmpId));
        } catch (Exception e) {
            LOGGER.error("getGovtUserId : " + logUserId + e);
        }
        LOGGER.debug("getGovtUserId : " + logUserId + "Ends");
        return list;
    }

    /**
     * Fetching info from Tbl_AdminMaster
     *
     * @param strUserId
     * @return List TblAdminMaster
     */
    public List<TblAdminMaster> getAdminDetail(String strUserId) {
        LOGGER.debug("getAdminId : " + logUserId + "Starts");
        List<TblAdminMaster> list = new ArrayList<TblAdminMaster>();
        try {
            list = tblAdminMasterDao.findTblAdminMaster("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(strUserId)));
        } catch (Exception e) {
            LOGGER.error("getAdminId : " + logUserId + e);
        }
        LOGGER.debug("getAdminId : " + logUserId + "Ends");
        return list;
    }

    /**
     * inserting values into TblUserActivationHistory
     *
     * @param tblUserActivationHistory
     * @return true if inserted else false
     */
    public boolean addActivationHistory(TblUserActivationHistory tblUserActivationHistory) {
        LOGGER.debug("addActivationHistory : " + logUserId + "Starts");
        boolean bSuccess = false;
        try {
            tblUserActivationHistoryDao.addTblUserActivationHistory(tblUserActivationHistory);
            bSuccess = true;
        } catch (Exception e) {
            LOGGER.error("addActivationHistory : " + logUserId + e);
        }
        LOGGER.debug("addActivationHistory : " + logUserId + "Ends");
        return bSuccess;
    }

    /**
     * updating info when employee transfer
     *
     * @param employeeTrasfer
     * @param password
     * @return true if updated successfully
     */
    public boolean transferEmployee(TblEmployeeTrasfer employeeTrasfer, String password) {
        LOGGER.debug("transferEmployee : " + logUserId + ends);
        boolean flag = false;
        String action = "Transfer Government User";
        String query = "";
        try {
            TblEmployeeMaster tblEmployeeMaster = tblEmployeeMasterDao.findEmployee("employeeId", Operation_enum.EQ, employeeTrasfer.getEmployeeId()).get(0);
            TblEmployeeMaster cloneEmployeeMaster = new TblEmployeeMaster();
            //BeanUtils.copyProperties(tblEmployeeMaster, cloneEmployeeMaster);
            cloneEmployeeMaster.setEmployeeId(tblEmployeeMaster.getEmployeeId());
            cloneEmployeeMaster.setTblDesignationMaster(tblEmployeeMaster.getTblDesignationMaster());
            cloneEmployeeMaster.setTblLoginMaster(tblEmployeeMaster.getTblLoginMaster());
            cloneEmployeeMaster.setFinPowerBy(tblEmployeeMaster.getFinPowerBy());
            cloneEmployeeMaster.setCreatedBy(tblEmployeeMaster.getCreatedBy());
            cloneEmployeeMaster.setEmployeeName(employeeTrasfer.getEmployeeName());
            cloneEmployeeMaster.setEmployeeNameBangla(employeeTrasfer.getEmployeeNameBangla());
            cloneEmployeeMaster.setNationalId(employeeTrasfer.getNationalId());
            cloneEmployeeMaster.setMobileNo(employeeTrasfer.getMobileNo());
            cloneEmployeeMaster.setUserEmployeeId(employeeTrasfer.getUserEmployeeId());
            cloneEmployeeMaster.setContactAddress(employeeTrasfer.getContactAddress());
//            tblEmployeeMasterDao.updateEmployee(cloneEmployeeMaster);
            query = " UPDATE [dbo].[tbl_EmployeeMaster]"
                    + " SET employeeName='"+cloneEmployeeMaster.getEmployeeName()+"',employeeNameBangla='"+cloneEmployeeMaster.getEmployeeNameBangla()+"'"
                    + ",designationId="+ cloneEmployeeMaster.getTblDesignationMaster().getDesignationId() +",mobileNo='"+cloneEmployeeMaster.getMobileNo()+"'"
                    + ",nationalId='"+cloneEmployeeMaster.getNationalId()+"'"
                    + ",finPowerBy='"+cloneEmployeeMaster.getFinPowerBy()+"',createdBy='"+cloneEmployeeMaster.getCreatedBy()+"'"
                    + ",userEmployeeId='"+cloneEmployeeMaster.getUserEmployeeId()+"',contactAddress='"+cloneEmployeeMaster.getContactAddress()+"' "
                    + "WHERE employeeId=" + cloneEmployeeMaster.getEmployeeId();
//            cloneEmployeeMaster = null;
//            tblEmployeeMaster = null;
//            int i = hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set hintQuestion='',hintAnswer='',failedAttempt=0,firstLogin='yes',password='" + password + "' where userId=" + employeeTrasfer.getTblLoginMaster().getUserId());
            query = query + " UPDATE tbl_LoginMaster SET hintQuestion='',hintAnswer='',"
                    + "failedAttempt=0,firstLogin='yes',password='" + password + "' "
                    + "WHERE userId=" + employeeTrasfer.getTblLoginMaster().getUserId();
//            if (i != 0) {
//            int j = hibernateQueryDao.updateDeleteNewQuery("update TblEmployeeTrasfer set isCurrent='no' where employeeId=" + employeeTrasfer.getEmployeeId());
            query = query + " UPDATE tbl_EmployeeTrasfer SET isCurrent='no' "
                    + "WHERE employeeId=" + employeeTrasfer.getEmployeeId();
//            if (j != 0) {
//                tblEmployeeTrasferDao.addTblEmployeeTrasfer(employeeTrasfer);
//                flag = true;
            query = query + " INSERT INTO [dbo].[tbl_EmployeeTrasfer]"
                    + "([userId],[transferDt]"
                    + ",[createdBy],[remarks]"
                    + ",[employeeName]"
                    + ",[mobileNo],[nationalId]"
                    + ",[finPowerBy],[employeeId]"
                    + ",[action],[emailId]"
                    + ",[replacedBy],[replacedByEmailId]"
                    + ",[isCurrent],[comments]"
                    + ",[userEmployeeId],[contactAddress])"
                    + "VALUES(" + employeeTrasfer.getTblLoginMaster().getUserId() + ", GETDATE(),"
                    + employeeTrasfer.getCreatedBy() + ",'" + employeeTrasfer.getRemarks() + "','"
                    + employeeTrasfer.getEmployeeName() + "','"
                    + employeeTrasfer.getMobileNo() + "','" + employeeTrasfer.getNationalId() + "','"
                    + employeeTrasfer.getFinPowerBy() + "'," + employeeTrasfer.getEmployeeId() + ",'"
                    + employeeTrasfer.getAction() + "','" + employeeTrasfer.getEmailId() + "','"
                    + employeeTrasfer.getReplacedBy() + "','" + employeeTrasfer.getReplacedByEmailId() + "','"
                    + employeeTrasfer.getIsCurrent() + "','" + employeeTrasfer.getComments() + "','"
                    + employeeTrasfer.getUserEmployeeId() + "','" + employeeTrasfer.getContactAddress()
                    + "')";
//            }
//            }
            CommonMsgChk commonMsgChk = null;
            commonMsgChk = sPAddUser.executeProcedure(0, "transferGovtUser", query).get(0);
            if (commonMsgChk.getFlag() == true) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("transferEmployee : " + logUserId, e);
            action = "Error in " + action + " " + e.getMessage();
        } finally {

            makeAuditTrailService.generateAudit(auditTrail, employeeTrasfer.getTblLoginMaster().getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
        }
        LOGGER.debug("transferEmployee : " + logUserId + ends);
        return flag;
    }

    /**
     * updating info when PE Admin transfer
     *
     * @param adminTrasfer
     * @param password
     * @return true if updated successfully code added by palash, dohatec.
     */
    public boolean transferAdmin(TblAdminTransfer adminTransfer, String password) {
        LOGGER.debug("transferEmployee : " + logUserId + ends);
        boolean flag = false;
        String action = "Transfer PE Admin User";
        try {
            TblAdminMaster tblAdminMaster = tblAdminMasterDao.findTblAdminMaster("adminId", Operation_enum.EQ, adminTransfer.getTblAdminMaster().getAdminId()).get(0);
            TblAdminMaster cloneAdminMaster = new TblAdminMaster();
            cloneAdminMaster.setAdminId(tblAdminMaster.getAdminId());
            cloneAdminMaster.setTblLoginMaster(tblAdminMaster.getTblLoginMaster());
            cloneAdminMaster.setFullName(adminTransfer.getFullName());
            cloneAdminMaster.setNationalId(adminTransfer.getNationalId());
            cloneAdminMaster.setMobileNo(adminTransfer.getMobileNo());

            cloneAdminMaster.setPhoneNo(tblAdminMaster.getPhoneNo());
            tblAdminMasterDao.updateTblAdminMaster(cloneAdminMaster);
            cloneAdminMaster = null;
            tblAdminMaster = null;
            int i = hibernateQueryDao.updateDeleteNewQuery("update TblLoginMaster set hintQuestion='',hintAnswer='',failedAttempt=0,firstLogin='yes',password='" + password + "' where userId=" + adminTransfer.getTblLoginMaster().getUserId());
            if (i != 0) {
                int j = hibernateQueryDao.updateDeleteNewQuery("update TblAdminTransfer set isCurrent='no' where adminId=" + adminTransfer.getTblAdminMaster().getAdminId());
                if (j != 0) {
                    tblAdminTransferDao.addTblAdminTransfer(adminTransfer);
                    flag = true;
                }
            }
        } catch (Exception e) {
            LOGGER.error("transferEmployee : " + logUserId, e);
            action = "Error in " + action + " " + e.getMessage();
        } finally {

            makeAuditTrailService.generateAudit(auditTrail, adminTransfer.getTblLoginMaster().getUserId(), "userId", EgpModule.Manage_Users.getName(), action, "");
        }
        LOGGER.debug("transferEmployee : " + logUserId + ends);
        return flag;
    }

    //End
    /**
     * Fetching info from Tbl_PartnerAdminTransfer
     *
     * @param strUserId userid of tbl_LoginMaster
     * @return list of Tbl_PartnerAdminTransfer
     */
    public List<TblPartnerAdminTransfer> viewTransferHistory(String strUserId) {
        LOGGER.debug("viewTransferHistory : " + logUserId + starts);
        List<TblPartnerAdminTransfer> list = null;
        try {

            list = tblPartnerAdminTransferDao.findTblPartnerAdminTransfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(strUserId)));
            Collections.sort(list, new CompareHistory());
        } catch (Exception e) {
            LOGGER.error("viewTransferHistory : " + logUserId + e);
        }
        LOGGER.debug("viewTransferHistory : " + logUserId + ends);
        return list;
    }

    /**
     * Fetching info from Tbl_AdminTransfer
     *
     * @param strUserId userid of tbl_LoginMaster
     * @return list of Tbl_AdminTransfer //Edited by palash for PE Admin
     * Transfer, Dohatec
     */
    public List<TblAdminTransfer> viewPEAdminTransferHistory(String strUserId) {
        LOGGER.debug("viewPEAdminTransferHistory : " + logUserId + starts);
        List<TblAdminTransfer> list_1 = null;
        try {

            list_1 = tblAdminTransferDao.findTblAdminTransfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(Integer.parseInt(strUserId)));
            Collections.sort(list_1, new CompareHistory_1());
        } catch (Exception e) {
            LOGGER.error("viewPEAdminTransferHistory : " + logUserId + e);
        }
        LOGGER.debug("viewPEAdminTransferHistory : " + logUserId + ends);
        return list_1;
    }
// End

    /**
     * fetching information for account status history
     *
     * @param strUserId
     * @return data for account history
     */
    public List<TblUserActivationHistory> viewAccountStatusHistory(String strUserId) {
        LOGGER.debug("viewAccountStatusHistory : " + logUserId + starts);
        List<TblUserActivationHistory> list = null;
        try {

            list = tblUserActivationHistoryDao.findTblUserActivationHistory("userId", Operation_enum.EQ, Integer.parseInt(strUserId));

        } catch (Exception e) {
            LOGGER.error("viewAccountStatusHistory : " + logUserId + e);
        }
        LOGGER.debug("viewAccountStatusHistory : " + logUserId + ends);
        return list;
    }

    /**
     * Fetching info from Tbl_PartnerAdminTransfer
     *
     * @param strUserId userid of tbl_LoginMaster
     * @return list of Tbl_PartnerAdminTransfer
     */
    public List<TblEmployeeTrasfer> viewAccountStatusHistoryEmployee(int strUserId) {
        LOGGER.debug("viewAccountStatusHistoryEmployee : " + logUserId + starts);
        List<TblEmployeeTrasfer> list = null;
        try {

            list = tblEmployeeTrasferDao.findTblEmployeeTrasfer("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(strUserId));

        } catch (Exception e) {
            LOGGER.error("viewAccountStatusHistoryEmployee : " + logUserId + e);
        }
        LOGGER.debug("viewAccountStatusHistoryEmployee : " + logUserId + ends);
        return list;
    }

    /**
     * fetching office officeAdminId
     *
     * @param userId
     * @return String
     */
    public String getPeAdminId(String userId) {
        LOGGER.debug("getPeAdminId : " + logUserId + "Starts");
        String adminid = "";
        try {
            List<Object> list = hibernateQueryDao.singleColQuery("select tlm.officeAdminId from TblOfficeAdmin tlm where tlm.tblLoginMaster.userId=" + Integer.parseInt(userId));
            if (!list.isEmpty()) {
                adminid = list.get(0).toString();
            }
        } catch (Exception e) {
            LOGGER.error("getPeAdminId : " + logUserId + e);
        }
        LOGGER.debug("getPeAdminId : " + logUserId + "Ends");
        return adminid;

    }

    /**
     * @return the sPAddUser
     */
    public SPAddUser getsPAddUser() {
        return sPAddUser;
    }

    /**
     * @param sPAddUser the sPAddUser to set
     */
    public void setsPAddUser(SPAddUser sPAddUser) {
        this.sPAddUser = sPAddUser;
    }
}

class CompareHistory implements Comparator<TblPartnerAdminTransfer> {

    public int compare(TblCurrencyMaster o1, TblCurrencyMaster o2) {
        return o1.getCurrencyName().compareTo(o2.getCurrencyName());
    }

    @Override
    public int compare(TblPartnerAdminTransfer o1, TblPartnerAdminTransfer o2) {
        return o1.getTranseredDt().compareTo(o2.getTranseredDt());
    }
}

/*
Edit by palash, Dohatec
 */
class CompareHistory_1 implements Comparator<TblAdminTransfer> {

    public int compare(TblCurrencyMaster o1, TblCurrencyMaster o2) {
        return o1.getCurrencyName().compareTo(o2.getCurrencyName());
    }

    @Override
    public int compare(TblAdminTransfer o1, TblAdminTransfer o2) {
        return o1.getTransferDt().compareTo(o2.getTransferDt());
    }
}
//End
