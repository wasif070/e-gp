/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblEmailLog;

/**
 *
 * @author Administrator
 */
public interface EmailLogService {

    /**
     * Insert Data to EmailLog
     * @param tblEmailLog
     */
    public void addEmailLog(TblEmailLog tblEmailLog);
    
    /**
     * Set User Id at Service layer
     * @param logUserId
     */
    public void setUserId(String logUserId);
}
