/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblCascadeLinkMaster;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public interface CascadeMasterService {
    /**
     * Get Cascade Master Details By Query
     * @param userTypeId
     * @param parentList
     * @return
     * @throws Exception
     */
    public List<TblCascadeLinkMaster> getCascadeMasterList(String userTypeId, String parentList) throws Exception;
}
