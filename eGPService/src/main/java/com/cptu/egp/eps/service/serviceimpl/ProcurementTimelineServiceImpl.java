/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.ProcurementTimelineData;
import com.cptu.egp.eps.dao.storedprocedure.SPProcurementTimeline;
import com.cptu.egp.eps.service.serviceinterface.ProcurementTimelineService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Istiak (Dohatec) - 26.May.15
 */
public class ProcurementTimelineServiceImpl implements ProcurementTimelineService {

    private static final Logger LOGGER = Logger.getLogger(ProcurementTimelineServiceImpl.class);
    private SPProcurementTimeline sPProcurementTimeline;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;

    /**
     * @return the sPProcurementTimeline
     */
    public SPProcurementTimeline getsPProcurementTimeline() {
        return sPProcurementTimeline;
    }

    /**
     * @param sPProcurementTimeline the sPProcurementTimeline to set
     */
    public void setsPProcurementTimeline(SPProcurementTimeline sPProcurementTimeline) {
        this.sPProcurementTimeline = sPProcurementTimeline;
    }

    /**
     * @return the logUserId
     */
    public String getLogUserId() {
        return logUserId;
    }

    /**
     * @param logUserId the logUserId to set
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * @return the auditTrail
     */
    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    /**
     * @param auditTrail the auditTrail to set
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    /**
     * @return the makeAuditTrailService
     */
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    /**
     * @param makeAuditTrailService the makeAuditTrailService to set
     */
    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }

    @Override
    public List<ProcurementTimelineData> procTimelineData(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15) {
        
        List<ProcurementTimelineData> list = null;
        String action = "";
        try{
            list = sPProcurementTimeline.executeProcedure(fieldName1, fieldName2, fieldName3, fieldName4, fieldName5, fieldName6, fieldName7, fieldName8, fieldName9, fieldName10, fieldName11, fieldName12, fieldName13, fieldName14, fieldName15);
            
            if(fieldName1.equalsIgnoreCase("DeleteRows")){
                if("true".equalsIgnoreCase(list.get(0).getFieldName1()) && list != null){
                    action = "Remove Procurement Approval Timeline Configuration";
                }else{
                    action = "Error in Remove Procurement Approval Timeline Configuration";
                }
                makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            }
        }catch(Exception e){
            LOGGER.error("\nprocTimelineData() : " + e.toString());
            action = "Error in Procurement Approval Timeline Configuration. " + e.toString();
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }

        return list;
    }

    @Override
    public int getAllRuleCount() {
        try{
            return Integer.parseInt(sPProcurementTimeline.executeProcedure("CountRules", "", "", "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1());
        }catch(Exception e){
            LOGGER.error("\ngetAllRuleCount() : " + e.toString());
        }
        return 0;
    }

    @Override
    public boolean insertOrUpdate(String[] ruleId, String[] NewOldEdit, String[] appAuthority, String[] procNature, String[] noOfDaysForTSC, String[] noOfDaysForTECorPEC, String[] noOfDaysForApproval) {
        String action = "";
        boolean isSuccess = false;
        try{
            StringBuilder insert = new StringBuilder();
            StringBuilder update = new StringBuilder();
            for(int i = 0; i < ruleId.length; i++)
            {
                if(NewOldEdit[i].equalsIgnoreCase("New"))
                {
                    if(noOfDaysForApproval[i].equalsIgnoreCase("Upto Tender Validity"))
                        noOfDaysForApproval[i] = "-1";

                    insert.append(" INSERT INTO tbl_ProcurementApprovalTimeline VALUES('");
                    insert.append(appAuthority[i]).append("', '");
                    insert.append(procNature[i]).append("', ");
                    insert.append(noOfDaysForTSC[i]).append(", ");
                    insert.append(noOfDaysForTECorPEC[i]).append(", ");
                    insert.append(noOfDaysForApproval[i]).append("); ");
                }
                else if(NewOldEdit[i].equalsIgnoreCase("Edit"))
                {
                    if(noOfDaysForApproval[i].equalsIgnoreCase("Upto Tender Validity"))
                        noOfDaysForApproval[i] = "-1";

                    update.append(" UPDATE tbl_ProcurementApprovalTimeline SET ApprovalAuthority = '").append(appAuthority[i]);
                    update.append("', ProcurementNature = '").append(procNature[i]);
                    update.append("', NoOfDaysForTSC = ").append(noOfDaysForTSC[i]);
                    update.append(", NoOfDaysForTECorPEC = ").append(noOfDaysForTECorPEC[i]);
                    update.append(", NoOfDaysForApproval = ").append(noOfDaysForApproval[i]);
                    update.append(" WHERE RuleId = ").append(ruleId[i]).append("; ");
                }
            }
            String flag = sPProcurementTimeline.executeProcedure("InsertOrUpdate", insert.toString(), update.toString(), "", "", "", "", "", "", "", "", "", "", "", "").get(0).getFieldName1();
            if(flag.equalsIgnoreCase("true")){
                action = "Insert/Update Procurement Approval Timeline Configuration";
                isSuccess = true;
            } else {
                action = "Error in Insert/Update Procurement Approval Timeline Configuration";
                isSuccess = false;
            }
        }catch(Exception e){
            LOGGER.error("\naddorUpdate() : " + e.toString());
            action = "Error in Insert/Update Procurement Approval Timeline Configuration. "  + e.toString();
            isSuccess = false;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
        }

        return isSuccess;
    }
}
