/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderRightsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderRights;
import com.cptu.egp.eps.service.serviceinterface.TenderSubRightService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ketan
 */
public class TenderSubRightServiceImpl implements TenderSubRightService {

    HibernateQueryDao hibernateQueryDao;
    TblTenderDetailsDao tblTenderDetailsDao;
    TblTenderRightsDao tblTenderRightsDao;
    SPCommonSearch sptender;
    static final Logger LOGGER = Logger.getLogger(TenderSubRightServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblTenderDetailsDao getTblTenderDetailsDao() {
        return tblTenderDetailsDao;
    }

    public void setTblTenderDetailsDao(TblTenderDetailsDao tblTenderDetailsDao) {
        this.tblTenderDetailsDao = tblTenderDetailsDao;
    }

    public TblTenderRightsDao getTblTenderRightsDao() {
        return tblTenderRightsDao;
    }

    public void setTblTenderRightsDao(TblTenderRightsDao tblTenderRightsDao) {
        this.tblTenderRightsDao = tblTenderRightsDao;
    }

    public SPCommonSearch getSptender() {
        return sptender;
    }

    public void setSptender(SPCommonSearch sptender) {
        this.sptender = sptender;
    }
    


    @Override
    public List<Object[]> getActiveUserOfCmpnyByUserId(String strUserId) {
        LOGGER.debug("getActivateUserOfCmpnyByUserId : " + logUserId + LOGGERSTART);
        List<Object[]> list = new ArrayList<Object[]>();
        String strQuery = "select tendererId,tblLoginMaster.userId,tblCompanyMaster.companyId,title,firstName,middleName,lastName,department from TblTendererMaster WHERE tblCompanyMaster.companyId in (select tblCompanyMaster.companyId FROM TblTendererMaster WHERE tblLoginMaster.userId = " + Integer.valueOf(strUserId) + ")";
        try {
            list = hibernateQueryDao.createNewQuery(strQuery);
        } catch (Exception e) {
            LOGGER.error("getActivateUserOfCmpnyByUserId : " + logUserId + e);
        }
        LOGGER.debug("getActivateUserOfCmpnyByUserId : " + logUserId + LOGGEREND);
        return list;
    }

    @Override
    public List<TblTenderDetails> getTenderDetails(String strTenderId) {
        LOGGER.debug("getTenderDetails : " + logUserId + LOGGERSTART);
        List<TblTenderDetails> listTenderDetails = new ArrayList<TblTenderDetails>();
        try {
            listTenderDetails = tblTenderDetailsDao.findTblTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.valueOf(strTenderId)));
        } catch (Exception e) {
            LOGGER.error("getTenderDetails : " + logUserId + e);
        }
        LOGGER.debug("getTenderDetails : " + logUserId + LOGGEREND);
        return listTenderDetails;
    }

    @Override
    public boolean isJvcaUser(String strUserId) {
        LOGGER.debug("isJvcaUser : " + logUserId + LOGGERSTART);
        boolean bSuccess = false;
        try {
            long cnt = hibernateQueryDao.countForNewQuery("TblLoginMaster", "userId = " + Integer.valueOf(strUserId) + " and isJvca = 'yes' ");
            if (cnt > 0) {
                bSuccess = true;
            } else {
                bSuccess = false;
            }
        } catch (Exception e) {
            LOGGER.error("isJvcaUser : " + logUserId + e);
        }
        LOGGER.debug("isJvcaUser : " + logUserId + LOGGEREND);
        return bSuccess;
    }

    @Override
    public boolean assingTenderRights(TblTenderRights tblTenderRights) {
        LOGGER.debug("assingTenderRights : " + logUserId + LOGGERSTART);
        int iTenderId = tblTenderRights.getTblTenderMaster().getTenderId();
        String strQuery = "update TblTenderRights ttr set ttr.isCurrent = 'no'   where ttr.tblTenderMaster.tenderId ='"+iTenderId+"'";
        try {
            int iRet = hibernateQueryDao.updateDeleteNewQuery(strQuery);
            tblTenderRightsDao.addEntity(tblTenderRights);

        } catch (Exception e) {
            LOGGER.error("assingTenderRights : " + logUserId + e);
        }
        LOGGER.debug("assingTenderRights : " + logUserId + LOGGEREND);
        return true;
    }

    @Override
    public List<SPCommonSearchData> getTenderRightAssignee(String strFunName, String strUserId, String strTenderId) {
        return sptender.executeProcedure(strFunName, strUserId, strTenderId, "", "", "", "", "", "", "",null);
    }
}
