/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.storedprocedure.SearchNOA;
import com.cptu.egp.eps.dao.storedprocedure.SearchNOAData;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Krishnraj
 */
public class SearchNOAService {

    static final Logger LOGGER = Logger.getLogger(SearchNOAService.class);
    private HibernateQueryDao hibernateQueryDao;
    SearchNOA srNOA;
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public SearchNOA getSrNOA() {
        return srNOA;
    }

    public void setSrNOA(SearchNOA srNOA) {
        this.srNOA = srNOA;
    }
    //Added 2 parameter advDtTo and noaDtTo by Proshanto Kumar Saha
    public List<SearchNOAData> returndata(String keyword, String contractDtFrom, String contractDtTo,
            int departmentId, int contractId, String contractNo, int tenderId,
            String tenderRefNo, String cpvCode, String contractAmt, int officeId,
            int page, int recordPerPage, String stateName, String advDt,String advDtTo,String procurementMethod, String noaDt,String noaDtTo,String contractAwardTo) {
        return srNOA.executeProcedure(keyword, contractDtFrom, contractDtTo, departmentId, contractId, contractNo, tenderId, tenderRefNo, cpvCode, contractAmt, officeId, page, recordPerPage, stateName, advDt,advDtTo,procurementMethod, noaDt,noaDtTo,contractAwardTo);
    }

    public List<Object[]> getPackageDetailsByTenderId(int Tenderid) {
        LOGGER.debug("getPackageDetailsByTenderId : " + logUserId + LOGGERSTART);
        List<Object[]> obj = null;
        try {
            String query = "select userId,pkgLotId from TblNoaIssueDetails where tblTenderMaster.tenderId = " + Tenderid;
            obj = hibernateQueryDao.createNewQuery(query);
        } catch (Exception ex) {
            LOGGER.error("getPackageDetailsByTenderId : " + logUserId + ex);
        }
        LOGGER.debug("getPackageDetailsByTenderId : " + logUserId + LOGGEREND);
        return obj;
    }
}
