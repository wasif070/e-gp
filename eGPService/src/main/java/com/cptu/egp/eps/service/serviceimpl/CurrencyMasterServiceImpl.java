/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblCurrencyMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import com.cptu.egp.eps.service.serviceinterface.CurrencyMasterService;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CurrencyMasterServiceImpl implements CurrencyMasterService {

    TblCurrencyMasterDao tblCurrencyMasterDao;

    final Logger logger = Logger.getLogger(CurrencyMasterServiceImpl.class);

    String logUserId = "0";

    public TblCurrencyMasterDao getTblCurrencyMasterDao() {
        return tblCurrencyMasterDao;
    }

    public void setTblCurrencyMasterDao(TblCurrencyMasterDao tblCurrencyMasterDao) {
        this.tblCurrencyMasterDao = tblCurrencyMasterDao;
    }

    @Override
    public List<TblCurrencyMaster> getAllCurrency(){
        logger.debug("getAllCurrency : "+logUserId+" Starts");
        List<TblCurrencyMaster> listCurrency = null;
        try{
            listCurrency = tblCurrencyMasterDao.getAllTblCurrencyMaster();
        Collections.sort(listCurrency, new CompareCurrency());
        }catch(Exception ex){
            logger.error("getAllCurrency : "+logUserId+" :"+ex);
        }
        logger.debug("getAllCurrency : "+logUserId+" Ends");
        return listCurrency;
    }

    @Override
    public List<TblCurrencyMaster> getAllCurrencySortByShortName(){
        logger.debug("getAllCurrency : "+logUserId+" Starts");
        List<TblCurrencyMaster> listCurrency = null;
        try{
            listCurrency = tblCurrencyMasterDao.getAllTblCurrencyMaster();
        Collections.sort(listCurrency, new CompareCurrencyByShortName());
        }catch(Exception ex){
            logger.error("getAllCurrency : "+logUserId+" :"+ex);
        }
        logger.debug("getAllCurrency : "+logUserId+" Ends");
        return listCurrency;
    }

    @Override
    public List<TblCurrencyMaster> getCurrencyDetails(int iCurrencyid){
        logger.debug("getCurrencyDetails : "+logUserId+" Starts");
        List<TblCurrencyMaster> list = null;
        try{
            list =  tblCurrencyMasterDao.findTblCurrencyMaster("currencyId",Operation_enum.EQ,iCurrencyid);
        }catch (Exception ex){
            logger.error("getCurrencyDetails : "+logUserId+" :"+ex);
    }
        logger.debug("getCurrencyDetails : "+logUserId+" Ends");
        return list;
    }

    @Override
    public List<TblCurrencyMaster> getCurrencyDetails(String strCurrencyShortName){
        logger.debug("getCurrencyDetails : "+logUserId+" Starts");
        List<TblCurrencyMaster> list = null;
        try{
            list =  tblCurrencyMasterDao.findTblCurrencyMaster("currencyShortName",Operation_enum.EQ,strCurrencyShortName);
        }catch (Exception ex){
            logger.error("getCurrencyDetails : "+logUserId+" :"+ex);
    }
        logger.debug("getCurrencyDetails : "+logUserId+" Ends");
        return list;
    }

    @Override
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }



}

class CompareCurrency implements Comparator<TblCurrencyMaster>{

    @Override
    public int compare(TblCurrencyMaster o1, TblCurrencyMaster o2) {
            return o1.getCurrencyName().compareTo(o2.getCurrencyName());
    }
}

class CompareCurrencyByShortName implements Comparator<TblCurrencyMaster>{

    @Override
    public int compare(TblCurrencyMaster o1, TblCurrencyMaster o2) {
            return o1.getCurrencyShortName().compareTo(o2.getCurrencyShortName());
    }
}
