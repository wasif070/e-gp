/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblBidRankDetailDao;
import com.cptu.egp.eps.dao.daointerface.TblBidderRankDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalRoundMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblEvalTerreportDao;
import com.cptu.egp.eps.dao.daointerface.TblReportColumnMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblReportFormsDao;
import com.cptu.egp.eps.dao.daointerface.TblReportFormulaMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblReportLotsDao;
import com.cptu.egp.eps.dao.daointerface.TblReportMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblReportTableMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderFormsDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPGenerateReport;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPEvalCommonSearchData;
import com.cptu.egp.eps.model.table.TblBidRankDetail;
import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblEvalTerreport;
import com.cptu.egp.eps.model.table.TblReportColumnMaster;
import com.cptu.egp.eps.model.table.TblReportForms;
import com.cptu.egp.eps.model.table.TblReportFormulaMaster;
import com.cptu.egp.eps.model.table.TblReportLots;
import com.cptu.egp.eps.model.table.TblReportMaster;
import com.cptu.egp.eps.model.table.TblReportTableMaster;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class ReportCreationService {

    static final Logger logger = Logger.getLogger(ReportCreationService.class);
    private String logUserId = "0";
    private TblReportMasterDao tblReportMasterDao;
    private TblReportTableMasterDao tblReportTableMasterDao;
    private TblReportColumnMasterDao tblReportColumnMasterDao;
    private TblTenderFormsDao tblTenderFormsDao;
    private HibernateQueryDao hibernateQueryDao;
    private TblReportFormulaMasterDao tblReportFormulaMasterDao;
    private TblReportFormsDao tblReportFormsDao;
    private SPTenderCommon sPTenderCommon;
    private SPGenerateReport sPGenerateReport;
    private SPGenerateReport sPGenerateNegReport;
    private TblBidderRankDao tblBidderRankDao;
    private TblBidRankDetailDao tblBidRankDetailDao;
    private CommonSearchDataMoreService commonSearchDataMoreService;
    private TblReportLotsDao tblReportLotsDao;
    private TblEvalTerreportDao tblEvalTerreportDao;
    private TblEvalRoundMasterDao tblEvalRoundMasterDao;
    private SPEvalCommonSearchData sPEvalCommonSearchData;

    public SPGenerateReport getsPGenerateNegReport() {
        return sPGenerateNegReport;
    }

    public void setsPGenerateNegReport(SPGenerateReport sPGenerateNegReport) {
        this.sPGenerateNegReport = sPGenerateNegReport;
    }

    public TblEvalRoundMasterDao getTblEvalRoundMasterDao() {
        return tblEvalRoundMasterDao;
    }

    public void setTblEvalRoundMasterDao(TblEvalRoundMasterDao tblEvalRoundMasterDao) {
        this.tblEvalRoundMasterDao = tblEvalRoundMasterDao;
    }

    public TblEvalTerreportDao getTblEvalTerreportDao() {
        return tblEvalTerreportDao;
    }

    public void setTblEvalTerreportDao(TblEvalTerreportDao tblEvalTerreportDao) {
        this.tblEvalTerreportDao = tblEvalTerreportDao;
    }

    public TblReportLotsDao getTblReportLotsDao() {
        return tblReportLotsDao;
    }

    public void setTblReportLotsDao(TblReportLotsDao tblReportLotsDao) {
        this.tblReportLotsDao = tblReportLotsDao;
    }

    public CommonSearchDataMoreService getCommonSearchDataMoreService() {
        return commonSearchDataMoreService;
    }

    public void setCommonSearchDataMoreService(CommonSearchDataMoreService commonSearchDataMoreService) {
        this.commonSearchDataMoreService = commonSearchDataMoreService;
    }
    
    public TblBidRankDetailDao getTblBidRankDetailDao() {
        return tblBidRankDetailDao;
    }

    public void setTblBidRankDetailDao(TblBidRankDetailDao tblBidRankDetailDao) {
        this.tblBidRankDetailDao = tblBidRankDetailDao;
    }

    public TblBidderRankDao getTblBidderRankDao() {
        return tblBidderRankDao;
    }

    public void setTblBidderRankDao(TblBidderRankDao tblBidderRankDao) {
        this.tblBidderRankDao = tblBidderRankDao;
    }

    public SPGenerateReport getsPGenerateReport() {
        return sPGenerateReport;
    }

    public void setsPGenerateReport(SPGenerateReport sPGenerateReport) {
        this.sPGenerateReport = sPGenerateReport;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    public TblReportFormsDao getTblReportFormsDao() {
        return tblReportFormsDao;
    }

    public void setTblReportFormsDao(TblReportFormsDao tblReportFormsDao) {
        this.tblReportFormsDao = tblReportFormsDao;
    }

    public TblReportFormulaMasterDao getTblReportFormulaMasterDao() {
        return tblReportFormulaMasterDao;
    }

    public void setTblReportFormulaMasterDao(TblReportFormulaMasterDao tblReportFormulaMasterDao) {
        this.tblReportFormulaMasterDao = tblReportFormulaMasterDao;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblReportColumnMasterDao getTblReportColumnMasterDao() {
        return tblReportColumnMasterDao;
    }

    public void setTblReportColumnMasterDao(TblReportColumnMasterDao tblReportColumnMasterDao) {
        this.tblReportColumnMasterDao = tblReportColumnMasterDao;
    }

    public TblReportMasterDao getTblReportMasterDao() {
        return tblReportMasterDao;
    }

    public void setTblReportMasterDao(TblReportMasterDao tblReportMasterDao) {
        this.tblReportMasterDao = tblReportMasterDao;
    }

    public TblReportTableMasterDao getTblReportTableMasterDao() {
        return tblReportTableMasterDao;
    }

    public void setTblReportTableMasterDao(TblReportTableMasterDao tblReportTableMasterDao) {
        this.tblReportTableMasterDao = tblReportTableMasterDao;
    }

    public TblTenderFormsDao getTblTenderFormsDao() {
        return tblTenderFormsDao;
    }

    public void setTblTenderFormsDao(TblTenderFormsDao tblTenderFormsDao) {
        this.tblTenderFormsDao = tblTenderFormsDao;
    }  
    
    public SPEvalCommonSearchData getsPEvalCommonSearchData() {
        return sPEvalCommonSearchData;
    }

    public void setsPEvalCommonSearchData(SPEvalCommonSearchData sPEvalCommonSearchData) {
        this.sPEvalCommonSearchData = sPEvalCommonSearchData;
    }
    
    /**
     * Creates PC Report
     * @param tblReportMaster  to be inserted
     * @param tblReportTableMaster to be inserted
     * @return reportId
     */
    public int createReport(TblReportMaster tblReportMaster, TblReportTableMaster tblReportTableMaster) {
        logger.debug("createReport : " + logUserId + " Starts");
        tblReportMasterDao.addTblReportMaster(tblReportMaster);
        tblReportTableMaster.setTblReportMaster(tblReportMaster);
        tblReportTableMasterDao.addTblReportTableMaster(tblReportTableMaster);
        logger.debug("createReport : " + logUserId + " Ends");
        return tblReportMaster.getReportId();
    }

    /**
     * Array of reportTableId,noOfCols,reportName,reportHeader,reportFooter,evalType
     * @param reportid search criteria
     * @return Array of reportTableId,noOfCols,reportName,reportHeader,reportFooter,evalType
     */
    public Object[] getReportTabIdNoOfCols(String reportid) {
        return hibernateQueryDao.createNewQuery("select trt.reportTableId,trt.noOfCols,trm.reportName,trm.reportHeader,trm.reportFooter,t.evalType from TblReportTableMaster trt,TblReportMaster trm,TblTenderDetails t where trt.tblReportMaster.reportId=trm.reportId  and t.tblTenderMaster.tenderId=trm.tblTenderMaster.tenderId and trm.reportId=" + reportid).get(0);
    }
    
    public Object[] getReportTabIdNoOfColsLots(String reportid,String PkgLotID) {
        return hibernateQueryDao.createNewQuery("select trt.reportTableId,trt.noOfCols,trm.reportName,trm.reportHeader,trm.reportFooter,t.evalType from TblReportTableMaster trt,TblReportMaster trm,TblTenderDetails t where trt.tblReportMaster.reportId=trm.reportId  and t.tblTenderMaster.tenderId=trm.tblTenderMaster.tenderId and trm.reportId=" + reportid + " and trt.pkgLotId=" + PkgLotID).get(0);
    }

    /**
     * Report Column Creation
     * @param list to be inserted
     */
    public void createReportColumns(List<TblReportColumnMaster> list) {
        tblReportColumnMasterDao.updateOrSaveReportColumn(list);
    }

    /**
     * Get Report Columns be reportId
     * @param reportMasterId from tbl_ReportMaster
     * @return List of TblReportColumnMaster
     * @throws Exception
     */
    public List<TblReportColumnMaster> getReportColumns(int reportMasterId) throws Exception {
        return tblReportColumnMasterDao.findTblReportColumnMaster("tblReportTableMaster", Operation_enum.EQ, new TblReportTableMaster(reportMasterId));
    }

    /**
     * Get Forms for Report
     * @param tenderid from tbl_TenderMaster
     * @param packageLotId from tbl_TenderLotSecurity
     * @return List of {tenderFormId,formName,evalType}
     */
    public List<Object[]> findFormsForReport(String tenderid, String packageLotId) {
        logger.debug("findFormsForReport : " + logUserId + " Starts");
        StringBuilder sb = new StringBuilder();
        sb.append("select tf.tenderFormId,tf.formName,td.evalType from TblTenderStd t,TblTenderSection ts,TblTenderForms tf,TblTenderDetails td where t.tenderStdId=ts.tblTenderStd.tenderStdId and ts.tenderSectionId=tf.tblTenderSection.tenderSectionId and td.tblTenderMaster.tenderId=t.tenderId and tf.isPriceBid='yes' and (tf.formStatus is null or tf.formStatus not in ('cp','c')) and t.tenderId=" + tenderid);
        if (packageLotId != null) {
            if (!hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderid).get(0).toString().equalsIgnoreCase("Services")) {
                sb.append(" and tf.pkgLotId=" + packageLotId);
            }
        }
        logger.debug("findFormsForReport : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(sb.toString());
    }

    /**
     * Dohatec ICT
     */
       public List<Object[]> findFormsForReportForTenderForms(String tenderid, String packageLotId) {
        logger.debug("findFormsForReport : " + logUserId + " Starts");
        StringBuilder sb = new StringBuilder();
        sb.append(" select tf.tenderFormId,tf.formName,td.evalType from TblTenderStd t,TblTenderSection ts,");
        sb.append(" TblTenderForms tf,TblTenderDetails td, TblTenderBidForm tbf ");
        sb.append(" where t.tenderStdId=ts.tblTenderStd.tenderStdId and ts.tenderSectionId=tf.tblTenderSection.tenderSectionId ");
        sb.append(" and td.tblTenderMaster.tenderId=t.tenderId and tf.isPriceBid='yes' ");
        sb.append(" and (tf.formStatus is null or tf.formStatus not in ('cp','c')) and t.tenderId=" + tenderid);
        if (packageLotId != null) {
            if (!hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderid).get(0).toString().equalsIgnoreCase("Services")) {
                sb.append(" and tf.pkgLotId=" + packageLotId);
            }
        }
        sb.append(" and tbf.tblTenderForms.tenderFormId = tf.tenderFormId ");
        sb.append(" and tbf.userId in ");
        sb.append(" ( select tnid.userId FROM TblNoaIssueDetails tnid ");
        sb.append(" where tnid.tblTenderMaster.tenderId = " + tenderid + " and tnid.pkgLotId = " +  packageLotId + ") ");
        logger.debug("findFormsForReport : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(sb.toString());
    }

    public void updateGovernCol(String repColId) {
        hibernateQueryDao.updateDeleteNewQuery("update TblReportColumnMaster set govCol='yes' where reportColId=" + repColId);
    }

    /**
     * Inserts Formula Created for the report
     * @param reportTableId on which formula is created
     * @param formula contains Formula
     * @param columnId on which formula is created
     * @param dispFormula Display for client
     */
    public void addRepColFormula(String reportTableId, String formula, String columnId, String dispFormula) {
        tblReportFormulaMasterDao.addTblReportFormulaMaster(new TblReportFormulaMaster(0, new TblReportTableMaster(Integer.parseInt(reportTableId)), formula, Integer.parseInt(columnId), dispFormula));
    }

    /**
     * Get Report Formula for the report
     * @param repTableId from tbl_ReportTableMaster
     * @return List of {reportFormulaId, .displayFormula, colHeader,reportColId}
     */
    public List<Object[]> getReportFormula(String repTableId) {
        return hibernateQueryDao.createNewQuery("select trm.reportFormulaId, trm.displayFormula, trcm.colHeader,trcm.reportColId from TblReportFormulaMaster trm,TblReportColumnMaster trcm where trm.columnId=trcm.columnId and trm.tblReportTableMaster.reportTableId=trcm.tblReportTableMaster.reportTableId and trcm.tblReportTableMaster.reportTableId=" + repTableId);
    }

    /**
     * Deletes Report Formula for the Report Table
     * @param reportFormulaId formula to be deleted
     * @param repColId report Column Id
     */
    public void delRepColFormula(String[] reportFormulaId, String[] repColId) {
        logger.debug("delRepColFormula : " + logUserId + " Starts");
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for (int i = 0; i < reportFormulaId.length; i++) {
            sb1.append(reportFormulaId[i] + ",");
            sb2.append(repColId[i] + ",");
        }
        hibernateQueryDao.updateDeleteNewQuery("update TblReportColumnMaster set govCol='no' where reportColId in (" + sb2.toString().substring(0, sb2.toString().length() - 1) + ")");
        hibernateQueryDao.updateDeleteNewQuery("delete from TblReportFormulaMaster trfm where trfm.reportFormulaId in (" + sb1.toString().substring(0, sb1.toString().length() - 1) + ")");
        sb1 = null;
        sb2 = null;
        logger.debug("delRepColFormula : " + logUserId + " Ends");
    }

    /**
     * Get Tender evalType
     * @param tenderid from tbl_TenderMaster
     * @return Tender Evaluation Type
     */
    public String getTenderEvalType(String tenderid) {
        return hibernateQueryDao.getSingleColQuery("select ttd.evalType from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId=" + tenderid).get(0).toString();
    }

     /**
     * Tender Forms on which Formula is to be created
     * @param repId reportId
     * @param formId formId(s)
     * @param isNot boolean
     */
    public void selectFormForRep(List<TblReportForms> reportFormses,boolean isNot) {
       if(isNot && (!reportFormses.isEmpty())){
            hibernateQueryDao.updateDeleteNewQuery("delete from TblReportForms where tblReportMaster.reportId="+reportFormses.get(0).getTblReportMaster().getReportId());
            hibernateQueryDao.updateDeleteNewQuery("delete from TblReportFormulaMaster where tblReportTableMaster.reportTableId in (select trm.reportTableId from TblReportTableMaster trm where trm.tblReportMaster.reportId="+reportFormses.get(0).getTblReportMaster().getReportId()+")");
       }
       tblReportFormsDao.updateOrSaveAllRepForms(reportFormses);
    }

    /**
     * Get Forms from tbl_ReportForms for Reports
     * @param repId from tbl_ReportMaster
     * @return List of {tenderFormId}
     */
    public List<Object> getFormsForReport(String repId) {
        return hibernateQueryDao.singleColQuery("select trf.tenderFormId from TblReportForms trf where trf.tblReportMaster.reportId=" + repId);
    }

    /**
     * Create Formula for Report in case of Item wise
     * @param repId from tbl_ReportMaster
     * @param repForm name of the form
     * @param repHead report Header(s)
     */
    public void createItemWiseFormula(String repId, String repForm, List<String> repHeadIds, List<String> repHeadNames) {
        logger.debug("createItemWiseFormula : " + logUserId + " Starts");
        Object obj[] = hibernateQueryDao.createNewQuery("select trtm.reportTableId,trcm.columnId from TblReportTableMaster trtm,TblReportMaster trm ,TblReportColumnMaster trcm where trm.reportId=trtm.tblReportMaster.reportId and trtm.reportTableId=trcm.tblReportTableMaster.reportTableId and trcm.govCol='yes' and trm.reportId=" + repId).get(0);
        List<TblReportFormulaMaster> list = new ArrayList<TblReportFormulaMaster>();
        for (int i = 0; i < repHeadIds.size(); i++) {
            list.add(new TblReportFormulaMaster(0, new TblReportTableMaster(Integer.parseInt(obj[0].toString())), repHeadIds.get(i), 0, repHeadNames.get(i)));
        }
        if (repForm != null) {
            list.add(new TblReportFormulaMaster(0, new TblReportTableMaster(Integer.parseInt(obj[0].toString())), repForm.split("@#")[1], Integer.parseInt(obj[1].toString()), repForm.split("@#")[0]));
        }
        tblReportFormulaMasterDao.updateOrSaveAllRepFormula(list);
        list = null;
        obj = null;
        logger.debug("createItemWiseFormula : " + logUserId + " Ends");
    }

     /**
     * Delete Report Column Formula
     * @param reportFormulaId to be deleted(s)
     */
    public void delRepColFormula(String[] reportFormulaId) {
        logger.debug("delRepColFormula : " + logUserId + " Starts");
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < reportFormulaId.length; i++) {
            sb1.append(reportFormulaId[i] + ",");
        }
        hibernateQueryDao.updateDeleteNewQuery("delete from TblReportFormulaMaster trfm where trfm.reportFormulaId in (" + sb1.toString().substring(0, sb1.toString().length() - 1) + ")");
        sb1 = null;
        logger.debug("delRepColFormula : " + logUserId + " Ends");
    }

    /**
     * Get reportTable Id from reportId
     * @param repId from tbl_ReportMaster
     * @return reportTableId
     */
    public String getReportTableId(String repId) {
        return hibernateQueryDao.singleColQuery("select trtm.reportTableId from TblReportTableMaster trtm,TblReportMaster trm where trm.reportId=trtm.tblReportMaster.reportId and trm.reportId=" + repId).get(0).toString();
    }
    
    public String getReportTableIdLot(String repId,String pkglotId) {
        return hibernateQueryDao.singleColQuery("select trtm.reportTableId from TblReportTableMaster trtm,TblReportMaster trm where trm.reportId=trtm.tblReportMaster.reportId and trtm.pkgLotId ="+ pkglotId +"and trm.reportId=" + repId).get(0).toString();
    }

    /**
     * List of Formula Created for ItemWise
     * @param repTableId from tbl_ReportTableMaster
     * @return List of TblReportFormulaMaster
     * @throws Exception
     */
    public List<TblReportFormulaMaster> getRepFormulaItemWise(int repTableId) throws Exception {
        return tblReportFormulaMasterDao.findTblReportFormulaMaster("tblReportTableMaster", Operation_enum.EQ, new TblReportTableMaster(repTableId));
    }

    /**
     * List of Formula Created for ItemWise
     * @param repTableId from tbl_ReportTableMaster
     * @return List of TblReportFormulaMaster
     * @throws Exception
     */
    public List<Object[]> findLotPkgForReport(String tenderid) {
        return hibernateQueryDao.createNewQuery("select ls.appPkgLotId,ls.lotNo,ls.lotDesc from TblTenderDetails tm,TblTenderLotSecurity ls where tm.tblTenderMaster.tenderId=ls.tblTenderMaster.tenderId and tm.tblTenderMaster.tenderId=" + tenderid);
    }

    /**
     * Get Lot selected for the report
     * @param tenderid from tbl_TenderMaster
     * @param repId from tbl_ReportMaster
     * @return List of {appPkgLotId,lotNo,lotDesc}
     */
    public List<Object[]> findLotPkgForReport(String tenderid,String repId) {//Not showing lot whose TER,TOR are made(Taher)
        StringBuilder query = new StringBuilder();
        query.append("select ls.appPkgLotId,ls.lotNo,ls.lotDesc ");
        query.append("from TblTenderDetails tm,TblTenderLotSecurity ls ");
        query.append("where tm.tblTenderMaster.tenderId=ls.tblTenderMaster.tenderId and tm.tblTenderMaster.tenderId=" + tenderid);
        query.append(" and ls.appPkgLotId not in (select trl.pkgLotId from TblReportLots trl,TblReportMaster trm where trl.reportId=trm.reportId and ");
        query.append("trm.reportId="+repId+" and trm.isTorter ='TOR' and trm.isTorter ='TER')");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    public List<EvalCommonSearchData> getBiddersForReport(String tenderId, String reportId) {
        return sPEvalCommonSearchData.executeProcedure("getFinalUserIdForReport", tenderId, reportId,null,null,null,null,null,null,null,null,null,null,null,null);
    }

    public List<SPTenderCommonData> getBiddersForReport(String tenderId) {
        return sPTenderCommon.executeProcedure("getNegotiatedFinalBidder", tenderId, null);
    }

    public List<EvalCommonSearchData> getAllBiddersForReport(String tenderId, String reportId) {
        return sPEvalCommonSearchData.executeProcedure("getAllBidders", tenderId, reportId,null,null,null,null,null,null,null,null,null,null,null,null);
    }

    public List<EvalCommonSearchData> getAllBiddersForTORReport(String tenderId, String reportId) {
        return sPEvalCommonSearchData.executeProcedure("getAllBiddersForTOR", tenderId, reportId,null,null,null,null,null,null,null,null,null,null,null,null);    
    }
    
    public List<EvalCommonSearchData> getBiddersForReport(String tenderId, String reportId,String pkgLotId) {
        return sPEvalCommonSearchData.executeProcedure("getFinalUserIdForReportLOT", tenderId, reportId,pkgLotId,null,null,null,null,null,null,null,null,null,null,null);  
    }
    public List<EvalCommonSearchData> getAllBiddersForReport(String tenderId, String reportId,String pkgLotId) {
         return sPEvalCommonSearchData.executeProcedure("getAllBiddersLot", tenderId, reportId,pkgLotId,null,null,null,null,null,null,null,null,null,null,null);
    }
    public List<EvalCommonSearchData> getAllBiddersForTORReport(String tenderId, String reportId,String pkgLotId) {
        return sPEvalCommonSearchData.executeProcedure("getAllBiddersForTORLot", tenderId, reportId,pkgLotId,null,null,null,null,null,null,null,null,null,null,null);
    }

    /**
     * Fetch Auto Column Data other than gov column
     * @param repTabId from tbl_ReportColumnMaster
     * @param autoCols auto col id
     * @param userId bidder userId
     * @return AutoCol Data for the bidder
     */
    public String getBidderDataForReport(int repTabId, int columnId, int userId) {
        return sPGenerateReport.exeProcedure(repTabId, columnId, userId);
    }

    public String getBidderDataForReportNeg(int repTabId, int columnId, int userId) {
        return sPGenerateNegReport.exeProcedure(repTabId, columnId, userId);
    }

    /**
     * Type of report TER or TOR
     * @param reportId from tbl_ReportMaster
     * @return report Type
     */
    public String getReportType(String reportId) {
        return hibernateQueryDao.getSingleColQuery("select trm.reportType from TblReportMaster trm where trm.reportId=" + reportId).get(0).toString();
    }

    /**
     * Lot Id for the report and tender
     * @param tenderid from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @return lotId
     */
    public String getPkgLotId(String tenderid, String reportId) {
        //return hibernateQueryDao.getSingleColQuery("select distinct std.packageLotId from TblReportForms r,TblTenderForms tf,TblTenderSection ts,TblTenderStd std where r.tenderFormId=tf.tenderFormId  and tf.tblTenderSection.tenderSectionId=ts.tenderSectionId and std.tenderStdId=ts.tblTenderStd.tenderStdId and std.tenderId=" + tenderid + " and r.tblReportMaster.reportId=" + reportId).get(0).toString();
        List<Object> list = hibernateQueryDao.getSingleColQuery("select rl.pkgLotId from TblReportLots rl where rl.reportId=" + reportId);
        String data = null;
        if(list.isEmpty()){
            data = "0";
        }else{
            data = list.get(0).toString();
        }
        return data;
    }

    /**
     * Save PC Report
     * @param roundMaster for the current round
     * @param list1 to be saved TblBidderRank
     * @param list2 to be saved TblBidRankDetail
     * @param noOfCols no of columns
     */
    public void saveReportData(TblEvalRoundMaster roundMaster,List<TblBidderRank> list1, List<TblBidRankDetail> list2, int noOfCols) {
        logger.debug("saveReportData : " + logUserId + " Starts");

         List<Object> lst = hibernateQueryDao.singleColQuery("select count(roundId) from TblEvalRoundMaster where tenderId = "+ roundMaster.getTenderId() +" and userId = "+roundMaster.getUserId()+" and reportType = '"+roundMaster.getReportType()+"' and evalCount = "+roundMaster.getEvalCount()+" and pkgLotId = "+roundMaster.getPkgLotId()+"");
         if(lst.get(0).toString().equals("0")){

          if(roundMaster!=null){
                           tblEvalRoundMasterDao.addTblEvalRoundMaster(roundMaster);
                for (int i = 0; i < list1.size(); i++) {
                    list1.get(i).setRoundId(roundMaster.getRoundId());
                }
            }
        
        tblBidderRankDao.updateOrSaveBidderRank(list1);
        int j = 0;
        for (TblBidderRank tbr : list1) {
            for (int i = 0; i < noOfCols; i++) {
                list2.get(j).setTblBidderRank(tbr);
                j++;
            }
        }
        tblBidRankDetailDao.updateOrSaveBidRankDetail(list2);
        }
        logger.debug("saveReportData : " + logUserId + " Ends");
    }
/***Added By Dohatec To Restrict Multiple Entry for LTM Start***/
    /**
     * Save PC Report
     * @param roundMaster for the current round
     * @param list1 to be saved TblBidderRank
     * @param list2 to be saved TblBidRankDetail
     * @param noOfCols no of columns
     * @param isLottery to check Lottery Or Not
     */
    public void saveReportData(TblEvalRoundMaster roundMaster,List<TblBidderRank> list1, List<TblBidRankDetail> list2, int noOfCols,boolean isLottery) {
        logger.debug("saveReportData LTM Goods Works : " + logUserId + " Starts");
        if(isLottery){
            List<Object> lst = hibernateQueryDao.singleColQuery("select count(roundId) from TblEvalRoundMaster where tenderId = "+ roundMaster.getTenderId()+" and evalCount = "+roundMaster.getEvalCount()+" and pkgLotId = "+roundMaster.getPkgLotId());
         if(lst.get(0).toString().equals("0")){
        if(roundMaster!=null){
            tblEvalRoundMasterDao.addTblEvalRoundMaster(roundMaster);
            for (int i = 0; i < list1.size(); i++) {
                list1.get(i).setRoundId(roundMaster.getRoundId());
            }
        }
        tblBidderRankDao.updateOrSaveBidderRank(list1);
        int j = 0;
        for (TblBidderRank tbr : list1) {
            for (int i = 0; i < noOfCols; i++) {
                list2.get(j).setTblBidderRank(tbr);
                j++;
            }
        }
        tblBidRankDetailDao.updateOrSaveBidRankDetail(list2);
            }
        }
        else {
            List<Object> lst = hibernateQueryDao.singleColQuery("select count(roundId) from TblEvalRoundMaster where tenderId = "+ roundMaster.getTenderId() +" and userId = "+roundMaster.getUserId()+" and reportType = '"+roundMaster.getReportType()+"' and evalCount = "+roundMaster.getEvalCount()+" and pkgLotId = "+roundMaster.getPkgLotId()+"");
         if(lst.get(0).toString().equals("0")){
        if(roundMaster!=null){
            tblEvalRoundMasterDao.addTblEvalRoundMaster(roundMaster);
            for (int i = 0; i < list1.size(); i++) {
                list1.get(i).setRoundId(roundMaster.getRoundId());
            }
        }
        tblBidderRankDao.updateOrSaveBidderRank(list1);
        int j = 0;
        for (TblBidderRank tbr : list1) {
            for (int i = 0; i < noOfCols; i++) {
                list2.get(j).setTblBidderRank(tbr);
                j++;
            }
        }
        tblBidRankDetailDao.updateOrSaveBidRankDetail(list2);
            }
        }
        logger.debug("saveReportData  LTM Goods Works : " + logUserId + " Ends");
    }
    /***Added By Dohatec To Restrict Multiple Entry for LTM Start***/

    /**
     * Is PC report save
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @return 1 or 0 for saved or not
     * @throws Exception
     */
    public long isReportSaved(String tenderId, String reportId) throws Exception {
        return hibernateQueryDao.countForNewQuery("TblBidderRank tbr", "tbr.reportId=" + reportId + " and tbr.tblTenderMaster.tenderId=" + tenderId);
    }

    /**
     * Is report saved roundwise
     * @param roundId
     * @return 1 or 0 for saved or not
     * @throws Exception
     */
    public long isReportRoundSaved(String roundId) throws Exception {
        return hibernateQueryDao.countForNewQuery("TblEvalRoundMaster ter", "ter.roundId=" + roundId);
    }

    /**
     *
     * @param tenderId
     * @param reportId
     * @param roundId
     * @return
     * @throws Exception
     */
    public List<TblBidderRank> findBidderRank(String tenderId, String reportId,String roundId) throws Exception {
        List<TblBidderRank> data = new ArrayList<TblBidderRank>();
        List<Object> list = hibernateQueryDao.singleColQuery("select userId from TblEvalRoundMaster rm where rm.roundId="+roundId);
        int winner = 0;
        if(!list.isEmpty()){
           winner = Integer.parseInt(list.get(0).toString());
        }
        data.addAll(tblBidderRankDao.findTblBidderRank("reportId", Operation_enum.EQ, Integer.parseInt(reportId), "tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderId)),"roundId",Operation_enum.EQ,Integer.parseInt(roundId),"userId",Operation_enum.EQ,winner));
        data.addAll(tblBidderRankDao.findTblBidderRank("reportId", Operation_enum.EQ, Integer.parseInt(reportId), "tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderId)),"roundId",Operation_enum.EQ,Integer.parseInt(roundId),"userId",Operation_enum.NE,winner));
        return data;
    }

    public List<TblBidRankDetail> findBidRankDetail(int bidderRankId) throws Exception {
        return tblBidRankDetailDao.findTblBidRankDetail("tblBidderRank", Operation_enum.EQ, new TblBidderRank(bidderRankId));
    }

    /**
     * Get Item wise Report Data
     * @param repId from tbl_ReportMaster
     * @return List of {cellvalue,rowId,enderTableId}
     */
    public List<Object[]> getItemWiseReportHeader(String repId) {
        logger.debug("getItemWiseReportHeader : " + logUserId + " Starts");
        String tableId = null;
        String colIds = "";
        String reportTableId = getReportTableId(repId);
        List<Object> list = hibernateQueryDao.singleColQuery("select tfm.formula from TblReportFormulaMaster tfm where tfm.tblReportTableMaster.reportTableId=" + reportTableId + " and tfm.columnId=0");
        tableId = list.get(0).toString().substring(0, list.get(0).toString().indexOf("_"));
        for (Object object : list) {
            colIds += object.toString().substring(object.toString().indexOf("_") + 1, object.toString().length()) + ",";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("select ttc.cellvalue,ttc.rowId,ttc.tblTenderTables.tenderTableId from TblTenderCells ttc "
                + "where ttc.tblTenderTables.tenderTableId = " + tableId + " and ttc.columnId in (" + colIds.substring(0, colIds.length() - 1) + ") order by ttc.rowId");
        logger.debug("getItemWiseReportHeader : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(sb.toString());
    }

    /**
     * Get Autocolumn Data in case of ItemWise PCR
     * @param repId from tbl_ReportMaster
     * @param rowId
     * @return List of {cellValue,userId,companyName,firstName,lastName}
     */
    public List<Object[]> getCellValueItemWiseReport(String repId, String rowId) {
        logger.debug("getCellValueItemWiseReport : " + logUserId + " Starts");
        String tab_colId = hibernateQueryDao.singleColQuery("select tfm.formula from TblReportFormulaMaster tfm where tfm.tblReportTableMaster.reportTableId=" + getReportTableId(repId) + " and tfm.columnId!=0").get(0).toString();
        StringBuilder sb = new StringBuilder();
        sb.append("select t.cellValue,tm.tblLoginMaster.userId,cm.companyName,tm.firstName,tm.lastName ");
        sb.append("from TblTenderBidPlainData t,TblTenderBidTable tbt,TblTenderBidForm bf,"
                + "TblTendererMaster tm,TblCompanyMaster cm ");
        sb.append("where t.tenderTableId=" + tab_colId.substring(0, tab_colId.indexOf("_")) + " "
                + "and t.tenderColId=" + tab_colId.substring(tab_colId.indexOf("_") + 1, tab_colId.length()) + " "
                + "and tbt.bidTableId=t.tblTenderBidTable.bidTableId and "
                + "bf.bidId=tbt.tblTenderBidForm.bidId and tm.tblLoginMaster.userId=bf.userId "
                + "and tm.tblCompanyMaster.companyId=cm.companyId "
                + "and t.rowId=" + rowId + " order by convert(numeric,t.cellValue)");
        if (getReportType(repId).equals("l1")) {
             sb.append(" asc");
        } else {
            sb.append(" dsc");
        }
        logger.debug("getCellValueItemWiseReport : " + logUserId + " Ends");
        return hibernateQueryDao.createNewQuery(sb.toString());
    }

    public void saveItemWiseReport(List<TblBidderRank> list) {
        tblBidderRankDao.updateOrSaveBidderRank(list);
    }

    /**
     * Get Saved Itemwise Report
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @return List of TblBidderRank
     * @throws Exception
     */
    public List<TblBidderRank> getSavedItemReport(String tenderId, String reportId) throws Exception {
        return tblBidderRankDao.findTblBidderRank("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderId)), "reportId", Operation_enum.EQ, Integer.parseInt(reportId));
    }

    /**
     * Count For Item Display
     * @param repId from tbl_ReportMaster
     * @return count
     * @throws Exception
     */
    public long getCountForItemDisplay(String repId) throws Exception {
        return hibernateQueryDao.countForNewQuery("TblReportFormulaMaster tfm", "tfm.tblReportTableMaster.reportTableId=" + getReportTableId(repId) + " and tfm.columnId=0");
    }

    /**
     *List of {reportId,reportName,evalType,isTorter}
     * @param tenderid from tbl_TenderMaster
     * @param docAvlMethod from tbl_TenderMaster
     * @return List of {reportId,reportName,evalType,isTorter}
     */
    public List<Object[]> getRepNameId(String tenderid,String docAvlMethod) {
        List<Object[]> list = null;
        StringBuilder query = new StringBuilder();        
        if(docAvlMethod.equalsIgnoreCase("Lot")){
            query.append("select trm.reportId,trm.reportName,t.evalType,trm.isTorter ");
            query.append("from TblReportMaster trm,TblTenderDetails t ");
            query.append("where t.tblTenderMaster.tenderId=trm.tblTenderMaster.tenderId and trm.tblTenderMaster.tenderId=" + tenderid+" and ");
            query.append("trm.reportId not in (select trl.reportId from TblReportLots trl,TblTenderLotSecurity tls  where trl.pkgLotId=tls.appPkgLotId and tls.tblTenderMaster.tenderId="+tenderid+")");
            list = hibernateQueryDao.createNewQuery(query.toString());
            query.delete(0, query.length());
        }else{
            query.append("select trm.reportId,trm.reportName,t.evalType,trm.isTorter from TblReportMaster trm,TblTenderDetails t where t.tblTenderMaster.tenderId="+tenderid+" and trm.tblTenderMaster.tenderId=" + tenderid);
            list = hibernateQueryDao.createNewQuery(query.toString());
        }
        query=null;
        return list;
    }

    /**
     *List of {reportId,reportName,evalType,isTorter }
     * @param tenderid from tbl_TenderMaster
     * @return List of {reportId,reportName,evalType,isTorter }
     */
    public List<Object[]> getRepNameId(String tenderid){
        List<Object[]> list = null;
        StringBuilder query = new StringBuilder();        
        query.append("select trm.reportId,trm.reportName,t.evalType,trm.isTorter from TblReportMaster trm,TblTenderDetails t where t.tblTenderMaster.tenderId="+tenderid+" and trm.tblTenderMaster.tenderId=" + tenderid);
        list = hibernateQueryDao.createNewQuery(query.toString());
        query=null;
        return list;
    }

    /**
     * List of {reportId,reportName,evalType,isTorter,lotNo,lotDesc,appPkgLotId }
     * @param tenderid from tbl_TenderMaster
     * @return  List of {reportId,reportName,evalType,isTorter,lotNo,lotDesc,appPkgLotId }
     */
    public List<Object[]> getRepNameLotWise(String tenderid) {
        List<Object[]> list = null;
        StringBuilder query = new StringBuilder();
        /*query.append("select trm.reportId,trm.reportName,t.evalType,trm.isTorter ");
        query.append("from TblReportMaster trm,TblTenderDetails t ");
        query.append("where t.tblTenderMaster.tenderId=trm.tblTenderMaster.tenderId and trm.tblTenderMaster.tenderId=" + tenderid+" and ");
        query.append("trm.reportId in (select trl.reportId from TblReportLots trl,TblTenderLotSecurity tls  where trl.pkgLotId=tls.appPkgLotId and tls.tblTenderMaster.tenderId="+tenderid+")");
        */
        query.append("select trm.reportId,trm.reportName,t.evalType,trm.isTorter,tls.lotNo,tls.lotDesc,tls.appPkgLotId ");
        query.append("from TblReportMaster trm,TblTenderDetails t,TblReportLots trl,TblTenderLotSecurity tls  ");
        query.append("where t.tblTenderMaster.tenderId=trm.tblTenderMaster.tenderId and trm.tblTenderMaster.tenderId=" + tenderid+" and ");
        query.append("trm.reportId = trl.reportId and trl.pkgLotId=tls.appPkgLotId and tls.tblTenderMaster.tenderId=t.tblTenderMaster.tenderId");
        list = hibernateQueryDao.createNewQuery(query.toString());
        return list;
    }

    public boolean deleteWholeReport(String reportId) {
        logger.debug("deleteWholeReport : " + logUserId + " Starts");
        boolean bSuccess = false;
        List<SPTenderCommonData> list = sPTenderCommon.executeProcedure("deleteReports", reportId, null);
        if (!list.isEmpty()) {
            if (list.get(0).getFieldName1().equals("true")) {
                bSuccess = true;
            } else {
               logger.debug("Error deleteWholeReport : " + list.get(0).getFieldName2());
                bSuccess = false;
            }
        } else {
            logger.debug("Error deleteWholeReport : " + list.get(0).getFieldName2());
            bSuccess = false;
        }
        logger.debug("deleteWholeReport : " + logUserId + " Ends");
        return bSuccess;
    }

    public List<SPCommonSearchDataMore> getOpeningReportData(String funName, String tenderid,String lotId,String type) {
        return commonSearchDataMoreService.getCommonSearchData(funName, tenderid, lotId, type, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    //Edit by Palash, Dohatec for TER4 Report
    public List<SPCommonSearchDataMore> getreTenderRecommendetion(String funName, String tenderid) {
        return commonSearchDataMoreService.getCommonSearchData(funName, tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
   }
    // End
     /**
     * Inserts Lot information for report
     * @param reportLots to be inserted
     * @param isNot flag
     * @return true or false for success or fail
     */
    public boolean insertReportLots(TblReportLots reportLotses,boolean isNot){
        logger.debug("insertReportLots : " + logUserId + " Starts");
        int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblReportLots where reportId="+reportLotses.getReportId());
        //+" and pkgLotId="+reportLotses.getPkgLotId()
        logger.debug(i+" Rows Deleted from TblReportLots");
        if(isNot){
            hibernateQueryDao.updateDeleteNewQuery("delete from TblReportForms where tblReportMaster.reportId="+reportLotses.getReportId());
            hibernateQueryDao.updateDeleteNewQuery("delete from TblReportFormulaMaster where tblReportTableMaster.reportTableId in (select trm.reportTableId from TblReportTableMaster trm where trm.tblReportMaster.reportId="+reportLotses.getReportId()+")");
        }
        boolean flag = false;
        try {
            tblReportLotsDao.addTblReportLots(reportLotses);
            flag = true;
        } catch (Exception e) {
            logger.error("insertReportLots : " + logUserId,e);
            flag = true;
        }
        logger.debug("insertReportLots : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Get reportId for TER or TOR
     * @param tenderId from tbl_TenderMaster 
     * @param lotId from tbl_TenderLotSecurity
     * @param isTorter containing TER or TOR
     * @return reportId
     */
    public Object findRepIdofTOR(String tenderId,String lotId,String isTorter){
        StringBuilder query = new StringBuilder();
        List<Object> list=null;
        Object repId=null;
        if(!lotId.equals("0")){
            query.append("select trm.reportId from TblReportMaster trm,TblReportLots trl ");
            query.append("where trm.reportId=trl.reportId and trm.isTorter='"+isTorter+"' and trm.tblTenderMaster.tenderId="+tenderId+" and trl.pkgLotId="+lotId);
            }else{
            query.append("select trm.reportId from TblReportMaster trm where trm.isTorter='"+isTorter+"' and trm.tblTenderMaster.tenderId="+tenderId);
            }
        list = hibernateQueryDao.singleColQuery(query.toString());
        if(list!=null && !list.isEmpty()){
            repId = list.get(0);
        }
        return repId;
    }

    /**
     * Get FinalSubmission User for the Tender
     * @param tenderId from tbl_TenderMaster
     * @return List of {userId,companyId,companyName,firstName,lastName}
     */
    public List<Object[]> getFinalSubmissionUser(String tenderId){
        StringBuilder query = new StringBuilder();
        query.append("select tfs.userId,tcm.companyId,tcm.companyName,ttm.firstName,ttm.lastName from TblFinalSubmission  tfs,TblTendererMaster ttm,TblCompanyMaster tcm ");
        query.append("where tfs.tblTenderMaster.tenderId="+tenderId+" and tfs.bidSubStatus='finalsubmission' and ttm.tblLoginMaster.userId=tfs.userId ");
        query.append("and ttm.tblCompanyMaster.companyId = tcm.companyId");
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    /**
     * Get TER1,2 configuration criteria
     * @param reportType TER1 or TER2
     * @param pNature  from tbl_TenderMaster
     * @return List of {criteriaId,criteria}
     */
    public List<Object[]> getTERCriteria(String reportType,String pNature){
        StringBuilder query = new StringBuilder();
        query.append("select ter.criteriaId,ter.criteria from TblEvalTerreportCriteria ter ");
        query.append("where ter.reportType='"+reportType+"'");
        if(pNature.equals("Goods")){
            query.append(" and ter.isAppInGoods='Yes'");
        }else{
            query.append(" and ter.isAppInWorks='Yes'");
        }
        return hibernateQueryDao.createNewQuery(query.toString());
    }

    /**
     * Insert Configuration for TER1 or 2
     * @param list to be inserted
     * @return true or false for success or fail
     */
    public boolean evalTERReportIns(List<TblEvalTerreport> list){
        logger.debug("evalTERReportIns : "+logUserId+" : Starts");
        boolean flag=false;
        try {
            if(!list.isEmpty()){
                hibernateQueryDao.updateDeleteNewQuery("delete from TblEvalTerreport where tenderId="+list.get(0).getTenderId()+" and pkgLotId="+list.get(0).getPkgLotId()+" and evalType='"+list.get(0).getEvalType()+"' and reportType='"+list.get(0).getReportType()+"' and createdBy="+list.get(0).getCreatedBy()+" and roundId="+list.get(0).getRoundId()+" and evalCount="+list.get(0).getEvalCount());
            }
            tblEvalTerreportDao.updateInsAllEvalTerreport(list);
            flag = true;
        } catch (Exception e) {
            logger.error("evalTERReportIns : "+logUserId+" : ",e);
        }
        logger.debug("evalTERReportIns : "+logUserId+" : Ends");
        return flag;
    }

    /**
     * Update PC Report Name
     * @param repId from tbl_ReportMaster
     * @param repName to be updated
     * @return true or false for success or fail
     */
    public boolean updateRepName(String repId,String repName){
        logger.debug("updateRepName : "+logUserId+" : Starts");
        boolean flag=false;
        try {
           if(hibernateQueryDao.updateDeleteNewQuery("update TblReportMaster set reportName='"+repName+"' where reportId="+repId)!=0){
               flag=true;
           }
        } catch (Exception e) {
            logger.error("updateRepName : "+logUserId,e);
        }
        logger.debug("updateRepName : "+logUserId+" : Ends");
        return flag;
    }

    /**
     * Delete PC Report Formula
     * @param repTabId
     */
    public void delRepFormula(String repTabId){
        //(select trm.reportTableId from TblReportTableMaster trm where trm.tblReportMaster.reportId="+repId+")
        hibernateQueryDao.updateDeleteNewQuery("delete from TblReportFormulaMaster where tblReportTableMaster.reportTableId = "+repTabId);
    }

    public Object getReportLots(String repId){
        logger.debug("getLotForRpt : "+logUserId+" : Starts");
        Object data =null;
        try {
            List<Object> list = hibernateQueryDao.getSingleColQuery("select trl.pkgLotId from TblReportLots trl where trl.reportId="+repId);
            if (!list.isEmpty()) {
                data = list.get(0);
            }
            list=null;
        } catch (Exception e) {
            logger.error("getLotForRpt : "+logUserId,e);
        }
        logger.debug("getLotForRpt : "+logUserId+" : Ends");
        return data;
    }

    public List<SPCommonSearchDataMore> getReportFormDetail(String tenderId,String repId){
        return commonSearchDataMoreService.getCommonSearchData("getReportFormDetail", tenderId, repId);
    }

    /**
     * Lotwise TOR or TER count
     * @param pkgLotId  from tbl_TenderLotSecurity
     * @param stat eval or reeval
     * @param repId from tbl_ReportMaster
     * @return count
     */
    public long lotRptTERTORCnt(String pkgLotId,String stat,String repId){
        logger.debug("lotRptTERTORCnt : "+logUserId+" : Starts");
        long cnt=0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblReportLots trl,TblReportMaster trm", "trl.reportId=trm.reportId and trl.pkgLotId="+pkgLotId+" and trm.isTorter in (select trm.isTorter from TblReportMaster trm where trm.reportId="+repId+")");
        } catch (Exception ex) {
            logger.error("lotRptTERTORCnt : "+logUserId,ex);
        }
        logger.debug("lotRptTERTORCnt : "+logUserId+" : Ends");
        return cnt;
    }

    /**
     *Committee Approval Count
     * @param tenderId from tbl_TenderMaster
     * @param comType committee type (TEC(PEC),TOC(POC))
     * @return
     */
    public long getCommitteeAppCount(String tenderId,String comType){
        logger.debug("getCommitteeAppCount : "+logUserId+" : Starts");
        long cnt=0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblCommittee c,TblCommitteeMembers cm", "c.tblTenderMaster.tenderId=" + tenderId + " and c.committeeId = cm.tblCommittee.committeeId and c.committeeType in (" + comType + ") and cm.appStatus='approved' and c.committeStatus='approved'");
        } catch (Exception ex) {
            logger.error("getCommitteeAppCount : "+logUserId,ex);
        }
        logger.debug("getCommitteeAppCount : "+logUserId+" : Ends");
        return cnt;
    }

    /**
     *Check JVCA Subcontract
     * @param tenderId from tbl_TenderMaster
     * @param userId bidder userId
     * @return In [0] {jvyes,jvno} and In [1] {subyes,subno}
     */
    public String[]  jvSubContractChk(String tenderId,String userId){
        //JVCA(jvno,jvyes),SubContractor(subno,subyes)
        logger.debug("jvSubContractChk : "+logUserId+" : Starts");
        String[] data = new String[2];
        StringBuilder query = new StringBuilder();
        try {
            query.append("select tlm.isJvca from TblLoginMaster tlm where tlm.userId="+userId);
            List<Object> list = hibernateQueryDao.singleColQuery(query.toString());
            if((!list.isEmpty()) && list.get(0).toString().equalsIgnoreCase("yes")){
                data[0] = "jvyes";
            }else{
                data[0]="jvno";
            }
            list=null;
            query.delete(0, query.length());
            if(hibernateQueryDao.countForNewQuery("TblSubContracting tsc ", "tsc.invFromUserId="+userId+" and tsc.tblTenderMaster.tenderId="+tenderId+" and tsc.invAcceptStatus='Approved'")!=0){
                data[1]="subyes";
            }else{
                data[1]="subno";
            }
            query=null;
        } catch (Exception e) {
            logger.error("jvSubContractChk : "+logUserId,e);
        }
        logger.debug("jvSubContractChk : "+logUserId+" : Ends");
        return data;
    }

    /**
     *Get CompanyName
     * @param invFromUserId bidder who invites
     * @param tenderId  from tbl_TenderMaster
     * @return LIst of {userId,tendererId,companyId,companyName,firstName,lastName}
     */
    public List<Object[]> getCompanyName(String invFromUserId,String tenderId){
        StringBuilder query = new StringBuilder();
        query.append("select ttm.tblLoginMaster.userId,ttm.tendererId,tcm.companyId,tcm.companyName,ttm.firstName,ttm.lastName ");
        query.append("from TblCompanyMaster tcm,TblTendererMaster ttm ");
        query.append("where tcm.companyId=ttm.tblCompanyMaster.companyId and ttm.tblLoginMaster.userId in ");
        query.append("(select tsc.invToUserId from TblSubContracting tsc where tsc.invFromUserId="+invFromUserId+" and tsc.tblTenderMaster.tenderId="+tenderId+" and tsc.invAcceptStatus='Approved')");
        List<Object[]> list = hibernateQueryDao.createNewQuery(query.toString());
        return list;
    }

    /**
     * Check Price Bid
     * @param tenderId from tbl_TenderMaster
     * @param lotId from tbl_TenderLotSecurity
     * @return price bid count
     */
    public long checkPriceBidMade(String tenderId,String lotId){
        logger.debug("checkPriceBidMade : "+logUserId+" : Starts");
        long cnt = 0;
        try {
            if(!hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId.trim()).get(0).toString().equalsIgnoreCase("Services")){
                cnt = hibernateQueryDao.countForNewQuery("TblReportMaster rm,TblReportTableMaster rtm ,TblReportFormulaMaster rfm,TblReportLots rl", "rfm.tblReportTableMaster.reportTableId = rtm.reportTableId and rtm.tblReportMaster.reportId=rm.reportId and rm.reportId=rl.reportId and rm.tblTenderMaster.tenderId="+tenderId+" and rl.pkgLotId="+lotId);
            }else{
                cnt = hibernateQueryDao.countForNewQuery("TblReportMaster rm,TblReportTableMaster rtm ,TblReportFormulaMaster rfm", "rfm.tblReportTableMaster.reportTableId = rtm.reportTableId and rtm.tblReportMaster.reportId=rm.reportId and rm.tblTenderMaster.tenderId="+tenderId);
            }
        } catch (Exception e) {
            logger.error("checkPriceBidMade : "+logUserId,e);
        }
        logger.debug("checkPriceBidMade : "+logUserId+" : Ends");
        return cnt;
    }

    /**
     * Get PC report Estimated Cost
     * @param tenderId from tbl_TenderMaster
     * @param repId from tbl_ReportMaster
     * @return estimated cost
     */
    public String getCREstCost(String tenderId,String repId){
        StringBuilder query = new StringBuilder();
        String data = "0.00";
        String procNature = hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId).get(0).toString();
        query.append("select es.estCost from TblTenderEstCost es where ");
        if(!procNature.equals("Services")){
            query.append("es.pkgLotId in (select rl.pkgLotId from TblReportLots rl where rl.reportId="+repId+") and ");
        }
        query.append("es.tblTenderMaster.tenderId="+tenderId);
        List<Object> list = hibernateQueryDao.singleColQuery(query.toString());
        if(!list.isEmpty()){
            data = new BigDecimal(list.get(0).toString()).setScale(2, 0).toString();
        }
        return data;        
    }

    /**
     * Get Bidder Status
     * @param userId
     * @param tenderId
     * @param pkgLotId
     * @return
     */
    public String getBidderStatus(String userId,String tenderId,String pkgLotId,int evalCount){
        logger.debug("getBidderStatus : "+logUserId+" : Starts");
        String bidStat="-";
        try {
            StringBuilder query = new StringBuilder();
            if(hibernateQueryDao.singleColQuery("select ttd.procurementNature from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId).get(0).toString().equals("Services")){
                query.append("select ebs.result from TblEvalBidderStatus ebs where ebs.userId="+userId+" and ebs.tenderId="+tenderId+" and ebs.evalCount="+evalCount);
            }else{
                if(hibernateQueryDao.singleColQuery("select ttd.docAvlMethod from TblTenderDetails ttd where ttd.tblTenderMaster.tenderId="+tenderId.trim()).get(0).toString().equalsIgnoreCase("Package")){
                    query.append("select ebs.bidderStatus from TblEvalBidderStatus ebs where ebs.userId="+userId+" and ebs.tenderId="+tenderId+" and ebs.evalCount="+evalCount);

                }else{
                    query.append("select ebs.bidderStatus from TblEvalBidderStatus ebs where ebs.userId="+userId+" and ebs.tenderId="+tenderId+" and ebs.pkgLotId="+pkgLotId+" and ebs.evalCount="+evalCount);
                }
            }
            List<Object> list = hibernateQueryDao.singleColQuery(query.toString());
            if(!list.isEmpty()){
                bidStat = list.get(0).toString();
            }
        } catch (Exception e) {
            logger.error("getBidderStatus : "+logUserId,e);
        }
        logger.debug("getBidderStatus : "+logUserId+" : Ends");
        return bidStat;
    }

    /**
     * Evaluation report count
     * @param tenderId
     * @param pkgLotId
     * @param evalType
     * @param reportType
     * @param condition
     * @return count
     */
    public long evalRepCount(String tenderId,String pkgLotId,String evalType,String reportType,String condition,int evalCount){
        logger.debug("evalRepCount : "+logUserId+" : Starts");
        long cnt=0;
        try {
            cnt = hibernateQueryDao.countForNewQuery("TblEvalTerreport", "tenderId="+tenderId+" and pkgLotId="+pkgLotId+" and evalType='"+evalType+"' and reportType='"+reportType+"' and evalCount='"+evalCount+"' and "+condition);

        } catch (Exception e) {
            logger.error("evalRepCount : "+logUserId,e);
        }
        logger.debug("evalRepCount : "+logUserId+" : Ends");
        return cnt;
    }

    /**
     * Get Evaluation Report Data
     * @param tenderId from tbl_TenderMaster
     * @param pkgLotId from tbl_TenderLotSecurity
     * @param evalType Eval or Reeval
     * @param reportType TER1 or 2
     * @param createdBy session id
     * @param isSign
     * @return List of {userId,criteriaId,value}
     */
    public List<Object[]> evalRepData(String tenderId,String pkgLotId,String evalType,String reportType,String createdBy,boolean isSign,int evalCount){
        logger.debug("evalRepData : "+logUserId+" : Starts");
        List<Object[]> data = null;
        StringBuilder query = new StringBuilder();
        try {
             query.append("select et.userId,et.tblEvalTerreportCriteria.criteriaId,et.value from TblEvalTerreport et where et.tenderId="+tenderId+" and et.pkgLotId="+pkgLotId+" and et.evalType='"+evalType+"' and et.reportType='"+reportType+"' and et.evalCount ="+evalCount+"");
             if(isSign){
                query.append(" and et.tecRole='cp'");
             }else{
                 query.append(" and et.createdBy="+createdBy);
             }
             data = hibernateQueryDao.createNewQuery(query.toString());
        } catch (Exception e) {
            logger.error("evalRepData : "+logUserId,e);
        }
        logger.debug("evalRepData : "+logUserId+" : Ends");
        return data;    
    }

    /**
     * Get FinalSubmission users for TER Report
     * @param tenderId  from tbl_TenderMaster
     * @param lotId from tbl_TenderLotSecurity
     * @param roundId from tbl_EvalRoundMaster
     * @return List of {userId,companyId,comapanyName,firstName,lastName }
     */
public List<Object[]> getFinalSubmissionUserForTer2(String tenderId,String lotId,String roundId,String evalCount){
        StringBuilder query = new StringBuilder();//ttm.firstName ||' '|| ttm.lastName
        int noaCount = 0;

        List<Object> countNOA = hibernateQueryDao.getSingleColQuery("select COUNT(tnid.userId) from TblNoaAcceptance tna,TblNoaIssueDetails tnid where tnid.noaIssueId = tna.tblNoaIssueDetails.noaIssueId and tnid.tblTenderMaster.tenderId="+tenderId+" and tna.acceptRejStatus in ('decline','Performance Security not paid')");
        if(!countNOA.isEmpty() && countNOA!=null){
                noaCount = Integer.parseInt(countNOA.get(0).toString());
            }

        if(Integer.parseInt(evalCount)>0 && noaCount > 0)
        {
            evalCount = String.valueOf(Integer.parseInt(evalCount) - 1);
            query.append("select tfs.userId,tcm.companyId,(CASE WHEN tcm.companyId = 1 THEN concat(ttm.firstName,' ',ttm.lastName)  ELSE tcm.companyName END) ,ttm.firstName,ttm.lastName from TblFinalSubmission  tfs,TblTendererMaster ttm,TblCompanyMaster tcm,TblEvalBidderStatus ebs ");
            query.append("where tfs.tblTenderMaster.tenderId="+tenderId+" and tfs.bidSubStatus='finalsubmission' and ttm.tblLoginMaster.userId=tfs.userId and ttm.tblCompanyMaster.companyId = tcm.companyId ");
            query.append("and ebs.tenderId = tfs.tblTenderMaster.tenderId and ebs.evalCount = "+evalCount+" and ebs.userId = tfs.userId and ebs.userId not in (select tnid.userId from TblNoaIssueDetails tnid where tnid.tblTenderMaster.tenderId = "+tenderId+")");
            if(!"0".equalsIgnoreCase(lotId)){
                query.append(" and tfs.userId in (select tbl.userId from TblBidderLots tbl where tbl.tenderId="+tenderId+" and tbl.pkgLotId = "+lotId+")");
            }
            if(!"0".equals(roundId)){
                query.append(" and tfs.userId in (select trm.userId from TblEvalRoundMaster trm where trm.roundId="+roundId+")");
            }
            query.append(" Order by (CASE WHEN tcm.companyId = 1 THEN concat(ttm.firstName,' ',ttm.lastName)  ELSE tcm.companyName END)");
        }
        else
        {
           query.append("select tfs.userId,tcm.companyId,(CASE WHEN tcm.companyId = 1 THEN concat(ttm.firstName,' ',ttm.lastName)  ELSE tcm.companyName END) ,ttm.firstName,ttm.lastName from TblFinalSubmission  tfs,TblTendererMaster ttm,TblCompanyMaster tcm ");
            query.append("where tfs.tblTenderMaster.tenderId="+tenderId+" and tfs.bidSubStatus='finalsubmission' and ttm.tblLoginMaster.userId=tfs.userId ");
            query.append("and ttm.tblCompanyMaster.companyId = tcm.companyId");
            if(!"0".equalsIgnoreCase(lotId)){
                query.append(" and tfs.userId in (select tbl.userId from TblBidderLots tbl where tbl.tenderId="+tenderId+" and tbl.pkgLotId = "+lotId+")");
            }
            if(!"0".equals(roundId)){
                query.append(" and tfs.userId in (select trm.userId from TblEvalRoundMaster trm where trm.roundId="+roundId+")");
            }
            query.append(" Order by (CASE WHEN tcm.companyId = 1 THEN concat(ttm.firstName,' ',ttm.lastName)  ELSE tcm.companyName END)");
 
        }
        return hibernateQueryDao.createNewQuery(query.toString());
    }
    public String saveReportRoundWise(int bidderId,String roundId){
        List<SPCommonSearchDataMore> list = commonSearchDataMoreService.geteGPData("roundWiseInsert",roundId,String.valueOf(bidderId));
        String msg = "0";
        if(!list.isEmpty()){
            msg = list.get(0).getFieldName1();
        }
        if(msg.equals("0")){
            logger.error("saveReportRoundWise : "+logUserId+" : Error in saving report roundwise");
        }
        return msg;
    }

    /**
     * Save T1L1 Report
     * @param roundMaster to be saved
     * @param list1 to be inserted
     * @return true or false for success or fail
     */
    public boolean saveT1L1Rpt(TblEvalRoundMaster roundMaster,List<TblBidderRank> list1){
        logger.debug("saveT1L1Rpt : "+logUserId+" : Starts");
        boolean flag = false;
        try {
            List<Object> lst = hibernateQueryDao.singleColQuery("select count(roundId) from TblEvalRoundMaster where tenderId = "+ roundMaster.getTenderId() +" and userId = "+roundMaster.getUserId()+" and reportType = '"+roundMaster.getReportType()+"'");
            if(lst.get(0).toString().equals("0")){
                if(roundMaster!=null){
                tblEvalRoundMasterDao.addTblEvalRoundMaster(roundMaster);
                    for (int i = 0; i < list1.size(); i++) {
                        list1.get(i).setRoundId(roundMaster.getRoundId());
                    }
                }
                tblBidderRankDao.updateOrSaveBidderRank(list1);
                flag= true;
        }} catch (Exception e) {
            logger.error("saveT1L1Rpt : "+logUserId+" : "+e);
        }
        logger.debug("saveT1L1Rpt : "+logUserId+" : Ends");
        return flag;
    }

    /**
     * Report Id Negotiation
     * @param negId negotiationId
     * @return reportId
     */
    public String getRepIdByNegId(String negId){
        String roundId = "0";
        List<Object> list = hibernateQueryDao.singleColQuery("select rm.reportId from TblNegotiation tn,TblEvalRoundMaster rm where tn.roundId = rm.roundId and tn.negId="+negId);
        if(list!=null && (!list.isEmpty())){
           roundId = list.get(0).toString();
        }
        return roundId;
    }


    public List<Object> getReportID4Tender(String tenderId){
        return hibernateQueryDao.singleColQuery("select rm.reportId from TblReportMaster rm where rm.tblTenderMaster.tenderId="+tenderId);
    }
    public void setUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    /**
     * Get recpord from tbl_evalRoundMaster as per roundID
     * @param roundId RoundId
     * @return Record of tbl_evalRoundMaster
     */
    public TblEvalRoundMaster getTblEvalRoundMasters(int roundId){
        List<TblEvalRoundMaster> list = new ArrayList<TblEvalRoundMaster>();
        TblEvalRoundMaster tblEvalRoundMaster = null;
        try {
            list = tblEvalRoundMasterDao.findTblEvalRoundMaster("roundId",Operation_enum.EQ,roundId);
            if(list!=null && !list.isEmpty()){
                tblEvalRoundMaster = list.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblEvalRoundMaster;
    }
    /**
     * Get records from tbl_biiderRank as per roundId
     * @param roundId roundId
     * @return list of tbl_bidderRank
     */
    public List<TblBidderRank> getTblBidderRanks(int roundId){
        List<TblBidderRank> list = new ArrayList<TblBidderRank>();
        try {
            list = tblBidderRankDao.findTblBidderRank("roundId",Operation_enum.EQ,roundId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    /**
     * Get records from Tbl_BidRankDetail as per list BidderRank
     * @param List of Tbl_BidderRank  listRank
     * @return list of tbl_bidRankDetail
     */
    public List<TblBidRankDetail> getTblBidRankDetails(List<TblBidderRank> listRank){
        List<TblBidRankDetail> list = new ArrayList<TblBidRankDetail>();
        try {
            for(TblBidderRank tblBidderRank : listRank){
            list.addAll(tblBidRankDetailDao.findTblBidRankDetail("tblBidderRank",Operation_enum.EQ,new TblBidderRank(tblBidderRank.getBidderRankId())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    /*public boolean saveNegotiationRejectedRank(TblEvalRoundMaster roundMaster,List<TblBidderRank> list1, List<TblBidRankDetail> list2, int noOfCols){
        logger.debug("saveNegotiationRejectedRank : " + logUserId + " Starts");
        boolean flag = true;
        try {
                if(roundMaster!=null){
                tblEvalRoundMasterDao.addTblEvalRoundMaster(roundMaster);
                    for (int i = 0; i < list1.size(); i++) {
                        list1.get(i).setRoundId(roundMaster.getRoundId());
                        tblBidderRankDao.addTblBidderRank(list1.get(i));
                    }
                }
            int j = 0;
                for (TblBidderRank tbr : list1) {
                    for (int i = 0; i < noOfCols; i++) {
                        list2.get(j).setTblBidderRank(tbr);
                        j++;
                        tblBidRankDetailDao.addTblBidRankDetail(list2.get(j));
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
        }
        logger.debug("saveNegotiationRejectedRank : " + logUserId + " Ends");
        return flag;
    }
    
    public boolean saveNegotiationRejectedRankDtl(List<TblBidRankDetail> listDtl){
        boolean flag = true;
        try {
            for(TblBidRankDetail tblBidderRank : listDtl){
                tblBidRankDetailDao.addTblBidRankDetail(tblBidderRank);
            }
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }*/
}
