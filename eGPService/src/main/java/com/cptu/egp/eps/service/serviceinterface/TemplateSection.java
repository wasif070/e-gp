/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TemplateSection {
    /**
     * Create Template Section
     * @param tblTemplateSections
     * @return true if created or false if not.
     */
    public boolean createTemplateSection(TblTemplateSections tblTemplateSections);
    /**
     * Get Template section 
     * @param templateId
     * @return List of Template Section
     */
    public List getTemplateSection(short templateId);
    /**
     * Get Section Types.
     * @param sectionId
     * @return List of Sections
     */
    public List getSectionType(int sectionId);
    /**
     *Set User Id 
     * @param userId
     */
    public void setUserId(String userId);
    /**
     * Get Section name by template Id
     * @param templateId
     * @param type
     * @return name of section
     */
    public List getSecName(short templateId,String type);
    
    public void setAuditTrail(AuditTrail auditTrail);

}
