/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblConfigClarificationDao;
import com.cptu.egp.eps.model.table.TblConfigClarification;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author MD. Emtazul Haque
 */
public class ConfigClarificationService {
    
    private HibernateQueryDao hibernateQueryDao;
    private TblConfigClarificationDao tblConfigClarificationDao;
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private static final Logger LOGGER = Logger.getLogger(ConfigClarificationService.class);
    MakeAuditTrailService makeAuditTrailService;
    
    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    /**
     *List of SBD
     * @return List of {templateId,templateName}
     */
    public List<Object[]> getSBDList() {
        return hibernateQueryDao.createNewQuery("select ttm.templateId,ttm.templateName from TblTemplateMaster ttm where ttm.status='A'");
    }
    
    /**
     *List of all TblConfigClarification
     * @return true or false for success or fail
     */
    public List<TblConfigClarification> getAllTblConfigClarification() {
        return tblConfigClarificationDao.getAllTblConfigClarification();
    }
    
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    public TblConfigClarificationDao getTblConfigClarificationDao() {
        return tblConfigClarificationDao;
    }

    public void setTblConfigClarificationDao(TblConfigClarificationDao tblConfigClarificationDao) {
        this.tblConfigClarificationDao = tblConfigClarificationDao;
    }
    
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
    
    /**
     * Unique count of TblConfigClarification
     * @param ConfigClarificationId
     * @param SBDName
     * @param ClarificationDays
     */
    public long uniqueConfigCount(String ConfigClarificationId,String SBDName ) throws Exception{
        return hibernateQueryDao.countForNewQuery("TblConfigClarification tcc", "tcc.templateId="+SBDName+" and configClarificationId!="+ConfigClarificationId);
    }
    
    /**
     *Inserts into TblConfigClarification
     * @param list to be inserted
     * @return true or false for success or fail
     */
    public boolean insertConfigClarification(List<TblConfigClarification> list) {
        LOGGER.debug("insertConfigClarification : "+logUserId+" Starts");
        boolean flag;
        String action = null;
        try {
            tblConfigClarificationDao.updateInsertAllConfigClarification(list);
            flag =  true;
            action = "Add Clarification days";
        } catch (Exception e) {
            LOGGER.error("insertConfigClarification : "+logUserId+" :"+e);
            flag =  false;
            action = "Error in Adding Clarification days : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("insertConfigClarification : "+logUserId+" Ends");
        return flag;
    }
    
    /**
     *Updates TblConfigClarification
     * @param list to be updated
     * @return true or false for success or fail
     */
    public boolean updateConfigClarification(List<TblConfigClarification> list) {
        LOGGER.debug("updateConfigClarification : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
        try {
            int i = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigClarification");
            if (i > 0) {
                tblConfigClarificationDao.updateInsertAllConfigClarification(list);
                flag = true;
            }
            action = "Edit Clarification days";
        } catch (Exception e) {
            LOGGER.error("updateConfigClarification : "+logUserId+" Ends"+e);
            action = "Error in editing Clarification days : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateConfigClarification : "+logUserId+" Ends");
        return flag;
        
    }
    
    /**
     *Deletes TblConfigClarification on ConfigClarificationId
     * @param ConfigClarificationId to be deleted on configClarificationId
     * @return number of rows deleted
     */
    public int deleteSingleConfigClarification(String ConfigClarificationId){
        LOGGER.debug("deleteSingleConfigClarification : "+logUserId+" Starts");
        int cnt = 0;
        String action = null;
        try {
            cnt = hibernateQueryDao.updateDeleteNewQuery("delete from TblConfigClarification where configClarificationId="+ConfigClarificationId);
            action = "Remove Clarification days";
        } catch (Exception e) {
            LOGGER.debug("deleteSingleConfigClarification : "+logUserId,e);
            action = "Error in Removing Clarification days : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("deleteSingleConfigClarification : "+logUserId+" Ends");
        return cnt;
    }
    
    
    /**
     *Update TblConfigClarification
     * @param tblConfigClarification to be updated
     * @return true or false for success or fail
     */
    public boolean updateSingleConfigClarification(TblConfigClarification tblConfigClarification) {
        LOGGER.debug("updateSingleConfigClarification : "+logUserId+" Starts");
        boolean flag = false;
        String action = null;
        try {
            tblConfigClarificationDao.updateTblConfigClarification(tblConfigClarification);
            flag = true;
            action = "Edit Clarification days";
        } catch (Exception e) {
            LOGGER.error("updateSingleConfigClarification : "+logUserId+" Ends"+ e);
            action = "Error in Editing Clarification days : "+e;
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(logUserId), "userId", EgpModule.Configuration.getName(), action, "");
            action = null;
        }
        LOGGER.debug("updateSingleConfigClarification : "+logUserId+" Ends");
        return flag;
    }
    
    /**
     *Count of all TblConfigClarification
     * @return count
     * @throws Exception
     */
    public long getAllConfigClarificationCount() throws Exception{
        return hibernateQueryDao.countForNewQuery("TblConfigClarification tcc","1=1");
    }
    
        /**
     *List of all TblConfigClarification by search
     * @param values search criteria
     * @return List of TblConfigClarification
     * @throws Exception
     */
    public List<TblConfigClarification> getTblConfigClarification(Object... values) throws Exception {
        return tblConfigClarificationDao.findTblConfigClarification(values);
    }
}
