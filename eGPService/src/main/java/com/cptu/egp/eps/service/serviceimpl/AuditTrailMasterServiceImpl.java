/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblAuditTrailMasterDao;
import com.cptu.egp.eps.model.table.TblAuditTrailMaster;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AuditTrailMasterServiceImpl {

    final Logger logger = Logger.getLogger(AuditTrailMasterServiceImpl.class);
    TblAuditTrailMasterDao tblAuditTrailMasterDao;
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public TblAuditTrailMasterDao getTblAuditTrailMasterDao() {
        return tblAuditTrailMasterDao;
    }

    public void setTblAuditTrailMasterDao(TblAuditTrailMasterDao tblAuditTrailMasterDao) {
        this.tblAuditTrailMasterDao = tblAuditTrailMasterDao;
    }

    /**
     * Add tblAuditTrailMaster Data
     * @param tblAuditTrailMaster
     */
    public void addDate(TblAuditTrailMaster tblAuditTrailMaster) {
        logger.debug("addDate : " + logUserId + loggerStart);
        try {
       tblAuditTrailMasterDao.addTblAuditTrailMaster(tblAuditTrailMaster);
        } catch (Exception e) {
            logger.error("addDate : " + logUserId + e);
   }
        logger.debug("addDate : " + logUserId + loggerEnd);
    }
}
