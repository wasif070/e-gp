/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.dao.storedprocedure.CommonFormData;
import com.cptu.egp.eps.model.table.TblBidModification;
import com.cptu.egp.eps.model.table.TblFinalSubDetail;
import com.cptu.egp.eps.model.table.TblFinalSubmission;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TenderBidService {
    
    /**
     * Get Tender Form Details.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return List of form Data
     */
    public List<CommonFormData> getTenderFormDetails(int tableId,int formId,int columnId,int userId,int bidId,int tenderId, String funName);

    /**
     * Insert Form Details.
     * @param action
     * @param tableName1
     * @param tableName2
     * @param tableName3
     * @param tableName4
     * @param xmlStr1
     * @param xmlStr2
     * @param xmlStr3
     * @param xmlStr4
     * @param table1PK
     * @param table2PK
     * @param table3PK
     * @param condition
     * @return true if added or false if not.
     */
    public boolean insertFormDetails(String action,String tableName1,String tableName2,String tableName3,String tableName4,String xmlStr1,String xmlStr2,String xmlStr3,String xmlStr4,String table1PK,String table2PK,String table3PK,String condition);

    /**
     * Update Final Submission Status.
     * @param tenderid
     * @param userid
     * @param reason
     * @param isModify
     * @param ipAddr
     */
    public void updateFinalSubStatus(int tenderid,int userid,String reason,boolean isModify,String ipAddr);

    /**
     * Get Tenderer Email and Mobile Nos.
     * @param userId
     * @return
     */
    public String[] getTendererMobileNEmail(String userId);

    /**
     * Find Bid Modified History
     * @param userId
     * @param tenderid
     * @return List Bid Modification.
     * @throws Exception
     */
    public List<TblBidModification> findBidModifyHistory(String userId,String tenderid)throws Exception;

    /**
     * Delete Bid form Details.
     * @param formId
     * @param userId
     * @param bidId
     * @return true if deleted or false if not.
     */
    public boolean deleteBidFormDetails(int formId,int userId,int bidId);

    /**
     * Get Buyer Password and Hash.
     * @param tenderId
     * @return Hash value.
     */
    public String getBuyerPwdHash(int tenderId);

    /**
     * Insert Bid Encrypt Details.
     * @param tableName
     * @param xmlBidEncrypt
     * @param whereCond
     * @param isInEditMode
     * @return true if added or false if not.
     */
    public boolean insertBidEncrypt(String tableName, String xmlBidEncrypt, String whereCond, boolean isInEditMode);

    /**
     * Insert in to Final Submission
     * @param tblFS
     * @return Id.
     */
    public int insertIntoFS(TblFinalSubmission tblFS);

    /**
     * Insert in to Final Submission details.
     * @param tblFSDtl
     * @return true if added or false if not.
     */
    public boolean insertIntoFSDtl(TblFinalSubDetail tblFSDtl);

    /**
     * Get Final Submission Id.
     * @param tenderId
     * @param userId
     * @return Id.
     */
    public int getFinalSubId(int tenderId, int userId);

    /**
     * Set User Id at Service layer
     * @param userId
     */
    public void setUserId(String userId);

    /**
     * Get Bid Id from user Id and tender Form Id.
     * @param userId
     * @param tenderFormId
     * @return List of ID.
     */
    public List<Object> getBidId(int userId, int tenderFormId);//fetch buidder ID

    /**
     * Get Tender List Cell Details.
     * @param formId
     * @param tableId
     * @param columnId
     * @param cellId
     * @return List of List Cell Details.
     */
    public List<TblTenderListCellDetail> getTenderListCellDetail(int formId,int tableId, short columnId, int cellId);

    /**
     * Get final Submission Tender Details.
     * @param tenderId
     * @param userId
     * @return Final Submision Details.
     */
    public Object[] finalSubTenderDetails(String tenderId,String userId);

    /**
     * Final Submission Lot Details
     * @param tenderId
     * @param userId
     * @return List of Submission Detials.
     */
    public List<Object[]> finalSubLotDetails(String tenderId,String userId);
    
    /**
     * Get Grand Total Caption cell 
     * @param tenderFormId
     * @param tenderTableId 
     * @return cell Id.
     */ 
    public int getCellForGrandTotalCaption(int tenderFormId, int tenderTableId);
    
    /**
     * Get Tender Column Details.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return List of Tender Common Data.
     */
    public List<CommonFormData> getTenderColumnDetails(int tableId, int formId, int columnId, int userId, int bidId, int tenderId, String funName);

    /**
     * Check Bid Form Submitted or Not.
     * @param tenderId = tbl_TenderBidForm.TenderId
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return no. of record for TenderBidForm
     */
    public long checkBidFormSubmited(int tenderId,int formId,int userId);

     /**
     * Check Form Encripted or Not.
     * @param tenderId = tbl_TenderBidForm.TenderId
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return no. of record for TenderBidForm
     */
    public long checkBidFormEncriptedAndSaved(int tenderId,int formId,int userId);
}
