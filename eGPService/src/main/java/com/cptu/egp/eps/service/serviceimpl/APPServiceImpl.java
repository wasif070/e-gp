/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.daointerface.TblAppEngEstDocDao;
import com.cptu.egp.eps.dao.daointerface.TblAppPackagesDao;
import com.cptu.egp.eps.dao.daointerface.TblAppWatchListDao;
import com.cptu.egp.eps.dao.daointerface.TblTendererAuditTrailDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.AppCommonData;
import com.cptu.egp.eps.dao.storedprocedure.AppMaster;
import com.cptu.egp.eps.dao.storedprocedure.AppPackage;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonWeekendsHolidays;
import com.cptu.egp.eps.dao.storedprocedure.SPCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.SPWeekendsHolidays;
import com.cptu.egp.eps.dao.storedprocedure.SearchMyApp;
import com.cptu.egp.eps.dao.storedprocedure.SearchMyAppData;
import com.cptu.egp.eps.model.table.TblAppEngEstDoc;
import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblAppWatchList;
import static com.cptu.egp.eps.service.serviceimpl.TenderCorriService.logger;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class APPServiceImpl implements APPService {

    AppCommonData appCommonData;
    AppMaster appMaster;
    AppPackage appPackage;
    SearchMyApp searchMyApp;
    SPTenderCommon sPTenderCommon;
    TblAppEngEstDocDao tblAppEngEstDocDao;
    TblAppWatchListDao tblAppWatchListDao;
    TblAppPackagesDao tblAppPackagesDao;
    SPWeekendsHolidays spWeekendsHolidays;
    HibernateQueryDao hibernateQueryDao;
    SPCommon sPCommon;
    static final Logger LOGGER = Logger.getLogger(APPServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private AuditTrail auditTrail;
    private MakeAuditTrailService makeAuditTrailService;
    private TblAppPackagesDao tblAppPackageDao;
    

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }

    public TblAppWatchListDao getTblWatchListDao() {
        return tblAppWatchListDao;
    }

    public void setTblWatchListDao(TblAppWatchListDao tblAppWatchListDao) {
        this.tblAppWatchListDao = tblAppWatchListDao;
    }

    public SearchMyApp getSearchMyApp() {
        return searchMyApp;
    }

    public TblAppEngEstDocDao getTblAppEngEstDocDao() {
        return tblAppEngEstDocDao;
    }

    public void setTblAppEngEstDocDao(TblAppEngEstDocDao tblAppEngEstDocDao) {
        this.tblAppEngEstDocDao = tblAppEngEstDocDao;
    }

    public void setSearchMyApp(SearchMyApp searchMyApp) {
        this.searchMyApp = searchMyApp;
    }

    public AppCommonData getAppCommonData() {
        return appCommonData;
    }

    public void setAppCommonData(AppCommonData appCommonData) {
        this.appCommonData = appCommonData;
    }

    public AppMaster getAppMaster() {
        return appMaster;
    }

    public void setAppMaster(AppMaster appMaster) {
        this.appMaster = appMaster;
    }

    public AppPackage getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(AppPackage appPackage) {
        this.appPackage = appPackage;
    }

    public SPWeekendsHolidays getSpWeekendsHolidays() {
        return spWeekendsHolidays;
    }

    public void setSpWeekendsHolidays(SPWeekendsHolidays spWeekendsHolidays) {
        this.spWeekendsHolidays = spWeekendsHolidays;
    }

    public SPCommon getsPCommon() {
        return sPCommon;
    }

    public void setsPCommon(SPCommon sPCommon) {
        this.sPCommon = sPCommon;
    }

    public TblAppPackagesDao getTblAppPackagesDao() {
        return tblAppPackagesDao;
    }

    public void setTblAppPackagesDao(TblAppPackagesDao tblAppPackagesDao) {
        this.tblAppPackagesDao = tblAppPackagesDao;
    }

    public TblAppWatchListDao getTblAppWatchListDao() {
        return tblAppWatchListDao;
    }

    public void setTblAppWatchListDao(TblAppWatchListDao tblAppWatchListDao) {
        this.tblAppWatchListDao = tblAppWatchListDao;
    }

    public SPTenderCommon getsPTenderCommon() {
        return sPTenderCommon;
    }

    public void setsPTenderCommon(SPTenderCommon sPTenderCommon) {
        this.sPTenderCommon = sPTenderCommon;
    }

    public AuditTrail getAuditTrail() {
        return auditTrail;
    }

    @Override
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    public MakeAuditTrailService getMakeAuditTrailService() {
        return makeAuditTrailService;
    }

    public void setMakeAuditTrailService(MakeAuditTrailService makeAuditTrailService) {
        this.makeAuditTrailService = makeAuditTrailService;
    }
    
    @Override
    public List<CommonAppData> getAPPDetailsBySP(String param1, String param2, String param3) throws Exception {
        LOGGER.debug("getAPPDetailsBySP : " + logUserId + LOGGERSTART);
        List<CommonAppData> commonAppData = null;
        try {
            commonAppData = appCommonData.executeProcedure(param1, param2, param3);
        } catch (Exception e) {
            LOGGER.error("getAPPDetailsBySP : " + logUserId + e);
    }
        LOGGER.debug("getAPPDetailsBySP : " + logUserId + LOGGEREND);
        return commonAppData;
    }

    @Override
    public List<CommonAppData> getUserDetailsForEmail(String param1, String param2, String param3) throws Exception {
        LOGGER.debug("getUserDetailsForEmail : " + logUserId + LOGGERSTART);
        List<CommonAppData> commonAppData = null;
        try {
            commonAppData = appCommonData.executeProcedure(param1, param2, param3);
        } catch (Exception e) {
            LOGGER.error("getUserDetailsForEmail : " + logUserId + e);
    }
        LOGGER.debug("getUserDetailsForEmail : " + logUserId + LOGGEREND);
        return commonAppData;
    }
    @Override
    public boolean CountAPPRevise(String action, int appID, Date LastRevision){
        LOGGER.debug("CountAPPRevise : " + logUserId + LOGGERSTART);
    List<CommonSPReturn> commonSPReturn = null;
    boolean count = false;
    try {
            commonSPReturn = appMaster.executeProcedure("",0, 0, "", 0, 
                    0, 0, "", 0, 0, 0, action, appID, "", "",
                    0, 0,LastRevision,null);
      
            if(commonSPReturn.get(0).getFlag()){
               count = true; 
            }
            
        } catch (Exception e) {
            LOGGER.error("CountAPPRevise : " + logUserId + e);
        }finally {
        
        }
    
    return count;
    }
    
    @Override
    public String updateAPPcount(String financialYear, String action, int appId){
    LOGGER.debug("updateAPPcount : " + logUserId + LOGGERSTART);
    List<CommonSPReturn> commonSPReturn = null;
    String str = "";
    try {
        commonSPReturn = appMaster.executeProcedure(financialYear,0, 0, "", 0, 
                    0, 0, "", 0, 0, 0, action, appId, "", "",
                    0, 0,null,null);
      
        if(commonSPReturn.get(0).getFlag()){
            if(commonSPReturn.get(0).getMsg().equalsIgnoreCase("Revision Count Not Filled")){
            System.out.println("Revision Count Not Filled");
            str = "Not Filled";
            }
            else if(commonSPReturn.get(0).getMsg().equalsIgnoreCase("Revision Count Filled")){
            System.out.println("Revision Count Filled");
            str = "Filled";
            }
            else{
            System.out.println("Error");
            }
        }
        } catch (Exception e) {
            LOGGER.error("updateAPPcount : " + logUserId + e);
 
        }finally {


        }
    return str;
    }
    
    
    @Override
    public List<CommonSPReturn> insertAPDetailsBySP(String financialYearId, int budgetType, int prjId, 
            String prjName, int officeId, int empId, int aaEmpId, String appCode, int createdBy, 
            int deptId, int aaProcRoleId, String action, int appId, 
            String appType, String entrustingAgency, Integer refAppId, int revisionCount, Date lastRevisionDate, String ActivityName) throws Exception {
        LOGGER.debug("insertAPDetailsBySP : " + logUserId + LOGGERSTART);
        List<CommonSPReturn> commonSPReturn = null;
        String auditAction = "";
        int auditappId = 0;
        try {
            /*Add Budget Code (Mohsina)*/
            commonSPReturn = appMaster.executeProcedure(financialYearId, budgetType, prjId, prjName, officeId, 
                    empId, aaEmpId, appCode, createdBy, deptId, aaProcRoleId, action, appId, appType, entrustingAgency,
                    refAppId, revisionCount, lastRevisionDate, ActivityName);
            if(!commonSPReturn.isEmpty()){
                auditappId = Integer.parseInt(commonSPReturn.get(0).getId().toString());
            }
            auditAction = "Create APP";
        } catch (Exception e) {
            LOGGER.error("insertAPDetailsBySP : " + logUserId + e);
            auditAction = "Error in Create APP";
        }finally {
            makeAuditTrailService.generateAudit(auditTrail,auditappId , "appId", EgpModule.APP.getName(), auditAction, "");
            auditAction = null;
        }
        LOGGER.debug("insertAPDetailsBySP : " + logUserId + LOGGEREND);
        return commonSPReturn;
    }

    @Override
    public List<CommonSPReturn> insertPkgBySP(Integer appId, String procNature, String serviceType, String pkgNo, String pkgDesc,String depoplanWork, String entrustingAgency, String timeFrame, String bidderCategory, String workCategory, Float aloBud, Float estCost, String cpvCode, Float pkgEstCost, Integer aaEmpId, String isPQReq, String reoiRfa, Integer procMtdId, String procType, String sourceFund, String appStatus, String wfStatus, Integer noOfStages, String action, Integer pkgId, String lotNo, String lotDesc, String qty, String unit, String lotEstCost, String pkgUrgency,String w1, String w2, String w3, String w4) throws Exception {
        LOGGER.debug("insertPkgBySP : " + logUserId + LOGGERSTART);
        List<CommonSPReturn> sPReturn = null;
        String auditAction = "";
        try {
            sPReturn = appPackage.executeProcedure(appId, procNature, serviceType, pkgNo, pkgDesc, depoplanWork,entrustingAgency,timeFrame, bidderCategory, workCategory, new BigDecimal(aloBud),  new BigDecimal(estCost), cpvCode,  new BigDecimal(pkgEstCost), aaEmpId, isPQReq, reoiRfa, procMtdId, procType, sourceFund, appStatus, wfStatus, noOfStages, action, pkgId, lotNo, lotDesc, qty, unit, lotEstCost, pkgUrgency,w1,w2,w3,w4);
            if("Update".equalsIgnoreCase(action)){
                auditAction = "Edit Package Detail";
            }else{
                auditAction = "Add Package Detail";
            }
        } catch (Exception e) {
            LOGGER.error("insertPkgBySP : " + logUserId + e);
            if("Update".equalsIgnoreCase(action)){
                auditAction = "Error in Edit Package Detail";
            }else{
                auditAction = "Error in Add Package Detail";
            }
        }finally {
            makeAuditTrailService.generateAudit(auditTrail,appId , "appId", EgpModule.APP.getName(), auditAction, "");
            auditAction = null;
        }
        LOGGER.debug("insertPkgBySP : " + logUserId + LOGGEREND);
        return sPReturn;
    }

    @Override
    public List<SearchMyAppData> getAPPSearchDetailsBySP(String financialYear, String budgetType, String projectName, String activityName, String Status, int pageinN, int recordPerPage_inN, Integer userId, Integer appId, String appCode, Integer createdBy, String sortColumn,String sortType) throws Exception {
        LOGGER.debug("getAPPSearchDetailsBySP : " + logUserId + LOGGERSTART);
        List<SearchMyAppData> myAppData = null;
        try {
            /*Budget Code (Mohsina)*/
            myAppData = searchMyApp.executeProcedure(financialYear, budgetType, projectName, activityName, Status, Integer.parseInt(String.valueOf(pageinN)), Integer.parseInt(String.valueOf(recordPerPage_inN)), userId, appId, appCode, createdBy, sortColumn,sortType);
        
        } catch (Exception e) {
            LOGGER.error("getAPPSearchDetailsBySP : " + logUserId + e);
    }
        LOGGER.debug("getAPPSearchDetailsBySP : " + logUserId + LOGGEREND);
        return myAppData;
    }

    @Override
    public void addAppEngEstDoc(TblAppEngEstDoc tblAppEngEstDoc) {
        LOGGER.debug("addAppEngEstDoc : " + logUserId + LOGGERSTART);
        try {
        tblAppEngEstDocDao.addEntity(tblAppEngEstDoc);
        } catch (Exception e) {
            LOGGER.error("addAppEngEstDoc : " + logUserId + e);
    }
        LOGGER.debug("addAppEngEstDoc : " + logUserId + LOGGEREND);
    }

    @Override
    public List<TblAppEngEstDoc> getAllAppEngEstDocDetails(int pckId) throws Exception {
        LOGGER.debug("getAllAppEngEstDocDetails : " + logUserId + LOGGERSTART);
        List<TblAppEngEstDoc> tblAppEngEstDocs = null;
        try {
            tblAppEngEstDocs = tblAppEngEstDocDao.findTblAppEngEstDoc("tblAppPackages.packageId", Operation_enum.EQ, pckId);
        } catch (Exception e) {
            LOGGER.error("getAllAppEngEstDocDetails : " + logUserId + e);
    }
        LOGGER.debug("getAllAppEngEstDocDetails : " + logUserId + LOGGEREND);
        return tblAppEngEstDocs;
    }

    @Override
    public int addToWatchList(TblAppWatchList tblAppWatchList) {
        LOGGER.debug("addToWatchList : " + logUserId + LOGGERSTART);
        int count = 0;
        String action = "App Added to Watchlist";
        try {
            tblAppWatchListDao.addTblAppWatchList(tblAppWatchList);
            count = tblAppWatchList.getAppWatchId();
        } catch (Exception ex) {
            LOGGER.error("addToWatchList : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblAppWatchList.getTblLoginMaster().getUserId(), "userId", EgpModule.WATCH_LIST.getName(), action, "App ID = "+tblAppWatchList.getTblAppMaster().getAppId());
            action = null;
        }
        LOGGER.debug("addToWatchList : " + logUserId + LOGGEREND);
        return count;
        }

    @Override
    public List<CommonWeekendsHolidays> getDateWithDiffBySP(Date getDated, Integer daysN) {
        LOGGER.debug("getDateWithDiffBySP : " + logUserId + LOGGERSTART);
        List<CommonWeekendsHolidays> commonWeekendsHoliday = null;
        try {
            commonWeekendsHoliday = spWeekendsHolidays.executeProcedure(getDated, daysN);
        } catch (Exception e) {
            LOGGER.error("getDateWithDiffBySP : " + logUserId + e);
    }
        LOGGER.debug("getDateWithDiffBySP : " + logUserId + LOGGEREND);
        return commonWeekendsHoliday;
    }

     @Override
    public List<TblAppEngEstDoc> getAppEngEstDocDetails(Object... values) throws Exception {
        LOGGER.debug("getAppEngEstDocDetails : " + logUserId + LOGGERSTART);
        List<TblAppEngEstDoc> tblAppEngEstDoc = null;
        try {
            tblAppEngEstDoc = tblAppEngEstDocDao.findTblAppEngEstDoc(values);
        } catch (Exception e) {
            LOGGER.error("getAppEngEstDocDetails : " + logUserId + e);
    }
        LOGGER.debug("getAppEngEstDocDetails : " + logUserId + LOGGEREND);
        return tblAppEngEstDoc;
    }

    @Override
    public int checkInWatchList(Object... values) {
        LOGGER.debug("checkInWatchList : " + logUserId + LOGGERSTART);
        int count = 0;
        try {
            List<TblAppWatchList> watchList = tblAppWatchListDao.findTblAppWatchList(values);
            if (watchList.isEmpty()) {
                count = 0;
            } else {
                count = watchList.get(0).getAppWatchId();
            }
        } catch (Exception ex) {
            count = 0;
            LOGGER.error("checkInWatchList : " + logUserId + ex);
            }
        LOGGER.debug("checkInWatchList : " + logUserId + LOGGEREND);
        return count;
        }

    @Override
    public boolean removeFromWatchList(int watchId) {
        LOGGER.debug("removeFromWatchList : " + logUserId + LOGGERSTART);
        boolean isWatchList = false;
        String action = "App Removed from Watchlist";
        TblAppWatchList tblAppWatchList = new TblAppWatchList();
        try {
                if(tblAppWatchListDao.findTblAppWatchList("appWatchId",Operation_enum.EQ,watchId)!=null && !tblAppWatchListDao.findTblAppWatchList("appWatchId",Operation_enum.EQ,watchId).isEmpty()){
                    tblAppWatchList = tblAppWatchListDao.findTblAppWatchList("appWatchId",Operation_enum.EQ,watchId).get(0);
                }
            String query = "Delete from TblAppWatchList watchLst where watchLst.appWatchId=" + watchId;
            int cnt = hibernateQueryDao.updateDeleteNewQuery(query);
            if (cnt > 0) {
                isWatchList = true;
            } else {
                isWatchList = false;
        }
        } catch (Exception ex) {
            isWatchList = false;
            LOGGER.error("removeFromWatchList : " + logUserId + ex);
            action = "Error in "+action+" "+ex.getMessage();
        }finally {
            makeAuditTrailService.generateAudit(auditTrail, tblAppWatchList.getTblLoginMaster().getUserId(), "userId", EgpModule.WATCH_LIST.getName(), action, "App ID = "+tblAppWatchList.getTblAppMaster().getAppId());
            action = null;
            tblAppWatchList = null;
        }
        
        LOGGER.debug("removeFromWatchList : " + logUserId + LOGGEREND);
        return isWatchList;

    }

    @Override
    public Boolean delPkgDetail(int pkgId,int appId) throws Exception {
        LOGGER.debug("delPkgDetail : " + logUserId + LOGGERSTART);
        Boolean isDelPkgDetail;
        String auditAction = "";
        
        try {
           isDelPkgDetail = appPackage.executeProcedure(0, null, null, null, null,null,null,null,null,null, BigDecimal.ZERO, BigDecimal.ZERO, null, BigDecimal.ZERO, 0, null, null, 0, null, null, null, null, 0, "Delete", pkgId, null, null, null, null, null, null,null, null, null, null).get(0).getFlag();
            auditAction = "Remove Package";
        } catch (Exception e) {
            isDelPkgDetail = false;
            LOGGER.error("delPkgDetail : " + logUserId + e);
            auditAction = "Remove Package";
        }finally{
            makeAuditTrailService.generateAudit(auditTrail,appId , "appId", EgpModule.APP.getName(), auditAction, "");
            auditAction = null;
        }
        LOGGER.debug("delPkgDetail : " + logUserId + LOGGEREND);
       return isDelPkgDetail;
      //return false;
    }

    @Override
    public String[] getTendererMailByCPVCode(int appId) throws Exception {
        LOGGER.debug("getTendererMailByCPVCode : " + logUserId + LOGGERSTART);
        String[] strings = null;
        try {
            String query = "select lm.emailId from TblLoginMaster lm,TblCompanyMaster cm where lm.userid=cm.userId and cm.specialization in (select ap.cpvCode from TblAppPackages ap where ap.appId = " + appId + ")";
            strings = ((String[]) hibernateQueryDao.getSingleColQuery(query).toArray());
        } catch (Exception e) {
            LOGGER.error("getTendererMailByCPVCode : " + logUserId + e);
    }
        LOGGER.debug("getTendererMailByCPVCode : " + logUserId + LOGGEREND);
        return strings;
    }

    @Override
    public String getAppAuthority(int aaEmpId) throws Exception {
        LOGGER.debug("getAppAuthority : " + logUserId + LOGGERSTART);
        String string = null;
        try {
            String query = "select tpr.procurementRole from TblProcurementRole tpr where tpr.procurementRoleId = " + aaEmpId;
            string = ((String) (hibernateQueryDao.getSingleColQuery(query).toArray())[0]);
        } catch (Exception e) {
            LOGGER.error("getAppAuthority : " + logUserId + e);
    }
        LOGGER.debug("getAppAuthority : " + logUserId + LOGGEREND);
        return string;
    }

    @Override
    public Boolean isPEorAU(int userId) throws Exception {
        LOGGER.debug("isPEorAU : " + logUserId + LOGGERSTART);
        Boolean isPEorAu = false;
        try {
            String query = "select pm.procurementRole from TblEmployeeRoles e,TblEmployeeMaster em,TblProcurementRole pm where e.tblEmployeeMaster.employeeId=em.employeeId and e.tblProcurementRole.procurementRoleId=pm.procurementRoleId and em.tblLoginMaster.userId = " + userId;
            Object str[] = ((Object[]) hibernateQueryDao.getSingleColQuery(query).toArray());
            for (int i = 0; i < str.length; i++) {
                if ("PE".equalsIgnoreCase((String) str[i]) || "AU".equalsIgnoreCase((String) str[i])) {
                    isPEorAu = true;
            }
        }
        } catch (Exception e) {
            isPEorAu = false;
            LOGGER.error("isPEorAU : " + logUserId + e);
    }
        LOGGER.debug("isPEorAU : " + logUserId + LOGGEREND);
        return isPEorAu;
    }

    @Override
    public Boolean chkPkgNo(String pkgNo, int pkgId) throws Exception {
        LOGGER.debug("chkPkgNo : " + logUserId + LOGGERSTART);
        boolean isPkgNo;
        try {
            String query = "select count(p.packageNo) as cnt from TblAppMaster a,TblAppPackages p "
                    + "where a.appId=p.tblAppMaster.appId"
//                    + " and a.tblOfficeMaster.officeId in(select eo.tblOfficeMaster.officeId "
//                    + "from TblEmployeeMaster e,TblEmployeeOffices eo "
//                    + "where e.employeeId=eo.tblEmployeeMaster.employeeId and a.tblLoginMaster.userId=" + userId + ") "
                    + " and p.packageNo='" + pkgNo + "' and p.packageId <> "+pkgId;
            Long cnt = ((Long) hibernateQueryDao.getSingleColQuery(query).get(0));
            if (cnt > 0) {
                isPkgNo = true;
            } else {
                isPkgNo = false;
        }
        } catch (Exception e) {
            isPkgNo = false;
            LOGGER.error("chkPkgNo : " + logUserId + e);
    }
        LOGGER.debug("chkPkgNo : " + logUserId + LOGGEREND);
        return isPkgNo;
    }

    @Override
    public String getPkgStatus(int appId, int pkgId) throws Exception {
        LOGGER.debug("getPkgStatus : " + logUserId + LOGGERSTART);
        String string = null;
        try {
            String query = "select a.appStatus from TblAppPackages a where a.tblAppMaster.appId = " + appId + " and a.packageId = " + pkgId;
            string = ((String) (hibernateQueryDao.getSingleColQuery(query).toArray())[0]);
        } catch (Exception e) {
            LOGGER.error("getPkgStatus : " + logUserId + e);
    }
        LOGGER.debug("getPkgStatus : " + logUserId + LOGGEREND);
        return string;
    }

    @Override
    public List<TblAppPackages> getCPVCodeByappId(int appId) {
        LOGGER.debug("getCPVCodeByappId : " + logUserId + LOGGERSTART);
        List<TblAppPackages> listPackages = null;
        try {
            listPackages = tblAppPackagesDao.findTblAppPackages("tblAppMaster", Operation_enum.EQ, new TblAppMaster(appId), "appStatus", Operation_enum.IN,new String[]{"Pending","BeingRevised"});
        } catch (Exception ex) {
            LOGGER.error("getCPVCodeByappId : " + logUserId + ex);
        }
        LOGGER.debug("getCPVCodeByappId : " + logUserId + LOGGEREND);
        return listPackages;
    }

     @Override
     public boolean isBusRuleConfig(int pkgId){

        LOGGER.debug("isBusRuleConfig : " + logUserId + LOGGERSTART);
        int count = 0;
        boolean flag = false;
        
        try 
        {
            List<SPTenderCommonData> list = sPTenderCommon.executeProcedure("GetAppPackageCount",""+pkgId, "");
            count = Integer.parseInt(list.get(0).getFieldName1());
            if(count!=0){
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("isBusRuleConfig : " + logUserId + ex);
        }
        LOGGER.debug("isBusRuleConfig : " + logUserId + LOGGEREND);
        return flag;
        
     }
     
    @Override
     public boolean isprocMethodRuleConfig(int pkgId){
         LOGGER.debug("isprocMethodRuleConfig : " + logUserId + LOGGERSTART);
        int count = 0;
        boolean flag = false;
        
        try 
        {
            List<SPTenderCommonData> list = sPTenderCommon.executeProcedure("GetAppProcurementCount",""+pkgId, "");
            count = Integer.parseInt(list.get(0).getFieldName1());
            if(count!=0){
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("isprocMethodRuleConfig : " + logUserId + ex);
        }
        LOGGER.debug("isprocMethodRuleConfig : " + logUserId + LOGGEREND);
        return flag;
     }
     
    
     
     @Override
     public String[] getMinMax(String pkgNo, String dptType){
         LOGGER.debug("getMinMax : " + logUserId + LOGGERSTART);
        String[] value = new String[7];
         
        try 
        {
            List<SPTenderCommonData> list = sPTenderCommon.executeProcedure("GetMaxMin",""+pkgNo,""+dptType);
            value[0] = list.get(0).getFieldName1();
            value[1] = list.get(0).getFieldName2();
            value[2] = list.get(0).getFieldName3();
            value[3] = list.get(0).getFieldName4();
        } catch (Exception ex) {
            LOGGER.error("getMinMax : " + logUserId + ex);
        }
        LOGGER.debug("getMinMax : " + logUserId + LOGGEREND);
        return value;
     }
     
     
     
     
     
     
    @Override 
    public boolean isHopaExist(String userID){
    LOGGER.debug("isHopaExist : " + logUserId + LOGGERSTART);
        boolean flag = true;
         
        try 
        {
            List<SPTenderCommonData> list = sPTenderCommon.executeProcedure("CheckforHopaExist",userID, "");
            if(list.isEmpty()){
              flag = false;
            }
        } catch (Exception ex) {
            LOGGER.error("isHopaExist : " + logUserId + ex);
        }
        LOGGER.debug("isHopaExist : " + logUserId + LOGGEREND);
        return flag;
    }
    
    @Override
    public Date getProjectEndDate(int prjId){

        LOGGER.debug("getProjectEndDate : " + logUserId + LOGGERSTART);
        List<Object> list = null;
        Date strEndDate = null;

        try {
            String sqlQuery = "select pm.projectEndDate from TblProjectMaster pm where pm.projectId = "+prjId;            
            list = hibernateQueryDao.singleColQuery(sqlQuery);

            if(!list.isEmpty()){
                strEndDate = (Date) list.get(0);
            }
        } catch (Exception ex) {
            LOGGER.error("getProjectEndDate : " + logUserId + ex);
        }
        
        LOGGER.debug("getProjectEndDate : " + logUserId + LOGGEREND);
        return strEndDate;
    }

    @Override
    public List<Object[]> getWorkflowHistorylink(int appId) {
        LOGGER.debug("getWorkflowHistorylink : " + logUserId + LOGGERSTART);
        List<Object[]> list = new ArrayList<Object[]>();
        
        try{
            String sqlQuery = "select distinct twf.objectId,twf.childId from TblWorkFlowFileHistory twf where twf.objectId="+appId;
            list = hibernateQueryDao.createQuery(sqlQuery);
        }catch(Exception ex){
             LOGGER.error("getWorkflowHistorylink : " + logUserId + ex);
        }
        
        LOGGER.debug("getWorkflowHistorylink : " + logUserId + LOGGEREND);
        return list;
    }

    @Override
    public List<Object> getFileOnHandStatus(int ObjectId,int childId,int userId,int eventId){
        LOGGER.debug("getFileOnHandStatus : " + logUserId + LOGGERSTART);
        List<Object> list = new ArrayList<Object>();

        try{
            String sqlQuery = "select twf.fileOnHand from TblWorkFlowLevelConfig twf where twf.objectId="+ObjectId+" and twf.childId="+childId+" and twf.tblLoginMaster.userId="+userId+" and twf.eventId="+eventId+" order by twf.wfRoleId asc";
            list = hibernateQueryDao.singleColQuery(sqlQuery);
        }catch(Exception ex){
             LOGGER.error("getFileOnHandStatus : " + logUserId + ex);
        }

        LOGGER.debug("getFileOnHandStatus : " + logUserId + LOGGEREND);
        return list;
    }
    
    @Override
    public String chkPkgNoExist(String pkgNo, int userId) throws Exception
    {
        LOGGER.debug("chkPkgNoExist : " + logUserId + LOGGERSTART);
          
         String pkgid=null;
        try {
            String query = "select p.packageId from TblAppMaster a,TblAppPackages p "
                    + "where a.appId=p.tblAppMaster.appId"
                    + " and p.packageNo='" + pkgNo + "'";
            List<Object> list = hibernateQueryDao.singleColQuery(query);
            if(list != null && !list.isEmpty())
            {
                pkgid=list.get(0).toString();
            }
        }
        catch (Exception e) {
           
            LOGGER.error("chkPkgNoExist : " + logUserId + e);
    }
        LOGGER.debug("chkPkgNoExist : " + logUserId + LOGGEREND);
         return pkgid;
    }
    
    public boolean chkPkgDatesExist(String pkgNo) throws Exception
    {
        LOGGER.debug("chkPkgDatesExist : " + logUserId + LOGGERSTART);
        boolean isPkgDatesExist;
         
        int cnt=-1;
        try {
            String query = "select count(a.pqDtId) as cnt from TblAppPqTenderDates a "
                    + "where a.tblAppPackages.packageId ='"+pkgNo + "'";
            List<Object> list=hibernateQueryDao.getSingleColQuery(query);
            if(list != null && !list.isEmpty())
            {
                cnt = Integer.parseInt(list.get(0).toString());
            }
            System.out.println("--------------------------------------cnt="+cnt);
            if (cnt > 0) {
                isPkgDatesExist = true;
            } else {
                isPkgDatesExist = false;
        }
        } catch (Exception e) {
            isPkgDatesExist = false;
            e.printStackTrace();
            LOGGER.error("chkPkgDatesExist : " + logUserId + e);
    }
        LOGGER.debug("chkPkgDatesExist : " + logUserId + LOGGEREND);
        System.out.println("===================================================is package exist ="+isPkgDatesExist);
        return isPkgDatesExist;
    }

    @Override
    public List<TblAppPackages> getAPPDetailsByPackageNo(String pkgNo) throws Exception{
        LOGGER.debug("getAPPDetailsByPackageNo : " + logUserId + LOGGERSTART);
        LOGGER.debug("getAPPDetailsByPackageNo : " + logUserId + LOGGEREND);    
        return tblAppPackageDao.findTblAppPackages("packageNo", Operation_enum.EQ, pkgNo);
    }

    @Override
    public CommonAppData getOfficeInfoFromAPP(String param1, String param2) throws Exception 
    {
        LOGGER.debug("getOfficeInfoFromAPP : " + logUserId + LOGGERSTART);
        
        try {
            return appCommonData.executeProcedure(param1, param2, "").get(0);
        } catch (Exception e) {
            LOGGER.error("getOfficeInfoFromAPP : " + logUserId + e);
        }
        LOGGER.debug("getOfficeInfoFromAPP : " + logUserId + LOGGEREND);
        
        return new CommonAppData();
    }

    @Override
    public CommonAppData getAPPFromCode(String code) throws Exception {
        LOGGER.debug("getAPPFromCode : " + logUserId + LOGGERSTART);
        
        try {
            //List<CommonAppData> data = appCommonData.executeProcedure("AppCodeFilter", code, "");
            return appCommonData.executeProcedure("AppCodeFilter", code, "").get(0);
        } catch (Exception e) {
            LOGGER.error("getAPPFromCode : " + logUserId + e);
        }
        LOGGER.debug("getAPPFromCode : " + logUserId + LOGGEREND);
        
        return new CommonAppData();
    }
        
    @Override
    public List<CommonSPReturn> publishApp(String action, Integer appId, String pkgNo) throws Exception
    {
        LOGGER.debug("publishApp : " + logUserId + LOGGERSTART);
        List<CommonSPReturn> sPReturn = null;
        String auditAction = "";
        try {
            sPReturn = appPackage.executeProcedure(appId,"","",pkgNo,"","","","","","",BigDecimal.ZERO,BigDecimal.ZERO,"",BigDecimal.ZERO,0,"","",0,"","","","",0,action,0,"","","","","","","","","","");
            auditAction = "Publish App";
            
        } catch (Exception e) {
            LOGGER.error("publishApp : " + logUserId + e);
            auditAction = "Error in Publish App";
            
        }finally {
            makeAuditTrailService.generateAudit(auditTrail,appId , "appId", EgpModule.APP.getName(), auditAction, "");
            auditAction = null;
        }
        LOGGER.debug("publishApp : " + logUserId + LOGGEREND);
        return sPReturn;
    }
}
