/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceinterface;

import com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author rikin.p
 */
public interface TenderCoriGrandSumDetailService {
    
     public int insertDataForGrandSummary(List<TblTenderCoriGrandSumDetail> lstGsDtl,int gsId,String corriId);
     public boolean checkCoriExistInCoriDetailTbl(String tenderGSId,String corriId);
     public Map<Integer, Integer> getCorriGSFormsTblDtl(String tenderGSId,String corriId);
     public Map<Integer, Integer> getCorriGSFormsDtl(String tenderGSId,String corriId);
     public String getTenderGrandSumId(String tenderId);
     public int performGrandSumCorriPublish(String tenderId, String corriId);
     
}
