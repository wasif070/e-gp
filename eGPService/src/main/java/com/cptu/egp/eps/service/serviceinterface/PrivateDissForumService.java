/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceinterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDao;
import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDocumentDao;
import com.cptu.egp.eps.model.table.TblPriTopicMaster;
import com.cptu.egp.eps.model.table.TblPriTopicReply;
import com.cptu.egp.eps.model.table.TblPriTopicReplyDocument;


/**
 *
 * @author dipal.shah
 */
public interface PrivateDissForumService {

    /**
     * This method is use for post Topic.
     * @param objTopicMaster
     * @param objTopicDoc
     * @return boolean if insertion done successfully else return false.
     */
    public boolean postTopic(TblPriTopicMaster objTopicMaster, TblPriTopicReplyDocument objTopicDoc);

    /**
     * This method is use for post Response.
     * @param objTopicMaster
     * @param objTopicDoc
     * @return boolean if insertion done successfully else return false.
     */
    public boolean postResponse(TblPriTopicReply objTopicReply, TblPriTopicReplyDocument objReplyDoc);

    public List<TblPriTopicMaster> searchTopicName(int offcet,int row,int tenderId, String searchField,String searchString,String searchOper,String orderby,String colName);

    public long getSearchTopicCount(int tenderId,String searchField, String searchString, String searchOper);

    public List<TblPriTopicMaster> getTopicName(int offcet, int row,int tenderId, String orderby, String colName);

    public long getTopicCount(int tenderId);

    public List<TblPriTopicReply> getReply(int topicId,int status);

    public List<TblPriTopicReply> getParticularReply(int topicReplyId);

    public List<TblPriTopicMaster> getTopic(int topicId);

    /**
     * This method is use to get uploaded document details
     * @param id -- Topic id / Response Id
     * @param type --  'Topic' = if want to get document of specific topic / 'Response' = if want to get document of specific Response
     * @return List of Document Details
     */
    public List<TblPriTopicReplyDocument> getDocumentDetail(int id,String type);

    /**
     * THis method will update read count for view reply.
     * @param topicReplyId
     * @return true if operation performed successfully else return false.
     */
    public boolean updateReadCountResponse(TblPriTopicReply tblPriTopicReply);

    /**
     * This method is use to find Total Post.
     * @return Total Post.
     */
    public long getTotalPostedReply(int tenderId);

    
}
