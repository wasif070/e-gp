/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.daointerface.TblContractAwardedOfflineDao;
import com.cptu.egp.eps.dao.daointerface.TblTenderOfflineDataDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import com.cptu.egp.eps.service.serviceinterface.OfflineDataService;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class OfflineDataServiceImpl implements OfflineDataService{
    static final Logger LOGGER = Logger.getLogger(OfflineDataServiceImpl.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private TblTenderOfflineDataDao offlineDataDao;
    private TblContractAwardedOfflineDao contractAwardedOfflineDao;

    @Override
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * @return the offlineDataDao
     */
    public TblTenderOfflineDataDao getOfflineDataDao() {
        return offlineDataDao;
    }

    /**
     * @param offlineDataDao the offlineDataDao to set
     */
    public void setOfflineDataDao(TblTenderOfflineDataDao offlineDataDao) {
        this.offlineDataDao = offlineDataDao;
    }

//    @Override
    public void saveTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData) {
         LOGGER.debug("savetenderdetailofflinedata : " + logUserId + LOGGERSTART);
        try{
        offlineDataDao.addTblTenderDetailOfflineData(tenderDetailOfflineData);
        }catch(Exception ex){
        System.out.println("savetenderdetailofflinedata : " + logUserId + ex);
        LOGGER.error("savetenderdetailofflinedata : " + logUserId + ex);
        }
        LOGGER.debug("savetenderdetailofflinedata : " + logUserId + LOGGEREND);
    }

    @Override
    public void deleteTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData) {
         LOGGER.debug("deleteTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        try{
            offlineDataDao.deleteTblTenderDetailOfflineData(tenderDetailOfflineData);
        }catch(Exception ex){
        LOGGER.error("deleteTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("deleteTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
    }

  //  @Override
    public void updateTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData) {
         LOGGER.debug("updateTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        try{
             offlineDataDao.updateTblTenderDetailOfflineData(tenderDetailOfflineData);
         }catch(Exception ex){
        LOGGER.error("updateTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("updateTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
    }

  //  @Override
    public long getCountForTblTenderDetailOfflineData() {
        LOGGER.debug("getCountForTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        long count = 0;
        try{
           count = offlineDataDao.getTblTenderDetailOfflineDataCount();
         }catch(Exception ex){
        LOGGER.error("getCountForTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("getCountForTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
        return count;
    }

    @Override
    public List<TblTenderDetailsOffline> getAllTblTenderDetailOfflineData() {
        List<TblTenderDetailsOffline> detailOfflineDatas = null ;
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        try{
        detailOfflineDatas = offlineDataDao.getAllTblTenderDetailOfflineData();
         }catch(Exception ex){
        LOGGER.error("getAllTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
        return detailOfflineDatas;
    }

     /**
     * get TblTenderDetailsOffline by Id
     * @return list of TblTenderDetailsOffline
     */
   // @Override
    public List<TblTenderDetailsOffline> getTblTenderDetailOfflineDataById(int tenderOLId){
    
         List<TblTenderDetailsOffline> detailOfflineDatas = null ;
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        try{
        detailOfflineDatas = offlineDataDao.findTblTenderDetailOfflineData("tenderOfflineId",Operation_enum.EQ,tenderOLId);
         }catch(Exception ex){
        LOGGER.error("getAllTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
        return detailOfflineDatas;
        
    }

    @Override
    public void saveTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline) {
         LOGGER.debug("saveTblContractAwardedOffline : " + logUserId + LOGGERSTART);
        try{
        contractAwardedOfflineDao.addTblContractAwardedOffline(contractAwardedOffline);
        }catch(Exception ex){
        LOGGER.error("saveTblContractAwardedOffline : " + logUserId + ex);
        }
        LOGGER.debug("saveTblContractAwardedOffline : " + logUserId + LOGGEREND);
    }

    @Override
    public void deleteTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline) {
          LOGGER.debug("deleteTblContractAwardedOffline : " + logUserId + LOGGERSTART);
        try{
            contractAwardedOfflineDao.deleteTblContractAwardedOffline(contractAwardedOffline);
        }catch(Exception ex){
        LOGGER.error("deleteTblContractAwardedOffline : " + logUserId + ex);
        }
        LOGGER.debug("deleteTblContractAwardedOffline : " + logUserId + LOGGEREND);
    }

    @Override
    public void updateTblContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline) {
         LOGGER.debug("updateTblContractAwardedOffline : " + logUserId + LOGGERSTART);
        try{
            contractAwardedOfflineDao.updateTblContractAwardedOffline(contractAwardedOffline);
         }catch(Exception ex){
        LOGGER.error("updateTblContractAwardedOffline : " + logUserId + ex);
        }
        LOGGER.debug("updateTblContractAwardedOffline : " + logUserId + LOGGEREND);
    }

      @Override
    public List<TblContractAwardedOffline> getTblContractAwardedOfflineById(int tenderOLId) {
        List<TblContractAwardedOffline> awardedOfflines = null ;
        LOGGER.debug("getTblContractAwardedOfflineById : " + logUserId + LOGGERSTART);
        try{
        awardedOfflines = contractAwardedOfflineDao.findTblContractAwardedOffline("conAwardOfflineId",Operation_enum.EQ,tenderOLId);
         }catch(Exception ex){
        LOGGER.error("getTblContractAwardedOfflineById : " + logUserId + ex);
        }
        LOGGER.debug("getTblContractAwardedOfflineById : " + logUserId + LOGGEREND);
        return awardedOfflines;
    }

    @Override
    public long getCountForTblContractAwardedOfflineData() {
         LOGGER.debug("getCountForTblContractAwardedOfflineData : " + logUserId + LOGGERSTART);
         long count = 0;
        try{
            count = contractAwardedOfflineDao.getTblContractAwardedOfflineCount();
         }catch(Exception ex){
        LOGGER.error("getCountForTblContractAwardedOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("getCountForTblContractAwardedOfflineData : " + logUserId + LOGGEREND);
        return count;
    }

    @Override
    public List<TblContractAwardedOffline> getAllTblContractAwardedOfflineData() {
        List<TblContractAwardedOffline> contractAwardedOfflines = null ;
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGERSTART);
        try{
        contractAwardedOfflines = contractAwardedOfflineDao.getAllTblContractAwardedOffline();
         }catch(Exception ex){
        LOGGER.error("getAllTblTenderDetailOfflineData : " + logUserId + ex);
        }
        LOGGER.debug("getAllTblTenderDetailOfflineData : " + logUserId + LOGGEREND);
        return contractAwardedOfflines;
    }


    /**
     * @return the contractAwardedOfflineDao
     */
    public TblContractAwardedOfflineDao getContractAwardedOfflineDao() {
        return contractAwardedOfflineDao;
    }

    /**
     * @param contractAwardedOfflineDao the contractAwardedOfflineDao to set
     */
    public void setContractAwardedOfflineDao(TblContractAwardedOfflineDao contractAwardedOfflineDao) {
        this.contractAwardedOfflineDao = contractAwardedOfflineDao;
    }

  

}
