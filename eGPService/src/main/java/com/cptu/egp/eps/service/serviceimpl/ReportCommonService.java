/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.dao.storedprocedure.SPReportCommon;
import com.cptu.egp.eps.dao.storedprocedure.SPReportCommonData;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Kinjal Shah
 */
public class ReportCommonService {

    static final Logger logger = Logger.getLogger(ReportCommonService.class);
    SPReportCommon sPReportCommon;
    

    public SPReportCommon getsPReportCommon() {
        return sPReportCommon;
    }

    public void setsPReportCommon(SPReportCommon sPReportCommon) {
        this.sPReportCommon = sPReportCommon;
    }

    public List<SPReportCommonData> returndata(int formId, int userId) {
        return sPReportCommon.executeProcedure(formId, userId);
    }
   SPReportCommon sPReportComp;

    public SPReportCommon getsPReportComp() {
        return sPReportComp;
    }

    public void setsPReportComp(SPReportCommon sPReportComp) {
        this.sPReportComp = sPReportComp;
    }

    public List<SPReportCommonData> returnCompReport(int formId, int userid) {
        return sPReportComp.executeProcedure(formId, userid);
    }

    
}
