/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.service.serviceimpl;

import com.cptu.egp.eps.service.serviceinterface.DomesticPreferenceService;
import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Ahsan
 */
public class DomesticPreferenceServiceImpl implements DomesticPreferenceService {

    private static final Logger LOGGER = Logger.getLogger(ContractAwardOfflineServiceImpl.class);
   private String logUserId = "0";
   private HibernateQueryDao hibernateQueryDao;
   private String getMinistryList = "getMinistryList : ";
   private String getOrgList = "getMinistryList : ";
   private String loggerStart = " Starts";
   private String loggerEnd = " Ends";

   @Override
   public Object IsDomesticPreferece(int tenderId)
    {
        LOGGER.debug(getMinistryList + logUserId + loggerStart);
   
        Object preference = null;
        try {
            String query = "select count(tenderId) as domesticPreference from TblBidderRank "+
                    "where tenderId = "+ tenderId +" and "+
                    "userId = (select MAX(userId) from TblTenderDomesticPref where domesticPref = 'Yes' and tenderId = "+ tenderId + ")";
            preference = getHibernateQueryDao().createQuery(query).get(0);
      } catch (Exception ex) {
            LOGGER.debug(getMinistryList + logUserId + ex);
        }
        LOGGER.debug(getMinistryList + logUserId + loggerEnd);
            return  preference;
    }

    /**
     * @return the hibernateQueryDao
     */
    public HibernateQueryDao getHibernateQueryDao() {
        return hibernateQueryDao;
    }

    /**
     * @param hibernateQueryDao the hibernateQueryDao to set
     */
    public void setHibernateQueryDao(HibernateQueryDao hibernateQueryDao) {
        this.hibernateQueryDao = hibernateQueryDao;
    }
}
