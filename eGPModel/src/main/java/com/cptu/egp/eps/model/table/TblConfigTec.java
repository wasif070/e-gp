package com.cptu.egp.eps.model.table;
// Generated Nov 15, 2010 5:06:33 PM by Hibernate Tools 3.2.1.GA

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblConfigTec generated by hbm2java
 */
@Entity
@Table(name = "tbl_ConfigTec", schema = "dbo")
public class TblConfigTec  implements java.io.Serializable {

     private int configTec;
     private TblProcurementNature tblProcurementNature;
     private String committeeType;
     private BigDecimal minTenderVal;
     private BigDecimal maxTenderVal;
     private byte minMemReq;
     private byte maxMemReq;
    private byte minMemOutSidePe;
     private byte minMemFromPe;
     private byte minMemFromTec;
     private byte minApproval;

    public TblConfigTec() {
    }

    //Constructor
    public TblConfigTec(int configTec) {
        this.configTec = configTec;
    }

    public TblConfigTec(int configTec, TblProcurementNature tblProcurementNature, String committeeType, BigDecimal minTenderVal, BigDecimal maxTenderVal, byte minMemReq, byte maxMemReq, byte minMemOutSidePe, byte minMemFromPe, byte minMemFromTec, byte minApproval) {
       this.configTec = configTec;
       this.tblProcurementNature = tblProcurementNature;
       this.committeeType = committeeType;
       this.minTenderVal = minTenderVal;
       this.maxTenderVal = maxTenderVal;
       this.minMemReq = minMemReq;
       this.maxMemReq = maxMemReq;
        this.minMemOutSidePe = minMemOutSidePe;
       this.minMemFromPe = minMemFromPe;
       this.minMemFromTec = minMemFromTec;
        this.minApproval = minApproval;
    }

     @Id
     @GeneratedValue(generator = "ConfigTecTypeSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ConfigTecTypeSequence", sequenceName = "configTec_sequence", allocationSize = 25)
    @Column(name = "configTec", unique = true, nullable = false)
    public int getConfigTec() {
        return this.configTec;
    }

    public void setConfigTec(int configTec) {
        this.configTec = configTec;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "procurementNatureId", nullable = false)
    public TblProcurementNature getTblProcurementNature() {
        return this.tblProcurementNature;
    }

    public void setTblProcurementNature(TblProcurementNature tblProcurementNature) {
        this.tblProcurementNature = tblProcurementNature;
    }

    @Column(name = "committeeType", nullable = false, length = 3)
    public String getCommitteeType() {
        return this.committeeType;
    }

    public void setCommitteeType(String committeeType) {
        this.committeeType = committeeType;
    }

    @Column(name = "minTenderVal", nullable = false, scale = 4)
    public BigDecimal getMinTenderVal() {
        return this.minTenderVal;
    }

    public void setMinTenderVal(BigDecimal minTenderVal) {
        this.minTenderVal = minTenderVal;
    }

    @Column(name = "maxTenderVal", nullable = false, scale = 4)
    public BigDecimal getMaxTenderVal() {
        return this.maxTenderVal;
    }

    public void setMaxTenderVal(BigDecimal maxTenderVal) {
        this.maxTenderVal = maxTenderVal;
    }

    @Column(name = "minMemReq", nullable = false)
    public byte getMinMemReq() {
        return this.minMemReq;
    }

    public void setMinMemReq(byte minMemReq) {
        this.minMemReq = minMemReq;
    }

    @Column(name = "maxMemReq", nullable = false)
    public byte getMaxMemReq() {
        return this.maxMemReq;
    }

    public void setMaxMemReq(byte maxMemReq) {
        this.maxMemReq = maxMemReq;
    }

    @Column(name = "minMemOutSidePe", nullable = false)
    public byte getMinMemOutSidePe() {
        return this.minMemOutSidePe;
    }

    public void setMinMemOutSidePe(byte minMemOutSidePe) {
        this.minMemOutSidePe = minMemOutSidePe;
    }

    @Column(name = "minMemFromPe", nullable = false)
    public byte getMinMemFromPe() {
        return this.minMemFromPe;
    }

    public void setMinMemFromPe(byte minMemFromPe) {
        this.minMemFromPe = minMemFromPe;
    }

    @Column(name = "minMemFromTec", nullable = false)
    public byte getMinMemFromTec() {
        return this.minMemFromTec;
    }

    public void setMinMemFromTec(byte minMemFromTec) {
        this.minMemFromTec = minMemFromTec;
    }

    @Column(name = "minApproval")
    public byte getMinApproval() {
        return this.minApproval;
    }

    public void setMinApproval(byte minApproval) {
        this.minApproval = minApproval;
    }
}
