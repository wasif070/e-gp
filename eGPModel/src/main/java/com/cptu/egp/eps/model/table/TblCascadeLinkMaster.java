/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.model.table;

/**
 *
 * @author tejasree.goriparthi
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_cascade_link_master", schema = "dbo")
public class TblCascadeLinkMaster  implements java.io.Serializable {
    private int linkId;
    private String linkName;
    private int parentLinkId;
    private String userTypeId;
    private String hyperLinkPageName;

    //constructor
    public TblCascadeLinkMaster() {
    }

    public TblCascadeLinkMaster(int linkId) {
        this.linkId = linkId;
    }

    public TblCascadeLinkMaster(int linkId, String linkName, int parentLinkId, String userTypeId, String hyperLinkPageName) {
        this.linkId = linkId;
        this.linkName = linkName;
        this.parentLinkId = parentLinkId;
        this.userTypeId = userTypeId;
        this.hyperLinkPageName = hyperLinkPageName;
    }

    @Id
    @GeneratedValue(generator = "cascadeMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "cascadeMasterSequence", sequenceName = "cascadeMaster_sequence", allocationSize = 25)
    @Column(name = "linkId", unique = true, nullable = false)
    public int getLinkId() {
        return linkId;
    }

    public void setLinkId(int linkId) {
        this.linkId = linkId;
    }

    @Column(name = "linkName", nullable = false)
    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    @Column(name = "parentLinkId", nullable = false)
    public int getParentLinkId() {
        return parentLinkId;
    }

    public void setParentLinkId(int parentLinkId) {
        this.parentLinkId = parentLinkId;
    }

    @Column(name = "userTypeId", nullable = false)
    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

     @Column(name = "hyperLinkPageName", nullable = false)
    public String getHyperLinkPageName() {
        return hyperLinkPageName;
    }

    public void setHyperLinkPageName(String hyperLinkPageName) {
        this.hyperLinkPageName = hyperLinkPageName;
    }



}
