package com.cptu.egp.eps.model.table;
// Generated Feb 7, 2011 3:43:32 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblEvalBidderStatus generated by hbm2java
 */
@Entity
@Table(name = "tbl_EvalBidderStatus", schema = "dbo")
public class TblEvalBidderStatus  implements java.io.Serializable {

     private int evaBidderStatusId;
     private int tenderId;
     private int userId;
     private String bidderStatus;
     private int evalBy;
     private Date evalDt;
     private String bidderMarks;
     private String passingMarks;
     private String remarks;
     private String result;
     private String evalStatus;
     private int pkgLotId;
     private int evalCount;

    public TblEvalBidderStatus() {
    }

    public TblEvalBidderStatus(int evaBidderStatusId, int tenderId, int userId, String bidderStatus, int evalBy, Date evalDt) {
       this.evaBidderStatusId = evaBidderStatusId;
       this.tenderId = tenderId;
       this.userId = userId;
       this.bidderStatus = bidderStatus;
       this.evalBy = evalBy;
       this.evalDt = evalDt;
    }

    @Id
    @GeneratedValue(generator = "TblEvalBidderStatusSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblEvalBidderStatusSequence", sequenceName = "tblEvalBidderStatus_sequence", allocationSize = 25)
    @Column(name = "evaBidderStatusId", unique = true, nullable = false)
    public int getEvaBidderStatusId() {
        return this.evaBidderStatusId;
    }

    public void setEvaBidderStatusId(int evaBidderStatusId) {
        this.evaBidderStatusId = evaBidderStatusId;
    }

    @Column(name = "tenderId", nullable = false)
    public int getTenderId() {
        return this.tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    @Column(name = "userId", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "bidderStatus", nullable = false, length = 50)
    public String getBidderStatus() {
        return this.bidderStatus;
    }

    public void setBidderStatus(String bidderStatus) {
        this.bidderStatus = bidderStatus;
    }

    @Column(name = "evalBy", nullable = false)
    public int getEvalBy() {
        return this.evalBy;
    }

    public void setEvalBy(int evalBy) {
        this.evalBy = evalBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "evalDt", nullable = false, length = 16)
    public Date getEvalDt() {
        return this.evalDt;
    }

    public void setEvalDt(Date evalDt) {
        this.evalDt = evalDt;
    }

    @Column(name = "bidderMarks", length = 50)
    public String getBidderMarks() {
        return this.bidderMarks;
    }

    public void setBidderMarks(String bidderMarks) {
        this.bidderMarks = bidderMarks;
    }

    @Column(name = "passingMarks", length = 50)
    public String getPassingMarks() {
        return this.passingMarks;
    }

    public void setPassingMarks(String passingMarks) {
        this.passingMarks = passingMarks;
    }

    @Column(name = "remarks", length = 500)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "result", length = 10)
    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Column(name = "evalStatus", length = 15)
    public String getEvalStatus() {
        return evalStatus;
    }

    public void setEvalStatus(String evalStatus) {
        this.evalStatus = evalStatus;
    }

    @Column(name = "pkgLotId", nullable = false)
    public int getPkgLotId() {
        return this.pkgLotId;
    }

    public void setPkgLotId(int pkgLotId) {
        this.pkgLotId = pkgLotId;
    }

    /**
     * @return the evalCount
     */
    public int getEvalCount() {
        return evalCount;
    }

    /**
     * @param evalCount the evalCount to set
     */
    public void setEvalCount(int evalCount) {
        this.evalCount = evalCount;
    }
}
