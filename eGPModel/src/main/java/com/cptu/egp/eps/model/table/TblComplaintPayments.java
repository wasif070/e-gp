package com.cptu.egp.eps.model.table;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tbl_CMS_ComplaintPayment", schema = "dbo")
public class TblComplaintPayments implements java.io.Serializable {

    private int complaintPaymentId;
    //complaintId
    private TblComplaintMaster complaintMaster;
    private String paymentFor;
    private String paymentInstType;
    private String instRefNumber;
    private BigDecimal amount;
    private Date instDate;
    private Date instValidUpto;
    private Date instValidityDt;
    private String bankName;
    private String branchName;
    private String comments;
    private int createdBy;
    private Date createdDate;
    private String status;
    private String paymentMode;
    private String currency;
    private String issuanceBank;
    private String issuanceBranch;
    private String isVerified;
    private String isLive;
    private Date dtOfAction;
    private int partTransId;

    @Id
    @GeneratedValue(generator = "ComplaintPaymentsSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ComplaintPaymentsSequence", sequenceName = "ComplaintPaymentsSequence")
    @Column(name = "complaintPaymentId", unique = true, nullable = false)
    public int getComplaintPaymentId() {
        return complaintPaymentId;
    }

    public void setComplaintPaymentId(int complaintPaymentId) {
        this.complaintPaymentId = complaintPaymentId;
    }

    @ManyToOne
    @JoinColumn(name = "complaintId")
    public TblComplaintMaster getComplaintMaster() {
        return complaintMaster;
    }

    public void setComplaintMaster(TblComplaintMaster complaintMaster) {
        this.complaintMaster = complaintMaster;
    }

    @Column(name = "paymentFor")
    public String getPaymentFor() {
        return paymentFor;
    }

    public void setPaymentFor(String paymentFor) {
        this.paymentFor = paymentFor;
    }

    @Column(name = "paymentInstType")
    public String getPaymentInstType() {
        return paymentInstType;
    }

    public void setPaymentInstType(String paymentInstType) {
        this.paymentInstType = paymentInstType;
    }

    @Column(name = "instRefNumber")
    public String getInstRefNumber() {
        return instRefNumber;
    }

    public void setInstRefNumber(String instRefNumber) {
        this.instRefNumber = instRefNumber;
    }

    @Column(name = "amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "instDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getInstDate() {
        return instDate;
    }

    public void setInstDate(Date instDate) {
        this.instDate = instDate;
    }

    @Column(name = "instValidUpto")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getInstValidUpto() {
        return instValidUpto;
    }

    public void setInstValidUpto(Date instValidUpto) {
        this.instValidUpto = instValidUpto;
    }

    @Column(name = "instValidityDt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getInstValidityDt() {
        return instValidityDt;
    }

    public void setInstValidityDt(Date instValidityDt) {
        this.instValidityDt = instValidityDt;
    }

    @Column(name = "bankName")
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Column(name = "branchName")
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Column(name = "comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "createdBy")
    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "createdDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "paymentMode")
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Column(name = "currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "issuanceBank")
    public String getIssuanceBank() {
        return issuanceBank;
    }

    public void setIssuanceBank(String issuanceBank) {
        this.issuanceBank = issuanceBank;
    }

    @Column(name = "issuanceBranch")
    public String getIssuanceBranch() {
        return issuanceBranch;
    }

    public void setIssuanceBranch(String issuanceBranch) {
        this.issuanceBranch = issuanceBranch;
    }

    @Column(name = "isVerified")
    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    @Column(name = "isLive")
    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    @Column(name = "dtOfAction")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDtOfAction() {
        return dtOfAction;
    }

    public void setDtOfAction(Date dtOfAction) {
        this.dtOfAction = dtOfAction;
    }

    @Column(name = "partTransId")
    public int getPartTransId() {
        return partTransId;
    }

    public void setPartTransId(int partTransId) {
        this.partTransId = partTransId;
    }
}
