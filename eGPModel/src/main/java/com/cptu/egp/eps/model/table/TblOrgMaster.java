/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.model.table;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Sudhir Chavhan
 */
@Entity
@Table(name = "Tbl_OrgMaster", schema = "dbo")
public class TblOrgMaster implements java.io.Serializable {

    private int Org_Id;
    private String Email_Id;
    private String Login_Pass;
    private String OrgName_En;
    private String OrgName_Bg;
    private String Org_Addr;
    private String Contact_Person;
    private int Mobile_No;
    private String WS_Rights;
    private int Created_By;
    private Date Created_Date;
    private String Is_Deleted;

    public TblOrgMaster() {
    }

    public TblOrgMaster(int Org_Id) {
        this.Org_Id = Org_Id;
    }

    public TblOrgMaster(int Org_Id, String Email_Id, String Login_Pass, String OrgName_En, String OrgName_Bg, String Org_Addr, String Contact_Person, int Mobile_No, String WS_Rights, int Created_By, Date Created_Date, String Is_Deleted) {
        this.Org_Id = Org_Id;
        this.Email_Id = Email_Id;
        this.Login_Pass = Login_Pass;
        this.OrgName_En = OrgName_En;
        this.OrgName_Bg = OrgName_Bg;
        this.Org_Addr = Org_Addr;
        this.Contact_Person = Contact_Person;
        this.Mobile_No = Mobile_No;
        this.WS_Rights = WS_Rights;
        this.Created_By = Created_By;
        this.Created_Date = Created_Date;
        this.Is_Deleted = Is_Deleted;
    }

    @Id
    @GeneratedValue(generator = "TblOrgMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblOrgMasterSequence", sequenceName = "tblOrgMaster_sequence", allocationSize = 25)
    @Column(name = "Org_Id", unique = true, nullable = false)
    public int getOrg_Id() {
        return this.Org_Id;
    }

    public void setOrg_Id(int Org_Id) {
        this.Org_Id = Org_Id;
    }

    @Column(name = "Email_Id", nullable = false, length = 50)
    public String getEmail_Id() {
        return this.Email_Id;
    }

    public void setEmail_Id(String Email_Id) {
        this.Email_Id = Email_Id;
    }

    @Column(name = "Login_Pass", nullable = false, length = 50)
    public String getLogin_Pass() {
        return this.Login_Pass;
    }

    public void setLogin_Pass(String Login_Pass) {
        this.Login_Pass = Login_Pass;
    }

    @Column(name = "OrgName_En", nullable = false, length = 100)
    public String getOrgName_En() {
        return this.OrgName_En;
    }

    public void setOrgName_En(String OrgName_En) {
        this.OrgName_En = OrgName_En;
    }

    @Column(name = "OrgName_Bg", nullable = false, length = 100)
    public String getOrgName_Bg() {
        return this.OrgName_Bg;
    }

    public void setOrgName_Bg(String OrgName_Bg) {
        this.OrgName_Bg = OrgName_Bg;
    }

    @Column(name = "Org_Addr", nullable = false, length = 200)
    public String getOrg_Addr() {
        return this.Org_Addr;
    }

    public void setOrg_Addr(String Org_Addr) {
        this.Org_Addr = Org_Addr;
    }

    @Column(name = "Contact_Person", nullable = false, length = 200)
    public String getContact_Person() {
        return this.Contact_Person;
    }

    public void setContact_Person(String Contact_Person) {
        this.Contact_Person = Contact_Person;
    }

    @Column(name = "Mobile_No", nullable = false)
    public int getMobile_No() {
        return this.Mobile_No;
    }

    public void setMobile_No(int Mobile_No) {
        this.Mobile_No = Mobile_No;
    }

    @Column(name = "WS_Rights", nullable = false, length = 100)
    public String getWS_Rights() {
        return this.WS_Rights;
    }

    public void setWS_Rights(String WS_Rights) {
        this.WS_Rights = WS_Rights;
    }

    @Column(name = "Created_By", nullable = false)
    public int getCreated_By() {
        return this.Created_By;
    }

    public void setCreated_By(int Created_By) {
        this.Created_By = Created_By;
    }

    @Column(name = "Created_Date", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCreated_Date() {
        return this.Created_Date;
    }

    public void setCreated_Date(Date Created_Date) {
        this.Created_Date = Created_Date;
    }

    @Column(name = "Is_Deleted", nullable = false, length = 1)
    public String getIs_Deleted() {
        return this.Is_Deleted;
    }

    public void setIs_Deleted(String Is_Deleted) {
        this.Is_Deleted = Is_Deleted;
    }
}
