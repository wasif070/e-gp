package com.cptu.egp.eps.model.table;
// Generated Oct 31, 2010 5:42:16 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblAppAuditTrial generated by hbm2java
 */
@Entity
@Table(name = "tbl_AppAuditTrial", schema = "dbo")
public class TblAppAuditTrial  implements java.io.Serializable {

     private int appAuditTrialId;
     private TblLoginMaster tblLoginMaster;
     private TblAppMaster tblAppMaster;
     private int childId;
     private Date actionDate;
     private String action;
     private String signature;
     private String remarks;

    public TblAppAuditTrial() {
    }

    public TblAppAuditTrial(int appAuditTrialId, TblLoginMaster tblLoginMaster, TblAppMaster tblAppMaster, int childId, Date actionDate, String action, String signature, String remarks) {
       this.appAuditTrialId = appAuditTrialId;
       this.tblLoginMaster = tblLoginMaster;
       this.tblAppMaster = tblAppMaster;
       this.childId = childId;
       this.actionDate = actionDate;
       this.action = action;
       this.signature = signature;
        this.remarks = remarks;
    }

    //Constructor
    public TblAppAuditTrial(int appAuditTrialId) {
        this.appAuditTrialId = appAuditTrialId;
    }

     @Id
     @GeneratedValue(generator = "AppAuditTrialTypeSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "AppAuditTrialTypeSequence", sequenceName = "appAuditTrial_sequence", allocationSize = 25)
    @Column(name = "appAuditTrialId", unique = true, nullable = false)
    public int getAppAuditTrialId() {
        return this.appAuditTrialId;
    }

    public void setAppAuditTrialId(int appAuditTrialId) {
        this.appAuditTrialId = appAuditTrialId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actionBy", nullable = false)
    public TblLoginMaster getTblLoginMaster() {
        return this.tblLoginMaster;
    }

    public void setTblLoginMaster(TblLoginMaster tblLoginMaster) {
        this.tblLoginMaster = tblLoginMaster;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "appId", nullable = false)
    public TblAppMaster getTblAppMaster() {
        return this.tblAppMaster;
    }

    public void setTblAppMaster(TblAppMaster tblAppMaster) {
        this.tblAppMaster = tblAppMaster;
    }

    @Column(name = "childId", nullable = false)
    public int getChildId() {
        return this.childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "actionDate", nullable = false, length = 16)
    public Date getActionDate() {
        return this.actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Column(name = "action", nullable = false, length = 20)
    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "signature", nullable = false, length = 4000)
    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Column(name = "remarks", nullable = false, length = 1000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
