package com.cptu.egp.eps.model.table;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_CMS_ComplaintLevels")
public class TblComplaintLevel implements java.io.Serializable {

    private int complaintLevelId;
    private String complaintLevel;
    private Set<TblComplaintMaster> tblComplaintMasters = new HashSet<TblComplaintMaster>();

    public TblComplaintLevel() {
    }

    public TblComplaintLevel(int complaintLevelId) {
        this.complaintLevelId = complaintLevelId;
    }
    @Id
    @GeneratedValue(generator = "ComplaintLevelSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ComplaintLevelSequence", sequenceName = "ComplaintLevelSequence")
    @Column(name = "complaintLevelId", unique = true, nullable = false)
    public int getComplaintLevelId() {
        return complaintLevelId;
    }

    public void setComplaintLevelId(int complaintLevelId) {
        this.complaintLevelId = complaintLevelId;
    }

    @Column(name = "complaintLevel")
    public String getComplaintLevel() {
        return complaintLevel;
    }

    public void setComplaintLevel(String complaintLevel) {
        this.complaintLevel = complaintLevel;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "complaintLevel")
    public Set<TblComplaintMaster> getTblComplaintMasters() {
        return tblComplaintMasters;
    }

    public void setTblComplaintMasters(Set<TblComplaintMaster> tblComplaintMasters) {
        this.tblComplaintMasters = tblComplaintMasters;
    }
}
