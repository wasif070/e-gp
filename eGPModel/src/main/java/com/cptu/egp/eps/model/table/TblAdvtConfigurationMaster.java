/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.model.table;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author spandana.vaddi
 */
@Entity
@Table(name = "tbl_AdvertisementConfig", schema = "dbo")
public class TblAdvtConfigurationMaster implements java.io.Serializable {

    private int adConfigId;
    private String userId;
    private String location;
    private String bannerSize;
    private String duration;
    private String instructions;
    private String fileFormat;
    private String dimensions;
    private Date createDate;
    private double fee;

    public TblAdvtConfigurationMaster() {
    }

    public TblAdvtConfigurationMaster(int adConfigId) {
        this.adConfigId = adConfigId;
    }

    public TblAdvtConfigurationMaster(int adConfigId, String userId, String location, String bannerSize, String duration, String instructions, String fileFormat, String dimensions, Date createDate, double fee) {
        this.adConfigId = adConfigId;
        this.userId = userId;
        this.location = location;
        this.bannerSize = bannerSize;
        this.duration = duration;
        this.instructions = instructions;
        this.fileFormat = fileFormat;
        this.dimensions = dimensions;
        this.createDate = createDate;
        this.fee = fee;
    }

    @Id
    @GeneratedValue(generator = "TblAdvtConfigurationMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblAdvtConfigurationMasterSequence", sequenceName = "tblAdvtConfigurationMaster_sequence", allocationSize = 25)
    @Column(name = "adConfigId", unique = true, nullable = false)
    public int getAdConfigId() {
        return adConfigId;
    }

    public void setAdConfigId(int adConfigId) {
        this.adConfigId = adConfigId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createDate", nullable = false)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "dimensions", nullable = false)
    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    @Column(name = "duration", nullable = false)
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Column(name = "fee", nullable = false)
    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    @Column(name = "fileFormat", nullable = false)
    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    @Column(name = "instructions", nullable = false)
    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    @Column(name = "location", nullable = false)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "bannerSize", nullable = false)
    public String getBannerSize() {
        return bannerSize;
    }

    public void setBannerSize(String bannerSize) {
        this.bannerSize = bannerSize;
    }

    @Column(name = "userId", nullable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
