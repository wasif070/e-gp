package com.cptu.egp.eps.model.table;
// Generated Nov 17, 2010 10:45:10 AM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblTenderStd generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderStd", schema = "dbo")
public class TblTenderStd  implements java.io.Serializable {

     private int tenderStdId;
     private TblTemplateMaster tblTemplateMaster;
     private String templateName;
     private byte noOfSections;
     private String status;
     private String procType;
     private int tenderId;
     private int packageLotId;
     private int createdBy;
     private Set<TblTenderSection> tblTenderSections = new HashSet<TblTenderSection>();

    public TblTenderStd() {
    }

    //Constructor
    public TblTenderStd(int tenderStdId) {
        this.tenderStdId = tenderStdId;
    }

    public TblTenderStd(int tenderStdId, TblTemplateMaster tblTemplateMaster, String templateName, byte noOfSections, String status, int tenderId, int packageLotId, int createdBy, String procType) {
        this.tenderStdId = tenderStdId;
        this.tblTemplateMaster = tblTemplateMaster;
        this.templateName = templateName;
        this.noOfSections = noOfSections;
        this.status = status;
        this.procType = procType;
        this.tenderId = tenderId;
        this.packageLotId = packageLotId;
        this.createdBy = createdBy;
    }

    public TblTenderStd(int tenderStdId, TblTemplateMaster tblTemplateMaster, String templateName, byte noOfSections, String status, int tenderId, int packageLotId, int createdBy, Set tblTenderSections) {
       this.tenderStdId = tenderStdId;
       this.tblTemplateMaster = tblTemplateMaster;
       this.templateName = templateName;
       this.noOfSections = noOfSections;
       this.status = status;
       this.tenderId = tenderId;
       this.packageLotId = packageLotId;
       this.createdBy = createdBy;
       this.tblTenderSections = tblTenderSections;
    }

     @Id
     @GeneratedValue(generator = "TenderStdSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TenderStdSequence", sequenceName = "tenderStd_sequence", allocationSize = 25)
    @Column(name = "tenderStdId", unique = true, nullable = false)
    public int getTenderStdId() {
        return this.tenderStdId;
    }

    public void setTenderStdId(int tenderStdId) {
        this.tenderStdId = tenderStdId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "templateId", nullable = false)
    public TblTemplateMaster getTblTemplateMaster() {
        return this.tblTemplateMaster;
    }

    public void setTblTemplateMaster(TblTemplateMaster tblTemplateMaster) {
        this.tblTemplateMaster = tblTemplateMaster;
    }

    @Column(name = "templateName", nullable = false, length = 150)
    public String getTemplateName() {
        return this.templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Column(name = "noOfSections", nullable = false)
    public byte getNoOfSections() {
        return this.noOfSections;
    }

    public void setNoOfSections(byte noOfSections) {
        this.noOfSections = noOfSections;
    }

    @Column(name = "procType", nullable = true, length = 50)
    public String getProcType() {
        return procType;
    }

    public void setProcType(String procType) {
        this.procType = procType;
    }

    @Column(name = "status", nullable = false, length = 15)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "tenderId", nullable = false)
    public int getTenderId() {
        return this.tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    @Column(name = "packageLotId", nullable = false)
    public int getPackageLotId() {
        return this.packageLotId;
    }

    public void setPackageLotId(int packageLotId) {
        this.packageLotId = packageLotId;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderStd")
    public Set<TblTenderSection> getTblTenderSections() {
        return this.tblTenderSections;
    }

    public void setTblTenderSections(Set<TblTenderSection> tblTenderSections) {
        this.tblTenderSections = tblTenderSections;
    }
}
