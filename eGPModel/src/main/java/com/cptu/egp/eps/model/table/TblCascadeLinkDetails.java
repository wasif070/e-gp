/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.model.table;

/**
 *
 * @author tejasree.goriparthi
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_cascade_link_details", schema = "dbo")
public class TblCascadeLinkDetails  implements java.io.Serializable {
    
    private int linkDetailsId;
    private String userTypeId;
    private String parentLinkList;
    private String pageName;
    private String status;

    //constructor
    public TblCascadeLinkDetails() {
    }

    public TblCascadeLinkDetails(int linkDetailsId) {
        this.linkDetailsId = linkDetailsId;
    }

    public TblCascadeLinkDetails(int linkDetailsId, String userTypeId, String parentLinkList, String pageName, String status) {
        this.linkDetailsId = linkDetailsId;
        this.userTypeId = userTypeId;
        this.parentLinkList = parentLinkList;
        this.pageName = pageName;
        this.status = status;
    }

    @Id
    @GeneratedValue(generator = "cascadeDetailsSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "cascadeDetailsSequence", sequenceName = "cascadeDetails_sequence", allocationSize = 25)
    @Column(name = "linkDetailsId", unique = true, nullable = false)
    public int getLinkDetailsId() {
        return linkDetailsId;
    }

    public void setLinkDetailsId(int linkDetailsId) {
        this.linkDetailsId = linkDetailsId;
    }

    @Column(name = "pageName", nullable = false)
    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    @Column(name = "parentLinkList", nullable = false)
    public String getParentLinkList() {
        return parentLinkList;
    }

    public void setParentLinkList(String parentLinkList) {
        this.parentLinkList = parentLinkList;
    }

    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "userTypeId", nullable = false)
    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }


}
