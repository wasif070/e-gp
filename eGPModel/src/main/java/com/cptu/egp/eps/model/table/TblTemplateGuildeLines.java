package com.cptu.egp.eps.model.table;
// Generated May 7, 2011 10:39:08 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTemplateGuildeLines generated by hbm2java
 */
@Entity
@Table(name = "tbl_templateGuildeLines", schema = "dbo")
public class TblTemplateGuildeLines implements java.io.Serializable {

    private int guidelinesId;
    private String description;
    private String docName;
    private int templateId;
    private String docSize;
    private Date uploadedDt;
    private int uploadedBy;

    public TblTemplateGuildeLines() {
    }

    public TblTemplateGuildeLines(int guidelinesId, String description, String docName, int templateId, String docSize, Date uploadedDt, int uploadedBy) {
        this.guidelinesId = guidelinesId;
        this.description = description;
        this.docName = docName;
        this.templateId = templateId;
        this.docSize = docSize;
        this.uploadedDt = uploadedDt;
        this.uploadedBy = uploadedBy;
    }

    @Id
    @GeneratedValue(generator = "TemplateGuidlinesSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TemplateGuidlinesSequence", sequenceName = "TemplateGuidlines_sequence", allocationSize = 25)
    @Column(name = "guidelinesId", unique = true, nullable = false)
    public int getGuidelinesId() {
        return this.guidelinesId;
    }

    public void setGuidelinesId(int guidelinesId) {
        this.guidelinesId = guidelinesId;
    }

    @Column(name = "description", nullable = false, length = 500)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "docName", nullable = false, length = 20)
    public String getDocName() {
        return this.docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Column(name = "templateId", nullable = false)
    public int getTemplateId() {
        return this.templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    @Column(name = "docSize", nullable = false, length = 20)
    public String getDocSize() {
        return this.docSize;
    }

    public void setDocSize(String docSize) {
        this.docSize = docSize;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "uploadedDt", nullable = false, length = 23)
    public Date getUploadedDt() {
        return this.uploadedDt;
    }

    public void setUploadedDt(Date uploadedDt) {
        this.uploadedDt = uploadedDt;
    }

    @Column(name = "uploadedBy", nullable = false)
    public int getUploadedBy() {
        return this.uploadedBy;
    }

    public void setUploadedBy(int uploadedBy) {
        this.uploadedBy = uploadedBy;
    }
}
