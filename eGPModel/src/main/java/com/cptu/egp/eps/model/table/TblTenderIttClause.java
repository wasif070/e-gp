package com.cptu.egp.eps.model.table;
// Generated Nov 20, 2010 10:32:57 AM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblTenderIttClause generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderIttClause", schema = "dbo")
public class TblTenderIttClause  implements java.io.Serializable {

     private int tenderIttClauseId;
     private TblTenderIttHeader tblTenderIttHeader;
     private String ittClauseName;
     private int tempplateIttClauseId;
     private Set<TblTenderIttSubClause> tblTenderIttSubClauses = new HashSet<TblTenderIttSubClause>();

    public TblTenderIttClause() {
    }
    //Constructor

    public TblTenderIttClause(int tenderIttClauseId) {
        this.tenderIttClauseId = tenderIttClauseId;
    }

    public TblTenderIttClause(int tenderIttClauseId, TblTenderIttHeader tblTenderIttHeader, String ittClauseName, int tempplateIttClauseId) {
        this.tenderIttClauseId = tenderIttClauseId;
        this.tblTenderIttHeader = tblTenderIttHeader;
        this.ittClauseName = ittClauseName;
        this.tempplateIttClauseId = tempplateIttClauseId;
    }

    public TblTenderIttClause(int tenderIttClauseId, TblTenderIttHeader tblTenderIttHeader, String ittClauseName, int tempplateIttClauseId, Set tblTenderIttSubClauses) {
       this.tenderIttClauseId = tenderIttClauseId;
       this.tblTenderIttHeader = tblTenderIttHeader;
       this.ittClauseName = ittClauseName;
       this.tempplateIttClauseId = tempplateIttClauseId;
       this.tblTenderIttSubClauses = tblTenderIttSubClauses;
    }

     @Id
     @GeneratedValue(generator = "TenderIttClauseSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "TenderIttClauseSequence", sequenceName = "tenderIttClause_sequence", allocationSize = 25)
    @Column(name = "tenderIttClauseId", unique = true, nullable = false)
    public int getTenderIttClauseId() {
        return this.tenderIttClauseId;
    }

    public void setTenderIttClauseId(int tenderIttClauseId) {
        this.tenderIttClauseId = tenderIttClauseId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tendederIttHeaderId", nullable = false)
    public TblTenderIttHeader getTblTenderIttHeader() {
        return this.tblTenderIttHeader;
    }

    public void setTblTenderIttHeader(TblTenderIttHeader tblTenderIttHeader) {
        this.tblTenderIttHeader = tblTenderIttHeader;
    }

    @Column(name = "ittClauseName", nullable = false, length = 200)
    public String getIttClauseName() {
        return this.ittClauseName;
    }

    public void setIttClauseName(String ittClauseName) {
        this.ittClauseName = ittClauseName;
    }

    @Column(name = "tempplateIttClauseId", nullable = false)
    public int getTempplateIttClauseId() {
        return this.tempplateIttClauseId;
    }

    public void setTempplateIttClauseId(int tempplateIttClauseId) {
        this.tempplateIttClauseId = tempplateIttClauseId;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderIttClause")
    public Set<TblTenderIttSubClause> getTblTenderIttSubClauses() {
        return this.tblTenderIttSubClauses;
    }

    public void setTblTenderIttSubClauses(Set<TblTenderIttSubClause> tblTenderIttSubClauses) {
        this.tblTenderIttSubClauses = tblTenderIttSubClauses;
    }
}
