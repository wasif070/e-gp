package com.cptu.egp.eps.model.table;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tbl_CMS_ComplaintDocs")
public class TblComplaintDocs implements java.io.Serializable {

    private int complaintDocId;
    //complaintId
    private TblComplaintMaster complaintMaster;
    private int uploadedBy;
    //userTypeId
    private TblUserTypeMaster userRole;
    private String docSize;
    private String docName;
    private Date uploadedDt;
    private String docDescription;
    //complaintLevelId
    private TblComplaintLevel complaintLevel;

    @Id
    @GeneratedValue(generator = "ComplaintDocSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ComplaintDocSequence", sequenceName = "ComplaintDocSequence")
    @Column(name = "complaintDocId", unique = true, nullable = false)
    public int getComplaintDocId() {
        return complaintDocId;
    }

    public void setComplaintDocId(int complaintDocId) {
        this.complaintDocId = complaintDocId;
    }

    @ManyToOne
    @JoinColumn(name = "complaintId")
    public TblComplaintMaster getComplaintMaster() {
        return complaintMaster;
    }

    public void setComplaintMaster(TblComplaintMaster complaintMaster) {
        this.complaintMaster = complaintMaster;
    }

    @Column(name = "uploadedBy")
    public int getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(int uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    @ManyToOne
    @JoinColumn(name = "userRole")
    public TblUserTypeMaster getUserRole() {
        return userRole;
    }

    public void setUserRole(TblUserTypeMaster userRole) {
        this.userRole = userRole;
    }

    @Column(name = "docSize")
    public String getDocSize() {
        return docSize;
    }

    public void setDocSize(String docSize) {
        this.docSize = docSize;
    }

    @Column(name = "docName")
    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Column(name = "uploadedDt")
    public Date getUploadedDt() {
        return uploadedDt;
    }

    public void setUploadedDt(Date uploadedDt) {
        this.uploadedDt = uploadedDt;
    }

    @ManyToOne
    @JoinColumn(name = "complaintLevelId")
    public TblComplaintLevel getComplaintLevel() {
        return complaintLevel;
    }

    public void setComplaintLevel(TblComplaintLevel complaintLevel) {
        this.complaintLevel = complaintLevel;
    }

    @Column(name = "docDescription")
    public String getDocDescription() {
        return docDescription;
    }

    public void setDocDescription(String docDescription) {
        this.docDescription = docDescription;
    }
}
