/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.model.table;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Sudhir Chavhan
 */
@Entity
@Table(name="Tbl_PromisIndicator",schema = "dbo")
public class TblPromisIndicator implements java.io.Serializable {

    private int Indicator_ID;
  //  private TblOfficeMaster tblOfficeMaster;
    private int officeId;
    private String PEOffice_Name;
    private int tenderId;
    private int PublishNewspaper;
    private int PublishCPTUWebsite;
    private int GoBProcRules;
    private int DPRules;
    private String MultiLocSubmission;
    private String DaysBetTenPubTenderClosing;
    private int SuffTenderSubTime;
    private String TenderDocSold;
    private String TenderersPartCount;
    private String TenderSubVSDocSold;
    private int TOCMemberFromTEC;
    private int TECFormedByAA;
    private int TECExternalMembers;
    private String EvaluationTime;
    private int EvalCompletedInTime;
    private String ResponsiveTenderersCount;
    private int IsReTendered;
    private int IsTenderCancelled;
    private String DaysBtwnTenEvalTenApp;
    private int IsApprovedByProperAA;
    private int SubofEvalReportAA;
    private int AppofContractByAAInTimr;
    private int TERReviewByOther;
    private int ContractAppByHighAuth;
    private String DaysBtwnContappNOA;
    private String DaysBtwnTenOpenNOA;
    private String DaysBtwnIFTNOA;
    private int AwardPubCPTUWebsite;
    private int AwardedInTenValidityPeriod;
    private int DeliveredInTime;
    private int LiquidatedDamage;
    private int FullyCompleted;
    private String DaysReleasePayment;
    private int LatePayment;
    private int InterestPaid;
    private int ComplaintReceived;
    private int ResolAwardModification;
    private int ComplaintResolved;
    private int RevPanelDecUpheld;
    private int IsContAmended;
    private int ContUnresolvDispute;
    private int DetFraudCorruption;
    private int Ministry_ID;
    private int Division_ID;
    private int Org_ID;
    private Date Modify_Date;
    private String Is_TenderOnline;
    private String Is_Progress;
    private String prmsPECode;

    public TblPromisIndicator()
    {
    }
    public TblPromisIndicator(int Indicator_ID)
    {
        this.Indicator_ID = Indicator_ID;
    }

    public TblPromisIndicator(int Indicator_ID, int officeId, String PEOffice_Name) {
        this.Indicator_ID = Indicator_ID;
        this.officeId = officeId;
     //   this.tblOfficeMaster = tblOfficeMaster;
        this.PEOffice_Name = PEOffice_Name;
    }

    public TblPromisIndicator(int Indicator_ID, int officeId, String PEOffice_Name, int PublishNewspaper, int PublishCPTUWebsite, int GoBProcRules,int DPRules, String MultiLocSubmission, String DaysBetTenPubTenderClosing, int SuffTenderSubTime, String TenderDocSold, String TenderersPartCount, String TenderSubVSDocSold, int TOCMemberFromTEC, int TECFormedByAA, int TECExternalMembers, String EvaluationTime, int EvalCompletedInTime, String ResponsiveTenderersCount, int IsReTendered, int IsTenderCancelled, String DaysBtwnTenEvalTenApp, int IsApprovedByProperAA, int SubofEvalReportAA, int AppofContractByAAInTimr, int TERReviewByOther, int ContractAppByHighAuth, String DaysBtwnContappNOA, String DaysBtwnTenOpenNOA, String DaysBtwnIFTNOA, int AwardPubCPTUWebsite, int AwardedInTenValidityPeriod, int DeliveredInTime, int LiquidatedDamage, int FullyCompleted, String DaysReleasePayment, int LatePayment, int InterestPaid, int ComplaintReceived, int ResolAwardModification, int ComplaintResolved, int RevPanelDecUpheld, int IsContAmended, int ContUnresolvDispute, int DetFraudCorruption, int Ministry_ID, int Division_ID, int Org_ID, Date Modify_Date, String Is_TenderOnline, String Is_Progress) {
        this.Indicator_ID = Indicator_ID;
        this.officeId = officeId;
      //  this.tblOfficeMaster = tblOfficeMaster;
        this.PEOffice_Name = PEOffice_Name;
        this.PublishNewspaper = PublishNewspaper;
        this.PublishCPTUWebsite = PublishCPTUWebsite;
        this.GoBProcRules = GoBProcRules;
        this.DPRules = DPRules;
        this.MultiLocSubmission = MultiLocSubmission;
        this.DaysBetTenPubTenderClosing = DaysBetTenPubTenderClosing;
        this.SuffTenderSubTime = SuffTenderSubTime;
        this.TenderDocSold = TenderDocSold;
        this.TenderersPartCount = TenderersPartCount;
        this.TenderSubVSDocSold = TenderSubVSDocSold;
        this.TOCMemberFromTEC = TOCMemberFromTEC;
        this.TECFormedByAA = TECFormedByAA;
        this.TECExternalMembers = TECExternalMembers;
        this.EvaluationTime = EvaluationTime;
        this.EvalCompletedInTime = EvalCompletedInTime;
        this.ResponsiveTenderersCount = ResponsiveTenderersCount;
        this.IsReTendered = IsReTendered;
        this.IsTenderCancelled = IsTenderCancelled;
        this.DaysBtwnTenEvalTenApp = DaysBtwnTenEvalTenApp;
        this.IsApprovedByProperAA = IsApprovedByProperAA;
        this.SubofEvalReportAA = SubofEvalReportAA;
        this.AppofContractByAAInTimr = AppofContractByAAInTimr;
        this.TERReviewByOther = TERReviewByOther;
        this.ContractAppByHighAuth = ContractAppByHighAuth;
        this.DaysBtwnContappNOA = DaysBtwnContappNOA;
        this.DaysBtwnTenOpenNOA = DaysBtwnTenOpenNOA;
        this.DaysBtwnIFTNOA = DaysBtwnIFTNOA;
        this.AwardPubCPTUWebsite = AwardPubCPTUWebsite;
        this.AwardedInTenValidityPeriod = AwardedInTenValidityPeriod;
        this.DeliveredInTime = DeliveredInTime;
        this.LiquidatedDamage = LiquidatedDamage;
        this.FullyCompleted = FullyCompleted;
        this.DaysReleasePayment = DaysReleasePayment;
        this.LatePayment = LatePayment;
        this.InterestPaid = InterestPaid;
        this.ComplaintReceived = ComplaintReceived;
        this.ResolAwardModification = ResolAwardModification;
        this.ComplaintResolved = ComplaintResolved;
        this.RevPanelDecUpheld = RevPanelDecUpheld;
        this.IsContAmended = IsContAmended;
        this.ContUnresolvDispute = ContUnresolvDispute;
        this.DetFraudCorruption = DetFraudCorruption;
        this.Ministry_ID = Ministry_ID;
        this.Division_ID = Division_ID;
        this.Org_ID = Org_ID;
        this.Modify_Date = Modify_Date;
        this.Is_TenderOnline = Is_TenderOnline;
        this.Is_Progress = Is_Progress;
    }

    @Id
    @GeneratedValue(generator = "TblPromisIndicatorSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblPromisIndicatorSequence", sequenceName = "tblPromisIndicator_sequence", allocationSize = 25)
    @Column(name = "Indicator_ID", unique = true, nullable = false)
    public int getIndicator_ID() {
        return Indicator_ID;
    }

    public void setIndicator_ID(int Indicator_ID) {
        this.Indicator_ID = Indicator_ID;
    }
 /*   @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "officeId", nullable = false)
    public TblOfficeMaster getTblOfficeMaster() {
        return this.tblOfficeMaster;
    }
    public void setTblOfficeMaster(TblOfficeMaster tblOfficeMaster) {
        this.tblOfficeMaster = tblOfficeMaster;
    }   */
    @Column(name = "officeId", nullable = false)
    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    } 
    @Column(name = "PEOffice_Name", nullable = true, length=50)
    public String getPEOffice_Name() {
        return PEOffice_Name;
    }

    public void setPEOffice_Name(String PEOffice_Name) {
        this.PEOffice_Name = PEOffice_Name;
    }
    @Column(name = "tenderId", nullable = true)
    public int getTenderId() {
        return tenderId;
    }
    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }
    @Column(name = "PublishNewspaper", nullable = true)
    public int getPublishNewspaper() {
        return PublishNewspaper;
    }

    public void setPublishNewspaper(int PublishNewspaper) {
        this.PublishNewspaper = PublishNewspaper;
    }
    @Column(name = "PublishCPTUWebsite", nullable = true)
    public int getPublishCPTUWebsite() {
        return PublishCPTUWebsite;
    }

    public void setPublishCPTUWebsite(int PublishCPTUWebsite) {
        this.PublishCPTUWebsite = PublishCPTUWebsite;
    }
    @Column(name = "GoBProcRules", nullable = true)
    public int getGoBProcRules() {
        return GoBProcRules;
    }

    public void setGoBProcRules(int GoBProcRules) {
        this.GoBProcRules = GoBProcRules;
    }
    @Column(name = "DPRules", nullable = true)
    public int getDPRules() {
        return DPRules;
    }

    public void setDPRules(int DPRules) {
        this.DPRules = DPRules;
    }
    @Column(name = "AppofContractByAAInTimr", nullable = true)
    public int getAppofContractByAAInTimr() {
        return AppofContractByAAInTimr;
    }

    public void setAppofContractByAAInTimr(int AppofContractByAAInTimr) {
        this.AppofContractByAAInTimr = AppofContractByAAInTimr;
    }
    @Column(name = "AwardPubCPTUWebsite", nullable = true)
    public int getAwardPubCPTUWebsite() {
        return AwardPubCPTUWebsite;
    }

    public void setAwardPubCPTUWebsite(int AwardPubCPTUWebsite) {
        this.AwardPubCPTUWebsite = AwardPubCPTUWebsite;
    }
    @Column(name = "AwardedInTenValidityPeriod", nullable = true)
    public int getAwardedInTenValidityPeriod() {
        return AwardedInTenValidityPeriod;
    }

    public void setAwardedInTenValidityPeriod(int AwardedInTenValidityPeriod) {
        this.AwardedInTenValidityPeriod = AwardedInTenValidityPeriod;
    }
    @Column(name = "ComplaintReceived", nullable = true)
    public int getComplaintReceived() {
        return ComplaintReceived;
    }

    public void setComplaintReceived(int ComplaintReceived) {
        this.ComplaintReceived = ComplaintReceived;
    }
    @Column(name = "ComplaintResolved", nullable = true)
    public int getComplaintResolved() {
        return ComplaintResolved;
    }

    public void setComplaintResolved(int ComplaintResolved) {
        this.ComplaintResolved = ComplaintResolved;
    }
    @Column(name = "ContUnresolvDispute", nullable = true)
    public int getContUnresolvDispute() {
        return ContUnresolvDispute;
    }

    public void setContUnresolvDispute(int ContUnresolvDispute) {
        this.ContUnresolvDispute = ContUnresolvDispute;
    }
    @Column(name = "ContractAppByHighAuth", nullable = true)
    public int getContractAppByHighAuth() {
        return ContractAppByHighAuth;
    }

    public void setContractAppByHighAuth(int ContractAppByHighAuth) {
        this.ContractAppByHighAuth = ContractAppByHighAuth;
    }
    @Column(name = "DaysBetTenPubTenderClosing", nullable = true, length = 50)
    public String getDaysBetTenPubTenderClosing() {
        return DaysBetTenPubTenderClosing;
    }

    public void setDaysBetTenPubTenderClosing(String DaysBetTenPubTenderClosing) {
        this.DaysBetTenPubTenderClosing = DaysBetTenPubTenderClosing;
    }
    @Column(name = "DaysBtwnContappNOA", nullable = true, length = 50)
    public String getDaysBtwnContappNOA() {
        return DaysBtwnContappNOA;
    }

    public void setDaysBtwnContappNOA(String DaysBtwnContappNOA) {
        this.DaysBtwnContappNOA = DaysBtwnContappNOA;
    }
    @Column(name = "DaysBtwnIFTNOA", nullable = true, length = 50)
    public String getDaysBtwnIFTNOA() {
        return DaysBtwnIFTNOA;
    }

    public void setDaysBtwnIFTNOA(String DaysBtwnIFTNOA) {
        this.DaysBtwnIFTNOA = DaysBtwnIFTNOA;
    }
    @Column(name = "DaysBtwnTenEvalTenApp", nullable = true, length = 50)
    public String getDaysBtwnTenEvalTenApp() {
        return DaysBtwnTenEvalTenApp;
    }

    public void setDaysBtwnTenEvalTenApp(String DaysBtwnTenEvalTenApp) {
        this.DaysBtwnTenEvalTenApp = DaysBtwnTenEvalTenApp;
    }
    @Column(name = "DaysBtwnTenOpenNOA", nullable = true, length = 50)
    public String getDaysBtwnTenOpenNOA() {
        return DaysBtwnTenOpenNOA;
    }

    public void setDaysBtwnTenOpenNOA(String DaysBtwnTenOpenNOA) {
        this.DaysBtwnTenOpenNOA = DaysBtwnTenOpenNOA;
    }
    @Column(name = "DaysReleasePayment", nullable = true, length = 50)
    public String getDaysReleasePayment() {
        return DaysReleasePayment;
    }

    public void setDaysReleasePayment(String DaysReleasePayment) {
        this.DaysReleasePayment = DaysReleasePayment;
    }
    @Column(name = "LatePayment", nullable = true)
    public int getLatePayment() {
        return LatePayment;
    }

    public void setLatePayment(int LatePayment) {
        this.LatePayment = LatePayment;
    }
    @Column(name = "DeliveredInTime", nullable = true)
    public int getDeliveredInTime() {
        return DeliveredInTime;
    }

    public void setDeliveredInTime(int DeliveredInTime) {
        this.DeliveredInTime = DeliveredInTime;
    }
    @Column(name = "DetFraudCorruption", nullable = true)
    public int getDetFraudCorruption() {
        return DetFraudCorruption;
    }

    public void setDetFraudCorruption(int DetFraudCorruption) {
        this.DetFraudCorruption = DetFraudCorruption;
    }
    @Column(name = "EvalCompletedInTime", nullable = true)
    public int getEvalCompletedInTime() {
        return EvalCompletedInTime;
    }

    public void setEvalCompletedInTime(int EvalCompletedInTime) {
        this.EvalCompletedInTime = EvalCompletedInTime;
    }
    @Column(name = "EvaluationTime", nullable = true, length = 50)
    public String getEvaluationTime() {
        return EvaluationTime;
    }

    public void setEvaluationTime(String EvaluationTime) {
        this.EvaluationTime = EvaluationTime;
    }
    @Column(name = "FullyCompleted", nullable = true)
    public int getFullyCompleted() {
        return FullyCompleted;
    }

    public void setFullyCompleted(int FullyCompleted) {
        this.FullyCompleted = FullyCompleted;
    }
    @Column(name = "InterestPaid", nullable = true)
    public int getInterestPaid() {
        return InterestPaid;
    }

    public void setInterestPaid(int InterestPaid) {
        this.InterestPaid = InterestPaid;
    }
    @Column(name = "IsApprovedByProperAA", nullable = true)
    public int getIsApprovedByProperAA() {
        return IsApprovedByProperAA;
    }

    public void setIsApprovedByProperAA(int IsApprovedByProperAA) {
        this.IsApprovedByProperAA = IsApprovedByProperAA;
    }
    @Column(name = "IsContAmended", nullable = true)
    public int getIsContAmended() {
        return IsContAmended;
    }

    public void setIsContAmended(int IsContAmended) {
        this.IsContAmended = IsContAmended;
    }
    @Column(name = "IsReTendered", nullable = true)
    public int getIsReTendered() {
        return IsReTendered;
    }

    public void setIsReTendered(int IsReTendered) {
        this.IsReTendered = IsReTendered;
    }
    @Column(name = "IsTenderCancelled", nullable = true)
    public int getIsTenderCancelled() {
        return IsTenderCancelled;
    }

    public void setIsTenderCancelled(int IsTenderCancelled) {
        this.IsTenderCancelled = IsTenderCancelled;
    }

    @Column(name = "LiquidatedDamage", nullable = true)
    public int getLiquidatedDamage() {
        return LiquidatedDamage;
    }

    public void setLiquidatedDamage(int LiquidatedDamage) {
        this.LiquidatedDamage = LiquidatedDamage;
    }
    @Column(name = "Ministry_ID", nullable = true)
    public int getMinistry_ID() {
        return Ministry_ID;
    }
    public void setMinistry_ID(int Ministry_ID) {
        this.Ministry_ID = Ministry_ID;
    }
    @Column(name = "Division_ID", nullable = true)
    public int getDivision_ID() {
        return Division_ID;
    }

    public void setDivision_ID(int Division_ID) {
        this.Division_ID = Division_ID;
    }
    @Column(name = "Org_ID", nullable = true)
    public int getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(int Org_ID) {
        this.Org_ID = Org_ID;
    }
    @Column(name = "Modify_Date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getModify_Date() {
        return Modify_Date;
    }
    public void setModify_Date(Date Modify_Date) {
        this.Modify_Date = Modify_Date;
    }
    @Column(name = "Is_TenderOnline", nullable = true, length = 10)
    public String getIs_TenderOnline() {
        return Is_TenderOnline;
    }
    public void setIs_TenderOnline(String Is_TenderOnline) {
        this.Is_TenderOnline = Is_TenderOnline;
    }
    @Column(name = "Is_Progress", nullable = true, length = 10)
    public String getIs_Progress() {
        return Is_Progress;
    }
    public void setIs_Progress(String Is_Progress) {
        this.Is_Progress = Is_Progress;
    }

    @Column(name = "MultiLocSubmission", nullable = true, length = 10)
    public String getMultiLocSubmission() {
        return MultiLocSubmission;
    }

    public void setMultiLocSubmission(String MultiLocSubmission) {
        this.MultiLocSubmission = MultiLocSubmission;
    }

    @Column(name = "ResolAwardModification", nullable = true)
    public int getResolAwardModification() {
        return ResolAwardModification;
    }

    public void setResolAwardModification(int ResolAwardModification) {
        this.ResolAwardModification = ResolAwardModification;
    }
    @Column(name = "ResponsiveTenderersCount", nullable = true, length = 50)
    public String getResponsiveTenderersCount() {
        return ResponsiveTenderersCount;
    }

    public void setResponsiveTenderersCount(String ResponsiveTenderersCount) {
        this.ResponsiveTenderersCount = ResponsiveTenderersCount;
    }
    @Column(name = "RevPanelDecUpheld", nullable = true)
    public int getRevPanelDecUpheld() {
        return RevPanelDecUpheld;
    }

    public void setRevPanelDecUpheld(int RevPanelDecUpheld) {
        this.RevPanelDecUpheld = RevPanelDecUpheld;
    }
    @Column(name = "SubofEvalReportAA", nullable = true)
    public int getSubofEvalReportAA() {
        return SubofEvalReportAA;
    }

    public void setSubofEvalReportAA(int SubofEvalReportAA) {
        this.SubofEvalReportAA = SubofEvalReportAA;
    }
    @Column(name = "SuffTenderSubTime", nullable = true)
    public int getSuffTenderSubTime() {
        return SuffTenderSubTime;
    }

    public void setSuffTenderSubTime(int SuffTenderSubTime) {
        this.SuffTenderSubTime = SuffTenderSubTime;
    }
    @Column(name = "TECExternalMembers", nullable = true)
    public int getTECExternalMembers() {
        return TECExternalMembers;
    }

    public void setTECExternalMembers(int TECExternalMembers) {
        this.TECExternalMembers = TECExternalMembers;
    }
    @Column(name = "TECFormedByAA", nullable = true)
    public int getTECFormedByAA() {
        return TECFormedByAA;
    }

    public void setTECFormedByAA(int TECFormedByAA) {
        this.TECFormedByAA = TECFormedByAA;
    }
    @Column(name = "TERReviewByOther", nullable = true)
    public int getTERReviewByOther() {
        return TERReviewByOther;
    }

    public void setTERReviewByOther(int TERReviewByOther) {
        this.TERReviewByOther = TERReviewByOther;
    }
    @Column(name = "TOCMemberFromTEC", nullable = true)
    public int getTOCMemberFromTEC() {
        return TOCMemberFromTEC;
    }

    public void setTOCMemberFromTEC(int TOCMemberFromTEC) {
        this.TOCMemberFromTEC = TOCMemberFromTEC;
    }
    @Column(name = "TenderDocSold", nullable = true, length = 50)
    public String getTenderDocSold() {
        return TenderDocSold;
    }

    public void setTenderDocSold(String TenderDocSold) {
        this.TenderDocSold = TenderDocSold;
    }
    @Column(name = "TenderSubVSDocSold", nullable = true, length = 50)
    public String getTenderSubVSDocSold() {
        return TenderSubVSDocSold;
    }

    public void setTenderSubVSDocSold(String TenderSubVSDocSold) {
        this.TenderSubVSDocSold = TenderSubVSDocSold;
    }
    @Column(name = "TenderersPartCount", nullable = true, length = 50)
    public String getTenderersPartCount() {
        return TenderersPartCount;
    }

    public void setTenderersPartCount(String TenderersPartCount) {
        this.TenderersPartCount = TenderersPartCount;
    }
    @Column(name = "prmsPECode", nullable = true, length = 50)
    public String getPrmsPECode() {
        return prmsPECode;
    }

    public void setPrmsPECode(String prmsPECode) {
        this.prmsPECode = prmsPECode;
    }
}
