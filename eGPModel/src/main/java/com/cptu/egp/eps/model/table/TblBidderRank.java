package com.cptu.egp.eps.model.table;
// Generated Jan 1, 2011 5:58:46 PM by Hibernate Tools 3.2.1.GA

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblBidderRank generated by hbm2java
 */
@Entity
@Table(name = "tbl_BidderRank", schema = "dbo")
public class TblBidderRank implements java.io.Serializable {

    private int bidderRankId;
    private TblTenderMaster tblTenderMaster;
    private int pkgLotId;
    private int userId;
    private Date createdTime;
    private int createdBy;
    private int rank;
    private int reportId;
    private BigDecimal amount;
    private int rowId;
    private int tableId;
    private String companyName;
    private int roundId;
    private BigDecimal t1Score;

   
    private BigDecimal l1Score;
    private Set<TblBidRankDetail> tblBidRankDetails = new HashSet<TblBidRankDetail>();

    public TblBidderRank() {
    }

    public TblBidderRank(int bidderRankId) {
        this.bidderRankId = bidderRankId;
    }

    public TblBidderRank(int bidderRankId,TblTenderMaster tblTenderMaster,  int pkgLotId, int userId, Date createdTime, int createdBy, int rank, int reportId, BigDecimal amount, int rowId, int tableId, int roundId) {
        this.bidderRankId = bidderRankId;
        this.tblTenderMaster = tblTenderMaster;
        this.pkgLotId = pkgLotId;
        this.userId = userId;
        this.createdTime = createdTime;
        this.createdBy = createdBy;
        this.rank = rank;
        this.reportId = reportId;
        this.amount = amount;
        this.rowId = rowId;
        this.tableId = tableId;
        this.roundId = roundId;
    }
    
    public TblBidderRank(int bidderRankId, TblTenderMaster tblTenderMaster, int pkgLotId, int userId, Date createdTime, int createdBy, int rank, int reportId, BigDecimal amount, int rowId, int tableId, String companyName) {
        this.bidderRankId = bidderRankId;
        this.tblTenderMaster = tblTenderMaster;
        this.pkgLotId = pkgLotId;
        this.userId = userId;
        this.createdTime = createdTime;
        this.createdBy = createdBy;
        this.rank = rank;
        this.reportId = reportId;
        this.amount = amount;
        this.rowId = rowId;
        this.tableId = tableId;
        this.companyName = companyName;
    }

    public TblBidderRank(int bidderRankId, TblTenderMaster tblTenderMaster, int pkgLotId, int userId, Date createdTime, int createdBy, int rank, int reportId, BigDecimal amount, int rowId, int tableId, Set<TblBidRankDetail> tblBidRankDetails) {
        this.bidderRankId = bidderRankId;
        this.tblTenderMaster = tblTenderMaster;
        this.pkgLotId = pkgLotId;
        this.userId = userId;
        this.createdTime = createdTime;
        this.createdBy = createdBy;
        this.rank = rank;
        this.reportId = reportId;
        this.amount = amount;
        this.rowId = rowId;
        this.tableId = tableId;
        this.tblBidRankDetails = tblBidRankDetails;
    }

    @Id
    @GeneratedValue(generator = "BidderRankSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "BidderRankSequence", sequenceName = "bidderRank_sequence", allocationSize = 25)
    @Column(name = "bidderRankId", unique = true, nullable = false)
    public int getBidderRankId() {
        return this.bidderRankId;
    }

    public void setBidderRankId(int bidderRankId) {
        this.bidderRankId = bidderRankId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderId", nullable = false)
    public TblTenderMaster getTblTenderMaster() {
        return this.tblTenderMaster;
    }

    public void setTblTenderMaster(TblTenderMaster tblTenderMaster) {
        this.tblTenderMaster = tblTenderMaster;
    }

    @Column(name = "pkgLotId", nullable = false)
    public int getPkgLotId() {
        return this.pkgLotId;
    }

    public void setPkgLotId(int pkgLotId) {
        this.pkgLotId = pkgLotId;
    }

    @Column(name = "userId", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdTime", nullable = false, length = 16)
    public Date getCreatedTime() {
        return this.createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "rank", nullable = false)
    public int getRank() {
        return this.rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Column(name = "reportId", nullable = false)
    public int getReportId() {
        return this.reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    @Column(name = "amount", nullable = false, precision = 20)
    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "rowId", nullable = false)
    public int getRowId() {
        return this.rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    @Column(name = "tableId", nullable = false)
    public int getTableId() {
        return this.tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblBidderRank")
    public Set<TblBidRankDetail> getTblBidRankDetails() {
        return this.tblBidRankDetails;
    }

    public void setTblBidRankDetails(Set<TblBidRankDetail> tblBidRankDetails) {
        this.tblBidRankDetails = tblBidRankDetails;
    }

    @Column(name = "companyName", length = 350)
    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    @Column(name="roundId", nullable=false)
    public int getRoundId() {
        return this.roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    @Column(name = "l1Score", precision = 10)
     public BigDecimal getL1Score() {
        return l1Score;
    }

    public void setL1Score(BigDecimal l1Score) {
        this.l1Score = l1Score;
    }

    @Column(name = "t1Score", precision = 10)
    public BigDecimal getT1Score() {
        return t1Score;
    }

    public void setT1Score(BigDecimal t1Score) {
        this.t1Score = t1Score;
    }
}
