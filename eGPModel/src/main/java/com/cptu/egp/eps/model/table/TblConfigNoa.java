package com.cptu.egp.eps.model.table;
// Generated Dec 24, 2010 2:48:11 PM by Hibernate Tools 3.2.1.GA

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblConfigNoa generated by hbm2java
 */
@Entity
@Table(name = "tbl_ConfigNoa", schema = "dbo")
public class TblConfigNoa  implements java.io.Serializable {

     private int configNoaId;
     private TblProcurementMethod tblProcurementMethod;
     private TblProcurementTypes tblProcurementTypes;
     private TblProcurementNature tblProcurementNature;
     private BigDecimal minValue;
     private BigDecimal maxValue;
     private short noaAcceptDays;
     private short perSecSubDays;
     private short conSignDays;
     private short loiNoa;

    public TblConfigNoa() {
    }
    //Constructor

    public TblConfigNoa(int configNoaId) {
        this.configNoaId = configNoaId;
    }

    public TblConfigNoa(int configNoaId, TblProcurementMethod tblProcurementMethod, TblProcurementTypes tblProcurementTypes, TblProcurementNature tblProcurementNature, BigDecimal minValue, BigDecimal maxValue, short noaAcceptDays, short perSecSubDays, short conSignDays, short loiNoa) 
    {
       this.configNoaId = configNoaId;
       this.tblProcurementMethod = tblProcurementMethod;
       this.tblProcurementTypes = tblProcurementTypes;
       this.tblProcurementNature = tblProcurementNature;
       this.minValue = minValue;
       this.maxValue = maxValue;
       this.noaAcceptDays = noaAcceptDays;
       this.perSecSubDays = perSecSubDays;
       this.conSignDays = conSignDays;
       this.loiNoa = loiNoa;
    }

     @Id
     @GeneratedValue(generator = "ConfigNoaSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "ConfigNoaSequence", sequenceName = "configNoa_sequence", allocationSize = 25)
    @Column(name = "configNoaId", unique = true, nullable = false)
    public int getConfigNoaId() {
        return this.configNoaId;
    }

    public void setConfigNoaId(int configNoaId) {
        this.configNoaId = configNoaId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "procurementMethodId", nullable = false)
    public TblProcurementMethod getTblProcurementMethod() {
        return this.tblProcurementMethod;
    }

    public void setTblProcurementMethod(TblProcurementMethod tblProcurementMethod) {
        this.tblProcurementMethod = tblProcurementMethod;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "procurementTypeId", nullable = false)
    public TblProcurementTypes getTblProcurementTypes() {
        return this.tblProcurementTypes;
    }

    public void setTblProcurementTypes(TblProcurementTypes tblProcurementTypes) {
        this.tblProcurementTypes = tblProcurementTypes;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "procurementNatureId", nullable = false)
    public TblProcurementNature getTblProcurementNature() {
        return this.tblProcurementNature;
    }

    public void setTblProcurementNature(TblProcurementNature tblProcurementNature) {
        this.tblProcurementNature = tblProcurementNature;
    }

    @Column(name = "minValue", nullable = false, precision = 10)
    public BigDecimal getMinValue() {
        return this.minValue;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    @Column(name = "maxValue", nullable = false, precision = 10)
    public BigDecimal getMaxValue() {
        return this.maxValue;
    }

    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }

    @Column(name = "noaAcceptDays", nullable = false)
    public short getNoaAcceptDays() {
        return this.noaAcceptDays;
    }

    public void setNoaAcceptDays(short noaAcceptDays) {
        this.noaAcceptDays = noaAcceptDays;
    }

    @Column(name = "perSecSubDays", nullable = false)
    public short getPerSecSubDays() {
        return this.perSecSubDays;
    }

    public void setPerSecSubDays(short perSecSubDays) {
        this.perSecSubDays = perSecSubDays;
    }

    @Column(name = "conSignDays", nullable = false)
    public short getConSignDays() {
        return this.conSignDays;
    }

    public void setConSignDays(short conSignDays) {
        this.conSignDays = conSignDays;
    }
    
    /* Nitish_Oritro Start **/ 
    
    @Column(name = "loiNoa", nullable = false)
    public short getLoiNoa() {
        return loiNoa;
    }

    public void setLoiNoa(short loiNoa) {
        this.loiNoa = loiNoa;
    }
    
    /**Here is a get set method fro tbl_ConfigNoa **/ 
    
    /* Nitish_Oritro End **/
}
