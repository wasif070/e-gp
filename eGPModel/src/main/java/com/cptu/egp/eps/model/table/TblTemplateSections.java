package com.cptu.egp.eps.model.table;
// Generated Nov 13, 2010 7:33:40 PM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblTemplateSections generated by hbm2java
 */
@Entity
@Table(name = "tbl_TemplateSections", schema = "dbo")
public class TblTemplateSections  implements java.io.Serializable {

     private int sectionId;
     private String sectionName;
     private short templateId;
     private String contentType;
     private String isSubSection;
     private String status;
     private Set<TblIttHeader> tblIttHeader = new HashSet<TblIttHeader>();

    public TblTemplateSections() {
    }

     public TblTemplateSections(int sectionId) {
        this.sectionId = sectionId;
     }

    public TblTemplateSections(int sectionId, String sectionName, short templateId, String contentType, String isSubSection, String status) {
        this.sectionId = sectionId;
        this.sectionName = sectionName;
        this.templateId = templateId;
        this.contentType = contentType;
        this.isSubSection = isSubSection;
        this.status = status;
    }

    public TblTemplateSections(int sectionId, String sectionName, short templateId, String contentType, String isSubSection, String status, Set tblIttHeader) {
       this.sectionId = sectionId;
       this.sectionName = sectionName;
       this.templateId = templateId;
       this.contentType = contentType;
       this.isSubSection = isSubSection;
       this.status = status;
       this.tblIttHeader = tblIttHeader;
    }

    @Id
    @GeneratedValue(generator = "TemplateSectionsSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TemplateSectionsSequence", sequenceName = "templateSections_sequence", allocationSize = 25)
    @Column(name = "sectionId", unique = true, nullable = false)
    public int getSectionId() {
        return this.sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    @Column(name = "sectionName", nullable = false, length = 500)
    public String getSectionName() {
        return this.sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Column(name = "templateId", nullable = false)
    public short getTemplateId() {
        return this.templateId;
    }

    public void setTemplateId(short templateId) {
        this.templateId = templateId;
    }

    @Column(name = "contentType", nullable = false, length = 30)
    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Column(name = "isSubSection", nullable = false, length = 3)
    public String getIsSubSection() {
        return this.isSubSection;
    }

    public void setIsSubSection(String isSubSection) {
        this.isSubSection = isSubSection;
    }

    @Column(name = "status", nullable = false, length = 15)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTemplateSections")
    public Set<TblIttHeader> getTblIttHeader() {
        return tblIttHeader;
    }

    public void setTblIttHeader(Set<TblIttHeader> tblIttHeader) {
        this.tblIttHeader = tblIttHeader;
    }
}
