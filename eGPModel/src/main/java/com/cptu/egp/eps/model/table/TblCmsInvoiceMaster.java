package com.cptu.egp.eps.model.table;
// Generated Aug 3, 2011 2:07:47 PM by Hibernate Tools 3.2.1.GA


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblCmsInvoiceMaster generated by hbm2java
 */
@Entity
@Table(name="tbl_CMS_InvoiceMaster"
    ,schema="dbo"    
)
public class TblCmsInvoiceMaster  implements java.io.Serializable {


     private int invoiceId;
     private BigDecimal totalInvAmt;     
     TblCmsWpMaster tblCmsWpMaster;
     private Date createdDate;
     private Date releasedDtNTime;
     private int createdBy;
     private String invStatus;
     private String remarks;
     private String conRemarks;   
     private int tillPrid;
     private int tenderId;
     private String isAdvInv;
     private String invoiceNo;
     private Set<TblCmsInvoiceDetails> tblCmsInvoiceDetails = new HashSet<TblCmsInvoiceDetails>();
     private Set<TblCmsInvoiceDocument> tblCmsInvoiceDocument = new HashSet<TblCmsInvoiceDocument>();
     private Set<TblCmsInvoiceAccDetails> tblCmsInvoiceAccDetails = new HashSet<TblCmsInvoiceAccDetails>();
     private Set<TblCmsInvRemarks> tblCmsInvRemarks = new HashSet<TblCmsInvRemarks>();
    
    public TblCmsInvoiceMaster(int invoiceId) {
        this.invoiceId=invoiceId;
    }

    public TblCmsInvoiceMaster(int invoiceId, BigDecimal totalInvAmt, TblCmsWpMaster tblCmsWpMaster, Date createdDate, int createdBy, String invStatus, int tillPrid) {
       this.invoiceId = invoiceId;
       this.totalInvAmt = totalInvAmt;
       this.tblCmsWpMaster = tblCmsWpMaster;
       this.createdDate = createdDate;
       this.createdBy = createdBy;
       this.invStatus = invStatus;
       this.tillPrid = tillPrid;
    }

    public TblCmsInvoiceMaster() {
    }
   
    @Id 
    @GeneratedValue(generator = "TblCmsInvoiceMaster", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblCmsInvoiceMaster", sequenceName = "tblCmsInvoiceMaster_sequence", allocationSize = 25)
    @Column(name="invoiceId", unique=true, nullable=false)
    public int getInvoiceId() {
        return this.invoiceId;
    }
    
    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }
    
    @Column(name="totalInvAmt", nullable=false, precision=15, scale=3)
    public BigDecimal getTotalInvAmt() {
        return this.totalInvAmt;
    }
    
    public void setTotalInvAmt(BigDecimal totalInvAmt) {
        this.totalInvAmt = totalInvAmt;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wpId", nullable = false)
    public TblCmsWpMaster getTblCmsWpMaster() {
        return tblCmsWpMaster;
    }

    public void setTblCmsWpMaster(TblCmsWpMaster tblCmsWpMaster) {
        this.tblCmsWpMaster = tblCmsWpMaster;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="createdDate", nullable=false, length=23)
    public Date getCreatedDate() {
        return this.createdDate;
    }
    
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    
    @Column(name="createdBy", nullable=false)
    public int getCreatedBy() {
        return this.createdBy;
    }
    
    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }
    
    @Column(name="invStatus", nullable=false, length=30)
    public String getInvStatus() {
        return this.invStatus;
    }
    
    public void setInvStatus(String invStatus) {
        this.invStatus = invStatus;
    }
    @Column(name="remarks", nullable=true, length=500)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    @Column(name="conRemarks", nullable=true, length=500)
    public String getConRemarks() {
        return conRemarks;
    }

    public void setConRemarks(String conRemarks) {
        this.conRemarks = conRemarks;
    }
    @Column(name="tillPRId", nullable=false)
    public int getTillPrid() {
        return this.tillPrid;
    }
    
    public void setTillPrid(int tillPrid) {
        this.tillPrid = tillPrid;
    }

    @Column(name="tenderId", nullable=false)
    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    @Column(name="isAdvInv", nullable=true, length=30)
    public String getIsAdvInv() {
        return isAdvInv;
    }

    public void setIsAdvInv(String isAdvInv) {
        this.isAdvInv = isAdvInv;
    }
    
    @Column(name="invoiceNo", nullable=true, length=20)
    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblCmsInvoiceMaster")
    public Set<TblCmsInvoiceDetails> getTblCmsInvoiceDetails() {
        return tblCmsInvoiceDetails;
    }

    public void setTblCmsInvoiceDetails(Set<TblCmsInvoiceDetails> tblCmsInvoiceDetails) {
        this.tblCmsInvoiceDetails = tblCmsInvoiceDetails;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblCmsInvoiceMaster")
    public Set<TblCmsInvoiceDocument> getTblCmsInvoiceDocument() {
        return tblCmsInvoiceDocument;
    }

    public void setTblCmsInvoiceDocument(Set<TblCmsInvoiceDocument> tblCmsInvoiceDocument) {
        this.tblCmsInvoiceDocument = tblCmsInvoiceDocument;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblCmsInvoiceMaster")
    public Set<TblCmsInvoiceAccDetails> getTblCmsInvoiceAccDetails() {
        return tblCmsInvoiceAccDetails;
    }

    public void setTblCmsInvoiceAccDetails(Set<TblCmsInvoiceAccDetails> tblCmsInvoiceAccDetails) {
        this.tblCmsInvoiceAccDetails = tblCmsInvoiceAccDetails;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblCmsInvoiceMaster")
    public Set<TblCmsInvRemarks> getTblCmsInvRemarks() {
        return tblCmsInvRemarks;
    }

    public void setTblCmsInvRemarks(Set<TblCmsInvRemarks> tblCmsInvRemarks) {
        this.tblCmsInvRemarks = tblCmsInvRemarks;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="releasedDtNTime", nullable=true, length=23)
    public Date getReleasedDtNTime() {
        return releasedDtNTime;
    }

    public void setReleasedDtNTime(Date releasedDtNTime) {
        this.releasedDtNTime = releasedDtNTime;
    }

}
