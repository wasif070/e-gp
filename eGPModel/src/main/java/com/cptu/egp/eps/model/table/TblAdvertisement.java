/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.model.table;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sambasiva.sugguna
 */
@Entity
@Table(name = "tbl_Advertisement", schema = "dbo")
public class TblAdvertisement implements java.io.Serializable {

    private int adId;
    private String userName;
    private String bannerName;
    private String description;
    private String comments;
    private String duration;
    private String address;
    private Date createDate;
    private Date paymentDate;
    private Date approvalDate;
    private Date publishDate;
    private Date expiryDate;
    private String location;
    private String dimension;
    private String bannerSize;
    private String price;
    private String organization;
    private String eMailId;
    private String phoneNumber;
    private String url;
    private String status;
    private String transId;
    private int userId;

    public TblAdvertisement() {
    }
    //Constructor

    public TblAdvertisement(int adId) {
        this.adId = adId;
    }

    public TblAdvertisement(String duration, String location) {
        this.duration = duration;
        this.location = location;
    }

    public TblAdvertisement(int adId, String comments) {
        this.adId = adId;
        this.comments = comments;
    }

    public TblAdvertisement(int adId, String userName, String bannerName, String description, String comments, String duration, String address, Date createDate, Date paymentDate,
            Date approvalDate, Date publishDate, Date expiryDate, String location, String dimension, String bannerSize, String price, String organization, String eMailId, String phoneNumber, String url, String status, String transId, int userId) {
        this.adId = adId;
        this.userName = userName;
        this.bannerName = bannerName;
        this.description = description;
        this.comments = comments;
        this.duration = duration;
        this.address = address;
        this.createDate = createDate;
        this.paymentDate = paymentDate;
        this.publishDate = publishDate;
        this.approvalDate = approvalDate;
        this.expiryDate = expiryDate;

        this.location = location;
        this.dimension = dimension;
        this.bannerSize = bannerSize;
        this.price = price;
        this.organization = organization;
        this.eMailId = eMailId;
        this.phoneNumber = phoneNumber;

        this.url = url;
        this.status = status;
        this.transId = transId;
        this.userId = userId;

    }

    @Id
    @GeneratedValue(generator = "Advertisement", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "Advertisement", sequenceName = "Advertisement_sequence", allocationSize = 25)
    @Column(name = "adId", unique = true, nullable = false)
    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    @Column(name = "address", nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approvalDate")
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publishDate")
    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Column(name = "bannerName", nullable = false)
    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    @Column(name = "bannerSize", nullable = false)
    public String getBannerSize() {
        return bannerSize;
    }

    public void setBannerSize(String bannerSize) {
        this.bannerSize = bannerSize;
    }

    @Column(name = "comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createDate", nullable = false)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "dimension", nullable = false)
    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Column(name = "duration", nullable = false)
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Column(name = "eMailId", nullable = false)
    public String geteMailId() {
        return eMailId;
    }

    public void seteMailId(String eMailId) {
        this.eMailId = eMailId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Column(name = "location", nullable = false)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "organization", nullable = false)
    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "paymentDate")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(name = "phoneNumber", nullable = false)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "price")
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "transId")
    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    @Column(name = "url", nullable = false)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "userName", nullable = false)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "userId")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
