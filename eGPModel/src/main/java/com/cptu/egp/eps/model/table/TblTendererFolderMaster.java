package com.cptu.egp.eps.model.table;
// Generated Oct 18, 2010 11:33:49 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTendererFolderMaster generated by hbm2java
 */
@Entity
@Table(name = "tbl_TendererFolderMaster", schema = "dbo")
public class TblTendererFolderMaster  implements java.io.Serializable {

     private int folderId;
     private TblTendererMaster tblTendererMaster;
     private String folderName;
     private Date creationDate;

    public TblTendererFolderMaster() {
    }

    public TblTendererFolderMaster(int folderId, TblTendererMaster tblTendererMaster, String folderName, Date creationDate) {
       this.folderId = folderId;
       this.tblTendererMaster = tblTendererMaster;
       this.folderName = folderName;
       this.creationDate = creationDate;
    }

    @Id
    @GeneratedValue(generator = "TendererFolderMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TendererFolderMasterSequence", sequenceName = "tendererFolderMaster_sequence", allocationSize = 25)
    @Column(name = "folderId", unique = true, nullable = false)
    public int getFolderId() {
        return this.folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tendererId", nullable = false)
    public TblTendererMaster getTblTendererMaster() {
        return this.tblTendererMaster;
    }

    public void setTblTendererMaster(TblTendererMaster tblTendererMaster) {
        this.tblTendererMaster = tblTendererMaster;
    }

    @Column(name = "folderName", nullable = false, length = 100)
    public String getFolderName() {
        return this.folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creationDate", nullable = false, length = 16)
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
