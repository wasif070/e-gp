package com.cptu.egp.eps.model.table;
// Generated Dec 7, 2010 8:22:33 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTenderPaymentStatus generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderPaymentStatus", schema = "dbo")
public class TblTenderPaymentStatus  implements java.io.Serializable {

     private int paymentStatusId;
     private TblTenderPayment tblTenderPayment;
     private String paymentStatus;
     private Date paymentStatusDt;
     private int createdBy;
     private String comments;
     private int tenderPayRefId;

    public TblTenderPaymentStatus() {
    }
    //Constructor

    public TblTenderPaymentStatus(int paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public TblTenderPaymentStatus(int paymentStatusId, TblTenderPayment tblTenderPayment, String paymentStatus, Date paymentStatusDt, int createdBy, String comments, int tenderPayRefId) {
       this.paymentStatusId = paymentStatusId;
       this.tblTenderPayment = tblTenderPayment;
       this.paymentStatus = paymentStatus;
       this.paymentStatusDt = paymentStatusDt;
       this.createdBy = createdBy;
       this.comments = comments;
       this.tenderPayRefId = tenderPayRefId;
    }

     @Id
     @GeneratedValue(generator = "TenderPaymentStatusSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "TenderPaymentStatusSequence", sequenceName = "tenderPaymentStatus_sequence", allocationSize = 25)
    @Column(name = "paymentStatusId", unique = true, nullable = false)
    public int getPaymentStatusId() {
        return this.paymentStatusId;
    }

    public void setPaymentStatusId(int paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderPaymentId", nullable = false)
    public TblTenderPayment getTblTenderPayment() {
        return this.tblTenderPayment;
    }

    public void setTblTenderPayment(TblTenderPayment tblTenderPayment) {
        this.tblTenderPayment = tblTenderPayment;
    }

    @Column(name = "paymentStatus", nullable = false, length = 15)
    public String getPaymentStatus() {
        return this.paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "paymentStatusDt", nullable = false, length = 16)
    public Date getPaymentStatusDt() {
        return this.paymentStatusDt;
    }

    public void setPaymentStatusDt(Date paymentStatusDt) {
        this.paymentStatusDt = paymentStatusDt;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "comments", nullable = false, length = 500)
    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "tenderPayRefId", nullable = false)
    public int getTenderPayRefId() {
        return this.tenderPayRefId;
    }

    public void setTenderPayRefId(int tenderPayRefId) {
        this.tenderPayRefId = tenderPayRefId;
    }
}
