/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.model.table;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Sudhir Chavhan
 */
@Entity
@Table(name="Tbl_PromisProcess",schema = "dbo")
public class TblPromisProcess implements java.io.Serializable {

    private int Process_ID;
    private int officeId;
    private String PEOffice_Name;
    private String AvgTrainedProcStaffPE;
    private String PerPETrainedProcStaff;
    private String TotalProcPersonOrgProcTraining;
    private int Ministry_ID;
    private int Division_ID;
    private int Org_ID;
    private Date Modify_Date;
    private String Is_TenderOnline;
    private String Is_Progress;
    private String prmsPECode;

    public TblPromisProcess() {
    }
    public TblPromisProcess(int Process_ID) {
        this.Process_ID = Process_ID;
    }
    public TblPromisProcess(int Process_ID, int officeId, String PEOffice_Name, String AvgTrainedProcStaffPE, String PerPETrainedProcStaff, String TotalProcPersonOrgProcTraining, int Ministry_ID, int Division_ID, int Org_ID, Date Modify_Date, String Is_TenderOnline, String Is_Progress, String prmsPECode) {
        this.Process_ID = Process_ID;
        this.officeId = officeId;
        this.PEOffice_Name = PEOffice_Name;
        this.AvgTrainedProcStaffPE = AvgTrainedProcStaffPE;
        this.PerPETrainedProcStaff = PerPETrainedProcStaff;
        this.TotalProcPersonOrgProcTraining = TotalProcPersonOrgProcTraining;
        this.Ministry_ID = Ministry_ID;
        this.Division_ID = Division_ID;
        this.Org_ID = Org_ID;
        this.Modify_Date = Modify_Date;
        this.Is_TenderOnline = Is_TenderOnline;
        this.Is_Progress = Is_Progress;
        this.prmsPECode = prmsPECode;
    }

    @Id
    @GeneratedValue(generator = "TblPromisProcessSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblPromisProcessSequence", sequenceName = "tblPromisProcess_sequence", allocationSize = 25)
    @Column(name = "Process_ID", unique = true, nullable = false)
    public int getProcess_ID() {
        return Process_ID;
    }
    public void setProcess_ID(int Process_ID) {
        this.Process_ID = Process_ID;
    }
    @Column(name = "officeId", nullable = false)
    public int getOfficeId() {
        return officeId;
    }
    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }
    @Column(name = "PEOffice_Name", nullable = true, length=50)
    public String getPEOffice_Name() {
        return PEOffice_Name;
    }
    public void setPEOffice_Name(String PEOffice_Name) {
        this.PEOffice_Name = PEOffice_Name;
    }
    @Column(name = "AvgTrainedProcStaffPE", nullable = true, length=50)
    public String getAvgTrainedProcStaffPE() {
        return AvgTrainedProcStaffPE;
    }
    public void setAvgTrainedProcStaffPE(String AvgTrainedProcStaffPE) {
        this.AvgTrainedProcStaffPE = AvgTrainedProcStaffPE;
    }
    @Column(name = "PerPETrainedProcStaff", nullable = true, length=50)
    public String getPerPETrainedProcStaff() {
        return PerPETrainedProcStaff;
    }
    public void setPerPETrainedProcStaff(String PerPETrainedProcStaff) {
        this.PerPETrainedProcStaff = PerPETrainedProcStaff;
    }
    @Column(name = "TotalProcPersonOrgProcTraining", nullable = true, length=50)
    public String getTotalProcPersonOrgProcTraining() {
        return TotalProcPersonOrgProcTraining;
    }
    public void setTotalProcPersonOrgProcTraining(String TotalProcPersonOrgProcTraining) {
        this.TotalProcPersonOrgProcTraining = TotalProcPersonOrgProcTraining;
    }
    @Column(name = "Ministry_ID", nullable = false)
    public int getMinistry_ID() {
        return Ministry_ID;
    }
    public void setMinistry_ID(int Ministry_ID) {
        this.Ministry_ID = Ministry_ID;
    }
    @Column(name = "Division_ID", nullable = false)
    public int getDivision_ID() {
        return Division_ID;
    }
    public void setDivision_ID(int Division_ID) {
        this.Division_ID = Division_ID;
    }
    @Column(name = "Org_ID", nullable = false)
    public int getOrg_ID() {
        return Org_ID;
    }
    public void setOrg_ID(int Org_ID) {
        this.Org_ID = Org_ID;
    }
    @Column(name = "Modify_Date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getModify_Date() {
        return Modify_Date;
    }
    public void setModify_Date(Date Modify_Date) {
        this.Modify_Date = Modify_Date;
    }
    @Column(name = "Is_TenderOnline", nullable = true, length = 10)
    public String getIs_TenderOnline() {
       return Is_TenderOnline;
    }
    public void setIs_TenderOnline(String Is_TenderOnline) {
        this.Is_TenderOnline = Is_TenderOnline;
    }
    @Column(name = "Is_Progress", nullable = true, length = 10)
    public String getIs_Progress() {
        return Is_Progress;
    }
    public void setIs_Progress(String Is_Progress) {
        this.Is_Progress = Is_Progress;
    }
    @Column(name = "prmsPECode", nullable = true, length = 50)
    public String getPrmsPECode() {
        return prmsPECode;
    }

    public void setPrmsPECode(String prmsPECode) {
        this.prmsPECode = prmsPECode;
    }
}
