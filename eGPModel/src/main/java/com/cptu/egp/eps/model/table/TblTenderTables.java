package com.cptu.egp.eps.model.table;
// Generated Nov 17, 2010 10:45:10 AM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblTenderTables generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderTables", schema = "dbo")
public class TblTenderTables  implements java.io.Serializable {

     private int tenderTableId;
     private TblTenderForms tblTenderForms;
     private String tableName;
     private String tableHeader;
     private String tableFooter;
     private short noOfRows;
     private short noOfCols;
     private String isMultipleFilling;
     private int templatetableId;
     //private Set<TblListCellDetail> tblListCellDetails = new HashSet<TblListCellDetail>();
     //entry for TblTenderCells
    private Set<TblTenderCells> tblTenderCellses = new HashSet<TblTenderCells>();
     //entry for TblTenderColumns
    private Set<TblTenderColumns> tblTenderColumnses = new HashSet<TblTenderColumns>();
     private Set<TblTenderListCellDetail> tblTenderListCellDetails = new HashSet<TblTenderListCellDetail>(0);

    public TblTenderTables() {
    }

    //Constructor
    public TblTenderTables(int tenderTableId) {
        this.tenderTableId = tenderTableId;
    }

    public TblTenderTables(int tenderTableId, TblTenderForms tblTenderForms, String tableName, String tableHeader, String tableFooter, short noOfRows, short noOfCols, String isMultipleFilling, int templatetableId) {
        this.tenderTableId = tenderTableId;
        this.tblTenderForms = tblTenderForms;
        this.tableName = tableName;
        this.tableHeader = tableHeader;
        this.tableFooter = tableFooter;
        this.noOfRows = noOfRows;
        this.noOfCols = noOfCols;
        this.isMultipleFilling = isMultipleFilling;
        this.templatetableId = templatetableId;
    }

    public TblTenderTables(int tenderTableId, TblTenderForms tblTenderForms, String tableName, String tableHeader, String tableFooter, short noOfRows, short noOfCols, String isMultipleFilling, int templatetableId, Set<TblTenderListCellDetail> tblTenderListCellDetails) {
       this.tenderTableId = tenderTableId;
       this.tblTenderForms = tblTenderForms;
       this.tableName = tableName;
       this.tableHeader = tableHeader;
       this.tableFooter = tableFooter;
       this.noOfRows = noOfRows;
       this.noOfCols = noOfCols;
       this.isMultipleFilling = isMultipleFilling;
       this.templatetableId = templatetableId;
       this.tblTenderListCellDetails = tblTenderListCellDetails;
    }

     @Id
     @GeneratedValue(generator = "TenderTablesSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TenderTablesSequence", sequenceName = "tenderTables_sequence", allocationSize = 25)
    @Column(name = "tenderTableId", unique = true, nullable = false)
    public int getTenderTableId() {
        return this.tenderTableId;
    }

    public void setTenderTableId(int tenderTableId) {
        this.tenderTableId = tenderTableId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderFormId", nullable = false)
    public TblTenderForms getTblTenderForms() {
        return this.tblTenderForms;
    }

    public void setTblTenderForms(TblTenderForms tblTenderForms) {
        this.tblTenderForms = tblTenderForms;
    }

    @Column(name = "tableName", nullable = false, length = 500)
    public String getTableName() {
        return this.tableName.replace("\n", "<br/>");
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Column(name = "tableHeader", nullable = false, length = 2000)
    public String getTableHeader() {
        return this.tableHeader;
    }

    public void setTableHeader(String tableHeader) {
        this.tableHeader = tableHeader;
    }

    @Column(name = "tableFooter", nullable = false, length = 2000)
    public String getTableFooter() {
        return this.tableFooter;
    }

    public void setTableFooter(String tableFooter) {
        this.tableFooter = tableFooter;
    }

    @Column(name = "noOfRows", nullable = false)
    public short getNoOfRows() {
        return this.noOfRows;
    }

    public void setNoOfRows(short noOfRows) {
        this.noOfRows = noOfRows;
    }

    @Column(name = "noOfCols", nullable = false)
    public short getNoOfCols() {
        return this.noOfCols;
    }

    public void setNoOfCols(short noOfCols) {
        this.noOfCols = noOfCols;
    }

    @Column(name = "isMultipleFilling", nullable = false, length = 3)
    public String getIsMultipleFilling() {
        return this.isMultipleFilling;
    }

    public void setIsMultipleFilling(String isMultipleFilling) {
        this.isMultipleFilling = isMultipleFilling;
    }

    @Column(name = "templatetableId", nullable = false)
    public int getTemplatetableId() {
        return this.templatetableId;
    }

    public void setTemplatetableId(int templatetableId) {
        this.templatetableId = templatetableId;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderTables")
    public Set<TblTenderListCellDetail> getTblTenderListCellDetails() {
        return this.tblTenderListCellDetails;
    }

    public void setTblTenderListCellDetails(Set<TblTenderListCellDetail> tblTenderListCellDetails) {
        this.tblTenderListCellDetails = tblTenderListCellDetails;
    }

    /*@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tblTenderTables")
    public Set<TblListCellDetail> getTblListCellDetails() {
        return this.tblListCellDetails;
    }
    
    public void setTblListCellDetails(Set<TblListCellDetail> tblListCellDetails) {
        this.tblListCellDetails = tblListCellDetails;
    }*/
    //entry for TblTenderCells
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderTables")
    public Set<TblTenderCells> getTblTenderCellses() {
        return this.tblTenderCellses;
    }

    public void setTblTenderCellses(Set<TblTenderCells> tblTenderCellses) {
        this.tblTenderCellses = tblTenderCellses;
    }
    //entry for TblTenderColumns

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderTables")
    public Set<TblTenderColumns> getTblTenderColumnses() {
        return this.tblTenderColumnses;
    }

    public void setTblTenderColumnses(Set<TblTenderColumns> tblTenderColumnses) {
        this.tblTenderColumnses = tblTenderColumnses;
    }
}
