package com.cptu.egp.eps.model.table;
// Generated Nov 30, 2010 10:56:13 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTenderGrandSum generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderGrandSum", schema = "dbo")
public class TblTenderGrandSum  implements java.io.Serializable {

     private int tenderSumId;
     private TblTenderMaster tblTenderMaster;
     private String summaryName;
     private Date creationDate;
     private int createdBy;
     private Set<TblTenderGrandSumDetail> tblTenderGrandSumDetails = new HashSet<TblTenderGrandSumDetail>();
     private int pkgLotId;

    public TblTenderGrandSum() {
    }
    //Constructor

    public TblTenderGrandSum(int tenderSumId) {
        this.tenderSumId = tenderSumId;
    }

    public TblTenderGrandSum(int tenderSumId, TblTenderMaster tblTenderMaster, String summaryName, Date creationDate, int createdBy, int pkgLotId) {
        this.tenderSumId = tenderSumId;
        this.tblTenderMaster = tblTenderMaster;
        this.summaryName = summaryName;
        this.creationDate = creationDate;
        this.createdBy = createdBy;
        this.pkgLotId = pkgLotId;
    }

    public TblTenderGrandSum(int tenderSumId, TblTenderMaster tblTenderMaster, String summaryName, Date creationDate, int createdBy, int pkgLotId, Set tblTenderGrandSumDetails) {
       this.tenderSumId = tenderSumId;
       this.tblTenderMaster = tblTenderMaster;
       this.summaryName = summaryName;
       this.creationDate = creationDate;
       this.createdBy = createdBy;
       this.tblTenderGrandSumDetails = tblTenderGrandSumDetails;
       this.pkgLotId = pkgLotId;
    }

     @Id
     @GeneratedValue(generator = "TenderGrandSumSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "TenderGrandSumSequence", sequenceName = "tenderGrandSum_sequence", allocationSize = 25)
    @Column(name = "tenderSumId", unique = true, nullable = false)
    public int getTenderSumId() {
        return this.tenderSumId;
    }

    public void setTenderSumId(int tenderSumId) {
        this.tenderSumId = tenderSumId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderId", nullable = false)
    public TblTenderMaster getTblTenderMaster() {
        return this.tblTenderMaster;
    }

    public void setTblTenderMaster(TblTenderMaster tblTenderMaster) {
        this.tblTenderMaster = tblTenderMaster;
    }

    @Column(name = "summaryName", nullable = false, length = 150)
    public String getSummaryName() {
        return this.summaryName;
    }

    public void setSummaryName(String summaryName) {
        this.summaryName = summaryName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creationDate", nullable = false, length = 16)
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblTenderGrandSum")
    public Set<TblTenderGrandSumDetail> getTblTenderGrandSumDetails() {
        return this.tblTenderGrandSumDetails;
    }

    public void setTblTenderGrandSumDetails(Set<TblTenderGrandSumDetail> tblTenderGrandSumDetails) {
        this.tblTenderGrandSumDetails = tblTenderGrandSumDetails;
    }

    public int getPkgLotId() {
        return pkgLotId;
    }

    public void setPkgLotId(int pkgLotId) {
        this.pkgLotId = pkgLotId;
    }
    
}
