package com.cptu.egp.eps.model.table;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tbl_CMS_ComplaintHistory")
public class TblComplaintHistory implements java.io.Serializable {

    private int complaintHistId;
    //complaintId
    private TblComplaintMaster complaint;
    /*//userId
    private TblLoginMaster user;*/
    //tendererId
    private int tendererId;
    //govUserId
    private int govUserId;
    private String comments;
    private Date commentsdt;
    //complaintLevelId
    private TblComplaintLevel complaintLevel;
    private String fileStatus;
    private String complaintStatus;
    private int panelId;
    private TblUserTypeMaster userRole;

    @Id
    @GeneratedValue(generator = "ComplaintHistorySequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ComplaintHistorySequence", sequenceName = "ComplaintHistorySequence")
    @Column(name = "complaintHistId", unique = true, nullable = false)
    public int getComplaintHistId() {
        return complaintHistId;
    }

    public void setComplaintHistId(int complaintHistId) {
        this.complaintHistId = complaintHistId;
    }

    @ManyToOne
    @JoinColumn(name = "complaintId")
    public TblComplaintMaster getComplaint() {
        return complaint;
    }

    public void setComplaint(TblComplaintMaster complaint) {
        this.complaint = complaint;
    }

    @Column(name = "comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "commentsdt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getCommentsdt() {
        return commentsdt;
    }

    public void setCommentsdt(Date commentsdt) {
        this.commentsdt = commentsdt;
    }

    @ManyToOne
    @JoinColumn(name = "complaintLevelId")
    public TblComplaintLevel getComplaintLevel() {
        return complaintLevel;
    }

    public void setComplaintLevel(TblComplaintLevel complaintLevel) {
        this.complaintLevel = complaintLevel;
    }

    @Column(name = "fileStatus")
    public String getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    @Column(name = "complaintStatus")
    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    @Column(name = "panelId")
    public int getPanelId() {
        return panelId;
    }

    public void setPanelId(int panelId) {
        this.panelId = panelId;
    }

    @ManyToOne
    @JoinColumn(name = "userTypeId")
    public TblUserTypeMaster getUserRole() {
        return userRole;
    }

    public void setUserRole(TblUserTypeMaster userRole) {
        this.userRole = userRole;
    }

    @Column(name = "govUserId")
    public int getGovUserId() {
        return govUserId;
    }

    public void setGovUserId(int govUserId) {
        this.govUserId = govUserId;
    }

    @Column(name = "tendererId")
    public int getTendererId() {
        return tendererId;
    }

    public void setTendererId(int tendererId) {
        this.tendererId = tendererId;
    }
}
