package com.cptu.egp.eps.model.table;
// Generated Jan 10, 2011 12:49:10 PM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblDebarmentTypes generated by hbm2java
 */
@Entity
@Table(name = "tbl_DebarmentTypes", schema = "dbo")
public class TblDebarmentTypes implements java.io.Serializable {

    private int debarTypeId;
    private String debarType;
    private Set<TblDebarmentReq> tblDebarmentReqs = new HashSet<TblDebarmentReq>();

    public TblDebarmentTypes() {
    }

    public TblDebarmentTypes(int debarTypeId) {
        this.debarTypeId = debarTypeId;
    }

    public TblDebarmentTypes(int debarTypeId, String debarType) {
        this.debarTypeId = debarTypeId;
        this.debarType = debarType;
    }

    public TblDebarmentTypes(int debarTypeId, String debarType, Set tblDebarmentReqs) {
        this.debarTypeId = debarTypeId;
        this.debarType = debarType;
        this.tblDebarmentReqs = tblDebarmentReqs;
    }

    @Id
    @GeneratedValue(generator = "TblDebarmentTypesSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblDebarmentTypesSequence", sequenceName = "tblDebarmentTypes_sequence", allocationSize = 25)
    @Column(name = "debarTypeId", unique = true, nullable = false)
    public int getDebarTypeId() {
        return this.debarTypeId;
    }

    public void setDebarTypeId(int debarTypeId) {
        this.debarTypeId = debarTypeId;
    }

    @Column(name = "debarType", nullable = false, length = 50)
    public String getDebarType() {
        return this.debarType;
    }

    public void setDebarType(String debarType) {
        this.debarType = debarType;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblDebarmentTypes")
    public Set<TblDebarmentReq> getTblDebarmentReqs() {
        return this.tblDebarmentReqs;
    }

    public void setTblDebarmentReqs(Set<TblDebarmentReq> tblDebarmentReqs) {
        this.tblDebarmentReqs = tblDebarmentReqs;
    }
}
