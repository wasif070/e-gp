package com.cptu.egp.eps.model.table;
// Generated Jun 15, 2011 4:27:13 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTenderDomesticPref generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderDomesticPref", schema = "dbo")
public class TblTenderDomesticPref implements java.io.Serializable {

    private int domPriceId;
    private TblTenderMaster tblTenderMaster;
    private int userId;
    private int createdBy;
    private Date createdDt;
    private String domesticPref;
    private String evalStatus;

    public TblTenderDomesticPref() {
    }

    public TblTenderDomesticPref(int domPriceId, int userId, int createdBy, Date createdDt, String domesticPref, String evalStatus) {
        this.domPriceId = domPriceId;
        this.userId = userId;
        this.createdBy = createdBy;
        this.createdDt = createdDt;
        this.domesticPref = domesticPref;
        this.evalStatus = evalStatus;
    }

    @Id
    @GeneratedValue(generator = "TenderDomesticPrefSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TenderDomesticPrefSequence", sequenceName = "tenderDomesticPref_sequence", allocationSize = 25)
    @Column(name = "domPriceId", unique = true, nullable = false)
    public int getDomPriceId() {
        return this.domPriceId;
    }

    public void setDomPriceId(int domPriceId) {
        this.domPriceId = domPriceId;
    }

    @Column(name = "userId", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDt", nullable = false, length = 23)
    public Date getCreatedDt() {
        return this.createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    @Column(name = "domesticPref", nullable = false, length = 3)
    public String getDomesticPref() {
        return this.domesticPref;
    }

    public void setDomesticPref(String domesticPref) {
        this.domesticPref = domesticPref;
    }

    @Column(name = "evalStatus", nullable = false, length = 15)
    public String getEvalStatus() {
        return this.evalStatus;
    }

    public void setEvalStatus(String evalStatus) {
        this.evalStatus = evalStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderId", nullable = false)
    public TblTenderMaster getTblTenderMaster() {
        return this.tblTenderMaster;
    }

    public void setTblTenderMaster(TblTenderMaster tblTenderMaster) {
        this.tblTenderMaster = tblTenderMaster;
    }
}
