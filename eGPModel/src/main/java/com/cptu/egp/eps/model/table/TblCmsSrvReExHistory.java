package com.cptu.egp.eps.model.table;
// Generated Dec 19, 2011 12:38:24 PM by Hibernate Tools 3.2.1.GA

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblCmsSrvReExHistory generated by hbm2java
 */
@Entity
@Table(name = "tbl_CMS_SrvReExHistory", schema = "dbo")
public class TblCmsSrvReExHistory implements java.io.Serializable {

    private int srvRehistId;
    private int srvReid;
    private int srvFormMapId;
    private String srNo;
    private String catagoryDesc;
    private String description;
    private String unit;
    private BigDecimal qty;
    private BigDecimal unitCost;
    private BigDecimal totalAmt;
    private int histCnt;
    private Date createdDate;

    public TblCmsSrvReExHistory() {
    }

    public TblCmsSrvReExHistory(int srvRehistId, int srvReid, int srvFormMapId, String srNo, String catagoryDesc, String description, String unit, BigDecimal qty, BigDecimal unitCost, BigDecimal totalAmt, int histCnt,Date createdDate) {
        this.srvRehistId = srvRehistId;
        this.srvReid = srvReid;
        this.srvFormMapId = srvFormMapId;
        this.srNo = srNo;
        this.catagoryDesc = catagoryDesc;
        this.description = description;
        this.unit = unit;
        this.qty = qty;
        this.unitCost = unitCost;
        this.totalAmt = totalAmt;
        this.histCnt = histCnt;
        this.createdDate = createdDate;
    }

    @Id
    @GeneratedValue(generator = "TblCmsSrvReExHistory", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblCmsSrvReExHistory", sequenceName = "TblCmsSrvReExHistory_sequence", allocationSize = 25)
    @Column(name = "srvREHistId", unique = true, nullable = false)
    public int getSrvRehistId() {
        return this.srvRehistId;
    }

    public void setSrvRehistId(int srvRehistId) {
        this.srvRehistId = srvRehistId;
    }

    @Column(name = "srvREId", nullable = false)
    public int getSrvReid() {
        return this.srvReid;
    }

    public void setSrvReid(int srvReid) {
        this.srvReid = srvReid;
    }

    @Column(name = "srvFormMapId", nullable = false)
    public int getSrvFormMapId() {
        return this.srvFormMapId;
    }

    public void setSrvFormMapId(int srvFormMapId) {
        this.srvFormMapId = srvFormMapId;
    }

    @Column(name = "srNo", nullable = false, length = 50)
    public String getSrNo() {
        return this.srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    @Column(name = "catagoryDesc", nullable = false, length = 500)
    public String getCatagoryDesc() {
        return this.catagoryDesc;
    }

    public void setCatagoryDesc(String catagoryDesc) {
        this.catagoryDesc = catagoryDesc;
    }

    @Column(name = "description", nullable = false, length = 500)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "unit", nullable = false, length = 30)
    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Column(name = "qty", nullable = false, precision = 15, scale = 3)
    public BigDecimal getQty() {
        return this.qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    @Column(name = "unitCost", nullable = false, precision = 15, scale = 3)
    public BigDecimal getUnitCost() {
        return this.unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    @Column(name = "totalAmt", nullable = false, precision = 15, scale = 3)
    public BigDecimal getTotalAmt() {
        return this.totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    @Column(name = "histCnt", nullable = false)
    public int getHistCnt() {
        return this.histCnt;
    }

    public void setHistCnt(int histCnt) {
        this.histCnt = histCnt;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="createdDate", nullable=true, length=23)
    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
