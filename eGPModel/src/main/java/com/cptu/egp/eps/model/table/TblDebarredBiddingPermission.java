package com.cptu.egp.eps.model.table;
// Generated Jun 25, 2016 1:07:07 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblDebarredBiddingPermission generated by hbm2java
 */
@Entity
@Table(name="tbl_DebarredBiddingPermission"
    ,schema="dbo"
    
)
public class TblDebarredBiddingPermission  implements java.io.Serializable {


     private long biddingPermissionId;
     private String procurementCategory;
     private String workType;
     private String workCategroy;
     private int companyId;
     private int userId;
     private Integer isReapplied;
     private Date debarredUpto;
     private Date debarredFrom;
     private String comments;
     private Date debarredDate;
     private Integer debarredBy;
     private String debarredReason;
     private Integer isActive;
     private String reinstateComments;

    public TblDebarredBiddingPermission() {
    }

	
    public TblDebarredBiddingPermission(long biddingPermissionId, String procurementCategory, int companyId, int userId) {
        this.biddingPermissionId = biddingPermissionId;
        this.procurementCategory = procurementCategory;
        this.companyId = companyId;
        this.userId = userId;
    }
    public TblDebarredBiddingPermission(long biddingPermissionId, String procurementCategory, String workType, String workCategroy, int companyId, int userId, Integer isReapplied, Date debarredUpto, Date debarredFrom, String comments, Date debarredDate, Integer debarredBy, String debarredReason, String reinstateComments, Integer isActive) {
       this.biddingPermissionId = biddingPermissionId;
       this.procurementCategory = procurementCategory;
       this.workType = workType;
       this.workCategroy = workCategroy;
       this.companyId = companyId;
       this.userId = userId;
       this.isReapplied = isReapplied;
       this.debarredUpto = debarredUpto;
       this.debarredFrom = debarredFrom;
       this.comments = comments;
       this.debarredDate = debarredDate;
       this.debarredBy = debarredBy;
       this.debarredReason = debarredReason;
       this.reinstateComments = reinstateComments;
       this.isActive=isActive;
    }
   
    @Id
    @GeneratedValue(generator = "DebarredBiddingPermissionTypeSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "DebarredBiddingPermissionTypeSequence", sequenceName = "DebarredBiddingPermission_sequence", allocationSize = 25)
    @Column(name="BiddingPermissionId", unique=true, nullable=false)
    public long getBiddingPermissionId() {
        return this.biddingPermissionId;
    }
    
    public void setBiddingPermissionId(long biddingPermissionId) {
        this.biddingPermissionId = biddingPermissionId;
    }

    
    @Column(name="ProcurementCategory", nullable=false, length=10)
    public String getProcurementCategory() {
        return this.procurementCategory;
    }
    
    public void setProcurementCategory(String procurementCategory) {
        this.procurementCategory = procurementCategory;
    }

    
    @Column(name="WorkType", length=50)
    public String getWorkType() {
        return this.workType;
    }
    
    public void setWorkType(String workType) {
        this.workType = workType;
    }

    
    @Column(name="WorkCategroy", length=50)
    public String getWorkCategroy() {
        return this.workCategroy;
    }
    
    public void setWorkCategroy(String workCategroy) {
        this.workCategroy = workCategroy;
    }

    
    @Column(name="CompanyID", nullable=false)
    public int getCompanyId() {
        return this.companyId;
    }
    
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    
    @Column(name="UserId", nullable=false)
    public int getUserId() {
        return this.userId;
    }
    
    public void setUserId(int userId) {
        this.userId = userId;
    }

    
    @Column(name="IsReapplied")
    public Integer getIsReapplied() {
        return this.isReapplied;
    }
    
    public void setIsReapplied(Integer isReapplied) {
        this.isReapplied = isReapplied;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DebarredUpto", length=16)
    public Date getDebarredUpto() {
        return this.debarredUpto;
    }
    
    public void setDebarredUpto(Date debarredUpto) {
        this.debarredUpto = debarredUpto;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DebarredFrom", length=16)
    public Date getDebarredFrom() {
        return this.debarredFrom;
    }
    
    public void setDebarredFrom(Date debarredFrom) {
        this.debarredFrom = debarredFrom;
    }

    
    @Column(name="Comments")
    public String getComments() {
        return this.comments;
    }
    
    public void setComments(String comments) {
        this.comments = comments;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DebarredDate", length=16)
    public Date getDebarredDate() {
        return this.debarredDate;
    }
    
    public void setDebarredDate(Date debarredDate) {
        this.debarredDate = debarredDate;
    }

    
    @Column(name="DebarredBy")
    public Integer getDebarredBy() {
        return this.debarredBy;
    }
    
    public void setDebarredBy(Integer debarredBy) {
        this.debarredBy = debarredBy;
    }

    
    @Column(name="DebarredReason", length=200)
    public String getDebarredReason() {
        return this.debarredReason;
    }
    
    public void setDebarredReason(String debarredReason) {
        this.debarredReason = debarredReason;
    }
    
    @Column(name="IsActive")
    public Integer getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    @Column(name="ReinstateComments")
    public String getReinstateComments() {
        return this.reinstateComments;
    }
    
    public void setReinstateComments(String reinstateComments) {
        this.reinstateComments = reinstateComments;
    }   


}


