package com.cptu.egp.eps.model.table;
// Generated Feb 25, 2011 11:24:51 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblPublicProcureForum generated by hbm2java
 */
@Entity
@Table(name = "tbl_PublicProcureForum", schema = "dbo")
public class TblPublicProcureForum implements java.io.Serializable {

    private int ppfId;
    private int postedBy;
    private String subject;
    private String description;
    private Date postDate;
    private Integer ppfParentId;
    private String fullName;
    private String status;
    private String comments;

    @Column(name = "comments", nullable = true, length = 500)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "status", nullable = false, length = 15)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "fullName", nullable = true, length = 500)
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public TblPublicProcureForum() {
    }

    public TblPublicProcureForum(int ppfId, int postedBy, String subject, String description, Date postDate) {
        this.ppfId = ppfId;
        this.postedBy = postedBy;
        this.subject = subject;
        this.description = description;
        this.postDate = postDate;
    }

    public TblPublicProcureForum(int ppfId, TblLoginMaster tblLoginMaster, String subject, String description, Date postDate, Integer ppfParentId) {
        this.ppfId = ppfId;
        this.postedBy = postedBy;
        this.subject = subject;
        this.description = description;
        this.postDate = postDate;
        this.ppfParentId = ppfParentId;
    }

    @Id
    @GeneratedValue(generator = "TblPublicProcureForumSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblPublicProcureForumSequence", sequenceName = "tblPublicProcureForum_sequence", allocationSize = 25)
    @Column(name = "ppfId", unique = true, nullable = false)
    public int getPpfId() {
        return this.ppfId;
    }

    public void setPpfId(int ppfId) {
        this.ppfId = ppfId;
    }
    
    @Column(name = "postedBy", nullable = false)
    public int getPostedBy() {
        return this.postedBy;
    }

    public void setPostedBy(int postedBy) {
        this.postedBy = postedBy;
    }

    @Column(name = "subject", nullable = false, length = 100)
    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Column(name = "description", nullable = false, length = 2000)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "postDate", nullable = false, length = 16)
    public Date getPostDate() {
        return this.postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    @Column(name = "ppfParentId")
    public Integer getPpfParentId() {
        return this.ppfParentId;
    }

    public void setPpfParentId(Integer ppfParentId) {
        this.ppfParentId = ppfParentId;
    }
}
