package com.cptu.egp.eps.model.table;
// Generated Dec 15, 2011 2:36:08 PM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblNegBidDtlSrv generated by hbm2java
 */
@Entity
@Table(name="tbl_NegBidDtlSrv"
    ,schema="dbo"
)
public class TblNegBidDtlSrv  implements java.io.Serializable {


     private int negBidDtlId;
     private int negBidTableId;
     private int tenderColumnId;
     private int tenderTableId;
     private String cellValue;
     private int rowId;
     private int cellId;
     private int formId;
     private int negId;

    public TblNegBidDtlSrv() {
    }

    public TblNegBidDtlSrv(int negBidDtlId, int negBidTableId, int tenderColumnId, int tenderTableId, String cellValue, int rowId, int cellId, int formId, int negId) {
       this.negBidDtlId = negBidDtlId;
       this.negBidTableId = negBidTableId;
       this.tenderColumnId = tenderColumnId;
       this.tenderTableId = tenderTableId;
       this.cellValue = cellValue;
       this.rowId = rowId;
       this.cellId = cellId;
       this.formId = formId;
       this.negId = negId;
    }
   
     @Id 
    @GeneratedValue(generator = "TblNegBidDtlSrvSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblNegBidDtlSrvSequence", sequenceName = "tblNegBidDtlSrv_sequence", allocationSize = 25)
    @Column(name="negBidDtlId", unique=true, nullable=false)
    public int getNegBidDtlId() {
        return this.negBidDtlId;
    }
    
    public void setNegBidDtlId(int negBidDtlId) {
        this.negBidDtlId = negBidDtlId;
    }
    
    @Column(name="negBidTableId", nullable=false)
    public int getNegBidTableId() {
        return this.negBidTableId;
    }
    
    public void setNegBidTableId(int negBidTableId) {
        this.negBidTableId = negBidTableId;
    }
    
    @Column(name="tenderColumnId", nullable=false)
    public int getTenderColumnId() {
        return this.tenderColumnId;
    }
    
    public void setTenderColumnId(int tenderColumnId) {
        this.tenderColumnId = tenderColumnId;
    }
    
    @Column(name="tenderTableId", nullable=false)
    public int getTenderTableId() {
        return this.tenderTableId;
    }
    
    public void setTenderTableId(int tenderTableId) {
        this.tenderTableId = tenderTableId;
    }
    
    @Column(name="cellValue", nullable=false)
    public String getCellValue() {
        return this.cellValue;
    }
    
    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }
    
    @Column(name="rowId", nullable=false)
    public int getRowId() {
        return this.rowId;
    }
    
    public void setRowId(int rowId) {
        this.rowId = rowId;
    }
    
    @Column(name="cellId", nullable=false)
    public int getCellId() {
        return this.cellId;
    }
    
    public void setCellId(int cellId) {
        this.cellId = cellId;
    }
    
    @Column(name="formId", nullable=false)
    public int getFormId() {
        return this.formId;
    }
    
    public void setFormId(int formId) {
        this.formId = formId;
    }
    
    @Column(name="negId", nullable=false)
    public int getNegId() {
        return this.negId;
    }
    
    public void setNegId(int negId) {
        this.negId = negId;
    }




}


