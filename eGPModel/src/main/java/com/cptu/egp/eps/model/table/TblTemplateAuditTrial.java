package com.cptu.egp.eps.model.table;
// Generated Oct 17, 2010 12:49:31 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTemplateAuditTrial generated by hbm2java
 */
@Entity
@Table(name = "tbl_TemplateAuditTrial", schema = "dbo")
public class TblTemplateAuditTrial  implements java.io.Serializable {

     private int templateAuditId;
     private short templateId;
     private int childId;
     private String childType;
     private String oldValue;
     private String newValue;
     private String childName;
     private Date actionDate;
     private int actionBy;

    public TblTemplateAuditTrial() {
    }

    public TblTemplateAuditTrial(int templateAuditId, short templateId, int childId, String childType, String oldValue, String newValue, String childName, Date actionDate, int actionBy) {
       this.templateAuditId = templateAuditId;
       this.templateId = templateId;
       this.childId = childId;
       this.childType = childType;
       this.oldValue = oldValue;
       this.newValue = newValue;
       this.childName = childName;
       this.actionDate = actionDate;
       this.actionBy = actionBy;
    }

    @Id
    @GeneratedValue(generator = "TemplateAuditTrialSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TemplateAuditTrialSequence", sequenceName = "templateAuditTrial_sequence", allocationSize = 25)
    @Column(name = "templateAuditId", unique = true, nullable = false)
    public int getTemplateAuditId() {
        return this.templateAuditId;
    }

    public void setTemplateAuditId(int templateAuditId) {
        this.templateAuditId = templateAuditId;
    }

    @Column(name = "templateId", nullable = false)
    public short getTemplateId() {
        return this.templateId;
    }

    public void setTemplateId(short templateId) {
        this.templateId = templateId;
    }

    @Column(name = "childId", nullable = false)
    public int getChildId() {
        return this.childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Column(name = "childType", nullable = false, length = 20)
    public String getChildType() {
        return this.childType;
    }

    public void setChildType(String childType) {
        this.childType = childType;
    }

    @Column(name = "oldValue", nullable = false, length = 4000)
    public String getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Column(name = "newValue", nullable = false, length = 4000)
    public String getNewValue() {
        return this.newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Column(name = "childName", nullable = false, length = 4000)
    public String getChildName() {
        return this.childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "actionDate", nullable = false, length = 16)
    public Date getActionDate() {
        return this.actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Column(name = "actionBY", nullable = false)
    public int getActionBy() {
        return this.actionBy;
    }

    public void setActionBy(int actionBy) {
        this.actionBy = actionBy;
    }
}
