package com.cptu.egp.eps.model.table;
// Generated Nov 28, 2010 12:30:32 PM by Hibernate Tools 3.2.1.GA

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTenderAdvertisement generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderAdvertisement", schema = "dbo")
public class TblTenderAdvertisement  implements java.io.Serializable {

     private int tenderAdvtId;
     private TblTenderMaster tblTenderMaster;
     private String newsPaper;
     private Serializable newsPaperPubDt;
     private String webSite;
     private Serializable webSiteAdvtDt;
     private Date creationDate;
     //private Date urlPubDt;

    public TblTenderAdvertisement() {
    }
    //Constructor

    public TblTenderAdvertisement(int tenderAdvtId) {
        this.tenderAdvtId = tenderAdvtId;
    }

    public TblTenderAdvertisement(int tenderAdvtId, TblTenderMaster tblTenderMaster, String newsPaper, Serializable newsPaperPubDt, String webSite, Serializable webSiteAdvtDt, Date creationDate) {
       this.tenderAdvtId = tenderAdvtId;
       this.tblTenderMaster = tblTenderMaster;
       this.newsPaper = newsPaper;
       this.newsPaperPubDt = newsPaperPubDt;
       this.webSite = webSite;
       this.webSiteAdvtDt = webSiteAdvtDt;
       this.creationDate = creationDate;
    }

     @Id
     @GeneratedValue(generator = "TenderAdvertisementSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "TenderAdvertisementSequence", sequenceName = "tenderAdvertisement_sequence", allocationSize = 25)
    @Column(name = "tenderAdvtId", unique = true, nullable = false)
    public int getTenderAdvtId() {
        return this.tenderAdvtId;
    }

    public void setTenderAdvtId(int tenderAdvtId) {
        this.tenderAdvtId = tenderAdvtId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderId", nullable = false)
    public TblTenderMaster getTblTenderMaster() {
        return this.tblTenderMaster;
    }

    public void setTblTenderMaster(TblTenderMaster tblTenderMaster) {
        this.tblTenderMaster = tblTenderMaster;
    }

    @Column(name = "newsPaper", nullable = false, length = 500)
    public String getNewsPaper() {
        return this.newsPaper;
    }

    public void setNewsPaper(String newsPaper) {
        this.newsPaper = newsPaper;
    }

    @Column(name = "newsPaperPubDt", nullable = false)
    public Serializable getNewsPaperPubDt() {
        return this.newsPaperPubDt;
    }

    public void setNewsPaperPubDt(Serializable newsPaperPubDt) {
        this.newsPaperPubDt = newsPaperPubDt;
    }

    @Column(name = "webSite", nullable = false, length = 500)
    public String getWebSite() {
        return this.webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    @Column(name = "webSiteAdvtDt", nullable = false)
    public Serializable getWebSiteAdvtDt() {
        return this.webSiteAdvtDt;
    }

    public void setWebSiteAdvtDt(Serializable webSiteAdvtDt) {
        this.webSiteAdvtDt = webSiteAdvtDt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creationDate", nullable = false, length = 16)
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
//    @Temporal(TemporalType.TIMESTAMP)
//    @Column(name="urlPubDt", nullable=false, length=23)
//    public Date getUrlPubDt() {
//        return this.urlPubDt;
//    }
//
//    public void setUrlPubDt(Date urlPubDt) {
//        this.urlPubDt = urlPubDt;
//    }
}
