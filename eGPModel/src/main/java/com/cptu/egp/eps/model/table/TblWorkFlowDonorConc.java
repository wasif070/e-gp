package com.cptu.egp.eps.model.table;
// Generated Oct 25, 2010 9:33:12 AM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblWorkFlowDonorConc generated by hbm2java
 */
@Entity
@Table(name="tbl_WorkFlowDonorConc"
    ,schema="dbo"
    
)
public class TblWorkFlowDonorConc  implements java.io.Serializable {


     private int wfDonorId;
     private TblModuleMaster tblModuleMaster;
     private int objectId;
     private String isDonConReq;

    public TblWorkFlowDonorConc() {
    }

    public TblWorkFlowDonorConc(int wfDonorId) {
        this.wfDonorId = wfDonorId;
    }



    public TblWorkFlowDonorConc(int wfDonorId, TblModuleMaster tblModuleMaster, int objectId, String isDonConReq) {
       this.wfDonorId = wfDonorId;
       this.tblModuleMaster = tblModuleMaster;
       this.objectId = objectId;
       this.isDonConReq = isDonConReq;
    }
   
     @Id 
    @GeneratedValue(generator = "WorkFlowDonorConcSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "WorkFlowDonorConcSequence", sequenceName = "workFlowDonorConc_sequence", allocationSize = 25)
    @Column(name="wfDonorId", unique=true, nullable=false)
    public int getWfDonorId() {
        return this.wfDonorId;
    }
    
    public void setWfDonorId(int wfDonorId) {
        this.wfDonorId = wfDonorId;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="moduleId", nullable=false)
    public TblModuleMaster getTblModuleMaster() {
        return this.tblModuleMaster;
    }
    
    public void setTblModuleMaster(TblModuleMaster tblModuleMaster) {
        this.tblModuleMaster = tblModuleMaster;
    }
    
    @Column(name="objectId", nullable=false)
    public int getObjectId() {
        return this.objectId;
    }
    
    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }
    
    @Column(name="isDonConReq", nullable=false, length=3)
    public String getIsDonConReq() {
        return this.isDonConReq;
    }
    
    public void setIsDonConReq(String isDonConReq) {
        this.isDonConReq = isDonConReq;
    }




}


