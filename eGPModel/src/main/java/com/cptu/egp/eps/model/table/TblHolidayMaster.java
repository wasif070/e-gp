package com.cptu.egp.eps.model.table;
// Generated Oct 30, 2010 9:29:55 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * TblHolidayMaster generated by hbm2java
 */
@Entity
@Table(name = "tbl_HolidayMaster", schema = "dbo")
public class TblHolidayMaster  implements java.io.Serializable {

     private int holidayId;
     private Date holidayDate;
     private String isWeekend;
     private String year;
     private String description;
     
    public TblHolidayMaster() {
    }

    public TblHolidayMaster(int holidayId) {
        this.holidayId = holidayId;
    }

    public TblHolidayMaster(int holidayId, Date holidayDate, String isWeekend, String year) {
       this.holidayId = holidayId;
       this.holidayDate = holidayDate;
       this.isWeekend = isWeekend;
       this.year = year;
    }

    @Id
    @GeneratedValue(generator = "HolidayMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "HolidayMasterSequence", sequenceName = "holidayMaster_sequence", allocationSize = 25)
    @Column(name = "holidayId", unique = true, nullable = false)
    public int getHolidayId() {
        return this.holidayId;
    }

    public void setHolidayId(int holidayId) {
        this.holidayId = holidayId;
    }

    @Column(name = "holidayDate", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getHolidayDate() {
        return this.holidayDate;
    }

    public void setHolidayDate(Date holidayDate) {
        this.holidayDate = holidayDate;
    }

    @Column(name = "isWeekend", nullable = false, length = 3)
    public String getIsWeekend() {
        return this.isWeekend;
    }

    public void setIsWeekend(String isWeekend) {
        this.isWeekend = isWeekend;
    }

    @Column(name = "year", nullable = false, length = 4)
    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the description
     */
    @Column(name = "Description", nullable = false, length = 50)
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
