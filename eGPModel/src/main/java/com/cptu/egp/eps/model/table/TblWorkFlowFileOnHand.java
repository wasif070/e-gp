package com.cptu.egp.eps.model.table;
// Generated Oct 25, 2010 9:33:12 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblWorkFlowFileOnHand generated by hbm2java
 */
@Entity
@Table(name = "tbl_WorkFlowFileOnHand", schema = "dbo")
public class TblWorkFlowFileOnHand  implements java.io.Serializable {

     private int wflowFileOnHandId;
     private TblLoginMaster tblLoginMasterByToUserId;
     private TblEventMaster tblEventMaster;
     private TblLoginMaster tblLoginMasterByFromUserId;
     private TblActivityMaster tblActivityMaster;
     private byte moduleId;
     private int objectId;
     private int childId;
     private Date processDate;
     private Date endDate;
     private String action;
     private String workflowDirection;
     private byte wfRoleId;
     private String remarks;
     private String wfTrigger;
     private String fileSentFrom;
     private String fileSentTo;

    public TblWorkFlowFileOnHand() {
    }

    public TblWorkFlowFileOnHand(int wflowFileOnHandId) {
        this.wflowFileOnHandId = wflowFileOnHandId;
    }

    public TblWorkFlowFileOnHand(int wflowFileOnHandId, TblLoginMaster tblLoginMasterByToUserId, TblEventMaster tblEventMaster, TblLoginMaster tblLoginMasterByFromUserId, TblActivityMaster tblActivityMaster, byte moduleId, int objectId, int childId, Date processDate, Date endDate, String action, String workflowDirection, byte wfRoleId, String remarks, String wfTrigger, String fileSentFrom, String fileSentTo) {
        this.wflowFileOnHandId = wflowFileOnHandId;
        this.tblLoginMasterByToUserId = tblLoginMasterByToUserId;
        this.tblEventMaster = tblEventMaster;
        this.tblLoginMasterByFromUserId = tblLoginMasterByFromUserId;
        this.tblActivityMaster = tblActivityMaster;
        this.moduleId = moduleId;
        this.objectId = objectId;
        this.childId = childId;
        this.processDate = processDate;
        this.endDate = endDate;
        this.action = action;
        this.workflowDirection = workflowDirection;
        this.wfRoleId = wfRoleId;
        this.remarks = remarks;
        this.wfTrigger = wfTrigger;
        this.fileSentFrom = fileSentFrom;
        this.fileSentTo = fileSentTo;
    }

    public TblWorkFlowFileOnHand(int wflowFileOnHandId, TblLoginMaster tblLoginMasterByToUserId, TblEventMaster tblEventMaster, TblLoginMaster tblLoginMasterByFromUserId, TblActivityMaster tblActivityMaster, byte moduleId, int objectId, int childId, Date processDate, Date endDate, String action, String workflowDirection, byte wfRoleId, String remarks, String wfTrigger, String fileSentFrom, String fileSentTo, Set tblWorkFlowFileHistories) {
       this.wflowFileOnHandId = wflowFileOnHandId;
       this.tblLoginMasterByToUserId = tblLoginMasterByToUserId;
       this.tblEventMaster = tblEventMaster;
       this.tblLoginMasterByFromUserId = tblLoginMasterByFromUserId;
       this.tblActivityMaster = tblActivityMaster;
       this.moduleId = moduleId;
       this.objectId = objectId;
       this.childId = childId;
       this.processDate = processDate;
       this.endDate = endDate;
       this.action = action;
       this.workflowDirection = workflowDirection;
       this.wfRoleId = wfRoleId;
       this.remarks = remarks;
       this.wfTrigger = wfTrigger;
       this.fileSentFrom = fileSentFrom;
       this.fileSentTo = fileSentTo;

    }

    @Id
    @GeneratedValue(generator = "WorkFlowFileOnHandSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "WorkFlowFileOnHandSequence", sequenceName = "workFlowFileOnHand_sequence", allocationSize = 25)
    @Column(name = "wflowFileOnHandId", unique = true, nullable = false)
    public int getWflowFileOnHandId() {
        return this.wflowFileOnHandId;
    }

    public void setWflowFileOnHandId(int wflowFileOnHandId) {
        this.wflowFileOnHandId = wflowFileOnHandId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "toUserId", nullable = false)
    public TblLoginMaster getTblLoginMasterByToUserId() {
        return this.tblLoginMasterByToUserId;
    }

    public void setTblLoginMasterByToUserId(TblLoginMaster tblLoginMasterByToUserId) {
        this.tblLoginMasterByToUserId = tblLoginMasterByToUserId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventId", nullable = false)
    public TblEventMaster getTblEventMaster() {
        return this.tblEventMaster;
    }

    public void setTblEventMaster(TblEventMaster tblEventMaster) {
        this.tblEventMaster = tblEventMaster;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fromUserId", nullable = false)
    public TblLoginMaster getTblLoginMasterByFromUserId() {
        return this.tblLoginMasterByFromUserId;
    }

    public void setTblLoginMasterByFromUserId(TblLoginMaster tblLoginMasterByFromUserId) {
        this.tblLoginMasterByFromUserId = tblLoginMasterByFromUserId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activityId", nullable = false)
    public TblActivityMaster getTblActivityMaster() {
        return this.tblActivityMaster;
    }

    public void setTblActivityMaster(TblActivityMaster tblActivityMaster) {
        this.tblActivityMaster = tblActivityMaster;
    }

    @Column(name = "moduleId", nullable = false)
    public byte getModuleId() {
        return this.moduleId;
    }

    public void setModuleId(byte moduleId) {
        this.moduleId = moduleId;
    }

    @Column(name = "objectId", nullable = false)
    public int getObjectId() {
        return this.objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    @Column(name = "childId", nullable = false)
    public int getChildId() {
        return this.childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "processDate", nullable = false, length = 16)
    public Date getProcessDate() {
        return this.processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate", nullable = false, length = 16)
    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "action", nullable = false, length = 20)
    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "workflowDirection", nullable = false, length = 4)
    public String getWorkflowDirection() {
        return this.workflowDirection;
    }

    public void setWorkflowDirection(String workflowDirection) {
        this.workflowDirection = workflowDirection;
    }

    @Column(name = "wfRoleId", nullable = false)
    public byte getWfRoleId() {
        return this.wfRoleId;
    }

    public void setWfRoleId(byte wfRoleId) {
        this.wfRoleId = wfRoleId;
    }

    @Column(name = "remarks", nullable = false, length = 1000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "wfTrigger", nullable = false, length = 10)
    public String getWfTrigger() {
        return this.wfTrigger;
    }

    public void setWfTrigger(String wfTrigger) {
        this.wfTrigger = wfTrigger;
    }

    @Column(name = "fileSentFrom", nullable = false, length = 400)
    public String getFileSentFrom() {
        return this.fileSentFrom;
    }

    public void setFileSentFrom(String fileSentFrom) {
        this.fileSentFrom = fileSentFrom;
    }

    @Column(name = "fileSentTo", nullable = false, length = 400)
    public String getFileSentTo() {
        return this.fileSentTo;
    }

    public void setFileSentTo(String fileSentTo) {
        this.fileSentTo = fileSentTo;
    }
}
