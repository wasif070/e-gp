package com.cptu.egp.eps.model.table;
// Generated Oct 25, 2010 9:33:12 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblMessageInBox generated by hbm2java
 */
@Entity
@Table(name = "tbl_MessageInBox", schema = "dbo")
public class TblMessageInBox  implements java.io.Serializable {

     private int msgBoxId;
     private TblLoginMaster tblLoginMasterByMsgFromUserId;
     private TblMessage tblMessage;
     private TblLoginMaster tblLoginMasterByMsgToUserId;
     private String msgToCcreply;
     private String msgStatus;
     private Date creationDate;
     private int folderId;
     private String isPriority;
     private String isMsgRead;
     private String isDocUploaded;
     private String processUrl;

    public TblMessageInBox() {
    }

    public TblMessageInBox(int msgBoxId) {
        this.msgBoxId = msgBoxId;
    }

    public TblMessageInBox(int msgBoxId, TblLoginMaster tblLoginMasterByMsgFromUserId, TblMessage tblMessage, TblLoginMaster tblLoginMasterByMsgToUserId, String msgToCcreply, String msgStatus, Date creationDate, int folderId, String isPriority, String isMsgRead, String isDocUploaded, String processUrl) {
       this.msgBoxId = msgBoxId;
       this.tblLoginMasterByMsgFromUserId = tblLoginMasterByMsgFromUserId;
       this.tblMessage = tblMessage;
       this.tblLoginMasterByMsgToUserId = tblLoginMasterByMsgToUserId;
       this.msgToCcreply = msgToCcreply;
       this.msgStatus = msgStatus;
       this.creationDate = creationDate;
       this.folderId = folderId;
       this.isPriority = isPriority;
       this.isMsgRead = isMsgRead;
       this.isDocUploaded = isDocUploaded;
       this.processUrl = processUrl;
    }

    @Id
    @GeneratedValue(generator = "MessageInBox Sequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "MessageInBox Sequence", sequenceName = "messageInBox _sequence", allocationSize = 25)
    @Column(name = "msgBoxId", unique = true, nullable = false)
    public int getMsgBoxId() {
        return this.msgBoxId;
    }

    public void setMsgBoxId(int msgBoxId) {
        this.msgBoxId = msgBoxId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "msgFromUserId", nullable = false)
    public TblLoginMaster getTblLoginMasterByMsgFromUserId() {
        return this.tblLoginMasterByMsgFromUserId;
    }

    public void setTblLoginMasterByMsgFromUserId(TblLoginMaster tblLoginMasterByMsgFromUserId) {
        this.tblLoginMasterByMsgFromUserId = tblLoginMasterByMsgFromUserId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "msgId", nullable = false)
    public TblMessage getTblMessage() {
        return this.tblMessage;
    }

    public void setTblMessage(TblMessage tblMessage) {
        this.tblMessage = tblMessage;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "msgToUserId", nullable = false)
    public TblLoginMaster getTblLoginMasterByMsgToUserId() {
        return this.tblLoginMasterByMsgToUserId;
    }

    public void setTblLoginMasterByMsgToUserId(TblLoginMaster tblLoginMasterByMsgToUserId) {
        this.tblLoginMasterByMsgToUserId = tblLoginMasterByMsgToUserId;
    }

    @Column(name = "msgToCCReply", nullable = false, length = 5)
    public String getMsgToCcreply() {
        return this.msgToCcreply;
    }

    public void setMsgToCcreply(String msgToCcreply) {
        this.msgToCcreply = msgToCcreply;
    }

    @Column(name = "msgStatus", nullable = false, length = 10)
    public String getMsgStatus() {
        return this.msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creationDate", nullable = false, length = 16)
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "folderId", nullable = false)
    public int getFolderId() {
        return this.folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    @Column(name = "isPriority", nullable = false, length = 3)
    public String getIsPriority() {
        return this.isPriority;
    }

    public void setIsPriority(String isPriority) {
        this.isPriority = isPriority;
    }

    @Column(name = "isMsgRead", nullable = false, length = 3)
    public String getIsMsgRead() {
        return this.isMsgRead;
    }

    public void setIsMsgRead(String isMsgRead) {
        this.isMsgRead = isMsgRead;
    }

    @Column(name = "isDocUploaded", nullable = false, length = 3)
    public String getIsDocUploaded() {
        return this.isDocUploaded;
    }

    public void setIsDocUploaded(String isDocUploaded) {
        this.isDocUploaded = isDocUploaded;
    }

    @Column(name = "processUrl", nullable = false, length = 250)
    public String getProcessUrl() {
        return this.processUrl;
    }

    public void setProcessUrl(String processUrl) {
        this.processUrl = processUrl;
    }
}
