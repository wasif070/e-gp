package com.cptu.egp.eps.model.table;
// Generated Jul 7, 2011 2:50:49 PM by Hibernate Tools 3.2.1.GA


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblEvalTernotifyMember generated by hbm2java
 */
@Entity
@Table(name="tbl_EvalTERNotifyMember"
    ,schema="dbo"
)
public class TblEvalTernotifyMember  implements java.io.Serializable {


     private int evalNotId;
     private int tenderId;
     private int pkgLotId;
     private int sentBy;
     private Date sentDt;
     private String reportType;
     private String notifyType;
     private int roundId;

    public TblEvalTernotifyMember() {
    }

    public TblEvalTernotifyMember(int evalNotId, int tenderId, int pkgLotId, int sentBy, Date sentDt, String reportType, String notifyType,int roundId) {
       this.evalNotId = evalNotId;
       this.tenderId = tenderId;
       this.pkgLotId = pkgLotId;
       this.sentBy = sentBy;
       this.sentDt = sentDt;
       this.reportType = reportType;
       this.notifyType = notifyType;
       this.roundId = roundId;
    }

    @Id
    @GeneratedValue(generator = "TblEvalTernotifyMemberSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "TblEvalTernotifyMemberSequence", sequenceName = "tblEvalTernotifyMember_sequence", allocationSize = 25)
    @Column(name="evalNotId", unique=true, nullable=false)
    public int getEvalNotId() {
        return this.evalNotId;
    }

    public void setEvalNotId(int evalNotId) {
        this.evalNotId = evalNotId;
    }

    @Column(name="tenderId", nullable=false)
    public int getTenderId() {
        return this.tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    @Column(name="pkgLotId", nullable=false)
    public int getPkgLotId() {
        return this.pkgLotId;
    }

    public void setPkgLotId(int pkgLotId) {
        this.pkgLotId = pkgLotId;
    }

    @Column(name="sentBy", nullable=false)
    public int getSentBy() {
        return this.sentBy;
    }

    public void setSentBy(int sentBy) {
        this.sentBy = sentBy;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="sentDt", nullable=false, length=23)
    public Date getSentDt() {
        return this.sentDt;
    }

    public void setSentDt(Date sentDt) {
        this.sentDt = sentDt;
    }

    @Column(name="reportType", nullable=false, length=5)
    public String getReportType() {
        return this.reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    @Column(name="notifyType", nullable=false, length=10)
    public String getNotifyType() {
        return this.notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    @Column(name="roundId", nullable=false)
    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }


    

}


