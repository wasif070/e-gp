package com.cptu.egp.eps.model.table;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tbl_CMS_ComplaintMaster")
public class TblComplaintMaster implements java.io.Serializable {

    private int complaintId;
    //tenderId (tbl_TendererMaster)
    private TblTenderMaster tenderMaster;
    //tendererId
    private TblTendererMaster tendererMaster;
    private String complaintSubject;
    private String complaintDetails;
    private String tendererComments;
    //govUserId
    private TblEmployeeTrasfer employeeTrasfer;
    private String officerComments;
    //panelId
    private int panelId;
    //complaintLevelId
    private TblComplaintLevel complaintLevel;
    private String complaintStatus;
    private Date complaintCreationDt;
    private Date officerProcessDate;
    private String notificationRemarksToPe;
    private Date notificationDtToPe;
    private Date lastModificationDt;
    private Date assignedtoPanelDt;
    //complaintTypeId
    private TblComplaintType complaintType;
    //paymentStatus
    private String paymentStatus;
    private String tenderRefNo;
    private String peResolved;
    private String hopeResolved;
    private String secResolved;
    private String rpResolved;
     private Set<TblComplaintPayments> tblComplaintPayments  = new HashSet<TblComplaintPayments>();

    public TblComplaintMaster() {
    }

    public TblComplaintMaster(int complaintId) {
        this.complaintId = complaintId;
    }




    @Id
    @GeneratedValue(generator = "ComplaintMasterSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ComplaintMasterSequence", sequenceName = "ComplaintMasterSequence")
    @Column(name = "complaintId", unique = true, nullable = false)
    public int getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(int complaintId) {
        this.complaintId = complaintId;
    }

    @Column(name = "complaintSubject")
    public String getComplaintSubject() {
        return complaintSubject;
    }

    public void setComplaintSubject(String complaintSubject) {
        this.complaintSubject = complaintSubject;
    }

    @Column(name = "complaintDetails")
    public String getComplaintDetails() {
        return complaintDetails;
    }

    public void setComplaintDetails(String complaintDetails) {
        this.complaintDetails = complaintDetails;
    }

    @Column(name = "tendererComments")
    public String getTendererComments() {
        return tendererComments;
    }

    public void setTendererComments(String tendererComments) {
        this.tendererComments = tendererComments;
    }

    @Column(name = "officerComments")
    public String getOfficerComments() {
        return officerComments;
    }

    public void setOfficerComments(String officerComments) {
        this.officerComments = officerComments;
    }

    @Column(name = "panelId")
    public int getPanelId() {
        return panelId;
    }

    public void setPanelId(int panelId) {
        this.panelId = panelId;
    }

    @ManyToOne
    @JoinColumn(name = "complaintLevelId")
    public TblComplaintLevel getComplaintLevel() {
        return complaintLevel;
    }

    public void setComplaintLevel(TblComplaintLevel complaintLevel) {
        this.complaintLevel = complaintLevel;
    }

    @Column(name = "complaintStatus")
    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    @Column(name = "complaintCreationDt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getComplaintCreationDt() {
        return complaintCreationDt;
    }

    public void setComplaintCreationDt(Date complaintCreationDt) {
        this.complaintCreationDt = complaintCreationDt;
    }

    @Column(name = "officerProcessDate")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getOfficerProcessDate() {
        return officerProcessDate;
    }

    public void setOfficerProcessDate(Date officerProcessDate) {
        this.officerProcessDate = officerProcessDate;
    }

    @Column(name = "notificationRemarksToPe")
    public String getNotificationRemarksToPe() {
        return notificationRemarksToPe;
    }

    public void setNotificationRemarksToPe(String notificationRemarksToPe) {
        this.notificationRemarksToPe = notificationRemarksToPe;
    }

    @Column(name = "notificationDtToPe")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getNotificationDtToPe() {
        return notificationDtToPe;
    }

    public void setNotificationDtToPe(Date notificationDtToPe) {
        this.notificationDtToPe = notificationDtToPe;
    }

    @Column(name = "lastModificationDt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getLastModificationDt() {
        return lastModificationDt;
    }

    public void setLastModificationDt(Date lastModificationDt) {
        this.lastModificationDt = lastModificationDt;
    }

    @Column(name = "assignedtoPanelDt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getAssignedtoPanelDt() {
        return assignedtoPanelDt;
    }

    public void setAssignedtoPanelDt(Date assignedtoPanelDt) {
        this.assignedtoPanelDt = assignedtoPanelDt;
    }

    @ManyToOne
    @JoinColumn(name = "complaintTypeId")
    public TblComplaintType getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(TblComplaintType complaintType) {
        this.complaintType = complaintType;
    }

    @Column(name = "paymentStatus")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @ManyToOne
    @JoinColumn(name = "govUserId")
    public TblEmployeeTrasfer getEmployeeTrasfer() {
        return employeeTrasfer;
    }

    public void setEmployeeTrasfer(TblEmployeeTrasfer employeeTrasfer) {
        this.employeeTrasfer = employeeTrasfer;
    }

    @ManyToOne
    @JoinColumn(name = "tenderId")
    public TblTenderMaster getTenderMaster() {
        return tenderMaster;
    }

    public void setTenderMaster(TblTenderMaster tenderMaster) {
        this.tenderMaster = tenderMaster;
    }

    @ManyToOne
    @JoinColumn(name = "tendererId")
    public TblTendererMaster getTendererMaster() {
        return tendererMaster;
    }

    public void setTendererMaster(TblTendererMaster tendererMaster) {
        this.tendererMaster = tendererMaster;
    }
    @Column(name = "tenderRefNo")
    public String getTenderRefNo() {
        return tenderRefNo;
    }

    public void setTenderRefNo(String tenderRefNo) {
        this.tenderRefNo = tenderRefNo;
    }
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "complaintMaster")
    public Set<TblComplaintPayments> getTblComplaintPayments() {
        return tblComplaintPayments;
    }

    public void setTblComplaintPayments(Set<TblComplaintPayments> tblComplaintPayments) {
        this.tblComplaintPayments = tblComplaintPayments;
    }
    @Column(name="hopeResolved")
    public String getHopeResolved() {
        return hopeResolved;
    }

    public void setHopeResolved(String hopeResolved) {
        this.hopeResolved = hopeResolved;
    }

    @Column(name="peResolved")
    public String getPeResolved() {
        return peResolved;
    }

    public void setPeResolved(String peResolved) {
        this.peResolved = peResolved;
    }

    @Column(name="rpResolved")
    public String getRpResolved() {
        return rpResolved;
    }

    public void setRpResolved(String rpResolved) {
        this.rpResolved = rpResolved;
    }

    @Column(name="secResolved")
    public String getSecResolved() {
        return secResolved;
    }

    public void setSecResolved(String secResolved) {
        this.secResolved = secResolved;
    }


}
