package com.cptu.egp.eps.model.table;
// Generated Apr 14, 2011 11:59:25 AM by Hibernate Tools 3.2.1.GA

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblEmployeeTrasfer generated by hbm2java
 */
@Entity
@Table(name = "tbl_EmployeeTrasfer", schema = "dbo")
public class TblEmployeeTrasfer implements java.io.Serializable {

    private int govUserId;
    private TblLoginMaster tblLoginMaster;
    private Date transferDt;
    private int createdBy;
    private String remarks;
    private String employeeName;
    private byte[] employeeNameBangla;
    private String mobileNo;
    private String nationalId;
    private String finPowerBy;
    private int employeeId;
    private String action;
    private String emailId;
    private String replacedBy;
    private String replacedByEmailId;
    private String isCurrent;
    private String comments;
    private String userEmployeeId;
    private String contactAddress;
    private Set<TblComplaintMaster> tblComplaintMasters = new HashSet<TblComplaintMaster>();

    public TblEmployeeTrasfer() {
    }

    public TblEmployeeTrasfer(int govUserId, int userId, Date transferDt, int createdBy, String remarks, String employeeName, String mobileNo, String nationalId, String finPowerBy, int employeeId, String action, String emailId, String replacedBy, String replacedByEmailId) {
        this.govUserId = govUserId;
        this.transferDt = transferDt;
        this.createdBy = createdBy;
        this.remarks = remarks;
        this.employeeName = employeeName;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.finPowerBy = finPowerBy;
        this.employeeId = employeeId;
        this.action = action;
        this.emailId = emailId;
        this.replacedBy = replacedBy;
        this.replacedByEmailId = replacedByEmailId;
    }

    public TblEmployeeTrasfer(int govUserId, int userId, Date transferDt, int createdBy, String remarks, String employeeName, Serializable employeeNameBangla, String mobileNo, String nationalId, String finPowerBy, int employeeId, String action, String emailId, String replacedBy, String replacedByEmailId) {
        this.govUserId = govUserId;
        this.transferDt = transferDt;
        this.createdBy = createdBy;
        this.remarks = remarks;
        this.employeeName = employeeName;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.finPowerBy = finPowerBy;
        this.employeeId = employeeId;
        this.action = action;
        this.emailId = emailId;
        this.replacedBy = replacedBy;
        this.replacedByEmailId = replacedByEmailId;
    }

    @Id
    @GeneratedValue(generator = "employeeTrasferSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "employeeTrasferSequence", sequenceName = "employeeTrasfer_sequence", allocationSize = 25)
    @Column(name = "govUserId", unique = true, nullable = false)
    public int getGovUserId() {
        return this.govUserId;
    }

    public void setGovUserId(int govUserId) {
        this.govUserId = govUserId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    public TblLoginMaster getTblLoginMaster() {
        return this.tblLoginMaster;
    }

    public void setTblLoginMaster(TblLoginMaster tblLoginMaster) {
        this.tblLoginMaster = tblLoginMaster;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transferDt", nullable = false, length = 23)
    public Date getTransferDt() {
        return this.transferDt;
    }

    public void setTransferDt(Date transferDt) {
        this.transferDt = transferDt;
    }

    @Column(name = "createdBy", nullable = false)
    public int getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "remarks", nullable = false, length = 2000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "employeeName", nullable = false, length = 200)
    public String getEmployeeName() {
        return this.employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Column(name = "employeeNameBangla")
    public byte[] getEmployeeNameBangla() {
        return this.employeeNameBangla;
    }

    public void setEmployeeNameBangla(byte[] employeeNameBangla) {
        this.employeeNameBangla = employeeNameBangla;
    }

    @Column(name = "mobileNo", nullable = false, length = 20)
    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Column(name = "nationalId", nullable = false, length = 30)
    public String getNationalId() {
        return this.nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    @Column(name = "userEmployeeId", nullable = false, length = 100)
    public String getUserEmployeeId() {
        return this.userEmployeeId;
    }

    public void setUserEmployeeId(String userEmployeeId) {
        this.userEmployeeId = userEmployeeId;
    }

    @Column(name = "contactAddress", nullable = false, length = 1000)
    public String getContactAddress() {
        return this.contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @Column(name = "finPowerBy", nullable = false, length = 10)
    public String getFinPowerBy() {
        return this.finPowerBy;
    }

    public void setFinPowerBy(String finPowerBy) {
        this.finPowerBy = finPowerBy;
    }

    @Column(name = "employeeId", nullable = false)
    public int getEmployeeId() {
        return this.employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Column(name = "action", nullable = false, length = 20)
    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "emailId", nullable = false, length = 150)
    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Column(name = "replacedBy", nullable = false, length = 200)
    public String getReplacedBy() {
        return this.replacedBy;
    }

    public void setReplacedBy(String replacedBy) {
        this.replacedBy = replacedBy;
    }

    @Column(name = "replacedByEmailId", nullable = false, length = 150)
    public String getReplacedByEmailId() {
        return this.replacedByEmailId;
    }

    public void setReplacedByEmailId(String replacedByEmailId) {
        this.replacedByEmailId = replacedByEmailId;
    }

    @Column(name = "isCurrent", nullable = false, length = 3)
    public String getIsCurrent() {
        return this.isCurrent;
    }

    public void setIsCurrent(String isCurrent) {
        this.isCurrent = isCurrent;
    }

    @Column(name = "comments", length = 2000)
    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employeeTrasfer")
    public Set<TblComplaintMaster> getTblComplaintMasters() {
        return tblComplaintMasters;
    }

    public void setTblComplaintMasters(Set<TblComplaintMaster> tblComplaintMasters) {
        this.tblComplaintMasters = tblComplaintMasters;
    }
}
