package com.cptu.egp.eps.model.table;
// Generated Nov 30, 2010 2:51:43 PM by Hibernate Tools 3.2.1.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ReviewPanel", schema = "dbo")
public class TblReviewPanel implements java.io.Serializable {

    private int reviewPanelId;
    private String reviewPanelName;
    private String reviewMembers;
    //userId
    private int userId;
    
  
    @Id
    @GeneratedValue(generator = "ReviewPanelSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "ReviewPanelSequence", sequenceName = "reviewPanel_sequence", allocationSize = 25)
    @Column(name = "reviewPanelId", unique = true, nullable = false)
   	public int getReviewPanelId() {
		return reviewPanelId;
	}

	public void setReviewPanelId(int reviewPanelId) {
		this.reviewPanelId = reviewPanelId;
	}
	 @Column(name = "reviewPanelName")
	public String getReviewPanelName() {
		return reviewPanelName;
	}

	public void setReviewPanelName(String reviewPanelName) {
		this.reviewPanelName = reviewPanelName;
	}
	 @Column(name = "reviewMembers")
	public String getReviewMembers() {
		return reviewMembers;
	}

	public void setReviewMembers(String reviewMembers) {
		this.reviewMembers = reviewMembers;
	}
	
	@Column(name = "userId") 
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
