package com.cptu.egp.eps.model.table;
// Generated Nov 11, 2010 9:15:24 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblTenderPhasing generated by hbm2java
 */
@Entity
@Table(name = "tbl_TenderPhasing", schema = "dbo")
public class TblTenderPhasing  implements java.io.Serializable {

     private int tenderPhasingId;
     private TblTenderMaster tblTenderMaster;
     private String phasingRefNo;
     private String phasingOfService;
     private String location;
     private Date indStartDt;
     private Date indEndDt;

    public TblTenderPhasing() {
    }

    //Constructor
    public TblTenderPhasing(int tenderPhasingId) {
        this.tenderPhasingId = tenderPhasingId;
    }

    public TblTenderPhasing(int tenderPhasingId, TblTenderMaster tblTenderMaster, String phasingRefNo, String phasingOfService, String location, Date indStartDt, Date indEndDt) {
       this.tenderPhasingId = tenderPhasingId;
       this.tblTenderMaster = tblTenderMaster;
       this.phasingRefNo = phasingRefNo;
       this.phasingOfService = phasingOfService;
       this.location = location;
       this.indStartDt = indStartDt;
       this.indEndDt = indEndDt;
    }

     @Id
     @GeneratedValue(generator = "TenderPhasingSequence", strategy = GenerationType.IDENTITY)
     @SequenceGenerator(name = "TenderPhasingSequence", sequenceName = "tenderPhasing_sequence", allocationSize = 25)
    @Column(name = "tenderPhasingId", unique = true, nullable = false)
    public int getTenderPhasingId() {
        return this.tenderPhasingId;
    }

    public void setTenderPhasingId(int tenderPhasingId) {
        this.tenderPhasingId = tenderPhasingId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderId", nullable = false)
    public TblTenderMaster getTblTenderMaster() {
        return this.tblTenderMaster;
    }

    public void setTblTenderMaster(TblTenderMaster tblTenderMaster) {
        this.tblTenderMaster = tblTenderMaster;
    }

    @Column(name = "phasingRefNo", nullable = false, length = 150)
    public String getPhasingRefNo() {
        return this.phasingRefNo;
    }

    public void setPhasingRefNo(String phasingRefNo) {
        this.phasingRefNo = phasingRefNo;
    }

    @Column(name = "phasingOfService", nullable = false, length = 500)
    public String getPhasingOfService() {
        return this.phasingOfService;
    }

    public void setPhasingOfService(String phasingOfService) {
        this.phasingOfService = phasingOfService;
    }

    @Column(name = "location", nullable = false, length = 100)
    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "indStartDt", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getIndStartDt() {
        return this.indStartDt;
    }

    public void setIndStartDt(Date indStartDt) {
        this.indStartDt = indStartDt;
    }

    @Column(name = "indEndDt", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getIndEndDt() {
        return this.indEndDt;
    }

    public void setIndEndDt(Date indEndDt) {
        this.indEndDt = indEndDt;
    }
}
