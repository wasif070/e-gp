package com.cptu.egp.eps.model.table;
// Generated Jan 24, 2011 10:53:41 PM by Hibernate Tools 3.2.1.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblUserPrefrence generated by hbm2java
 */
@Entity
@Table(name = "tbl_UserPrefrence", schema = "dbo")
public class TblUserPrefrence  implements java.io.Serializable {

     private int userPrefId;
     private int userId;
     private String emailAlert = "";
     private String smsAlert = "";

    public TblUserPrefrence() {
    }

    public TblUserPrefrence(int userPrefId, int userId, String emailAlert, String smsAlert) {
       this.userPrefId = userPrefId;
       this.userId = userId;
       this.emailAlert = emailAlert;
       this.smsAlert = smsAlert;
    }

    @Id
     @GeneratedValue(generator = "UserPrefrenceSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "UserPrefrenceSequence", sequenceName = "userPrefrence_sequence", allocationSize = 25)
    @Column(name = "userPrefId", unique = true, nullable = false)
    public int getUserPrefId() {
        return this.userPrefId;
    }

    public void setUserPrefId(int userPrefId) {
        this.userPrefId = userPrefId;
    }

    @Column(name = "userId", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "emailAlert", nullable = false, length = 150)
    public String getEmailAlert() {
        return this.emailAlert;
    }

    public void setEmailAlert(String emailAlert) {
        this.emailAlert = emailAlert;
    }

    @Column(name = "smsAlert", nullable = false, length = 150)
    public String getSmsAlert() {
        return this.smsAlert;
    }

    public void setSmsAlert(String smsAlert) {
        this.smsAlert = smsAlert;
    }
}
