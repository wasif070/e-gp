package com.cptu.egp.eps.model.table;
// Generated Nov 6, 2010 4:59:25 PM by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * TblPccHeader generated by hbm2java
 */
@Entity
@Table(name = "tbl_PccHeader", schema = "dbo")
public class TblPccHeader  implements java.io.Serializable {

     private int pccHeaderId;
     private String pccHeaderName;
     private Set<TblPccSubClause> tblPccSubClauses = new HashSet<TblPccSubClause>();

    public TblPccHeader() {
    }

    //Constructor
    public TblPccHeader(int pccHeaderId) {
        this.pccHeaderId = pccHeaderId;
    }

    public TblPccHeader(int pccHeaderId, String pccHeaderName) {
        this.pccHeaderId = pccHeaderId;
        this.pccHeaderName = pccHeaderName;
    }

    public TblPccHeader(int pccHeaderId, String pccHeaderName, Set tblPccSubClauses) {
       this.pccHeaderId = pccHeaderId;
       this.pccHeaderName = pccHeaderName;
       this.tblPccSubClauses = tblPccSubClauses;
    }

    @Id
     @GeneratedValue(generator = "PccHeaderTypeSequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "PccHeaderTypeSequence", sequenceName = "pccHeader_sequence", allocationSize = 25)
    @Column(name = "pccHeaderId", unique = true, nullable = false)
    public int getPccHeaderId() {
        return this.pccHeaderId;
    }

    public void setPccHeaderId(int pccHeaderId) {
        this.pccHeaderId = pccHeaderId;
    }

    @Column(name = "pccHeaderName", nullable = false, length = 100)
    public String getPccHeaderName() {
        return this.pccHeaderName;
    }

    public void setPccHeaderName(String pccHeaderName) {
        this.pccHeaderName = pccHeaderName;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tblPccHeader")
    public Set<TblPccSubClause> getTblPccSubClauses() {
        return this.tblPccSubClauses;
    }

    public void setTblPccSubClauses(Set<TblPccSubClause> tblPccSubClauses) {
        this.tblPccSubClauses = tblPccSubClauses;
    }
}
