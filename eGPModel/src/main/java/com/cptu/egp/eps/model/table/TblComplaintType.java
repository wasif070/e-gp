package com.cptu.egp.eps.model.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_CMS_ComplaintTypes")
public class TblComplaintType implements java.io.Serializable{

	private int complaintTypeId;
	private String complaintType;

	@Id
	@GeneratedValue(generator = "ComplaintTypeSequence", strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "ComplaintTypeSequence", sequenceName = "ComplaintTypeSequence")
	@Column(name = "complaintTypeId", unique = true, nullable = false)
	public int getComplaintTypeId() {
		return complaintTypeId;
	}
	public void setComplaintTypeId(int complaintTypeId) {
		this.complaintTypeId = complaintTypeId;
	}
	
	@Column(name = "complaintType")
	public String getComplaintType() {
		return complaintType;
	}
	public void setComplaintType(String complaintType) {
		this.complaintType = complaintType;
	}
	
}
