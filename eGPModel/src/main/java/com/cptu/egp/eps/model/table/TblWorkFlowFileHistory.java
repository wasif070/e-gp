package com.cptu.egp.eps.model.table;
// Generated Oct 25, 2010 9:33:12 AM by Hibernate Tools 3.2.1.GA

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblWorkFlowFileHistory generated by hbm2java
 */
@Entity
@Table(name = "tbl_WorkFlowFileHistory", schema = "dbo")
public class TblWorkFlowFileHistory  implements java.io.Serializable {

     private int wfHistoryId;
     private String moduleName;
     private String eventName;
     private String activityName;
     private short activityId;
     private int childId;
     private int objectId;
     private Date processDate;
     private Date endDate;
     private int fromUserId;
     private int toUserId;
     private String fileSentFrom;
     private String fileSentTo;
     private byte wfRoleid;
     private String fileOnHand;
     private String action;
     private String workflowDirection;
     private String remarks;
     private String wfTrigger;

    public TblWorkFlowFileHistory() {
    }

    public TblWorkFlowFileHistory(int wfHistoryId) {
        this.wfHistoryId = wfHistoryId;
    }

    public TblWorkFlowFileHistory(int wfHistoryId, TblWorkFlowFileOnHand tblWorkFlowFileOnHand, String moduleName, String eventName, String activityName, short activityId, int childId, int objectId, Date processDate, Date endDate, int fromUserId, int toUserId, String fileSentFrom, String fileSentTo, byte wfRoleid, String fileOnHand, String action, String workflowDirection, String remarks, String wfTrigger) {
       this.wfHistoryId = wfHistoryId;
       this.moduleName = moduleName;
       this.eventName = eventName;
       this.activityName = activityName;
       this.activityId = activityId;
       this.childId = childId;
       this.objectId = objectId;
       this.processDate = processDate;
       this.endDate = endDate;
       this.fromUserId = fromUserId;
       this.toUserId = toUserId;
       this.fileSentFrom = fileSentFrom;
       this.fileSentTo = fileSentTo;
       this.wfRoleid = wfRoleid;
       this.fileOnHand = fileOnHand;
       this.action = action;
       this.workflowDirection = workflowDirection;
       this.remarks = remarks;
       this.wfTrigger = wfTrigger;
    }

    @Id
    @GeneratedValue(generator = "WorkFlowFileHistorySequence", strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = "WorkFlowFileHistorySequence", sequenceName = "workFlowFileHistory_sequence", allocationSize = 25)
    @Column(name = "wfHistoryId", unique = true, nullable = false)
    public int getWfHistoryId() {
        return this.wfHistoryId;
    }

    public void setWfHistoryId(int wfHistoryId) {
        this.wfHistoryId = wfHistoryId;
    }

    @Column(name = "moduleName", nullable = false, length = 50)
    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Column(name = "eventName", nullable = false, length = 50)
    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Column(name = "activityName", nullable = false, length = 50)
    public String getActivityName() {
        return this.activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Column(name = "activityId", nullable = false)
    public short getActivityId() {
        return this.activityId;
    }

    public void setActivityId(short activityId) {
        this.activityId = activityId;
    }

    @Column(name = "childId", nullable = false)
    public int getChildId() {
        return this.childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    @Column(name = "objectId", nullable = false)
    public int getObjectId() {
        return this.objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "processDate", nullable = false, length = 16)
    public Date getProcessDate() {
        return this.processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate", nullable = false, length = 16)
    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "fromUserId", nullable = false)
    public int getFromUserId() {
        return this.fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }

    @Column(name = "toUserId", nullable = false)
    public int getToUserId() {
        return this.toUserId;
    }

    public void setToUserId(int toUserId) {
        this.toUserId = toUserId;
    }

    @Column(name = "fileSentFrom", nullable = false, length = 400)
    public String getFileSentFrom() {
        return this.fileSentFrom;
    }

    public void setFileSentFrom(String fileSentFrom) {
        this.fileSentFrom = fileSentFrom;
    }

    @Column(name = "fileSentTo", nullable = false, length = 400)
    public String getFileSentTo() {
        return this.fileSentTo;
    }

    public void setFileSentTo(String fileSentTo) {
        this.fileSentTo = fileSentTo;
    }

    @Column(name = "wfRoleid", nullable = false)
    public byte getWfRoleid() {
        return this.wfRoleid;
    }

    public void setWfRoleid(byte wfRoleid) {
        this.wfRoleid = wfRoleid;
    }

    @Column(name = "fileOnHand", nullable = false, length = 3)
    public String getFileOnHand() {
        return this.fileOnHand;
    }

    public void setFileOnHand(String fileOnHand) {
        this.fileOnHand = fileOnHand;
    }

    @Column(name = "action", nullable = false, length = 20)
    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "workflowDirection", nullable = false, length = 4)
    public String getWorkflowDirection() {
        return this.workflowDirection;
    }

    public void setWorkflowDirection(String workflowDirection) {
        this.workflowDirection = workflowDirection;
    }

    @Column(name = "remarks", nullable = false, length = 1000)
    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "wfTrigger", nullable = false, length = 10)
    public String getWfTrigger() {
        return this.wfTrigger;
    }

    public void setWfTrigger(String wfTrigger) {
        this.wfTrigger = wfTrigger;
    }
}
