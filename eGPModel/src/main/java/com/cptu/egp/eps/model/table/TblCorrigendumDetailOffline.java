package com.cptu.egp.eps.model.table;
// Generated 03-Sep-2012 13:08:09 by Hibernate Tools 3.2.1.GA


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="tbl_CorrigendumDetailOffline"
    ,schema="dbo"
  
)
public class TblCorrigendumDetailOffline  implements java.io.Serializable {


     private int corrigendumOlid;
     //private int tenderOfflineId;
     private TblTenderDetailsOffline tblTenderDetailsOffline;
     private String fieldName;
     private String oldValue;
     private String newValue;
     private String corrigendumStatus;
     private Integer userId;
     private Date insertUpdateDate;

     private String LotorPhaseIdentity;
     private int CorNo;

    public TblCorrigendumDetailOffline() {
    }


    public TblCorrigendumDetailOffline(int corrigendumOlid) {
        this.corrigendumOlid = corrigendumOlid;
    }


    public TblCorrigendumDetailOffline(int corrigendumOlid, TblTenderDetailsOffline tblTenderDetailsOffline, String fieldName, String oldValue, String newValue) {
        this.corrigendumOlid = corrigendumOlid;
        //this.tenderOfflineId = tenderOfflineId;
        this.tblTenderDetailsOffline = tblTenderDetailsOffline;
        this.fieldName = fieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }
    public TblCorrigendumDetailOffline(int corrigendumOlid, TblTenderDetailsOffline tblTenderDetailsOffline, String LotorPhaseIdentity, String fieldName, String oldValue, String newValue, String corrigendumStatus, Integer CorNo, Integer userId, Date insertUpdateDate) {
       this.corrigendumOlid = corrigendumOlid;
       //this.tenderOfflineId = tenderOfflineId;
       this.tblTenderDetailsOffline = tblTenderDetailsOffline;
       this.fieldName = fieldName;
       this.oldValue = oldValue;
       this.newValue = newValue;
       this.corrigendumStatus = corrigendumStatus;
       this.userId = userId;
       this.insertUpdateDate = insertUpdateDate;
       this.LotorPhaseIdentity = LotorPhaseIdentity;
       this.CorNo = CorNo;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="corrigendumOLId", unique=true, nullable=false)
    public int getCorrigendumOlid() {
        return this.corrigendumOlid;
    }

    public void setCorrigendumOlid(int corrigendumOlid) {
        this.corrigendumOlid = corrigendumOlid;
    }

    @Column(name="fieldName", nullable=false, length=50)
    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Column(name="oldValue", nullable=false, length=500)
    public String getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Column(name="newValue", nullable=false, length=500)
    public String getNewValue() {
        return this.newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Column(name="CorrigendumStatus", length=15)
    public String getCorrigendumStatus() {
        return this.corrigendumStatus;
    }

    public void setCorrigendumStatus(String corrigendumStatus) {
        this.corrigendumStatus = corrigendumStatus;
    }

    @Column(name="userID")
    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="insertUpdateDate", length=16)
    public Date getInsertUpdateDate() {
        return this.insertUpdateDate;
    }

    public void setInsertUpdateDate(Date insertUpdateDate) {
        this.insertUpdateDate = insertUpdateDate;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenderOfflineId", nullable = false)

    public TblTenderDetailsOffline getTblCorrigendumDetailOffline() {
        return this.tblTenderDetailsOffline;
    }

        public void setTblCorrigendumDetailOffline(TblTenderDetailsOffline tblTenderDetailsOffline) {
        this.tblTenderDetailsOffline = tblTenderDetailsOffline;
    }

    /**
     * @return the LotorPhaseIdentity
     */
    public String getLotorPhaseIdentity() {
        return LotorPhaseIdentity;
    }

    /**
     * @param LotorPhaseIdentity the LotorPhaseIdentity to set
     */
    public void setLotorPhaseIdentity(String LotorPhaseIdentity) {
        this.LotorPhaseIdentity = LotorPhaseIdentity;
    }

    /**
     * @return the CorNo
     */
    public int getCorNo() {
        return CorNo;
    }

    /**
     * @param CorNo the CorNo to set
     */
    public void setCorNo(int CorNo) {
        this.CorNo = CorNo;
    }

}